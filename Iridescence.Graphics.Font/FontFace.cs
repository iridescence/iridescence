﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents an abstract font face.
	/// </summary>
	public abstract class FontFace
	{
		#region Properties

		/// <summary>
		/// Gets or sets the name of the font.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets a font with the specified size.
		/// </summary>
		/// <param name="size"></param>
		/// <returns></returns>
		public Font this[double size] => this.GetFont(size);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Font.
		/// </summary>
		protected FontFace(string name)
		{
			this.Name = name;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets an instance of this font face with the specified size.
		/// </summary>
		/// <param name="size">The size (height) of the font in pixels.</param>
		/// <returns></returns>
		public abstract Font GetFont(double size);

		/// <summary>
		/// Returns the <see cref="Glyph"/> for the specified code point.
		/// </summary>
		/// <param name="size">The font size.</param>
		/// <param name="codePoint">The unicode code point.</param>
		/// <returns>The glyph for the specified unicode code point, or null if that glyph does not exist in the font. You can use <see cref="GetUndefinedGlyph"/> in the latter case.</returns>
		public abstract Glyph GetGlyph(double size, uint codePoint);

		/// <summary>
		/// Returns the glyph for an undefined character code (e.g. a question-mark glyph, depends on the font and/or implementation).
		/// </summary>
		/// <param name="size">The font size.</param>
		/// <param name="codePoint">The code point that will be used for the glyph. This is only used for the <see cref="Glyph.CodePoint"/> property so that it can be mapped back to a character.</param>
		/// <returns>The 'undefined' glyph.</returns>
		public abstract Glyph GetUndefinedGlyph(double size, uint codePoint = 0);

		/// <summary>
		/// Enumerates all defined glyphs in the font.
		/// </summary>
		/// <param name="size">The font size.</param>
		/// <returns></returns>
		public abstract IEnumerable<Glyph> GetGlyphs(double size);

		/// <summary>
		/// Returns the kerning vector between two glyphs.
		/// </summary>
		/// <param name="leftGlyph"></param>
		/// <param name="rightGlyph"></param>
		/// <param name="size"></param>
		/// <returns></returns>
		public abstract Vector2D GetKerning(uint leftGlyph, uint rightGlyph, double size);

		public override string ToString()
		{
			return this.Name;
		}

		#endregion
	}
}
