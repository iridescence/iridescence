﻿using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents the rendered image of a <see cref="Glyph"/>.
	/// </summary>
	public abstract class GlyphImage : ReadOnlyImage
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value (in pixels) that determines how far the image is offset from the <see cref="Glyph"/>'s origin.
		/// </summary>
		public abstract Vector2 Offset { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="GlyphImage"/> class. 
		/// </summary>
		protected GlyphImage(Format format, int width, int height)
			: base(format, width, height, 1)
		{

		}

		#endregion

		#region Methods
		
		#endregion
	}
}
