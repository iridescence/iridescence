﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Returns how far to move the cursor after the first element.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="first"></param>
	/// <param name="second"></param>
	/// <returns></returns>
	public delegate double AdvanceFunction<in T>(T first, T second);

	/// <summary>
	/// Implements a text or flow layout builder.
	/// </summary>
	public sealed class TextLayoutBuilder<T> : IReadOnlyList<PositionedLayoutElement<T>> where T : ILayoutElement
	{
		#region Nested Types

		internal sealed class LineInfo
		{
			public int ElementOffset;
			public int ElementCount;
			public double Width;
			public double Height;
			public double MaxOffset;
			public double XOffset;
			
			public override string ToString()
			{
				return $"{this.ElementOffset}, {this.ElementCount}";
			}
		}

		internal struct ElementInfo
		{
			public LineInfo Line;
			public T Element;
			public Vector2D Position;
			public bool Ignore;

			public override string ToString()
			{
				return this.Element.ToString();
			}
		}
		
		#endregion

		#region Fields

		internal readonly List<LineInfo> Lines;
		internal ElementInfo[] Elements;
		internal int ElementCount;

		private RectangleD bounds;
		private Vector2D currentPosition;
		private bool skipPlaceholders;
		private T lastElement;
		private bool haveLastElement;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the size available for the layout.
		/// </summary>
		public Vector2D ContainerSize { get; }

		/// <summary>
		/// Gets the function that calculates how far apart two elements should be.
		/// </summary>
		public AdvanceFunction<T> Advance { get; }

		/// <summary>
		/// Gets or sets the wrap mode.
		/// </summary>
		public LayoutWrapMode WrapMode { get; }

		/// <summary>
		/// Gets the X alignment.
		/// </summary>
		public double XAlignment { get; }

		/// <summary>
		/// Gets the Y alignment.
		/// </summary>
		public double YAlignment { get; }
		
		/// <summary>
		/// Gets a value that indicates whether elements are reversed in their respective lines.
		/// </summary>
		public bool ReverseElements { get; }

		/// <summary>
		/// Gets a value that indicates whether the order of lines is reversed.
		/// </summary>
		public bool ReverseLines { get; }

		/// <summary>
		/// Gets the current bounding rectangle of the elements.
		/// </summary>
		public RectangleD Bounds => this.bounds;

		/// <summary>
		/// Gets the number of glyphs currently managed by the <see cref="TextLayouter"/>.
		/// </summary>
		public int Count => this.ElementCount;

		/// <summary>
		/// Gets the <see cref="PositionedLayoutElement{T}"/> at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public PositionedLayoutElement<T> this[int index]
		{
			get
			{
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException(nameof(index));

				Vector2D position = this.Elements[index].Position;
				LineInfo line = this.Elements[index].Line;

				if (this.ReverseLines)
				{
					// Reverse order of lines.
					position.Y = this.bounds.Height - position.Y - line.Height;
				}
				
				// Y alignment.
				position.Y += this.YAlignment * (this.ContainerSize.Y - this.bounds.Height);

				// Offset.
				position.Y += line.MaxOffset;
				
				// Alignment within line.
				position.Y += this.Elements[index].Element.LineAlignment * (line.Height - this.Elements[index].Element.LineSize);
				
				if (this.ReverseElements)
				{
					// Reverse order of elements.
					position.X = this.bounds.Width - position.X - this.Elements[index].Element.Advance;
					
					// X alignment.
					position.X -= line.XOffset;
				}
				else
				{
					position.X += line.XOffset;
				}
				
				return new PositionedLayoutElement<T>(this.Elements[index].Element, position);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="TextLayoutBuilder{T}"/>
		/// </summary>
		public TextLayoutBuilder(Vector2D containerSize, AdvanceFunction<T> advance, LayoutWrapMode wrapMode, double xAlignment, double yAlignment, bool reverseElements, bool reverseLines)
		{
			this.ContainerSize = containerSize;
			this.Advance = advance;
			this.WrapMode = wrapMode;
			this.XAlignment = xAlignment;
			this.YAlignment = yAlignment;
			this.ReverseElements = reverseElements;
			this.ReverseLines = reverseLines;

			this.Lines = new List<LineInfo>();
			this.Elements = new ElementInfo[0];

			this.Clear();
		}

		#endregion

		#region Methods

		private void newLine()
		{
			LineInfo currentLine = this.Lines[this.Lines.Count - 1];
			this.updateLine(currentLine);

			this.Lines.Add(new LineInfo
			{
				ElementOffset = currentLine.ElementOffset + currentLine.ElementCount,
			});

			this.currentPosition.X = 0.0f;
			this.currentPosition.Y += currentLine.Height;
		}

		private void updateLine(LineInfo line)
		{
			line.MaxOffset = 0.0f;
			line.Width = 0.0f;
			line.Height = 0.0f;

			// Compute line width and height.		

			for (int i = line.ElementOffset + line.ElementCount - 1; i >= line.ElementOffset; --i)
			{
				if (!this.Elements[i].Ignore)
				{
					line.Width = this.Elements[i].Position.X + this.Elements[i].Element.Advance;
					break;
				}
			}

			for (int i = line.ElementOffset; i < line.ElementOffset + line.ElementCount; ++i)
			{
				if (this.Elements[i].Element.LineSize > line.Height)
				{
					line.Height = this.Elements[i].Element.LineSize;
				}
				
				if (this.Elements[i].Element.Offset > line.MaxOffset)
				{
					line.MaxOffset = this.Elements[i].Element.Offset;
				}
			}

			line.XOffset = this.XAlignment * (this.ContainerSize.X - line.Width);
		}

		private void updateBounds()
		{
			if (this.Lines.Count == 0)
			{
				this.bounds.X = this.XAlignment * this.ContainerSize.X;
				this.bounds.Y = this.YAlignment * this.ContainerSize.Y;
				this.bounds.Width = 0.0f;
				this.bounds.Height = 0.0f;
			}
			else
			{
				this.bounds.Width = this.Lines.Max(line => line.Width);
				this.bounds.Height = this.Lines.Sum(line => line.Height);
				this.bounds.X = this.Lines.Min(line => line.XOffset);
				this.bounds.Y = this.YAlignment * (this.ContainerSize.Y - this.bounds.Height);
			}
		}

		/// <summary>
		/// Clears the builder.
		/// </summary>
		public void Clear()
		{
			this.Elements = new ElementInfo[0];
			this.ElementCount = 0;

			this.Lines.Clear();

			this.currentPosition = Vector2.Zero;
			this.skipPlaceholders = false;
			this.lastElement = default(T);
			this.haveLastElement = false;
		}

		/// <summary>
		/// Appends the elements.
		/// </summary>
		/// <param name="elements"></param>
		public void Append(IEnumerable<T> elements)
		{
			T[] array = elements.ToArray();
			Array.Resize(ref this.Elements, this.Elements.Length + array.Length);

			for (int i = 0; i < array.Length; ++i)
			{
				int elementIndex = this.ElementCount + i;
				LineInfo line;

				if (this.Lines.Count == 0)
				{
					line = new LineInfo();
					this.Lines.Add(line);
				}
				else
				{
					line = this.Lines[this.Lines.Count - 1];
				}

				T element = i < 0 ? this.Elements[elementIndex].Element : array[i];
				this.Elements[elementIndex].Line = line;
				this.Elements[elementIndex].Element = element;
				this.Elements[elementIndex].Position = this.currentPosition;
				++line.ElementCount;

				if (element.ForceBreak)
				{
					// Break.
					this.Elements[elementIndex].Ignore = element.IsPlaceholder;
					this.newLine();
					continue;
				}

				if (this.skipPlaceholders)
				{
					// Skip placeholders.
					if (element.IsPlaceholder)
					{
						this.Elements[elementIndex].Ignore = true;
						continue;
					}

					this.skipPlaceholders = false;
				}

				// Check if end of line is reached.
				if (this.currentPosition.X + element.Advance > this.ContainerSize.X && line.ElementCount > 1 && this.WrapMode != LayoutWrapMode.Off)
				{
					int j = 0;
					if (this.WrapMode == LayoutWrapMode.WordWrap)
					{
						// Find word separators in current line.
						int end = line.ElementOffset + line.ElementCount - 1;
						while (!this.Elements[end - j].Element.IsWordSeparator)
						{
							if (j == line.ElementCount - 1)
							{
								// We're at the end of the line.
								// Word end not found, just cut the word in two.
								j = 1;
								break;
							}

							++j;
						}

						// Find placeholders at end and ignore them.
						int k = j;
						while (this.Elements[end - k].Element.IsPlaceholder)
						{
							if (k == line.ElementCount - 1)
								break;
							this.Elements[end - k].Ignore = true;
							++k;
						}
					}
					else
					{
						j = 1;
					}

					// Remove the last word from the current line and go back.
					i -= j;
					line.ElementCount -= j;

					// Skip placeholders in the next line if we're using word wrap.
					if (this.WrapMode == LayoutWrapMode.WordWrap)
						this.skipPlaceholders = true;

					// Start new line.
					this.newLine();
					continue;
				}

				this.Elements[elementIndex].Ignore = false;

				// advance.
				float advance;
				if (this.haveLastElement)
				{
					advance = (float)this.Advance(this.lastElement, element);
				}
				else
				{
					advance = (float)element.Advance;
				}
				
				this.currentPosition.X += advance;
			}

			// Update last line.
			if (this.Lines.Count > 0)
				this.updateLine(this.Lines[this.Lines.Count - 1]);

			this.updateBounds();
			
			this.ElementCount += array.Length;
		}

		public void Append(T element)
		{
			this.Append(Enumerable.Repeat(element, 1));
		}
		
		public IEnumerator<PositionedLayoutElement<T>> GetEnumerator()
		{
			for (int i = 0; i < this.Count; ++i)
			{
				yield return this[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		#endregion
	}
}

