﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents an element that can be used in a <see cref="TextLayoutBuilder{T}"/>.
	/// </summary>
	public interface ILayoutElement
	{	
		/// <summary>
		/// Gets the element's advance distance.
		/// This is only used for advancing when there is no pair of elements for the <see cref="AdvanceFunction{T}"/>.
		/// </summary>
		double Advance { get; }

		/// <summary>
		/// Gets the line size of the element.
		/// </summary>
		double LineSize { get; }
	
		/// <summary>
		/// Gets a value that indicates whether the element is a placeholder.
		/// A placeholder does not cause breaks if they extend past the end of a line and are skipped if they are the first element of a new line caused by word wrap.
		/// </summary>
		bool IsPlaceholder { get; }
		
		/// <summary>
		/// Gets a value that indicates whether the element separates words.
		/// </summary>
		bool IsWordSeparator { get; }
		
		/// <summary>
		/// Gets a value that determines whether a new line must be started after this element.
		/// </summary>
		bool ForceBreak { get; }
		
		/// <summary>
		/// Gets the element's offset.
		/// All elements in a line are offset by the maximum offset of all elements in the line.
		/// This can be used to implement a "base line", useful for text layout.
		/// </summary>
		double Offset { get; }
		
		/// <summary>
		/// Gets a value that determines where the element is positioned within its line.
		/// </summary>
		double LineAlignment { get; }
	}
}
