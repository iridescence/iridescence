﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// An interface for carets in a layout.
	/// </summary>
	public abstract class Caret
	{
		#region Events

		/// <summary>
		/// Occurs whenever the caret was moved.
		/// </summary>
		public event EventHandler Moved; 

		#endregion

		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the position of the caret at the baseline.
		/// </summary>
		public abstract Vector2D Position { get; }

		/// <summary>
		/// Gets the index of the character to the left of the caret.
		/// </summary>
		/// <returns></returns>
		public abstract int Left { get; }
		
		/// <summary>
		/// Gets the index of the character to the right of the caret.
		/// </summary>
		/// <returns></returns>
		public abstract int Right { get; }

		#endregion

		#region Constructors

		#endregion

		#region Methods

		/// <summary>
		/// Moves the caret one position to the left.
		/// </summary>
		public abstract void GoLeft();

		/// <summary>
		/// Moves the caret one position to the right.
		/// </summary>
		public abstract void GoRight();

		/// <summary>
		/// Moves the caret one position upwards.
		/// </summary>
		public abstract void GoUp();

		/// <summary>
		/// Moves the caret one position downwards.
		/// </summary>
		public abstract void GoDown();
		
		/// <summary>
		/// Moves the caret to the left of the specified character.
		/// </summary>
		/// <param name="index"></param>
		public abstract void GoTo(int index);

		/// <summary>
		/// Moves the caret to the specified position.
		/// </summary>
		/// <param name="position"></param>
		public abstract void GoTo(Vector2D position);

		protected virtual void OnMoved(EventArgs e)
		{
			this.Moved?.Invoke(this, e);
		}

		#endregion
	}
}
