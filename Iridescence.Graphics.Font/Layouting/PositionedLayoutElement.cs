﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents a positioned layout element.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public struct PositionedLayoutElement<T> : IEquatable<PositionedLayoutElement<T>>
	{
		#region Fields

		/// <summary>
		/// The element.
		/// </summary>
		public readonly T Element;

		/// <summary>
		/// The position of the element.
		/// </summary>
		public readonly Vector2D Position;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public PositionedLayoutElement(T element, Vector2D position)
		{
			this.Element = element;
			this.Position = position;
		}

		#endregion

		#region Methods

		public bool Equals(PositionedLayoutElement<T> other)
		{
			return Equals(this.Element, other.Element) && this.Position.Equals(other.Position);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is PositionedLayoutElement<T> && this.Equals((PositionedLayoutElement<T>)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((this.Element != null ? this.Element.GetHashCode() : 0) * 397) ^ this.Position.GetHashCode();
			}
		}

		public static bool operator ==(PositionedLayoutElement<T> left, PositionedLayoutElement<T> right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(PositionedLayoutElement<T> left, PositionedLayoutElement<T> right)
		{
			return !left.Equals(right);
		}

		public override string ToString()
		{
			return $"'{this.Element}' @ {this.Position}";
		}

		#endregion
	}
}
