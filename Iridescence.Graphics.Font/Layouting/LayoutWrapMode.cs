﻿namespace Iridescence.Graphics.Font
{
	public enum LayoutWrapMode
	{
		/// <summary>
		/// No text wrapping. Characters will overflow the layout rectangle.
		/// </summary>
		Off,
		
		/// <summary>
		/// Simple wrapping. If a character does not fit into the current line, it is placed into the next line.
		/// </summary>
		LineWrap,

		/// <summary>
		/// Word wrapping. If a character does not fit into the current line, the whole word that contains that character is placed into the next line.
		/// </summary>
		WordWrap
	}
}