﻿using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	internal struct GlyphLayout
	{
		public LineLayout Line;
		public Glyph Glyph;
		public Vector2 Position;
		public bool Ignore;
		public float AdvanceX;

		public override string ToString()
		{
			return this.Glyph.ToString();
		}
	}
}
