﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Defines the text alignments.
	/// </summary>
	public enum TextAlignment
	{
		TopLeft,
		TopCenter,
		TopRight,
		CenterLeft,
		CenterCenter,
		CenterRight,
		BottomLeft,
		BottomCenter,
		BottomRight
	}
}
