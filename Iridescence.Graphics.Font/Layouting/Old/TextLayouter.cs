﻿using System.Collections;
using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents a text layouter, capable of positioning glyphs in a rectangle.
	/// </summary>
	public abstract class TextLayouter : IReadOnlyList<PositionedLayoutElement<Glyph>>
	{
		#region Properties

		/// <summary>
		/// Gets or sets the size available for the layouter.
		/// </summary>
		public abstract Vector2 LayoutSize { get; set; }

		/// <summary>
		/// Gets the current bounds of the text.
		/// </summary>
		public abstract Vector2 TextSize { get; }

		/// <summary>
		/// Gets or sets the default line height for new lines.
		/// </summary>
		public abstract float DefaultLineHeight { get; set; }

		/// <summary>
		/// Gets or sets the default ascender for new lines.
		/// </summary>
		public abstract float DefaultAscender { get; set; }

		/// <summary>
		/// Gets or sets the wrap mode.
		/// </summary>
		public abstract LayoutWrapMode WrapMode { get; set; }

		/// <summary>
		/// Gets or sets the alignment.
		/// </summary>
		public abstract TextAlignment Alignment { get; set; }

		/// <summary>
		/// Get or sets extra glyph spacing.
		/// </summary>
		public abstract float Spacing { get; set; }

		/// <summary>
		/// Get or sets minimum glyph spacing.
		/// </summary>
		public abstract float MinimumSpacing { get; set; }

		/// <summary>
		/// Gets the number of glyphs currently managed by the <see cref="TextLayouter"/>.
		/// </summary>
		public abstract int Count { get; }

		/// <summary>
		/// Gets the <see cref="PositionedLayoutElement{Glyph}"/> at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public abstract PositionedLayoutElement<Glyph> this[int index] { get; }
		
		#endregion

		#region Constructors

		#endregion

		#region Methods

		/// <summary>
		/// Clears all glyphs.
		/// </summary>
		public abstract void Clear();

		/// <summary>
		/// Appends the specified glyphs.
		/// </summary>
		/// <param name="glyphs"></param>
		public abstract void Append(IEnumerable<Glyph> glyphs);

		/// <summary>
		/// Creates a caret for text editing.
		/// </summary>
		/// <returns></returns>
		public abstract SimpleCaret CreateCaret();

		public abstract IEnumerator<PositionedLayoutElement<Glyph>> GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}
}
