﻿namespace Iridescence.Graphics.Font
{
	internal sealed class LineLayout
	{
		public int Offset;
		public int Count;
		public float Width;
		public float Height;
		public float Ascender;
		public float Descender;
		public float XOffset;
			
		public override string ToString()
		{
			return $"{this.Offset}, {this.Count}";
		}
	}
}
