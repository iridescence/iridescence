﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Implements a text layouter.
	/// Note that this implementation is rather limited and probably only works on latin and similar scripts.
	/// </summary>
	public sealed class SimpleTextLayouter : TextLayouter
	{
		#region Fields

		private Vector2 layoutSize;
		private Vector2 textSize;

		internal readonly List<LineLayout> Lines;
		internal GlyphLayout[] Characters;
		internal int CharacterCount;
		internal float VerticalOffset;

		private Vector2 currentPosition;
		private bool skipWhitespaces;
		private Glyph lastGlyph;

		#endregion

		#region Properties

		public override Vector2 LayoutSize
		{
			get => this.layoutSize;
			set
			{
				if (this.layoutSize != value)
				{
					this.layoutSize = value;

					this.updateSize();
					this.updateVerticalOffset();
				}
			}
		}

		public override Vector2 TextSize => this.textSize;

		public override float DefaultLineHeight { get; set; }

		public override float DefaultAscender { get; set; }

		public override LayoutWrapMode WrapMode { get; set; }

		public override TextAlignment Alignment { get; set; }

		public override float Spacing { get; set; }

		public override float MinimumSpacing { get; set; }

		public override int Count => this.CharacterCount;

		public override PositionedLayoutElement<Glyph> this[int index]
		{
			get
			{
				if (index < 0 || index >= this.Count)
					throw new ArgumentOutOfRangeException(nameof(index));

				Glyph glyph = this.Characters[index].Glyph;
				Vector2 position = this.Characters[index].Position;
				position.Y += this.VerticalOffset;
				position.X += this.Characters[index].Line.XOffset;

				return new PositionedLayoutElement<Glyph>(glyph, position);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new text layouter.
		/// </summary>
		public SimpleTextLayouter()
		{
			this.Lines = new List<LineLayout>();
			this.Characters = new GlyphLayout[0];

			this.Clear();
		}

		#endregion

		#region Methods

		private void newLine()
		{
			LineLayout currentLine = this.Lines[this.Lines.Count - 1];
			this.updateLine(currentLine);

			this.Lines.Add(new LineLayout
			{
				Offset = currentLine.Offset + currentLine.Count,
			});

			this.currentPosition.X = 0.0f;
			this.currentPosition.Y -= currentLine.Height;
		}

		private void updateVerticalOffset()
		{
			this.VerticalOffset = -this.Lines[0].Ascender;

			switch (this.Alignment)
			{
				case TextAlignment.TopLeft:
				case TextAlignment.TopCenter:
				case TextAlignment.TopRight:
					this.VerticalOffset += this.layoutSize.Y;
					break;

				case TextAlignment.BottomLeft:
				case TextAlignment.BottomCenter:
				case TextAlignment.BottomRight:
					this.VerticalOffset += this.textSize.Y;
					break;

				default:
					this.VerticalOffset += this.layoutSize.Y * 0.5f + this.textSize.Y * 0.5f;
					break;
			}
		}

		private void updateSize()
		{
			this.textSize = Vector2.Zero;
			foreach (LineLayout line in this.Lines)
			{
				if (line.Width > this.textSize.X)
					this.textSize.X = line.Width;

				this.textSize.Y += line.Height;
			}
		}

		private void updateLine(LineLayout line)
		{
			// line width.
			line.Width = 0.0f;
			if (line.Count > 0)
			{
				for (int i = line.Offset + line.Count - 1; i >= line.Offset; --i)
				{
					if (!this.Characters[i].Ignore)
					{
						line.Width = (this.Characters[i].Position.X + this.Characters[i].AdvanceX);
						break;
					}
				}
			}

			// line height/ascender/descender.
			if (line.Count == 0)
			{
				line.Height = this.DefaultLineHeight;
				line.Ascender = this.DefaultAscender;
				line.Descender = 0.0f;
			}
			else
			{
				line.Height = float.NegativeInfinity;
				line.Ascender = float.NegativeInfinity;
				line.Descender = float.PositiveInfinity;
				for (int i = 0; i < line.Count; i++)
				{
					Font font = this.Characters[line.Offset + i].Glyph.Font;

					if (font.LineHeight > line.Height)
						line.Height = (float)font.LineHeight;

					if (font.Ascender > line.Ascender)
						line.Ascender = (float)font.Ascender;

					if (font.Descender < line.Descender)
						line.Descender = (float)font.Descender;
				}
			}

			// xoffset.
			switch (this.Alignment)
			{
				case TextAlignment.TopLeft:
				case TextAlignment.CenterLeft:
				case TextAlignment.BottomLeft:
					line.XOffset = 0.0f;
					break;

				case TextAlignment.TopRight:
				case TextAlignment.CenterRight:
				case TextAlignment.BottomRight:
					line.XOffset = this.layoutSize.X - line.Width;
					break;

				default:
					line.XOffset = this.layoutSize.X * 0.5f - line.Width * 0.5f;
					break;
			}
		}

		public override void Clear()
		{
			this.Characters = new GlyphLayout[0];
			this.CharacterCount = 0;

			this.Lines.Clear();
			this.Lines.Add(new LineLayout());

			this.currentPosition = Vector2.Zero;
			this.skipWhitespaces = false;
			this.lastGlyph = null;
			
			this.updateLine(this.Lines[0]);
			this.updateSize();
			this.updateVerticalOffset();
		}

		public override void Append(IEnumerable<Glyph> glyphs)
		{
			Glyph[] array = glyphs.ToArray();
			Array.Resize(ref this.Characters, this.Characters.Length + array.Length);

			// lines & word wrap.
			for (int i = 0; i < array.Length; ++i)
			{
				int index = this.CharacterCount + i;
				LineLayout line = this.Lines[this.Lines.Count - 1];
				Glyph glyph = i < 0 ? this.Characters[index].Glyph : array[i];
				string c = glyph.Character;

				this.Characters[index].Line = line;
				this.Characters[index].Glyph = glyph;
				this.Characters[index].Position = this.currentPosition;
				++line.Count;

				// simple new line.
				if (c == "\n")
				{
					this.Characters[index].Ignore = true;
					this.newLine();
					continue;
				}

				// do we want to skip any whitespace?
				if (this.skipWhitespaces)
				{
					if (char.IsWhiteSpace(c[0]))
					{
						this.Characters[index].Ignore = true;
						continue;
					}

					this.skipWhitespaces = false;
				}
				
				// end of line reached.
				if (this.currentPosition.X + glyph.Advance.X > this.layoutSize.X && line.Count > 1 && this.WrapMode != LayoutWrapMode.Off)
				{
					// find whitespace in current line.
					int j = 0;
					if (this.WrapMode == LayoutWrapMode.WordWrap)
					{
						int end = line.Offset + line.Count - 1;
						while (!char.IsWhiteSpace(this.Characters[end - j].Glyph.Character[0]))
						{
							if (j == line.Count - 1)
							{
								// we're at the end of the line.
								// word end not found, just cut the word in two.
								j = 1;
								break;
							}

							++j;
						}

						// find whitespaces at end and ignore them.
						int k = j;
						while (char.IsWhiteSpace(this.Characters[end - k].Glyph.Character[0]))
						{
							if (k == line.Count - 1)
								break;
							this.Characters[end - k].Ignore = true;
							++k;
						}
					}
					else
					{
						j = 1;
					}

					// remove the last word from the current line and go back.
					i -= j;
					line.Count -= j;

					// skip whitespaces in the next line if we're using word wrap.
					if (this.WrapMode == LayoutWrapMode.WordWrap)
						this.skipWhitespaces = true;

					// go to next line.
					this.newLine();
					continue;
				}
			
				this.Characters[index].Ignore = false;

				// advance.
				Vector2 advance;
				advance.X = (float)glyph.Advance.X;
				advance.Y = 0.0f;
				if (this.lastGlyph != null)
				{
					Vector2 kerning = (Vector2)this.lastGlyph.Font.GetKerning(this.lastGlyph.CodePoint, glyph.CodePoint);
					Vector2.Add(advance, kerning, out advance);
				}

				// add spacing
				advance.X += this.Spacing;

				// abide by minimum spacing
				advance.X = Utility.Max(this.MinimumSpacing, advance.X);

				this.Characters[index].AdvanceX = advance.X;

				Vector2.Add(this.currentPosition, advance, out this.currentPosition);
				this.lastGlyph = glyph;
			}

			foreach(LineLayout line in this.Lines)
				this.updateLine(line);

			this.updateSize();
			this.updateVerticalOffset();

			this.CharacterCount += array.Length;
		}

		public override SimpleCaret CreateCaret()
		{
			return new SimpleCaret(this);
		}

		public override IEnumerator<PositionedLayoutElement<Glyph>> GetEnumerator()
		{
			for (int i = 0; i < this.Count; ++i)
			{
				yield return this[i];
			}
		}

		#endregion
	}
}

