﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Caret implementation for <see cref="SimpleTextLayouter"/>.
	/// </summary>
	public sealed class SimpleCaret
	{
		#region Events

		/// <summary>
		/// Occurs whenever the caret was moved.
		/// </summary>
		public event EventHandler Moved; 

		#endregion

		#region Fields

		private readonly SimpleTextLayouter layouter;

		private int index;
		private bool isRightOfCharacter;

		#endregion

		#region Properties

		public Vector2D Position
		{
			get
			{
				this.validateIndex();

				Vector2D position;

				if (this.layouter.CharacterCount == 0)
				{
					position.X = this.layouter.Lines[0].XOffset;
					position.Y = this.layouter.VerticalOffset;
				}
				else
				{
					PositionedLayoutElement<Glyph> glyph = this.layouter[this.index];
					position = glyph.Position;

					if (this.isRightOfCharacter)
					{
						// Caret is to the right, go there.
						if (this.layouter[this.index].Element.Character == "\n")
						{
							// If the current character is a new line, being to the right of it means being on the next line.
							LineLayout line = this.layouter.Characters[this.index].Line;
							position.X = line.XOffset;
							position.Y -= line.Height;
						}
						else
						{
							// Advance to the right of the character.
							position.X += glyph.Element.Advance.X;
						}
					}
				}

				return position;
			}
		}

		public int Left
		{
			get
			{
				this.validateIndex();

				int left;
				if (this.isRightOfCharacter)
					left = this.index;
				else
					left = this.index - 1;

				if (left >= 0 && char.IsLowSurrogate((char)this.layouter.Characters[left].Glyph.CodePoint))
					--left;

				return left;
			}
		}

		public int Right
		{
			get
			{
				this.validateIndex();

				int right;
				if (this.isRightOfCharacter)
					right = this.index + 1;
				else
					right = this.index;

				if (right < this.layouter.CharacterCount && char.IsHighSurrogate((char)this.layouter.Characters[right].Glyph.CodePoint))
					++right;

				return right;
			}
		}

		#endregion

		#region Constructors

		public SimpleCaret(SimpleTextLayouter layouter)
		{
			this.layouter = layouter;
		}

		#endregion

		#region Methods

		private void validateIndex()
		{
			if (this.layouter.CharacterCount == 0)
				this.goTo(0, false);
			else if (this.layouter.CharacterCount > 0 && this.index >= this.layouter.CharacterCount)
				this.goToEnd();
		}

		public void GoLeft()
		{
			this.validateIndex();
			if (this.isRightOfCharacter)
			{
				this.goTo(this.index, false);
			}
			else
			{
				if (this.index == 0)
					return;

				int newIndex = this.index;
				LineLayout currentLine = this.layouter.Characters[this.index].Line;

				// Find next non-ignored character.
				do
				{
					--newIndex;
					if (newIndex < 0)
					{
						this.goTo(0, false);
						return;
					}
				} while (this.layouter.Characters[newIndex].Ignore && this.layouter.Characters[newIndex].Line == currentLine);

				LineLayout newLine = this.layouter.Characters[newIndex].Line;

				if (currentLine == newLine)
				{
					// New index is still on current line, go to the left of it.
					this.goTo(newIndex, false, true);
				}
				else
				{
					if (!this.goToEndOfLine(newLine))
						this.goToStartOfLine(newLine);
				}
			}
		}

		public void GoRight()
		{
			this.validateIndex();
			int newIndex = this.index;

			if (this.index >= this.layouter.CharacterCount)
				return;

			LineLayout currentLine = this.layouter.Characters[this.index].Line;

			// Find next non-ignored character.
			do
			{
				++newIndex;
				if (newIndex >= this.layouter.CharacterCount)
				{
					this.goToEnd();
					return;
				}
			} while (this.layouter.Characters[newIndex].Ignore && this.layouter.Characters[newIndex].Line == currentLine);

			LineLayout newLine = this.layouter.Characters[newIndex].Line;

			if (currentLine == newLine)
			{
				// New index is still on current line, go to the left of it.
				this.goTo(newIndex, false, true);
			}
			else
			{
				// New index is on left line. If we are already to the right of the last char, go to beginning of next line.
				if (this.isRightOfCharacter)
				{
					this.goToStartOfLine(newLine);
				}
				else
				{
					// Otherwise, try to go to the end of the current line, and if that is empty, go to the start of next line.
					if (!this.goToEndOfLine(currentLine))
						this.goToStartOfLine(newLine);
				}
			}
		}

		public void GoUp()
		{
			this.validateIndex();

			if (this.layouter.CharacterCount == 0)
			{
				this.goTo(0, false);
				return;
			}

			LineLayout current = this.layouter.Characters[this.index].Line;
			int lineIndex = this.layouter.Lines.IndexOf(current);
			if (lineIndex <= 0)
			{
				this.goTo(0, false);
			}
			else
			{
				LineLayout newLine = this.layouter.Lines[lineIndex - 1];
				this.goToLine(newLine, this.Position.X);
			}
		}

		public void GoDown()
		{
			this.validateIndex();

			if (this.layouter.CharacterCount == 0)
			{
				this.goTo(0, false);
				return;
			}

			LineLayout current = this.layouter.Characters[this.index].Line;
			int lineIndex = this.layouter.Lines.IndexOf(current);
			if (lineIndex >= this.layouter.Lines.Count - 1)
			{
				this.goTo(this.layouter.Characters.Length - 1, true);
			}
			else
			{
				LineLayout newLine = this.layouter.Lines[lineIndex + 1];
				this.goToLine(newLine, this.Position.X);
			}
		}

		public void GoToStartOfLine()
		{
			this.validateIndex();
			if (this.layouter.CharacterCount == 0)
			{
				this.goTo(0, false);
				return;
			}

			LineLayout current = this.layouter.Characters[this.index].Line;
			this.goToStartOfLine(current);
		}

		public void GoToEndOfLine()
		{
			this.validateIndex();
			if (this.layouter.CharacterCount == 0)
			{
				this.goTo(0, false);
				return;
			}

			LineLayout current = this.layouter.Characters[this.index].Line;
			if (!this.goToEndOfLine(current))
				this.goToStartOfLine(current);
		}

		private void goTo(int index, bool right, bool dodgeRight = false)
		{
			if (index < 0 || (this.layouter.CharacterCount != 0 && index >= this.layouter.CharacterCount))
				throw new ArgumentOutOfRangeException(nameof(index));

			if (this.layouter.CharacterCount > 0)
			{
				char c = (char)this.layouter.Characters[index].Glyph.CodePoint;
				if(char.IsHighSurrogate(c) && dodgeRight)
				{
					if (index < this.layouter.CharacterCount - 1)
					{
						++index;
						right = true;
					}
				}
				else if(char.IsLowSurrogate(c) && !dodgeRight)
				{
					if (index > 0)
					{
						right = false;
						--index;
					}
				}
			}

			if (this.index != index || right != this.isRightOfCharacter)
			{
				this.index = index;
				this.isRightOfCharacter = right;
				this.OnMoved(EventArgs.Empty);
			}
		}

		private void OnMoved(EventArgs e)
		{
			this.Moved?.Invoke(this, e);
		}

		public void GoTo(int index)
		{
			if (index < 0)
			{
				this.goTo(0, false);
			}
			else if (index >= this.layouter.CharacterCount)
			{
				this.goToEnd();
			}
			else
			{
				this.goTo(index, false);
			}
		}

		private bool goToStartOfLine(LineLayout line)
		{
			for (int i = line.Offset; i < line.Offset + line.Count; ++i)
			{
				if (!this.layouter.Characters[i].Ignore)
				{
					this.goTo(i, false);
					return true;
				}
			}

			this.goTo(line.Offset, false);
			return false;
		}

		private bool goToEndOfLine(LineLayout line)
		{
			for (int i = line.Offset + line.Count - 1; i >= line.Offset; --i)
			{
				if (!this.layouter.Characters[i].Ignore)
				{
					this.goTo(i, true);
					return true;
				}
			}

			return false;
		}

		private void goToLine(LineLayout line, double x)
		{
			x -= line.XOffset;

			// Find the character at the specified index.
			for (int i = 0; i < line.Count; ++i)
			{
				if (this.layouter.Characters[line.Offset + i].Ignore)
					continue;

				Glyph g = this.layouter.Characters[line.Offset + i].Glyph;

				if (x <= g.Size.X * 0.5f)
				{
					this.goTo(line.Offset + i, false);
					return;
				}

				x -= g.Advance.X;
			}

			// Did not find a character, so we go to the end of the line, or if the line does not have any characters, to the start of it.
			if (!this.goToEndOfLine(line))
				this.goToStartOfLine(line);
		}

		private void goToEnd()
		{
			if (this.layouter.CharacterCount == 0)
				this.goTo(0, false);
			else
				this.goTo(this.layouter.CharacterCount - 1, true);
		}

		public void GoTo(Vector2D position)
		{
			if (this.layouter.Lines.Count == 0)
			{
				this.goTo(0, false);
				return;
			}

			position.Y -= this.layouter.VerticalOffset;

			LineLayout line = null;

			for (int i = 0; i < this.layouter.Lines.Count; ++i)
			{
				if (position.Y >= this.layouter.Lines[i].Descender)
				{
					line = this.layouter.Lines[i];
					break;
				}

				position.Y += this.layouter.Lines[i].Height;
			}

			if (line == null)
			{
				this.goToEnd();
				return;
			}

			this.goToLine(line, position.X);
		}

		#endregion
	}
}
