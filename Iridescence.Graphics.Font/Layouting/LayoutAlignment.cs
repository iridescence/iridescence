﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Specifies direction-independent alignment positions.
	/// </summary>
	public enum LayoutAlignment
	{
		Near,
		Center,
		Far,
	}
}
