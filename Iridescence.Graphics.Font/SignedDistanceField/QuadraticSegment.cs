﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	internal sealed class QuadraticSegment : EdgeSegment
	{
		#region Fields

		public readonly QuadraticBezier Bezier;

		#endregion

		#region Constructors

		public QuadraticSegment(QuadraticBezier bezier)
		{
			this.Bezier = bezier;
		}

		#endregion

		#region Methods

		public override Vector2D GetValue(double t) => this.Bezier.GetValue(t);

		public override Vector2D GetTangent(double t) => this.Bezier.GetTangent(t);

		public override IEnumerable<double> GetIntersections(in LineSegmentD lineSegment) => Curve.GetIntersections(this.Bezier, lineSegment, 0.1);

		public override int GetIntersections(in Vector2D r, IntersectionCallback callback) => ScanLine.CrossQuad(r, this.Bezier.Point1, this.Bezier.Point2, this.Bezier.Point3, 0, callback);

		public override SignedDistance GetDistance(Vector2D origin, out double t)
		{
			Vector2D qa = this.Bezier.Point1 - origin;
			Vector2D ab = this.Bezier.Point2 - this.Bezier.Point1;
			Vector2D br = this.Bezier.Point1 + this.Bezier.Point3 - this.Bezier.Point2 - this.Bezier.Point2;
			double a = Vector2D.Dot(br, br);
			double b = 3 * Vector2D.Dot(ab, br);
			double c = 2 * Vector2D.Dot(ab, ab) + Vector2D.Dot(qa, br);
			double d = Vector2D.Dot(qa, ab);
			SolveSolutions solutions = Utility.SolveCubic(a, b, c, d);

			double minDistance = Utility.NonZeroSign(Vector2D.Cross(ab, qa)) * qa.Length; // distance from A
			t = -Vector2D.Dot(qa, ab) / Vector2D.Dot(ab, ab);
			{
				double distance = Utility.NonZeroSign(Vector2D.Cross(this.Bezier.Point3 - this.Bezier.Point2, this.Bezier.Point3 - origin)) * (this.Bezier.Point3 - origin).Length; // distance from B
				if (Utility.Absolute(distance) < Utility.Absolute(minDistance))
				{
					minDistance = distance;
					t = Vector2D.Dot(origin - this.Bezier.Point2, this.Bezier.Point3 - this.Bezier.Point2) / Vector2D.Dot(this.Bezier.Point3 - this.Bezier.Point2, this.Bezier.Point3 - this.Bezier.Point2);
				}
			}
			for (int i = 0; i < solutions.Count; ++i)
			{
				if (solutions[i] > 0 && solutions[i] < 1)
				{
					Vector2D endpoint = this.Bezier.Point1 + 2 * solutions[i] * ab + solutions[i] * solutions[i] * br;
					double distance = Utility.NonZeroSign(Vector2D.Cross(this.Bezier.Point3 - this.Bezier.Point1, endpoint - origin)) * (endpoint - origin).Length;
					if (Utility.Absolute(distance) <= Utility.Absolute(minDistance))
					{
						minDistance = distance;
						t = solutions[i];
					}
				}
			}

			if (t >= 0.0 && t <= 1.0)
				return new SignedDistance(minDistance, 0);

			if (t < 0.5)
				return new SignedDistance(minDistance, Utility.Absolute(Vector2D.Dot(ab.Normalize(), qa.Normalize())));

			return new SignedDistance(minDistance, Utility.Absolute(Vector2D.Dot((this.Bezier.Point3 - this.Bezier.Point2).Normalize(), (this.Bezier.Point3 - origin).Normalize())));
		}

		public override void SplitInThirds(out EdgeSegment segment1, out EdgeSegment segment2, out EdgeSegment segment3)
		{
			this.Bezier.Split(1.0 / 3.0, out QuadraticBezier b1, out QuadraticBezier b2);
			b2.Split(0.5, out b2, out QuadraticBezier b3);
			segment1 = new QuadraticSegment(b1);
			segment2 = new QuadraticSegment(b2);
			segment3 = new QuadraticSegment(b3);
		}

		public override void GetBounds(out RectangleD bounds)
		{
			bounds = this.Bezier.GetBounds();
		}

		#endregion
	}
}
