﻿using System;
using System.Text;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents the signed distance from one point to a feature on a segment (e.g. a point on a bezier curve).
	/// </summary>
	internal readonly struct SignedDistance : IEquatable<SignedDistance>
	{
		#region Fields

		/// <summary>
		/// The signed distance.
		/// </summary>
		public readonly double Distance;

		/// <summary>
		/// The dot product of the tangent at the closest point on the segment and the vector pointing to the queried point
		/// (e.g. 0 if the vector from the segment to the point is perpendicular to the segment).
		/// </summary>
		public readonly double Dot;

		public static readonly SignedDistance PositiveInfinity = new SignedDistance(double.PositiveInfinity, 1.0);
		public static readonly SignedDistance NegativeInfinity = new SignedDistance(double.NegativeInfinity, 1.0);

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public SignedDistance(double distance, double dot)
		{
			this.Distance = distance;
			this.Dot = dot;
		}

		#endregion

		#region Methods

		public bool Equals(SignedDistance other)
		{
			return this.Distance.Equals(other.Distance) && this.Dot.Equals(other.Dot);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is SignedDistance && this.Equals((SignedDistance)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (this.Distance.GetHashCode() * 397) ^ this.Dot.GetHashCode();
			}
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();
			str.Append(this.Distance);

			if (this.Dot != 0.0)
			{
				str.Append(" (");
				str.Append(this.Dot);
				str.Append(")");
			}

			return str.ToString();
		}

		public static bool operator ==(SignedDistance left, SignedDistance right)
		{
			return left.Distance == right.Distance && left.Dot == right.Dot;
		}

		public static bool operator !=(SignedDistance left, SignedDistance right)
		{
			return left.Distance == right.Distance && left.Dot == right.Dot;
		}

		public static bool operator <(SignedDistance a, SignedDistance b)
		{
			return Utility.Absolute(a.Distance) < Utility.Absolute(b.Distance) || (Utility.Absolute(a.Distance) == Utility.Absolute(b.Distance) && a.Dot < b.Dot);
		}

		public static bool operator >(SignedDistance a, SignedDistance b)
		{
			return Utility.Absolute(a.Distance) > Utility.Absolute(b.Distance) || (Utility.Absolute(a.Distance) == Utility.Absolute(b.Distance) && a.Dot > b.Dot);
		}

		public static bool operator <=(SignedDistance a, SignedDistance b)
		{
			return Utility.Absolute(a.Distance) < Utility.Absolute(b.Distance) || (Utility.Absolute(a.Distance) == Utility.Absolute(b.Distance) && a.Dot <= b.Dot);
		}

		public static bool operator >=(SignedDistance a, SignedDistance b)
		{
			return Utility.Absolute(a.Distance) > Utility.Absolute(b.Distance) || (Utility.Absolute(a.Distance) == Utility.Absolute(b.Distance) && a.Dot >= b.Dot);
		}

		#endregion
	}
}
