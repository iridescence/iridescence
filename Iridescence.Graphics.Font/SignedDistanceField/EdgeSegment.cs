﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	internal delegate void IntersectionCallback(Vector2D p, int winding);

	internal abstract class EdgeSegment
	{
		#region Fields

		public EdgeColor Color;

		#endregion

		#region Constructors

		protected EdgeSegment()
		{
			this.Color = EdgeColor.White;
		}
		
		#endregion

		#region Methods

		public abstract Vector2D GetValue(double t);

		public abstract Vector2D GetTangent(double t);
	
		public abstract SignedDistance GetDistance(Vector2D origin, out double t);

		public abstract IEnumerable<double> GetIntersections(in LineSegmentD lineSegment);

		public abstract int GetIntersections(in Vector2D origin, IntersectionCallback callback);

		public void DistanceToPseudoDistance(ref SignedDistance distance, Vector2D origin, double t)
		{
			if (t < 0)
			{
				Vector2D dir = this.GetTangent(0);
				Vector2D aq = origin - this.GetValue(0);
				double ts = Vector2D.Dot(aq, dir);
				if (ts < 0)
				{
					double pseudoDistance = Vector2D.Cross(aq, dir);
					if (Utility.Absolute(pseudoDistance) <= Utility.Absolute(distance.Distance))
					{
						distance = new SignedDistance(pseudoDistance, 0.0);
					}
				}
			}
			else if (t > 1)
			{
				Vector2D dir = this.GetTangent(1);
				Vector2D bq = origin - this.GetValue(1);
				double ts = Vector2D.Dot(bq, dir);
				if (ts > 0)
				{
					double pseudoDistance = Vector2D.Cross(bq, dir);
					if (Utility.Absolute(pseudoDistance) <= Utility.Absolute(distance.Distance))
					{
						distance = new SignedDistance(pseudoDistance, 0.0);
					}
				}
			}
		}

		public abstract void SplitInThirds(out EdgeSegment segment1, out EdgeSegment segment2, out EdgeSegment segment3);

		public abstract void GetBounds(out RectangleD bounds);

		#endregion
	}
}
