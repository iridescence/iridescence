﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	internal sealed class LinearSegment : EdgeSegment
	{
		#region Fields

		public readonly LineSegmentD Line;

		#endregion

		#region Constructors

		public LinearSegment(LineSegmentD line)
		{
			this.Line = line;
		}
		
		#endregion

		#region Methods

		public override Vector2D GetValue(double t) => this.Line.GetValue(t);

		public override Vector2D GetTangent(double t) => (this.Line.End - this.Line.Start).Normalize();

		public override IEnumerable<double> GetIntersections(in LineSegmentD lineSegment) => this.Line.GetIntersections(lineSegment);

		public override int GetIntersections(in Vector2D r, IntersectionCallback callback) => ScanLine.CrossLine(r, this.Line.Start, this.Line.End, callback);

		public override SignedDistance GetDistance(Vector2D origin, out double t)
		{
			Vector2D aq = origin - this.Line.Start;
			Vector2D ab = this.Line.End - this.Line.Start;
			t = Vector2D.Dot(aq, ab) / Vector2D.Dot(ab, ab);
			Vector2D eq = (t > 0.5 ? this.Line.End : this.Line.Start) - origin;
			double endpointDistance = eq.Length;
			if (t > 0 && t < 1)
			{
				double orthoDistance = Vector2D.Dot(new Vector2D(ab.Y, -ab.X).Normalize(), aq);

				if (Utility.Absolute(orthoDistance) < endpointDistance)
					return new SignedDistance(orthoDistance, 0);
			}

			return new SignedDistance(Utility.NonZeroSign(Vector2D.Cross(aq, ab)) * endpointDistance, Utility.Absolute(Vector2D.Dot(ab.Normalize(), eq.Normalize())));
		}

		public override void SplitInThirds(out EdgeSegment segment1, out EdgeSegment segment2, out EdgeSegment segment3)
		{
			Vector2D p1 = this.Line.Start + (1.0 / 3.0) * (this.Line.End - this.Line.Start);
			Vector2D p2 = this.Line.Start + (2.0 / 3.0) * (this.Line.End - this.Line.Start);
			segment1 = new LinearSegment(new LineSegmentD(this.Line.Start, p1));
			segment2 = new LinearSegment(new LineSegmentD(p1, p2));
			segment3 = new LinearSegment(new LineSegmentD(p2, this.Line.End));
		}

		public override void GetBounds(out RectangleD bounds)
		{
			bounds = this.Line.GetBounds();
		}

		#endregion
	}
}
