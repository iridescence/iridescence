﻿/*
 * MULTI-CHANNEL SIGNED DISTANCE FIELD GENERATOR v1.5 (2017-07-23)
 * ---------------------------------------------------------------
 * A utility by Viktor Chlumsky, (c) 2014 - 2017
 *
 * The technique used to generate multi-channel distance fields in this code
 * has been developed by Viktor Chlumsky in 2014 for his master's thesis,
 * "Shape Decomposition for Multi-Channel Distance Fields". It provides improved
 * quality of sharp corners in glyphs and other 2D shapes in comparison to monochrome
 * distance fields. To reconstruct an image of the shape, apply the median of three
 * operation on the triplet of sampled distance field values.
 *
 */
 
using System;
using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Signed distance field generation.
	/// </summary>
	public static class SignedDistanceField
	{
		#region Methods

		/// <summary>
		/// Generates a multi-channel signed distance field for the specified geometry and returns the generated image.
		/// </summary>
		/// <param name="geometry"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public static Image GenerateMsdfImage(this Geometry geometry, in SignedDistanceFieldParameters parameters)
		{
			int w = parameters.Size.X;
			int h = parameters.Size.Y;

			Vector3[] sdf = new Vector3[w * h];
			ArrayMsdfImage image = new ArrayMsdfImage(sdf, w, h);
			geometry.GenerateMsdf(image, parameters.Scale, parameters.Translation);

			if (parameters.EdgeThreshold > 0.0)
			{
				Vector2 threshold = (Vector2)(parameters.EdgeThreshold / parameters.Scale);
				msdfErrorCorrection(image, threshold);
			}
			
			double range = parameters.Range;

			if (parameters.RangeMode == RangeMode.Pixels)
				range /= Utility.Min(parameters.Scale.X, parameters.Scale.Y);

			image.Transform((float)(1.0 / range), 0.5f);

			MemoryImage pixels = MemoryImage.Create(sdf, new Format(FormatType.R32G32B32, FormatSemantic.Float), w, h);
			return pixels;
		}

		private readonly struct Intersection
		{
			public readonly double X;
			public readonly int W;

			public Intersection(double x, int w)
			{
				this.X = x;
				this.W = w;
			}
		}

		public static void GenerateMsdf<T>(this Geometry geometry, T image, Vector2D scale, Vector2D translation)
			where T : IMsdfImage
		{
			Shape shape = Shape.FromGeometry(geometry.ToPath());
			shape.ColorEdges(3, 0);
			shape.GetBounds(out RectangleD bounds);

			int width = image.Width;
			int height = image.Height;

			Vector2D scaleInv = 1.0 / scale;

			List<Intersection> intersections = new List<Intersection>();
			void add(Vector2D point, int winding)
			{
				intersections.Add(new Intersection(point.X, winding));
			}

			EdgePoint empty = EdgePoint.Empty;
			for (int y = 0; y < height; ++y)
			{
				Vector2D p;
				p.X = bounds.X;
				p.Y = (y + 0.5) * scaleInv.Y - translation.Y;
				int index = 0;

				intersections.Clear();
				shape.GetIntersections(new Vector2D(bounds.X - 0.1, p.Y), add);
				intersections.Sort((a, b) => a.X.CompareTo(b.X));

				int currentWinding = 0;
				for (int x = 0; x < width; ++x)
				{
					p.X = (x + 0.5) * scaleInv.X - translation.X;
					while (index < intersections.Count && intersections[index].X < p.X)
					{
						currentWinding += intersections[index].W;
						++index;
					}
					
					EdgePoint shapeR = empty;
					EdgePoint shapeG = empty;
					EdgePoint shapeB = empty;
					int expectedSign = (currentWinding & 1) != 0 ? 1 : -1;

					for (int i = 0; i < shape.Contours.Length; i++)
					{
						Contour contour = shape.Contours[i];
						EdgePoint r = empty;
						EdgePoint g = empty;
						EdgePoint b = empty;

						for (int j = 0; j < contour.Edges.Length; j++)
						{
							EdgeSegment edge = contour.Edges[j];
							SignedDistance distance = edge.GetDistance(p, out double t);

							if ((edge.Color & EdgeColor.Red) != 0 && distance < r.MinDistance)
							{
								r.MinDistance = distance;
								r.NearEdge = edge;
								r.NearT = t;
							}

							if ((edge.Color & EdgeColor.Green) != 0 && distance < g.MinDistance)
							{
								g.MinDistance = distance;
								g.NearEdge = edge;
								g.NearT = t;
							}

							if ((edge.Color & EdgeColor.Blue) != 0 && distance < b.MinDistance)
							{
								b.MinDistance = distance;
								b.NearEdge = edge;
								b.NearT = t;
							}
						}

						if (r.MinDistance < shapeR.MinDistance)
							shapeR = r;

						if (g.MinDistance < shapeG.MinDistance)
							shapeG = g;

						if (b.MinDistance < shapeB.MinDistance)
							shapeB = b;
					}

					shapeR.NearEdge?.DistanceToPseudoDistance(ref shapeR.MinDistance, p, shapeR.NearT);
					shapeG.NearEdge?.DistanceToPseudoDistance(ref shapeG.MinDistance, p, shapeG.NearT);
					shapeB.NearEdge?.DistanceToPseudoDistance(ref shapeB.MinDistance, p, shapeB.NearT);

					double dr = shapeR.MinDistance.Distance;
					double dg = shapeG.MinDistance.Distance;
					double db = shapeB.MinDistance.Distance;

					double med = Utility.Median(dr, dg, db);
					if (Utility.Sign(med) != expectedSign)
					{
						dr = -dr;
						dg = -dg;
						db = -db;
					}

					Vector3 result;
					result.X = (float)dr;
					result.Y = (float)dg;
					result.Z = (float)db;
					image[x, y] = result;
				}
			}
		}

		private static void msdfErrorCorrection<T>(T image, Vector2 threshold)
			where T : IMsdfImage
		{
			Span<Vector2I> temp = stackalloc Vector2I[8];
			SpanList<Vector2I> clashes = new SpanList<Vector2I>(temp);

			int width = image.Width;
			int height = image.Height;

			for (int y = 0; y < height; ++y)
			{
				for (int x = 0; x < width; ++x)
				{
					Vector3 v = image[x, y];
					if ((x > 0 && pixelClash(v, image[x - 1, y], threshold.X)) ||
					    (x < width - 1 && pixelClash(v, image[x + 1, y], threshold.X)) ||
					    (y > 0 && pixelClash(v, image[x, y - 1], threshold.Y)) ||
					    (y < height - 1 && pixelClash(v, image[x, y + 1], threshold.Y)))
						clashes.Add((x, y));
				}
			}

			if (clashes.Count == 0)
				return;

			foreach (Vector2I clash in clashes)
			{
				ref Vector3 pixel = ref image[clash.X, clash.Y];

				float med = Utility.Median(pixel.X, pixel.Y, pixel.Z);

				pixel.X = med;
				pixel.Y = med;
				pixel.Z = med;
			}
		}

		private static bool pixelClash(Vector3 a, Vector3 b, float threshold)
		{
			// Only consider pair where both are on the inside or both are on the outside
			bool aIn = (a.X > 0f ? 1 : 0) + (a.Y > 0f ? 1 : 0) + (a.Z > 0f ? 1 : 0) >= 2;
			bool bIn = (b.X > 0f ? 1 : 0) + (b.Y > 0f ? 1 : 0) + (b.Z > 0f ? 1 : 0) >= 2;
			if (aIn != bIn) return false;
			// If the change is 0 <-> 1 or 2 <-> 3 channels and not 1 <-> 1 or 2 <-> 2, it is not a clash
			if (a.X > 0f && a.Y > 0f && a.Z > 0f || a.X < 0f && a.Y < 0f && a.Z < 0f ||
				b.X > 0f && b.Y > 0f && b.Z > 0f || b.X < 0f && b.Y < 0f && b.Z < 0f)
				return false;
			// Find which color is which: _a, _b = the changing channels, _c = the remaining one
			float aa, ab, ba, bb, ac, bc;
			if (a.X > 0f != b.X > 0f && a.X < 0f != b.X < 0f)
			{
				aa = a.X;
				ba = b.X;
				if (a.Y > 0f != b.Y > 0f && a.Y < 0f != b.Y < 0f)
				{
					ab = a.Y;
					bb = b.Y;
					ac = a.Z;
					bc = b.Z;
				}
				else if (a.Z > 0f != b.Z > 0f && a.Z < 0f != b.Z < 0f)
				{
					ab = a.Z;
					bb = b.Z;
					ac = a.Y;
					bc = b.Y;
				}
				else
				{
					return false; // this should never happen
				}

			}
			else if (a.Y > 0f != b.Y > 0f && a.Y < 0f != b.Y < 0f &&
					 a.Z > 0f != b.Z > 0f && a.Z < 0f != b.Z < 0f)
			{
				aa = a.Y;
				ba = b.Y;
				ab = a.Z;
				bb = b.Z;
				ac = a.X;
				bc = b.X;
			}
			else
			{
				return false;
			}

			// Find if the channels are in fact discontinuous
			return Utility.Absolute(aa - ba) >= threshold &&
				   Utility.Absolute(ab - bb) >= threshold &&
				   Utility.Absolute(ac) >= Utility.Absolute(bc); // Out of the pair, only flag the pixel farther from a shape edge
		}

		#endregion
	}
}
