﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Implements an array-based <see cref="IMsdfImage"/>.
	/// </summary>
	public struct ArrayMsdfImage : IMsdfImage
	{
		#region Fields

		private readonly Vector3[] array;
		
		#endregion
		
		#region Properties
		
		public int Width { get; }

		public int Height { get; }

		public ref Vector3 this[int x, int y] => ref this.array[y * this.Width + x];

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ArrayMsdfImage"/> class. 
		/// </summary>
		public ArrayMsdfImage(Vector3[] array, int width, int height)
		{
			this.array = array ?? throw new ArgumentNullException(nameof(array));
			if (array.Length < width * height)
				throw new ArgumentException("Array is too small", nameof(array));

			this.Width = width;
			this.Height = height;
		}

		#endregion
		
		#region Methods
		
		#endregion
	}
}
