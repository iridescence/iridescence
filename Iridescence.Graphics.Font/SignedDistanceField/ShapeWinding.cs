﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Defines SDF shape winding modes.
	/// </summary>
	public enum ShapeWinding
	{
		/// <summary>
		/// Keeps the shape winding as-is.
		/// </summary>
		Keep,

		/// <summary>
		/// Reverses the shape winding (what's inside becomes outside and the other way around).
		/// </summary>
		Reverse,

		/// <summary>
		/// Guesses the winding and reverses it if necessary.
		/// </summary>
		Guess
	}
}