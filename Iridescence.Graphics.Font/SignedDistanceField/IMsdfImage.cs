﻿using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents an abstract MSDF image.
	/// </summary>
	public interface IMsdfImage
	{
		/// <summary>
		/// Gets the width.
		/// </summary>
		int Width { get; }

		/// <summary>
		/// Gets the height.
		/// </summary>
		int Height { get; }

		/// <summary>
		/// Gets or sets the vector at the specified coordinates.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		ref Vector3 this[int x, int y] { get; }
	}

	public static class MsdfImageExtensions
	{
		public static void Transform<T>(this T image, float scale, float bias) where T : IMsdfImage
		{
			int w = image.Width;
			int h = image.Height;

			for (int y = 0; y < h; ++y)
			{
				for (int x = 0; x < w; ++x)
				{
					ref Vector3 v = ref image[x, y];

					v.X = v.X * scale + bias;
					v.Y = v.Y * scale + bias;
					v.Z = v.Z * scale + bias;
				}
			}
		}
	}
}
