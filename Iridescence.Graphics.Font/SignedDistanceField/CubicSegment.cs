﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	internal sealed class CubicSegment : EdgeSegment
	{
		#region Fields

		public readonly CubicBezier Bezier;
		
		#endregion

		#region Properties

		#endregion

		#region Constructors

		public CubicSegment(CubicBezier bezier)
		{
			this.Bezier = bezier;
		}

		#endregion

		#region Methods

		public override Vector2D GetValue(double t) => this.Bezier.GetValue(t);

		public override Vector2D GetTangent(double t) => this.Bezier.GetTangent(t);

		public override IEnumerable<double> GetIntersections(in LineSegmentD lineSegment) => Curve.GetIntersections(this.Bezier, lineSegment, 0.1);

		public override int GetIntersections(in Vector2D r, IntersectionCallback callback) => ScanLine.CrossCubic(r, this.Bezier.Point1, this.Bezier.Point2, this.Bezier.Point3, this.Bezier.Point4, 0, callback);

		public override SignedDistance GetDistance(Vector2D origin, out double param)
		{
			const int numStarts = 4;
			const int numSteps = 4;

			Vector2D qa = this.Bezier.Point1 - origin;
			Vector2D ab = this.Bezier.Point2 - this.Bezier.Point1;
			Vector2D br = this.Bezier.Point3 - this.Bezier.Point2 - ab;
			Vector2D @as = (this.Bezier.Point4 - this.Bezier.Point3) - (this.Bezier.Point3 - this.Bezier.Point2) - br;

			Vector2D epDir = this.Bezier.GetTangent(0);
			double minDistance = Utility.NonZeroSign(Vector2D.Cross(epDir, qa)) * qa.Length; // distance from A
			param = -Vector2D.Dot(qa, epDir) / Vector2D.Dot(epDir, epDir);
			{
				epDir = this.Bezier.GetTangent(1);
				double distance = Utility.NonZeroSign(Vector2D.Cross(epDir, this.Bezier.Point4 - origin)) * (this.Bezier.Point4 - origin).Length; // distance from B
				if (Utility.Absolute(distance) < Utility.Absolute(minDistance))
				{
					minDistance = distance;
					param = Vector2D.Dot(origin + epDir - this.Bezier.Point4, epDir) / Vector2D.Dot(epDir, epDir);
				}
			}
			// Iterative minimum distance search
			for (int i = 0; i <= numStarts; ++i)
			{
				double t = (double)i / numStarts;
				for (int step = 0;; ++step)
				{
					Vector2D qpt = this.Bezier.GetValue(t) - origin;
					double distance = Utility.NonZeroSign(Vector2D.Cross(this.Bezier.GetTangent(t), qpt)) * qpt.Length;
					if (Utility.Absolute(distance) < Utility.Absolute(minDistance))
					{
						minDistance = distance;
						param = t;
					}
					if (step == numSteps)
						break;
					// Improve t
					Vector2D d1 = 3 * @as * t * t + 6 * br * t + 3 * ab;
					Vector2D d2 = 6 * @as * t + 6 * br;
					t -= Vector2D.Dot(qpt, d1) / (Vector2D.Dot(d1, d1) + Vector2D.Dot(qpt, d2));
					if (t < 0 || t > 1)
						break;
				}
			}

			if (param >= 0 && param <= 1)
				return new SignedDistance(minDistance, 0);

			if (param < 0.5)
				return new SignedDistance(minDistance, Utility.Absolute(Vector2D.Dot(this.Bezier.GetTangent(0), qa.Normalize())));

			return new SignedDistance(minDistance, Utility.Absolute(Vector2D.Dot(this.Bezier.GetTangent(1), (this.Bezier.Point4 - origin).Normalize())));
		}

		public override void SplitInThirds(out EdgeSegment segment1, out EdgeSegment segment2, out EdgeSegment segment3)
		{
			this.Bezier.Split(1.0 / 3.0, out CubicBezier b1, out CubicBezier b2);
			b2.Split(0.5, out b2, out CubicBezier b3);
			segment1 = new CubicSegment(b1);
			segment2 = new CubicSegment(b2);
			segment3 = new CubicSegment(b3);
		}

		public override void GetBounds(out RectangleD bounds)
		{
			bounds = this.Bezier.GetBounds();
		}

		#endregion
	}
}
