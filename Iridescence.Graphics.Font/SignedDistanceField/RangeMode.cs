﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Specifies how to interpret the "range" parameter for signed distance fields.
	/// </summary>
	public enum RangeMode
	{
		/// <summary>
		/// The range is expressed in geometry units.
		/// </summary>
		Units,

		/// <summary>
		/// The range is expressed in distance field cells/pixels.
		/// </summary>
		Pixels,
	}
}
