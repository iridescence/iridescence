﻿// Adapted from https://github.com/ckohnert/msdfgen

using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Scan line intersection methods.
	/// </summary>
	internal static class ScanLine
	{
		private const int maxDepth = 30;

		/// Check how many times a ray from point R extending to the +X direction intersects
		/// the given segment:
		///  0 = no intersection or co-linear
		/// +1 = intersection increasing in the Y axis
		/// -1 = intersection decreasing in the Y axis
		public static int CrossLine(in Vector2D r, in Vector2D p0, in Vector2D p1, IntersectionCallback cb)
		{
			if (r.Y < Utility.Min(p0.Y, p1.Y))
				return 0;
			if (r.Y >= Utility.Max(p0.Y, p1.Y))
				return 0;
			if (r.X >= Utility.Max(p0.X, p1.X))
				return 0;
			// Intersect the line at r.Y and see if the intersection is before or after the origin.
			double xintercept = (p0.X + (r.Y - p0.Y) * (p1.X - p0.X) / (p1.Y - p0.Y));
			if (r.X < xintercept)
			{
				int w = (p0.Y < p1.Y) ? 1 : -1;
				cb?.Invoke((xintercept, r.Y), w);
				return w;
			}

			return 0;
		}

		/// Check how many times a ray from point R extending to the +X direction intersects
		/// the given segment:
		///  0 = no intersection or co-linear
		/// +1 = for each intersection increasing in the Y axis
		/// -1 = for each intersection decreasing in the Y axis
		public static int CrossQuad(in Vector2D r, in Vector2D p0, in Vector2D c0, in Vector2D p1, int depth, IntersectionCallback cb)
		{
			if (r.Y < Utility.Min(p0.Y, Utility.Min(c0.Y, p1.Y)))
				return 0;
			if (r.Y > Utility.Max(p0.Y, Utility.Max(c0.Y, p1.Y)))
				return 0;
			if (r.X >= Utility.Max(p0.X, Utility.Max(c0.X, p1.X)))
				return 0;

			if (p1.Y == p0.Y && c0.Y == p0.Y)
			{
				// in fact, this segment is a line and since r.Y == p1.Y == p0.Y == c0.Y,
				// the ray has an infinite number of the intersection points
				return 0;
			}

			// Recursively subdivide the curve to find the intersection point(s). If we haven't
			// converged on a solution by a given depth, just treat it as a linear segment
			// and call the approximation good enough.
			if (depth > maxDepth)
				return CrossLine(r, p0, p1, cb);

			depth++;

			Vector2D mc0 = (p0 + c0) * 0.5;
			Vector2D mc1 = (c0 + p1) * 0.5;
			Vector2D mid = (mc0 + mc1) * 0.5;

			return CrossQuad(r, p0, mc0, mid, depth, cb) + CrossQuad(r, mid, mc1, p1, depth, cb);
		}

		/// Check how many times a ray from point R extending to the +X direction intersects
		/// the given segment:
		///  0 = no intersection or co-linear
		/// +1 = for each intersection increasing in the Y axis
		/// -1 = for each intersection decreasing in the Y axis
		public static int CrossCubic(in Vector2D r, in Vector2D p0, in Vector2D c0, in Vector2D c1, in Vector2D p1, int depth, IntersectionCallback cb)
		{
			if (r.Y < Utility.Min(p0.Y, Utility.Min(c0.Y, Utility.Min(c1.Y, p1.Y))))
				return 0;
			if (r.Y > Utility.Max(p0.Y, Utility.Max(c0.Y, Utility.Max(c1.Y, p1.Y))))
				return 0;
			if (r.X >= Utility.Max(p0.X, Utility.Max(c0.X, Utility.Max(c1.X, p1.X))))
				return 0;

			if (p0.Y == c0.Y && p0.Y == c1.Y && p0.Y == p1.Y)
				return 0;

			// Recursively subdivide the curve to find the intersection point(s). If we haven't
			// converged on a solution by a given depth, just treat it as a linear segment
			// and call the approximation good enough.
			if (depth > maxDepth)
				return CrossLine(r, p0, p1, cb);

			depth++;

			Vector2D mid = (c0 + c1) * 0.5;
			Vector2D c00 = (p0 + c0) * 0.5;
			Vector2D c11 = (c1 + p1) * 0.5;
			Vector2D c01 = (c00 + mid) * 0.5;
			Vector2D c10 = (c11 + mid) * 0.5;

			mid = (c01 + c10) * 0.5;

			return CrossCubic(r, p0, c00, c01, mid, depth, cb) + CrossCubic(r, mid, c10, c11, p1, depth, cb);
		}

	}
}
