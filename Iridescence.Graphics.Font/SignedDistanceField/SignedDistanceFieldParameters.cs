﻿using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Defines the parameters for signed distance field generation.
	/// </summary>
	public struct SignedDistanceFieldParameters
	{
		#region Fields

		public static readonly SignedDistanceFieldParameters Default = new SignedDistanceFieldParameters()
		{
			Size = (64, 64),
			Range = 2.0,
			RangeMode = RangeMode.Pixels,
			Scale = (1, 1),
			Translation = (0, 0),
			EdgeThreshold = 1.00000001,
			Winding = ShapeWinding.Guess
		};

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the size of the output distance field.
		/// </summary>
		public Vector2I Size { get; set; }

		/// <summary>
		/// Gets or sets the range. This value specifies the width of the range between the lowest and highest signed distance (in pixels or units, see <see cref="RangeMode"/>).
		/// </summary>
		/// <remarks>
		/// This is essentially the width of the "outline" around the shape.
		/// </remarks>
		public double Range { get; set; }

		/// <summary>
		/// Gets or sets a value that determines how the <see cref="Range"/> parameter is interpreted.
		/// </summary>
		public RangeMode RangeMode { get; set; }

		/// <summary>
		/// Gets or sets a scale factor multiplied with all shape coordinates.
		/// </summary>
		public Vector2D Scale { get; set; }

		/// <summary>
		/// Gets or sets a translation vector added to all shape coordinates.
		/// </summary>
		public Vector2D Translation { get; set; }

		/// <summary>
		/// Gets or sets the threshold used to detect and correct potential artifacts. 0 disables error correction.
		/// </summary>
		public double EdgeThreshold { get; set; }

		/// <summary>
		/// Gets or sets the winding mode.
		/// </summary>
		public ShapeWinding Winding { get; set; }

		#endregion

		#region Constructors

		#endregion

		#region Methods

		/// <summary>
		/// Sets the <see cref="Translation"/> and <see cref="Scale"/> properties to fit the specified rectangle.
		/// </summary>
		/// <param name="bounds"></param>
		public void FitBounds(RectangleD bounds)
		{
			Vector2D frame = this.Size;

			if (this.RangeMode == RangeMode.Pixels)
			{
				frame.X -= 2.0 * this.Range;
				frame.Y -= 2.0 * this.Range;
			}
			else
			{
				bounds.X -= this.Range;
				bounds.Y -= this.Range;
				bounds.Width += 2.0 * this.Range;
				bounds.Height += 2.0 * this.Range;
			}

			if (bounds.Width * frame.Y < bounds.Height * frame.X)
			{
				this.Translation = (0.5 * (frame.X / frame.Y * bounds.Height - bounds.Width) - bounds.X, -bounds.Y);
				this.Scale = new Vector2D(frame.Y / bounds.Height);
			}
			else
			{
				this.Translation = (-bounds.X, 0.5 * (frame.Y / frame.X * bounds.Width - bounds.Height) - bounds.Y);
				this.Scale = new Vector2D(frame.X / bounds.Width);
			}

			if (this.RangeMode == RangeMode.Pixels)
			{
				this.Translation += (this.Range / this.Scale.X, this.Range / this.Scale.Y);
			}
		}

		/// <summary>
		/// Sets the <see cref="Translation"/> and <see cref="Scale"/> properties to fit the specified geometry.
		/// </summary>
		/// <param name="geometry"></param>
		public void FitGeometry(Geometry geometry)
		{
			this.FitBounds(geometry.GetBounds());
		}

		#endregion
	}
}
