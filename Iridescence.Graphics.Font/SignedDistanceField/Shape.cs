﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	internal sealed class Shape
	{
		#region Fields

		public readonly Contour[] Contours;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public Shape(IEnumerable<Contour> contours)
		{
			this.Contours = contours.ToArray();
		}

		#endregion

		#region Methods

		public static Shape FromGeometry(PathGeometry geometry)
		{
			return new Shape(geometry.Figures.Select(Contour.FromFigure));
		}

		private static bool isCorner(Vector2D aDir, Vector2D bDir, double crossThreshold)
		{
			return Vector2D.Dot(aDir, bDir) <= 0 || Utility.Absolute(Vector2D.Cross(aDir, bDir)) > crossThreshold;
		}

		private static void switchColor(ref EdgeColor color, ref ulong seed, EdgeColor banned = EdgeColor.Black)
		{
			EdgeColor combined = color & banned;
			if (combined == EdgeColor.Red ||
			    combined == EdgeColor.Green ||
			    combined == EdgeColor.Blue)
			{
				color = combined ^ EdgeColor.White;
				return;
			}

			if (color == EdgeColor.Black || color == EdgeColor.White)
			{
				switch (seed % 3)
				{
					case 0:
						color = EdgeColor.Cyan;
						break;
					case 1:
						color = EdgeColor.Magenta;
						break;
					case 2:
						color = EdgeColor.Yellow;
						break;
				}

				seed /= 3;
				return;
			}
			int shifted = (int)color << (1 + (int)(seed & 1));
			color = (EdgeColor)(shifted | shifted >> 3) & EdgeColor.White;
			seed >>= 1;
		}

		/// <summary>
		/// Colors all <see cref="EdgeSegment">edge segments</see> in the <see cref="Shape"/>'s contours.
		/// </summary>
		/// <param name="angleThreshold"></param>
		/// <param name="seed"></param>
		public void ColorEdges(double angleThreshold, ulong seed)
		{
			double crossThreshold = Utility.Sin(angleThreshold);
			Span<int> temp = stackalloc int[4];
			SpanList<int> corners = new SpanList<int>(temp);

			for (int c = 0; c < this.Contours.Length; ++c)
			{
				Contour contour = this.Contours[c];

				// Identify corners.
				corners.Clear();
				if (contour.Edges.Length > 0)
				{
					Vector2D prevDirection = contour.Edges[contour.Edges.Length - 1].GetTangent(1.0);

					for (int i = 0; i < contour.Edges.Length; i++)
					{
						if (isCorner(prevDirection, contour.Edges[i].GetTangent(0), crossThreshold))
						{
							corners.Add(i);
						}

						prevDirection = contour.Edges[i].GetTangent(1);
					}
				}

				if (corners.Count == 0)
				{
					// Smooth contour.
					foreach (EdgeSegment edge in contour.Edges)
					{
						edge.Color = EdgeColor.White;
					}
				}
				else if (corners.Count == 1)
				{
					// "Teardrop" case.
					Span<EdgeColor> colors = stackalloc EdgeColor[] {EdgeColor.White, EdgeColor.White, EdgeColor.White};
					switchColor(ref colors[0], ref seed);
					colors[2] = colors[0];
					switchColor(ref colors[2], ref seed);

					int corner = corners[0];
					if (contour.Edges.Length >= 3)
					{
						int m = contour.Edges.Length;
						for (int i = 0; i < m; ++i)
							contour.Edges[(corner + i) % m].Color = colors[1 + (int)(3 + 2.875 * i / (m - 1) - 1.4375 + .5) - 3];
					}
					else if (contour.Edges.Length >= 1)
					{
						// Less than three edge segments for three colors => edges must be split.
						EdgeSegment[] parts = new EdgeSegment[7];
						contour.Edges[0].SplitInThirds(out parts[0 + 3 * corner], out parts[1 + 3 * corner], out parts[2 + 3 * corner]);

						if (contour.Edges.Length >= 2)
						{
							contour.Edges[1].SplitInThirds(out parts[3 - 3 * corner], out parts[4 - 3 * corner], out parts[5 - 3 * corner]);
							parts[0].Color = parts[1].Color = colors[0];
							parts[2].Color = parts[3].Color = colors[1];
							parts[4].Color = parts[5].Color = colors[2];
						}
						else
						{
							parts[0].Color = colors[0];
							parts[1].Color = colors[1];
							parts[2].Color = colors[2];
						}

						this.Contours[c] = new Contour(parts);
					}
				}
				else
				{
					// Multiple corners.
					int cornerCount = corners.Count;
					int spline = 0;
					int start = corners[0];
					int m = contour.Edges.Length;
					EdgeColor color = EdgeColor.White;
					switchColor(ref color, ref seed);
					EdgeColor initialColor = color;
					for (int i = 0; i < m; ++i)
					{
						int index = (start + i) % m;
						if (spline + 1 < cornerCount && corners[spline + 1] == index)
						{
							++spline;
							switchColor(ref color, ref seed, (EdgeColor)((spline == cornerCount - 1 ? 1 : 0) * (int)initialColor));
						}
						contour.Edges[index].Color = color;
					}
				}
			}
		}

		public IEnumerable<(Contour, EdgeSegment, double)> GetIntersections(LineSegmentD lineSegment)
		{
			foreach (Contour contour in this.Contours)
			{
				foreach ((EdgeSegment edge, double intersection) in contour.GetIntersections(lineSegment))
				{
					yield return (contour, edge, intersection);
				}
			}
		}

		public int GetIntersections(in Vector2D origin, IntersectionCallback callback)
		{
			int sum = 0;
			foreach (Contour contour in this.Contours)
			{
				sum += contour.GetIntersections(origin, callback);
			}
			return sum;
		}

		public void GetBounds(out RectangleD bounds)
		{
			if (this.Contours.Length == 0)
			{
				bounds = RectangleD.Zero;
				return;
			}

			this.Contours[0].GetBounds(out bounds);

			for (int i = 1; i < this.Contours.Length; ++i)
			{
				this.Contours[i].GetBounds(out RectangleD temp);
				RectangleD.Union(bounds, temp, out bounds);
			}
		}

		public SignedDistance GetMinDistance(in Vector2D p)
		{
			SignedDistance minDistance = SignedDistance.PositiveInfinity;

			foreach (Contour c in this.Contours)
			{
				SignedDistance d = c.GetMinDistance(p);
				if (d < minDistance)
				{
					minDistance = d;
				}
			}

			return minDistance;
		}

		#endregion
	}
}
