﻿namespace Iridescence.Graphics.Font
{
	internal struct EdgePoint
	{
		public static readonly EdgePoint Empty = new EdgePoint
		{
			MinDistance = SignedDistance.NegativeInfinity,
			NearEdge = null,
			NearT = 0.0
		};

		public SignedDistance MinDistance;
		public EdgeSegment NearEdge;
		public double NearT;
	}
}
