﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	internal sealed class Contour
	{
		#region Fields

		public readonly EdgeSegment[] Edges;

		#endregion

		#region Constructors

		public Contour(IEnumerable<EdgeSegment> edges)
		{
			if (edges == null)
				throw new ArgumentNullException(nameof(edges));

			this.Edges = edges.ToArray();
		}

		#endregion

		#region Methods

		public void GetBounds(out RectangleD bounds)
		{
			if (this.Edges.Length == 0)
			{
				bounds = RectangleD.Zero;
				return;
			}

			this.Edges[0].GetBounds(out bounds);

			for (int i = 1; i < this.Edges.Length; ++i)
			{
				this.Edges[i].GetBounds(out RectangleD temp);
				RectangleD.Union(bounds, temp, out bounds);
			}
		}

		public IEnumerable<(EdgeSegment, double)> GetIntersections(LineSegmentD lineSegment)
		{
			foreach (EdgeSegment edge in this.Edges)
			{
				foreach(double intersection in edge.GetIntersections(lineSegment))
				{
					yield return (edge, intersection);
				}
			}
		}

		public int GetIntersections(in Vector2D origin, IntersectionCallback callback)
		{
			int sum = 0;
			foreach (EdgeSegment edge in this.Edges)
			{
				sum += edge.GetIntersections(origin, callback);
			}
			return sum;
		}


		public SignedDistance GetMinDistance(in Vector2D p)
		{
			SignedDistance minDistance = SignedDistance.PositiveInfinity;

			foreach (EdgeSegment e in this.Edges)
			{
				SignedDistance d = e.GetDistance(p, out _);
				if (d < minDistance)
				{
					minDistance = d;
				}
			}

			return minDistance;
		}

		public static Contour FromFigure(PathFigure figure)
		{
			Vector2D currentPoint = figure.StartPoint;

			List<EdgeSegment> edges = new List<EdgeSegment>(figure.Segments.Select<PathSegment, EdgeSegment>(seg =>
			{
				switch (seg)
				{
					case LinePathSegment lineSegment:
						return new LinearSegment(new LineSegmentD(currentPoint, currentPoint = lineSegment.Point));

					case QuadraticBezierPathSegment quadSegment:
						return new QuadraticSegment(new QuadraticBezier(currentPoint, quadSegment.ControlPoint, currentPoint = quadSegment.EndPoint));

					case CubicBezierPathSegment cubicSegment:
						return new CubicSegment(new CubicBezier(currentPoint, cubicSegment.ControlPoint1, cubicSegment.ControlPoint2, currentPoint = cubicSegment.EndPoint));

					default:
						throw new NotSupportedException();
				}
			}));

			if (figure.IsClosed && currentPoint != figure.StartPoint)
			{
				edges.Add(new LinearSegment(new LineSegmentD(currentPoint, figure.StartPoint)));
			}

			return new Contour(edges);
		}

		#endregion
	}
}
