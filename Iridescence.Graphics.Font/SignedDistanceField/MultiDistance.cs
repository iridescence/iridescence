﻿namespace Iridescence.Graphics.Font
{
	internal struct MultiDistance
	{
		public double R;
		public double G;
		public double B;
		public double Med;
	}
}
