﻿using System;
using System.Collections.Generic;
using System.Text;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents an abstract font.
	/// </summary>
	public class Font
	{
		#region Fields

		private static readonly UTF32Encoding encoding = new UTF32Encoding(false, false);
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the font face.
		/// </summary>
		public FontFace Face { get; }

		/// <summary>
		/// Gets the size of the font instance (in pixels).
		/// </summary>
		public double Size { get; }

		/// <summary>
		/// Gets the distance between to baselines of the font (in pixels).
		/// </summary>
		public double LineHeight { get; }

		/// <summary>
		/// Gets the distance form the baseline to the highest character coordinate.
		/// </summary>
		public double Ascender { get; }

		/// <summary>
		/// Gets the distance form the baseline to the lowest character coordinate.
		/// </summary>
		public double Descender { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Font.
		/// </summary>
		protected Font(FontFace face, double size, double lineHeight, double ascender, double descender)
		{
			if (!size.IsFinite())
				throw new ArgumentException("Invalid size.", nameof(size));

			if (!lineHeight.IsFinite())
				throw new ArgumentException("Invalid line height.", nameof(lineHeight));

			if (!ascender.IsFinite())
				throw new ArgumentException("Invalid ascender.", nameof(ascender));

			if (!descender.IsFinite())
				throw new ArgumentException("Invalid descender.", nameof(descender));

			this.Face = face ?? throw new ArgumentNullException(nameof(face));
			this.Size = size;
			this.LineHeight = lineHeight;
			this.Ascender = ascender;
			this.Descender = descender;
		}

		#endregion

		#region Methods

		public override bool Equals(object obj)
		{
			if (!(obj is Font other))
				return false;

			if (other.Face == this.Face && 
				System.Math.Abs(other.Size - this.Size) <= double.Epsilon)
				return true;

			return false;
		}

		public override int GetHashCode()
		{
			return this.Face.GetHashCode() ^ this.Size.GetHashCode();
		}

		/// <summary>
		/// Returns the <see cref="Glyph"/> for the specified code point.
		/// </summary>
		/// <param name="codePoint">The unicode code point.</param>
		/// <returns>The glyph for the specified unicode code point, or null if that glyph does not exist in the font. You can use <see cref="GetUndefinedGlyph"/> in the latter case.</returns>
		public Glyph GetGlyph(uint codePoint)
		{
			return this.Face.GetGlyph(this.Size, codePoint);
		}

		/// <summary>
		/// Returns the glyph for an undefined character code (e.g. a question-mark glyph, depends on the font and/or implementation).
		/// </summary>
		/// <param name="codePoint">The code point that will be used for the glyph. This is only used for the <see cref="Glyph.CodePoint"/> property so that it can be mapped back to a character.</param>
		/// <returns>The 'undefined' glyph.</returns>
		public Glyph GetUndefinedGlyph(uint codePoint = 0)
		{
			return this.Face.GetUndefinedGlyph(this.Size, codePoint);
		}

		/// <summary>
		/// Returns the <see cref="Glyph">Glyphs</see> that correspond to the specified Unicode code points.
		/// </summary>
		/// <param name="codePoints">The unicode code points. The code points are expected to be real unicode code points (i.e. UTF-32). No surrogate pair decoding is performed.</param>
		/// <param name="useUndefinedGlyphs">True, if the 'undefined' glyph should be used in case any of the characters does not have a glyph in this font.</param>
		/// <returns>An enumerable that returns one <see cref="Glyph"/> for each input code point. The glyph returned for a code point can be null if the font does not contain that glyph.</returns>
		public virtual IEnumerable<Glyph> GetGlyphs(IEnumerable<uint> codePoints, bool useUndefinedGlyphs)
		{
			foreach (uint codePoint in codePoints)
				yield return this.GetGlyph(codePoint) ?? (useUndefinedGlyphs ? this.GetUndefinedGlyph(codePoint) : null);
		}

		/// <summary>
		/// Returns the <see cref="Glyph">Glyphs</see> for the specified string.
		/// </summary>
		/// <param name="str">A string. Note that strings are encoded in UTF-16, but this function returns glyphs for the "real" unicode characters of the string. Fewer glyphs than there are characters in the string might be returned.</param>
		/// <param name="useUndefinedGlyphs">True, if the 'undefined' glyph should be used in case any of the characters does not have a glyph in this font.</param>
		/// <returns></returns>
		public virtual IEnumerable<Glyph> GetGlyphs(string str, bool useUndefinedGlyphs)
		{
			if(str == null)
				throw new ArgumentNullException(nameof(str));

			byte[] buffer = encoding.GetBytes(str);

			IEnumerable<uint> enumerateCodePoints()
			{
				for (int i = 0; i < buffer.Length; i += 4)
				{
					yield return BitConverter.ToUInt32(buffer, i);
				}
			}

			return this.GetGlyphs(enumerateCodePoints(), useUndefinedGlyphs);
		}

		/// <summary>
		/// Returns the kerning value of a pair of glyphs.
		/// </summary>
		/// <param name="first">The unicode code point of the first character.</param>
		/// <param name="second">The unicode code point of the second character.</param>
		/// <returns>The kerning value of the specified pair of glyphs.</returns>
		public Vector2D GetKerning(uint first, uint second) => this.Face.GetKerning(first, second, this.Size);

		public override string ToString()
		{
			return $"{this.Face.Name} {this.Size}";
		}

		#endregion
	}
}
