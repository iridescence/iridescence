﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents an abstract glyph of a font.
	/// </summary>
	[Serializable]
	public abstract class Glyph : ILayoutElement
	{
		#region Fields

		[NonSerialized] private Font font; // Only functions as a cache.
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the <see cref="Font"/> that this glyph belongs to.
		/// </summary>
		public FontFace Face { get; }

		/// <summary>
		/// Gets the <see cref="Font"/> that this glyph belongs to.
		/// </summary>
		public Font Font => this.font ??= this.Face.GetFont(this.FontSize);

		/// <summary>
		/// Gets the font size of this glyph.
		/// </summary>
		public double FontSize { get; }

		/// <summary>
		/// Gets the unicode code point of the glyph.
		/// </summary>
		public uint CodePoint { get; }

		/// <summary>
		/// Gets the character or the surrogate pair of this glyph.
		/// </summary>
		public string Character
		{
			get
			{
				if (this.CodePoint <= 0xFFFF)
				{
					char c = (char)this.CodePoint;
					if (char.IsHighSurrogate(c) || char.IsLowSurrogate(c))
						return new string(c, 1);
				}
				return char.ConvertFromUtf32((int)this.CodePoint);
			}
		}

		/// <summary>
		/// Gets the size of the glyph.
		/// </summary>
		/// <remarks>
		/// This is not always equal to the size of the glyph image.
		/// </remarks>
		public abstract Vector2D Size { get; }

		/// <summary>
		/// Gets the distance between the left edge of this glyph and the left edge of the next glyph.
		/// This is the default distance. For per-glyph-pair values, please use <see cref="GetKerning"/>.
		/// </summary>
		public abstract Vector2D Advance { get; }
		
		/// <summary>
		/// Gets the glyph geometry.
		/// Can be null in case the glyph doesn't have an outline (e.g. because it is a bitmap-only glyph).
		/// </summary>
		public abstract Geometry Geometry { get; }

		double ILayoutElement.Advance => this.Advance.X;

		double ILayoutElement.LineSize => this.Font.LineHeight;

		double ILayoutElement.Offset => -this.Font.Descender;

		bool ILayoutElement.IsPlaceholder => char.IsWhiteSpace(this.Character[0]);

		bool ILayoutElement.IsWordSeparator => char.IsWhiteSpace(this.Character[0]);

		bool ILayoutElement.ForceBreak => this.CodePoint == '\n';

		double ILayoutElement.LineAlignment => 0.0f;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="Glyph"/>.
		/// </summary>
		protected Glyph(FontFace face, double fontSize, uint codePoint)
		{
			this.Face = face;
			this.FontSize = fontSize;
			this.CodePoint = codePoint;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the kerning value for this <see cref="Glyph"/> followed by the specified <see cref="Glyph"/>.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public Vector2D GetKerning(Glyph other)
		{
			if (other == null)
				throw new ArgumentNullException(nameof(other));

			if (!ReferenceEquals(other.Font, this.Font))
				throw new ArgumentException("The glyphs do not belong to the same font.", nameof(other));
		
			return this.Font.GetKerning(this.CodePoint, other.CodePoint);
		}

		/// <summary>
		/// Renders the glyph into a <see cref="GlyphImage"/>.
		/// </summary>
		/// <param name="kind"></param>
		/// <returns>The rendered glyph, or null of the glyph does not contain an image/geometry.</returns>
		public abstract GlyphImage Draw(GlyphImageKind kind);

		/// <summary>
		/// Gets the advance/translation vector between two glyphs (including kerning).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static Vector2D GetAdvance(Glyph a, Glyph b)
		{
			return a.Advance + a.GetKerning(b);
		}

		public override string ToString()
		{
			return this.Character;
		}

		#endregion
	}
}
