﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Defines the different kinds of glyph images.
	/// </summary>
	public enum GlyphImageKind
	{
		Monochrome,
		Grayscale,
	}
}
