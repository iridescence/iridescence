﻿using System;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Implements a <see cref="TextureFont"/> that utilizes signed distance fields generated using <see cref="SignedDistanceField"/>.
	/// </summary>
	public class SignedDistanceFieldFont : TextureFont
	{
		#region Fields

		private readonly ISignedDistanceFieldParameterProvider parameterProvider;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignedDistanceFieldFont"/> class. 
		/// </summary>
		public SignedDistanceFieldFont(Font font, ITextureFontImageProvider imageProvider, ISignedDistanceFieldParameterProvider parameterProvider)
			: base(font, imageProvider)
		{
			this.parameterProvider = parameterProvider ?? throw new ArgumentNullException(nameof(parameterProvider));
		}

		#endregion

		#region Methods

		public override ReadOnlyImage DrawGlyph(uint codePoint)
		{
			Glyph glyph = this.Font.GetGlyph(codePoint) ?? this.Font.GetUndefinedGlyph(codePoint);

			Geometry geometry = glyph?.Geometry;
			if (geometry == null)
				return null;

			SignedDistanceFieldParameters parameters = this.parameterProvider.GetParameters(glyph);
			return geometry.GenerateMsdfImage(parameters);
		}

		#endregion
	}
}
