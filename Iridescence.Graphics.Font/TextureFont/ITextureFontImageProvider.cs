﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents an image provider for texture fonts.
	/// </summary>
	public interface ITextureFontImageProvider
	{
		/// <summary>
		/// Returns a new image.
		/// </summary>
		/// <returns></returns>
		Image CreateImage();
	}
}
