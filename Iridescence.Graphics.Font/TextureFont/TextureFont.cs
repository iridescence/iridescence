﻿using System;
using System.Collections.Generic;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents a texture font for rendering. Dynamically generates texture pages containing the glyphs defined in a font.
	/// </summary>
	public class TextureFont
	{
		#region Fields

		private readonly List<TextureFontPage> pages;
		private readonly Dictionary<uint, int> charLookup;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the font.
		/// </summary>
		public Font Font { get; }

		/// <summary>
		/// Gets the image provider of the texture font.
		/// </summary>
		public ITextureFontImageProvider ImageProvider { get; }

		/// <summary>
		/// Gets the pages of this <see cref="TextureFont"/>.
		/// </summary>
		public IReadOnlyList<TextureFontPage> Pages => this.pages;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new texture font using the specified font.
		/// </summary>
		public TextureFont(Font font, ITextureFontImageProvider imageProvider)
		{
			this.Font = font ?? throw new ArgumentNullException(nameof(font));
			this.ImageProvider = imageProvider ?? throw new ArgumentNullException(nameof(imageProvider));

			this.pages = new List<TextureFontPage>();
			this.charLookup = new Dictionary<uint, int>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the image of the specified glyph.
		/// </summary>
		/// <param name="codePoint"></param>
		/// <returns></returns>
		public virtual ReadOnlyImage DrawGlyph(uint codePoint)
		{
			return (this.Font.GetGlyph(codePoint) ?? this.Font.GetUndefinedGlyph(codePoint))?.Draw(GlyphImageKind.Grayscale);
		}

		/// <summary>
		/// Gets the index of the page that contains the specified character.
		/// </summary>
		/// <param name="codePoint"></param>
		/// <returns></returns>
		public int GetPageIndex(uint codePoint)
		{
			if (!this.charLookup.TryGetValue(codePoint, out int pageIndex))
			{
				// try to fit the character into an existing page.
				for (int i = this.pages.Count - 1; i >= 0; i--)
				{
					if (this.pages[i].Add(codePoint))
					{
						this.charLookup.Add(codePoint, i);
						return i;
					}
				}

				// create a new page.
				pageIndex = this.pages.Count;

				Image image = this.ImageProvider.CreateImage();
				if (image == null)
					throw new Exception("Failed to create image for new texture font page.");

				TextureFontPage newPage = new TextureFontPage(this, pageIndex, image);
				this.pages.Add(newPage);

				newPage.Add(codePoint);
				this.charLookup.Add(codePoint, pageIndex);
			}

			return pageIndex;
		}

		/// <summary>
		/// Gets the texture font page for the specified character.
		/// </summary>
		/// <param name="codePoint">The character code point whose page is to be returned.</param>
		/// <returns></returns>
		public TextureFontPage GetPage(uint codePoint)
		{
			return this.pages[this.GetPageIndex(codePoint)];
		}

		/// <summary>
		/// Gets the texture font glyph for the specific character.
		/// </summary>
		/// <param name="codePoint"></param>
		/// <returns></returns>
		public TextureFontGlyph GetGlyph(uint codePoint)
		{
			return this.GetPage(codePoint).GetGlyph(codePoint);
		}

		#endregion
	}
}
