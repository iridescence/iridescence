﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents a glyph on a texture font page.
	/// </summary>
	public class TextureFontGlyph
	{
		#region Fields

		/// <summary>
		/// X position in the image, in pixels.
		/// </summary>
		public readonly int X;

		/// <summary>
		/// Y position in the image, in pixels.
		/// </summary>
		public readonly int Y;

		/// <summary>
		/// Width in pixels.
		/// </summary>
		public readonly int Width;

		/// <summary>
		/// Height in pixels.
		/// </summary>
		public readonly int Height;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureFontGlyph.
		/// </summary>
		public TextureFontGlyph(int x, int y, int width, int height)
		{
			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;
		}

		#endregion
	}
}
