﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Decides
	/// </summary>
	public interface ISignedDistanceFieldParameterProvider
	{
		/// <summary>
		/// Returns the <see cref="SignedDistanceFieldParameters"/> for the specified <see cref="Glyph"/>.
		/// </summary>
		/// <param name="glyph"></param>
		/// <returns></returns>
		SignedDistanceFieldParameters GetParameters(Glyph glyph);
	}
}
