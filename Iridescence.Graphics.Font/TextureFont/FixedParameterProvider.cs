﻿namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Implements a <see cref="ISignedDistanceFieldParameterProvider"/> that returns the same parameters for each glyph.
	/// </summary>
	public class FixedParameterProvider : ISignedDistanceFieldParameterProvider
	{
		#region Properties

		/// <summary>
		/// Gets the parameters.
		/// </summary>
		public SignedDistanceFieldParameters Parameters { get; }

		#endregion

		#region Constructors

		public FixedParameterProvider(SignedDistanceFieldParameters parameters)
		{
			this.Parameters = parameters;
		}

		#endregion

		#region Methods

		public SignedDistanceFieldParameters GetParameters(Glyph glyph)
		{
			SignedDistanceFieldParameters p = this.Parameters;
			p.FitGeometry(glyph.Geometry);
			return p;
		}
		
		#endregion
	}
}
