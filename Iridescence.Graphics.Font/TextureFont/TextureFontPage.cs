﻿using System.Collections.Generic;

namespace Iridescence.Graphics.Font
{
	/// <summary>
	/// Represents a texture font page that contains the glyph texture and the vertex data.
	/// </summary>
	public class TextureFontPage
	{
		#region Fields

		private readonly int padding;

		private readonly BinaryTreePackingNode packer;
		private readonly Dictionary<uint, TextureFontGlyph> glyphs;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the texture font that this page belongs to.
		/// </summary>
		public TextureFont TextureFont { get; }

		/// <summary>
		/// Gets the font instance.
		/// </summary>
		public Font Font => this.TextureFont.Font;

		/// <summary>
		/// Gets the page index.
		/// </summary>
		public int Index { get; }

		/// <summary>
		/// Gets the image of the texture font page.
		/// </summary>
		public Image Image { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureFontPage.
		/// </summary>
		internal TextureFontPage(TextureFont font, int index, Image image)
		{
			this.TextureFont = font;
			this.Index = index;
			this.Image = image;
			this.padding = 2;

			this.glyphs = new Dictionary<uint, TextureFontGlyph>();
			this.packer = new BinaryTreePackingNode(image.Width, image.Height);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds a glyph.
		/// </summary>
		/// <param name="codePoint"></param>
		/// <returns></returns>
		public bool Add(uint codePoint)
		{
			// render glyph.
			MemoryImage glyphImage = this.TextureFont.DrawGlyph(codePoint)?.ToMemoryImage(false);
			
			if (glyphImage != null)
			{
				// pack glyph into image.
				BinaryTreePackingNode node = this.packer.Add(glyphImage.Width + this.padding * 2, glyphImage.Height + this.padding * 2);
				if (node == null)
					return false;

				int x = node.X + this.padding;
				int y = node.Y + this.padding;

				// copy glyph.
				if (glyphImage.Width > 0 && glyphImage.Height > 0)
					this.Image.Write(glyphImage, x, y, 0);

				// create texture font glyph.
				this.glyphs.Add(codePoint, new TextureFontGlyph(x, y, glyphImage.Width, glyphImage.Height));
			}
			else
			{
				this.glyphs.Add(codePoint, new TextureFontGlyph(0, 0, 0, 0));
			}

			return true;
		}

		/// <summary>
		/// Returns the texture font glyph for the specified character.
		/// </summary>
		/// <param name="codePoint"></param>
		/// <returns></returns>
		public TextureFontGlyph GetGlyph(uint codePoint)
		{
			if (this.glyphs.TryGetValue(codePoint, out TextureFontGlyph glyph))
				return glyph;

			return null;
		}

		#endregion
	}
}
