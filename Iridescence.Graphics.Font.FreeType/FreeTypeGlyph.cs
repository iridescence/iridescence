﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics.Font.FreeType
{
	/// <summary>
	/// Implements Glyph using FreeType.
	/// </summary>
	[Serializable]
	internal sealed class FreeTypeGlyph : Glyph
	{
		#region Fields

		internal readonly FreeTypeFace FTFace;
		internal readonly uint GlyphIndex;

		private bool hasGeometry;
		private Geometry geometry;

		#endregion

		#region Properties

		public override Vector2D Size { get; }

		public override Vector2D Advance { get; }

		public override Geometry Geometry
		{
			get
			{
				if (!this.hasGeometry)
				{
					this.geometry = this.getGeometry();
					this.hasGeometry = true;
				}

				return this.geometry;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FreeTypeGlyph.
		/// </summary>
		internal FreeTypeGlyph(FreeTypeFace face, double size, uint codePoint, uint glyphIndex)
			: base(face, size, codePoint)
		{
			this.FTFace = face;
			this.GlyphIndex = glyphIndex;

			FT_GlyphSlot glyphRec;
			lock (this.FTFace.Mutex)
			{
				IntPtr glyphPtr = this.FTFace.LoadGlyph(this.GlyphIndex, this.FontSize, FT_Load_Flags.NoScale);
				glyphRec = FreeType.ReadGlyphSlot(glyphPtr);
			}

			double scale = size / this.FTFace.UnitsPerEM;
			this.Size = new Vector2D(glyphRec.metrics.width * scale, glyphRec.metrics.height * scale);
			this.Advance = new Vector2D(glyphRec.metrics.horiAdvance * scale, glyphRec.metrics.vertAdvance * scale);

			//Vector2 offsetVec = new Vector2(glyphRec.metrics.horiBearingX / 64.0f, -glyphRec.metrics.horiBearingY / 64.0f);
		}

		#endregion

		#region Methods

		public override GlyphImage Draw(GlyphImageKind kind)
		{
			switch (kind)
			{
				case GlyphImageKind.Monochrome:
					return FreeTypeGlyphImage.Create(this, FT_Load_Flags.Render | FT_Load_Flags.Monochrome,  FT_Render_Mode.Mono);
				case GlyphImageKind.Grayscale:
					return FreeTypeGlyphImage.Create(this, FT_Load_Flags.Render, FT_Render_Mode.Normal);
				default:
					throw new ArgumentOutOfRangeException(nameof(kind), kind, null);
			}
		}

		private Geometry getGeometry()
		{
			lock (this.FTFace.Mutex)
			{
				IntPtr glyphPtr = this.FTFace.LoadGlyph(this.GlyphIndex, this.FontSize, FT_Load_Flags.Default);
				FT_GlyphSlot slot = FreeType.ReadGlyphSlot(glyphPtr);

				FillRule rule = FillRule.NonZero;
				if ((slot.outline.flags & FT_Outline_Flags.EvenOddFill) != 0)
					rule = FillRule.EvenOdd;

				PathGeometry path = new PathGeometry(fillRule: rule);

				PathFigure currentFigure = null;

				void addFigure()
				{
					if (currentFigure != null && currentFigure.Segments.Length > 0)
					{
						path = path.AddFigure(currentFigure);
					}
				}

				FreeType.DecomposeOutline(slot.outline,
					moveTo: to =>
					{
						addFigure();
						currentFigure = new PathFigure(to, isClosed: true);
					},
					lineTo: to => { currentFigure = currentFigure?.LineTo(to); },
					conicTo: (cp, to) => { currentFigure = currentFigure?.QuadTo(cp, to); },
					cubicTo: (cp1, cp2, to) => { currentFigure = currentFigure?.CubicTo(cp1, cp2, to); });
				
				addFigure();
				
				if (path.Figures.Length == 0)
					return null;

				return path;
			}
		}

		#endregion
	}
}
