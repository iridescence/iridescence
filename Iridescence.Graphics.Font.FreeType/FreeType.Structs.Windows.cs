﻿using System;
using System.Runtime.InteropServices;
using Iridescence.Math;

using FT_Short = System.Int16;
using FT_UShort = System.UInt16;
using FT_Int = System.Int32;
using FT_UInt = System.UInt32;
using FT_Long = System.Int32;
using FT_Fixed = System.Int32;
using FT_Pos = System.Int32;

namespace Iridescence.Graphics.Font.FreeType
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_Vector
	{
		public FT_Pos x;
		public FT_Pos y;

		public static explicit operator Vector2(FT_Vector v)
		{
			Vector2 result;
			result.X = v.x / 64.0f;
			result.Y = v.y / 64.0f;
			return result;
		}

		public static explicit operator Vector2D(FT_Vector v)
		{
			Vector2D result;
			result.X = v.x / 64.0;
			result.Y = v.y / 64.0;
			return result;
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_BBox
	{
		public FT_Pos xMin, yMin;
		public FT_Pos xMax, yMax;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_Glyph_Metrics
	{
		public FT_Pos width;
		public FT_Pos height;

		public FT_Pos horiBearingX;
		public FT_Pos horiBearingY;
		public FT_Pos horiAdvance;

		public FT_Pos vertBearingX;
		public FT_Pos vertBearingY;
		public FT_Pos vertAdvance;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal sealed class FT_Face
	{
		public FT_Long num_faces;
		public FT_Long face_index;

		public FT_Long face_flags;
		public FT_Long style_flags;

		public FT_Long num_glyphs;

		public IntPtr family_name;
		public IntPtr style_name;

		public FT_Int num_fixed_sizes;
		public IntPtr available_sizes;

		public FT_Int num_charmaps;
		public IntPtr charmaps;

		public FT_Generic generic;

		public FT_BBox bbox;

		public FT_UShort units_per_EM;
		public FT_Short ascender;
		public FT_Short descender;
		public FT_Short height;

		public FT_Short max_advance_width;
		public FT_Short max_advance_height;

		public FT_Short underline_position;
		public FT_Short underline_thickness;

		public IntPtr glyph;
		public IntPtr size;
		public IntPtr charmap;

		internal IntPtr driver;
		internal IntPtr memory;
		internal IntPtr stream;

		internal IntPtr sizes_list;
		internal FT_Generic autohint;
		internal IntPtr extensions;

		internal IntPtr @internal;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal sealed class FT_GlyphSlot
	{
		public IntPtr library;
		public IntPtr face;
		public IntPtr next;
		public FT_UInt reserved;
		public FT_Generic generic;

		public FT_Glyph_Metrics metrics;
		public FT_Fixed linearHoriAdvance;
		public FT_Fixed linearVertAdvance;
		public FT_Vector advance;

		public FT_Glyph_Format format;

		public FT_Bitmap bitmap;
		public FT_Int bitmap_left;
		public FT_Int bitmap_top;

		public FT_Outline outline;

		public FT_UInt num_subglyphs;
		public IntPtr subglyphs;

		public IntPtr control_data;
		public FT_Long control_len;

		public FT_Pos lsb_delta;
		public FT_Pos rsb_delta;

		public IntPtr other;

		internal IntPtr @internal;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_Outline_Funcs
	{
		public FT_Outline_MoveToFunc move_to;
		public FT_Outline_LineToFunc line_to;
		public FT_Outline_ConicToFunc conic_to;
		public FT_Outline_CubicToFunc cubic_to;
		public int shift;
		public FT_Pos delta;
	}
}
