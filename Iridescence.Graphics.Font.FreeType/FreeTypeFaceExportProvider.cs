﻿using System;
using System.IO;
using Iridescence.FileSystem.Composition;
using File = Iridescence.FileSystem.File;

namespace Iridescence.Graphics.Font.FreeType
{
	/// <summary>
	/// Loads fonts using the FreeType library.
	/// </summary>
	public class FreeTypeFaceExportProvider : FileExportDescriptorProviderBase
	{
		#region Fields

		private readonly FreeTypeLibrary library;

		#endregion

		#region Properties

		protected override Type PartType => typeof(FreeTypeFace);

		protected override FileExportSharingMode SharingMode => FileExportSharingMode.HardReference;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FreeTypeFontLoader.
		/// </summary>
		public FreeTypeFaceExportProvider(FreeTypeLibrary library)
		{
			this.library = library;
		}

		#endregion

		#region Methods

		protected override bool IsSupportedName(string name)
		{
			return name.EndsWith(".ttf") ||
				   name.EndsWith(".otf") ||
				   name.EndsWith(".pfa") ||
				   name.EndsWith(".pfb") ||
				   name.EndsWith(".pfm") ||
				   name.EndsWith(".pfr") ||
				   name.EndsWith(".fnt");
		}

		protected override object Load(File file)
		{
			using (Stream stream = file.Open(FileMode.Open, FileAccess.Read))
			{
				return this.library.FromStream(stream);
			}
		}

		#endregion
	}
}
