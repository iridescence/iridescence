﻿using System;
using System.Runtime.InteropServices;
using Iridescence.Math;

namespace Iridescence.Graphics.Font.FreeType
{	
	/// <summary>
	/// FreeType functions and types.
	/// </summary>
	internal static class FreeType
	{
		#region Constants

		private const string libraryName = "freetype.dll";
		private const CallingConvention callConvention = CallingConvention.Cdecl;

		#endregion

		#region Fields

		public static readonly bool UseWindowsStructs;
		public static readonly int FTLongSize;

		#endregion

		#region Methods

		static FreeType()
		{
			PlatformID platform = Environment.OSVersion.Platform;

			FTLongSize = IntPtr.Size;

			// on windows, FT_Long is still 4 bytes, hence the different structs.
			if (platform == PlatformID.Win32NT ||
			    platform == PlatformID.Win32S || 
			    platform == PlatformID.Win32Windows || 
			    platform == PlatformID.WinCE ||
			    platform == PlatformID.Xbox)
			{
				FTLongSize = 4;
				UseWindowsStructs = true;
			}
		}

		public static unsafe FT_Vector ReadVector(IntPtr pointer)
		{
			if (UseWindowsStructs)
				return *(FT_Vector*)pointer;
			
			return *(FT_Vector2*)pointer;
		}

		public static FT_Face ReadFace(IntPtr pointer)
		{
			if (UseWindowsStructs)
			{
				FT_Face face = new FT_Face();
				Marshal.PtrToStructure(pointer, face);
				return face;
			}

			FT_Face2 face2 = new FT_Face2();
			Marshal.PtrToStructure(pointer, face2);
			return face2;
		}

		public static FT_GlyphSlot ReadGlyphSlot(IntPtr pointer)
		{
			if (UseWindowsStructs)
			{
				FT_GlyphSlot slot = new FT_GlyphSlot();
				Marshal.PtrToStructure(pointer, slot);
				return slot;
			}

			FT_GlyphSlot2 slot2 = new FT_GlyphSlot2();
			Marshal.PtrToStructure(pointer, slot2);
			return slot2;
		}

		public static void DecomposeOutline(FT_Outline outline, Action<Vector2D> moveTo, Action<Vector2D> lineTo, Action<Vector2D, Vector2D> conicTo, Action<Vector2D, Vector2D, Vector2D> cubicTo)
		{
			if (UseWindowsStructs)
			{
				FT_Outline_Funcs funcs = new FT_Outline_Funcs
				{
					move_to = (to, user) =>
					{
						moveTo((Vector2D)ReadVector(to));
						return 0;
					},
					line_to = (to, user) =>
					{
						lineTo((Vector2D)ReadVector(to));
						return 0;
					},
					conic_to = (cp1, to, user) =>
					{
						conicTo((Vector2D)ReadVector(cp1), (Vector2D)ReadVector(to));
						return 0;
					},
					cubic_to = (cp1, cp2, to, user) =>
					{
						cubicTo((Vector2D)ReadVector(cp1), (Vector2D)ReadVector(cp2), (Vector2D)ReadVector(to));
						return 0;
					},
					shift = 0,
					delta = 0
				};

				IntPtr outlinePtr = Marshal.AllocHGlobal(Marshal.SizeOf<FT_Outline>());
				IntPtr funcsPtr = Marshal.AllocHGlobal(Marshal.SizeOf<FT_Outline_Funcs>());
				try
				{
					Marshal.StructureToPtr(outline, outlinePtr, false);
					Marshal.StructureToPtr(funcs, funcsPtr, false);
					FT_Outline_Decompose(outlinePtr, funcsPtr, IntPtr.Zero);
				}
				finally
				{
					Marshal.FreeHGlobal(outlinePtr);
					Marshal.FreeHGlobal(funcsPtr);
				}
			}
			else
			{
				FT_Outline_Funcs2 funcs = new FT_Outline_Funcs2
				{
					move_to = (to, user) =>
					{
						moveTo((Vector2D)ReadVector(to));
						return 0;
					},
					line_to = (to, user) =>
					{
						lineTo((Vector2D)ReadVector(to));
						return 0;
					},
					conic_to = (cp1, to, user) =>
					{
						conicTo((Vector2D)ReadVector(cp1), (Vector2D)ReadVector(to));
						return 0;
					},
					cubic_to = (cp1, cp2, to, user) =>
					{
						cubicTo((Vector2D)ReadVector(cp1), (Vector2D)ReadVector(cp2), (Vector2D)ReadVector(to));
						return 0;
					},
					shift = 0,
					delta = IntPtr.Zero
				};

				IntPtr outlinePtr = Marshal.AllocHGlobal(Marshal.SizeOf<FT_Outline>());
				IntPtr funcsPtr = Marshal.AllocHGlobal(Marshal.SizeOf<FT_Outline_Funcs2>());
				try
				{
					Marshal.StructureToPtr(outline, outlinePtr, false);
					Marshal.StructureToPtr(funcs, funcsPtr, false);
					FT_Outline_Decompose(outlinePtr, funcsPtr, IntPtr.Zero);
				}
				finally
				{
					Marshal.FreeHGlobal(outlinePtr);
					Marshal.FreeHGlobal(funcsPtr);
				}
			}
		}

		public static void Assert(FT_Error error)
		{
			if (error != FT_Error.Ok) 
				throw new FreeTypeException(error);
		}

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_Init_FreeType(out IntPtr alibrary);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_Done_FreeType(IntPtr library);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_New_Face(IntPtr library, [MarshalAs(UnmanagedType.LPStr)] string filepathname, int face_index, out IntPtr aface);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_New_Memory_Face(IntPtr library, IntPtr file_base, int file_size, int face_index, out IntPtr aface);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_Done_Face(IntPtr face);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_Select_Charmap(IntPtr face, int encoding);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_Set_Char_Size(IntPtr face, uint char_width, uint char_height, uint horz_resolution, uint vert_resolution);
		
		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_Set_Pixel_Sizes(IntPtr face, uint pixel_width, uint pixel_height);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_Get_Kerning(IntPtr face, uint left_glyph, uint right_glyph, FT_Kerning_Mode kern_mode, IntPtr akerning);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern uint FT_Get_Char_Index(IntPtr face, uint charcode);

		public static ulong FT_Get_First_Char(IntPtr face, out uint agindex)
		{
			if (UseWindowsStructs)
				return FT_Get_First_Char_Win(face, out agindex);
			return FT_Get_First_Char_Unix(face, out agindex);
		}

		[DllImport(libraryName, CallingConvention = callConvention, EntryPoint = "FT_Get_First_Char")]
		private static extern ulong FT_Get_First_Char_Unix(IntPtr face, [Out] out uint agindex);

		[DllImport(libraryName, CallingConvention = callConvention, EntryPoint = "FT_Get_First_Char")]
		private static extern uint FT_Get_First_Char_Win(IntPtr face, [Out] out uint agindex);

		public static ulong FT_Get_Next_Char(IntPtr face, ulong char_code, out uint agindex)
		{
			if (UseWindowsStructs)
				return FT_Get_Next_Char_Win(face, (uint)char_code, out agindex);
			return FT_Get_Next_Char_Unix(face, char_code, out agindex);
		}

		[DllImport(libraryName, CallingConvention = callConvention, EntryPoint = "FT_Get_Next_Char")]
		private static extern ulong FT_Get_Next_Char_Unix(IntPtr face, ulong char_code, [Out] out uint agindex);

		[DllImport(libraryName, CallingConvention = callConvention, EntryPoint = "FT_Get_Next_Char")]
		private static extern uint FT_Get_Next_Char_Win(IntPtr face, uint char_code, [Out] out uint agindex);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_Load_Glyph(IntPtr face, uint glyph_index, FT_Load_Flags load_flags);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern FT_Error FT_Render_Glyph(IntPtr slot, FT_Render_Mode render_mode);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern void FT_Done_Glyph(IntPtr glyph);

		[DllImport(libraryName, CallingConvention = callConvention)]
		public static extern void FT_Outline_Decompose(IntPtr outline, IntPtr outlineFuncs, IntPtr user);

		#endregion
	}
}
