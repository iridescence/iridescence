﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Iridescence.Math;

namespace Iridescence.Graphics.Font.FreeType
{
	/// <summary>
	/// Implements FontFace using FreeType.
	/// </summary>
	internal sealed class FreeTypeFace : FontFace, IDisposable
	{
		#region Fields

		private readonly FreeTypeLibrary library;
		private readonly IntPtr fontData;
		private readonly Dictionary<KerningPair, Vector2I> kernings;
		internal readonly IntPtr facePtr;
		private ConcurrentFlag isDisposed;

		internal readonly object Mutex;

		private const double referenceSize = 64.0;
		internal readonly int UnitsPerEM;
		internal readonly bool HasKerning;
		private readonly double lineHeight;
		private readonly double ascender;
		private readonly double descender;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FreeTypeFace.
		/// </summary>
		public FreeTypeFace(string name, FreeTypeLibrary library, IntPtr fontData, IntPtr face)
			: base(name)
		{
			this.isDisposed = new ConcurrentFlag();

			this.library = library;
			this.library.Disposed += this.onLibraryDisposed;

			this.fontData = fontData;
			this.facePtr = face;

			// Select unicode charset.
			const int unicode = ('u' << 24 | 'n' << 16 | 'i' << 8 | 'c');
			FreeType.Assert(FreeType.FT_Select_Charmap(this.facePtr, unicode));

			this.SetPixelSize(referenceSize);
			FT_Face faceRec = FreeType.ReadFace(this.facePtr);
			this.UnitsPerEM = faceRec.units_per_EM;
			this.HasKerning = (faceRec.face_flags & 0x0040) != 0;
			
			double scale = referenceSize / this.UnitsPerEM;
			this.lineHeight = faceRec.height * scale;
			this.ascender = faceRec.ascender * scale;
			this.descender = faceRec.descender * scale;

			this.kernings = new Dictionary<KerningPair, Vector2I>();
			this.Mutex = new object();
		}

		~FreeTypeFace()
		{
			this.Dispose();
		}

		#endregion

		#region Methods

		internal void SetPixelSize(double size)
		{
			uint charHeight = (uint)(64 * size);
			FreeType.Assert(FreeType.FT_Set_Char_Size(this.facePtr, 0, charHeight, 72, 72));
		}

		public override Font GetFont(double size)
		{
			return new FreeTypeFont(this, size, 
				this.lineHeight * size / referenceSize,
				this.ascender * size / referenceSize,
				this.descender * size / referenceSize);
		}

		public override Glyph GetGlyph(double size, uint codePoint)
		{
			uint glyphIndex = FreeType.FT_Get_Char_Index(this.facePtr, codePoint);
			if (glyphIndex == 0)
				return null;
			return new FreeTypeGlyph(this, size, codePoint, glyphIndex);
		}
		
		public override Glyph GetUndefinedGlyph(double size, uint codePoint = 0)
		{
			return new FreeTypeGlyph(this, size, codePoint, 0);
		}

		public override IEnumerable<Glyph> GetGlyphs(double size)
		{
			ulong code = FreeType.FT_Get_First_Char(this.facePtr, out uint glyphIndex);

			while (glyphIndex != 0)
			{
				yield return new FreeTypeGlyph(this, size, (uint)code, glyphIndex);

				code = FreeType.FT_Get_Next_Char(this.facePtr, code, out glyphIndex);
			}
		}

		public override Vector2D GetKerning(uint first, uint second, double size)
		{
			KerningPair pair = new KerningPair(first, second);

			if (!this.kernings.TryGetValue(pair, out Vector2I result))
			{
				uint firstIndex = FreeType.FT_Get_Char_Index(this.facePtr, first);
				uint secondIndex = FreeType.FT_Get_Char_Index(this.facePtr, second);

				if (this.HasKerning) // has kerning.
				{
					IntPtr kerning = Marshal.AllocHGlobal(FreeType.FTLongSize * 2);

					try
					{
						FreeType.Assert(FreeType.FT_Get_Kerning(this.facePtr, firstIndex, secondIndex, FT_Kerning_Mode.Unscaled, kerning));

						FT_Vector vec = FreeType.ReadVector(kerning);
						result.X = vec.x;
						result.Y = vec.y;
					}
					finally
					{
						Marshal.FreeHGlobal(kerning);
					}
				}

				this.kernings.Add(pair, result);
			}

			double scale = size / this.UnitsPerEM;
			return new Vector2D(result.X * scale, result.Y * scale);
		}

		internal IntPtr LoadGlyph(uint glyphIndex, double pixelSize, FT_Load_Flags flags)
		{
			this.SetPixelSize(pixelSize);
			FreeType.Assert(FreeType.FT_Load_Glyph(this.facePtr, glyphIndex, flags));
			FT_Face faceRec = FreeType.ReadFace(this.facePtr);
			return faceRec.glyph;
		}

		private void onLibraryDisposed(object sender, EventArgs e)
		{
			this.Dispose();
		}

		public void Dispose()
		{
			if (!this.isDisposed.TrySet())
				return;

			this.library.Disposed -= this.onLibraryDisposed;

			FreeType.Assert(FreeType.FT_Done_Face(this.facePtr));

			if (this.fontData != IntPtr.Zero)
			{
				// Free font data if necessary.
				Marshal.FreeHGlobal(this.fontData);
			}
		}

		#endregion

		#region Nested Types

		private readonly struct KerningPair : IEquatable<KerningPair>
		{
			public readonly uint First;
			public readonly uint Second;

			public KerningPair(uint first, uint second)
			{
				this.First = first;
				this.Second = second;
			}

			public bool Equals(KerningPair other)
			{
				return this.First == other.First && this.Second == other.Second;
			}

			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				return obj is KerningPair pair && this.Equals(pair);
			}

			public override int GetHashCode()
			{
				unchecked
				{
					var hashCode = (int)this.First;
					hashCode = (hashCode * 397) ^ (int)this.Second;
					return hashCode;
				}
			}

			public static bool operator ==(KerningPair left, KerningPair right)
			{
				return left.Equals(right);
			}

			public static bool operator !=(KerningPair left, KerningPair right)
			{
				return !left.Equals(right);
			}
		}
		
		#endregion
	}
}