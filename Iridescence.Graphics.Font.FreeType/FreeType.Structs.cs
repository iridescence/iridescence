﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.Font.FreeType
{
	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	internal delegate void FT_Generic_Finalizer(IntPtr obj);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	internal delegate int FT_Outline_MoveToFunc(IntPtr to, IntPtr user);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	internal delegate int FT_Outline_LineToFunc(IntPtr to, IntPtr user);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	internal delegate int FT_Outline_ConicToFunc(IntPtr control, IntPtr to, IntPtr user);

	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	internal delegate int FT_Outline_CubicToFunc(IntPtr control1, IntPtr control2, IntPtr to, IntPtr user);
	
	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_Generic
	{
		public IntPtr data;
		public FT_Generic_Finalizer finalizer;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_Bitmap
	{
		public uint rows;
		public uint width;
		public int pitch;
		public IntPtr buffer;
		public ushort num_grays;
		public FT_Pixel_Mode pixel_mode;
		public byte palette_mode;
		public IntPtr palette;
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_Outline
	{
		public short n_contours;
		public short n_points;

		public IntPtr points;
		public IntPtr tags;
		public IntPtr contours;

		public FT_Outline_Flags flags;
	}
}
