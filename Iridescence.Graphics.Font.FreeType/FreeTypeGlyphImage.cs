﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Iridescence.Math;

namespace Iridescence.Graphics.Font.FreeType
{
	/// <summary>
	/// FreeType implementation of <see cref="GlyphImage"/>.
	/// </summary>
	internal sealed class FreeTypeGlyphImage : GlyphImage
	{
		#region Fields

		internal readonly FreeTypeGlyph FTGlyph;
		private readonly MemoryImage pixels;
		
		#endregion

		#region Properties

		public override Vector2 Offset { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FreeTypeGlyphImage"/> class. 
		/// </summary>
		public FreeTypeGlyphImage(FreeTypeGlyph glyph, MemoryImage pixels, Vector2 offset)
			: base(pixels.Format, pixels.Width, pixels.Height)
		{
			this.FTGlyph = glyph;
			this.pixels = pixels;
			this.Offset = offset;
		}

		#endregion

		#region Methods
		
		internal static unsafe FreeTypeGlyphImage Create(FreeTypeGlyph glyph, FT_Load_Flags loadFlags, FT_Render_Mode renderMode)
		{
			byte[] bytes;
			Format format;
			FT_GlyphSlot glyphRec;
			int width;
			int height;

			lock (glyph.FTFace.Mutex)
			{
				IntPtr glyphPtr = glyph.FTFace.LoadGlyph(glyph.GlyphIndex, glyph.FontSize, loadFlags);
				FreeType.Assert(FreeType.FT_Render_Glyph(glyphPtr, renderMode));
				glyphRec = FreeType.ReadGlyphSlot(glyphPtr);

				width = (int)glyphRec.bitmap.width;
				height = (int)glyphRec.bitmap.rows;
				if (width == 0 || height == 0)
					return null;

				void copyBitmap(int bpp)
				{
					IntPtr ptr = glyphRec.bitmap.buffer;
					for (int i = height - 1; i >= 0; i--)
					{
						Marshal.Copy(ptr, bytes, i * width, width);
						ptr += glyphRec.bitmap.pitch;
					}
				}

				switch (glyphRec.bitmap.pixel_mode)
				{
					case FT_Pixel_Mode.Mono:
					{
						format = new Format(FormatType.R8, FormatSemantic.UNorm,
							(ColorMappingChannel.Red, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
							(ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.One));
						bytes = new byte[width * height];

						IntPtr ptr = glyphRec.bitmap.buffer;
						int rowLength = (width + 7) / 8;
						byte[] row = new byte[rowLength];
						for (int i = height - 1; i >= 0; i--)
						{
							Marshal.Copy(ptr, row, 0, rowLength);
							ptr += glyphRec.bitmap.pitch;

							int j = i * width;
							for (int x = 0; x < width; ++x)
							{
								bytes[j++] = unchecked((byte)(-((row[x >> 3] >> (7 - (x & 7))) & 1)));
							}
						}
						break;
					}

					case FT_Pixel_Mode.Gray:
					{
						format = new Format(FormatType.R8, FormatSemantic.UNorm,
							(ColorMappingChannel.Red, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
							(ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.One));
						bytes = new byte[width * height];
						copyBitmap(1);
						break;
					}

					case FT_Pixel_Mode.Lcd:
					case FT_Pixel_Mode.VerticalLcd:
					{
						format = new Format(FormatType.R8G8B8, FormatSemantic.UNorm);
						bytes = new byte[width * height * 3];
						copyBitmap(3);
						break;
					}

					default:
						throw new NotSupportedException("FreeType pixel mode is not supported.");
				}

			}

			MemoryImage pixels = new MemoryImage(bytes, format, width, height);

			Vector2 offset;
			offset.X = glyphRec.bitmap_left;
			offset.Y = glyphRec.bitmap_top - glyphRec.bitmap.rows;

			return new FreeTypeGlyphImage(glyph, pixels, offset);
		}

		public override void Read(MemoryImage dest, int sourceX, int sourceY, int sourceZ)
		{
			this.pixels.Read(dest, sourceX, sourceY, sourceZ);
		}

		public override Task ReadAsync(MemoryImage dest, int sourceX, int sourceY, int sourceZ)
		{
			return this.pixels.ReadAsync(dest, sourceX, sourceY, sourceZ);
		}

		#endregion
	}
}
