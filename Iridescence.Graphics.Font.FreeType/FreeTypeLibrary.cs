﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.Font.FreeType
{
	/// <summary>
	/// Represents a FreeType library.
	/// </summary>
	public sealed class FreeTypeLibrary : IDisposable
	{
		#region Events

		internal event EventHandler Disposed;

		#endregion

		#region Fields

		private ConcurrentFlag isDisposed;

		internal readonly IntPtr ptr;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FreeTypeLibrary.
		/// </summary>
		public FreeTypeLibrary()
		{
			FreeType.Assert(FreeType.FT_Init_FreeType(out this.ptr));
		}

		~FreeTypeLibrary()
		{
			this.Dispose();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Reads a <see cref="FontFace"/> from a stream using freetype.
		/// </summary>
		/// <param name="stream">The stream to read from.</param>
		/// <param name="faceIndex">The face index.</param>
		/// <returns>The <see cref="FontFace"/> loaded from the stream.</returns>
		public FontFace FromStream(Stream stream, int faceIndex)
		{
			long len;

			try
			{
				len = stream.Length;
			}
			catch (NotSupportedException)
			{
				len = -1;
			}

			byte[] data;
			if (len >= 0)
			{
				data = stream.ReadOrThrow((int)stream.Length);
			}
			else
			{
				using (MemoryStream mem = new MemoryStream())
				{
					stream.CopyTo(mem);
					data = mem.ToArray();
				}
			}

			return this.FromMemory(data, faceIndex);
		}

		/// <summary>
		/// Reads a <see cref="FontFace"/> from a stream using freetype.
		/// </summary>
		/// <param name="stream">The stream to read from.</param>
		/// <returns>The <see cref="FontFace"/> loaded from the stream.</returns>
		public FontFace FromStream(Stream stream)
		{
			return this.FromStream(stream, 0);
		}
		
		/// <summary>
		/// Loads a <see cref="FontFace"/> from memory using freetype.
		/// </summary>
		/// <param name="data">The byte-array containing the font data.</param>
		/// <param name="faceIndex">The face index.</param>
		/// <returns>The <see cref="FontFace"/> loaded from memory.</returns>
		public FontFace FromMemory(ReadOnlySpan<byte> data, int faceIndex)
		{
			FreeTypeFace face = null;
			IntPtr facePtr = IntPtr.Zero;
			
			// Get unmanaged pointer for the data.
			IntPtr fontData = Marshal.AllocHGlobal(data.Length);
			try
			{
				unsafe
				{
					Span<byte> temp = new Span<byte>(fontData.ToPointer(), data.Length);
					data.CopyTo(temp);
				}

				// Create face from buffer.
				FreeType.Assert(FreeType.FT_New_Memory_Face(this.ptr, fontData, data.Length, faceIndex, out facePtr));

				// Get name.
				FT_Face rec = FreeType.ReadFace(facePtr);
				string familyName = Marshal.PtrToStringAnsi(rec.family_name);

				face = new FreeTypeFace(familyName, this, fontData, facePtr);
			}
			finally
			{
				if (face == null)
				{
					if (facePtr != IntPtr.Zero)
					{
						// FT face has been created but something else went wrong.
						FreeType.Assert(FreeType.FT_Done_Face(facePtr));
					}

					// Font has not been created. Free memory.
					Marshal.FreeHGlobal(fontData);
				}
			}

			return face;
		}

		/// <summary>
		/// Loads a <see cref="FontFace"/> from memory using freetype.
		/// </summary>
		/// <param name="data">The byte-array containing the font data.</param>
		/// <returns>The <see cref="FontFace"/> loaded from memory.</returns>
		public FontFace FromMemory(byte[] data)
		{
			return this.FromMemory(data, 0);
		}

		/// <summary>
		/// Loads a <see cref="FontFace"/> from a file.
		/// </summary>
		/// <param name="path">The file name.</param>
		/// <param name="faceIndex">The face index.</param>
		/// <returns>The <see cref="FontFace"/> loaded from the file.</returns>
		public FontFace FromFile(string path, int faceIndex)
		{
			FreeType.Assert(FreeType.FT_New_Face(this.ptr, path, faceIndex, out IntPtr facePtr));

			// Get name.
			FT_Face rec = FreeType.ReadFace(facePtr);
			string familyName = Marshal.PtrToStringAnsi(rec.family_name);

			return new FreeTypeFace(familyName, this, IntPtr.Zero, facePtr);
		}

		/// <summary>
		/// Loads a <see cref="FontFace"/> from a file.
		/// </summary>
		/// <param name="path">The file name.</param>
		/// <returns>The <see cref="FontFace"/> loaded from the file.</returns>
		public FontFace FromFile(string path)
		{
			return this.FromFile(path, 0);
		}

		public void Dispose()
		{
			if (!this.isDisposed.TrySet())
				return;

			this.Disposed?.Invoke(this, EventArgs.Empty);

			if (this.ptr != IntPtr.Zero)
			{
				FreeType.FT_Done_FreeType(this.ptr);	
			}
		}

		#endregion
	}
}