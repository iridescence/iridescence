﻿namespace Iridescence.Graphics.Font.FreeType
{
	/// <summary>
	/// Implements Font using FreeType.
	/// </summary>
	internal sealed class FreeTypeFont : Font
	{
		#region Constructors

		/// <summary>
		/// Creates a new FreeTypeFont.
		/// </summary>
		public FreeTypeFont(FreeTypeFace face, double size, double lineHeight, double ascender, double descender)
			: base(face, size, lineHeight, ascender, descender)
		{
			
		}

		#endregion
	}
}
