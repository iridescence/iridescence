﻿using System;
using System.Runtime.InteropServices;
using FT_Short = System.Int16;
using FT_UShort = System.UInt16;
using FT_Int = System.Int32;
using FT_UInt = System.UInt32;
using FT_Long = System.IntPtr;
using FT_Fixed = System.IntPtr;
using FT_Pos = System.IntPtr;

namespace Iridescence.Graphics.Font.FreeType
{
	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_Vector2
	{
		public FT_Pos x;
		public FT_Pos y;

		public static implicit operator FT_Vector(FT_Vector2 input)
		{
			FT_Vector result;
			result.x = (int)input.x;
			result.y = (int)input.y;
			return result;
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_BBox2
	{
		public FT_Pos xMin, yMin;
		public FT_Pos xMax, yMax;

		public static implicit operator FT_BBox(FT_BBox2 input)
		{
			FT_BBox result;
			result.xMin = (int)input.xMin;
			result.yMin = (int)input.yMin;
			result.xMax = (int)input.xMax;
			result.yMax = (int)input.yMax;
			return result;
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_Glyph_Metrics2
	{
		public FT_Pos width;
		public FT_Pos height;

		public FT_Pos horiBearingX;
		public FT_Pos horiBearingY;
		public FT_Pos horiAdvance;

		public FT_Pos vertBearingX;
		public FT_Pos vertBearingY;
		public FT_Pos vertAdvance;

		public static implicit operator FT_Glyph_Metrics(FT_Glyph_Metrics2 input)
		{
			FT_Glyph_Metrics result;
			result.width = (int)input.width;
			result.height = (int)input.height;
			result.horiBearingX = (int)input.horiBearingX;
			result.horiBearingY = (int)input.horiBearingY;
			result.horiAdvance = (int)input.horiAdvance;
			result.vertBearingX = (int)input.vertBearingX;
			result.vertBearingY = (int)input.vertBearingY;
			result.vertAdvance = (int)input.vertAdvance;
			return result;
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	internal sealed class FT_Face2
	{
		public FT_Long num_faces;
		public FT_Long face_index;

		public FT_Long face_flags;
		public FT_Long style_flags;

		public FT_Long num_glyphs;

		public IntPtr family_name;
		public IntPtr style_name;

		public FT_Int num_fixed_sizes;
		public IntPtr available_sizes;

		public FT_Int num_charmaps;
		public IntPtr charmaps;

		public FT_Generic generic;

		public FT_BBox bbox;

		public FT_UShort units_per_EM;
		public FT_Short ascender;
		public FT_Short descender;
		public FT_Short height;

		public FT_Short max_advance_width;
		public FT_Short max_advance_height;

		public FT_Short underline_position;
		public FT_Short underline_thickness;

		public IntPtr glyph;
		public IntPtr size;
		public IntPtr charmap;

		internal IntPtr driver;
		internal IntPtr memory;
		internal IntPtr stream;

		internal IntPtr sizes_list;
		internal FT_Generic autohint;
		internal IntPtr extensions;

		internal IntPtr @internal;

		public static implicit operator FT_Face(FT_Face2 input)
		{
			return new FT_Face
			{
				num_faces = (int)input.num_faces,
				face_index = (int)input.face_index,

				face_flags = (int)input.face_flags,
				style_flags = (int)input.style_flags,

				num_glyphs = (int)input.num_glyphs,

				family_name = input.family_name,
				style_name = input.style_name,

				num_fixed_sizes = input.num_fixed_sizes,
				available_sizes = input.available_sizes,

				num_charmaps = input.num_charmaps,
				charmaps = input.charmaps,

				generic = input.generic,

				bbox = input.bbox,

				units_per_EM = input.units_per_EM,
				ascender = input.ascender,
				descender = input.descender,
				height = input.height,

				max_advance_width = input.max_advance_width,
				max_advance_height = input.max_advance_height,

				underline_position = input.underline_position,
				underline_thickness = input.underline_thickness,

				glyph = input.glyph,
				size = input.size,
				charmap = input.charmap,

				driver = input.driver,
				memory = input.memory,
				stream = input.stream,

				sizes_list = input.sizes_list,

				autohint = input.autohint,
				extensions = input.extensions,

				@internal = input.@internal,
			};
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	internal sealed class FT_GlyphSlot2
	{
		public IntPtr library;
		public IntPtr face;
		public IntPtr next;
		public FT_UInt reserved;
		public FT_Generic generic;

		public FT_Glyph_Metrics metrics;
		public FT_Fixed linearHoriAdvance;
		public FT_Fixed linearVertAdvance;
		public FT_Vector advance;

		public FT_Glyph_Format format;

		public FT_Bitmap bitmap;
		public FT_Int bitmap_left;
		public FT_Int bitmap_top;

		public FT_Outline outline;

		public FT_UInt num_subglyphs;
		public IntPtr subglyphs;

		public IntPtr control_data;
		public FT_Long control_len;

		public FT_Pos lsb_delta;
		public FT_Pos rsb_delta;

		public IntPtr other;

		private IntPtr @internal;

		public static implicit operator FT_GlyphSlot(FT_GlyphSlot2 input)
		{
			return new FT_GlyphSlot
			{
				library = input.library,
				face = input.face,
				next = input.next,
				reserved = input.reserved,
				generic = input.generic,

				metrics = input.metrics,
				linearHoriAdvance = (int)input.linearHoriAdvance,
				linearVertAdvance = (int)input.linearVertAdvance,
				advance = input.advance,

				format = input.format,

				bitmap = input.bitmap,
				bitmap_left = input.bitmap_left,
				bitmap_top = input.bitmap_top,

				outline = input.outline,

				num_subglyphs = input.num_subglyphs,
				subglyphs = input.subglyphs,

				control_data = input.control_data,
				control_len = (int)input.control_len,

				lsb_delta = (int)input.lsb_delta,
				rsb_delta = (int)input.rsb_delta,

				other = input.other,
				
				@internal = input.@internal,
			};
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct FT_Outline_Funcs2
	{
		public FT_Outline_MoveToFunc move_to;
		public FT_Outline_LineToFunc line_to;
		public FT_Outline_ConicToFunc conic_to;
		public FT_Outline_CubicToFunc cubic_to;
		public int shift;
		public FT_Pos delta;
	}
}
