﻿using System;

namespace Iridescence.Graphics.Font.FreeType
{
	internal sealed class FreeTypeException : Exception
	{
		#region Constructors

		/// <summary>
		/// Creates a new FTException.
		/// </summary>
		public FreeTypeException(FT_Error error)
			: base(error.ToString())
		{

		}

		#endregion
	}
}
