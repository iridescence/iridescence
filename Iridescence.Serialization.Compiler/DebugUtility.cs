﻿using System;
using System.Reflection;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Debug utility.
	/// </summary>
	internal static class DebugUtility
	{
		#region Fields

		private static int indentation;

		#endregion

		#region Methods

		public static void WriteLine(string text, params object[] args)
		{
			string str = string.Format(text, args);
			Console.WriteLine($"{new string(' ', 3 * indentation)}{str}");
		}

		public static readonly MethodInfo WriteLineMethod = typeof(DebugUtility).GetMethod(nameof(WriteLine));

		public static void ChangeIndentation(int levels)
		{
			indentation += levels;
		}

		public static readonly MethodInfo ChangeIndentationMethod = typeof(DebugUtility).GetMethod(nameof(ChangeIndentation));

		#endregion
	}
}
