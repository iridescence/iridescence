﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents an abstract emit fragment.
	/// </summary>
	public interface IFragment<in TEmit> 
		where TEmit : EmitterBase
	{
		/// <summary>
		/// Emits the instructions to the specified <see cref="TEmit"/>.
		/// </summary>
		/// <param name="emit"></param>
		void Emit(TEmit emit);
	}
}
