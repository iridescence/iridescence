﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents an abstract writer build step.
	/// </summary>
	public interface IWriterBody : IFragment<WriterEmitter>
	{
		/// <summary>
		/// Gets the number of bytes that need to be reserved in the output stream before the code is executed.
		/// </summary>
		int ByteSize { get; }
	}
}