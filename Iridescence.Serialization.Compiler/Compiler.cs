﻿using System;
using System.Collections.Generic;

namespace Iridescence.Serialization.Compiler
{
	public class Compiler : ICompiler
	{
		#region Fields

		private readonly Dictionary<Type, RWCompiler> cache;

		#endregion

		#region Properties

		public CompilerConfiguration Configuration { get; }

		#endregion

		#region Constructors

		public Compiler(CompilerConfiguration config)
		{
			this.Configuration = config;
			this.cache = new Dictionary<Type, RWCompiler>();
		}

		#endregion

		#region Methods

		public IWriterCompiler To(Type outputType)
		{
			lock (this.cache)
			{
				if (!this.cache.TryGetValue(outputType, out RWCompiler tuple) || tuple.Writer == null)
				{
					tuple.Writer = new WriterCompiler(this.Configuration, outputType);
					this.cache[outputType] = tuple;
				}

				return tuple.Writer;
			}
		}

		public IReaderCompiler From(Type inputType)
		{
			lock (this.cache)
			{
				if (!this.cache.TryGetValue(inputType, out RWCompiler tuple) || tuple.Reader == null)
				{
					tuple.Reader = new ReaderCompiler(this.Configuration, inputType);
					this.cache[inputType] = tuple;
				}

				return tuple.Reader;
			}
		}

		#endregion

		#region Nested Types

		public struct RWCompiler
		{
			public IReaderCompiler Reader;
			public IWriterCompiler Writer;
		}

		#endregion
	}
}
