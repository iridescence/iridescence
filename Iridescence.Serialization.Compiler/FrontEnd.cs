﻿using System;

namespace Iridescence.Serialization.Compiler
{
	#region Basic

	public interface IDelegatePair
	{
		Delegate Generic { get; }
		Delegate NonGeneric { get; }
	}

	public interface IDelegatePairTuple
	{
		IDelegatePair Content { get; }
		IDelegatePair Member { get; }
	}

	public interface IReaderDelegatePairTuple : IDelegatePairTuple
	{
		IDelegatePairTuple Deferred(Type stateType);
	}

	public interface IWriterCompiler
	{
		IDelegatePairTuple For(SerializationMember memberSpec);
	}

	public interface IReaderCompiler
	{
		IReaderDelegatePairTuple For(SerializationMember memberSpec);
	}

	public interface ICompiler
	{
		IWriterCompiler To(Type outputType);
		IReaderCompiler From(Type inputType);
	}

	public interface IMaskable
	{
		MaskingInfo MaskingInfo { get; }
	}

	#endregion

	#region Generic

	public interface IWriterDelegatePair<in TOutput, TObj>
	{
		ObjectWriterRef<TOutput, TObj> Generic { get; }
		ObjectWriter<TOutput, object> NonGeneric { get; }
	}

	public interface IWriterDelegatePairTuple<in TOutput, TObj>
	{
		IWriterDelegatePair<TOutput, TObj> Content { get; }
		IWriterDelegatePair<TOutput, TObj> Member { get; }
	}

	public interface IWriterCompiler<in TOutput>
	{
		IWriterDelegatePairTuple<TOutput, TObj> For<TObj>(SerializationMember memberSpec = null);
	}

	public interface IReaderDelegatePair<in TInput, TObj>
	{
		ObjectReader<TInput, TObj> Generic { get; }
		ObjectReader<TInput, object> NonGeneric { get; }
	}

	public interface IDeferredReaderDelegatePair<in TInput, out TObj, TState>
	{
		ObjectReaderDeferred<TInput, TObj, TState> Generic { get; }
		ObjectReaderDeferred<TInput, object, TState> NonGeneric { get; }
	}

	public interface IDeferredReaderDelegatePairTuple<in TInput, out TObj, TState>
	{
		IDeferredReaderDelegatePair<TInput, TObj, TState> Content { get; }
		IDeferredReaderDelegatePair<TInput, TObj, TState> Member { get; }
	}

	public interface IReaderDelegatePairTuple<in TInput, TObj>
	{
		IReaderDelegatePair<TInput, TObj> Content { get; }
		IReaderDelegatePair<TInput, TObj> Member { get; }
		IDeferredReaderDelegatePairTuple<TInput, TObj, TState> Deferred<TState>();
	}

	public interface IReaderCompiler<in TInput>
	{
		IReaderDelegatePairTuple<TInput, TObj> For<TObj>(SerializationMember memberSpec = null);
	}

	public static class GenericExtensions
	{
		public static IWriterCompiler<TOutput> To<TOutput>(this ICompiler compiler)
		{
			return new WriterCompilerAdapter<TOutput>(compiler.To(typeof(TOutput)));
		}

		public static IReaderCompiler<TInput> From<TInput>(this ICompiler compiler)
		{
			return new ReaderCompilerAdapter<TInput>(compiler.From(typeof(TInput)));
		}

		public static IWriterDelegatePairTuple<TOutput, TObj> For<TOutput, TObj>(this IWriterCompiler compiler, SerializationMember memberSpec = null)
		{
			memberSpec ??= new SerializationMember(typeof(TObj));
			return new WriterDelegatePairTupleAdapter<TOutput, TObj>(compiler.For(memberSpec));
		}

		public static IReaderDelegatePairTuple<TInput, TObj> For<TInput, TObj>(this IReaderCompiler compiler, SerializationMember memberSpec = null)
		{
			memberSpec ??= new SerializationMember(typeof(TObj));
			return new ReaderDelegatePairTupleAdapter<TInput, TObj>(compiler.For(memberSpec));
		}

		private sealed class WriterDelegatePairAdapter<TOutput, TObj> : IWriterDelegatePair<TOutput, TObj>
		{
			private readonly IDelegatePair inner;

			public ObjectWriterRef<TOutput, TObj> Generic => (ObjectWriterRef<TOutput, TObj>)this.inner.Generic;

			public ObjectWriter<TOutput, object> NonGeneric => (ObjectWriter<TOutput, object>)this.inner.NonGeneric;

			public WriterDelegatePairAdapter(IDelegatePair inner)
			{
				this.inner = inner;
			}
		}

		private sealed class ReaderDelegatePairAdapter<TInput, TObj> : IReaderDelegatePair<TInput, TObj>
		{
			private readonly IDelegatePair inner;

			public ObjectReader<TInput, TObj> Generic => (ObjectReader<TInput, TObj>)this.inner.Generic;

			public ObjectReader<TInput, object> NonGeneric => (ObjectReader<TInput, object>)this.inner.NonGeneric;

			public ReaderDelegatePairAdapter(IDelegatePair inner)
			{
				this.inner = inner;
			}
		}

		private sealed class WriterDelegatePairTupleAdapter<TOutput, TObj> : IWriterDelegatePairTuple<TOutput, TObj>
		{
			private readonly IDelegatePairTuple inner;

			public WriterDelegatePairTupleAdapter(IDelegatePairTuple inner)
			{
				this.inner = inner;
			}

			public IWriterDelegatePair<TOutput, TObj> Content => new WriterDelegatePairAdapter<TOutput, TObj>(this.inner.Content);

			public IWriterDelegatePair<TOutput, TObj> Member => new WriterDelegatePairAdapter<TOutput, TObj>(this.inner.Member);
		}

		private sealed class DeferredReaderDelegatePairAdapter<TInput, TObj, TState> : IDeferredReaderDelegatePair<TInput, TObj, TState>
		{
			private readonly IDelegatePair inner;

			public ObjectReaderDeferred<TInput, TObj, TState> Generic => (ObjectReaderDeferred<TInput, TObj, TState>)this.inner.Generic;

			public ObjectReaderDeferred<TInput, object, TState> NonGeneric => (ObjectReaderDeferred<TInput, object, TState>)this.inner.NonGeneric;

			public DeferredReaderDelegatePairAdapter(IDelegatePair inner)
			{
				this.inner = inner;
			}
		}

		private sealed class DeferredReaderDelegatePairTupleAdapter<TInput, TObj, TState> : IDeferredReaderDelegatePairTuple<TInput, TObj, TState>
		{
			private readonly IDelegatePairTuple inner;

			public IDeferredReaderDelegatePair<TInput, TObj, TState> Content => new DeferredReaderDelegatePairAdapter<TInput, TObj, TState>(this.inner.Content);

			public IDeferredReaderDelegatePair<TInput, TObj, TState> Member => new DeferredReaderDelegatePairAdapter<TInput, TObj, TState>(this.inner.Member);

			public DeferredReaderDelegatePairTupleAdapter(IDelegatePairTuple inner)
			{
				this.inner = inner;
			}
		}

		private sealed class ReaderDelegatePairTupleAdapter<TInput, TObj> : IReaderDelegatePairTuple<TInput, TObj>
		{
			private readonly IReaderDelegatePairTuple inner;

			public ReaderDelegatePairTupleAdapter(IReaderDelegatePairTuple inner)
			{
				this.inner = inner;
			}

			public IReaderDelegatePair<TInput, TObj> Content => new ReaderDelegatePairAdapter<TInput, TObj>(this.inner.Content);

			public IReaderDelegatePair<TInput, TObj> Member => new ReaderDelegatePairAdapter<TInput, TObj>(this.inner.Member);

			public IDeferredReaderDelegatePairTuple<TInput, TObj, TState> Deferred<TState>()
			{
				return new DeferredReaderDelegatePairTupleAdapter<TInput, TObj, TState>(this.inner.Deferred(typeof(TState)));
			}
		}

		private sealed class WriterCompilerAdapter<TOutput> : IWriterCompiler<TOutput>
		{
			private readonly IWriterCompiler inner;

			public WriterCompilerAdapter(IWriterCompiler inner)
			{
				this.inner = inner;
			}

			public IWriterDelegatePairTuple<TOutput, TObj> For<TObj>(SerializationMember memberSpec = null)
			{
				return this.inner.For<TOutput, TObj>(memberSpec);
			}
		}

		private sealed class ReaderCompilerAdapter<TInput> : IReaderCompiler<TInput>
		{
			private readonly IReaderCompiler inner;

			public ReaderCompilerAdapter(IReaderCompiler inner)
			{
				this.inner = inner;
			}

			public IReaderDelegatePairTuple<TInput, TObj> For<TObj>(SerializationMember memberSpec = null)
			{
				return this.inner.For<TInput, TObj>(memberSpec);
			}
		}
	}

	#endregion
}