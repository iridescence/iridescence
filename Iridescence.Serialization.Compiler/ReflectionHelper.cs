﻿using System;
using System.Reflection;

namespace Iridescence.Serialization.Compiler
{
	internal static class ReflectionHelper
	{
		public static readonly MethodInfo EmittedMethod_GetCapturedValue = typeof(EmittedMethod).FindMethod(nameof(EmittedMethod.GetCapturedValue));

		public static readonly MethodInfo IWriterCompiler_For = typeof(IWriterCompiler).FindMethod(nameof(IWriterCompiler.For));
		public static readonly MethodInfo IReaderCompiler_For = typeof(IReaderCompiler).FindMethod(nameof(IReaderCompiler.For));

		public static readonly MethodInfo IDelegatePairTuple_Content = typeof(IDelegatePairTuple).FindPropertyGetter(nameof(IDelegatePairTuple.Content));
		public static readonly MethodInfo IDelegatePairTuple_Member = typeof(IDelegatePairTuple).FindPropertyGetter(nameof(IDelegatePairTuple.Member));

		public static readonly MethodInfo IReaderDelegatePairTuple_Deferred = typeof(IReaderDelegatePairTuple).FindMethod(nameof(IReaderDelegatePairTuple.Deferred), new[] {typeof(Type)});
		
		public static readonly MethodInfo IDelegatePair_Generic = typeof(IDelegatePair).FindPropertyGetter(nameof(IDelegatePair.Generic));
		public static readonly MethodInfo IDelegatePair_NonGeneric = typeof(IDelegatePair).FindPropertyGetter(nameof(IDelegatePair.NonGeneric));

		public static readonly MethodInfo IMaskable_GetMaskingInfo = typeof(IMaskable).FindPropertyGetter(nameof(IMaskable.MaskingInfo));
		public static readonly MethodInfo MaskingInfo_GetMember = typeof(MaskingInfo).FindMethod(nameof(MaskingInfo.GetMember));

		public static readonly MethodInfo SerializationMember_GetType = typeof(SerializationMember).FindPropertyGetter(nameof(SerializationMember.Type));
		public static readonly MethodInfo SerializationMember_ChangeType = typeof(SerializationMember).FindMethod(nameof(SerializationMember.ChangeType), new[] {typeof(Type)});

		public static readonly MethodInfo ReaderAdapterProvider_Get = typeof(ReaderAdapterProvider).FindMethod(nameof(ReaderAdapterProvider.Get));

		public static readonly MethodInfo Object_GetType = typeof(object).FindMethod(nameof(GetType));
	}
}