﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents a compilation error.
	/// </summary>
	[Serializable]
	public class CompilationException : Exception
	{
		public CompilationException()
		{
		}

		public CompilationException(string message) : base(message)
		{
		}

		public CompilationException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected CompilationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
