﻿using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// A debug print <see cref="IAugmentation"/> that writes a line to the console.
	/// </summary>
	public class DebugAugmentation : IAugmentation
	{
		#region Fields

		private readonly int indentation;
		private readonly string[] text;

		#endregion

		#region Properties

		public AugmentationPosition Position { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DebugAugmentation"/> class. 
		/// </summary>
		public DebugAugmentation(AugmentationPosition position, int indentation, params string[] text)
		{
			this.Position = position;
			this.text = text;
			this.indentation = indentation;
		}

		#endregion

		#region Methods

		public void Emit(AugmentationEmitter emit)
		{
			if (this.indentation < 0)
			{
				emit.LoadConstant(this.indentation);
				emit.Call(DebugUtility.ChangeIndentationMethod);
			}

			using (Local args = emit.DeclareLocal(typeof(object[]), "debugArgs"))
			{
				emit.LoadConstant(1);
				emit.NewArray(typeof(object));
				emit.StoreLocal(args);

				emit.LoadLocal(args);
				emit.LoadConstant(0);
				emit.LoadStream();
				emit.CallVirtual(Utility.StreamGetPositionMethod);
				emit.Box(typeof(long));
				emit.StoreElement(typeof(object));

				foreach (string line in this.text)
				{
					emit.LoadConstant(line);
					emit.LoadLocal(args);
					emit.Call(DebugUtility.WriteLineMethod);
				}
			}

			if (this.indentation > 0)
			{
				emit.LoadConstant(this.indentation);
				emit.Call(DebugUtility.ChangeIndentationMethod);
			}
		}
		
		#endregion
	}
}