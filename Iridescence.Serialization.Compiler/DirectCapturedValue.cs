﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents a directly captured value.
	/// </summary>
	public class DirectCapturedValue : ICapturedValue
	{
		private readonly object value;

		public Type Type { get; }

		public DirectCapturedValue(object value, Type type = null)
		{
			this.value = value;
			this.Type = type ?? value.GetType();
		}

		public object GetValue()
		{
			return this.value;
		}

		public override bool Equals(object obj)
		{
			return ReferenceEquals(this, obj) || (obj is DirectCapturedValue v && Equals(v.value, this.value));
		}

		public override int GetHashCode()
		{
			return this.value?.GetHashCode() ?? 0;
		}
	}
}