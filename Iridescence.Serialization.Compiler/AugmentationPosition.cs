﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Defines the placement of an <see cref="IAugmentation"/>.
	/// </summary>
	public enum AugmentationPosition
	{
		/// <summary>
		/// The code will be emitted before the main (de)serialization takes place.
		/// </summary>
		BeforeBody,

		/// <summary>
		/// The code will be emitted after the main (de)serialization took place.
		/// </summary>
		AfterBody,

		/// <summary>
		/// The code will be emitted into the deferred callback after deserialization.
		/// In a non-deferred context this has the same meaning as <see cref="AfterBody"/>.
		/// </summary>
		AfterBodyDeferred
	}
}
