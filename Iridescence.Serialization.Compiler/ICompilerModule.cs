﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents an abstract compiler module.
	/// </summary>
	public interface ICompilerModule
	{
		/// <summary>
		/// Adds the build steps for the writer to the build.
		/// </summary>
		/// <param name="template"></param>
		void ConfigureWriter(WriterTemplate template);

		/// <summary>
		/// Checks whether this module is compatible with the specified destination/output/stream class.
		/// </summary>
		/// <param name="outputType"></param>
		void CheckWriterCompatibility(Type outputType);

		/// <summary>
		/// Adds the build steps for the reader to the build.
		/// </summary>
		/// <param name="template"></param>
		void ConfigureReader(ReaderTemplate template);

		/// <summary>
		/// Checks whether this module is compatible with the specified source/input/stream class.
		/// </summary>
		/// <param name="inputType"></param>
		void CheckReaderCompatibility(Type inputType);
	}

	/// <summary>
	/// Represents a compiler module that emits code to read or write the contents of a specific type.
	/// </summary>
	public interface IContentModule : ICompilerModule
	{
		
	}

	/// <summary>
	/// Represents a compiler module that emits code to read or write a member.
	/// </summary>
	public interface IMemberModule : ICompilerModule
	{
		
	}
}
