﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents an augmentation emit fragment that can be used to place code before or after the body of an emitted method.
	/// Possible uses for this include invoking methods/events/callback before or after an object is read or written, preparing or initializing the object or providing debug tracing.
	/// </summary>
	public interface IAugmentation : IFragment<AugmentationEmitter>
	{
		/// <summary>
		/// Gets a value that determines where to put this augmentation.
		/// </summary>
		AugmentationPosition Position { get; }
	}
}
