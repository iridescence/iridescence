﻿using System;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Augmentation-specific stack operations.
	/// </summary>
	public class AugmentationEmitter : EmitterBase
	{
		#region Fields

		private readonly LoadFunc load;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value that indicates whether the value can be loaded.
		/// </summary>
		public bool HasValue => this.load != null;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AugmentationEmitter"/> class. 
		/// </summary>
		public AugmentationEmitter(EmitterBase emit, LoadFunc load)
			 : base(emit.Unit, emit, emit.CapturedValues, emit.Trace)
		{
			this.load = load;
		}

		public AugmentationEmitter(WriterEmitter emit)
			: base(emit.Unit, emit, emit.CapturedValues, emit.Trace)
		{
			this.load = emit.Load;
		}

		public AugmentationEmitter(ReaderEmitterDirect emit)
			: base(emit.Unit, emit, emit.CapturedValues, emit.Trace)
		{
			this.load = emit.Load;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Loads the value that is being written or read onto the stack.
		/// Only possible if <see cref="HasValue"/> is true.
		/// </summary>
		public void LoadValue()
		{
			if (!this.HasValue)
				throw new InvalidOperationException("Can't load value.");
			this.load(LoadMode.Value);
		}

		/// <summary>
		/// Loads the address of the value that is being written or read onto the stack.
		/// Only possible if <see cref="HasValue"/> is true.
		/// </summary>
		public void LoadValueAddress()
		{
			if (!this.HasValue)
				throw new InvalidOperationException("Can't load value.");
			this.load(LoadMode.Address);
		}

		#endregion
	}
}
