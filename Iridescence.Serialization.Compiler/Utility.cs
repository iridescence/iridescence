﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Utility functionality.
	/// </summary>
	internal static class Utility
	{
		#region Fields

		internal static readonly MethodInfo StreamGetPositionMethod = typeof(Stream).GetProperty(nameof(Stream.Position))?.GetGetMethod() ?? throw new MissingMethodException();

		#endregion

		#region Properties

		/// <summary>
		/// Gets the default encoding for strings.
		/// </summary>
		public static Encoding DefaultEncoding { get; } = new UTF8Encoding(false, true);

		#endregion

		#region Methods

		/// <summary>
		/// Returns the "TSource" or "TDest" parameter of <see cref="ObjectWriter{TDest,TInput}"/>, <see cref="ObjectWriterRef{TDest, TInput}"/>, <see cref="ObjectReader{TSource, TResult}"/> and <see cref="ObjectReaderDeferred{TSource, TResult, TState}"/>
		/// </summary>
		/// <returns></returns>
		public static Type GetIOType(Delegate d)
		{
			return GetIOType(d.GetType());
		}
		
		/// <summary>
		/// Returns the "TSource" or "TDest" parameter of <see cref="ObjectWriter{TDest,TInput}"/>, <see cref="ObjectWriterRef{TDest, TInput}"/>, <see cref="ObjectReader{TSource, TResult}"/> and <see cref="ObjectReaderDeferred{TSource, TResult, TState}"/>
		/// </summary>
		/// <returns></returns>
		public static Type GetIOType(Type delegateType)
		{
			if (!delegateType.IsConstructedGenericType)
				throw new ArgumentException("Not an object reader/writer delegate type.", nameof(delegateType));

			Type genericDef = delegateType.GetGenericTypeDefinition();
			if (genericDef != typeof(ObjectWriter<,>) && genericDef != typeof(ObjectWriterRef<,>) && genericDef != typeof(ObjectReader<,>) && genericDef != typeof(ObjectReaderDeferred<,,>))
				throw new ArgumentException("Not an object reader/writer delegate type.", nameof(delegateType));

			return delegateType.GetGenericArguments()[0];
		}

		/// <summary>
		/// Returns the "TState" parameter of <see cref="ObjectReaderDeferred{TSource, TResult, TState}"/>
		/// </summary>
		/// <returns></returns>
		public static Type GetStateType(Delegate d)
		{
			return GetStateType(d.GetType());
		}
		
		/// <summary>
		/// Returns the "TState" parameter of <see cref="ObjectReaderDeferred{TSource, TResult, TState}"/>
		/// </summary>
		/// <returns></returns>
		public static Type GetStateType(Type delegateType)
		{
			if (!delegateType.IsConstructedGenericType)
				throw new ArgumentException("Not a deferred object reader delegate type.", nameof(delegateType));

			Type genericDef = delegateType.GetGenericTypeDefinition();
			if (genericDef != typeof(ObjectReaderDeferred<,,>))
				throw new ArgumentException("Not a deferred object reader delegate type.", nameof(delegateType));

			return delegateType.GetGenericArguments()[2];
		}

		internal static FieldInfo FindField(this Type type, string fieldName)
		{
			FieldInfo field = type.GetField(fieldName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			if (field == null)
				throw new MissingFieldException(type.GetDisplayName(), fieldName);

			return field;
		}

		internal static MethodInfo FindMethod(this Type type, string methodName)
		{
			MethodInfo method = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			if (method == null)
				throw new MissingMethodException(type.GetDisplayName(), methodName);

			return method;
		}

		internal static MethodInfo FindMethod(this Type type, string methodName, Type[] parameters)
		{
			MethodInfo method = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, parameters, null);
			if (method == null)
				throw new MissingMethodException(type.GetDisplayName(), methodName);

			return method;
		}

		internal static MethodInfo FindGenericMethod(this Type type, string methodName, int numParams, Func<Type, int, bool> parameterFilter)
		{
			foreach (MethodInfo method in type
				.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
				.Where(m => m.Name == methodName && m.IsGenericMethodDefinition))
			{
				ParameterInfo[] parameters = method.GetParameters();
				if (parameters.Length != numParams)
					continue;

				bool match = true;
				for (int i = 0; i < parameters.Length; ++i)
				{
					Type p = parameters[i].ParameterType;
					if (p.IsGenericType)
						p = p.GetGenericTypeDefinition();

					if (!parameterFilter(p, i))
					{
						match = false;
						break;
					}
				}

				if (match)
				{
					return method;
				}
			}

			throw new MissingMethodException(type.GetDisplayName(), methodName);
		}

		internal static MethodInfo FindPropertyGetter(this Type type, string propName)
		{
			PropertyInfo prop = type.GetProperty(propName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			if (prop == null)
				throw new MissingMemberException(type.GetDisplayName(), propName);

			MethodInfo getter = prop.GetGetMethod(true);
			if (getter == null)
				throw new MissingMethodException(type.GetDisplayName(), $"get_{propName}");

			return getter;
		}
		
		#endregion
	}
}
