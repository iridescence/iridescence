﻿using System;
using System.Text;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements a content module for strings that encodes/decodes string to/from the stream using a specified <see cref="Encoding"/>.
	/// </summary>
	public class StringContentModule : IContentModule
	{
		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.Specification.Type != typeof(string) || template.HasBody)
				return;

			template.Body = new StringBody();
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.Specification.Type != typeof(string) || template.HasBody)
				return;

			template.Body = new StringBody();
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class StringBody : IWriterBody, IReaderBody
		{
			public int ByteSize => 0;

			public bool IsDeferred => false;

			public bool CustomAugmentationHandling => false;

			public void Emit(WriterEmitter emit)
			{
				emit.LoadStream();
				emit.LoadValue();
				emit.Stream.WriteString();
			}

			public void Emit(ReaderEmitter emit)
			{
				using (emit.Store())
				{
					emit.LoadStream();
					emit.Stream.ReadString();
				}
			}
		}
	}
}
