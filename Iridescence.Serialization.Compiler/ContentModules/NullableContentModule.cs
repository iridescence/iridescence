﻿using System;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements reader/writer compiling for <see cref="Nullable{T}"/>.
	/// </summary>
	public class NullableContentModule : IContentModule
	{
		#region Methods

		public void ConfigureWriter(WriterTemplate template)
		{
			if (!template.Specification.Type.IsNullable(out Type nullableBaseType) || template.HasBody)
				return;

			SerializationMember valueMember = template.Specification.Descend(nullableBaseType, "Value");
			WriterTemplate writer = template.TemplateFactory.CreateTemplate(valueMember, SerializationMode.Member);

			template.Body = new NullableWriterBody(template.Specification, writer);
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (!template.Specification.Type.IsNullable(out Type nullableBaseType) || template.HasBody)
				return;

			SerializationMember valueMember = template.Specification.Descend(nullableBaseType, "Value");
			ReaderTemplate reader = template.TemplateFactory.CreateTemplate(valueMember, SerializationMode.Member);

			template.Body = new NullableReaderBody(template.Specification, reader, new AugmentationCollection(template));
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class NullableWriterBody : IWriterBody
		{
			private readonly SerializationMember member;
			private readonly WriterTemplate valueWriter;
			private readonly Type nullableBaseType;

			public int ByteSize => 1;

			public NullableWriterBody(SerializationMember member, WriterTemplate valueWriter)
			{
				this.member = member;
				this.valueWriter = valueWriter;
				this.nullableBaseType = this.member.Type.GenericTypeArguments[0];
			}

			public void Emit(WriterEmitter emit)
			{
				// Write true/false for HasValue, followed by value if true.
				Label hasValueLabel = emit.DefineLabel();
				Label endLabel = emit.DefineLabel();

				// If !obj.HasValue
				emit.LoadValueAddress();
				emit.Call(this.member.Type.GetProperty("HasValue").GetGetMethod());
				emit.BranchIfTrue(hasValueLabel);
				{
					// Write false to stream.
					emit.LoadStream();
					emit.LoadConstant(false);
					emit.Stream.WriteBoolean(false);
				}
				// Else
				emit.Branch(endLabel);
				emit.MarkLabel(hasValueLabel);
				{
					// Write true to stream.
					emit.LoadStream();
					emit.LoadConstant(true);
					emit.Stream.WriteBoolean(false);

					using (Local nullableValueLocal = emit.DeclareLocal(this.nullableBaseType, "nullable"))
					{
						// Get obj.Value and store in a local.
						emit.LoadValueAddress();
						emit.Call(this.member.Type.GetProperty("Value").GetGetMethod());
						emit.StoreLocal(nullableValueLocal);

						// Write value.
						this.valueWriter.Emit(emit.WithLoad(LoadStore.LoadLocal(emit, nullableValueLocal)), true);
					}
				}
				emit.MarkLabel(endLabel);
			}
		}

		private sealed class NullableReaderBody : IReaderBody
		{
			private readonly SerializationMember member;
			private readonly ReaderTemplate readerTemplate;
			private readonly AugmentationCollection augs;
			private readonly Type nullableBaseType;

			public int ByteSize => 1;

			public bool IsDeferred => this.readerTemplate.IsDeferred;

			public bool CustomAugmentationHandling => true;

			public NullableReaderBody(SerializationMember member, ReaderTemplate readerTemplate, AugmentationCollection augs)
			{
				this.member = member;
				this.readerTemplate = readerTemplate;
				this.augs = augs;
				this.nullableBaseType = this.member.Type.GenericTypeArguments[0];
			}

			public void Emit(ReaderEmitter emit)
			{
				using (Local value = emit.DeclareLocal(this.nullableBaseType, "nullableValue"))
				{
					Local nullable = null;

					bool needStore = false;
					ReaderEmitterDirect dstack;

					if (emit is ReaderEmitterDeferred)
					{
						// No loadable value, create local.
						nullable = emit.DeclareLocal(this.member.Type, "nullable");
						dstack = emit.WithLocal(nullable);
						needStore = true;
					}
					else if (emit is ReaderEmitterDirect direct)
					{
						dstack = direct;
					}
					else
					{
						throw new NotSupportedException();
					}

					// Pre-steps.
					this.augs.Emit(new AugmentationEmitter(dstack), AugmentationPosition.BeforeBody);

					// If !context.GetBoolean()
					emit.LoadStream();
					emit.Stream.ReadBoolean(false);
					Label readValueLabel = emit.DefineLabel();
					Label endLabel = emit.DefineLabel();
					emit.BranchIfTrue(readValueLabel);
					{
						// Create "null" nullable.
						dstack.LoadValueAddress();
						emit.InitializeObject(this.member.Type);

						if (needStore)
						{
							using (emit.Store())
							{
								dstack.LoadValue();
							}
						}
					}
					// Else
					emit.Branch(endLabel);
					emit.MarkLabel(readValueLabel);
					{
						if (emit is ReaderEmitterDirect direct)
						{
							this.readerTemplate.Emit(direct.WithLocal(value), true);

							using (emit.Store())
							{
								emit.LoadLocal(value);
								emit.NewObject(this.member.Type.GetConstructor(new[] {this.nullableBaseType}));
							}
						}
						else
						{
							ReaderEmitterDeferred deferred = (ReaderEmitterDeferred)emit;
							Type stateType = typeof(NullableCallbackState<,>).MakeGenericType(this.nullableBaseType, deferred.StateType);
							using (Local state = emit.DeclareLocal(stateType, "state"))
							{
								// state.Callback = callback;
								emit.LoadLocalAddress(state);
								deferred.LoadCallback();
								emit.StoreField(stateType.GetField("Callback"));

								// state.State = state;
								emit.LoadLocalAddress(state);
								deferred.LoadState();
								emit.StoreField(stateType.GetField("State"));

								this.readerTemplate.Emit(deferred.WithDeferred(
									typeof(DeferredCallback<,>).MakeGenericType(this.nullableBaseType, stateType),
									() =>
									{
										emit.LoadField(stateType.GetField("Delegate", BindingFlags.Static | BindingFlags.Public));
									},
									() =>
									{
										emit.LoadLocal(state);
									}), true);
							}
						}
					}
					emit.MarkLabel(endLabel);

					// Post-steps.
					this.augs.Emit(new AugmentationEmitter(dstack), AugmentationPosition.AfterBody, AugmentationPosition.AfterBodyDeferred);

					nullable?.Dispose();
				}
			}

			private struct NullableCallbackState<T, TState> where T : struct
			{
#pragma warning disable 649
				public readonly DeferredCallback<T?, TState> Callback;
				public readonly TState State;
#pragma warning restore 649

				public static void WrapInNullable(T value, NullableCallbackState<T, TState> state)
				{
					state.Callback(value, state.State);
				}

				public static readonly DeferredCallback<T, NullableCallbackState<T, TState>> Delegate = WrapInNullable;
			}
		}

		#endregion
	}
}
