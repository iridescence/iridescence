﻿using System;
using System.Collections.Generic;

namespace Iridescence.Serialization.Compiler
{
	[Serializable]
	internal sealed class ISerializableMaskAttribute : Attribute, ISerializationMemberAttribute
	{
		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			yield break;
		}
	}
}