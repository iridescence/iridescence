using System;
using System.Runtime.Serialization;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents an entry in a <see cref="SerializationInfo"/>, without the value.
	/// </summary>
	public struct SerializationInfoMember : IEquatable<SerializationInfoMember>
	{
		#region Properties

		/// <summary>
		/// The type of the entry.
		/// </summary>
		public Type Type { get; internal set; }

		/// <summary>
		/// The name of the entry.
		/// </summary>
		public string Name { get; internal set; }

		#endregion

		#region Constructors

		public SerializationInfoMember(Type type, string name)
		{
			this.Type = type;
			this.Name = name;
		}

		#endregion

		#region Methods

		public bool Equals(SerializationInfoMember other)
		{
			return this.Type == other.Type && string.Equals(this.Name, other.Name);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is SerializationInfoMember && this.Equals((SerializationInfoMember)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((this.Type != null ? this.Type.GetHashCode() : 0) * 397) ^ (this.Name != null ? this.Name.GetHashCode() : 0);
			}
		}

		public static bool operator ==(SerializationInfoMember left, SerializationInfoMember right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(SerializationInfoMember left, SerializationInfoMember right)
		{
			return !left.Equals(right);
		}

		#endregion
	}
}