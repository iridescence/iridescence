﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Emits code to read/write types implementing <see cref="ISerializable"/>.
	/// </summary>
	public class ISerializableModule : IContentModule
	{
		#region Fields

		private static readonly MethodInfo writeMethod = typeof(ISerializableModule).GetMethod(nameof(write), BindingFlags.NonPublic | BindingFlags.Static);
		private static readonly MethodInfo readMethod = typeof(ISerializableModule).GetMethod(nameof(read), BindingFlags.NonPublic | BindingFlags.Static);
		private static readonly Type[] iSerializableCtorParams = {typeof(SerializationInfo), typeof(StreamingContext)};

		private readonly ISerializableParameters parameters;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the <see cref="StreamingContext"/> passed to <see cref="ISerializable"/> implementors and the <see cref="ISurrogateSelector"/>.
		/// </summary>
		public StreamingContext StreamingContext => this.parameters.StreamingContext;

		/// <summary>
		/// Gets the <see cref="IFormatterConverter"/> used when creating <see cref="SerializationInfo"/>.
		/// </summary>
		public IFormatterConverter FormatterConverter => this.parameters.FormatterConverter;

		/// <summary>
		/// Gets the <see cref="ISurrogateSelector"/>.
		/// </summary>
		public ISurrogateSelector SurrogateSelector => this.parameters.SurrogateSelector;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ISerializableModule"/> class. 
		/// </summary>
		public ISerializableModule(StreamingContext streamingContext, IFormatterConverter formatterConverter)
			: this(streamingContext, formatterConverter, null)
		{

		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ISerializableModule"/> class. 
		/// </summary>
		public ISerializableModule(StreamingContext streamingContext, IFormatterConverter formatterConverter, ISurrogateSelector surrogateSelector)
		{
			this.parameters = new ISerializableParameters(streamingContext, formatterConverter ?? throw new ArgumentNullException(nameof(formatterConverter)), surrogateSelector);
		}

		#endregion

		#region Methods

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody)
				return;

			ISerializationSurrogate surrogate = this.SurrogateSelector?.GetSurrogate(template.Specification.Type, this.StreamingContext, out _);
			if (surrogate != null || typeof(ISerializable).IsAssignableFrom(template.Specification.Type))
			{
				template.Body = new SerializableWriterBody(template.Specification, this.parameters, surrogate);
				template.MaskingInfo = new MaskingInfo(typeof(object), typeof(ISerializableMaskAttribute));
			}
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.Specification.Attributes.Any(a => a is ISerializableMaskAttribute))
			{
				template.Body = new SerializableReaderBody(template.Specification, this.parameters, null);
				return;
			}

			if (template.HasBody)
				return;

			ISerializationSurrogate surrogate = this.SurrogateSelector?.GetSurrogate(template.Specification.Type, this.StreamingContext, out _);
			if (surrogate != null || typeof(ISerializable).IsAssignableFrom(template.Specification.Type))
			{
				template.Body = new SerializableReaderBody(template.Specification, this.parameters, surrogate);
			}
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private static SerializationInfo fixNetCoreHashSetQuirk(SerializationInfo info, Type type, IFormatterConverter formatterConverter)
		{
			// Workaround for an issue in .NET Core: https://github.com/dotnet/corefx/issues/30891
			Type itemType = type.GetGenericArguments()[0];
			Type comparerType = typeof(IComparer<>).MakeGenericType(itemType);
			if (info.AsEnumerable().Any(e => e.Name == "Comparer" && e.ObjectType == comparerType))
			{
				Type eqComparerType = typeof(IEqualityComparer<>).MakeGenericType(itemType);
				SerializationInfo fixedInfo = new SerializationInfo(info.ObjectType, formatterConverter);
				foreach (SerializationEntry e in info)
				{
					if (e.Name == "Comparer")
					{
						fixedInfo.AddValue(e.Name, e.Value, eqComparerType);
					}
					else
					{
						fixedInfo.AddValue(e.Name, e.Value, e.ObjectType);
					}
				}

				info = fixedInfo;
			}

			return info;
		}

		
		/// <summary>
		/// Writes an <see cref="ISerializable"/> or <see cref="ISerializationSurrogate"/> to the specified context.
		/// </summary>
		private static void write<TOutput, TObj>(
			IWriterCompiler compiler, 
			SerializationMember member, 
			TOutput stream,
			ISerializableParameters parameters,
			ISerializationSurrogate surrogate,
			ref TObj obj)
		{
			Type type = obj.GetType();

			// Retreive serialization info.
			SerializationInfo info = new SerializationInfo(type, parameters.FormatterConverter);
			if (surrogate != null)
			{
				surrogate.GetObjectData(obj, info, parameters.StreamingContext);
			}
			else
			{
				ISerializable ser = (ISerializable)obj;
				ser.GetObjectData(info, parameters.StreamingContext);	
			}

			if (type.IsConstructedGenericType && type.GetGenericTypeDefinition() == typeof(HashSet<>))
			{
				info = fixNetCoreHashSetQuirk(info, type, parameters.FormatterConverter);
			}

			// Write layout of serialization info.
			if (stream is ISerializationInfoWriter infoWriter)
			{
				infoWriter.WriteSerializationInfoLayout(info.ObjectType, info.AsEnumerable().Select(entry => new SerializationInfoMember(entry.ObjectType, entry.Name)).ToList());
			}
			else if (stream is Stream stream2)
			{
				Action<Type> writeType;
				if (stream is IMetadataWriter mw)
					writeType = t => mw.WriteMember(t);
				else
					writeType = t => stream2.WriteStringV(t.AssemblyQualifiedName);

				// Generic fallback implementation.
				writeType(info.ObjectType);
				stream2.WriteInt32V(info.MemberCount);

				foreach (SerializationEntry entry in info)
				{
					writeType(entry.ObjectType);
				}

				foreach (SerializationEntry entry in info)
				{
					stream2.WriteStringV(entry.Name);
				}
			}
			else
			{
				throw new NotSupportedException($"Can not write a SerializationInfo to {typeof(TOutput).GetDisplayName()}");
			}

			// Write serialization info.
			foreach (SerializationEntry entry in info)
			{
				SerializationMember entryMember = member.Descend(entry.ObjectType, entry.Name);
				ObjectWriter<TOutput, object> del = (ObjectWriter<TOutput, object>)compiler.For(entryMember).Member.NonGeneric;
				del(stream, entry.Value);
			}
		}

		private static void read<TInput, TObj, TState>(
			IReaderCompiler builder, 
			SerializationMember member, 
			TInput stream,
			ISerializableParameters parameters,
			ISerializationSurrogate surrogate,
			DeferredCallback<TObj, TState> callback, 
			TState state)
		{
			Type objectType;
			SerializationInfoMember[] members;

			// Read layout of serialization info.
			if (stream is ISerializationInfoReader infoReader)
			{
				infoReader.ReadSerializationInfoLayout(out objectType, out members);
			}
			else if (stream is Stream stream2)
			{
				// Generic fallback implementation.
				Func<Type> readType;
				if (stream is IMetadataReader mr)
					readType = () => (Type)mr.ReadMember();
				else
					readType = () => Type.GetType(stream2.ReadStringV(), true);
				
				objectType = readType();
				int numMembers = stream2.ReadInt32V();
				members = new SerializationInfoMember[numMembers];

				for (int i = 0; i < numMembers; ++i)
				{
					members[i].Type = readType();
				}
				
				for (int i = 0; i < numMembers; ++i)
				{
					members[i].Name = stream2.ReadStringV();
				}
			}
			else
			{
				throw new NotSupportedException($"Can not read a SerializationInfo from {typeof(TInput).GetDisplayName()}");
			}

			// Create serialization info.
			SerializationInfo info = new SerializationInfo(objectType, parameters.FormatterConverter);

			bool submitted = false;
			void submit()
			{
				if (submitted)
					return;

				object obj = FormatterServices.GetUninitializedObject(objectType);

				if (surrogate == null)
				{
					// Find ISerializable constructor.
					ConstructorInfo ctor = objectType.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public, null, iSerializableCtorParams, null);
					if (ctor == null)
					{
						if (info.MemberCount == 0)
						{
							// It appears to be legal (in .NET) to call SerializationInfo.ChangeType during GetObjectData with a type that does not implement ISerializable.
							// We can't really implement that except for empty SerializationInfos (which is the only kind I encountered anyway).
							obj = Activator.CreateInstance(objectType);
						}
						else
						{
							throw new SerializationException($"ISerializable class \"{objectType}\" does not have an apropriate constructor.");
						}
					}
					else
					{
						// Call constructor.
						ctor.Invoke(obj, new object[] {info, parameters.StreamingContext});
					}
				}
				else
				{
					// Make use of surrogate.
					object newObj = surrogate.SetObjectData(obj, info, parameters.StreamingContext, parameters.SurrogateSelector);
					if (newObj != null)
						obj = newObj;
				}

				if (obj is IObjectReference reference)
				{
					// Resolve reference.
					obj = reference.GetRealObject(parameters.StreamingContext);
				}

				callback((TObj)obj, state);
				submitted = true;
			}

			int membersRemaining = members.Length;

			// Read members.
			foreach (SerializationInfoMember infoMember in members)
			{
				SerializationMember entryMember = member.Descend(infoMember.Type, infoMember.Name);
				ObjectReaderDeferred<TInput, object, SerializationInfoMember> reader = (ObjectReaderDeferred<TInput, object, SerializationInfoMember>)builder.For(entryMember).Deferred(typeof(SerializationInfoMember)).Member.NonGeneric;
				reader(stream, (value, m) =>
				{
					info.AddValue(m.Name, value, m.Type);
					--membersRemaining;
					if (membersRemaining == 0)
					{
						submit();
					}
				}, infoMember);
			}

			if (!objectType.IsValueType && !typeof(IObjectReference).IsAssignableFrom(objectType))
			{
				submit();
			}
		}

		private sealed class SerializableWriterBody : IWriterBody
		{
			private readonly SerializationMember memberSpec;
			private readonly ISerializableParameters parameters;
			private readonly ISerializationSurrogate surrogate;

			public int ByteSize => 0;

			public SerializableWriterBody(SerializationMember memberSpec, ISerializableParameters parameters, ISerializationSurrogate surrogate)
			{
				this.memberSpec = memberSpec;
				this.parameters = parameters;
				this.surrogate = surrogate;
			}

			public void Emit(WriterEmitter emit)
			{
				MethodInfo method = writeMethod.MakeGenericMethod(emit.Stream.Type, this.memberSpec.Type);
				emit.LoadCompiler();
				emit.LoadCaptured(this.memberSpec);
				emit.LoadStream();
				emit.LoadCaptured(this.parameters);
				emit.LoadCaptured(this.surrogate);
				emit.LoadValueAddress();
				emit.Call(method);
			}
		}

		private sealed class SerializableReaderBody : IReaderBody
		{
			private readonly SerializationMember memberSpec;
			private readonly ISerializableParameters parameters;
			private readonly ISerializationSurrogate surrogate;

			public int ByteSize { get; }

			public bool IsDeferred => true;

			public bool CustomAugmentationHandling => false;

			public SerializableReaderBody(SerializationMember memberSpec, ISerializableParameters parameters, ISerializationSurrogate surrogate)
			{
				this.memberSpec = memberSpec;
				this.parameters = parameters;
				this.surrogate = surrogate;
			}

			public void Emit(ReaderEmitter emit)
			{
				ReaderEmitterDeferred deferred = (ReaderEmitterDeferred)emit;
				MethodInfo method = readMethod.MakeGenericMethod(deferred.Stream.Type, this.memberSpec.Type, deferred.StateType);

				deferred.LoadCompiler();
				deferred.LoadCaptured(this.memberSpec);
				deferred.LoadStream();
				deferred.LoadCaptured(this.parameters);
				deferred.LoadCaptured(this.surrogate);
				deferred.LoadCallback();
				deferred.LoadState();
				deferred.Call(method);
			}
		}
		#endregion
	}
}
