using System.Runtime.Serialization;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Contains the relevant parameters for <see cref="ISerializable"/> and <see cref="ISerializationSurrogate"/> implementors.
	/// </summary>
	internal sealed class ISerializableParameters
	{
		public StreamingContext StreamingContext { get; }
		public IFormatterConverter FormatterConverter { get; }
		public ISurrogateSelector SurrogateSelector { get; }

		public ISerializableParameters(StreamingContext streamingContext, IFormatterConverter formatterConverter, ISurrogateSelector surrogateSelector)
		{
			this.StreamingContext = streamingContext;
			this.FormatterConverter = formatterConverter;
			this.SurrogateSelector = surrogateSelector;
		}
	}
}