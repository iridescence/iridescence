using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Interface that allows custom writing of <see cref="SerializationInfo"/> layouts in conjunction with <see cref="ISerializableModule"/>.
	/// </summary>
	public interface ISerializationInfoWriter
	{
		void WriteSerializationInfoLayout(Type objectType, IReadOnlyList<SerializationInfoMember> members);
	}
}