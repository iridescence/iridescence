using System;
using System.Runtime.Serialization;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Interface that allows custom reading of <see cref="SerializationInfo"/> layouts in conjunction with <see cref="ISerializableModule"/>.
	/// </summary>
	public interface ISerializationInfoReader
	{
		void ReadSerializationInfoLayout(out Type objectType, out SerializationInfoMember[] members);
	}
}