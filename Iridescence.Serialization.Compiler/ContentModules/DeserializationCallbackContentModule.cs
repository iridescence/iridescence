﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Interface that needs to be implemented on the <see cref="BufferedInputStream"/> to support <see cref="DeserializationCallbackContentModule"/>.
	/// </summary>
	public interface IDeserializationCallbackCollector
	{
		void Add(IDeserializationCallback obj);
	}

	/// <summary>
	/// Emits code to store implementors of <see cref="IDeserializationCallback"/> into a <see cref="IDeserializationCallbackCollector"/>.
	/// </summary>
	public class DeserializationCallbackContentModule : IContentModule
	{
		#region Fields

		private static readonly MethodInfo addMethod = typeof(IDeserializationCallbackCollector).GetMethod(nameof(IDeserializationCallbackCollector.Add));

		#endregion

		#region Methods

		public void ConfigureWriter(WriterTemplate template)
		{
			
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if(template.Specification.Type.IsValueType)
				return;

			bool implementsInterface = template.Specification.Type.GetInterfaces().Contains(typeof(IDeserializationCallback));
			if(template.Specification.Type.IsSealed && !implementsInterface)
				return;

			template.Augmentations.Add(new CallbackAugmentation());
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class CallbackAugmentation : IAugmentation
		{
			public AugmentationPosition Position => AugmentationPosition.AfterBodyDeferred;

			public void Emit(AugmentationEmitter emit)
			{
				if (!emit.HasValue)
					return;

				using (Local local = emit.DeclareLocal(typeof(IDeserializationCallback), "dc", false))
				{
					emit.LoadValue();
					emit.IsInstance(typeof(IDeserializationCallback));
					emit.StoreLocal(local);

					Label endLabel = emit.DefineLabel("dcend");

					emit.LoadLocal(local);
					emit.BranchIfFalse(endLabel);

					emit.LoadStream();
					emit.CastClass(typeof(IDeserializationCallbackCollector));
					emit.LoadLocal(local);
					emit.CallVirtual(addMethod);

					emit.MarkLabel(endLabel);
				}
			}
		}
		
		#endregion
	}
}
