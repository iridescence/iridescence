﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Base reader state class.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	internal class ReaderState<T>
	{
#pragma warning disable 649
		// ReSharper disable once MemberCanBeProtected.Local
		/// <summary>
		/// The parent object instance that declares the member.
		/// </summary>
		public T Instance;
#pragma warning restore 649

		// ReSharper disable once MemberCanBeProtected.Local
		public ReaderState()
		{
				
		}

		/// <summary>
		/// Called when a member was set.
		/// </summary>
		public virtual void OnMemberSet()
		{

		}

		/// <summary>
		/// Called when all fields have been read.
		/// This does not guarantee that all members have been set (i.e. that their callback have been invoked).
		/// </summary>
		public virtual void OnFieldsRead()
		{
				
		}
	}
}