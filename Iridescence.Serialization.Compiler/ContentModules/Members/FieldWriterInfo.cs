﻿using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements <see cref="MemberWriterInfo"/> for a <see cref="FieldInfo"/>.
	/// </summary>
	public class FieldWriterInfo : MemberWriterInfo
	{
		#region Properties

		/// <summary>
		/// Gets the field that is to be serialized.
		/// </summary>
		public FieldInfo Field { get; }

		#endregion

		#region Constructors

		public FieldWriterInfo(SerializationMember member, WriterTemplate template, FieldInfo field)
			: base(member, template)
		{
			this.Field = field;
		}

		#endregion

		#region Methods

		public override LoadFunc GetLoad(ILBuilder emit, LoadFunc loadInstance)
		{
			return LoadStore.LoadField(emit, this.Field, loadInstance);
		}

		#endregion
	}
}
