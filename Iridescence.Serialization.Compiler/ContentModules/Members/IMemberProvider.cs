﻿using System.Collections.Generic;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Provides the members to serialize for a given <see cref="SerializationMember"/>.
	/// </summary>
	public interface IMemberProvider
	{
		/// <summary>
		/// Returns the member reader infos for the specified parent member.
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="factory"></param>
		/// <returns></returns>
		IEnumerable<MemberReaderInfo> GetMemberReaders(SerializationMember parent, IReaderTemplateFactory factory);

		/// <summary>
		/// Returns the member writer infos for the specified parent member.
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="factory"></param>
		/// <returns></returns>
		IEnumerable<MemberWriterInfo> GetMemberWriters(SerializationMember parent, IWriterTemplateFactory factory);
	}
}
