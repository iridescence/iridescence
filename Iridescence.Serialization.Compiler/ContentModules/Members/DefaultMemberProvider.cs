﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements the default member provider.
	/// </summary>
	public class DefaultMemberProvider : IMemberProvider
	{
		#region Methods

		/// <summary>
		/// Returns a value that indicates whether the specified field should be serialized.
		/// </summary>
		/// <param name="field"></param>
		/// <returns></returns>
		protected virtual bool IsSerialized(FieldInfo field)
		{
			return (field.Attributes & FieldAttributes.NotSerialized) == 0;
		}

		/// <summary>
		/// Returns the fields eligible for serialization of the specified type.
		/// </summary>
		private IReadOnlyList<(FieldInfo, SerializationMember)> getFields(SerializationMember member)
		{
			const string separator = "\x1f";

			List<(FieldInfo, SerializationMember)> fields = new List<(FieldInfo, SerializationMember)>();

			string namePrefix = null;
			Type type = member.Type;

			for (;;)
			{
				BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic;

				if (namePrefix == null)
				{
					// If this is the actual type, include its public fields.
					bindingFlags |= BindingFlags.Public;
				}

				// Get fields.
				FieldInfo[] typeFields = type.GetFields(bindingFlags);

				foreach (FieldInfo field in typeFields)
				{
					if (!this.IsSerialized(field))
						continue;

					// Get field name.
					string name;
					if (namePrefix == null)
					{
						name = field.Name;
					}
					else
					{
						name = namePrefix + separator + field.Name;
					}

					fields.Add((field, member.Descend(field, name)));
				}

				type = type.BaseType;
				if (type == null || type == typeof(object))
					break;

				// Append name prefix.
				if (namePrefix == null)
				{
					namePrefix = type.Name;
				}
				else
				{
					namePrefix = type.Name + separator + namePrefix;
				}
			}

			return fields;
		}
		
		public virtual IEnumerable<MemberReaderInfo> GetMemberReaders(SerializationMember parent, IReaderTemplateFactory builder)
		{
			return this.getFields(parent)
				.Select(e => new FieldReaderInfo(e.Item2, builder.CreateTemplate(e.Item2, SerializationMode.Member), e.Item1));
		}

		public virtual IEnumerable<MemberWriterInfo> GetMemberWriters(SerializationMember parent, IWriterTemplateFactory builder)
		{
			return this.getFields(parent)
				.Select(e => new FieldWriterInfo(e.Item2, builder.CreateTemplate(e.Item2, SerializationMode.Member), e.Item1));
		}
		
		#endregion
	}
}
