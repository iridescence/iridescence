﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Reader state class that calls the original callback once all members have been read.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TState"></typeparam>
	internal sealed class ReaderStateWithCompletion<T, TState> : ReaderState<T>
	{
		private int membersRemaining;
		private bool fieldsRead;
		private readonly DeferredCallback<T, TState> callback;
		private readonly TState state;

		public ReaderStateWithCompletion(int numMembers, DeferredCallback<T, TState> callback, TState state)
		{
			this.membersRemaining = numMembers;
			this.callback = callback;
			this.state = state;
		}

		private void checkComplete()
		{
			if (!this.fieldsRead)
				return;

			if (this.membersRemaining > 0)
				return;

			// All members have been set. Call the deferred callback now.
			this.callback(this.Instance, this.state);
		}

		public override void OnMemberSet()
		{
			base.OnMemberSet();

			if (this.membersRemaining == 0)
				throw new InvalidOperationException("Read member although none were remaining.");
				
			--this.membersRemaining;
			this.checkComplete();
		}

		public override void OnFieldsRead()
		{
			base.OnFieldsRead();

			this.fieldsRead = true;
			this.checkComplete();
		}
	}
}