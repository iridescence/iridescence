﻿using System;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Associates a <see cref="ReaderTemplate"/> with a member of a class or structure and provides means to load/store the value of the member.
	/// </summary>
	public abstract class MemberReaderInfo
	{
		#region Fields

		/// <summary>
		/// Gets the <see cref="SerializationMember"/>. This provides the attributes.
		/// </summary>
		public SerializationMember Member { get; }

		/// <summary>
		/// Gets the <see cref="ReaderTemplate"/> for the member.
		/// </summary>
		public ReaderTemplate Template { get; }

		#endregion

		#region Properties

		#endregion

		#region Constructors

		protected MemberReaderInfo(SerializationMember member, ReaderTemplate template)
		{
			this.Member = member ?? throw new ArgumentNullException(nameof(member));
			this.Template = template ?? throw new ArgumentNullException(nameof(template));
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets the load and store function for the member.
		/// </summary>
		public abstract void GetLoadStore(ILBuilder emit, LoadFunc loadInstance, out LoadFunc load, out StoreFunc store);

		#endregion
	}
}
