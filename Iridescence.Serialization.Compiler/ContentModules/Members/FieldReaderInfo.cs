﻿using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements <see cref="MemberReaderInfo"/> for a <see cref="FieldInfo"/>.
	/// </summary>
	public class FieldReaderInfo : MemberReaderInfo
	{
		#region Properties

		/// <summary>
		/// Gets the field that is to be deserialized.
		/// </summary>
		public FieldInfo Field { get; }

		#endregion

		#region Constructors

		public FieldReaderInfo(SerializationMember member, ReaderTemplate template, FieldInfo field)
			: base(member, template)
		{
			this.Field = field;
		}

		#endregion

		#region Methods

		public override void GetLoadStore(ILBuilder emit, LoadFunc loadInstance, out LoadFunc load, out StoreFunc store)
		{
			load = LoadStore.LoadField(emit, this.Field, loadInstance);
			store = LoadStore.StoreField(emit, this.Field, loadInstance);
		}

		#endregion
	}
}
