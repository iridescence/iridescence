﻿using System;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Associates a <see cref="WriterTemplate"/> with a member and provides means to load the member onto the evaluation stack.
	/// </summary>
	public abstract class MemberWriterInfo
	{
		#region Fields

		/// <summary>
		/// Gets the <see cref="SerializationMember"/>. This provides the attributes.
		/// </summary>
		public SerializationMember Member { get; }

		/// <summary>
		/// Gets the <see cref="WriterTemplate"/> for the member.
		/// </summary>
		public WriterTemplate Template { get; }

		#endregion

		#region Properties

		#endregion

		#region Constructors

		protected MemberWriterInfo(SerializationMember member, WriterTemplate template)
		{
			this.Member = member ?? throw new ArgumentNullException(nameof(member));
			this.Template = template ?? throw new ArgumentNullException(nameof(template));
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets the load function for the member.
		/// </summary>
		public abstract LoadFunc GetLoad(ILBuilder emit, LoadFunc loadInstance);

		#endregion
	}
}
