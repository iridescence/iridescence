﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Emits code to write/read members of a class or struct.
	/// </summary>
	public class MemberContentModule : IContentModule
	{
		#region Fields

		private static readonly MethodInfo getUninitializedObjectMethod = typeof(FormatterServices).GetMethod(nameof(FormatterServices.GetUninitializedObject), BindingFlags.Public | BindingFlags.Static);

		private readonly IMemberProvider memberProvider;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MemberContentModule"/> class. 
		/// </summary>
		/// <param name="memberProvider">An optional <see cref="IMemberProvider"/> that determines which members get serialized.</param>
		public MemberContentModule(IMemberProvider memberProvider = null)
		{
			this.memberProvider = memberProvider ?? new DefaultMemberProvider();
		}
		
		#endregion

		#region Methods

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody)
				return;

			template.Body = new MemberWriterBody(template.Specification, this.memberProvider.GetMemberWriters(template.Specification, template.TemplateFactory));
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.HasBody)
				return;

			template.Body = new MemberReaderBody(template.Specification, this.memberProvider.GetMemberReaders(template.Specification, template.TemplateFactory), new AugmentationCollection(template));
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class MemberWriterBody : IWriterBody
		{
			private readonly SerializationMember parent;
			private readonly IEnumerable<MemberWriterInfo> members;

			public int ByteSize => this.members.Sum(w => w.Template.ByteSize);

			public MemberWriterBody(SerializationMember parent, IEnumerable<MemberWriterInfo> writers)
			{
				this.parent = parent;
				this.members = writers;
			}

			public void Emit(WriterEmitter emit)
			{
				foreach (MemberWriterInfo writer in this.members)
				{
					writer.Template.Emit(emit.WithLoad(writer.GetLoad(emit, emit.Load)), false);
				}
			}
		}

		private delegate void FieldSetter<in T, in TState>(T value, TState state);

		private static CompilableEmitter emitSetter(ICompilationUnit unit, Type declType, MemberReaderInfo info, Type stateType)
		{
			FieldInfo objField = stateType.FindField("Instance");
			Type delType = typeof(FieldSetter<,>).MakeGenericType(info.Member.Type, stateType);

			CompilableEmitter emit = unit.CreateMethod(MethodSignature.FromDelegate(delType), $"{declType?.GetDisplayName()}.{info.Member.Name}.SetDeferred", withCapturedValues: false);
			
			info.GetLoadStore(emit,
				LoadStore.LoadField(emit, objField,
					LoadStore.LoadArgument(emit, stateType, 1)),
				out _, out StoreFunc store);

			// Set field on state.Object.
			using (store())
			{
				emit.LoadArgument(0);
			}
			
			// Call OnMemberSet.
			emit.LoadArgument(1);
			emit.CallVirtual(stateType.GetMethod("OnMemberSet"));
			emit.Return();

			return emit;
		}

		private sealed class MemberReaderBody : IReaderBody
		{
			private readonly SerializationMember parent;
			private readonly IEnumerable<MemberReaderInfo> members;
			private readonly AugmentationCollection augs;
			private readonly int deferredMembers;
			private readonly bool storeWhenComplete;

			public int ByteSize => this.members.Sum(w => w.Template.ByteSize);

			public bool IsDeferred { get; }
			
			public bool CustomAugmentationHandling => true;

			public MemberReaderBody(SerializationMember parent, IEnumerable<MemberReaderInfo> readers, AugmentationCollection augs)
			{
				this.parent = parent;
				this.members = readers;
				this.augs = augs;
	
				this.deferredMembers = this.members.Count(reader => reader.Template.IsDeferred);
				this.IsDeferred = this.deferredMembers > 0;

				this.storeWhenComplete = parent.Type.IsValueType;
			}

			public void Emit(ReaderEmitter emit)
			{
				Type stateType = null;
				Local stateLocal = null;
				Local valueLocal = null;
				bool needStore;
				ReaderEmitterDirect dstack;

				if (emit is ReaderEmitterDeferred deferred)
				{
					needStore = true;

					if (this.deferredMembers > 0)
					{
						stateType = typeof(ReaderState<>).MakeGenericType(this.parent.Type);
						FieldInfo objField = stateType.FindField("Instance");

						stateLocal = deferred.DeclareLocal(stateType, "state", false);

						// Create state instance.
						if (this.storeWhenComplete)
						{
							Type stateType2 = typeof(ReaderStateWithCompletion<,>).MakeGenericType(this.parent.Type, deferred.StateType);
							deferred.LoadConstant(this.deferredMembers);
							deferred.LoadCallback();
							deferred.LoadState();
							deferred.NewObject(stateType2.GetConstructor(new[] {typeof(int), typeof(DeferredCallback<,>).MakeGenericType(this.parent.Type, deferred.StateType), deferred.StateType}));

							// The callback state class will call the call on completion, we don't need to store.
							needStore = false;
						}
						else
						{
							deferred.NewObject(stateType.GetConstructor(new Type[0]));
						}

						deferred.StoreLocal(stateLocal);

						if (!this.parent.Type.IsValueType)
						{
							// Set "Instance" field on state local to uninitialized object.
							deferred.LoadLocal(stateLocal);
							deferred.LoadType(this.parent.Type);
							deferred.Call(getUninitializedObjectMethod);
							deferred.CastClass(this.parent.Type);
							deferred.StoreField(objField);
						}

						// Make a direct stack for the Instance field on the state.
						dstack = deferred.WithLoadStore(
							mode =>
							{
								deferred.LoadLocal(stateLocal);
								if (mode == LoadMode.Address)
								{
									deferred.LoadFieldAddress(objField);
								}
								else
								{
									deferred.LoadField(objField);
								}
							},
							() =>
							{
								deferred.LoadLocal(stateLocal);
								return new StoreEnd(() => { deferred.StoreField(objField); });
							});
					}
					else
					{
						valueLocal = deferred.DeclareLocal(this.parent.Type, "value", false);
						if (this.parent.Type.IsValueType)
						{
							// default(T).
							deferred.LoadLocalAddress(valueLocal);
							deferred.InitializeObject(this.parent.Type);
						}
						else
						{
							// Get uninitialized object.
							deferred.LoadType(this.parent.Type);
							deferred.Call(getUninitializedObjectMethod);
							deferred.CastClass(this.parent.Type);
							deferred.StoreLocal(valueLocal);
						}

						dstack = deferred.WithLocal(valueLocal);
					}
				}
				else if (emit is ReaderEmitterDirect direct)
				{
					needStore = false;

					if (this.parent.Type.IsValueType)
					{
						// default(T).
						direct.LoadValueAddress();
						direct.InitializeObject(this.parent.Type);
					}
					else
					{
						// Get uninitialized object.
						direct.LoadValueAddress();
						direct.LoadType(this.parent.Type);
						direct.Call(getUninitializedObjectMethod);
						direct.CastClass(this.parent.Type);
						direct.StoreIndirect(this.parent.Type);
					}

					dstack = direct;
				}
				else
				{
					throw new NotSupportedException();
				}

				// Pre-steps.
				this.augs.Emit(new AugmentationEmitter(dstack), AugmentationPosition.BeforeBody);

				foreach (MemberReaderInfo info in this.members)
				{
					if (info.Template.IsDeferred)
					{
						if (!this.storeWhenComplete && needStore)
						{
							// As soon as we encounter a deferred member, we need to store the object.
							// This is necessary in order to prevent infinite loops or dead-locks for cyclic
							// references, for example this member we're about to read could have a reference
							// to the object we have created. The object won't be complete, but at least its
							// reference is already available.
							using (emit.Store())
							{
								dstack.LoadValue();
							}

							needStore = false;
						}

						CompilableEmitter fieldSetter = emitSetter(emit.Unit, this.parent.Type, info, stateType);
						Type callbackType = typeof(DeferredCallback<,>).MakeGenericType(info.Member.Type, stateType);
						
						info.Template.Emit(emit.WithDeferred(
							callbackType,
							loadDeferredCallback: () => { emit.LoadCapturedValue(new LazyCapturedValue(() => fieldSetter.GetCompiledMethod().CreateDelegate(callbackType, null), callbackType)); },
							loadDeferredState: () =>
							{
								emit.LoadLocal(stateLocal);
							}), false);
					}
					else
					{
						info.GetLoadStore(emit, dstack.Load, out LoadFunc load, out StoreFunc store);
						ReaderEmitterDirect dstackField = emit.WithLoadStore(load, store);
						info.Template.Emit(dstackField, false);
					}
				}

				if (stateLocal != null)
				{
					emit.LoadLocal(stateLocal);
					emit.CallVirtual(stateType.FindMethod("OnFieldsRead"));
				}

				// Post-steps.
				this.augs.Emit(new AugmentationEmitter(dstack), AugmentationPosition.AfterBody, AugmentationPosition.AfterBodyDeferred);

				if (needStore)
				{
					using (emit.Store())
					{
						dstack.LoadValue();
					}
				}

				stateLocal?.Dispose();
				valueLocal?.Dispose();
			}
		}
		
		#endregion
	}
}
