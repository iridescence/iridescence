﻿using System;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Emits code to write/read arrays.
	/// </summary>
	public class ArrayContentModule : IContentModule
	{
		#region Fields

		private static readonly MethodInfo arrayGetLongLengthMethodProperty = typeof(Array).GetProperty("LongLength")?.GetGetMethod() ?? throw new MissingMethodException();
		private static readonly MethodInfo arrayGetLongLengthMethod = typeof(Array).GetMethod("GetLongLength") ?? throw new MissingMethodException();
		private static readonly MethodInfo arrayGetLowerBoundMethod = typeof(Array).GetMethod("GetLowerBound") ?? throw new MissingMethodException();

		#endregion

		#region Methods

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody)
				return;

			if (!template.Specification.Type.IsArray)
				return;

			Type elementType = template.Specification.Type.GetElementType();

			if (template.Specification.Type.IsSzArray() && elementType.IsPrimitive && elementType != typeof(IntPtr) && elementType != typeof(UIntPtr))
			{
				template.Body = new PrimitiveArray(template.Specification, null);
			}
			else
			{
				template.Body = new ComplexArray(template.Specification, null, template.TemplateFactory);
			}
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.HasBody)
				return;

			if (!template.Specification.Type.IsArray)
				return;

			Type elementType = template.Specification.Type.GetElementType();

			if (template.Specification.Type.IsSzArray() && elementType.IsPrimitive && elementType != typeof(IntPtr) && elementType != typeof(UIntPtr))
			{
				template.Body = new PrimitiveArray(template.Specification, new AugmentationCollection(template));
			}
			else
			{
				template.Body = new ComplexArray(template.Specification, new AugmentationCollection(template), template.TemplateFactory);
			}
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class PrimitiveArray : IWriterBody, IReaderBody
		{
			private readonly SerializationMember member;
			private readonly AugmentationCollection augs;

			public int ByteSize => 0;

			public bool IsDeferred => false;

			public bool CustomAugmentationHandling => true;

			public PrimitiveArray(SerializationMember member, AugmentationCollection augs)
			{
				this.member = member;
				this.augs = augs;
			}

			public void Emit(WriterEmitter emit)
			{
				Type arrayType = this.member.Type;
				Type elementType = arrayType.GetElementType();

				// Write length.
				emit.LoadStream();
				emit.LoadValue();
				emit.CallVirtual(arrayGetLongLengthMethodProperty);
				emit.Stream.WriteVarInt();
	
				// Fast path for primitive arrays: Directly call the generic T[] method on the stream.
				emit.LoadStream();
				emit.LoadValue();
				emit.Stream.WriteArray(elementType);
			}

			public void Emit(ReaderEmitter emit)
			{
				Type arrayType = this.member.Type;
				Type elementType = arrayType.GetElementType();

				// Fast path for primitive arrays: Directly call the generic T[] method on the InputStream.
				using (Local lengthLocal = emit.DeclareLocal(typeof(long), "length", false))
				{
					// Read length.
					emit.LoadStream();
					emit.Stream.ReadVarInt();
					emit.StoreLocal(lengthLocal);

					if (emit is ReaderEmitterDirect direct)
					{
						using (direct.Store())
						{
							// Create array.
							emit.LoadLocal(lengthLocal);
							emit.ConvertOverflow<IntPtr>();
							emit.NewArray(elementType);
						}

						// Pre-steps.
						this.augs.Emit(new AugmentationEmitter(direct), AugmentationPosition.BeforeBody);

						// Read array.
						emit.LoadStream();
						direct.LoadValue();
						emit.Stream.ReadArray(elementType);

						// Post-steps.
						this.augs.Emit(new AugmentationEmitter(direct), AugmentationPosition.AfterBody, AugmentationPosition.AfterBodyDeferred);
					}
					else if (emit is ReaderEmitterDeferred deferred)
					{
						using (Local arrayLocal = emit.DeclareLocal(arrayType, "array", false))
						{
							// Create array and store in a local.
							emit.LoadLocal(lengthLocal);
							emit.ConvertOverflow<IntPtr>();
							emit.NewArray(elementType);
							emit.StoreLocal(arrayLocal);

							// Pre-steps.
							this.augs.Emit(new AugmentationEmitter(emit, LoadStore.LoadLocal(emit, arrayLocal)), AugmentationPosition.BeforeBody);

							// Read array.
							emit.LoadStream();
							emit.LoadLocal(arrayLocal);
							emit.Stream.ReadArray(elementType);

							// Post-steps.
							this.augs.Emit(new AugmentationEmitter(emit, LoadStore.LoadLocal(emit, arrayLocal)), AugmentationPosition.AfterBody, AugmentationPosition.AfterBodyDeferred);

							// Invoke callback.
							using (deferred.Store())
							{
								emit.LoadLocal(arrayLocal);
							}
						}
					}
					else
					{
						throw new NotSupportedException();
					}
				}
			}
		}

		private sealed class ComplexArray : IWriterBody, IReaderBody
		{
			private readonly SerializationMember member;
			private readonly AugmentationCollection augs;
			private readonly IWriterTemplateFactory writerTemplateFactory;
			private readonly IReaderTemplateFactory readerTemplateFactory;

			public int ByteSize => 0;

			public bool IsDeferred => false;

			public bool CustomAugmentationHandling => true;

			public ComplexArray(SerializationMember member, AugmentationCollection augs, IWriterTemplateFactory writerTemplateFactory)
			{
				this.member = member;
				this.augs = augs;
				this.writerTemplateFactory = writerTemplateFactory;
			}

			public ComplexArray(SerializationMember member, AugmentationCollection augs, IReaderTemplateFactory readerTemplateFactory)
			{
				this.member = member;
				this.augs = augs;
				this.readerTemplateFactory = readerTemplateFactory;
			}

			public void Emit(WriterEmitter emit)
			{
				Type arrayType = this.member.Type;
				Type elementType = arrayType.GetElementType();
				bool sz = arrayType.IsSzArray();
				int rank = arrayType.GetArrayRank();
				MethodInfo getMethod = arrayType.GetMethod("Get");
				MethodInfo addressMethod = arrayType.GetMethod("Address");

				// Get lengths and lower bounds, and store them in the stream.
				Local[] lowerBoundLocals = new Local[rank];
				Local[] lengthLocals = new Local[rank];
				for (int i = 0; i < rank; ++i)
				{
					if (!sz)
					{
						lowerBoundLocals[i] = emit.DeclareLocal(typeof(long), $"lowerBound{i}", false);

						// Set lowerBound[i] by calling GetLowerBound on the array.
						emit.LoadValue();
						emit.LoadConstant(i);
						emit.CallVirtual(arrayGetLowerBoundMethod);
						emit.Convert<long>();
						emit.StoreLocal(lowerBoundLocals[i]);

						// Write lowerbound to stream.
						emit.LoadStream();
						emit.LoadLocal(lowerBoundLocals[i]);
						emit.Stream.WriteVarInt();
					}

					// Set length[i] by calling GetLongLength on the array.
					lengthLocals[i] = emit.DeclareLocal(typeof(long), $"length{i}", false);
					emit.LoadValue();
					emit.LoadConstant(i);
					emit.CallVirtual(arrayGetLongLengthMethod);
					emit.StoreLocal(lengthLocals[i]);

					// Write length to stream.
					emit.LoadStream();
					emit.LoadLocal(lengthLocals[i]);
					emit.Stream.WriteVarInt();
				}

				// Loop starts.
				Local[] indexLocals = new Local[rank];
				Label[] loopBeginLabels = new Label[rank];
				Label[] loopEndLabels = new Label[rank];
				for (int i = 0; i < rank; ++i)
				{
					loopBeginLabels[i] = emit.DefineLabel($"loopBegin{i}");
					loopEndLabels[i] = emit.DefineLabel($"loopEnd{i}");
					indexLocals[i] = emit.DeclareLocal(typeof(long), $"index{i}", false);

					emit.LoadConstant(0L);
					emit.StoreLocal(indexLocals[i]);
					emit.Branch(loopEndLabels[i]);
					emit.MarkLabel(loopBeginLabels[i]);
				}

				// Load method for SZ arrays.
				void loadSZElement(LoadMode mode)
				{
					emit.LoadValue();

					emit.LoadLocal(indexLocals[0]);
					emit.ConvertOverflow<IntPtr>();

					if (mode == LoadMode.Address)
					{
						emit.LoadElementAddress(elementType);
					}
					else
					{
						emit.LoadElement(elementType);
					}
				}

				// Load method for multi-dimensional arrays.
				void loadMDElement(LoadMode mode)
				{
					emit.LoadValue();

					for (int i = 0; i < rank; ++i)
					{
						emit.LoadLocal(lowerBoundLocals[i]);
						emit.LoadLocal(indexLocals[i]);
						emit.Add();
						emit.ConvertOverflow<int>();
					}

					if (mode == LoadMode.Address)
					{
						emit.Call(addressMethod);
					}
					else
					{
						emit.Call(getMethod);
					}
				}

				// Emit member writer.
				SerializationMember elementMember = this.member.Descend(elementType, "Element");
				WriterTemplate elementTemplate = this.writerTemplateFactory.CreateTemplate(elementMember, SerializationMode.Member);
				elementTemplate.Emit(emit.WithLoad(sz ? new LoadFunc(loadSZElement) : new LoadFunc(loadMDElement)), true);

				// Loop ends.
				for (int i = rank - 1; i >= 0; --i)
				{
					// index[i] += 1
					emit.LoadLocal(indexLocals[i]);
					emit.LoadConstant(1L);
					emit.Add();
					emit.StoreLocal(indexLocals[i]);

					// If index[i] < length[i] goto loopStart[i]
					emit.MarkLabel(loopEndLabels[i]);
					emit.LoadLocal(indexLocals[i]);
					emit.LoadLocal(lengthLocals[i]);
					emit.BranchIfLess(loopBeginLabels[i]);
				}

				// Clean up.
				for (int i = 0; i < rank; ++i)
				{
					indexLocals[i].Dispose();
					lengthLocals[i].Dispose();
					lowerBoundLocals[i]?.Dispose();
				}
			}

			public void Emit(ReaderEmitter emit)
			{
				Type arrayType = this.member.Type;
				Type elementType = arrayType.GetElementType();
				bool sz = arrayType.IsSzArray();
				int rank = arrayType.GetArrayRank();
				MethodInfo getMethod = arrayType.GetMethod("Get");
				MethodInfo setMethod = arrayType.GetMethod("Set");
				MethodInfo addressMethod = arrayType.GetMethod("Address");

				// Create locals/labels, get lengths and store them.
				Local[] lowerBoundLocals = new Local[rank];
				Local[] lengthLocals = new Local[rank];
				for (int i = 0; i < rank; ++i)
				{
					if(!sz)
					{
						// Read lower bound.
						lowerBoundLocals[i] = emit.DeclareLocal(typeof(long), $"lowerBound{i}", false);
						emit.LoadStream();
						emit.Stream.ReadVarInt();
						emit.StoreLocal(lowerBoundLocals[i]);
					}

					// Read length.
					lengthLocals[i] = emit.DeclareLocal(typeof(long), $"length{i}", false);
					emit.LoadStream();
					emit.Stream.ReadVarInt();
					emit.StoreLocal(lengthLocals[i]);
				}

				using (Local arrayLocal = emit.DeclareLocal(arrayType, "array", false))
				{
					// Create array.
					if (sz)
					{
						// Use SZ creation opcode.
						emit.LoadLocal(lengthLocals[0]);
						emit.ConvertOverflow<IntPtr>();
						emit.NewArray(elementType);
					}
					else
					{
						// Invoke MD constructor with lower bounds and lengths.
						for (int i = 0; i < rank; ++i)
						{
							emit.LoadLocal(lowerBoundLocals[i]);
							emit.ConvertOverflow<int>();

							emit.LoadLocal(lengthLocals[i]);
							emit.ConvertOverflow<int>();
						}
						emit.NewObject(arrayType.GetConstructor(Enumerable.Repeat(typeof(int), 2 * rank).ToArray()));
					}
					emit.StoreLocal(arrayLocal);

					// Pre-steps.
					this.augs.Emit(new AugmentationEmitter(emit, LoadStore.LoadLocal(emit, arrayLocal)), AugmentationPosition.BeforeBody);

					// Loop starts.
					Local[] indexLocals = new Local[rank];
					Label[] loopBeginLabels = new Label[rank];
					Label[] loopEndLabels = new Label[rank];
					for (int i = 0; i < rank; ++i)
					{
						loopBeginLabels[i] = emit.DefineLabel($"loopBegin{i}");
						loopEndLabels[i] = emit.DefineLabel($"loopEnd{i}");
						indexLocals[i] = emit.DeclareLocal(typeof(long), $"index{i}", false);

						emit.LoadConstant(0L);
						emit.StoreLocal(indexLocals[i]);
						emit.Branch(loopEndLabels[i]);
						emit.MarkLabel(loopBeginLabels[i]);
					}

					// Emit member reader.
					SerializationMember elementMember = this.member.Descend(elementType, "Element");
					ReaderTemplate elementTemplate = this.readerTemplateFactory.CreateTemplate(elementMember, SerializationMode.Member);

					if (elementTemplate.IsDeferred)
					{
						if (sz)
						{
							Type stateType = typeof(SzCallbackState<>).MakeGenericType(elementType);
							using (Local stateLocal = emit.DeclareLocal(stateType, "state", false))
							{
								// state.Array = array
								emit.LoadLocalAddress(stateLocal);
								emit.LoadLocal(arrayLocal);
								emit.StoreField(stateType.GetField("Array"));

								// state.Index = indices[0]
								emit.LoadLocalAddress(stateLocal);
								emit.LoadLocal(indexLocals[0]);
								emit.StoreField(stateType.GetField("Index"));

								elementTemplate.Emit(emit.WithDeferred(
									typeof(DeferredCallback<,>).MakeGenericType(elementType, stateType),
									() => { emit.LoadField(stateType.GetField("Delegate", BindingFlags.Public | BindingFlags.Static)); },
									() => { emit.LoadLocal(stateLocal); }), true);
							}
						}
						else
						{
							Type stateType = typeof(MdCallbackState<>).MakeGenericType(elementType);
							using (Local stateLocal = emit.DeclareLocal(stateType, "state", false))
							{
								FieldInfo indicesField = stateType.GetField("Indices");

								// state.Array = array
								emit.LoadLocalAddress(stateLocal);
								emit.LoadLocal(arrayLocal);
								emit.StoreField(stateType.GetField("Array"));

								// state.Indices = new long[rank]
								emit.LoadLocalAddress(stateLocal);
								emit.LoadConstant(rank);
								emit.NewArray<long>();
								emit.StoreField(indicesField);

								// Store current indices in state.Indices.
								for (int i = 0; i < rank; ++i)
								{
									emit.LoadLocalAddress(stateLocal);
									emit.LoadField(indicesField);
									emit.LoadConstant(i);
									emit.LoadLocal(lowerBoundLocals[i]);
									emit.LoadLocal(indexLocals[i]);
									emit.Add();
									emit.StoreElement<long>();
								}

								elementTemplate.Emit(emit.WithDeferred(
									typeof(DeferredCallback<,>).MakeGenericType(elementType, stateType),
									() => { emit.LoadField(stateType.GetField("Delegate", BindingFlags.Public | BindingFlags.Static)); },
									() => { emit.LoadLocal(stateLocal); }), true);
							}
						}
					}
					else
					{
						if (sz)
						{
							// Load method for SZ arrays.
							void loadElement(LoadMode mode)
							{
								emit.LoadLocal(arrayLocal);
								emit.LoadLocal(indexLocals[0]);
								emit.ConvertOverflow<IntPtr>();

								if (mode == LoadMode.Address)
								{
									emit.LoadElementAddress(elementType);
								}
								else
								{
									emit.LoadElement(elementType);
								}
							}

							// Store method for SZ arrays.
							StoreEnd storeElement()
							{
								emit.LoadLocal(arrayLocal);
								emit.LoadLocal(indexLocals[0]);
								emit.ConvertOverflow<IntPtr>();

								return new StoreEnd(() => { emit.StoreElement(elementType); });
							}

							elementTemplate.Emit(emit.WithLoadStore(loadElement, storeElement), true);
						}
						else
						{
							// Load method for multi-dimensional arrays.
							void loadElement(LoadMode mode)
							{
								emit.LoadLocal(arrayLocal);

								// Load indices with lower bound added to them.
								for (int i = 0; i < rank; ++i)
								{
									emit.LoadLocal(lowerBoundLocals[i]);
									emit.LoadLocal(indexLocals[i]);
									emit.Add();
									emit.ConvertOverflow<int>();
								}

								if (mode == LoadMode.Address)
								{
									emit.Call(addressMethod);
								}
								else
								{
									emit.Call(getMethod);
								}
							}

							// Store method for MD arrays.
							StoreEnd storeElement()
							{
								emit.LoadLocal(arrayLocal);

								// Load indices with lower bound added to them.
								for (int i = 0; i < rank; ++i)
								{
									emit.LoadLocal(lowerBoundLocals[i]);
									emit.LoadLocal(indexLocals[i]);
									emit.Add();
									emit.ConvertOverflow<int>();
								}

								return new StoreEnd(() => { emit.Call(setMethod); });
							}

							elementTemplate.Emit(emit.WithLoadStore(loadElement, storeElement), true);
						}
					}

					// Loop ends.
					for (int i = rank - 1; i >= 0; --i)
					{
						// index[i] += 1
						emit.LoadLocal(indexLocals[i]);
						emit.LoadConstant(1L);
						emit.Add();
						emit.StoreLocal(indexLocals[i]);

						// If index[i] < length[i] goto loopStart[i]
						emit.MarkLabel(loopEndLabels[i]);
						emit.LoadLocal(indexLocals[i]);
						emit.LoadLocal(lengthLocals[i]);
						emit.BranchIfLess(loopBeginLabels[i]);
					}

					// Store array. Note that not all elements might be read at this point since there can be uninvoked callbacks.
					// This is not a problem since an array is a reference type and those missing elements can be set at a later point.
					using (emit.Store())
					{
						emit.LoadLocal(arrayLocal);
					}

					// Post-steps.
					this.augs.Emit(new AugmentationEmitter(emit, LoadStore.LoadLocal(emit, arrayLocal)), AugmentationPosition.AfterBody, AugmentationPosition.AfterBodyDeferred);

					// Clean up.
					for (int i = 0; i < rank; ++i)
					{
						indexLocals[i].Dispose();
						lengthLocals[i].Dispose();
						lowerBoundLocals[i]?.Dispose();
					}
				}
			}			
			
#pragma warning disable 649

			private struct SzCallbackState<TElement>
			{
				public TElement[] Array;
				public long Index;

				public static void Set(TElement value, SzCallbackState<TElement> state)
				{
					state.Array[state.Index] = value;
				}

				public static readonly DeferredCallback<TElement, SzCallbackState<TElement>> Delegate = Set;
			}
			
			private struct MdCallbackState<TElement>
			{
				public Array Array;
				public long[] Indices;

				public static void Set(TElement value, MdCallbackState<TElement> state)
				{
					state.Array.SetValue(value, state.Indices);
				}

				public static readonly DeferredCallback<TElement, MdCallbackState<TElement>> Delegate = Set;
			}

#pragma warning restore 649

		}
		
		#endregion
	}
}
