﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Interface for object content surrogates. Used with <see cref="SurrogateContentModule"/>.
	/// An implementer needs to also declare a constructor that takes a single argument of type <typeparamref name="T"/>.
	/// </summary>
	public interface IObjectSurrogate<out T>
	{
		/// <summary>
		/// Returns the real object represented through this proxy.
		/// </summary>
		/// <returns></returns>
		T GetRealObject();
	}
}