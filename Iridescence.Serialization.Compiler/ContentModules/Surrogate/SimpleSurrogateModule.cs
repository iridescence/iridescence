﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents a simple surrogate module that simply exchanges one type for another.
	/// </summary>
	public class SimpleSurrogateModule : SurrogateContentModule
	{
		private readonly Type real;
		private readonly Type surrogate;

		public SimpleSurrogateModule(Type real, Type surrogate)
		{
			this.real = real;
			this.surrogate = surrogate;
		}

		protected override SerializationMember GetSurrogate(SerializationMember member)
		{
			if (member.Type == this.real)
				return member.ChangeType(this.surrogate);

			return null;
		}
	}
}