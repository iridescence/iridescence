﻿using System;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements a content module that replaces an object with a different object at runtime.
	/// </summary>
	public abstract class SurrogateContentModule : IContentModule
	{
		#region Properties

		protected virtual SerializationMode Mode => SerializationMode.Member;

		#endregion

		#region Methods

		/// <summary>
		/// Returns a <see cref="SerializationMember"/> that is used as proxy for the specified member.
		/// The type of the returned proxy member must implement <see cref="IObjectSurrogate{T}"/>, where 'T' is the real object type, (i.e. <paramref name="member"/>.Type).
		/// </summary>
		/// <param name="member"></param>
		/// <returns></returns>
		protected abstract SerializationMember GetSurrogate(SerializationMember member);

		private static Type getInterfaceType(Type realType)
		{
			return typeof(IObjectSurrogate<>).MakeGenericType(realType);
		}

		private static ConstructorInfo getConstructor(Type declType, Type argType, out bool isRef)
		{
			ConstructorInfo ctor = declType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, new[] {argType.MakeByRefType()}, null);
			if (ctor != null)
			{
				isRef = true;
				return ctor;
			}

			isRef = false;
			ctor = declType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, new[] {argType}, null);
			return ctor;
		}

		private static ConstructorInfo getProxyConstructor(Type realType, Type proxyType, out bool isRef)
		{
			Type argType = realType;

			for (;;)
			{
				ConstructorInfo ctor = getConstructor(proxyType, argType, out isRef);
				if (ctor != null)
					return ctor;

				if (argType == typeof(object) || argType.IsValueType)
					break;

				argType = argType.BaseType;
			}

			return null;
		}

		private static MethodInfo getRealObjectMethod(Type realType)
		{
			return getInterfaceType(realType).GetMethod("GetRealObject");
		}

		private static MethodInfo getRealObjectMethod(Type realType, Type proxyType)
		{
			MethodInfo method = getRealObjectMethod(realType);
			Type iface = getInterfaceType(realType);
			InterfaceMapping map = proxyType.GetInterfaceMap(iface);
			for (int i = 0; i < map.InterfaceMethods.Length; ++i)
			{
				if (map.InterfaceMethods[i] == method)
					return map.TargetMethods[i];
			}

			throw new InvalidOperationException();
		}

		private static bool validateProxy(SerializationMember member, SerializationMember proxy)
		{
			Type interfaceType = getInterfaceType(member.Type);

			// Check if it implements IContentProxy<RealType>.
			if (!proxy.Type.GetInterfaces().Contains(interfaceType))
				return false;

			// Check if it has an appropriate constructor.
			ConstructorInfo ctor = getProxyConstructor(member.Type, proxy.Type, out _);
			if(ctor == null)
				return false;
			
			return true;
		}

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody)
				return;

			SerializationMember proxyMember = this.GetSurrogate(template.Specification);
			if(proxyMember == null)
				return;

			if (!validateProxy(template.Specification, proxyMember))
				throw new InvalidOperationException("Invalid proxy.");

			WriterTemplate proxyWriter = template.TemplateFactory.CreateTemplate(proxyMember, this.Mode);
			template.Body = new WriterBody(template.Specification, proxyMember, proxyWriter);
		}

		public virtual void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.HasBody)
				return;

			SerializationMember proxyMember = this.GetSurrogate(template.Specification);
			if(proxyMember == null)
				return;

			if (!validateProxy(template.Specification, proxyMember))
				throw new InvalidOperationException("Invalid proxy.");

			ReaderTemplate proxyReader = template.TemplateFactory.CreateTemplate(proxyMember, this.Mode);
			template.Body = new ReaderBody(template.Specification, proxyMember, proxyReader);
		}

		public virtual void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class WriterBody : IWriterBody
		{
			private readonly SerializationMember member;
			private readonly SerializationMember proxyMember;
			private readonly WriterTemplate proxyWriter;

			public int ByteSize => this.proxyWriter.ByteSize;

			public WriterBody(SerializationMember member, SerializationMember proxyMember, WriterTemplate proxyWriter)
			{
				this.member = member;
				this.proxyMember = proxyMember;
				this.proxyWriter = proxyWriter;
			}

			public void Emit(WriterEmitter emit)
			{
				using (Local proxyLocal = emit.DeclareLocal(this.proxyMember.Type, "proxy"))
				{
					ConstructorInfo ctor = getProxyConstructor(this.member.Type, this.proxyMember.Type, out bool isRef);
					if (isRef)
						emit.LoadValueAddress();
					else
						emit.LoadValue();

					emit.NewObject(ctor);
					emit.StoreLocal(proxyLocal);

					this.proxyWriter.Emit(emit.WithLoad(LoadStore.LoadLocal(emit, proxyLocal)), false);
				}
			}
		}

		private sealed class ReaderBody : IReaderBody
		{
			private readonly SerializationMember member;
			private readonly SerializationMember proxyMember;
			private readonly ReaderTemplate proxyReader;

			public int ByteSize => this.proxyReader.ByteSize;

			public bool IsDeferred => this.proxyReader.IsDeferred;

			public bool CustomAugmentationHandling => false;

			public ReaderBody(SerializationMember member, SerializationMember proxyMember, ReaderTemplate proxyReader)
			{
				this.member = member;
				this.proxyMember = proxyMember;
				this.proxyReader = proxyReader;
			}

			public void Emit(ReaderEmitter emit)
			{
				if (this.proxyReader.IsDeferred)
				{
					ReaderEmitterDeferred deferred = (ReaderEmitterDeferred)emit;

					Type realType = this.member.Type;
					Type proxyType = this.proxyMember.Type;
					Type stateType = typeof(SurrogateCallbackState<,,>).MakeGenericType(realType, proxyType, deferred.StateType);
					Type callbackType = typeof(DeferredCallback<,>).MakeGenericType(proxyType, stateType);

					using (Local stateLocal = emit.DeclareLocal(stateType, "state"))
					{
						deferred.LoadLocalAddress(stateLocal);
						deferred.LoadState();
						deferred.StoreField(stateType.FindField("State"));

						deferred.LoadLocalAddress(stateLocal);
						deferred.LoadCallback();
						deferred.StoreField(stateType.FindField("Callback"));

						ReaderEmitterDeferred proxyEmit = emit.WithDeferred(
							callbackType,
							() => { deferred.LoadField(stateType.FindField("Delegate")); },
							() => { deferred.LoadLocal(stateLocal); });

						this.proxyReader.Emit(proxyEmit, false);
					}
				}
				else
				{
					using (Local proxyLocal = emit.DeclareLocal(this.proxyMember.Type, "proxy"))
					{
						this.proxyReader.Emit(emit.WithLocal(proxyLocal), false);

						using (emit.Store())
						{
							if(this.proxyMember.Type.IsValueType)
								emit.LoadLocalAddress(proxyLocal);
							else
								emit.LoadLocal(proxyLocal);
							emit.CallVirtual(getRealObjectMethod(this.member.Type, this.proxyMember.Type));
						}
					}
				}
			}

			private readonly struct SurrogateCallbackState<TReal, TProxy, TState>
				where TProxy : IObjectSurrogate<TReal>
			{
#pragma warning disable 649
				public readonly DeferredCallback<TReal, TState> Callback;
				public readonly TState State;
#pragma warning restore 649

				public static readonly DeferredCallback<TProxy, SurrogateCallbackState<TReal, TProxy, TState>> Delegate = Invoke;

				public static void Invoke(TProxy proxy, SurrogateCallbackState<TReal, TProxy, TState> state)
				{
					TReal realObj = proxy.GetRealObject();
					state.Callback(realObj, state.State);
				}
			}
		}

		#endregion
	}
}
