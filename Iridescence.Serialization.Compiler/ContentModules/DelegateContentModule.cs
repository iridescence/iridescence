﻿using System;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Efficiently stores multicast delegates.
	/// </summary>
	public class DelegateContentModule : IContentModule
	{
		#region Fields

		private static readonly MethodInfo getInvocationListMethod = typeof(Delegate).GetMethod(nameof(Delegate.GetInvocationList)) ?? throw new MissingMethodException();
		private static readonly MethodInfo getMethodMethod = typeof(Delegate).GetProperty(nameof(Delegate.Method))?.GetGetMethod() ?? throw new MissingMethodException();
		private static readonly MethodInfo getTargetMethod = typeof(Delegate).GetProperty(nameof(Delegate.Target))?.GetGetMethod() ?? throw new MissingMethodException();

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DelegateContentModule"/> class. 
		/// </summary>
		public DelegateContentModule()
		{

		}

		#endregion

		#region Methods

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody || !typeof(Delegate).IsAssignableFrom(template.Specification.Type))
				return;

			SerializationMember invocationMember = template.Specification.Descend(typeof(Delegate), "Invocation");
			SerializationMember methodMember = invocationMember.Descend(typeof(MethodInfo), "Method");
			SerializationMember targetMember = invocationMember.Descend(typeof(object), "Target");

			WriterTemplate methodWriter = template.TemplateFactory.CreateTemplate(methodMember, SerializationMode.Member);
			WriterTemplate targetWriter = template.TemplateFactory.CreateTemplate(targetMember, SerializationMode.Member);

			template.Body = new DelegateWriterBody(methodWriter, targetWriter);
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.HasBody || !typeof(Delegate).IsAssignableFrom(template.Specification.Type))
				return;

			SerializationMember invocationMember = template.Specification.Descend(typeof(Delegate), "Invocation");
			SerializationMember methodMember = invocationMember.Descend(typeof(MethodInfo), "Method");
			SerializationMember targetMember = invocationMember.Descend(typeof(object), "Target");
			
			ReaderTemplate methodWriter = template.TemplateFactory.CreateTemplate(methodMember, SerializationMode.Member, true);
			ReaderTemplate targetWriter = template.TemplateFactory.CreateTemplate(targetMember, SerializationMode.Member, true);

			template.Body = new DelegateReaderBody(template.Specification.Type, methodWriter, targetWriter);
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class DelegateWriterBody : IWriterBody
		{
			private readonly WriterTemplate methodWriter;
			private readonly WriterTemplate targetWriter;

			public int ByteSize => this.methodWriter.ByteSize + this.targetWriter.ByteSize;

			public DelegateWriterBody(WriterTemplate methodWriter, WriterTemplate targetWriter)
			{
				this.methodWriter = methodWriter;
				this.targetWriter = targetWriter;
			}

			public void Emit(WriterEmitter emit)
			{
				using (Local invocationList = emit.DeclareLocal(typeof(Delegate[]), "invocationList", false))
				{
					// invocationList = delegate.GetInvocationList()
					emit.LoadValue();
					emit.CallVirtual(getInvocationListMethod);
					emit.StoreLocal(invocationList);

					using (Local numInvocations = emit.DeclareLocal(typeof(int), "numInvocations", false))
					using (Local index = emit.DeclareLocal(typeof(int), "index"))
					{
						Label loopStart = emit.DefineLabel("loopStart");
						Label loopEnd = emit.DefineLabel("loopEnd");

						// numInvocations = invocationList.Length
						emit.LoadLocal(invocationList);
						emit.LoadLength();
						emit.StoreLocal(numInvocations);

						// stream.WriteVarInt(numInvocations)
						emit.LoadStream();
						emit.LoadLocal(numInvocations);
						emit.ConvertToInt64();
						emit.Stream.WriteVarInt();

						// for i = 0 .. numInvocations
						emit.Branch(loopEnd);
						emit.MarkLabel(loopStart);

						using (Local method = emit.DeclareLocal(typeof(MethodInfo), "delegateMethod", false))
						{
							emit.LoadLocal(invocationList);
							emit.LoadLocal(index);
							emit.LoadElement(typeof(Delegate));
							emit.CallVirtual(getMethodMethod);
							emit.StoreLocal(method);
							this.methodWriter.Emit(emit.WithLoad(LoadStore.LoadLocal(emit, method)), false);
						}

						using (Local target = emit.DeclareLocal(typeof(object), "delegateTarget", false))
						{
							emit.LoadLocal(invocationList);
							emit.LoadLocal(index);
							emit.LoadElement(typeof(Delegate));
							emit.CallVirtual(getTargetMethod);
							emit.StoreLocal(target);
							this.targetWriter.Emit(emit.WithLoad(LoadStore.LoadLocal(emit, target)), false);
						}

						// i += 1
						emit.LoadLocal(index);
						emit.LoadConstant(1);
						emit.Add();
						emit.StoreLocal(index);

						// If i < numInvocations goto loopStart
						emit.MarkLabel(loopEnd);
						emit.LoadLocal(index);
						emit.LoadLocal(numInvocations);
						emit.BranchIfLess(loopStart);
					}
				}
			}
		}

		/// <summary>
		/// Represents one slot (invocation) in a multicast delegate.
		/// </summary>
		private struct DelegateSlot
		{
			public MethodInfo Method;
			public object Target;
			public bool HasTarget;
			public bool IsComplete => this.Method != null && this.HasTarget;
		}

		/// <summary>
		/// Helper class to build multicast delegates.
		/// Invokes the deferred callback once the delegate is built.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TState"></typeparam>
		private sealed class DelegateBuilder<T, TState> 
			where T : class /* Delegate */
		{
			private readonly DeferredCallback<T, TState> callback;
			private readonly TState state;
			private readonly DelegateSlot[] slots;
			private int incompleteSlots;

			public DelegateBuilder(int numInvocations, DeferredCallback<T, TState> callback, TState state)
			{
				if (numInvocations <= 0)
					throw new ArgumentOutOfRangeException(nameof(numInvocations));

				this.slots = new DelegateSlot[numInvocations];
				this.callback = callback;
				this.state = state;
				this.incompleteSlots = numInvocations;
			}

			/// <summary>
			/// Check whether the delegate is complete. Invoke the callback if it is.
			/// </summary>
			/// <param name="index"></param>
			private void checkComplete(int index)
			{
				if (this.slots[index].IsComplete)
					--this.incompleteSlots;

				if (this.incompleteSlots == 0)
				{
					T del;
					if (this.slots.Length == 1)
					{
						del = DelegateFactory.GetFactory<T>(this.slots[0].Method)(this.slots[0].Target);
					}
					else
					{
						Delegate[] delegates = new Delegate[this.slots.Length];

						for (int i = 0; i < delegates.Length; ++i)
						{
							delegates[i] = DelegateFactory.GetFactory(typeof(T), this.slots[i].Method)(this.slots[i].Target);
						}

						// This is a bit ugly, but C# doesn't allow Delegate to be used as generic constraint, so we have no choice.
						del = (T)(object)Delegate.Combine(delegates);
					}
					this.callback(del, this.state);
				}
			}

			/// <summary>
			/// Sets the method for the specified slot.
			/// </summary>
			/// <param name="index"></param>
			/// <param name="method"></param>
			public void SetMethod(int index, MethodInfo method)
			{
				if (this.slots[index].Method != null)
					throw new InvalidOperationException($"Method at {index} is already set.");

				this.slots[index].Method = method;
				this.checkComplete(index);
			}

			/// <summary>
			/// Sets the target object for the specified slot.
			/// </summary>
			/// <param name="index"></param>
			/// <param name="target"></param>
			public void SetTarget(int index, object target)
			{
				if (this.slots[index].HasTarget)
					throw new InvalidOperationException($"Target at {index} is already set.");

				this.slots[index].Target = target;
				this.slots[index].HasTarget = true;
				this.checkComplete(index);
			}
		}

		/// <summary>
		/// Addresses a slot in the <see cref="DelegateBuilder{T,TState}"/>.
		/// </summary>
		private struct SlotInfo<T, TState>
			where T : class /* Delegate */
		{
#pragma warning disable 649
			public DelegateBuilder<T, TState> Builder;
			public int Index;
#pragma warning restore 649

			// Static callback delegates so we don't need to allocate them every time.
			public static readonly DeferredCallback<MethodInfo, SlotInfo<T, TState>> SetMethodDelegate = setMethod;
			public static readonly DeferredCallback<object, SlotInfo<T, TState>> SetTargetDelegate = setTarget;
		}

		/// <summary>
		/// Static method for DelegateBuilder.SetMethod.
		/// </summary>
		private static void setMethod<T, TState>(MethodInfo method, SlotInfo<T, TState> state)
			where T : class /* Delegate */
		{
			state.Builder.SetMethod(state.Index, method);
		}

		/// <summary>
		/// Static method for DelegateBuilder.SetTarget.
		/// </summary>
		private static void setTarget<T, TState>(object target, SlotInfo<T, TState> state)
			where T : class /* Delegate */
		{
			state.Builder.SetTarget(state.Index, target);
		}

		private sealed class DelegateReaderBody : IReaderBody
		{
			private readonly Type delegateType;
			private readonly ReaderTemplate methodReader;
			private readonly ReaderTemplate targetReader;

			public int ByteSize => this.methodReader.ByteSize + this.targetReader.ByteSize;

			public bool IsDeferred => true;

			public bool CustomAugmentationHandling => false;

			public DelegateReaderBody(Type delegateType, ReaderTemplate methodReader, ReaderTemplate targetReader)
			{
				this.delegateType = delegateType;
				this.methodReader = methodReader;
				this.targetReader = targetReader;
			}

			public void Emit(ReaderEmitter emit)
			{
				ReaderEmitterDeferred deferred = (ReaderEmitterDeferred)emit;

				Type builderType = typeof(DelegateBuilder<,>).MakeGenericType(this.delegateType, deferred.StateType);
				Type stateType = typeof(SlotInfo<,>).MakeGenericType(this.delegateType, deferred.StateType);
				FieldInfo builderField = stateType.GetField("Builder");
				FieldInfo indexField = stateType.GetField("Index");

				Type callbackTypeMethod = typeof(DeferredCallback<,>).MakeGenericType(typeof(MethodInfo), stateType);
				FieldInfo setMethodDelegateField = stateType.GetField("SetMethodDelegate", BindingFlags.Public | BindingFlags.Static);

				Type callbackTypeTarget = typeof(DeferredCallback<,>).MakeGenericType(typeof(object), stateType);
				FieldInfo setTargetDelegateField = stateType.GetField("SetTargetDelegate", BindingFlags.Public | BindingFlags.Static);

				using (Local numInvocations = emit.DeclareLocal(typeof(int), "numInvocations", false))
				using (Local state = emit.DeclareLocal(stateType, "state"))
				{
					// numInvocation = strema.ReadVarInt()
					emit.LoadStream();
					emit.Stream.ReadVarInt();
					emit.ConvertToInt32Overflow();
					emit.StoreLocal(numInvocations);

					// state.Builder = new DelegateBuilder<TDelegate, TState>(numInvocations, callback, state)
					emit.LoadLocalAddress(state);
					emit.LoadLocal(numInvocations);
					deferred.LoadCallback();
					deferred.LoadState();
					emit.NewObject(builderType.GetConstructor(new[] {typeof(int), deferred.CallbackType, deferred.StateType}));
					emit.StoreField(builderField);

					Label loopStart = emit.DefineLabel("loopStart");
					Label loopEnd = emit.DefineLabel("loopEnd");

					// for i = 0 .. numInvocations
					emit.Branch(loopEnd);
					emit.MarkLabel(loopStart);

					this.methodReader.Emit(deferred.WithDeferred(callbackTypeMethod,
						() =>
						{
							emit.LoadField(setMethodDelegateField);
						},
						() =>
						{
							emit.LoadLocal(state);
						}), false);
	
					this.targetReader.Emit(deferred.WithDeferred(callbackTypeTarget,
						() =>
						{
							emit.LoadField(setTargetDelegateField);
						},
						() =>
						{
							emit.LoadLocal(state);
						}), false);

					// state.Index += 1
					emit.LoadLocalAddress(state);
					emit.Duplicate();
					emit.LoadField(indexField);
					emit.LoadConstant(1);
					emit.Add();
					emit.StoreField(indexField);

					// If i < numInvocations goto loopStart
					emit.MarkLabel(loopEnd);
					emit.LoadLocalAddress(state);
					emit.LoadField(indexField);
					emit.LoadLocal(numInvocations);
					emit.BranchIfLess(loopStart);
				}
			}
		}

		#endregion
	}
}
