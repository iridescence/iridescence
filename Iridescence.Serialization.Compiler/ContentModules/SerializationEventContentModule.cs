﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Emits a call to methods marked with any of the 4 .NET serialization attributes, <see cref="OnSerializingAttribute"/>, <see cref="OnSerializedAttribute"/>, <see cref="OnDeserializingAttribute"/> and <see cref="OnDeserializedAttribute"/>.
	/// </summary>
	public class SerializationEventContentModule : IContentModule
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the streaming context that is passed to the callbacks.
		/// </summary>
		public StreamingContext StreamingContext { get; }
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SerializationEventContentModule"/> class. 
		/// </summary>
		/// <param name="streamingContext">The <see cref="StreamingContext"/> to pass to the method.</param>
		public SerializationEventContentModule(StreamingContext streamingContext)
		{
			this.StreamingContext = streamingContext;
		}

		#endregion

		#region Methods

		public void ConfigureWriter(WriterTemplate template)
		{
			MethodInfo[] onSerializing = template.Specification.Type.GetMethodsWithAttribute(typeof(OnSerializingAttribute)).ToArray();
			MethodInfo[] onSerialized = template.Specification.Type.GetMethodsWithAttribute(typeof(OnSerializedAttribute)).ToArray();

			if (onSerializing.Length > 0)
				template.Augmentations.Add(new CallbackAugmentation(template.Specification.Type, onSerializing, this.StreamingContext, AugmentationPosition.BeforeBody));

			if (onSerialized.Length > 0)
				template.Augmentations.Add(new CallbackAugmentation(template.Specification.Type, onSerialized, this.StreamingContext, AugmentationPosition.AfterBodyDeferred));
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			MethodInfo[] onDeserializing = template.Specification.Type.GetMethodsWithAttribute(typeof(OnDeserializingAttribute)).ToArray();
			MethodInfo[] onDeserialized = template.Specification.Type.GetMethodsWithAttribute(typeof(OnDeserializedAttribute)).ToArray();

			if (onDeserializing.Length > 0)
				template.Augmentations.Add(new CallbackAugmentation(template.Specification.Type, onDeserializing, this.StreamingContext, AugmentationPosition.BeforeBody));

			if (onDeserialized.Length > 0)
				template.Augmentations.Add(new CallbackAugmentation(template.Specification.Type, onDeserialized, this.StreamingContext, AugmentationPosition.AfterBodyDeferred));
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class CallbackAugmentation : IAugmentation
		{
			private readonly Type objType;
			private readonly MethodInfo[] methods;
			private readonly StreamingContext streamingContext;

			public AugmentationPosition Position { get; }

			public CallbackAugmentation(Type objType, MethodInfo[] methods, StreamingContext streamingContext, AugmentationPosition position)
			{
				this.objType = objType;
				this.methods = methods;
				this.streamingContext = streamingContext;
				this.Position = position;
			}

			public void Emit(AugmentationEmitter emit)
			{
				foreach(MethodInfo method in this.methods)
				{
					if (!method.IsStatic)
					{
						// Load object onto stack.
						if (this.objType.IsValueType)
							emit.LoadValueAddress();
						else
							emit.LoadValue();
					}

					// Load StreamingContext.
					emit.LoadCaptured(this.streamingContext);

					// Call delegate.
					emit.Call(method);
				}
			}
		}
		
		#endregion
	}
}
