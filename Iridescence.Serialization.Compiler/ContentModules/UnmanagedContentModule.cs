﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Serializes the contents of a type by storing the raw memory contents in the stream.
	/// </summary>
	public class UnmanagedContentModule : IContentModule
	{
		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody || !template.Specification.Attributes.Any(attr => attr is SerializeUnmanagedAttribute))
				return;

			template.Body = (IWriterBody)Activator.CreateInstance(typeof(UnmanagedBody<>).MakeGenericType(template.Specification.Type));
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.HasBody || !template.Specification.Attributes.Any(attr => attr is SerializeUnmanagedAttribute))
				return;

			template.Body = (IReaderBody)Activator.CreateInstance(typeof(UnmanagedBody<>).MakeGenericType(template.Specification.Type));
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class UnmanagedBody<T> : IWriterBody, IReaderBody
			where T : struct
		{
			public bool IsDeferred => false;
			public bool CustomAugmentationHandling => false;
			public int ByteSize => Unsafe.SizeOf<T>();

			public UnmanagedBody()
			{
				
			}

			public void Emit(WriterEmitter emit)
			{
				emit.LoadStream();
				emit.LoadValueAddress();
				emit.ConvertToNativeUInt();
				emit.LoadConstant(1);
				emit.NewObject(typeof(ReadOnlySpan<T>).GetConstructor(new[] {typeof(void*), typeof(int)}));
				emit.Stream.WriteSpan(typeof(T), false);
			}

			public void Emit(ReaderEmitter emit)
			{
				if (emit is ReaderEmitterDeferred)
				{
					using (Local value = emit.DeclareLocal(typeof(T), "value"))
					{
						emit.LoadStream();
						emit.LoadLocalAddress(value);
						emit.ConvertToNativeUInt();
						emit.LoadConstant(1);
						emit.NewObject(typeof(Span<T>).GetConstructor(new[] {typeof(void*), typeof(int)}));
						emit.Stream.ReadSpan(typeof(T), false);

						using (emit.Store())
						{
							emit.LoadLocal(value);
						}
					}
				}
				else if (emit is ReaderEmitterDirect direct)
				{
					emit.LoadStream();
					direct.LoadValueAddress();
					emit.ConvertToNativeUInt();
					emit.LoadConstant(1);
					emit.NewObject(typeof(Span<T>).GetConstructor(new[] {typeof(void*), typeof(int)}));
					emit.Stream.ReadSpan(typeof(T), false);
				}
				else
				{
					throw new NotSupportedException();
				}
			}
		}
	}
}
