﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements logic to support method invocation serialization.
	/// </summary>
	public class MethodInvocationContentModule : IContentModule
	{
		#region Fields
		
		private static readonly MethodInfo makeMemberMethod = typeof(MethodInvocationContentModule).FindMethod(nameof(makeMember));
		private static readonly MethodInfo undeferMethod = typeof(DelegateExtensions).GetMethod(nameof(DelegateExtensions.UnDefer), BindingFlags.Static | BindingFlags.Public);

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MethodInvocationContentModule"/> class. 
		/// </summary>
		public MethodInvocationContentModule()
		{
		
		}
		
		#endregion
		
		#region Methods

		private static bool isDynamicMethodInvokation(SerializationMember member, out MemberInfo methodMember, out SerializationMember memberWithoutAttrib)
		{
			methodMember = null;
			memberWithoutAttrib = null;

			DynamicMethodInvocationAttribute attribute = (DynamicMethodInvocationAttribute)member.Attributes.SingleOrDefault(a => a is DynamicMethodInvocationAttribute);
			if(attribute == null)
				return false;

			methodMember = (MemberInfo)member.Type.GetField(attribute.MethodMember, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public) ??
			               (MemberInfo)member.Type.GetProperty(attribute.MethodMember, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

			if (methodMember == null)
				throw new MissingMemberException(member.Type.GetDisplayName(), attribute.MethodMember);

			memberWithoutAttrib = new SerializationMember(
				member.Type,
				member.TypeAttributes.Where(a => !(a is DynamicMethodInvocationAttribute)).ToImmutableArray(),
				member.MemberAttributes.Where(a => !(a is DynamicMethodInvocationAttribute)).ToImmutableArray(),
				member.Name);

			return true;
		}

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody)
				return;

			if (!isDynamicMethodInvokation(template.Specification, out MemberInfo methodMember, out SerializationMember memberWithoutAttrib))
				return;

			template.Body = new Body(memberWithoutAttrib, methodMember);
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.HasBody)
				return;

			if (!isDynamicMethodInvokation(template.Specification, out MemberInfo methodMember, out SerializationMember memberWithoutAttrib))
				return;

			template.Body = new Body(memberWithoutAttrib, methodMember);
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private static SerializationMember makeMember(SerializationMember content, MethodBase method)
		{
			return new SerializationMember(
				content.Type,
				content.TypeAttributes.Add(new MethodInvokationAttribute(method)),
				content.MemberAttributes,
				content.Name);
		}

		private sealed class Body : IWriterBody, IReaderBody
		{
			private readonly SerializationMember content;
			private readonly MemberInfo methodMember;

			public int ByteSize => 0;
			public bool IsDeferred => false;
			public bool CustomAugmentationHandling => false;

			public Body(SerializationMember content, MemberInfo methodMember)
			{
				this.content = content;
				this.methodMember = methodMember;
			}

			public void Emit(WriterEmitter emit)
			{
				using (Local methodLocal = emit.DeclareLocal(typeof(MethodBase)))
				using (Local memberLocal = emit.DeclareLocal(typeof(SerializationMember)))
				{
					// Get the MethodBase from the object instance.
					if (this.content.Type.IsValueType)
					{
						emit.LoadValueAddress();
					}
					else
					{
						emit.LoadValue();
					}

					if (this.methodMember is FieldInfo methodField)
					{
						emit.LoadField(methodField);
					}
					else if (this.methodMember is PropertyInfo methodProperty)
					{
						emit.CallVirtual(methodProperty.GetGetMethod());
					}
					else
					{
						throw new InvalidOperationException("Method member must be either a field or a property.");
					}

					emit.StoreLocal(methodLocal);

					// Write the method.
					Type writerDelType = typeof(ObjectWriterRef<,>).MakeGenericType(emit.Stream.Type, typeof(MethodBase));
					emit.LoadPairTuple(new SerializationMember(typeof(MethodBase)));
					emit.CallVirtual(ReflectionHelper.IDelegatePairTuple_Member);
					emit.CallVirtual(ReflectionHelper.IDelegatePair_Generic);
					emit.CastClass(writerDelType);
					emit.LoadStream();
					emit.LoadLocalAddress(methodLocal);
					emit.InvokeDelegate(typeof(ObjectWriterRef<,>).MakeGenericType(emit.Stream.Type, typeof(MethodBase)));

					// Derive the member.
					emit.LoadCaptured(this.content);
					emit.LoadLocal(methodLocal);
					emit.Call(makeMemberMethod);
					emit.StoreLocal(memberLocal);

					// Write contents of call.
					writerDelType = typeof(ObjectWriterRef<,>).MakeGenericType(emit.Stream.Type, this.content.Type);
					emit.LoadCompiler();
					emit.LoadLocal(memberLocal);
					emit.CallVirtual(ReflectionHelper.IWriterCompiler_For);
					emit.CallVirtual(ReflectionHelper.IDelegatePairTuple_Member);
					emit.CallVirtual(ReflectionHelper.IDelegatePair_Generic);
					emit.CastClass(writerDelType);

					// Invoke writer.
					emit.LoadStream();
					emit.LoadValueAddress();
					emit.InvokeDelegate(writerDelType);
				}
			}

			public void Emit(ReaderEmitter emit)
			{
				using (Local methodLocal = emit.DeclareLocal(typeof(MethodBase)))
				using (Local memberLocal = emit.DeclareLocal(typeof(SerializationMember)))
				{
					Type readerDelType = typeof(ObjectReaderDeferred<,,>).MakeGenericType(emit.Stream.Type, typeof(MethodBase), typeof(EmptyDeferredState));
					emit.LoadPairTuple(new SerializationMember(typeof(MethodBase)));
					emit.LoadType(typeof(EmptyDeferredState));
					emit.CallVirtual(ReflectionHelper.IReaderDelegatePairTuple_Deferred);
					emit.CallVirtual(ReflectionHelper.IDelegatePairTuple_Member);
					emit.CallVirtual(ReflectionHelper.IDelegatePair_Generic);
					emit.CastClass(readerDelType);
					emit.Call(undeferMethod.MakeGenericMethod(emit.Stream.Type, typeof(MethodBase), typeof(EmptyDeferredState)));

					// Read the method.
					emit.LoadStream();
					emit.LoadLocalAddress(methodLocal);
					emit.InvokeDelegate(typeof(ObjectReader<,>).MakeGenericType(emit.Stream.Type, typeof(MethodBase)));

					// Derive the member.
					emit.LoadCaptured(this.content);
					emit.LoadLocal(methodLocal);
					emit.Call(makeMemberMethod);
					emit.StoreLocal(memberLocal);

					if (emit is ReaderEmitterDeferred deferred)
					{
						// Read contents of call.
						readerDelType = typeof(ObjectReaderDeferred<,,>).MakeGenericType(deferred.Stream.Type, this.content.Type, deferred.StateType);
						deferred.LoadCompiler();
						deferred.LoadLocal(memberLocal);
						deferred.CallVirtual(ReflectionHelper.IReaderCompiler_For);
						deferred.LoadType(deferred.StateType);
						deferred.CallVirtual(ReflectionHelper.IReaderDelegatePairTuple_Deferred);
						deferred.CallVirtual(ReflectionHelper.IDelegatePairTuple_Member);
						deferred.CallVirtual(ReflectionHelper.IDelegatePair_Generic);
						deferred.CastClass(readerDelType);

						// Invoke reader.
						deferred.LoadStream();
						deferred.LoadCallback();
						deferred.LoadState();
						deferred.InvokeDelegate(readerDelType);
					}
					else
					{
						ReaderEmitterDirect direct = (ReaderEmitterDirect)emit;
						readerDelType = typeof(ObjectReader<,>).MakeGenericType(direct.Stream.Type, this.content.Type);
						// Read contents of call.
						direct.LoadCompiler();
						direct.LoadLocal(memberLocal);
						direct.CallVirtual(ReflectionHelper.IReaderCompiler_For);
						direct.CallVirtual(ReflectionHelper.IDelegatePairTuple_Member);
						direct.CallVirtual(ReflectionHelper.IDelegatePair_Generic);
						direct.CastClass(readerDelType);

						if (direct.Load != null)
						{
							direct.LoadStream();
							direct.LoadValueAddress();
							direct.InvokeDelegate(readerDelType);
						}
						else
						{
							using (Local temp = emit.DeclareLocal(this.content.Type))
							{
								direct.LoadStream();
								direct.LoadLocalAddress(temp);
								direct.InvokeDelegate(readerDelType);

								using (direct.Store())
								{
									direct.LoadLocal(temp);
								}
							}
						}
					}
				}
			}
		}

		#endregion
	}
}
