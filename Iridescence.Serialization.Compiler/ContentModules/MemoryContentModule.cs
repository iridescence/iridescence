﻿using System;
using System.Buffers;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Emits code to serialize/deserialize <see cref="Memory{T}"/>.
	/// </summary>
	public class MemoryContentModule : IContentModule
	{
		#region Fields

		private static readonly MethodInfo writeMemoryMethod = typeof(MemoryContentModule).GetMethod(nameof(writeMemory), BindingFlags.Static | BindingFlags.NonPublic);
		private static readonly MethodInfo readMemoryMethod = typeof(MemoryContentModule).GetMethod(nameof(readMemory), BindingFlags.Static | BindingFlags.NonPublic);
		private static readonly MethodInfo readReadOnlyMemoryMethod = typeof(MemoryContentModule).GetMethod(nameof(readReadOnlyMemory), BindingFlags.Static | BindingFlags.NonPublic);
	
		#endregion
		
		#region Methods

		public void CheckWriterCompatibility(Type destType)
		{
			if (!typeof(Stream).IsAssignableFrom(destType))
				throw new ArgumentException("Destination must be a Stream.", nameof(destType));
		}

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody)
				return;

			if(!template.Specification.Type.IsConstructedGenericType)
				return;

			Type def = template.Specification.Type.GetGenericTypeDefinition();
			bool isMemory = def == typeof(Memory<>);
			bool isReadOnlyMemory = def == typeof(ReadOnlyMemory<>);

			if(!isMemory && !isReadOnlyMemory)
				return;

			template.Body = new WriterBody(template.Specification.Type.GetGenericArguments()[0], isReadOnlyMemory);
		}

		public void CheckReaderCompatibility(Type sourceType)
		{
			if (!typeof(Stream).IsAssignableFrom(sourceType))
				throw new ArgumentException("Source must be a Stream.", nameof(sourceType));
		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.HasBody)
				return;

			if(!template.Specification.Type.IsConstructedGenericType)
				return;

			Type def = template.Specification.Type.GetGenericTypeDefinition();
			bool isMemory = def == typeof(Memory<>);
			bool isReadOnlyMemory = def == typeof(ReadOnlyMemory<>);

			if(!isMemory && !isReadOnlyMemory)
				return;

			template.Body = new ReaderBody(template.Specification.Type.GetGenericArguments()[0], isReadOnlyMemory);
		}

		/// <summary>
		/// Specifies what kind of memory is stored in the stream.
		/// </summary>
		private enum MemoryKind
		{
			/// <summary>
			/// Empty memory, nothing else is required.
			/// </summary>
			Empty,

			/// <summary>
			/// Member writer for T[].
			/// </summary>
			Array,

			/// <summary>
			/// Offset + Length + Member writer for T[].
			/// </summary>
			ArraySegment,
			
			/// <summary>
			/// Member writer for <see cref="MemoryManager{T}"/>.
			/// </summary>
			MemoryManager,

			/// <summary>
			/// Offset + Length + Member writer for <see cref="MemoryManager{T}"/>.
			/// </summary>
			MemoryManagerSegment,

			/// <summary>
			/// Raw array content.
			/// </summary>
			Unknown,
		}

		private static void writeMemory<TOutput, TElement>(IWriterCompiler writerCompiler, TOutput dest, ReadOnlyMemory<TElement> memory)
			where TOutput : Stream
		{
			if (memory.IsEmpty)
			{
				dest.WriteByte((byte)MemoryKind.Empty);
			}
			else if (MemoryMarshal.TryGetArray(memory, out ArraySegment<TElement> segment))
			{
				if (segment.Count == 0)
				{
					dest.WriteByte((byte)MemoryKind.Empty);
					return;
				}

				TElement[] array = segment.Array;
				var arrayWriter = writerCompiler.For<TOutput, TElement[]>().Member.Generic;
				if (segment.Offset == 0 && segment.Count == segment.Array.Length)
				{
					dest.WriteByte((byte)MemoryKind.Array);
					arrayWriter(dest, ref array);
				}
				else
				{
					dest.WriteByte((byte)MemoryKind.ArraySegment);
					dest.WriteInt32V(segment.Offset);
					dest.WriteInt32V(segment.Count);
					arrayWriter(dest, ref array);
				}
			}
			else if (MemoryMarshal.TryGetMemoryManager(memory, out MemoryManager<TElement> manager, out int start, out int length))
			{
				var managerWriter = writerCompiler.For<TOutput, MemoryManager<TElement>>().Member.Generic;

				if (start == 0 && length == memory.Length)
				{
					dest.WriteByte((byte)MemoryKind.MemoryManager);
					managerWriter(dest, ref manager);
				}
				else
				{
					dest.WriteByte((byte)MemoryKind.MemoryManagerSegment);
					dest.WriteInt32V(start);
					dest.WriteInt32V(length);
					managerWriter(dest, ref manager);
				}
			}
			else
			{
				dest.WriteByte((byte)MemoryKind.Unknown);
				TElement[] array = memory.ToArray();

				var arrayWriter = writerCompiler.For<TOutput, TElement[]>().Content.Generic;
				arrayWriter(dest, ref array);
			}
		}

		private readonly struct MemoryCallbackState<T, TState>
		{
			public readonly DeferredCallback<Memory<T>, TState> Callback;
			public readonly TState State;
			public readonly int Start;
			public readonly int Length;

			public MemoryCallbackState(DeferredCallback<Memory<T>, TState> callback, TState state)
			{
				this.Callback = callback;
				this.State = state;
				this.Start = -1;
				this.Length = -1;
			}

			public MemoryCallbackState(DeferredCallback<Memory<T>, TState> callback, TState state, int start, int length)
			{
				this.Callback = callback;
				this.State = state;
				this.Start = start;
				this.Length = length;
			}
		}

		private static void memoryCallback<T, TState>(MemoryManager<T> manager, MemoryCallbackState<T, TState> state)
		{
			Memory<T> memory = manager.Memory;

			if (state.Start >= 0)
			{
				memory = memory.Slice(state.Start, state.Length);
			}

			state.Callback(memory, state.State);
		}

		private static void memoryCallback<T, TState>(T[] array, MemoryCallbackState<T, TState> state)
		{
			Memory<T> memory = new Memory<T>(array);

			if (state.Start >= 0)
			{
				memory = memory.Slice(state.Start, state.Length);
			}

			state.Callback(memory, state.State);
		}

		private static void readMemory<TInput, TElement, TState>(IReaderCompiler readerCompiler, TInput source, DeferredCallback<Memory<TElement>, TState> callback, TState state)
			where TInput : Stream
		{
			MemoryKind kind = (MemoryKind)source.ReadByteOrThrow();
			switch (kind)
			{
				case MemoryKind.Empty:
				{
					callback(Memory<TElement>.Empty, state);
					break;
				}

				case MemoryKind.Array:
				{
					var arrayReader = readerCompiler.For<TInput, TElement[]>().Deferred<MemoryCallbackState<TElement, TState>>().Member.Generic;
					arrayReader(source, memoryCallback, new MemoryCallbackState<TElement, TState>(callback, state));
					break;
				}
					
				case MemoryKind.ArraySegment:
				{
					var arrayReader = readerCompiler.For<TInput, TElement[]>().Deferred<MemoryCallbackState<TElement, TState>>().Member.Generic;
					int start = source.ReadInt32V();
					int length = source.ReadInt32V();
					arrayReader(source, memoryCallback, new MemoryCallbackState<TElement, TState>(callback, state, start, length));
					break;
				}

				case MemoryKind.MemoryManager:
				{
					var managerReader = readerCompiler.For<TInput, MemoryManager<TElement>>().Deferred<MemoryCallbackState<TElement, TState>>().Member.Generic;
					managerReader(source, memoryCallback, new MemoryCallbackState<TElement, TState>(callback, state));
					break;
				}
					
				case MemoryKind.MemoryManagerSegment:
				{
					var managerReader = readerCompiler.For<TInput, MemoryManager<TElement>>().Deferred<MemoryCallbackState<TElement, TState>>().Member.Generic;
					int start = source.ReadInt32V();
					int length = source.ReadInt32V();
					managerReader(source, memoryCallback, new MemoryCallbackState<TElement, TState>(callback, state, start, length));
					return;
				}

				case MemoryKind.Unknown:
				{
					var arrayReader = readerCompiler.For<TInput, TElement[]>().Content.Generic;
					arrayReader(source, out TElement[] array);
					callback(new Memory<TElement>(array), state);
					return;
				}

				default:
					throw new InvalidDataException("Invalid memory kind.");
			}
		}

		private readonly struct ReadOnlyMemoryCallbackState<T, TState>
		{
			public readonly DeferredCallback<ReadOnlyMemory<T>, TState> Callback;
			public readonly TState State;

			public ReadOnlyMemoryCallbackState(DeferredCallback<ReadOnlyMemory<T>, TState> callback, TState state)
			{
				this.Callback = callback;
				this.State = state;
			}
		}
		
		private static void readOnlyMemoryCallback<T, TState>(Memory<T> memory, ReadOnlyMemoryCallbackState<T, TState> state)
		{
			state.Callback(memory, state.State);
		}

		private static void readReadOnlyMemory<TSource, T, TState>(IReaderCompiler readerCompiler, TSource source, DeferredCallback<ReadOnlyMemory<T>, TState> callback, TState state)
			where TSource : Stream
		{
			readMemory<TSource, T, ReadOnlyMemoryCallbackState<T, TState>>(readerCompiler, source, readOnlyMemoryCallback, new ReadOnlyMemoryCallbackState<T, TState>(callback, state));
		}

		private sealed class WriterBody : IWriterBody
		{
			private readonly Type elementType;
			private readonly bool isReadOnly;

			public int ByteSize => 0;

			public WriterBody(Type elementType, bool isReadOnly)
			{
				this.elementType = elementType;
				this.isReadOnly = isReadOnly;
			}

			public void Emit(WriterEmitter emit)
			{
				emit.LoadCompiler();
				emit.LoadStream();
				emit.LoadValue();

				if (!this.isReadOnly)
				{
					Type memoryType = typeof(Memory<>).MakeGenericType(this.elementType);
					MethodInfo castMethod = memoryType.GetMethod("op_Implicit", BindingFlags.Static | BindingFlags.Public, null, new[] {memoryType}, null);
					emit.Call(castMethod);
				}

				emit.Call(writeMemoryMethod.MakeGenericMethod(emit.Stream.Type, this.elementType));
			}
		}

		private sealed class ReaderBody : IReaderBody
		{
			private readonly Type elementType;
			private readonly bool isReadOnly;

			public int ByteSize => 0;

			public bool IsDeferred => true;

			public bool CustomAugmentationHandling => false;

			public ReaderBody(Type elementType, bool isReadOnly)
			{
				this.elementType = elementType;
				this.isReadOnly = isReadOnly;
			}

			public void Emit(ReaderEmitter emit)
			{
				ReaderEmitterDeferred deferredStack = (ReaderEmitterDeferred)emit;

				emit.LoadCompiler();
				emit.LoadStream();
				deferredStack.LoadCallback();
				deferredStack.LoadState();

				if (this.isReadOnly)
				{
					emit.Call(readReadOnlyMemoryMethod.MakeGenericMethod(emit.Stream.Type, this.elementType, deferredStack.StateType));
				}
				else
				{
					emit.Call(readMemoryMethod.MakeGenericMethod(emit.Stream.Type, this.elementType, deferredStack.StateType));
				}
			}
		}

		#endregion
	}
}
