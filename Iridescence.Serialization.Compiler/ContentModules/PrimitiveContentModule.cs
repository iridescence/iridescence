﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Emits code to write primitive types.
	/// </summary>
	public class PrimitiveContentModule : IContentModule
	{
		#region Nested Types

		private struct PrimitiveInfo
		{
			public readonly int ByteCount;

			public readonly Action<WriterEmitter> EmitWrite;
			public readonly Action<ReaderEmitter> ReadMethod;

			public PrimitiveInfo(int byteCount, Action<WriterEmitter> emitWrite, Action<ReaderEmitter> emitRead)
			{
				this.ByteCount = byteCount;
				this.EmitWrite = emitWrite;
				this.ReadMethod = emitRead;
			}
		}

		#endregion

		#region Fields

		private static readonly ReadOnlyDictionary<Type, PrimitiveInfo> primitives = new ReadOnlyDictionary<Type, PrimitiveInfo>(new Dictionary<Type, PrimitiveInfo>()
		{
			{typeof(sbyte), new PrimitiveInfo(1, e => e.Stream.WriteInt8(false), e => e.Stream.ReadInt8(false))},
			{typeof(byte), new PrimitiveInfo(1, e => e.Stream.WriteUInt8(false), e => e.Stream.ReadUInt8(false))},
			{typeof(short), new PrimitiveInfo(2, e => e.Stream.WriteInt16(false), e => e.Stream.ReadInt16(false))},
			{typeof(ushort), new PrimitiveInfo(2, e => e.Stream.WriteUInt16(false), e => e.Stream.ReadUInt16(false))},
			{typeof(int), new PrimitiveInfo(4, e => e.Stream.WriteInt32(false), e => e.Stream.ReadInt32(false))},
			{typeof(uint), new PrimitiveInfo(4, e => e.Stream.WriteUInt32(false), e => e.Stream.ReadUInt32(false))},
			{typeof(long), new PrimitiveInfo(8, e => e.Stream.WriteInt64(false), e => e.Stream.ReadInt64(false))},
			{typeof(ulong), new PrimitiveInfo(8, e => e.Stream.WriteUInt64(false), e => e.Stream.ReadUInt64(false))},
			{typeof(float), new PrimitiveInfo(4, e => e.Stream.WriteFloat32(false), e => e.Stream.ReadFloat32(false))},
			{typeof(double), new PrimitiveInfo(8, e => e.Stream.WriteFloat64(false), e => e.Stream.ReadFloat64(false))},
			{typeof(char), new PrimitiveInfo(2, e => e.Stream.WriteUInt16(false), e => e.Stream.ReadUInt16(false))},
			{typeof(bool), new PrimitiveInfo(1, e => e.Stream.WriteBoolean(false), e => e.Stream.ReadBoolean(false))},
			{
				typeof(IntPtr),
				new PrimitiveInfo(8, e =>
				{
					e.ConvertToInt64();
					e.Stream.WriteInt64(false);
				}, e =>
				{
					e.Stream.ReadInt64(false);
					e.ConvertToNativeIntOverflow();
				})
			},
			{
				typeof(UIntPtr),
				new PrimitiveInfo(8, e =>
				{
					e.ConvertToUInt64();
					e.Stream.WriteUInt64(false);
				}, e =>
				{
					e.Stream.ReadUInt64(false);
					e.ConvertToNativeUIntOverflow();
				})
			},
		});

		#endregion

		#region Methods

		public void ConfigureWriter(WriterTemplate template)
		{
			if (!template.Specification.Type.IsPrimitive || template.HasBody)
				return;

			PrimitiveInfo info = primitives[template.Specification.Type];
			template.Body = new PrimitiveWriterBody(info.EmitWrite, info.ByteCount);
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (!template.Specification.Type.IsPrimitive || template.HasBody)
				return;

			PrimitiveInfo info = primitives[template.Specification.Type];
			template.Body = new PrimitiveReaderBody(info.ReadMethod, info.ByteCount);
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		private sealed class PrimitiveWriterBody : IWriterBody
		{
			private readonly Action<WriterEmitter> emitWrite;

			public int ByteSize { get; }

			public PrimitiveWriterBody(Action<WriterEmitter> emitWrite, int bytes)
			{
				this.emitWrite = emitWrite;
				this.ByteSize = bytes;
			}

			public void Emit(WriterEmitter emit)
			{
				emit.LoadStream();
				emit.LoadValue();
				this.emitWrite(emit);
			}
		}

		private sealed class PrimitiveReaderBody : IReaderBody
		{
			private readonly Action<ReaderEmitter> emitRead;

			public int ByteSize { get; }

			public bool IsDeferred => false;

			public bool CustomAugmentationHandling => false;

			public PrimitiveReaderBody(Action<ReaderEmitter> emitRead, int bytes)
			{
				this.emitRead = emitRead;
				this.ByteSize = bytes;
			}

			public void Emit(ReaderEmitter emit)
			{
				using (emit.Store())
				{
					emit.LoadStream();
					this.emitRead(emit);
				}
			}
		}

		#endregion
	}
}
