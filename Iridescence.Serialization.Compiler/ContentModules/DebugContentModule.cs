﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Debug print content module.
	/// </summary>
	public class DebugContentModule : IContentModule
	{
		public void ConfigureWriter(WriterTemplate template)
		{
			template.Augmentations.Add(new DebugAugmentation(AugmentationPosition.BeforeBody, 1, $"Content: \"{template.Specification}\", Body: {template.Body}, Position: {{0}}", "("));
			template.Augmentations.Add(new DebugAugmentation(AugmentationPosition.AfterBody, -1, ")"));
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			template.Augmentations.Add(new DebugAugmentation(AugmentationPosition.BeforeBody, 1, $"Content: \"{template.Specification}\", Body: {template.Body}, Position: {{0}}", "("));
			template.Augmentations.Add(new DebugAugmentation(AugmentationPosition.AfterBody, -1, ")"));
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}
	}
}
