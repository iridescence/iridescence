﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Emits code to store <see cref="MemberInfo"/> and <see cref="AssemblyName"/> instances using <see cref="IMetadataWriter"/> and load them using <see cref="IMetadataReader"/>.
	/// </summary>
	public class MetadataContentModule : IContentModule
	{
		#region Fields

		private static readonly MethodInfo writeAssemblyNameMethod = typeof(IMetadataWriter).GetMethod(nameof(IMetadataWriter.WriteAssemblyName));
		private static readonly MethodInfo writeMemberMethod = typeof(IMetadataWriter).GetMethod(nameof(IMetadataWriter.WriteMember));
		private static readonly MethodInfo readAssemblyNameMethod = typeof(IMetadataReader).GetMethod(nameof(IMetadataReader.ReadAssemblyName));
		private static readonly MethodInfo readMemberMethod = typeof(IMetadataReader).GetMethod(nameof(IMetadataReader.ReadMember));

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MetadataContentModule"/> class. 
		/// </summary>
		public MetadataContentModule()
		{

		}

		#endregion

		#region Methods

		public void CheckReaderCompatibility(Type sourceType)
		{
			if (!typeof(IMetadataReader).IsAssignableFrom(sourceType))
				throw new ArgumentException($"Source class must implement {nameof(IMetadataReader)} in order to use {nameof(MetadataContentModule)}.", nameof(sourceType));
		}

		public void CheckWriterCompatibility(Type destType)
		{
			if (!typeof(IMetadataWriter).IsAssignableFrom(destType))
				throw new ArgumentException($"Dest class must implement {nameof(IMetadataWriter)} in order to use {nameof(MetadataContentModule)}.", nameof(destType));
		}

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody)
				return;

			if (template.Specification.Type == typeof(AssemblyName))
			{
				template.Body = new AssemblyNameWriterBody();
			}

			if (MetadataUtility.IsRuntimeMetadata(template.Specification.Type))
			{
				template.Body = new MemberInfoWriterBody();

				Type constraintType = template.Specification.Type;
				if (template.Specification.Type.IsSubclassOf(typeof(Type)))
					constraintType = typeof(Type);
				else if (template.Specification.Type.IsSubclassOf(typeof(FieldInfo)))
					constraintType = typeof(FieldInfo);
				else if (template.Specification.Type.IsSubclassOf(typeof(MethodInfo)))
					constraintType = typeof(MethodInfo);
				else if (template.Specification.Type.IsSubclassOf(typeof(ConstructorInfo)))
					constraintType = typeof(ConstructorInfo);
				else if (template.Specification.Type.IsSubclassOf(typeof(EventInfo)))
					constraintType = typeof(EventInfo);
				else if (template.Specification.Type.IsSubclassOf(typeof(PropertyInfo)))
					constraintType = typeof(PropertyInfo);

				template.MaskingInfo = new MaskingInfo(constraintType, typeof(RuntimeMetadataMaskAttribute));
			}
		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.Specification.MemberAttributes.Any(attr => attr is RuntimeMetadataMaskAttribute))
			{
				template.Body = new MemberInfoReaderBody(template.Specification.Type);
				return;
			}

			if (template.HasBody)
				return;

			if (typeof(AssemblyName).IsAssignableFrom(template.Specification.Type))
			{
				template.Body = new AssemblyNameReaderBody();
			}

			if (MetadataUtility.IsRuntimeMetadata(template.Specification.Type))
			{
				template.Body = new MemberInfoReaderBody(template.Specification.Type);
			}
		}
		
		private sealed class AssemblyNameWriterBody : IWriterBody
		{
			public int ByteSize => 0;

			public void Emit(WriterEmitter emit)
			{
				emit.LoadStream();
				emit.CastClass(typeof(IMetadataWriter));
				emit.LoadValue();
				emit.CallVirtual(writeAssemblyNameMethod);
			}
		}

		private sealed class AssemblyNameReaderBody : IReaderBody
		{
			public int ByteSize => 0;

			public bool IsDeferred => false;

			public bool CustomAugmentationHandling => false;

			public void Emit(ReaderEmitter emit)
			{
				using (emit.Store())
				{
					emit.LoadStream();
					emit.CastClass(typeof(IMetadataReader));
					emit.CallVirtual(readAssemblyNameMethod);
				}
			}
		}

		private sealed class MemberInfoWriterBody : IWriterBody
		{
			public int ByteSize => 0;

			public void Emit(WriterEmitter emit)
			{
				emit.LoadStream();
				emit.CastClass(typeof(IMetadataWriter));
				emit.LoadValue();
				emit.CallVirtual(writeMemberMethod);
			}
		}

		private sealed class MemberInfoReaderBody : IReaderBody
		{
			private readonly Type expectedType;
			private static readonly ConstructorInfo serExCtor = typeof(SerializationException).GetConstructor(new[] {typeof(string)}) ?? throw new MissingMethodException(nameof(SerializationException), ".ctor");

			public int ByteSize => 0;

			public bool IsDeferred => false;

			public bool CustomAugmentationHandling => false;

			public MemberInfoReaderBody(Type expectedType)
			{
				this.expectedType = expectedType;
			}

			public void Emit(ReaderEmitter emit)
			{
				using (emit.Store())
				{
					Label storeLabel = emit.DefineLabel("store");
					emit.LoadStream();
					emit.CastClass(typeof(IMetadataReader));
					emit.CallVirtual(readMemberMethod);
					emit.IsInstance(this.expectedType);
					emit.Duplicate();
					emit.BranchIfTrue(storeLabel);

					using (Local temp = emit.DeclareLocal(typeof(object), "temp", false))
					{
						emit.StoreLocal(temp);
						emit.LoadConstant($"Expected member of type \"{this.expectedType}\" but got \"{{0}}\" instead.");
						emit.FormatString(temp);
					}
						
					emit.NewObject(serExCtor);
					emit.Throw();

					emit.MarkLabel(storeLabel);
				}
			}
		}

		#endregion
	}

	[Serializable]
	internal sealed class RuntimeMetadataMaskAttribute : Attribute, ISerializationMemberAttribute
	{
		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			yield break;
		}
	}

}
