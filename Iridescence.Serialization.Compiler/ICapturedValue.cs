﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents an entry in the captured value table (i.e. closure) of a method.
	/// </summary>
	public interface ICapturedValue
	{
		/// <summary>
		/// The compile-time type of the value.
		/// </summary>
		Type Type { get; }

		/// <summary>
		/// Retrieves the value.
		/// </summary>
		/// <returns></returns>
		object GetValue();
	}
}
