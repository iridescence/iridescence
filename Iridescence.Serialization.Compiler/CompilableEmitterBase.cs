﻿using System;
using System.Collections.Generic;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents an emitter that can be compiled to a real method/delegate.
	/// </summary>
	public abstract class CompilableEmitter : EmitterBase
	{
		protected CompilableEmitter(ICompilationUnit unit, ILBuilder emit, IList<ICapturedValue> capturedValues, bool trace)
			: base(unit, emit, capturedValues, trace)
		{

		}

		/// <summary>
		/// Returns the compiled method after the compilation unit has been completed.
		/// </summary>
		/// <returns></returns>
		public abstract CompiledMethod GetCompiledMethod();
	}

	/// <summary>
	/// Represents a compiled method resulting from a <see cref="CompilableEmitter"/>.
	/// </summary>
	public abstract class CompiledMethod
	{
		/// <summary>
		/// Returns a <see cref="CallableMethod"/> that can be used in other IL builders to call this method.
		/// </summary>
		public abstract CallableMethod Callable { get; }

		/// <summary>
		/// Returns a delegate for this method of the specified type bound to the specified target.
		/// </summary>
		/// <param name="delegateType"></param>
		/// <param name="target"></param>
		/// <returns></returns>
		public abstract Delegate CreateDelegate(Type delegateType, object target);
	}
}