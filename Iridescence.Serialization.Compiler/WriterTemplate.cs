﻿using System;
using System.Collections.Generic;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents a template for a writer method.
	/// </summary>
	public class WriterTemplate : Template
	{
		#region Properties

		/// <summary>
		/// Gets the <see cref="IWriterTemplateFactory"/> that created this template.
		/// </summary>
		public IWriterTemplateFactory TemplateFactory { get; }

		public Type OutputType => this.OutputEmitterFactory.OutputType;

		public OutputEmitterFactory OutputEmitterFactory { get; }

		/// <summary>
		/// Gets the body build steps.
		/// </summary>
		public IWriterBody Body { get; set; }

		/// <summary>
		/// Gets or sets <see cref="MaskingInfo"/> for the member.
		/// </summary>
		public MaskingInfo MaskingInfo { get; set; }

		/// <summary>
		/// Gets a value that indicates whether the method has any body build steps.
		/// </summary>
		public bool HasBody => this.Body != null;

		/// <summary>
		/// Gets the total number of bytes that have to be reserved.
		/// </summary>
		public int ByteSize => this.Body?.ByteSize ?? 0;

		#endregion

		#region Constructors

		private WriterTemplate(IWriterTemplateFactory templateFactory, OutputEmitterFactory outputEmitterFactory, SerializationMember memberSpec)
			: base(memberSpec)
		{
			this.TemplateFactory = templateFactory ?? throw new ArgumentNullException(nameof(templateFactory));
			this.OutputEmitterFactory = outputEmitterFactory ?? throw new ArgumentNullException(nameof(outputEmitterFactory));
		}

		#endregion

		#region Methods

		public static WriterTemplate Create(IWriterTemplateFactory templateFactory, OutputEmitterFactory outputEmitterFactory, SerializationMember memberSpec, IEnumerable<ICompilerModule> modules)
		{
			WriterTemplate template = new WriterTemplate(templateFactory, outputEmitterFactory, memberSpec);

			foreach (ICompilerModule module in modules)
			{
				module.ConfigureWriter(template);
			}

			if (!template.HasBody)
			{
				throw new CompilationException($"Writer for \"{memberSpec}\" doesn't have a body.");
			}

			return template;
		}

		/// <summary>
		/// Emits the code.
		/// </summary>
		public void Emit(WriterEmitter emit, bool reserveBytes)
		{
			if (emit.Stream.IsBuffered && reserveBytes && this.ByteSize > 0)
			{
				// Reserve bytes for writer.
				emit.LoadStream();
				emit.LoadConstant(this.ByteSize);
				emit.Stream.Reserve();
			}

			AugmentationCollection augs = new AugmentationCollection(this);
			AugmentationEmitter augStack = new AugmentationEmitter(emit);

			augs.Emit(augStack, AugmentationPosition.BeforeBody);

			this.Body.Emit(emit);

			augs.Emit(augStack, AugmentationPosition.AfterBody, AugmentationPosition.AfterBodyDeferred);
		}

		#endregion
	}
}