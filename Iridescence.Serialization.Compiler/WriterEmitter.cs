﻿using System;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents the state of the writer compiler.
	/// </summary>
	public class WriterEmitter : EmitterBase
	{
		#region Properties

		/// <summary>
		/// Gets the compiler.
		/// </summary>
		public IWriterCompiler WriterCompiler { get; }

		/// <summary>
		/// Gets the stream emit API.
		/// </summary>
		public IOutputEmitter Stream { get; }

		/// <summary>
		/// Gets the <see cref="LoadFunc"/> to load the value onto the stack.
		/// </summary>
		public LoadFunc Load { get; }

		#endregion

		#region Constructors

		public WriterEmitter(EmitterBase emit, IWriterCompiler writerCompiler, IOutputEmitterFactory outputEmitterFactory, LoadFunc load)
			: base(emit)
		{
			this.WriterCompiler = writerCompiler ?? throw new ArgumentNullException(nameof(writerCompiler));
			this.Stream = outputEmitterFactory.CreateOutputEmitter(this);
			this.Load = load ?? throw new ArgumentNullException(nameof(load));
		}

		private WriterEmitter(WriterEmitter other, LoadFunc load)
			: base(other)
		{
			this.WriterCompiler = other.WriterCompiler;
			this.Stream = other.Stream;
			this.Load = load ?? throw new ArgumentNullException(nameof(load));
		}

		#endregion

		#region Methods

		/// <summary>
		/// Loads the <see cref="WriterCompiler"/> value onto the stack.
		/// </summary>
		public void LoadCompiler()
		{
			this.LoadCaptured(this.WriterCompiler);
		}

		/// <summary>
		/// Loads the <see cref="IDelegatePairTuple"/> for the specified member onto the stack.
		/// </summary>
		public void LoadPairTuple(SerializationMember member)
		{
			this.LoadCompiler();
			this.LoadCaptured(member);
			this.CallVirtual(ReflectionHelper.IWriterCompiler_For);
		}

		/// <summary>
		/// Loads the <see cref="IDelegatePairTuple"/> for the specified member (with changed type) onto the stack.
		/// </summary>
		public void LoadPairTupleChangedType(SerializationMember member, ILoadProvider loadType)
		{
			this.LoadCompiler();
			this.LoadCaptured(member);
			loadType.Load(this, LoadMode.Value);
			//this.LoadNull();
			this.Call(ReflectionHelper.SerializationMember_ChangeType);
			this.CallVirtual(ReflectionHelper.IWriterCompiler_For);
		}

		/// <summary>
		/// Loads the member value onto the stack.
		/// </summary>
		public void LoadValue()
		{
			this.Load(LoadMode.Value);
		}

		/// <summary>
		/// Loads the address to the member value onto the stack.
		/// </summary>
		public void LoadValueAddress()
		{
			this.Load(LoadMode.Address);
		}

		public WriterEmitter WithLoad(LoadFunc newLoad)
		{
			return new WriterEmitter(this, newLoad);
		}

		#endregion
	}
}