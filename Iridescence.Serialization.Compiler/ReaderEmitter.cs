﻿using System;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents the state of the reader compiler.
	/// </summary>
	public abstract class ReaderEmitter : EmitterBase
	{
		#region Properties

		/// <summary>
		/// Gets the compiler.
		/// </summary>
		public IReaderCompiler ReaderCompiler { get; }

		/// <summary>
		/// Gets the stream emit API.
		/// </summary>
		public IInputEmitter Stream { get; }

		#endregion

		#region Constructors

		protected ReaderEmitter(EmitterBase emit, IReaderCompiler readerCompiler, IInputEmitterFactory inputEmitterFactory)
			: base(emit)
		{
			this.ReaderCompiler = readerCompiler ?? throw new ArgumentNullException(nameof(readerCompiler));
			this.Stream = inputEmitterFactory.CreateInputEmitter(this);
		}

		protected ReaderEmitter(ReaderEmitter other)
			: base(other)
		{
			this.ReaderCompiler = other.ReaderCompiler;
			this.Stream = other.Stream;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Loads the <see cref="IReaderCompiler"/> value onto the stack.
		/// </summary>
		public void LoadCompiler()
		{
			this.LoadCaptured(this.ReaderCompiler);
		}
		
		/// <summary>
		/// Loads the <see cref="IReaderDelegatePairTuple"/> for the specified member onto the stack.
		/// </summary>
		public void LoadPairTuple(SerializationMember member)
		{
			this.LoadCompiler();
			this.LoadCaptured(member);
			this.CallVirtual(ReflectionHelper.IReaderCompiler_For);
		}

		/// <summary>
		/// Loads the <see cref="IReaderDelegatePairTuple"/> for the specified member (with changed type) onto the stack.
		/// </summary>
		public void LoadPairTupleChangedType(SerializationMember member, ILoadProvider loadType)
		{
			this.LoadCompiler();
			this.LoadCaptured(member);
			loadType.Load(this, LoadMode.Value);
			//this.LoadNull();
			this.Call(ReflectionHelper.SerializationMember_ChangeType);
			this.CallVirtual(ReflectionHelper.IReaderCompiler_For);
		}
		
		/// <summary>
		/// Stores the value.
		/// The call to this method starts the store operation.
		/// Disposing the object returned by this method completes the store operation.
		/// The value to store is supposed to be put on the stack in between these two.
		/// </summary>
		/// <returns></returns>
		public abstract StoreEnd Store();

		public ReaderEmitterDirect WithLoadStore(LoadFunc loadFunc, StoreFunc storeFunc)
		{
			return new ReaderEmitterDirect(this, loadFunc, storeFunc);
		}

		public ReaderEmitterDirect WithLocal(Local local)
		{
			return new ReaderEmitterDirect(this, LoadStore.LoadLocal(this, local), LoadStore.StoreLocal(this, local));
		}

		public ReaderEmitterDeferred WithDeferred(Type callbackType, Action loadDeferredCallback, Action loadDeferredState)
		{
			return new ReaderEmitterDeferred(this, callbackType, loadDeferredCallback, loadDeferredState);
		}

		#endregion
	}
}
