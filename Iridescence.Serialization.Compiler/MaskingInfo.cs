﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Provides information about how member information should be masked in case it can not be written directly to the output.
	/// This allows for opaque types to be used in serialization such that the actual runtime type is not stored.
	/// </summary>
	public sealed class MaskingInfo
	{
		#region Fields
		
		#endregion
		
		#region Properties

		/// <summary>
		/// The type constraint for the deserializer (i.e. what type it can expect).
		/// </summary>
		public Type TypeConstraint { get; }
		
		/// <summary>
		/// Gets the types of the member attributes to apply.
		/// To keep this simple, attribute data is not supported - only parameterless attributes.
		/// </summary>
		public IReadOnlyList<Type> MemberAttributes { get; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MaskingInfo"/> class. 
		/// </summary>
		public MaskingInfo(Type typeContraint, params Type[] memberAttributes)
		{
			this.TypeConstraint = typeContraint;
			this.MemberAttributes = memberAttributes.ToReadOnlyList(true);
		}
		
		#endregion
		
		#region Methods

		public SerializationMember GetMember(SerializationMember original)
		{
			if (original == null)
				throw new ArgumentNullException(nameof(original));

			IEnumerable<ISerializationMemberAttribute> memberAttrs = this.MemberAttributes.Select(Activator.CreateInstance).Cast<ISerializationMemberAttribute>();

			return new SerializationMember(
				this.TypeConstraint ?? original.Type,
				original.TypeAttributes,
				original.MemberAttributes.AddRange(memberAttrs));
		}

		#endregion
	}
}
