﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// The base class for a compiled method's target (or "this" instance), providing some built-in functionality.
	/// </summary>
	internal abstract class EmittedMethod
	{
		#region Fields

		private readonly object[] capturedValues;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="EmittedMethod"/> class. 
		/// </summary>
		protected EmittedMethod(object[] capturedValues)
		{
			this.capturedValues = capturedValues;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the captured value at the specified index.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="index"></param>
		/// <returns></returns>
		public T GetCapturedValue<T>(int index)
		{
			return (T)this.capturedValues[index];
		}

		#endregion
	}
}
