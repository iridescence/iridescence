﻿using System;
using System.Collections.Generic;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	public class ReaderCompiler : IReaderCompiler, IReaderTemplateFactory
	{
		#region Fields

		private readonly Dictionary<MemberCacheKeyDeferred, IDelegatePairTuple> cache;
		private readonly InputEmitterFactory inputEmitterFactory;
		private readonly Type nonGenericDelegateType;

		#endregion

		#region Properties

		public CompilerConfiguration Configuration { get; }

		public Type InputType => this.inputEmitterFactory.InputType;

		#endregion

		#region Constructors

		public ReaderCompiler(CompilerConfiguration configuration, Type inputType)
		{
			this.Configuration = configuration;
			this.inputEmitterFactory = new InputEmitterFactory(configuration.IOModule, inputType);

			foreach (IContentModule module in configuration.ContentModules)
			{
				module.CheckReaderCompatibility(this.InputType);
			}

			foreach (IMemberModule module in configuration.MemberModules)
			{
				module.CheckReaderCompatibility(this.InputType);
			}

			this.cache = new Dictionary<MemberCacheKeyDeferred, IDelegatePairTuple>();

			this.nonGenericDelegateType = typeof(ObjectReader<,>).MakeGenericType(this.InputType, typeof(object));
		}

		#endregion

		#region Methods
		
		ReaderTemplate IReaderTemplateFactory.CreateTemplate(SerializationMember member, SerializationMode mode, bool forceDeferred)
		{
			if (mode == SerializationMode.Member)
				return ReaderTemplate.Create(this, this.inputEmitterFactory, member, this.Configuration.MemberModules, forceDeferred);

			// Check whether we can actually serialize that type.
			this.Configuration.Arbiter?.VerifyType(member.Type);
			return ReaderTemplate.Create(this, this.inputEmitterFactory, member, this.Configuration.ContentModules, forceDeferred);
		}

		public IReaderDelegatePairTuple For(SerializationMember memberSpec)
		{
			return (IReaderDelegatePairTuple)this.getOrAdd(memberSpec, null);
		}

		private IDelegatePairTuple getOrAdd(SerializationMember memberSpec, Type stateType)
		{
			MemberCacheKeyDeferred key = new MemberCacheKeyDeferred(new MemberCacheKey(memberSpec), stateType);

			lock (this.cache)
			{
				if (!this.cache.TryGetValue(key, out IDelegatePairTuple tuple))
				{
					tuple = stateType == null ? this.GetPairTuple(memberSpec) : this.GetPairTuple(memberSpec, stateType);
					this.cache.Add(key, tuple);
				}

				return tuple;
			}
		}

		protected virtual IReaderDelegatePairTuple GetPairTuple(SerializationMember memberSpec)
		{
			return new CompiledPairTuple(this, memberSpec);
		}
		
		protected virtual IDelegatePairTuple GetPairTuple(SerializationMember memberSpec, Type stateType)
		{
			return new CompiledDeferredPairTuple(this, memberSpec, stateType);
		}

		protected virtual IDelegatePair GetPair(SerializationMember memberSpec, SerializationMode mode)
		{
			ReaderTemplate template = ((IReaderTemplateFactory)this).CreateTemplate(memberSpec, mode);
			if (template.IsDeferred)
				throw new ArgumentException("The template requires a deferred execution mode.", nameof(template));

			Type delType = typeof(ObjectReader<,>).MakeGenericType(this.InputType, memberSpec.Type);

			// Compile.
			ICompilationUnit unit = this.Configuration.CompilerFactory.CreateUnit();
			string name = $"{memberSpec.Type.GetDisplayName()}.Read{mode}";
			CompilableEmitter compilable = unit.CreateMethod(MethodSignature.FromDelegate(delType).WithThis(typeof(EmittedMethod)), name);

			ReaderEmitter readerEmit = new ReaderEmitterDirect(compilable, this, this.inputEmitterFactory,
				mode1 =>
				{
					if (mode1 == LoadMode.Address)
					{
						compilable.LoadArgument(2);
					}
					else
					{
						compilable.LoadArgument(2);
						compilable.LoadIndirect(template.Specification.Type);
					}
				},
				() =>
				{
					//if (template.Specification.Type.IsValueType)
					//{
					//	return new StoreEnd(() => compilable.StoreArgument(2));
					//}

					compilable.LoadArgument(2);
					return new StoreEnd(() => compilable.StoreIndirect(template.Specification.Type));
				});

			// Emit template.
			template.Emit(readerEmit, true);
			compilable.Return();
			CompilableEmitter emit = compilable;
			unit.Complete();

			return new CompiledPair(this, memberSpec, mode, emit.GetCompiledMethod(), emit.CompileCapturedValues());
		}

		protected virtual IDelegatePair GetPair(SerializationMember memberSpec, SerializationMode mode, Type stateType)
		{
			ReaderTemplate template = ((IReaderTemplateFactory)this).CreateTemplate(memberSpec, mode, true);
			if (!template.IsDeferred)
				throw new ArgumentException("The template requires a linear execution mode.", nameof(template));

			Type delType = typeof(ObjectReaderDeferred<,,>).MakeGenericType(this.InputType, memberSpec.Type, stateType);
			Type callbackType = typeof(DeferredCallback<,>).MakeGenericType(memberSpec.Type, stateType);

			// Compile.
			ICompilationUnit unit = this.Configuration.CompilerFactory.CreateUnit();
			string name = $"{memberSpec.Type.GetDisplayName()}.Read{mode}Deferred<{stateType.GetDisplayName()}>";
			CompilableEmitter compilable = unit.CreateMethod(MethodSignature.FromDelegate(delType).WithThis(typeof(EmittedMethod)), name);

			ReaderEmitterDeferred readerEmit = new ReaderEmitterDeferred(compilable, this, this.inputEmitterFactory, callbackType, () => compilable.LoadArgument(2), () => compilable.LoadArgument(3));

			// Emit template.
			template.Emit(readerEmit, true);
			compilable.Return();
			CompilableEmitter emit = compilable;
			unit.Complete();

			return new CompiledDeferredPair(this, memberSpec, mode, stateType, emit.GetCompiledMethod(), emit.CompileCapturedValues());
		}

		protected virtual Delegate GetNonGenericWrapper(SerializationMember memberSpec, SerializationMode mode)
		{
			// Get underlying generic delegate.
			IDelegatePairTuple tuple = this.For(memberSpec);
			IDelegatePair pair = mode == SerializationMode.Content ? tuple.Content : tuple.Member;

			CallableMethod method;
			object target;
			if (pair is CompiledPair p)
			{
				method = p.Method.Callable;
				target = p;
			}
			else
			{
				method = pair.Generic.GetType().GetInvokeMethod();
				target = pair.Generic;
			}

			// Compile non-generic wrapper around it.
			Type type = memberSpec.Type;
			ICompilationUnit unit = this.Configuration.CompilerFactory.CreateUnit();
			string name = $"{memberSpec.Type.GetDisplayName()}.Read{mode}NonGeneric";
			MethodSignature sig = MethodSignature.FromDelegate(this.nonGenericDelegateType);
			if (target != null) sig = sig.WithThis(target.GetType());
			CompilableEmitter emit = unit.CreateMethod(sig, name, withCapturedValues: false);

			Local tempLocal = emit.DeclareLocal(type, "temp");

			if (!type.IsValueType)
			{
				emit.LoadArgument(2);
				emit.LoadIndirect(typeof(object));
				emit.CastClass(type);
				emit.StoreLocal(tempLocal);
			}

			// this.genericDelegate(context, ref temp);
			emit.LoadArgument(0);
			emit.LoadArgument(1);
			emit.LoadLocalAddress(tempLocal);

			if (method.Method.IsStatic)
				emit.Call(method);
			else
				emit.CallVirtual(method);

			// obj = (object)temp;
			emit.LoadArgument(2);
			emit.LoadLocal(tempLocal);
			if (type.IsValueType)
			{
				emit.Box(type);
			}

			emit.StoreIndirect(typeof(object));

			emit.Return();
			return emit.GetCompiledMethod().CreateDelegate(this.nonGenericDelegateType, target);
		}

		protected virtual Delegate GetNonGenericWrapper(SerializationMember memberSpec, SerializationMode mode, Type stateType)
		{
			// Get underlying generic delegate.
			IDelegatePairTuple tuple = this.For(memberSpec).Deferred(stateType);
			IDelegatePair pair = mode == SerializationMode.Content ? tuple.Content : tuple.Member;


			CallableMethod method;
			object target;
			if (pair is CompiledPair p)
			{
				method = p.Method.Callable;
				target = p;
			}
			else
			{
				method = pair.Generic.GetType().GetInvokeMethod();
				target = pair.Generic;
			}

			// Compile non-generic wrapper around it.
			Type delType = typeof(ObjectReaderDeferred<,,>).MakeGenericType(this.InputType, typeof(object), stateType);
			ICompilationUnit unit = this.Configuration.CompilerFactory.CreateUnit();
			MethodSignature sig = MethodSignature.FromDelegate(delType);
			if (target != null) sig = sig.WithThis(target.GetType());
			string name = $"{memberSpec.Type.GetDisplayName()}.Read{mode}Deferred<{stateType.GetDisplayName()}>NonGeneric";
			CompilableEmitter emit = unit.CreateMethod(sig, name, withCapturedValues: false);

			Type nonGenericCallbackType = typeof(DeferredCallback<,>).MakeGenericType(typeof(object), stateType);
			Type genericCallbackType = typeof(DeferredCallback<,>).MakeGenericType(memberSpec.Type, stateType);

			emit.LoadArgument(0);
			emit.LoadArgument(1);
			emit.LoadArgument(2);

			if (memberSpec.Type.IsValueType)
			{
				Type wrapperType = typeof(DeferredCallbackWrapper<,>).MakeGenericType(memberSpec.Type, stateType);
				
				// Create instance of wrapper.
				emit.NewObject(wrapperType.GetConstructor(new[] {nonGenericCallbackType}));
				emit.LoadFunctionPointer(wrapperType.GetMethod("Invoke"));
				emit.NewObject(genericCallbackType.GetConstructor(new[] {typeof(object), typeof(IntPtr)}));
			}

			emit.CastClass(genericCallbackType); // This shouldn't be necessary.
			emit.LoadArgument(3);
			//emit.InvokeDelegate(this.genericDelegate.GetType());

			if(method.Method.IsStatic)
				emit.Call(method);
			else
				emit.CallVirtual(method);
			
			emit.Return();

			unit.Complete();
			return emit.GetCompiledMethod().CreateDelegate(delType, target);
		}

		#endregion

		#region Nested Types

		private readonly struct MemberCacheKeyDeferred : IEquatable<MemberCacheKeyDeferred>
		{
			public readonly MemberCacheKey Key;
			public readonly Type StateType;

			public MemberCacheKeyDeferred(MemberCacheKey key, Type stateType)
			{
				this.Key = key;
				this.StateType = stateType;
			}

			public bool Equals(MemberCacheKeyDeferred other)
			{
				return this.Key.Equals(other.Key) && this.StateType == other.StateType;
			}

			public override bool Equals(object obj)
			{
				return obj is MemberCacheKeyDeferred other && this.Equals(other);
			}

			public override int GetHashCode()
			{
				return this.Key.GetHashCode() ^ (this.StateType != null ? this.StateType.GetHashCode() : 0);
			}
		}

		private sealed class CompiledPairTuple : IReaderDelegatePairTuple
		{
			private readonly ReaderCompiler compiler;
			private readonly SerializationMember memberSpec;
			private IDelegatePair contentReader;
			private IDelegatePair memberReader;

			public IDelegatePair Content => this.contentReader ??= this.compiler.GetPair(this.memberSpec, SerializationMode.Content);
			public IDelegatePair Member => this.memberReader ??= this.compiler.GetPair(this.memberSpec, SerializationMode.Member);

			public CompiledPairTuple(ReaderCompiler compiler, SerializationMember memberSpec)
			{
				this.compiler = compiler;
				this.memberSpec = memberSpec;
			}

			public IDelegatePairTuple Deferred(Type stateType)
			{
				return this.compiler.getOrAdd(this.memberSpec, stateType);
			}
		}
		
		private sealed class CompiledPair : EmittedMethod, IDelegatePair
		{
			private readonly ReaderCompiler compiler;
			private readonly SerializationMember memberSpec;
			private readonly SerializationMode mode;
			public readonly CompiledMethod Method;
			private Delegate genericDelegate;
			private Delegate nonGenericDelegate;

			public Delegate Generic => this.genericDelegate ??=
				this.Method.CreateDelegate(typeof(ObjectReader<,>).MakeGenericType(this.compiler.InputType, this.memberSpec.Type), this);

			public Delegate NonGeneric => this.nonGenericDelegate ??=
				this.compiler.GetNonGenericWrapper(this.memberSpec, this.mode);

			public CompiledPair(ReaderCompiler compiler, SerializationMember memberSpec, SerializationMode mode, CompiledMethod method, object[] capturedValues)
				: base(capturedValues)
			{
				this.compiler = compiler;
				this.memberSpec = memberSpec;
				this.mode = mode;
				this.Method = method;
			}
		}

		private sealed class CompiledDeferredPairTuple : IDelegatePairTuple
		{
			private readonly ReaderCompiler compiler;
			private readonly SerializationMember memberSpec;
			private readonly Type stateType;
			private IDelegatePair contentReader;
			private IDelegatePair memberReader;

			public IDelegatePair Content => this.contentReader ??= this.compiler.GetPair(this.memberSpec, SerializationMode.Content, this.stateType);
			public IDelegatePair Member => this.memberReader ??= this.compiler.GetPair(this.memberSpec, SerializationMode.Member, this.stateType);

			public CompiledDeferredPairTuple(ReaderCompiler compiler, SerializationMember memberSpec, Type stateType)
			{
				this.compiler = compiler;
				this.memberSpec = memberSpec;
				this.stateType = stateType;
			}
		}
		
		private sealed class CompiledDeferredPair : EmittedMethod, IDelegatePair
		{
			private readonly ReaderCompiler compiler;
			private readonly SerializationMember memberSpec;
			private readonly SerializationMode mode;
			private readonly Type stateType;
			public readonly CompiledMethod Method;
			private Delegate genericDelegate;
			private Delegate nonGenericDelegate;

			public Delegate Generic => this.genericDelegate ??= 
				this.Method.CreateDelegate(typeof(ObjectReaderDeferred<,,>).MakeGenericType(this.compiler.InputType, this.memberSpec.Type, this.stateType), this);

			public Delegate NonGeneric => this.nonGenericDelegate ??=
				this.compiler.GetNonGenericWrapper(this.memberSpec, this.mode, this.stateType);

			public CompiledDeferredPair(ReaderCompiler compiler, SerializationMember memberSpec, SerializationMode mode, Type stateType, CompiledMethod method, object[] capturedValues)
				: base(capturedValues)
			{
				this.compiler = compiler;
				this.memberSpec = memberSpec;
				this.mode = mode;
				this.stateType = stateType;
				this.Method = method;
			}
		}

		#endregion
	}
}