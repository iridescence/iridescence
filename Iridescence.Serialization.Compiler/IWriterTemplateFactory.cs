﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Factory for <see cref="WriterTemplate">writer templates</see>.
	/// </summary>
	public interface IWriterTemplateFactory
	{
		WriterTemplate CreateTemplate(SerializationMember member, SerializationMode mode);
	}
}