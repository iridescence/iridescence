﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	public class MethodBuilderEmitter : CompilableEmitter
	{
		#region Fields

		private string name;
		private readonly MethodBuilderUnit unit;
		private readonly MethodILBuilder ilBuilder;
		private MethodInfo compiledMethod;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public MethodBuilderEmitter(MethodBuilderUnit unit, MethodILBuilder ilBuilder, IList<ICapturedValue> externalValues, bool trace)
			: base(unit, ilBuilder, externalValues, trace)
		{
			this.ilBuilder = ilBuilder;
			this.unit = unit;
			this.unit.Completing += this.onCompleting;
			this.unit.Completed += this.onCompleted;
		}

		#endregion

		#region Methods

		private void onCompleting(object sender, EventArgs e)
		{
			this.unit.Completing -= this.onCompleting;

			MethodBuilder m = this.ilBuilder.Compile();
			this.name = m.Name;
		}

		private void onCompleted(object sender, EventArgs e)
		{
			this.unit.Completed -= this.onCompleted;

			this.compiledMethod = this.unit.Type.GetMethod(this.name, BindingFlags.Public | BindingFlags.Static);
			this.name = null;
		}
		
		public override CompiledMethod GetCompiledMethod()
		{
			if (this.compiledMethod == null)
				throw new CompilationNotCompletedException();

			return new CompiledMethodBuilder(this.compiledMethod);
		}

		private sealed class CompiledMethodBuilder : CompiledMethod
		{
			private readonly MethodInfo method;

			public override CallableMethod Callable => this.method;

			public CompiledMethodBuilder(MethodInfo method)
			{
				this.method = method;
			}

			public override Delegate CreateDelegate(Type delegateType, object target)
			{
				return this.method.CreateDelegate(delegateType, target);
			}
		}
		
		#endregion
	}
}
