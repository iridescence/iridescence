﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements a <see cref="ICompilationUnit"/> that creates <see cref="MethodBuilder"/> instances in a <see cref="System.Reflection.Emit.TypeBuilder" />.
	/// </summary>
	public class MethodBuilderUnit : ICompilationUnit
	{
		#region Fields

		private readonly HashSet<string> names;

		#endregion

		#region Properties
		
		public TypeBuilder TypeBuilder { get; }

		public Type Type { get; private set; }

		/// <summary>
		/// Gets a value that indicates whether this compilation unit has been completed.
		/// </summary>
		public bool IsCompleted => this.Type != null;

		/// <summary>
		/// Gets a value that indicates whether tracing is enabled.
		/// </summary>
		public bool Trace { get; }

		#endregion

		#region Constructors

		public MethodBuilderUnit(TypeBuilder typeBuilder, bool trace)
		{
			this.TypeBuilder = typeBuilder ?? throw new ArgumentNullException(nameof(typeBuilder));
			this.names = new HashSet<string>();
			this.Trace = trace;
		}

		#endregion

		#region Methods

		public CompilableEmitter CreateMethod(MethodSignature signature, string name, bool withExternalValues = true)
		{
			if (!this.names.Add(name))
				throw new ArgumentException("A method with the same name already exists within this unit.", nameof(name));

			MethodILBuilder ilBuilder = MethodILBuilder.Create(
				signature,
				this.TypeBuilder,
				name,
				MethodAttributes.Static | MethodAttributes.Public | MethodAttributes.HideBySig,
				CallingConventions.Standard,
				ILBuilderOptions.Default | ILBuilderOptions.AllowUnreachable);

			return new MethodBuilderEmitter(this, ilBuilder, withExternalValues ? new List<ICapturedValue>() : null, this.Trace);
		}

		internal event EventHandler Completing;

		internal event EventHandler Completed;

		public void Complete()
		{
			if (this.Type != null)
				throw new CompilationCompletedException();

			this.Completing?.Invoke(this, EventArgs.Empty);

			this.Type = this.TypeBuilder.CreateTypeInfo();

			this.Completed?.Invoke(this, EventArgs.Empty);
		}

		#endregion
	}
}