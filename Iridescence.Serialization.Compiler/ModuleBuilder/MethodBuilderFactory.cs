﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Serialization.Compiler
{
	public class MethodBuilderFactory : ICompilationUnitFactory
	{
		#region Fields

		private readonly ModuleBuilder module;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value that indicates whether tracing is enabled.
		/// </summary>
		public bool Trace { get; }

		#endregion

		#region Constructors

		public MethodBuilderFactory(ModuleBuilder module, bool trace = false)
		{
			this.module = module ?? throw new ArgumentNullException(nameof(module));
			this.Trace = Trace;
		}

		#endregion

		#region Methods

		public ICompilationUnit CreateUnit()
		{
			string typeName = Guid.NewGuid().ToString();
			TypeBuilder typeBuilder = this.module.DefineType(typeName, TypeAttributes.Class | TypeAttributes.Sealed);

			return new MethodBuilderUnit(typeBuilder, this.Trace);
		}

		#endregion
	}
}
