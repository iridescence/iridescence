﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Forwards a deferred callback after calling an <see cref="AugmentationDelegate{TStream,TObj}"/>.
	/// </summary>
	internal sealed class AugmentationCallbackForwarder<TStream, TObj, TState>
	{
		#region Fields

		private readonly TStream stream;
		private readonly AugmentationDelegate<TStream, TObj> augmentations;
		private readonly DeferredCallback<TObj, TState> originalCallback;

		#endregion

		#region Constructors

		public AugmentationCallbackForwarder(TStream stream, AugmentationDelegate<TStream, TObj> augmentations, DeferredCallback<TObj, TState> originalCallback)
		{
			this.stream = stream;
			this.augmentations = augmentations;
			this.originalCallback = originalCallback;
		}

		#endregion

		#region Methods

		public void Invoke(TObj obj, TState state)
		{
			this.augmentations(this.stream, ref obj);
			this.originalCallback(obj, state);
		}

		#endregion
	}
}