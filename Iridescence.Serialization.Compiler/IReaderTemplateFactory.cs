﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Factory for <see cref="ReaderTemplate">reader templates</see>.
	/// </summary>
	public interface IReaderTemplateFactory
	{
		ReaderTemplate CreateTemplate(SerializationMember member, SerializationMode mode, bool forceDeferred = false);
	}
}