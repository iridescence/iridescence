﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents a template for a reader method.
	/// </summary>
	public class ReaderTemplate : Template
	{
		#region Properties
		/// <summary>
		/// Gets the <see cref="IReaderTemplateFactory"/> that created this template.
		/// </summary>
		public IReaderTemplateFactory TemplateFactory { get; }

		public Type InputType => this.InputEmitterFactory.InputType;

		public InputEmitterFactory InputEmitterFactory { get; }

		/// <summary>
		/// Gets the body build steps.
		/// </summary>
		public IReaderBody Body { get; set; }

		/// <summary>
		/// Gets a value that indicates whether the method has any body build steps.
		/// </summary>
		public bool HasBody => this.Body != null;

		/// <summary>
		/// Gets a value that indicates whether this template is forced to be deferred.
		/// </summary>
		public bool IsForcedDeferred { get; }

		/// <summary>
		/// Gets a value that indicates whether this template is deferred.
		/// </summary>
		public bool IsDeferred => this.IsForcedDeferred || this.Body.IsDeferred;

		/// <summary>
		/// Gets the total number of bytes that have to be acquired.
		/// </summary>
		public int ByteSize => this.Body?.ByteSize ?? 0;

		#endregion

		#region Constructors

		private ReaderTemplate(IReaderTemplateFactory templateFactory, InputEmitterFactory inputEmitterFactory, SerializationMember memberSpec, bool forceDeferred)
			: base(memberSpec)
		{
			this.TemplateFactory = templateFactory ?? throw new ArgumentNullException(nameof(templateFactory));
			this.InputEmitterFactory = inputEmitterFactory ?? throw new ArgumentNullException(nameof(inputEmitterFactory));
			this.IsForcedDeferred = forceDeferred;
		}

		#endregion

		#region Methods

		public static ReaderTemplate Create(IReaderTemplateFactory templateFactory, InputEmitterFactory inputEmitterFactory, SerializationMember memberSpec, IEnumerable<ICompilerModule> modules, bool forceDeferred)
		{
			ReaderTemplate template = new ReaderTemplate(templateFactory, inputEmitterFactory, memberSpec, forceDeferred);

			foreach (ICompilerModule module in modules)
			{
				module.ConfigureReader(template);
			}

			if (!template.HasBody)
			{
				throw new CompilationException($"Reader for \"{memberSpec}\" doesn't have a body.");
			}

			return template;
		}

		/// <summary>
		/// Emits the body.
		/// </summary>
		public void Emit(ReaderEmitter emit, bool acquireBytes)
		{
			if (emit.Stream.IsBuffered && acquireBytes && this.ByteSize > 0)
			{
				// Reserve bytes for writer.
				emit.LoadStream();
				emit.LoadConstant(this.ByteSize);
				emit.Stream.Acquire();
			}

			if (!this.Body.CustomAugmentationHandling)
			{
				AugmentationCollection augs = new AugmentationCollection(this);
				if (emit is ReaderEmitterDirect direct)
				{
					AugmentationEmitter augStack = new AugmentationEmitter(direct);	
					augs.Emit(augStack, AugmentationPosition.BeforeBody);
				
					this.Body.Emit(emit);

					augs.Emit(augStack, AugmentationPosition.AfterBody, AugmentationPosition.AfterBodyDeferred);
				}
				else if (emit is ReaderEmitterDeferred deferred)
				{
					// Deferred augmentations are a bit tricky. Before the body we can't really do anything because we have no object to work with.
					// Augmentations will simply not be able to load the value onto the stack.
					AugmentationEmitter augStack = new AugmentationEmitter(emit, null);	
					augs.Emit(augStack, AugmentationPosition.BeforeBody);

					if (this.Augmentations.Any(aug => aug.Position == AugmentationPosition.AfterBodyDeferred))
					{
						// The AfterBody augmentations however do get an object, but it's passed to the DeferredCallback.
						// So we actually have to run the augmentations inside the callback. To do this, we compile them into a standalone method.
						ICapturedValue compiledAugs = EmittedAugmentations.Compile(emit.Unit, emit.Stream.Type, this.Specification.Type, e =>
						{
							augs.Emit(e, AugmentationPosition.AfterBodyDeferred);
						});

						Type forwarderType = typeof(AugmentationCallbackForwarder<,,>).MakeGenericType(emit.Stream.Type, this.Specification.Type, deferred.StateType);
						using (Local forwarder = emit.DeclareLocal(forwarderType, "fwd"))
						using (Local forwarderCallback = emit.DeclareLocal(deferred.CallbackType, "fwd_del"))
						{
							// We then create an instance of the CallbackForwarder class with the relevant information.
							emit.LoadStream();
							emit.LoadCapturedValue(compiledAugs);
							deferred.LoadCallback();

							ConstructorInfo ctor = forwarderType.GetConstructor(new[] {emit.Stream.Type, compiledAugs.Type, deferred.CallbackType});
							emit.NewObject(ctor);
							emit.StoreLocal(forwarder);

							// Then we create a new DeferredCallback.
							emit.LoadLocal(forwarder);
							emit.LoadFunctionPointer(forwarderType.GetMethod("Invoke"));
							emit.NewObject(deferred.CallbackType.GetConstructor(new[] {typeof(object), typeof(IntPtr)}));
							emit.StoreLocal(forwarderCallback);

							// After this we can emit the body as usual, but instead of calling the original callback, it will call
							// our forwarder, which will run the augmentations first, then continue with the original callback.
							ReaderEmitterDeferred redirected = deferred.WithDeferred(
								deferred.CallbackType,
								() => { emit.LoadLocal(forwarderCallback); },
								deferred.LoadState);

							this.Body.Emit(redirected);
						}
					}
					else
					{
						this.Body.Emit(emit);	
					}

					augs.Emit(augStack, AugmentationPosition.AfterBody);
				}
				else
				{
					throw new NotSupportedException();
				}
			}
			else
			{
				this.Body.Emit(emit);
			}
		}

		#endregion
	}
}