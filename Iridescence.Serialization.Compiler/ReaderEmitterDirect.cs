﻿using System;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Reader-specific stack operations for non-deferred methods.
	/// </summary>
	public class ReaderEmitterDirect : ReaderEmitter
	{
		#region Fields

		private readonly StoreFunc store;
		private readonly LoadFunc load;

		#endregion

		#region Properties

		public LoadFunc Load => this.load;

		#endregion

		#region Constructors

		public ReaderEmitterDirect(EmitterBase emit, IReaderCompiler readerCompiler, IInputEmitterFactory inputEmitterFactory, LoadFunc load, StoreFunc store)
			: base(emit, readerCompiler, inputEmitterFactory)
		{
			this.store = store;
			this.load = load;
		}

		public ReaderEmitterDirect(EmitterBase emit, IReaderCompiler readerCompiler, IInputEmitterFactory inputEmitterFactory, bool isValueType, ReaderEmitterDirect parent, FieldInfo field)
			: base(emit, readerCompiler, inputEmitterFactory)
		{
			this.store = () =>
			{
				if (isValueType)
				{
					parent.LoadValueAddress();
				}
				else
				{
					parent.LoadValue();
				}

				return new StoreEnd(() =>
				{
					emit.StoreField(field);
				});
			};

			this.load = mode =>
			{
				if (isValueType)
				{
					parent.LoadValueAddress();
				}
				else
				{
					parent.LoadValue();
				}

				if (mode == LoadMode.Address)
				{
					emit.LoadFieldAddress(field);
				}
				else
				{
					emit.LoadField(field);
				}
			};
		}

		internal ReaderEmitterDirect(ReaderEmitter other, LoadFunc load, StoreFunc store)
			: base(other)
		{
			this.store = store;
			this.load = load;
		}

		#endregion

		#region Methods

		public override StoreEnd Store()
		{
			return this.store();
		}

		/// <summary>
		/// Loads the member value onto the stack.
		/// </summary>
		public void LoadValue()
		{
			if (this.load == null)
				throw new InvalidOperationException("Value can not be loaded.");

			this.load(LoadMode.Value);
		}

		/// <summary>
		/// Loads the address to the member value onto the stack.
		/// </summary>
		public void LoadValueAddress()
		{
			if (this.load == null)
				throw new InvalidOperationException("Value can not be loaded.");

			this.load(LoadMode.Address);
		}
		
		#endregion
	}
}