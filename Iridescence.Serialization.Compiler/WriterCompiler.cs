﻿using System;
using System.Collections.Generic;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	public class WriterCompiler : IWriterCompiler, IWriterTemplateFactory
	{
		#region Fields

		private readonly Dictionary<MemberCacheKey, IDelegatePairTuple> cache;
		private readonly OutputEmitterFactory outputEmitterFactory;
		private readonly Type nonGenericDelegateType;

		#endregion

		#region Properties

		public CompilerConfiguration Configuration { get; }

		public Type OutputType => this.outputEmitterFactory.OutputType;

		#endregion

		#region Constructors

		public WriterCompiler(CompilerConfiguration configuration, Type outputType)
		{
			this.Configuration = configuration;
			this.outputEmitterFactory = new OutputEmitterFactory(configuration.IOModule, outputType);

			foreach (IContentModule module in configuration.ContentModules)
			{
				module.CheckWriterCompatibility(this.OutputType);
			}

			foreach (IMemberModule module in configuration.MemberModules)
			{
				module.CheckWriterCompatibility(this.OutputType);
			}

			this.cache = new Dictionary<MemberCacheKey, IDelegatePairTuple>();

			this.nonGenericDelegateType = typeof(ObjectWriter<,>).MakeGenericType(this.OutputType, typeof(object));
		}

		#endregion

		#region Methods

		WriterTemplate IWriterTemplateFactory.CreateTemplate(SerializationMember memberSpec, SerializationMode mode)
		{
			if (mode == SerializationMode.Member)
				return WriterTemplate.Create(this, this.outputEmitterFactory, memberSpec, this.Configuration.MemberModules);

			// Check whether we can actually serialize that type.
			this.Configuration.Arbiter?.VerifyType(memberSpec.Type);
			return WriterTemplate.Create(this, this.outputEmitterFactory, memberSpec, this.Configuration.ContentModules);
		}

		public IDelegatePairTuple For(SerializationMember memberSpec)
		{
			MemberCacheKey key = new MemberCacheKey(memberSpec);

			lock (this.cache)
			{
				if (!this.cache.TryGetValue(key, out IDelegatePairTuple tuple))
				{
					tuple = this.GetPairTuple(memberSpec);
					this.cache.Add(key, tuple);
				}

				return tuple;
			}
		}

		protected virtual IDelegatePairTuple GetPairTuple(SerializationMember memberSpec)
		{
			return new CompiledTuple(this, memberSpec);
		}

		protected virtual IDelegatePair GetPair(SerializationMember memberSpec, SerializationMode mode)
		{
			WriterTemplate template = ((IWriterTemplateFactory)this).CreateTemplate(memberSpec, mode);
			Type delType = typeof(ObjectWriterRef<,>).MakeGenericType(this.OutputType, memberSpec.Type);

			// Compile.
			ICompilationUnit unit = this.Configuration.CompilerFactory.CreateUnit();
			string name = $"{memberSpec.Type.GetDisplayName()}.Write{mode}";
			CompilableEmitter compilable = unit.CreateMethod(MethodSignature.FromDelegate(delType).WithThis(typeof(EmittedMethod)), name);

			// Emit template.
			WriterEmitter writerEmit = new WriterEmitter(compilable, this, this.outputEmitterFactory, LoadStore.LoadArgument(compilable, template.Specification.Type.MakeByRefType(), 2));
			template.Emit(writerEmit, true);
			compilable.Return();
			unit.Complete();

			return new CompiledPair(this, memberSpec, mode, template.MaskingInfo, compilable.GetCompiledMethod(), compilable.CompileCapturedValues());
		}

		protected virtual Delegate GetNonGenericWrapper(SerializationMember memberSpec, SerializationMode mode)
		{
			// Get underlying generic delegate.
			IDelegatePairTuple tuple = this.For(memberSpec);
			IDelegatePair pair = mode == SerializationMode.Content ? tuple.Content : tuple.Member;

			CallableMethod method;
			object target;
			if (pair is CompiledPair p)
			{
				method = p.Method.Callable;
				target = p;
			}
			else
			{
				method = pair.Generic.GetType().GetInvokeMethod();
				target = pair.Generic;
			}

			// Compile non-generic wrapper around it.
			Type type = memberSpec.Type;
			ICompilationUnit unit = this.Configuration.CompilerFactory.CreateUnit();
			string name = $"{memberSpec.Type.GetDisplayName()}.Write{mode}NonGeneric";
			MethodSignature sig = MethodSignature.FromDelegate(this.nonGenericDelegateType);
			if (target != null) sig = sig.WithThis(target.GetType());
			CompilableEmitter emit = unit.CreateMethod(sig, name, withCapturedValues: false);

			// T temp = (T)obj;
			Local unboxingLocal = emit.DeclareLocal(type, "temp");
			emit.LoadArgument(2);
			emit.UnboxAny(type);
			emit.StoreLocal(unboxingLocal);

			// Call generic method.
			emit.LoadArgument(0);
			emit.LoadArgument(1);
			emit.LoadLocalAddress(unboxingLocal);

			if(method.Method.IsStatic)
				emit.Call(method);
			else
				emit.CallVirtual(method);
			
			emit.Return();

			unit.Complete();
			return emit.GetCompiledMethod().CreateDelegate(this.nonGenericDelegateType, target);
		}
		
		#endregion

		#region Nested Types

		private sealed class CompiledTuple : IDelegatePairTuple
		{
			private readonly WriterCompiler compiler;
			private readonly SerializationMember memberSpec;
			private IDelegatePair contentWriter;
			private IDelegatePair memberWriter;
			
			public IDelegatePair Content => this.contentWriter ??= this.compiler.GetPair(this.memberSpec, SerializationMode.Content);
			public IDelegatePair Member => this.memberWriter ??= this.compiler.GetPair(this.memberSpec, SerializationMode.Member);
			
			public CompiledTuple(WriterCompiler compiler, SerializationMember memberSpec)
			{
				this.compiler = compiler;
				this.memberSpec = memberSpec;
			}
		}

		private sealed class CompiledPair : EmittedMethod, IDelegatePair, IMaskable
		{
			private readonly WriterCompiler compiler;
			private readonly SerializationMember memberSpec;
			public readonly CompiledMethod Method;
			private readonly SerializationMode mode;
			private Delegate genericDelegate;
			private Delegate nonGenericDelegate;

			public Delegate Generic => this.genericDelegate ??=
				this.Method.CreateDelegate(typeof(ObjectWriterRef<,>).MakeGenericType(this.compiler.OutputType, this.memberSpec.Type), this);

			public Delegate NonGeneric => this.nonGenericDelegate ??=
				this.compiler.GetNonGenericWrapper(this.memberSpec, this.mode);

			public MaskingInfo MaskingInfo { get; }

			public CompiledPair(WriterCompiler compiler, SerializationMember memberSpec, SerializationMode mode, MaskingInfo maskingInfo, CompiledMethod method, object[] capturedValues)
				: base(capturedValues)
			{
				this.compiler = compiler;
				this.memberSpec = memberSpec;
				this.mode = mode;
				this.MaskingInfo = maskingInfo;
				this.Method = method;
			}
		}

		#endregion
	}
}