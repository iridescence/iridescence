﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents a delegate for a standalone compiled <see cref="IAugmentation"/> method.
	/// </summary>
	/// <typeparam name="TStream">The type of the IO stream.</typeparam>
	/// <typeparam name="TObj">The type of the object.</typeparam>
	/// <param name="stream"></param>
	/// <param name="obj"></param>
	public delegate void AugmentationDelegate<in TStream, TObj>(TStream stream, ref TObj obj);

}
