﻿using System;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Reader-specific stack operations for deferred methods.
	/// </summary>
	public class ReaderEmitterDeferred : ReaderEmitter
	{
		#region Fields

		private readonly Action loadDeferredCallback;
		private readonly Action loadDeferredState;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the type of the deferred callback delegate.
		/// </summary>
		public Type CallbackType { get; }

		/// <summary>
		/// Gets the type of the deferred callback argument.
		/// </summary>
		public Type StateType { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ReaderEmitterDeferred"/> class. 
		/// </summary>
		public ReaderEmitterDeferred(EmitterBase emit, IReaderCompiler readerCompiler, IInputEmitterFactory inputEmitterFactory, Type callbackType, Action loadDeferredCallback, Action loadDeferredState)
			: base(emit, readerCompiler, inputEmitterFactory)
		{
			this.loadDeferredCallback = loadDeferredCallback ?? throw new ArgumentNullException(nameof(loadDeferredCallback));
			this.loadDeferredState = loadDeferredState ?? throw new ArgumentNullException(nameof(loadDeferredState));

			if (callbackType == null)
				throw new ArgumentNullException(nameof(callbackType));
		
			if (!callbackType.IsGenericType)
				throw new ArgumentException("Callback type is not a DeferredCallback<T, TState>.", nameof(callbackType));

			Type callbackDef = callbackType.GetGenericTypeDefinition();
			if (callbackDef != typeof(DeferredCallback<,>))
				throw new ArgumentException("Callback type is not a DeferredCallback<T, TState>.", nameof(callbackType));

			this.CallbackType = callbackType;
			this.StateType = callbackType.GetGenericArguments()[1];	
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ReaderEmitterDeferred"/> class. 
		/// </summary>
		internal ReaderEmitterDeferred(ReaderEmitter other, Type callbackType, Action loadDeferredCallback, Action loadDeferredState)
			: base(other)
		{
			this.loadDeferredCallback = loadDeferredCallback ?? throw new ArgumentNullException(nameof(loadDeferredCallback));
			this.loadDeferredState = loadDeferredState ?? throw new ArgumentNullException(nameof(loadDeferredState));

			if (callbackType == null)
				throw new ArgumentNullException(nameof(callbackType));

			if (!callbackType.IsGenericType)
				throw new ArgumentException("Callback type is not a DeferredCallback<T, TState>.", nameof(callbackType));

			Type callbackDef = callbackType.GetGenericTypeDefinition();
			if (callbackDef != typeof(DeferredCallback<,>))
				throw new ArgumentException("Callback type is not a DeferredCallback<T, TState>.", nameof(callbackType));

			this.CallbackType = callbackType;
			this.StateType = callbackType.GetGenericArguments()[1];
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Stores the value by invoking the deferred callback with it.
		/// </summary>
		/// <returns></returns>
		public override StoreEnd Store()
		{
			this.LoadCallback();

			return new StoreEnd(() =>
			{
				this.LoadState();
				this.InvokeDelegate(this.CallbackType);
			});
		}

		/// <summary>
		/// Loads the deferred callback onto the stack.
		/// </summary>
		public void LoadCallback()
		{
			this.loadDeferredCallback();
		}

		/// <summary>
		/// Loads the deferred callback argument onto the stack.
		/// </summary>
		public void LoadState()
		{
			this.loadDeferredState();
		}
		
		#endregion
	}
}