﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Holds the configuration for a serialization compiler.
	/// </summary>
	public class CompilerConfiguration
	{
		#region Fields
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the <see cref="ISerializationArbiter"/> that decides which types are serializable.
		/// </summary>
		public ISerializationArbiter Arbiter { get; private set; }

		/// <summary>
		/// Gets the <see cref="ICompilationUnitFactory"/> that creates the compilation units used during build.
		/// </summary>
		public ICompilationUnitFactory CompilerFactory { get; private set; }

		/// <summary>
		/// Gets the <see cref="IIOModule"/> that creates the stream emit implementations.
		/// </summary>
		public IIOModule IOModule { get; private set; }
		
		/// <summary>
		/// Gets the list of content modules.
		/// </summary>
		public ImmutableList<IContentModule> ContentModules { get; private set; }

		/// <summary>
		/// Gets the list of member modules.
		/// </summary>
		public ImmutableList<IMemberModule> MemberModules { get; private set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CompilerConfiguration"/> class. 
		/// </summary>
		public CompilerConfiguration()
		{
			this.IOModule = StreamIOModule.Instance;
			this.CompilerFactory = DynamicMethodFactory.Instance;
			this.ContentModules = ImmutableList<IContentModule>.Empty;
			this.MemberModules = ImmutableList<IMemberModule>.Empty;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CompilerConfiguration"/> class. 
		/// </summary>
		public CompilerConfiguration(IEnumerable<IContentModule> contentModules, IEnumerable<IMemberModule> memberModules)
		{
			this.IOModule = StreamIOModule.Instance;
			this.CompilerFactory = DynamicMethodFactory.Instance;
			this.ContentModules = ImmutableList.CreateRange(contentModules);
			this.MemberModules = ImmutableList.CreateRange(memberModules);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CompilerConfiguration"/> class. 
		/// </summary>
		private CompilerConfiguration(CompilerConfiguration configuration)
		{
			this.Arbiter = configuration.Arbiter;
			this.CompilerFactory = configuration.CompilerFactory;
			this.IOModule = configuration.IOModule;
			this.ContentModules = configuration.ContentModules;
			this.MemberModules = configuration.MemberModules;
		}

		#endregion

		#region Methods

		public CompilerConfiguration WithArbiter(ISerializationArbiter arbiter)
		{
			if (arbiter == null)
				throw new ArgumentNullException(nameof(arbiter));

			return new CompilerConfiguration(this) { Arbiter = arbiter };
		}

		public CompilerConfiguration WithCompilerFactory(ICompilationUnitFactory factory)
		{
			if (factory == null)
				throw new ArgumentNullException(nameof(factory));

			return new CompilerConfiguration(this) {CompilerFactory = factory};
		}

		public CompilerConfiguration WithIOModule(IIOModule module)
		{
			if (module == null)
				throw new ArgumentNullException(nameof(module));

			return new CompilerConfiguration(this) {IOModule = module};
		}

		/// <summary>
		/// Returns a <see cref="CompilerConfiguration"/> with the specified <see cref="IContentModule"/> added.
		/// </summary>
		/// <param name="module"></param>
		/// <param name="index"></param>
		/// <returns></returns>
		public CompilerConfiguration WithContentModule(IContentModule module, int? index = null)
		{
			if (module == null)
				throw new ArgumentNullException(nameof(module));

			CompilerConfiguration configuration = new CompilerConfiguration(this);

			if(index.HasValue)
				configuration.ContentModules = configuration.ContentModules.Insert(index.Value, module);
			else
				configuration.ContentModules = configuration.ContentModules.Add(module);

			return configuration;
		}

		/// <summary>
		/// Returns a <see cref="CompilerConfiguration"/> with the specified <see cref="IMemberModule"/> added.
		/// </summary>
		/// <param name="module"></param>
		/// <param name="index"></param>
		/// <returns></returns>
		public CompilerConfiguration WithMemberModule(IMemberModule module, int? index = null)
		{
			if (module == null)
				throw new ArgumentNullException(nameof(module));

			CompilerConfiguration configuration = new CompilerConfiguration(this);

			if (index.HasValue)
				configuration.MemberModules = configuration.MemberModules.Insert(index.Value, module);
			else
				configuration.MemberModules = configuration.MemberModules.Add(module);

			return configuration;
		}

		/// <summary>
		/// Returns a <see cref="CompilerConfiguration"/> with the specified type surrogate added.
		/// </summary>
		/// <typeparam name="TReal"></typeparam>
		/// <typeparam name="TSurrogate"></typeparam>
		/// <param name="index"></param>
		/// <returns></returns>
		public CompilerConfiguration WithSurrogate<TReal, TSurrogate>(int? index = null)
			where TSurrogate : IObjectSurrogate<TReal>
		{
			IContentModule module = new SimpleSurrogateModule(typeof(TReal), typeof(TSurrogate));
			return this.WithContentModule(module, index);
		}

		#endregion
	}
}
