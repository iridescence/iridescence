﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	internal delegate void MaskingInfoWriter<in TDest>(TDest dest, MaskingInfo info);

	internal delegate MaskingInfo MaskingInfoReader<in TSource>(TSource src);

	internal static class MaskingInfoIO
	{ 
		private static readonly MethodInfo maskGetTypeConstraintMethod = typeof(MaskingInfo).FindPropertyGetter(nameof(MaskingInfo.TypeConstraint));
		private static readonly MethodInfo maskGetAttrTypesMethod = typeof(MaskingInfo).FindPropertyGetter(nameof(MaskingInfo.MemberAttributes));
		private static readonly ConstructorInfo maskCtor = typeof(MaskingInfo).GetConstructor(new[] {typeof(Type), typeof(Type[])});
		private static readonly MethodInfo roListGetCountMethod = typeof(IReadOnlyCollection<Type>).FindPropertyGetter("Count");
		private static readonly MethodInfo roListGetItemMethod = typeof(IReadOnlyList<Type>).FindMethod("get_Item");

		private static readonly ConditionalWeakTable<object, StrongBox<CallableMethod>> readerCache = new ConditionalWeakTable<object, StrongBox<CallableMethod>>();
		private static readonly ConditionalWeakTable<object, StrongBox<CallableMethod>> writerCache = new ConditionalWeakTable<object, StrongBox<CallableMethod>>();

		private static CallableMethod compileWriter(ICompilationUnit unit, Type outputType, IOutputEmitterFactory outputEmitterFactory)
		{
			Type delType = typeof(MaskingInfoWriter<>).MakeGenericType(outputType);
			CompilableEmitter emit = unit.CreateMethod(MethodSignature.FromDelegate(delType), delType.GetDisplayName());
			IOutputEmitter stream = outputEmitterFactory.CreateOutputEmitter(emit);

			// Write constraint so that the reader know what to look for.
			emit.LoadArgument(0);
			emit.LoadArgument(1);
			emit.CallVirtual(maskGetTypeConstraintMethod);
			stream.WriteType();

			Local listLocal = emit.DeclareLocal(typeof(IReadOnlyList<Type>), "attrs");
			Local countLocal = emit.DeclareLocal(typeof(int), "attrCount");
			Local indexLocal = emit.DeclareLocal(typeof(int), "attrIndex");
			Label startLabel = emit.DefineLabel("attrLoopStart");
			Label endLabel = emit.DefineLabel("attrLoopEnd");

			// list = mask.MemberAttributes;
			emit.LoadArgument(1);
			emit.CallVirtual(maskGetAttrTypesMethod);
			emit.StoreLocal(listLocal);

			// count = list.Count;
			emit.LoadLocal(listLocal);
			emit.CallVirtual(roListGetCountMethod);
			emit.StoreLocal(countLocal);

			// WriteVarInt(count)
			emit.LoadArgument(0);
			emit.LoadLocal(countLocal);
			emit.ConvertToInt8OverflowUnsigned();
			stream.WriteUInt8();

			// for(int i = 0; i < count; ++i)
			emit.LoadConstant<int>(0);
			emit.StoreLocal(indexLocal);
			emit.Branch(endLabel);
			emit.MarkLabel(startLabel);

			// stream.WriteType(list[i]);
			emit.LoadArgument(0);
			emit.LoadLocal(listLocal);
			emit.LoadLocal(indexLocal);
			emit.CallVirtual(roListGetItemMethod);
			stream.WriteType();

			// ++i;
			emit.LoadLocal(indexLocal);
			emit.LoadConstant<int>(1);
			emit.Add();
			emit.StoreLocal(indexLocal);

			// if(i < count) continue;
			emit.MarkLabel(endLabel);
			emit.LoadLocal(indexLocal);
			emit.LoadLocal(countLocal);
			emit.BranchIfLess(startLabel);

			emit.Return();

			return emit.Method;
		}

		private static CallableMethod compileReader(ICompilationUnit unit, Type inputType, IInputEmitterFactory inputEmitterFactory)
		{
			Type delType = typeof(MaskingInfoReader<>).MakeGenericType(inputType);
			CompilableEmitter emit = unit.CreateMethod(MethodSignature.FromDelegate(delType), delType.GetDisplayName());
			IInputEmitter stream = inputEmitterFactory.CreateInputEmitter(emit);
			
			// Read constraint type.
			Local typeContraintLocal = emit.DeclareLocal(typeof(Type), "constraint");
			emit.LoadArgument(0);
			stream.ReadType();
			emit.StoreLocal(typeContraintLocal);

			// Read count.
			Local countLocal = emit.DeclareLocal(typeof(int), "attrCount");
			emit.LoadArgument(0);
			stream.ReadUInt8();
			emit.ConvertToInt32();
			emit.StoreLocal(countLocal);

			// Create array for member attribute types.
			Local arrayLocal = emit.DeclareLocal(typeof(Type[]), "attrTypes");
			emit.LoadLocal(countLocal);
			emit.NewArray(typeof(Type));
			emit.StoreLocal(arrayLocal);

			Label startLabel = emit.DefineLabel("attrLoopStart");
			Label endLabel = emit.DefineLabel("attrLoopEnd");

			// for(int i = 0; i < count; ++i)
			Local indexLocal = emit.DeclareLocal(typeof(int), "attrIndex");
			emit.LoadConstant<int>(0);
			emit.StoreLocal(indexLocal);
			emit.Branch(endLabel);
			emit.MarkLabel(startLabel);

			// stream.WriteType(list[i]);
			emit.LoadLocal(arrayLocal);
			emit.LoadLocal(indexLocal);
			emit.LoadArgument(0);
			stream.ReadType();
			emit.StoreElement(typeof(Type));

			// ++i;
			emit.LoadLocal(indexLocal);
			emit.LoadConstant<int>(1);
			emit.Add();
			emit.StoreLocal(indexLocal);

			// if(i < count) continue;
			emit.MarkLabel(endLabel);
			emit.LoadLocal(indexLocal);
			emit.LoadLocal(countLocal);
			emit.BranchIfLess(startLabel);

			emit.LoadLocal(typeContraintLocal);
			emit.LoadLocal(arrayLocal);
			emit.NewObject(maskCtor);
			emit.Return();

			return emit.Method;
		}

		public static CallableMethod GetWriter(object cacheKey, ICompilationUnit unit, Type outputType, IOutputEmitterFactory outputEmitterFactory)
		{
			return writerCache.GetValue(cacheKey, _ => new StrongBox<CallableMethod>(compileWriter(unit, outputType, outputEmitterFactory))).Value;
		}

		public static CallableMethod GetReader(object cacheKey, ICompilationUnit unit, Type inputType, IInputEmitterFactory inputEmitterFactory)
		{
			return readerCache.GetValue(cacheKey, _ => new StrongBox<CallableMethod>(compileReader(unit, inputType, inputEmitterFactory))).Value;
		}
	}
}
