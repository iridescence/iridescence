namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Interfaces for a <see cref="BufferedInputStream"/> to support <see cref="ReferencingMemberModule"/>.
	/// </summary>
	/// <typeparam name="TIdentifier"></typeparam>
	public interface IReferencingInputStream<TIdentifier> : IDeferredReferenceResolver<object, TIdentifier>
	{
		/// <summary>
		/// Reads an identifier from the stream.
		/// </summary>
		/// <returns></returns>
		TIdentifier ReadIdentifier();
	}
}