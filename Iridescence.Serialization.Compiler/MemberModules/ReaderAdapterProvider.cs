﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Generates adapters/conversion wrappers for <see cref="ObjectReaderDeferred{TSource,TResult,TState}"/>.
	/// </summary>
	internal sealed class ReaderAdapterProvider
	{
		#region Fields


		private readonly ConcurrentDictionary<Key, CompiledMethod> cache;
		private readonly ICompilationUnitFactory factory;

		#endregion

		#region Constructors

		public ReaderAdapterProvider(ICompilationUnitFactory factory = null)
		{
			this.cache = new ConcurrentDictionary<Key, CompiledMethod>();
			this.factory = factory ?? DynamicMethodFactory.Instance;
		}

		#endregion

		#region Methods

		public ObjectReaderDeferred<TSource, TResult, TState> Get<TSource, TResult, TState>(Type originalResultType, Delegate original)
		{
			if (original is ObjectReaderDeferred<TSource, TResult, TState> adapter)
				return adapter;

			Key k = new Key(typeof(TSource), typeof(TResult), typeof(TState), originalResultType);
			return (ObjectReaderDeferred<TSource, TResult, TState>)this.cache
				.GetOrAdd(k, this.create)
				.CreateDelegate(typeof(ObjectReaderDeferred<TSource, TResult, TState>), original);
		}

		private CompiledMethod create(Key k)
		{
			Type outDelType = typeof(ObjectReaderDeferred<,,>).MakeGenericType(k.Source, k.Result, k.State);
			Type inDelType = typeof(ObjectReaderDeferred<,,>).MakeGenericType(k.Source, k.ResultOriginal, k.State);

			ICompilationUnit unit = this.factory.CreateUnit();

			CompilableEmitter emit = unit.CreateMethod(
				MethodSignature.FromDelegate(outDelType).WithThis(inDelType),
				$"ReaderAdapter<{k.ResultOriginal.GetDisplayName()}>",
				withCapturedValues: false);

			Type callbackAdapter = typeof(CallbackAdapter<,,>).MakeGenericType(k.ResultOriginal, k.Result, k.State);
			Type callbackDelType = typeof(DeferredCallback<,>).MakeGenericType(k.Result, k.State);
			ConstructorInfo adapterCtor = callbackAdapter.GetConstructor(new[] {callbackDelType});
			FieldInfo delegateField = callbackAdapter.GetField("Delegate");

			emit.LoadArgument(0);
			emit.LoadArgument(1);

			emit.LoadArgument(2);
			emit.NewObject(adapterCtor);
			emit.LoadField(delegateField);

			emit.LoadArgument(3);
			emit.InvokeDelegate(inDelType);

			emit.Return();

			unit.Complete();
			return emit.GetCompiledMethod();
		}

		#endregion

		#region Nested Types

		private readonly struct Key : IEquatable<Key>
		{
			public readonly Type Source;
			public readonly Type Result;
			public readonly Type State;
			public readonly Type ResultOriginal;

			public Key(Type source, Type result, Type state, Type resultOriginal)
			{
				this.Source = source;
				this.Result = result;
				this.State = state;
				this.ResultOriginal = resultOriginal;
			}

			public bool Equals(Key other)
			{
				return this.Source == other.Source &&
				       this.Result == other.Result &&
				       this.State == other.State && 
				       this.ResultOriginal == other.ResultOriginal;
			}

			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				return obj is Key other && this.Equals(other);
			}

			public override int GetHashCode()
			{
				return HashCode.Combine(this.Source, this.Result.Name, this.State, this.ResultOriginal);
			}
		}

		#endregion
	}
}
