﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Member module that inlines the content of a member.
	/// Applies to all value types if no filter is specified.
	/// </summary>
	public class InlineMemberModule : IMemberModule
	{
		#region Fields

		private readonly Predicate<SerializationMember> memberFilter;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="InlineMemberModule"/> class. 
		/// </summary>
		public InlineMemberModule()
			: this(m => m.Type.IsValueType)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InlineMemberModule"/> class. 
		/// </summary>
		public InlineMemberModule(Predicate<SerializationMember> memberFilter)
		{
			this.memberFilter = memberFilter;
		}

		#endregion

		#region Methods

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody || !this.memberFilter(template.Specification))
				return;

			template.Body = new WriterStep(template.TemplateFactory.CreateTemplate(template.Specification, SerializationMode.Content));
		}

		public void CheckWriterCompatibility(Type outputType)
		{

		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.HasBody || !this.memberFilter(template.Specification))
				return;

			template.Body = new ReaderStep(template.TemplateFactory.CreateTemplate(template.Specification, SerializationMode.Content, template.IsForcedDeferred));
		}

		public void CheckReaderCompatibility(Type inputType)
		{

		}

		#endregion

		#region Nested Types

		private sealed class WriterStep : IWriterBody
		{
			private readonly WriterTemplate template;

			public int ByteSize => this.template.ByteSize;

			public WriterStep(WriterTemplate template)
			{
				this.template = template;
			}

			public void Emit(WriterEmitter emit)
			{
				this.template.Emit(emit, false);
			}
		}

		private sealed class ReaderStep : IReaderBody
		{
			private readonly ReaderTemplate template;

			public int ByteSize => this.template.ByteSize;

			public bool IsDeferred => this.template.IsDeferred;

			public bool CustomAugmentationHandling => false;

			public ReaderStep(ReaderTemplate template)
			{
				this.template = template;
			}

			public void Emit(ReaderEmitter emit)
			{
				this.template.Emit(emit, false);
			}
		}

		#endregion
	}
}
