﻿using System;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements a member writing module that stores object instances through references.
	/// The stream class used at runtime must implement the <see cref="IReferencingOutputStream{TIdentifier}"/> interface.
	/// </summary>
	public class ReferencingMemberModule : IMemberModule
	{
		#region Properties

		/// <summary>
		/// Gets the type of the identifiers.
		/// </summary>
		public Type IdentifierType { get; }

		/// <summary>
		/// Gets a value that indicates whether the "wasAdded" parameter from the <see cref="IReferenceProvider{T,TIdentifier}"/> is stored in the output.
		/// </summary>
		public bool StoreAddFlag { get; }

		/// <summary>
		/// Gets a value that indicates whether the module handles cases where a boxed value type could be referenced.
		/// </summary>
		public bool HandleBoxedValueTypes { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ReferencingMemberModule"/> class. 
		/// </summary>
		/// <param name="identifierType">The type of the object identifier.</param>
		/// <param name="storeAddFlag">Whether the module stores the "wasAdded" returned from the reference provider.</param>
		/// <param name="handleBoxedValueTypes">
		/// Whether the module should make sure that value types are never added to/retreived from the reference provider.
		/// This can be the for members of type "object" or interface types.
		/// </param>
		public ReferencingMemberModule(Type identifierType, bool storeAddFlag = false, bool handleBoxedValueTypes = true)
		{
			this.IdentifierType = identifierType ?? throw new ArgumentNullException(nameof(identifierType));
			this.StoreAddFlag = storeAddFlag;
			this.HandleBoxedValueTypes = handleBoxedValueTypes;
		}

		#endregion

		#region Methods
		
		public void CheckReaderCompatibility(Type sourceType)
		{
			Type iface = typeof(IReferencingInputStream<>).MakeGenericType(this.IdentifierType);
			if (!iface.IsAssignableFrom(sourceType))
				throw new ArgumentException($"Source class must implement {iface.GetDisplayName()} in order to use {nameof(MetadataContentModule)}.", nameof(sourceType));
		}

		public void CheckWriterCompatibility(Type destType)
		{
			Type iface = typeof(IReferencingOutputStream<>).MakeGenericType(this.IdentifierType);
			if (!iface.IsAssignableFrom(destType))
				throw new ArgumentException($"Dest class must implement {iface.GetDisplayName()} in order to use {nameof(MetadataContentModule)}.", nameof(destType));
		}

		public void ConfigureWriter(WriterTemplate template)
		{
			if (template.HasBody)
				return;

			ReferencingBodyFlags flags = 0;

			if (!template.Specification.TreatAsValue)
				flags |= ReferencingBodyFlags.IsReferenced;

			if (this.IsPolymorphic(template.Specification))
				flags |= ReferencingBodyFlags.IsPolymorphic;

			if (this.HandleBoxedValueTypes && couldBeBoxed(template.Specification))
				flags |= ReferencingBodyFlags.HandleBoxing;

			if (this.StoreAddFlag)
				flags |= ReferencingBodyFlags.StoreAddFlag;

			template.Body = new ReferencingBody(template.Specification, this.IdentifierType, flags, template.OutputEmitterFactory);
		}

		public void ConfigureReader(ReaderTemplate template)
		{
			if (template.HasBody)
				return;

			ReferencingBodyFlags flags = 0;

			if (!template.Specification.TreatAsValue)
				flags |= ReferencingBodyFlags.IsReferenced;

			if (this.IsPolymorphic(template.Specification))
				flags |= ReferencingBodyFlags.IsPolymorphic;

			if (this.HandleBoxedValueTypes && couldBeBoxed(template.Specification))
				flags |= ReferencingBodyFlags.HandleBoxing;

			if (this.StoreAddFlag)
				flags |= ReferencingBodyFlags.StoreAddFlag;

			if ((flags & ReferencingBodyFlags.IsReferenced | ReferencingBodyFlags.IsPolymorphic) == 0 && !template.IsForcedDeferred)
			{
				// If there's only one content reader due to the member not being polymorphic we can find out
				// whether the member reader needs to be deferred by looking at the content reader.
				// Otherwise we'll have to "assume the worst".
				ReaderTemplate contentReader = template.TemplateFactory.CreateTemplate(template.Specification, SerializationMode.Content);
				if (contentReader.IsDeferred)
					flags |= ReferencingBodyFlags.RequiresDefer;
			}
			else
			{
				flags |= ReferencingBodyFlags.RequiresDefer;
			}

			ReaderAdapterProvider adapterProvider = new ReaderAdapterProvider();
			template.Body = new ReferencingBody(template.Specification, this.IdentifierType, flags, template.InputEmitterFactory, adapterProvider);
		}

		/// <summary>
		/// Returns a value that determines whether the specified type can be polymorphic.
		/// This has an effect on whether a type identifier is written to the stream for instances of that type.
		/// </summary>
		/// <param name="member"></param>
		/// <returns></returns>
		protected virtual bool IsPolymorphic(SerializationMember member)
		{
			// Sealed classes are not polymorphic.
			return !member.Type.IsSealed;
		}

		private static bool couldBeBoxed(SerializationMember member)
		{
			return member.Type == typeof(object) || member.Type.IsInterface;
		}

		[Flags]
		private enum ReferencingBodyFlags
		{
			IsReferenced = 1,
			IsPolymorphic = 2,
			HandleBoxing = 4,
			StoreAddFlag = 8,
			RequiresDefer = 16,
		}

		private sealed class ReferencingBody : IWriterBody, IReaderBody
		{
			private readonly SerializationMember memberSpec;
			private readonly ReferencingBodyFlags flags;
			private readonly Type identifierType;

			private readonly OutputEmitterFactory outputEmitterFactory;
			private readonly Type writerInterfaceType;
			private readonly MethodInfo writeIdentifierMethod;
			private readonly MethodInfo getOrAddMethod;
			
			private readonly InputEmitterFactory inputEmitterFactory;
			private readonly ReaderAdapterProvider adapterProvider;
			private readonly Type readerInterfaceType;
			private readonly Type callbackStateType;
			private readonly MethodInfo readerIdentifierMethod;
			private readonly MethodInfo getMethod;

			public int ByteSize => 0;

			public bool IsDeferred => (this.flags & ReferencingBodyFlags.RequiresDefer) != 0;

			public bool CustomAugmentationHandling => false;

			private ReferencingBody(SerializationMember member, Type identifierType, ReferencingBodyFlags flags)
			{
				this.memberSpec = member;
				this.identifierType = identifierType;
				this.flags = flags;

			}
			public ReferencingBody(SerializationMember member, Type identifierType, ReferencingBodyFlags flags, OutputEmitterFactory outputEmitterFactory)
				: this(member, identifierType, flags)
			{
				this.outputEmitterFactory = outputEmitterFactory;
				Type referenceProviderType = typeof(IReferenceProvider<,>).MakeGenericType(typeof(object), identifierType);
				this.writerInterfaceType = typeof(IReferencingOutputStream<>).MakeGenericType(identifierType);
				this.writeIdentifierMethod = this.writerInterfaceType.GetMethod("WriteIdentifier") ?? throw new MissingMethodException();
				this.getOrAddMethod = referenceProviderType.GetMethod("GetOrAdd") ?? throw new MissingMethodException();
			}

			public ReferencingBody(SerializationMember member, Type identifierType, ReferencingBodyFlags flags, InputEmitterFactory inputEmitterFactory, ReaderAdapterProvider adapterProvider)
				: this(member, identifierType, flags)
			{
				this.inputEmitterFactory = inputEmitterFactory;
				this.adapterProvider = adapterProvider;
				Type referenceResolverType = typeof(IDeferredReferenceResolver<,>).MakeGenericType(typeof(object), identifierType);
				this.readerInterfaceType = typeof(IReferencingInputStream<>).MakeGenericType(identifierType);
				this.callbackStateType = typeof(SetCallbackState<>).MakeGenericType(this.identifierType);
				this.readerIdentifierMethod = this.readerInterfaceType.GetMethod("ReadIdentifier") ?? throw new MissingMethodException();
				this.getMethod = referenceResolverType.GetMethod("Get") ?? throw new MissingMethodException();
			}

			private void traceBegin(EmitterBase emit, string text)
			{
				emit.TraceBegin(new SerializationTraceInfo(this.memberSpec, emit is WriterEmitter ? SerializationDirection.Write : SerializationDirection.Read, SerializationMode.Member, text));
			}

			private void traceEnd(EmitterBase emit, string text)
			{
				emit.TraceEnd(new SerializationTraceInfo(this.memberSpec, emit is WriterEmitter ? SerializationDirection.Write : SerializationDirection.Read, SerializationMode.Member, text));
			}

			public void Emit(WriterEmitter emit)
			{
				// For reference type we retreive the identifier from reference provider and dynamically invoke the writer for an object based on its type.
				using (Local objLocal = emit.DeclareLocal(this.memberSpec.Type, "obj", false))
				using (Local idLocal = emit.DeclareLocal(this.identifierType, "identifier", false))
				using (Local wasAddedLocal = emit.DeclareLocal(typeof(bool), "wasAdded", false))
				{
					Local refProviderLocal = null;
					Local isBoxed = null;
					Label beginWrite = emit.DefineLabel("beginWrite");
					Label endWrite = emit.DefineLabel("endWrite");

					// Load member and store in local.
					emit.LoadValue();
					emit.StoreLocal(objLocal);

					if ((this.flags & ReferencingBodyFlags.IsReferenced) != 0)
					{
						if ((this.flags & ReferencingBodyFlags.HandleBoxing) != 0)
						{
							isBoxed = emit.DeclareLocal(typeof(bool), "isBoxed", false);

							// isBoxed = obj is ValueType;
							emit.LoadLocal(objLocal);
							emit.IsInstance(typeof(ValueType));
							emit.LoadNull();
							emit.CompareGreaterThanUnsigned();
							emit.StoreLocal(isBoxed);

							// stream.WriteBoolean(isBoxed)
							this.traceBegin(emit, "isBoxed");
							emit.LoadStream();
							emit.LoadLocal(isBoxed);
							emit.Stream.WriteBoolean();
							this.traceEnd(emit, "isBoxed");

							// If isBoxed goto beginWrite
							emit.LoadLocal(isBoxed);
							emit.BranchIfTrue(beginWrite);
						}

						// Referencing logic...
						refProviderLocal = emit.DeclareLocal(this.writerInterfaceType, "refProvider", false);

						// Cast context to interface and store in local.
						emit.LoadStream();
						emit.CastClass(this.writerInterfaceType);
						emit.StoreLocal(refProviderLocal);

						// Get identifier and wasAdded flag.
						emit.LoadLocal(refProviderLocal);
						emit.LoadLocal(objLocal);
						if (this.memberSpec.Type.IsValueType) emit.Box(this.memberSpec.Type);
						emit.LoadLocalAddress(wasAddedLocal);
						emit.CallVirtual(this.getOrAddMethod);
						emit.StoreLocal(idLocal);

						// Write identifier.
						this.traceBegin(emit, "identifier");
						emit.LoadLocal(refProviderLocal);
						emit.LoadLocal(idLocal);
						emit.CallVirtual(this.writeIdentifierMethod);
						this.traceEnd(emit, "identifier");

						if ((this.flags & ReferencingBodyFlags.StoreAddFlag) != 0)
						{
							// Write wasAdded flag.
							this.traceBegin(emit, "wasAdded");
							emit.LoadStream();
							emit.LoadLocal(wasAddedLocal);
							emit.Stream.WriteBoolean();
							this.traceEnd(emit, "wasAdded");
						}

						// If wasAdded goto endWrite
						emit.LoadLocal(wasAddedLocal);
						emit.BranchIfFalse(endWrite);
					}

					// Write object data.
					using (Local typeLocal = emit.DeclareLocal(typeof(Type), "memberType", false))
					{
						emit.MarkLabel(beginWrite);

						// Get type.
						emit.LoadLocal(objLocal);
						if (this.memberSpec.Type.IsValueType) emit.Box(this.memberSpec.Type);
						emit.CallVirtual(ReflectionHelper.Object_GetType);
						emit.StoreLocal(typeLocal);

						if ((this.flags & ReferencingBodyFlags.IsPolymorphic) != 0)
						{
							using (Local writerPair = emit.DeclareLocal(typeof(IDelegatePair), "writerPair"))
							using (Local maskable = emit.DeclareLocal(typeof(IMaskable), "maskable", false))
							using (Local mask = emit.DeclareLocal(typeof(MaskingInfo), "mask", false))
							{
								Label noMask = emit.DefineLabel("noMask");
								Label noMaskEnd = emit.DefineLabel("noMaskEnd");

								// writerPair = Tuple.Content
								emit.LoadPairTupleChangedType(this.memberSpec, LoadStore.LoadLocal(typeLocal));
								emit.CallVirtual(ReflectionHelper.IDelegatePairTuple_Content);
								emit.StoreLocal(writerPair);

								// maskable = writerPair as IMaskable
								emit.LoadLocal(writerPair);
								emit.IsInstance(typeof(IMaskable));
								emit.StoreLocal(maskable);

								// if(maskable == null) goto noMask;
								emit.LoadLocal(maskable);
								emit.BranchIfFalse(noMask);

								// mask = maskable.MaskingInfo;
								emit.LoadLocal(maskable);
								emit.CallVirtual(ReflectionHelper.IMaskable_GetMaskingInfo);
								emit.StoreLocal(mask);

								// if(mask == null) goto noMask;
								emit.LoadLocal(mask);
								emit.BranchIfFalse(noMask);

								// Member is masked. Write null type.
								this.traceBegin(emit, "type");
								emit.LoadStream();
								emit.LoadNull();
								emit.Stream.WriteType();
								this.traceEnd(emit, "type");

								// Write masking info.
								this.traceBegin(emit, "maskInfo");
								emit.LoadStream();
								emit.LoadLocal(mask);
								emit.Call(MaskingInfoIO.GetWriter(emit.WriterCompiler, emit.Unit, this.outputEmitterFactory.OutputType, this.outputEmitterFactory));
								this.traceEnd(emit, "maskInfo");

								emit.Branch(noMaskEnd);

								// Member is not masked. Write memberType.
								emit.MarkLabel(noMask);

								this.traceBegin(emit, "type");
								emit.LoadStream();
								emit.LoadLocal(typeLocal);
								emit.Stream.WriteType();
								this.traceEnd(emit, "type");

								emit.MarkLabel(noMaskEnd);

								// Get non-generic writer.
								Type writerDelegateType = typeof(ObjectWriter<,>).MakeGenericType(emit.Stream.Type, typeof(object));
								emit.LoadLocal(writerPair);
								emit.CallVirtual(ReflectionHelper.IDelegatePair_NonGeneric);
								emit.CastClass(writerDelegateType);

								// Invoke it.
								emit.LoadStream();
								emit.LoadLocal(objLocal);
								if (this.memberSpec.Type.IsValueType) emit.Box(this.memberSpec.Type);
								emit.InvokeDelegate(writerDelegateType);
							}
						}
						else
						{
							Type writerDelType = typeof(ObjectWriterRef<,>).MakeGenericType(emit.Stream.Type, this.memberSpec.Type);

							// Load content writer delegate.
							emit.LoadPairTuple(this.memberSpec);
							emit.CallVirtual(ReflectionHelper.IDelegatePairTuple_Content);
							emit.CallVirtual(ReflectionHelper.IDelegatePair_Generic);
							emit.CastClass(writerDelType);

							// Invoke it.
							emit.LoadStream();
							emit.LoadLocalAddress(objLocal);
							emit.InvokeDelegate(writerDelType);
						}

						emit.MarkLabel(endWrite);
					}

					refProviderLocal?.Dispose();
					isBoxed?.Dispose();
				}
			}

			public void Emit(ReaderEmitter emit)
			{
				void loadReader(Type stateType)
				{
					if ((this.flags & ReferencingBodyFlags.IsPolymorphic) != 0)
					{
						using (Local typeLocal = emit.DeclareLocal(typeof(Type), "memberType", false))
						{
							// Read type.
							this.traceBegin(emit, "type");
							emit.LoadStream();
							emit.Stream.ReadType();
							emit.StoreLocal(typeLocal);
							this.traceEnd(emit, "type");

							// Masking logic.
							Label noMask = emit.DefineLabel("noMask");
							Label noMaskEnd = emit.DefineLabel("noMaskEnd");

							// Mask applies only if type == null.
							emit.LoadLocal(typeLocal); 
							emit.BranchIfTrue(noMask);

							using (Local maskLocal = emit.DeclareLocal(typeof(MaskingInfo), "mask", false))
							{
								// Read mask info.
								this.traceBegin(emit, "maskInfo");
								emit.LoadStream();
								emit.Call(MaskingInfoIO.GetReader(emit.ReaderCompiler, emit.Unit, this.inputEmitterFactory.InputType, this.inputEmitterFactory));
								emit.StoreLocal(maskLocal);
								this.traceEnd(emit, "maskInfo");

								// Load member from mask.
								emit.LoadCompiler();
								emit.LoadLocal(maskLocal);
								emit.LoadCaptured(this.memberSpec);
								emit.Call(ReflectionHelper.MaskingInfo_GetMember);
								
								// Store new type.
								emit.Duplicate();
								emit.Call(ReflectionHelper.SerializationMember_GetType);
								emit.StoreLocal(typeLocal);

								// Get delegate.
								emit.CallVirtual(ReflectionHelper.IReaderCompiler_For);
								if (stateType != null)
								{
									emit.LoadType(stateType);
									emit.CallVirtual(ReflectionHelper.IReaderDelegatePairTuple_Deferred);
								}

								emit.CallVirtual(ReflectionHelper.IDelegatePairTuple_Content);
								emit.CallVirtual(ReflectionHelper.IDelegatePair_Generic);
							}

							emit.Branch(noMaskEnd);
							
							emit.MarkLabel(noMask);

							// Get the generic content reader for that type, dynamically.
							emit.LoadPairTupleChangedType(this.memberSpec, LoadStore.LoadLocal(typeLocal));
							if (stateType != null)
							{
								emit.LoadType(stateType);
								emit.CallVirtual(ReflectionHelper.IReaderDelegatePairTuple_Deferred);
							}

							emit.CallVirtual(ReflectionHelper.IDelegatePairTuple_Content);
							emit.CallVirtual(ReflectionHelper.IDelegatePair_Generic);

							emit.MarkLabel(noMaskEnd);

							if (emit is ReaderEmitterDeferred deferred)
							{
								using (Local delegateTemp = emit.DeclareLocal(typeof(Delegate), "delegateTemp", false))
								{
									emit.StoreLocal(delegateTemp);

									emit.LoadCaptured(this.adapterProvider);
									emit.LoadLocal(typeLocal);
									emit.LoadLocal(delegateTemp);
									emit.Call(ReflectionHelper.ReaderAdapterProvider_Get.MakeGenericMethod(deferred.Stream.Type, this.memberSpec.Type, stateType));
								}
							}
						}
					}
					else
					{
						// Get the generic content reader for that type, statically.
						emit.LoadPairTuple(this.memberSpec);

						if (stateType != null)
						{
							emit.LoadType(stateType);
							emit.CallVirtual(ReflectionHelper.IReaderDelegatePairTuple_Deferred);
						}

						emit.CallVirtual(ReflectionHelper.IDelegatePairTuple_Content);
						emit.CallVirtual(ReflectionHelper.IDelegatePair_Generic);
					}
				}

				if ((this.flags & ReferencingBodyFlags.IsReferenced) == 0)
				{
					// Not a reference class, the content is stored inline.
					if (emit is ReaderEmitterDeferred deferred)
					{
						// Invoke reader.
						Type delType = typeof(ObjectReaderDeferred<,,>).MakeGenericType(deferred.Stream.Type, this.memberSpec.Type, deferred.StateType);
						loadReader(deferred.StateType);
						deferred.CastClass(delType);
						deferred.LoadStream();
						deferred.LoadCallback();
						deferred.LoadState();
						deferred.InvokeDelegate(delType);
					}
					else if (emit is ReaderEmitterDirect direct)
					{
						// Invoke reader.
						Type delType = typeof(ObjectReader<,>).MakeGenericType(direct.Stream.Type, this.memberSpec.Type);
						loadReader(null);
						direct.CastClass(delType);
						direct.LoadStream();
						direct.LoadValueAddress();
						direct.InvokeDelegate(delType);
					}
					else
					{
						throw new NotSupportedException();
					}
				}
				else
				{
					ReaderEmitterDeferred deferred = (ReaderEmitterDeferred)emit;

					using (Local idLocal = emit.DeclareLocal(this.identifierType, "identifier", false))
					using (Local refResolverLocal = emit.DeclareLocal(this.readerInterfaceType, "refResolver", false))
					using (Local stateLocal = emit.DeclareLocal(this.callbackStateType, "state", false))
					{
						Label beginReadBoxed = emit.DefineLabel("beginReadBoxed");
						Label endRead = emit.DefineLabel("endRead");
						
						if ((this.flags & ReferencingBodyFlags.HandleBoxing) != 0)
						{
							// If isBoxed goto beginRead
							this.traceBegin(emit, "isBoxed");
							emit.LoadStream();
							emit.Stream.ReadBoolean();
							this.traceEnd(emit, "isBoxed");

							emit.BranchIfTrue(beginReadBoxed);
						}

						// Cast context to interface and store in local.
						emit.LoadStream();
						emit.CastClass(this.readerInterfaceType);
						emit.StoreLocal(refResolverLocal);

						// Read identifier
						this.traceBegin(emit, "identifier");
						emit.LoadLocal(refResolverLocal);
						emit.CallVirtual(this.readerIdentifierMethod);
						emit.StoreLocal(idLocal);
						this.traceEnd(emit, "identifier");

						if ((this.flags & ReferencingBodyFlags.StoreAddFlag) != 0)
						{
							// Write wasAdded flag.
							this.traceBegin(emit, "wasAdded");
							emit.LoadStream();
							emit.Stream.ReadBoolean();
							emit.Pop(); // TODO: Check the flag and throw exception.
							this.traceEnd(emit, "wasAdded");
						}

						// Context.Get<TState>(id, callback, state)
						emit.LoadLocal(refResolverLocal);
						emit.LoadLocal(idLocal);
						deferred.LoadCallback();
						deferred.LoadState();

						emit.CallVirtual(this.getMethod.MakeGenericMethod(this.memberSpec.Type, deferred.StateType));

						// If result of Get == Initial
						emit.LoadConstant((int)DeferredReferencingResult.Initial);
						emit.CompareEqual();
						emit.BranchIfFalse(endRead);
						{
							// state.Context = context
							emit.LoadLocalAddress(stateLocal);
							emit.LoadLocal(refResolverLocal);
							emit.StoreField(this.callbackStateType.GetField("Context"));

							// state.Identifier = id;
							emit.LoadLocalAddress(stateLocal);
							emit.LoadLocal(idLocal);
							emit.StoreField(this.callbackStateType.GetField("Identifier"));

							// Call reader with the "Set" method as callback.
							Type delType = typeof(ObjectReaderDeferred<,,>).MakeGenericType(emit.Stream.Type, this.memberSpec.Type, this.callbackStateType);
							loadReader(this.callbackStateType);

							emit.CastClass(delType);
							emit.LoadStream();
							emit.LoadField(typeof(SetCallback<,>).MakeGenericType(this.memberSpec.Type, this.identifierType).GetField("Delegate", BindingFlags.Public | BindingFlags.Static));
							emit.LoadLocal(stateLocal);
							emit.InvokeDelegate(delType);
							emit.Branch(endRead);
						}
						
						{
							// Version for boxed types without the extra "Set" callback.
							// This is branched to by the 'If isBoxed' check a couple of lines up.
							emit.MarkLabel(beginReadBoxed);

							// Call reader with original callback and state.
							loadReader(deferred.StateType);
							Type delType = typeof(ObjectReaderDeferred<,,>).MakeGenericType(emit.Stream.Type, this.memberSpec.Type, deferred.StateType);

							emit.CastClass(delType);
							emit.LoadStream();
							deferred.LoadCallback();
							deferred.LoadState();
							emit.InvokeDelegate(delType);
						}

						emit.MarkLabel(endRead);
					}
				}
			}

			/// <summary>
			/// Callback argument for the "Set" callback.
			/// </summary>
			/// <typeparam name="TIdentifier"></typeparam>
			private struct SetCallbackState<TIdentifier>
			{
#pragma warning disable 649
				public IReferencingInputStream<TIdentifier> Context;
				public TIdentifier Identifier;
#pragma warning restore 649
			}

			/// <summary>
			/// Provides the callback delegate that actually invokes <see cref="IDeferredReferenceResolver{T, TIdentifier}.Set(TIdentifier, T)" />.
			/// </summary>
			/// <typeparam name="T"></typeparam>
			/// <typeparam name="TIdentifier"></typeparam>
			private static class SetCallback<T, TIdentifier>
			{
				// ReSharper disable once UnusedMember.Local
				public static readonly DeferredCallback<T, SetCallbackState<TIdentifier>> Delegate = set;

				private static void set(T value, SetCallbackState<TIdentifier> state)
				{
					state.Context.Set(state.Identifier, value);
				}
			}
		}

		#endregion
	}
}
