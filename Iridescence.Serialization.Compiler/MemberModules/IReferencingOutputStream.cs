namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Interfaces for a stream to support <see cref="ReferencingMemberModule"/>.
	/// </summary>
	/// <typeparam name="TIdentifier"></typeparam>
	public interface IReferencingOutputStream<TIdentifier> : IReferenceProvider<object, TIdentifier>
	{
		/// <summary>
		/// Writes the specified identifier to the stream.
		/// </summary>
		/// <param name="id"></param>
		void WriteIdentifier(TIdentifier id);
	}
}