﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements a member module that invokes begin and end methods of a <see cref="ISerializationTraceStream"/> for each member reader/writer.
	/// </summary>
	public class TraceMemberModule : IMemberModule
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TraceMemberModule"/> class. 
		/// </summary>
		public TraceMemberModule()
		{

		}

		#endregion

		#region Methods

		public void CheckReaderCompatibility(Type sourceType)
		{
			if (!typeof(ISerializationTraceStream).IsAssignableFrom(sourceType))
				throw new ArgumentException($"Source type must implement {nameof(ISerializationTraceStream)}.", nameof(sourceType));
		}

		public void CheckWriterCompatibility(Type destType)
		{
			if (!typeof(ISerializationTraceStream).IsAssignableFrom(destType))
				throw new ArgumentException($"Destination type must implement {nameof(ISerializationTraceStream)}.", nameof(destType));
		}

		public void ConfigureWriter(WriterTemplate template)
		{
			template.Augmentations.Add(new TraceAugmentation(AugmentationPosition.BeforeBody, SerializationDirection.Write, SerializationMode.Member, template.Specification));
			template.Augmentations.Add(new TraceAugmentation(AugmentationPosition.AfterBody, SerializationDirection.Write, SerializationMode.Member, template.Specification));
		}

		public void ConfigureReader(ReaderTemplate template)
		{
			template.Augmentations.Add(new TraceAugmentation(AugmentationPosition.BeforeBody, SerializationDirection.Read, SerializationMode.Member, template.Specification));
			template.Augmentations.Add(new TraceAugmentation(AugmentationPosition.AfterBody, SerializationDirection.Read, SerializationMode.Member, template.Specification));
		}

		#endregion
	}
}
