﻿using System.Collections.Generic;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents an abstract template for an emitted method.
	/// </summary>
	public abstract class Template
	{
		#region Properties

		/// <summary>
		/// Gets the member to build.
		/// </summary>
		public SerializationMember Specification { get; }

		/// <summary>
		/// Gets the list of <see cref="IAugmentation"/> for the method.
		/// </summary>
		public List<IAugmentation> Augmentations { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Template"/> class. 
		/// </summary>
		protected Template(SerializationMember memberSpec)
		{
			this.Specification = memberSpec;
			this.Augmentations = new List<IAugmentation>();
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			return $"{this.GetType().Name} for {this.Specification}";
		}

		#endregion
	}
}
