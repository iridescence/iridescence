﻿using System;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	internal static class TraceExtensions
	{

		private static readonly MethodInfo beginMethod = typeof(ISerializationTraceStream).GetMethod(nameof(ISerializationTraceStream.OnBegin)) ?? throw new MissingMethodException();
		private static readonly MethodInfo endMethod = typeof(ISerializationTraceStream).GetMethod(nameof(ISerializationTraceStream.OnEnd)) ?? throw new MissingMethodException();
		private static readonly ConstructorInfo constructor = typeof(SerializationTraceInfo).GetConstructor(new Type[] {typeof(SerializationMember), typeof(SerializationDirection), typeof(SerializationMode), typeof(string)});

		private static void trace(EmitterBase emit, MethodInfo traceMethod, in SerializationTraceInfo info)
		{
			if (!emit.Trace)
				return;

			using (Local infoLocal = emit.DeclareLocal(typeof(SerializationTraceInfo), "info", false))
			{
				emit.LoadLocalAddress(infoLocal);
				emit.LoadCaptured(info.Member);
				emit.LoadConstant((int)info.Direction);
				emit.LoadConstant((int)info.Mode);
				if (info.Comment != null)
					emit.LoadConstant(info.Comment);
				else
					emit.LoadNull();
				emit.Call(constructor);

				emit.LoadStream();
				emit.CastClass(typeof(ISerializationTraceStream));
				emit.LoadLocalAddress(infoLocal);
				emit.CallVirtual(traceMethod);
			}
		}

		internal static void TraceBegin(this IEmitOwner owner, in SerializationTraceInfo info)
		{
			trace(owner.Emit, beginMethod, info);
		}
		
		internal static void TraceEnd(this IEmitOwner owner, in SerializationTraceInfo info)
		{
			trace(owner.Emit, endMethod, info);
		}
	}
}
