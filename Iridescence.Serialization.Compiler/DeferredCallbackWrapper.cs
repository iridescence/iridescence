using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Generic callback wrapper that acts as a generic <see cref="DeferredCallback{TObj,TState}"/>, but calls a non-generic callback.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <typeparam name="TState"></typeparam>
	internal sealed class DeferredCallbackWrapper<T, TState>
	{
		#region Fields

		private readonly DeferredCallback<object, TState> callback;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new generic wrapper for the specified non-generic callback.
		/// </summary>
		/// <param name="callback"></param>
		public DeferredCallbackWrapper(DeferredCallback<object, TState> callback)
		{
			this.callback = callback ?? throw new ArgumentNullException(nameof(callback));
		}

		#endregion

		#region Methods

		/// <summary>
		/// Invokes the non-generic callback.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="state"></param>
		public void Invoke(T value, TState state)
		{
			this.callback(value, state);
		}

		#endregion
	}
}