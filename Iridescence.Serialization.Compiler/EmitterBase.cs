﻿using System;
using System.Collections.Generic;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents the base for reader/writer code generation.
	/// </summary>
	public class EmitterBase : ILBuilderWrapper, IEmitOwner
	{
		#region Fields

		internal readonly IList<ICapturedValue> CapturedValues;

		#endregion

		#region Properties

		EmitterBase IEmitOwner.Emit => this;

		/// <summary>
		/// Gets or sets whether debug tracing is enabled.
		/// </summary>
		public bool Trace { get; }

		/// <summary>
		/// Gets the <see cref="ICompilationUnit"/> this emitter is part of.
		/// </summary>
		public ICompilationUnit Unit { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="EmitterBase"/> class. 
		/// </summary>
		public EmitterBase(ICompilationUnit unit, ILBuilder emit, IList<ICapturedValue> capturedValues, bool trace)
			: base(emit)
		{
			this.Unit = unit ?? throw new ArgumentNullException(nameof(unit));
			this.CapturedValues = capturedValues;
			this.Trace = trace;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EmitterBase"/> class. 
		/// </summary>
		public EmitterBase(EmitterBase emit)
			: base(emit)
		{
			this.Unit = emit.Unit;
			this.CapturedValues = emit.CapturedValues;
			this.Trace = emit.Trace;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Loads the emitted method target onto the stack.
		/// </summary>
		protected void LoadThis()
		{
			this.LoadArgument(0);
		}

		/// <summary>
		/// Loads the stream supplied at runtime onto the stack.
		/// </summary>
		public void LoadStream()
		{
			this.LoadArgument(1);
		}

		/// <summary>
		/// Adds the specified value to the emitted method's external value table and emits the instructions to load it onto the stack.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value"></param>
		public void LoadCaptured<T>(T value)
		{
			this.LoadCapturedValue(new DirectCapturedValue(value, typeof(T)));
		}

		/// <summary>
		/// Adds the specified value to the emitted method's external value table and emits the instructions to load it onto the stack.
		/// </summary>
		/// <param name="value"></param>
		public void LoadCapturedValue(ICapturedValue value)
		{
			if (value == null)
			{
				this.LoadNull();
				return;
			}

			int index = this.captureValue(value);

			this.LoadThis();
			this.LoadConstant(index);
			this.Call(ReflectionHelper.EmittedMethod_GetCapturedValue.MakeGenericMethod(value.Type));
		}

		private int captureValue(ICapturedValue value)
		{
			for (int i = 0; i < this.CapturedValues.Count; ++i)
			{
				if (value.Equals(this.CapturedValues[i]))
					return i;
			}

			int index = this.CapturedValues.Count;
			this.CapturedValues.Add(value);

			return index;
		}

		/// <summary>
		/// Compiles the captured values into an array.
		/// </summary>
		/// <returns></returns>
		internal object[] CompileCapturedValues()
		{
			if (this.CapturedValues.Count == 0)
				return Array.Empty<object>();

			object[] values = new object[this.CapturedValues.Count];

			for (int i = 0; i < values.Length; ++i)
			{
				values[i] = this.CapturedValues[i].GetValue();
			}

			return values;
		}

		#endregion
	}
}
