﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Contains the pre- and post-body emit steps of a <see cref="ReaderTemplate"/>.
	/// </summary>
	public class AugmentationCollection : IReadOnlyList<IAugmentation>
	{
		#region Fields

		private readonly IReadOnlyList<IAugmentation> list;

		#endregion

		#region Properties

		public int Count => this.list.Count;

		public IAugmentation this[int index] => this.list[index];

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AugmentationCollection"/> class. 
		/// </summary>
		public AugmentationCollection(IReadOnlyList<IAugmentation> list)
		{
			this.list = list;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AugmentationCollection"/> class. 
		/// </summary>
		public AugmentationCollection(Template template)
			: this(template.Augmentations)
		{
			
		}

		#endregion

		#region Methods

		public void Emit(AugmentationEmitter emit, params AugmentationPosition[] positions)
		{
			foreach (IAugmentation step in this.Where(s => positions.Contains(s.Position)))
			{
				step.Emit(emit);
			}
		}

		public IEnumerator<IAugmentation> GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)this.list).GetEnumerator();
		}

		#endregion
	}
}
