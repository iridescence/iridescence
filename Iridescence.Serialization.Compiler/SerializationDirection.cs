﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Defines "Read" and "Write" serialization directions.
	/// </summary>
	public enum SerializationDirection
	{
		/// <summary>
		/// The input or read direction.
		/// </summary>
		Read,

		/// <summary>
		/// The output or write direction.
		/// </summary>
		Write,
	}
}
