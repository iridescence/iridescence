﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Serialization.Compiler
{
	[Serializable]
	public class CompilationNotCompletedException : CompilationException
	{
		public CompilationNotCompletedException() : base("The compilation unit is not yet complete.")
		{
		}

		public CompilationNotCompletedException(string message) : base(message)
		{
		}

		public CompilationNotCompletedException(string message, Exception inner) : base(message, inner)
		{
		}

		protected CompilationNotCompletedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
