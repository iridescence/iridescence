﻿using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	public interface ICompilationUnit
	{
		/// <summary>
		/// Creates a new method to compile to.
		/// </summary>
		/// <param name="signature">The signature of the method.</param>
		/// <param name="name">The name of the method.</param>
		/// <param name="withCapturedValues">Whether the method supports capturing external values (closure).</param>
		/// <returns></returns>
		CompilableEmitter CreateMethod(MethodSignature signature, string name, bool withCapturedValues = true);

		/// <summary>
		/// Completes the compilation.
		/// </summary>
		void Complete();
	}
}