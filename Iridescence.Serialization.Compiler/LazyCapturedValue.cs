﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents a lazily captured value. The value becomes available only after the method was compiled.
	/// </summary>
	public class LazyCapturedValue : ICapturedValue
	{
		private readonly Lazy<object> lazy;

		public Type Type { get; }

		public LazyCapturedValue(Lazy<object> lazy, Type type)
		{
			this.lazy = lazy;
			this.Type = type ?? throw new ArgumentNullException(nameof(type));
		}

		public LazyCapturedValue(Func<object> factory, Type type)
		{
			this.lazy = new Lazy<object>(factory);
			this.Type = type ?? throw new ArgumentNullException(nameof(type));
		}

		public object GetValue()
		{
			return this.lazy.Value;	
		}
	}
}