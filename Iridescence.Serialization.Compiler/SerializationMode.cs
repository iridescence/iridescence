﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Defines the serialization mode of an emitted reader or writer method.
	/// </summary>
	public enum SerializationMode
	{
		/// <summary>
		/// (De)serializes the contents of a specific type.
		/// </summary>
		Content,

		/// <summary>
		/// (De)serializes a member of a certain type.
		/// </summary>
		Member,
	}
}
