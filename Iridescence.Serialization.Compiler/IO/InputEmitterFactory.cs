﻿using System;

namespace Iridescence.Serialization.Compiler
{
	public sealed class InputEmitterFactory : IInputEmitterFactory
	{
		private readonly IInputEmitterFactory factory;

		public Type InputType { get; }

		public InputEmitterFactory(IIOModule module, Type type)
		{
			this.factory = module.CreateInput(type);
			this.InputType = type;
		}

		public IInputEmitter CreateInputEmitter(EmitterBase emit)
		{
			return this.factory.CreateInputEmitter(emit);
		}
	}
}