﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents the abstract reader stream code generation API.
	/// </summary>
	public interface IInputEmitter
	{
		#region Properties

		/// <summary>
		/// Gets the type of the stream.
		/// </summary>
		Type Type { get; }

		/// <summary>
		/// Whether the stream is buffered. If this is set to false, the <see cref="Acquire"/> method and the "acquire" parameters have no effect.
		/// </summary>
		bool IsBuffered { get; }

		#endregion
		
		#region Methods

		/// <summary>
		/// Pops an integer N from the stack and reads N bytes from the stream into the internal buffer.
		/// </summary>
		void Acquire();

		/// <summary>
		/// Reads a boolean from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadBoolean(bool acquire = true);

		/// <summary>
		/// Reads a signed byte from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadInt8(bool acquire = true);

		/// <summary>
		/// Reads an unsigned from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadUInt8(bool acquire = true);

		/// <summary>
		/// Reads a signed 16-bit integer from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadInt16(bool acquire = true);
		
		/// <summary>
		/// Reads an unsigned 16-bit integer from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadUInt16(bool acquire = true);
		
		/// <summary>
		/// Reads a signed 32-bit integer from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadInt32(bool acquire = true);
		
		/// <summary>
		/// Reads an unsigned 32-bit integer from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadUInt32(bool acquire = true);
		
		/// <summary>
		/// Reads a signed 64-bit integer from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadInt64(bool acquire = true);
		
		/// <summary>
		/// Reads an unsigned 64-bit integer from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadUInt64(bool acquire = true);
		
		/// <summary>
		/// Reads a single-precision floating-point number from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadFloat32(bool acquire = true);
		
		/// <summary>
		/// Reads a double-precision floating-point number from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadFloat64(bool acquire = true);
		
		/// <summary>
		/// Reads a signed variable-length integer from the internal buffer, acquiring more bytes as needed. The result is pushed to the stack.
		/// </summary>
		void ReadVarInt();
		
		/// <summary>
		/// Reads an unsigned variable-length integer from the internal buffer, acquiring more bytes as needed. The result is pushed to the stack.
		/// </summary>
		void ReadVarUInt();
		
		/// <summary>
		/// Reads a primitive array from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="elementType">The element type. Must be a primitive.</param>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadArray(Type elementType, bool acquire = true);
		
		/// <summary>
		/// Reads a primitive span from the internal buffer, optionally invoking <see cref="Acquire"/> beforehand. The result is pushed to the stack.
		/// </summary>
		/// <param name="elementType">The element type. Must be a primitive.</param>
		/// <param name="acquire">Whether to acquire the required number of bytes before reading.</param>
		void ReadSpan(Type elementType, bool acquire = true);
		
		/// <summary>
		/// Reads a string from the internal buffer, acquiring the required number of bytes beforehand. The result is pushed to the stack.
		/// </summary>
		void ReadString();
		
		/// <summary>
		/// Reads and parses a <see cref="Type"/> from the internal buffer, acquiring the required number of bytes beforehand. The result is pushed to the stack.
		/// </summary>
		void ReadType();
		
		#endregion
	}
}
