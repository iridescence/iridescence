﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// The interface a stream must implement to use <see cref="TraceMemberModule"/> and <see cref="TraceContentModule"/>
	/// </summary>
	public interface ISerializationTraceStream
	{
		/// <summary>
		/// Notifies the analyzer that the data of the specified member begins.
		/// </summary>
		/// <param name="info"></param>
		void OnBegin(in SerializationTraceInfo info);

		/// <summary>
		/// Notifies the analyzer that the data of the specified member has ended.
		/// </summary>
		/// <param name="info"></param>
		void OnEnd(in SerializationTraceInfo info);
	}
}
