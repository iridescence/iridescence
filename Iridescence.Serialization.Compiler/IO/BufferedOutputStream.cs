﻿#if DEBUG
#define CHECKBOUNDS
#endif
//#define STREAMDEBUG

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Buffered output stream for serialization.
	/// </summary>
	public class BufferedOutputStream : Stream
	{
		#region Fields

		private readonly bool leaveOpen;
		private byte[] buffer;
		private int bufferOffset;
		private int bufferLimit;
		private long positionOffset;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the underlying <see cref="Stream"/> to write to.
		/// </summary>
		public Stream Destination { get; }

		/// <summary>
		/// Gets the current buffer.
		/// </summary>
		public Span<byte> Buffer => new Span<byte>(this.buffer, this.bufferOffset, this.bufferLimit - this.bufferOffset);

		/// <summary>
		/// Gets the position in the stream (or the total amount of bytes written).
		/// </summary>
		public override long Position
		{
			get => this.positionOffset + this.bufferOffset;
			set => throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override bool CanRead => false;

		/// <inheritdoc/>
		public override bool CanSeek => false;

		/// <inheritdoc/>
		public override bool CanWrite => true;

		/// <inheritdoc/>
		public override long Length => throw new NotSupportedException();

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="BufferedOutputStream"/> class. 
		/// </summary>
		/// <param name="destination">The destination <see cref="Stream"/> to write to.</param>
		/// <param name="initialCapacity">The initial buffer size.</param>
		/// <param name="leaveOpen">True, the the destination stream should be left open when this instance is disposed.</param>
		public BufferedOutputStream(Stream destination, int initialCapacity, bool leaveOpen)
		{
			if (initialCapacity <= 0)
				throw new ArgumentOutOfRangeException(nameof(initialCapacity));

			this.Destination = destination ?? throw new ArgumentNullException(nameof(destination));
			this.buffer = new byte[initialCapacity];
			this.leaveOpen = leaveOpen;
		}

		#endregion

		#region Methods

		#region Stream Implementation

		/// <inheritdoc/>
		public override long Seek(long offset, SeekOrigin origin) => throw new NotSupportedException();

		/// <inheritdoc/>
		public override void SetLength(long value) => throw new NotSupportedException();

		/// <inheritdoc/>
		public override int Read(byte[] buffer, int offset, int count) => throw new NotSupportedException();

		/// <inheritdoc/>
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.Write(new ReadOnlySpan<byte>(buffer, offset, count));
		}

		/// <inheritdoc/>
		public override void WriteByte(byte value)
		{
			this.Reserve(1);
			this.buffer[this.bufferOffset++] = value;
		}

		#endregion

		[Conditional("CHECKBOUNDS")]
		private void throwIfOverflow(int numBytes)
		{
			if (this.bufferOffset + numBytes > this.bufferLimit)
				throw new InvalidOperationException($"Writing {numBytes} bytes would overflow the buffer, which has only {this.bufferLimit - this.bufferOffset} bytes available.");
		}

#if STREAMDEBUG
		private void debugWrite(string str)
		{
			this.Reserve(4);
			this.buffer[this.bufferOffset] = str.Length > 0 ? (byte)str[0] : (byte)0x20;
			this.buffer[this.bufferOffset + 1] = str.Length > 1 ? (byte)str[1] : (byte)0x20;
			this.buffer[this.bufferOffset + 2] = str.Length > 2 ? (byte)str[2] : (byte)0x20;
			this.buffer[this.bufferOffset + 3] = str.Length > 3 ? (byte)str[3] : (byte)0x20;
			this.bufferOffset += 4;
		}
#endif

		private void flushBuffer()
		{
			if (this.bufferOffset == 0)
				return;

			this.Destination.Write(this.buffer, 0, this.bufferOffset);
			this.bufferLimit -= this.bufferOffset;
			this.positionOffset += this.bufferOffset;
			this.bufferOffset = 0;
		}

		private async ValueTask flushBufferAsync()
		{
			if (this.bufferOffset == 0)
				return;

			await this.Destination.WriteAsync(this.buffer, 0, this.bufferOffset);
			this.bufferLimit -= this.bufferOffset;
			this.positionOffset += this.bufferOffset;
			this.bufferOffset = 0;
		}

		/// <summary>
		/// Flushes all data to the output <see cref="Destination"/>.
		/// </summary>
		public override void Flush()
		{
			this.flushBuffer();
			this.Destination.Flush();
		}

		public override async Task FlushAsync(CancellationToken cancellationToken)
		{
			await this.flushBufferAsync();
			await this.Destination.FlushAsync(cancellationToken);
		}

		/// <summary>
		/// Flushes all data to the output <see cref="Destination"/>.
		/// </summary>
		/// <param name="throwIfBytesAvailable">True, if an exception should be thrown in case there's still unfilled space in the buffer.</param>
		public void Flush(bool throwIfBytesAvailable)
		{
			if (throwIfBytesAvailable && this.bufferOffset != this.bufferLimit)
				throw new InvalidOperationException("Buffer has unfilled space and can not be flushed.");

			this.Flush();
		}

		private void reserveUnlikely()
		{
			// Make room by flushing the existing data.
			this.flushBuffer();

			if (this.bufferLimit > this.buffer.Length)
			{
				// The buffer is too small, resize.
				this.buffer = new byte[4096 * ((this.bufferLimit + 4095) / 4096)];
			}
		}

		/// <summary>
		/// Ensures that at least the specified amount of bytes are available in the buffer.
		/// </summary>
		/// <param name="numBytes"></param>
		/// <returns>The current position in the stream.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Reserve(int numBytes)
		{
#if CHECKBOUNDS
			if (numBytes < 0)
				throw new ArgumentOutOfRangeException(nameof(numBytes));
#endif

			this.bufferLimit += numBytes;
			if (this.bufferLimit > this.buffer.Length)
				this.reserveUnlikely();
		}

		/// <summary>
		/// Advances the buffer by the specified amount of bytes.
		/// </summary>
		/// <param name="numBytes"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Advance(int numBytes)
		{
#if CHECKBOUNDS
			if (this.bufferOffset + numBytes > this.bufferLimit)
				throw new InvalidOperationException($"Can't advance {numBytes} bytes, the buffer only has {this.bufferLimit - this.bufferOffset} bytes available.");
#endif
			this.bufferOffset += numBytes;
		}


		/// <summary>
		/// Writes the specified data into the stream.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		public void Put<T>(ReadOnlySpan<T> data) where T : struct
		{
			ReadOnlySpan<byte> bytes = data.AsBytes();
			this.throwIfOverflow(bytes.Length);
			bytes.CopyTo(this.Buffer);
			this.Advance(bytes.Length);
		}

		/// <summary>
		/// Writes the specified data into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		public void Write<T>(ReadOnlySpan<T> data) where T : struct
		{
			const int bufferSize = 65536;
			ReadOnlySpan<byte> bytes = data.AsBytes();
			int offset = 0;

			while (offset < bytes.Length)
			{
				int numBytes = Math.Min(bytes.Length - offset, bufferSize);
				this.Reserve(numBytes);
				bytes.Slice(offset, numBytes).CopyTo(this.Buffer);
				this.Advance(numBytes);
				offset += numBytes;
			}
		}

		/// <summary>
		/// Writes the specified data into the stream.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Put<T>(T[] data) where T : struct
		{
			this.Put(new ReadOnlySpan<T>(data));
		}


		/// <summary>
		/// Writes the specified data into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Write<T>(T[] array) where T : struct
		{
			this.Write(new ReadOnlySpan<T>(array));
		}

		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutBoolean(bool value)
		{
			this.throwIfOverflow(1);
#if STREAMDEBUG
			this.debugWrite("bool");
#endif
			this.buffer[this.bufferOffset++] = value ? (byte)1 : (byte)0;
		}


		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteBoolean(bool value)
		{
#if STREAMDEBUG
			this.debugWrite("bool");
#endif
			this.Reserve(1);
			this.buffer[this.bufferOffset++] = value ? (byte)1 : (byte)0;
		}


		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutInt8(sbyte value)
		{
			this.throwIfOverflow(1);
#if STREAMDEBUG
			this.debugWrite("i8");
#endif
			this.buffer[this.bufferOffset++] = unchecked((byte)value);
		}


		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteInt8(sbyte value)
		{
#if STREAMDEBUG
			this.debugWrite("i8");
#endif
			this.Reserve(1);
			this.buffer[this.bufferOffset++] = unchecked((byte)value);
		}


		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutUInt8(byte value)
		{
			this.throwIfOverflow(1);
#if STREAMDEBUG
			this.debugWrite("ui8");
#endif
			this.buffer[this.bufferOffset++] = value;
		}


		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteUInt8(byte value)
		{
#if STREAMDEBUG
			this.debugWrite("ui8");
#endif
			this.Reserve(1);
			this.buffer[this.bufferOffset++] = value;
		}

	
		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutInt16(short value)
		{
			this.throwIfOverflow(2);
#if STREAMDEBUG
			this.debugWrite("i16");
#endif
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 2;			
		}


		/// <summary>
		/// Writes the specified value into the stream. This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteInt16(short value)
		{
#if STREAMDEBUG
			this.debugWrite("i16");
#endif
			this.Reserve(2);
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 2;			
		}


		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutUInt16(ushort value)
		{
			this.throwIfOverflow(2);
#if STREAMDEBUG
			this.debugWrite("ui16");
#endif
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 2;
		}

		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteUInt16(ushort value)
		{
#if STREAMDEBUG
			this.debugWrite("ui16");
#endif
			this.Reserve(2);
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 2;
		}

		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutInt32(int value)
		{
			this.throwIfOverflow(4);
#if STREAMDEBUG
			this.debugWrite("i32");
#endif
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 4;
		}

		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteInt32(int value)
		{
#if STREAMDEBUG
			this.debugWrite("i32");
#endif
			this.Reserve(4);
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 4;
		}

		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutUInt32(uint value)
		{
			this.throwIfOverflow(4);
#if STREAMDEBUG
			this.debugWrite("ui32");
#endif
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 4;
		}

		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteUInt32(uint value)
		{
#if STREAMDEBUG
			this.debugWrite("ui32");
#endif
			this.Reserve(4);
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 4;
		}

		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutInt64(long value)
		{
			this.throwIfOverflow(8);
#if STREAMDEBUG
			this.debugWrite("i64");
#endif
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 8;
		}

		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteInt64(long value)
		{
#if STREAMDEBUG
			this.debugWrite("i64");
#endif
			this.Reserve(8);
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 8;
		}

		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutUInt64(ulong value)
		{
			this.throwIfOverflow(8);
#if STREAMDEBUG
			this.debugWrite("ui64");
#endif
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 8;
		}

		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteUInt64(ulong value)
		{
#if STREAMDEBUG
			this.debugWrite("ui64");
#endif
			this.Reserve(8);
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 8;
		}

		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutFloat32(float value)
		{
			this.throwIfOverflow(4);
#if STREAMDEBUG
			this.debugWrite("f32");
#endif
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 4;
		}

		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteFloat32(float value)
		{
#if STREAMDEBUG
			this.debugWrite("f32");
#endif
			this.Reserve(4);
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 4;
		}

		/// <summary>
		/// Writes the specified value into the stream without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutFloat64(double value)
		{
			this.throwIfOverflow(8);
#if STREAMDEBUG
			this.debugWrite("f64");
#endif
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 8;
		}

		/// <summary>
		/// Writes the specified value into the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteFloat64(double value)
		{
#if STREAMDEBUG
			this.debugWrite("f64");
#endif
			this.Reserve(8);
			Unsafe.WriteUnaligned(ref this.buffer[this.bufferOffset], value);
			this.bufferOffset += 8;
		}
		
		/// <summary>
		/// Writes the specified value into the stream using variable-length encoding, without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void PutVarInt(long value)
		{
			this.PutVarUInt(unchecked((ulong)value));
		}

		/// <summary>
		/// Writes the specified value into the stream using variable-length encoding.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteVarInt(long value)
		{
			this.WriteVarUInt(unchecked((ulong)value));
		}

		/// <summary>
		/// Writes the specified value into the stream using variable-length encoding, without reserving space first.
		/// The caller is expected to ensure that there is enough space available by calling <see cref="Reserve"/> first.
		/// </summary>
		/// <param name="value">The value to write.</param>
		public void PutVarUInt(ulong value)
		{
#if STREAMDEBUG
			this.debugWrite("vi");
#endif
			int length = VarUInt.GetLength(value);
			this.throwIfOverflow(length);
			for (int i = 0; i < length - 1; ++i)
			{
				this.buffer[this.bufferOffset++] = (byte)((value & 0x7F) | 0x80);
				value >>= 7;
			}
			this.buffer[this.bufferOffset++] = (byte)value;
		}

		/// <summary>
		/// Writes the specified value into the stream using variable-length encoding.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="value">The value to write.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void WriteVarUInt(ulong value)
		{
#if STREAMDEBUG
			this.debugWrite("vi");
#endif
			int length = VarUInt.GetLength(value);
			this.Reserve(length);
			for (int i = 0; i < length - 1; ++i)
			{
				this.buffer[this.bufferOffset++] = (byte)((value & 0x7F) | 0x80);
				value >>= 7;
			}
			this.buffer[this.bufferOffset++] = (byte)value;
		}

		/// <summary>
		/// Writes a string to the stream.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="str">The string to write.</param>
		/// <param name="encoding">The encoding to use.</param>
		public void WriteString(string str, Encoding encoding)
		{
#if STREAMDEBUG
			this.debugWrite("str");
#endif
			if (str == null)
			{
				this.WriteVarUInt(0);
				return;
			}

			byte[] encodedStr = encoding.GetBytes(str);
			ulong lenPrefix = (ulong)encodedStr.Length + 1;
			this.Reserve(VarUInt.GetLength(lenPrefix) + encodedStr.Length);

			this.PutVarUInt(lenPrefix);
			this.Put(encodedStr);
		}
		
		/// <summary>
		/// Writes a string to the stream using the default encoding.
		/// This method invokes <see cref="Reserve"/> to allocate the required space.
		/// </summary>
		/// <param name="str">The string to write.</param>
		public void WriteString(string str)
		{
			this.WriteString(str, Utility.DefaultEncoding);
		}

		/// <summary>
		/// Writes a type to the stream.
		/// </summary>
		/// <param name="type"></param>
		public virtual void WriteType(Type type)
		{
#if STREAMDEBUG
			this.debugWrite("type");
#endif
			this.WriteString(type.AssemblyQualifiedName);
		}

		/// <summary>
		/// Flushes the buffer and optionally disposes the underlying <see cref="Destination"/>.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			this.Flush(true);

			if (!this.leaveOpen)
			{
				this.Destination.Dispose();	
			}
		}

		#endregion
	}
}
