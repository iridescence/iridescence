﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements an <see cref="IIOModule"/> that creates <see cref="BufferedInputStreamEmitter"/> and <see cref="BufferedOutputStreamEmitter"/>.
	/// </summary>
	public class BufferedStreamIOModule : IIOModule
	{
		public static BufferedStreamIOModule Instance { get; } = new BufferedStreamIOModule();

		public IOutputEmitterFactory CreateOutput(Type outputType)
		{
			return new ForType(outputType);
		}

		public IInputEmitterFactory CreateInput(Type inputType)
		{
			return new ForType(inputType);
		}

		private sealed class ForType : IOutputEmitterFactory, IInputEmitterFactory
		{
			private readonly Type type;

			public ForType(Type type)
			{
				this.type = type;
			}

			public IOutputEmitter CreateOutputEmitter(EmitterBase emit)
			{
				return new BufferedOutputStreamEmitter(this.type, emit);
			}

			public IInputEmitter CreateInputEmitter(EmitterBase emit)
			{
				return new BufferedInputStreamEmitter(this.type, emit);
			}
		}
	}
}
