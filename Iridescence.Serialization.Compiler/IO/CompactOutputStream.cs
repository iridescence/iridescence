﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Augments the <see cref="BufferedOutputStream"/> class using a <see cref="CompactMetadataWriter"/> to write metadata.
	/// </summary>
	public class CompactOutputStream : BufferedOutputStream, IMetadataWriter, ISerializationInfoWriter
	{
		#region Fields

		private readonly CompactMetadataWriter metadataWriter;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public CompactOutputStream(Stream stream, int initialCapacity, bool leaveOpen, CompactMetadataOptions options) 
			: base(stream, initialCapacity, leaveOpen)
		{
			this.metadataWriter = new CompactMetadataWriter(options);
		}

		#endregion

		#region Methods

		public override void WriteType(Type type)
		{
			this.metadataWriter.WriteType(this, type);
		}
		
		public void WriteAssemblyName(AssemblyName name)
		{
			this.metadataWriter.WriteAssemblyName(this, name);
		}

		public void WriteMember(MemberInfo member)
		{
			this.metadataWriter.WriteMember(this, member);
		}

		public void WriteSerializationInfoLayout(Type objectType, IReadOnlyList<SerializationInfoMember> members)
		{
			this.WriteType(objectType);
			this.WriteVarInt(members.Count);

			foreach (SerializationInfoMember member in members)
			{
				this.WriteType(member.Type);
			}

			foreach (SerializationInfoMember member in members)
			{
				this.WriteString(member.Name);
			}
		}

		#endregion
	}
}
