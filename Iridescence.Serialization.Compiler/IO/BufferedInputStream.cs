﻿#define CHECKBOUNDS
//#define STREAMDEBUG

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements a greedy buffered input stream that is able to read primitive types and arrays thereof.
	/// </summary>
	public class BufferedInputStream : Stream
	{
		#region Fields

		private readonly bool leaveOpen;
		private byte[] buffer;
		private int bufferOffset;
		private int bufferLimit;
		private int bufferReadAhead;
		private long positionOffset;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the underlying <see cref="Stream"/> to read from.
		/// </summary>
		public Stream Source { get; }

		/// <summary>
		/// Gets the current buffer.
		/// </summary>
		public Span<byte> Buffer => new Span<byte>(this.buffer, this.bufferOffset, this.bufferLimit - this.bufferOffset);

		/// <summary>
		/// Gets the position in the stream (or the total amount of bytes written).
		/// </summary>
		public override long Position
		{
			get => this.positionOffset - this.bufferReadAhead + this.bufferOffset;
			set => throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override bool CanRead => true;

		/// <inheritdoc/>
		public override bool CanSeek => false;

		/// <inheritdoc/>
		public override bool CanWrite => false;

		/// <inheritdoc/>
		public override long Length => throw new NotSupportedException();

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="BufferedInputStream"/> class. 
		/// </summary>
		/// <param name="source">The source <see cref="Stream"/> to read from.</param>
		/// <param name="initialCapacity">The initial buffer size.</param>
		/// <param name="leaveOpen">True, the the source stream should be left open when this instance is disposed.</param>
		public BufferedInputStream(Stream source, int initialCapacity, bool leaveOpen)
		{
			if (initialCapacity <= 0)
				throw new ArgumentOutOfRangeException(nameof(initialCapacity));

			this.Source = source ?? throw new ArgumentNullException(nameof(source));
			this.buffer = new byte[initialCapacity];
			this.leaveOpen = leaveOpen;
		}

		#endregion

		#region Methods

		#region Stream Implementation

		/// <inheritdoc/>
		public override long Seek(long offset, SeekOrigin origin) => throw new NotSupportedException();

		/// <inheritdoc/>
		public override void SetLength(long value) => throw new NotSupportedException();

		/// <inheritdoc/>
		public override int Read(byte[] buffer, int offset, int count)
		{
			int numBytesToRead = this.TryAcquire(count);
			if (numBytesToRead == 0)
				return 0;

			this.Buffer.Slice(0, numBytesToRead).CopyTo(new Span<byte>(buffer, offset, numBytesToRead));
			this.Advance(numBytesToRead);
			return numBytesToRead;
		}

		/// <inheritdoc/>
		public override int ReadByte()
		{
			this.TryAcquire(1);

			if (this.Buffer.Length == 0)
				return -1;

			return this.buffer[this.bufferOffset++];
		}

		/// <inheritdoc/>
		public override void Write(byte[] buffer, int offset, int count) => throw new NotSupportedException();

		/// <inheritdoc/>
		public override void Flush()
		{

		}

		#endregion

#if STREAMDEBUG
		private readonly char[] debugBuf = new char[4];
		private void debugRead(string str)
		{
			this.Acquire(4);
			this.debugBuf[0] = (char)this.buffer[this.bufferOffset];
			this.debugBuf[1] = (char)this.buffer[this.bufferOffset + 1];
			this.debugBuf[2] = (char)this.buffer[this.bufferOffset + 2];
			this.debugBuf[3] = (char)this.buffer[this.bufferOffset + 3];
			this.bufferOffset += 4;

			if (this.debugBuf[0] != (str.Length > 0 ? (byte)str[0] : (byte)0x20) ||
			    this.debugBuf[1] != (str.Length > 1 ? (byte)str[1] : (byte)0x20) ||
			    this.debugBuf[2] != (str.Length > 2 ? (byte)str[2] : (byte)0x20) ||
			    this.debugBuf[3] != (str.Length > 3 ? (byte)str[3] : (byte)0x20))
			{
				throw new Exception($"Expected \"{str}\", found \"{new string(this.debugBuf).Trim()}\"");
			}
		}
#endif

		[Conditional("CHECKBOUNDS")]
		private void throwIfUnderflow(int numBytes)
		{
			if (this.bufferOffset + numBytes > this.bufferLimit)
				throw new InvalidOperationException($"Reading {numBytes} bytes would underflow the buffer, which has only {this.bufferLimit - this.bufferOffset} bytes available.");
		}

		/// <summary>
		/// Ensures that the specified amount of bytes are available in the buffer.
		/// </summary>
		/// <param name="numBytes">The number of bytes to acquire.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Acquire(int numBytes)
		{
			if (this.TryAcquire(numBytes) != numBytes)
				throw new EndOfStreamException();
		}

		private int tryAcquireUnlikely(int numBytes)
		{
			if (this.buffer.Length - this.bufferReadAhead < numBytes)
			{
				// We don't have enough space at the end of the buffer.
				int requiredSize = this.bufferLimit - this.bufferOffset + numBytes;
				if (requiredSize > this.buffer.Length)
				{
					// Won't fit at all, resize buffer and copy unprocessed data.
					byte[] newBuffer = new byte[4096 * ((requiredSize + 4095) / 4096)];
					Array.Copy(this.buffer, this.bufferOffset, newBuffer, 0, this.bufferReadAhead - this.bufferOffset);
					this.buffer = newBuffer;
					//Console.WriteLine($"resize input buffer to {newBuffer.Length}");
				}
				else
				{
					// Move unprocessed data to the beginning of the buffer.
					Array.Copy(this.buffer, this.bufferOffset, this.buffer, 0, this.bufferReadAhead - this.bufferOffset);
				}

				this.bufferLimit -= this.bufferOffset;
				this.bufferReadAhead -= this.bufferOffset;
				this.bufferOffset = 0;
			}

			int numBytesRead = this.bufferReadAhead - this.bufferLimit;
			while (numBytesRead < numBytes)
			{
				// Read data until we have all the bytes we need.
				int n = this.Source.Read(this.buffer, this.bufferReadAhead, this.buffer.Length - this.bufferReadAhead);
				if (n == 0)
					break;

				numBytesRead += n;
				this.bufferReadAhead += n;
				this.positionOffset += n;
			}

			numBytes = Math.Min(numBytes, numBytesRead);
			this.bufferLimit += numBytes;
			return numBytes;
		}

		/// <summary>
		/// Tries to read the specified amount of bytes from the stream.
		/// </summary>
		/// <param name="numBytes">The number of bytes to acquire.</param>
		/// <returns>The number of bytes left in the stream. Always equal to or less than the specified number of bytes to acquire.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public int TryAcquire(int numBytes)
		{
#if CHECKBOUNDS
			if (numBytes < 0)
				throw new ArgumentOutOfRangeException(nameof(numBytes));
#endif

			if (this.bufferLimit + numBytes <= this.bufferReadAhead)
			{
				// Enough bytes are still available in the buffer, do nothing.
				this.bufferLimit += numBytes;
				return numBytes;
			}

			return this.tryAcquireUnlikely(numBytes);
		}

		/// <summary>
		/// Tries to read one byte from the stream. If it succeeds, the byte is put back into the buffer.
		/// </summary>
		/// <returns>True, if at least one byte is available. False otherwise.</returns>
		public bool TryPeek()
		{
			if(this.TryAcquire(1) == 0)
				return false;

			this.bufferLimit -= 1;
			return true;
		}

		/// <summary>
		/// Advances the buffer by the specified amount of bytes.
		/// </summary>
		/// <param name="numBytes"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Advance(int numBytes)
		{
#if CHECKBOUNDS
			if (this.bufferOffset + numBytes > this.bufferLimit)
				throw new InvalidOperationException($"Can't advance {numBytes} bytes, the buffer only has {this.bufferLimit - this.bufferOffset} bytes available.");
#endif
			this.bufferOffset += numBytes;
		}

		/// <summary>
		/// Reads data from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dest"></param>
		public void Get<T>(Span<T> dest) where T : struct
		{
			Span<byte> bytes = dest.AsBytes();
			this.throwIfUnderflow(bytes.Length);
			this.Buffer.CopyTo(bytes);
			this.Advance(bytes.Length);
		}

		/// <summary>
		/// Reads data from the stream into the specified destination.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dest"></param>
		public void Read<T>(Span<T> dest) where T : struct
		{
			const int bufferSize = 65536;
			Span<byte> bytes = dest.AsBytes();
			int offset = 0;

			while (offset < bytes.Length)
			{
				int numBytes = Math.Min(bytes.Length - offset, bufferSize);
				this.Acquire(numBytes);
				this.Buffer.Slice(0, numBytes).CopyTo(bytes.Slice(offset, numBytes));
				this.Advance(numBytes);
				offset += numBytes;
			}
		}

		/// <summary>
		/// Reads data from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dest"></param>
		public void Get<T>(T[] dest) where T : struct
		{
			this.Get(new Span<T>(dest));
		}

		/// <summary>
		/// Reads data from the stream into the specified destination.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dest"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Read<T>(T[] dest) where T : struct
		{
			this.Read(new Span<T>(dest));
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool GetBoolean()
		{
#if STREAMDEBUG
			this.debugRead("bool");
#endif
			this.throwIfUnderflow(1);
			return this.buffer[this.bufferOffset++] != 0;
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool ReadBoolean()
		{
#if STREAMDEBUG
			this.debugRead("bool");
#endif
			this.Acquire(1);
			return this.buffer[this.bufferOffset++] != 0;
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public sbyte GetInt8()
		{
#if STREAMDEBUG
			this.debugRead("i8");
#endif
			this.throwIfUnderflow(1);
			return unchecked ((sbyte)this.buffer[this.bufferOffset++]);
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public sbyte ReadInt8()
		{
#if STREAMDEBUG
			this.debugRead("i8");
#endif
			this.Acquire(1);
			return unchecked((sbyte)this.buffer[this.bufferOffset++]);
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public byte GetUInt8()
		{
#if STREAMDEBUG
			this.debugRead("ui8");
#endif
			this.throwIfUnderflow(1);
			return this.buffer[this.bufferOffset++];
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public byte ReadUInt8()
		{
#if STREAMDEBUG
			this.debugRead("ui8");
#endif
			this.Acquire(1);
			return this.buffer[this.bufferOffset++];
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public short GetInt16()
		{
#if STREAMDEBUG
			this.debugRead("i16");
#endif
			this.throwIfUnderflow(2);
			short value = Unsafe.ReadUnaligned<short>(ref this.buffer[this.bufferOffset]);
			this.bufferOffset += 2;
			return value;
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public short ReadInt16()
		{
			this.Acquire(2);
			return this.GetInt16();
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ushort GetUInt16()
		{
#if STREAMDEBUG
			this.debugRead("ui16");
#endif
			this.throwIfUnderflow(2);
			ushort value = Unsafe.ReadUnaligned<ushort>(ref this.buffer[this.bufferOffset]);
			this.bufferOffset += 2;
			return value;
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ushort ReadUInt16()
		{
			this.Acquire(2);
			return this.GetUInt16();
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public int GetInt32()
		{
#if STREAMDEBUG
			this.debugRead("i32");
#endif
			this.throwIfUnderflow(4);
			int value = Unsafe.ReadUnaligned<int>(ref this.buffer[this.bufferOffset]);
			this.bufferOffset += 4;
			return value;
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public int ReadInt32()
		{
			this.Acquire(4);
			return this.GetInt32();
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public uint GetUInt32()
		{
#if STREAMDEBUG
			this.debugRead("ui32");
#endif
			this.throwIfUnderflow(4);
			uint value = Unsafe.ReadUnaligned<uint>(ref this.buffer[this.bufferOffset]);
			this.bufferOffset += 4;
			return value;
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public uint ReadUInt32()
		{
			this.Acquire(4);
			return this.GetUInt32();
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public long GetInt64()
		{
#if STREAMDEBUG
			this.debugRead("i64");
#endif
			this.throwIfUnderflow(8);
			long value = Unsafe.ReadUnaligned<long>(ref this.buffer[this.bufferOffset]);
			this.bufferOffset += 8;
			return value;
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public long ReadInt64()
		{
			this.Acquire(8);
			return this.GetInt64();
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ulong GetUInt64()
		{
#if STREAMDEBUG
			this.debugRead("ui64");
#endif
			this.throwIfUnderflow(8);
			ulong value = Unsafe.ReadUnaligned<ulong>(ref this.buffer[this.bufferOffset]);
			this.bufferOffset += 8;
			return value;
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ulong ReadUInt64()
		{
			this.Acquire(8);
			return this.GetUInt64();
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public float GetFloat32()
		{
#if STREAMDEBUG
			this.debugRead("f32");
#endif
			this.throwIfUnderflow(4);
			float value = Unsafe.ReadUnaligned<float>(ref this.buffer[this.bufferOffset]);
			this.bufferOffset += 4;
			return value;
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public float ReadFloat32()
		{
			this.Acquire(4);
			return this.GetFloat32();
		}


		/// <summary>
		/// Reads a value from the buffer.
		/// The caller is expected to ensure that there is enough data buffered by calling <see cref="Acquire"/> first.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public double GetFloat64()
		{
#if STREAMDEBUG
			this.debugRead("f64");
#endif
			this.throwIfUnderflow(8);
			double value = Unsafe.ReadUnaligned<double>(ref this.buffer[this.bufferOffset]);
			this.bufferOffset += 8;
			return value;
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public double ReadFloat64()
		{
			this.Acquire(8);
			return this.GetFloat64();
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public long ReadVarInt()
		{
			return unchecked((long)this.ReadVarUInt());
		}


		/// <summary>
		/// Reads a value from the stream.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ulong ReadVarUInt()
		{
#if STREAMDEBUG
			this.debugRead("vi");
#endif
			ulong value = 0;
			int shift = 0;

			for (;;)
			{
				this.Acquire(1);
				byte input = this.buffer[this.bufferOffset++];

				if (shift == 56)
				{
					value |= (ulong)input << 56;
					break;
				}

				value |= (input & 0x7FUL) << shift;

				if ((input & 0x80) == 0)
					break;

				shift += 7;
			}

			return value;
		}


		/// <summary>
		/// Reads a string from the stream using the specified encoding.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public string ReadString(Encoding encoding)
		{
#if STREAMDEBUG
			this.debugRead("str");
#endif
			ulong lenPrefix = this.ReadVarUInt();
			if (lenPrefix == 0)
				return null;

			int length = checked((int)(lenPrefix - 1));
			byte[] encodedStr = new byte[length];
			this.Read(encodedStr);
			return encoding.GetString(encodedStr);
		}

		/// <summary>
		/// Reads a string from the stream using the default encoding.
		/// This method invokes <see cref="Acquire"/> to read the required data from the stream.
		/// </summary>
		/// <returns></returns>
		public string ReadString()
		{
			return this.ReadString(Utility.DefaultEncoding);
		}

		/// <summary>
		/// Reads a type from the stream.
		/// </summary>
		public virtual Type ReadType()
		{
#if STREAMDEBUG
			this.debugRead("type");
#endif
			string name = this.ReadString();
			return Type.GetType(name);
		}

		protected override void Dispose(bool disposing)
		{
			if (this.bufferOffset != this.bufferLimit)
				throw new InvalidOperationException("Buffer has unread data.");

			if (!this.leaveOpen)
			{
				this.Source.Dispose();
			}
		}

		#endregion
	}
}
