﻿namespace Iridescence.Serialization.Compiler
{
	public interface IOutputEmitterFactory
	{
		/// <summary>
		/// Creates a new <see cref="IOutputEmitter"/> that emits code using the specified <see cref="EmitterBase"/>.
		/// </summary>
		/// <param name="emit"></param>
		/// <returns></returns>
		IOutputEmitter CreateOutputEmitter(EmitterBase emit);
	}
}