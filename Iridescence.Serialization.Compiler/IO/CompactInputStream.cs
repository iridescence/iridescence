﻿using System;
using System.IO;
using System.Reflection;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Augments the <see cref="BufferedInputStream"/> class using a <see cref="CompactMetadataReader"/> to read metadata.
	/// </summary>
	public class CompactInputStream : BufferedInputStream, IMetadataReader, ISerializationInfoReader
	{
		#region Fields

		private readonly CompactMetadataReader metadataReader;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public CompactInputStream(Stream stream, int initialCapacity, bool leaveOpen, CompactMetadataOptions options, Func<AssemblyName, Assembly> assemblyResolver)
			: base(stream, initialCapacity, leaveOpen)
		{
			this.metadataReader = new CompactMetadataReader(options, assemblyResolver);
		}

		#endregion

		#region Methods

		public override Type ReadType()
		{
			return this.metadataReader.ReadType(this);
		}

		public AssemblyName ReadAssemblyName()
		{
			return this.metadataReader.ReadAssemblyName(this);
		}

		public MemberInfo ReadMember()
		{
			return this.metadataReader.ReadMember(this);
		}

		public void ReadSerializationInfoLayout(out Type objectType, out SerializationInfoMember[] members)
		{
			objectType = this.ReadType();

			int numMembers = checked((int)this.ReadVarInt());
			members = new SerializationInfoMember[numMembers];

			for (int i = 0; i < numMembers; ++i)
			{
				members[i].Type = this.ReadType();
			}

			for (int i = 0; i < numMembers; ++i)
			{
				members[i].Name = this.ReadString();
			}
		}

		#endregion
	}
}
