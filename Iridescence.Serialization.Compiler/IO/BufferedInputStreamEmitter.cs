﻿using System;
using System.Reflection;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements the emit interface for <see cref="BufferedInputStream"/>.
	/// </summary>
	public class BufferedInputStreamEmitter : IInputEmitter, IEmitOwner
	{
		#region Fields

		private readonly EmitterBase emit;

		private readonly MethodInfo acquireMethod;
		private readonly MethodInfo getBooleanMethod;
		private readonly MethodInfo readBooleanMethod;
		private readonly MethodInfo getInt8Method;
		private readonly MethodInfo readInt8Method;
		private readonly MethodInfo getUInt8Method;
		private readonly MethodInfo readUInt8Method;
		private readonly MethodInfo getInt16Method;
		private readonly MethodInfo readInt16Method;
		private readonly MethodInfo getUInt16Method;
		private readonly MethodInfo readUInt16Method;
		private readonly MethodInfo getInt32Method;
		private readonly MethodInfo readInt32Method;
		private readonly MethodInfo getUInt32Method;
		private readonly MethodInfo readUInt32Method;
		private readonly MethodInfo getInt64Method;
		private readonly MethodInfo readInt64Method;
		private readonly MethodInfo getUInt64Method;
		private readonly MethodInfo readUInt64Method;
		private readonly MethodInfo getFloat32Method;
		private readonly MethodInfo readFloat32Method;
		private readonly MethodInfo getFloat64Method;
		private readonly MethodInfo readFloat64Method;
		private readonly MethodInfo readVarIntMethod;
		private readonly MethodInfo readVarUIntMethod;
		private readonly MethodInfo getArrayMethod;
		private readonly MethodInfo readArrayMethod;
		private readonly MethodInfo getSpanMethod;
		private readonly MethodInfo readSpanMethod;
		private readonly MethodInfo readStringMethod;
		private readonly MethodInfo readTypeMethod;

		private static readonly MethodInfo typeGetTypeMethod = typeof(Type).GetMethod(nameof(System.Type.GetType), BindingFlags.Public | BindingFlags.Static, null, new[] {typeof(string)}, null);

		#endregion

		#region Properties

		public Type Type { get; }

		public bool IsBuffered => true;

		EmitterBase IEmitOwner.Emit => this.emit;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="BufferedInputStreamEmitter"/> class. 
		/// </summary>
		public BufferedInputStreamEmitter(Type type, EmitterBase emit)
		{
			this.emit = emit;
			this.Type = type;

			this.acquireMethod = this.Type.FindMethod(nameof(BufferedInputStream.Acquire), new[] {typeof(int)});
			this.getBooleanMethod = this.Type.FindMethod(nameof(BufferedInputStream.GetBoolean), Type.EmptyTypes);
			this.readBooleanMethod = this.Type.FindMethod(nameof(BufferedInputStream.ReadBoolean), Type.EmptyTypes);
			this.getInt8Method = this.Type.FindMethod(nameof(BufferedInputStream.GetInt8), Type.EmptyTypes);
			this.readInt8Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadInt8), Type.EmptyTypes);
			this.getUInt8Method = this.Type.FindMethod(nameof(BufferedInputStream.GetUInt8), Type.EmptyTypes);
			this.readUInt8Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadUInt8), Type.EmptyTypes);
			this.getInt16Method = this.Type.FindMethod(nameof(BufferedInputStream.GetInt16), Type.EmptyTypes);
			this.readInt16Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadInt16), Type.EmptyTypes);
			this.getUInt16Method = this.Type.FindMethod(nameof(BufferedInputStream.GetUInt16), Type.EmptyTypes);
			this.readUInt16Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadUInt16), Type.EmptyTypes);
			this.getInt32Method = this.Type.FindMethod(nameof(BufferedInputStream.GetInt32), Type.EmptyTypes);
			this.readInt32Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadInt32), Type.EmptyTypes);
			this.getUInt32Method = this.Type.FindMethod(nameof(BufferedInputStream.GetUInt32), Type.EmptyTypes);
			this.readUInt32Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadUInt32), Type.EmptyTypes);
			this.getInt64Method = this.Type.FindMethod(nameof(BufferedInputStream.GetInt64), Type.EmptyTypes);
			this.readInt64Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadInt64), Type.EmptyTypes);
			this.getUInt64Method = this.Type.FindMethod(nameof(BufferedInputStream.GetUInt64), Type.EmptyTypes);
			this.readUInt64Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadUInt64), Type.EmptyTypes);
			this.getFloat32Method = this.Type.FindMethod(nameof(BufferedInputStream.GetFloat32), Type.EmptyTypes);
			this.readFloat32Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadFloat32), Type.EmptyTypes);
			this.getFloat64Method = this.Type.FindMethod(nameof(BufferedInputStream.GetFloat64), Type.EmptyTypes);
			this.readFloat64Method = this.Type.FindMethod(nameof(BufferedInputStream.ReadFloat64), Type.EmptyTypes);
			this.readVarIntMethod = this.Type.FindMethod(nameof(BufferedInputStream.ReadVarInt), Type.EmptyTypes);
			this.readVarUIntMethod = this.Type.FindMethod(nameof(BufferedInputStream.ReadVarUInt), Type.EmptyTypes);
			this.readStringMethod = this.Type.FindMethod(nameof(BufferedInputStream.ReadString), Type.EmptyTypes);
			this.readTypeMethod = this.Type.GetMethod(nameof(BufferedInputStream.ReadType), Type.EmptyTypes);
			this.getArrayMethod = this.Type.FindGenericMethod(nameof(BufferedInputStream.Get), 1, (t, i) => t.IsArray);
			this.readArrayMethod = this.Type.FindGenericMethod(nameof(BufferedInputStream.Read), 1, (t, i) => t.IsArray);
			this.getSpanMethod = this.Type.FindGenericMethod(nameof(BufferedInputStream.Get), 1, (t, i) => t == typeof(Span<>));
			this.readSpanMethod = this.Type.FindGenericMethod(nameof(BufferedInputStream.Read), 1, (t, i) => t == typeof(Span<>));
		}

		#endregion

		#region Methods

		public void Acquire()
		{
			this.emit.Call(this.acquireMethod);
		}

		public void ReadBoolean(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readBooleanMethod : this.getBooleanMethod);
		}

		public void ReadInt8(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readInt8Method : this.getInt8Method);
		}

		public void ReadUInt8(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readUInt8Method : this.getUInt8Method);
		}

		public void ReadInt16(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readInt16Method : this.getInt16Method);
		}

		public void ReadUInt16(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readUInt16Method : this.getUInt16Method);
		}

		public void ReadInt32(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readInt32Method : this.getInt32Method);
		}

		public void ReadUInt32(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readUInt32Method : this.getUInt32Method);
		}

		public void ReadInt64(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readInt64Method : this.getInt64Method);
		}

		public void ReadUInt64(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readUInt64Method : this.getUInt64Method);
		}

		public void ReadFloat32(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readFloat32Method : this.getFloat32Method);
		}

		public void ReadFloat64(bool acquire = true)
		{
			this.emit.Call(acquire ? this.readFloat64Method : this.getFloat64Method);
		}

		public void ReadVarInt()
		{
			this.emit.Call(this.readVarIntMethod);
		}

		public void ReadVarUInt()
		{
			this.emit.Call(this.readVarUIntMethod);
		}

		public void ReadArray(Type elementType, bool acquire = true)
		{
			this.emit.Call((acquire ? this.readArrayMethod : this.getArrayMethod).MakeGenericMethod(elementType));
		}

		public void ReadSpan(Type elementType, bool acquire = true)
		{
			this.emit.Call((acquire ? this.readSpanMethod : this.getSpanMethod).MakeGenericMethod(elementType));
		}

		public void ReadString()
		{
			this.emit.Call(this.readStringMethod);
		}

		public void ReadType()
		{
			if (this.readTypeMethod != null)
			{
				this.emit.Call(this.readTypeMethod);
			}
			else
			{
				this.ReadString();
				this.emit.Call(typeGetTypeMethod);
			}
		}

		#endregion
	}
}
