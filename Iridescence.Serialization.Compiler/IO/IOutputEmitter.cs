﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents the abstract writer stream code generation API.
	/// </summary>
	public interface IOutputEmitter
	{
		#region Properties

		/// <summary>
		/// Gets the type of the stream.
		/// </summary>
		Type Type { get; }

		/// <summary>
		/// Whether the stream is buffered. If this is set to false, the <see cref="Reserve"/> method and the "reserve" parameters have no effect.
		/// </summary>
		bool IsBuffered { get; }

		#endregion

		#region Methods

		/// <summary>
		/// Pops an integer N from the stack and reserves N bytes in the internal buffer.
		/// </summary>
		void Reserve();

		/// <summary>
		/// Pops a boolean from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteBoolean(bool reserve = true);

		/// <summary>
		/// Pops a signed byte from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteInt8(bool reserve = true);

		/// <summary>
		/// Pops an unsigned from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteUInt8(bool reserve = true);

		/// <summary>
		/// Pops a signed 16-bit integer from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteInt16(bool reserve = true);

		/// <summary>
		/// Pops an unsigned 16-bit integer from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteUInt16(bool reserve = true);

		/// <summary>
		/// Pops a signed 32-bit integer from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteInt32(bool reserve = true);

		/// <summary>
		/// Pops an unsigned 32-bit integer from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteUInt32(bool reserve = true);

		/// <summary>
		/// Pops a signed 64-bit integer from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteInt64(bool reserve = true);

		/// <summary>
		/// Pops an unsigned 64-bit integer from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteUInt64(bool reserve = true);

		/// <summary>
		/// Pops a single-precision floating-point number from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteFloat32(bool reserve = true);
	
		/// <summary>
		/// Pops a double-precision floating-point number from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteFloat64(bool reserve = true);

		/// <summary>
		/// Pops an integer from the stack and writes it to the internal buffer using a variable-length encoding. Reserves the required number of bytes beforehand.
		/// </summary>
		void WriteVarInt();

		/// <summary>
		/// Pops an unsigned integer from the stack and writes it to the internal buffer using a variable-length encoding. Reserves the required number of bytes beforehand.
		/// </summary>
		void WriteVarUInt();

		/// <summary>
		/// Pops an array from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="elementType">The element type. Must be a primitive.</param>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteArray(Type elementType, bool reserve = true);
	
		/// <summary>
		/// Pops an span from the stack and writes it to the internal buffer, optionally invoking <see cref="Reserve"/> to allocate space in the internal buffer.
		/// </summary>
		/// <param name="elementType">The element type. Must be a primitive.</param>
		/// <param name="reserve">Whether to reserve the required number of bytes.</param>
		void WriteSpan(Type elementType, bool reserve = true);

		/// <summary>
		/// Pops a string from the stack and writes it to the internal buffer using a variable-length encoding. Reserves the required number of bytes beforehand.
		/// </summary>
		void WriteString();
		
		/// <summary>
		/// Pops a type from the stack and writes it to the internal buffer using a variable-length encoding. Reserves the required number of bytes beforehand.
		/// </summary>
		void WriteType();

		#endregion
	}
}
