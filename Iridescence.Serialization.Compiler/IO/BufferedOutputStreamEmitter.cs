﻿using System;
using System.Reflection;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements the emit interface for <see cref="BufferedOutputStream"/>.
	/// </summary>
	public class BufferedOutputStreamEmitter : IOutputEmitter, IEmitOwner
	{
		#region Fields

		private readonly EmitterBase emit;

		private readonly MethodInfo reserveMethod;
		private readonly MethodInfo putBooleanMethod;
		private readonly MethodInfo writeBooleanMethod;
		private readonly MethodInfo putInt8Method;
		private readonly MethodInfo writeInt8Method;
		private readonly MethodInfo putUInt8Method;
		private readonly MethodInfo writeUInt8Method;
		private readonly MethodInfo putInt16Method;
		private readonly MethodInfo writeInt16Method;
		private readonly MethodInfo putUInt16Method;
		private readonly MethodInfo writeUInt16Method;
		private readonly MethodInfo putInt32Method;
		private readonly MethodInfo writeInt32Method;
		private readonly MethodInfo putUInt32Method;
		private readonly MethodInfo writeUInt32Method;
		private readonly MethodInfo putInt64Method;
		private readonly MethodInfo writeInt64Method;
		private readonly MethodInfo putUInt64Method;
		private readonly MethodInfo writeUInt64Method;
		private readonly MethodInfo putFloat32Method;
		private readonly MethodInfo writeFloat32Method;
		private readonly MethodInfo putFloat64Method;
		private readonly MethodInfo writeFloat64Method;
		private readonly MethodInfo writeVarIntMethod;
		private readonly MethodInfo writeVarUIntMethod;
		private readonly MethodInfo putArrayMethod;
		private readonly MethodInfo writeArrayMethod;
		private readonly MethodInfo putSpanMethod;
		private readonly MethodInfo writeSpanMethod;
		private readonly MethodInfo writeStringMethod;
		private readonly MethodInfo writeTypeMethod;

		private static readonly MethodInfo typeGetAssemblyQualifiedNameMethod = typeof(Type).GetProperty(nameof(System.Type.AssemblyQualifiedName))?.GetGetMethod() ?? throw new MissingMethodException();

		#endregion

		#region Properties

		public Type Type { get; }

		public bool IsBuffered => true;
		
		EmitterBase IEmitOwner.Emit => this.emit;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="BufferedOutputStreamEmitter"/> class. 
		/// </summary>
		public BufferedOutputStreamEmitter(Type type, EmitterBase emit)
		{
			this.emit = emit;
			this.Type = type;

			this.reserveMethod = this.Type.FindMethod(nameof(BufferedOutputStream.Reserve));
			this.putBooleanMethod = this.Type.FindMethod(nameof(BufferedOutputStream.PutBoolean));
			this.writeBooleanMethod = this.Type.FindMethod(nameof(BufferedOutputStream.WriteBoolean));
			this.putInt8Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutInt8));
			this.writeInt8Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteInt8));
			this.putUInt8Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutUInt8));
			this.writeUInt8Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteUInt8));
			this.putInt16Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutInt16));
			this.writeInt16Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteInt16));
			this.putUInt16Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutUInt16));
			this.writeUInt16Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteUInt16));
			this.putInt32Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutInt32));
			this.writeInt32Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteInt32));
			this.putUInt32Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutUInt32));
			this.writeUInt32Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteUInt32));
			this.putInt64Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutInt64));
			this.writeInt64Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteInt64));
			this.putUInt64Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutUInt64));
			this.writeUInt64Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteUInt64));
			this.putFloat32Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutFloat32));
			this.writeFloat32Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteFloat32));
			this.putFloat64Method = this.Type.FindMethod(nameof(BufferedOutputStream.PutFloat64));
			this.writeFloat64Method = this.Type.FindMethod(nameof(BufferedOutputStream.WriteFloat64));
			this.writeVarIntMethod = this.Type.FindMethod(nameof(BufferedOutputStream.WriteVarInt));
			this.writeVarUIntMethod = this.Type.FindMethod(nameof(BufferedOutputStream.WriteVarUInt));
			this.writeStringMethod = this.Type.FindMethod(nameof(BufferedOutputStream.WriteString), new[] {typeof(string)});
			this.writeTypeMethod = this.Type.GetMethod(nameof(BufferedOutputStream.WriteType));
			this.putArrayMethod = this.Type.FindGenericMethod(nameof(BufferedOutputStream.Put), 1, (t, i) => t.IsArray);
			this.writeArrayMethod = this.Type.FindGenericMethod(nameof(BufferedOutputStream.Write), 1, (t, i) => t.IsArray);
			this.putSpanMethod = this.Type.FindGenericMethod(nameof(BufferedOutputStream.Put), 1, (t, i) => t == typeof(ReadOnlySpan<>));
			this.writeSpanMethod = this.Type.FindGenericMethod(nameof(BufferedOutputStream.Write), 1, (t, i) => t == typeof(ReadOnlySpan<>));
		}

		#endregion

		#region Methods

		public void Reserve()
		{
			this.emit.Call(this.reserveMethod);
		}

		public void WriteBoolean(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeBooleanMethod : this.putBooleanMethod);
		}

		public void WriteInt8(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeInt8Method : this.putInt8Method);
		}

		public void WriteUInt8(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeUInt8Method : this.putUInt8Method);
		}

		public void WriteInt16(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeInt16Method : this.putInt16Method);
		}

		public void WriteUInt16(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeUInt16Method : this.putUInt16Method);
		}

		public void WriteInt32(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeInt32Method : this.putInt32Method);
		}

		public void WriteUInt32(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeUInt32Method : this.putUInt32Method);
		}

		public void WriteInt64(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeInt64Method : this.putInt64Method);
		}

		public void WriteUInt64(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeUInt64Method : this.putUInt64Method);
		}

		public void WriteFloat32(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeFloat32Method : this.putFloat32Method);
		}

		public void WriteFloat64(bool reserve = true)
		{
			this.emit.Call(reserve ? this.writeFloat64Method : this.putFloat64Method);
		}

		public void WriteVarInt()
		{
			this.emit.Call(this.writeVarIntMethod);
		}

		public void WriteVarUInt()
		{
			this.emit.Call(this.writeVarUIntMethod);
		}

		public void WriteArray(Type elementType, bool reserve = true)
		{
			this.emit.Call((reserve ? this.writeArrayMethod : this.putArrayMethod).MakeGenericMethod(elementType));
		}

		public void WriteSpan(Type elementType, bool reserve = true)
		{
			this.emit.Call((reserve ? this.writeSpanMethod : this.putSpanMethod).MakeGenericMethod(elementType));
		}

		public void WriteString()
		{
			this.emit.Call(this.writeStringMethod);
		}

		public void WriteType()
		{
			if (this.writeTypeMethod != null)
			{
				this.emit.CallVirtual(this.writeTypeMethod);
			}
			else
			{
				this.emit.Call(typeGetAssemblyQualifiedNameMethod);
				this.WriteString();
			}
		}

		#endregion
	}
}
