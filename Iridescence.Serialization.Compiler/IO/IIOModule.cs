﻿using System;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Factory for <see cref="IOutputEmitter"/> and <see cref="IInputEmitter"/>.
	/// </summary>
	public interface IIOModule
	{
		IOutputEmitterFactory CreateOutput(Type outputType);

		IInputEmitterFactory CreateInput(Type inputType);
	}
}
