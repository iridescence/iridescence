﻿namespace Iridescence.Serialization.Compiler
{
	public interface IInputEmitterFactory
	{
		/// <summary>
		/// Creates a new <see cref="IInputEmitter"/> that emits code using the specified <see cref="EmitterBase"/>.
		/// </summary>
		/// <param name="emit"></param>
		/// <returns></returns>
		IInputEmitter CreateInputEmitter(EmitterBase emit);
	}
}