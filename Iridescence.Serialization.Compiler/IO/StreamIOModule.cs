﻿using System;
using System.IO;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements an <see cref="IIOModule"/> that creates <see cref="BufferedInputStreamEmitter"/> and <see cref="BufferedOutputStreamEmitter"/>.
	/// </summary>
	public class StreamIOModule : IIOModule
	{
		public static StreamIOModule Instance { get; } = new StreamIOModule();

		public IOutputEmitterFactory CreateOutput(Type outputType)
		{
			if (!typeof(Stream).IsAssignableFrom(outputType))
				throw new ArgumentException("The output type must be compatible with System.IO.Stream.", nameof(outputType));

			return new ForType(outputType);
		}

		public IInputEmitterFactory CreateInput(Type inputType)
		{
			if (!typeof(Stream).IsAssignableFrom(inputType))
				throw new ArgumentException("The input type must be compatible with System.IO.Stream.", nameof(inputType));

			return new ForType(inputType);
		}

		private sealed class ForType : IOutputEmitterFactory, IInputEmitterFactory
		{
			private readonly Type type;

			public ForType(Type type)
			{
				this.type = type;
			}

			public IOutputEmitter CreateOutputEmitter(EmitterBase emit)
			{
				return new StreamEmitter(this.type, emit);
			}

			public IInputEmitter CreateInputEmitter(EmitterBase emit)
			{
				return new StreamEmitter(this.type, emit);
			}
		}
	}
}