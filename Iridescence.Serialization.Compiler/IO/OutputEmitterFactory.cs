﻿using System;

namespace Iridescence.Serialization.Compiler
{
	public sealed class OutputEmitterFactory : IOutputEmitterFactory
	{
		private readonly IOutputEmitterFactory factory;

		public Type OutputType { get; }

		public OutputEmitterFactory(IIOModule module, Type type)
		{
			this.factory = module.CreateOutput(type);
			this.OutputType = type;
		}

		public IOutputEmitter CreateOutputEmitter(EmitterBase emit)
		{
			return this.factory.CreateOutputEmitter(emit);
		}
	}
}
