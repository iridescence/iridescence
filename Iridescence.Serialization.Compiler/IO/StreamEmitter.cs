﻿using System;
using System.IO;
using System.Reflection;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Provides IO emit functionality for a simple <see cref="Stream"/>.
	/// </summary>
	public class StreamEmitter : IOutputEmitter, IInputEmitter, IEmitOwner
	{
		#region Fields

		private readonly EmitterBase emit;

		private static readonly MethodInfo writeByteMethod = typeof(Stream).FindMethod(nameof(Stream.WriteByte));
		private static readonly MethodInfo readByteOrThrowMethod = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadByteOrThrow));
		private static readonly MethodInfo writeInt16Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteInt16L));
		private static readonly MethodInfo readInt16Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadInt16L));
		private static readonly MethodInfo writeUInt16Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteUInt16L));
		private static readonly MethodInfo readUInt16Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadUInt16L));
		private static readonly MethodInfo writeInt32Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteInt32L));
		private static readonly MethodInfo readInt32Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadInt32L));
		private static readonly MethodInfo writeUInt32Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteUInt32L));
		private static readonly MethodInfo readUInt32Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadUInt32L));
		private static readonly MethodInfo writeInt64Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteInt64L));
		private static readonly MethodInfo readInt64Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadInt64L));
		private static readonly MethodInfo writeUInt64Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteUInt64L));
		private static readonly MethodInfo readUInt64Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadUInt64L));
		private static readonly MethodInfo writeFloat32Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteFloat32L));
		private static readonly MethodInfo readFloat32Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadFloat32L));
		private static readonly MethodInfo writeFloat64Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteFloat64L));
		private static readonly MethodInfo readFloat64Method = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadFloat64L));
		private static readonly MethodInfo writeVarIntMethod = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteInt64V));
		private static readonly MethodInfo readVarIntMethod = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadInt64V));
		private static readonly MethodInfo writeVarUIntMethod = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.WriteUInt64V));
		private static readonly MethodInfo readVarUIntMethod = typeof(StreamExtensions).FindMethod(nameof(StreamExtensions.ReadUInt64V));
		private static readonly MethodInfo writeArrayMethod = typeof(StreamEmitter).FindMethod(nameof(writeArray));
		private static readonly MethodInfo readArrayMethod = typeof(StreamEmitter).FindMethod(nameof(readArray));
		private static readonly MethodInfo writeStringMethod = typeof(StreamEmitter).FindMethod(nameof(writeString));
		private static readonly MethodInfo readStringMethod = typeof(StreamEmitter).FindMethod(nameof(readString));
		private static readonly MethodInfo writeTypeMethod = typeof(StreamEmitter).FindMethod(nameof(writeType));
		private static readonly MethodInfo readTypeMethod = typeof(StreamEmitter).FindMethod(nameof(readType));
		private static readonly MethodInfo writeSpanMethod = typeof(StreamEmitter).FindMethod(nameof(writeSpan));
		private static readonly MethodInfo readSpanMethod = typeof(StreamEmitter).FindMethod(nameof(readSpan));

		#endregion

		#region Properties

		public Type Type { get; }

		public bool IsBuffered => false;

		EmitterBase IEmitOwner.Emit => this.emit;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamEmitter"/> class. 
		/// </summary>
		public StreamEmitter(Type type, EmitterBase emit)
		{
			this.emit = emit;
			this.Type = type;
		}

		#endregion

		#region Methods

		private static void writeArray<T>(Stream stream, T[] array)
			where T : struct
		{
			writeSpan(stream, new ReadOnlySpan<T>(array));
		}

		private static void readArray<T>(Stream stream, T[] array)
			where T : struct
		{
			readSpan(stream, new Span<T>(array));
		}

		private static void writeSpan<T>(Stream stream, ReadOnlySpan<T> span)
			where T : struct
		{
			ReadOnlySpan<byte> bytes = span.AsBytes();
			byte[] buffer = bytes.ToArray();
			stream.Write(buffer, 0, buffer.Length);
		}

		private static void readSpan<T>(Stream stream, Span<T> span)
			where T : struct
		{
			Span<byte> bytes = span.AsBytes();
			byte[] buffer = new byte[Math.Max(bytes.Length, 4096)];
			while (bytes.Length > 0)
			{
				int n = stream.Read(buffer, 0, Math.Min(bytes.Length, buffer.Length));
				if (n == 0)
					throw new EndOfStreamException();
				new Span<byte>(buffer, 0, n).CopyTo(bytes.Slice(0, n));
				bytes = bytes.Slice(n);
			}
		}

		private static void writeString(Stream stream, string str)
		{
			stream.WriteStringV(str, Utility.DefaultEncoding);
		}

		private static string readString(Stream stream)
		{
			return stream.ReadStringV(Utility.DefaultEncoding);
		}

		private static void writeType(Stream stream, Type type)
		{
			if (stream is IMetadataWriter mw)
			{
				mw.WriteMember(type);
			}
			else
			{
				stream.WriteStringV(type?.AssemblyQualifiedName);
			}
		}

		private static Type readType(Stream stream)
		{
			if (stream is IMetadataReader mr)
			{
				return (Type)mr.ReadMember();
			}
			else
			{
				string name = stream.ReadStringV();
				if (name == null)
					return null;
				return Type.GetType(name, true);
			}
		}

		public void Reserve()
		{
			// Pop stream and number of bytes.
			this.emit.Pop();
			this.emit.Pop();
		}

		public void Acquire()
		{
			// Pop stream and number of bytes.
			this.emit.Pop();
			this.emit.Pop();
		}

		public void WriteBoolean(bool reserve = true)
		{
			this.emit.LoadConstant(0);
			this.emit.CompareGreaterThanUnsigned();
			this.emit.CallVirtual(writeByteMethod);
		}

		public void ReadBoolean(bool acquire = true)
		{
			this.emit.Call(readByteOrThrowMethod);
			this.emit.LoadConstant(0);
			this.emit.CompareGreaterThanUnsigned();
		}

		public void WriteInt8(bool reserve = true)
		{
			this.emit.ConvertToUInt8();
			this.emit.CallVirtual(writeByteMethod);
		}

		public void ReadInt8(bool acquire = true)
		{
			this.emit.Call(readByteOrThrowMethod);
			this.emit.ConvertToInt8();
		}

		public void WriteUInt8(bool reserve = true)
		{
			this.emit.CallVirtual(writeByteMethod);
		}

		public void ReadUInt8(bool acquire = true)
		{
			this.emit.Call(readByteOrThrowMethod);
		}

		public void WriteInt16(bool reserve = true)
		{
			this.emit.Call(writeInt16Method);
		}

		public void ReadInt16(bool acquire = true)
		{
			this.emit.Call(readInt16Method);
		}

		public void WriteUInt16(bool reserve = true)
		{
			this.emit.Call(writeUInt16Method);
		}

		public void ReadUInt16(bool acquire = true)
		{
			this.emit.Call(readUInt16Method);
		}

		public void WriteInt32(bool reserve = true)
		{
			this.emit.Call(writeInt32Method);
		}

		public void ReadInt32(bool acquire = true)
		{
			this.emit.Call(readInt32Method);
		}

		public void WriteUInt32(bool reserve = true)
		{
			this.emit.Call(writeUInt32Method);
		}

		public void ReadUInt32(bool acquire = true)
		{
			this.emit.Call(readUInt32Method);
		}

		public void WriteInt64(bool reserve = true)
		{
			this.emit.Call(writeInt64Method);
		}

		public void ReadInt64(bool acquire = true)
		{
			this.emit.Call(readInt64Method);
		}

		public void WriteUInt64(bool reserve = true)
		{
			this.emit.Call(writeUInt64Method);
		}

		public void ReadUInt64(bool acquire = true)
		{
			this.emit.Call(readUInt64Method);
		}

		public void WriteFloat32(bool reserve = true)
		{
			this.emit.Call(writeFloat32Method);
		}

		public void ReadFloat32(bool acquire = true)
		{
			this.emit.Call(readFloat32Method);
		}

		public void WriteFloat64(bool reserve = true)
		{
			this.emit.Call(writeFloat64Method);
		}

		public void ReadFloat64(bool acquire = true)
		{
			this.emit.Call(readFloat64Method);
		}

		public void WriteVarInt()
		{
			this.emit.Call(writeVarIntMethod);
		}

		public void ReadVarInt()
		{
			this.emit.Call(readVarIntMethod);
		}

		public void WriteVarUInt()
		{
			this.emit.Call(writeVarUIntMethod);
		}

		public void ReadVarUInt()
		{
			this.emit.Call(readVarUIntMethod);
		}

		public void WriteArray(Type elementType, bool reserve = true)
		{
			this.emit.Call(writeArrayMethod.MakeGenericMethod(elementType));
		}

		public void ReadArray(Type elementType, bool acquire = true)
		{
			this.emit.Call(readArrayMethod.MakeGenericMethod(elementType));
		}

		public void WriteSpan(Type elementType, bool reserve = true)
		{
			this.emit.Call(writeSpanMethod.MakeGenericMethod(elementType));
		}

		public void ReadSpan(Type elementType, bool acquire = true)
		{
			this.emit.Call(readSpanMethod.MakeGenericMethod(elementType));
		}

		public void WriteString()
		{
			this.emit.Call(writeStringMethod);
		}

		public void ReadString()
		{
			this.emit.Call(readStringMethod);
		}

		public void WriteType()
		{
			this.emit.Call(writeTypeMethod);
		}

		public void ReadType()
		{
			this.emit.Call(readTypeMethod);
		}

		#endregion
	}
}
