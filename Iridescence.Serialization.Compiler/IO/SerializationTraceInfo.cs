﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Holds information about serialization trace entries.
	/// </summary>
	public readonly struct SerializationTraceInfo
	{
		/// <summary>
		/// The member being (de)serialized.
		/// </summary>
		public readonly SerializationMember Member;

		/// <summary>
		/// The serialization direction.
		/// </summary>
		public readonly SerializationDirection Direction;

		/// <summary>
		/// The serialization mode.
		/// </summary>
		public readonly SerializationMode Mode;

		/// <summary>
		/// A comment.
		/// </summary>
		public readonly string Comment;

		public SerializationTraceInfo(SerializationMember member, SerializationDirection direction, SerializationMode mode, string comment = null)
		{
			this.Member = member;
			this.Direction = direction;
			this.Mode = mode;
			this.Comment = comment;
		}
	}
}