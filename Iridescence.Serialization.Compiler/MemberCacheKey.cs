﻿using System;
using System.Collections.Immutable;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Internal member caching key.
	/// </summary>
	[Serializable]
	public readonly struct MemberCacheKey : IEquatable<MemberCacheKey>
	{
		#region Fields

		private readonly Type type;
		private readonly ISerializationMemberAttribute[] attributes;
		private readonly int typeAttributeEnd;
		private readonly int hash;

		#endregion

		#region Properties

		public Type Type => this.type;

		public ReadOnlySpan<ISerializationMemberAttribute> TypeAttributes => new ReadOnlySpan<ISerializationMemberAttribute>(this.attributes, 0, this.typeAttributeEnd);

		public ReadOnlySpan<ISerializationMemberAttribute> MemberAttributes => new ReadOnlySpan<ISerializationMemberAttribute>(this.attributes, this.typeAttributeEnd, this.attributes.Length - this.typeAttributeEnd);

		#endregion

		#region Constructors

		public MemberCacheKey(SerializationMember member)
		{
			this.type = member.Type;

			int attrCount = member.TypeAttributes.Length + member.MemberAttributes.Length;
			int attrIndex = 0;

			if (attrCount == 0)
				this.attributes = Array.Empty<ISerializationMemberAttribute>();
			else
				this.attributes = new ISerializationMemberAttribute[attrCount];

			HashCode h = new HashCode();
			h.Add(this.type);

			foreach (var attr in member.TypeAttributes)
			{
				if (attr is IAttributeCacheControl cacheControl && !cacheControl.IncludeInCache)
					continue;

				this.attributes[attrIndex++] = attr;
				h.Add(attr);
			}

			this.typeAttributeEnd = attrIndex;
			h.Add(this.typeAttributeEnd);

			foreach (var attr in member.MemberAttributes)
			{
				if (attr is IAttributeCacheControl cacheControl && !cacheControl.IncludeInCache)
					continue;

				this.attributes[attrIndex++] = attr;
				h.Add(attr);
			}

			if (attrIndex != attrCount)
				Array.Resize(ref this.attributes, attrIndex);

			this.hash = h.ToHashCode();
		}

		#endregion

		#region Methods

		public SerializationMember GetMember(string name = null)
		{
			return new SerializationMember(
				this.type,
				ImmutableArray.Create(this.attributes, 0, this.typeAttributeEnd),
				ImmutableArray.Create(this.attributes, this.typeAttributeEnd, this.attributes.Length - this.typeAttributeEnd),
				name);
		}

		public bool Equals(in MemberCacheKey other)
		{
			if (this.type != other.type)
				return false;

			if (this.attributes.Length != other.attributes.Length)
				return false;

			if (this.typeAttributeEnd != other.typeAttributeEnd)
				return false;

			for (int i = 0; i < this.attributes.Length; ++i)
			{
				if (!Equals(this.attributes[i], other.attributes[i]))
					return false;
			}

			return true;
		}

		bool IEquatable<MemberCacheKey>.Equals(MemberCacheKey other) => this.Equals(other);

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is MemberCacheKey key && this.Equals(key);
		}

		public override int GetHashCode()
		{
			return this.hash;
		}

		public override string ToString()
		{
			return this.type.ToString();
		}

		#endregion
	}
}
