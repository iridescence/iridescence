﻿using System;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Target class for compiled <see cref="IAugmentation"/> methods.
	/// </summary>
	internal sealed class EmittedAugmentations : EmittedMethod
	{
		#region Constructors

		public EmittedAugmentations(object[] externalValues)
			: base(externalValues)
		{

		}

		#endregion

		#region Methods

		public static ICapturedValue Compile(ICompilationUnit unit, Type streamType, Type objType, Action<AugmentationEmitter> body)
		{
			Type delType = typeof(AugmentationDelegate<,>).MakeGenericType(streamType, objType);

			CompilableEmitter emit = unit.CreateMethod(MethodSignature.FromDelegate(delType).WithThis(typeof(EmittedAugmentations)), $"Augmentations-{Guid.NewGuid()}");
			
			body(new AugmentationEmitter(emit,
				mode =>
				{
					if (mode == LoadMode.Address)
					{
						emit.LoadArgument(2);
					}
					else
					{
						emit.LoadArgument(2);
						emit.LoadIndirect(objType);
					}
				}));

			emit.Return();

			return new LazyCapturedValue(() => emit.GetCompiledMethod().CreateDelegate(delType, new EmittedAugmentations(emit.CompileCapturedValues())), delType);
		}

		#endregion
	}
}