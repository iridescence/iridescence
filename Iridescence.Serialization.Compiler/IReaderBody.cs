﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Represents an abstract reader build step.
	/// </summary>
	public interface IReaderBody : IFragment<ReaderEmitter>
	{
		/// <summary>
		/// Gets the number of bytes that need to be acquired from the stream before the code is executed.
		/// </summary>
		int ByteSize { get; }

		/// <summary>
		/// Gets a value that indicates whether the body requires a deferred execution model.
		/// </summary>
		bool IsDeferred { get; }

		/// <summary>
		/// Gets a value that indicates whether the pre- and post-body method augmentations are handled by the implementation.
		/// </summary>
		bool CustomAugmentationHandling { get; }
	}
}