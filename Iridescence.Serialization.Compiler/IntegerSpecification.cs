﻿using System;

namespace Iridescence.Serialization.Compiler
{
	[Flags]
	public enum IntegerFlags
	{
		None = 0,
		Signed = 0x01,
	}

	/// <summary>
	/// Specifies an integer range.
	/// </summary>
	public readonly struct IntegerSpecification
	{
		#region Fields

		#endregion
		
		#region Properties

		public IntegerFlags Flags { get; }

		public bool IsSigned => (this.Flags & IntegerFlags.Signed) != 0;

		public ulong MinUnsignedValue { get; }

		public ulong MaxUnsignedValue { get; }

		public long MinSignedValue => unchecked((long)this.MinUnsignedValue);

		public long MaxSignedValue  => unchecked((long)this.MaxUnsignedValue);

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="IntegerSpecification"/> class. 
		/// </summary>
		public IntegerSpecification(IntegerFlags flags, ulong minValue, ulong maxValue)
		{
			this.Flags = flags;
			this.MinUnsignedValue = minValue;
			this.MaxUnsignedValue = maxValue;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="IntegerSpecification"/> class. 
		/// </summary>
		public IntegerSpecification(IntegerFlags flags, long minValue, long maxValue)
		{
			this.Flags = flags;
			this.MinUnsignedValue = unchecked((ulong)minValue);
			this.MaxUnsignedValue = unchecked((ulong)maxValue);
		}

		#endregion

		#region Methods

		#endregion
	}
}
