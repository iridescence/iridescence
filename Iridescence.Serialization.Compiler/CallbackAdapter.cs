﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Adapter for <see cref="DeferredCallback{TObj,TState}"/>.
	/// </summary>
	internal sealed class CallbackAdapter<TObjIn, TObjOut, TState>
	{
		#region Fields

		private readonly DeferredCallback<TObjOut, TState> callback;

		#endregion

		#region Properties

		public readonly DeferredCallback<TObjIn, TState> Delegate;

		#endregion

		#region Constructors

		public CallbackAdapter(DeferredCallback<TObjOut, TState> callback)
		{
			this.callback = callback;
			this.Delegate = this.invoke;
		}

		#endregion

		#region Methods

		private void invoke(TObjIn obj, TState state)
		{
			this.callback((TObjOut)(object)obj, state);
		}

		#endregion
	}
}
