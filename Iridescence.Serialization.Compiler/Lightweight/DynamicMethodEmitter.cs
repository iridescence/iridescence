﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	public class DynamicMethodEmitter : CompilableEmitter
	{
		private readonly DynamicMethodUnit unit;
		private readonly DynamicMethodBuilder ilBuilder;
		private DynamicMethod method;

		public DynamicMethodEmitter(DynamicMethodUnit unit, DynamicMethodBuilder ilBuilder, IList<ICapturedValue> capturedValues, bool trace)
			: base(unit, ilBuilder, capturedValues, trace)
		{
			this.ilBuilder = ilBuilder;
			this.unit = unit;
			this.unit.Completed += this.onUnitCompleted;
		}

		private void onUnitCompleted(object sender, EventArgs e)
		{
			this.unit.Completed += this.onUnitCompleted;
			this.method = this.ilBuilder.Compile();
		}

		public override CompiledMethod GetCompiledMethod()
		{
			if (this.method == null)
				throw new CompilationNotCompletedException();

			return new CompiledDynamicMethod(this.method);
		}

		private sealed class CompiledDynamicMethod : CompiledMethod
		{
			private readonly DynamicMethod method;

			public override CallableMethod Callable => this.method;

			public CompiledDynamicMethod(DynamicMethod method)
			{
				this.method = method;
			}

			public override Delegate CreateDelegate(Type delegateType, object target)
			{
				Delegate del = this.method.CreateDelegate(delegateType, target);
				RuntimeHelpers.PrepareDelegate(del);
				return del;
			}
		}
	}
}