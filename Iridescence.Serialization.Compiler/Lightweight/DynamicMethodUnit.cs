﻿using System;
using System.Collections.Generic;
using Iridescence.Emit;

namespace Iridescence.Serialization.Compiler
{
	public class DynamicMethodUnit : ICompilationUnit
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value that indicates whether this compilation unit has been completed.
		/// </summary>
		public bool IsCompleted { get; private set; }

		/// <summary>
		/// Gets a value that indicates whether tracing is enabled.
		/// </summary>
		public bool Trace { get; }

		#endregion

		#region Constructors

		public DynamicMethodUnit(bool trace)
		{
			this.Trace = trace;
		}

		#endregion

		#region Methods

		public virtual CompilableEmitter CreateMethod(MethodSignature signature, string name, bool withCapturedValues = true)
		{
			if (this.IsCompleted)
				throw new CompilationCompletedException();

			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(signature, name, options: ILBuilderOptions.Default | ILBuilderOptions.AllowUnreachable);
			return new DynamicMethodEmitter(this, emit, withCapturedValues ? new List<ICapturedValue>() : null, this.Trace);
		}

		internal event EventHandler Completed;

		public void Complete()
		{
			if (this.IsCompleted)
				throw new CompilationCompletedException();

			this.IsCompleted = true;
			this.Completed?.Invoke(this, EventArgs.Empty);
		}

		#endregion
	}
}