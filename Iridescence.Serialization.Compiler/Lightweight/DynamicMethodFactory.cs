﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Implements a <see cref="ICompilationUnitFactory"/> that creates <see cref="DynamicMethodUnit"/>.
	/// </summary>
	public class DynamicMethodFactory : ICompilationUnitFactory
	{
		#region Properties

		/// <summary>
		/// Gets a singleton instance of the <see cref="DynamicMethodFactory"/> without tracing support.
		/// </summary>
		public static DynamicMethodFactory Instance { get; } = new DynamicMethodFactory(false);

		/// <summary>
		/// Gets a value indicating whether tracing is enabled.
		/// </summary>
		public bool Trace { get; }

		#endregion

		#region Constructors

		public DynamicMethodFactory(bool trace)
		{
			this.Trace = trace;
		}

		#endregion

		#region Methods

		public ICompilationUnit CreateUnit()
		{
			return new DynamicMethodUnit(this.Trace);
		}

		#endregion
	}
}