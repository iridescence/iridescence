﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Serialization.Compiler
{
	[Serializable]
	public class CompilationCompletedException : CompilationException
	{
		public CompilationCompletedException() : base("The compilation unit is already complete.")
		{
		}

		public CompilationCompletedException(string message) : base(message)
		{
		}

		public CompilationCompletedException(string message, Exception inner) : base(message, inner)
		{
		}

		protected CompilationCompletedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}