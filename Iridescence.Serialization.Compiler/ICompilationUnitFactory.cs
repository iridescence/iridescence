﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// Interface for low-level method compilation.
	/// </summary>
	public interface ICompilationUnitFactory
	{
		/// <summary>
		/// Begins a new compilation operation.
		/// </summary>
		/// <returns></returns>
		ICompilationUnit CreateUnit();
	}
}
