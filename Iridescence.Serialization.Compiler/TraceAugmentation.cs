﻿namespace Iridescence.Serialization.Compiler
{
	/// <summary>
	/// A trace <see cref="IAugmentation"/> that calls <see cref="ISerializationTraceStream.OnBegin(SerializationMember, SerializationDirection, SerializationMode)"/> and <see cref="ISerializationTraceStream.OnEnd(SerializationMember, SerializationDirection, SerializationMode)"/>.
	/// </summary>
	public class TraceAugmentation : IAugmentation
	{
		#region Fields

		#endregion

		#region Properties

		public AugmentationPosition Position { get; }

		public SerializationDirection Direction { get; }

		public SerializationMode Mode { get; }
		
		public SerializationMember Member { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TraceAugmentation"/> class. 
		/// </summary>
		public TraceAugmentation(AugmentationPosition position, SerializationDirection direction, SerializationMode mode, SerializationMember member)
		{
			this.Position = position;
			this.Direction = direction;
			this.Mode = mode;
			this.Member = member;
		}

		#endregion

		#region Methods

		public void Emit(AugmentationEmitter emit)
		{
			SerializationTraceInfo info = new SerializationTraceInfo(this.Member, this.Direction, this.Mode);

			if (this.Position == AugmentationPosition.BeforeBody)
			{
				emit.TraceBegin(info);
			}
			else
			{
				emit.TraceEnd(info);
			}
		}

		#endregion
	}
}