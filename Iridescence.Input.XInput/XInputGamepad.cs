﻿using System;
using System.Collections.Immutable;
using Iridescence.Math;

namespace Iridescence.Input.XInput
{
	/// <summary>
	/// Represents a <see cref="Gamepad"/> that uses the XInput API.
	/// </summary>
    public class XInputGamepad : Gamepad
    {
	    #region Fields
		
	    /// <summary>
	    /// Gets the <see cref="XInputGamepad"/> instances for every possible user index. See also <seealso cref="UserIndex"/>.
	    /// </summary>
	    public static readonly ImmutableArray<XInputGamepad> Gamepads;

	    private static readonly XInputButton[] buttons = (XInputButton[])Enum.GetValues(typeof(XInputButton));
		
		private Native.XINPUT_STATE state;
		private Native.XINPUT_VIBRATION vibration;

		private readonly string deviceIdentifier;

		#endregion

	    #region Properties

		/// <summary>
		/// Gets the gamepad user index.
		/// </summary>
		/// <remarks>
		/// Each controller that is connected to the computer is assigned a unique user index by the XInput driver.
		/// The first controller has the index 0, the second the index 1 and so on.
		/// The XInput API supports up to 4 concurrent controllers, which means the maximum value of this property is 3.
		/// </remarks>
	    public uint UserIndex { get; }

		/// <summary>
		/// Gets or sets the left vibration motor strength.
		/// </summary>
		/// <remarks>
		/// The value is mapped to the interval [0, 1].
		/// The left motor is a low-frequency rumble motor.
		/// </remarks>
	    public float LeftVibration
	    {
		    get => (float)this.vibration.wLeftMotorSpeed / ushort.MaxValue;
		    set
		    {
			    if (value != this.vibration.wLeftMotorSpeed)
			    {
				    this.vibration.wLeftMotorSpeed = (ushort) (value * ushort.MaxValue);
				    Native.XInputSetState(this.UserIndex, ref this.vibration);
			    }
		    }
	    }
		
	    /// <summary>
	    /// Gets or sets the right vibration motor strength.
	    /// </summary>
	    /// <remarks>
	    /// The value is mapped to the interval [0, 1].
	    /// The right motor is a high-frequency rumble motor.
	    /// </remarks>
	    public float RightVibration
	    {
		    get => (float)this.vibration.wRightMotorSpeed / ushort.MaxValue;
		    set
		    {
			    if (value != this.vibration.wRightMotorSpeed)
			    {
				    this.vibration.wRightMotorSpeed = (ushort) (value * ushort.MaxValue);
				    Native.XInputSetState(this.UserIndex, ref this.vibration);
			    }
		    }
	    }

	    /// <summary>
	    /// Gets or sets the current vibration power.
	    /// </summary>
	    /// <remarks>
	    /// The value is mapped to the interval [0, 1].
	    /// </remarks>
	    public override float VibrationPower
	    {
		    get => (this.LeftVibration + this.RightVibration) / 2;
		    set => this.LeftVibration = this.RightVibration = value / 2;
	    }

		public override string DeviceIdentifier => this.deviceIdentifier;

		#endregion

		#region Constructors

		static XInputGamepad()
	    {
			// Initialize all possible gamepads.
			var gamepads = ImmutableArray.CreateBuilder<XInputGamepad>(4);
		    for (uint i = 0; i < 4; i++)
		    {
			    XInputGamepad gamepad = new XInputGamepad(i)
			    {
				    // Set default deadzone values from XInput.h
				    LeftAnalogStickDeadZone = (float) 7849 / short.MaxValue,
				    RightAnalogStickDeadZone = (float) 8689 / short.MaxValue,
					TriggerDeadZone = (float) 30 / byte.MaxValue,
			    };
				gamepads.Add(gamepad);
		    }
			Gamepads = gamepads.ToImmutable();
		}

		/// <summary>
		/// Creates a new <see cref="XInputGamepad"/>
		/// </summary>
		/// <param name="userIndex">The user index. Must be lower than 4.</param>
	    private XInputGamepad(uint userIndex)
		{
			if (userIndex > 3)
				throw new ArgumentOutOfRangeException(nameof(userIndex), "Invalid user index.");
			this.UserIndex = userIndex;
			this.deviceIdentifier = $"XInputGamepad{this.UserIndex}";
		}

		#endregion

	    #region Methods

		/// <summary>
		/// Updates the gamepad's state.
		/// </summary>
		public override void Update()
	    {
		    uint result = Native.XInputGetState(this.UserIndex, ref this.state);

		    this.IsAvailable = result == 0; // The XInputGetState method returns 0 (ERROR_SUCCESS) if it is available.
		    if (this.IsAvailable)
		    {
			    foreach (XInputButton button in buttons)
			    {
				    this[xInputButtonToGamepadButton(button)] = this.state.Gamepad.wButtons.HasFlag(button);
			    }
				
			    this.LeftTrigger = computeTriggerValue(this.state.Gamepad.bLeftTrigger, this.TriggerDeadZone);
			    this.RightTrigger = computeTriggerValue(this.state.Gamepad.bRightTrigger, this.TriggerDeadZone);
				
			    Vector2 leftAnalogStick = computeAnalogStickValues(this.state.Gamepad.sThumbLX,  this.state.Gamepad.sThumbLY, (short) (this.LeftAnalogStickDeadZone * short.MaxValue));
			    this.LeftAnalogStickX = leftAnalogStick.X;
			    this.LeftAnalogStickY = leftAnalogStick.Y;
				
			    Vector2 rightAnalogStick = computeAnalogStickValues(this.state.Gamepad.sThumbRX,  this.state.Gamepad.sThumbRY, (short) (this.RightAnalogStickDeadZone * short.MaxValue));
			    this.RightAnalogStickX = rightAnalogStick.X;
			    this.RightAnalogStickY = rightAnalogStick.Y;
		    }
		    else
		    {
				// Set everything to zero if the gamepad is not available.
			    this.LeftTrigger = 0f;
			    this.RightTrigger = 0f;
			    this.LeftAnalogStickX = 0f;
			    this.LeftAnalogStickY = 0f;
			    this.RightAnalogStickX = 0f;
			    this.RightAnalogStickY = 0f;
			    this.LeftVibration = 0f;
			    this.RightVibration = 0f;
		    }
	    }
		
	    /// <summary>
	    /// Computes an analog trigger value by factoring in a deadzone.
	    /// </summary>
	    /// <returns></returns>
	    private static float computeTriggerValue(byte value, float normalizedDeadzone)
	    {
			return Utility.Max(0, (float)value / byte.MaxValue - normalizedDeadzone) / (1 - normalizedDeadzone);
	    }

		/// <summary>
		/// Computes analog axis values by factoring in a deadzone.
		/// </summary>
		/// <returns></returns>
	    private static Vector2 computeAnalogStickValues(short valueX, short valueY, short deadzone)
	    {
		    float magnitude = Utility.Sqrt(valueX * valueX + valueY * valueY);
			
		    float normalizedValueX = valueX / magnitude;
		    float normalizedValueY = valueY / magnitude;

		    float normalizedMagnitude;
			
		    if (magnitude > deadzone)
		    {
			    if (magnitude > short.MaxValue)
				    magnitude = short.MaxValue;
  
			    magnitude -= deadzone;
				
			    normalizedMagnitude = magnitude / (short.MaxValue - deadzone);
		    }
		    else
			    normalizedMagnitude = 0f;
			
		    return (normalizedMagnitude * normalizedValueX, normalizedMagnitude * normalizedValueY);;
	    }
		
	    private static GamepadButton xInputButtonToGamepadButton(XInputButton button)
	    {
		    switch (button)
		    {
			    case XInputButton.XINPUT_GAMEPAD_DPAD_UP:
				    return GamepadButton.DPadUp;
			    case XInputButton.XINPUT_GAMEPAD_DPAD_DOWN:
				    return GamepadButton.DPadDown;
			    case XInputButton.XINPUT_GAMEPAD_DPAD_LEFT:
				    return GamepadButton.DPadLeft;
			    case XInputButton.XINPUT_GAMEPAD_DPAD_RIGHT:
				    return GamepadButton.DPadRight;
			    case XInputButton.XINPUT_GAMEPAD_START:
				    return GamepadButton.Button9;
			    case XInputButton.XINPUT_GAMEPAD_BACK:
				    return GamepadButton.Button10;
			    case XInputButton.XINPUT_GAMEPAD_LEFT_THUMB:
				    return GamepadButton.Button7;
			    case XInputButton.XINPUT_GAMEPAD_RIGHT_THUMB:
				    return GamepadButton.Button8;
			    case XInputButton.XINPUT_GAMEPAD_LEFT_SHOULDER:
				    return GamepadButton.Button5;
			    case XInputButton.XINPUT_GAMEPAD_RIGHT_SHOULDER:
				    return GamepadButton.Button6;
			    case XInputButton.XINPUT_GAMEPAD_A:
				    return GamepadButton.Button1;
			    case XInputButton.XINPUT_GAMEPAD_B:
				    return GamepadButton.Button2;
			    case XInputButton.XINPUT_GAMEPAD_X:
				    return GamepadButton.Button3;
			    case XInputButton.XINPUT_GAMEPAD_Y:
				    return GamepadButton.Button4;
			    default:
				    throw new ArgumentOutOfRangeException(nameof(button), button, null);
		    }
	    }

	    #endregion
    }
}
