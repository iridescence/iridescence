﻿using System.Runtime.InteropServices;

namespace Iridescence.Input.XInput
{
	internal static class Native
	{
		private const string lib = "xinput9_1_0.dll";

		[DllImport(lib)]
		public static extern uint XInputGetState
		(
			uint dwUserIndex,
			ref XINPUT_STATE pState
		);

		[DllImport(lib)]
		public static extern uint XInputSetState
		(
			uint dwUserIndex,
			ref XINPUT_VIBRATION pVibration
		);

		[StructLayout(LayoutKind.Explicit)]
		public struct XINPUT_STATE
		{
			[FieldOffset(0)]
			public uint dwPacketNumber;

			[FieldOffset(4)]
			public XINPUT_GAMEPAD Gamepad;
		}

		[StructLayout(LayoutKind.Explicit)]
		public struct XINPUT_GAMEPAD
		{
			[MarshalAs(UnmanagedType.I2)]
			[FieldOffset(0)]
			public XInputButton wButtons;

			[MarshalAs(UnmanagedType.I1)] 
			[FieldOffset(2)]
			public byte bLeftTrigger;

			[MarshalAs(UnmanagedType.I1)]
			[FieldOffset(3)]
			public byte bRightTrigger;

			[MarshalAs(UnmanagedType.I2)]
			[FieldOffset(4)]
			public short sThumbLX;

			[MarshalAs(UnmanagedType.I2)]
			[FieldOffset(6)]
			public short sThumbLY;

			[MarshalAs(UnmanagedType.I2)]
			[FieldOffset(8)]
			public short sThumbRX;

			[MarshalAs(UnmanagedType.I2)]
			[FieldOffset(10)]
			public short sThumbRY;
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct XINPUT_VIBRATION
		{
			[MarshalAs(UnmanagedType.I2)]
			public ushort wLeftMotorSpeed;

			[MarshalAs(UnmanagedType.I2)]
			public ushort wRightMotorSpeed;
		}
	}
}