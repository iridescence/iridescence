﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Iridescence.IO.Compression.LZ4.Encoders;
using Iridescence.IO.Compression.LZ4.Internal;
using Iridescence.IO.Compression.LZ4.XXHash;

namespace Iridescence.IO.Compression.LZ4.Streams
{
	/// <summary>
	/// LZ4 Decompression stream handling.
	/// </summary>
	public class LZ4DecoderStream: Stream, IDisposable
	{
		private readonly bool _interactive = true;
		private readonly bool _leaveOpen;

		private readonly Stream _inner;
		
		// ReSharper disable once InconsistentNaming
		private const int _length16 = 16; // we intend to use only 16 bytes
		private readonly byte[] _buffer16 = new byte[_length16 + 8];  
		private int _index16;

		private readonly Func<ILZ4Descriptor, ILZ4Decoder> _decoderFactory;

		private ILZ4Descriptor _frameInfo;
		private ILZ4Decoder _decoder;
		private int _decoded;
		private byte[] _buffer;

		private long _position;

		/// <summary>Creates new instance <see cref="LZ4DecoderStream"/>.</summary>
		/// <param name="inner">Inner stream.</param>
		/// <param name="decoderFactory">A function which will create appropriate decoder depending
		/// on frame descriptor.</param>
		/// <param name="leaveOpen">If <c>true</c> inner stream will not be closed after disposing.</param>
		public LZ4DecoderStream(
			Stream inner,
			Func<ILZ4Descriptor, ILZ4Decoder> decoderFactory,
			bool leaveOpen = false)
		{
			this._inner = inner;
			this._decoderFactory = decoderFactory;
			this._leaveOpen = leaveOpen;
			this._position = 0;
		}

		/// <inheritdoc />
		public override void Flush() =>
			this._inner.Flush();

		/// <inheritdoc />
		public override Task FlushAsync(CancellationToken cancellationToken) =>
			this._inner.FlushAsync(cancellationToken);

		/// <inheritdoc />
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (!this.EnsureFrame())
				return 0;

			var read = 0;
			while (count > 0)
			{
				if (this._decoded <= 0 && (this._decoded = this.ReadBlock()) == 0)
					break;

				if (this.ReadDecoded(buffer, ref offset, ref count, ref read))
					break;
			}

			return read;
		}

		/// <inheritdoc />
		public override int ReadByte() =>
			this.Read(this._buffer16, _length16, 1) > 0 ? this._buffer16[_length16] : -1;

		private bool EnsureFrame() => this._decoder != null || this.ReadFrame();

		[SuppressMessage("ReSharper", "InconsistentNaming")]
		private bool ReadFrame()
		{
			this.FlushPeek();

			var magic = this.TryPeek32();
			
			if (!magic.HasValue)
				return false;

			if (magic != 0x184D2204)
				throw MagicNumberExpected();

			this.FlushPeek();

			var FLG_BD = this.Peek16();

			var FLG = FLG_BD & 0xFF;
			var BD = (FLG_BD >> 8) & 0xFF;

			var version = (FLG >> 6) & 0x11;

			if (version != 1)
				throw UnknownFrameVersion(version);

			var blockChaining = ((FLG >> 5) & 0x01) == 0;
			var blockChecksum = ((FLG >> 4) & 0x01) != 0;
			var hasContentSize = ((FLG >> 3) & 0x01) != 0;
			var contentChecksum = ((FLG >> 2) & 0x01) != 0;
			var hasDictionary = (FLG & 0x01) != 0;
			var blockSizeCode = (BD >> 4) & 0x07;

			var contentLength = hasContentSize ? (long?) this.Peek64() : null;
			var dictionaryId = hasDictionary ? (uint?) this.Peek32() : null;

			var actualHC = (byte) (XXH32.DigestOf(this._buffer16, 0, this._index16) >> 8);
			var expectedHC = this.Peek8();

			if (actualHC != expectedHC)
				throw InvalidHeaderChecksum();

			var blockSize = MaxBlockSize(blockSizeCode);

			if (hasDictionary)
				throw this.NotImplemented(
					"Predefined dictionaries feature is not implemented"); // Write32(dictionaryId);

			// ReSharper disable once ExpressionIsAlwaysNull
			this._frameInfo = new LZ4Descriptor(
				contentLength, contentChecksum, blockChaining, blockChecksum, dictionaryId,
				blockSize);
			this._decoder = this._decoderFactory(this._frameInfo);
			this._buffer = new byte[blockSize];

			return true;
		}

		private void CloseFrame()
		{
			if (this._decoder == null)
				return;

			try
			{
				this._frameInfo = null;
				this._buffer = null;

				// if you need any exceptions throw them here

				this._decoder.Dispose();
			}
			finally
			{
				this._decoder = null;
			}
		}

		private static int MaxBlockSize(int blockSizeCode)
		{
			switch (blockSizeCode)
			{
				case 7: return Mem.M4;
				case 6: return Mem.M1;
				case 5: return Mem.K256;
				case 4: return Mem.K64;
				default: return Mem.K64;
			}
		}

		private unsafe int ReadBlock()
		{
			this.FlushPeek();

			var blockLength = (int) this.Peek32();
			if (blockLength == 0)
			{
				if (this._frameInfo.ContentChecksum)
					this.Peek32();
				this.CloseFrame();
				return 0;
			}

			var uncompressed = (blockLength & 0x80000000) != 0;
			blockLength &= 0x7FFFFFFF;

			this.PeekN(this._buffer, 0, blockLength);

			if (this._frameInfo.BlockChecksum)
				this.Peek32();

			fixed (byte* bufferP = this._buffer)
				return uncompressed
					? this._decoder.Inject(bufferP, blockLength)
					: this._decoder.Decode(bufferP, blockLength);
		}

		private bool ReadDecoded(byte[] buffer, ref int offset, ref int count, ref int read)
		{
			if (this._decoded <= 0)
				return true;

			var length = Math.Min(count, this._decoded);
			this._decoder.Drain(buffer, offset, -this._decoded, length);
			this._position += length;
			this._decoded -= length;
			offset += length;
			count -= length;
			read += length;

			return this._interactive;
		}

		private int PeekN(byte[] buffer, int offset, int count, bool optional = false)
		{
			var index = 0;
			while (count > 0)
			{
				var read = this._inner.Read(buffer, index + offset, count);
				if (read == 0)
				{
					if (index == 0 && optional)
						return 0;

					throw EndOfStream();
				}

				index += read;
				count -= read;
			}

			return index;
		}

		private bool PeekN(int count, bool optional = false)
		{
			if (count == 0) return true;

			var read = this.PeekN(this._buffer16, this._index16, count, optional);
			this._index16 += read;
			return read > 0;
		}

		private void FlushPeek() { this._index16 = 0; }

		// ReSharper disable once UnusedMethodReturnValue.Local
		private ulong Peek64()
		{
			this.PeekN(sizeof(ulong));
			return BitConverter.ToUInt64(this._buffer16, this._index16 - sizeof(ulong));
		}

		private uint? TryPeek32()
		{
			if (!this.PeekN(sizeof(uint), true))
				return null;

			return BitConverter.ToUInt32(this._buffer16, this._index16 - sizeof(uint));
		}

		private uint Peek32()
		{
			this.PeekN(sizeof(uint));
			return BitConverter.ToUInt32(this._buffer16, this._index16 - sizeof(uint));
		}

		private ushort Peek16()
		{
			this.PeekN(sizeof(ushort));
			return BitConverter.ToUInt16(this._buffer16, this._index16 - sizeof(ushort));
		}

		private byte Peek8()
		{
			this.PeekN(sizeof(byte));
			return this._buffer16[this._index16 - 1];
		}

		/// <inheritdoc />
		public new void Dispose()
		{
			this.Dispose(true);
			base.Dispose();
		}

		/// <inheritdoc />
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (!disposing)
				return;

			this.CloseFrame();
			if (!this._leaveOpen)
				this._inner.Dispose();
		}

		/// <inheritdoc />
		public override bool CanRead => this._inner.CanRead;

		/// <inheritdoc />
		public override bool CanSeek => false;

		/// <inheritdoc />
		public override bool CanWrite => false;

		/// <summary>
		/// Length of stream. Please note, this will only work if original LZ4 stream has
		/// <c>ContentLength</c> field set in descriptor. Otherwise returned value will be <c>-1</c>.
		/// </summary>
		public override long Length
		{
			get
			{
				this.EnsureFrame();
				return this._frameInfo?.ContentLength ?? -1;
			}
		}

		/// <summary>
		/// Position within the stream. Position can be read, but cannot be set as LZ4 stream does
		/// not have <c>Seek</c> capability.
		/// </summary>
		public override long Position
		{
			get => this._position;
			set => throw this.InvalidOperation("SetPosition");
		}

		/// <inheritdoc />
		public override bool CanTimeout => this._inner.CanTimeout;

		/// <inheritdoc />
		public override int WriteTimeout
		{
			get => this._inner.WriteTimeout;
			set => this._inner.WriteTimeout = value;
		}

		/// <inheritdoc />
		public override int ReadTimeout
		{
			get => this._inner.ReadTimeout;
			set => this._inner.ReadTimeout = value;
		}

		/// <inheritdoc />
		public override long Seek(long offset, SeekOrigin origin) =>
			throw this.InvalidOperation("Seek");

		/// <inheritdoc />
		public override void SetLength(long value) =>
			throw this.InvalidOperation("SetLength");

		/// <inheritdoc />
		public override void Write(byte[] buffer, int offset, int count) =>
			throw this.InvalidOperation("Write");

		/// <inheritdoc />
		public override void WriteByte(byte value) =>
			throw this.InvalidOperation("WriteByte");

		/// <inheritdoc />
		public override Task WriteAsync(
			byte[] buffer, int offset, int count, CancellationToken cancellationToken) =>
			throw this.InvalidOperation("WriteAsync");

		private NotImplementedException NotImplemented(string operation) =>
			new NotImplementedException(
				$"Feature {operation} has not been implemented in {this.GetType().Name}");

		private static InvalidDataException InvalidHeaderChecksum() =>
			new InvalidDataException("Invalid LZ4 frame header checksum");

		private static InvalidDataException MagicNumberExpected() =>
			new InvalidDataException("LZ4 frame magic number expected");

		private static InvalidDataException UnknownFrameVersion(int version) =>
			new InvalidDataException($"LZ4 frame version {version} is not supported");

		private InvalidOperationException InvalidOperation(string operation) =>
			new InvalidOperationException(
				$"Operation {operation} is not allowed for {this.GetType().Name}");

		private static EndOfStreamException EndOfStream() =>
			new EndOfStreamException("Unexpected end of stream. Data might be corrupted.");
	}
}
