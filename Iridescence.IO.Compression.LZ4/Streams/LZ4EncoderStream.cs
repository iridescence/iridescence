﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Iridescence.IO.Compression.LZ4.Encoders;
using Iridescence.IO.Compression.LZ4.Internal;
using Iridescence.IO.Compression.LZ4.XXHash;

namespace Iridescence.IO.Compression.LZ4.Streams
{
	/// <summary>
	/// LZ4 compression stream. 
	/// </summary>
	public class LZ4EncoderStream: Stream, IDisposable
	{
		private readonly Stream _inner;
		
		// ReSharper disable once InconsistentNaming
		private const int _length16 = 16;
		private readonly byte[] _buffer16 = new byte[_length16 + 8];
		private int _index16;

		private ILZ4Encoder _encoder;
		private readonly Func<ILZ4Descriptor, ILZ4Encoder> _encoderFactory;

		private readonly ILZ4Descriptor _descriptor;
		private readonly bool _leaveOpen;

		private byte[] _buffer;
		private long _position;

		/// <summary>Creates new instance of <see cref="LZ4EncoderStream"/>.</summary>
		/// <param name="inner">Inner stream.</param>
		/// <param name="descriptor">LZ4 Descriptor.</param>
		/// <param name="encoderFactory">Function which will take descriptor and return
		/// appropriate encoder.</param>
		/// <param name="leaveOpen">Indicates if <paramref name="inner"/> stream should be left
		/// open after disposing.</param>
		public LZ4EncoderStream(
			Stream inner,
			ILZ4Descriptor descriptor,
			Func<ILZ4Descriptor, ILZ4Encoder> encoderFactory,
			bool leaveOpen = false)
		{
			this._inner = inner;
			this._descriptor = descriptor;
			this._encoderFactory = encoderFactory;
			this._leaveOpen = leaveOpen;
		}

		/// <inheritdoc />
		public override void Flush() => this._inner.Flush();

		/// <inheritdoc />
		public override Task FlushAsync(CancellationToken cancellationToken) =>
			this._inner.FlushAsync(cancellationToken);

		#if NETSTANDARD1_6
		/// <summary>Closes stream.</summary>
		public void Close() { CloseFrame(); }
		#else
		/// <inheritdoc />
		public override void Close() { this.CloseFrame(); }
		#endif

		/// <inheritdoc />
		public override void WriteByte(byte value)
		{
			this._buffer16[_length16] = value;
			this.Write(this._buffer16, _length16, 1);
		}

		/// <inheritdoc />
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (this._encoder == null)
				this.WriteFrame();

			while (count > 0)
			{
				var action = this._encoder.TopupAndEncode(
					buffer, offset, count,
					this._buffer, 0, this._buffer.Length,
					false, true,
					out var loaded,
					out var encoded);
				this.WriteBlock(encoded, action);
				
				this._position += loaded;
				
				offset += loaded;
				count -= loaded;
			}
		}

		[SuppressMessage("ReSharper", "InconsistentNaming")]
		private void WriteFrame()
		{
			this.Stash32(0x184D2204);
			this.FlushStash();

			const int versionCode = 0x01;
			var blockChaining = this._descriptor.Chaining;
			var blockChecksum = this._descriptor.BlockChecksum;
			var contentChecksum = this._descriptor.ContentChecksum;
			var hasContentSize = this._descriptor.ContentLength.HasValue;
			var hasDictionary = this._descriptor.Dictionary.HasValue;

			var FLG =
				(versionCode << 6) |
				((blockChaining ? 0 : 1) << 5) |
				((blockChecksum ? 1 : 0) << 4) |
				((hasContentSize ? 1 : 0) << 3) |
				((contentChecksum ? 1 : 0) << 2) |
				(hasDictionary ? 1 : 0);

			var blockSize = this._descriptor.BlockSize;

			var BD = this.MaxBlockSizeCode(blockSize) << 4;

			this.Stash16((ushort) ((FLG & 0xFF) | (BD & 0xFF) << 8));

			if (hasContentSize)
				throw this.NotImplemented(
					"ContentSize feature is not implemented"); // Write64(contentSize);

			if (hasDictionary)
				throw this.NotImplemented(
					"Predefined dictionaries feature is not implemented"); // Write32(dictionaryId);

			var HC = (byte) (XXH32.DigestOf(this._buffer16, 0, this._index16) >> 8);

			this.Stash8(HC);
			this.FlushStash();

			this._encoder = this.CreateEncoder();
			this._buffer = new byte[LZ4Codec.MaximumOutputSize(blockSize)];
		}

		private ILZ4Encoder CreateEncoder()
		{
			var encoder = this._encoderFactory(this._descriptor);
			if (encoder.BlockSize > this._descriptor.BlockSize)
				throw InvalidValue("BlockSize is greater than declared");

			return encoder;
		}

		private void CloseFrame()
		{
			if (this._encoder == null)
				return;

			try
			{
				var action = this._encoder.FlushAndEncode(
					this._buffer, 0, this._buffer.Length, true, out var encoded);
				this.WriteBlock(encoded, action);

				this.Stash32(0);
				this.FlushStash();

				if (this._descriptor.ContentChecksum)
					throw this.NotImplemented("ContentChecksum");

				this._buffer = null;

				this._encoder.Dispose();
			}
			finally
			{
				this._encoder = null;
			}
		}

		private int MaxBlockSizeCode(int blockSize) =>
			blockSize <= Mem.K64 ? 4 :
			blockSize <= Mem.K256 ? 5 :
			blockSize <= Mem.M1 ? 6 :
			blockSize <= Mem.M4 ? 7 :
			throw this.InvalidBlockSize(blockSize);

		private void WriteBlock(int length, EncoderAction action)
		{
			switch (action)
			{
				case EncoderAction.Copied:
					this.WriteBlock(length, false);
					break;
				case EncoderAction.Encoded:
					this.WriteBlock(length, true);
					break;
			}
		}

		private void WriteBlock(int length, bool compressed)
		{
			if (length <= 0)
				return;

			this.Stash32((uint) length | (compressed ? 0 : 0x80000000));
			this.FlushStash();

			this._inner.Write(this._buffer, 0, length);

			if (this._descriptor.BlockChecksum)
				throw this.NotImplemented("BlockChecksum");
		}

		/// <inheritdoc />
		public new void Dispose()
		{
			this.Dispose(true);
			base.Dispose();
		}

		/// <inheritdoc />
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (!disposing)
				return;

			this.CloseFrame();
			if (!this._leaveOpen)
				this._inner.Dispose();
		}

		private void Stash8(byte value) { this._buffer16[this._index16++] = value; }

		private void Stash16(ushort value)
		{
			this._buffer16[this._index16 + 0] = (byte) value;
			this._buffer16[this._index16 + 1] = (byte) (value >> 8);
			this._index16 += 2;
		}

		private void Stash32(uint value)
		{
			this._buffer16[this._index16 + 0] = (byte) value;
			this._buffer16[this._index16 + 1] = (byte) (value >> 8);
			this._buffer16[this._index16 + 2] = (byte) (value >> 16);
			this._buffer16[this._index16 + 3] = (byte) (value >> 24);
			this._index16 += 4;
		}

		/*
		private void Write64(ulong value)
		{
		    _buffer16[_index16 + 0] = (byte) value;
		    _buffer16[_index16 + 1] = (byte) (value >> 8);
		    _buffer16[_index16 + 2] = (byte) (value >> 16);
		    _buffer16[_index16 + 3] = (byte) (value >> 24);
		    _buffer16[_index16 + 4] = (byte) (value >> 32);
		    _buffer16[_index16 + 5] = (byte) (value >> 40);
		    _buffer16[_index16 + 6] = (byte) (value >> 48);
		    _buffer16[_index16 + 7] = (byte) (value >> 56);
		    _index16 += 8;
		}
		*/

		private void FlushStash()
		{
			if (this._index16 > 0)
				this._inner.Write(this._buffer16, 0, this._index16);
			this._index16 = 0;
		}

		/// <inheritdoc />
		public override bool CanRead => false;

		/// <inheritdoc />
		public override bool CanSeek => false;

		/// <inheritdoc />
		public override bool CanWrite => this._inner.CanWrite;

		/// <summary>Length of the stream and number of bytes written so far.</summary>
		public override long Length => this._position;

		/// <summary>Read-only position in the stream. Trying to set it will throw
		/// <see cref="InvalidOperationException"/>.</summary>
		public override long Position
		{
			get => this._position;
			set => throw this.InvalidOperation("Position");
		}

		/// <inheritdoc />
		public override bool CanTimeout => this._inner.CanTimeout;

		/// <inheritdoc />
		public override int ReadTimeout
		{
			get => this._inner.ReadTimeout;
			set => this._inner.ReadTimeout = value;
		}

		/// <inheritdoc />
		public override int WriteTimeout
		{
			get => this._inner.WriteTimeout;
			set => this._inner.WriteTimeout = value;
		}

		/// <inheritdoc />
		public override long Seek(long offset, SeekOrigin origin) =>
			throw this.InvalidOperation("Seek");

		/// <inheritdoc />
		public override void SetLength(long value) =>
			throw this.InvalidOperation("SetLength");

		/// <inheritdoc />
		public override int Read(byte[] buffer, int offset, int count) =>
			throw this.InvalidOperation("Read");

		/// <inheritdoc />
		public override Task<int> ReadAsync(
			byte[] buffer, int offset, int count, CancellationToken cancellationToken) =>
			throw this.InvalidOperation("ReadAsync");

		/// <inheritdoc />
		public override int ReadByte() => throw this.InvalidOperation("ReadByte");

		private NotImplementedException NotImplemented(string operation) =>
			new NotImplementedException(
				$"Feature {operation} has not been implemented in {this.GetType().Name}");

		private InvalidOperationException InvalidOperation(string operation) =>
			new InvalidOperationException(
				$"Operation {operation} is not allowed for {this.GetType().Name}");

		private static ArgumentException InvalidValue(string description) =>
			new ArgumentException(description);

		private ArgumentException InvalidBlockSize(int blockSize) =>
			new ArgumentException($"Invalid block size ${blockSize} for {this.GetType().Name}");
	}
}
