﻿using System;
using Iridescence.IO.Compression.LZ4.Internal;

namespace Iridescence.IO.Compression.LZ4.Encoders
{
	/// <summary>
	/// LZ4 decoder used with independent blocks mode. Plase note, that it will fail
	/// if input data has been compressed with chained blocks
	/// (<see cref="LZ4FastChainEncoder"/> and <see cref="LZ4HighChainEncoder"/>)
	/// </summary>
	public unsafe class LZ4BlockDecoder: UnmanagedResources, ILZ4Decoder
	{
		private readonly int _blockSize;
		private readonly int _outputLength;
		private int _outputIndex;
		private readonly byte* _outputBuffer;

		/// <inheritdoc />
		public int BlockSize => this._blockSize;

		/// <inheritdoc />
		public int BytesReady => this._outputIndex;

		/// <summary>Creates new instance of block decoder.</summary>
		/// <param name="blockSize">Block size. Must be equal or greater to one used for compression.</param>
		public LZ4BlockDecoder(int blockSize)
		{
			blockSize = Mem.RoundUp(Math.Max(blockSize, Mem.K1), Mem.K1);
			this._blockSize = blockSize;
			this._outputLength = this._blockSize + 8;
			this._outputIndex = 0;
			this._outputBuffer = (byte*) Mem.Alloc(this._outputLength + 8);
		}

		/// <inheritdoc />
		public int Decode(byte* source, int length, int blockSize = 0)
		{
			this.ThrowIfDisposed();
			
			if (blockSize <= 0)
				blockSize = this._blockSize;

			if (blockSize > this._blockSize)
				throw new InvalidOperationException();

			var decoded = LZ4Codec.Decode(source, length, this._outputBuffer, this._outputLength);
			if (decoded < 0)
				throw new InvalidOperationException();

			this._outputIndex = decoded;
			return this._outputIndex;
		}

		/// <inheritdoc />
		public int Inject(byte* source, int length)
		{
			this.ThrowIfDisposed();
			
			if (length <= 0)
				return this._outputIndex = 0;

			if (length > this._outputLength)
				throw new InvalidOperationException();

			Mem.Move(this._outputBuffer, source, length);
			this._outputIndex = length;
			return length;
		}

		/// <inheritdoc />
		public void Drain(byte* target, int offset, int length)
		{
			this.ThrowIfDisposed();

			offset = this._outputIndex + offset; // NOTE: negative value
			if (offset < 0 || length < 0 || offset + length > this._outputIndex)
				throw new InvalidOperationException();

			Mem.Move(target, this._outputBuffer + offset, length);
		}

		/// <inheritdoc />
		protected override void ReleaseUnmanaged()
		{
			base.ReleaseUnmanaged();
			Mem.Free(this._outputBuffer);
		}
	}
}
