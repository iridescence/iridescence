﻿using System;
using Iridescence.IO.Compression.LZ4.Engine;
using Iridescence.IO.Compression.LZ4.Internal;

namespace Iridescence.IO.Compression.LZ4.Encoders
{
	// fast decoder context
	using LZ4Context = LZ4_xx.LZ4_streamDecode_t;

	/// <summary>LZ4 decoder handling dependent blocks.</summary>
	public unsafe class LZ4ChainDecoder: UnmanagedResources, ILZ4Decoder
	{
		private readonly LZ4Context* _context;
		private readonly int _blockSize;
		private readonly byte* _outputBuffer;
		private readonly int _outputLength;
		private int _outputIndex;

		/// <summary>Creates new instance of <see cref="LZ4ChainDecoder"/>.</summary>
		/// <param name="blockSize">Block size.</param>
		/// <param name="extraBlocks">Number of extra blocks.</param>
		public LZ4ChainDecoder(int blockSize, int extraBlocks)
		{
			blockSize = Mem.RoundUp(Math.Max(blockSize, Mem.K1), Mem.K1);
			extraBlocks = Math.Max(extraBlocks, 0);

			this._blockSize = blockSize;
			this._outputLength = Mem.K64 + (1 + extraBlocks) * this._blockSize + 8;
			this._outputIndex = 0;
			this._outputBuffer = (byte*) Mem.Alloc(this._outputLength + 8);
			this._context = (LZ4Context*) Mem.AllocZero(sizeof(LZ4Context));
		}

		/// <inheritdoc />
		public int BlockSize => this._blockSize;

		/// <inheritdoc />
		public int BytesReady => this._outputIndex;

		/// <inheritdoc />
		public int Decode(byte* source, int length, int blockSize)
		{
			if (blockSize <= 0)
				blockSize = this._blockSize;

			this.Prepare(blockSize);

			var decoded = this.DecodeBlock(source, length, this._outputBuffer + this._outputIndex, blockSize);

			if (decoded < 0)
				throw new InvalidOperationException();

			this._outputIndex += decoded;

			return decoded;
		}

		/// <inheritdoc />
		public int Inject(byte* source, int length)
		{
			if (length <= 0)
				return 0;

			if (length > Math.Max(this._blockSize, Mem.K64))
				throw new InvalidOperationException();

			if (this._outputIndex + length < this._outputLength)
			{
				Mem.Move(this._outputBuffer + this._outputIndex, source, length);
				this._outputIndex = this.ApplyDict(this._outputIndex + length);
			} 
			else if (length >= Mem.K64)
			{
				Mem.Move(this._outputBuffer, source, length);
				this._outputIndex = this.ApplyDict(length);
			}
			else
			{
				var tailSize = Math.Min(Mem.K64 - length, this._outputIndex);
				Mem.Move(this._outputBuffer, this._outputBuffer + this._outputIndex - tailSize, tailSize);
				Mem.Move(this._outputBuffer + tailSize, source, length);
				this._outputIndex = this.ApplyDict(tailSize + length);
			}

			return length;
		}

		/// <inheritdoc />
		public void Drain(byte* target, int offset, int length)
		{
			offset = this._outputIndex + offset; // NOTE: negative value
			if (offset < 0 || length < 0 || offset + length > this._outputIndex)
				throw new InvalidOperationException();

			Mem.Move(target, this._outputBuffer + offset, length);
		}

		private void Prepare(int blockSize)
		{
			if (this._outputIndex + blockSize <= this._outputLength)
				return;

			this._outputIndex = this.CopyDict(this._outputIndex);
		}

		private int CopyDict(int index)
		{
			var dictStart = Math.Max(index - Mem.K64, 0);
			var dictSize = index - dictStart;
			Mem.Move(this._outputBuffer, this._outputBuffer + dictStart, dictSize);
			LZ4_xx.LZ4_setStreamDecode(this._context, this._outputBuffer, dictSize);
			return dictSize;
		}

		private int ApplyDict(int index)
		{ 
			var dictStart = Math.Max(index - Mem.K64, 0);
			var dictSize = index - dictStart;
			LZ4_xx.LZ4_setStreamDecode(this._context, this._outputBuffer + dictStart, dictSize);
			return index;
		}

		private int DecodeBlock(byte* source, int sourceLength, byte* target, int targetLength) =>
			LZ4_xx.LZ4_decompress_safe_continue(this._context, source, target, sourceLength, targetLength);

		/// <inheritdoc />
		protected override void ReleaseUnmanaged()
		{
			base.ReleaseUnmanaged();
			Mem.Free(this._context);
			Mem.Free(this._outputBuffer);
		}
	}
}
