﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Iridescence.IO.Compression.LZ4.Internal
{
	internal class Pool<T>
	{
		private readonly ConcurrentQueue<T> _queue;
		private readonly Func<T> _create;
		private readonly Action<T> _reset;
		private readonly Action<T> _destroy;
		private int _freeSlots;

		public Pool(Func<T> create, Action<T> reset, Action<T> destroy, int size)
		{
			this._queue = new ConcurrentQueue<T>();
			this._create = create;
			this._reset = reset ?? (_ => { });
			this._destroy = destroy ?? (_ => { });
			this._freeSlots = size;
		}

		public T Borrow()
		{
			if (!this._queue.TryDequeue(out var resource))
				return this._create();

			this._reset(resource);
			Interlocked.Increment(ref this._freeSlots);
			return resource;
		}

		public void Return(T resource)
		{
			if (Interlocked.Decrement(ref this._freeSlots) < 0)
			{
				Interlocked.Increment(ref this._freeSlots);
				this._destroy(resource);
			}
			else
			{
				this._queue.Enqueue(resource);
			}
		}
	}
}
