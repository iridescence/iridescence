﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL fence object.
	/// </summary>
	public class Fence : GLContextChild
	{
		#region Fields

		#endregion

		#region Properties

		public IntPtr Sync { get; }

		public bool IsSignaled
		{
			get
			{
				return this.Context.Invoke(() =>
				{
					int value = 0;
					this.Context.GL.GetSync(this.Sync, SyncParameterName.SyncStatus, 1, out int _, ref value);
					return value == (int)All.Signaled;
				});
			}
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Fence"/> class. 
		/// </summary>
		internal Fence(GLContext context, IntPtr sync)
			: base(context)
		{
			this.Sync = sync;
		}

		#endregion

		#region Methods

		public WaitSyncStatus Wait(TimeSpan timeout)
		{
			return this.Context.Invoke(() => this.Context.GL.WaitSync(this.Sync, WaitSyncFlags.None, (ulong)timeout.Ticks * 100));
		}

		public ValueTask<WaitSyncStatus> WaitAsync(TimeSpan timeout)
		{
			return this.Context.InvokeAsync(() => this.Context.GL.WaitSync(this.Sync, WaitSyncFlags.None, (ulong)timeout.Ticks * 100));
		}

		public WaitSyncStatus ClientWait(TimeSpan timeout, ClientWaitSyncFlags flags = ClientWaitSyncFlags.None)
		{
			return this.Context.Invoke(() => this.Context.GL.ClientWaitSync(this.Sync, flags, (ulong)timeout.Ticks * 100));
		}

		public ValueTask<WaitSyncStatus> ClientWaitAsync(TimeSpan timeout, ClientWaitSyncFlags flags = ClientWaitSyncFlags.None)
		{
			return this.Context.InvokeAsync(() => this.Context.GL.ClientWaitSync(this.Sync, flags, (ulong)timeout.Ticks * 100));
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			void delete()
			{
				this.Context.GL.DeleteSync(this.Sync);
			}

			if (managed)
				this.Context.Invoke(delete);
			else
				this.Context.Post(delete);
		}

		#endregion
	}
}
