﻿using System;
using System.Collections.Generic;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Pixel format utility.
	/// </summary>
	public static class FormatUtility
	{
		#region Fields

		private static readonly Dictionary<PixelInternalFormat, Format> formats = new Dictionary<PixelInternalFormat, Format>()
		{
			{PixelInternalFormat.R8, new Format(FormatType.R8, FormatSemantic.UNorm)},
			{PixelInternalFormat.R8Snorm, new Format(FormatType.R8, FormatSemantic.SNorm)},
			{PixelInternalFormat.R16, new Format(FormatType.R16, FormatSemantic.UNorm)},
			{PixelInternalFormat.R16Snorm, new Format(FormatType.R16, FormatSemantic.SNorm)},
			{PixelInternalFormat.Rg8, new Format(FormatType.R8G8, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rg8Snorm, new Format(FormatType.R8G8, FormatSemantic.SNorm)},
			{PixelInternalFormat.Rg16, new Format(FormatType.R16G16, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rg16Snorm, new Format(FormatType.R16G16, FormatSemantic.SNorm)},
			{PixelInternalFormat.R3G3B2, new Format(FormatType.R3G3B2, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rgb4, new Format(FormatType.R4G4B4A4, FormatSemantic.UNorm, ColorMapping.RGBN, ColorMapping.RGB1)},
			{PixelInternalFormat.Rgb5, new Format(FormatType.R5G5B5A1, FormatSemantic.UNorm, ColorMapping.RGBN, ColorMapping.RGB1)},
			{(PixelInternalFormat)(All.Rgb565), new Format(FormatType.R5G6B5, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rgb8, new Format(FormatType.R8G8B8, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rgb8Snorm, new Format(FormatType.R8G8B8, FormatSemantic.SNorm)},
			{PixelInternalFormat.Rgb10, new Format(FormatType.R10G10B10A2, FormatSemantic.UNorm, ColorMapping.RGBN, ColorMapping.RGB1)},
			{PixelInternalFormat.Rgb12, new Format(FormatType.R16G16B16, FormatSemantic.UNorm)}, // mismatch
			{PixelInternalFormat.Rgb16, new Format(FormatType.R16G16B16, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rgb16Snorm, new Format(FormatType.R16G16B16, FormatSemantic.SNorm)},
			{PixelInternalFormat.Rgba2, new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm)}, // mismatch
			{PixelInternalFormat.Rgba4, new Format(FormatType.R4G4B4A4, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rgb5A1, new Format(FormatType.R5G5B5A1, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rgba8, new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rgba8Snorm, new Format(FormatType.R8G8B8A8, FormatSemantic.SNorm)},
			{PixelInternalFormat.Rgb10A2, new Format(FormatType.R10G10B10A2, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rgb10A2ui, new Format(FormatType.R10G10B10A2, FormatSemantic.UInt)},
			{PixelInternalFormat.Rgba12, new Format(FormatType.R16G16B16A16, FormatSemantic.UNorm)}, // mismatch
			{PixelInternalFormat.Rgba16, new Format(FormatType.R16G16B16A16, FormatSemantic.UNorm)},
			{PixelInternalFormat.Rgba16Snorm, new Format(FormatType.R16G16B16A16, FormatSemantic.SNorm)},
			{PixelInternalFormat.Srgb8, new Format(FormatType.R8G8B8, FormatSemantic.UNorm, FormatFlags.Srgb)},
			{PixelInternalFormat.Srgb8Alpha8, new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm, FormatFlags.Srgb)},
			{PixelInternalFormat.R16f, new Format(FormatType.R16, FormatSemantic.Float)},
			{PixelInternalFormat.Rg16f, new Format(FormatType.R16G16, FormatSemantic.Float)},
			{PixelInternalFormat.Rgb16f, new Format(FormatType.R16G16B16, FormatSemantic.Float)},
			{PixelInternalFormat.Rgba16f, new Format(FormatType.R16G16B16A16, FormatSemantic.Float)},
			{PixelInternalFormat.R32f, new Format(FormatType.R32, FormatSemantic.Float)},
			{PixelInternalFormat.Rg32f, new Format(FormatType.R32G32, FormatSemantic.Float)},
			{PixelInternalFormat.Rgb32f, new Format(FormatType.R32G32B32, FormatSemantic.Float)},
			{PixelInternalFormat.Rgba32f, new Format(FormatType.R32G32B32A32, FormatSemantic.Float)},
			{PixelInternalFormat.R11fG11fB10f, new Format(FormatType.R11G11B10, FormatSemantic.Float)},
			{PixelInternalFormat.Rgb9E5, new Format(FormatType.R9G9B9E5, FormatSemantic.Float)},
			{PixelInternalFormat.R8i, new Format(FormatType.R8, FormatSemantic.SInt)},
			{PixelInternalFormat.R8ui, new Format(FormatType.R8, FormatSemantic.UInt)},
			{PixelInternalFormat.R16i, new Format(FormatType.R16, FormatSemantic.SInt)},
			{PixelInternalFormat.R16ui, new Format(FormatType.R16, FormatSemantic.UInt)},
			{PixelInternalFormat.R32i, new Format(FormatType.R32, FormatSemantic.SInt)},
			{PixelInternalFormat.R32ui, new Format(FormatType.R32, FormatSemantic.UInt)},
			{PixelInternalFormat.Rg8i, new Format(FormatType.R8G8, FormatSemantic.SInt)},
			{PixelInternalFormat.Rg8ui, new Format(FormatType.R8G8, FormatSemantic.UInt)},
			{PixelInternalFormat.Rg16i, new Format(FormatType.R16G16, FormatSemantic.SInt)},
			{PixelInternalFormat.Rg16ui, new Format(FormatType.R16G16, FormatSemantic.UInt)},
			{PixelInternalFormat.Rg32i, new Format(FormatType.R32G32, FormatSemantic.SInt)},
			{PixelInternalFormat.Rg32ui, new Format(FormatType.R32G32, FormatSemantic.UInt)},
			{PixelInternalFormat.Rgb8i, new Format(FormatType.R8G8B8, FormatSemantic.SInt)},
			{PixelInternalFormat.Rgb8ui, new Format(FormatType.R8G8B8, FormatSemantic.UInt)},
			{PixelInternalFormat.Rgb16i, new Format(FormatType.R16G16B16, FormatSemantic.SInt)},
			{PixelInternalFormat.Rgb16ui, new Format(FormatType.R16G16B16, FormatSemantic.UInt)},
			{PixelInternalFormat.Rgb32i, new Format(FormatType.R32G32B32, FormatSemantic.SInt)},
			{PixelInternalFormat.Rgb32ui, new Format(FormatType.R32G32B32, FormatSemantic.UInt)},
			{PixelInternalFormat.Rgba8i, new Format(FormatType.R8G8B8A8, FormatSemantic.SInt)},
			{PixelInternalFormat.Rgba8ui, new Format(FormatType.R8G8B8A8, FormatSemantic.UInt)},
			{PixelInternalFormat.Rgba16i, new Format(FormatType.R16G16B16A16, FormatSemantic.SInt)},
			{PixelInternalFormat.Rgba16ui, new Format(FormatType.R16G16B16A16, FormatSemantic.UInt)},
			{PixelInternalFormat.Rgba32i, new Format(FormatType.R32G32B32A32, FormatSemantic.SInt)},
			{PixelInternalFormat.Rgba32ui, new Format(FormatType.R32G32B32A32, FormatSemantic.UInt)},
			{PixelInternalFormat.DepthComponent16, new Format(FormatType.D16, FormatSemantic.UNorm)},
			{PixelInternalFormat.DepthComponent24, new Format(FormatType.D24, FormatSemantic.UNorm)},
			{PixelInternalFormat.DepthComponent32, new Format(FormatType.D32, FormatSemantic.Float)},
			{PixelInternalFormat.DepthComponent32f, new Format(FormatType.D32, FormatSemantic.Float)},
			{PixelInternalFormat.Depth24Stencil8, new Format(FormatType.D24S8, FormatSemantic.UNorm)},
			{PixelInternalFormat.Depth32fStencil8, new Format(FormatType.D32S8, FormatSemantic.Float)},
			{(PixelInternalFormat)All.StencilIndex8, new Format(FormatType.S8, FormatSemantic.UInt)},
		};

		#endregion

		#region Properties

		#endregion

		#region Constructors

		#endregion

		#region Methods

		/// <summary>
		/// Maps the specified OpenGL internal format to its closest counterpart in the engine.
		/// </summary>
		/// <param name="internalFormat"></param>
		/// <returns></returns>
		public static Format GetClosestFormat(PixelInternalFormat internalFormat)
		{
			if (!formats.TryGetValue(internalFormat, out Format format))
				throw new NotSupportedException($"\"{internalFormat}\" is not supported.");

			return format;
		}

		/// <summary>
		/// Gets the preferred pixel format for the specified internal format.
		/// </summary>
		/// <param name="internalFormat"></param>
		/// <returns></returns>
		public static PixelFormat GetPixelFormatForInternalFormat(PixelInternalFormat internalFormat)
		{
			switch (internalFormat)
			{
				case PixelInternalFormat.R8:
					return PixelFormat.Red;
				case PixelInternalFormat.R8Snorm:
					return PixelFormat.Red;
				case PixelInternalFormat.R16:
					return PixelFormat.Red;
				case PixelInternalFormat.R16Snorm:
					return PixelFormat.Red;

				case PixelInternalFormat.Rg8:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rg8Snorm:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rg16:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rg16Snorm:
					return PixelFormat.Rg;

				case PixelInternalFormat.R3G3B2:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb4:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb5:
					return PixelFormat.Rgb;
				case (PixelInternalFormat)(All.Rgb565):
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb8:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb8Snorm:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb10:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb12:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb16:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb16Snorm:
					return PixelFormat.Rgb;

				case PixelInternalFormat.Rgba2:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba4:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgb5A1:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba8:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba8Snorm:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgb10A2:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgb10A2ui:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba12:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba16:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba16Snorm:
					return PixelFormat.Rgba;

				case PixelInternalFormat.Srgb8:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Srgb8Alpha8:
					return PixelFormat.Rgba;

				case PixelInternalFormat.R16f:
					return PixelFormat.Red;
				case PixelInternalFormat.Rg16f:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rgb16f:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgba16f:
					return PixelFormat.Rgba;

				case PixelInternalFormat.R32f:
					return PixelFormat.Red;
				case PixelInternalFormat.Rg32f:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rgb32f:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgba32f:
					return PixelFormat.Rgba;

				case PixelInternalFormat.R11fG11fB10f:
					return PixelFormat.Rgb;

				case PixelInternalFormat.Rgb9E5:
					return PixelFormat.Rgb;

				case PixelInternalFormat.R8i:
					return PixelFormat.Red;
				case PixelInternalFormat.R8ui:
					return PixelFormat.Red;
				case PixelInternalFormat.R16i:
					return PixelFormat.Red;
				case PixelInternalFormat.R16ui:
					return PixelFormat.Red;
				case PixelInternalFormat.R32i:
					return PixelFormat.Red;
				case PixelInternalFormat.R32ui:
					return PixelFormat.Red;

				case PixelInternalFormat.Rg8i:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rg8ui:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rg16i:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rg16ui:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rg32i:
					return PixelFormat.Rg;
				case PixelInternalFormat.Rg32ui:
					return PixelFormat.Rg;

				case PixelInternalFormat.Rgb8i:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb8ui:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb16i:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb16ui:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb32i:
					return PixelFormat.Rgb;
				case PixelInternalFormat.Rgb32ui:
					return PixelFormat.Rgb;

				case PixelInternalFormat.Rgba8i:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba8ui:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba16i:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba16ui:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba32i:
					return PixelFormat.Rgba;
				case PixelInternalFormat.Rgba32ui:
					return PixelFormat.Rgba;
			

				case PixelInternalFormat.DepthComponent16:
					return PixelFormat.DepthComponent;
				case PixelInternalFormat.DepthComponent24:
					return PixelFormat.DepthComponent;
				case PixelInternalFormat.DepthComponent32:
					return PixelFormat.DepthComponent;
				case PixelInternalFormat.DepthComponent32f:
					return PixelFormat.DepthComponent;

				case PixelInternalFormat.Depth24Stencil8:
					return PixelFormat.DepthStencil;
				case PixelInternalFormat.Depth32fStencil8:
					return PixelFormat.DepthStencil;

				case (PixelInternalFormat)(All.StencilIndex1):
					return PixelFormat.StencilIndex;
				case (PixelInternalFormat)(All.StencilIndex4):
					return PixelFormat.StencilIndex;
				case (PixelInternalFormat)(All.StencilIndex8):
					return PixelFormat.StencilIndex;
				case (PixelInternalFormat)(All.StencilIndex16):
					return PixelFormat.StencilIndex;



				case PixelInternalFormat.CompressedRed:
					return PixelFormat.Red;
				case PixelInternalFormat.CompressedRg:
					return PixelFormat.Rg;
				case PixelInternalFormat.CompressedRgb:
					return PixelFormat.Rgb;
				case PixelInternalFormat.CompressedRgba:
					return PixelFormat.Rgba;
				case PixelInternalFormat.CompressedSrgb:
					return PixelFormat.Rgb;
				case PixelInternalFormat.CompressedSrgbAlpha:
					return PixelFormat.Rgba;

				case PixelInternalFormat.CompressedRedRgtc1:
					return PixelFormat.Red;
				case PixelInternalFormat.CompressedSignedRedRgtc1:
					return PixelFormat.Red;
				case PixelInternalFormat.CompressedRgRgtc2:
					return PixelFormat.Rg;
				case PixelInternalFormat.CompressedSignedRgRgtc2:
					return PixelFormat.Rg;
				case PixelInternalFormat.CompressedRgbaBptcUnorm:
					return PixelFormat.Rgba;
				case (PixelInternalFormat)All.CompressedSrgbAlphaBptcUnorm:
					return PixelFormat.Rgba;
				case PixelInternalFormat.CompressedRgbBptcSignedFloat:
					return PixelFormat.Rgb;
				case PixelInternalFormat.CompressedRgbBptcUnsignedFloat:
					return PixelFormat.Rgb;

				case (PixelInternalFormat)(All.CompressedRgb8Etc2):
					return PixelFormat.Rgb;
				case (PixelInternalFormat)(All.CompressedSrgb8Etc2):
					return PixelFormat.Rgb;
				case (PixelInternalFormat)(All.CompressedRgb8PunchthroughAlpha1Etc2):
					return PixelFormat.Rgb;
				case (PixelInternalFormat)(All.CompressedSrgb8PunchthroughAlpha1Etc2):
					return PixelFormat.Rgb;
				case (PixelInternalFormat)(All.CompressedRgba8Etc2Eac):
					return PixelFormat.Rgba;
				case (PixelInternalFormat)(All.CompressedSrgb8Alpha8Etc2Eac):
					return PixelFormat.Rgba;

				case (PixelInternalFormat)(All.CompressedR11Eac):
					return PixelFormat.Red;
				case (PixelInternalFormat)(All.CompressedSignedR11Eac):
					return PixelFormat.Red;
				case (PixelInternalFormat)(All.CompressedRg11Eac):
					return PixelFormat.Rg;
				case (PixelInternalFormat)(All.CompressedSignedRg11Eac):
					return PixelFormat.Rg;
			}

			throw new NotSupportedException($"\"{internalFormat}\" is not supported.");
		}
		
		/// <summary>
		/// Returns the closest OpenGL pixel internal format for the specified engine format.
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public static PixelInternalFormat GetClosestInternalFormat(Format format)
		{
			switch (format.Type)
			{
				case FormatType.R8:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm: return PixelInternalFormat.R8;
						case FormatSemantic.SNorm: return PixelInternalFormat.R8Snorm;
						case FormatSemantic.UInt: return PixelInternalFormat.R8ui;
						case FormatSemantic.SInt: return PixelInternalFormat.R8i;
					}
					break;

				case FormatType.R8G8:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm: return PixelInternalFormat.Rg8;
						case FormatSemantic.SNorm: return PixelInternalFormat.Rg8Snorm;
						case FormatSemantic.UInt: return PixelInternalFormat.Rg8ui;
						case FormatSemantic.SInt: return PixelInternalFormat.Rg8i;
					}
					break;

				case FormatType.R8G8B8:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm: return (format.Flags & FormatFlags.Srgb) != 0 ? PixelInternalFormat.Srgb8 : PixelInternalFormat.Rgb8;
						case FormatSemantic.SNorm: return PixelInternalFormat.Rgb8Snorm;
						case FormatSemantic.UInt: return PixelInternalFormat.Rgb8ui;
						case FormatSemantic.SInt: return PixelInternalFormat.Rgb8i;
					}
					break;

				case FormatType.R8G8B8A8:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm: return (format.Flags & FormatFlags.Srgb) != 0 ? PixelInternalFormat.Srgb8Alpha8 : PixelInternalFormat.Rgba8;
						case FormatSemantic.SNorm: return PixelInternalFormat.Rgba8Snorm;
						case FormatSemantic.UInt: return PixelInternalFormat.Rgba8ui;
						case FormatSemantic.SInt: return PixelInternalFormat.Rgba8i;
					}
					break;

				case FormatType.R16:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm: return PixelInternalFormat.R16;
						case FormatSemantic.SNorm: return PixelInternalFormat.R16Snorm;
						case FormatSemantic.UInt: return PixelInternalFormat.R16ui;
						case FormatSemantic.SInt: return PixelInternalFormat.R16i;
						case FormatSemantic.Float: return PixelInternalFormat.R16f;
					}
					break;

				case FormatType.R16G16:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm: return PixelInternalFormat.Rg16;
						case FormatSemantic.SNorm: return PixelInternalFormat.Rg16Snorm;
						case FormatSemantic.UInt: return PixelInternalFormat.Rg16ui;
						case FormatSemantic.SInt: return PixelInternalFormat.Rg16i;
						case FormatSemantic.Float: return PixelInternalFormat.Rg16f;
					}
					break;

				case FormatType.R16G16B16:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm: return PixelInternalFormat.Rgb16;
						case FormatSemantic.SNorm: return PixelInternalFormat.Rgb16Snorm;
						case FormatSemantic.UInt: return PixelInternalFormat.Rgb16ui;
						case FormatSemantic.SInt: return PixelInternalFormat.Rgb16i;
						case FormatSemantic.Float: return PixelInternalFormat.Rgb16f;
					}
					break;

				case FormatType.R16G16B16A16:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm: return PixelInternalFormat.Rgba16;
						case FormatSemantic.SNorm: return PixelInternalFormat.Rgba16Snorm;
						case FormatSemantic.UInt: return PixelInternalFormat.Rgba16ui;
						case FormatSemantic.SInt: return PixelInternalFormat.Rgba16i;
						case FormatSemantic.Float: return PixelInternalFormat.Rgba16f;
					}
					break;
					
				case FormatType.R32:
					switch (format.Semantic)
					{
						case FormatSemantic.UInt: return PixelInternalFormat.R32ui;
						case FormatSemantic.SInt: return PixelInternalFormat.R32i;
						case FormatSemantic.Float: return PixelInternalFormat.R32f;
					}
					break;

				case FormatType.R32G32:
					switch (format.Semantic)
					{
						case FormatSemantic.UInt: return PixelInternalFormat.Rg32ui;
						case FormatSemantic.SInt: return PixelInternalFormat.Rg32i;
						case FormatSemantic.Float: return PixelInternalFormat.Rg32f;
					}
					break;

				case FormatType.R32G32B32:
					switch (format.Semantic)
					{
						case FormatSemantic.UInt: return PixelInternalFormat.Rgb32ui;
						case FormatSemantic.SInt: return PixelInternalFormat.Rgb32i;
						case FormatSemantic.Float: return PixelInternalFormat.Rgb32f;
					}
					break;

				case FormatType.R32G32B32A32:
					switch (format.Semantic)
					{
						case FormatSemantic.UInt: return PixelInternalFormat.Rgba32ui;
						case FormatSemantic.SInt: return PixelInternalFormat.Rgba32i;
						case FormatSemantic.Float: return PixelInternalFormat.Rgba32f;
					}
					break;

				case FormatType.R3G3B2:
					return PixelInternalFormat.R3G3B2;

				case FormatType.R5G6B5:
					return PixelInternalFormat.Rgb8;

				case FormatType.R5G5B5A1:
					return PixelInternalFormat.Rgb5A1;

				case FormatType.R4G4B4A4:
					return PixelInternalFormat.Rgba4;

				case FormatType.R10G10B10A2:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm: return PixelInternalFormat.Rgb10A2;
						case FormatSemantic.UInt: return PixelInternalFormat.Rgb10A2ui;
					}
					break;

				case FormatType.R11G11B10:
					return PixelInternalFormat.R11fG11fB10f;

				case FormatType.R9G9B9E5:
					return PixelInternalFormat.Rgb9E5;

				case FormatType.S8:
					return (PixelInternalFormat)All.StencilIndex8;

				case FormatType.D16:
					return PixelInternalFormat.DepthComponent16;

				case FormatType.D24:
					return PixelInternalFormat.DepthComponent24;

				case FormatType.D32:
					return PixelInternalFormat.DepthComponent32f;

				case FormatType.D24S8:
					return PixelInternalFormat.Depth24Stencil8;

				case FormatType.D32S8:
					return PixelInternalFormat.Depth32fStencil8;
			}

			throw new NotSupportedException($"\"{format}\" is not supported.");
		}


		/// <summary>
		/// Gets the OpenGL pixel format that corresponds to the specified engine format.
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public static PixelFormat GetPixelFormat(Format format)
		{
			bool integer = format.Semantic == FormatSemantic.SInt || format.Semantic == FormatSemantic.UInt;

			switch (format.Type)
			{
				case FormatType.R8:
				case FormatType.R16:
				case FormatType.R32:
					return integer ? PixelFormat.RedInteger : PixelFormat.Red;
					
				case FormatType.R8G8:
				case FormatType.R16G16:
				case FormatType.R32G32:
					return integer ? PixelFormat.RgInteger : PixelFormat.Rg;

				case FormatType.R8G8B8:
				case FormatType.R16G16B16:
				case FormatType.R32G32B32:
				case FormatType.R3G3B2:
				case FormatType.R5G6B5:
				case FormatType.R9G9B9E5:
				case FormatType.R11G11B10:
					return integer ? PixelFormat.RgbInteger : PixelFormat.Rgb;

				case FormatType.R8G8B8A8:
				case FormatType.R16G16B16A16:
				case FormatType.R32G32B32A32:
				case FormatType.R5G5B5A1:
				case FormatType.R4G4B4A4:
				case FormatType.R10G10B10A2:
					return integer ? PixelFormat.RgbInteger : PixelFormat.Rgba;

				case FormatType.D16:
				case FormatType.D24:
				case FormatType.D32:
					return PixelFormat.DepthComponent;

				case FormatType.D24S8:
				case FormatType.D32S8:
					return PixelFormat.DepthStencil;

				case FormatType.S8:
					return PixelFormat.StencilIndex;
			}

			throw new NotSupportedException($"\"{format}\" is not supported.");
		}

		/// <summary>
		/// Gets the pixel type for the specified engine format.
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public static PixelType GetPixelType(Format format)
		{
			switch (format.Type)
			{
				case FormatType.R8:
				case FormatType.R8G8:
				case FormatType.R8G8B8:
				case FormatType.R8G8B8A8:
					switch (format.Semantic)
					{
						case FormatSemantic.SInt:
						case FormatSemantic.SNorm:
							return PixelType.Byte;

						default:
							return PixelType.UnsignedByte;
					}

				case FormatType.R16:
				case FormatType.R16G16:
				case FormatType.R16G16B16:
				case FormatType.R16G16B16A16:
					switch (format.Semantic)
					{
						case FormatSemantic.SInt:
						case FormatSemantic.SNorm:
							return PixelType.Short;

						case FormatSemantic.UInt:
						case FormatSemantic.UNorm:
							return PixelType.UnsignedShort;

						default:
							return PixelType.Float;
					}


				case FormatType.R32:
				case FormatType.R32G32:
				case FormatType.R32G32B32:
				case FormatType.R32G32B32A32:
					switch (format.Semantic)
					{
						case FormatSemantic.SInt:
						case FormatSemantic.SNorm:
							return PixelType.Short;

						case FormatSemantic.UInt:
						case FormatSemantic.UNorm:
							return PixelType.UnsignedShort;

						default:
							return PixelType.Float;
					}

				case FormatType.R3G3B2:
					if (format.Semantic == FormatSemantic.UNorm)
						return format.IsReversed ? PixelType.UnsignedByte233Reversed : PixelType.UnsignedByte332;
					break;

				case FormatType.R5G6B5:
					if (format.Semantic == FormatSemantic.UNorm)
						return format.IsReversed ? PixelType.UnsignedShort565Reversed : PixelType.UnsignedShort565;
					break;

				case FormatType.R5G5B5A1:
					if (format.Semantic == FormatSemantic.UNorm)
						return format.IsReversed ? PixelType.UnsignedShort1555Reversed : PixelType.UnsignedShort5551;
					break;

				case FormatType.R4G4B4A4:
					if (format.Semantic == FormatSemantic.UNorm)
						return format.IsReversed ? PixelType.UnsignedShort4444Reversed : PixelType.UnsignedShort4444;
					break;

				case FormatType.R10G10B10A2:
					if (format.Semantic == FormatSemantic.UNorm)
						return format.IsReversed ? PixelType.UnsignedInt2101010Reversed : PixelType.UnsignedInt1010102;
					break;

				case FormatType.R11G11B10:
					if (format.Semantic == FormatSemantic.Float && format.IsReversed)
						return PixelType.UnsignedInt10F11F11FRev;
					break;

				case FormatType.R9G9B9E5:
					if (format.Semantic == FormatSemantic.Float && format.IsReversed)
						return PixelType.UnsignedInt5999Rev;
					break;

				case FormatType.D16:
					if (format.Semantic == FormatSemantic.UNorm)
						return PixelType.UnsignedShort;
					break;

				case FormatType.D24:
					if (format.Semantic == FormatSemantic.UNorm)
						return PixelType.UnsignedInt248;
					break;

				case FormatType.D32:
					if (format.Semantic == FormatSemantic.Float)
						return PixelType.Float;
					break;

				case FormatType.D24S8:
					if (format.Semantic == FormatSemantic.UNorm)
						return PixelType.UnsignedInt248;
					break;

				case FormatType.D32S8:
					if (format.Semantic == FormatSemantic.Float)
						return PixelType.Float32UnsignedInt248Rev;
					break;

				case FormatType.S8:
					if (format.Semantic == FormatSemantic.UInt)
						return PixelType.UnsignedByte;
					break;
			}

			throw new NotSupportedException($"\"{format}\" is not supported.");
		}

		/// <summary>
		/// Maps a pixel format/type combination to an engine format.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="type"></param>
		/// <param name="norm"></param>
		/// <returns></returns>
		public static Format GetFormat(PixelFormat format, PixelType type, bool norm)
		{
			ColorMapping input = ColorMapping.RGBA;
			ColorMapping output = ColorMapping.RGBA;
			
			if (format == PixelFormat.Bgr || format == PixelFormat.Bgra ||
				format == PixelFormat.BgrInteger || format == PixelFormat.BgraInteger)
			{
				output = input = ColorMapping.BGRA;
			}

			FormatFlags flags = FormatFlags.None;

			if (format == (PixelFormat)All.Srgb)
			{
				flags |= FormatFlags.Srgb;
				format = PixelFormat.Rgb;
			}
			else if (format == (PixelFormat)All.SrgbAlpha)
			{
				flags |= FormatFlags.Srgb;
				format = PixelFormat.Rgba;
			}

			switch (format)
			{
				case PixelFormat.Red:
					switch (type)
					{
						case PixelType.Byte: return new Format(FormatType.R8, norm ? FormatSemantic.SNorm : FormatSemantic.SInt);
						case PixelType.UnsignedByte: return new Format(FormatType.R8, norm ? FormatSemantic.UNorm : FormatSemantic.UInt);
						case PixelType.Short: return new Format(FormatType.R16, norm ? FormatSemantic.SNorm : FormatSemantic.SInt);
						case PixelType.UnsignedShort: return new Format(FormatType.R16, norm ? FormatSemantic.UNorm : FormatSemantic.UInt);
						case PixelType.Int: return new Format(FormatType.R32, FormatSemantic.SInt);
						case PixelType.UnsignedInt: return new Format(FormatType.R32, FormatSemantic.UInt);
						case PixelType.HalfFloat: return new Format(FormatType.R16, FormatSemantic.Float);
						case PixelType.Float: return new Format(FormatType.R32, FormatSemantic.Float);
					}
					break;

				case PixelFormat.RedInteger:
					switch (type)
					{
						case PixelType.Byte: return new Format(FormatType.R8, FormatSemantic.SInt);
						case PixelType.UnsignedByte: return new Format(FormatType.R8, FormatSemantic.UInt);
						case PixelType.Short: return new Format(FormatType.R16, FormatSemantic.SInt);
						case PixelType.UnsignedShort: return new Format(FormatType.R16, FormatSemantic.UInt);
						case PixelType.Int: return new Format(FormatType.R32, FormatSemantic.SInt);
						case PixelType.UnsignedInt: return new Format(FormatType.R32, FormatSemantic.UInt);
					}
					break;

				case PixelFormat.Rg:
					switch (type)
					{
						case PixelType.Byte: return new Format(FormatType.R8G8, norm ? FormatSemantic.SNorm : FormatSemantic.SInt);
						case PixelType.UnsignedByte: return new Format(FormatType.R8G8, norm ? FormatSemantic.UNorm : FormatSemantic.UInt);
						case PixelType.Short: return new Format(FormatType.R16G16, norm ? FormatSemantic.SNorm : FormatSemantic.SInt);
						case PixelType.UnsignedShort: return new Format(FormatType.R16G16, norm ? FormatSemantic.UNorm : FormatSemantic.UInt);
						case PixelType.Int: return new Format(FormatType.R32G32, FormatSemantic.SInt);
						case PixelType.UnsignedInt: return new Format(FormatType.R32G32, FormatSemantic.UInt);
						case PixelType.HalfFloat: return new Format(FormatType.R16G16, FormatSemantic.Float);
						case PixelType.Float: return new Format(FormatType.R32G32, FormatSemantic.Float);
					}
					break;

				case PixelFormat.RgInteger:
					switch (type)
					{
						case PixelType.Byte: return new Format(FormatType.R8G8, FormatSemantic.SInt);
						case PixelType.UnsignedByte: return new Format(FormatType.R8G8, FormatSemantic.UInt);
						case PixelType.Short: return new Format(FormatType.R16G16, FormatSemantic.SInt);
						case PixelType.UnsignedShort: return new Format(FormatType.R16G16, FormatSemantic.UInt);
						case PixelType.Int: return new Format(FormatType.R32G32, FormatSemantic.SInt);
						case PixelType.UnsignedInt: return new Format(FormatType.R32G32, FormatSemantic.UInt);
					}
					break;

				case PixelFormat.Rgb:
				case PixelFormat.Bgr:
					switch (type)
					{
						case PixelType.Byte: return new Format(FormatType.R8G8B8, norm ? FormatSemantic.SNorm : FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedByte: return new Format(FormatType.R8G8B8, norm ? FormatSemantic.UNorm : FormatSemantic.UInt, input, output, flags);
						case PixelType.Short: return new Format(FormatType.R16G16B16, norm ? FormatSemantic.SNorm : FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedShort: return new Format(FormatType.R16G16B16, norm ? FormatSemantic.UNorm : FormatSemantic.UInt, input, output, flags);
						case PixelType.Int: return new Format(FormatType.R32G32B32, FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedInt: return new Format(FormatType.R32G32B32, FormatSemantic.UInt, input, output, flags);
						case PixelType.HalfFloat: return new Format(FormatType.R16G16B16, FormatSemantic.Float, input, output, flags);
						case PixelType.Float: return new Format(FormatType.R32G32B32, FormatSemantic.Float, input, output, flags);
					}
					break;

				case PixelFormat.BgrInteger:
				case PixelFormat.RgbInteger:
					switch (type)
					{
						case PixelType.Byte: return new Format(FormatType.R8G8B8, FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedByte: return new Format(FormatType.R8G8B8, FormatSemantic.UInt, input, output, flags);
						case PixelType.Short: return new Format(FormatType.R16G16B16, FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedShort: return new Format(FormatType.R16G16B16, FormatSemantic.UInt, input, output, flags);
						case PixelType.Int: return new Format(FormatType.R32G32B32, FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedInt: return new Format(FormatType.R32G32B32, FormatSemantic.UInt, input, output, flags);
					}
					break;

				case PixelFormat.Rgba:
				case PixelFormat.Bgra:
					switch (type)
					{
						case PixelType.Byte: return new Format(FormatType.R8G8B8A8, norm ? FormatSemantic.SNorm : FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedByte: return new Format(FormatType.R8G8B8A8, norm ? FormatSemantic.UNorm : FormatSemantic.UInt, input, output, flags);
						case PixelType.Short: return new Format(FormatType.R16G16B16A16, norm ? FormatSemantic.SNorm : FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedShort: return new Format(FormatType.R16G16B16A16, norm ? FormatSemantic.UNorm : FormatSemantic.UInt, input, output, flags);
						case PixelType.Int: return new Format(FormatType.R32G32B32A32, FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedInt: return new Format(FormatType.R32G32B32A32, FormatSemantic.UInt, input, output, flags);
						case PixelType.HalfFloat: return new Format(FormatType.R16G16B16A16, FormatSemantic.Float, input, output, flags);
						case PixelType.Float: return new Format(FormatType.R32G32B32A32, FormatSemantic.Float, input, output, flags);
					}
					break;

				case PixelFormat.BgraInteger:
				case PixelFormat.RgbaInteger:
					switch (type)
					{
						case PixelType.Byte: return new Format(FormatType.R8G8B8A8, FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedByte: return new Format(FormatType.R8G8B8A8, FormatSemantic.UInt, input, output, flags);
						case PixelType.Short: return new Format(FormatType.R16G16B16A16, FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedShort: return new Format(FormatType.R16G16B16A16, FormatSemantic.UInt, input, output, flags);
						case PixelType.Int: return new Format(FormatType.R32G32B32A32, FormatSemantic.SInt, input, output, flags);
						case PixelType.UnsignedInt: return new Format(FormatType.R32G32B32A32, FormatSemantic.UInt, input, output, flags);
					}
					break;
					
				case PixelFormat.DepthComponent:
					switch (type)
					{
						case PixelType.UnsignedShort: return new Format(FormatType.D16, FormatSemantic.UNorm);
						case PixelType.UnsignedInt248: return new Format(FormatType.D24, FormatSemantic.UNorm);
						case PixelType.Float: return new Format(FormatType.D32, FormatSemantic.Float);
					}
					break;

				case PixelFormat.DepthStencil:
					switch (type)
					{
						case PixelType.UnsignedInt248: return new Format(FormatType.D24S8, FormatSemantic.UNorm);
						case PixelType.Float32UnsignedInt248Rev: return new Format(FormatType.D32S8, FormatSemantic.Float);
					}
					break;

				case PixelFormat.StencilIndex:
					switch (type)
					{
						case PixelType.UnsignedByte: return new Format(FormatType.S8, FormatSemantic.UInt);
					}
					break;
			}

			throw new NotSupportedException($"Unable to map OpenGL pixel format \"{format}\" and type \"{type}\" to an engine pixel format.");
		}

		/// <summary>
		/// Gets the vertex attribute type for the specified format.
		/// </summary>
		/// <param name="format"></param>
		/// <returns></returns>
		public static VertexAttribPointerType GetVertexType(Format format)
		{
			if ((format.Flags & FormatFlags.Srgb) != 0)
				throw new ArgumentException("sRGB can not be used for vertices", nameof(format));

			switch (format.Type)
			{
				case FormatType.R8:
				case FormatType.R8G8:
				case FormatType.R8G8B8:
				case FormatType.R8G8B8A8:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm:
						case FormatSemantic.UInt:
							return VertexAttribPointerType.UnsignedByte;

						case FormatSemantic.SNorm:
						case FormatSemantic.SInt:
							return VertexAttribPointerType.Byte;
					}
					break;

				case FormatType.R16:
				case FormatType.R16G16:
				case FormatType.R16G16B16:
				case FormatType.R16G16B16A16:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm:
						case FormatSemantic.UInt:
							return VertexAttribPointerType.UnsignedShort;

						case FormatSemantic.SNorm:
						case FormatSemantic.SInt:
							return VertexAttribPointerType.Short;

						case FormatSemantic.Float:
							return VertexAttribPointerType.HalfFloat;
					}
					break;

				case FormatType.R32:
				case FormatType.R32G32:
				case FormatType.R32G32B32:
				case FormatType.R32G32B32A32:
					switch (format.Semantic)
					{
						case FormatSemantic.UNorm:
						case FormatSemantic.UInt:
							return VertexAttribPointerType.UnsignedInt;

						case FormatSemantic.SNorm:
						case FormatSemantic.SInt:
							return VertexAttribPointerType.Int;

						case FormatSemantic.Float:
							return VertexAttribPointerType.Float;
					}
					break;

				case FormatType.R10G10B10A2:
					if (format.IsReversed)
					{
						switch (format.Semantic)
						{
							case FormatSemantic.UNorm:
							case FormatSemantic.UInt:
								return VertexAttribPointerType.UnsignedInt2101010Rev;

							case FormatSemantic.SNorm:
							case FormatSemantic.SInt:
								return VertexAttribPointerType.Int2101010Rev;
						}
					}
					break;
			}
			
			throw new NotSupportedException($"\"{format}\" is not supported.");
		}

		public static PixelType GetDefaultPixelType(PixelFormat format)
		{
			switch (format)
			{
				case PixelFormat.DepthStencil:
					return PixelType.UnsignedInt248;
				default:
					return PixelType.UnsignedByte;
			}
		}

		#endregion
	}
}
