﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// An interface for all OpenGL objects.
	/// </summary>
	public interface IGLObject
	{
		#region Properties

		/// <summary>
		/// Gets the <see cref="GLContext"/> this object belongs to.
		/// </summary>
		GLContext Context { get; }
		
		/// <summary>
		/// Gets the object ID.
		/// </summary>
		uint ID { get; }
		
		#endregion
	}
}
