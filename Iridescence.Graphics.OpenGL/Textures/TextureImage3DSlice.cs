﻿using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a slice of a 3D texture image.
	/// </summary>
	public class TextureImage3DSlice : TextureImage, IFramebufferAttachment
	{
		#region Fields

		#endregion

		#region Properties
		
		internal TextureImage3D Image { get; }

		public int ZOffset { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureImage3DSlice.
		/// </summary>
		internal TextureImage3DSlice(TextureImage3D image, int zOffset)
			: base(image.Texture, image.Target, image.InternalFormat, image.Width, image.Height, 1, image.Level)
		{
			this.Image = image;
			this.ZOffset = zOffset;
		}

		#endregion

		#region Methods

		public override void Read(MemoryImage dest, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, dest);
			this.Image.Read(dest, x, y, this.ZOffset);
		}

		public override Task ReadAsync(MemoryImage dest, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, dest);
			return this.Image.ReadAsync(dest, x, y, this.ZOffset);
		}

		public override void Write(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source);
			this.Image.Write(source, x, y, this.ZOffset);
		}

		public override Task WriteAsync(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source);
			return this.Image.WriteAsync(source, x, y, this.ZOffset);
		}

		public void AttachToFramebuffer(FramebufferTarget target, FramebufferAttachment attachment)
		{
			//GL.FramebufferTexture3D(target, attachment, this.Image.Target, this.Texture.ID, this.Image.Level, this.ZOffset);
			this.Context.GL.FramebufferTextureLayer(target, attachment, this.Texture.ID, this.Image.Level, this.ZOffset);
		}
		
		#endregion
	}
}
