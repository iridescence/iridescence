﻿using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL sampler object.
	/// </summary>
	public sealed class Sampler : GLContextChild, IGLObject
	{
		#region Fields

		private TextureMinFilter minFilter;
		private TextureMagFilter magFilter;
		private float lodBias;
		private float minLod;
		private float maxLod;
		private TextureWrapMode wrapX;
		private TextureWrapMode wrapY;
		private TextureWrapMode wrapZ;
		private Color4 borderColor;
		private float maxAnisotropy;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the ID of the sampler object.
		/// </summary>
		public uint ID { get; }

		/// <summary>
		/// Gets or sets the texture minification filter.
		/// </summary>
		public TextureMinFilter MinFilter 
		{
			get => this.minFilter;
			set
			{
				this.minFilter = value;
				this.Context.Invoke(() =>
				{
					this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureMinFilter, (int)value);
				});
			}
		}
		
		/// <summary>
		/// Gets or sets the texture magnification filter.
		/// </summary>
		public TextureMagFilter MagFilter
		{
			get => this.magFilter;
			set
			{
				this.magFilter = value;
				this.Context.Invoke(() =>
				{
					this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureMagFilter, (int)value);
				});
			}
		}

		/// <summary>
		/// Gets or sets the texture level of detail bias.
		/// </summary>
		public float LodBias
		{
			get => this.lodBias;
			set
			{
				this.lodBias = value;
				this.Context.Invoke(() =>
				{
					this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureLodBias, value);
				});
			}
		}

		/// <summary>
		/// Gest or sets the minimum level of detail.
		/// </summary>
		public float MinLod
		{
			get => this.minLod;
			set
			{
				this.minLod = value;
				this.Context.Invoke(() =>
				{
					this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureMinLod, value);
				});
			}
		}

		/// <summary>
		/// Gest or sets the maximum level of detail.
		/// </summary>
		public float MaxLod
		{
			get => this.maxLod;
			set
			{
				this.maxLod = value;
				this.Context.Invoke(() =>
				{
					this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureMaxLod, value);
				});
			}
		}

		/// <summary>
		/// Gets the texture wrap mode on the X axis.
		/// </summary>
		public TextureWrapMode WrapX
		{
			get => this.wrapX;
			set
			{
				this.wrapX = value;
				this.Context.Invoke(() =>
				{
					this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureWrapS, (int)value);
				});
			}
		}

		/// <summary>
		/// Gets the texture wrap mode on the Y axis.
		/// </summary>
		public TextureWrapMode WrapY
		{
			get => this.wrapY;
			set
			{
				this.wrapY = value;
				this.Context.Invoke(() =>
				{
					this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureWrapT, (int)value);
				});
			}
		}

		/// <summary>
		/// Gets the texture wrap mode on the Z axis.
		/// </summary>
		public TextureWrapMode WrapZ
		{
			get => this.wrapZ;
			set
			{
				this.wrapZ = value;
				this.Context.Invoke(() =>
				{
					this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureWrapR, (int)value);
				});
			}
		}

		/// <summary>
		/// Gets or sets the texture border color.
		/// </summary>
		public Color4 BorderColor
		{
			get => this.borderColor;
			set
			{
				this.borderColor = value;
				this.Context.Invoke(() =>
				{
					this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureBorderColor, value.ToArray());
				});
			}
		}
		
		/// <summary>
		/// Gets or sets the maximum level of anisotropy.
		/// </summary>
		public float MaxAnisotropy
		{
			get => this.maxAnisotropy;
			set
			{
				value = Utility.Clamp(value, 0.0f, this.Context.Capabilities.MaxAnisotropy);

				this.maxAnisotropy = value;

				if (this.Context.Capabilities.HasExtension("GL_EXT_texture_filter_anisotropic"))
				{
					this.Context.Invoke(() =>
					{
						this.Context.GL.SamplerParameter(this.ID, SamplerParameterName.TextureMaxAnisotropyExt, value);
					});
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Sampler.
		/// </summary>
		public Sampler(GLContext context)
			: base(context)
		{
			this.ID = this.Context.Invoke(this.Context.GL.GenSampler);
			GLTrace.Instance.ObjectCreated(nameof(Sampler), this.ID);

			this.minFilter = TextureMinFilter.NearestMipmapLinear;
			this.magFilter = TextureMagFilter.Linear;
			this.lodBias = 0.0f;
			this.minLod = -1000.0f;
			this.maxLod = 1000.0f;
			this.wrapX = TextureWrapMode.Repeat;
			this.wrapY = TextureWrapMode.Repeat;
			this.wrapZ = TextureWrapMode.Repeat;
			this.borderColor = Color4.Transparency;
			this.maxAnisotropy = 1.0f;
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			return $"{this.ID}";
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			void delete()
			{
				this.Context.GL.DeleteSampler(this.ID);
				GLTrace.Instance.ObjectDeleted(nameof(Sampler), this.ID);
			}

			if (managed)
				this.Context.Invoke(delete);
			else
				this.Context.Post(delete);
		}

		#endregion
	}
}

