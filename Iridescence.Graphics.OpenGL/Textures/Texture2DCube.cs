﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a 2D cubemap texture.
	/// </summary>
	public class Texture2DCube : Texture
	{
		#region Fields

		private readonly TextureImage2D[,] images;

		private static readonly TextureTarget[] faceTargets =
		{
			TextureTarget.TextureCubeMapPositiveX,
			TextureTarget.TextureCubeMapNegativeX,
			TextureTarget.TextureCubeMapPositiveY,
			TextureTarget.TextureCubeMapNegativeY,
			TextureTarget.TextureCubeMapPositiveZ,
			TextureTarget.TextureCubeMapNegativeZ,
		};

		#endregion

		#region Properties

		/// <summary>
		/// Gets the width of the texture.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the height of the texture.
		/// </summary>
		public int Height { get; }

		/// <summary>
		/// Gets the number of mip maps in the texture.
		/// </summary>
		public int MipMaps { get; }

		/// <summary>
		/// Gets the texture parameters of this texture.
		/// </summary>
		public TextureParameters Parameters { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture2DCube.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="internalFormat">The internal texture format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture2DCube(GLContext context, PixelInternalFormat internalFormat, int width, int height, int mipMaps)
			: base(context, TextureTarget.TextureCubeMap, internalFormat)
		{
			PixelFormat pixelFormat = FormatUtility.GetPixelFormatForInternalFormat(internalFormat);
			PixelType pixelType = FormatUtility.GetDefaultPixelType(pixelFormat);;

			this.Width = width;
			this.Height = height;
			this.MipMaps = mipMaps;
			this.Parameters = new TextureParameters(this);

			this.images = new TextureImage2D[6, mipMaps];
			for (int face = 0; face < 6; ++face)
			{
				for (int i = 0; i < mipMaps; ++i)
				{
					this.images[face, i] = new TextureImage2D(this, faceTargets[face], internalFormat, width, height, i);
					if (width > 1) width >>= 1;
					if (height > 1) height >>= 1;
				}
			}

			// reserve memory.
			this.Context.Invoke(() =>
			{
				this.Bind(true);
				this.Parameters.MaxLevel = mipMaps - 1;
				for (int face = 0; face < 6; ++face)
				{
					for (int i = 0; i < mipMaps; ++i)
					{
						this.Context.GL.TexImage2D(faceTargets[face], i, internalFormat, this.images[face, i].Width, this.images[face, i].Height, 0, pixelFormat, pixelType, IntPtr.Zero);
					}
				}
			});
		}

		/// <summary>
		/// Creates a new Texture2DCube.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="format">The texture pixel format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture2DCube(GLContext context, Format format, int width, int height, int mipMaps)
			: this(context, FormatUtility.GetClosestInternalFormat(format), width, height, mipMaps)
		{
			if (!format.Output.IsIdentity)
			{
				this.Parameters.Swizzle = format.Output;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the image for the specified face and mip index.
		/// </summary>
		/// <param name="face"></param>
		/// <param name="mipIndex"></param>
		/// <returns></returns>
		public TextureImage2D GetImage(CubeFace face, int mipIndex)
		{
			return this.images[(int)face, mipIndex];
		}

		#endregion
	}
}
