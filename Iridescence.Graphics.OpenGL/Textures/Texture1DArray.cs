﻿using System;
using System.Threading.Tasks;
using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a 1D array texture.
	/// </summary>
	public class Texture1DArray : Texture
	{
		#region Fields

		private readonly TextureImage2D[] images;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the width of the texture.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the number of textures in the array.
		/// </summary>
		public int ArrayCount { get; }

		/// <summary>
		/// Gets the number of mip maps in the texture.
		/// </summary>
		public int MipMaps { get; }

		/// <summary>
		/// Gets the texture parameters of this texture.
		/// </summary>
		public TextureParameters Parameters { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture2D.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="internalFormat">The internal texture format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="arrayCount">The number of textures in the array.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture1DArray(GLContext context, PixelInternalFormat internalFormat, int width, int arrayCount, int mipMaps)
			: base(context, TextureTarget.Texture1DArray, internalFormat)
		{
			PixelFormat pixelFormat = FormatUtility.GetPixelFormatForInternalFormat(internalFormat);
			PixelType pixelType = FormatUtility.GetDefaultPixelType(pixelFormat);;

			this.Width = width;
			this.ArrayCount = arrayCount;
			this.MipMaps = mipMaps;
			this.Parameters = new TextureParameters(this);

			this.images = new TextureImage2D[mipMaps];
			for (int i = 0; i < mipMaps; ++i)
			{
				this.images[i] = new TextureImage2D(this, TextureTarget.Texture1DArray, internalFormat, width, arrayCount, i);
				if (width > 1) width >>= 1;
			}

			// reserve memory.
			this.Context.Invoke(() =>
			{
				this.Bind(true);
				this.Parameters.MaxLevel = mipMaps - 1;
				for (int i = 0; i < mipMaps; ++i)
				{
					this.Context.GL.TexImage2D(TextureTarget.Texture1DArray, i, internalFormat, this.images[i].Width, this.images[i].Height, 0, pixelFormat, pixelType, IntPtr.Zero);
				}
			});
		}

		/// <summary>
		/// Creates a new Texture2D.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="format">The texture pixel format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="arrayCount">The number of textures in the array.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture1DArray(GLContext context, Format format, int width, int arrayCount, int mipMaps)
			: this(context, FormatUtility.GetClosestInternalFormat(format), width, arrayCount, mipMaps)
		{
			if (!format.Output.IsIdentity)
			{
				this.Parameters.Swizzle = format.Output;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the image for the specified array and mip index.
		/// </summary>
		/// <param name="arrayIndex"></param>
		/// <param name="mipIndex"></param>
		/// <returns></returns>
		public TextureImage2DRow GetImage(int arrayIndex, int mipIndex)
		{
			return this.images[mipIndex].GetRow(arrayIndex);
		}

		/// <summary>
		/// Returns the image representing the whole array at the specified mip index.
		/// </summary>
		/// <param name="mipIndex"></param>
		/// <returns></returns>
		public TextureImage2D GetArray(int mipIndex)
		{
			return this.images[mipIndex];
		}

		/// <summary>
		/// Creates a new Texture2DArray with the specified number of textures and copies the textures from this array to the new array.
		/// </summary>
		/// <param name="newCount">The new array count.</param>
		/// <returns></returns>
		public Texture1DArray Resize(int newCount)
		{
			Texture1DArray newArray = new Texture1DArray(this.Context, this.InternalFormat, this.Width, newCount, this.MipMaps);

			int shared = Utility.Min(newCount, this.ArrayCount);

			for (int i = 0; i < this.MipMaps; ++i)
			{
				this.images[i].CopyTo(newArray.images[i], 0, 0, 0, 0, 0, 0, this.images[i].Width, shared, 1);
			}

			this.Parameters.CopyTo(newArray.Parameters);

			return newArray;
		}

		/// <summary>
		/// Creates a new Texture2DArray with the specified number of textures and copies the textures from this array to the new array.
		/// </summary>
		/// <param name="newCount">The new array count.</param>
		/// <returns></returns>
		public async Task<Texture1DArray> ResizeAsync(int newCount)
		{
			Texture1DArray newArray = new Texture1DArray(this.Context, this.InternalFormat, this.Width, newCount, this.MipMaps);

			int shared = Utility.Min(newCount, this.ArrayCount);

			for (int i = 0; i < this.MipMaps; ++i)
			{
				await this.images[i].CopyToAsync(newArray.images[i], 0, 0, 0, 0, 0, 0, this.images[i].Width, shared, 1);
			}

			await this.Parameters.CopyToAsync(newArray.Parameters);

			return newArray;
		}

		#endregion
	}
}
