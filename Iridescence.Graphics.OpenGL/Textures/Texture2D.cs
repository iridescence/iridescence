﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a 2D texture.
	/// </summary>
	public class Texture2D : Texture
	{
		#region Fields

		private readonly TextureImage2D[] images;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the width of the texture.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the height of the texture.
		/// </summary>
		public int Height { get; }

		/// <summary>
		/// Gets the number of mip maps in the texture.
		/// </summary>
		public int MipMaps { get; }

		/// <summary>
		/// Gets the texture parameters of this texture.
		/// </summary>
		public TextureParameters Parameters { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture2D.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="internalFormat">The internal texture format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture2D(GLContext context, PixelInternalFormat internalFormat, int width, int height, int mipMaps)
			: base(context, TextureTarget.Texture2D, internalFormat)
		{
			PixelFormat pixelFormat = FormatUtility.GetPixelFormatForInternalFormat(internalFormat);
			PixelType pixelType = FormatUtility.GetDefaultPixelType(pixelFormat);

			this.Width = width;
			this.Height = height;
			this.MipMaps = mipMaps;
			this.Parameters = new TextureParameters(this);

			this.images = new TextureImage2D[mipMaps];
			for (int i = 0; i < mipMaps; ++i)
			{
				this.images[i] = new TextureImage2D(this, TextureTarget.Texture2D, internalFormat, width, height, i);
				if (width > 1) width >>= 1;
				if (height > 1) height >>= 1;
			}

			// reserve memory.
			this.Context.Invoke(() =>
			{
				this.Bind(true);
				this.Parameters.MaxLevel = mipMaps - 1;
				for (int i = 0; i < mipMaps; ++i)
				{
					this.Context.GL.TexImage2D(TextureTarget.Texture2D, i, internalFormat, this.images[i].Width, this.images[i].Height, 0, pixelFormat, pixelType, IntPtr.Zero);
				}
			});
		}

		/// <summary>
		/// Creates a new Texture2D.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="format">The texture pixel format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture2D(GLContext context, Format format, int width, int height, int mipMaps)
			: this(context, FormatUtility.GetClosestInternalFormat(format), width, height, mipMaps)
		{
			if (!format.Output.IsIdentity)
			{
				this.Parameters.Swizzle = format.Output;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates a <see cref="Texture2D"/> from a 2D image.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="image"></param>
		/// <param name="withMipMaps"></param>
		/// <returns></returns>
		public static Texture2D FromImage(GLContext context, Image image, bool withMipMaps)
		{
			if(image.Depth > 1)
				throw new ArgumentException("Image is three-dimensional.", nameof(image));

			int numMipMaps = withMipMaps ? GetNumberOfMipMaps(image.Width, image.Height, 1) : 1;

			Texture2D texture = new Texture2D(context, image.Format, image.Width, image.Height, numMipMaps);

			image.CopyTo(texture.GetImage(0));

			return texture;
		}

		/// <summary>
		/// Asynchronously creates a <see cref="Texture2D"/> from a 2D image.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="image"></param>
		/// <param name="withMipMaps"></param>
		/// <returns></returns>
		public static async Task<Texture2D> FromImageAsync(GLContext context, Image image, bool withMipMaps)
		{
			if(image.Depth > 1)
				throw new ArgumentException("Image is three-dimensional.", nameof(image));

			int numMipMaps = withMipMaps ? GetNumberOfMipMaps(image.Width, image.Height, 1) : 1;

			Texture2D texture = new Texture2D(context, image.Format, image.Width, image.Height, numMipMaps);

			await image.CopyToAsync(texture.GetImage(0));

			return texture;
		}

		/// <summary>
		/// Returns the image for the specified mip index.
		/// </summary>
		/// <param name="mipIndex"></param>
		/// <returns></returns>
		public TextureImage2D GetImage(int mipIndex)
		{
			return this.images[mipIndex];
		}

		/// <summary>
		/// Generates map maps for the texture.
		/// </summary>
		public void GenerateMipMaps()
		{
			this.Context.Invoke(() =>
			{
				this.Bind(true);
				this.Context.GL.GenerateMipmap((GenerateMipmapTarget)this.Target);
			});
		}

		#endregion
	}
}
