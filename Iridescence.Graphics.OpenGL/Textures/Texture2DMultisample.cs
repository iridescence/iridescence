﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a 2D multisample texture.
	/// </summary>
	public class Texture2DMultisample : Texture
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the width of the texture.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the height of the texture.
		/// </summary>
		public int Height { get; }

		/// <summary>
		/// Gets the texture parameters of this texture.
		/// </summary>
		public TextureParameters Parameters { get; }

		/// <summary>
		/// Gets the texture image.
		/// </summary>
		public TextureImage2DMultisample Image { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture2DMultisample.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="internalFormat">The internal texture format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="samples">The number of samples in the multisample texture's image.</param>
		/// <param name="fixedSampleLocations">Specifies whether the image will use identical sample locations and the same number of samples for all texels in the image, and the sample locations will not depend on the internal format or size of the image.</param>
		public Texture2DMultisample(GLContext context, PixelInternalFormat internalFormat, int width, int height, int samples, bool fixedSampleLocations)
			: base(context, TextureTarget.Texture2DMultisample, internalFormat)
		{
			this.Width = width;
			this.Height = height;
			this.Parameters = new TextureParameters(this);
			this.Image = new TextureImage2DMultisample(this, internalFormat, width, height, samples);
			this.Context.Invoke(() =>
			{
				this.Bind(true);
				this.Context.GL.TexImage2DMultisample(TextureTarget.Texture2DMultisample, samples, internalFormat, width, height, fixedSampleLocations);
			});
		}

		/// <summary>
		/// Creates a new Texture2DMultisample.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="format">The texture pixel format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="samples">The number of samples in the multisample texture's image.</param>
		/// <param name="fixedSampleLocations">Specifies whether the image will use identical sample locations and the same number of samples for all texels in the image, and the sample locations will not depend on the internal format or size of the image.</param>
		public Texture2DMultisample(GLContext context, Format format, int width, int height, int samples, bool fixedSampleLocations)
			: this(context, FormatUtility.GetClosestInternalFormat(format), width, height, samples, fixedSampleLocations)
		{
			if (!format.Output.IsIdentity)
			{
				this.Parameters.Swizzle = format.Output;
			}
		}

		#endregion

		#region Methods

		#endregion
	}
}
