﻿using System;
using System.Linq;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Image copying methods.
	/// </summary>
	internal static class TextureImageUtil
	{
		#region Methods

		internal static Framebuffer WrapInFramebuffer(GLContext context, IFramebufferAttachment attachment)
		{
			// create FBO.
			Framebuffer fbo = new Framebuffer(context);

			// bind FBO.
			context.GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, fbo.ID);
			
			State currentState = context.CurrentState;
			if (currentState != null)
				currentState.ReadFramebuffer = fbo.ID;
			
			// attach to FBO.
			attachment.AttachToFramebuffer(FramebufferTarget.ReadFramebuffer, FramebufferAttachment.ColorAttachment0);
			context.GL.ReadBuffer(ReadBufferMode.ColorAttachment0);

			if (context.GL.CheckFramebufferStatus(FramebufferTarget.ReadFramebuffer) != FramebufferErrorCode.FramebufferComplete)
				throw new NotSupportedException("Can't render to framebuffer.");

			return fbo;
		}

		internal static Framebuffer WrapInFramebuffer(GLContext context, IFramebufferLayerAttachment attachment, int layer)
		{
			// create FBO.
			Framebuffer fbo = new Framebuffer(context);

			// bind FBO.
			context.GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, fbo.ID);

			State currentState = context.CurrentState;
			if (currentState != null)
				currentState.ReadFramebuffer = fbo.ID;

			// attach to FBO.
			attachment.AttachToFramebuffer(FramebufferTarget.ReadFramebuffer, FramebufferAttachment.ColorAttachment0, layer);
			context.GL.ReadBuffer(ReadBufferMode.ColorAttachment0);

			if (context.GL.CheckFramebufferStatus(FramebufferTarget.ReadFramebuffer) != FramebufferErrorCode.FramebufferComplete)
				throw new NotSupportedException("Can't render to framebuffer.");

			return fbo;
		}

		private static bool any(params object[] objs)
		{
			return objs.Any(t => t != null);
		}

		/// <summary>
		/// Tries to copy image data on hardware.
		/// </summary>
		/// <returns>True, if the image was copied. False otherwise.</returns>
		public static bool TryCopy(Image source, Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			source.ThrowIfOutOfBounds(sourceX, sourceY, sourceZ, width, height, depth);
			dest.ThrowIfOutOfBounds(destX, destY, destZ, width, height, depth);

			TextureImage1D dest1D = dest as TextureImage1D;
			TextureImage2DRow dest2DRow = dest as TextureImage2DRow;
			TextureImage2D dest2D = dest as TextureImage2D;
			TextureImage3DSlice dest3DSlice = dest as TextureImage3DSlice;

			if (!any(dest1D, dest2D, dest2DRow, dest3DSlice))
				return false;

			var gl = (dest1D?.Context ?? dest2DRow?.Context ?? dest2D?.Context ?? dest3DSlice?.Context).GL;
			
			switch (source)
			{
				case TextureImage1D source1D:
					using (WrapInFramebuffer(source1D.Texture.Context, source1D))
					{
						if (dest1D != null)
						{
							dest1D.Texture.Bind(true);
							gl.CopyTexSubImage1D(dest1D.Target, dest1D.Level, destX, sourceX, 0, width);
						}
						else if (dest2DRow != null)
						{
							dest2DRow.Texture.Bind(true);
							gl.CopyTexSubImage2D(dest2DRow.Target, dest2DRow.Level, destX, dest2DRow.YOffset, sourceX, 0, width, 1);
						}
						else if (dest2D != null)
						{
							dest2D.Texture.Bind(true);
							gl.CopyTexSubImage2D(dest2D.Target, dest2D.Level, destX, destY, sourceX, 0, width, 1);
						}
						else if (dest3DSlice != null)
						{
							dest3DSlice.Texture.Bind(true);
							gl.CopyTexSubImage3D(dest3DSlice.Target, dest3DSlice.Level, destX, destY, dest3DSlice.ZOffset, sourceX, 0, width, 1);
						}
						else
						{
							return false;
						}
					}
					break;

				case TextureImage2DRow source2DRow:
					using (WrapInFramebuffer(source2DRow.Texture.Context, source2DRow.Image))
					{
						if (dest1D != null)
						{
							dest1D.Texture.Bind(true);
							gl.CopyTexSubImage1D(dest1D.Target, dest1D.Level, destX, sourceX, source2DRow.YOffset, width);
						}
						else if (dest2DRow != null)
						{
							dest2DRow.Texture.Bind(true);
							gl.CopyTexSubImage2D(dest2DRow.Target, dest2DRow.Level, destX, dest2DRow.YOffset, sourceX, source2DRow.YOffset, width, 1);
						}
						else if (dest2D != null)
						{
							dest2D.Texture.Bind(true);
							gl.CopyTexSubImage2D(dest2D.Target, dest2D.Level, destX, destY, sourceX, source2DRow.YOffset, width, 1);
						}
						else if (dest3DSlice != null)
						{
							dest3DSlice.Texture.Bind(true);
							gl.CopyTexSubImage3D(dest3DSlice.Target, dest3DSlice.Level, destX, destY, dest3DSlice.ZOffset, sourceX, source2DRow.YOffset, width, 1);
						}
						else
						{
							return false;
						}
					}
					break;

				case TextureImage2D source2D:
					using (WrapInFramebuffer(source2D.Texture.Context, source2D))
					{
						if (dest1D != null)
						{
							dest1D.Texture.Bind(true);
							gl.CopyTexSubImage1D(dest1D.Target, dest1D.Level, destX, sourceX, sourceY, width);
						}
						else if (dest2DRow != null)
						{
							dest2DRow.Texture.Bind(true);
							gl.CopyTexSubImage2D(dest2DRow.Target, dest2DRow.Level, destX, dest2DRow.YOffset, sourceX, sourceY, width, 1);
						}
						else if (dest2D != null)
						{
							dest2D.Texture.Bind(true);
							gl.CopyTexSubImage2D(dest2D.Target, dest2D.Level, destX, destY, sourceX, sourceY, width, height);
						}
						else if (dest3DSlice != null)
						{
							dest3DSlice.Texture.Bind(true);
							gl.CopyTexSubImage3D(dest3DSlice.Target, dest3DSlice.Level, destX, destY, dest3DSlice.ZOffset, sourceX, sourceY, width, height);
						}
						else
						{
							return false;
						}
					}
					break;

				case TextureImage3DSlice source3DSlice:
					using (WrapInFramebuffer(source3DSlice.Texture.Context, source3DSlice))
					{
						if (dest1D != null)
						{
							dest1D.Texture.Bind(true);
							gl.CopyTexSubImage1D(dest1D.Target, dest1D.Level, destX, sourceX, sourceY, width);
						}
						else if (dest2DRow != null)
						{
							dest2DRow.Texture.Bind(true);
							gl.CopyTexSubImage2D(dest2DRow.Target, dest2DRow.Level, destX, dest2DRow.YOffset, sourceX, sourceY, width, 1);
						}
						else if (dest2D != null)
						{
							dest2D.Texture.Bind(true);
							gl.CopyTexSubImage2D(dest2D.Target, dest2D.Level, destX, destY, sourceX, sourceY, width, height);
						}
						else if (dest3DSlice != null)
						{
							dest3DSlice.Texture.Bind(true);
							gl.CopyTexSubImage3D(dest3DSlice.Target, dest3DSlice.Level, destX, destY, dest3DSlice.ZOffset, sourceX, sourceY, width, height);
						}
						else
						{
							return false;
						}
					}
					break;

				default:
					return false;
			}

			return true;
		}

		#endregion
	}
}
