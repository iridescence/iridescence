﻿using System;
using System.Buffers;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an image of a 1D texture.
	/// </summary>
	public class TextureImage1D : TextureImage, IFramebufferAttachment
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureImage1D.
		/// </summary>
		internal TextureImage1D(Texture texture, TextureTarget target, PixelInternalFormat format, int width, int level)
			: base(texture, target, format, width, 1, 1, level)
		{

		}

		#endregion

		#region Methods

		public override void Write(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source.Width, source.Height, source.Depth);

			MemoryImage tempImage = this.GetCompatibleSource(source, out int rowLength, out int imageHeight, out PixelFormat format, out PixelType type);

			this.Texture.Context.Invoke(() =>
			{
				this.Texture.Bind(true);

				this.Context.GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackRowLength, rowLength);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackImageHeight, imageHeight);

				unsafe
				{
					using MemoryHandle handle = tempImage.Memory.Pin();
					this.Context.GL.TexSubImage1D(this.Target, this.Level, x, source.Width, format, type, new IntPtr(handle.Pointer));
				}
			});
		}

		public override Task WriteAsync(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source.Width, source.Height, source.Depth);

			MemoryImage tempImage = this.GetCompatibleSource(source, out int rowLength, out int imageHeight, out PixelFormat format, out PixelType type);

			return this.Texture.Context.InvokeAsync(() =>
			{
				this.Texture.Bind(true);

				this.Context.GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackRowLength, rowLength);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackImageHeight, imageHeight);

				unsafe
				{
					using MemoryHandle handle = tempImage.Memory.Pin();
					this.Context.GL.TexSubImage1D(this.Target, this.Level, x, source.Width, format, type, new IntPtr(handle.Pointer));
				}
			}).AsTask();
		}

		public void AttachToFramebuffer(FramebufferTarget target, FramebufferAttachment attachment)
		{
			this.Context.GL.FramebufferTexture1D(target, attachment, this.Target, this.Texture.ID, this.Level);
		}

		#endregion
	}
}
