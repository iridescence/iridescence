﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a 3D texture.
	/// </summary>
	public class Texture3D : Texture
	{
		#region Fields

		private readonly TextureImage3D[] images;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the width of the texture.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the height of the texture.
		/// </summary>
		public int Height { get; }

		/// <summary>
		/// Gets the depth of the texture.
		/// </summary>
		public int Depth { get; }

		/// <summary>
		/// Gets the number of mip maps in the texture.
		/// </summary>
		public int MipMaps { get; }

		/// <summary>
		/// Gets the texture parameters of this texture.
		/// </summary>
		public TextureParameters Parameters { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture3D.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="internalFormat">The internal texture format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="depth">The texture depth.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture3D(GLContext context, PixelInternalFormat internalFormat, int width, int height, int depth, int mipMaps)
			: base(context, TextureTarget.Texture3D, internalFormat)
		{
			PixelFormat pixelFormat = FormatUtility.GetPixelFormatForInternalFormat(internalFormat);
			PixelType pixelType = FormatUtility.GetDefaultPixelType(pixelFormat);;

			this.Width = width;
			this.Height = height;
			this.Depth = depth;
			this.MipMaps = mipMaps;
			this.Parameters = new TextureParameters(this);

			this.images = new TextureImage3D[mipMaps];
			for (int i = 0; i < mipMaps; ++i)
			{
				this.images[i] = new TextureImage3D(this, TextureTarget.Texture3D, internalFormat, width, height, depth, i);
				if (width > 1) width >>= 1;
				if (height > 1) height >>= 1;
				if (depth > 1) depth >>= 1;
			}

			// reserve memory.
			this.Context.Invoke(() =>
			{
				this.Bind(true);
				this.Parameters.MaxLevel = mipMaps - 1;
				for (int i = 0; i < mipMaps; ++i)
				{
					this.Context.GL.TexImage3D(TextureTarget.Texture3D, i, internalFormat, this.images[i].Width, this.images[i].Height, this.images[i].Depth, 0, pixelFormat, pixelType, IntPtr.Zero);
				}
			});
		}

		/// <summary>
		/// Creates a new Texture3D.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="format">The texture pixel format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="depth">The texture depth.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture3D(GLContext context, Format format, int width, int height, int depth, int mipMaps)
			: this(context, FormatUtility.GetClosestInternalFormat(format), width, height, depth, mipMaps)
		{
			if (!format.Output.IsIdentity)
			{
				this.Parameters.Swizzle = format.Output;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates a Texture3D from a 3D image.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="image"></param>
		/// <param name="withMipMaps"></param>
		/// <returns></returns>
		public static Texture3D FromImage(GLContext context, Image image, bool withMipMaps)
		{
			int numMipMaps = withMipMaps ? GetNumberOfMipMaps(image.Width, image.Height, image.Depth) : 1;

			Texture3D texture = new Texture3D(context, image.Format, image.Width, image.Height, image.Depth, numMipMaps);

			image.CopyTo(texture.GetImage(0));

			return texture;
		}

		/// <summary>
		/// Asynchronously creates a Texture2D from a 2D image.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="image"></param>
		/// <param name="withMipMaps"></param>
		/// <returns></returns>
		public static async Task<Texture3D> FromImageAsync(GLContext context, Image image, bool withMipMaps)
		{
			int numMipMaps = withMipMaps ? GetNumberOfMipMaps(image.Width, image.Height, image.Depth) : 1;

			Texture3D texture = new Texture3D(context, image.Format, image.Width, image.Height, image.Depth, numMipMaps);

			await image.CopyToAsync(texture.GetImage(0));

			return texture;
		}

		/// <summary>
		/// Returns the image for the specified mip index.
		/// </summary>
		/// <param name="mipIndex"></param>
		/// <returns></returns>
		public TextureImage3D GetImage(int mipIndex)
		{
			return this.images[mipIndex];
		}

		/// <summary>
		/// Generates map maps for the texture.
		/// </summary>
		public void GenerateMipMaps()
		{
			this.Bind(true);
			this.Context.GL.GenerateMipmap((GenerateMipmapTarget)this.Target);
		}

		#endregion
	}
}
