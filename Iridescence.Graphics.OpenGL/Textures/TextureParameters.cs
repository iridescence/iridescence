﻿using System;
using System.Threading.Tasks;
using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents the OpenGL texture object parameters.
	/// </summary>
	public sealed class TextureParameters
	{
		#region Fields

		private readonly Texture texture;
		private readonly TextureTarget target;

		private ColorMappingChannel swizzleR;
		private ColorMappingChannel swizzleG;
		private ColorMappingChannel swizzleB;
		private ColorMappingChannel swizzleA;
		private TextureMinFilter minFilter;
		private TextureMagFilter magFilter;
		private float lodBias;
		private float minLod;
		private float maxLod;
		private int baseLevel;
		private int maxLevel;
		private TextureWrapMode wrapX;
		private TextureWrapMode wrapY;
		private TextureWrapMode wrapZ;
		private Color4 borderColor;
		private float maxAnisotropy;

		#endregion

		#region Properties

		public GLContext Context => this.texture.Context;
		
		/// <summary>
		/// Gets or sets the red component swizzle.
		/// </summary>
		public ColorMappingChannel SwizzleR 
		{
			get => this.swizzleR;
			set
			{
				this.swizzleR = value;
				this.applySwizzle();
			}
		}

		/// <summary>
		/// Gets or sets the green component swizzle.
		/// </summary>
		public ColorMappingChannel SwizzleG 
		{
			get => this.swizzleG;
			set
			{
				this.swizzleG = value;
				this.applySwizzle();
			}
		}

		/// <summary>
		/// Gets or sets the blue component swizzle.
		/// </summary>
		public ColorMappingChannel SwizzleB
		{
			get => this.swizzleB;
			set
			{
				this.swizzleB = value;
				this.applySwizzle();
			}
		}
		
		/// <summary>
		/// Gets or sets the alpha component swizzle.
		/// </summary>
		public ColorMappingChannel SwizzleA
		{
			get => this.swizzleA;
			set
			{
				this.swizzleA = value;
				this.applySwizzle();
			}
		}

		/// <summary>
		/// Gets or sets the swizzle parameters.
		/// </summary>
		public ColorMapping Swizzle
		{
			get => new ColorMapping(this.swizzleR, this.swizzleG, this.swizzleB, this.swizzleA);
			set
			{
				this.swizzleR = value.Red;
				this.swizzleG = value.Green;
				this.swizzleB = value.Blue;
				this.swizzleA = value.Alpha;
				this.applySwizzle();
			}
		}
		
		/// <summary>
		/// Gets or sets the texture minification filter.
		/// </summary>
		public TextureMinFilter MinFilter 
		{
			get => this.minFilter;
			set
			{
				this.minFilter = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureMinFilter, (int)value);
				});
			}
		}
		
		/// <summary>
		/// Gets or sets the texture magnification filter.
		/// </summary>
		public TextureMagFilter MagFilter
		{
			get => this.magFilter;
			set
			{
				this.magFilter = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureMagFilter, (int)value);
				});
			}
		}
		
		/// <summary>
		/// Gets or sets the texture level of detail bias.
		/// </summary>
		public float LodBias
		{
			get => this.lodBias;
			set
			{
				this.lodBias = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureLodBias, value);
				});
			}
		}

		/// <summary>
		/// Gets or sets the minimum level of detail.
		/// </summary>
		public float MinLod
		{
			get => this.minLod;
			set
			{
				this.minLod = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureMinLod, value);
				});
			}
		}

		/// <summary>
		/// Gets or sets the maximum level of detail.
		/// </summary>
		public float MaxLod
		{
			get => this.maxLod;
			set
			{
				this.maxLod = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureMaxLod, value);
				});
			}
		}
	
		/// <summary>
		/// Gets or sets the index of the lowest defined mip map level.
		/// </summary>
		public int BaseLevel
		{
			get => this.baseLevel;
			set
			{
				this.baseLevel = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureBaseLevel, value);
				});
			}
		}

		/// <summary>
		/// Gets or sets the index of the highest defined mip map level.
		/// </summary>
		public int MaxLevel
		{
			get => this.maxLevel;
			set
			{
				this.maxLevel = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureMaxLevel, value);
				});
			}
		}

		/// <summary>
		/// Gets the texture wrap mode on the X axis.
		/// </summary>
		public TextureWrapMode WrapX
		{
			get => this.wrapX;
			set
			{
				this.wrapX = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureWrapS, (int)value);
				});
			}
		}

		/// <summary>
		/// Gets the texture wrap mode on the Y axis.
		/// </summary>
		public TextureWrapMode WrapY
		{
			get => this.wrapY;
			set
			{
				this.wrapY = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureWrapT, (int)value);
				});
			}
		}

		/// <summary>
		/// Gets the texture wrap mode on the Z axis.
		/// </summary>
		public TextureWrapMode WrapZ
		{
			get => this.wrapZ;
			set
			{
				this.wrapZ = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureWrapR, (int)value);
				});
			}
		}

		/// <summary>
		/// Gets or sets the texture border color.
		/// </summary>
		public Color4 BorderColor
		{
			get => this.borderColor;
			set
			{
				this.borderColor = value;
				this.texture.Context.Invoke(() =>
				{
					this.texture.Bind(true);
					this.Context.GL.TexParameter(this.target, TextureParameterName.TextureBorderColor, value.ToArray());
				});
			}
		}

		/// <summary>
		/// Gets or sets the maximum level of anisotropy.
		/// </summary>
		public float MaxAnisotropy
		{
			get => this.maxAnisotropy;
			set
			{
				value = Utility.Clamp(value, 0.0f, this.texture.Context.Capabilities.MaxAnisotropy);

				this.maxAnisotropy = value;
				if (this.texture.Context.Capabilities.HasExtension("GL_EXT_texture_filter_anisotropic"))
				{
					this.texture.Context.Invoke(() =>
					{
						this.texture.Bind(true);
						this.Context.GL.TexParameter(this.target, (TextureParameterName)All.TextureMaxAnisotropyExt, value);
					});
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureParameters.
		/// </summary>
		internal TextureParameters(Texture texture)
		{
			this.texture = texture;
			this.target = texture.Target;

			this.swizzleR = ColorMappingChannel.Red;
			this.swizzleG = ColorMappingChannel.Green;
			this.swizzleB = ColorMappingChannel.Blue;
			this.swizzleA = ColorMappingChannel.Alpha;
			this.minFilter = TextureMinFilter.NearestMipmapLinear;
			this.magFilter = TextureMagFilter.Linear;
			this.lodBias = 0.0f;
			this.minLod = -1000.0f;
			this.maxLod = 1000.0f;
			this.wrapX = TextureWrapMode.Repeat;
			this.wrapY = TextureWrapMode.Repeat;
			this.wrapZ = TextureWrapMode.Repeat;
			this.borderColor = Color4.Transparency;
			this.maxAnisotropy = 1.0f;
		}

		#endregion

		#region Methods

		private void copyTo(TextureParameters other)
		{
			other.swizzleR = this.swizzleR;
			other.swizzleG = this.swizzleG;
			other.swizzleB = this.swizzleB;
			other.swizzleA = this.swizzleA;
			other.minFilter = this.minFilter;
			other.magFilter = this.magFilter;
			other.lodBias = this.lodBias;
			other.minLod = this.minLod;
			other.maxLod = this.maxLod;
			other.baseLevel = this.baseLevel;
			other.maxLevel = this.maxLevel;
			other.wrapX = this.wrapX;
			other.wrapY = this.wrapY;
			other.wrapZ = this.wrapZ;
			other.maxAnisotropy = this.maxAnisotropy;
		}

		/// <summary>
		/// Copies all parameters to a different instance.
		/// </summary>
		/// <param name="other"></param>
		public void CopyTo(TextureParameters other)
		{
			this.copyTo(other);
			this.texture.Context.Invoke(other.apply);
		}

		/// <summary>
		/// Copies all parameters to a different instance.
		/// </summary>
		/// <param name="other"></param>
		public ValueTask CopyToAsync(TextureParameters other)
		{
			this.copyTo(other);
			return this.texture.Context.InvokeAsync(other.apply);
		}

		private void apply()
		{
			Span<int> swizzle = stackalloc int[4];
			swizzle[0] = getGLSwizzle(this.swizzleR);
			swizzle[1] = getGLSwizzle(this.swizzleG);
			swizzle[2] = getGLSwizzle(this.swizzleB);
			swizzle[3] = getGLSwizzle(this.swizzleA);

			this.texture.Bind(true);
			var gl = this.Context.GL;
			gl.TexParameter(this.target, TextureParameterName.TextureSwizzleRgba, swizzle);
			gl.TexParameter(this.target, TextureParameterName.TextureMinFilter, (int)this.minFilter);
			gl.TexParameter(this.target, TextureParameterName.TextureMagFilter, (int)this.magFilter);
			gl.TexParameter(this.target, TextureParameterName.TextureLodBias, this.lodBias);
			gl.TexParameter(this.target, TextureParameterName.TextureMinLod, this.minLod);
			gl.TexParameter(this.target, TextureParameterName.TextureMaxLod, this.maxLod);
			gl.TexParameter(this.target, TextureParameterName.TextureBaseLevel, this.baseLevel);
			gl.TexParameter(this.target, TextureParameterName.TextureMaxLevel, this.maxLevel);
			gl.TexParameter(this.target, TextureParameterName.TextureWrapS, (int)this.wrapX);
			gl.TexParameter(this.target, TextureParameterName.TextureWrapT, (int)this.wrapY);
			gl.TexParameter(this.target, TextureParameterName.TextureWrapR, (int)this.wrapZ);

			if (this.texture.Context.Capabilities.HasExtension("GL_EXT_texture_filter_anisotropic"))
				gl.TexParameter(this.target, (TextureParameterName)All.TextureMaxAnisotropyExt, this.maxAnisotropy);
		}

		private static int getGLSwizzle(ColorMappingChannel mapping)
		{
			switch (mapping)
			{
				case ColorMappingChannel.Zero: return (int)All.Zero;
				case ColorMappingChannel.One: return (int)All.One;
				case ColorMappingChannel.Red: return (int)All.Red;
				case ColorMappingChannel.Green: return (int)All.Green;
				case ColorMappingChannel.Blue: return (int)All.Blue;
				case ColorMappingChannel.Alpha: return (int)All.Alpha;
			}

			throw new ArgumentException("Invalid format channel mapping.", nameof(mapping));
		}

		/// <summary>
		/// Sets the channel mappings for all channels.
		/// </summary>
		/// <param name="red"></param>
		/// <param name="green"></param>
		/// <param name="blue"></param>
		/// <param name="alpha"></param>
		public void SetSwizzle(ColorMappingChannel red, ColorMappingChannel green, ColorMappingChannel blue, ColorMappingChannel alpha)
		{
			this.swizzleR = red;
			this.swizzleG = green;
			this.swizzleB = blue;
			this.swizzleA = alpha;
			this.applySwizzle();
		}

		private void applySwizzle()
		{
			int[] swizzle = new int[4];
			swizzle[0] = getGLSwizzle(this.swizzleR);
			swizzle[1] = getGLSwizzle(this.swizzleG);
			swizzle[2] = getGLSwizzle(this.swizzleB);
			swizzle[3] = getGLSwizzle(this.swizzleA);

			this.texture.Context.Invoke(() =>
			{
				this.texture.Bind(true);
				this.Context.GL.TexParameter(this.target, TextureParameterName.TextureSwizzleRgba, swizzle);
			});
		}

		#endregion
	}
}
