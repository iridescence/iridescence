﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a 1D texture.
	/// </summary>
	public class Texture1D : Texture
	{
		#region Fields

		private readonly TextureImage1D[] images;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the width of the texture.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the number of mip maps in the texture.
		/// </summary>
		public int MipMaps { get; }

		/// <summary>
		/// Gets the texture parameters of this texture.
		/// </summary>
		public TextureParameters Parameters { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture1D.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="internalFormat">The internal texture format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture1D(GLContext context, PixelInternalFormat internalFormat, int width, int mipMaps)
			: base(context, TextureTarget.Texture1D, internalFormat)
		{
			PixelFormat pixelFormat = FormatUtility.GetPixelFormatForInternalFormat(internalFormat);
			PixelType pixelType = FormatUtility.GetDefaultPixelType(pixelFormat);;

			this.Width = width;
			this.MipMaps = mipMaps;
			this.Parameters = new TextureParameters(this);

			this.images = new TextureImage1D[mipMaps];
			for (int i = 0; i < mipMaps; ++i)
			{
				this.images[i] = new TextureImage1D(this, TextureTarget.Texture1D, internalFormat, width, i);
				if (width > 1) width >>= 1;
			}

			// reserve memory.
			this.Context.Invoke(() =>
			{
				this.Bind(true);
				this.Parameters.MaxLevel = mipMaps - 1;
				for (int i = 0; i < mipMaps; ++i)
				{
					this.Context.GL.TexImage1D(TextureTarget.Texture1D, i, internalFormat, this.images[i].Width, 0, pixelFormat, pixelType, IntPtr.Zero);
				}
			});
		}

		/// <summary>
		/// Creates a new Texture1D.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="format">The texture pixel format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture1D(GLContext context, Format format, int width, int mipMaps)
			: this(context, FormatUtility.GetClosestInternalFormat(format), width, mipMaps)
		{
			if (!format.Output.IsIdentity)
			{
				this.Parameters.Swizzle = format.Output;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates a Texture1D from a 1D image.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="image"></param>
		/// <param name="withMipMaps"></param>
		/// <returns></returns>
		public static Texture1D FromImage(GLContext context, Image image, bool withMipMaps)
		{
			if(image.Height > 1 || image.Depth > 1)
				throw new ArgumentException("Image is not one-dimensional.", nameof(image));

			int numMipMaps = withMipMaps ? GetNumberOfMipMaps(image.Width, 1, 1) : 1;

			Texture1D texture = new Texture1D(context, image.Format, image.Width, numMipMaps);

			image.CopyTo(texture.GetImage(0));

			return texture;
		}

		/// <summary>
		/// Asynchronously creates a Texture2D from a 1D image.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="image"></param>
		/// <param name="withMipMaps"></param>
		/// <returns></returns>
		public static async Task<Texture1D> FromImageAsync(GLContext context, Image image, bool withMipMaps)
		{
			if(image.Height > 1 || image.Depth > 1)
				throw new ArgumentException("Image is not one-dimensional.", nameof(image));

			int numMipMaps = withMipMaps ? GetNumberOfMipMaps(image.Width, 1, 1) : 1;

			Texture1D texture = new Texture1D(context, image.Format, image.Width, numMipMaps);

			await image.CopyToAsync(texture.GetImage(0));

			return texture;
		}

		/// <summary>
		/// Returns the image for the specified mip index.
		/// </summary>
		/// <param name="mipIndex"></param>
		/// <returns></returns>
		public TextureImage1D GetImage(int mipIndex)
		{
			return this.images[mipIndex];
		}

		/// <summary>
		/// Generates map maps for the texture.
		/// </summary>
		public void GenerateMipMaps()
		{
			this.Bind(true);
			this.Context.GL.GenerateMipmap((GenerateMipmapTarget)this.Target);
		}

		#endregion
	}
}
