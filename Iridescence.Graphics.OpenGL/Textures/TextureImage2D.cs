﻿using System;
using System.Buffers;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an image of a 2D texture.
	/// </summary>
	public class TextureImage2D : TextureImage, IFramebufferAttachment
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureImage2D.
		/// </summary>
		internal TextureImage2D(Texture texture, TextureTarget target, PixelInternalFormat format, int width, int height, int level)
			: base(texture, target, format, width, height, 1, level)
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a row of this 2D image.
		/// </summary>
		/// <param name="y">The Y index of the row to return.</param>
		/// <returns></returns>
		public TextureImage2DRow GetRow(int y)
		{
			if(y < 0 || y >= this.Height)
				throw new ArgumentOutOfRangeException(nameof(y));

			return new TextureImage2DRow(this, y);
		}
	
		public override void Write(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source.Width, source.Height, source.Depth);

			MemoryImage tempImage = this.GetCompatibleSource(source, out int rowLength, out int imageHeight, out PixelFormat format, out PixelType type);

			this.Texture.Context.Invoke(() =>
			{
				this.Texture.Bind(true);

				this.Context.GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackRowLength, rowLength);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackImageHeight, imageHeight);

				unsafe
				{
					using MemoryHandle handle = tempImage.Memory.Pin();
					this.Context.GL.TexSubImage2D(this.Target, this.Level, x, y, source.Width, source.Height, format, type, new IntPtr(handle.Pointer));
				}
			});
		}

		public override Task WriteAsync(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source.Width, source.Height, source.Depth);

			MemoryImage tempImage = this.GetCompatibleSource(source, out int rowLength, out int imageHeight, out PixelFormat format, out PixelType type);

			return this.Texture.Context.InvokeAsync(() =>
			{
				this.Texture.Bind(true);

				this.Context.GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackRowLength, rowLength);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackImageHeight, imageHeight);

				unsafe
				{
					using MemoryHandle handle = tempImage.Memory.Pin();
					this.Context.GL.TexSubImage2D(this.Target, this.Level, x, y, source.Width, source.Height, format, type, new IntPtr(handle.Pointer));
				}
			}).AsTask();
		}

		public void AttachToFramebuffer(FramebufferTarget target, FramebufferAttachment attachment)
		{
			this.Context.GL.FramebufferTexture2D(target, attachment, this.Target, this.Texture.ID, this.Level);
		}

		#endregion
	}
}
