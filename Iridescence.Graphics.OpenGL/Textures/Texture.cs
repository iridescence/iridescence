﻿using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL texture.
	/// </summary>
	public abstract class Texture : GLContextChild, IGLObject
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the texture object ID.
		/// </summary>
		public uint ID { get; }

		/// <summary>
		/// Gets the target this texture binds to.
		/// </summary>
		public TextureTarget Target { get; }

		/// <summary>
		/// Gets the internal pixel format of the texture.
		/// </summary>
		public PixelInternalFormat InternalFormat { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture.
		/// </summary>
		protected Texture(GLContext context, TextureTarget target, PixelInternalFormat internalFormat)
			: base(context)
		{
			// create texture.
			this.ID = context.Invoke(this.Context.GL.GenTexture);
			this.Target = target;
			this.InternalFormat = internalFormat;
			GLTrace.Instance.ObjectCreated(nameof(Texture), this.ID);
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			return $"{this.ID}";
		}

		/// <summary>
		/// Returns the number of possible mip maps for the specified sizes.
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="depth"></param>
		/// <returns></returns>
		public static int GetNumberOfMipMaps(int width, int height, int depth)
		{
			int numMipMaps = 1;
			while (width > 1 || height > 1 || depth > 1)
			{
				width = Utility.Max(width >> 1, 1);
				height = Utility.Max(height >> 1, 1);
				depth = Utility.Max(depth >> 1, 1);
				++numMipMaps;
			}
			return numMipMaps;
		}

		/// <summary>
		/// Binds the texture.
		/// </summary>
		/// <param name="modifyState">Whether to modify the current state of the context.</param>
		public void Bind(bool modifyState = true)
		{
			if (modifyState)
			{
				State currentState = this.Context.CurrentState;
				if (currentState != null)
				{
					currentState.TextureUnits[currentState.ActiveTexture].Target = this.Target;
					currentState.TextureUnits[currentState.ActiveTexture].Texture = this.ID;
				}
			}
				
			this.Context.GL.BindTexture(this.Target, this.ID);
		}

		/// <summary>
		/// Unbinds the texture.
		/// </summary>
		/// <param name="modifyState">Whether to modify the current state of the context.</param>
		public void Unbind(bool modifyState = true)
		{
			if (modifyState)
			{
				State currentState = this.Context.CurrentState;
				if (currentState != null)
				{
					currentState.TextureUnits[currentState.ActiveTexture].Target = this.Target;
					currentState.TextureUnits[currentState.ActiveTexture].Texture = 0;
				}
			}
			
			this.Context.GL.BindTexture(this.Target, 0);
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			void delete()
			{
				this.Context.GL.DeleteTexture(this.ID);
				GLTrace.Instance.ObjectDeleted(nameof(Texture), this.ID);
			}

			if (managed)
				this.Context.Invoke(delete);
			else
				this.Context.Post(delete);
		}
		
		#endregion
	}
}
