﻿using System;
using System.Buffers;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represnts an OpenGL image.
	/// </summary>
	public abstract class TextureImage : Image
	{
		#region Properties

		/// <summary>
		/// Gets the texture this image belongs to.
		/// </summary>
		public Texture Texture { get; }

		/// <summary>
		/// Gets the context that owns this texture image.
		/// </summary>
		public GLContext Context => this.Texture.Context;

		internal TextureTarget Target { get; }
		internal PixelInternalFormat InternalFormat { get; }
		internal int Level { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureImage.
		/// </summary>
		protected TextureImage(Texture texture, TextureTarget target, PixelInternalFormat internalFormat, int width, int height, int depth, int level)
			: base(FormatUtility.GetClosestFormat(internalFormat), width, height, depth)
		{
			this.Texture = texture;
			this.Target = target;
			this.InternalFormat = internalFormat;
			this.Level = level;
		}

		#endregion

		#region Methods

		private static bool tryGetPack(MemoryImage box, out int rowLength, out int imageHeight)
		{
			imageHeight = rowLength = 0;

			if (!box.XDimension.IsConsecutive)
				return false;

			if (box.YDimension.Stride % box.XDimension.ElementSize != 0)
				return false;

			if (box.ZDimension.Stride % box.YDimension.ElementSize != 0)
				return false;

			rowLength = box.YDimension.Stride / box.XDimension.ElementSize;
			imageHeight = box.ZDimension.Stride / box.YDimension.ElementSize;

			return true;
		}

		internal MemoryImage GetCompatibleDestination(MemoryImage pixelBox, bool allowFbo, out int rowLength, out int imageHeight)
		{
			// Pixels are stored without swizzling (that's done on the fly when reading the texture in a shader).
			Format actualFormat = new Format(this.Format.Type, this.Format.Semantic);

			// If the destination has the exact same dimensions and format as this texture, we can copy to it directly.
			// It also needs to be pixel-aligned in order to use row count and row length.
			MemoryImage result;
			if (pixelBox.Format == actualFormat &&
			    pixelBox.Width == this.Width &&
			    pixelBox.Height == this.Height &&
			    pixelBox.Depth == this.Depth &&
			    tryGetPack(pixelBox, out rowLength, out imageHeight))
			{
				result = pixelBox;
			}
			else
			{
				if (allowFbo && pixelBox.Format.Input.IsIdentity &&
				    (this is IFramebufferAttachment || this is IFramebufferLayerAttachment) && 
				    tryGetPack(pixelBox, out rowLength, out imageHeight))
				{
					result = null;
				}
				else
				{
					Trace.WriteWarning($"Allocation of temporary image is required ({this.Width}x{this.Height}x{this.Depth}).");
					result = MemoryImage.Allocate(actualFormat, this.Width, this.Height, this.Depth);
					rowLength = this.Width;
					imageHeight = this.Height;
				}
			}

			return result;
		}

		internal MemoryImage GetCompatibleSource(MemoryImage pixelBox, out int rowLength, out int imageHeight, out PixelFormat format, out PixelType type)
		{
			format = FormatUtility.GetPixelFormat(pixelBox.Format);
			type = FormatUtility.GetPixelType(pixelBox.Format);

			MemoryImage result;
			if (tryGetPack(pixelBox, out rowLength, out imageHeight))
			{
				result = pixelBox;
			}
			else
			{
				Trace.WriteWarning($"Allocation of temporary image is required ({pixelBox.Width}x{pixelBox.Height}x{pixelBox.Depth}).");
				Format f = FormatUtility.GetFormat(format, type, true);
				result = MemoryImage.Allocate(f, pixelBox.Width, pixelBox.Height, pixelBox.Depth);
			}

			return result;
		}

		private void readViaFramebuffer(MemoryImage dest, int x, int y, int z, int rowLength, int rowCount)
		{
			Framebuffer fbo;

			if (this is IFramebufferAttachment attachment)
			{
				fbo = TextureImageUtil.WrapInFramebuffer(this.Context, attachment);
			}
			else if (this is IFramebufferLayerAttachment layerAttachment)
			{
				fbo = TextureImageUtil.WrapInFramebuffer(this.Context, layerAttachment, z);
			}
			else
			{
				throw new InvalidOperationException();
			}

			using (fbo)
			{
				this.Context.GL.BindBuffer(BufferTarget.PixelPackBuffer, 0);
				this.Context.GL.PixelStore(PixelStoreParameter.PackAlignment, 1);
				this.Context.GL.PixelStore(PixelStoreParameter.PackRowLength, rowLength);
				this.Context.GL.PixelStore(PixelStoreParameter.PackImageHeight, rowCount);

				unsafe
				{
					using MemoryHandle handle = dest.Memory.Pin();
					if (this.Context.Capabilities.HasExtension("GL_ARB_robustness"))
					{
						this.Context.GL.ReadnPixels(x, y, dest.Width, dest.Height, FormatUtility.GetPixelFormat(dest.Format), FormatUtility.GetPixelType(dest.Format), dest.Memory.Length, new IntPtr(handle.Pointer));
					}
					else
					{
						this.Context.GL.ReadPixels(x, y, dest.Width, dest.Height, FormatUtility.GetPixelFormat(dest.Format), FormatUtility.GetPixelType(dest.Format), new IntPtr(handle.Pointer));
					}
				}
			}
		}

		private void read(MemoryImage dest, int rowLength, int rowCount)
		{
			this.Texture.Bind(true);
			this.Context.GL.PixelStore(PixelStoreParameter.PackAlignment, 1);
			this.Context.GL.PixelStore(PixelStoreParameter.PackRowLength, rowLength);
			this.Context.GL.PixelStore(PixelStoreParameter.PackImageHeight, rowCount);

			unsafe
			{
				using MemoryHandle handle = dest.Memory.Pin();
				this.Context.GL.GetTexImage(this.Target, this.Level, FormatUtility.GetPixelFormat(dest.Format), FormatUtility.GetPixelType(dest.Format), new IntPtr(handle.Pointer));
			}
		}

		public override void Read(MemoryImage dest, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, dest.Width, dest.Height, dest.Depth);

			MemoryImage tempImage = this.GetCompatibleDestination(dest, dest.Depth == 1, out int rowLength, out int rowCount);
			
			if (tempImage == null)
			{
				this.Texture.Context.Invoke(() => this.readViaFramebuffer(dest, x, y, z, rowLength, rowCount));
			}
			else
			{
				this.Texture.Context.Invoke(() => this.read(tempImage, rowLength, rowCount));

				// if we created a temporary image, copy it now.
				if (tempImage != dest)
					tempImage.CopyTo(dest, x, y, z, 0, 0, 0, dest.Width, dest.Height, dest.Depth);
			}
		}

		public override async Task ReadAsync(MemoryImage dest, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, dest.Width, dest.Height, dest.Depth);

			MemoryImage tempImage = this.GetCompatibleDestination(dest, false, out int rowLength, out int rowCount);

			if (tempImage == null)
			{
				await this.Texture.Context.InvokeAsync(() => this.readViaFramebuffer(dest, x, y, z, rowLength, rowCount));
			}
			else
			{
				await this.Texture.Context.InvokeAsync(() => this.read(tempImage, rowLength, rowCount));

				// if we created a temporary image, copy it now.
				if (tempImage != dest)
					await tempImage.CopyToAsync(dest, x, y, z, 0, 0, 0, dest.Width, dest.Height, dest.Depth);
			}
		}

		public override void CopyTo(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			if (!this.Texture.Context.Invoke(() => TextureImageUtil.TryCopy(this, dest, sourceX, sourceY, sourceZ, destX, destY, destZ, width, height, depth)))
			{
				Trace.WriteVerbose($"Can not copy from {this.GetType()} to {dest.GetType()} on HW!");
				base.CopyTo(dest, sourceX, sourceY, sourceZ, destX, destY, destZ, width, height, depth);
			}
		}

		public override async Task CopyToAsync(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			if (!await this.Texture.Context.InvokeAsync(() => TextureImageUtil.TryCopy(this, dest, sourceX, sourceY, sourceZ, destX, destY, destZ, width, height, depth)))
			{
				Trace.WriteVerbose($"Can not copy from {this.GetType()} to {dest.GetType()} on HW!");
				base.CopyTo(dest, sourceX, sourceY, sourceZ, destX, destY, destZ, width, height, depth);
			}
		}
	
		/// <summary>
		/// Clears the texture image.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="format">The pixel format of the data.</param>
		/// <param name="data">The data to clear the image with.</param>
		/// <param name="x">The X offset of the region to clear.</param>
		/// <param name="y">The Y offset of the region to clear.</param>
		/// <param name="z">The Z offset of the region to clear.</param>
		/// <param name="width">The width of the region to clear.</param>
		/// <param name="height">The height of the region to clear.</param>
		/// <param name="depth">The depth of the region to clear.</param>
		public unsafe void Clear(Format format, void* data, int x, int y, int z, int width, int height, int depth)
		{
			if (this.Context.Capabilities.HasExtension("GL_ARB_clear_texture"))
			{
				this.Context.Invoke(() =>
				{
					this.Context.GL.ClearTexSubImage(this.Texture.ID, this.Level, x, y, z, width, height, depth, FormatUtility.GetPixelFormat(format), FormatUtility.GetPixelType(format), new IntPtr(data));
				});
			}
			else
			{
				MemoryImage clearBox = MemoryImage.Allocate(format, width, height, depth);
				clearBox.PixelBox.FillRaw(new Span<byte>(data, clearBox.PixelBox.XDimension.ElementSize));
				this.Write(clearBox, x, y, z);
			}
		}

		/// <summary>
		/// Clears the texture image.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="format">The pixel format of the data.</param>
		/// <param name="data">The data to clear the image with.</param>
		/// <param name="x">The X offset of the region to clear.</param>
		/// <param name="y">The Y offset of the region to clear.</param>
		/// <param name="z">The Z offset of the region to clear.</param>
		/// <param name="width">The width of the region to clear.</param>
		/// <param name="height">The height of the region to clear.</param>
		/// <param name="depth">The depth of the region to clear.</param>
		public unsafe void Clear<T>(Format format, ReadOnlySpan<T> data, int x, int y, int z, int width, int height, int depth)
			where T : unmanaged
		{
			fixed (T* p = &data[0])
			{
				this.Clear(format, p, x, y, z, width, height, depth);
			}
		}

		public override string ToString()
		{
			return $"{this.InternalFormat}, {this.Width}x{this.Height}x{this.Depth}";
		}

		#endregion
	}
}
