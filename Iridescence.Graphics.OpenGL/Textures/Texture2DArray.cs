﻿using System;
using System.Threading.Tasks;
using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a 2D array texture.
	/// </summary>
	public class Texture2DArray : Texture
	{
		#region Fields

		private readonly TextureImage3D[] images;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the width of the texture.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the height of the texture.
		/// </summary>
		public int Height { get; }

		/// <summary>
		/// Gets the number of textures in the array.
		/// </summary>
		public int ArrayCount { get; }

		/// <summary>
		/// Gets the number of mip maps in the texture.
		/// </summary>
		public int MipMaps { get; }

		/// <summary>
		/// Gets the texture parameters of this texture.
		/// </summary>
		public TextureParameters Parameters { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Texture2DArray.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="internalFormat">The internal texture format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="arrayCount">The number of textures in the array.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture2DArray(GLContext context, PixelInternalFormat internalFormat, int width, int height, int arrayCount, int mipMaps)
			: base(context, TextureTarget.Texture2DArray, internalFormat)
		{
			PixelFormat pixelFormat = FormatUtility.GetPixelFormatForInternalFormat(internalFormat);
			PixelType pixelType = FormatUtility.GetDefaultPixelType(pixelFormat);;

			this.Width = width;
			this.Height = height;
			this.ArrayCount = arrayCount;
			this.MipMaps = mipMaps;
			this.Parameters = new TextureParameters(this);

			this.images = new TextureImage3D[mipMaps];
			for (int i = 0; i < mipMaps; ++i)
			{
				this.images[i] = new TextureImage3D(this, TextureTarget.Texture2DArray, internalFormat, width, height, arrayCount, i);
				if (width > 1) width >>= 1;
				if (height > 1) height >>= 1;
			}

			// reserve memory.
			this.Context.Invoke(() =>
			{
				this.Bind(true);
				this.Parameters.MaxLevel = mipMaps - 1;
				for (int i = 0; i < mipMaps; ++i)
				{
					this.Context.GL.TexImage3D(TextureTarget.Texture2DArray, i, internalFormat, this.images[i].Width, this.images[i].Height, this.images[i].Depth, 0, pixelFormat, pixelType, IntPtr.Zero);
				}
			});
		}

		/// <summary>
		/// Creates a new Texture2DArray.
		/// </summary>
		/// <param name="context">The OpenGL context to create the texture on.</param>
		/// <param name="format">The texture pixel format.</param>
		/// <param name="width">The texture width.</param>
		/// <param name="height">The texture height.</param>
		/// <param name="arrayCount">The number of textures in the array.</param>
		/// <param name="mipMaps">The number of mip maps.</param>
		public Texture2DArray(GLContext context, Format format, int width, int height, int arrayCount, int mipMaps)
			: this(context, FormatUtility.GetClosestInternalFormat(format), width, height, arrayCount, mipMaps)
		{
			if (!format.Output.IsIdentity)
			{
				this.Parameters.Swizzle = format.Output;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the image for the specified array and mip index.
		/// </summary>
		/// <param name="arrayIndex"></param>
		/// <param name="mipIndex"></param>
		/// <returns></returns>
		public TextureImage3DSlice GetImage(int arrayIndex, int mipIndex)
		{
			return this.images[mipIndex].GetSlice(arrayIndex);
		}

		/// <summary>
		/// Returns the image representing the whole array at the specified mip index.
		/// </summary>
		/// <param name="mipIndex"></param>
		/// <returns></returns>
		public TextureImage3D GetArray(int mipIndex)
		{
			return this.images[mipIndex];
		}

		/// <summary>
		/// Uses the GL_ARB_copy_image extension to copy from this array to another array. Assumes both arrays have the same size and format.
		/// </summary>
		/// <param name="dest"></param>
		private void copyImage(Texture2DArray dest)
		{
			int w = this.Width;
			int h = this.Height;
			int d = this.ArrayCount;
			for (int mip = 0; mip < this.MipMaps; ++mip)
			{
				this.Context.GL.CopyImageSubData(this.ID, ImageTarget.Texture2DArray, mip, 0, 0, 0, dest.ID, ImageTarget.Texture2DArray, mip, 0, 0, 0, w, h, d);
				w = Utility.Max(1, w >> 1);
				h = Utility.Max(1, h >> 1);
			}
		}
		
		/// <summary>
		/// Creates a new <see cref="Texture2DArray"/> with the specified number of textures and copies the textures from this array to the new array.
		/// </summary>
		/// <param name="newCount">The new array count.</param>
		/// <returns></returns>
		public Texture2DArray Resize(int newCount)
		{
			Texture2DArray newArray = new Texture2DArray(this.Context, this.InternalFormat, this.Width, this.Height, newCount, this.MipMaps);

			int shared = Utility.Min(newCount, this.ArrayCount);
			bool copyImageExt = this.Context.Capabilities.HasExtension("GL_ARB_copy_image");

			if (copyImageExt)
			{
				this.Context.Invoke(() => this.copyImage(newArray));
			}
			else
			{
				for (int mip = 0; mip < this.MipMaps; ++mip)
				{
					for (int i = 0; i < shared; ++i)
					{
						TextureImage3DSlice src = this.GetImage(i, mip);
						TextureImage3DSlice dst = newArray.GetImage(i, mip);
						src.CopyTo(dst, 0, 0, 0, 0, 0, 0, src.Width, src.Height, 1);
					}
				}
			}

			this.Parameters.CopyTo(newArray.Parameters);

			return newArray;
		}

		/// <summary>
		/// Creates a new <see cref="Texture2DArray"/> with the specified number of textures and copies the textures from this array to the new array.
		/// </summary>
		/// <param name="newCount">The new array count.</param>
		/// <returns></returns>
		public async ValueTask<Texture2DArray> ResizeAsync(int newCount)
		{
			Texture2DArray newArray = new Texture2DArray(this.Context, this.InternalFormat, this.Width, this.Height, newCount, this.MipMaps);

			int shared = Utility.Min(newCount, this.ArrayCount);
			bool copyImageExt = this.Context.Capabilities.HasExtension("GL_ARB_copy_image");

			if (copyImageExt)
			{
				await this.Context.InvokeAsync(() => this.copyImage(newArray));
			}
			else
			{
				for (int mip = 0; mip < this.MipMaps; ++mip)
				{
					for (int i = 0; i < shared; ++i)
					{
						TextureImage3DSlice src = this.GetImage(i, mip);
						TextureImage3DSlice dst = newArray.GetImage(i, mip);
						await src.CopyToAsync(dst, 0, 0, 0, 0, 0, 0, src.Width, src.Height, 1);
					}
				}
			}

			await this.Parameters.CopyToAsync(newArray.Parameters);

			return newArray;
		}

		#endregion
	}
}
