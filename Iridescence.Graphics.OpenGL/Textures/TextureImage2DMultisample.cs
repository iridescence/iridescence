﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an image of a 2D multisample texture.
	/// </summary>
	public class TextureImage2DMultisample : TextureImage, IFramebufferAttachment
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the number of samples in the texture image.
		/// </summary>
		public int Samples {get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureImage2DMultisample.
		/// </summary>
		internal TextureImage2DMultisample(Texture2DMultisample texture, PixelInternalFormat internalFormat, int width, int height, int samples)
			: base(texture, TextureTarget.Texture2DMultisample, internalFormat, width, height, 1, 0)
		{
			this.Samples = samples;
		}

		#endregion

		#region Methods

		public override void Read(MemoryImage dest, int sourceX, int sourceY, int sourceZ)
		{
			throw new NotSupportedException("Reading from multisample textures is not supported.");
		}

		public override Task ReadAsync(MemoryImage dest, int x, int y, int z)
		{
			throw new NotSupportedException("Reading from multisample textures is not supported.");
		}

		public override void Write(MemoryImage source, int destX, int destY, int destZ)
		{
			throw new NotSupportedException("Writing to multisample textures is not supported.");
		}

		public override Task WriteAsync(MemoryImage source, int destX, int destY, int destZ)
		{
			throw new NotSupportedException("Writing to multisample textures is not supported.");
		}

		public override void CopyTo(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			throw new NotSupportedException("Copying multisample textures is not supported.");
		}

		public override Task CopyToAsync(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			throw new NotSupportedException("Copying multisample textures is not supported.");
		}

		public void AttachToFramebuffer(FramebufferTarget target, FramebufferAttachment attachment)
		{
			this.Context.GL.FramebufferTexture2D(target, attachment, this.Target, this.Texture.ID, this.Level);
		}

		#endregion
	}
}
