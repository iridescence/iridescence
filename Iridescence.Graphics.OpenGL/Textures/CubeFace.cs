﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Defines the cube map faces.
	/// </summary>
	public enum CubeFace
	{
		PositiveX,
		NegativeX,
		
		PositiveY,
		NegativeY,

		PositiveZ,
		NegativeZ,
	}
}
