﻿using System;
using System.Buffers;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an image of a 3D texture.
	/// </summary>
	public class TextureImage3D : TextureImage, IFramebufferLayerAttachment
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureImage3D.
		/// </summary>
		internal TextureImage3D(Texture texture, TextureTarget target, PixelInternalFormat format, int width, int height, int depth, int level)
			: base(texture, target, format, width, height, depth, level)
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a slice of this 3D image.
		/// </summary>
		/// <param name="z">The Z index of the slice to return.</param>
		/// <returns></returns>
		public TextureImage3DSlice GetSlice(int z)
		{
			if(z < 0 || z >= this.Depth)
				throw new ArgumentOutOfRangeException(nameof(z));

			return new TextureImage3DSlice(this, z);
		}

		public override void Write(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source.Width, source.Height, source.Depth);

			MemoryImage tempImage = this.GetCompatibleSource(source, out int rowLength, out int imageHeight, out PixelFormat format, out PixelType type);

			this.Texture.Context.Invoke(() =>
			{
				this.Texture.Bind(true);

				this.Context.GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackRowLength, rowLength);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackImageHeight, imageHeight);

				unsafe
				{
					using MemoryHandle handle = tempImage.Memory.Pin();
					this.Context.GL.TexSubImage3D(this.Target, this.Level, x, y, z, source.Width, source.Height, source.Depth, format, type, new IntPtr(handle.Pointer));
				}
			});
		}

		public override Task WriteAsync(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source.Width, source.Height, source.Depth);

			MemoryImage tempImage = this.GetCompatibleSource(source, out int rowLength, out int imageHeight, out PixelFormat format, out PixelType type);

			return this.Texture.Context.InvokeAsync(() =>
			{
				this.Texture.Bind(true);

				this.Context.GL.PixelStore(PixelStoreParameter.UnpackAlignment, 1);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackRowLength, rowLength);
				this.Context.GL.PixelStore(PixelStoreParameter.UnpackImageHeight, imageHeight);

				unsafe
				{
					using MemoryHandle handle = tempImage.Memory.Pin();
					this.Context.GL.TexSubImage3D(this.Target, this.Level, x, y, z, source.Width, source.Height, source.Depth, format, type, new IntPtr(handle.Pointer));
				}
			}).AsTask();
		}

		public void AttachToFramebuffer(FramebufferTarget target, FramebufferAttachment attachment, int layer)
		{
			this.Context.GL.FramebufferTextureLayer(target, attachment, this.Texture.ID, this.Level, layer);
		}
		
		#endregion
	}
}
