﻿using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a row of a 2D texture image.
	/// </summary>
	public class TextureImage2DRow : TextureImage
	{
		#region Fields

		#endregion

		#region Properties

		internal TextureImage2D Image { get; }

		public int YOffset { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TextureImage2DRow.
		/// </summary>
		internal TextureImage2DRow(TextureImage2D image, int yOffset)
			: base(image.Texture, image.Target, image.InternalFormat, image.Width, 1, 1, image.Level)
		{
			this.Image = image;
			this.YOffset = yOffset;
		}

		#endregion

		#region Methods

		public override void Read(MemoryImage dest, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, dest);
			this.Image.Read(dest, x, this.YOffset, 0);
		}

		public override Task ReadAsync(MemoryImage dest, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, dest);
			return this.Image.ReadAsync(dest, x, this.YOffset, 0);
		}

		public override void Write(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source);
			this.Image.Write(source, x, this.YOffset, 0);
		}

		public override Task WriteAsync(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source);
			return this.Image.WriteAsync(source, x, this.YOffset, 0);
		}
		
		#endregion
	}
}
