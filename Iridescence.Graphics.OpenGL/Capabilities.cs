﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// OpenGL implementation specific capabilities.
	/// </summary>
	[Serializable]
	public sealed class Capabilities
	{
		#region Fields

		private readonly HashSet<string> extensions;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the extensions supported by the OpenGL implementation.
		/// </summary>
		public IEnumerable<string> Extensions => this.extensions;

		/// <summary>
		/// Gets the maximum supported level of anisotropy.
		/// </summary>
		public float MaxAnisotropy { get; }

		/// <summary>
		/// Gets the OpenGL version (e.g. 330 for OpenGL 3.3).
		/// </summary>
		public int GLVersion { get; }

		/// <summary>
		/// Gets the GLSL version (e.g. 330 for GLSL 330).
		/// </summary>
		public int GLSLVersion { get; }

		/// <summary>
		/// Gets the name of the renderer.
		/// </summary>
		public string Renderer { get; }

		/// <summary>
		/// Gets the name of the vendor.
		/// </summary>
		public string Vendor { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Capabilities.
		/// </summary>
		internal Capabilities(GL gl)
		{
			// Extract OpenGL version in XXX format.
			string glVersionStr = gl.GetString(StringName.Version);
			Match versionMatch = Regex.Match(glVersionStr, @"^(\d+)\.(\d)");
			if (versionMatch.Success)
			{
				this.GLVersion = int.Parse(versionMatch.Groups[1].Value, CultureInfo.InvariantCulture) * 100 + int.Parse(versionMatch.Groups[2].Value, CultureInfo.InvariantCulture) * 10;
			}
			else
			{
				Trace.WriteWarning($"Failed to retrieve OpenGL version due to a malformed version string \"{glVersionStr}\".");
				this.GLVersion = 100;
			}

			// Extract GLSL version in XXX format.
			string glslVersionStr = gl.GetString(StringName.ShadingLanguageVersion);
			versionMatch = Regex.Match(glslVersionStr, @"^(\d+)\.(\d)(\d)?");
			if (versionMatch.Success)
			{
				this.GLSLVersion = int.Parse(versionMatch.Groups[1].Value, CultureInfo.InvariantCulture) * 100 + int.Parse(versionMatch.Groups[2].Value, CultureInfo.InvariantCulture) * 10;
				if (versionMatch.Groups[3].Success) this.GLSLVersion += int.Parse(versionMatch.Groups[3].Value, CultureInfo.InvariantCulture);
			}
			else
			{
				if (this.GLVersion < 200) this.GLSLVersion = 100;
				else if (this.GLVersion >= 200) this.GLSLVersion = 110;
				else if (this.GLVersion >= 210) this.GLSLVersion = 120;
				else if (this.GLVersion >= 300) this.GLSLVersion = 130;
				else if (this.GLVersion >= 310) this.GLSLVersion = 140;
				else if (this.GLVersion >= 320) this.GLSLVersion = 150;
				else if (this.GLVersion >= 330) this.GLSLVersion = this.GLVersion;
				Trace.WriteWarning($"Failed to retrieve GLSL version due to a malformed version string \"{glslVersionStr}\". Assuming GLSL version {this.GLSLVersion}.");
			}

			// Some (probably) helpful info.
			this.Renderer = gl.GetString(StringName.Renderer);
			this.Vendor = gl.GetString(StringName.Vendor);

			Trace.WriteInformation($"OpenGL Version: {this.GLVersion}.");
			Trace.WriteInformation($"GLSL Version: {this.GLSLVersion}.");
			Trace.WriteInformation($"Renderer: {this.Renderer}.");
			Trace.WriteInformation($"Vendor: {this.Vendor}.");

			// Get extensions.
			int numExtensions = gl.GetInteger(GetPName.NumExtensions);

			this.extensions = new HashSet<string>();
			for (uint i = 0; i < numExtensions; i++)
			{
				this.extensions.Add(gl.GetString(StringNameIndexed.Extensions, i));
			}

			if (this.HasExtension("GL_EXT_texture_filter_anisotropic"))
			{
				float temp = gl.GetFloat((GetPName)ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt);
				this.MaxAnisotropy = temp;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a value that indicates whether the specified extension is supported.
		/// </summary>
		/// <param name="ext">The name of the extension.</param>
		/// <returns></returns>
		public bool HasExtension(string ext)
		{
			return this.extensions.Contains(ext);
		}
	
		#endregion
	}
}
