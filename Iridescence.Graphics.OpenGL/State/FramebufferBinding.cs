﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Defines the framebuffer binding targets.
	/// </summary>
	[Flags]
	public enum FramebufferBinding
	{
		Draw = 1,
		Read = 2,
		Both = Draw | Read
	}
}
