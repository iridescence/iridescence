﻿using System.Collections.Generic;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a collection of texture unit bindings.
	/// </summary>
	public class TextureCollection : Dictionary<uint, Texture>
	{
		#region Constructors

		/// <summary>
		/// Creates a new TextureCollection.
		/// </summary>
		public TextureCollection()
		{

		}

		/// <summary>
		/// Creates a new TextureCollection.
		/// </summary>
		public TextureCollection(TextureCollection other)
			: this()
		{
			other.CopyTo(this, true);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Copies the bindings of this collection to the other collection.
		/// </summary>
		/// <param name="other">The <see cref="TextureCollection"/> to copy to.</param>
		/// <param name="replace">True, if all the entries in the other collection should be replaced, false to only change the ones also defined in this collection.</param>
		public void CopyTo(TextureCollection other, bool replace)
		{
			if (replace)
			{
				other.Clear();
				foreach (KeyValuePair<uint, Texture> binding in this)
				{
					other.Add(binding.Key, binding.Value);
				}
			}
			else
			{
				foreach (KeyValuePair<uint, Texture> binding in this)
				{
					other[binding.Key] = binding.Value;
				}
			}
		}

		#endregion
	}
}
