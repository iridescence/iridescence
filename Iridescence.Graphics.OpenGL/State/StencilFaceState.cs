﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents the state of a stencil face.
	/// </summary>
	[Serializable]
	public struct StencilFaceState
	{
		#region Fields

		public static readonly StencilFaceState Default = new StencilFaceState
		{
			Function = StencilFunction.Always,
			Mask = ~0u,
			Reference = 0,
			Fail = StencilOp.Keep,
			DepthFail = StencilOp.Keep,
			DepthPass = StencilOp.Keep,
		};

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the stencil function.
		/// </summary>
		public StencilFunction Function { get; set; }

		/// <summary>
		/// Gets or sets the mask.
		/// </summary>
		public uint Mask { get; set; }

		/// <summary>
		/// Gets or sets the reference value.
		/// </summary>
		public int Reference { get; set; }

		/// <summary>
		/// Gets or sets the fail action.
		/// </summary>
		public StencilOp Fail { get; set; }

		/// <summary>
		/// Gets or sets the depth buffer fail action.
		/// </summary>
		public StencilOp DepthFail {get; set; }

		/// <summary>
		/// Gets or sets the depth buffer pass action.
		/// </summary>
		public StencilOp DepthPass { get; set; }

		#endregion

		#region Constructors

		#endregion

		#region Methods

		/// <summary>
		/// Determines whether the specified stencil faces are equal.
		/// </summary>
		/// <param name="a">The first face.</param>
		/// <param name="b">The second face.</param>
		/// <returns>True if the specified stencil face is equal to the current stencil face; otherwise, false.</returns>
		public static bool Equals(in StencilFaceState a, in StencilFaceState b)
		{
			return a.Function == b.Function &&
			       a.Mask == b.Mask &&
			       a.Reference == b.Reference &&
			       a.Fail == b.Fail &&
			       a.DepthFail == b.DepthFail &&
			       a.DepthPass == b.DepthPass;
		}
		
		/// <summary>
		/// Determines whether the specified stencil face's "StencilOp" values are equal.
		/// </summary>
		/// <param name="a">The first face.</param>
		/// <param name="b">The second face.</param>
		/// <returns>True if the specified stencil face is equal to the current stencil face; otherwise, false.</returns>
		public static bool OpEquals(in StencilFaceState a, in StencilFaceState b)
		{
			return a.Fail == b.Fail &&
			       a.DepthFail == b.DepthFail &&
			       a.DepthPass == b.DepthPass;
		}

		/// <summary>
		/// Determines whether the specified stencil face's "StencilFunc" values are equal.
		/// </summary>
		/// <param name="a">The first face.</param>
		/// <param name="b">The second face.</param>
		/// <returns>True if the specified stencil face is equal to the current stencil face; otherwise, false.</returns>
		public static bool FuncEquals(in StencilFaceState a, in StencilFaceState b)
		{
			return a.Function == b.Function &&
			       a.Mask == b.Mask &&
			       a.Reference == b.Reference;
		}
		
		/// <summary>
		/// Determines whether the specified stencil face is equal to the current stencil face.
		/// </summary>
		/// <param name="other">The stencil face to compare with the current stencil face.</param>
		/// <returns>True if the specified stencil face is equal to the current stencil face; otherwise, false.</returns>
		public bool Equals(StencilFaceState other)
		{
			return this.Function == other.Function &&
				   this.Mask == other.Mask &&
				   this.Reference == other.Reference &&
				   this.Fail == other.Fail &&
				   this.DepthFail == other.DepthFail &&
				   this.DepthPass == other.DepthPass;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is StencilFaceState && this.Equals((StencilFaceState)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = (int)this.Function;
				hashCode = (hashCode * 397) ^ (int)this.Mask;
				hashCode = (hashCode * 397) ^ this.Reference;
				hashCode = (hashCode * 397) ^ (int)this.Fail;
				hashCode = (hashCode * 397) ^ (int)this.DepthFail;
				hashCode = (hashCode * 397) ^ (int)this.DepthPass;
				return hashCode;
			}
		}

		#endregion
	}
}
