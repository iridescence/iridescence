﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a stack of <see cref="StateOp"/>.
	/// </summary>
	public class StateStack : IStateStack
	{
		#region Fields

		internal StateOp[] Stack;
		internal int Index;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StateStack"/> class. 
		/// </summary>
		public StateStack()
		{
			this.Stack = new StateOp[4];
		}

		#endregion

		#region Methods

		internal void MergeState(State initialState)
		{
			for (int i = 0; i < this.Index; ++i)
			{
				this.Stack[i].Apply(initialState);
			}
		}

		/// <summary>
		/// Pushes the specified state part onto the stack and returns an <see cref="IDisposable"/> that must be disposed to pop it.
		/// </summary>
		/// <param name="part">The state to push.</param>
		public StateStackItem With(StateOp part)
		{
			return new StateStackItem(this).With(part);
		}
		
		/// <summary>
		/// Pushes the specified state parts onto the stack and returns an <see cref="IDisposable"/> that must be disposed to pop them.
		/// </summary>
		/// <param name="parts">The <see cref="StateOp">StateParts</see> to push. They are pushed in the order they appear in the collection.</param>
		public StateStackItem With(params StateOp[] parts)
		{
			StateStackItem item = new StateStackItem(this);
			foreach (StateOp part in parts)
				item = item.With(part);
			return item;
		}

		/// <summary>
		/// Pushes the specified state part onto the stack.
		/// </summary>
		/// <param name="state"></param>
		public void Push(StateOp state)
		{
			if (state == null)
				throw new ArgumentNullException(nameof(state));

			if (this.Index >= this.Stack.Length)
			{
				Array.Resize(ref this.Stack, this.Stack.Length + 4);
			}

			this.Stack[this.Index++] = state;
		}

		/// <summary>
		/// Pops a state part from the stack.
		/// </summary>
		public StateOp Pop()
		{
			if(this.Index == 0)
				throw new InvalidOperationException("Stack is empty.");

			--this.Index;

			StateOp state = this.Stack[this.Index];
			this.Stack[this.Index] = null;
			return state;
		}

		#endregion
	}
}
