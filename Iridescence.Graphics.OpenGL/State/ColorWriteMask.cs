﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a color write mask.
	/// </summary>
	[Serializable]
	public struct ColorWriteMask
	{
		#region Fields

		/// <summary>
		/// Red channel write enabled.
		/// </summary>
		public bool R;

		/// <summary>
		/// Green channel write enabled.
		/// </summary>
		public bool G;

		/// <summary>
		/// Blue channel write enabled.
		/// </summary>
		public bool B;

		/// <summary>
		/// Alpha channel write enabled.
		/// </summary>
		public bool A;


		/// <summary>
		/// All color channels.
		/// </summary>
		public static readonly ColorWriteMask All = new ColorWriteMask(true, true, true, true);

		/// <summary>
		/// No color channels.
		/// </summary>
		public static readonly ColorWriteMask None = new ColorWriteMask(false, false, false, false);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ColorWriteMask.
		/// </summary>
		public ColorWriteMask(bool r, bool g, bool b, bool a)
		{
			this.R = r;
			this.G = g;
			this.B = b;
			this.A = a;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Indicates whether this color mask and a specified color mask are equal.
		/// </summary>
		/// <param name="other">The color mask to compare with the current color mask.</param>
		/// <returns>True if <paramref name="other"/> and this color mask are the same type and represent the same value; otherwise, false.</returns>
		public bool Equals(ColorWriteMask other)
		{
			return this.R == other.R && this.G == other.G && this.B == other.B && this.A == other.A;
		}

		/// <summary>
		/// Indicates whether this instance and a specified object are equal.
		/// </summary>
		/// <param name="obj">The object to compare with the current instance.</param>
		/// <returns>True if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is ColorWriteMask && this.Equals((ColorWriteMask)obj);
		}

		/// <summary>
		/// Returns the hash code for this instance.
		/// </summary>
		/// <returns>A 32-bit signed integer that is the hash code for this instance.</returns>
		public override int GetHashCode()
		{
			return (this.R ? 1 : 0) | (this.G ? 2 : 0) | (this.B ? 4 : 0) | (this.A ? 8 : 0);
		}

		#endregion
	}
}
