﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents the state of an OpenGL texture unit.
	/// </summary>
	[Serializable]
	public struct TextureUnitState
	{
		#region Fields
		
		public TextureTarget Target;
		public uint Texture;
		public uint Sampler;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		#endregion

		#region Methods

		#endregion
	}
}
