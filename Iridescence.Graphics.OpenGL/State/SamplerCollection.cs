﻿using System.Collections.Generic;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a collection of sampler unit bindings.
	/// </summary>
	public class SamplerCollection : Dictionary<uint, Sampler>
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SamplerCollection.
		/// </summary>
		public SamplerCollection()
		{

		}

		/// <summary>
		/// Creates a new SamplerCollection.
		/// </summary>
		public SamplerCollection(SamplerCollection other)
			: this()
		{
			other.CopyTo(this, true);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Copies the bindings of this collection to the other collection.
		/// </summary>
		/// <param name="other">The <see cref="SamplerCollection"/> to copy to.</param>
		/// <param name="replace">True, if all the entries in the other collection should be replaced, false to only change the ones also defined in this collection.</param>
		public void CopyTo(SamplerCollection other, bool replace)
		{
			if (replace)
			{
				other.Clear();
				foreach (KeyValuePair<uint, Sampler> binding in this)
				{
					other.Add(binding.Key, binding.Value);
				}
			}
			else
			{
				foreach (KeyValuePair<uint, Sampler> binding in this)
				{
					other[binding.Key] = binding.Value;
				}
			}
		}

		#endregion
	}
}
