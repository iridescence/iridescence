﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	public struct BufferBindings
	{
		#region Fields

		private uint arrayBuffer;
		private uint pixelPackBuffer;
		private uint pixelUnpackBuffer;
		private uint uniformBuffer;
		private uint textureBuffer;
		private uint transformFeedbackBuffer;
		private uint copyReadBuffer;
		private uint copyWriteBuffer;
		private uint drawIndirectBuffer;
		private uint shaderStorageBuffer;
		private uint dispatchIndirectBuffer;
		private uint queryBuffer;
		private uint atomicCounterBuffer;

		internal static readonly BufferTarget[] Targets =
		{
			BufferTarget.ArrayBuffer,
			BufferTarget.PixelPackBuffer,
			BufferTarget.PixelUnpackBuffer,
			BufferTarget.UniformBuffer,
			BufferTarget.TextureBuffer,
			BufferTarget.TransformFeedbackBuffer,
			BufferTarget.CopyReadBuffer,
			BufferTarget.CopyWriteBuffer,
			BufferTarget.DrawIndirectBuffer,
			BufferTarget.ShaderStorageBuffer,
			BufferTarget.DispatchIndirectBuffer,
			BufferTarget.QueryBuffer,
			BufferTarget.AtomicCounterBuffer
		};

		#endregion

		#region Properties

		public uint this[BufferTarget target]
		{
			get
			{
				switch (target)
				{
					case BufferTarget.ArrayBuffer:
						return this.arrayBuffer;
					case BufferTarget.PixelPackBuffer:
						return this.pixelPackBuffer;
					case BufferTarget.PixelUnpackBuffer:
						return this.pixelUnpackBuffer;
					case BufferTarget.UniformBuffer:
						return this.uniformBuffer;
					case BufferTarget.TextureBuffer:
						return this.textureBuffer;
					case BufferTarget.TransformFeedbackBuffer:
						return this.transformFeedbackBuffer;
					case BufferTarget.CopyReadBuffer:
						return this.copyReadBuffer;
					case BufferTarget.CopyWriteBuffer:
						return this.copyWriteBuffer;
					case BufferTarget.DrawIndirectBuffer:
						return this.drawIndirectBuffer;
					case BufferTarget.ShaderStorageBuffer:
						return this.shaderStorageBuffer;
					case BufferTarget.DispatchIndirectBuffer:
						return this.dispatchIndirectBuffer;
					case BufferTarget.QueryBuffer:
						return this.queryBuffer;
					case BufferTarget.AtomicCounterBuffer:
						return this.atomicCounterBuffer;
					default:
						throw new ArgumentOutOfRangeException(nameof(target));
				}
			}
			set
			{
				switch (target)
				{
					case BufferTarget.ArrayBuffer:
						this.arrayBuffer = value;
						break;
					case BufferTarget.PixelPackBuffer:
						this.pixelPackBuffer = value;
						break;
					case BufferTarget.PixelUnpackBuffer:
						this.pixelUnpackBuffer = value;
						break;
					case BufferTarget.UniformBuffer:
						this.uniformBuffer = value;
						break;
					case BufferTarget.TextureBuffer:
						this.textureBuffer = value;
						break;
					case BufferTarget.TransformFeedbackBuffer:
						this.transformFeedbackBuffer = value;
						break;
					case BufferTarget.CopyReadBuffer:
						this.copyReadBuffer = value;
						break;
					case BufferTarget.CopyWriteBuffer:
						this.copyWriteBuffer = value;
						break;
					case BufferTarget.DrawIndirectBuffer:
						this.drawIndirectBuffer = value;
						break;
					case BufferTarget.ShaderStorageBuffer:
						this.shaderStorageBuffer = value;
						break;
					case BufferTarget.DispatchIndirectBuffer:
						this.dispatchIndirectBuffer = value;
						break;
					case BufferTarget.QueryBuffer:
						this.queryBuffer = value;
						break;
					case BufferTarget.AtomicCounterBuffer:
						this.atomicCounterBuffer = value;
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(target));
				}
			}
		}

		#endregion

		#region Constructors

		#endregion

		#region Methods

		public void Unbind(uint id)
		{
			foreach (BufferTarget target in Targets)
			{
				if (this[target] == id)
					this[target] = 0;
			}
		}

		#endregion
	}
}
