﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Internal state for draw calls.
	/// </summary>
	internal sealed class DrawState : StateOp, ICloneable
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the program state to use.
		/// </summary>
		public Program Program { get; set; }

		/// <summary>
		/// Gets or sets the vertex array to use.
		/// </summary>
		public VertexArray VertexArray { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new DrawState.
		/// </summary>
		public DrawState()
		{
			this.VertexArray = null;
			this.Program = null;
		}

		/// <summary>
		/// Creates a copy of the specified scissor state.
		/// </summary>
		/// <param name="state"></param>
		public DrawState(DrawState state)
		{
			state.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.VertexArray = this.VertexArray.ID;
			state.Program = this.Program.ID;
		}

		/// <summary>
		/// Copies this scissor state to the specified state.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(DrawState dest)
		{
			dest.VertexArray = this.VertexArray;
			dest.Program = this.Program;
		}

		/// <summary>
		/// Determines whether the specified draw state is equal to the current draw state.
		/// </summary>
		/// <param name="other">The draw state to compare with the current draw state.</param>
		/// <returns>True if the specified draw state is equal to the current draw state; otherwise, false.</returns>
		public bool Equals(DrawState other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(this.VertexArray, other.VertexArray) &&
				   Equals(this.Program, other.Program);
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is DrawState && this.Equals((DrawState)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.VertexArray.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Program.GetHashCode();
				return hashCode;
			}
		}

		
		/// <summary>
		/// Creates a copy of the draw state.
		/// </summary>
		/// <returns></returns>
		public DrawState Clone()
		{
			return new DrawState(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion

	}
}
