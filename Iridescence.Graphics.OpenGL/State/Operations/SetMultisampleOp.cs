﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets a value that determines whether multisampling is active.
	/// </summary>
	public sealed class SetMultisampleOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets whether multisampling is enabled.
		/// </summary>
		public bool Enabled { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetMultisampleOp"/>.
		/// </summary>
		public SetMultisampleOp()
		{
			this.Enabled = true;
		}

		/// <summary>
		/// Creates a new <see cref="SetMultisampleOp"/> with the specified parameters.
		/// </summary>
		/// <param name="enabled"></param>
		public SetMultisampleOp(bool enabled)
		{
			this.Enabled = enabled;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetMultisampleOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetMultisampleOp(SetMultisampleOp other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.MultisampleEnabled = this.Enabled;
		}

		/// <summary>
		/// Copies this <see cref="SetMultisampleOp"/> to the specified <see cref="SetMultisampleOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetMultisampleOp dest)
		{
			dest.Enabled = this.Enabled;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetMultisampleOp"/> is equal to the current <see cref="SetMultisampleOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetMultisampleOp"/> to compare with the current <see cref="SetMultisampleOp"/>.</param>
		/// <returns>True if the specified <see cref="SetMultisampleOp"/> is equal to the current <see cref="SetMultisampleOp"/>; otherwise, false.</returns>
		public bool Equals(SetMultisampleOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Enabled == other.Enabled;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetMultisampleOp && this.Equals((SetMultisampleOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>
		/// A hash code for the current object.
		/// </returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.Enabled.GetHashCode();
				return hashCode;
			}
		}

		/// <summary>
		/// Returns a copy of this <see cref="SetMultisampleOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetMultisampleOp Clone()
		{
			return new SetMultisampleOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
