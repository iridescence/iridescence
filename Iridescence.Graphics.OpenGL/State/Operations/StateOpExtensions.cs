using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	public static class StateOpExtensions
	{
		/// <summary>
		/// Binds a framebuffer by invoking <see cref="IStateStack.With"/> using a new <see cref="BindFramebufferOp"/> with the specified <see cref="Framebuffer"/>.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="framebuffer"></param>
		/// <returns></returns>
		public static StateStackItem WithFramebuffer(this IStateStack stack, Framebuffer framebuffer)
		{
			return stack.With(new BindFramebufferOp(FramebufferBinding.Both, framebuffer));
		}

		/// <summary>
		/// Binds a framebuffer by invoking <see cref="IStateStack.With"/> using a new <see cref="BindFramebufferOp"/> with the specified <see cref="Framebuffer"/>.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="framebuffer"></param>
		/// <returns></returns>
		public static StateStackItem WithDrawFramebuffer(this IStateStack stack, Framebuffer framebuffer)
		{
			return stack.With(new BindFramebufferOp(FramebufferBinding.Draw, framebuffer));
		}

		/// <summary>
		/// Binds a framebuffer by invoking <see cref="IStateStack.With"/> using a new <see cref="BindFramebufferOp"/> with the specified <see cref="Framebuffer"/>.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="framebuffer"></param>
		/// <returns></returns>
		public static StateStackItem WithReadFramebuffer(this IStateStack stack, Framebuffer framebuffer)
		{
			return stack.With(new BindFramebufferOp(FramebufferBinding.Read, framebuffer));
		}

		/// <summary>
		/// Sets the viewport by invoking <see cref="IStateStack.With"/> using a new <see cref="SetViewportOp"/> with the specified viewport.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="viewport"></param>
		/// <returns></returns>
		public static StateStackItem WithViewport(this IStateStack stack, RectangleI viewport)
		{
			return stack.With(new SetViewportOp(viewport));
		}

		/// <summary>
		/// Sets the viewport by invoking <see cref="IStateStack.With"/> using a new <see cref="SetViewportOp"/> with the specified viewport.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns></returns>
		public static StateStackItem WithViewport(this IStateStack stack, int x, int y, int width, int height)
		{
			return stack.WithViewport(new RectangleI(x, y, width, height));
		}

		/// <summary>
		/// Sets the depth test parameters by invoking <see cref="IStateStack.With"/> using a new <see cref="SetDepthTestOp"/> with the specified parameters.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="depthTest"></param>
		/// <param name="depthFunction"></param>
		/// <returns></returns>
		public static StateStackItem WithDepthTest(this IStateStack stack, bool depthTest, DepthFunction depthFunction)
		{
			return stack.With(new SetDepthTestOp(depthTest, depthFunction));
		}
		
		/// <summary>
		/// Disables the depth test by invoking <see cref="IStateStack.With"/> using a new <see cref="SetDepthTestOp"/> with the depthTest set to false.
		/// </summary>
		/// <param name="stack"></param>
		/// <returns></returns>
		public static StateStackItem WithoutDepthTest(this IStateStack stack)
		{
			return stack.With(new SetDepthTestOp(false, DepthFunction.Always));
		}
		
		/// <summary>
		/// Sets the depth clamping parameters by invoking <see cref="IStateStack.With"/> using a new <see cref="SetDepthRangeOp"/> with the specified parameters.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="depthNear"></param>
		/// <param name="depthFar"></param>
		/// <param name="depthClamp"></param>
		/// <returns></returns>
		public static StateStackItem WithDepthRange(this IStateStack stack, float depthNear, float depthFar, bool depthClamp)
		{
			return stack.With(new SetDepthRangeOp(depthNear, depthFar, depthClamp));
		}

		/// <summary>
		/// Sets the scissor test parameters by invoking <see cref="IStateStack.With"/> using a new <see cref="SetScissorOp"/> with the specified parameters.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="scissorTest"></param>
		/// <param name="scissor"></param>
		/// <returns></returns>
		public static StateStackItem WithScissor(this IStateStack stack, bool scissorTest, RectangleI scissor)
		{
			return stack.With(new SetScissorOp(scissorTest, scissor));
		}

		/// <summary>
		/// Sets the scissor test parameters by invoking <see cref="IStateStack.With"/> using a new <see cref="SetScissorOp"/> with the specified parameters.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="scissorTest"></param>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns></returns>
		public static StateStackItem WithScissor(this IStateStack stack, bool scissorTest, int x, int y, int width, int height)
		{
			return stack.WithScissor(scissorTest, new RectangleI(x, y, width, height));
		}

		/// <summary>
		/// Disables the scissor test by invoking <see cref="IStateStack.With"/> using a new <see cref="SetScissorOp"/> with <see cref="SetScissorOp.ScissorTest"/> set to false.
		/// </summary>
		/// <param name="stack"></param>
		/// <returns></returns>
		public static StateStackItem WithoutScissor(this IStateStack stack)
		{
			return stack.WithScissor(false, RectangleI.Zero);
		}

		/// <summary>
		/// Binds a texture to a texture unit by invoking <see cref="IStateStack.With"/> using a new <see cref="BindTextureOp"/>.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="unit"></param>
		/// <param name="texture"></param>
		/// <returns></returns>
		public static StateStackItem WithTexture(this IStateStack stack, uint unit, Texture texture)
		{
			return stack.With(new BindTextureOp(unit, texture));
		}

		/// <summary>
		/// Binds a texture to a texture unit by invoking <see cref="IStateStack.With"/> using a new <see cref="BindTextureIdOp"/>.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="unit"></param>
		/// <param name="target"></param>
		/// <param name="texture"></param>
		/// <returns></returns>
		public static StateStackItem WithTexture(this IStateStack stack, uint unit, TextureTarget target, uint texture)
		{
			return stack.With(new BindTextureIdOp(unit, target, texture));
		}
		
		/// <summary>
		/// Binds a sampler to a texture unit by invoking <see cref="IStateStack.With"/> using a new <see cref="BindSamplerOp"/>.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="unit"></param>
		/// <param name="sampler"></param>
		/// <returns></returns>
		public static StateStackItem WithSampler(this IStateStack stack, uint unit, Sampler sampler)
		{
			return stack.With(new BindSamplerOp(unit, sampler));
		}

		/// <summary>
		/// Sets the clear parameters by invoking <see cref="IStateStack.With"/> using a new <see cref="SetClearParametersOp"/>.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="clearColor"></param>
		/// <param name="clearDepth"></param>
		/// <param name="clearStencil"></param>
		/// <returns></returns>
		public static StateStackItem WithClearParameters(this IStateStack stack, Color4 clearColor, float clearDepth, int clearStencil)
		{
			return stack.With(new SetClearParametersOp(clearColor, clearDepth, clearStencil));
		}

		/// <summary>
		/// Sets the write masks by invoking <see cref="IStateStack.With"/> using a new <see cref="SetWriteMasksOp"/>.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="colorMask"></param>
		/// <param name="depthMask"></param>
		/// <param name="stencilFrontMask"></param>
		/// <param name="stencilBackMask"></param>
		/// <returns></returns>
		public static StateStackItem WithWriteMasks(this IStateStack stack, ColorWriteMask colorMask, bool depthMask, uint stencilFrontMask, uint stencilBackMask)
		{
			return stack.With(new SetWriteMasksOp(colorMask, depthMask, stencilFrontMask, stencilBackMask));
		}
		
		/// <summary>
		/// Sets the write masks by invoking <see cref="IStateStack.With"/> using a new <see cref="SetWriteMasksOp"/>.
		/// </summary>
		/// <param name="stack"></param>
		/// <param name="redMask"></param>
		/// <param name="greenMask"></param>
		/// <param name="blueMask"></param>
		/// <param name="alphaMask"></param>
		/// <param name="depthMask"></param>
		/// <param name="stencilFrontMask"></param>
		/// <param name="stencilBackMask"></param>
		/// <returns></returns>
		public static StateStackItem WithWriteMasks(this IStateStack stack, bool redMask, bool greenMask, bool blueMask, bool alphaMask, bool depthMask, uint stencilFrontMask, uint stencilBackMask)
		{
			return stack.With(new SetWriteMasksOp(new ColorWriteMask(redMask, greenMask, blueMask, alphaMask), depthMask, stencilFrontMask, stencilBackMask));
		}

		public static StateStackItem WithBlend(this IStateStack stack, bool enabled, BlendingFactorSrc source, BlendingFactorDest dest)
		{
			return stack.With(new SetBlendOp {Enabled = enabled, Source = source, SourceAlpha = source, Destination = dest, DestinationAlpha = dest});
		}

		public static StateStackItem WithBlend(this IStateStack stack, bool enabled, BlendingFactorSrc source, BlendingFactorDest dest, BlendingFactorSrc sourceAlpha, BlendingFactorDest destAlpha)
		{
			return stack.With(new SetBlendOp {Enabled = enabled, Source = source, SourceAlpha = sourceAlpha, Destination = dest, DestinationAlpha = destAlpha});
		}

		public static StateStackItem WithoutBlend(this IStateStack stack)
		{
			return stack.With(new SetBlendOp {Enabled = false});
		}

		public static StateStackItem WithMultisample(this IStateStack stack)
		{
			return stack.With(new SetMultisampleOp(true));
		}

		public static StateStackItem WithMultisample(this IStateStack stack, bool enabled)
		{
			return stack.With(new SetMultisampleOp(enabled));
		}
		
		public static StateStackItem WithoutMultisample(this IStateStack stack)
		{
			return stack.With(new SetMultisampleOp(false));
		}

		public static StateStackItem WithFramebufferSrgb(this IStateStack stack)
		{
			return stack.With(new SetFramebufferSrgb(true));
		}

		public static StateStackItem WithFramebufferSrgb(this IStateStack stack, bool enabled)
		{
			return stack.With(new SetFramebufferSrgb(enabled));
		}

		public static StateStackItem WithoutFramebufferSrgb(this IStateStack stack)
		{
			return stack.With(new SetFramebufferSrgb(false));
		}

	}
}