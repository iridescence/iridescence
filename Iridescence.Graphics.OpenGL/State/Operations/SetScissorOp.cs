﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the scissor test parameters.
	/// </summary>
	public sealed class SetScissorOp : StateOp, ICloneable
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a value that determines whether the scissor test is enabled.
		/// </summary>
		public bool ScissorTest { get; set; }

		/// <summary>
		/// Gets or sets the scissor rectangle.
		/// </summary>
		public RectangleI Scissor { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetScissorOp"/>.
		/// </summary>
		public SetScissorOp()
		{
			this.ScissorTest = false;
			this.Scissor = RectangleI.Zero;
		}

		/// <summary>
		/// Creates a new <see cref="SetScissorOp"/> with the specified parameters.
		/// </summary>
		/// <param name="scissorTest"></param>
		/// <param name="scissor"></param>
		public SetScissorOp(bool scissorTest, RectangleI scissor)
		{
			this.ScissorTest = scissorTest;
			this.Scissor = scissor;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetScissorOp"/>.
		/// </summary>
		/// <param name="state"></param>
		public SetScissorOp(SetScissorOp state)
		{
			state.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.ScissorTest = this.ScissorTest;
			state.Scissor = this.Scissor;
		}

		/// <summary>
		/// Copies this <see cref="SetScissorOp"/> to the specified <see cref="SetScissorOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetScissorOp dest)
		{
			dest.ScissorTest = this.ScissorTest;
			dest.Scissor = this.Scissor;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetScissorOp"/> is equal to the current <see cref="SetScissorOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetScissorOp"/> to compare with the current <see cref="SetScissorOp"/>.</param>
		/// <returns>True if the specified <see cref="SetScissorOp"/> is equal to the current <see cref="SetScissorOp"/>; otherwise, false.</returns>
		public bool Equals(SetScissorOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.ScissorTest == other.ScissorTest &&
				   this.Scissor.Equals(other.Scissor);
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetScissorOp && this.Equals((SetScissorOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.ScissorTest.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Scissor.GetHashCode();
				return hashCode;
			}
		}
		
		/// <summary>
		/// Creates a copy of the <see cref="SetScissorOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetScissorOp Clone()
		{
			return new SetScissorOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
