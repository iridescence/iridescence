﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an abstract OpenGL state operation.
	/// </summary>
	public abstract class StateOp
	{
		/// <summary>
		/// Applies the operation to a <see cref="State"/>.
		/// </summary>
		/// <param name="state"></param>
		public abstract void Apply(State state);
	}
}
