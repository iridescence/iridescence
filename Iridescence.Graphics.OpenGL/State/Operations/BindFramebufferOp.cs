﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that binds a framebuffer to a framebuffer target.
	/// </summary>
	public sealed class BindFramebufferOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets the target to bind the framebuffer to.
		/// </summary>
		public FramebufferBinding Binding { get; set; }

		/// <summary>
		/// Gets or sets the framebuffer to bind.
		/// Set this to null to draw to the default framebuffer (i.e. the window).
		/// </summary>
		public Framebuffer Framebuffer { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="BindFramebufferOp"/>.
		/// </summary>
		public BindFramebufferOp()
		{
			this.Binding = FramebufferBinding.Both;
			this.Framebuffer = null;
		}

		/// <summary>
		/// Creates a new <see cref="BindFramebufferOp"/>.
		/// </summary>
		/// <param name="binding">The binding.</param>
		/// <param name="framebuffer">The framebuffer to bind.</param>
		public BindFramebufferOp(FramebufferBinding binding, Framebuffer framebuffer)
		{
			this.Binding = binding;
			this.Framebuffer = framebuffer;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="BindFramebufferOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public BindFramebufferOp(BindFramebufferOp other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			if ((this.Binding & FramebufferBinding.Read) != 0)
				state.ReadFramebuffer = this.Framebuffer?.ID ?? 0;

			if ((this.Binding & FramebufferBinding.Draw) != 0)
				state.DrawFramebuffer = this.Framebuffer?.ID ?? 0;
		}

		/// <summary>
		/// Copies this <see cref="BindFramebufferOp"/> to the specified <see cref="BindFramebufferOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(BindFramebufferOp dest)
		{
			dest.Binding = this.Binding;
			dest.Framebuffer = this.Framebuffer;
		}

		/// <summary>
		/// Determines whether the specified <see cref="BindFramebufferOp"/> is equal to the current <see cref="BindFramebufferOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="BindFramebufferOp"/> to compare with the current <see cref="BindFramebufferOp"/>.</param>
		/// <returns>True if the specified <see cref="BindFramebufferOp"/> is equal to the current <see cref="BindFramebufferOp"/>; otherwise, false.</returns>
		public bool Equals(BindFramebufferOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Binding == other.Binding &&
			       Equals(this.Framebuffer, other.Framebuffer);
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is BindFramebufferOp && this.Equals((BindFramebufferOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				return ((int)this.Binding * 397) ^ (this.Framebuffer?.GetHashCode() ?? 0);
			}
		}

		/// <summary>
		/// Creates a copy of this <see cref="BindFramebufferOp"/>
		/// </summary>
		/// <returns></returns>
		public BindFramebufferOp Clone()
		{
			return new BindFramebufferOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
