﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the point rasterization parameters.
	/// </summary>
	public sealed class SetPointParametersOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets the point size.
		/// </summary>
		public float PointSize { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetPointParametersOp"/>.
		/// </summary>
		public SetPointParametersOp()
		{
			this.PointSize = 1.0f;
		}

		/// <summary>
		/// Creates a new <see cref="SetPointParametersOp"/> with the specified parameters.
		/// </summary>
		/// <param name="pointSize"></param>
		/// <param name="pointSmoothing"></param>
		public SetPointParametersOp(float pointSize)
		{
			this.PointSize = pointSize;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetPointParametersOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetPointParametersOp(SetPointParametersOp other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.PointSize = this.PointSize;
		}

		/// <summary>
		/// Copies this <see cref="SetPointParametersOp"/> to the specified <see cref="SetPointParametersOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetPointParametersOp dest)
		{
			dest.PointSize = this.PointSize;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetPointParametersOp"/> is equal to the current <see cref="SetPointParametersOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetPointParametersOp"/> to compare with the current <see cref="SetPointParametersOp"/>.</param>
		/// <returns>True if the specified <see cref="SetPointParametersOp"/> is equal to the current <see cref="SetPointParametersOp"/>; otherwise, false.</returns>
		public bool Equals(SetPointParametersOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.PointSize.Equals(other.PointSize);
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetPointParametersOp && this.Equals((SetPointParametersOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>
		/// A hash code for the current object.
		/// </returns>
		public override int GetHashCode()
		{
			return this.PointSize.GetHashCode();
		}

		/// <summary>
		/// Returns a copy of this <see cref="SetRasterizationParametersOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetPointParametersOp Clone()
		{
			return new SetPointParametersOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
