﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the line rasterization parameters.
	/// </summary>
	public sealed class SetLineParametersOp : StateOp, ICloneable
	{
		#region Properties
		
		/// <summary>
		/// Gets or sets the line width.
		/// </summary>
		public float LineWidth { get; set; }

		/// <summary>
		/// Gets or sets a value that determines whether line antialiasing is enabled.
		/// </summary>
		public bool LineSmoothing { get; set; }
	
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetLineParametersOp"/>.
		/// </summary>
		public SetLineParametersOp()
		{
			this.LineWidth = 1.0f;
			this.LineSmoothing = false;
		}

		/// <summary>
		/// Creates a new <see cref="SetLineParametersOp"/> with the specified parameters.
		/// </summary>
		/// <param name="lineWidth"></param>
		/// <param name="lineSmoothing"></param>
		public SetLineParametersOp(float lineWidth, bool lineSmoothing)
		{
			this.LineWidth = lineWidth;
			this.LineSmoothing = lineSmoothing;
		}
		
		/// <summary>
		/// Creates a copy of the specified <see cref="SetLineParametersOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetLineParametersOp(SetLineParametersOp other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.LineWidth = this.LineWidth;
			state.LineSmoothing = this.LineSmoothing;
		}

		/// <summary>
		/// Copies this <see cref="SetLineParametersOp"/> to the specified <see cref="SetLineParametersOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetLineParametersOp dest)
		{
			dest.LineWidth = this.LineWidth;
			dest.LineSmoothing = this.LineSmoothing;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetLineParametersOp"/> is equal to the current <see cref="SetLineParametersOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetLineParametersOp"/> to compare with the current <see cref="SetLineParametersOp"/>.</param>
		/// <returns>True if the specified <see cref="SetLineParametersOp"/> is equal to the current <see cref="SetLineParametersOp"/>; otherwise, false.</returns>
		public bool Equals(SetLineParametersOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.LineWidth.Equals(other.LineWidth) &&
				   this.LineSmoothing == other.LineSmoothing;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetLineParametersOp && this.Equals((SetLineParametersOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>
		/// A hash code for the current object.
		/// </returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.LineWidth.GetHashCode();
				hashCode = (hashCode * 397) ^ this.LineSmoothing.GetHashCode();
				return hashCode;
			}
		}

		/// <summary>
		/// Returns a copy of this <see cref="SetLineParametersOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetLineParametersOp Clone()
		{
			return new SetLineParametersOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
