﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the depth test parameters.
	/// </summary>
	public sealed class SetDepthTestOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets a value that determines whether depth test is enabled.
		/// </summary>
		public bool DepthTest { get; set; }

		/// <summary>
		/// Gets or sets the depth function.
		/// </summary>
		public DepthFunction DepthFunction { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetDepthTestOp"/>.
		/// </summary>
		public SetDepthTestOp()
		{
			this.DepthTest = false;
			this.DepthFunction = DepthFunction.Less;
		}

		/// <summary>
		/// Creates a new <see cref="SetDepthTestOp"/> with the specified parameters.
		/// </summary>
		public SetDepthTestOp(bool depthTest, DepthFunction depthFunction)
		{
			this.DepthTest = depthTest;
			this.DepthFunction = depthFunction;
		}
		
		/// <summary>
		/// Creates a copy of the specified <see cref="SetDepthTestOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetDepthTestOp(SetDepthTestOp other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.DepthTest = this.DepthTest;
			state.DepthFunction = this.DepthFunction;
		}

		/// <summary>
		/// Copies this <see cref="SetDepthTestOp"/> to the specified <see cref="SetDepthTestOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetDepthTestOp dest)
		{
			dest.DepthTest = this.DepthTest;
			dest.DepthFunction = this.DepthFunction;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetDepthTestOp"/> is equal to the current <see cref="SetDepthTestOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetDepthTestOp"/> to compare with the current <see cref="SetDepthTestOp"/>.</param>
		/// <returns>True if the specified <see cref="SetDepthTestOp"/> is equal to the <see cref="SetDepthTestOp"/> state; otherwise, false.</returns>
		public bool Equals(SetDepthTestOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.DepthTest == other.DepthTest &&
				   this.DepthFunction == other.DepthFunction;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetDepthTestOp && this.Equals((SetDepthTestOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.DepthTest.GetHashCode();
				hashCode = (hashCode * 397) ^ (int)this.DepthFunction;
				return hashCode;
			}
		}

		/// <summary>
		/// Returns a copy of the <see cref="SetDepthTestOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetDepthTestOp Clone()
		{
			return new SetDepthTestOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
