﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the stencil test parameters.
	/// </summary>
	public sealed class SetStencilTestOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets a value that determines whether the stencil test is enabled.
		/// </summary>
		public bool StencilTest { get; set; }

		/// <summary>
		/// Gets or sets the stencil front face state.
		/// </summary>
		public StencilFaceState StencilFront { get; set; }

		/// <summary>
		/// Gets or sets the stencil back face state.
		/// </summary>
		public StencilFaceState StencilBack { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetStencilTestOp"/>.
		/// </summary>
		public SetStencilTestOp()
		{
			this.StencilTest = false;
			this.StencilFront = new StencilFaceState();
			this.StencilBack = new StencilFaceState();
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetStencilTestOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetStencilTestOp(SetStencilTestOp other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.StencilTest = this.StencilTest;
			state.StencilFront = this.StencilFront;
			state.StencilBack = this.StencilBack;
		}

		/// <summary>
		/// Copies this <see cref="SetStencilTestOp"/> to the specified <see cref="SetStencilTestOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetStencilTestOp dest)
		{
			dest.StencilTest = this.StencilTest;
			dest.StencilFront = this.StencilFront;
			dest.StencilBack = this.StencilBack;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetStencilTestOp"/> is equal to the current <see cref="SetStencilTestOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetStencilTestOp"/> to compare with the current <see cref="SetStencilTestOp"/>.</param>
		/// <returns>True if the specified <see cref="SetStencilTestOp"/> is equal to the current <see cref="SetStencilTestOp"/>; otherwise, false.</returns>
		public bool Equals(SetStencilTestOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.StencilTest == other.StencilTest &&
				   Equals(this.StencilFront, other.StencilFront) &&
				   Equals(this.StencilBack, other.StencilBack);
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetStencilTestOp && this.Equals((SetStencilTestOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.StencilTest.GetHashCode();
				hashCode = (hashCode * 397) ^ this.StencilFront.GetHashCode();
				hashCode = (hashCode * 397) ^ this.StencilBack.GetHashCode();
				return hashCode;
			}
		}

		/// <summary>
		/// Returns a copy of the <see cref="SetStencilTestOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetStencilTestOp Clone()
		{
			return new SetStencilTestOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
