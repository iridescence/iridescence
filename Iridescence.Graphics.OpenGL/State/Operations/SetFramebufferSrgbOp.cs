﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets a value that determines whether framebuffer sRGB mode is active.
	/// </summary>
	public sealed class SetFramebufferSrgb : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets whether sRGB is enabled.
		/// </summary>
		public bool Enabled { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetFramebufferSrgb"/>.
		/// </summary>
		public SetFramebufferSrgb()
		{
			this.Enabled = true;
		}

		/// <summary>
		/// Creates a new <see cref="SetFramebufferSrgb"/> with the specified parameters.
		/// </summary>
		/// <param name="enabled"></param>
		public SetFramebufferSrgb(bool enabled)
		{
			this.Enabled = enabled;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetFramebufferSrgb"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetFramebufferSrgb(SetFramebufferSrgb other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.FramebufferSrgbEnabled = this.Enabled;
		}

		/// <summary>
		/// Copies this <see cref="SetMultisampleOp"/> to the specified <see cref="SetMultisampleOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetFramebufferSrgb dest)
		{
			dest.Enabled = this.Enabled;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetFramebufferSrgb"/> is equal to the current <see cref="SetFramebufferSrgb"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetFramebufferSrgb"/> to compare with the current <see cref="SetFramebufferSrgb"/>.</param>
		/// <returns>True if the specified <see cref="SetFramebufferSrgb"/> is equal to the current <see cref="SetFramebufferSrgb"/>; otherwise, false.</returns>
		public bool Equals(SetFramebufferSrgb other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Enabled == other.Enabled;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetFramebufferSrgb && this.Equals((SetFramebufferSrgb)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>
		/// A hash code for the current object.
		/// </returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.Enabled.GetHashCode();
				return hashCode;
			}
		}

		/// <summary>
		/// Returns a copy of this <see cref="SetFramebufferSrgb"/>.
		/// </summary>
		/// <returns></returns>
		public SetFramebufferSrgb Clone()
		{
			return new SetFramebufferSrgb(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
