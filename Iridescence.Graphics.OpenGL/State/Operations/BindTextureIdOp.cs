﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that binds a texture to a texture unit.
	/// </summary>
	public sealed class BindTextureIdOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets the texture unit index.
		/// </summary>
		public uint Unit { get; set; }
		
		/// <summary>
		/// Gets or sets the texture target.
		/// </summary>
		public TextureTarget Target { get; set; }

		/// <summary>
		/// Gets or sets the texture bound to the unit.
		/// </summary>
		public uint Texture { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="BindTextureIdOp"/>.
		/// </summary>
		public BindTextureIdOp()
		{
			this.Unit = 0;
			this.Target = TextureTarget.Texture2D;
			this.Texture = 0;
		}

		/// <summary>
		/// Creates a new <see cref="BindTextureOp"/> with the specified unit and texture.
		/// </summary>
		/// <param name="unit"></param>
		/// <param name="target"></param>
		/// <param name="texture"></param>
		public BindTextureIdOp(uint unit, TextureTarget target, uint texture)
		{
			this.Unit = unit;
			this.Target = target;
			this.Texture = texture;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="BindTextureOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public BindTextureIdOp(BindTextureIdOp other)
		{
			other.CopyTo(this);	
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.TextureUnits[this.Unit].Target = this.Target;
			state.TextureUnits[this.Unit].Texture = this.Texture;
		}

		/// <summary>
		/// Copies this <see cref="BindTextureOp"/> to the specified <see cref="BindTextureOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(BindTextureIdOp dest)
		{
			dest.Unit = this.Unit;
			dest.Target = this.Target;
			dest.Texture = this.Texture;
		}

		/// <summary>
		/// Determines whether the specified <see cref="BindTextureOp" /> is equal to the current <see cref="BindTextureOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="BindTextureOp"/> to compare with the current <see cref="BindTextureOp"/>. </param>
		/// <returns>True if the specified <see cref="BindTextureOp"/> is equal to the current <see cref="BindTextureOp"/>; otherwise, false.</returns>
		public bool Equals(BindTextureIdOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Unit == other.Unit && this.Target == other.Target && this.Texture == other.Texture;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is BindTextureIdOp && this.Equals((BindTextureIdOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>
		/// A hash code for the current object.
		/// </returns>
		public override int GetHashCode()
		{
			return HashCode.Combine(this.Unit, this.Target, this.Texture);
		}

		/// <summary>
		/// Creates a deep copy of the <see cref="BindTextureOp"/>
		/// </summary>
		/// <returns></returns>
		public BindTextureIdOp Clone()
		{
			return new BindTextureIdOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
