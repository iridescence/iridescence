﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the framebuffer write masks.
	/// </summary>
	public sealed class SetWriteMasksOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets the color write mask.
		/// </summary>
		public ColorWriteMask ColorMask { get; set; }

		/// <summary>
		/// Gets or sets a value that determines whether draw calls can write to the depth buffer.
		/// </summary>
		public bool DepthMask { get; set; }

		/// <summary>
		/// Gets or sets a write mask for stencil front faces.
		/// </summary>
		public uint StencilFrontMask { get; set; }

		/// <summary>
		/// Gets or sets a write mask for stencil back faces.
		/// </summary>
		public uint StencilBackMask { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetWriteMasksOp"/>.
		/// </summary>
		public SetWriteMasksOp()
		{
			this.ColorMask = new ColorWriteMask(true, true, true, true);
			this.DepthMask = true;
			this.StencilFrontMask = ~0u;
			this.StencilBackMask = ~0u;
		}

		/// <summary>
		/// Creates a new <see cref="SetWriteMasksOp"/> with the specified parameters.
		/// </summary>
		/// <param name="colorMask"></param>
		/// <param name="depthMask"></param>
		/// <param name="stencilFrontMask"></param>
		/// <param name="stencilBackMask"></param>
		public SetWriteMasksOp(ColorWriteMask colorMask, bool depthMask, uint stencilFrontMask, uint stencilBackMask)
		{
			this.ColorMask = colorMask;
			this.DepthMask = depthMask;
			this.StencilFrontMask = stencilFrontMask;
			this.StencilBackMask = stencilBackMask;
		}
		
		/// <summary>
		/// Creates a copy of the specified <see cref="SetWriteMasksOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetWriteMasksOp(SetWriteMasksOp other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods
		
		public override void Apply(State state)
		{
			state.ColorMask = this.ColorMask;
			state.DepthMask = this.DepthMask;
			state.StencilFrontMask = this.StencilFrontMask;
			state.StencilBackMask = this.StencilBackMask;
		}

		/// <summary>
		/// Copies this <see cref="SetWriteMasksOp"/> to the specified <see cref="SetWriteMasksOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetWriteMasksOp dest)
		{
			dest.ColorMask = this.ColorMask;
			dest.DepthMask = this.DepthMask;
			dest.StencilFrontMask = this.StencilFrontMask;
			dest.StencilBackMask = this.StencilBackMask;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetWriteMasksOp"/> is equal to the current <see cref="SetWriteMasksOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetWriteMasksOp"/> to compare with the current <see cref="SetWriteMasksOp"/>.</param>
		/// <returns>True if the specified <see cref="SetWriteMasksOp"/> is equal to the current <see cref="SetWriteMasksOp"/>; otherwise, false.</returns>
		public bool Equals(SetWriteMasksOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.ColorMask.Equals(other.ColorMask) &&
			       this.DepthMask == other.DepthMask &&
			       this.StencilFrontMask == other.StencilFrontMask &&
			       this.StencilBackMask == other.StencilBackMask;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetWriteMasksOp && this.Equals((SetWriteMasksOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.ColorMask.GetHashCode();
				hashCode = (hashCode * 397) ^ this.DepthMask.GetHashCode();
				hashCode = (hashCode * 397) ^ (int)this.StencilFrontMask;
				hashCode = (hashCode * 397) ^ (int)this.StencilBackMask;
				return hashCode;
			}
		}

		/// <summary>
		/// Returns a copy of this <see cref="SetWriteMasksOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetWriteMasksOp Clone()
		{
			return new SetWriteMasksOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
