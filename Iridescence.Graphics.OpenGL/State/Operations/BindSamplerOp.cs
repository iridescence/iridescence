﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that binds a sampler to a texture unit.
	/// </summary>
	public sealed class BindSamplerOp : StateOp, ICloneable
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the texture unit index.
		/// </summary>
		public uint Unit { get; set; }

		/// <summary>
		/// Gets or sets the sampler bound to the unit.
		/// </summary>
		public Sampler Sampler { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="BindSamplerOp"/>.
		/// </summary>
		public BindSamplerOp()
		{
			this.Unit = 0;
			this.Sampler = null;
		}

		/// <summary>
		/// Creates a new <see cref="BindSamplerOp"/> with the specified unit and texture.
		/// </summary>
		/// <param name="unit"></param>
		/// <param name="sampler"></param>
		public BindSamplerOp(uint unit, Sampler sampler)
		{
			this.Unit = unit;
			this.Sampler = sampler;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="BindSamplerOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public BindSamplerOp(BindSamplerOp other)
		{
			other.CopyTo(this);	
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.TextureUnits[this.Unit].Sampler = this.Sampler?.ID ?? 0;
		}

		/// <summary>
		/// Copies this <see cref="BindSamplerOp"/> to the specified <see cref="BindSamplerOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(BindSamplerOp dest)
		{
			dest.Unit = this.Unit;
			dest.Sampler = this.Sampler;
		}

		/// <summary>
		/// Determines whether the specified <see cref="BindSamplerOp" /> is equal to the current <see cref="BindSamplerOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="BindSamplerOp"/> to compare with the current <see cref="BindSamplerOp"/>. </param>
		/// <returns>True if the specified <see cref="BindSamplerOp"/> is equal to the current <see cref="BindSamplerOp"/>; otherwise, false.</returns>
		public bool Equals(BindSamplerOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Unit == other.Unit && this.Sampler == other.Sampler;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is BindSamplerOp && this.Equals((BindSamplerOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>
		/// A hash code for the current object.
		/// </returns>
		public override int GetHashCode()
		{
			return (this.Sampler?.GetHashCode() ?? 0) ^ unchecked((int)this.Unit);
		}

		/// <summary>
		/// Creates a deep copy of the <see cref="BindSamplerOp"/>
		/// </summary>
		/// <returns></returns>
		public BindSamplerOp Clone()
		{
			return new BindSamplerOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
