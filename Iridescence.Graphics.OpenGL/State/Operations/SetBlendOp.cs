﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the blend parameters.
	/// </summary>
	public sealed class SetBlendOp : StateOp, ICloneable
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets whether blending is enabled.
		/// </summary>
		public bool Enabled { get; set; }

		/// <summary>
		/// Gets or sets the source RGB blend factor.
		/// </summary>
		public BlendingFactorSrc Source { get; set; }

		/// <summary>
		/// Gets or sets the source alpha blend factor.
		/// </summary>
		public BlendingFactorSrc SourceAlpha { get; set; }

		/// <summary>
		/// Gets or sets the destination RGB blend factor.
		/// </summary>
		public BlendingFactorDest Destination { get; set; }

		/// <summary>
		/// Gets or sets the destination alpha blend factor.
		/// </summary>
		public BlendingFactorDest DestinationAlpha { get; set; }

		/// <summary>
		/// Gets or sets the RGB blend equation mode.
		/// </summary>
		public BlendEquationMode Equation { get; set; }

		/// <summary>
		/// Gets or sets the alpha blend equation mode.
		/// </summary>
		public BlendEquationMode EquationAlpha { get; set; }

		/// <summary>
		/// Gets or sets the constant blend color.
		/// </summary>
		public Color4 ConstantColor { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetBlendOp"/>.
		/// </summary>
		public SetBlendOp()
		{
			this.Enabled = false;
			this.Source = BlendingFactorSrc.One;
			this.SourceAlpha = BlendingFactorSrc.One;
			this.Destination = BlendingFactorDest.Zero;
			this.DestinationAlpha = BlendingFactorDest.Zero;
			this.Equation = BlendEquationMode.FuncAdd;
			this.EquationAlpha = BlendEquationMode.FuncAdd;
			this.ConstantColor = Color4.Transparency;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetBlendOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetBlendOp(SetBlendOp other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.BlendEnabled = this.Enabled;
			state.BlendSource = this.Source;
			state.BlendSourceAlpha = this.SourceAlpha;
			state.BlendDestination = this.Destination;
			state.BlendDestinationAlpha = this.DestinationAlpha;
			state.BlendEquation = this.Equation;
			state.BlendEquationAlpha = this.EquationAlpha;
			state.BlendConstantColor = this.ConstantColor;
		}

		/// <summary>
		/// Copies this <see cref="SetBlendOp"/> to the specified <see cref="SetBlendOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetBlendOp dest)
		{
			dest.Enabled = this.Enabled;
			dest.Source = this.Source;
			dest.SourceAlpha = this.SourceAlpha;
			dest.Destination = this.Destination;
			dest.DestinationAlpha = this.DestinationAlpha;
			dest.Equation = this.Equation;
			dest.EquationAlpha = this.EquationAlpha;
			dest.ConstantColor = this.ConstantColor;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetBlendOp"/> is equal to the current <see cref="SetBlendOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetBlendOp"/> to compare with the current <see cref="SetBlendOp"/>.</param>
		/// <returns>True if the specified <see cref="SetBlendOp"/> is equal to the current <see cref="SetBlendOp"/>; otherwise, false.</returns>
		public bool Equals(SetBlendOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Enabled == other.Enabled &&
				   this.Source == other.Source &&
				   this.SourceAlpha == other.SourceAlpha &&
				   this.Destination == other.Destination &&
				   this.DestinationAlpha == other.DestinationAlpha &&
				   this.Equation == other.Equation &&
				   this.EquationAlpha == other.EquationAlpha &&
				   this.ConstantColor.Equals(other.ConstantColor);
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object.</param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetBlendOp && this.Equals((SetBlendOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.Enabled.GetHashCode();
				hashCode = (hashCode * 397) ^ (int)this.Source;
				hashCode = (hashCode * 397) ^ (int)this.SourceAlpha;
				hashCode = (hashCode * 397) ^ (int)this.Destination;
				hashCode = (hashCode * 397) ^ (int)this.DestinationAlpha;
				hashCode = (hashCode * 397) ^ (int)this.Equation;
				hashCode = (hashCode * 397) ^ (int)this.EquationAlpha;
				hashCode = (hashCode * 397) ^ this.ConstantColor.GetHashCode();
				return hashCode;
			}
		}

		/// <summary>
		/// Creates a copy of the <see cref="SetBlendOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetBlendOp Clone()
		{
			return new SetBlendOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
