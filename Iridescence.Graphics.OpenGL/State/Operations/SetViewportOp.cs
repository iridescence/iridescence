﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the viewport.
	/// </summary>
	public sealed class SetViewportOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets the viewport.
		/// </summary>
		public RectangleI Viewport { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetViewportOp"/>.
		/// </summary>
		public SetViewportOp()
		{
			this.Viewport = new RectangleI();
		}

		/// <summary>
		/// Creates a new <see cref="SetViewportOp"/> with the specified viewport rectangle.
		/// </summary>
		/// <param name="viewport"></param>
		public SetViewportOp(RectangleI viewport)
		{
			this.Viewport = viewport;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetViewportOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetViewportOp(SetViewportOp other)
		{
			other.CopyTo(this);	
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.Viewport = this.Viewport;
		}

		/// <summary>
		/// Copies this <see cref="SetViewportOp"/> to the specified state.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetViewportOp dest)
		{
			dest.Viewport = this.Viewport;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetViewportOp" /> is equal to the current <see cref="SetViewportOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetViewportOp"/> to compare with the current <see cref="SetViewportOp"/>. </param>
		/// <returns>True if the specified <see cref="SetViewportOp"/> is equal to the current <see cref="SetViewportOp"/>; otherwise, false.</returns>
		public bool Equals(SetViewportOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Viewport.Equals(other.Viewport);
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetViewportOp && this.Equals((SetViewportOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>
		/// A hash code for the current object.
		/// </returns>
		public override int GetHashCode()
		{
			return this.Viewport.GetHashCode();
		}

		/// <summary>
		/// Creates a copy of the <see cref="SetViewportOp"/>
		/// </summary>
		/// <returns></returns>
		public SetViewportOp Clone()
		{
			return new SetViewportOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
