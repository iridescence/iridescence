﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the depth range and clamping parameters.
	/// </summary>
	public sealed class SetDepthRangeOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets the depth range near value.
		/// </summary>
		public float DepthNear { get; set; }

		/// <summary>
		/// Gets or sets the depth range far value.
		/// </summary>
		public float DepthFar { get; set; }

		/// <summary>
		/// Gets or sets a value that determines whether depth clamping is enabled.
		/// </summary>
		public bool DepthClamp { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetDepthRangeOp"/>
		/// </summary>
		public SetDepthRangeOp()
		{
			this.DepthNear = 0.0f;
			this.DepthFar = 1.0f;
			this.DepthClamp = false;
		}

		/// <summary>
		/// Creates a new <see cref="SetDepthRangeOp"/> with the specified parameters.
		/// </summary>
		/// <param name="depthNear"></param>
		/// <param name="depthFar"></param>
		/// <param name="depthClamp"></param>
		public SetDepthRangeOp(float depthNear, float depthFar, bool depthClamp)
		{
			this.DepthNear = depthNear;
			this.DepthFar = depthFar;
			this.DepthClamp = depthClamp;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetDepthRangeOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetDepthRangeOp(SetDepthRangeOp other)
		{
			other.CopyTo(this);	
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.DepthNear = this.DepthNear;
			state.DepthFar = this.DepthFar;
			state.DepthClamp = this.DepthClamp;
		}

		/// <summary>
		/// Copies this <see cref="SetDepthRangeOp"/> to the specified <see cref="SetDepthRangeOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetDepthRangeOp dest)
		{
			dest.DepthNear = this.DepthNear;
			dest.DepthFar = this.DepthFar;
			dest.DepthClamp = this.DepthClamp;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetDepthRangeOp"/> is equal to the current <see cref="SetDepthRangeOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetDepthRangeOp"/> to compare with the current <see cref="SetDepthRangeOp"/>. </param>
		/// <returns>True if the specified <see cref="SetDepthRangeOp"/> is equal to the current <see cref="SetDepthRangeOp"/>; otherwise, false.</returns>
		public bool Equals(SetDepthRangeOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.DepthNear.Equals(other.DepthNear) &&
			       this.DepthFar.Equals(other.DepthFar) &&
			       this.DepthClamp == other.DepthClamp;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetDepthRangeOp && this.Equals((SetDepthRangeOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>
		/// A hash code for the current object.
		/// </returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = (this.DepthNear.GetHashCode() * 397) ^ this.DepthFar.GetHashCode();
				if (this.DepthClamp)
					hashCode = -hashCode;
				return hashCode;
			}
		}

		/// <summary>
		/// Returns a copy of the <see cref="SetDepthRangeOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetDepthRangeOp Clone()
		{
			return new SetDepthRangeOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
