﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the culling parameters.
	/// </summary>
	public sealed class SetCullOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets whether face culling is enabled.
		/// </summary>
		public bool CullFace { get; set; }

		/// <summary>
		/// Gets or sets the face culling mode.
		/// </summary>
		public CullFaceMode CullFaceMode { get; set; }

		/// <summary>
		/// Gets or sets the front face.
		/// </summary>
		public FrontFaceDirection FrontFace { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetCullOp"/>.
		/// </summary>
		public SetCullOp()
		{
			this.CullFace = false;
			this.CullFaceMode = CullFaceMode.Back;
			this.FrontFace = FrontFaceDirection.Ccw;
		}

		/// <summary>
		/// Creates a new <see cref="SetCullOp"/> with the specified parameters.
		/// </summary>
		/// <param name="cullFace"></param>
		/// <param name="cullFaceMode"></param>
		/// <param name="frontFace"></param>
		public SetCullOp(bool cullFace, CullFaceMode cullFaceMode, FrontFaceDirection frontFace)
		{
			this.CullFace = cullFace;
			this.CullFaceMode = cullFaceMode;
			this.FrontFace = frontFace;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetCullOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetCullOp(SetCullOp other)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.CullFace = this.CullFace;
			state.CullFaceMode = this.CullFaceMode;
			state.FrontFace = this.FrontFace;
		}

		/// <summary>
		/// Copies this <see cref="SetCullOp"/> to the specified <see cref="SetCullOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetCullOp dest)
		{
			dest.CullFace = this.CullFace;
			dest.CullFaceMode = this.CullFaceMode;
			dest.FrontFace = this.FrontFace;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetCullOp"/> is equal to the current <see cref="SetCullOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetCullOp"/> to compare with the current <see cref="SetCullOp"/>.</param>
		/// <returns>True if the specified <see cref="SetCullOp"/> is equal to the current <see cref="SetCullOp"/>; otherwise, false.</returns>
		public bool Equals(SetCullOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.CullFace == other.CullFace &&
				   this.CullFaceMode == other.CullFaceMode &&
				   this.FrontFace == other.FrontFace;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetCullOp && this.Equals((SetCullOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>
		/// A hash code for the current object.
		/// </returns>
		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.CullFace.GetHashCode();
				hashCode = (hashCode * 397) ^ (int)this.CullFaceMode;
				hashCode = (hashCode * 397) ^ (int)this.FrontFace;
				return hashCode;
			}
		}

		/// <summary>
		/// Returns a copy of this <see cref="SetCullOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetCullOp Clone()
		{
			return new SetCullOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
