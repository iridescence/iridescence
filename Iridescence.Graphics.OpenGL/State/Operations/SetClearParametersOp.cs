﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a <see cref="StateOp"/> that sets the clear parameters.
	/// </summary>
	public sealed class SetClearParametersOp : StateOp, ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets or sets the clear color.
		/// </summary>
		public Color4 ClearColor { get; set; }

		/// <summary>
		/// Gets or set the depth clear value.
		/// </summary>
		public float ClearDepth { get; set; }

		/// <summary>
		/// Gets or sets the stencil clear value.
		/// </summary>
		public int ClearStencil { get; set; }
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SetClearParametersOp"/>
		/// </summary>
		public SetClearParametersOp()
		{
			this.ClearColor = Color4.Transparency;
			this.ClearDepth = 1.0f;
			this.ClearStencil = 0;
		}

		/// <summary>
		/// Creates a new <see cref="SetDepthRangeOp"/> with the specified parameters.
		/// </summary>
		/// <param name="clearColor"></param>
		/// <param name="clearDepth"></param>
		/// <param name="clearStencil"></param>
		public SetClearParametersOp(Color4 clearColor, float clearDepth, int clearStencil)
		{
			this.ClearColor=clearColor;
			this.ClearDepth = clearDepth;
			this.ClearStencil = clearStencil;
		}

		/// <summary>
		/// Creates a copy of the specified <see cref="SetClearParametersOp"/>.
		/// </summary>
		/// <param name="other"></param>
		public SetClearParametersOp(SetClearParametersOp other)
		{
			other.CopyTo(this);	
		}

		#endregion

		#region Methods

		public override void Apply(State state)
		{
			state.ClearColor = this.ClearColor;
			state.ClearDepth = this.ClearDepth;
			state.ClearStencil = this.ClearStencil;
		}

		/// <summary>
		/// Copies this <see cref="SetClearParametersOp"/> to the specified <see cref="SetClearParametersOp"/>.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(SetClearParametersOp dest)
		{
			dest.ClearColor = this.ClearColor;
			dest.ClearDepth = this.ClearDepth;
			dest.ClearStencil = this.ClearStencil;
		}

		/// <summary>
		/// Determines whether the specified <see cref="SetClearParametersOp"/> is equal to the current <see cref="SetClearParametersOp"/>.
		/// </summary>
		/// <param name="other">The <see cref="SetClearParametersOp"/> to compare with the current <see cref="SetClearParametersOp"/>. </param>
		/// <returns>True if the specified <see cref="SetClearParametersOp"/> is equal to the current <see cref="SetClearParametersOp"/>; otherwise, false.</returns>
		public bool Equals(SetClearParametersOp other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.ClearColor.Equals(other.ClearColor) &&
			       this.ClearDepth == other.ClearDepth &&
			       this.ClearStencil == other.ClearStencil;
		}

		/// <summary>
		/// Determines whether the specified object is equal to the current object.
		/// </summary>
		/// <param name="obj">The object to compare with the current object. </param>
		/// <returns>True if the specified object is equal to the current object; otherwise, false.</returns>
		public override bool Equals(object obj)
		{
			return obj is SetClearParametersOp && this.Equals((SetClearParametersOp)obj);
		}

		/// <summary>
		/// Serves as the default hash function. 
		/// </summary>
		/// <returns>A hash code for the current object.</returns>
		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = this.ClearColor.GetHashCode();
				hashCode = (hashCode * 397) ^ this.ClearDepth.GetHashCode();
				hashCode = (hashCode * 397) ^ this.ClearStencil;
				return hashCode;
			}
		}

		/// <summary>
		/// Returns a copy of the <see cref="SetDepthRangeOp"/>.
		/// </summary>
		/// <returns></returns>
		public SetClearParametersOp Clone()
		{
			return new SetClearParametersOp(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
