﻿using System.Collections;
using System.Collections.Generic;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a compound <see cref="StateOp"/> made up of multiple <see cref="StateOp">StateOps</see>.
	/// </summary>
	public class CompoundOp : StateOp, IList<StateOp>
	{
		#region Fields

		private readonly List<StateOp> parts;

		#endregion

		#region Properties

		public StateOp this[int index]
		{
			get => this.parts[index];
			set => this.parts[index] = value;
		}

		public int Count => this.parts.Count;

		public bool IsReadOnly => ((IList<StateOp>)this.parts).IsReadOnly;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="CompoundOp"/>.
		/// </summary>
		public CompoundOp(IEnumerable<StateOp> parts)
		{
			this.parts = new List<StateOp>(parts);
		}
		
		/// <summary>
		/// Creates a new <see cref="CompoundOp"/>.
		/// </summary>
		public CompoundOp(params StateOp[] parts)
		{
			this.parts = new List<StateOp>(parts);
		}
		
		#endregion

		#region Methods

		public override void Apply(State state)
		{
			foreach(StateOp part in this.parts)
				part.Apply(state);
		}

		public void Add(StateOp item)
		{
			this.parts.Add(item);
		}

		public void Clear()
		{
			this.parts.Clear();
		}

		public bool Contains(StateOp item)
		{
			return this.parts.Contains(item);
		}

		public void CopyTo(StateOp[] array, int arrayIndex)
		{
			this.parts.CopyTo(array, arrayIndex);
		}
		
		public int IndexOf(StateOp item)
		{
			return this.parts.IndexOf(item);
		}

		public void Insert(int index, StateOp item)
		{
			this.parts.Insert(index, item);
		}

		public bool Remove(StateOp item)
		{
			return this.parts.Remove(item);
		}

		public void RemoveAt(int index)
		{
			this.parts.RemoveAt(index);
		}
		
		public IEnumerator<StateOp> GetEnumerator()
		{
			return ((IList<StateOp>)this.parts).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IList<StateOp>)this.parts).GetEnumerator();
		}

		#endregion
	}
}
