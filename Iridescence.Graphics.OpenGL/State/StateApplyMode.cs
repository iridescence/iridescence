﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// State apply modes.
	/// </summary>
	public enum StateApplyMode
	{
		Invalid,
		Draw,
		Clear,
		Blit,
		Buffer,
	}
}
