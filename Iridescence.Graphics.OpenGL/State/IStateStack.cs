﻿namespace Iridescence.Graphics.OpenGL
{
	public interface IStateStack
	{
		StateStackItem With(StateOp part);
	}
}
