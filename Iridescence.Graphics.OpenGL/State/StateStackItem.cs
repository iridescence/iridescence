﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	public struct StateStackItem : IStateStack, IDisposable
	{
		#region Fields

		private readonly StateStack stack;
		private int lastIndex;
		private int count;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public StateStackItem(StateStack stack)
		{
			this.stack = stack;
			this.lastIndex = stack.Index;
			this.count = 0;
		}

		#endregion

		#region Methods

		public StateStackItem With(StateOp part)
		{
			if (part == null)
				throw new ArgumentNullException(nameof(part));

			if (this.count < 0)
				throw new InvalidOperationException("Stack item was already popped.");

			if (this.stack.Index != this.lastIndex)
				throw new InvalidOperationException("Stack was modified.");

			if (this.lastIndex >= this.stack.Stack.Length)
			{
				Array.Resize(ref this.stack.Stack, this.stack.Stack.Length + 4);
			}

			this.stack.Stack[this.stack.Index++] = part;
			this.lastIndex = this.stack.Index;
			++this.count;

			return this;
		}

		public void Dispose()
		{
			if (this.count < 0)
				return;

			if (this.stack.Index != this.lastIndex)
				throw new InvalidOperationException("Stack was modified.");

			this.stack.Index -= this.count;

			for (int i = 0; i < this.count; ++i)
			{
				this.stack.Stack[this.lastIndex - 1 - i] = null;
			}

			this.count = -1;
		}

		#endregion
	}
}
