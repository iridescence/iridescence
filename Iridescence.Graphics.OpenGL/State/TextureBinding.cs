﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Binds a texture to a texture target.
	/// </summary>
	[Serializable]
	public struct TextureBinding : IEquatable<TextureBinding>
	{
		#region Fields

		public readonly TextureTarget Target;
		public readonly uint Texture;

		public static readonly TextureBinding Null = new TextureBinding();

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TextureBinding"/> class. 
		/// </summary>
		public TextureBinding(TextureTarget target, uint texture)
		{
			this.Target = target;
			this.Texture = texture;
		}

		#endregion

		#region Methods

		public bool Equals(TextureBinding other)
		{
			return other.Target == this.Target && other.Texture == this.Texture;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is TextureBinding))
				return false;

			return this.Equals((TextureBinding)obj);
		}

		public override int GetHashCode()
		{
			return ((int)this.Target * 397) ^ (int)this.Texture;
		}

		public override string ToString()
		{
			return $"{this.Target} {this.Texture}";
		}

		#endregion
	}
}
