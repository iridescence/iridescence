﻿using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents the OpenGL state.
	/// </summary>
	public sealed class State
	{
		#region Fields

		public uint DrawFramebuffer = 0;
		public uint ReadFramebuffer = 0;
		public ColorWriteMask ColorMask = new ColorWriteMask(true, true, true, true);
		public bool DepthMask = true;
		public uint StencilFrontMask = ~0u;
		public uint StencilBackMask = ~0u;
		public Color4 ClearColor = Color4.Transparency;
		public float ClearDepth = 1.0f;
		public int ClearStencil = 0;

		public RectangleI Viewport = RectangleI.Zero;
		public float DepthNear = 0.0f;
		public float DepthFar = 1.0f;
		public bool DepthClamp = false;

		public float PointSize = 1.0f;
		public float LineWidth = 1.0f;
		public bool LineSmoothing = false;
		public bool PolygonSmoothing = false;
		public bool CullFace = false;
		public CullFaceMode CullFaceMode = CullFaceMode.Back;
		public FrontFaceDirection FrontFace = FrontFaceDirection.Ccw;

		public bool ScissorTest = false;
		public RectangleI Scissor = RectangleI.Zero;

		public bool StencilTest = false;
		public StencilFaceState StencilFront = StencilFaceState.Default;
		public StencilFaceState StencilBack = StencilFaceState.Default;
		public bool DepthTest = false;
		public DepthFunction DepthFunction = DepthFunction.Less;

		public bool BlendEnabled = false;
		public BlendingFactorSrc BlendSource = BlendingFactorSrc.One;
		public BlendingFactorSrc BlendSourceAlpha = BlendingFactorSrc.One;
		public BlendingFactorDest BlendDestination = BlendingFactorDest.Zero;
		public BlendingFactorDest BlendDestinationAlpha = BlendingFactorDest.Zero;
		public BlendEquationMode BlendEquation = BlendEquationMode.FuncAdd;
		public BlendEquationMode BlendEquationAlpha = BlendEquationMode.FuncAdd;
		public Color4 BlendConstantColor = Color4.Transparency;

		public bool MultisampleEnabled = true;
		public bool FramebufferSrgbEnabled = false;

		internal uint ActiveTexture = 0;
		public readonly TextureUnitState[] TextureUnits = new TextureUnitState[32];

		public uint VertexArray;
		public uint Program;
		public BufferBindings Buffers;

		#endregion

		#region Methods

		/// <summary>
		/// Copies this state to the other state.
		/// </summary>
		/// <param name="state"></param>
		public void CopyTo(State state)
		{
			state.DrawFramebuffer = this.DrawFramebuffer;
			state.ReadFramebuffer = this.ReadFramebuffer;
			state.ColorMask = this.ColorMask;
			state.DepthMask = this.DepthMask;
			state.StencilFrontMask = this.StencilFrontMask;
			state.StencilBackMask = this.StencilBackMask;
			state.ClearColor = this.ClearColor;
			state.ClearDepth = this.ClearDepth;
			state.ClearStencil = this.ClearStencil;

			state.Viewport = this.Viewport;
			state.DepthNear = this.DepthNear;
			state.DepthFar = this.DepthFar;
			state.DepthClamp = this.DepthClamp;

			state.PointSize = this.PointSize;
			state.LineWidth = this.LineWidth;
			state.LineSmoothing = this.LineSmoothing;
			state.PolygonSmoothing = this.PolygonSmoothing;
			state.CullFace = this.CullFace;
			state.CullFaceMode = this.CullFaceMode;
			state.FrontFace = this.FrontFace;

			state.ScissorTest = this.ScissorTest;
			state.Scissor = this.Scissor;

			state.StencilTest = this.StencilTest;
			state.StencilFront = this.StencilFront;
			state.StencilBack = this.StencilBack;
			state.DepthTest = this.DepthTest;
			state.DepthFunction = this.DepthFunction;

			state.BlendEnabled = this.BlendEnabled;
			state.BlendSource = this.BlendSource;
			state.BlendSourceAlpha = this.BlendSourceAlpha;
			state.BlendDestination = this.BlendDestination;
			state.BlendDestinationAlpha = this.BlendDestinationAlpha;
			state.BlendEquation = this.BlendEquation;
			state.BlendEquationAlpha = this.BlendEquationAlpha;
			state.BlendConstantColor = this.BlendConstantColor;

			state.MultisampleEnabled = this.MultisampleEnabled;
			state.FramebufferSrgbEnabled = this.FramebufferSrgbEnabled;
			
			state.ActiveTexture = this.ActiveTexture;
			for (int i = 0; i < state.TextureUnits.Length; ++i)
			{
				state.TextureUnits[i] = this.TextureUnits[i];
			}

			state.VertexArray = this.VertexArray;
			state.Program = this.Program;
			state.Buffers = this.Buffers;
		}

		private void setActiveTexture(GL gl, uint unit, bool invalid)
		{
			if (invalid || this.ActiveTexture != unit)
			{
				gl.ActiveTexture(TextureUnit.Texture0 + (int)unit);
				this.ActiveTexture = unit;
			}
		}

		public void Apply(GL gl, State currentState, StateApplyMode mode)
		{
			bool invalid = mode == StateApplyMode.Invalid;
			
			if (invalid ||
			    currentState.DrawFramebuffer != this.DrawFramebuffer ||
			    currentState.ReadFramebuffer != this.ReadFramebuffer)
			{
				if (this.DrawFramebuffer == this.ReadFramebuffer)
				{
					gl.BindFramebuffer(FramebufferTarget.Framebuffer, this.DrawFramebuffer);
				}
				else
				{
					if (invalid || 
					    currentState.DrawFramebuffer != this.DrawFramebuffer)
						gl.BindFramebuffer(FramebufferTarget.DrawFramebuffer, this.DrawFramebuffer);

					if (invalid || 
					    currentState.ReadFramebuffer != this.ReadFramebuffer)
						gl.BindFramebuffer(FramebufferTarget.ReadFramebuffer, this.ReadFramebuffer);
				}
				
				currentState.DrawFramebuffer = this.DrawFramebuffer;
				currentState.ReadFramebuffer = this.ReadFramebuffer;
			}

			if (invalid || 
			    currentState.FramebufferSrgbEnabled != this.FramebufferSrgbEnabled)
			{
				gl.SetEnabled(EnableCap.FramebufferSrgb, this.FramebufferSrgbEnabled);
				currentState.FramebufferSrgbEnabled = this.FramebufferSrgbEnabled;
			}

			if (invalid ||
			    currentState.ScissorTest != this.ScissorTest)
			{
				gl.SetEnabled(EnableCap.ScissorTest, this.ScissorTest);
				currentState.ScissorTest = this.ScissorTest;
			}

			if (invalid ||
			    !RectangleI.Equals(in currentState.Scissor, in this.Scissor))
			{
				gl.Scissor(this.Scissor.X, this.Scissor.Y, this.Scissor.Width, this.Scissor.Height);
				currentState.Scissor = this.Scissor;
			}


			if (invalid || mode == StateApplyMode.Draw || mode == StateApplyMode.Clear)
			{
				if (invalid || 
				    !currentState.ColorMask.Equals(this.ColorMask))
				{
					gl.ColorMask(this.ColorMask.R, this.ColorMask.G, this.ColorMask.B, this.ColorMask.A);
					currentState.ColorMask = this.ColorMask;
				}

				if (invalid || 
				    currentState.DepthMask != this.DepthMask)
				{
					gl.DepthMask(this.DepthMask);
					currentState.DepthMask = this.DepthMask;
				}
			
				if (invalid || 
				    currentState.StencilFrontMask != this.StencilFrontMask ||
				    currentState.StencilBackMask != this.StencilBackMask)
				{
					if (this.StencilFrontMask == this.StencilBackMask)
					{
						gl.StencilMask(this.StencilFrontMask);
					}
					else
					{
						gl.StencilMaskSeparate(StencilFace.Front, this.StencilFrontMask);
						gl.StencilMaskSeparate(StencilFace.Back, this.StencilBackMask);
					}

					currentState.StencilFrontMask = this.StencilFrontMask;
					currentState.StencilBackMask = this.StencilBackMask;
				}
			}

			if (invalid || mode == StateApplyMode.Clear)
			{
				if (invalid ||
				    !currentState.ClearColor.Equals(this.ClearColor))
				{
					gl.ClearColor(this.ClearColor.R, this.ClearColor.G, this.ClearColor.B, this.ClearColor.A);
					currentState.ClearColor = this.ClearColor;
				}

				if (invalid || 
				    currentState.ClearDepth != this.ClearDepth)
				{
					gl.ClearDepth(this.ClearDepth);
					currentState.ClearDepth = this.ClearDepth;
				}
				
				if (invalid || 
				      currentState.ClearStencil != this.ClearStencil)
				{
					gl.ClearStencil(this.ClearStencil);
					currentState.ClearStencil = this.ClearStencil;
				}				
			}

			if (invalid || mode == StateApplyMode.Draw)
			{
				if (invalid ||
				    !RectangleI.Equals(in currentState.Viewport, in this.Viewport))
				{
					gl.Viewport(this.Viewport.X, this.Viewport.Y, this.Viewport.Width, this.Viewport.Height);
					currentState.Viewport = this.Viewport;
				}

				if (invalid ||
				    currentState.DepthNear != this.DepthNear || 
				    currentState.DepthFar != this.DepthFar)
				{
					gl.DepthRange(this.DepthNear, this.DepthFar);
					currentState.DepthNear = this.DepthNear;
					currentState.DepthFar = this.DepthFar;
				}

				if (invalid || 
				    currentState.DepthClamp != this.DepthClamp)
				{
					gl.SetEnabled(EnableCap.DepthClamp, this.DepthClamp);
					currentState.DepthClamp = this.DepthClamp;
				}

				if (invalid ||
				    currentState.PointSize != this.PointSize)
				{
					gl.PointSize(this.PointSize);
					currentState.PointSize = this.PointSize;
				}

				if (invalid ||
				    currentState.LineWidth != this.LineWidth)
				{
					gl.LineWidth(this.LineWidth);
					currentState.LineWidth = this.LineWidth;
				}

				if (invalid ||
				    currentState.LineSmoothing != this.LineSmoothing)
				{
					gl.SetEnabled(EnableCap.LineSmooth, this.LineSmoothing);
					currentState.LineSmoothing = this.LineSmoothing;
				}

				if (invalid ||
				    currentState.PolygonSmoothing != this.PolygonSmoothing)
				{
					gl.SetEnabled(EnableCap.PolygonSmooth, this.PolygonSmoothing);
					currentState.PolygonSmoothing = this.PolygonSmoothing;
				}

				if (invalid || 
				    currentState.CullFace != this.CullFace)
				{
					gl.SetEnabled(EnableCap.CullFace, this.CullFace);
					currentState.CullFace = this.CullFace;
				}

				if (invalid || 
				    currentState.CullFaceMode != this.CullFaceMode)
				{
					gl.CullFace(this.CullFaceMode);
					currentState.CullFaceMode = this.CullFaceMode;
				}

				if (invalid ||
				    currentState.StencilTest != this.StencilTest)
				{
					gl.SetEnabled(EnableCap.StencilTest, this.StencilTest);
					currentState.StencilTest = this.StencilTest;
				}

				if (invalid || 
				    !StencilFaceState.Equals(currentState.StencilFront, this.StencilFront) ||
				    !StencilFaceState.Equals(currentState.StencilBack, this.StencilBack))
				{
					if (StencilFaceState.Equals(this.StencilFront, this.StencilBack))
					{
						if (invalid ||
						    !StencilFaceState.OpEquals(currentState.StencilFront, this.StencilFront))
							gl.StencilOp(this.StencilFront.Fail, this.StencilFront.DepthFail, this.StencilFront.DepthPass);
						if (invalid || 
						    !StencilFaceState.FuncEquals(currentState.StencilFront, this.StencilFront))
							gl.StencilFunc(this.StencilFront.Function, this.StencilFront.Reference, this.StencilFront.Mask);
					}
					else
					{
						if (invalid ||
						    !StencilFaceState.OpEquals(currentState.StencilFront, this.StencilFront))
							gl.StencilOpSeparate(StencilFace.Front, this.StencilFront.Fail, this.StencilFront.DepthFail, this.StencilFront.DepthPass);
						if (invalid || 
						    !StencilFaceState.FuncEquals(currentState.StencilFront, this.StencilFront))
							gl.StencilFuncSeparate(StencilFace.Front, this.StencilFront.Function, this.StencilFront.Reference, this.StencilFront.Mask);

						if (invalid || 
						    !StencilFaceState.OpEquals(currentState.StencilBack, this.StencilBack))
							gl.StencilOpSeparate(StencilFace.Back, this.StencilBack.Fail, this.StencilBack.DepthFail, this.StencilBack.DepthPass);
						if (invalid ||
						    !StencilFaceState.FuncEquals(currentState.StencilBack, this.StencilBack))
							gl.StencilFuncSeparate(StencilFace.Back, this.StencilBack.Function, this.StencilBack.Reference, this.StencilBack.Mask);
					}

					currentState.StencilFront = this.StencilFront;
					currentState.StencilBack = this.StencilBack;
				}

				if (invalid || 
				    currentState.DepthTest != this.DepthTest)
				{
					gl.SetEnabled(EnableCap.DepthTest, this.DepthTest);
					currentState.DepthTest = this.DepthTest;
				}

				if (invalid || 
				    currentState.DepthFunction != this.DepthFunction)
				{
					gl.DepthFunc(this.DepthFunction);
					currentState.DepthFunction = this.DepthFunction;
				}

				if (invalid || 
				    currentState.BlendEnabled != this.BlendEnabled)
				{
					gl.SetEnabled(EnableCap.Blend, this.BlendEnabled);
					currentState.BlendEnabled = this.BlendEnabled;
				}

				if (invalid || 
				    currentState.BlendSource != this.BlendSource ||
				    currentState.BlendSourceAlpha != this.BlendSourceAlpha ||
				    currentState.BlendDestination != this.BlendDestination ||
				    currentState.BlendDestinationAlpha != this.BlendDestinationAlpha)
				{
					if (this.BlendSource == this.BlendSourceAlpha && this.BlendDestination == this.BlendDestinationAlpha)
						gl.BlendFunc(this.BlendSource, this.BlendDestination);
					else
						gl.BlendFuncSeparate(this.BlendSource, this.BlendDestination, this.BlendSourceAlpha, this.BlendDestinationAlpha);

					currentState.BlendSource = this.BlendSource;
					currentState.BlendSourceAlpha = this.BlendSourceAlpha;
					currentState.BlendDestination = this.BlendDestination;
					currentState.BlendDestinationAlpha = this.BlendDestinationAlpha;
				}

				if (invalid || 
				    currentState.BlendEquation != this.BlendEquation || 
				    currentState.BlendEquationAlpha != this.BlendEquationAlpha)
				{
					if (this.BlendEquation == this.BlendEquationAlpha)
						gl.BlendEquation(this.BlendEquation);
					else
						gl.BlendEquationSeparate(this.BlendEquation, this.BlendEquationAlpha);

					currentState.BlendEquation = this.BlendEquation;
					currentState.BlendEquationAlpha = this.BlendEquationAlpha;
				}

				if (invalid || 
				    currentState.BlendConstantColor != this.BlendConstantColor)
				{
					gl.BlendColor(this.BlendConstantColor.R, this.BlendConstantColor.G, this.BlendConstantColor.B, this.BlendConstantColor.A);
					currentState.BlendConstantColor = this.BlendConstantColor;
				}

				if (invalid || 
				    currentState.MultisampleEnabled != this.MultisampleEnabled)
				{
					gl.SetEnabled(EnableCap.Multisample, this.MultisampleEnabled);
					currentState.MultisampleEnabled = this.MultisampleEnabled;
				}

				this.ActiveTexture = currentState.ActiveTexture;

				for (uint i = 0; i < this.TextureUnits.Length; ++i)
				{
					if (invalid || 
					    !currentState.TextureUnits[i].Texture.Equals(this.TextureUnits[i].Texture))
					{
						this.setActiveTexture(gl, i, invalid);

						if (currentState.TextureUnits[i].Target != 0 && currentState.TextureUnits[i].Target != this.TextureUnits[i].Target)
						{
							gl.BindTexture(currentState.TextureUnits[i].Target, 0);
						}

						if (this.TextureUnits[i].Target != 0)
						{
							gl.BindTexture(this.TextureUnits[i].Target, this.TextureUnits[i].Texture);
						}

						currentState.TextureUnits[i].Target = this.TextureUnits[i].Target;
						currentState.TextureUnits[i].Texture = this.TextureUnits[i].Texture;
					}

					if (invalid || currentState.TextureUnits[i].Sampler != this.TextureUnits[i].Sampler)
					{
						this.setActiveTexture(gl, i, invalid);
						gl.BindSampler(i, this.TextureUnits[i].Sampler);

						currentState.TextureUnits[i].Sampler = this.TextureUnits[i].Sampler;
					}
				}

				currentState.ActiveTexture = this.ActiveTexture;

				if (invalid || 
				    currentState.VertexArray != this.VertexArray)
				{
					gl.BindVertexArray(this.VertexArray);
					currentState.VertexArray = this.VertexArray;
				}

				if (invalid || 
				    currentState.Program != this.Program)
				{
					gl.UseProgram(this.Program);
					currentState.Program = this.Program;
				}
			}

			if (invalid || mode == StateApplyMode.Buffer)
			{
				foreach (BufferTarget target in BufferBindings.Targets)
				{
					uint current = currentState.Buffers[target];
					uint mine = this.Buffers[target];

					if (current != mine)
					{
						gl.BindBuffer(target, mine);
						currentState.Buffers[target] = mine;
					}
				}
			}
		}

		#endregion
	}
}
