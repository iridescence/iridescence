﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Defines the signature of a debug callback for 
	/// <see cref="GL.DebugMessageCallback"/>.
	/// </summary>
	/// <param name="source">The <see cref="DebugSource"/> for this debug message.</param>
	/// <param name="type">The <see cref="DebugType"/> for this debug message.</param>
	/// <param name="id">The id of this debug message.</param>
	/// <param name="severity">The <see cref="DebugSeverity"/> for this debug message.</param>
	/// <param name="length">The length of this debug message.</param>
	/// <param name="message">A pointer to a null-terminated ASCII C string, representing the content of this debug message.</param>
	/// <param name="userParam">A pointer to a user-specified parameter.</param>
	[UnmanagedFunctionPointer(CallingConvention.Winapi)]
	public delegate void DebugProc(DebugSource source, DebugType type, int id, DebugSeverity severity, int length, IntPtr message, IntPtr userParam);
}