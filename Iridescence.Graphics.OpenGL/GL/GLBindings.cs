﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Iridescence.Emit;

namespace Iridescence.Graphics.OpenGL
{
	public partial class GLBindings
	{
		private GLBindings()
		{

		}
		
		/// <summary>
		/// Loads all OpenGL procedures using the specified <see cref="GetProcAddress"/> implementation.
		/// </summary>
		/// <param name="getProcAddress"></param>
		/// <returns></returns>
		public static GLBindings Load(GetProcAddress getProcAddress)
		{
			if (getProcAddress == null)
				throw new ArgumentNullException(nameof(getProcAddress));

			GLBindings bindings = new GLBindings();

			Type type = typeof(GLBindings);
			FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public);

			foreach (FieldInfo field in fields)
			{
				Type delegateType = field.FieldType;
				Delegate value;

				IntPtr address = getProcAddress(field.Name);

				// apparently some bad drivers return 1 or 2.
				if (address == IntPtr.Zero || address == (IntPtr)1 || address == (IntPtr)2)
				{
					value = null;
				}
				else
				{
					value = Marshal.GetDelegateForFunctionPointer(address, delegateType);
				}

				field.SetValue(bindings, value);
			}

			return bindings;
		}

		/// <summary>
		/// Creates a debug wrapper for the specified <see cref="GLBindings"/>.
		/// </summary>
		/// <param name="inner"></param>
		/// <returns></returns>
		public static GLBindings WrapDebug(GLBindings inner)
		{
			Type type = typeof(GLBindings);
			FieldInfo[] fields = type.GetFields(BindingFlags.Instance | BindingFlags.Public);

			GLBindings debug = new GLBindings();

			foreach (FieldInfo field in fields)
			{
				Delegate value = field.GetValue(inner) as Delegate;
				if (value == null)
					continue;

				if (!ReferenceEquals(value, inner.glGetError))
					value = generateDebugWrapper(inner, field.Name, value);
				
				field.SetValue(debug, value);
			}

			return debug;
		}

		/// <summary>
		/// Generates a wrapper that calls the specified method and checks glGetError afterwards.
		/// </summary>
		private static Delegate generateDebugWrapper(GLBindings bindings, string name, Delegate d)
		{
			if (d == null)
				throw new ArgumentNullException(nameof(d));

			if (d.Target != null)
				throw new ArgumentException("Method must be static.");

			Type thisType = typeof(DebugWrapper<>).MakeGenericType(d.GetType());
			FieldInfo delegateField = thisType.GetField("Delegate", BindingFlags.Instance | BindingFlags.Public);
			MethodInfo checkErrorMethod = thisType.GetMethod("CheckError", BindingFlags.Instance | BindingFlags.Public);
			
			MethodInfo method = d.Method;
			ParameterInfo[] parameters = method.GetParameters();
			MethodSignature signature = new MethodSignature(method.ReturnType, method.GetParameters().Select(p => p.ParameterType).Prepend(thisType));
			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(signature, "d_" + name);

			// Load delegate.
			emit.LoadArgument(0);
			emit.LoadField(delegateField);
			
			for (int i = 0; i < parameters.Length; ++i)
			{
				emit.LoadArgument((ushort)(i + 1));
			}

			// Call method.
			emit.CallVirtual(method);
			
			// Call CheckError().
			emit.LoadArgument(0);
			emit.Call(checkErrorMethod);

			emit.Return();

			object instance = Activator.CreateInstance(thisType, bindings, d);
			return emit.CreateDelegate(d.GetType(), instance);
		}
		
		public sealed class DebugWrapper<T>
		{
			public readonly GLBindings Owner;
			public readonly T Delegate;

			public DebugWrapper(GLBindings owner, T @delegate)
			{
				this.Owner = owner;
				this.Delegate = @delegate;
			}

			private static void throwGLError(ErrorCode error)
			{
				throw new OpenGLException(error);
			}
			
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public void CheckError()
			{
				ErrorCode error = (ErrorCode)this.Owner.glGetError();
				if (error != ErrorCode.NoError)
					throwGLError(error);
			}
		}
	}
}
