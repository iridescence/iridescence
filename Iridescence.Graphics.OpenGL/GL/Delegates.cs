﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL.Delegates
{
	public delegate void glBlendEquationiARB(UInt32 buf, Int32 mode);

	public delegate void glBlendEquationSeparateiARB(UInt32 buf, Int32 modeRGB, Int32 modeAlpha);

	public delegate void glBlendFunciARB(UInt32 buf, Int32 src, Int32 dst);

	public delegate void glBlendFuncSeparateiARB(UInt32 buf, Int32 srcRGB, Int32 dstRGB, Int32 srcAlpha, Int32 dstAlpha);

	public delegate void glBufferPageCommitmentARB(Int32 target, IntPtr offset, Int32 size, bool commit);

	public unsafe delegate void glCompileShaderIncludeARB(UInt32 shader, Int32 count, IntPtr path, Int32* length);

	public unsafe delegate IntPtr glCreateSyncFromCLeventARB([Out] IntPtr* context, [OutAttribute] IntPtr* @event, UInt32 flags);

	public delegate void glDebugMessageCallbackARB(DebugProc callback, IntPtr userParam);

	public unsafe delegate void glDebugMessageControlARB(Int32 source, Int32 type, Int32 severity, Int32 count, UInt32* ids, bool enabled);

	public delegate void glDebugMessageInsertARB(Int32 source, Int32 type, UInt32 id, Int32 severity, Int32 length, IntPtr buf);

	public delegate void glDeleteNamedStringARB(Int32 namelen, IntPtr name);

	public delegate void glDispatchComputeGroupSizeARB(UInt32 num_groups_x, UInt32 num_groups_y, UInt32 num_groups_z, UInt32 group_size_x, UInt32 group_size_y, UInt32 group_size_z);

	public unsafe delegate Int32 glGetDebugMessageLogARB(UInt32 count, Int32 bufSize, [OutAttribute] Int32* sources, [OutAttribute] Int32* types, [OutAttribute] UInt32* ids, [OutAttribute] Int32* severities, [OutAttribute] Int32* lengths, [OutAttribute] IntPtr messageLog);

	public delegate Int32 glGetGraphicsResetStatusARB();

	public delegate Int64 glGetImageHandleARB(UInt32 texture, Int32 level, bool layered, Int32 layer, Int32 format);

	public unsafe delegate void glGetNamedStringARB(Int32 namelen, IntPtr name, Int32 bufSize, [OutAttribute] Int32* stringlen, [OutAttribute] IntPtr @string);

	public unsafe delegate void glGetNamedStringivARB(Int32 namelen, IntPtr name, Int32 pname, [OutAttribute] Int32* @params);

	public delegate void glGetnColorTableARB(Int32 target, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr table);

	public delegate void glGetnCompressedTexImageARB(Int32 target, Int32 lod, Int32 bufSize, [OutAttribute] IntPtr img);

	public delegate void glGetnConvolutionFilterARB(Int32 target, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr image);

	public delegate void glGetnHistogramARB(Int32 target, bool reset, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr values);

	public unsafe delegate void glGetnMapdvARB(Int32 target, Int32 query, Int32 bufSize, [OutAttribute] Double* v);

	public unsafe delegate void glGetnMapfvARB(Int32 target, Int32 query, Int32 bufSize, [OutAttribute] Single* v);

	public unsafe delegate void glGetnMapivARB(Int32 target, Int32 query, Int32 bufSize, [OutAttribute] Int32* v);

	public delegate void glGetnMinmaxARB(Int32 target, bool reset, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr values);

	public unsafe delegate void glGetnPixelMapfvARB(Int32 map, Int32 bufSize, [OutAttribute] Single* values);

	public unsafe delegate void glGetnPixelMapuivARB(Int32 map, Int32 bufSize, [OutAttribute] UInt32* values);

	public unsafe delegate void glGetnPixelMapusvARB(Int32 map, Int32 bufSize, [OutAttribute] UInt16* values);

	public unsafe delegate void glGetnPolygonStippleARB(Int32 bufSize, [OutAttribute] Byte* pattern);

	public delegate void glGetnSeparableFilterARB(Int32 target, Int32 format, Int32 type, Int32 rowBufSize, [OutAttribute] IntPtr row, Int32 columnBufSize, [OutAttribute] IntPtr column, [OutAttribute] IntPtr span);

	public delegate void glGetnTexImageARB(Int32 target, Int32 level, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr img);

	public unsafe delegate void glGetnUniformdvARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Double* @params);

	public unsafe delegate void glGetnUniformfvARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Single* @params);

	public unsafe delegate void glGetnUniformivARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetnUniformuivARB(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] UInt32* @params);

	public delegate Int64 glGetTextureHandleARB(UInt32 texture);

	public delegate Int64 glGetTextureSamplerHandleARB(UInt32 texture, UInt32 sampler);

	public unsafe delegate void glGetVertexAttribLui64vARB(UInt32 index, Int32 pname, [OutAttribute] UInt64* @params);

	public delegate byte glIsImageHandleResidentARB(UInt64 handle);

	public delegate byte glIsNamedStringARB(Int32 namelen, IntPtr name);

	public delegate byte glIsTextureHandleResidentARB(UInt64 handle);

	public delegate void glMakeImageHandleNonResidentARB(UInt64 handle);

	public delegate void glMakeImageHandleResidentARB(UInt64 handle, Int32 access);

	public delegate void glMakeTextureHandleNonResidentARB(UInt64 handle);

	public delegate void glMakeTextureHandleResidentARB(UInt64 handle);

	public delegate void glMinSampleShadingARB(Single value);

	public delegate void glMultiDrawArraysIndirectCountARB(Int32 mode, IntPtr indirect, IntPtr drawcount, Int32 maxdrawcount, Int32 stride);

	public delegate void glMultiDrawElementsIndirectCountARB(Int32 mode, Int32 type, IntPtr indirect, IntPtr drawcount, Int32 maxdrawcount, Int32 stride);

	public delegate void glNamedBufferPageCommitmentARB(UInt32 buffer, IntPtr offset, Int32 size, bool commit);

	public delegate void glNamedStringARB(Int32 type, Int32 namelen, IntPtr name, Int32 stringlen, IntPtr @string);

	public delegate void glProgramUniformHandleui64ARB(UInt32 program, Int32 location, UInt64 value);

	public unsafe delegate void glProgramUniformHandleui64vARB(UInt32 program, Int32 location, Int32 count, UInt64* values);

	public delegate void glReadnPixelsARB(Int32 x, Int32 y, Int32 width, Int32 height, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr data);

	public delegate void glTexPageCommitmentARB(Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, bool resident);

	public delegate void glUniformHandleui64ARB(Int32 location, UInt64 value);

	public unsafe delegate void glUniformHandleui64vARB(Int32 location, Int32 count, UInt64* value);

	public delegate void glVertexAttribL1ui64ARB(UInt32 index, UInt64 x);

	public unsafe delegate void glVertexAttribL1ui64vARB(UInt32 index, UInt64* v);

	public delegate void glActiveShaderProgram(UInt32 pipeline, UInt32 program);

	public delegate void glActiveTexture(Int32 texture);

	public delegate void glAttachShader(UInt32 program, UInt32 shader);

	public delegate void glBeginConditionalRender(UInt32 id, Int32 mode);

	public delegate void glBeginQuery(Int32 target, UInt32 id);

	public delegate void glBeginQueryIndexed(Int32 target, UInt32 index, UInt32 id);

	public delegate void glBeginTransformFeedback(Int32 primitiveMode);

	public delegate void glBindAttribLocation(UInt32 program, UInt32 index, IntPtr name);

	public delegate void glBindBuffer(Int32 target, UInt32 buffer);

	public delegate void glBindBufferBase(Int32 target, UInt32 index, UInt32 buffer);

	public delegate void glBindBufferRange(Int32 target, UInt32 index, UInt32 buffer, IntPtr offset, IntPtr size);

	public unsafe delegate void glBindBuffersBase(Int32 target, UInt32 first, Int32 count, UInt32* buffers);

	public unsafe delegate void glBindBuffersRange(Int32 target, UInt32 first, Int32 count, UInt32* buffers, IntPtr* offsets, IntPtr* sizes);

	public delegate void glBindFragDataLocation(UInt32 program, UInt32 color, IntPtr name);

	public delegate void glBindFragDataLocationIndexed(UInt32 program, UInt32 colorNumber, UInt32 index, IntPtr name);

	public delegate void glBindFramebuffer(Int32 target, UInt32 framebuffer);

	public delegate void glBindImageTexture(UInt32 unit, UInt32 texture, Int32 level, bool layered, Int32 layer, Int32 access, Int32 format);

	public unsafe delegate void glBindImageTextures(UInt32 first, Int32 count, UInt32* textures);

	public delegate void glBindProgramPipeline(UInt32 pipeline);

	public delegate void glBindRenderbuffer(Int32 target, UInt32 renderbuffer);

	public delegate void glBindSampler(UInt32 unit, UInt32 sampler);

	public unsafe delegate void glBindSamplers(UInt32 first, Int32 count, UInt32* samplers);

	public delegate void glBindTexture(Int32 target, UInt32 texture);

	public unsafe delegate void glBindTextures(UInt32 first, Int32 count, UInt32* textures);

	public delegate void glBindTextureUnit(UInt32 unit, UInt32 texture);

	public delegate void glBindTransformFeedback(Int32 target, UInt32 id);

	public delegate void glBindVertexArray(UInt32 array);

	public delegate void glBindVertexBuffer(UInt32 bindingindex, UInt32 buffer, IntPtr offset, Int32 stride);

	public unsafe delegate void glBindVertexBuffers(UInt32 first, Int32 count, UInt32* buffers, IntPtr* offsets, Int32* strides);

	public delegate void glBlendColor(Single red, Single green, Single blue, Single alpha);

	public delegate void glBlendEquation(Int32 mode);

	public delegate void glBlendEquationi(UInt32 buf, Int32 mode);

	public delegate void glBlendEquationSeparate(Int32 modeRGB, Int32 modeAlpha);

	public delegate void glBlendEquationSeparatei(UInt32 buf, Int32 modeRGB, Int32 modeAlpha);

	public delegate void glBlendFunc(Int32 sfactor, Int32 dfactor);

	public delegate void glBlendFunci(UInt32 buf, Int32 src, Int32 dst);

	public delegate void glBlendFuncSeparate(Int32 sfactorRGB, Int32 dfactorRGB, Int32 sfactorAlpha, Int32 dfactorAlpha);

	public delegate void glBlendFuncSeparatei(UInt32 buf, Int32 srcRGB, Int32 dstRGB, Int32 srcAlpha, Int32 dstAlpha);

	public delegate void glBlitFramebuffer(Int32 srcX0, Int32 srcY0, Int32 srcX1, Int32 srcY1, Int32 dstX0, Int32 dstY0, Int32 dstX1, Int32 dstY1, Int32 mask, Int32 filter);

	public delegate void glBlitNamedFramebuffer(UInt32 readFramebuffer, UInt32 drawFramebuffer, Int32 srcX0, Int32 srcY0, Int32 srcX1, Int32 srcY1, Int32 dstX0, Int32 dstY0, Int32 dstX1, Int32 dstY1, Int32 mask, Int32 filter);

	public delegate void glBufferData(Int32 target, IntPtr size, IntPtr data, Int32 usage);

	public delegate void glBufferStorage(Int32 target, IntPtr size, IntPtr data, Int32 flags);

	public delegate void glBufferSubData(Int32 target, IntPtr offset, IntPtr size, IntPtr data);

	public delegate Int32 glCheckFramebufferStatus(Int32 target);

	public delegate Int32 glCheckNamedFramebufferStatus(UInt32 framebuffer, Int32 target);

	public delegate void glClampColor(Int32 target, Int32 clamp);

	public delegate void glClear(Int32 mask);

	public delegate void glClearBufferData(Int32 target, Int32 internalformat, Int32 format, Int32 type, IntPtr data);

	public delegate void glClearBufferfi(Int32 buffer, Int32 drawbuffer, Single depth, Int32 stencil);

	public unsafe delegate void glClearBufferfv(Int32 buffer, Int32 drawbuffer, Single* value);

	public unsafe delegate void glClearBufferiv(Int32 buffer, Int32 drawbuffer, Int32* value);

	public delegate void glClearBufferSubData(Int32 target, Int32 internalformat, IntPtr offset, IntPtr size, Int32 format, Int32 type, IntPtr data);

	public unsafe delegate void glClearBufferuiv(Int32 buffer, Int32 drawbuffer, UInt32* value);

	public delegate void glClearColor(Single red, Single green, Single blue, Single alpha);

	public delegate void glClearDepth(Double depth);

	public delegate void glClearDepthf(Single d);

	public delegate void glClearNamedBufferData(UInt32 buffer, Int32 internalformat, Int32 format, Int32 type, IntPtr data);

	public delegate void glClearNamedBufferSubData(UInt32 buffer, Int32 internalformat, IntPtr offset, Int32 size, Int32 format, Int32 type, IntPtr data);

	public delegate void glClearNamedFramebufferfi(UInt32 framebuffer, Int32 buffer, Single depth, Int32 stencil);

	public unsafe delegate void glClearNamedFramebufferfv(UInt32 framebuffer, Int32 buffer, Int32 drawbuffer, Single* value);

	public unsafe delegate void glClearNamedFramebufferiv(UInt32 framebuffer, Int32 buffer, Int32 drawbuffer, Int32* value);

	public unsafe delegate void glClearNamedFramebufferuiv(UInt32 framebuffer, Int32 buffer, Int32 drawbuffer, UInt32* value);

	public delegate void glClearStencil(Int32 s);

	public delegate void glClearTexImage(UInt32 texture, Int32 level, Int32 format, Int32 type, IntPtr data);

	public delegate void glClearTexSubImage(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, Int32 type, IntPtr data);

	public delegate Int32 glClientWaitSync(IntPtr sync, Int32 flags, UInt64 timeout);

	public delegate void glClipControl(Int32 origin, Int32 depth);

	public delegate void glColorMask(bool red, bool green, bool blue, bool alpha);

	public delegate void glColorMaski(UInt32 index, bool r, bool g, bool b, bool a);

	public delegate void glColorP3ui(Int32 type, UInt32 color);

	public unsafe delegate void glColorP3uiv(Int32 type, UInt32* color);

	public delegate void glColorP4ui(Int32 type, UInt32 color);

	public unsafe delegate void glColorP4uiv(Int32 type, UInt32* color);

	public delegate void glColorSubTable(Int32 target, Int32 start, Int32 count, Int32 format, Int32 type, IntPtr data);

	public delegate void glColorTable(Int32 target, Int32 internalformat, Int32 width, Int32 format, Int32 type, IntPtr table);

	public unsafe delegate void glColorTableParameterfv(Int32 target, Int32 pname, Single* @params);

	public unsafe delegate void glColorTableParameteriv(Int32 target, Int32 pname, Int32* @params);

	public delegate void glCompileShader(UInt32 shader);

	public delegate void glCompressedTexImage1D(Int32 target, Int32 level, Int32 internalformat, Int32 width, Int32 border, Int32 imageSize, IntPtr data);

	public delegate void glCompressedTexImage2D(Int32 target, Int32 level, Int32 internalformat, Int32 width, Int32 height, Int32 border, Int32 imageSize, IntPtr data);

	public delegate void glCompressedTexImage3D(Int32 target, Int32 level, Int32 internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, Int32 imageSize, IntPtr data);

	public delegate void glCompressedTexSubImage1D(Int32 target, Int32 level, Int32 xoffset, Int32 width, Int32 format, Int32 imageSize, IntPtr data);

	public delegate void glCompressedTexSubImage2D(Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, Int32 format, Int32 imageSize, IntPtr data);

	public delegate void glCompressedTexSubImage3D(Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, Int32 imageSize, IntPtr data);

	public delegate void glCompressedTextureSubImage1D(UInt32 texture, Int32 level, Int32 xoffset, Int32 width, Int32 format, Int32 imageSize, IntPtr data);

	public delegate void glCompressedTextureSubImage2D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, Int32 format, Int32 imageSize, IntPtr data);

	public delegate void glCompressedTextureSubImage3D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, Int32 imageSize, IntPtr data);

	public delegate void glConvolutionFilter1D(Int32 target, Int32 internalformat, Int32 width, Int32 format, Int32 type, IntPtr image);

	public delegate void glConvolutionFilter2D(Int32 target, Int32 internalformat, Int32 width, Int32 height, Int32 format, Int32 type, IntPtr image);

	public delegate void glConvolutionParameterf(Int32 target, Int32 pname, Single @params);

	public unsafe delegate void glConvolutionParameterfv(Int32 target, Int32 pname, Single* @params);

	public delegate void glConvolutionParameteri(Int32 target, Int32 pname, Int32 @params);

	public unsafe delegate void glConvolutionParameteriv(Int32 target, Int32 pname, Int32* @params);

	public delegate void glCopyBufferSubData(Int32 readTarget, Int32 writeTarget, IntPtr readOffset, IntPtr writeOffset, IntPtr size);

	public delegate void glCopyColorSubTable(Int32 target, Int32 start, Int32 x, Int32 y, Int32 width);

	public delegate void glCopyColorTable(Int32 target, Int32 internalformat, Int32 x, Int32 y, Int32 width);

	public delegate void glCopyConvolutionFilter1D(Int32 target, Int32 internalformat, Int32 x, Int32 y, Int32 width);

	public delegate void glCopyConvolutionFilter2D(Int32 target, Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 height);

	public delegate void glCopyImageSubData(UInt32 srcName, Int32 srcTarget, Int32 srcLevel, Int32 srcX, Int32 srcY, Int32 srcZ, UInt32 dstName, Int32 dstTarget, Int32 dstLevel, Int32 dstX, Int32 dstY, Int32 dstZ, Int32 srcWidth, Int32 srcHeight, Int32 srcDepth);

	public delegate void glCopyNamedBufferSubData(UInt32 readBuffer, UInt32 writeBuffer, IntPtr readOffset, IntPtr writeOffset, Int32 size);

	public delegate void glCopyTexImage1D(Int32 target, Int32 level, Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 border);

	public delegate void glCopyTexImage2D(Int32 target, Int32 level, Int32 internalformat, Int32 x, Int32 y, Int32 width, Int32 height, Int32 border);

	public delegate void glCopyTexSubImage1D(Int32 target, Int32 level, Int32 xoffset, Int32 x, Int32 y, Int32 width);

	public delegate void glCopyTexSubImage2D(Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 x, Int32 y, Int32 width, Int32 height);

	public delegate void glCopyTexSubImage3D(Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 x, Int32 y, Int32 width, Int32 height);

	public delegate void glCopyTextureSubImage1D(UInt32 texture, Int32 level, Int32 xoffset, Int32 x, Int32 y, Int32 width);

	public delegate void glCopyTextureSubImage2D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 x, Int32 y, Int32 width, Int32 height);

	public delegate void glCopyTextureSubImage3D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 x, Int32 y, Int32 width, Int32 height);

	public unsafe delegate void glCreateBuffers(Int32 n, [OutAttribute] UInt32* buffers);

	public unsafe delegate void glCreateFramebuffers(Int32 n, [OutAttribute] UInt32* framebuffers);

	public delegate Int32 glCreateProgram();

	public unsafe delegate void glCreateProgramPipelines(Int32 n, [OutAttribute] UInt32* pipelines);

	public unsafe delegate void glCreateQueries(Int32 target, Int32 n, [OutAttribute] UInt32* ids);

	public unsafe delegate void glCreateRenderbuffers(Int32 n, [OutAttribute] UInt32* renderbuffers);

	public unsafe delegate void glCreateSamplers(Int32 n, [OutAttribute] UInt32* samplers);

	public delegate Int32 glCreateShader(Int32 type);

	public delegate Int32 glCreateShaderProgramv(Int32 type, Int32 count, IntPtr strings);

	public unsafe delegate void glCreateTextures(Int32 target, Int32 n, [OutAttribute] UInt32* textures);

	public unsafe delegate void glCreateTransformFeedbacks(Int32 n, [OutAttribute] UInt32* ids);

	public unsafe delegate void glCreateVertexArrays(Int32 n, [OutAttribute] UInt32* arrays);

	public delegate void glCullFace(Int32 mode);

	public delegate void glDebugMessageCallback(DebugProc callback, IntPtr userParam);

	public unsafe delegate void glDebugMessageControl(Int32 source, Int32 type, Int32 severity, Int32 count, UInt32* ids, bool enabled);

	public delegate void glDebugMessageInsert(Int32 source, Int32 type, UInt32 id, Int32 severity, Int32 length, IntPtr buf);

	public unsafe delegate void glDeleteBuffers(Int32 n, UInt32* buffers);

	public unsafe delegate void glDeleteFramebuffers(Int32 n, UInt32* framebuffers);

	public delegate void glDeleteProgram(UInt32 program);

	public unsafe delegate void glDeleteProgramPipelines(Int32 n, UInt32* pipelines);

	public unsafe delegate void glDeleteQueries(Int32 n, UInt32* ids);

	public unsafe delegate void glDeleteRenderbuffers(Int32 n, UInt32* renderbuffers);

	public unsafe delegate void glDeleteSamplers(Int32 count, UInt32* samplers);

	public delegate void glDeleteShader(UInt32 shader);

	public delegate void glDeleteSync(IntPtr sync);

	public unsafe delegate void glDeleteTextures(Int32 n, UInt32* textures);

	public unsafe delegate void glDeleteTransformFeedbacks(Int32 n, UInt32* ids);

	public unsafe delegate void glDeleteVertexArrays(Int32 n, UInt32* arrays);

	public delegate void glDepthFunc(Int32 func);

	public delegate void glDepthMask(bool flag);

	public delegate void glDepthRange(Double near, Double far);

	public unsafe delegate void glDepthRangeArrayv(UInt32 first, Int32 count, Double* v);

	public delegate void glDepthRangef(Single n, Single f);

	public delegate void glDepthRangeIndexed(UInt32 index, Double n, Double f);

	public delegate void glDetachShader(UInt32 program, UInt32 shader);

	public delegate void glDisable(Int32 cap);

	public delegate void glDisablei(Int32 target, UInt32 index);

	public delegate void glDisableVertexArrayAttrib(UInt32 vaobj, UInt32 index);

	public delegate void glDisableVertexAttribArray(UInt32 index);

	public delegate void glDispatchCompute(UInt32 num_groups_x, UInt32 num_groups_y, UInt32 num_groups_z);

	public delegate void glDispatchComputeIndirect(IntPtr indirect);

	public delegate void glDrawArrays(Int32 mode, Int32 first, Int32 count);

	public delegate void glDrawArraysIndirect(Int32 mode, IntPtr indirect);

	public delegate void glDrawArraysInstanced(Int32 mode, Int32 first, Int32 count, Int32 instancecount);

	public delegate void glDrawArraysInstancedBaseInstance(Int32 mode, Int32 first, Int32 count, Int32 instancecount, UInt32 baseinstance);

	public delegate void glDrawBuffer(Int32 buf);

	public unsafe delegate void glDrawBuffers(Int32 n, Int32* bufs);

	public delegate void glDrawElements(Int32 mode, Int32 count, Int32 type, IntPtr indices);

	public delegate void glDrawElementsBaseVertex(Int32 mode, Int32 count, Int32 type, IntPtr indices, Int32 basevertex);

	public delegate void glDrawElementsIndirect(Int32 mode, Int32 type, IntPtr indirect);

	public delegate void glDrawElementsInstanced(Int32 mode, Int32 count, Int32 type, IntPtr indices, Int32 instancecount);

	public delegate void glDrawElementsInstancedBaseInstance(Int32 mode, Int32 count, Int32 type, IntPtr indices, Int32 instancecount, UInt32 baseinstance);

	public delegate void glDrawElementsInstancedBaseVertex(Int32 mode, Int32 count, Int32 type, IntPtr indices, Int32 instancecount, Int32 basevertex);

	public delegate void glDrawElementsInstancedBaseVertexBaseInstance(Int32 mode, Int32 count, Int32 type, IntPtr indices, Int32 instancecount, Int32 basevertex, UInt32 baseinstance);

	public delegate void glDrawRangeElements(Int32 mode, UInt32 start, UInt32 end, Int32 count, Int32 type, IntPtr indices);

	public delegate void glDrawRangeElementsBaseVertex(Int32 mode, UInt32 start, UInt32 end, Int32 count, Int32 type, IntPtr indices, Int32 basevertex);

	public delegate void glDrawTransformFeedback(Int32 mode, UInt32 id);

	public delegate void glDrawTransformFeedbackInstanced(Int32 mode, UInt32 id, Int32 instancecount);

	public delegate void glDrawTransformFeedbackStream(Int32 mode, UInt32 id, UInt32 stream);

	public delegate void glDrawTransformFeedbackStreamInstanced(Int32 mode, UInt32 id, UInt32 stream, Int32 instancecount);

	public delegate void glEnable(Int32 cap);

	public delegate void glEnablei(Int32 target, UInt32 index);

	public delegate void glEnableVertexArrayAttrib(UInt32 vaobj, UInt32 index);

	public delegate void glEnableVertexAttribArray(UInt32 index);

	public delegate void glEndConditionalRender();

	public delegate void glEndQuery(Int32 target);

	public delegate void glEndQueryIndexed(Int32 target, UInt32 index);

	public delegate void glEndTransformFeedback();

	public delegate IntPtr glFenceSync(Int32 condition, Int32 flags);

	public delegate void glFinish();

	public delegate void glFlush();

	public delegate void glFlushMappedBufferRange(Int32 target, IntPtr offset, IntPtr length);

	public delegate void glFlushMappedNamedBufferRange(UInt32 buffer, IntPtr offset, Int32 length);

	public delegate void glFramebufferParameteri(Int32 target, Int32 pname, Int32 param);

	public delegate void glFramebufferRenderbuffer(Int32 target, Int32 attachment, Int32 renderbuffertarget, UInt32 renderbuffer);

	public delegate void glFramebufferTexture(Int32 target, Int32 attachment, UInt32 texture, Int32 level);

	public delegate void glFramebufferTexture1D(Int32 target, Int32 attachment, Int32 textarget, UInt32 texture, Int32 level);

	public delegate void glFramebufferTexture2D(Int32 target, Int32 attachment, Int32 textarget, UInt32 texture, Int32 level);

	public delegate void glFramebufferTexture3D(Int32 target, Int32 attachment, Int32 textarget, UInt32 texture, Int32 level, Int32 zoffset);

	public delegate void glFramebufferTextureLayer(Int32 target, Int32 attachment, UInt32 texture, Int32 level, Int32 layer);

	public delegate void glFrontFace(Int32 mode);

	public unsafe delegate void glGenBuffers(Int32 n, [OutAttribute] UInt32* buffers);

	public delegate void glGenerateMipmap(Int32 target);

	public delegate void glGenerateTextureMipmap(UInt32 texture);

	public unsafe delegate void glGenFramebuffers(Int32 n, [OutAttribute] UInt32* framebuffers);

	public unsafe delegate void glGenProgramPipelines(Int32 n, [OutAttribute] UInt32* pipelines);

	public unsafe delegate void glGenQueries(Int32 n, [OutAttribute] UInt32* ids);

	public unsafe delegate void glGenRenderbuffers(Int32 n, [OutAttribute] UInt32* renderbuffers);

	public unsafe delegate void glGenSamplers(Int32 count, [OutAttribute] UInt32* samplers);

	public unsafe delegate void glGenTextures(Int32 n, [OutAttribute] UInt32* textures);

	public unsafe delegate void glGenTransformFeedbacks(Int32 n, [OutAttribute] UInt32* ids);

	public unsafe delegate void glGenVertexArrays(Int32 n, [OutAttribute] UInt32* arrays);

	public unsafe delegate void glGetActiveAtomicCounterBufferiv(UInt32 program, UInt32 bufferIndex, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetActiveAttrib(UInt32 program, UInt32 index, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] Int32* size, [OutAttribute] Int32* type, [OutAttribute] IntPtr name);

	public unsafe delegate void glGetActiveSubroutineName(UInt32 program, Int32 shadertype, UInt32 index, Int32 bufsize, [OutAttribute] Int32* length, [OutAttribute] IntPtr name);

	public unsafe delegate void glGetActiveSubroutineUniformiv(UInt32 program, Int32 shadertype, UInt32 index, Int32 pname, [OutAttribute] Int32* values);

	public unsafe delegate void glGetActiveSubroutineUniformName(UInt32 program, Int32 shadertype, UInt32 index, Int32 bufsize, [OutAttribute] Int32* length, [OutAttribute] IntPtr name);

	public unsafe delegate void glGetActiveUniform(UInt32 program, UInt32 index, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] Int32* size, [OutAttribute] Int32* type, [OutAttribute] IntPtr name);

	public unsafe delegate void glGetActiveUniformBlockiv(UInt32 program, UInt32 uniformBlockIndex, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetActiveUniformBlockName(UInt32 program, UInt32 uniformBlockIndex, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr uniformBlockName);

	public unsafe delegate void glGetActiveUniformName(UInt32 program, UInt32 uniformIndex, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr uniformName);

	public unsafe delegate void glGetActiveUniformsiv(UInt32 program, Int32 uniformCount, UInt32* uniformIndices, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetAttachedShaders(UInt32 program, Int32 maxCount, [OutAttribute] Int32* count, [OutAttribute] UInt32* shaders);

	public delegate Int32 glGetAttribLocation(UInt32 program, IntPtr name);

	public unsafe delegate void glGetBooleani_v(Int32 target, UInt32 index, [OutAttribute] bool* data);

	public unsafe delegate void glGetBooleanv(Int32 pname, [OutAttribute] bool* data);

	public unsafe delegate void glGetBufferParameteri64v(Int32 target, Int32 pname, [OutAttribute] Int64* @params);

	public unsafe delegate void glGetBufferParameteriv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public delegate void glGetBufferPointerv(Int32 target, Int32 pname, [OutAttribute] IntPtr @params);

	public delegate void glGetBufferSubData(Int32 target, IntPtr offset, IntPtr size, [OutAttribute] IntPtr data);

	public delegate void glGetColorTable(Int32 target, Int32 format, Int32 type, [OutAttribute] IntPtr table);

	public unsafe delegate void glGetColorTableParameterfv(Int32 target, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetColorTableParameteriv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public delegate void glGetCompressedTexImage(Int32 target, Int32 level, [OutAttribute] IntPtr img);

	public delegate void glGetCompressedTextureImage(UInt32 texture, Int32 level, Int32 bufSize, [OutAttribute] IntPtr pixels);

	public delegate void glGetCompressedTextureSubImage(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 bufSize, [OutAttribute] IntPtr pixels);

	public delegate void glGetConvolutionFilter(Int32 target, Int32 format, Int32 type, [OutAttribute] IntPtr image);

	public unsafe delegate void glGetConvolutionParameterfv(Int32 target, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetConvolutionParameteriv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate Int32 glGetDebugMessageLog(UInt32 count, Int32 bufSize, [OutAttribute] Int32* sources, [OutAttribute] Int32* types, [OutAttribute] UInt32* ids, [OutAttribute] Int32* severities, [OutAttribute] Int32* lengths, [OutAttribute] IntPtr messageLog);

	public unsafe delegate void glGetDoublei_v(Int32 target, UInt32 index, [OutAttribute] Double* data);

	public unsafe delegate void glGetDoublev(Int32 pname, [OutAttribute] Double* data);

	public delegate Int32 glGetError();

	public unsafe delegate void glGetFloati_v(Int32 target, UInt32 index, [OutAttribute] Single* data);

	public unsafe delegate void glGetFloatv(Int32 pname, [OutAttribute] Single* data);

	public delegate Int32 glGetFragDataIndex(UInt32 program, IntPtr name);

	public delegate Int32 glGetFragDataLocation(UInt32 program, IntPtr name);

	public unsafe delegate void glGetFramebufferAttachmentParameteriv(Int32 target, Int32 attachment, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetFramebufferParameteriv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public delegate Int32 glGetGraphicsResetStatus();

	public delegate void glGetHistogram(Int32 target, bool reset, Int32 format, Int32 type, [OutAttribute] IntPtr values);

	public unsafe delegate void glGetHistogramParameterfv(Int32 target, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetHistogramParameteriv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetInteger64i_v(Int32 target, UInt32 index, [OutAttribute] Int64* data);

	public unsafe delegate void glGetInteger64v(Int32 pname, [OutAttribute] Int64* data);

	public unsafe delegate void glGetIntegeri_v(Int32 target, UInt32 index, [OutAttribute] Int32* data);

	public unsafe delegate void glGetIntegerv(Int32 pname, [OutAttribute] Int32* data);

	public unsafe delegate void glGetInternalformati64v(Int32 target, Int32 internalformat, Int32 pname, Int32 bufSize, [OutAttribute] Int64* @params);

	public unsafe delegate void glGetInternalformativ(Int32 target, Int32 internalformat, Int32 pname, Int32 bufSize, [OutAttribute] Int32* @params);

	public delegate void glGetMinmax(Int32 target, bool reset, Int32 format, Int32 type, [OutAttribute] IntPtr values);

	public unsafe delegate void glGetMinmaxParameterfv(Int32 target, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetMinmaxParameteriv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetMultisamplefv(Int32 pname, UInt32 index, [OutAttribute] Single* val);

	public unsafe delegate void glGetNamedBufferParameteri64v(UInt32 buffer, Int32 pname, [OutAttribute] Int64* @params);

	public unsafe delegate void glGetNamedBufferParameteriv(UInt32 buffer, Int32 pname, [OutAttribute] Int32* @params);

	public delegate void glGetNamedBufferPointerv(UInt32 buffer, Int32 pname, [OutAttribute] IntPtr @params);

	public delegate void glGetNamedBufferSubData(UInt32 buffer, IntPtr offset, Int32 size, [OutAttribute] IntPtr data);

	public unsafe delegate void glGetNamedFramebufferAttachmentParameteriv(UInt32 framebuffer, Int32 attachment, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetNamedFramebufferParameteriv(UInt32 framebuffer, Int32 pname, [OutAttribute] Int32* param);

	public unsafe delegate void glGetNamedRenderbufferParameteriv(UInt32 renderbuffer, Int32 pname, [OutAttribute] Int32* @params);

	public delegate void glGetnColorTable(Int32 target, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr table);

	public delegate void glGetnCompressedTexImage(Int32 target, Int32 lod, Int32 bufSize, [OutAttribute] IntPtr pixels);

	public delegate void glGetnConvolutionFilter(Int32 target, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr image);

	public delegate void glGetnHistogram(Int32 target, bool reset, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr values);

	public unsafe delegate void glGetnMapdv(Int32 target, Int32 query, Int32 bufSize, [OutAttribute] Double* v);

	public unsafe delegate void glGetnMapfv(Int32 target, Int32 query, Int32 bufSize, [OutAttribute] Single* v);

	public unsafe delegate void glGetnMapiv(Int32 target, Int32 query, Int32 bufSize, [OutAttribute] Int32* v);

	public delegate void glGetnMinmax(Int32 target, bool reset, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr values);

	public unsafe delegate void glGetnPixelMapfv(Int32 map, Int32 bufSize, [OutAttribute] Single* values);

	public unsafe delegate void glGetnPixelMapuiv(Int32 map, Int32 bufSize, [OutAttribute] UInt32* values);

	public unsafe delegate void glGetnPixelMapusv(Int32 map, Int32 bufSize, [OutAttribute] UInt16* values);

	public unsafe delegate void glGetnPolygonStipple(Int32 bufSize, [OutAttribute] Byte* pattern);

	public delegate void glGetnSeparableFilter(Int32 target, Int32 format, Int32 type, Int32 rowBufSize, [OutAttribute] IntPtr row, Int32 columnBufSize, [OutAttribute] IntPtr column, [OutAttribute] IntPtr span);

	public delegate void glGetnTexImage(Int32 target, Int32 level, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr pixels);

	public unsafe delegate void glGetnUniformdv(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Double* @params);

	public unsafe delegate void glGetnUniformfv(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Single* @params);

	public unsafe delegate void glGetnUniformiv(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetnUniformuiv(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] UInt32* @params);

	public unsafe delegate void glGetObjectLabel(Int32 identifier, UInt32 name, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr label);

	public unsafe delegate void glGetObjectPtrLabel(IntPtr ptr, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr label);

	public delegate void glGetPointerv(Int32 pname, [OutAttribute] IntPtr @params);

	public unsafe delegate void glGetProgramBinary(UInt32 program, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] Int32* binaryFormat, [OutAttribute] IntPtr binary);

	public unsafe delegate void glGetProgramInfoLog(UInt32 program, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr infoLog);

	public unsafe delegate void glGetProgramInterfaceiv(UInt32 program, Int32 programInterface, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetProgramiv(UInt32 program, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetProgramPipelineInfoLog(UInt32 pipeline, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr infoLog);

	public unsafe delegate void glGetProgramPipelineiv(UInt32 pipeline, Int32 pname, [OutAttribute] Int32* @params);

	public delegate Int32 glGetProgramResourceIndex(UInt32 program, Int32 programInterface, IntPtr name);

	public unsafe delegate void glGetProgramResourceiv(UInt32 program, Int32 programInterface, UInt32 index, Int32 propCount, Int32* props, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] Int32* @params);

	public delegate Int32 glGetProgramResourceLocation(UInt32 program, Int32 programInterface, IntPtr name);

	public delegate Int32 glGetProgramResourceLocationIndex(UInt32 program, Int32 programInterface, IntPtr name);

	public unsafe delegate void glGetProgramResourceName(UInt32 program, Int32 programInterface, UInt32 index, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr name);

	public unsafe delegate void glGetProgramStageiv(UInt32 program, Int32 shadertype, Int32 pname, [OutAttribute] Int32* values);

	public unsafe delegate void glGetQueryIndexediv(Int32 target, UInt32 index, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetQueryiv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetQueryObjecti64v(UInt32 id, Int32 pname, [OutAttribute] Int64* @params);

	public unsafe delegate void glGetQueryObjectiv(UInt32 id, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetQueryObjectui64v(UInt32 id, Int32 pname, [OutAttribute] UInt64* @params);

	public unsafe delegate void glGetQueryObjectuiv(UInt32 id, Int32 pname, [OutAttribute] UInt32* @params);

	public unsafe delegate void glGetRenderbufferParameteriv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetSamplerParameterfv(UInt32 sampler, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetSamplerParameterIiv(UInt32 sampler, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetSamplerParameterIuiv(UInt32 sampler, Int32 pname, [OutAttribute] UInt32* @params);

	public unsafe delegate void glGetSamplerParameteriv(UInt32 sampler, Int32 pname, [OutAttribute] Int32* @params);

	public delegate void glGetSeparableFilter(Int32 target, Int32 format, Int32 type, [OutAttribute] IntPtr row, [OutAttribute] IntPtr column, [OutAttribute] IntPtr span);

	public unsafe delegate void glGetShaderInfoLog(UInt32 shader, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr infoLog);

	public unsafe delegate void glGetShaderiv(UInt32 shader, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetShaderPrecisionFormat(Int32 shadertype, Int32 precisiontype, [OutAttribute] Int32* range, [OutAttribute] Int32* precision);

	public unsafe delegate void glGetShaderSource(UInt32 shader, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr source);

	public delegate IntPtr glGetString(Int32 name);

	public delegate IntPtr glGetStringi(Int32 name, UInt32 index);

	public delegate Int32 glGetSubroutineIndex(UInt32 program, Int32 shadertype, IntPtr name);

	public delegate Int32 glGetSubroutineUniformLocation(UInt32 program, Int32 shadertype, IntPtr name);

	public unsafe delegate void glGetSynciv(IntPtr sync, Int32 pname, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] Int32* values);

	public delegate void glGetTexImage(Int32 target, Int32 level, Int32 format, Int32 type, [OutAttribute] IntPtr pixels);

	public unsafe delegate void glGetTexLevelParameterfv(Int32 target, Int32 level, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetTexLevelParameteriv(Int32 target, Int32 level, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetTexParameterfv(Int32 target, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetTexParameterIiv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetTexParameterIuiv(Int32 target, Int32 pname, [OutAttribute] UInt32* @params);

	public unsafe delegate void glGetTexParameteriv(Int32 target, Int32 pname, [OutAttribute] Int32* @params);

	public delegate void glGetTextureImage(UInt32 texture, Int32 level, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr pixels);

	public unsafe delegate void glGetTextureLevelParameterfv(UInt32 texture, Int32 level, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetTextureLevelParameteriv(UInt32 texture, Int32 level, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetTextureParameterfv(UInt32 texture, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetTextureParameterIiv(UInt32 texture, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetTextureParameterIuiv(UInt32 texture, Int32 pname, [OutAttribute] UInt32* @params);

	public unsafe delegate void glGetTextureParameteriv(UInt32 texture, Int32 pname, [OutAttribute] Int32* @params);

	public delegate void glGetTextureSubImage(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr pixels);

	public unsafe delegate void glGetTransformFeedbacki_v(UInt32 xfb, Int32 pname, UInt32 index, [OutAttribute] Int32* param);

	public unsafe delegate void glGetTransformFeedbacki64_v(UInt32 xfb, Int32 pname, UInt32 index, [OutAttribute] Int64* param);

	public unsafe delegate void glGetTransformFeedbackiv(UInt32 xfb, Int32 pname, [OutAttribute] Int32* param);

	public unsafe delegate void glGetTransformFeedbackVarying(UInt32 program, UInt32 index, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] Int32* size, [OutAttribute] Int32* type, [OutAttribute] IntPtr name);

	public delegate Int32 glGetUniformBlockIndex(UInt32 program, IntPtr uniformBlockName);

	public unsafe delegate void glGetUniformdv(UInt32 program, Int32 location, [OutAttribute] Double* @params);

	public unsafe delegate void glGetUniformfv(UInt32 program, Int32 location, [OutAttribute] Single* @params);

	public unsafe delegate void glGetUniformIndices(UInt32 program, Int32 uniformCount, IntPtr uniformNames, [OutAttribute] UInt32* uniformIndices);

	public unsafe delegate void glGetUniformiv(UInt32 program, Int32 location, [OutAttribute] Int32* @params);

	public delegate Int32 glGetUniformLocation(UInt32 program, IntPtr name);

	public unsafe delegate void glGetUniformSubroutineuiv(Int32 shadertype, Int32 location, [OutAttribute] UInt32* @params);

	public unsafe delegate void glGetUniformuiv(UInt32 program, Int32 location, [OutAttribute] UInt32* @params);

	public unsafe delegate void glGetVertexArrayIndexed64iv(UInt32 vaobj, UInt32 index, Int32 pname, [OutAttribute] Int64* param);

	public unsafe delegate void glGetVertexArrayIndexediv(UInt32 vaobj, UInt32 index, Int32 pname, [OutAttribute] Int32* param);

	public unsafe delegate void glGetVertexArrayiv(UInt32 vaobj, Int32 pname, [OutAttribute] Int32* param);

	public unsafe delegate void glGetVertexAttribdv(UInt32 index, Int32 pname, [OutAttribute] Double* @params);

	public unsafe delegate void glGetVertexAttribfv(UInt32 index, Int32 pname, [OutAttribute] Single* @params);

	public unsafe delegate void glGetVertexAttribIiv(UInt32 index, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetVertexAttribIuiv(UInt32 index, Int32 pname, [OutAttribute] UInt32* @params);

	public unsafe delegate void glGetVertexAttribiv(UInt32 index, Int32 pname, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetVertexAttribLdv(UInt32 index, Int32 pname, [OutAttribute] Double* @params);

	public delegate void glGetVertexAttribPointerv(UInt32 index, Int32 pname, [OutAttribute] IntPtr pointer);

	public delegate void glHint(Int32 target, Int32 mode);

	public delegate void glHistogram(Int32 target, Int32 width, Int32 internalformat, bool sink);

	public delegate void glInvalidateBufferData(UInt32 buffer);

	public delegate void glInvalidateBufferSubData(UInt32 buffer, IntPtr offset, IntPtr length);

	public unsafe delegate void glInvalidateFramebuffer(Int32 target, Int32 numAttachments, Int32* attachments);

	public unsafe delegate void glInvalidateNamedFramebufferData(UInt32 framebuffer, Int32 numAttachments, Int32* attachments);

	public unsafe delegate void glInvalidateNamedFramebufferSubData(UInt32 framebuffer, Int32 numAttachments, Int32* attachments, Int32 x, Int32 y, Int32 width, Int32 height);

	public unsafe delegate void glInvalidateSubFramebuffer(Int32 target, Int32 numAttachments, Int32* attachments, Int32 x, Int32 y, Int32 width, Int32 height);

	public delegate void glInvalidateTexImage(UInt32 texture, Int32 level);

	public delegate void glInvalidateTexSubImage(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth);

	public delegate byte glIsBuffer(UInt32 buffer);

	public delegate byte glIsEnabled(Int32 cap);

	public delegate byte glIsEnabledi(Int32 target, UInt32 index);

	public delegate byte glIsFramebuffer(UInt32 framebuffer);

	public delegate byte glIsProgram(UInt32 program);

	public delegate byte glIsProgramPipeline(UInt32 pipeline);

	public delegate byte glIsQuery(UInt32 id);

	public delegate byte glIsRenderbuffer(UInt32 renderbuffer);

	public delegate byte glIsSampler(UInt32 sampler);

	public delegate byte glIsShader(UInt32 shader);

	public delegate byte glIsSync(IntPtr sync);

	public delegate byte glIsTexture(UInt32 texture);

	public delegate byte glIsTransformFeedback(UInt32 id);

	public delegate byte glIsVertexArray(UInt32 array);

	public delegate void glLineWidth(Single width);

	public delegate void glLinkProgram(UInt32 program);

	public delegate void glLogicOp(Int32 opcode);

	public delegate IntPtr glMapBuffer(Int32 target, Int32 access);

	public delegate IntPtr glMapBufferRange(Int32 target, IntPtr offset, IntPtr length, Int32 access);

	public delegate IntPtr glMapNamedBuffer(UInt32 buffer, Int32 access);

	public delegate IntPtr glMapNamedBufferRange(UInt32 buffer, IntPtr offset, Int32 length, Int32 access);

	public delegate void glMemoryBarrier(Int32 barriers);

	public delegate void glMemoryBarrierByRegion(Int32 barriers);

	public delegate void glMinmax(Int32 target, Int32 internalformat, bool sink);

	public delegate void glMinSampleShading(Single value);

	public unsafe delegate void glMultiDrawArrays(Int32 mode, Int32* first, Int32* count, Int32 drawcount);

	public delegate void glMultiDrawArraysIndirect(Int32 mode, IntPtr indirect, Int32 drawcount, Int32 stride);

	public unsafe delegate void glMultiDrawElements(Int32 mode, Int32* count, Int32 type, IntPtr* indices, Int32 drawcount);

	public unsafe delegate void glMultiDrawElementsBaseVertex(Int32 mode, Int32* count, Int32 type, IntPtr* indices, Int32 drawcount, Int32* basevertex);

	public delegate void glMultiDrawElementsIndirect(Int32 mode, Int32 type, IntPtr indirect, Int32 drawcount, Int32 stride);

	public delegate void glMultiTexCoordP1ui(Int32 texture, Int32 type, UInt32 coords);

	public unsafe delegate void glMultiTexCoordP1uiv(Int32 texture, Int32 type, UInt32* coords);

	public delegate void glMultiTexCoordP2ui(Int32 texture, Int32 type, UInt32 coords);

	public unsafe delegate void glMultiTexCoordP2uiv(Int32 texture, Int32 type, UInt32* coords);

	public delegate void glMultiTexCoordP3ui(Int32 texture, Int32 type, UInt32 coords);

	public unsafe delegate void glMultiTexCoordP3uiv(Int32 texture, Int32 type, UInt32* coords);

	public delegate void glMultiTexCoordP4ui(Int32 texture, Int32 type, UInt32 coords);

	public unsafe delegate void glMultiTexCoordP4uiv(Int32 texture, Int32 type, UInt32* coords);

	public delegate void glNamedBufferData(UInt32 buffer, Int32 size, IntPtr data, Int32 usage);

	public delegate void glNamedBufferStorage(UInt32 buffer, Int32 size, IntPtr data, Int32 flags);

	public delegate void glNamedBufferSubData(UInt32 buffer, IntPtr offset, Int32 size, IntPtr data);

	public delegate void glNamedFramebufferDrawBuffer(UInt32 framebuffer, Int32 buf);

	public unsafe delegate void glNamedFramebufferDrawBuffers(UInt32 framebuffer, Int32 n, Int32* bufs);

	public delegate void glNamedFramebufferParameteri(UInt32 framebuffer, Int32 pname, Int32 param);

	public delegate void glNamedFramebufferReadBuffer(UInt32 framebuffer, Int32 src);

	public delegate void glNamedFramebufferRenderbuffer(UInt32 framebuffer, Int32 attachment, Int32 renderbuffertarget, UInt32 renderbuffer);

	public delegate void glNamedFramebufferTexture(UInt32 framebuffer, Int32 attachment, UInt32 texture, Int32 level);

	public delegate void glNamedFramebufferTextureLayer(UInt32 framebuffer, Int32 attachment, UInt32 texture, Int32 level, Int32 layer);

	public delegate void glNamedRenderbufferStorage(UInt32 renderbuffer, Int32 internalformat, Int32 width, Int32 height);

	public delegate void glNamedRenderbufferStorageMultisample(UInt32 renderbuffer, Int32 samples, Int32 internalformat, Int32 width, Int32 height);

	public delegate void glNormalP3ui(Int32 type, UInt32 coords);

	public unsafe delegate void glNormalP3uiv(Int32 type, UInt32* coords);

	public delegate void glObjectLabel(Int32 identifier, UInt32 name, Int32 length, IntPtr label);

	public delegate void glObjectPtrLabel(IntPtr ptr, Int32 length, IntPtr label);

	public unsafe delegate void glPatchParameterfv(Int32 pname, Single* values);

	public delegate void glPatchParameteri(Int32 pname, Int32 value);

	public delegate void glPauseTransformFeedback();

	public delegate void glPixelStoref(Int32 pname, Single param);

	public delegate void glPixelStorei(Int32 pname, Int32 param);

	public delegate void glPointParameterf(Int32 pname, Single param);

	public unsafe delegate void glPointParameterfv(Int32 pname, Single* @params);

	public delegate void glPointParameteri(Int32 pname, Int32 param);

	public unsafe delegate void glPointParameteriv(Int32 pname, Int32* @params);

	public delegate void glPointSize(Single size);

	public delegate void glPolygonMode(Int32 face, Int32 mode);

	public delegate void glPolygonOffset(Single factor, Single units);

	public delegate void glPopDebugGroup();

	public delegate void glPrimitiveRestartIndex(UInt32 index);

	public delegate void glProgramBinary(UInt32 program, Int32 binaryFormat, IntPtr binary, Int32 length);

	public delegate void glProgramParameteri(UInt32 program, Int32 pname, Int32 value);

	public delegate void glProgramUniform1d(UInt32 program, Int32 location, Double v0);

	public unsafe delegate void glProgramUniform1dv(UInt32 program, Int32 location, Int32 count, Double* value);

	public delegate void glProgramUniform1f(UInt32 program, Int32 location, Single v0);

	public unsafe delegate void glProgramUniform1fv(UInt32 program, Int32 location, Int32 count, Single* value);

	public delegate void glProgramUniform1i(UInt32 program, Int32 location, Int32 v0);

	public unsafe delegate void glProgramUniform1iv(UInt32 program, Int32 location, Int32 count, Int32* value);

	public delegate void glProgramUniform1ui(UInt32 program, Int32 location, UInt32 v0);

	public unsafe delegate void glProgramUniform1uiv(UInt32 program, Int32 location, Int32 count, UInt32* value);

	public delegate void glProgramUniform2d(UInt32 program, Int32 location, Double v0, Double v1);

	public unsafe delegate void glProgramUniform2dv(UInt32 program, Int32 location, Int32 count, Double* value);

	public delegate void glProgramUniform2f(UInt32 program, Int32 location, Single v0, Single v1);

	public unsafe delegate void glProgramUniform2fv(UInt32 program, Int32 location, Int32 count, Single* value);

	public delegate void glProgramUniform2i(UInt32 program, Int32 location, Int32 v0, Int32 v1);

	public unsafe delegate void glProgramUniform2iv(UInt32 program, Int32 location, Int32 count, Int32* value);

	public delegate void glProgramUniform2ui(UInt32 program, Int32 location, UInt32 v0, UInt32 v1);

	public unsafe delegate void glProgramUniform2uiv(UInt32 program, Int32 location, Int32 count, UInt32* value);

	public delegate void glProgramUniform3d(UInt32 program, Int32 location, Double v0, Double v1, Double v2);

	public unsafe delegate void glProgramUniform3dv(UInt32 program, Int32 location, Int32 count, Double* value);

	public delegate void glProgramUniform3f(UInt32 program, Int32 location, Single v0, Single v1, Single v2);

	public unsafe delegate void glProgramUniform3fv(UInt32 program, Int32 location, Int32 count, Single* value);

	public delegate void glProgramUniform3i(UInt32 program, Int32 location, Int32 v0, Int32 v1, Int32 v2);

	public unsafe delegate void glProgramUniform3iv(UInt32 program, Int32 location, Int32 count, Int32* value);

	public delegate void glProgramUniform3ui(UInt32 program, Int32 location, UInt32 v0, UInt32 v1, UInt32 v2);

	public unsafe delegate void glProgramUniform3uiv(UInt32 program, Int32 location, Int32 count, UInt32* value);

	public delegate void glProgramUniform4d(UInt32 program, Int32 location, Double v0, Double v1, Double v2, Double v3);

	public unsafe delegate void glProgramUniform4dv(UInt32 program, Int32 location, Int32 count, Double* value);

	public delegate void glProgramUniform4f(UInt32 program, Int32 location, Single v0, Single v1, Single v2, Single v3);

	public unsafe delegate void glProgramUniform4fv(UInt32 program, Int32 location, Int32 count, Single* value);

	public delegate void glProgramUniform4i(UInt32 program, Int32 location, Int32 v0, Int32 v1, Int32 v2, Int32 v3);

	public unsafe delegate void glProgramUniform4iv(UInt32 program, Int32 location, Int32 count, Int32* value);

	public delegate void glProgramUniform4ui(UInt32 program, Int32 location, UInt32 v0, UInt32 v1, UInt32 v2, UInt32 v3);

	public unsafe delegate void glProgramUniform4uiv(UInt32 program, Int32 location, Int32 count, UInt32* value);

	public unsafe delegate void glProgramUniformMatrix2dv(UInt32 program, Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glProgramUniformMatrix2fv(UInt32 program, Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glProgramUniformMatrix2x3dv(UInt32 program, Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glProgramUniformMatrix2x3fv(UInt32 program, Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glProgramUniformMatrix2x4dv(UInt32 program, Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glProgramUniformMatrix2x4fv(UInt32 program, Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glProgramUniformMatrix3dv(UInt32 program, Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glProgramUniformMatrix3fv(UInt32 program, Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glProgramUniformMatrix3x2dv(UInt32 program, Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glProgramUniformMatrix3x2fv(UInt32 program, Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glProgramUniformMatrix3x4dv(UInt32 program, Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glProgramUniformMatrix3x4fv(UInt32 program, Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glProgramUniformMatrix4dv(UInt32 program, Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glProgramUniformMatrix4fv(UInt32 program, Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glProgramUniformMatrix4x2dv(UInt32 program, Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glProgramUniformMatrix4x2fv(UInt32 program, Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glProgramUniformMatrix4x3dv(UInt32 program, Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glProgramUniformMatrix4x3fv(UInt32 program, Int32 location, Int32 count, bool transpose, Single* value);

	public delegate void glProvokingVertex(Int32 mode);

	public delegate void glPushDebugGroup(Int32 source, UInt32 id, Int32 length, IntPtr message);

	public delegate void glQueryCounter(UInt32 id, Int32 target);

	public delegate void glReadBuffer(Int32 src);

	public delegate void glReadnPixels(Int32 x, Int32 y, Int32 width, Int32 height, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr data);

	public delegate void glReadPixels(Int32 x, Int32 y, Int32 width, Int32 height, Int32 format, Int32 type, [OutAttribute] IntPtr pixels);

	public delegate void glReleaseShaderCompiler();

	public delegate void glRenderbufferStorage(Int32 target, Int32 internalformat, Int32 width, Int32 height);

	public delegate void glRenderbufferStorageMultisample(Int32 target, Int32 samples, Int32 internalformat, Int32 width, Int32 height);

	public delegate void glResetHistogram(Int32 target);

	public delegate void glResetMinmax(Int32 target);

	public delegate void glResumeTransformFeedback();

	public delegate void glSampleCoverage(Single value, bool invert);

	public delegate void glSampleMaski(UInt32 maskNumber, UInt32 mask);

	public delegate void glSamplerParameterf(UInt32 sampler, Int32 pname, Single param);

	public unsafe delegate void glSamplerParameterfv(UInt32 sampler, Int32 pname, Single* param);

	public delegate void glSamplerParameteri(UInt32 sampler, Int32 pname, Int32 param);

	public unsafe delegate void glSamplerParameterIiv(UInt32 sampler, Int32 pname, Int32* param);

	public unsafe delegate void glSamplerParameterIuiv(UInt32 sampler, Int32 pname, UInt32* param);

	public unsafe delegate void glSamplerParameteriv(UInt32 sampler, Int32 pname, Int32* param);

	public delegate void glScissor(Int32 x, Int32 y, Int32 width, Int32 height);

	public unsafe delegate void glScissorArrayv(UInt32 first, Int32 count, Int32* v);

	public delegate void glScissorIndexed(UInt32 index, Int32 left, Int32 bottom, Int32 width, Int32 height);

	public unsafe delegate void glScissorIndexedv(UInt32 index, Int32* v);

	public delegate void glSecondaryColorP3ui(Int32 type, UInt32 color);

	public unsafe delegate void glSecondaryColorP3uiv(Int32 type, UInt32* color);

	public delegate void glSeparableFilter2D(Int32 target, Int32 internalformat, Int32 width, Int32 height, Int32 format, Int32 type, IntPtr row, IntPtr column);

	public unsafe delegate void glShaderBinary(Int32 count, UInt32* shaders, Int32 binaryformat, IntPtr binary, Int32 length);

	public unsafe delegate void glShaderSource(UInt32 shader, Int32 count, IntPtr @string, Int32* length);

	public delegate void glShaderStorageBlockBinding(UInt32 program, UInt32 storageBlockIndex, UInt32 storageBlockBinding);

	public delegate void glStencilFunc(Int32 func, Int32 @ref, UInt32 mask);

	public delegate void glStencilFuncSeparate(Int32 face, Int32 func, Int32 @ref, UInt32 mask);

	public delegate void glStencilMask(UInt32 mask);

	public delegate void glStencilMaskSeparate(Int32 face, UInt32 mask);

	public delegate void glStencilOp(Int32 fail, Int32 zfail, Int32 zpass);

	public delegate void glStencilOpSeparate(Int32 face, Int32 sfail, Int32 dpfail, Int32 dppass);

	public delegate void glTexBuffer(Int32 target, Int32 internalformat, UInt32 buffer);

	public delegate void glTexBufferRange(Int32 target, Int32 internalformat, UInt32 buffer, IntPtr offset, IntPtr size);

	public delegate void glTexCoordP1ui(Int32 type, UInt32 coords);

	public unsafe delegate void glTexCoordP1uiv(Int32 type, UInt32* coords);

	public delegate void glTexCoordP2ui(Int32 type, UInt32 coords);

	public unsafe delegate void glTexCoordP2uiv(Int32 type, UInt32* coords);

	public delegate void glTexCoordP3ui(Int32 type, UInt32 coords);

	public unsafe delegate void glTexCoordP3uiv(Int32 type, UInt32* coords);

	public delegate void glTexCoordP4ui(Int32 type, UInt32 coords);

	public unsafe delegate void glTexCoordP4uiv(Int32 type, UInt32* coords);

	public delegate void glTexImage1D(Int32 target, Int32 level, Int32 internalformat, Int32 width, Int32 border, Int32 format, Int32 type, IntPtr pixels);

	public delegate void glTexImage2D(Int32 target, Int32 level, Int32 internalformat, Int32 width, Int32 height, Int32 border, Int32 format, Int32 type, IntPtr pixels);

	public delegate void glTexImage2DMultisample(Int32 target, Int32 samples, Int32 internalformat, Int32 width, Int32 height, bool fixedsamplelocations);

	public delegate void glTexImage3D(Int32 target, Int32 level, Int32 internalformat, Int32 width, Int32 height, Int32 depth, Int32 border, Int32 format, Int32 type, IntPtr pixels);

	public delegate void glTexImage3DMultisample(Int32 target, Int32 samples, Int32 internalformat, Int32 width, Int32 height, Int32 depth, bool fixedsamplelocations);

	public delegate void glTexParameterf(Int32 target, Int32 pname, Single param);

	public unsafe delegate void glTexParameterfv(Int32 target, Int32 pname, Single* @params);

	public delegate void glTexParameteri(Int32 target, Int32 pname, Int32 param);

	public unsafe delegate void glTexParameterIiv(Int32 target, Int32 pname, Int32* @params);

	public unsafe delegate void glTexParameterIuiv(Int32 target, Int32 pname, UInt32* @params);

	public unsafe delegate void glTexParameteriv(Int32 target, Int32 pname, Int32* @params);

	public delegate void glTexStorage1D(Int32 target, Int32 levels, Int32 internalformat, Int32 width);

	public delegate void glTexStorage2D(Int32 target, Int32 levels, Int32 internalformat, Int32 width, Int32 height);

	public delegate void glTexStorage2DMultisample(Int32 target, Int32 samples, Int32 internalformat, Int32 width, Int32 height, bool fixedsamplelocations);

	public delegate void glTexStorage3D(Int32 target, Int32 levels, Int32 internalformat, Int32 width, Int32 height, Int32 depth);

	public delegate void glTexStorage3DMultisample(Int32 target, Int32 samples, Int32 internalformat, Int32 width, Int32 height, Int32 depth, bool fixedsamplelocations);

	public delegate void glTexSubImage1D(Int32 target, Int32 level, Int32 xoffset, Int32 width, Int32 format, Int32 type, IntPtr pixels);

	public delegate void glTexSubImage2D(Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, Int32 format, Int32 type, IntPtr pixels);

	public delegate void glTexSubImage3D(Int32 target, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, Int32 type, IntPtr pixels);

	public delegate void glTextureBarrier();

	public delegate void glTextureBuffer(UInt32 texture, Int32 internalformat, UInt32 buffer);

	public delegate void glTextureBufferRange(UInt32 texture, Int32 internalformat, UInt32 buffer, IntPtr offset, Int32 size);

	public delegate void glTextureParameterf(UInt32 texture, Int32 pname, Single param);

	public unsafe delegate void glTextureParameterfv(UInt32 texture, Int32 pname, Single* param);

	public delegate void glTextureParameteri(UInt32 texture, Int32 pname, Int32 param);

	public unsafe delegate void glTextureParameterIiv(UInt32 texture, Int32 pname, Int32* @params);

	public unsafe delegate void glTextureParameterIuiv(UInt32 texture, Int32 pname, UInt32* @params);

	public unsafe delegate void glTextureParameteriv(UInt32 texture, Int32 pname, Int32* param);

	public delegate void glTextureStorage1D(UInt32 texture, Int32 levels, Int32 internalformat, Int32 width);

	public delegate void glTextureStorage2D(UInt32 texture, Int32 levels, Int32 internalformat, Int32 width, Int32 height);

	public delegate void glTextureStorage2DMultisample(UInt32 texture, Int32 samples, Int32 internalformat, Int32 width, Int32 height, bool fixedsamplelocations);

	public delegate void glTextureStorage3D(UInt32 texture, Int32 levels, Int32 internalformat, Int32 width, Int32 height, Int32 depth);

	public delegate void glTextureStorage3DMultisample(UInt32 texture, Int32 samples, Int32 internalformat, Int32 width, Int32 height, Int32 depth, bool fixedsamplelocations);

	public delegate void glTextureSubImage1D(UInt32 texture, Int32 level, Int32 xoffset, Int32 width, Int32 format, Int32 type, IntPtr pixels);

	public delegate void glTextureSubImage2D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 width, Int32 height, Int32 format, Int32 type, IntPtr pixels);

	public delegate void glTextureSubImage3D(UInt32 texture, Int32 level, Int32 xoffset, Int32 yoffset, Int32 zoffset, Int32 width, Int32 height, Int32 depth, Int32 format, Int32 type, IntPtr pixels);

	public delegate void glTextureView(UInt32 texture, Int32 target, UInt32 origtexture, Int32 internalformat, UInt32 minlevel, UInt32 numlevels, UInt32 minlayer, UInt32 numlayers);

	public delegate void glTransformFeedbackBufferBase(UInt32 xfb, UInt32 index, UInt32 buffer);

	public delegate void glTransformFeedbackBufferRange(UInt32 xfb, UInt32 index, UInt32 buffer, IntPtr offset, Int32 size);

	public delegate void glTransformFeedbackVaryings(UInt32 program, Int32 count, IntPtr varyings, Int32 bufferMode);

	public delegate void glUniform1d(Int32 location, Double x);

	public unsafe delegate void glUniform1dv(Int32 location, Int32 count, Double* value);

	public delegate void glUniform1f(Int32 location, Single v0);

	public unsafe delegate void glUniform1fv(Int32 location, Int32 count, Single* value);

	public delegate void glUniform1i(Int32 location, Int32 v0);

	public unsafe delegate void glUniform1iv(Int32 location, Int32 count, Int32* value);

	public delegate void glUniform1ui(Int32 location, UInt32 v0);

	public unsafe delegate void glUniform1uiv(Int32 location, Int32 count, UInt32* value);

	public delegate void glUniform2d(Int32 location, Double x, Double y);

	public unsafe delegate void glUniform2dv(Int32 location, Int32 count, Double* value);

	public delegate void glUniform2f(Int32 location, Single v0, Single v1);

	public unsafe delegate void glUniform2fv(Int32 location, Int32 count, Single* value);

	public delegate void glUniform2i(Int32 location, Int32 v0, Int32 v1);

	public unsafe delegate void glUniform2iv(Int32 location, Int32 count, Int32* value);

	public delegate void glUniform2ui(Int32 location, UInt32 v0, UInt32 v1);

	public unsafe delegate void glUniform2uiv(Int32 location, Int32 count, UInt32* value);

	public delegate void glUniform3d(Int32 location, Double x, Double y, Double z);

	public unsafe delegate void glUniform3dv(Int32 location, Int32 count, Double* value);

	public delegate void glUniform3f(Int32 location, Single v0, Single v1, Single v2);

	public unsafe delegate void glUniform3fv(Int32 location, Int32 count, Single* value);

	public delegate void glUniform3i(Int32 location, Int32 v0, Int32 v1, Int32 v2);

	public unsafe delegate void glUniform3iv(Int32 location, Int32 count, Int32* value);

	public delegate void glUniform3ui(Int32 location, UInt32 v0, UInt32 v1, UInt32 v2);

	public unsafe delegate void glUniform3uiv(Int32 location, Int32 count, UInt32* value);

	public delegate void glUniform4d(Int32 location, Double x, Double y, Double z, Double w);

	public unsafe delegate void glUniform4dv(Int32 location, Int32 count, Double* value);

	public delegate void glUniform4f(Int32 location, Single v0, Single v1, Single v2, Single v3);

	public unsafe delegate void glUniform4fv(Int32 location, Int32 count, Single* value);

	public delegate void glUniform4i(Int32 location, Int32 v0, Int32 v1, Int32 v2, Int32 v3);

	public unsafe delegate void glUniform4iv(Int32 location, Int32 count, Int32* value);

	public delegate void glUniform4ui(Int32 location, UInt32 v0, UInt32 v1, UInt32 v2, UInt32 v3);

	public unsafe delegate void glUniform4uiv(Int32 location, Int32 count, UInt32* value);

	public delegate void glUniformBlockBinding(UInt32 program, UInt32 uniformBlockIndex, UInt32 uniformBlockBinding);

	public unsafe delegate void glUniformMatrix2dv(Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glUniformMatrix2fv(Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glUniformMatrix2x3dv(Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glUniformMatrix2x3fv(Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glUniformMatrix2x4dv(Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glUniformMatrix2x4fv(Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glUniformMatrix3dv(Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glUniformMatrix3fv(Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glUniformMatrix3x2dv(Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glUniformMatrix3x2fv(Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glUniformMatrix3x4dv(Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glUniformMatrix3x4fv(Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glUniformMatrix4dv(Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glUniformMatrix4fv(Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glUniformMatrix4x2dv(Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glUniformMatrix4x2fv(Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glUniformMatrix4x3dv(Int32 location, Int32 count, bool transpose, Double* value);

	public unsafe delegate void glUniformMatrix4x3fv(Int32 location, Int32 count, bool transpose, Single* value);

	public unsafe delegate void glUniformSubroutinesuiv(Int32 shadertype, Int32 count, UInt32* indices);

	public delegate byte glUnmapBuffer(Int32 target);

	public delegate byte glUnmapNamedBuffer(UInt32 buffer);

	public delegate void glUseProgram(UInt32 program);

	public delegate void glUseProgramStages(UInt32 pipeline, Int32 stages, UInt32 program);

	public delegate void glValidateProgram(UInt32 program);

	public delegate void glValidateProgramPipeline(UInt32 pipeline);

	public delegate void glVertexArrayAttribBinding(UInt32 vaobj, UInt32 attribindex, UInt32 bindingindex);

	public delegate void glVertexArrayAttribFormat(UInt32 vaobj, UInt32 attribindex, Int32 size, Int32 type, bool normalized, UInt32 relativeoffset);

	public delegate void glVertexArrayAttribIFormat(UInt32 vaobj, UInt32 attribindex, Int32 size, Int32 type, UInt32 relativeoffset);

	public delegate void glVertexArrayAttribLFormat(UInt32 vaobj, UInt32 attribindex, Int32 size, Int32 type, UInt32 relativeoffset);

	public delegate void glVertexArrayBindingDivisor(UInt32 vaobj, UInt32 bindingindex, UInt32 divisor);

	public delegate void glVertexArrayElementBuffer(UInt32 vaobj, UInt32 buffer);

	public delegate void glVertexArrayVertexBuffer(UInt32 vaobj, UInt32 bindingindex, UInt32 buffer, IntPtr offset, Int32 stride);

	public unsafe delegate void glVertexArrayVertexBuffers(UInt32 vaobj, UInt32 first, Int32 count, UInt32* buffers, IntPtr* offsets, Int32* strides);

	public delegate void glVertexAttrib1d(UInt32 index, Double x);

	public unsafe delegate void glVertexAttrib1dv(UInt32 index, Double* v);

	public delegate void glVertexAttrib1f(UInt32 index, Single x);

	public unsafe delegate void glVertexAttrib1fv(UInt32 index, Single* v);

	public delegate void glVertexAttrib1s(UInt32 index, Int16 x);

	public unsafe delegate void glVertexAttrib1sv(UInt32 index, Int16* v);

	public delegate void glVertexAttrib2d(UInt32 index, Double x, Double y);

	public unsafe delegate void glVertexAttrib2dv(UInt32 index, Double* v);

	public delegate void glVertexAttrib2f(UInt32 index, Single x, Single y);

	public unsafe delegate void glVertexAttrib2fv(UInt32 index, Single* v);

	public delegate void glVertexAttrib2s(UInt32 index, Int16 x, Int16 y);

	public unsafe delegate void glVertexAttrib2sv(UInt32 index, Int16* v);

	public delegate void glVertexAttrib3d(UInt32 index, Double x, Double y, Double z);

	public unsafe delegate void glVertexAttrib3dv(UInt32 index, Double* v);

	public delegate void glVertexAttrib3f(UInt32 index, Single x, Single y, Single z);

	public unsafe delegate void glVertexAttrib3fv(UInt32 index, Single* v);

	public delegate void glVertexAttrib3s(UInt32 index, Int16 x, Int16 y, Int16 z);

	public unsafe delegate void glVertexAttrib3sv(UInt32 index, Int16* v);

	public unsafe delegate void glVertexAttrib4bv(UInt32 index, SByte* v);

	public delegate void glVertexAttrib4d(UInt32 index, Double x, Double y, Double z, Double w);

	public unsafe delegate void glVertexAttrib4dv(UInt32 index, Double* v);

	public delegate void glVertexAttrib4f(UInt32 index, Single x, Single y, Single z, Single w);

	public unsafe delegate void glVertexAttrib4fv(UInt32 index, Single* v);

	public unsafe delegate void glVertexAttrib4iv(UInt32 index, Int32* v);

	public unsafe delegate void glVertexAttrib4Nbv(UInt32 index, SByte* v);

	public unsafe delegate void glVertexAttrib4Niv(UInt32 index, Int32* v);

	public unsafe delegate void glVertexAttrib4Nsv(UInt32 index, Int16* v);

	public delegate void glVertexAttrib4Nub(UInt32 index, Byte x, Byte y, Byte z, Byte w);

	public unsafe delegate void glVertexAttrib4Nubv(UInt32 index, Byte* v);

	public unsafe delegate void glVertexAttrib4Nuiv(UInt32 index, UInt32* v);

	public unsafe delegate void glVertexAttrib4Nusv(UInt32 index, UInt16* v);

	public delegate void glVertexAttrib4s(UInt32 index, Int16 x, Int16 y, Int16 z, Int16 w);

	public unsafe delegate void glVertexAttrib4sv(UInt32 index, Int16* v);

	public unsafe delegate void glVertexAttrib4ubv(UInt32 index, Byte* v);

	public unsafe delegate void glVertexAttrib4uiv(UInt32 index, UInt32* v);

	public unsafe delegate void glVertexAttrib4usv(UInt32 index, UInt16* v);

	public delegate void glVertexAttribBinding(UInt32 attribindex, UInt32 bindingindex);

	public delegate void glVertexAttribDivisor(UInt32 index, UInt32 divisor);

	public delegate void glVertexAttribFormat(UInt32 attribindex, Int32 size, Int32 type, bool normalized, UInt32 relativeoffset);

	public delegate void glVertexAttribI1i(UInt32 index, Int32 x);

	public unsafe delegate void glVertexAttribI1iv(UInt32 index, Int32* v);

	public delegate void glVertexAttribI1ui(UInt32 index, UInt32 x);

	public unsafe delegate void glVertexAttribI1uiv(UInt32 index, UInt32* v);

	public delegate void glVertexAttribI2i(UInt32 index, Int32 x, Int32 y);

	public unsafe delegate void glVertexAttribI2iv(UInt32 index, Int32* v);

	public delegate void glVertexAttribI2ui(UInt32 index, UInt32 x, UInt32 y);

	public unsafe delegate void glVertexAttribI2uiv(UInt32 index, UInt32* v);

	public delegate void glVertexAttribI3i(UInt32 index, Int32 x, Int32 y, Int32 z);

	public unsafe delegate void glVertexAttribI3iv(UInt32 index, Int32* v);

	public delegate void glVertexAttribI3ui(UInt32 index, UInt32 x, UInt32 y, UInt32 z);

	public unsafe delegate void glVertexAttribI3uiv(UInt32 index, UInt32* v);

	public unsafe delegate void glVertexAttribI4bv(UInt32 index, SByte* v);

	public delegate void glVertexAttribI4i(UInt32 index, Int32 x, Int32 y, Int32 z, Int32 w);

	public unsafe delegate void glVertexAttribI4iv(UInt32 index, Int32* v);

	public unsafe delegate void glVertexAttribI4sv(UInt32 index, Int16* v);

	public unsafe delegate void glVertexAttribI4ubv(UInt32 index, Byte* v);

	public delegate void glVertexAttribI4ui(UInt32 index, UInt32 x, UInt32 y, UInt32 z, UInt32 w);

	public unsafe delegate void glVertexAttribI4uiv(UInt32 index, UInt32* v);

	public unsafe delegate void glVertexAttribI4usv(UInt32 index, UInt16* v);

	public delegate void glVertexAttribIFormat(UInt32 attribindex, Int32 size, Int32 type, UInt32 relativeoffset);

	public delegate void glVertexAttribIPointer(UInt32 index, Int32 size, Int32 type, Int32 stride, IntPtr pointer);

	public delegate void glVertexAttribL1d(UInt32 index, Double x);

	public unsafe delegate void glVertexAttribL1dv(UInt32 index, Double* v);

	public delegate void glVertexAttribL2d(UInt32 index, Double x, Double y);

	public unsafe delegate void glVertexAttribL2dv(UInt32 index, Double* v);

	public delegate void glVertexAttribL3d(UInt32 index, Double x, Double y, Double z);

	public unsafe delegate void glVertexAttribL3dv(UInt32 index, Double* v);

	public delegate void glVertexAttribL4d(UInt32 index, Double x, Double y, Double z, Double w);

	public unsafe delegate void glVertexAttribL4dv(UInt32 index, Double* v);

	public delegate void glVertexAttribLFormat(UInt32 attribindex, Int32 size, Int32 type, UInt32 relativeoffset);

	public delegate void glVertexAttribLPointer(UInt32 index, Int32 size, Int32 type, Int32 stride, IntPtr pointer);

	public delegate void glVertexAttribP1ui(UInt32 index, Int32 type, bool normalized, UInt32 value);

	public unsafe delegate void glVertexAttribP1uiv(UInt32 index, Int32 type, bool normalized, UInt32* value);

	public delegate void glVertexAttribP2ui(UInt32 index, Int32 type, bool normalized, UInt32 value);

	public unsafe delegate void glVertexAttribP2uiv(UInt32 index, Int32 type, bool normalized, UInt32* value);

	public delegate void glVertexAttribP3ui(UInt32 index, Int32 type, bool normalized, UInt32 value);

	public unsafe delegate void glVertexAttribP3uiv(UInt32 index, Int32 type, bool normalized, UInt32* value);

	public delegate void glVertexAttribP4ui(UInt32 index, Int32 type, bool normalized, UInt32 value);

	public unsafe delegate void glVertexAttribP4uiv(UInt32 index, Int32 type, bool normalized, UInt32* value);

	public delegate void glVertexAttribPointer(UInt32 index, Int32 size, Int32 type, bool normalized, Int32 stride, IntPtr pointer);

	public delegate void glVertexBindingDivisor(UInt32 bindingindex, UInt32 divisor);

	public delegate void glVertexP2ui(Int32 type, UInt32 value);

	public unsafe delegate void glVertexP2uiv(Int32 type, UInt32* value);

	public delegate void glVertexP3ui(Int32 type, UInt32 value);

	public unsafe delegate void glVertexP3uiv(Int32 type, UInt32* value);

	public delegate void glVertexP4ui(Int32 type, UInt32 value);

	public unsafe delegate void glVertexP4uiv(Int32 type, UInt32* value);

	public delegate void glViewport(Int32 x, Int32 y, Int32 width, Int32 height);

	public unsafe delegate void glViewportArrayv(UInt32 first, Int32 count, Single* v);

	public delegate void glViewportIndexedf(UInt32 index, Single x, Single y, Single w, Single h);

	public unsafe delegate void glViewportIndexedfv(UInt32 index, Single* v);

	public delegate Int32 glWaitSync(IntPtr sync, Int32 flags, UInt64 timeout);

	public delegate void glNamedBufferPageCommitmentEXT(UInt32 buffer, IntPtr offset, Int32 size, bool commit);

	public delegate void glDebugMessageCallbackKHR(DebugProc callback, IntPtr userParam);

	public unsafe delegate void glDebugMessageControlKHR(Int32 source, Int32 type, Int32 severity, Int32 count, UInt32* ids, bool enabled);

	public delegate void glDebugMessageInsertKHR(Int32 source, Int32 type, UInt32 id, Int32 severity, Int32 length, IntPtr buf);

	public unsafe delegate Int32 glGetDebugMessageLogKHR(UInt32 count, Int32 bufSize, [OutAttribute] Int32* sources, [OutAttribute] Int32* types, [OutAttribute] UInt32* ids, [OutAttribute] Int32* severities, [OutAttribute] Int32* lengths, [OutAttribute] IntPtr messageLog);

	public delegate Int32 glGetGraphicsResetStatusKHR();

	public unsafe delegate void glGetnUniformfvKHR(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Single* @params);

	public unsafe delegate void glGetnUniformivKHR(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] Int32* @params);

	public unsafe delegate void glGetnUniformuivKHR(UInt32 program, Int32 location, Int32 bufSize, [OutAttribute] UInt32* @params);

	public unsafe delegate void glGetObjectLabelKHR(Int32 identifier, UInt32 name, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr label);

	public unsafe delegate void glGetObjectPtrLabelKHR(IntPtr ptr, Int32 bufSize, [OutAttribute] Int32* length, [OutAttribute] IntPtr label);

	public delegate void glGetPointervKHR(Int32 pname, [OutAttribute] IntPtr @params);

	public delegate void glObjectLabelKHR(Int32 identifier, UInt32 name, Int32 length, IntPtr label);

	public delegate void glObjectPtrLabelKHR(IntPtr ptr, Int32 length, IntPtr label);

	public delegate void glPopDebugGroupKHR();

	public delegate void glPushDebugGroupKHR(Int32 source, UInt32 id, Int32 length, IntPtr message);

	public delegate void glReadnPixelsKHR(Int32 x, Int32 y, Int32 width, Int32 height, Int32 format, Int32 type, Int32 bufSize, [OutAttribute] IntPtr data);
}
