﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// OpenGL wrappers.
	/// </summary>
	public partial class GL
	{
		public ErrorCode GetError()
		{
			return (ErrorCode)this.Bindings.glGetError();
		}

		public void Viewport(int x, int y, int width, int height)
		{
			this.Bindings.glViewport(x, y, width, height);
		}

		public void VertexAttribPointer(uint index, int size, VertexAttribPointerType type, bool normalized, int stride, IntPtr pointer)
		{
			this.Bindings.glVertexAttribPointer(index, size, (int)type, normalized, stride, pointer);
		}

		public void VertexAttribIPointer(uint index, int size, VertexAttribIntegerType type, int stride, IntPtr pointer)
		{
			this.Bindings.glVertexAttribIPointer(index, size, (int)type, stride, pointer);
		}

		public void UseProgram(uint program)
		{
			this.Bindings.glUseProgram(program);
		}

		public void UnmapBuffer(BufferTarget target)
		{
			this.Bindings.glUnmapBuffer((int)target);
		}

		public unsafe void UniformMatrix4x3(int location, int count, bool transpose, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniformMatrix4x3fv(location, count, transpose, valuePtr);
		}

		public unsafe void UniformMatrix4x2(int location, int count, bool transpose, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniformMatrix4x2fv(location, count, transpose, valuePtr);
		}

		public unsafe void UniformMatrix4(int location, int count, bool transpose, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniformMatrix4fv(location, count, transpose, valuePtr);
		}

		public unsafe void UniformMatrix3x4(int location, int count, bool transpose, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniformMatrix3x4fv(location, count, transpose, valuePtr);
		}

		public unsafe void UniformMatrix3x2(int location, int count, bool transpose, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniformMatrix3x2fv(location, count, transpose, valuePtr);
		}

		public unsafe void UniformMatrix3(int location, int count, bool transpose, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniformMatrix3fv(location, count, transpose, valuePtr);
		}

		public unsafe void UniformMatrix2x4(int location, int count, bool transpose, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniformMatrix2x4fv(location, count, transpose, valuePtr);
		}

		public unsafe void UniformMatrix2x3(int location, int count, bool transpose, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniformMatrix2x3fv(location, count, transpose, valuePtr);
		}

		public unsafe void UniformMatrix2(int location, int count, bool transpose, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniformMatrix2fv(location, count, transpose, valuePtr);
		}

		public void UniformBlockBinding(uint program, uint uniformBlockIndex, uint binding)
		{
			this.Bindings.glUniformBlockBinding(program, uniformBlockIndex, binding);
		}

		public void BindBuffer(BufferTarget target, uint buffer)
		{
			this.Bindings.glBindBuffer((int)target, buffer);
		}

		public void TexSubImage1D(TextureTarget target, int level, int xoffset, int width, PixelFormat format, PixelType type, IntPtr pixels)
		{
			this.Bindings.glTexSubImage1D((int)target, level, xoffset, width, (int)format, (int)type, pixels);
		}

		public void TexSubImage2D(TextureTarget target, int level, int xoffset, int yoffset, int width, int height, PixelFormat format, PixelType type, IntPtr pixels)
		{
			this.Bindings.glTexSubImage2D((int)target, level, xoffset, yoffset, width, height, (int)format, (int)type, pixels);
		}

		public void TexSubImage3D(TextureTarget target, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, IntPtr pixels)
		{
			this.Bindings.glTexSubImage3D((int)target, level, xoffset, yoffset, zoffset, width, height, depth, (int)format, (int)type, pixels);
		}

		public void TexParameter(TextureTarget target, TextureParameterName pname, int value)
		{
			this.Bindings.glTexParameteri((int)target, (int)pname, value);
		}

		public void TexParameter(TextureTarget target, TextureParameterName pname, float value)
		{
			this.Bindings.glTexParameterf((int)target, (int)pname, value);
		}

		public unsafe void TexParameter(TextureTarget target, TextureParameterName pname, int[] value)
		{
			fixed (int* ptr = &value[0])
			{
				this.Bindings.glTexParameteriv((int)target, (int)pname, ptr);
			}
		}

		public unsafe void TexParameter(TextureTarget target, TextureParameterName pname, ReadOnlySpan<int> value)
		{
			fixed (int* ptr = &value[0])
			{
				this.Bindings.glTexParameteriv((int)target, (int)pname, ptr);
			}
		}

		public unsafe void TexParameter(TextureTarget target, TextureParameterName pname, float[] value)
		{
			fixed (float* ptr = &value[0])
			{
				this.Bindings.glTexParameterfv((int)target, (int)pname, ptr);
			}
		}

		public unsafe void TexParameter(TextureTarget target, TextureParameterName pname, ReadOnlySpan<float> value)
		{
			fixed (float* ptr = &value[0])
			{
				this.Bindings.glTexParameterfv((int)target, (int)pname, ptr);
			}
		}

		public void TexImage1D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int border, PixelFormat format, PixelType type, IntPtr pixels)
		{
			this.Bindings.glTexImage1D((int)target, level, (int)internalFormat, width, border, (int)format, (int)type, pixels);
		}

		public void TexImage2D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int height, int border, PixelFormat format, PixelType type, IntPtr pixels)
		{
			this.Bindings.glTexImage2D((int)target, level, (int)internalFormat, width, height, border, (int)format, (int)type, pixels);
		}

		public void TexImage3D(TextureTarget target, int level, PixelInternalFormat internalFormat, int width, int height, int depth, int border, PixelFormat format, PixelType type, IntPtr pixels)
		{
			this.Bindings.glTexImage3D((int)target, level, (int)internalFormat, width, height, depth, border, (int)format, (int)type, pixels);
		}

		public void TexImage2DMultisample(TextureTarget target, int samples, PixelInternalFormat internalFormat, int width, int height, bool fixedSampleLocations)
		{
			this.Bindings.glTexImage2DMultisample((int)target, samples, (int)internalFormat, width, height, fixedSampleLocations);
		}

		public void StencilOp(StencilOp fail, StencilOp zfail, StencilOp zpass)
		{
			this.Bindings.glStencilOp((int)fail, (int)zfail, (int)zpass);
		}

		public void StencilFunc(StencilFunction func, int @ref, uint mask)
		{
			this.Bindings.glStencilFunc((int)func, @ref, mask);
		}

		public void StencilOpSeparate(StencilFace face, StencilOp fail, StencilOp zfail, StencilOp zpass)
		{
			this.Bindings.glStencilOpSeparate((int)face, (int)fail, (int)zfail, (int)zpass);
		}

		public void StencilFuncSeparate(StencilFace face, StencilFunction func, int @ref, uint mask)
		{
			this.Bindings.glStencilFuncSeparate((int)face, (int)func, @ref, mask);
		}

		public void DepthFunc(DepthFunction func)
		{
			this.Bindings.glDepthFunc((int)func);
		}

		public void StencilMaskSeparate(StencilFace face, uint mask)
		{
			this.Bindings.glStencilMaskSeparate((int)face, mask);
		}

		public void StencilMask(uint mask)
		{
			this.Bindings.glStencilMask(mask);
		}

		public unsafe void ShaderSource(uint shader, int count, string[] sources, int[] length)
		{
			IntPtr array = GL.MarshalStringArrayToPtr(sources);

			try
			{
				fixed (int* lengthPtr = &length[0])
				{
					this.Bindings.glShaderSource(shader, 1, array, lengthPtr);
				}
			}
			finally
			{
				GL.FreeStringArrayPtr(array, sources.Length);
			}
		}

		public void Scissor(int x, int y, int width, int height)
		{
			this.Bindings.glScissor(x, y, width, height);
		}

		public void SamplerParameter(uint sampler, SamplerParameterName pname, int value)
		{
			this.Bindings.glSamplerParameteri(sampler, (int)pname, value);
		}

		public void SamplerParameter(uint sampler, SamplerParameterName pname, float value)
		{
			this.Bindings.glSamplerParameterf(sampler, (int)pname, value);
		}
		
		public unsafe void SamplerParameter(uint sampler, SamplerParameterName pname, int[] value)
		{
			fixed (int* ptr = &value[0])
			{
				this.Bindings.glSamplerParameteriv(sampler, (int)pname, ptr);
			}
		}

		public unsafe void SamplerParameter(uint sampler, SamplerParameterName pname, float[] value)
		{
			fixed (float* ptr = &value[0])
			{
				this.Bindings.glSamplerParameterfv(sampler, (int)pname, ptr);
			}
		}
		
		public void PointSize(float size)
		{
			this.Bindings.glPointSize(size);
		}

		public void LineWidth(float width)
		{
			this.Bindings.glLineWidth(width);
		}
		
		public void PixelStore(PixelStoreParameter pname, int value)
		{
			this.Bindings.glPixelStorei((int)pname, value);
		}

		public void PixelStore(PixelStoreParameter pname, float value)
		{
			this.Bindings.glPixelStoref((int)pname, value);
		}

		public IntPtr MapBufferRange(BufferTarget target, IntPtr offset, IntPtr length, BufferAccessMask access)
		{
			return this.Bindings.glMapBufferRange((int)target, offset, length, (int)access);
		}

		public IntPtr MapBuffer(BufferTarget target, BufferAccess mask)
		{
			return this.Bindings.glMapBuffer((int)target, (int)mask);
		}

		public void LinkProgram(uint program)
		{
			this.Bindings.glLinkProgram(program);
		}

		public unsafe void GetProgram(uint program, GetProgramParameterName pname, out int value)
		{
			fixed (int* valuePtr = &value)
				this.Bindings.glGetProgramiv(program, (int)pname, valuePtr);
		}

		public unsafe void GetShader(uint shader, ShaderParameter pname, out int value)
		{
			fixed (int* valuePtr = &value)
				this.Bindings.glGetShaderiv(shader, (int)pname, valuePtr);
		}

		public unsafe string GetProgramInfoLog(uint program)
		{
			int length;
			GetProgram(program, GetProgramParameterName.InfoLogLength, out length);
			if (length == 0)
				return string.Empty;

			byte[] buffer = new byte[length];
			GCHandle bufferHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

			try
			{
				int actualLength;
				this.Bindings.glGetProgramInfoLog(program, length, &actualLength, bufferHandle.AddrOfPinnedObject());
				return Encoding.Default.GetString(buffer, 0, actualLength);
			}
			finally
			{
				bufferHandle.Free();
			}
		}

		public unsafe string GetShaderInfoLog(uint shader)
		{
			int length;
			GetShader(shader, ShaderParameter.InfoLogLength, out length);
			if (length == 0)
				return string.Empty;

			byte[] buffer = new byte[length];
			GCHandle bufferHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

			try
			{
				int actualLength;
				this.Bindings.glGetShaderInfoLog(shader, length, &actualLength, bufferHandle.AddrOfPinnedObject());
				return Encoding.Default.GetString(buffer, 0, actualLength);
			}
			finally
			{
				bufferHandle.Free();
			}
		}

		public int GetUniformLocation(uint program, string name)
		{
			IntPtr str = Marshal.StringToHGlobalAnsi(name);

			try
			{
				return this.Bindings.glGetUniformLocation(program, str);
			}
			finally
			{
				Marshal.FreeHGlobal(str);
			}
		}

		public void GetTexImage(TextureTarget target, int level, PixelFormat format, PixelType type, IntPtr pixels)
		{
			this.Bindings.glGetTexImage((int)target, level, (int)format, (int)type, pixels);
		}

		public string GetString(StringName pname)
		{
			IntPtr ptr = this.Bindings.glGetString((int)pname);
			return Marshal.PtrToStringAnsi(ptr);
		}

		public string GetString(StringNameIndexed pname, uint index)
		{
			IntPtr ptr = this.Bindings.glGetStringi((int)pname, index);
			return Marshal.PtrToStringAnsi(ptr);
		}

		public unsafe int GetInteger(GetPName pname)
		{
			int value;
			this.Bindings.glGetIntegerv((int)pname, &value);
			return value;
		}

		public unsafe float GetFloat(GetPName pname)
		{
			float value;
			this.Bindings.glGetFloatv((int)pname, &value);
			return value;
		}

		public void GetBufferSubData(BufferTarget target, IntPtr offset, IntPtr size, IntPtr data)
		{
			this.Bindings.glGetBufferSubData((int)target, offset, size, data);
		}

		public unsafe void GetActiveUniforms(uint program, int uniformCount, ref uint uniformIndices, ActiveUniformParameter pname, out int value)
		{
			fixed (uint* uniformIndicesPtr = &uniformIndices)
			fixed (int* valuePtr = &value)
				this.Bindings.glGetActiveUniformsiv(program, uniformCount, uniformIndicesPtr, (int)pname, valuePtr);
		}

		public unsafe void GetActiveUniformBlockName(uint program, uint uniformBlockIndex, int bufSize, out int length, StringBuilder sb)
		{
			IntPtr buf = Marshal.AllocHGlobal(sb.Capacity);

			try
			{
				int tempLength;

				this.Bindings.glGetActiveUniformBlockName(program, uniformBlockIndex, bufSize, &tempLength, buf);
				GL.MarshalPtrToStringBuilder(buf, sb);

				length = tempLength;
			}
			finally
			{
				Marshal.FreeHGlobal(buf);
			}
		}

		public unsafe void GetActiveUniformBlock(uint program, uint uniformBlockIndex, ActiveUniformBlockParameter pname, out int value)
		{
			fixed (int* valuePtr = &value)
				this.Bindings.glGetActiveUniformBlockiv(program, uniformBlockIndex, (int)pname, valuePtr);
		}

		public unsafe void GetActiveUniform(uint program, uint index, int bufSize, out int length, out int size, out ActiveUniformType type, StringBuilder name)
		{
			IntPtr nameBuf = Marshal.AllocHGlobal(name.Capacity);

			try
			{
				int tempLength;
				int tempSize;
				int tempType;

				this.Bindings.glGetActiveUniform(program, index, bufSize, &tempLength, &tempSize, &tempType, nameBuf);
				GL.MarshalPtrToStringBuilder(nameBuf, name);

				length = tempLength;
				size = tempSize;
				type = (ActiveUniformType)tempType;
			}
			finally
			{
				Marshal.FreeHGlobal(nameBuf);
			}
		}
		
		public void FramebufferTexture1D(FramebufferTarget target, FramebufferAttachment attachment, TextureTarget textureTarget, uint texture, int level)
		{
			this.Bindings.glFramebufferTexture1D((int)target, (int)attachment, (int)textureTarget, texture, level);
		}

		public void FramebufferTexture2D(FramebufferTarget target, FramebufferAttachment attachment, TextureTarget textureTarget, uint texture, int level)
		{
			this.Bindings.glFramebufferTexture2D((int)target, (int)attachment, (int)textureTarget, texture, level);
		}

		public void FramebufferTexture3D(FramebufferTarget target, FramebufferAttachment attachment, TextureTarget textureTarget, uint texture, int level, int zoffset)
		{
			this.Bindings.glFramebufferTexture3D((int)target, (int)attachment, (int)textureTarget, texture, level, zoffset);
		}

		public void FramebufferTextureLayer(FramebufferTarget target, FramebufferAttachment attachment, uint texture, int level, int layer)
		{
			this.Bindings.glFramebufferTextureLayer((int)target, (int)attachment, texture, level, layer);
		}

		public void EnableVertexAttribArray(uint index)
		{
			this.Bindings.glEnableVertexAttribArray(index);
		}

		public void DisableVertexAttribArray(uint index)
		{
			this.Bindings.glDisableVertexAttribArray(index);
		}

		public void Enable(EnableCap cap)
		{
			this.Bindings.glEnable((int)cap);
		}

		public void Disable(EnableCap cap)
		{
			this.Bindings.glDisable((int)cap);
		}

		public void DrawElementsInstancedBaseVertexBaseInstance(PrimitiveType mode, int count, DrawElementsType type, IntPtr indices, int instanceCount, int baseVertex, uint baseInstance)
		{
			this.Bindings.glDrawElementsInstancedBaseVertexBaseInstance((int)mode, count, (int)type, indices, instanceCount, baseVertex, baseInstance);
		}

		public void DrawElementsInstancedBaseVertex(PrimitiveType mode, int count, DrawElementsType type, IntPtr indices, int instanceCount, int baseVertex)
		{
			this.Bindings.glDrawElementsInstancedBaseVertex((int)mode, count, (int)type, indices, instanceCount, baseVertex);
		}

		public void DrawElementsInstancedBaseInstance(PrimitiveType mode, int count, DrawElementsType type, IntPtr indices, int instanceCount, uint baseInstance)
		{
			this.Bindings.glDrawElementsInstancedBaseInstance((int)mode, count, (int)type, indices, instanceCount, baseInstance);
		}

		public void DrawElementsInstanced(PrimitiveType mode, int count, DrawElementsType type, IntPtr indices, int instanceCount)
		{
			this.Bindings.glDrawElementsInstanced((int)mode, count, (int)type, indices, instanceCount);
		}

		public void DrawElements(PrimitiveType mode, int count, DrawElementsType type, IntPtr indices)
		{
			this.Bindings.glDrawElements((int)mode, count, (int)type, indices);
		}

		public void DrawElementsBaseVertex(PrimitiveType mode, int count, DrawElementsType type, IntPtr indices, int baseVertex)
		{
			this.Bindings.glDrawElementsBaseVertex((int)mode, count, (int)type, indices, baseVertex);
		}

		public void DrawArraysInstancedBaseInstance(PrimitiveType mode, int first, int count, int instanceCount, uint baseInstance)
		{
			this.Bindings.glDrawArraysInstancedBaseInstance((int)mode, first, count, instanceCount, baseInstance);
		}

		public void DrawArraysInstanced(PrimitiveType mode, int first, int count, int instanceCount)
		{
			this.Bindings.glDrawArraysInstanced((int)mode, first, count, instanceCount);
		}

		public void DrawArrays(PrimitiveType mode, int first, int count)
		{
			this.Bindings.glDrawArrays((int)mode, first, count);
		}

		public void AttachShader(uint program, uint shader)
		{
			this.Bindings.glAttachShader(program, shader);
		}

		public void DetachShader(uint program, uint shader)
		{
			this.Bindings.glDetachShader(program, shader);
		}

		public void DepthRange(float near, float far)
		{
			this.Bindings.glDepthRangef(near, far);
		}

		public void DepthRange(double near, double far)
		{
			this.Bindings.glDepthRange(near, far);
		}

		public void DepthMask(bool flag)
		{
			this.Bindings.glDepthMask(flag);
		}

		public void DebugMessageCallback(DebugProc callback, IntPtr userParam)
		{
			this.Bindings.glDebugMessageCallback(callback, userParam);
		}

		public void CullFace(CullFaceMode mode)
		{
			this.Bindings.glCullFace((int)mode);
		}

		public void CompileShader(uint shader)
		{
			this.Bindings.glCompileShader(shader);
		}

		public void ColorMask(bool red, bool green, bool blue, bool alpha)
		{
			this.Bindings.glColorMask(red, green, blue, alpha);
		}

		public void ClearStencil(int s)
		{
			this.Bindings.glClearStencil(s);
		}

		public void ClearDepth(float depth)
		{
			this.Bindings.glClearDepthf(depth);
		}

		public void ClearDepth(double depth)
		{
			this.Bindings.glClearDepth(depth);
		}

		public void ClearColor(float red, float green, float blue, float alpha)
		{
			this.Bindings.glClearColor(red, green, blue, alpha);
		}

		public void Clear(ClearBufferMask mask)
		{
			this.Bindings.glClear((int)mask);
		}

		public FramebufferErrorCode CheckFramebufferStatus(FramebufferTarget target)
		{
			return (FramebufferErrorCode)this.Bindings.glCheckFramebufferStatus((int)target);
		}

		public void BufferSubData(BufferTarget target, IntPtr offset, IntPtr size, IntPtr data)
		{
			this.Bindings.glBufferSubData((int)target, offset, size, data);
		}

		public void BufferData(BufferTarget target, IntPtr size, IntPtr data, BufferUsageHint usage)
		{
			this.Bindings.glBufferData((int)target, size, data, (int)usage);
		}

		public void BlendFuncSeparate(BlendingFactorSrc sfactorRGB, BlendingFactorDest dfactorRGB, BlendingFactorSrc sfactorAlpha, BlendingFactorDest dfactorAlpha)
		{
			this.Bindings.glBlendFuncSeparate((int)sfactorRGB, (int)dfactorRGB, (int)sfactorAlpha, (int)dfactorAlpha);
		}

		public void BlendFunc(BlendingFactorSrc sfactor, BlendingFactorDest dfactor)
		{
			this.Bindings.glBlendFunc((int)sfactor, (int)dfactor);
		}

		public void BlendEquation(BlendEquationMode mode)
		{
			this.Bindings.glBlendEquation((int)mode);
		}

		public void BlendEquationSeparate(BlendEquationMode modeRGB, BlendEquationMode modeAlpha)
		{
			this.Bindings.glBlendEquationSeparate((int)modeRGB, (int)modeAlpha);
		}

		public void BlendColor(float red, float green, float blue, float alpha)
		{
			this.Bindings.glBlendColor(red, green, blue, alpha);
		}

		public void BindVertexArray(uint array)
		{
			this.Bindings.glBindVertexArray(array);
		}

		public void BindTexture(TextureTarget target, uint texture)
		{
			this.Bindings.glBindTexture((int)target, texture);
		}

		public void BindSampler(uint unit, uint sampler)
		{
			this.Bindings.glBindSampler(unit, sampler);
		}

		public void BindFramebuffer(FramebufferTarget target, uint framebuffer)
		{
			this.Bindings.glBindFramebuffer((int)target, framebuffer);
		}

		public void BindBufferRange(BufferRangeTarget target, uint index, uint buffer, IntPtr offset, IntPtr size)
		{
			this.Bindings.glBindBufferRange((int)target, index, buffer, offset, size);
		}

		public void BindRenderbuffer(RenderbufferTarget target, uint renderbuffer)
		{
			this.Bindings.glBindRenderbuffer((int)target, renderbuffer);
		}

		public void ActiveTexture(TextureUnit texture)
		{
			this.Bindings.glActiveTexture((int)texture);
		}

		public void CopyTexSubImage1D(TextureTarget target, int level, int xoffset, int x, int y, int width)
		{
			this.Bindings.glCopyTexSubImage1D((int)target, level, xoffset, x, y, width);
		}

		public void CopyTexSubImage2D(TextureTarget target, int level, int xoffset, int yoffset, int x, int y, int width, int height)
		{
			this.Bindings.glCopyTexSubImage2D((int)target, level, xoffset, yoffset, x, y, width, height);
		}

		public void CopyTexSubImage3D(TextureTarget target, int level, int xoffset, int yoffset, int zoffset, int x, int y, int width, int height)
		{
			this.Bindings.glCopyTexSubImage3D((int)target, level, xoffset, yoffset, zoffset, x, y, width, height);
		}

		public void ReadBuffer(ReadBufferMode src)
		{
			this.Bindings.glReadBuffer((int)src);
		}

		public void DrawBuffer(DrawBuffersEnum buf)
		{
			this.Bindings.glDrawBuffer((int)buf);
		}

		public unsafe void DrawBuffers(int n, DrawBufferMode[] bufs)
		{
			fixed (DrawBufferMode* bufsPtr = &bufs[0])
			{
				this.Bindings.glDrawBuffers(n, (int*)bufsPtr);
			}
		}

		public unsafe void DrawBuffers(int n, ReadOnlySpan<DrawBufferMode> bufs)
		{
			fixed (DrawBufferMode* bufsPtr = &bufs[0])
			{
				this.Bindings.glDrawBuffers(n, (int*)bufsPtr);
			}
		}

		public unsafe void MultiDrawArrays(PrimitiveType mode, int[] first, int[] count, int drawCount)
		{
			fixed (int* firstPtr = &first[0])
			fixed (int* countPtr = &count[0])
			{
				this.Bindings.glMultiDrawArrays((int)mode, firstPtr, countPtr, drawCount);
			}
		}

		public unsafe void MultiDrawArrays(PrimitiveType mode, ref int first, ref int count, int drawCount)
		{
			fixed (int* firstPtr = &first)
			fixed (int* countPtr = &count)
			{
				this.Bindings.glMultiDrawArrays((int)mode, firstPtr, countPtr, drawCount);
			}
		}

		public unsafe void MultiDrawElements(PrimitiveType mode, int[] count, DrawElementsType type, IntPtr[] indices, int drawCount)
		{
			fixed (int* countPtr = &count[0])
			fixed (IntPtr* indicesPtr = &indices[0])
			{
				this.Bindings.glMultiDrawElements((int)mode, countPtr, (int)type, indicesPtr, drawCount);
			}
		}

		public unsafe void MultiDrawElements(PrimitiveType mode, ref int count, DrawElementsType type, ref IntPtr indices, int drawCount)
		{
			fixed (int* countPtr = &count)
			fixed (IntPtr* indicesPtr = &indices)
			{
				this.Bindings.glMultiDrawElements((int)mode, countPtr, (int)type, indicesPtr, drawCount);
			}
		}

		public unsafe void MultiDrawElementsBaseVertex(PrimitiveType mode, int[] count, DrawElementsType type, IntPtr[] indices, int drawCount, int[] baseVertex)
		{
			fixed (int* countPtr = &count[0])
			fixed (int* baseVertexPtr = &baseVertex[0])
			fixed (IntPtr* indicesPtr = &indices[0])
			{
				this.Bindings.glMultiDrawElementsBaseVertex((int)mode, countPtr, (int)type, indicesPtr, drawCount, baseVertexPtr);
			}
		}

		public unsafe void MultiDrawElementsBaseVertex(PrimitiveType mode, ref int count, DrawElementsType type, ref IntPtr indices, int drawCount, ref int baseVertex)
		{
			fixed (int* countPtr = &count)
			fixed (int* baseVertexPtr = &baseVertex)
			fixed (IntPtr* indicesPtr = &indices)
			{
				this.Bindings.glMultiDrawElementsBaseVertex((int)mode, countPtr, (int)type, indicesPtr, drawCount, baseVertexPtr);
			}
		}

		public void BlitFramebuffer(int srcX0, int srcY0, int srcX1, int srcY1, int dstX0, int dstY0, int dstX1, int dstY1, ClearBufferMask mask, BlitFramebufferFilter filter)
		{
			this.Bindings.glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, (int)mask, (int)filter);
		}

		public void ClearTexImage(uint texture, int level, PixelFormat format, PixelType type, IntPtr data)
		{
			this.Bindings.glClearTexImage(texture, level, (int)format, (int)type, data);
		}

		public void ClearTexSubImage(uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, PixelFormat format, PixelType type, IntPtr data)
		{
			this.Bindings.glClearTexSubImage(texture, level, xoffset, yoffset, zoffset, width, height, depth, (int)format, (int)type, data);
		}

		public void SetEnabled(EnableCap cap, bool state)
		{
			if (state)
			{
				this.Enable(cap);
			}
			else
			{
				this.Disable(cap);
			}
		}

		public void RenderbufferStorage(RenderbufferTarget target, RenderbufferStorage internalFormat, int width, int height)
		{
			this.Bindings.glRenderbufferStorage((int)target, (int)internalFormat, width, height);
		}

		public void RenderbufferStorageMultisample(RenderbufferTarget target, int samples, RenderbufferStorage internalFormat, int width, int height)
		{
			this.Bindings.glRenderbufferStorageMultisample((int)target, samples, (int)internalFormat, width, height);
		}

		public void FramebufferRenderbuffer(FramebufferTarget target,FramebufferAttachment attachment, RenderbufferTarget renderbufferTarget, uint renderbuffer)
		{
			this.Bindings.glFramebufferRenderbuffer((int)target, (int)attachment, (int)renderbufferTarget, renderbuffer);
		}

		public void GenerateMipmap(GenerateMipmapTarget target)
		{
			this.Bindings.glGenerateMipmap((int)target);
		}

		public void ObjectLabel(ObjectLabelIdentifier identifier, uint name, string label)
		{
			if (label == null)
			{
				this.Bindings.glObjectLabel((int)identifier, name, 0, IntPtr.Zero);
				return;
			}
			
			IntPtr str = Marshal.StringToHGlobalAnsi(label);

			try
			{
				this.Bindings.glObjectLabel((int)identifier, name, label.Length, str);
			}
			finally
			{
				Marshal.FreeHGlobal(str);
			}
		}

		public unsafe void GetObjectLabel(ObjectLabelIdentifier identifier, uint name, int bufSize, out int length, StringBuilder label)
		{
			IntPtr buf = Marshal.AllocHGlobal(label.Capacity);

			try
			{
				int tempLength;

				this.Bindings.glGetObjectLabel((int)identifier, name, bufSize, &tempLength, buf);
				GL.MarshalPtrToStringBuilder(buf, label);

				length = tempLength;
			}
			finally
			{
				Marshal.FreeHGlobal(buf);
			}
		}

		public void PushDebugGroup(DebugSourceExternal source, uint id, string message)
		{
			if (message == null)
			{
				this.Bindings.glPushDebugGroup((int)source, id, 0, IntPtr.Zero);
				return;
			}
			
			IntPtr str = Marshal.StringToHGlobalAnsi(message);

			try
			{
				this.Bindings.glPushDebugGroup((int)source, id, message.Length, str);
			}
			finally
			{
				Marshal.FreeHGlobal(str);
			}
		}

		public void PopDebugGroup()
		{
			this.Bindings.glPopDebugGroup();
		}

		public void ReadPixels(int x, int y, int width, int height, PixelFormat format, PixelType type, IntPtr data)
		{
			this.Bindings.glReadPixels(x, y, width, height, (int)format, (int)type, data);
		}

		public void ReadnPixels(int x, int y, int width, int height, PixelFormat format, PixelType type, int bufSize, IntPtr data)
		{
			if(this.Bindings.glReadnPixels != null)
				this.Bindings.glReadnPixels(x, y, width, height, (int)format, (int)type, bufSize, data);
			else
				this.Bindings.glReadnPixelsARB(x, y, width, height, (int)format, (int)type, bufSize, data);
		}

		public bool IsBuffer(uint buffer)
		{
			return this.Bindings.glIsBuffer(buffer) != 0;
		}

		public bool IsShader(uint shader)
		{
			return this.Bindings.glIsShader(shader) != 0;
		}

		public bool IsProgram(uint program)
		{
			return this.Bindings.glIsProgram(program) != 0;
		}

		public bool IsVertexArray(uint array)
		{
			return this.Bindings.glIsVertexArray(array) != 0;
		}

		public bool IsSampler(uint sampler)
		{
			return this.Bindings.glIsSampler(sampler) != 0;
		}

		public bool IsTexture(uint texture)
		{
			return this.Bindings.glIsTexture(texture) != 0;
		}

		public bool IsRenderbuffer(uint renderbuffer)
		{
			return this.Bindings.glIsRenderbuffer(renderbuffer) != 0;
		}

		public bool IsFramebuffer(uint framebuffer)
		{
			return this.Bindings.glIsFramebuffer(framebuffer) != 0;
		}

		public IntPtr FenceSync(SyncCondition condition, int flags)
		{
			return this.Bindings.glFenceSync((int)condition, flags);
		}

		public void DeleteSync(IntPtr sync)
		{
			this.Bindings.glDeleteSync(sync);
		}

		public WaitSyncStatus WaitSync(IntPtr sync, WaitSyncFlags flags, ulong timeout)
		{
			return (WaitSyncStatus)this.Bindings.glWaitSync(sync, (int)flags, timeout);
		}

		public WaitSyncStatus ClientWaitSync(IntPtr sync, ClientWaitSyncFlags flags, ulong timeout)
		{
			return (WaitSyncStatus)this.Bindings.glClientWaitSync(sync, (int)flags, timeout);
		}

		public bool IsSync(IntPtr sync)
		{
			return this.Bindings.glIsSync(sync) != 0;
		}

		public unsafe void GetSync(IntPtr sync, SyncParameterName pname, int bufSize, out int length, int[] values)
		{
			fixed (int* valuesPtr = &values[0])
			fixed (int* lengthPtr = &length)
			{
				this.Bindings.glGetSynciv(sync, (int)pname, bufSize, lengthPtr, valuesPtr);
			}
		}

		public unsafe void GetSync(IntPtr sync, SyncParameterName pname, int bufSize, out int length, ref int values)
		{
			fixed (int* valuesPtr = &values)
			fixed (int* lengthPtr = &length)
			{
				this.Bindings.glGetSynciv(sync, (int)pname, bufSize, lengthPtr, valuesPtr);
			}
		}

		public void CopyImageSubData(uint srcName, ImageTarget srcTarget, int srcLevel, int srcX, int srcY, int srcZ,
			uint dstName, ImageTarget dstTarget, int dstLevel, int dstX, int dstY, int dstZ,
			int srcWidth, int srcHeight, int srcDepth)
		{
			this.Bindings.glCopyImageSubData(srcName, (int)srcTarget, srcLevel, srcX, srcY, srcZ, dstName, (int)dstTarget, dstLevel, dstX, dstY, dstZ, srcWidth, srcHeight, srcDepth);
		}

		public unsafe void GetFramebufferAttachmentParameter(FramebufferTarget target, FramebufferAttachment attachment, FramebufferParameterName pname, out int value)
		{
			fixed (int* valuesPtr = &value)
			{
				this.Bindings.glGetFramebufferAttachmentParameteriv((int) target, (int) attachment, (int) pname, valuesPtr);
			}
		}

		public void Finish()
		{
			this.Bindings.glFinish();
		}

		public void Flush()
		{
			this.Bindings.glFlush();
		}

		#region Gen/Delete

		public unsafe uint GenVertexArray()
		{
			uint id;
			this.Bindings.glGenVertexArrays(1, &id);
			return id;
		}

		public unsafe void DeleteVertexArray(uint vertexArray)
		{
			this.Bindings.glDeleteVertexArrays(1, &vertexArray);
		}
		
		public unsafe uint GenTexture()
		{
			uint id;
			this.Bindings.glGenTextures(1, &id);
			return id;
		}

		public unsafe void DeleteTexture(uint texture)
		{
			this.Bindings.glDeleteTextures(1, &texture);
		}

		public unsafe uint GenBuffer()
		{
			uint id;
			this.Bindings.glGenBuffers(1, &id);
			return id;
		}

		public unsafe void DeleteBuffer(uint buffer)
		{
			this.Bindings.glDeleteBuffers(1, &buffer);
		}

		public unsafe uint GenSampler()
		{
			uint id;
			this.Bindings.glGenSamplers(1, &id);
			return id;
		}

		public unsafe void DeleteSampler(uint sampler)
		{
			this.Bindings.glDeleteSamplers(1, &sampler);
		}

		public unsafe uint GenFramebuffer()
		{
			uint id;
			this.Bindings.glGenFramebuffers(1, &id);
			return id;
		}

		public unsafe void DeleteFramebuffer(uint framebuffer)
		{
			this.Bindings.glDeleteFramebuffers(1, &framebuffer);
		}

		public unsafe uint GenRenderbuffer()
		{
			uint id;
			this.Bindings.glGenRenderbuffers(1, &id);
			return id;
		}

		public unsafe void DeleteRenderbuffer(uint renderbuffer)
		{
			this.Bindings.glDeleteRenderbuffers(1, &renderbuffer);
		}

		public int CreateProgram()
		{
			return this.Bindings.glCreateProgram();
		}

		public void DeleteProgram(uint program)
		{
			this.Bindings.glDeleteProgram(program);
		}

		public int CreateShader(ShaderType type)
		{
			return this.Bindings.glCreateShader((int)type);
		}

		public void DeleteShader(uint shader)
		{
			this.Bindings.glDeleteShader(shader);
		}

		#endregion

		#region Uniform1

		public void Uniform1(int location, float value)
		{
			this.Bindings.glUniform1f(location, value);
		}

		public void Uniform1(int location, int value)
		{
			this.Bindings.glUniform1i(location, value);
		}

		public void Uniform1(int location, uint value)
		{
			this.Bindings.glUniform1ui(location, value);
		}

		public void Uniform1(int location, double value)
		{
			this.Bindings.glUniform1d(location, value);
		}


		public unsafe void Uniform1(int location, int count, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniform1fv(location, count, valuePtr);
		}

		public unsafe void Uniform1(int location, int count, ref int value)
		{
			fixed (int* valuePtr = &value)
				this.Bindings.glUniform1iv(location, count, valuePtr);
		}

		public unsafe void Uniform1(int location, int count, ref uint value)
		{
			fixed (uint* valuePtr = &value)
				this.Bindings.glUniform1uiv(location, count, valuePtr);
		}

		public unsafe void Uniform1(int location, int count, ref double value)
		{
			fixed (double* valuePtr = &value)
				this.Bindings.glUniform1dv(location, count, valuePtr);
		}


		public unsafe void Uniform1(int location, int count, float[] values)
		{
			fixed (float* ptr = values)
				this.Bindings.glUniform1fv(location, count, ptr);
		}

		public unsafe void Uniform1(int location, int count, int[] values)
		{
			fixed (int* ptr = values)
				this.Bindings.glUniform1iv(location, count, ptr);
		}

		public unsafe void Uniform1(int location, int count, uint[] values)
		{
			fixed (uint* ptr = values)
				this.Bindings.glUniform1uiv(location, count, ptr);
		}

		public unsafe void Uniform1(int location, int count, double[] values)
		{
			fixed (double* ptr = values)
				this.Bindings.glUniform1dv(location, count, ptr);
		}

		#endregion

		#region Uniform2

		public void Uniform2(int location, float v0, float v1)
		{
			this.Bindings.glUniform2f(location, v0, v1);
		}

		public void Uniform2(int location, int v0, int v1)
		{
			this.Bindings.glUniform2i(location, v0, v1);
		}

		public void Uniform2(int location, uint v0, uint v1)
		{
			this.Bindings.glUniform2ui(location, v0, v1);
		}

		public void Uniform2(int location, double v0, double v1)
		{
			this.Bindings.glUniform2d(location, v0, v1);
		}


		public unsafe void Uniform2(int location, int count, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniform2fv(location, count, valuePtr);
		}

		public unsafe void Uniform2(int location, int count, ref int value)
		{
			fixed (int* valuePtr = &value)
				this.Bindings.glUniform2iv(location, count, valuePtr);
		}

		public unsafe void Uniform2(int location, int count, ref uint value)
		{
			fixed (uint* valuePtr = &value)
				this.Bindings.glUniform2uiv(location, count, valuePtr);
		}

		public unsafe void Uniform2(int location, int count, ref double value)
		{
			fixed (double* valuePtr = &value)
				this.Bindings.glUniform2dv(location, count, valuePtr);
		}


		public unsafe void Uniform2(int location, int count, float[] value)
		{
			fixed (float* valuePtr = value)
				this.Bindings.glUniform2fv(location, count, valuePtr);
		}

		public unsafe void Uniform2(int location, int count, int[] value)
		{
			fixed (int* valuePtr = value)
				this.Bindings.glUniform2iv(location, count, valuePtr);
		}

		public unsafe void Uniform2(int location, int count, uint[] value)
		{
			fixed (uint* valuePtr = value)
				this.Bindings.glUniform2uiv(location, count, valuePtr);
		}

		public unsafe void Uniform2(int location, int count, double[] value)
		{
			fixed (double* valuePtr = value)
				this.Bindings.glUniform2dv(location, count, valuePtr);
		}

		#endregion

		#region Uniform3

		public void Uniform3(int location, float v0, float v1, float v2)
		{
			this.Bindings.glUniform3f(location, v0, v1, v2);
		}

		public void Uniform3(int location, int v0, int v1, int v2)
		{
			this.Bindings.glUniform3i(location, v0, v1, v2);
		}

		public void Uniform3(int location, uint v0, uint v1, uint v2)
		{
			this.Bindings.glUniform3ui(location, v0, v1, v2);
		}

		public void Uniform3(int location, double v0, double v1, double v2)
		{
			this.Bindings.glUniform3d(location, v0, v1, v2);
		}


		public unsafe void Uniform3(int location, int count, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniform3fv(location, count, valuePtr);
		}

		public unsafe void Uniform3(int location, int count, ref int value)
		{
			fixed (int* valuePtr = &value)
				this.Bindings.glUniform3iv(location, count, valuePtr);
		}
		
		public unsafe void Uniform3(int location, int count, ref uint value)
		{
			fixed (uint* valuePtr = &value)
				this.Bindings.glUniform3uiv(location, count, valuePtr);
		}
		
		public unsafe void Uniform3(int location, int count, ref double value)
		{
			fixed (double* valuePtr = &value)
				this.Bindings.glUniform3dv(location, count, valuePtr);
		}


		public unsafe void Uniform3(int location, int count, float[] value)
		{
			fixed (float* valuePtr = value)
				this.Bindings.glUniform3fv(location, count, valuePtr);
		}

		public unsafe void Uniform3(int location, int count, int[] value)
		{
			fixed (int* valuePtr = value)
				this.Bindings.glUniform3iv(location, count, valuePtr);
		}
		
		public unsafe void Uniform3(int location, int count, uint[] value)
		{
			fixed (uint* valuePtr = value)
				this.Bindings.glUniform3uiv(location, count, valuePtr);
		}
		
		public unsafe void Uniform3(int location, int count, double[] value)
		{
			fixed (double* valuePtr = value)
				this.Bindings.glUniform3dv(location, count, valuePtr);
		}
		
		#endregion

		#region Uniform4

		public void Uniform4(int location, float v0, float v1, float v2, float v3)
		{
			this.Bindings.glUniform4f(location, v0, v1, v2, v3);
		}
		
		public void Uniform4(int location, int v0, int v1, int v2, int v3)
		{
			this.Bindings.glUniform4i(location, v0, v1, v2, v3);
		}

		public void Uniform4(int location, uint v0, uint v1, uint v2, uint v3)
		{
			this.Bindings.glUniform4ui(location, v0, v1, v2, v3);
		}

		public void Uniform4(int location, double v0, double v1, double v2, double v3)
		{
			this.Bindings.glUniform4d(location, v0, v1, v2, v3);
		}


		public unsafe void Uniform4(int location, int count, ref float value)
		{
			fixed (float* valuePtr = &value)
				this.Bindings.glUniform4fv(location, count, valuePtr);
		}
		
		public unsafe void Uniform4(int location, int count, ref int value)
		{
			fixed (int* valuePtr = &value)
				this.Bindings.glUniform4iv(location, count, valuePtr);
		}

		public unsafe void Uniform4(int location, int count, ref uint value)
		{
			fixed (uint* valuePtr = &value)
				this.Bindings.glUniform4uiv(location, count, valuePtr);
		}

		public unsafe void Uniform4(int location, int count, ref double value)
		{
			fixed (double* valuePtr = &value)
				this.Bindings.glUniform4dv(location, count, valuePtr);
		}


		public unsafe void Uniform4(int location, int count, float[] value)
		{
			fixed (float* valuePtr = value)
				this.Bindings.glUniform4fv(location, count, valuePtr);
		}
		
		public unsafe void Uniform4(int location, int count, int[] value)
		{
			fixed (int* valuePtr = value)
				this.Bindings.glUniform4iv(location, count, valuePtr);
		}

		public unsafe void Uniform4(int location, int count, uint[] value)
		{
			fixed (uint* valuePtr = value)
				this.Bindings.glUniform4uiv(location, count, valuePtr);
		}

		public unsafe void Uniform4(int location, int count, double[] value)
		{
			fixed (double* valuePtr = value)
				this.Bindings.glUniform4dv(location, count, valuePtr);
		}

		public void CopyBufferSubData(BufferTarget readTarget, BufferTarget writeTarget, IntPtr readOffset, IntPtr writeOffset, IntPtr size)
		{
			this.Bindings.glCopyBufferSubData((int)readTarget, (int)writeTarget, readOffset, writeOffset, size);
		}
		
		#endregion
	}
}
