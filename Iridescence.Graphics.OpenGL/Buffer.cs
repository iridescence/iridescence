﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL buffer object.
	/// </summary>
	public partial class Buffer : GLContextChild, IGLObject
	{
		#region Fields
	
		#endregion

		#region Properties

		/// <summary>
		/// Gets the OpenGL object ID.
		/// </summary>
		public uint ID { get; }

		/// <summary>
		/// Gets the size of the buffer.
		/// </summary>
		public int Size { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new OpenGL buffer with the specified size and contents.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="size"></param>
		/// <param name="initialContents"></param>
		/// <param name="hint"></param>
		public Buffer(GLContext context, int size, ReadOnlySpan<byte> initialContents, BufferUsageHint hint = BufferUsageHint.StaticDraw)
			: base(context)
		{
			if (!initialContents.IsEmpty && initialContents.Length < size)
				throw new ArgumentException("The initial contents for the buffer are not the same size as the buffer.", nameof(initialContents));

			// Create buffer.
			this.ID = this.Context.Invoke(this.Context.GL.GenBuffer);
			this.Size = size;
			GLTrace.Instance.ObjectCreated(nameof(Buffer), this.ID);
			
			if (initialContents.IsEmpty)
			{
				this.Context.Invoke(() =>
				{
					this.Bind(BufferTarget.ArrayBuffer);
					this.Context.GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(size), IntPtr.Zero, hint);
				});
			}
			else
			{
				unsafe
				{
					fixed (byte* b = &initialContents.GetPinnableReference())
					{
						IntPtr ptr = new IntPtr(b);
						this.Context.Invoke(() =>
						{
							this.Bind(BufferTarget.ArrayBuffer);
							this.Context.GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(size), ptr, hint);
						});
					}
				}
			}
		}

		/// <summary>
		/// Creates a new uninitialized OpenGL buffer with the specified size.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="size"></param>
		/// <param name="hint"></param>
		public Buffer(GLContext context, int size, BufferUsageHint hint = BufferUsageHint.StaticDraw)
			: this(context, size, ReadOnlySpan<byte>.Empty, hint)
		{

		}

		/// <summary>
		/// Creates a new OpenGL buffer with the specified contents.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="initialContents"></param>
		/// <param name="hint"></param>
		public Buffer(GLContext context, ReadOnlySpan<byte> initialContents, BufferUsageHint hint = BufferUsageHint.StaticDraw)
			: this(context, initialContents.Length, initialContents, hint)
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Binds the buffer.
		/// </summary>
		/// <param name="target">The buffer target.</param>
		/// <param name="modifyState">Whether to modify the current state of the context.</param>
		public void Bind(BufferTarget target, bool modifyState = true)
		{
			this.Context.Invoke(() =>
			{
				if (modifyState)
				{
					State currentState = this.Context.CurrentState;

					if (currentState != null)
					{
						if (currentState.Buffers[target] == this.ID)
							return;

						currentState.Buffers[target] = this.ID;
					}
				}

				this.Context.GL.BindBuffer(target, this.ID);
			});
		}

		/// <summary>
		/// Unbinds the buffer currently bound to the specified target.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="target"></param>
		/// <param name="modifyState"></param>
		public static void Unbind(GLContext context, BufferTarget target, bool modifyState = true)
		{
			context.Invoke(() =>
			{
				if (modifyState)
				{
					State currentState = context.CurrentState;
					
					if (currentState != null)
					{
						if (currentState.Buffers[target] == 0)
							return;

						currentState.Buffers[target] = 0;
					}
				}

				context.GL.BindBuffer(target, 0);
			});
		}

		/// <summary>
		/// Creates a buffer with the specified value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="context"></param>
		/// <param name="initialContents"></param>
		/// <param name="hint"></param>
		/// <returns></returns>
		public static Buffer Create<T>(GLContext context, ReadOnlySpan<T> initialContents, BufferUsageHint hint = BufferUsageHint.StaticDraw) where T : struct
		{
			ReadOnlySpan<byte> bytes = initialContents.AsBytes();
			return new Buffer(context, bytes.Length, bytes, hint);
		}
		
		/// <summary>
		/// Checks whether the range lies within the memory boundaries.
		/// </summary>
		/// <param name="offset">The offset, in bytes.</param>
		/// <param name="length">The number of bytes.</param>
		private void checkBounds(int offset, int length)
		{
			if (offset < 0)
				throw new ArgumentException("Offset must be greater than or equal to 0.", nameof(offset));

			if (length <= 0)
				throw new ArgumentException("Length must be greater than 0.", nameof(length));

			if (offset + length > this.Size)
				throw new ArgumentOutOfRangeException();
		}

		/// <summary>
		/// Copies the contents of this buffer to the specified buffer.
		/// </summary>
		/// <param name="sourceOffset"></param>
		/// <param name="destBuffer"></param>
		/// <param name="destOffset"></param>
		/// <param name="count"></param>
		public void CopyTo(int sourceOffset, Buffer destBuffer, int destOffset, int count)
		{
			if (destBuffer.Context != this.Context)
				throw new GLContextMismatchException();

			this.Context.Invoke(() =>
			{
				this.Bind(BufferTarget.CopyReadBuffer);
				destBuffer.Bind(BufferTarget.CopyWriteBuffer);
				this.Context.GL.CopyBufferSubData(BufferTarget.CopyReadBuffer, BufferTarget.CopyWriteBuffer, (IntPtr)sourceOffset, (IntPtr)destOffset, (IntPtr)count);
			});
		}

		/// <summary>
		/// Copies the contents of this buffer to the specified buffer.
		/// </summary>
		/// <param name="sourceOffset"></param>
		/// <param name="destBuffer"></param>
		/// <param name="destOffset"></param>
		/// <param name="count"></param>
		public ValueTask CopyToAsync(int sourceOffset, Buffer destBuffer, int destOffset, int count)
		{
			if (destBuffer.Context != this.Context)
				throw new GLContextMismatchException();

			return this.Context.InvokeAsync(() =>
			{
				this.Bind(BufferTarget.CopyReadBuffer);
				destBuffer.Bind(BufferTarget.CopyWriteBuffer);
				this.Context.GL.CopyBufferSubData(BufferTarget.CopyReadBuffer, BufferTarget.CopyWriteBuffer, (IntPtr)sourceOffset, (IntPtr)destOffset, (IntPtr)count);
			});
		}

		/// <summary>
		/// Reads the specified stream and uploads the data to the memory region.
		/// </summary>
		/// <param name="stream"></param>
		public void FromStream(Stream stream)
		{
			byte[] buffer = new byte[4096];
			int numBytes;
			int offset = 0;

			while ((numBytes = stream.Read(buffer, 0, buffer.Length)) > 0)
			{
				this.Set<byte>(offset, buffer.ToSpan(0, numBytes));
				offset += numBytes;
			}
		}

		/// <summary>
		/// Writes the contents of the memory region to the stream.
		/// </summary>
		/// <param name="stream"></param>
		public void ToStream(Stream stream)
		{
			byte[] buffer = new byte[4096];
			int offset = 0;

			while(offset < this.Size)
			{
				int numBytes = buffer.Length;
				if (numBytes > this.Size - offset)
					numBytes = this.Size - offset;

				this.Get<byte>(offset, buffer.ToSpan(0, numBytes));
				offset += numBytes;
				stream.Write(buffer, 0, numBytes);
			}
		}

		public override string ToString()
		{
			return $"{this.ID}";
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			void delete()
			{
				this.Context.GL.BindVertexArray(0);
				this.Context.GL.DeleteBuffer(this.ID);

				State currentState = this.Context.CurrentState;

				if (currentState != null)
				{
					currentState.VertexArray = 0;
					currentState.Buffers.Unbind(this.ID);
				}
					
				GLTrace.Instance.ObjectDeleted(nameof(Buffer), this.ID);
			}

			if (managed)
				this.Context.Invoke(delete);
			else
				this.Context.Post(delete);
		}

		#endregion
	}
}
