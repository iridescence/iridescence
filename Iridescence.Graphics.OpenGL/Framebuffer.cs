﻿using System.Collections.Generic;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL framebuffer object. 
	/// </summary>
	public class Framebuffer : GLContextChild, IGLObject
	{
		#region Fields

		private readonly Dictionary<FramebufferAttachment, IFramebufferAttachment> attachments;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the framebuffer object ID.
		/// </summary>
		public uint ID { get; }
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Framebuffer.
		/// </summary>
		public Framebuffer(GLContext context)
			: base(context)
		{
			this.ID = context.Invoke(this.Context.GL.GenFramebuffer);
			GLTrace.Instance.ObjectCreated(nameof(Framebuffer), this.ID);

			this.attachments = new Dictionary<FramebufferAttachment, IFramebufferAttachment>();
		}

		#endregion

		#region Methods

		private void attach(FramebufferAttachment attachment, IFramebufferAttachment obj)
		{
			this.Context.Invoke(() =>
			{
				this.Context.GL.BindFramebuffer(FramebufferTarget.Framebuffer, this.ID);
				
				State currentState = this.Context.CurrentState;
				if (currentState != null)
					currentState.DrawFramebuffer = currentState.ReadFramebuffer = this.ID;

				if (obj == null)
				{
					this.Context.GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, attachment, TextureTarget.Texture2D, 0, 0);
				}
				else
				{
					obj.AttachToFramebuffer(FramebufferTarget.Framebuffer, attachment);
				}

				this.attachments[attachment] = obj;
			});
		}

		/// <summary>
		/// Sets a color framebuffer attachment.
		/// </summary>
		/// <param name="index">The attachment slot index.</param>
		/// <param name="obj">The framebuffer attachment to attach.</param>
		public void SetColorAttachment(int index, IFramebufferAttachment obj)
		{
			this.attach(FramebufferAttachment.ColorAttachment0 + index, obj);
		}

		/// <summary>
		/// Sets a depth framebuffer attachment.
		/// </summary>
		/// <param name="obj"></param>
		public void SetDepthAttachment(IFramebufferAttachment obj)
		{
			this.attach(FramebufferAttachment.DepthAttachment, obj);
		}
		
		/// <summary>
		/// Sets the stencil framebuffer attachment.
		/// </summary>
		/// <param name="obj"></param>
		public void SetStencilAttachment(IFramebufferAttachment obj)
		{
			this.attach(FramebufferAttachment.StencilAttachment, obj);
		}
		
		/// <summary>
		/// Sets a depth/stencil framebuffer attachment.
		/// </summary>
		/// <param name="obj"></param>
		public void SetDepthStencilAttachment(IFramebufferAttachment obj)
		{
			this.attach(FramebufferAttachment.DepthStencilAttachment, obj);
		}
	
		/// <summary>
		/// Returns the framebuffer status.
		/// </summary>
		/// <returns></returns>
		public FramebufferErrorCode GetStatus()
		{
			return this.Context.Invoke(() =>
			{
				this.Context.GL.BindFramebuffer(FramebufferTarget.Framebuffer, this.ID);
				
				State currentState = this.Context.CurrentState;
				if (currentState != null)
					currentState.DrawFramebuffer = currentState.ReadFramebuffer = this.ID;
				
				return this.Context.GL.CheckFramebufferStatus(FramebufferTarget.Framebuffer);
			});
		}

		/// <summary>
		/// Sets the buffer to read from in read operations.
		/// </summary>
		/// <param name="src"></param>
		public void SetReadBuffer(ReadBufferMode src)
		{
			this.Context.Invoke(() =>
			{
				this.Context.GL.BindFramebuffer(FramebufferTarget.ReadFramebuffer, this.ID);
				
				State currentState = this.Context.CurrentState;
				if (currentState != null)
					currentState.ReadFramebuffer = this.ID;
				
				this.Context.GL.ReadBuffer(src);
			});
		}

		/// <summary>
		/// Sets the buffers to draw to in draw operations.
		/// </summary>
		/// <param name="bufs"></param>
		public void SetDrawBuffers(params DrawBufferMode[] bufs)
		{
			this.Context.Invoke(() =>
			{
				this.Context.GL.BindFramebuffer(FramebufferTarget.DrawFramebuffer, this.ID);
				
				State currentState = this.Context.CurrentState;
				if (currentState != null)
					currentState.DrawFramebuffer = this.ID;
				
				this.Context.GL.DrawBuffers(bufs.Length, bufs);
			});
		}

		public override string ToString()
		{
			return $"{this.ID}";
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			void delete()
			{
				this.Context.GL.DeleteFramebuffer(this.ID);
				GLTrace.Instance.ObjectDeleted(nameof(Framebuffer), this.ID);
			}

			if (managed)
				this.Context.Invoke(delete);
			else
				this.Context.Post(delete);
		}

		#endregion
	}
}
