﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL renderbuffer object.
	/// </summary>
	public class Renderbuffer : GLContextChild, IGLObject, IFramebufferAttachment
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the renderbuffer object ID.
		/// </summary>
		public uint ID { get; }

		/// <summary>
		/// Gets the pixel format of the renderbuffer.
		/// </summary>
		public RenderbufferStorage Format { get; }

		/// <summary>
		/// Gets the width of the renderbuffer.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the height of the renderbuffer.
		/// </summary>
		public int Height { get; }

		/// <summary>
		/// Gets the number of samples.
		/// </summary>
		public int Samples { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Renderbuffer"/> class. 
		/// </summary>
		/// <param name="context">The OpenGL context.</param>
		/// <param name="format">The pixel format for the renderbuffer.</param>
		/// <param name="width">The width of the buffer, in pixels.</param>
		/// <param name="height">The height of the buffer, in pixels.</param>
		/// <param name="samples">The number of multisampling samples. Specify 0 to disable multisampling.</param>
		public Renderbuffer(GLContext context, RenderbufferStorage format, int width, int height, int samples)
			: base(context)
		{
			this.ID = context.Invoke(this.Context.GL.GenRenderbuffer);
			GLTrace.Instance.ObjectCreated(nameof(Renderbuffer), this.ID);

			this.Format = format;
			this.Width = width;
			this.Height = height;
			this.Samples = samples;

			this.Context.Invoke(() =>
			{
				this.Context.GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, this.ID);

				if (samples == 0)
				{
					this.Context.GL.RenderbufferStorage(RenderbufferTarget.Renderbuffer, format, width, height);
				}
				else
				{
					this.Context.GL.RenderbufferStorageMultisample(RenderbufferTarget.Renderbuffer, samples, format, width, height);
				}
			});
		}

		#endregion

		#region Methods

		public void AttachToFramebuffer(FramebufferTarget target, FramebufferAttachment attachment)
		{
			this.Context.GL.BindRenderbuffer(RenderbufferTarget.Renderbuffer, this.ID);
			this.Context.GL.FramebufferRenderbuffer(target, attachment, RenderbufferTarget.Renderbuffer, this.ID);
		}

		public override string ToString()
		{
			return $"{this.ID}";
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);
			
			void delete()
			{
				this.Context.GL.DeleteRenderbuffer(this.ID);
				GLTrace.Instance.ObjectDeleted(nameof(Renderbuffer), this.ID);
			}

			if (managed)
				this.Context.Invoke(delete);
			else
				this.Context.Post(delete);
		}

		#endregion
	}
}
