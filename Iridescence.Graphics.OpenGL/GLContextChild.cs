﻿using System;
using System.Runtime.CompilerServices;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL context child object.
	/// </summary>
	public class GLContextChild : IDisposable
	{
		#region Fields

		[NonSerialized]
		private ConcurrentFlag isDisposed;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value indicating whether this object has been disposed or not.
		/// </summary>
		public bool IsDisposed => this.isDisposed.IsSet;
		
		/// <summary>
		/// Gets the OpenGL context.
		/// </summary>
		public GLContext Context { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ContextChild.
		/// </summary>
		public GLContextChild(GLContext context)
		{
			this.Context = context;
			this.Context.Children.Add(this);
		}

		~GLContextChild()
		{
			if (this.isDisposed.TrySet())
			{
				this.Dispose(false);
			}
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Throws an exception if the object has been disposed.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		protected void CheckDisposed()
		{
			if (this.IsDisposed)
				throw new ObjectDisposedException(this.GetType().Name);
		}

		/// <summary>
		/// Disposes the object and releases all resources.
		/// </summary>
		public void Dispose()
		{
			if (this.isDisposed.TrySet())
			{
				this.Dispose(true);
			}

			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Disposes the object and releases all resources.
		/// </summary>
		/// <param name="managed">Indicates whether managed resources should be released too.</param>
		protected virtual void Dispose(bool managed)
		{
			// add clean up code here...
		}
		
		#endregion
	}
}
