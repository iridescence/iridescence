﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL error.
	/// </summary>
	[Serializable]
	public class OpenGLException : Exception
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the OpenGL error code.
		/// </summary>
		public ErrorCode ErrorCode { get; }
		
		/// <summary>
		/// Gets the name of the OpenGL function that caused the exception.
		/// </summary>
		public string Function { get; }
	
		#endregion

		#region Constructors

		protected OpenGLException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			this.ErrorCode = (ErrorCode)info.GetInt32("ErrorCode");
			this.Function = info.GetString("Function");
		}

		/// <summary>
		/// Creates a new OpenGLException.
		/// </summary>
		public OpenGLException(ErrorCode errorCode)
			: base(errorCode.ToString())
		{
			this.ErrorCode = errorCode;
		}

		/// <summary>
		/// Creates a new OpenGLException.
		/// </summary>
		public OpenGLException(ErrorCode errorCode, string function)
			: base($"{errorCode} in {function}")
		{
			this.ErrorCode = errorCode;
			this.Function = function;
		}
		
		#endregion

		#region Methods

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("ErrorCode", (int)this.ErrorCode);
			info.AddValue("Function", this.Function);
		}

		/// <summary>
		/// Throws an exception if the specified code is an error.
		/// </summary>
		/// <param name="errorCode"></param>
		public static void ThrowOnError(ErrorCode errorCode)
		{
			if (errorCode != ErrorCode.NoError)
				throw new OpenGLException(errorCode);
		}
		
		#endregion
	}
}
