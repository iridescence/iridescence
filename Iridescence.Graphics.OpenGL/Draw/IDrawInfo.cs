﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Holds information about a draw command.
	/// </summary>
	public interface IDrawInfo
	{
		/// <summary>
		/// Draws the elements.
		/// </summary>
		void Draw(GLContext context);
	}
}
