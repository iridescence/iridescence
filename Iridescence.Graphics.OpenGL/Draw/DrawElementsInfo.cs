﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Holds information about a glDrawElements* draw call.
	/// </summary>
	public readonly struct DrawElementsInfo : IDrawInfo
	{
		#region Properties

		/// <summary>
		/// The primitive type.
		/// </summary>
		public readonly PrimitiveType PrimitiveType;

		/// <summary>
		/// The index data type.
		/// </summary>
		public readonly DrawElementsType IndexType ;

		/// <summary>
		/// The offset in the index buffer.
		/// </summary>
		public readonly IntPtr IndexOffset;

		/// <summary>
		/// The number of vertices to draw.
		/// </summary>
		public readonly int VertexCount;

		/// <summary>
		/// The index of the first instance to draw.
		/// </summary>
		public readonly int InstanceOffset;

		/// <summary>
		/// The number of instances to draw.
		/// </summary>
		public readonly int InstanceCount;

		/// <summary>
		/// The base vertex (an offset that is added to all indices).
		/// </summary>
		public readonly int BaseVertex;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DrawElementsInfo class.
		/// </summary>
		/// <param name="primitiveType">The type of the primitives to draw.</param>
		/// <param name="indexType">The index data type.</param>
		/// <param name="indexOffset">The offset in the index buffer.</param>
		/// <param name="vertexCount">The number of vertices to draw.</param>
		/// <param name="instanceOffset">The index of the first instance.</param>
		/// <param name="instanceCount">The number of instances.</param>
		/// <param name="baseVertex">The base vertex.</param>
		public DrawElementsInfo(PrimitiveType primitiveType, DrawElementsType indexType, IntPtr indexOffset, int vertexCount, int instanceOffset = 0, int instanceCount = 1, int baseVertex = 0)
		{
			this.PrimitiveType = primitiveType;
			this.IndexType = indexType;

			if (indexOffset.ToInt64() < 0) throw new ArgumentException("Index offset must be greater than or equal to 0.", nameof(indexOffset));
			this.IndexOffset = indexOffset;

			if (vertexCount <= 0) throw new ArgumentException("Vertex count must be greater than 0.", nameof(vertexCount));
			this.VertexCount = vertexCount;

			if (instanceOffset < 0) throw new ArgumentException("Instance offset must be greater than or equal to 0.", nameof(instanceOffset));
			this.InstanceOffset = instanceOffset;

			if (instanceCount <= 0) throw new ArgumentException("Instance count must be greater than 0.", nameof(instanceCount));
			this.InstanceCount = instanceCount;

			if (baseVertex < 0) throw new ArgumentException("Base vertex must be greater than or equal to 0.", nameof(baseVertex));
			this.BaseVertex = baseVertex;
		}

		/// <summary>
		/// Initializes a new instance of the DrawElementsInfo class.
		/// </summary>
		/// <param name="primitiveType">The type of the primitives to draw.</param>
		/// <param name="indexType">The index data type.</param>
		/// <param name="indexOffset">The offset in the index buffer.</param>
		/// <param name="vertexCount">The number of vertices to draw.</param>
		/// <param name="instanceOffset">The index of the first instance.</param>
		/// <param name="instanceCount">The number of instances.</param>
		/// <param name="baseVertex">The base vertex.</param>
		public DrawElementsInfo(PrimitiveType primitiveType, DrawElementsType indexType, int indexOffset, int vertexCount, int instanceOffset = 0, int instanceCount = 1, int baseVertex = 0)
			: this(primitiveType, indexType, (IntPtr)indexOffset, vertexCount, instanceOffset, instanceCount, baseVertex)
		{

		}
	
		#endregion

		#region Methods

		/// <summary>
		/// Draws the elements.
		/// </summary>
		void IDrawInfo.Draw(GLContext context)
		{
			if (this.BaseVertex == 0)
			{
				if (this.InstanceOffset == 0)
				{
					if (this.InstanceCount == 1)
					{
						context.GL.DrawElements(this.PrimitiveType, this.VertexCount, this.IndexType, this.IndexOffset);
					}
					else
					{
						context.GL.DrawElementsInstanced(this.PrimitiveType, this.VertexCount, this.IndexType, this.IndexOffset, this.InstanceCount);
					}
				}
				else
				{
					context.GL.DrawElementsInstancedBaseInstance(this.PrimitiveType, this.VertexCount, this.IndexType, this.IndexOffset, this.InstanceCount, (uint)this.InstanceOffset);
				}
			}
			else
			{
				if (this.InstanceOffset == 0)
				{
					if (this.InstanceCount == 1)
					{
						context.GL.DrawElementsBaseVertex(this.PrimitiveType, this.VertexCount, this.IndexType, this.IndexOffset, this.BaseVertex);
					}
					else
					{
						context.GL.DrawElementsInstancedBaseVertex(this.PrimitiveType, this.VertexCount, this.IndexType, this.IndexOffset, this.InstanceCount, this.BaseVertex);
					}
				}
				else
				{
					context.GL.DrawElementsInstancedBaseVertexBaseInstance(this.PrimitiveType, this.VertexCount, this.IndexType, this.IndexOffset, this.InstanceCount, this.BaseVertex, (uint)this.InstanceOffset);
				}
			}
		}

		#endregion
	}
}
