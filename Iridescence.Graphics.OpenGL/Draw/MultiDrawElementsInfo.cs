﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Holds information about a glMultiDrawElements* draw call.
	/// </summary>
	public readonly struct MultiDrawElementsInfo : IDrawInfo
	{
		#region Fields

		/// <summary>
		/// The primitive type.
		/// </summary>
		public readonly PrimitiveType PrimitiveType;

		/// <summary>
		/// The index data type.
		/// </summary>
		public readonly DrawElementsType IndexType;

		/// <summary>
		/// The number of draw calls to perform.
		/// </summary>
		public readonly int Count;

		/// <summary>
		/// The offsets in the index buffer of each draw operation.
		/// </summary>
		public readonly ReadOnlyMemory<IntPtr> IndexOffsets;

		/// <summary>
		/// The number of vertices of each draw operation.
		/// </summary>
		public readonly ReadOnlyMemory<int> VertexCounts;

		/// <summary>
		/// The base vertices (an offset that is added to all indices) of each draw operation.
		/// Can be null to disable.
		/// </summary>
		public readonly ReadOnlyMemory<int>? BaseVertices;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MultiDrawElementsInfo class.
		/// </summary>
		/// <param name="primitiveType">The type of the primitives to draw.</param>
		/// <param name="indexType">The index data type.</param>
		/// <param name="count">The number of draw operations.</param>
		/// <param name="indexOffsets">The offsets in the index buffer of each draw operation.</param>
		/// <param name="vertexCounts">The number of vertices of each draw operation.</param>
		/// <param name="baseVertices">The base vertices of each draw operation.</param>
		public MultiDrawElementsInfo(PrimitiveType primitiveType, DrawElementsType indexType, int count, ReadOnlyMemory<IntPtr> indexOffsets, ReadOnlyMemory<int> vertexCounts, ReadOnlyMemory<int>? baseVertices = null)
		{
			this.PrimitiveType = primitiveType;
			this.IndexType = indexType;

			if (count < 0) throw new ArgumentException("Count must be greater than 0.", nameof(count));
			this.Count = count;

			this.IndexOffsets = indexOffsets;
			this.VertexCounts = vertexCounts;
			this.BaseVertices = baseVertices;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Draws the elements.
		/// </summary>
		void IDrawInfo.Draw(GLContext context)
		{
			if (this.BaseVertices != null)
			{
				context.GL.MultiDrawElementsBaseVertex(this.PrimitiveType, ref MemoryMarshal.GetReference(this.VertexCounts.Span), this.IndexType, ref MemoryMarshal.GetReference(this.IndexOffsets.Span), this.Count, ref MemoryMarshal.GetReference(this.BaseVertices.Value.Span));
			}
			else
			{
				context.GL.MultiDrawElements(this.PrimitiveType, ref MemoryMarshal.GetReference(this.VertexCounts.Span), this.IndexType, ref MemoryMarshal.GetReference(this.IndexOffsets.Span), this.Count);
			}
		}

		#endregion
	}
}
