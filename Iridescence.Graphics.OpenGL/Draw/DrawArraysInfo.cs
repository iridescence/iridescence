﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Holds information about a glDrawArrays* draw call.
	/// </summary>
	public readonly struct DrawArraysInfo : IDrawInfo
	{
		#region Fields

		/// <summary>
		/// The primitive type.
		/// </summary>
		public readonly PrimitiveType PrimitiveType;

		/// <summary>
		/// The index of the first vertex to draw.
		/// </summary> 
		public readonly int VertexOffset;

		/// <summary>
		/// The number of vertices to draw.
		/// </summary>
		public readonly int VertexCount;
	
		/// <summary>
		/// The index of the first instance to draw.
		/// </summary>
		public readonly int InstanceOffset;

		/// <summary>
		/// The number of instances to draw.
		/// </summary>
		public readonly int InstanceCount;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the DrawArraysInfo class.
		/// </summary>
		/// <param name="primitiveType">The type of the primitives to draw.</param>
		/// <param name="vertexOffset">The index of the first vertex.</param>
		/// <param name="vertexCount">The number of vertices.</param>
		/// <param name="instanceOffset">The index of the first instance.</param>
		/// <param name="instanceCount">The number of instances.</param>
		public DrawArraysInfo(PrimitiveType primitiveType, int vertexOffset, int vertexCount, int instanceOffset = 0, int instanceCount = 1)
		{
			this.PrimitiveType = primitiveType;

			if (vertexOffset < 0) throw new ArgumentException("Vertex offset must be greater than or equal to 0.", nameof(vertexOffset));
			this.VertexOffset = vertexOffset;
			
			if (vertexCount <= 0) throw new ArgumentException("Vertex count must be greater than 0.", nameof(vertexCount));
			this.VertexCount = vertexCount;

			if (instanceOffset < 0) throw new ArgumentException("Instance offset must be greater than or equal to 0.", nameof(instanceOffset));
			this.InstanceOffset = instanceOffset;

			if (instanceCount <= 0) throw new ArgumentException("Instance count must be greater than 0.", nameof(instanceCount));
			this.InstanceCount = instanceCount;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Draws the elements.
		/// </summary>
		void IDrawInfo.Draw(GLContext context)
		{
			if (this.InstanceOffset == 0)
			{
				if (this.InstanceCount == 1)
				{
					context.GL.DrawArrays(this.PrimitiveType, this.VertexOffset, this.VertexCount);
				}
				else
				{
					context.GL.DrawArraysInstanced(this.PrimitiveType, this.VertexOffset, this.VertexCount, this.InstanceCount);
				}
			}
			else
			{
				context.GL.DrawArraysInstancedBaseInstance(this.PrimitiveType, this.VertexOffset, this.VertexCount, this.InstanceCount, (uint)this.InstanceOffset);
			}
		}

		#endregion
	}
}
