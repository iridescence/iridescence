﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Holds information about a glMultiDrawArrays* draw call.
	/// </summary>
	public readonly struct MultiDrawArraysInfo : IDrawInfo
	{
		#region Fields

		/// <summary>
		/// The primitive type.
		/// </summary>
		public readonly PrimitiveType PrimitiveType;

		/// <summary>
		/// The number of draw calls to perform.
		/// </summary>
		public readonly int Count;

		/// <summary>
		/// The the indices of the first vertex of each draw operation.
		/// </summary>
		public readonly ReadOnlyMemory<int> VertexOffsets;

		/// <summary>
		/// The number of vertices of each draw operation.
		/// </summary>
		public readonly ReadOnlyMemory<int> VertexCounts;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MultiDrawArraysInfo class.
		/// </summary>
		/// <param name="primitiveType">The type of the primitives to draw.</param>
		/// <param name="count">The number of draw operations to perform.</param>
		/// <param name="vertexOffsets">The indices of the first vertex of each draw operation.</param>
		/// <param name="vertexCounts">The number of vertices of each draw operation.</param>
		public MultiDrawArraysInfo(PrimitiveType primitiveType, int count, ReadOnlyMemory<int> vertexOffsets, ReadOnlyMemory<int> vertexCounts)
		{
			this.PrimitiveType = primitiveType;

			if (count < 0) throw new ArgumentException("Count must be greater than 0.", nameof(count));
			this.Count = count;

			this.VertexOffsets = vertexOffsets;

			this.VertexCounts = vertexCounts;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Draws the elements.
		/// </summary>
		void IDrawInfo.Draw(GLContext context)
		{
			context.GL.MultiDrawArrays(this.PrimitiveType, ref MemoryMarshal.GetReference(this.VertexOffsets.Span), ref MemoryMarshal.GetReference(this.VertexCounts.Span), this.Count);
		}

		#endregion
	}
}
