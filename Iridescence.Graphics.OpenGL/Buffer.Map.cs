﻿using System;
using System.Buffers;
using System.Threading.Tasks;
using Iridescence.Memory;

namespace Iridescence.Graphics.OpenGL
{
	public partial class Buffer : IMappable
	{
		private IntPtr mappedAddress;
		private BufferTarget mappedTarget;

		private sealed class DummyPinnable : IPinnable
		{
			private readonly Buffer buffer;

			public DummyPinnable(Buffer buffer)
			{
				this.buffer = buffer;
			}

			public MemoryHandle Pin(int elementIndex) => throw new InvalidOperationException();

			public void Unpin() => this.buffer.Unmap();
		}

		/// <summary>
		/// Maps the buffer into virtual memory of the CPU.
		/// </summary>
		/// <param name="mode"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public MemoryHandle Map(AccessMode mode, int start, int length) => this.Map(mode, start, length);

		/// <summary>
		/// Maps the buffer into virtual memory of the CPU.
		/// </summary>
		/// <param name="mode"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <param name="target">The buffer target to bind to.</param>
		/// <returns></returns>
		public MemoryHandle Map(AccessMode mode, int start, int length, BufferTarget target)
		{
			this.checkBounds(start, length);
			
			BufferAccess glAccess;
			BufferAccessMask glMask;

			if (mode == AccessMode.ReadWrite)
			{
				glAccess = BufferAccess.ReadWrite;
				glMask = BufferAccessMask.MapReadBit | BufferAccessMask.MapWriteBit;
			}
			else if (mode == AccessMode.Read)
			{
				glAccess = BufferAccess.ReadOnly;
				glMask = BufferAccessMask.MapReadBit;
			}
			else if (mode == AccessMode.Write)
			{
				glAccess = BufferAccess.WriteOnly;
				glMask = BufferAccessMask.MapReadBit;
			}
			else
			{
				throw new ArgumentException("Invalid access mode", nameof(mode));
			}

			this.Context.Invoke(() =>
			{
				this.Bind(target);

				this.mappedTarget = target;
				if (start > 0 || length < this.Size)
				{
					this.mappedAddress = this.Context.GL.MapBufferRange(this.mappedTarget, (IntPtr)start, (IntPtr)length, glMask);
				}
				else
				{
					this.mappedAddress = this.Context.GL.MapBuffer(this.mappedTarget, glAccess);
				}
			});

			if (this.mappedAddress == IntPtr.Zero)
				throw new InvalidOperationException("The buffer could not be mapped. OpenGL did not return a valid address.");
			
			unsafe
			{
				return new MemoryHandle(this.mappedAddress.ToPointer(), pinnable: new DummyPinnable(this));
			}
		}

		/// <summary>
		/// Maps the buffer into virtual memory of the CPU.
		/// </summary>
		/// <param name="mode"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <param name="target">The buffer target to bind to.</param>
		/// <returns></returns>
		public async ValueTask<MemoryHandle> MapAsync(AccessMode mode, int start, int length, BufferTarget target)
		{
			this.checkBounds(start, length);

			BufferAccess glAccess;
			BufferAccessMask glMask;

			if (mode == AccessMode.ReadWrite)
			{
				glAccess = BufferAccess.ReadWrite;
				glMask = BufferAccessMask.MapReadBit | BufferAccessMask.MapWriteBit;
			}
			else if (mode == AccessMode.Read)
			{
				glAccess = BufferAccess.ReadOnly;
				glMask = BufferAccessMask.MapReadBit;
			}
			else if (mode == AccessMode.Write)
			{
				glAccess = BufferAccess.WriteOnly;
				glMask = BufferAccessMask.MapReadBit;
			}
			else
			{
				throw new ArgumentException("Invalid access mode", nameof(mode));
			}

			await this.Context.InvokeAsync(() =>
			{
				this.Bind(target);

				this.mappedTarget = target;
				if (start > 0 || length < this.Size)
				{
					this.mappedAddress = this.Context.GL.MapBufferRange(this.mappedTarget, (IntPtr)start, (IntPtr)length, glMask);
				}
				else
				{
					this.mappedAddress = this.Context.GL.MapBuffer(this.mappedTarget, glAccess);
				}
			});

			if (this.mappedAddress == IntPtr.Zero)
				throw new InvalidOperationException("The buffer could not be mapped. OpenGL did not return a valid address.");

			unsafe
			{
				return new MemoryHandle(this.mappedAddress.ToPointer(), pinnable: new DummyPinnable(this));
			}
		}

		/// <summary>
		/// Unmaps the buffer if it was mapped with <see cref="Map"/>.
		/// </summary>
		public void Unmap()
		{
			int err = 0;

			this.Context.Invoke(() =>
			{
				if (this.mappedAddress == IntPtr.Zero)
				{
					err = 1;
					return;
				}

				this.Bind(this.mappedTarget);
				this.Context.GL.UnmapBuffer(this.mappedTarget);

				this.mappedTarget = 0;
				this.mappedAddress = IntPtr.Zero;
			});

			switch (err)
			{
				case 1:
					throw new InvalidOperationException("The buffer is not mapped.");
			}
		}

		/// <summary>
		/// Unmaps the buffer if it was mapped with <see cref="MapAsync"/>.
		/// </summary>
		public async ValueTask UnmapAsync()
		{
			int err = 0;

			await this.Context.InvokeAsync(() =>
			{
				if (this.mappedAddress == IntPtr.Zero)
				{
					err = 1;
					return;
				}

				this.Bind(this.mappedTarget);
				this.Context.GL.UnmapBuffer(this.mappedTarget);

				this.mappedTarget = 0;
			});

			switch (err)
			{
				case 1:
					throw new InvalidOperationException("The buffer is not mapped.");
			}
		}
	}
}
