﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Thrown when objects from incompatible contexts are used together.
	/// </summary>
	public sealed class GLContextMismatchException : Exception
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ContextMismatchException.
		/// </summary>
		public GLContextMismatchException()
			: base("The object belongs to a different context.")
		{

		}

		#endregion

		#region Methods

		#endregion
	}
}
