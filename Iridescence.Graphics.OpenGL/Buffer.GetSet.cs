﻿using System;
using System.Buffers;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	public partial class Buffer
	{
		/// <summary>
		/// Reads data from the buffer.
		/// </summary>
		/// <param name="sourceOffset">The source byte offset.</param>
		/// <param name="dest">A pointer to the source data.</param>
		/// <param name="length">The number of bytes to read.</param>
		public unsafe void Get(int sourceOffset, void* dest, int length)
		{
			this.CheckDisposed();

			if (length < 0)
				throw new ArgumentOutOfRangeException(nameof(length));

			if (length == 0)
				return;

			this.Context.Invoke(() =>
			{
				this.Bind(BufferTarget.ArrayBuffer);
				this.Context.GL.GetBufferSubData(BufferTarget.ArrayBuffer, new IntPtr(sourceOffset), new IntPtr(length), new IntPtr(dest));
			});
		}

		/// <summary>
		/// Reads data from the buffer asynchronously.
		/// </summary>
		/// <param name="sourceOffset">The source byte offset.</param>
		/// <param name="dest">A pointer to the source data.</param>
		/// <param name="length">The number of bytes to read.</param>
		public unsafe ValueTask GetAsync(int sourceOffset, void* dest, int length)
		{
			this.CheckDisposed();

			if (length < 0)
				throw new ArgumentOutOfRangeException(nameof(length));

			if (length == 0)
				return new ValueTask();
			
			return this.Context.InvokeAsync(() =>
			{
				this.Bind(BufferTarget.ArrayBuffer);
				this.Context.GL.GetBufferSubData(BufferTarget.ArrayBuffer, new IntPtr(sourceOffset), new IntPtr(length), new IntPtr(dest));
			});
		}

		/// <summary>
		/// Writes data to the buffer.
		/// </summary>
		/// <param name="destOffset">The destination byte offset.</param>
		/// <param name="source">A pointer to the destination.</param>
		/// <param name="length">The number of bytes to read.</param>
		public unsafe void Set(int destOffset, void* source, int length)
		{
			this.CheckDisposed();

			if (length < 0)
				throw new ArgumentOutOfRangeException(nameof(length));

			if (length == 0)
				return;

			this.Context.Invoke(() =>
			{
				this.Bind(BufferTarget.ArrayBuffer);
				this.Context.GL.BufferSubData(BufferTarget.ArrayBuffer, new IntPtr(destOffset), new IntPtr(length), new IntPtr(source));
			});
		}

		/// <summary>
		/// Writes data to the buffer asynchronously.
		/// </summary>
		/// <param name="destOffset">The destination byte offset.</param>
		/// <param name="source">A pointer to the destination.</param>
		/// <param name="length">The number of bytes to read.</param>
		public unsafe ValueTask SetAsync(int destOffset, void* source, int length)
		{
			this.CheckDisposed();

			if (length < 0)
				throw new ArgumentOutOfRangeException(nameof(length));

			if (length == 0)
				return new ValueTask();

			return this.Context.InvokeAsync(() =>
			{
				this.Bind(BufferTarget.ArrayBuffer);
				this.Context.GL.BufferSubData(BufferTarget.ArrayBuffer, new IntPtr(destOffset), new IntPtr(length), new IntPtr(source));
			});
		}

		/// <summary>
		/// Reads data from the buffer and stores it to the specified span.
		/// </summary>
		/// <typeparam name="T">The data type.</typeparam>
		/// <param name="sourceOffset">The source byte offset.</param>
		/// <param name="dest">The destination span.</param>
		public unsafe void Get<T>(int sourceOffset, Span<T> dest) where T : unmanaged
		{
			this.CheckDisposed();

			if (dest.IsEmpty)
				return;

			int length = dest.Length * sizeof(T);
			this.checkBounds(sourceOffset, length);

			fixed (T* ptr = &dest[0])
			{
				this.Get(sourceOffset, ptr, length);
			}
		}

		/// <summary>
		/// Reads data from the buffer asynchronously and stores it to the specified memory.
		/// </summary>
		/// <typeparam name="T">The data type.</typeparam>
		/// <param name="sourceOffset">The source byte offset.</param>
		/// <param name="dest">The destination span.</param>
		public unsafe ValueTask GetAsync<T>(int sourceOffset, Memory<T> dest) where T : unmanaged
		{
			this.CheckDisposed();

			if (dest.IsEmpty)
				return new ValueTask();

			int length = dest.Length * sizeof(T);
			this.checkBounds(sourceOffset, length);

			return this.Context.InvokeAsync(() =>
			{
				using MemoryHandle pin = dest.Pin();
				this.Bind(BufferTarget.ArrayBuffer);
				this.Context.GL.GetBufferSubData(BufferTarget.ArrayBuffer, new IntPtr(sourceOffset), new IntPtr(length), new IntPtr(pin.Pointer));
			});
		}

		/// <summary>
		/// Writes data from the specified span to the buffer.
		/// </summary>
		/// <typeparam name="T">The data type.</typeparam>
		/// <param name="destOffset">The destination byte offset.</param>
		/// <param name="source">The source span.</param>
		public unsafe void Set<T>(int destOffset, ReadOnlySpan<T> source) where T : unmanaged
		{
			this.CheckDisposed();

			if (source.IsEmpty)
				return;

			int length = source.Length * sizeof(T);
			this.checkBounds(destOffset, length);

			fixed (T* ptr = &source[0])
			{
				this.Set(destOffset, ptr, length);
			}
		}
		
		/// <summary>
		/// Writes data from the specified memory to the buffer asynchronously.
		/// </summary>
		/// <typeparam name="T">The data type.</typeparam>
		/// <param name="destOffset">The destination byte offset.</param>
		/// <param name="source">The source span.</param>
		public unsafe ValueTask SetAsync<T>(int destOffset, ReadOnlyMemory<T> source) where T : unmanaged
		{
			this.CheckDisposed();

			if (source.IsEmpty)
				return new ValueTask();

			int length = source.Length * sizeof(T);
			this.checkBounds(destOffset, length);

			return this.Context.InvokeAsync(() =>
			{
				using MemoryHandle pin = source.Pin();
				this.Bind(BufferTarget.ArrayBuffer);
				this.Context.GL.BufferSubData(BufferTarget.ArrayBuffer, new IntPtr(destOffset), new IntPtr(length), new IntPtr(pin.Pointer));
			});
		}

		/// <summary>
		/// Reads a single item from the buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceOffset">The source byte offset.</param>
		/// <param name="item">The resulting item.</param>
		public unsafe void GetItem<T>(int sourceOffset, out T item) where T : unmanaged
		{
			fixed (T* ptr = &item)
			{
				this.Get(sourceOffset, ptr, sizeof(T));
			}
		}

		/// <summary>
		/// Writes a single item to the buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="destOffset"></param>
		/// <param name="item"></param>
		public unsafe void SetItem<T>(int destOffset, in T item) where T : unmanaged
		{
			fixed (T* ptr = &item)
			{
				this.Set(destOffset, ptr, sizeof(T));
			}
		}
	}
}
