﻿ using System;
using System.Collections.Generic;

 namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a vertex array object.
	/// </summary>
	public class VertexArray : GLContextChild, IGLObject
	{
		#region Fields

		private Buffer elementBuffer;
		private Dictionary<uint, Buffer> buffers;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the object ID of the vertex array.
		/// </summary>
		public uint ID { get; }

		/// <summary>
		/// Gets or sets the element (or index) buffer.
		/// </summary>
		public Buffer ElementBuffer
		{
			get => this.elementBuffer;
			set
			{
				if (value != this.elementBuffer)
				{
					this.elementBuffer = value;
					this.Context.Invoke(() =>
					{
						this.Bind();
						this.Context.GL.BindBuffer(BufferTarget.ElementArrayBuffer, this.elementBuffer.ID);
					});
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new VertexArray.
		/// </summary>
		public VertexArray(GLContext context)
			: base(context)
		{
			// create vertex array.
			this.ID = this.Context.Invoke(this.Context.GL.GenVertexArray);
			GLTrace.Instance.ObjectCreated(nameof(VertexArray), this.ID);
		}

		#endregion

		#region Methods

		internal void Bind()
		{
			State currentState = this.Context.CurrentState;
			if (currentState != null)
				currentState.VertexArray = this.ID;
			
			this.Context.GL.BindVertexArray(this.ID);
		}

		/// <summary>
		/// Enables the specified slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		public void Enable(uint index)
		{
			this.Context.Invoke(() =>
			{
				this.Bind();
				this.Context.GL.EnableVertexAttribArray(index);
			});
		}

		/// <summary>
		/// Disables the specified slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		public void Disable(uint index)
		{
			this.Context.Invoke(() =>
			{
				this.Bind();
				this.Context.GL.DisableVertexAttribArray(index);
			});
		}

		private void setBuffer(uint index, Buffer buffer)
		{
			if (this.buffers == null)
				this.buffers = new Dictionary<uint, Buffer>();

			this.buffers[index] = buffer;
		}

		/// <summary>
		/// Binds a buffer to the vertex array and enables the slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		/// <param name="buffer"></param>
		/// <param name="size"></param>
		/// <param name="type"></param>
		/// <param name="normalized"></param>
		/// <param name="stride">The stride, in bytes.</param>
		/// <param name="offset">The offset, in bytes.</param>
		public void Bind(uint index, Buffer buffer, int size, VertexAttribPointerType type, bool normalized, int stride, IntPtr offset)
		{
			this.Context.Invoke(() =>
			{
				this.setBuffer(index, buffer);

				this.Bind();
				buffer.Bind(BufferTarget.ArrayBuffer);
				this.Context.GL.VertexAttribPointer(index, size, type, normalized, stride, offset);
				this.Context.GL.EnableVertexAttribArray(index);
			});
		}

		/// <summary>
		/// Binds a buffer to the vertex array and enables the slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		/// <param name="buffer"></param>
		/// <param name="format"></param>
		/// <param name="stride">The stride, in bytes.</param>
		/// <param name="offset">The offset, in bytes.</param>
		public void Bind(uint index, Buffer buffer, Format format, int stride, IntPtr offset)
		{
			this.Bind(index, buffer, format.Components, FormatUtility.GetVertexType(format), format.IsNormalized, stride, offset);
		}

		/// <summary>
		/// Binds a buffer to the vertex array in integer mode and enables the slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		/// <param name="buffer"></param>
		/// <param name="size"></param>
		/// <param name="type"></param>
		/// <param name="stride">The stride, in bytes.</param>
		/// <param name="offset">The offset, in bytes.</param>
		public void BindInteger(uint index, Buffer buffer, int size, VertexAttribIntegerType type, int stride, IntPtr offset)
		{
			this.Context.Invoke(() =>
			{
				this.setBuffer(index, buffer);

				this.Bind();
				buffer.Bind(BufferTarget.ArrayBuffer);
				this.Context.GL.VertexAttribIPointer(index, size, type, stride, offset);
				this.Context.GL.EnableVertexAttribArray(index);
			});
		}

		/// <summary>
		/// Binds a buffer to the vertex array in integer mode and enables the slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		/// <param name="buffer"></param>
		/// <param name="format"></param>
		/// <param name="stride">The stride, in bytes.</param>
		/// <param name="offset">The offset, in bytes.</param>
		public void BindInteger(uint index, Buffer buffer, Format format, int stride, IntPtr offset)
		{
			this.BindInteger(index, buffer, format.Components, (VertexAttribIntegerType)FormatUtility.GetVertexType(format), stride, offset);
		}

		/// <summary>
		/// Binds a buffer to the vertex array and enables the slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		/// <param name="buffer"></param>
		/// <param name="size"></param>
		/// <param name="type"></param>
		/// <param name="normalized"></param>
		/// <param name="stride">The stride, in bytes.</param>
		/// <param name="offset">The offset, in bytes.</param>
		public void Bind(uint index, Buffer buffer, int size, VertexAttribPointerType type, bool normalized, int stride, int offset)
		{
			this.Bind(index, buffer, size, type, normalized, stride, (IntPtr)offset);
		}

		/// <summary>
		/// Binds a buffer to the vertex array and enables the slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		/// <param name="buffer"></param>
		/// <param name="format"></param>
		/// <param name="stride">The stride, in bytes.</param>
		/// <param name="offset">The offset, in bytes.</param>
		public void Bind(uint index, Buffer buffer, Format format, int stride, int offset)
		{
			this.Bind(index, buffer, format, stride, (IntPtr)offset);
		}

		/// <summary>
		/// Binds a buffer to the vertex array in integer mode and enables the slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		/// <param name="buffer"></param>
		/// <param name="size"></param>
		/// <param name="type"></param>
		/// <param name="stride">The stride, in bytes.</param>
		/// <param name="offset">The offset, in bytes.</param>
		public void BindInteger(uint index, Buffer buffer, int size, VertexAttribIntegerType type, int stride, int offset)
		{
			this.BindInteger(index, buffer, size, type, stride, (IntPtr)offset);
		}

		/// <summary>
		/// Binds a buffer to the vertex array in integer mode and enables the slot.
		/// </summary>
		/// <param name="index">The slot index.</param>
		/// <param name="buffer"></param>
		/// <param name="format"></param>
		/// <param name="stride">The stride, in bytes.</param>
		/// <param name="offset">The offset, in bytes.</param>
		public void BindInteger(uint index, Buffer buffer, Format format, int stride, int offset)
		{
			this.BindInteger(index, buffer, format, stride, (IntPtr)offset);
		}

		/// <summary>
		/// Unbinds the buffer bound to the specified slot and disables the slot.
		/// </summary>
		/// <param name="index"></param>
		public void Unbind(uint index)
		{
			this.Context.Invoke(() =>
			{
				this.setBuffer(index, null);
				
				this.Bind();
				
				State currentState = this.Context.CurrentState;
				if (currentState != null)
					currentState.Buffers[BufferTarget.ArrayBuffer] = 0;
				
				this.Context.GL.BindBuffer(BufferTarget.ArrayBuffer, 0);

				this.Context.GL.VertexAttribPointer(index, 4, VertexAttribPointerType.Float, false, 0, IntPtr.Zero);
				this.Context.GL.DisableVertexAttribArray(index);
			});
		}

		public override string ToString()
		{
			return $"{this.ID}";
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			void delete()
			{
				this.Context.GL.DeleteVertexArray(this.ID);
				this.buffers.Clear();
				this.buffers = null;
				GLTrace.Instance.ObjectDeleted(nameof(VertexArray), this.ID);
			}

			if (managed)
				this.Context.Invoke(delete);
			else
				this.Context.Post(delete);
		}

		#endregion
	}
}
