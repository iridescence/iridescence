﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a member of a uniform block.
	/// </summary>
	public sealed class UniformBlockMember
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the declaring block.
		/// </summary>
		public UniformBlock Block { get; }

		/// <summary>
		/// Gets the name of the member.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets the type of the member.
		/// </summary>
		public ActiveUniformType Type { get; }

		/// <summary>
		/// Gets the offset of the member.
		/// </summary>
		public int Offset { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new UniformBlockMember.
		/// </summary>
		internal UniformBlockMember(UniformBlock block, string name, ActiveUniformType type, int offset)
		{
			this.Block = block;
			this.Name = name;
			this.Type = type;
			this.Offset = offset;
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			return $"{UniformUtil.GetDisplayName(this.Type)} {this.Block.Name}.{this.Name}";
		}

		#endregion
	}
}
