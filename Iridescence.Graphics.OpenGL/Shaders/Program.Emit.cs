﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using Iridescence.Emit;
using Iridescence.Math;
using Label = Iridescence.Emit.Label;

namespace Iridescence.Graphics.OpenGL
{
	public partial class Program
	{
		#region Fields

		private static readonly PropertyInfo programStateProgramProperty = typeof(IProgramState).GetProperty(nameof(IProgramState.Program), BindingFlags.Instance | BindingFlags.Public);
		private static readonly MethodInfo programStateApplyMethod = typeof(IProgramState).GetMethod(nameof(IProgramState.Apply), BindingFlags.Instance | BindingFlags.Public);
		private static readonly MethodInfo programGetContextMethod = typeof(Program).GetProperty(nameof(Program.Context), BindingFlags.Instance | BindingFlags.Public).GetGetMethod();
		private static readonly MethodInfo contextGetGLMethod = typeof(GLContext).GetProperty(nameof(GLContext.GL), BindingFlags.Instance | BindingFlags.Public).GetGetMethod();
		private static readonly FieldInfo glBindingsField = typeof(GL).GetField(nameof(GL.Bindings), BindingFlags.Instance | BindingFlags.Public);
		private static readonly ConstructorInfo argumentExceptionConstructor = typeof(ArgumentException).GetConstructor(new Type[] {typeof(string), typeof(string)});
		private static readonly ConstructorInfo argumentNullExceptionConstructor = typeof(ArgumentNullException).GetConstructor(new Type[] {typeof(string)});
		private static readonly FieldInfo glBindBufferRangeField = typeof(GLBindings).GetField("glBindBufferRange", BindingFlags.Instance | BindingFlags.Public);
		private static readonly MethodInfo glBindBufferRangeInvokeMethod = glBindBufferRangeField.FieldType.GetMethod("Invoke");
		private static readonly MethodInfo uniformBlockBindingBufferGetter = typeof(UniformBlockBinding).GetProperty("Buffer", BindingFlags.Instance | BindingFlags.Public).GetGetMethod();
		private static readonly MethodInfo uniformBlockBindingOffsetGetter = typeof(UniformBlockBinding).GetProperty("Offset", BindingFlags.Instance | BindingFlags.Public).GetGetMethod();
		private static readonly MethodInfo uniformBlockBindingSizeGetter = typeof(UniformBlockBinding).GetProperty("Size", BindingFlags.Instance | BindingFlags.Public).GetGetMethod();
		private static readonly MethodInfo bufferIDGetter = typeof(Buffer).GetProperty("ID", BindingFlags.Instance | BindingFlags.Public).GetGetMethod();

		private static int uniqueID;
		private static ModuleBuilder moduleBuilder;

		private readonly ConcurrentDictionary<Type, Type> generatedTypes;

		#endregion

		#region Properties

		private static ModuleBuilder ModuleBuilder
		{
			get
			{
				if (moduleBuilder == null)
				{
					moduleBuilder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName("Program.Emit"), AssemblyBuilderAccess.Run).DefineDynamicModule("Module");
				}
				return moduleBuilder;
			}
		}

		#endregion
		
		#region Methods

		private static void overrideSimpleProperty(TypeBuilder typeBuilder, PropertyInfo property, FieldInfo backingField, FieldInfo changeField = null)
		{
			Type t = backingField.FieldType;

			// Getter.
			MethodInfo getter = property.GetGetMethod();
			if (getter != null)
			{
				MethodILBuilder emit = MethodILBuilder.CreateOverride(typeBuilder, getter);

				emit.LoadArgument(0);
				emit.LoadField(backingField);
				emit.Return();

				typeBuilder.DefineMethodOverride(emit.Compile(), getter);
			}

			// Setter.
			MethodInfo setter = property.GetSetMethod();
			if (setter != null)
			{
				MethodILBuilder emit = MethodILBuilder.CreateOverride(typeBuilder, setter);

				if (changeField != null)
				{
					// Compare values.
					Label equalLabel = emit.DefineLabel();
					Label equalEndLabel = emit.DefineLabel();

					if (t.IsPrimitive)
					{
						emit.LoadArgument(0);
						emit.LoadField(backingField);

						emit.LoadArgument(1);

						emit.BranchIfEqual(equalLabel);
					}
					else
					{
						Type equatableInterface = typeof(IEquatable<>).MakeGenericType(t);
						Type[] ifaces = t.GetInterfaces();
						if (ifaces.Contains(equatableInterface))
						{
							MethodInfo equalsMethod = equatableInterface.GetMethod("Equals", BindingFlags.Public | BindingFlags.Instance);

							emit.LoadArgument(0);
							if (t.IsValueType)
								emit.LoadFieldAddress(backingField);
							else
								emit.LoadField(backingField);

							emit.LoadArgument(1);
							
							emit.CallVirtual(equalsMethod, t);
							emit.BranchIfTrue(equalLabel);
						}
						else
						{
							MethodInfo equalsMethod = typeof(object).GetMethod("Equals", BindingFlags.Public | BindingFlags.Static);

							emit.LoadArgument(0);
							emit.LoadField(backingField);
							if (t.IsValueType) emit.Box(t);

							emit.LoadArgument(1);
							if (t.IsValueType) emit.Box(t);

							emit.Call(equalsMethod);
							emit.BranchIfTrue(equalLabel);
						}
					}

					// Set change field to true.
					emit.LoadArgument(0);
					emit.LoadConstant(true);
					emit.StoreField(changeField);
					emit.Branch(equalEndLabel);

					// If they are equal, do nothing.
					emit.MarkLabel(equalLabel);
					emit.Return();

					emit.MarkLabel(equalEndLabel);
				}

				emit.LoadArgument(0);
				emit.LoadArgument(1);
				emit.StoreField(backingField);
				emit.Return();

				typeBuilder.DefineMethodOverride(emit.Compile(), setter);
			}
		}

		private static FieldInfo getFirstField(Type type)
		{
			if (type == typeof(Vector2) ||
				type == typeof(Vector2I) ||
				type == typeof(Vector3) ||
				type == typeof(Vector3I) ||
				type == typeof(Vector4) ||
				type == typeof(Vector4I))
				return type.GetField("X");

			if (type == typeof(Matrix2x2) ||
				type == typeof(Matrix2x3) ||
				type == typeof(Matrix2x4) ||
				type == typeof(Matrix3x2) ||
				type == typeof(Matrix3x3) ||
				type == typeof(Matrix3x4) ||
				type == typeof(Matrix4x2) ||
				type == typeof(Matrix4x3) ||
				type == typeof(Matrix4x4))
				return type.GetField("M11");

			throw new NotSupportedException($"Type {type} is not supported.");
		}

		private static FieldInfo getUniformMethodField(UniformKind kind, UniformBaseType baseType, bool array)
		{
			StringBuilder name = new StringBuilder();
			name.Append("gl");

			switch (kind)
			{
				case UniformKind.Uniform1:
					name.Append("Uniform1");
					break;
				case UniformKind.Uniform2:
					name.Append("Uniform2");
					break;
				case UniformKind.Uniform3:
					name.Append("Uniform3");
					break;
				case UniformKind.Uniform4:
					name.Append("Uniform4");
					break;
				case UniformKind.UniformMatrix2:
					name.Append("UniformMatrix2");
					break;
				case UniformKind.UniformMatrix3:
					name.Append("UniformMatrix3");
					break;
				case UniformKind.UniformMatrix4:
					name.Append("UniformMatrix4");
					break;
				case UniformKind.UniformMatrix2x3:
					name.Append("UniformMatrix2x3");
					break;
				case UniformKind.UniformMatrix3x2:
					name.Append("UniformMatrix3x2");
					break;
				case UniformKind.UniformMatrix2x4:
					name.Append("UniformMatrix2x4");
					break;
				case UniformKind.UniformMatrix4x2:
					name.Append("UniformMatrix4x2");
					break;
				case UniformKind.UniformMatrix3x4:
					name.Append("UniformMatrix3x4");
					break;
				case UniformKind.UniformMatrix4x3:
					name.Append("UniformMatrix4x3");
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(kind), kind, null);
			}

			switch (baseType)
			{
				case UniformBaseType.Float:
					name.Append("f");
					break;
				case UniformBaseType.Integer:
					name.Append("i");
					break;
				case UniformBaseType.UnsignedInteger:
					name.Append("ui");
					break;
				case UniformBaseType.Double:
					name.Append("d");
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(baseType), baseType, null);
			}

			if (array)
				name.Append("v");

			FieldInfo field = typeof(GLBindings).GetField(name.ToString(), BindingFlags.Instance | BindingFlags.Public);
			if (field == null)
				throw new ArgumentException($"No uniform setter method: {name}");

			return field;
		}

		private static void callUniformSetter(ILBuilder emit, FieldInfo glField, FieldInfo field, Uniform uniform, bool transpose)
		{
			if (emit == null)
				throw new ArgumentNullException(nameof(emit));
			
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			if (uniform == null)
				throw new ArgumentNullException(nameof(uniform));

			UniformBaseType baseType = UniformUtil.GetBaseType(uniform.Type);
			UniformKind kind = UniformUtil.GetKind(uniform.Type);
			int numElements = UniformUtil.GetElementCount(kind);

			if (field.FieldType.IsArray)
			{
				Type elementType = field.FieldType.GetElementType();

				if (baseType == UniformBaseType.Float && elementType != typeof(float))
					throw new ArgumentException($"Incompatible types. Expected a float array for uniform {uniform}.");

				if ((baseType == UniformBaseType.Integer || baseType == UniformBaseType.UnsignedInteger) && elementType != typeof(int) && elementType != typeof(uint))
					throw new ArgumentException($"Incompatible types. Expected an (unsigned) integer array for uniform {uniform}.");

				if (baseType == UniformBaseType.Float && elementType != typeof(double))
					throw new ArgumentException($"Incompatible types. Expected a double array for uniform {uniform}.");

				using Local pinnedArray = emit.DeclareLocal(elementType.MakePointerType(), pinned: true);
				Label arrayNullLabel = emit.DefineLabel();
				Label arrayTooSmallLabel = emit.DefineLabel();
				Label endLabel = emit.DefineLabel();

				// Load array.
				emit.LoadArgument(0);
				emit.LoadField(field);
					
				// Check if array is null.
				emit.BranchIfFalse(arrayNullLabel);
					
				// Check if array.Length < numElements * arraySize.
				emit.LoadArgument(0);
				emit.LoadField(field);
				emit.LoadLength();
				emit.ConvertToInt32();
				emit.LoadConstant(numElements * uniform.ArraySize);
				emit.BranchIfLess(arrayTooSmallLabel);

				// Load array and store address of first element.
				emit.LoadArgument(0);
				emit.LoadField(field);
				emit.LoadConstant(0);
				emit.LoadElementAddress(elementType);
				emit.StoreLocal(pinnedArray);
				emit.Branch(endLabel);

				// Throw ArgumentNullException.
				emit.MarkLabel(arrayNullLabel);
				emit.LoadConstant(uniform.Name);
				emit.NewObject(argumentNullExceptionConstructor);
				emit.Throw();
					
				// Throw ArgumentException.
				emit.MarkLabel(arrayTooSmallLabel);
				emit.LoadConstant("Array is too small.");
				emit.LoadConstant(uniform.Name);
				emit.NewObject(argumentExceptionConstructor);
				emit.Throw();

				emit.MarkLabel(endLabel);

				// Call method.
				emit.LoadArgument(0);
				emit.LoadField(glField);
				FieldInfo uniformSetterField = getUniformMethodField(kind, baseType, true);
				MethodInfo invokeMethod = uniformSetterField.FieldType.GetMethod("Invoke");
				emit.LoadField(uniformSetterField);
				emit.LoadConstant(uniform.Location);
				emit.LoadConstant(uniform.ArraySize);

				if (UniformUtil.GetCategory(uniform.Type) == UniformTypeCategory.Matrix)
				{
					// Transpose true/false for matrices.
					emit.LoadConstant(transpose ? 1 : 0);
				}

				emit.LoadLocal(pinnedArray);
				emit.ConvertToNativeInt();
				emit.CallVirtual(invokeMethod);

				// Unpin array.
				emit.LoadConstant(0);
				emit.ConvertToNativeInt();
				emit.StoreLocal(pinnedArray);
			}
			else
			{
				if (field.FieldType == typeof(bool))
				{
					using Local value = emit.DeclareLocal(typeof(int));
					emit.LoadArgument(0);
					emit.LoadField(field);
					emit.LoadConstant(true);
					emit.CompareEqual();
					emit.StoreLocal(value);

					// Call method.
					emit.LoadArgument(0);
					emit.LoadField(glField);
					FieldInfo uniformSetterField = getUniformMethodField(kind, baseType, false);
					MethodInfo invokeMethod = uniformSetterField.FieldType.GetMethod("Invoke");
					emit.LoadField(uniformSetterField);
					emit.LoadConstant(uniform.Location);
					emit.LoadLocal(value);
					emit.CallVirtual(invokeMethod);
				}
				else
				{
					using Local value = emit.DeclareLocal(field.FieldType.MakeByRefType(), pinned: true);
					// Load &field.
					emit.LoadArgument(0);
					emit.LoadFieldAddress(field);
					emit.StoreLocal(value);

					// Call method.
					emit.LoadArgument(0);
					emit.LoadField(glField);
					FieldInfo uniformSetterField = getUniformMethodField(kind, baseType, true);
					MethodInfo invokeMethod = uniformSetterField.FieldType.GetMethod("Invoke");
					emit.LoadField(uniformSetterField);
					emit.LoadConstant(uniform.Location);
					emit.LoadConstant(uniform.ArraySize);

					if (UniformUtil.GetCategory(uniform.Type) == UniformTypeCategory.Matrix)
					{
						// Transpose true/false for matrices.
						emit.LoadConstant(transpose ? 1 : 0);
					}

					emit.LoadLocal(value);
					emit.ConvertToNativeInt();
					emit.CallVirtual(invokeMethod);

					// Unpin array.
					emit.LoadConstant(0);
					emit.ConvertToNativeInt();
					emit.StoreLocal(value);
				}

			}
		}

		private static void bindUniformBlock(ILBuilder emit, FieldInfo glField, int bindingPoint, FieldInfo field)
		{
			if (emit == null)
				throw new ArgumentNullException(nameof(emit));
			
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			// Load binding into local.
			using Local binding = emit.DeclareLocal(typeof(UniformBlockBinding));
			emit.LoadArgument(0);
			emit.LoadField(field);
			emit.StoreLocal(binding);

			// if(binding != null)
			Label elseLabel = emit.DefineLabel();
			Label endLabel = emit.DefineLabel();
			emit.LoadLocal(binding);
			emit.BranchIfFalse(elseLabel);
			
			// glBindBufferRange(GL_UNIFORM_BUFFER, bindingPoint, binding.Buffer.ID, binding.Offset, binding.Size)
			emit.LoadArgument(0);
			emit.LoadField(glField);
			emit.LoadField(glBindBufferRangeField);
			emit.LoadConstant((int)BufferRangeTarget.UniformBuffer);
			emit.LoadConstant(bindingPoint);
			emit.LoadLocal(binding);
			emit.CallVirtual(uniformBlockBindingBufferGetter);
			emit.CallVirtual(bufferIDGetter);
			emit.LoadLocal(binding);
			emit.CallVirtual(uniformBlockBindingOffsetGetter);
			emit.LoadLocal(binding);
			emit.CallVirtual(uniformBlockBindingSizeGetter);
			emit.CallVirtual(glBindBufferRangeInvokeMethod);
			emit.Branch(endLabel);

			// else
			emit.MarkLabel(elseLabel);

			// glBindBufferRange(GL_UNIFORM_BUFFER, bindingPoint, 0, NULL, NULL)
			emit.LoadArgument(0);
			emit.LoadField(glField);
			emit.LoadField(glBindBufferRangeField);
			emit.LoadConstant((int)BufferRangeTarget.UniformBuffer);
			emit.LoadConstant(bindingPoint);
			emit.LoadConstant(0);
			emit.LoadConstant(0);
			emit.ConvertToNativeInt();
			emit.LoadConstant(0);
			emit.ConvertToNativeInt();
			emit.CallVirtual(glBindBufferRangeInvokeMethod);
				
			emit.MarkLabel(endLabel);
		}

		private Type buildType(Type interfaceType, bool changeTracking = true)
		{
			if (!interfaceType.IsInterface)
				throw new ArgumentException("Type must be an interface.", nameof(interfaceType));

			if (!interfaceType.GetInterfaces().Contains(typeof(IProgramState)) && interfaceType != typeof(IProgramState))
				throw new ArgumentException($"Type must inherit from {nameof(IProgramState)}", nameof(interfaceType));

			List<PropertyInfo> properties = new List<PropertyInfo>();

			// Scan interface.
			foreach(Type currentType in interfaceType.GetInterfaces().Append(interfaceType).Where(t => t != typeof(IProgramState)))
			{
				foreach (MemberInfo member in currentType.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly))
				{
					MethodInfo method = member as MethodInfo;
					if (method != null)
					{
						// Check if the method is a getter or setter for a property.
						if (method.DeclaringType.GetProperties().Any(p => p.GetSetMethod() == method || p.GetGetMethod() == method))
							continue;
					}

					PropertyInfo property = member as PropertyInfo;
					if (property == null)
						throw new ArgumentException($"Interface contains unsupported member \"{method}\".");

					properties.Add(property);
				}
			}

			// Define type.
			TypeBuilder typeBuilder = ModuleBuilder.DefineType($"Program{this.ID}.{interfaceType.Name}_{Interlocked.Increment(ref uniqueID)}", TypeAttributes.Public | TypeAttributes.Sealed);
			typeBuilder.AddInterfaceImplementation(interfaceType);

			// Override Program property.
			FieldBuilder programField = typeBuilder.DefineField("__program", typeof(Program), FieldAttributes.Private);
			overrideSimpleProperty(typeBuilder, programStateProgramProperty, programField);

			// Define field that contains GL bindings.
			FieldBuilder glField = typeBuilder.DefineField("__gl", typeof(GLBindings), FieldAttributes.Private);
			
			HashSet<FieldInfo> transposed = new HashSet<FieldInfo>();

			// Properties for uniforms and blocks.
			Dictionary<string, FieldBuilder> uniformFields = new Dictionary<string, FieldBuilder>();
			Dictionary<string, FieldBuilder> uniformChangeFields = null;

			if (changeTracking)
			{
				uniformChangeFields = new Dictionary<string, FieldBuilder>();
			}

			Dictionary<int, FieldBuilder> blockFields = new Dictionary<int, FieldBuilder>();
			foreach (PropertyInfo property in properties)
			{
				UniformBlockBindingAttribute blockBindingAttrib = property.GetCustomAttribute<UniformBlockBindingAttribute>();
				if (blockBindingAttrib != null)
				{
					if (blockFields.ContainsKey(blockBindingAttrib.Index))
						throw new ArgumentException($"A property for uniform block binding {blockBindingAttrib.Index} already exists.");

					// Add backing field.
					FieldBuilder field = typeBuilder.DefineField($"block{blockBindingAttrib.Index}", property.PropertyType, FieldAttributes.Private);
					blockFields.Add(blockBindingAttrib.Index, field);

					overrideSimpleProperty(typeBuilder, property, field);
				}
				else
				{
					// Get name of uniform.
					string uniformName = property.Name;
					UniformNameAttribute nameAttrib = property.GetCustomAttribute<UniformNameAttribute>();
					if (nameAttrib != null)
						uniformName = nameAttrib.Name;

					// Add backing field.
					if (!uniformFields.TryGetValue(uniformName, out FieldBuilder field))
					{
						field = typeBuilder.DefineField($"_{property.Name}", property.PropertyType, FieldAttributes.Private);
						uniformFields.Add(uniformName, field);
					}

					// Add change field.
					FieldBuilder changeField = null;
					if (changeTracking)
					{
						if (!uniformChangeFields.TryGetValue(uniformName, out changeField))
						{
							changeField = typeBuilder.DefineField($"_{property.Name}<changed>", typeof(bool), FieldAttributes.Private);
							uniformChangeFields.Add(uniformName, changeField);
						}
					}

					// Matrix transpose.
					UniformMatrixTransposeAttribute transposeAttrib = property.GetCustomAttribute<UniformMatrixTransposeAttribute>();
					if (transposeAttrib != null && transposeAttrib.Transpose)
						transposed.Add(field);

					overrideSimpleProperty(typeBuilder, property, field, changeField);
				}
			}

			// Apply method.
			MethodILBuilder emit = MethodILBuilder.CreateOverride(typeBuilder, programStateApplyMethod);

			// Apply uniforms.
			foreach (Uniform uniform in this.Uniforms)
			{
				uniformFields.TryGetValue(uniform.Name, out FieldBuilder field);
				if (field == null)
					continue;

				FieldBuilder changeField = null;
				uniformChangeFields?.TryGetValue(uniform.Name, out changeField);
				Label setterEnd = null;
				if (changeField != null)
				{
					// Check if change field was set to true.
					setterEnd = emit.DefineLabel();
					emit.LoadArgument(0);
					emit.LoadField(changeField);
					emit.BranchIfFalse(setterEnd);

					// Set it to false.
					emit.LoadArgument(0);
					emit.LoadConstant(false);
					emit.StoreField(changeField);
				}

				callUniformSetter(emit, glField, field, uniform, transposed.Contains(field));
				
				if (setterEnd != null)
					emit.MarkLabel(setterEnd);
			}

			// Bind blocks.
			foreach (KeyValuePair<int, FieldBuilder> blockField in blockFields)
			{
				bindUniformBlock(emit, glField, blockField.Key, blockField.Value);
			}

			emit.Return();

			// Override "Apply" method.
			typeBuilder.DefineMethodOverride(emit.Compile(), programStateApplyMethod);

			// Constructor.
			ConstructorILBuilder ctor = ConstructorILBuilder.Create(new[] {typeof(Program)}, typeBuilder, MethodAttributes.Public);
			
			// this.program = program;
			ctor.LoadArgument(0);
			ctor.LoadArgument(1);
			ctor.StoreField(programField);

			// this.gl = program.Context.GL.Bindings;
			ctor.LoadArgument(0);
			ctor.LoadArgument(0);
			ctor.LoadField(programField);
			ctor.CallVirtual(programGetContextMethod);
			ctor.CallVirtual(contextGetGLMethod);
			ctor.LoadField(glBindingsField);
			ctor.StoreField(glField);
			
			if (uniformChangeFields != null)
			{
				// Initialize all change fields to true.
				foreach (FieldBuilder changeField in uniformChangeFields.Values)
				{
					ctor.LoadArgument(0);
					ctor.LoadConstant(true);
					ctor.StoreField(changeField);
				}
			}

			ctor.Return();
			ctor.Compile();
			
			return typeBuilder.CreateTypeInfo();
		}

		public T CreateState<T>()
			where T : IProgramState
		{
			Type interfaceType = typeof(T);
			Type generatedtype = this.generatedTypes.GetOrAdd(interfaceType, this.buildType(interfaceType));
			T instance = (T)Activator.CreateInstance(generatedtype, this);
			return instance;
		}

		#endregion
	}
}
