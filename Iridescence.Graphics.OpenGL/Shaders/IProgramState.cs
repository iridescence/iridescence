﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Common interface for program states.
	/// </summary>
	public interface IProgramState
	{
		/// <summary>
		/// Gets the underlying <see cref="Program"/>.
		/// </summary>
		Program Program { get; }

		/// <summary>
		/// Applies the state to its underyling <see cref="Program"/>.
		/// </summary>
		void Apply();
	}
}
