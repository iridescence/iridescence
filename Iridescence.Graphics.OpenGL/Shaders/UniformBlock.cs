﻿using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a uniform block.
	/// </summary>
	public sealed class UniformBlock
	{
		#region Fields

		private readonly List<UniformBlockMember> members;
		private int binding;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the program this uniform block belongs to.
		/// </summary>
		public Program Program { get; }

		/// <summary>
		/// Gets the block index.
		/// </summary>
		public int Index { get; }

		/// <summary>
		/// Gets the name of the block.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets the size of the storage needed to hold this uniform block's data.
		/// </summary>
		public int Size { get; }

		/// <summary>
		/// Gets or sets the current buffer binding point associated with this uniform block
		/// </summary>
		public int Binding
		{
			get => this.binding;
			set
			{
				this.binding = value;
				this.Program.Context.Invoke(() =>
				{
					this.Program.Context.GL.UniformBlockBinding(this.Program.ID, (uint)this.Index, (uint)value);
				});
			}
		}

		/// <summary>
		/// Gets the members of the block.
		/// </summary>
		public IReadOnlyList<UniformBlockMember> Members => this.members;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new UniformBlock.
		/// </summary>
		internal UniformBlock(Program program, int index, string name, int size, int binding)
		{
			this.Program = program;
			this.members = new List<UniformBlockMember>();
			this.Index = index;
			this.Name = name;
			this.Size = size;
			this.binding = binding;
		}

		#endregion

		#region Methods

		internal void AddMember(UniformBlockMember member)
		{
			this.members.Add(member);
		}

		/// <summary>
		/// Returns the block member with the specified name.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public UniformBlockMember GetMember(string name)
		{
			return this.members.FirstOrDefault(member => member.Name == name);
		}

		public override string ToString()
		{
			return this.Name;
		}

		#endregion
	}
}
