﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a geometry shader.
	/// </summary>
	public sealed class GeometryShader : Shader
	{
		#region Constructors

		/// <summary>
		/// Creates a new GeometryShader.
		/// </summary>
		public GeometryShader(GLContext context, params string[] sources)
			: base(context, ShaderType.GeometryShader, sources)
		{

		}

		#endregion
	}
}
