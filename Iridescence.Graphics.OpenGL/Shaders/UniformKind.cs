﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Defines the API base method of a uniform (1, 2, 3, 4 or Matrix*)
	/// </summary>
	public enum UniformKind
	{
		None,
		Uniform1,
		Uniform2,
		Uniform3,
		Uniform4,
		UniformMatrix2,
		UniformMatrix3,
		UniformMatrix4,
		UniformMatrix2x3,
		UniformMatrix3x2,
		UniformMatrix2x4,
		UniformMatrix4x2,
		UniformMatrix3x4,
		UniformMatrix4x3,
	}
}
