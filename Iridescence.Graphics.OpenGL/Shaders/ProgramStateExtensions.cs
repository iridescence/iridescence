﻿using System.Collections.Generic;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// <see cref="IProgramState"/> extension methods.
	/// </summary>
	public static class ProgramStateExtensions
	{
		public static void Draw<T>(this IProgramState programState, VertexArray vertexArray, T drawInfo)
			where T : class, IDrawInfo
		{
			programState.Program.Context.Draw(programState, vertexArray, drawInfo);
		}

		public static void Draw<T>(this IProgramState programState, VertexArray vertexArray, in T drawInfo)
			where T : struct, IDrawInfo
		{
			programState.Program.Context.Draw(programState, vertexArray, drawInfo);
		}

		public static void Draw<T>(this IProgramState programState, VertexArray vertexArray, params T[] drawInfos)
			where T : IDrawInfo
		{
			programState.Program.Context.Draw(programState, vertexArray, drawInfos);
		}

		public static void Draw<T>(this IProgramState programState, VertexArray vertexArray, IEnumerable<T> drawInfos)
			where T : IDrawInfo
		{
			programState.Program.Context.Draw(programState, vertexArray, drawInfos);
		}
	}
}
