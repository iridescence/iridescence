﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Exception thrown when linking of a program fails.
	/// </summary>
	[Serializable]
	public class ProgramLinkingException : Exception
	{
		#region Properties

		/// <summary>
		/// Gets the shader info log.
		/// </summary>
		public string InfoLog { get; }

		#endregion

		#region Constructors

		protected ProgramLinkingException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			this.InfoLog = info.GetString("InfoLog");
		}
		
		/// <summary>
		/// Creates a new ProgramLinkingException.
		/// </summary>
		public ProgramLinkingException(string message, string infoLog)
			: base(message)
		{
			this.InfoLog = infoLog;
		}

		#endregion

		#region Methods

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("InfoLog", this.InfoLog);
		}

		public override string ToString()
		{
			return $"{this.Message}\n{this.InfoLog}";
		}

		#endregion
	}
}
