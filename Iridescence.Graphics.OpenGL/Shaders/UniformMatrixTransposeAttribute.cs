﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Specifies whether a uniform matrix should be transposed when passed to the shader.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class UniformMatrixTransposeAttribute : Attribute
	{
		#region Properties

		/// <summary>
		/// Gets a value that indicates whether the matrix should be transposed.
		/// </summary>
		public bool Transpose { get; }

		#endregion

		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="UniformMatrixTransposeAttribute"/> class with <see cref="Transpose"/> set to true.
		/// </summary>
		public UniformMatrixTransposeAttribute()
			: this(true)
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="UniformMatrixTransposeAttribute"/> class. 
		/// </summary>
		public UniformMatrixTransposeAttribute(bool transpose)
		{
			this.Transpose = transpose;
		}

		#endregion
	}
}
