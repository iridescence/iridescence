﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a tesselation evaluation shader.
	/// </summary>
	public sealed class TessEvaluationShader : Shader
	{
		#region Constructors

		/// <summary>
		/// Creates a new TessEvaluationShader.
		/// </summary>
		public TessEvaluationShader(GLContext context, params string[] sources)
			: base(context, ShaderType.TessEvaluationShader, sources)
		{

		}

		#endregion
	}
}
