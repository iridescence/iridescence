﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Specifies the name of a uniform that coresponds to a property.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class UniformNameAttribute : Attribute
	{
		#region Properties

		/// <summary>
		/// Gets the property name.
		/// </summary>
		public string Name { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="UniformNameAttribute"/> class. 
		/// </summary>
		public UniformNameAttribute(string name)
		{
			this.Name = name;
		}

		#endregion
	}
}
