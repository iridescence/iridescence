﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Utility functions for uniforms.
	/// </summary>
	public static class UniformUtil
	{
		#region Methods

		/// <summary>
		/// Returns the display name for the specified <see cref="ActiveUniformType"/>.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static string GetDisplayName(ActiveUniformType type)
		{
			switch (type)
			{
				case ActiveUniformType.Float:
					return "float";
				case ActiveUniformType.FloatVec2:
					return "vec2";
				case ActiveUniformType.FloatVec3:
					return "vec3";
				case ActiveUniformType.FloatVec4:
					return "vec4";

				case ActiveUniformType.Double:
					return "double";
				case ActiveUniformType.DoubleVec2:
					return "dvec2";
				case ActiveUniformType.DoubleVec3:
					return "dvec3";
				case ActiveUniformType.DoubleVec4:
					return "dvec4";

				case ActiveUniformType.Int:
					return "int";
				case ActiveUniformType.IntVec2:
					return "ivec2";
				case ActiveUniformType.IntVec3:
					return "ivec3";
				case ActiveUniformType.IntVec4:
					return "ivec4";

				case ActiveUniformType.UnsignedInt:
					return "uint";
				case ActiveUniformType.UnsignedIntVec2:
					return "uvec2";
				case ActiveUniformType.UnsignedIntVec3:
					return "uvec3";
				case ActiveUniformType.UnsignedIntVec4:
					return "uvec4";

				case ActiveUniformType.Bool:
					return "bool";
				case ActiveUniformType.BoolVec2:
					return "bvec2";
				case ActiveUniformType.BoolVec3:
					return "bvec3";
				case ActiveUniformType.BoolVec4:
					return "bvec4";

				case ActiveUniformType.FloatMat2:
					return "mat2";
				case ActiveUniformType.FloatMat3:
					return "mat3";
				case ActiveUniformType.FloatMat4:
					return "mat4";
				case ActiveUniformType.FloatMat2x3:
					return "mat2x3";
				case ActiveUniformType.FloatMat2x4:
					return "mat2x4";
				case ActiveUniformType.FloatMat3x2:
					return "mat3x2";
				case ActiveUniformType.FloatMat3x4:
					return "mat3x4";
				case ActiveUniformType.FloatMat4x2:
					return "mat4x2";
				case ActiveUniformType.FloatMat4x3:
					return "mat4x3";

				case ActiveUniformType.DoubleMat2:
					return "dmat2";
				case ActiveUniformType.DoubleMat3:
					return "dmat3";
				case ActiveUniformType.DoubleMat4:
					return "dmat4";
				case ActiveUniformType.DoubleMat2x3:
					return "dmat2x3";
				case ActiveUniformType.DoubleMat2x4:
					return "dmat2x4";
				case ActiveUniformType.DoubleMat3x2:
					return "dmat3x2";
				case ActiveUniformType.DoubleMat3x4:
					return "dmat3x4";
				case ActiveUniformType.DoubleMat4x2:
					return "dmat4x2";
				case ActiveUniformType.DoubleMat4x3:
					return "dmat4x3";

				case ActiveUniformType.Sampler1D:
					return "sampler1D";
				case ActiveUniformType.Sampler2D:
					return "sampler2D";
				case ActiveUniformType.Sampler3D:
					return "sampler3D";
				case ActiveUniformType.SamplerCube:
					return "samplerCube";
				case ActiveUniformType.Sampler1DShadow:
					return "sampler1DShadow";
				case ActiveUniformType.Sampler2DShadow:
					return "sampler2DShadow";
				case ActiveUniformType.Sampler1DArray:
					return "sampler1DArray";
				case ActiveUniformType.Sampler2DArray:
					return "sampler2DArray";
				case ActiveUniformType.Sampler1DArrayShadow:
					return "sampler1DArrayShadow";
				case ActiveUniformType.Sampler2DArrayShadow:
					return "sampler2DArrayShadow";
				case ActiveUniformType.Sampler2DMultisample:
					return "sampler2DMS";
				case ActiveUniformType.Sampler2DMultisampleArray:
					return "sampler2DMSArray";
				case ActiveUniformType.SamplerCubeShadow:
					return "samplerCubeShadow";
				case ActiveUniformType.SamplerCubeMapArray:
					return "samplerCubeArray";
				case ActiveUniformType.SamplerCubeMapArrayShadow:
					return "samplerCubeArrayShadow";
				case ActiveUniformType.SamplerBuffer:
					return "samplerBuffer";
				case ActiveUniformType.Sampler2DRect:
					return "sampler2DRect";
				case ActiveUniformType.Sampler2DRectShadow:
					return "sampler2DRectShadow";

				case ActiveUniformType.IntSampler1D:
					return "isampler1D";
				case ActiveUniformType.IntSampler2D:
					return "isampler2D";
				case ActiveUniformType.IntSampler3D:
					return "isampler3D";
				case ActiveUniformType.IntSamplerCube:
					return "isamplerCube";
				case ActiveUniformType.IntSampler1DArray:
					return "isampler1DArray";
				case ActiveUniformType.IntSampler2DArray:
					return "isampler2DArray";
				case ActiveUniformType.IntSampler2DMultisample:
					return "isampler2DMS";
				case ActiveUniformType.IntSampler2DMultisampleArray:
					return "isampler2DMSArray";
				case ActiveUniformType.IntSamplerCubeMapArray:
					return "isamplerCubeArray";
				case ActiveUniformType.IntSamplerBuffer:
					return "isamplerBuffer";
				case ActiveUniformType.IntSampler2DRect:
					return "isampler2DRect";

				case ActiveUniformType.UnsignedIntSampler1D:
					return "usampler1D";
				case ActiveUniformType.UnsignedIntSampler2D:
					return "usampler2D";
				case ActiveUniformType.UnsignedIntSampler3D:
					return "usampler3D";
				case ActiveUniformType.UnsignedIntSamplerCube:
					return "usamplerCube";
				case ActiveUniformType.UnsignedIntSampler1DArray:
					return "usampler1DArray";
				case ActiveUniformType.UnsignedIntSampler2DArray:
					return "usampler2DArray";
				case ActiveUniformType.UnsignedIntSampler2DMultisample:
					return "usampler2DMS";
				case ActiveUniformType.UnsignedIntSampler2DMultisampleArray:
					return "usampler2DMSArray";
				case ActiveUniformType.UnsignedIntSamplerCubeMapArray:
					return "usamplerCubeArray";
				case ActiveUniformType.UnsignedIntSamplerBuffer:
					return "usamplerBuffer";
				case ActiveUniformType.UnsignedIntSampler2DRect:
					return "usampler2DRect";

				case ActiveUniformType.Image1D:
					return "image1D";
				case ActiveUniformType.Image2D:
					return "image2D";
				case ActiveUniformType.Image3D:
					return "image3D";
				case ActiveUniformType.Image2DRect:
					return "image2DRect";
				case ActiveUniformType.ImageCube:
					return "imageCube";
				case ActiveUniformType.ImageBuffer:
					return "imageBuffer";
				case ActiveUniformType.Image1DArray:
					return "image1DArray";
				case ActiveUniformType.Image2DArray:
					return "image2DArray";
				case ActiveUniformType.ImageCubeMapArray:
					return "imageCubeArray";
				case ActiveUniformType.Image2DMultisample:
					return "image2DMS";
				case ActiveUniformType.Image2DMultisampleArray:
					return "image2DMSArray";

				case ActiveUniformType.IntImage1D:
					return "iimage1D";
				case ActiveUniformType.IntImage2D:
					return "iimage2D";
				case ActiveUniformType.IntImage3D:
					return "iimage3D";
				case ActiveUniformType.IntImage2DRect:
					return "iimage2DRect";
				case ActiveUniformType.IntImageCube:
					return "iimageCube";
				case ActiveUniformType.IntImageBuffer:
					return "iimageBuffer";
				case ActiveUniformType.IntImage1DArray:
					return "iimage1DArray";
				case ActiveUniformType.IntImage2DArray:
					return "iimage2DArray";
				case ActiveUniformType.IntImageCubeMapArray:
					return "iimageCubeArray";
				case ActiveUniformType.IntImage2DMultisample:
					return "iimage2DMS";
				case ActiveUniformType.IntImage2DMultisampleArray:
					return "iimage2DMSArray";

				case ActiveUniformType.UnsignedIntImage1D:
					return "uimage1D";
				case ActiveUniformType.UnsignedIntImage2D:
					return "uimage2D";
				case ActiveUniformType.UnsignedIntImage3D:
					return "uimage3D";
				case ActiveUniformType.UnsignedIntImage2DRect:
					return "uimage2DRect";
				case ActiveUniformType.UnsignedIntImageCube:
					return "uimageCube";
				case ActiveUniformType.UnsignedIntImageBuffer:
					return "uimageBuffer";
				case ActiveUniformType.UnsignedIntImage1DArray:
					return "uimage1DArray";
				case ActiveUniformType.UnsignedIntImage2DArray:
					return "uimage2DArray";
				case ActiveUniformType.UnsignedIntImageCubeMapArray:
					return "uimageCubeArray";
				case ActiveUniformType.UnsignedIntImage2DMultisample:
					return "uimage2DMS";
				case ActiveUniformType.UnsignedIntImage2DMultisampleArray:
					return "uimage2DMSArray";

				case ActiveUniformType.UnsignedIntAtomicCounter:
					return "atomic_uint";

				default:
					throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}
		}

		/// <summary>
		/// Maps a <see cref="ActiveUniformType"/> to a .NET or engine type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns>The <see cref="Type"/> that corresponds to the specified <see cref="ActiveUniformType"/>, or null if the type is not supported.</returns>
		public static Type MapToType(ActiveUniformType type)
		{
			switch (type)
			{
				case ActiveUniformType.Float:
					return typeof(float);
				case ActiveUniformType.FloatVec2:
					return typeof(Vector2);
				case ActiveUniformType.FloatVec3:
					return typeof(Vector3);
				case ActiveUniformType.FloatVec4:
					return typeof(Vector4);
				case ActiveUniformType.Double:
					return typeof(double);
				case ActiveUniformType.DoubleVec2:
					return null; //typeof(Vector2D);
				case ActiveUniformType.DoubleVec3:
					return null; //typeof(Vector3D);
				case ActiveUniformType.DoubleVec4:
					return null; //typeof(Vector4D);
				case ActiveUniformType.Int:
					return typeof(int);
				case ActiveUniformType.IntVec2:
					return typeof(Vector2I);
				case ActiveUniformType.IntVec3:
					return typeof(Vector3I);
				case ActiveUniformType.IntVec4:
					return typeof(Vector4I);
				case ActiveUniformType.UnsignedInt:
					return typeof(uint);
				case ActiveUniformType.UnsignedIntVec2:
					return null; //typeof(Vector2U);
				case ActiveUniformType.UnsignedIntVec3:
					return null; //typeof(Vector3U);
				case ActiveUniformType.UnsignedIntVec4:
					return null; //typeof(Vector4U);
				case ActiveUniformType.Bool:
					return typeof(bool);
				case ActiveUniformType.BoolVec2:
					return null; //typeof(Vector2B);
				case ActiveUniformType.BoolVec3:
					return null; //typeof(Vector3B);
				case ActiveUniformType.BoolVec4:
					return null; //typeof(Vector4B);
				case ActiveUniformType.FloatMat2:
					return typeof(Matrix2x2);
				case ActiveUniformType.FloatMat3:
					return typeof(Matrix3x3);
				case ActiveUniformType.FloatMat4:
					return typeof(Matrix4x4);
				case ActiveUniformType.FloatMat2x3:
					return typeof(Matrix2x3);
				case ActiveUniformType.FloatMat2x4:
					return typeof(Matrix2x4);
				case ActiveUniformType.FloatMat3x2:
					return typeof(Matrix3x2);
				case ActiveUniformType.FloatMat3x4:
					return typeof(Matrix3x4);
				case ActiveUniformType.FloatMat4x2:
					return typeof(Matrix4x2);
				case ActiveUniformType.FloatMat4x3:
					return typeof(Matrix4x3);
				case ActiveUniformType.DoubleMat2:
					return null; //typeof(Matrix2x2D);
				case ActiveUniformType.DoubleMat3:
					return null; //typeof(Matrix3x3D);
				case ActiveUniformType.DoubleMat4:
					return null; //typeof(Matrix4x4D);
				case ActiveUniformType.DoubleMat2x3:
					return null; //typeof(Matrix2x3D);
				case ActiveUniformType.DoubleMat2x4:
					return null; //typeof(Matrix2x4D);
				case ActiveUniformType.DoubleMat3x2:
					return null; //typeof(Matrix3x2D);
				case ActiveUniformType.DoubleMat3x4:
					return null; //typeof(Matrix3x2D);
				case ActiveUniformType.DoubleMat4x2:
					return null; //typeof(Matrix4x2D);
				case ActiveUniformType.DoubleMat4x3:
					return null; //typeof(Matrix4x3D);
				case ActiveUniformType.Sampler1D:
				case ActiveUniformType.Sampler2D:
				case ActiveUniformType.Sampler3D:
				case ActiveUniformType.SamplerCube:
				case ActiveUniformType.Sampler1DShadow:
				case ActiveUniformType.Sampler2DShadow:
				case ActiveUniformType.Sampler1DArray:
				case ActiveUniformType.Sampler2DArray:
				case ActiveUniformType.Sampler1DArrayShadow:
				case ActiveUniformType.Sampler2DArrayShadow:
				case ActiveUniformType.Sampler2DMultisample:
				case ActiveUniformType.Sampler2DMultisampleArray:
				case ActiveUniformType.SamplerCubeShadow:
				case ActiveUniformType.SamplerCubeMapArray:
				case ActiveUniformType.SamplerCubeMapArrayShadow:								
				case ActiveUniformType.SamplerBuffer:
				case ActiveUniformType.Sampler2DRect:
				case ActiveUniformType.Sampler2DRectShadow:
				case ActiveUniformType.IntSampler1D:
				case ActiveUniformType.IntSampler2D:
				case ActiveUniformType.IntSampler3D:
				case ActiveUniformType.IntSamplerCube:
				case ActiveUniformType.IntSampler1DArray:
				case ActiveUniformType.IntSampler2DArray:
				case ActiveUniformType.IntSampler2DMultisample:
				case ActiveUniformType.IntSampler2DMultisampleArray:
				case ActiveUniformType.IntSamplerCubeMapArray:
				case ActiveUniformType.IntSamplerBuffer:
				case ActiveUniformType.IntSampler2DRect:
				case ActiveUniformType.UnsignedIntSampler1D:
				case ActiveUniformType.UnsignedIntSampler2D:
				case ActiveUniformType.UnsignedIntSampler3D:
				case ActiveUniformType.UnsignedIntSamplerCube:
				case ActiveUniformType.UnsignedIntSampler1DArray:
				case ActiveUniformType.UnsignedIntSampler2DArray:
				case ActiveUniformType.UnsignedIntSampler2DMultisample:
				case ActiveUniformType.UnsignedIntSampler2DMultisampleArray:
				case ActiveUniformType.UnsignedIntSamplerCubeMapArray:
				case ActiveUniformType.UnsignedIntSamplerBuffer:
				case ActiveUniformType.UnsignedIntSampler2DRect:
				case ActiveUniformType.Image1D:
				case ActiveUniformType.Image2D:
				case ActiveUniformType.Image3D:
				case ActiveUniformType.Image2DRect:
				case ActiveUniformType.ImageCube:
				case ActiveUniformType.ImageBuffer:
				case ActiveUniformType.Image1DArray:
				case ActiveUniformType.Image2DArray:
				case ActiveUniformType.ImageCubeMapArray:
				case ActiveUniformType.Image2DMultisample:
				case ActiveUniformType.Image2DMultisampleArray:
				case ActiveUniformType.IntImage1D:
				case ActiveUniformType.IntImage2D:
				case ActiveUniformType.IntImage3D:
				case ActiveUniformType.IntImage2DRect:
				case ActiveUniformType.IntImageCube:
				case ActiveUniformType.IntImageBuffer:
				case ActiveUniformType.IntImage1DArray:
				case ActiveUniformType.IntImage2DArray:
				case ActiveUniformType.IntImageCubeMapArray:
				case ActiveUniformType.IntImage2DMultisample:
				case ActiveUniformType.IntImage2DMultisampleArray:
				case ActiveUniformType.UnsignedIntImage1D:
				case ActiveUniformType.UnsignedIntImage2D:
				case ActiveUniformType.UnsignedIntImage3D:
				case ActiveUniformType.UnsignedIntImage2DRect:
				case ActiveUniformType.UnsignedIntImageCube:
				case ActiveUniformType.UnsignedIntImageBuffer:
				case ActiveUniformType.UnsignedIntImage1DArray:
				case ActiveUniformType.UnsignedIntImage2DArray:
				case ActiveUniformType.UnsignedIntImageCubeMapArray:
				case ActiveUniformType.UnsignedIntImage2DMultisample:
				case ActiveUniformType.UnsignedIntImage2DMultisampleArray:
					return typeof(int);
				case ActiveUniformType.UnsignedIntAtomicCounter:
					return typeof(uint);
				
				default:
					throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}
		}

		/// <summary>
		/// Returns the category of the specified <see cref="ActiveUniformType"/>.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static UniformTypeCategory GetCategory(ActiveUniformType type)
		{
			switch (type)
			{
				case ActiveUniformType.Float:
					return UniformTypeCategory.Scalar;
				case ActiveUniformType.FloatVec2:
				case ActiveUniformType.FloatVec3:
				case ActiveUniformType.FloatVec4:
					return UniformTypeCategory.Vector;
				case ActiveUniformType.Double:
					return UniformTypeCategory.Scalar;
				case ActiveUniformType.DoubleVec2:
				case ActiveUniformType.DoubleVec3:
				case ActiveUniformType.DoubleVec4:
					return UniformTypeCategory.Vector;
				case ActiveUniformType.Int:
					return UniformTypeCategory.Scalar;
				case ActiveUniformType.IntVec2:
				case ActiveUniformType.IntVec3:
				case ActiveUniformType.IntVec4:
					return UniformTypeCategory.Vector;
				case ActiveUniformType.UnsignedInt:
					return UniformTypeCategory.Scalar;
				case ActiveUniformType.UnsignedIntVec2:
				case ActiveUniformType.UnsignedIntVec3:
				case ActiveUniformType.UnsignedIntVec4:
					return UniformTypeCategory.Vector;
				case ActiveUniformType.Bool:
					return UniformTypeCategory.Scalar;
				case ActiveUniformType.BoolVec2:
				case ActiveUniformType.BoolVec3:
				case ActiveUniformType.BoolVec4:
					return UniformTypeCategory.Vector;
				case ActiveUniformType.FloatMat2:
				case ActiveUniformType.FloatMat3:
				case ActiveUniformType.FloatMat4:
				case ActiveUniformType.FloatMat2x3:
				case ActiveUniformType.FloatMat2x4:
				case ActiveUniformType.FloatMat3x2:
				case ActiveUniformType.FloatMat3x4:
				case ActiveUniformType.FloatMat4x2:
				case ActiveUniformType.FloatMat4x3:
				case ActiveUniformType.DoubleMat2:
				case ActiveUniformType.DoubleMat3:
				case ActiveUniformType.DoubleMat4:
				case ActiveUniformType.DoubleMat2x3:
				case ActiveUniformType.DoubleMat2x4:
				case ActiveUniformType.DoubleMat3x2:
				case ActiveUniformType.DoubleMat3x4:
				case ActiveUniformType.DoubleMat4x2:
				case ActiveUniformType.DoubleMat4x3:
					return UniformTypeCategory.Matrix;
				case ActiveUniformType.Sampler1D:
				case ActiveUniformType.Sampler2D:
				case ActiveUniformType.Sampler3D:
				case ActiveUniformType.SamplerCube:
				case ActiveUniformType.Sampler1DShadow:
				case ActiveUniformType.Sampler2DShadow:
				case ActiveUniformType.Sampler1DArray:
				case ActiveUniformType.Sampler2DArray:
				case ActiveUniformType.Sampler1DArrayShadow:
				case ActiveUniformType.Sampler2DArrayShadow:
				case ActiveUniformType.Sampler2DMultisample:
				case ActiveUniformType.Sampler2DMultisampleArray:
				case ActiveUniformType.SamplerCubeShadow:
				case ActiveUniformType.SamplerCubeMapArray:
				case ActiveUniformType.SamplerCubeMapArrayShadow:
				case ActiveUniformType.SamplerBuffer:
				case ActiveUniformType.Sampler2DRect:
				case ActiveUniformType.Sampler2DRectShadow:
				case ActiveUniformType.IntSampler1D:
				case ActiveUniformType.IntSampler2D:
				case ActiveUniformType.IntSampler3D:
				case ActiveUniformType.IntSamplerCube:
				case ActiveUniformType.IntSampler1DArray:
				case ActiveUniformType.IntSampler2DArray:
				case ActiveUniformType.IntSampler2DMultisample:
				case ActiveUniformType.IntSampler2DMultisampleArray:
				case ActiveUniformType.IntSamplerCubeMapArray:
				case ActiveUniformType.IntSamplerBuffer:
				case ActiveUniformType.IntSampler2DRect:
				case ActiveUniformType.UnsignedIntSampler1D:
				case ActiveUniformType.UnsignedIntSampler2D:
				case ActiveUniformType.UnsignedIntSampler3D:
				case ActiveUniformType.UnsignedIntSamplerCube:
				case ActiveUniformType.UnsignedIntSampler1DArray:
				case ActiveUniformType.UnsignedIntSampler2DArray:
				case ActiveUniformType.UnsignedIntSampler2DMultisample:
				case ActiveUniformType.UnsignedIntSampler2DMultisampleArray:
				case ActiveUniformType.UnsignedIntSamplerCubeMapArray:
				case ActiveUniformType.UnsignedIntSamplerBuffer:
				case ActiveUniformType.UnsignedIntSampler2DRect:
					return UniformTypeCategory.Sampler;
				case ActiveUniformType.Image1D:
				case ActiveUniformType.Image2D:
				case ActiveUniformType.Image3D:
				case ActiveUniformType.Image2DRect:
				case ActiveUniformType.ImageCube:
				case ActiveUniformType.ImageBuffer:
				case ActiveUniformType.Image1DArray:
				case ActiveUniformType.Image2DArray:
				case ActiveUniformType.ImageCubeMapArray:
				case ActiveUniformType.Image2DMultisample:
				case ActiveUniformType.Image2DMultisampleArray:
				case ActiveUniformType.IntImage1D:
				case ActiveUniformType.IntImage2D:
				case ActiveUniformType.IntImage3D:
				case ActiveUniformType.IntImage2DRect:
				case ActiveUniformType.IntImageCube:
				case ActiveUniformType.IntImageBuffer:
				case ActiveUniformType.IntImage1DArray:
				case ActiveUniformType.IntImage2DArray:
				case ActiveUniformType.IntImageCubeMapArray:
				case ActiveUniformType.IntImage2DMultisample:
				case ActiveUniformType.IntImage2DMultisampleArray:
				case ActiveUniformType.UnsignedIntImage1D:
				case ActiveUniformType.UnsignedIntImage2D:
				case ActiveUniformType.UnsignedIntImage3D:
				case ActiveUniformType.UnsignedIntImage2DRect:
				case ActiveUniformType.UnsignedIntImageCube:
				case ActiveUniformType.UnsignedIntImageBuffer:
				case ActiveUniformType.UnsignedIntImage1DArray:
				case ActiveUniformType.UnsignedIntImage2DArray:
				case ActiveUniformType.UnsignedIntImageCubeMapArray:
				case ActiveUniformType.UnsignedIntImage2DMultisample:
				case ActiveUniformType.UnsignedIntImage2DMultisampleArray:
					return UniformTypeCategory.Image;
				case ActiveUniformType.UnsignedIntAtomicCounter:
					return UniformTypeCategory.Atomic;

				default:
					throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}
		}

		/// <summary>
		/// Returns the base type of the specified <see cref="ActiveUniformType"/>.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static UniformBaseType GetBaseType(ActiveUniformType type)
		{
			switch (type)
			{
				case ActiveUniformType.Float:
				case ActiveUniformType.FloatVec2:
				case ActiveUniformType.FloatVec3:
				case ActiveUniformType.FloatVec4:
					return UniformBaseType.Float;
				case ActiveUniformType.Double:
				case ActiveUniformType.DoubleVec2:
				case ActiveUniformType.DoubleVec3:
				case ActiveUniformType.DoubleVec4:
					return UniformBaseType.Double;
				case ActiveUniformType.Int:
				case ActiveUniformType.IntVec2:
				case ActiveUniformType.IntVec3:
				case ActiveUniformType.IntVec4:
					return UniformBaseType.Integer;
				case ActiveUniformType.UnsignedInt:
				case ActiveUniformType.UnsignedIntVec2:
				case ActiveUniformType.UnsignedIntVec3:
				case ActiveUniformType.UnsignedIntVec4:
					return UniformBaseType.UnsignedInteger;
				case ActiveUniformType.Bool:
				case ActiveUniformType.BoolVec2:
				case ActiveUniformType.BoolVec3:
				case ActiveUniformType.BoolVec4:
					return UniformBaseType.Integer;
				case ActiveUniformType.FloatMat2:
				case ActiveUniformType.FloatMat3:
				case ActiveUniformType.FloatMat4:
				case ActiveUniformType.FloatMat2x3:
				case ActiveUniformType.FloatMat2x4:
				case ActiveUniformType.FloatMat3x2:
				case ActiveUniformType.FloatMat3x4:
				case ActiveUniformType.FloatMat4x2:
				case ActiveUniformType.FloatMat4x3:
					return UniformBaseType.Float;
				case ActiveUniformType.DoubleMat2:
				case ActiveUniformType.DoubleMat3:
				case ActiveUniformType.DoubleMat4:
				case ActiveUniformType.DoubleMat2x3:
				case ActiveUniformType.DoubleMat2x4:
				case ActiveUniformType.DoubleMat3x2:
				case ActiveUniformType.DoubleMat3x4:
				case ActiveUniformType.DoubleMat4x2:
				case ActiveUniformType.DoubleMat4x3:
					return UniformBaseType.Double;
				case ActiveUniformType.Sampler1D:
				case ActiveUniformType.Sampler2D:
				case ActiveUniformType.Sampler3D:
				case ActiveUniformType.SamplerCube:
				case ActiveUniformType.Sampler1DShadow:
				case ActiveUniformType.Sampler2DShadow:
				case ActiveUniformType.Sampler1DArray:
				case ActiveUniformType.Sampler2DArray:
				case ActiveUniformType.Sampler1DArrayShadow:
				case ActiveUniformType.Sampler2DArrayShadow:
				case ActiveUniformType.Sampler2DMultisample:
				case ActiveUniformType.Sampler2DMultisampleArray:
				case ActiveUniformType.SamplerCubeShadow:
				case ActiveUniformType.SamplerCubeMapArray:
				case ActiveUniformType.SamplerCubeMapArrayShadow:
				case ActiveUniformType.SamplerBuffer:
				case ActiveUniformType.Sampler2DRect:
				case ActiveUniformType.Sampler2DRectShadow:
				case ActiveUniformType.IntSampler1D:
				case ActiveUniformType.IntSampler2D:
				case ActiveUniformType.IntSampler3D:
				case ActiveUniformType.IntSamplerCube:
				case ActiveUniformType.IntSampler1DArray:
				case ActiveUniformType.IntSampler2DArray:
				case ActiveUniformType.IntSampler2DMultisample:
				case ActiveUniformType.IntSampler2DMultisampleArray:
				case ActiveUniformType.IntSamplerCubeMapArray:
				case ActiveUniformType.IntSamplerBuffer:
				case ActiveUniformType.IntSampler2DRect:
				case ActiveUniformType.UnsignedIntSampler1D:
				case ActiveUniformType.UnsignedIntSampler2D:
				case ActiveUniformType.UnsignedIntSampler3D:
				case ActiveUniformType.UnsignedIntSamplerCube:
				case ActiveUniformType.UnsignedIntSampler1DArray:
				case ActiveUniformType.UnsignedIntSampler2DArray:
				case ActiveUniformType.UnsignedIntSampler2DMultisample:
				case ActiveUniformType.UnsignedIntSampler2DMultisampleArray:
				case ActiveUniformType.UnsignedIntSamplerCubeMapArray:
				case ActiveUniformType.UnsignedIntSamplerBuffer:
				case ActiveUniformType.UnsignedIntSampler2DRect:
				case ActiveUniformType.Image1D:
				case ActiveUniformType.Image2D:
				case ActiveUniformType.Image3D:
				case ActiveUniformType.Image2DRect:
				case ActiveUniformType.ImageCube:
				case ActiveUniformType.ImageBuffer:
				case ActiveUniformType.Image1DArray:
				case ActiveUniformType.Image2DArray:
				case ActiveUniformType.ImageCubeMapArray:
				case ActiveUniformType.Image2DMultisample:
				case ActiveUniformType.Image2DMultisampleArray:
				case ActiveUniformType.IntImage1D:
				case ActiveUniformType.IntImage2D:
				case ActiveUniformType.IntImage3D:
				case ActiveUniformType.IntImage2DRect:
				case ActiveUniformType.IntImageCube:
				case ActiveUniformType.IntImageBuffer:
				case ActiveUniformType.IntImage1DArray:
				case ActiveUniformType.IntImage2DArray:
				case ActiveUniformType.IntImageCubeMapArray:
				case ActiveUniformType.IntImage2DMultisample:
				case ActiveUniformType.IntImage2DMultisampleArray:
				case ActiveUniformType.UnsignedIntImage1D:
				case ActiveUniformType.UnsignedIntImage2D:
				case ActiveUniformType.UnsignedIntImage3D:
				case ActiveUniformType.UnsignedIntImage2DRect:
				case ActiveUniformType.UnsignedIntImageCube:
				case ActiveUniformType.UnsignedIntImageBuffer:
				case ActiveUniformType.UnsignedIntImage1DArray:
				case ActiveUniformType.UnsignedIntImage2DArray:
				case ActiveUniformType.UnsignedIntImageCubeMapArray:
				case ActiveUniformType.UnsignedIntImage2DMultisample:
				case ActiveUniformType.UnsignedIntImage2DMultisampleArray:
					return UniformBaseType.Integer;
				case ActiveUniformType.UnsignedIntAtomicCounter:
					return UniformBaseType.None;

				default:
					throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}
		}

		/// <summary>
		/// Returns the kind of uniform API call for the specified <see cref="ActiveUniformType"/>.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static UniformKind GetKind(ActiveUniformType type)
		{
			switch (type)
			{
				case ActiveUniformType.Float:
					return UniformKind.Uniform1;
				case ActiveUniformType.FloatVec2:
					return UniformKind.Uniform2;
				case ActiveUniformType.FloatVec3:
					return UniformKind.Uniform3;
				case ActiveUniformType.FloatVec4:
					return UniformKind.Uniform4;

				case ActiveUniformType.Double:
					return UniformKind.Uniform1;
				case ActiveUniformType.DoubleVec2:
					return UniformKind.Uniform2;
				case ActiveUniformType.DoubleVec3:
					return UniformKind.Uniform3;
				case ActiveUniformType.DoubleVec4:
					return UniformKind.Uniform4;

				case ActiveUniformType.Int:
					return UniformKind.Uniform1;
				case ActiveUniformType.IntVec2:
					return UniformKind.Uniform2;
				case ActiveUniformType.IntVec3:
					return UniformKind.Uniform3;
				case ActiveUniformType.IntVec4:
					return UniformKind.Uniform4;

				case ActiveUniformType.UnsignedInt:
					return UniformKind.Uniform1;
				case ActiveUniformType.UnsignedIntVec2:
					return UniformKind.Uniform2;
				case ActiveUniformType.UnsignedIntVec3:
					return UniformKind.Uniform3;
				case ActiveUniformType.UnsignedIntVec4:
					return UniformKind.Uniform4;

				case ActiveUniformType.Bool:
					return UniformKind.Uniform1;
				case ActiveUniformType.BoolVec2:
					return UniformKind.Uniform2;
				case ActiveUniformType.BoolVec3:
					return UniformKind.Uniform3;
				case ActiveUniformType.BoolVec4:
					return UniformKind.Uniform4;

				case ActiveUniformType.FloatMat2:
					return UniformKind.UniformMatrix2;
				case ActiveUniformType.FloatMat3:
					return UniformKind.UniformMatrix3;
				case ActiveUniformType.FloatMat4:
					return UniformKind.UniformMatrix4;
				case ActiveUniformType.FloatMat2x3:
					return UniformKind.UniformMatrix2x3;
				case ActiveUniformType.FloatMat2x4:
					return UniformKind.UniformMatrix2x4;
				case ActiveUniformType.FloatMat3x2:
					return UniformKind.UniformMatrix3x2;
				case ActiveUniformType.FloatMat3x4:
					return UniformKind.UniformMatrix3x4;
				case ActiveUniformType.FloatMat4x2:
					return UniformKind.UniformMatrix4x2;
				case ActiveUniformType.FloatMat4x3:
					return UniformKind.UniformMatrix4x3;

				case ActiveUniformType.DoubleMat2:
					return UniformKind.UniformMatrix2;
				case ActiveUniformType.DoubleMat3:
					return UniformKind.UniformMatrix3;
				case ActiveUniformType.DoubleMat4:
					return UniformKind.UniformMatrix4;
				case ActiveUniformType.DoubleMat2x3:
					return UniformKind.UniformMatrix2x3;
				case ActiveUniformType.DoubleMat2x4:
					return UniformKind.UniformMatrix2x4;
				case ActiveUniformType.DoubleMat3x2:
					return UniformKind.UniformMatrix3x2;
				case ActiveUniformType.DoubleMat3x4:
					return UniformKind.UniformMatrix3x4;
				case ActiveUniformType.DoubleMat4x2:
					return UniformKind.UniformMatrix4x2;
				case ActiveUniformType.DoubleMat4x3:
					return UniformKind.UniformMatrix4x3;

				case ActiveUniformType.Sampler1D:
				case ActiveUniformType.Sampler2D:
				case ActiveUniformType.Sampler3D:
				case ActiveUniformType.SamplerCube:
				case ActiveUniformType.Sampler1DShadow:
				case ActiveUniformType.Sampler2DShadow:
				case ActiveUniformType.Sampler1DArray:
				case ActiveUniformType.Sampler2DArray:
				case ActiveUniformType.Sampler1DArrayShadow:
				case ActiveUniformType.Sampler2DArrayShadow:
				case ActiveUniformType.Sampler2DMultisample:
				case ActiveUniformType.Sampler2DMultisampleArray:
				case ActiveUniformType.SamplerCubeShadow:
				case ActiveUniformType.SamplerCubeMapArray:
				case ActiveUniformType.SamplerCubeMapArrayShadow:
				case ActiveUniformType.SamplerBuffer:
				case ActiveUniformType.Sampler2DRect:
				case ActiveUniformType.Sampler2DRectShadow:
				case ActiveUniformType.IntSampler1D:
				case ActiveUniformType.IntSampler2D:
				case ActiveUniformType.IntSampler3D:
				case ActiveUniformType.IntSamplerCube:
				case ActiveUniformType.IntSampler1DArray:
				case ActiveUniformType.IntSampler2DArray:
				case ActiveUniformType.IntSampler2DMultisample:
				case ActiveUniformType.IntSampler2DMultisampleArray:
				case ActiveUniformType.IntSamplerCubeMapArray:
				case ActiveUniformType.IntSamplerBuffer:
				case ActiveUniformType.IntSampler2DRect:
				case ActiveUniformType.UnsignedIntSampler1D:
				case ActiveUniformType.UnsignedIntSampler2D:
				case ActiveUniformType.UnsignedIntSampler3D:
				case ActiveUniformType.UnsignedIntSamplerCube:
				case ActiveUniformType.UnsignedIntSampler1DArray:
				case ActiveUniformType.UnsignedIntSampler2DArray:
				case ActiveUniformType.UnsignedIntSampler2DMultisample:
				case ActiveUniformType.UnsignedIntSampler2DMultisampleArray:
				case ActiveUniformType.UnsignedIntSamplerCubeMapArray:
				case ActiveUniformType.UnsignedIntSamplerBuffer:
				case ActiveUniformType.UnsignedIntSampler2DRect:
				case ActiveUniformType.Image1D:
				case ActiveUniformType.Image2D:
				case ActiveUniformType.Image3D:
				case ActiveUniformType.Image2DRect:
				case ActiveUniformType.ImageCube:
				case ActiveUniformType.ImageBuffer:
				case ActiveUniformType.Image1DArray:
				case ActiveUniformType.Image2DArray:
				case ActiveUniformType.ImageCubeMapArray:
				case ActiveUniformType.Image2DMultisample:
				case ActiveUniformType.Image2DMultisampleArray:
				case ActiveUniformType.IntImage1D:
				case ActiveUniformType.IntImage2D:
				case ActiveUniformType.IntImage3D:
				case ActiveUniformType.IntImage2DRect:
				case ActiveUniformType.IntImageCube:
				case ActiveUniformType.IntImageBuffer:
				case ActiveUniformType.IntImage1DArray:
				case ActiveUniformType.IntImage2DArray:
				case ActiveUniformType.IntImageCubeMapArray:
				case ActiveUniformType.IntImage2DMultisample:
				case ActiveUniformType.IntImage2DMultisampleArray:
				case ActiveUniformType.UnsignedIntImage1D:
				case ActiveUniformType.UnsignedIntImage2D:
				case ActiveUniformType.UnsignedIntImage3D:
				case ActiveUniformType.UnsignedIntImage2DRect:
				case ActiveUniformType.UnsignedIntImageCube:
				case ActiveUniformType.UnsignedIntImageBuffer:
				case ActiveUniformType.UnsignedIntImage1DArray:
				case ActiveUniformType.UnsignedIntImage2DArray:
				case ActiveUniformType.UnsignedIntImageCubeMapArray:
				case ActiveUniformType.UnsignedIntImage2DMultisample:
				case ActiveUniformType.UnsignedIntImage2DMultisampleArray:
					return UniformKind.Uniform1;

				case ActiveUniformType.UnsignedIntAtomicCounter:
					return UniformKind.None;

				default:
					throw new ArgumentOutOfRangeException(nameof(type), type, null);
			}
		}

		/// <summary>
		/// Returns the number of elements per value.
		/// </summary>
		/// <param name="kind"></param>
		/// <returns></returns>
		public static int GetElementCount(UniformKind kind)
		{
			switch (kind)
			{
				case UniformKind.None:
					return 0;
				case UniformKind.Uniform1:
					return 1;
				case UniformKind.Uniform2:
					return 2;
				case UniformKind.Uniform3:
					return 3;
				case UniformKind.Uniform4:
					return 4;
				case UniformKind.UniformMatrix2:
					return 4;
				case UniformKind.UniformMatrix3:
					return 9;
				case UniformKind.UniformMatrix4:
					return 16;
				case UniformKind.UniformMatrix2x3:
					return 6;
				case UniformKind.UniformMatrix3x2:
					return 6;
				case UniformKind.UniformMatrix2x4:
					return 8;
				case UniformKind.UniformMatrix4x2:
					return 8;
				case UniformKind.UniformMatrix3x4:
					return 12;
				case UniformKind.UniformMatrix4x3:
					return 12;
				default:
					throw new ArgumentOutOfRangeException(nameof(kind), kind, null);
			}
		}

		#endregion
	}
}
