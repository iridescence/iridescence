﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL program.
	/// </summary>
	public partial class Program : GLContextChild, IGLObject
	{
		#region Fields

		private List<Shader> attachedShaders;
		private IReadOnlyList<Uniform> uniforms;
		private IReadOnlyList<UniformBlock> blocks;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the ID of the program.
		/// </summary>
		public uint ID { get; }

		/// <summary>
		/// Gets a list of uniforms.
		/// </summary>
		public IReadOnlyList<Uniform> Uniforms => this.uniforms ?? throw new ObjectDisposedException(nameof(Program));

		/// <summary>
		/// Gets a list of blocks.
		/// </summary>
		public IReadOnlyList<UniformBlock> Blocks => this.blocks ?? throw new ObjectDisposedException(nameof(Program));

		/// <summary>
		/// Gets a list of attached shaders.
		/// </summary>
		public IReadOnlyList<Shader> AttachedShaders => this.attachedShaders ?? throw new ObjectDisposedException(nameof(Program));

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Program.
		/// </summary>
		public Program(GLContext context)
			: base(context)
		{
			// create program.
			this.ID = (uint)this.Context.Invoke(this.Context.GL.CreateProgram);
			GLTrace.Instance.ObjectCreated(nameof(Program), this.ID);

			this.attachedShaders = new List<Shader>();
			this.generatedTypes = new ConcurrentDictionary<Type, Type>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Attaches the specified shader.
		///  </summary>
		/// <param name="shader">The shader object to attach.</param>
		/// <remarks>
		/// The program needs to be linked using the <see cref="Link"/> method after shaders have been attached.
		/// </remarks>
		public void Attach(Shader shader)
		{
			if(shader == null)
				throw new ArgumentNullException(nameof(shader));

			this.Context.Invoke(() => this.Context.GL.AttachShader(this.ID, shader.ID));

			this.attachedShaders.Add(shader);
		}

		/// <summary>
		/// Detaches the specified shader.
		/// </summary>
		/// <param name="shader">The shader object to detach.</param>
		/// <remarks>
		/// The program needs to be linked using the <see cref="Link"/> method after shaders have been detached.
		/// </remarks>
		public bool Detach(Shader shader)
		{
			if(shader == null)
				throw new ArgumentNullException(nameof(shader));

			this.Context.Invoke(() => this.Context.GL.DetachShader(this.ID, shader.ID));

			return this.attachedShaders.Remove(shader);
		}

		private void populateUniforms()
		{
			var gl = this.Context.GL;
			
			// Blocks.
			gl.GetProgram(this.ID, GetProgramParameterName.ActiveUniformBlocks, out int numBlocks);
			if (numBlocks > 0)
			{
				UniformBlock[] blocks = new UniformBlock[numBlocks];
				gl.GetProgram(this.ID, GetProgramParameterName.ActiveUniformBlockMaxNameLength, out int maxLength);

				// Workaround for a bug in the Intel HD OpenGL implementation.
				if (maxLength == 0) maxLength = 4096;

				StringBuilder str = new StringBuilder(maxLength);

				for (int i = 0; i < numBlocks; ++i)
				{
					gl.GetActiveUniformBlockName(this.ID, (uint)i, maxLength, out int length, str);
					string name = str.ToString(0, length);

					gl.GetActiveUniformBlock(this.ID, (uint)i, ActiveUniformBlockParameter.UniformBlockDataSize, out int dataSize);
					gl.GetActiveUniformBlock(this.ID, (uint)i, ActiveUniformBlockParameter.UniformBlockBinding, out int binding);

					blocks[i] = new UniformBlock(this, i, name, dataSize, binding);
				}
				this.blocks = blocks;
			}
			else
			{
				this.blocks = Array.Empty<UniformBlock>();
			}

			// Uniforms.
			gl.GetProgram(this.ID, GetProgramParameterName.ActiveUniforms, out int numUniforms);
			if (numUniforms > 0)
			{
				gl.GetProgram(this.ID, GetProgramParameterName.ActiveUniformMaxLength, out int maxLength);
				StringBuilder str = new StringBuilder(maxLength);

				List<Uniform> uniforms = new List<Uniform>();

				for (int i = 0; i < numUniforms; ++i)
				{
					uint uniformIndex = (uint)i;
					gl.GetActiveUniform(this.ID, uniformIndex, maxLength, out int length, out int size, out ActiveUniformType type, str);

					string name = str.ToString(0, length);

					// remove array index.
					if (name.EndsWith("[0]"))
						name = name.Substring(0, name.Length - 3);

					gl.GetActiveUniforms(this.ID, 1, ref uniformIndex, ActiveUniformParameter.UniformBlockIndex, out int blockIndex);

					if (blockIndex >= 0)
					{
						UniformBlock block = blocks[blockIndex];
						gl.GetActiveUniforms(this.ID, 1, ref uniformIndex, ActiveUniformParameter.UniformOffset, out int offset);

						block.AddMember(new UniformBlockMember(block, name, type, offset));
					}
					else
					{
						int loc = gl.GetUniformLocation(this.ID, name);

						Uniform uniform = new Uniform(name, type, loc, size);
						uniforms.Add(uniform);
					}
				}

				this.uniforms = uniforms.ToArray();
			}
			else
			{
				this.uniforms = Array.Empty<Uniform>();
			}
		}

		/// <summary>
		/// Links the program and populates the program info.
		/// </summary>
		/// <exception cref="ProgramLinkingException">The program failed to link.</exception>
		public void Link()
		{
			// Link the program.
			int linkStatus = this.Context.Invoke(() =>
			{
				this.Context.GL.LinkProgram(this.ID);
				this.Context.GL.GetProgram(this.ID, GetProgramParameterName.LinkStatus, out int result);
				return result;
			});

			// Check status.
			if (linkStatus == 0)
			{
				// Get program log.
				string log = this.Context.Invoke(() => this.Context.GL.GetProgramInfoLog(this.ID));

				Trace.WriteError(log);
				throw new ProgramLinkingException($"Linking of program {this} failed.", log);
			}

			this.Context.Invoke(this.populateUniforms);
		}

		public override string ToString()
		{
			return $"{this.ID}";
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			if (managed)
			{
				this.attachedShaders = null;
				this.uniforms = null;
				this.blocks = null;
			}

			void delete()
			{
				this.Context.GL.DeleteProgram(this.ID);
				GLTrace.Instance.ObjectDeleted(nameof(Program), this.ID);
			}

			if (managed)
				this.Context.Invoke(delete);
			else
				this.Context.Post(delete);
		}

		#endregion
	}
}
