﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a uniform.
	/// </summary>
	public sealed class Uniform
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the uniform.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets the type of the uniform.
		/// </summary>
		public ActiveUniformType Type { get; }

		/// <summary>
		/// Gets the location of the uniform.
		/// </summary>
		public int Location { get; }

		/// <summary>
		/// Gets the size of the array.
		/// </summary>
		public int ArraySize { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Uniform.
		/// </summary>
		internal Uniform(string name, ActiveUniformType type, int location, int arraySize)
		{
			this.Name = name;
			this.Type = type;
			this.Location = location;
			this.ArraySize = arraySize;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>
		/// A string that represents the current object.
		/// </returns>
		public override string ToString()
		{
			return $"{UniformUtil.GetDisplayName(this.Type)} {this.Name}";
		}

		#endregion
	}
}
