﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents the state of an OpenGL program (its uniforms and bindings).
	/// </summary>
	public class ProgramState : ICloneable, IProgramState
	{
		#region Fields

		private readonly Dictionary<Uniform, object> uniformValues;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the OpenGL context.
		/// </summary>
		public GLContext Context { get; }

		/// <summary>
		/// Gets the program that this state belongs to.
		/// </summary>
		public Program Program { get; }
		
		/// <summary>
		/// Gets or sets the parent state.
		/// This allows a state to have undefined values for uniforms which are then taken from the parent state(s), recursively.
		/// </summary>
		public ProgramState Parent { get; set; }

		/// <summary>
		/// Gets or sets the uniform block buffer bindings.
		/// </summary>
		public ProgramStateBlockBindings BlockBindings { get; set; }

		#endregion

		#region Constructors

		private ProgramState(Program program, bool init)
		{
			if(program == null)
				throw new ArgumentNullException(nameof(program));

			this.Program = program;
			this.Context = program.Context;
			this.BlockBindings = new ProgramStateBlockBindings();
			
			this.uniformValues = new Dictionary<Uniform, object>();

			if (init)
			{
				foreach (Uniform uniform in program.Uniforms)
				{
					Type type = UniformUtil.MapToType(uniform.Type);

					if (type == null) // for not implemented types.
						continue;

					Array array = Array.CreateInstance(type, uniform.ArraySize);

					this.Set(uniform.Name, array);
				}
			}
		}

		/// <summary>
		/// Creates a new ProgramState.
		/// </summary>
		public ProgramState(Program program)
			: this(program, true)
		{
		}

		/// <summary>
		/// Creates a copy of the specified ProgramState.
		/// </summary>
		/// <param name="other">The state to copy.</param>
		public ProgramState(ProgramState other)
			: this(other.Program, false)
		{
			other.CopyTo(this);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates a new "layer" on top of the current state.
		/// The new state will not do anything by default, but it can override values without changes this state.
		/// </summary>
		/// <returns></returns>
		public ProgramState CreateDerivedState()
		{
			ProgramState state = new ProgramState(this.Program, false);
			state.Parent = this;
			return state;
		}

		/// <summary>
		/// Binds buffer/offset/size to a uniform block binding point.
		/// </summary>
		/// <param name="bindingPoint">A binding point.</param>
		/// <param name="binding">The binding value.</param>
		public void Bind(int bindingPoint, UniformBlockBinding binding)
		{
			this.BlockBindings[bindingPoint] = binding;
		}

		/// <summary>
		/// Binds buffer/offset/size to a uniform block binding point.
		/// </summary>
		/// <param name="block">The block whose current binding point is used.</param>
		/// <param name="binding">The binding value.</param>
		public void Bind(UniformBlock block, UniformBlockBinding binding)
		{
			if (block == null)
				throw new ArgumentNullException(nameof(block));

			this.Bind(block.Binding, binding);
		}
		
		/// <summary>
		/// Binds buffer/offset/size to a uniform block binding point.
		/// </summary>
		/// <param name="blockName">The name of the block whose current binding point is used.</param>
		/// <param name="binding">The binding value.</param>
		/// <returns>False if a block with the specified name could not be found. True otherwise.</returns>
		public bool Bind(string blockName, UniformBlockBinding binding)
		{
			UniformBlock block = this.Program.Blocks.FirstOrDefault(b => b.Name == blockName);
			if (block == null)
				return false;

			this.Bind(block.Binding, binding);
			return true;
		}

		/// <summary>
		/// Sets a uniform value.
		/// </summary>
		/// <param name="name">The name of the uniform to set.</param>
		/// <param name="value">The value.</param>
		/// <returns>True, if the value was set. False if the uniform does not exist.</returns>
		public bool Set(string name, object value)
		{
			if (name == null)
				throw new ArgumentNullException(nameof(name));

			Uniform uniform = this.Program.Uniforms.FirstOrDefault(u => u.Name == name);
			if (uniform == null)
				return false;

			return this.Set(uniform, value);
		}

		/// <summary>
		/// Sets a uniform value.
		/// </summary>
		/// <param name="uniform">The uniform to set.</param>
		/// <param name="value">The value.</param>
		/// <returns>True, if the value was set. False if the uniform does not exist.</returns>
		public bool Set(Uniform uniform, object value) 
		{
			if (uniform == null)
				throw new ArgumentNullException(nameof(uniform));

			if (value == null)
			{
				this.uniformValues[uniform] = null;
				return true;
			}

			Type uniformType = UniformUtil.MapToType(uniform.Type);
			if (uniformType == null)
				throw new NotImplementedException($"Uniforms of type \"{uniform.Type}\" are not implemented yet.");

			Type valueType = value.GetType();

			Array array = value as Array;
			if (array == null)
			{
				if(uniform.ArraySize > 1)
					throw new ArgumentException($"Excepted array with at least {uniform.ArraySize} for uniform \"{uniform}\".");

				if (valueType != uniformType && (uniformType != typeof(Vector4) || valueType != typeof(Color4)))
					throw new ArgumentException($"Invalid value type. Uniform \"{uniform}\" is of type \"{uniformType}\".");
			}
			else
			{
				if(array.Length < uniform.ArraySize)
					throw new ArgumentException($"Excepted array with at least {uniform.ArraySize} for uniform \"{uniform}\".");

				uniformType = uniformType.MakeArrayType();
				if (valueType != uniformType && (uniformType != typeof(Vector4[]) || valueType != typeof(Color4[])))
					throw new ArgumentException($"Invalid value type. Uniform \"{uniform}\" is of type \"{uniformType}\".");
			}

			this.uniformValues[uniform] = value;

			return true;
		}

		private object getValue(Uniform uniform)
		{
			object value;

			if (this.uniformValues.TryGetValue(uniform, out value))
				return value;
			
			if (this.Parent == null)
				throw new Exception($"No value set for uniform \"{uniform}\".");

			return this.Parent.getValue(uniform);
		}

		private UniformBlockBinding getBinding(int bindingPoint)
		{
			UniformBlockBinding binding = this.BlockBindings?[bindingPoint];
			if (binding == null && this.Parent != null)
				binding = this.Parent.getBinding(bindingPoint);

			return binding;
		}

		private void setUniformValue(Uniform uniform, object value)
		{
			Array array = value as Array;
			if (array != null)
			{
				switch (uniform.Type)
				{
					case ActiveUniformType.Float:
						this.Context.GL.Uniform1(uniform.Location, array.Length, (float[])array);
						break;
					case ActiveUniformType.FloatVec2:
						this.Context.GL.Uniform2(uniform.Location, array.Length, ref ((Vector2[])array)[0].X);
						break;
					case ActiveUniformType.FloatVec3:
						this.Context.GL.Uniform3(uniform.Location, array.Length, ref ((Vector3[])array)[0].X);
						break;
					case ActiveUniformType.FloatVec4:
						Color4[] colors = array as Color4[];
						IntPtr p;
						if (colors != null)
						{
							unsafe
							{
								fixed (float* ptr = &colors[0].R)
								{
									p = (IntPtr)ptr;
								}
							}

							this.Context.GL.Uniform4(uniform.Location, array.Length, ref colors[0].R);
						}
						else
						{
							this.Context.GL.Uniform4(uniform.Location, array.Length, ref ((Vector4[])array)[0].X);
						}

						break;

					case ActiveUniformType.Double:
						this.Context.GL.Uniform1(uniform.Location, array.Length, (double[])array);
						break;
					case ActiveUniformType.DoubleVec2:
						throw new NotImplementedException();
					case ActiveUniformType.DoubleVec3:
						throw new NotImplementedException();
					case ActiveUniformType.DoubleVec4:
						throw new NotImplementedException();

					case ActiveUniformType.Int:
						this.Context.GL.Uniform1(uniform.Location, array.Length, (int[])array);
						break;
					case ActiveUniformType.IntVec2:
						this.Context.GL.Uniform2(uniform.Location, array.Length, ref ((Vector2[])array)[0].X);
						break;
					case ActiveUniformType.IntVec3:
						this.Context.GL.Uniform3(uniform.Location, array.Length, ref ((Vector3[])array)[0].X);
						break;
					case ActiveUniformType.IntVec4:
						this.Context.GL.Uniform4(uniform.Location, array.Length, ref ((Vector4[])array)[0].X);
						break;

					case ActiveUniformType.UnsignedInt:
						this.Context.GL.Uniform1(uniform.Location, array.Length, (uint[])array);
						break;
					case ActiveUniformType.UnsignedIntVec2:
						throw new NotImplementedException();
					case ActiveUniformType.UnsignedIntVec3:
						throw new NotImplementedException();
					case ActiveUniformType.UnsignedIntVec4:
						throw new NotImplementedException();

					case ActiveUniformType.Bool:
						this.Context.GL.Uniform1(uniform.Location, array.Length, (int[])array);
						break;
					case ActiveUniformType.BoolVec2:
						throw new NotImplementedException();
					case ActiveUniformType.BoolVec3:
						throw new NotImplementedException();
					case ActiveUniformType.BoolVec4:
						throw new NotImplementedException();

					case ActiveUniformType.FloatMat2:
						this.Context.GL.UniformMatrix2(uniform.Location, array.Length, true, ref ((Matrix2x2[])array)[0].M11);
						break;
					case ActiveUniformType.FloatMat3:
						this.Context.GL.UniformMatrix3(uniform.Location, array.Length, true, ref ((Matrix3x3[])array)[0].M11);
						break;
					case ActiveUniformType.FloatMat4:
						this.Context.GL.UniformMatrix4(uniform.Location, array.Length, true, ref ((Matrix4x4[])array)[0].M11);
						break;
					case ActiveUniformType.FloatMat2x3:
						this.Context.GL.UniformMatrix2x3(uniform.Location, array.Length, true, ref ((Matrix2x3[])array)[0].M11);
						break;
					case ActiveUniformType.FloatMat2x4:
						this.Context.GL.UniformMatrix2x4(uniform.Location, array.Length, true, ref ((Matrix2x4[])array)[0].M11);
						break;
					case ActiveUniformType.FloatMat3x2:
						this.Context.GL.UniformMatrix3x2(uniform.Location, array.Length, true, ref ((Matrix3x2[])array)[0].M11);
						break;
					case ActiveUniformType.FloatMat3x4:
						this.Context.GL.UniformMatrix3x4(uniform.Location, array.Length, true, ref ((Matrix3x4[])array)[0].M11);
						break;
					case ActiveUniformType.FloatMat4x2:
						this.Context.GL.UniformMatrix4x2(uniform.Location, array.Length, true, ref ((Matrix4x2[])array)[0].M11);
						break;
					case ActiveUniformType.FloatMat4x3:
						this.Context.GL.UniformMatrix4x3(uniform.Location, array.Length, true, ref ((Matrix4x3[])array)[0].M11);
						break;

					case (ActiveUniformType)All.DoubleMat2:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat3:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat4:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat2x3:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat2x4:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat3x2:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat3x4:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat4x2:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat4x3:
						throw new NotImplementedException();

					case ActiveUniformType.Sampler1D:
					case ActiveUniformType.Sampler2D:
					case ActiveUniformType.Sampler3D:
					case ActiveUniformType.SamplerCube:
					case ActiveUniformType.Sampler1DShadow:
					case ActiveUniformType.Sampler2DShadow:
					case ActiveUniformType.Sampler1DArray:
					case ActiveUniformType.Sampler2DArray:
					case ActiveUniformType.Sampler1DArrayShadow:
					case ActiveUniformType.Sampler2DArrayShadow:
					case ActiveUniformType.Sampler2DMultisample:
					case ActiveUniformType.Sampler2DMultisampleArray:
					case ActiveUniformType.SamplerCubeShadow:
					case ActiveUniformType.SamplerBuffer:
					case ActiveUniformType.Sampler2DRect:
					case ActiveUniformType.Sampler2DRectShadow:
					case ActiveUniformType.IntSampler1D:
					case ActiveUniformType.IntSampler2D:
					case ActiveUniformType.IntSampler3D:
					case ActiveUniformType.IntSamplerCube:
					case ActiveUniformType.IntSampler1DArray:
					case ActiveUniformType.IntSampler2DArray:
					case ActiveUniformType.IntSampler2DMultisample:
					case ActiveUniformType.IntSampler2DMultisampleArray:
					case ActiveUniformType.IntSamplerBuffer:
					case ActiveUniformType.IntSampler2DRect:
					case ActiveUniformType.UnsignedIntSampler1D:
					case ActiveUniformType.UnsignedIntSampler2D:
					case ActiveUniformType.UnsignedIntSampler3D:
					case ActiveUniformType.UnsignedIntSamplerCube:
					case ActiveUniformType.UnsignedIntSampler1DArray:
					case ActiveUniformType.UnsignedIntSampler2DArray:
					case ActiveUniformType.UnsignedIntSampler2DMultisample:
					case ActiveUniformType.UnsignedIntSampler2DMultisampleArray:
					case ActiveUniformType.UnsignedIntSamplerBuffer:
					case ActiveUniformType.UnsignedIntSampler2DRect:
						this.Context.GL.Uniform1(uniform.Location, array.Length, (int[])array);
						break;
				}
			}
			else
			{
				switch (uniform.Type)
				{
					case ActiveUniformType.Float:
						this.Context.GL.Uniform1(uniform.Location, (float)value);
						break;
					case ActiveUniformType.FloatVec2:
						Vector2 vec2 = (Vector2)value;
						this.Context.GL.Uniform2(uniform.Location, vec2.X, vec2.Y);
						break;
					case ActiveUniformType.FloatVec3:
						Vector3 vec3 = (Vector3)value;
						this.Context.GL.Uniform3(uniform.Location, vec3.X, vec3.Y, vec3.Z);
						break;
					case ActiveUniformType.FloatVec4:
						if (value is Color4)
						{
							Color4 color = (Color4)value;
							this.Context.GL.Uniform4(uniform.Location, color.R, color.G, color.B, color.A);
						}
						else
						{
							Vector4 vec4 = (Vector4)value;
							this.Context.GL.Uniform4(uniform.Location, vec4.X, vec4.Y, vec4.Z, vec4.W);
						}

						break;

					case ActiveUniformType.Double:
						this.Context.GL.Uniform1(uniform.Location, (double)value);
						break;
					case ActiveUniformType.DoubleVec2:
						throw new NotImplementedException();
					case ActiveUniformType.DoubleVec3:
						throw new NotImplementedException();
					case ActiveUniformType.DoubleVec4:
						throw new NotImplementedException();

					case ActiveUniformType.Int:
						this.Context.GL.Uniform1(uniform.Location, (int)value);
						break;
					case ActiveUniformType.IntVec2:
						Vector2I ivec2 = (Vector2I)value;
						this.Context.GL.Uniform2(uniform.Location, ivec2.X, ivec2.Y);
						break;
					case ActiveUniformType.IntVec3:
						Vector3I ivec3 = (Vector3I)value;
						this.Context.GL.Uniform3(uniform.Location, ivec3.X, ivec3.Y, ivec3.Z);
						break;
					case ActiveUniformType.IntVec4:
						Vector4I ivec4 = (Vector4I)value;
						this.Context.GL.Uniform4(uniform.Location, ivec4.X, ivec4.Y, ivec4.Z, ivec4.W);
						break;

					case ActiveUniformType.UnsignedInt:
						this.Context.GL.Uniform1(uniform.Location, (uint)value);
						break;
					case ActiveUniformType.UnsignedIntVec2:
						throw new NotImplementedException();
					case ActiveUniformType.UnsignedIntVec3:
						throw new NotImplementedException();
					case ActiveUniformType.UnsignedIntVec4:
						throw new NotImplementedException();

					case ActiveUniformType.Bool:
						this.Context.GL.Uniform1(uniform.Location, (bool)value ? 1 : 0);
						break;
					case ActiveUniformType.BoolVec2:
						throw new NotImplementedException();
					case ActiveUniformType.BoolVec3:
						throw new NotImplementedException();
					case ActiveUniformType.BoolVec4:
						throw new NotImplementedException();

					case ActiveUniformType.FloatMat2:
						Matrix2x2 mat2 = (Matrix2x2)value;
						this.Context.GL.UniformMatrix2(uniform.Location, 1, true, ref mat2.M11);
						break;
					case ActiveUniformType.FloatMat3:
						Matrix3x3 mat3 = (Matrix3x3)value;
						this.Context.GL.UniformMatrix3(uniform.Location, 1, true, ref mat3.M11);
						break;
					case ActiveUniformType.FloatMat4:
						Matrix4x4 mat4 = (Matrix4x4)value;
						this.Context.GL.UniformMatrix4(uniform.Location, 1, true, ref mat4.M11);
						break;
					case ActiveUniformType.FloatMat2x3:
						Matrix2x3 mat2x3 = (Matrix2x3)value;
						this.Context.GL.UniformMatrix2x3(uniform.Location, 1, true, ref mat2x3.M11);
						break;
					case ActiveUniformType.FloatMat2x4:
						Matrix2x4 mat2x4 = (Matrix2x4)value;
						this.Context.GL.UniformMatrix2x4(uniform.Location, 1, true, ref mat2x4.M11);
						break;
					case ActiveUniformType.FloatMat3x2:
						Matrix3x2 mat3x2 = (Matrix3x2)value;
						this.Context.GL.UniformMatrix3x2(uniform.Location, 1, true, ref mat3x2.M11);
						break;
					case ActiveUniformType.FloatMat3x4:
						Matrix3x4 mat3x4 = (Matrix3x4)value;
						this.Context.GL.UniformMatrix3x4(uniform.Location, 1, true, ref mat3x4.M11);
						break;
					case ActiveUniformType.FloatMat4x2:
						Matrix4x2 mat4x2 = (Matrix4x2)value;
						this.Context.GL.UniformMatrix4x2(uniform.Location, 1, true, ref mat4x2.M11);
						break;
					case ActiveUniformType.FloatMat4x3:
						Matrix4x3 mat4x3 = (Matrix4x3)value;
						this.Context.GL.UniformMatrix4x3(uniform.Location, 1, true, ref mat4x3.M11);
						break;

					case (ActiveUniformType)All.DoubleMat2:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat3:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat4:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat2x3:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat2x4:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat3x2:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat3x4:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat4x2:
						throw new NotImplementedException();
					case (ActiveUniformType)All.DoubleMat4x3:
						throw new NotImplementedException();

					case ActiveUniformType.Sampler1D:
					case ActiveUniformType.Sampler2D:
					case ActiveUniformType.Sampler3D:
					case ActiveUniformType.SamplerCube:
					case ActiveUniformType.Sampler1DShadow:
					case ActiveUniformType.Sampler2DShadow:
					case ActiveUniformType.Sampler1DArray:
					case ActiveUniformType.Sampler2DArray:
					case ActiveUniformType.Sampler1DArrayShadow:
					case ActiveUniformType.Sampler2DArrayShadow:
					case ActiveUniformType.Sampler2DMultisample:
					case ActiveUniformType.Sampler2DMultisampleArray:
					case ActiveUniformType.SamplerCubeShadow:
					case ActiveUniformType.SamplerBuffer:
					case ActiveUniformType.Sampler2DRect:
					case ActiveUniformType.Sampler2DRectShadow:
					case ActiveUniformType.IntSampler1D:
					case ActiveUniformType.IntSampler2D:
					case ActiveUniformType.IntSampler3D:
					case ActiveUniformType.IntSamplerCube:
					case ActiveUniformType.IntSampler1DArray:
					case ActiveUniformType.IntSampler2DArray:
					case ActiveUniformType.IntSampler2DMultisample:
					case ActiveUniformType.IntSampler2DMultisampleArray:
					case ActiveUniformType.IntSamplerBuffer:
					case ActiveUniformType.IntSampler2DRect:
					case ActiveUniformType.UnsignedIntSampler1D:
					case ActiveUniformType.UnsignedIntSampler2D:
					case ActiveUniformType.UnsignedIntSampler3D:
					case ActiveUniformType.UnsignedIntSamplerCube:
					case ActiveUniformType.UnsignedIntSampler1DArray:
					case ActiveUniformType.UnsignedIntSampler2DArray:
					case ActiveUniformType.UnsignedIntSampler2DMultisample:
					case ActiveUniformType.UnsignedIntSampler2DMultisampleArray:
					case ActiveUniformType.UnsignedIntSamplerBuffer:
					case ActiveUniformType.UnsignedIntSampler2DRect:
						this.Context.GL.Uniform1(uniform.Location, (int)value);
						break;
				}
			}
		}

		/// <summary>
		/// Applies the state to the program.
		/// </summary>
		void IProgramState.Apply()
		{
			// Apply uniform values.
			foreach (Uniform uniform in this.Program.Uniforms)
			{
				this.setUniformValue(uniform, this.getValue(uniform));
			}

			// Bind buffers to blocks.
			foreach (int bindingPoint in this.Program.Blocks.Select(block => block.Binding).Distinct())
			{
				UniformBlockBinding binding = this.getBinding(bindingPoint);
				if (binding == null)
				{
					this.Context.GL.BindBufferRange(BufferRangeTarget.UniformBuffer, (uint)bindingPoint, 0, IntPtr.Zero, IntPtr.Zero);
				}
				else
				{
					this.Context.GL.BindBufferRange(BufferRangeTarget.UniformBuffer, (uint)bindingPoint, binding.Buffer.ID, binding.Offset, binding.Size);
				}
			}
		}

		/// <summary>
		/// Copies this program state to the specified state.
		/// </summary>
		/// <param name="dest"></param>
		public void CopyTo(ProgramState dest)
		{
			if (dest.Program != this.Program)
				throw new ArgumentException("The specified program state belongs to a different program.");

			dest.Parent = this.Parent;

			dest.uniformValues.Clear();
			foreach (KeyValuePair<Uniform, object> pair in this.uniformValues)
			{
				object valueClone = (pair.Value as ICloneable)?.Clone() ?? pair.Value;
				dest.uniformValues.Add(pair.Key, valueClone);
			}
		}

		/// <summary>
		/// Creates a copy of this program state.
		/// </summary>
		/// <returns></returns>
		public ProgramState Clone()
		{
			return new ProgramState(this);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}
		
		#endregion
	}
}
