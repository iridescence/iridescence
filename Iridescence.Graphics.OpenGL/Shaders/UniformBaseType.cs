﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Defines the type of API call a uniform uses (f, i, ui, d).
	/// </summary>
	public enum UniformBaseType
	{
		None,
		Float,
		Integer,
		UnsignedInteger,
		Double
	}
}
