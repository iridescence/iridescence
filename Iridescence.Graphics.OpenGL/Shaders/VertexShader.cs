﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a vertex shader.
	/// </summary>
	public sealed class VertexShader : Shader
	{
		#region Constructors

		/// <summary>
		/// Creates a new VertexShader.
		/// </summary>
		public VertexShader(GLContext context, params string[] sources)
			: base(context, ShaderType.VertexShader, sources)
		{

		}

		#endregion
	}
}
