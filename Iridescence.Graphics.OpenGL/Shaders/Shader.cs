﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL shader object.
	/// </summary>
	public class Shader : GLContextChild, IGLObject
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the OpenGL object ID.
		/// </summary>
		public uint ID { get; }

		/// <summary>
		/// Gets the shader type.
		/// </summary>
		public ShaderType Type { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new shader from source code.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="type"></param>
		/// <param name="sources"></param>
		internal Shader(GLContext context, ShaderType type, params string[] sources)
			: base(context)
		{
			this.Type = type;

			// create shader.
			this.ID = (uint)this.Context.Invoke(() => this.Context.GL.CreateShader(type));
			GLTrace.Instance.ObjectCreated(nameof(Shader), this.ID);

			// get lengths.
			int[] lengths = new int[sources.Length];
			for (int i = 0; i < sources.Length; i++)
				lengths[i] = sources[i].Length;

			// compile.
			int compileStatus = this.Context.Invoke(() =>
			{
				this.Context.GL.ShaderSource(this.ID, sources.Length, sources, lengths);
				this.Context.GL.CompileShader(this.ID);

				this.Context.GL.GetShader(this.ID, ShaderParameter.CompileStatus, out int result);
				return result;
			});
			
			// check status.
			if (compileStatus == 0)
			{
				// get shader log.
				string log = this.Context.Invoke(() => this.Context.GL.GetShaderInfoLog(this.ID));

				Trace.WriteError(log);
				throw new ShaderCompilationException($"Compilation of shader {this} failed.", log);
			}
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			return $"{this.ID} ({this.Type})";
		}

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);

			void delete()
			{
				this.Context.GL.DeleteShader(this.ID);
				GLTrace.Instance.ObjectDeleted(nameof(Shader), this.ID);
			}

			if (managed)
				this.Context.Invoke(delete);
			else
				this.Context.Post(delete);
		}

		#endregion
	}
}
