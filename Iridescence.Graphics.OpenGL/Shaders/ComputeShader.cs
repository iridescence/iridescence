﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a compute shader.
	/// </summary>
	public sealed class ComputeShader : Shader
	{
		#region Constructors

		/// <summary>
		/// Creates a new ComputeShader.
		/// </summary>
		public ComputeShader(GLContext context, params string[] sources)
			: base(context, ShaderType.ComputeShader, sources)
		{

		}

		#endregion
	}
}
