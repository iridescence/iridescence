﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using Iridescence.Composition;
using Iridescence.FileSystem.Composition;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Resource handler for programs.
	/// </summary>
	public class ProgramExportProvider : IExportDescriptorProvider
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the context used to create programs.
		/// </summary>
		public GLContext Context { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Program resource that loads programs for the specified context.
		/// </summary>
		/// <param name="context">The context that programs are created on.</param>
		public ProgramExportProvider(GLContext context)
		{
			this.Context = context ?? throw new ArgumentNullException(nameof(context));
		}

		#endregion

		#region Methods

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			if (!contract.IsAssignableTo(typeof(Program)))
				yield break;

			yield return new ExportDescriptorPromise(
				contract,
				typeof(ProgramExportProvider).Name,
				false,
				() => new[] {dependencyAccessor.ResolveRequiredDependency("definition", contract.ChangeType(typeof(JsonObject)), DependencyFlags.Prerequisite)},
				deps =>
				{
					ExportDescriptor definitionDescriptor = deps.GetDependency("definition").GetDescriptor();
					return ExportDescriptor.Create((context, operation) =>
					{
						JsonObject json = (JsonObject)CompositionOperation.Run(context, definitionDescriptor.Activator);

						Program program = new Program(this.Context);
						if (!contract.TryExtractName(out string name, out CompositionContract namelessContract))
							name = null;

						CompositionContext localContext = context.RelativeToFile(name);

						string shaderName;
						if (json.TryGetValue("VertexShader", out shaderName))
							program.Attach((VertexShader)localContext.GetExport(new CompositionContract(typeof(VertexShader), namelessContract.Metadata.Add(new NameMetadata(shaderName)))));

						if (json.TryGetValue("TessControlShader", out shaderName))
							program.Attach((TessControlShader)localContext.GetExport(new CompositionContract(typeof(TessControlShader), namelessContract.Metadata.Add(new NameMetadata(shaderName)))));

						if (json.TryGetValue("TessEvaluationShader", out shaderName))
							program.Attach((TessEvaluationShader)localContext.GetExport(new CompositionContract(typeof(TessEvaluationShader), namelessContract.Metadata.Add(new NameMetadata(shaderName)))));

						if (json.TryGetValue("GeometryShader", out shaderName))
							program.Attach((GeometryShader)localContext.GetExport(new CompositionContract(typeof(GeometryShader), namelessContract.Metadata.Add(new NameMetadata(shaderName)))));

						if (json.TryGetValue("FragmentShader", out shaderName))
							program.Attach((FragmentShader)localContext.GetExport(new CompositionContract(typeof(FragmentShader), namelessContract.Metadata.Add(new NameMetadata(shaderName)))));

						if (json.TryGetValue("ComputeShader", out shaderName))
							program.Attach((ComputeShader)localContext.GetExport(new CompositionContract(typeof(ComputeShader), namelessContract.Metadata.Add(new NameMetadata(shaderName)))));

						program.Link();

						if (json.TryGetValue("UniformBlockBindings", out JsonObject blockBindings))
						{
							foreach (KeyValuePair<string, JsonValue> binding in blockBindings)
							{
								if (!(binding.Value is JsonPrimitive value))
									throw new Exception("Invalid uniform block binding.");

								UniformBlock block = program.Blocks.FirstOrDefault(b => b.Name == binding.Key);
								if (block == null)
									continue;

								block.Binding = value;
							}
						}

						program.TrySetLabel(name);

						return program;

					});
				});
		}

		#endregion
	}
}

