﻿using System.Collections.Generic;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a collection of uniform block bindings.
	/// </summary>
	public class ProgramStateBlockBindings
	{
		#region Fields

		private readonly Dictionary<int, UniformBlockBinding> bindings;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the binding for the specified binding point.
		/// </summary>
		/// <param name="bindingPoint"></param>
		/// <returns></returns>
		public UniformBlockBinding this[int bindingPoint]
		{
			get
			{
				UniformBlockBinding binding;
				if (this.bindings.TryGetValue(bindingPoint, out binding))
					return binding;
				return null;
			}
			set => this.bindings[bindingPoint] = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ProgramStateBlockBindings.
		/// </summary>
		public ProgramStateBlockBindings()
		{
			this.bindings = new Dictionary<int, UniformBlockBinding>();
		}

		#endregion

		#region Methods

		#endregion
	}
}
