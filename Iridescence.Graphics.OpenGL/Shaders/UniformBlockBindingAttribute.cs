﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Specifies the block binding index of a uniform block.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class UniformBlockBindingAttribute : Attribute
	{
		#region Properties

		/// <summary>
		/// Gets the property name.
		/// </summary>
		public int Index { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="UniformBlockBindingAttribute"/> class. 
		/// </summary>
		public UniformBlockBindingAttribute(int index)
		{
			this.Index = index;
		}

		#endregion
	}
}
