﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Exception thrown when compilation of a shader fails.
	/// </summary>
	[Serializable]
	public class ShaderCompilationException : Exception
	{
		#region Properties

		/// <summary>
		/// Gets the shader info log.
		/// </summary>
		public string InfoLog { get; }

		#endregion

		#region Constructors

		protected ShaderCompilationException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			this.InfoLog = info.GetString("InfoLog");
		}

		/// <summary>
		/// Creates a new ShaderCompilerException.
		/// </summary>
		public ShaderCompilationException(string message, string infoLog)
			: base(message)
		{
			this.InfoLog = infoLog;
		}

		#endregion

		#region Methods

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("InfoLog", this.InfoLog);
		}

		public override string ToString()
		{
			return $"{this.Message}\n{this.InfoLog}";
		}

		#endregion
	}
}
