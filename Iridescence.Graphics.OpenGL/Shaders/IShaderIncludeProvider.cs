﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Resolves shader include directives.
	/// </summary>
	public interface IShaderIncludeProvider
	{
		/// <summary>
		/// Gets the code for the specified include name.
		/// </summary>
		/// <param name="include"></param>
		/// <returns></returns>
		string GetInclude(string include);
	}
}
