﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a uniform block buffer binding that can be bound to a binding point.
	/// </summary>
	public class UniformBlockBinding : IEquatable<UniformBlockBinding>
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the buffer to bind.
		/// </summary>
		public Buffer Buffer { get; set; }

		/// <summary>
		/// Gets or sets the buffer offset.
		/// </summary>
		public IntPtr Offset { get; set; }

		/// <summary>
		/// Gets or sets the number of bytes to use.
		/// </summary>
		public IntPtr Size { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new UniformBlockBinding.
		/// </summary>
		public UniformBlockBinding(Buffer buffer, IntPtr offset, IntPtr size)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset.ToInt64() + size.ToInt64() > buffer.Size)
				throw new ArgumentOutOfRangeException(nameof(offset));

			this.Buffer = buffer;
			this.Offset = offset;
			this.Size = size;
		}

		/// <summary>
		/// Creates a new UniformBlockBinding.
		/// </summary>
		public UniformBlockBinding(Buffer buffer, int offset, int size)
			: this(buffer, (IntPtr)offset, (IntPtr)size)
		{

		}

		/// <summary>
		/// Creates a new UniformBlockBinding.
		/// </summary>
		public UniformBlockBinding(Buffer buffer)
			: this(buffer ?? throw new ArgumentNullException(nameof(buffer)), 0, buffer.Size)
		{

		}

		#endregion

		#region Methods

		public bool Equals(UniformBlockBinding other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(this.Buffer, other.Buffer) &&
			       this.Offset.Equals(other.Offset) &&
			       this.Size.Equals(other.Size);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((UniformBlockBinding)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = (this.Buffer != null ? this.Buffer.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ this.Offset.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Size.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(UniformBlockBinding left, UniformBlockBinding right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(UniformBlockBinding left, UniformBlockBinding right)
		{
			return !Equals(left, right);
		}

		public static implicit operator UniformBlockBinding(Buffer buffer)
		{
			return buffer == null ? null : new UniformBlockBinding(buffer);
		}
		
		#endregion
	}
}
