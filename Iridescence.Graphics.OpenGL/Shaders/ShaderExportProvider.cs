﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Iridescence.Composition;
using Iridescence.FileSystem.Composition;
using Iridescence.Graphics.OpenGL.Shaders;
using Path = Iridescence.FileSystem.Path;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Resource handler for GLSL shaders.
	/// </summary>
	public sealed class ShaderExportProvider : IExportDescriptorProvider
	{
		#region Fields

		private static readonly Regex versionRegex = new Regex("^\\s*#version\\s+");
		private static readonly Regex includeRegex = new Regex("^\\s*#include\\s+(?:\"(.+)\"|<(.+)>)");
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the context used to create shaders.
		/// </summary>
		public GLContext Context { get; }

		#endregion
		
		#region Constructors

		/// <summary>
		/// Creates a new GLSL resource handler for the specified context.
		/// </summary>
		/// <param name="context">The context that the shaders are created on.</param>
		public ShaderExportProvider(GLContext context)
		{
			this.Context = context ?? throw new ArgumentNullException(nameof(context));
		}

		#endregion

		#region Methods

		private static bool isShader(Type type)
		{
			return type == typeof(VertexShader) ||
			       type == typeof(TessControlShader) ||
			       type == typeof(TessEvaluationShader) ||
			       type == typeof(GeometryShader) ||
			       type == typeof(FragmentShader) ||
			       type == typeof(ComputeShader);
		}
		
		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			if (!isShader(contract.Type))
				yield break;

			IReadOnlyList<ShaderHeaderMetadata> headerMetadata =  contract.ExtractMetadata<ShaderHeaderMetadata>(out CompositionContract contract2);

			string header = null;
			if (headerMetadata.Count > 0)
			{
				StringBuilder str = new StringBuilder();

				foreach (ShaderHeaderMetadata m in headerMetadata)
				{
					str.AppendLine(m.Text);
				}

				header = str.ToString();
			}

			yield return new ExportDescriptorPromise(
				contract,
				typeof(ShaderExportProvider).Name,
				false,
				() => new[] {dependencyAccessor.ResolveRequiredDependency("code", contract2.ChangeType(typeof(TextReader)), DependencyFlags.Prerequisite)},
				deps =>
				{
					ExportDescriptor codeDescriptor = deps.GetDependency("code").GetDescriptor();

					return ExportDescriptor.Create((context, operation) =>
					{
						StringBuilder output = new StringBuilder();
						int sourceNumber = 0;

						using (TextReader codeReader = (TextReader)CompositionOperation.Run(context, codeDescriptor.Activator))
						{
							this.preprocess(context.RelativeToFile(contract), ref sourceNumber, codeReader, header, output);
						}

						string source = output.ToString();

						Shader shader;
						if (contract.Type == typeof(VertexShader))
							shader = new VertexShader(this.Context, source);
						else if (contract.Type == typeof(TessControlShader))
							shader = new TessControlShader(this.Context, source);
						else if (contract.Type == typeof(TessEvaluationShader))
							shader = new TessEvaluationShader(this.Context, source);
						else if (contract.Type == typeof(GeometryShader))
							shader = new GeometryShader(this.Context, source);
						else if (contract.Type == typeof(FragmentShader))
							shader = new FragmentShader(this.Context, source);
						else if (contract.Type == typeof(ComputeShader))
							shader = new ComputeShader(this.Context, source);
						else
							throw new InvalidOperationException();

						if (contract.TryGetName(out string name))
							shader.TrySetLabel(name);

						return shader;
					});
				});
		}

		private void preprocess(CompositionContext context, ref int sourceNumber, TextReader source, string header, StringBuilder output)
		{
			bool lineInserted = false;

			int myNumber = sourceNumber++;
			int lineCounter = 0;

			string line;
			while ((line = source.ReadLine()) != null)
			{
				++lineCounter;

				Match match;
				if (versionRegex.IsMatch(line))
				{
					// Append #version first.
					output.Append(line);
					output.Append('\n');

					if (header != null)
					{
						// Append supplied header.
						this.preprocess(context, ref sourceNumber, new StringReader(header), null, output);
						lineInserted = false;
					}

					header = null;
				}
				else if ((match = includeRegex.Match(line)).Success)
				{
					string includeName = match.Groups[1].Success ? match.Groups[1].Value : match.Groups[2].Value;
					using (TextReader includeSource = context.GetExport<TextReader>(includeName))
					{
						CompositionContext includeContext = context.ChangeDirectory(Path.Parse(includeName).Up());
						this.preprocess(includeContext, ref sourceNumber, includeSource, header, output);
					}
					
					lineInserted = false;
				}
				else
				{
					if (!lineInserted)
					{
						output.Append($"#line {lineCounter} {myNumber}\n");
						lineInserted = true;
					}

					output.Append(line);
					output.Append('\n');
				}
			}
		}

		#endregion
	}
}
