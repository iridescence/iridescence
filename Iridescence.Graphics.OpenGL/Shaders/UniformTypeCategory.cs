﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Defines categories for uniform types.
	/// </summary>
	public enum UniformTypeCategory
	{
		/// <summary>
		/// Primitive scalar values (int, float, ...).
		/// </summary>
		Scalar,

		/// <summary>
		/// Primitive vectors (vec2, ivec2, ...)
		/// </summary>
		Vector,

		/// <summary>
		/// Primitive matrices (mat2, dmat4x2, ...)
		/// </summary>
		Matrix,

		/// <summary>
		/// Sampler bindings such as sampler2D.
		/// </summary>
		Sampler,

		/// <summary>
		/// Image bindings such as image2D.
		/// </summary>
		Image,

		/// <summary>
		/// Atomic variable.
		/// </summary>
		Atomic,
	}
}
