﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a tesselation control shader.
	/// </summary>
	public sealed class TessControlShader : Shader
	{
		#region Constructors

		/// <summary>
		/// Creates a new TessControlShader.
		/// </summary>
		public TessControlShader(GLContext context, params string[] sources)
			: base(context, ShaderType.TessControlShader, sources)
		{

		}

		#endregion
	}
}
