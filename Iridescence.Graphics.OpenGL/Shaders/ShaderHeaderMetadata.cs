﻿using System;
using Iridescence.Composition;

namespace Iridescence.Graphics.OpenGL.Shaders
{
	/// <summary>
	/// Shader composition metadata that allows the user to prepend text to the shader source code (e.g. defines).
	/// </summary>
	public class ShaderHeaderMetadata : CompositionMetadata, IEquatable<ShaderHeaderMetadata>
	{
		#region Fields
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the header text.
		/// </summary>
		public string Text { get; }
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ShaderHeaderMetadata"/> class. 
		/// </summary>
		public ShaderHeaderMetadata(string text)
		{
			this.Text = text ?? throw new ArgumentNullException(nameof(text));
		}
		
		#endregion
		
		#region Methods
		
		public bool Equals(ShaderHeaderMetadata other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return string.Equals(this.Text, other.Text);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((ShaderHeaderMetadata)obj);
		}

		public override int GetHashCode()
		{
			return this.Text.GetHashCode();
		}
		
		#endregion
	}
}
