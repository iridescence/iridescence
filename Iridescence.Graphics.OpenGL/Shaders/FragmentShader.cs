﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents a fragment shader.
	/// </summary>
	public sealed class FragmentShader : Shader
	{
		#region Constructors

		/// <summary>
		/// Creates a new FragmentShader.
		/// </summary>
		public FragmentShader(GLContext context, params string[] sources)
			: base(context, ShaderType.FragmentShader, sources)
		{

		}

		#endregion
	}
}
