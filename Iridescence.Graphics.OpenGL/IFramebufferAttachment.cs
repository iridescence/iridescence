﻿namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Interface for objects that can be attached to framebuffers.
	/// </summary>
	public interface IFramebufferAttachment
	{
		void AttachToFramebuffer(FramebufferTarget target, FramebufferAttachment attachment);
	}

	public interface IFramebufferLayerAttachment
	{
		void AttachToFramebuffer(FramebufferTarget target, FramebufferAttachment attachment, int layer);
	}
}
