﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// OpenGL debug functionality.
	/// </summary>
	public static class DebugUtility
	{
		#region Methods

		private static string getDebugSourceText(DebugSource source)
		{
			switch (source)
			{
				case DebugSource.DebugSourceApi: return "API";
				case DebugSource.DebugSourceApplication: return "Application";
				case DebugSource.DebugSourceOther: return "Other";
				case DebugSource.DebugSourceShaderCompiler: return "Shader compiler";
				case DebugSource.DebugSourceThirdParty: return "Third party";
				case DebugSource.DebugSourceWindowSystem: return "Window system";
			}

			return "Unknown";
		}

		private static string getDebugTypeText(DebugType type)
		{
			switch (type)
			{
				case DebugType.DebugTypeDeprecatedBehavior: return "Deprecated";
				case DebugType.DebugTypeError: return "Error";
				case DebugType.DebugTypeMarker: return "Marker";
				case DebugType.DebugTypeOther: return "Other";
				case DebugType.DebugTypePerformance: return "Performance";
				case DebugType.DebugTypePopGroup: return "PopGroup";
				case DebugType.DebugTypePortability: return "Portability";
				case DebugType.DebugTypePushGroup: return "PushGroup";
				case DebugType.DebugTypeUndefinedBehavior: return "Undefined";
			}

			return "Unknown";
		}

		internal static void TraceDebugProc(DebugSource source, DebugType type, int id, DebugSeverity severity, int length, IntPtr message, IntPtr userParam)
		{
			string messageStr = Marshal.PtrToStringAnsi(message, length).Trim();
			GLTrace.Instance.DebugMessage(source, type, id, severity, messageStr);
			
			if (type == DebugType.DebugTypePushGroup || type == DebugType.DebugTypePopGroup)
				return;
			
			string sourceStr = getDebugSourceText(source);
			string typeStr = getDebugTypeText(type);
			string text = $"[{sourceStr}/{typeStr}] {messageStr} ({id:X})";

			switch (type)
			{
				case DebugType.DebugTypeError:
					Trace.WriteError(text);
					break;

				case DebugType.DebugTypeDeprecatedBehavior:
				case DebugType.DebugTypeUndefinedBehavior:
					Trace.WriteWarning(text);
					break;
					
				default:
					Trace.WriteInformation(text);
					break;
			}

		}

		private static ObjectLabelIdentifier getObjectLabelIdentifier(IGLObject obj)
		{
			switch (obj)
			{
				case Buffer _:
					return ObjectLabelIdentifier.Buffer;
				case Shader _:
					return ObjectLabelIdentifier.Shader;
				case Program _:
					return ObjectLabelIdentifier.Program;
				case VertexArray _:
					return ObjectLabelIdentifier.VertexArray;
				case Sampler _:
					return ObjectLabelIdentifier.Sampler;
				case Texture _:
					return ObjectLabelIdentifier.Texture;
				case Renderbuffer _:
					return ObjectLabelIdentifier.Renderbuffer;
				case Framebuffer _:
					return ObjectLabelIdentifier.Framebuffer;
				default:
					throw new NotSupportedException("Unsupported OpenGL object.");
			}
		}

		private static bool exists(IGLObject obj)
		{
			var gl = obj.Context.GL;
			
			switch (obj)
			{
				case Buffer _:
					return gl.IsBuffer(obj.ID);
				case Shader _:
					return gl.IsShader(obj.ID);
				case Program _:
					return gl.IsProgram(obj.ID);
				case VertexArray _:
					return gl.IsVertexArray(obj.ID);
				case Sampler _:
					return gl.IsSampler(obj.ID);
				case Texture _:
					return gl.IsTexture(obj.ID);
				case Renderbuffer _:
					return gl.IsRenderbuffer(obj.ID);
				case Framebuffer _:
					return gl.IsFramebuffer(obj.ID);
				default:
					throw new NotSupportedException("Unsupported OpenGL object.");
			}
		}


		/// <summary>
		/// Tries to set the object label for the specified object.
		/// The OpenGL implementation must support the GL_KHR_debug extension.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="label"></param>
		/// <returns></returns>
		public static bool TrySetLabel(this IGLObject obj, string label)
		{
			if (!obj.Context.Capabilities.HasExtension("GL_KHR_debug"))
				return false;

			ObjectLabelIdentifier identifier = getObjectLabelIdentifier(obj);
			bool res = obj.Context.Invoke(() =>
			{
				int maxLength = obj.Context.GL.GetInteger((GetPName)KhrDebug.MaxLabelLength);
				if (label.Length > maxLength)
				{
					label = label.Substring(0, maxLength);
				}

				obj.Context.GL.ObjectLabel(identifier, obj.ID, label);
				return true;
			});

			if (res)
				GLTrace.Instance.ObjectLabelAssigned(obj.GetType().Name, obj.ID, label);
			
			return res;
		}

		/// <summary>
		/// Tries to set the object label for the specified object.
		/// The OpenGL implementation must support the GL_KHR_debug extension.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="label"></param>
		/// <returns></returns>
		public static async ValueTask<bool> TrySetLabelAsync(this IGLObject obj, string label)
		{
			if (!obj.Context.Capabilities.HasExtension("GL_KHR_debug"))
				return false;

			ObjectLabelIdentifier identifier = getObjectLabelIdentifier(obj);
			bool res = await obj.Context.InvokeAsync(() =>
			{
				int maxLength = obj.Context.GL.GetInteger((GetPName)KhrDebug.MaxLabelLength);
				if (label.Length > maxLength)
				{
					label = label.Substring(0, maxLength);
				}

				obj.Context.GL.ObjectLabel(identifier, obj.ID, label);
				return true;
			});

			if (res)
				GLTrace.Instance.ObjectLabelAssigned(obj.GetType().Name, obj.ID, label);

			return res;
		}

		/// <summary>
		/// Tries to get the object label for the specified object.
		/// The OpenGL implementation must support the GL_KHR_debug extension.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="label"></param>
		/// <returns></returns>
		public static bool TryGetLabel(this IGLObject obj, out string label)
		{
			label = null;
			if (!obj.Context.Capabilities.HasExtension("GL_KHR_debug"))
				return false;

			ObjectLabelIdentifier identifier = getObjectLabelIdentifier(obj);

			bool success = true;

			label = obj.Context.Invoke(() =>
			{
				if (!exists(obj))
				{
					success = false;
					return null;
				}

				int maxLength = obj.Context.GL.GetInteger((GetPName)KhrDebug.MaxLabelLength);
				StringBuilder str = new StringBuilder(maxLength + 1);

				try
				{
					obj.Context.GL.GetObjectLabel(identifier, obj.ID, str.Capacity, out int length, str);
					return str.ToString(0, length);
				}
				catch (OpenGLException)
				{
					success = false;
					return null;
				}
			});

			if (!success)
				label = null;

			return success;
		}

		#endregion
	}
}
