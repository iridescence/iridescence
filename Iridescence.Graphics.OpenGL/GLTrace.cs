﻿using System.Diagnostics.Tracing;

namespace Iridescence.Graphics.OpenGL
{
	[EventSource(Name = "Iridescence-OpenGL-Trace")]
	internal sealed class GLTrace : EventSource
	{
		#region Fields

		public static readonly GLTrace Instance = new GLTrace();

		#endregion

		#region Properties

		#endregion

		#region Constructors

		#endregion

		#region Methods

		[Event(1)]
		public void ObjectCreated(string type, uint id)
		{
			//Debug.WriteLine($"+ {type} {id}");
			this.WriteEvent(1, type, id);
		}

		[Event(2)]
		public void ObjectDeleted(string type, uint id)
		{
			//Debug.WriteLine($"- {type} {id}");
			this.WriteEvent(2, type, id);
		}

		[Event(3)]
		public void ObjectLabelAssigned(string type, uint id, string name)
		{
			this.WriteEvent(3, type, id, name ?? string.Empty);
		}

		[Event(4)]
		public void DebugMessage(DebugSource source, DebugType type, int id, DebugSeverity severity, string message)
		{
			this.WriteEvent(4, source, type, id, message);
		}

		#endregion
	}
}
