﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// Represents an OpenGL context.
	/// </summary>
	/// <remarks>
	/// This class does not provide methods to create context.
	/// It is assumed that the thread that creates a <see cref="GLContext"/> has a current OpenGL context.
	/// </remarks>
	public sealed class GLContext : IDisposable, ITaskDispatcher
	{
		#region Fields

		private readonly ITaskDispatcher dispatcher;

		private DebugProc debugProc;

		private readonly State defaultState;
		private readonly State currentState;
		private bool currentStateInvalid;
		private readonly State tempState;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a collection of context child objects.
		/// </summary>
		internal GLContextChildCollection Children { get; }

		/// <summary>
		/// Gets the capabilities.
		/// </summary>
		public Capabilities Capabilities { get; private set; }

		/// <summary>
		/// Gets the OpenGL state stack.
		/// </summary>
		public StateStack State { get; }

		/// <summary>
		/// Gets the current OpenGL pipeline state.
		/// </summary>
		internal State CurrentState => this.currentState;
		
		/// <summary>
		/// Gets the OpenGL functions.
		/// </summary>
		public GL GL { get; }

		/// <summary>
		/// Gets a value that indicates whether the context has debugging features enabled.
		/// </summary>
		public bool Debug { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new OpenGL Context. This expects the current thread to have a valid OpenGL context attached.
		/// </summary>
		/// <param name="bindings">The OpenGL native bindings.</param>
		/// <param name="dispatcher">The dispatcher implementation that invokes code in the OpenGL context.</param>
		/// <param name="debug">Specifies whether debugging should be enabled, if available.</param>
		public GLContext(GLBindings bindings, ITaskDispatcher dispatcher, bool debug)
		{
			this.dispatcher = dispatcher ?? throw new ArgumentNullException(nameof(dispatcher));
			this.Debug = debug;
			this.GL = new GL(bindings);

			this.Children = new GLContextChildCollection();
			this.State = new StateStack();

			this.defaultState = new State();
			this.currentState = new State();
			this.currentStateInvalid = true;
			this.tempState = new State();

			Trace.WriteVerbose("Querying capabilities...");
			this.Capabilities = new Capabilities(this.GL);
			if (this.Debug && this.Capabilities.HasExtension("GL_ARB_debug_output"))
			{
				this.debugProc = DebugUtility.TraceDebugProc;
				this.GL.DebugMessageCallback(this.debugProc, IntPtr.Zero);
				this.GL.Enable(EnableCap.DebugOutputSynchronous);
				this.GL.ThrowOnError();

				Trace.WriteVerbose("Enabled debug output extension.");
			}

			this.GL.ThrowOnError();
		}

		#endregion

		#region Methods

		public void Dispose()
		{
			GC.KeepAlive(this.debugProc);
			this.Children?.Dispose();
		}

		/// <summary>
		/// Notifies the <see cref="GLContext"/> that the internal state tracking is not valid anymore and the state has to be reset.
		/// </summary>
		public void InvalidateState()
		{
			this.currentStateInvalid = true;
		}

		/// <summary>
		/// Applies the currently selected state.
		/// </summary>
		internal void ApplyState(StateApplyMode mode)
		{
			// Merge state stack. tempState contains the defaults.
			this.State.MergeState(this.tempState);

			// Apply merged state relative to current state.
			this.tempState.Apply(this.GL, this.currentState, this.currentStateInvalid ? StateApplyMode.Invalid : mode);
			this.currentStateInvalid = false;

			// Reset new tempState to defaults.
			this.defaultState.CopyTo(this.tempState);
		}

		/// <summary>
		/// Clears the framebuffer specified in the current state.
		/// </summary>
		/// <param name="buffers">The buffers to clear.</param>
		public void Clear(ClearBufferMask buffers)
		{
			this.Invoke(() =>
			{
				// apply state.
				this.ApplyState(StateApplyMode.Clear);

				// clear.
				this.GL.Clear(buffers);
			});
		}

		/// <summary>
		/// Clears the framebuffer specified in the current state.
		/// </summary>
		/// <param name="color">Determines whether to clear the color buffer.</param>
		/// <param name="depth">Determines whether to clear the depth buffer.</param>
		/// <param name="stencil">Determines whether to clear the stencil buffer.</param>
		public void Clear(bool color, bool depth, bool stencil)
		{
			ClearBufferMask mask = 0;
			if (color) mask |= ClearBufferMask.ColorBufferBit;
			if (depth) mask |= ClearBufferMask.DepthBufferBit;
			if (stencil) mask |= ClearBufferMask.StencilBufferBit;
			this.Clear(mask);
		}

		/// <summary>
		/// Draws primitives using a <see cref="ProgramState"/> and <see cref="VertexArray"/>.
		/// </summary>
		/// <param name="programState">The program state to use.</param>
		/// <param name="vertexArray">The vertex array that specified the input data.</param>
		/// <param name="batch">An enumeration of <see cref="IDrawInfo"/> objects that describe which parts to render.</param>
		public void Draw<T>(IProgramState programState, VertexArray vertexArray, IEnumerable<T> batch)
			where T : IDrawInfo
		{
			this.Invoke(() =>
			{
				// Apply state.
				using (this.State.With(new DrawState {Program = programState.Program, VertexArray = vertexArray}))
				{
					this.ApplyState(StateApplyMode.Draw);
					programState.Apply();

					// Invoke draw calls.
					foreach (T item in batch)
					{
						item.Draw(this);
					}
				}
			});
		}

		/// <summary>
		/// Draws primitives using a <see cref="ProgramState"/> and <see cref="VertexArray"/>.
		/// </summary>
		/// <param name="programState">The program state to use.</param>
		/// <param name="vertexArray">The vertex array that specified the input data.</param>
		/// <param name="drawInfo">A <see cref="IDrawInfo"/> object that describe what to render.</param>
		public void Draw<T>(IProgramState programState, VertexArray vertexArray, T drawInfo)
			where T : IDrawInfo
		{
			this.Invoke(() =>
			{
				// Apply state.
				using (this.State.With(new DrawState {Program = programState.Program, VertexArray = vertexArray}))
				{
					this.ApplyState(StateApplyMode.Draw);
					programState.Apply();

					// Invoke draw call.
					drawInfo.Draw(this);
				}
			});
		}

		/// <summary>
		/// Draws primitives using a <see cref="ProgramState"/> and <see cref="VertexArray"/>.
		/// </summary>
		/// <param name="programState">The program state to use.</param>
		/// <param name="vertexArray">The vertex array that specified the input data.</param>
		/// <param name="batch">An array of <see cref="IDrawInfo"/> objects that describe which parts to render.</param>
		public void Draw<T>(IProgramState programState, VertexArray vertexArray, params T[] batch)
			where T : IDrawInfo
		{
			this.Draw(programState, vertexArray, (IEnumerable<T>)batch);
		}

		/// <summary>
		/// Copies a block of pixels from the read framebuffer to the draw framebuffer.
		/// </summary>
		/// <param name="sourceX0">Source region start X position.</param>
		/// <param name="sourceY0">Source region start Y position.</param>
		/// <param name="sourceX1">Source region end X position.</param>
		/// <param name="sourceY1">Source region end Y position.</param>
		/// <param name="destX0">Destination region start X position.</param>
		/// <param name="destY0">Destination region start Y position.</param>
		/// <param name="destX1">Destination region end X position.</param>
		/// <param name="destY1">Destination region end Y position.</param>
		/// <param name="mask">The buffer mask.</param>
		/// <param name="filter">The filter to use.</param>
		public void BlitFramebuffer(int sourceX0, int sourceY0, int sourceX1, int sourceY1, int destX0, int destY0, int destX1, int destY1, ClearBufferMask mask, BlitFramebufferFilter filter)
		{
			this.Invoke(() =>
			{
				// apply state.
				this.ApplyState(StateApplyMode.Blit);

				// blit.
				this.GL.BlitFramebuffer(sourceX0, sourceY0, sourceX1, sourceY1, destX0, destY0, destX1, destY1, mask, filter);
			});
		}

		/// <summary>
		/// Returns a new <see cref="Fence"/> object retrieved by calling <see cref="GL.FenceSync(SyncCondition, int)"/>.
		/// </summary>
		/// <returns></returns>
		public Fence CreateFence()
		{
			return this.Invoke(() =>
			{
				IntPtr sync = this.GL.FenceSync(SyncCondition.SyncGpuCommandsComplete, 0);
				return new Fence(this, sync);
			});
		}

		/// <summary>
		/// Returns a new <see cref="Fence"/> object retrieved by calling <see cref="GL.FenceSync(SyncCondition, int)"/>.
		/// </summary>
		/// <returns></returns>
		public async ValueTask<Fence> CreateFenceAsync()
		{
			return await this.InvokeAsync(() =>
			{
				IntPtr sync = this.GL.FenceSync(SyncCondition.SyncGpuCommandsComplete, 0);
				return new Fence(this, sync);
			});
		}

		/// <summary>
		/// Pushes a debug group. Dispose the returned <see cref="IDisposable"/> to pop the group.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="id"></param>
		/// <param name="message"></param>
		/// <returns></returns>
		/// <remarks>
		/// Requires the KHR_debug extension.
		/// </remarks>
		public IDisposable PushDebugGroup(DebugSourceExternal source, uint id, string message)
		{
			if (!this.Capabilities.HasExtension("GL_KHR_debug"))
				return EmptyDisposable.Instance;

			this.Invoke(() => { this.GL.PushDebugGroup(source, id, message); });
			return new DisposableDelegate(() => { this.Invoke(this.GL.PopDebugGroup); });
		}

		#region Dispatcher

		public ValueTask InvokeAsync(Action action) => this.dispatcher.InvokeAsync(action);

		public ValueTask<T> InvokeAsync<T>(Func<T> func) => this.dispatcher.InvokeAsync(func);

		public void Invoke(Action action) => this.dispatcher.Invoke(action);

		public T Invoke<T>(Func<T> func) => this.dispatcher.Invoke(func);

		public void Post(Action action) => this.dispatcher.Post(action);

		#endregion

		#endregion
	}
}
