﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// The delegate for function pointer address retrieval.
	/// </summary>
	/// <param name="name">The name of the function.</param>
	/// <returns>The address of the function, or zero if the function does not exist.</returns>
	public delegate IntPtr GetProcAddress(string name);
}