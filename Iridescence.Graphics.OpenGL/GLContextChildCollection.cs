﻿using System;

namespace Iridescence.Graphics.OpenGL
{
	/// <summary>
	/// A collection of context child objects.
	/// </summary>
	internal sealed class GLContextChildCollection : IDisposable
	{
		#region Fields

		private readonly WeakCollection<GLContextChild> objects;
		private ConcurrentFlag isDisposed;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ContextChildCollection.
		/// </summary>
		public GLContextChildCollection()
		{
			this.objects = new WeakCollection<GLContextChild>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds the specified context child.
		/// </summary>
		/// <param name="child"></param>
		public void Add(GLContextChild child)
		{
			if (this.isDisposed.IsSet)
				throw new ObjectDisposedException(nameof(GLContext));

			this.objects.Add(child);
		}

		public void Dispose()
		{
			if(!this.isDisposed.TrySet())
				return;

			foreach (GLContextChild child in this.objects)
			{
				child.Dispose();
			}

			this.objects.Clear();
		}

		#endregion
	}
}
