﻿using System;
using System.IO;
using Iridescence.Graphics;
using Iridescence.Graphics.Font;
using Iridescence.Graphics.Font.FreeType;
using Iridescence.Graphics.IO;
using Iridescence.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class FontTest
	{
		#region Methods

		[TestMethod]
		[DeploymentItem("Resources/Roboto-Regular.ttf")]
		public void FromFile()
		{
			using (FreeTypeLibrary library = new FreeTypeLibrary())
			{
				FontFace face = library.FromFile("Roboto-Regular.ttf", 0);
				Console.WriteLine(face.Name);

				Font font = face.GetFont(16.0f);
				Console.WriteLine($"Size = {font.Size}");
				Console.WriteLine($"Ascender = {font.Ascender}");
				Console.WriteLine($"Descender = {font.Descender}");
				Console.WriteLine($"LineHeight = {font.LineHeight}");

				for (char c = 'A'; c <= 'Z'; ++c)
				{
					Glyph glyph = font.GetGlyph(c);
					PathGeometry geom = glyph.Geometry.ToPath();

					using (FileStream outputStream = new FileStream($"Geometry - {c}.svg", FileMode.Create, FileAccess.Write))
					using (StreamWriter writer = new StreamWriter(outputStream))
					{
						writer.WriteLine($"<svg height=\"{glyph.Size.X}\" width=\"{glyph.Size.Y}\" xmlns=\"http://www.w3.org/2000/svg\">");

						foreach (PathFigure figure in geom.Figures)
						{
							writer.Write($"<path d=\"M {figure.StartPoint.X} {figure.StartPoint.Y} ");

							foreach (PathSegment segment in figure.Segments)
							{
								switch (segment)
								{
									case LinePathSegment line:
										writer.Write($"L {line.Point.X} {line.Point.Y} ");
										break;

									case QuadraticBezierPathSegment quad:
										writer.Write($"Q {quad.ControlPoint.X} {quad.ControlPoint.Y}, {quad.EndPoint.X} {quad.EndPoint.Y} ");
										break;

									case CubicBezierPathSegment cubic:
										writer.Write($"C {cubic.ControlPoint1.X} {cubic.ControlPoint1.Y}, {cubic.ControlPoint2.X} {cubic.ControlPoint2.Y}, {cubic.EndPoint.X} {cubic.EndPoint.Y} ");
										break;
								}
							}

							if (figure.IsClosed)
							{
								writer.Write("Z");
							}

							writer.WriteLine("\" fill=\"black\" />");
						}

						writer.WriteLine("</svg>");
					}					
				}
			}
		}

		private sealed class DemoProvider : ITextureFontImageProvider, ISignedDistanceFieldParameterProvider
		{
			public Image CreateImage()
			{
				return new PixelBox(new Format(FormatType.R8G8B8, FormatSemantic.UNorm), 512, 512);
			}

			public SignedDistanceFieldParameters GetParameters(Glyph glyph)
			{
				SignedDistanceFieldParameters p = SignedDistanceFieldParameters.Default;
				p.FitGeometry(glyph.Geometry);
				return p;
			}
		}

		[TestMethod]
		[DeploymentItem("Resources/Roboto-Regular.ttf")]
		public void GenerateMsdf()
		{
			SignedDistanceFieldParameters parameters = SignedDistanceFieldParameters.Default;
			parameters.Size = (32, 32);

			using (FreeTypeLibrary library = new FreeTypeLibrary())
			{
				FontFace face = library.FromFile("Roboto-Regular.ttf", 0);
				Console.WriteLine(face.Name);

				Font font = face.GetFont(16.0f);
				Console.WriteLine($"Size = {font.Size}");
				Console.WriteLine($"Ascender = {font.Ascender}");
				Console.WriteLine($"Descender = {font.Descender}");
				Console.WriteLine($"LineHeight = {font.LineHeight}");

				DemoProvider provider = new DemoProvider();
				SignedDistanceFieldFont sdfFont = new SignedDistanceFieldFont(font, provider, provider);

				for (uint c = 0; c < 127; ++c)
				{
					sdfFont.GetGlyph(c);
				}

				foreach (TextureFontPage pg in sdfFont.Pages)
				{
					using (FileStream stream = new FileStream($"Page {pg.Index}.tga", FileMode.Create, FileAccess.Write))
					{
						Tga.Write(stream, pg.Image);
					}
				}
				
			}
		}


		#endregion
	}
}
