﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management.Instrumentation;
using System.Reflection;
using Iridescence;
using Iridescence.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class MetadataTests
	{
		[TestMethod]
		public void SignatureTest()
		{
			IEnumerable<MethodBase> methods = AppDomain.CurrentDomain
				.GetAssemblies()
				.SelectMany(a => a.GetTypes())
				.SelectMany(t => t.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static).Cast<MethodBase>()
					.Concat(t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static)));

			foreach (MethodBase method in methods)
			{
				Signature sig1 = new Signature(method);
				Signature sig2 = new Signature(method);
				Assert.AreEqual(sig1, sig2);
			}
		}

		[TestMethod]
		public void EncodeDecodeAllMembers()
		{
			IEnumerable<Type> declaredTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes());

			void encodeAll(IEnumerable<MemberInfo> members)
			{
				using (MemoryStream tempStream = new MemoryStream())
				{
					foreach (MemberInfo member in members)
					{
						CompactMetadataWriter writer = new CompactMetadataWriter();
						CompactMetadataReader reader = new CompactMetadataReader(Assembly.Load);

						tempStream.Position = 0;
						writer.WriteMember(tempStream, member);

						tempStream.Position = 0;
						MemberInfo decodedMember = reader.ReadMember(tempStream);

						Assert.AreEqual(member.MetadataToken, decodedMember.MetadataToken, $"{member} != {decodedMember}");
					}
				}
			}

			encodeAll(declaredTypes);
			encodeAll(declaredTypes.SelectMany(type => type.GetMembers(BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly)));
		}
	}
}
