﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class ArrayHeapTest
	{
		private sealed class HeapSegment
		{
			public int Index;
			public int Count;
			public bool Used;

			public HeapSegment(MemoryHeapSegment segment, bool used)
			{
				this.Index = segment.Index;
				this.Count = segment.Count;
				this.Used = used;
			}

			public override string ToString()
			{
				return $"{(this.Used ? "used" : "free")} {this.Index} to {this.Index + this.Count - 1} ({this.Count})";
			}
		}

		private static void checkHeap<T>(ArrayHeap<T> heap)
		{
			HeapSegment[] segments = heap.FreeSegments.Select(s => new HeapSegment(s, false)).Union(heap.UsedSegments.Select(s => new HeapSegment(s, true))).ToArray();
			HeapSegment[] itemSegments = new HeapSegment[heap.Capacity];

			foreach (HeapSegment segment in segments.OrderBy(s => s.Index))
			{
				Console.WriteLine(segment);
			}
			Console.WriteLine();

			foreach (HeapSegment segment in segments)
			{
				if(segment.Count <= 0 || segment.Index < 0 || segment.Index + segment.Count > heap.Capacity)
					throw new Exception($"Invalid segment {segment}");

				for (int i = segment.Index; i < segment.Index + segment.Count; ++i)
				{
					if (itemSegments[i] != null)
						throw new Exception($"{segment} overlaps with {itemSegments[i]}.");

					itemSegments[i] = segment;
				}
			}

			HeapSegment last = null;
			for (int i = 0; i < itemSegments.Length; ++i)
			{
				if (itemSegments[i] == null)
				{
					throw new Exception($"Index {i} is neither free nor used.");
				}

				if (last != null)
				{
					if (!itemSegments[i].Used && !last.Used && itemSegments[i] != last)
						throw new Exception("Two consecutive free segments.");
				}

				last = itemSegments[i];
			}
		}

		[TestMethod]
		public void Allocate()
		{
			ArrayHeap<int> heap = new ArrayHeap<int>(8);
			checkHeap(heap);

			for (int i = 1; i <= 10; ++i)
			{
				heap.Allocate(i);
				checkHeap(heap);
			}
		}

		[TestMethod]
		public void Free()
		{
			ArrayHeap<int> heap = new ArrayHeap<int>(16);
			checkHeap(heap);

			List<int> indices = new List<int>();

			for (int i = 1; i <= 4; ++i)
			{
				indices.Add(heap.Allocate(i));
				checkHeap(heap);
			}

			foreach (int index in indices)
			{
				Assert.IsTrue(heap.Free(index));
				checkHeap(heap);
			}
		}

		[TestMethod]
		public void Free2()
		{
			ArrayHeap<int> heap = new ArrayHeap<int>(20);
			checkHeap(heap);

			int r1 = heap.Allocate(5);
			checkHeap(heap);
			int r2 = heap.Allocate(10);
			checkHeap(heap);
			int r3 = heap.Allocate(5);
			checkHeap(heap);

			Assert.IsTrue(heap.Free(r3));
			checkHeap(heap);
			Assert.IsTrue(heap.Free(r2));
			checkHeap(heap);
			Assert.IsTrue(heap.Free(r1));
			checkHeap(heap);
		}

		[TestMethod]
		public void RandomOperations()
		{
			ArrayHeap<int> heap = new ArrayHeap<int>(32);
			checkHeap(heap);

			Random rng = new Random(0);
			List<int> indices = new List<int>();

			for (int i = 0; i < 100; ++i)
			{
				int action = rng.Next(0, indices.Count > 0 ? 2 : 1);
				if (action == 0)
				{
					int count = rng.Next(1, 100);
					Console.Write($"Allocate({count})");

					int index = heap.Allocate(count);
					indices.Add(index);
					Console.WriteLine($" => {index}");
				}
				else
				{
					int j = rng.Next(0, indices.Count);
					int index = indices[j];

					Console.WriteLine($"Free({index})");

					Assert.IsTrue(heap.Free(index));
					indices.RemoveAt(j);
				}

				checkHeap(heap);
			}
		}

		[TestMethod]
		public void TrimExcess()
		{
			ArrayHeap<int> heap = new ArrayHeap<int>(32);
			checkHeap(heap);

			heap.Allocate(7);
			checkHeap(heap);

			heap.TrimExcess();
			checkHeap(heap);
			Assert.AreEqual(7, heap.Capacity);

			heap.TrimExcess();
			checkHeap(heap);
			Assert.AreEqual(7, heap.Capacity);
		}

		[TestMethod]
		public void Compact()
		{
			MemoryHeapSegmentMoveCallback callback = (s, i) =>
			{
				Console.WriteLine($"Moved {s} to {i}.");
			};
			ArrayHeap<int> heap = new ArrayHeap<int>(50);
			checkHeap(heap);

			Console.WriteLine("Allocate(10, 10, 10, 10, 10)");
			heap.Allocate(10);
			heap.Allocate(10);
			heap.Allocate(10);
			heap.Allocate(10);
			heap.Allocate(10);
			checkHeap(heap);

			Console.WriteLine("Free(10, 30)");
			Assert.IsTrue(heap.Free(10));
			Assert.IsTrue(heap.Free(30));
			checkHeap(heap);

			Console.WriteLine("Compact()");
			heap.Compact(callback);
			checkHeap(heap);
			
			Console.WriteLine("Allocate(10, 10)");
			heap.Allocate(10);
			heap.Allocate(10);
			checkHeap(heap);

			Console.WriteLine("Free(0)");
			Assert.IsTrue(heap.Free(0));
			checkHeap(heap);

			Console.WriteLine("Compact()");
			heap.Compact(callback);
			checkHeap(heap);

			Console.WriteLine("Compact()");
			heap.Compact(callback);
			checkHeap(heap);

			Console.WriteLine("Allocate(10)");
			int i6 = heap.Allocate(10);
			checkHeap(heap);

			Console.WriteLine("Compact()");
			heap.Compact(callback);
			checkHeap(heap);
			
			Console.WriteLine("Free(0, 20, 40)");
			heap.Free(0);
			heap.Free(20);
			heap.Free(40);
			checkHeap(heap);
			
			Console.WriteLine("Compact()");
			heap.Compact(callback);
			checkHeap(heap);

			Console.WriteLine("TrimExcess()");
			heap.TrimExcess();
			checkHeap(heap);

			Assert.AreEqual(20, heap.Capacity);
		}
	}
}


