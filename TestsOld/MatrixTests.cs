﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using Iridescence.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class MatrixTests
	{
		#region Fields

		private const int testsPerOp = 100;

		private readonly Random rng = new Random(0);

		#endregion

		#region Methods

		#region Misc

		private static TTo cast<TTo>(object obj)
		{
			return (TTo)Convert.ChangeType(obj, typeof(TTo));
		}

		private static void invokeMethod(MethodInfo method, object[] parameters)
		{
			object[] actualParameters;
			object obj = null;

			bool firstArgIsThis = false;
			if (!method.IsStatic)
			{
				firstArgIsThis = true;
				obj = parameters[0];
				actualParameters = parameters.Skip(1).ToArray();
			}
			else
			{
				actualParameters = parameters;
			}

			bool lastArgIsReturnValue = false;
			if (method.ReturnType != typeof(void))
			{
				Array.Resize(ref actualParameters, actualParameters.Length - 1);
				lastArgIsReturnValue = true;
			}

			object result = method.Invoke(obj, actualParameters);
			
			if (lastArgIsReturnValue)
			{
				parameters[parameters.Length - 1] = result;
			}

			if (firstArgIsThis)
			{
				parameters[0] = obj;
				Array.Copy(actualParameters, 0, parameters, 1, actualParameters.Length);
			}
			else
			{
				Array.Copy(actualParameters, 0, parameters, 0, actualParameters.Length);
			}
		}

		private static Type getElementType(Type type)
		{
			if (type.IsByRef)
				return type.GetElementType();
			return type;
		}

		private static Type getMatrixType(int rows, int columns, string suffix)
		{
			if (columns == 1)
				return typeof(Matrix).Assembly.GetType($"Iridescence.Math.Vector{rows}{suffix}");
			return typeof(Matrix).Assembly.GetType($"Iridescence.Math.Matrix{rows}x{columns}{suffix}");
		}

		private static string toStringSageMath<T>(IMatrix<T> matrix)
		{
			StringBuilder str = new StringBuilder();
			str.Append("matrix([");
			
			for (int row = 0; row < matrix.Rows; ++row)
			{
				if (row > 0)
					str.Append(", ");

				str.Append("[");

				for (int col = 0; col < matrix.Columns; ++col)
				{
					if (col > 0)
						str.Append(", ");

					T val = matrix[row, col];
					IFormattable formattable = val as IFormattable;
					if (formattable != null)
						str.Append(formattable.ToString(null, NumberFormatInfo.InvariantInfo));
					else
						str.Append(val);
				}

				str.Append("]");
			}

			str.Append("])");
			return str.ToString();
		}

		private void randomMatrix<T>(IMatrix<T> matrix, double min, double max)
		{
			matrix.ForEach(v =>
			{
				for (;;)
				{
					double rand = min + this.rng.NextDouble() * (max - min);
					T val = (T)Convert.ChangeType(rand, typeof(T));
					if (!val.Equals(default(T)))
						return val;
				}
				
			}, matrix);
		}

		private void simpleMatrix<T>(IMatrix<T> matrix)
		{
			matrix.ForEach((v, r, c) =>
			{
				int idx = r * matrix.Rows + c;
				return (T)Convert.ChangeType(idx, typeof(T));
			}, matrix);
		}

		private void constantMatrix<T>(IMatrix<T> matrix, T value)
		{
			matrix.ForEach(v => value, matrix);
		}

		#endregion

		#region Constructor

		private void testConstructors<TMatrix, T>() where TMatrix : IMatrix<T>
		{
			Type matrixType = typeof(TMatrix);
			TMatrix matrix = (TMatrix)Activator.CreateInstance(matrixType);

			int rows = matrix.Rows;
			int cols = matrix.Columns;

			T[] array1D = new T[rows * cols];
			T[,] array2D = new T[rows, cols];
			for (int row = 0; row < rows; ++row)
			{
				for (int col = 0; col < cols; ++col)
				{
					T val = cast<T>(row * 1000 + col);
					array1D[row * cols + col] = val;
					array2D[row, col] = val;
				}
			}

			Console.WriteLine($"Constructor {typeof(TMatrix).Name}");

			ArrayMatrix<T> genericMatrix = new ArrayMatrix<T>(array2D);

			matrix = (TMatrix)Activator.CreateInstance(matrixType, array1D.Cast<object>().ToArray());
			Assert.IsTrue(Matrix.Equals(genericMatrix, matrix));
			
			matrix = (TMatrix)Activator.CreateInstance(matrixType, new object[] {array1D});
			Assert.IsTrue(Matrix.Equals(genericMatrix, matrix));

			if (cols > 1)
			{
				matrix = (TMatrix)Activator.CreateInstance(matrixType, new object[] {array2D});
				Assert.IsTrue(Matrix.Equals(genericMatrix, matrix));			
			}
		}

		private void testAllConstructors<T>(string suffix)
		{
			MethodInfo testConstructorsMethod = this.GetType().GetMethod(nameof(this.testConstructors), BindingFlags.Instance | BindingFlags.NonPublic);

			for (int rows = 1; rows <= 4; ++rows)
			{
				for (int cols = 1; cols <= 4; ++cols)
				{
					Type matrixType = getMatrixType(rows, cols, suffix);

					if (matrixType == null)
						continue;

					testConstructorsMethod.MakeGenericMethod(matrixType, typeof(T)).Invoke(this, new object[] {});
				}
			}
		}

		#endregion

		#region BinaryOperator

		private void testBinaryOperator<TLeft, TRight, TResult, T>(IEnumerable<string> names, Action<IMatrix<T>, IMatrix<T>, IMatrix<T>> genericImpl, IEqualityComparer<T> comparer) 
			where TLeft : IMatrix<T>
			where TRight : IMatrix<T>
			where TResult : IMatrix<T>
		{
			Type leftType = typeof(TLeft);
			Type rightType = typeof(TRight);
			Type resultType = typeof(TResult);

			// Find relevant methods to test.
			MethodInfo[] methods = leftType.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance).Union(
					rightType.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance)).Union(
					resultType.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance))
				.Where(method => names.Contains(method.Name))
				.Where(method =>
				{
					ParameterInfo[] parameters = method.GetParameters();

					if (parameters.Length == 2)
						return getElementType(parameters[0].ParameterType) == leftType &&
						       getElementType(parameters[1].ParameterType) == rightType &&
						       method.ReturnType == resultType;
					if (parameters.Length == 3)
						return getElementType(parameters[0].ParameterType) == leftType &&
						       getElementType(parameters[1].ParameterType) == rightType &&
						       getElementType(parameters[2].ParameterType) == resultType && parameters[2].IsOut;
					return false;
				}).ToArray();

			if (methods.Length == 0)
				return;

			Console.WriteLine($"{names.First(),12}: {methods.Length}");

			for (int i = 0; i < testsPerOp; ++i)
			{
				// Create matrices.
				IMatrix<T> left = (IMatrix<T>)Activator.CreateInstance(leftType);
				IMatrix<T> right = (IMatrix<T>)Activator.CreateInstance(rightType);
				IMatrix<T> result = (IMatrix<T>)Activator.CreateInstance(resultType);

				// Fill matrices.
				this.randomMatrix(left, -10, 10);
				this.randomMatrix(right, -10, 10);
				this.constantMatrix(result, default(T));

				// Do generic implementation.
				genericImpl(left, right, result);

				// Backup the variables.
				ArrayMatrix<T> leftCopy = new ArrayMatrix<T>(left);
				ArrayMatrix<T> rightCopy = new ArrayMatrix<T>(right);
				ArrayMatrix<T> resultCopy = new ArrayMatrix<T>(result);

				foreach (MethodInfo method in methods)
				{
					// Invoke method.
					this.constantMatrix(result, default(T));
					object[] p = {left, right, result};
					invokeMethod(method, p);

					// Retreive results.
					TLeft leftAfter = (TLeft)p[0];
					TRight rightAfter = (TRight)p[1];
					TResult resultAfter = (TResult)p[2];

					// Compare to generic results.
					Assert.IsTrue(Matrix.Equals(leftCopy, leftAfter));
					Assert.IsTrue(Matrix.Equals(rightCopy, rightAfter));
					Assert.IsTrue(Matrix.Equals(resultCopy, resultAfter, comparer));
				}
			}
		}

		private void testBinaryOperators<TLeft, TRight, TResult, T>(IEqualityComparer<T> comparer)
			where TLeft : IMatrix<T>
			where TRight : IMatrix<T>
			where TResult : IMatrix<T>
		{
			Console.WriteLine($"{typeof(TLeft).Name}/{typeof(TRight).Name} -> {typeof(TResult).Name}");
			this.testBinaryOperator<TLeft, TRight, TResult, T>(new[] {"Add", "op_Addition"}, Matrix.Add, comparer);
			this.testBinaryOperator<TLeft, TRight, TResult, T>(new[] {"Subtract", "op_Subtraction"}, Matrix.Subtract, comparer);
			this.testBinaryOperator<TLeft, TRight, TResult, T>(new[] {"Multiply", "op_Multiply"}, Matrix.Multiply, comparer);
		}

		private void testAllBinaryOperators<T>(string suffix, IEqualityComparer<T> comparer)
		{
			MethodInfo binaryOperatorMethod = this.GetType().GetMethod(nameof(this.testBinaryOperators), BindingFlags.Instance | BindingFlags.NonPublic);

			for (int left = 1; left <= 4; ++left)
			{
				for (int mid = 1; mid <= 4; ++mid)
				{
					for (int right = 1; right <= 4; ++right)
					{
						Type leftType = getMatrixType(left, mid, suffix);
						Type rightType = getMatrixType(mid, right, suffix);
						Type resultType = getMatrixType(left, right, suffix);

						if (leftType == null || rightType == null || resultType == null)
							continue;

						binaryOperatorMethod.MakeGenericMethod(leftType, rightType, resultType, typeof(T)).Invoke(this, new object[] {comparer});
					}
				}
			}
		}

		#endregion

		#region ScalarOperator

		private void testScalarOperator<TMatrix, T>(IEnumerable<string> names, Action<IMatrix<T>, T, IMatrix<T>> genericImpl,  Action<T, IMatrix<T>, IMatrix<T>> genericImplInv, IEqualityComparer<T> comparer) 
			where TMatrix : IMatrix<T>
		{
			Type matrixType = typeof(TMatrix);
			Type scalarType = typeof(T);

			// Find relevant methods to test.
			MethodInfo[] methods = matrixType.GetMethods(BindingFlags.Public | BindingFlags.Static)
				.Where(method => names.Contains(method.Name))
				.Where(method =>
				{
					ParameterInfo[] parameters = method.GetParameters();

					if (parameters.Length == 2)
						return method.ReturnType == matrixType &&
						       ((getElementType(parameters[0].ParameterType) == matrixType &&
						         getElementType(parameters[1].ParameterType) == scalarType) ||
						        (getElementType(parameters[0].ParameterType) == scalarType &&
						         getElementType(parameters[1].ParameterType) == matrixType));

					if (parameters.Length == 3)
						return (getElementType(parameters[2].ParameterType) == matrixType && parameters[2].IsOut) &&
						       ((getElementType(parameters[0].ParameterType) == matrixType &&
						         getElementType(parameters[1].ParameterType) == scalarType) ||
						        (getElementType(parameters[0].ParameterType) == scalarType &&
						         getElementType(parameters[1].ParameterType) == matrixType));
					return false;
				}).ToArray();

			if (methods.Length == 0)
				return;

			Console.WriteLine($"{names.First(),12}: {methods.Length}");

			for (int i = 0; i < testsPerOp; ++i)
			{
				// Create parameters.
				IMatrix<T> matrix = (IMatrix<T>)Activator.CreateInstance(matrixType);
				T scalar = cast<T>(1.0 + this.rng.NextDouble() * 100.0);
				IMatrix<T> result = (IMatrix<T>)Activator.CreateInstance(matrixType);
				IMatrix<T> resultInv = (IMatrix<T>)Activator.CreateInstance(matrixType);

				// Fill matrices.
				this.randomMatrix(matrix, -10, 10);
				this.constantMatrix(result, default(T));
				this.constantMatrix(resultInv, default(T));

				// Do generic implementation.
				genericImpl(matrix, scalar, result);
				genericImplInv?.Invoke(scalar, matrix, resultInv);

				// Backup the variables.
				ArrayMatrix<T> matrixCopy = new ArrayMatrix<T>(matrix);
				ArrayMatrix<T> resultCopy = new ArrayMatrix<T>(result);
				ArrayMatrix<T> resultInvCopy = new ArrayMatrix<T>(resultInv);

				foreach (MethodInfo method in methods)
				{
					ParameterInfo[] parameters = method.GetParameters();
					bool inv = method.IsStatic && getElementType(parameters[0].ParameterType) == scalarType;

					// Invoke method.
					this.constantMatrix(result, default(T));
					object[] p = {matrix, scalar, result};
					if (inv) Utility.Swap(ref p[0], ref p[1]);
					invokeMethod(method, p);
					if (inv) Utility.Swap(ref p[0], ref p[1]);

					// Retreive results.
					TMatrix matrixAfter = (TMatrix)p[0];
					T scalarAfter = (T)p[1];
					TMatrix resultAfter = (TMatrix)p[2];

					// Compare to generic results.
					Assert.IsTrue(Matrix.Equals(matrixCopy, matrixAfter));
					Assert.AreEqual(scalar, scalarAfter);
					Assert.IsTrue(Matrix.Equals(inv ? resultInvCopy : resultCopy, resultAfter, comparer));
				}
			}
		}

		private void testScalarOperators<TMatrix, T>(IEqualityComparer<T> comparer)
			where TMatrix : IMatrix<T>
		{
			Console.WriteLine($"{typeof(TMatrix).Name}/{typeof(T).Name} -> {typeof(TMatrix).Name}");
			this.testScalarOperator<TMatrix, T>(new[] {"Add", "op_Addition"}, Matrix.Add, (s, m, r) => Matrix.Add(m, s, r), comparer);
			this.testScalarOperator<TMatrix, T>(new[] {"Subtract", "op_Subtraction"}, Matrix.Subtract, Matrix.Subtract, comparer);
			this.testScalarOperator<TMatrix, T>(new[] {"Multiply", "op_Multiply"}, Matrix.Multiply, (s, m, r) => Matrix.Multiply(m, s, r), comparer);
			this.testScalarOperator<TMatrix, T>(new[] {"Divide", "op_Division"}, Matrix.Divide, Matrix.Divide, comparer);
		}

		private void testAllScalarOperators<T>(string suffix, IEqualityComparer<T> comparer)
		{
			MethodInfo scalarOperatorMethod = this.GetType().GetMethod(nameof(this.testScalarOperators), BindingFlags.Instance | BindingFlags.NonPublic);

			for (int rows = 1; rows <= 4; ++rows)
			{
				for (int cols = 1; cols <= 4; ++cols)
				{
					Type matrixType = getMatrixType(rows, cols, suffix);

					if (matrixType == null)
						continue;

					scalarOperatorMethod.MakeGenericMethod(matrixType, typeof(T)).Invoke(this, new object[] {comparer});
				}
			}
		}

		#endregion

		#region Tranpose

		private void testTranspose<TInput, TOutput, T>()
			where TInput : IMatrix<T>
			where TOutput : IMatrix<T>
		{
			Type inputType = typeof(TInput);
			Type outputType = typeof(TOutput);

			// Find relevant methods to test.
			MethodInfo[] methods = inputType.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance)
				.Where(method => method.Name == "Transpose")
				.Where(method =>
				{
					ParameterInfo[] parameters = method.GetParameters();

					if (parameters.Length == 0)
						return method.ReturnType == outputType;

					if (parameters.Length == 1)
						return getElementType(parameters[0].ParameterType) == inputType &&
						       method.ReturnType == outputType;

					if (parameters.Length == 2)
						return getElementType(parameters[0].ParameterType) == inputType &&
						       getElementType(parameters[1].ParameterType) == outputType && parameters[1].IsOut;

					return false;
				}).ToArray();

			if (methods.Length == 0)
				return;

			Console.WriteLine($"Transpose {typeof(TInput).Name} -> {typeof(TOutput).Name}: {methods.Length}");

			IMatrix<T> input = (IMatrix<T>)Activator.CreateInstance(inputType);
			IMatrix<T> output = (IMatrix<T>)Activator.CreateInstance(outputType);

			this.simpleMatrix(input);
			this.constantMatrix(output, default(T));

			input.Transpose(output);

			ArrayMatrix<T> inputCopy = new ArrayMatrix<T>(input);
			ArrayMatrix<T> outputCopy = new ArrayMatrix<T>(output);

			foreach (MethodInfo method in methods)
			{
				// Invoke method.
				this.constantMatrix(output, default(T));
				object[] p = {input, output};
				invokeMethod(method, p);

				TInput inputAfter = (TInput)p[0];
				TOutput outputAfter = (TOutput)p[1];

				Assert.IsTrue(Matrix.Equals(inputCopy, inputAfter));
				Assert.IsTrue(Matrix.Equals(outputCopy, outputAfter));
			}
		}

		private void testAllTranspose<T>(string suffix)
		{
			MethodInfo testTransposeMethod = this.GetType().GetMethod(nameof(this.testTranspose), BindingFlags.Instance | BindingFlags.NonPublic);

			for (int rows = 1; rows <= 4; ++rows)
			{
				for (int cols = 1; cols <= 4; ++cols)
				{
					Type inputType = getMatrixType(rows, cols, suffix);
					Type outputType = getMatrixType(cols, rows, suffix);
					if (inputType == null || outputType == null)
						continue;

					testTransposeMethod.MakeGenericMethod(inputType, outputType, typeof(T)).Invoke(this, new object[0]);
				}
			}
		}

		#endregion

		#region Equality

		private void testEquality<TMatrix, T>()
			where TMatrix : IMatrix<T>
		{
			Type matrixType = typeof(TMatrix);

			// Find relevant methods to test.
			MethodInfo[] methods = matrixType.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance)
				.Where(method => method.Name == "Equals" || method.Name == "op_Equality" || method.Name=="op_Inequality")
				.Where(method =>
				{
					ParameterInfo[] parameters = method.GetParameters();

					if (parameters.Length == 1)
						return getElementType(parameters[0].ParameterType).IsAssignableFrom(matrixType) &&
						       method.ReturnType == typeof(bool);

					if (parameters.Length == 2)
						return getElementType(parameters[0].ParameterType) == matrixType &&
						       getElementType(parameters[1].ParameterType) == matrixType &&
						       method.ReturnType == typeof(bool);

					return false;
				}).ToArray();

			if (methods.Length == 0)
				return;

			Console.WriteLine($"Equals {typeof(TMatrix).Name}: {methods.Length}");

			List<IMatrix<T>> matrices = new List<IMatrix<T>>();

			IMatrix<T> temp = (IMatrix<T>)Activator.CreateInstance(matrixType);
			this.constantMatrix(temp, default(T));
			matrices.Add(temp);

			temp = (IMatrix<T>)Activator.CreateInstance(matrixType);
			this.simpleMatrix(temp);
			matrices.Add(temp);

			int rows = temp.Rows;
			int cols = temp.Columns;

			for (int row = 0; row < rows; ++row)
			{
				for (int col = 0; col < cols; ++col)
				{
					temp = (IMatrix<T>)Activator.CreateInstance(matrixType);
					this.constantMatrix(temp, default(T));
					temp[row, col] = cast<T>(1);
					matrices.Add(temp);
				}
			}

			for (int i = 0; i < matrices.Count; ++i)
			{
				for (int j = 0; j < matrices.Count; ++j)
				{
					foreach (MethodInfo method in methods)
					{
						bool expected = Matrix.Equals(matrices[i], matrices[j]);
						ArrayMatrix<T> before1 = new ArrayMatrix<T>(matrices[i]);
						ArrayMatrix<T> before2 = new ArrayMatrix<T>(matrices[j]);

						// Invoke method.
						object[] p = {matrices[i], matrices[j], null};
						invokeMethod(method, p);

						TMatrix after1 = (TMatrix)p[0];
						TMatrix after2 = (TMatrix)p[1];
						bool result = (bool)p[2];

						if (method.Name == "op_Inequality")
							result = !result;

						Assert.IsTrue(Matrix.Equals(before1, after1));
						Assert.IsTrue(Matrix.Equals(before2, after2));
						Assert.AreEqual(expected, result);
					}
				}
			}
		}

		private void testAllEquality<T>(string suffix)
		{
			MethodInfo testEqualityMethod = this.GetType().GetMethod(nameof(this.testEquality), BindingFlags.Instance | BindingFlags.NonPublic);

			for (int rows = 1; rows <= 4; ++rows)
			{
				for (int cols = 1; cols <= 4; ++cols)
				{
					Type matrixType = getMatrixType(rows, cols, suffix);
					if (matrixType == null)
						continue;

					testEqualityMethod.MakeGenericMethod(matrixType, typeof(T)).Invoke(this, new object[0]);
				}
			}
		}

		#endregion

		#region Single

		[TestMethod]
		public void SingleConstructors()
		{
			this.testAllConstructors<float>("");
		}
		
		[TestMethod]
		public void SingleBinaryOp()
		{
			this.testAllBinaryOperators<float>("", new ApproximateSingleComparer(0.0001f));
		}

		[TestMethod]
		public void SingleScalarOp()
		{
			this.testAllScalarOperators<float>("", new ApproximateSingleComparer(0.0001f));
		}

		[TestMethod]
		public void SingleTranspose()
		{
			this.testAllTranspose<float>("");
		}
		
		[TestMethod]
		public void SingleEquality()
		{
			this.testAllEquality<float>("");
		}

		#endregion

		#region Double

		[TestMethod]
		public void DoubleConstructors()
		{
			this.testAllConstructors<double>("D");
		}
		
		[TestMethod]
		public void DoubleBinaryOp()
		{
			this.testAllBinaryOperators<double>("D", new ApproximateDoubleComparer(0.000001));
		}

		[TestMethod]
		public void DoubleScalarOp()
		{
			this.testAllScalarOperators<double>("D", new ApproximateDoubleComparer(0.000001));
		}

		[TestMethod]
		public void DoubleTranspose()
		{
			this.testAllTranspose<double>("D");
		}

		[TestMethod]
		public void DoubleEquality()
		{
			this.testAllEquality<double>("D");
		}

		#endregion

		#region Int

		[TestMethod]
		public void IntConstructors()
		{
			this.testAllConstructors<int>("I");
		}
		
		[TestMethod]
		public void IntBinaryOp()
		{
			this.testAllBinaryOperators<int>("I", EqualityComparer<int>.Default);
		}

		[TestMethod]
		public void IntScalarOp()
		{
			this.testAllScalarOperators<int>("I", EqualityComparer<int>.Default);
		}

		[TestMethod]
		public void IntTranspose()
		{
			this.testAllTranspose<int>("I");
		}

		[TestMethod]
		public void IntEquality()
		{
			this.testAllEquality<int>("I");
		}

		#endregion

		#endregion

		private sealed class ApproximateSingleComparer : IEqualityComparer<float>
		{
			private readonly float tolerance;

			public ApproximateSingleComparer(float tolerance)
			{
				this.tolerance = tolerance;
			}

			public bool Equals(float x, float y)
			{
				return Utility.Absolute(x - y) <= this.tolerance;
			}

			public int GetHashCode(float obj)
			{
				return 0;
			}
		}
		private sealed class ApproximateDoubleComparer : IEqualityComparer<double>
		{
			private readonly double tolerance;

			public ApproximateDoubleComparer(double tolerance)
			{
				this.tolerance = tolerance;
			}

			public bool Equals(double x, double y)
			{
				return Utility.Absolute(x - y) <= this.tolerance;
			}

			public int GetHashCode(double obj)
			{
				return 0;
			}
		}

	}
}
