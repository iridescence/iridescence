﻿using System;
using Iridescence;
using Iridescence.Graphics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class SwizzleCopyTest
	{
		#region Methods

		[TestMethod]
		public unsafe void AllZero()
		{
			MemoryCopyFunction func = SwizzleHelper.GetSwizzleCopyMethod(typeof(int), new[]
			{
				FormatMappingChannel.Zero,
				FormatMappingChannel.Zero,
				FormatMappingChannel.Zero,
				FormatMappingChannel.Zero
			});

			int[] source = {1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4};
			int[] dest = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

			fixed (int* s = source)
			fixed (int* d = dest)
			{
				func(new IntPtr(s), new IntPtr(d), 3);
			}

			for (int i = 0; i < dest.Length; ++i)
			{
				Assert.AreEqual(0, dest[i]);
			}
		}

		[TestMethod]
		public unsafe void AllOne()
		{
			MemoryCopyFunction func = SwizzleHelper.GetSwizzleCopyMethod(typeof(int), new[]
			{
				FormatMappingChannel.One,
				FormatMappingChannel.One,
				FormatMappingChannel.One,
				FormatMappingChannel.One
			});

			int[] source = {1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4};
			int[] dest = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

			fixed (int* s = source)
			fixed (int* d = dest)
			{
				func(new IntPtr(s), new IntPtr(d), 3);
			}

			for (int i = 0; i < dest.Length; ++i)
			{
				Assert.AreEqual(1, dest[i]);
			}
		}

		[TestMethod]
		public unsafe void Identity()
		{
			MemoryCopyFunction func = SwizzleHelper.GetSwizzleCopyMethod(typeof(int), new[]
			{
				FormatMappingChannel.Red,
				FormatMappingChannel.Green,
				FormatMappingChannel.Blue,
				FormatMappingChannel.Alpha
			});

			int[] source = {1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4};
			int[] dest = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

			fixed (int* s = source)
			fixed (int* d = dest)
			{
				func(new IntPtr(s), new IntPtr(d), 3);
			}

			for (int i = 0; i < dest.Length; ++i)
			{
				Assert.AreEqual(i % 4 + 1, dest[i]);
			}
		}

		[TestMethod]
		public unsafe void Reverse()
		{
			MemoryCopyFunction func = SwizzleHelper.GetSwizzleCopyMethod(typeof(int), new[]
			{
				FormatMappingChannel.Alpha,
				FormatMappingChannel.Blue,
				FormatMappingChannel.Green,
				FormatMappingChannel.Red
			});

			int[] source = {1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4};
			int[] dest = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

			fixed (int* s = source)
			fixed (int* d = dest)
			{
				func(new IntPtr(s), new IntPtr(d), 3);
			}

			for (int i = 0; i < dest.Length; ++i)
			{
				Assert.AreEqual(3 - i % 4 + 1, dest[i]);
			}
		}

		#endregion
	}
}
