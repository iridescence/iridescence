﻿using System.Threading;
using Iridescence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class SlidingStreamTest
	{
		#region Methods

		[TestMethod]
		public void SimpleTest()
		{
			using (SlidingStream stream = new SlidingStream())
			{
				stream.Write(new byte[1], 0, 1);
				Assert.AreEqual(1, stream.ReadBytes(1).Length);
				
				stream.Write(new byte[9999], 0, 9999);
				Assert.AreEqual(9999, stream.ReadBytes(9999).Length);

				for (int i = 0; i < 10; ++i)
					stream.Write(new byte[10000], 0, 10000);
				
				for (int i = 0; i < 100; ++i)
					Assert.AreEqual(1000, stream.ReadBytes(1000).Length);
			}
		}

		[TestMethod]
		public void Completion()
		{
			using (SlidingStream stream = new SlidingStream())
			{
				stream.Write(new byte[10], 0, 10);
				stream.Complete();
				Assert.AreEqual(5, stream.Read(new byte[5], 0, 5));
				Assert.AreEqual(4, stream.Read(new byte[4], 0, 4));
				Assert.AreEqual(1, stream.Read(new byte[3], 0, 3));
				Assert.AreEqual(0, stream.Read(new byte[2], 0, 2));
				Assert.AreEqual(0, stream.Read(new byte[2], 0, 2));
				Assert.AreEqual(0, stream.Read(new byte[2], 0, 2));
			}
		}

		[TestMethod]
		public void MultipleProducersSingleConsumer()
		{
			Thread[] producers = new Thread[8];
			const int numWrites = 100;
			const int bytesPerWrite = 100;

			using (SlidingStream stream = new SlidingStream())
			{
				for (int i = 0; i < producers.Length; ++i)
				{
					producers[i] = new Thread(() =>
					{
						Thread.Sleep(10);
						for (int j = 0; j < numWrites; ++j)
							stream.Write(new byte[bytesPerWrite], 0, bytesPerWrite);
					});
				}

				for (int i = 0; i < producers.Length; ++i)
				{
					producers[i].Start();
				}

				for (int i = 0; i < numWrites * producers.Length; ++i)
				{
					Assert.AreEqual(bytesPerWrite, stream.ReadBytes(bytesPerWrite).Length);
				}
				
				for (int i = 0; i < producers.Length; ++i)
				{
					producers[i].Join();
				}

				stream.Complete();
				
				Assert.AreEqual(-1, stream.ReadByte());
			}
		}
		
		#endregion
	}
}
