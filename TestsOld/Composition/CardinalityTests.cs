﻿using Iridescence.Composition;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Composition
{
	[TestClass]
	public class CardinalityTests : ContainerTests
	{
		public interface ILog { }

		[Export(typeof(ILog))]
		public class LogA : ILog { }

		[Export(typeof(ILog))]
		public class LogB : ILog { }

		[Export]
		public class UsesLog
		{
			[ImportingConstructor]
			public UsesLog(ILog log) { }
		}

		[TestMethod]
		public void RequestingOneWhereMultipleArePresentFails()
		{
			var c = CreateContainer(typeof(LogA), typeof(LogB));

			try
			{
				c.GetExport<ILog>();
				Assert.Fail();
			}
			catch (CompositionException x)
			{
				Assert.IsTrue(x.Message.Contains("LogA"));
				Assert.IsTrue(x.Message.Contains("LogB"));
			}
			
		}

		[TestMethod]
		public void ImportingOneWhereMultipleArePresentFails()
		{
			var c = CreateContainer(typeof(LogA), typeof(LogB), typeof(UsesLog));

			try
			{
				c.GetExport<UsesLog>();
				Assert.Fail();
			}
			catch (CompositionException x)
			{
				Assert.IsTrue(x.Message.Contains("LogA"));
				Assert.IsTrue(x.Message.Contains("LogB"));
			}
		}
	}
}
