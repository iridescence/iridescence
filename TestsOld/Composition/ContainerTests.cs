﻿using System;
using Iridescence.Composition;

namespace Tests.Composition
{
	public class ContainerTests
	{
		protected static CompositionContainer CreateContainer(params Type[] types)
		{
			return new ContainerBuilder()
				.WithParts(types)
				.Build();
		}
	}
}
