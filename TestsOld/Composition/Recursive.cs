﻿using Iridescence.Composition;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Composition
{
	[TestClass]
	public class Recursive
	{
		[Export]
		public class A
		{
			[Import] public B B;
		}

		[Export]
		public class B
		{
			[Import] public A A;
		}

		[TestMethod]
		public void Test()
		{
			using (CompositionContainer container = new ContainerBuilder().WithAssembly(typeof(Typed).Assembly).Build())
			{
				A a = container.GetExport<A>();
			}
		}
	}
}
