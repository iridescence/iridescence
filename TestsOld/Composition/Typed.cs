﻿using System.Collections.Generic;
using System.Linq;
using Iridescence.Composition;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Composition
{
	[TestClass]
	public class Typed
	{
		public class Missing
		{

		}

		[Export]
		public class TestClass
		{
		}

		[Export("asdf")]
		public class TestClass2
		{
		}

		[Export]
		public class ImportTestClass
		{
			[Import]
			public TestClass Import1 { get; set; }
		}

		[Export]
		public class ImportTestClassMissing
		{
			[Import]
			public Missing Import1 { get; set; }
		}
		[Export]
		public class ImportTestClassMissingOptional
		{
			[Import(IsOptional = true)]
			public Missing Import1 { get; set; }
		}

		[Export]
		public class ImportManyTestClass
		{
			[ImportMany]
			public TestClass[] Import1 { get; set; }

			[ImportMany]
			public IEnumerable<TestClass> Import2 { get; set; }

			[ImportMany]
			public IReadOnlyList<TestClass> Import3 { get; set; }

			[ImportMany]
			public List<TestClass> Import4 { get; set; }

			[ImportMany]
			public HashSet<TestClass> Import5 { get; set; }
		}

		[Export]
		public class ImportManyTestClassNoneFound
		{
			[ImportMany]
			public Missing[] Import1 { get; set; }

			[ImportMany]
			public IEnumerable<Missing> Import2 { get; set; }

			[ImportMany]
			public IReadOnlyList<Missing> Import3 { get; set; }

			[ImportMany]
			public List<Missing> Import4 { get; set; }

			[ImportMany]
			public HashSet<Missing> Import5 { get; set; }
		}

		[TestMethod]
		public void Test()
		{
			using (CompositionContainer container = new ContainerBuilder().WithAssembly(typeof(Typed).Assembly).Build())
			{
				var t1 = container.GetExport<TestClass>();
				Assert.IsNotNull(t1);

				Assert.IsFalse(container.TryGetExport<TestClass2>(out TestClass2 _));

				var t3 = container.GetExport<TestClass2>("asdf");
				Assert.IsNotNull(t3);

				try
				{
					var t4 = container.GetExport<ImportTestClassMissing>();
					Assert.Fail();
				}
				catch (MissingDependencyException ex)
				{
					Assert.AreEqual(1, ex.Dependencies.Count);
				}

				var t5 = container.GetExport<ImportTestClassMissingOptional>();
				Assert.IsNotNull(t5);
				Assert.IsNull(t5.Import1);
			}
		}

		[TestMethod]
		public void ManyTest()
		{
			using (CompositionContainer container = new ContainerBuilder()
				.WithAssembly(typeof(Typed).Assembly)
				.WithProvider(new ImportManyExportProvider())
				.Build())
			{
				var t1 = container.GetExport<ImportTestClass>();
				Assert.IsNotNull(t1);
				Assert.IsNotNull(t1.Import1);

				var t2 = container.GetExport<ImportManyTestClass>();
				Assert.IsNotNull(t2);
				Assert.IsNotNull(t2.Import1);
				Assert.IsNotNull(t2.Import2);
				Assert.IsNotNull(t2.Import3);
				Assert.IsNotNull(t2.Import4);
				Assert.IsNotNull(t2.Import5);
				Assert.AreEqual(1, t2.Import1.Length);
				Assert.AreEqual(1, t2.Import2.Count());
				Assert.AreEqual(1, t2.Import3.Count);
				Assert.AreEqual(1, t2.Import4.Count);
				Assert.AreEqual(1, t2.Import5.Count);

				var t3 = container.GetExport<ImportManyTestClassNoneFound>();
				Assert.IsNotNull(t3);
				Assert.IsNotNull(t3.Import1);
				Assert.IsNotNull(t3.Import2);
				Assert.IsNotNull(t3.Import3);
				Assert.IsNotNull(t3.Import4);
				Assert.IsNotNull(t3.Import5);
				Assert.AreEqual(0, t3.Import1.Length);
				Assert.AreEqual(0, t3.Import2.Count());
				Assert.AreEqual(0, t3.Import3.Count);
				Assert.AreEqual(0, t3.Import4.Count);
				Assert.AreEqual(0, t3.Import5.Count);
			}
		}
	}
}
