﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using Iridescence.Composition;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Composition
{
	[Export]
	public class Imported { }

	[Export]
	public class TracksImportSatisfaction
	{
		[Import]
		public Imported Imported { get; set; }

		public Imported SetOnImportsSatisfied { get; set; }

		[OnImported]
		public void OnImportsSatisfied()
		{
			SetOnImportsSatisfied = Imported;
		}
	}

	[TestClass]
	public class ActivationEventOrderingTests : ContainerTests
	{
		[TestMethod]
		public void OnImportsSatisfiedIsCalledAfterPropertyInjection()
		{
			var cc = CreateContainer(typeof(TracksImportSatisfaction), typeof(Imported));

			var tis = cc.GetExport<TracksImportSatisfaction>();

			Assert.IsNotNull(tis.SetOnImportsSatisfied);
		}
	}
}
