﻿using Iridescence.Composition;
using Iridescence.FileSystem;
using Iridescence.FileSystem.Composition;
using Iridescence.Graphics;
using Iridescence.Graphics.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Composition
{
	[TestClass]
	public class Images
	{
		[Iridescence.Composition.Export("/Something")]
		public class PngTest
		{
			[Iridescence.Composition.Import("/Data/Test.png")]
			public Image Test { get; set; }
		}

		[Iridescence.Composition.Export("/Data/Asdf/Indir")]
		public class Indirection
		{
			[Iridescence.Composition.Import("../Test.png")]
			public Image Test { get; set; }
		}


		[Iridescence.Composition.Export("/Data/Test")]
		public class PngTest2
		{
			[Iridescence.Composition.Import("Test.png")]
			public Image Test { get; set; }

			[Iridescence.Composition.Import("Asdf/../Test.png")]
			public Image Test2 { get; set; }

			[Iridescence.Composition.Import("Asdf/Indir")]
			public Indirection Test3 { get; set; }
		}



		[TestMethod]
		[DeploymentItem("Resources/Test.png")]
		public void Png()
		{
			FileSystem fs = new FileSystem();

			string dir = System.IO.Directory.GetCurrentDirectory();
			fs.Mount(Path.Absolute("Data"), new SystemDirectory(null, dir, dir));

			FileSystemTest.DumpDirectory(fs.Root);

			using (CompositionContainer container = new ContainerBuilder()
				.WithAssembly(typeof(Typed).Assembly)
				.WithProvider(new PngExportProvider())
				.WithFileSystem(fs.Root)
				.Build())
			{
				PngTest t = container.GetExport<PngTest>("/Something");
				Assert.IsNotNull(t);
				Assert.IsNotNull(t.Test);
			}
		}

		[TestMethod]
		[DeploymentItem("Resources/Test.png")]
		public void Png2()
		{
			FileSystem fs = new FileSystem();

			string dir = System.IO.Directory.GetCurrentDirectory();
			fs.Mount(Path.Absolute("Data"), new SystemDirectory(null, dir, dir));

			FileSystemTest.DumpDirectory(fs.Root);

			using (CompositionContainer container = new ContainerBuilder()
				.WithAssembly(typeof(Typed).Assembly)
				.WithProvider(new PngExportProvider())
				.WithFileSystem(fs.Root)
				.Build())
			{
				PngTest2 t = container.GetExport<PngTest2>("/Data/Test");
				Assert.IsNotNull(t);
				Assert.IsNotNull(t.Test);
			}
		}
	}
}
