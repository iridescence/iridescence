﻿using System.Collections.Generic;
using System.Linq;
using Iridescence.Composition;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Composition
{
	[TestClass]
	public class Simple
	{
		public class Wrapper
		{
			public readonly string Value;

			public Wrapper(string value)
			{
				this.Value = value;
			}
		}

		public class ContractNameProvider : IExportDescriptorProvider
		{
			public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyFactory)
			{
				if (!contract.IsAssignableTo(typeof(string)))
					yield break;

				if (!contract.TryExtractName(out string name, out contract) || name == null)
					yield break;

				yield return new ExportDescriptorPromise(contract, "ContractNameProvider", false, Dependency.NoDependencies, _ =>
				{
					return ExportDescriptor.Create((ctx, op) => name);
				});
			}
		}

		public class ContractNameWrapperProvider : IExportDescriptorProvider
		{
			public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyFactory)
			{
				if (!contract.IsAssignableTo(typeof(Wrapper)))
					yield break;

				IEnumerable<Dependency> getDependencies()
				{
					yield return dependencyFactory.ResolveRequiredDependency("Value", contract.ChangeType(typeof(string)), true);
					if (dependencyFactory.TryResolveOptionalDependency("OptionalTest", contract.ChangeType(typeof(int)), true, out Dependency d))
					{
						Assert.Fail();
						yield return d;
					}
				}

				yield return new ExportDescriptorPromise(contract, "ContractNameWrapperProvider", false, getDependencies, deps =>
				{
					var dep = deps.GetDependency("Value");
					return ExportDescriptor.Create((ctx, op) => new Wrapper((string)dep.GetDescriptor().Activator(ctx, op)));
				});
			}
		}

		[TestMethod]
		public void Test()
		{
			CompositionContainer ctx = new CompositionContainer(new ContractNameProvider());
			var s = ctx.GetExport<string>("asdf");
			Assert.AreEqual(s, "asdf");
		}

		[TestMethod]
		public void DependencyTest()
		{
			CompositionContainer ctx = new CompositionContainer(new ContractNameProvider(), new ContractNameWrapperProvider());
			var w = ctx.GetExport<Wrapper>("asdf");
			Assert.AreEqual(w.Value, "asdf");
		}
	}
}
