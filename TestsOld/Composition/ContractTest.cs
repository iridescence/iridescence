﻿using System;
using System.Collections.Generic;
using Iridescence.Composition;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Composition
{
	[TestClass]
	public class ContractTest
	{
		private class AType { }

		private class AMetadata : CompositionMetadata
		{
			public readonly string Value;
			
			public AMetadata(string value)
			{
				this.Value = value;
			}

			public override bool Equals(object obj)
			{
				return obj is AMetadata metadata &&
					   this.Value == metadata.Value;
			}

			public override int GetHashCode()
			{
				return this.Value.GetHashCode();
			}
		}

		private static readonly Type s_defaultContractType = typeof(AType);

		[TestMethod]
		public void FormattingAContractWithNoDiscriminatorShowsTheSimpleTypeName()
		{
			var c = new CompositionContract(typeof(AType));
			var s = c.ToString();
			Assert.AreEqual("Tests.Composition.ContractTest.AType", s);
		}

		[TestMethod]
		public void FormattingAContractWithAStringDiscriminatorShowsTheDiscriminatorInQuotes()
		{
			var c = new CompositionContract(typeof(AType), "at");
			var s = c.ToString();
			Assert.AreEqual("Tests.Composition.ContractTest.AType Name=\"at\"", s);
		}

		[TestMethod] 
		public void ChangingTheTypeOfAContractChangesTheContractType()
		{
			var c = new CompositionContract(typeof(object));
			var d = c.ChangeType(typeof(AType));
			Assert.AreEqual(typeof(AType), d.Type);
		}

		[TestMethod]
		public void ConstraintsWithEquivalentKeysAndValuesAreEqual()
		{
			var mcd1 = new CompositionContract(s_defaultContractType, new AMetadata("B"));
			var mcd2 = new CompositionContract(s_defaultContractType, new AMetadata("B"));
			Assert.IsTrue(mcd1.Equals(mcd2));
		}

		[TestMethod]
		public void ConstraintsWithEquivalentKeysAndValuesHaveTheSameHashCode()
		{
			var mcd1 = new CompositionContract(s_defaultContractType, new AMetadata("B"));
			var mcd2 = new CompositionContract(s_defaultContractType, new AMetadata("B"));
			Assert.AreEqual(mcd1.GetHashCode(), mcd2.GetHashCode());
		}

		[TestMethod]
		public void AContractWithConstraintIsNotEqualToAContractWithoutConstraint()
		{
			var first = new CompositionContract(typeof(string), new AMetadata("B"));
			var second = new CompositionContract(typeof(string));
			Assert.IsFalse(first.Equals(second));
		}
	}
}
