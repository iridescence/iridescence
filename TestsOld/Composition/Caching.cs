﻿using System.Collections.Generic;
using Iridescence.Composition;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Composition
{
	[TestClass]
	public class Caching
	{
		private struct TestStruct
		{
			public int NumProviders;
			public int NumDescriptors;
			public int NumActivations;
		}

		public class TestProvider : IExportDescriptorProvider
		{
			private int numProviders;
			private int numDescriptors;
			private int numActivations;

			public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
			{
				if (!contract.IsAssignableTo(typeof(TestStruct)))
					yield break;

				++this.numProviders;

				yield return new ExportDescriptorPromise(contract, "test", false, Dependency.NoDependencies, deps =>
				{
					++this.numDescriptors;
					return ExportDescriptor.Create((context, operation) =>
					{
						++this.numActivations;
						return new TestStruct
						{
							NumProviders = this.numProviders,
							NumDescriptors = this.numDescriptors,
							NumActivations = this.numActivations
						};
					});
				});
			}
		}

		[TestMethod]
		public void ProviderCalledOnce()
		{
			using (var c = new ContainerBuilder().WithProvider(new TestProvider()).Build())
			{
				TestStruct s1 = c.GetExport<TestStruct>();
				TestStruct s2 = c.GetExport<TestStruct>();
				TestStruct s3 = c.GetExport<TestStruct>();
				TestStruct s4 = c.GetExport<TestStruct>("asdf");
				TestStruct s5 = c.GetExport<TestStruct>("asdf");

				Assert.AreEqual(1, s1.NumProviders);
				Assert.AreEqual(1, s2.NumProviders);
				Assert.AreEqual(1, s3.NumProviders);
				Assert.AreEqual(2, s4.NumProviders);
				Assert.AreEqual(2, s5.NumProviders);

				Assert.AreEqual(1, s1.NumDescriptors);
				Assert.AreEqual(1, s2.NumDescriptors);
				Assert.AreEqual(1, s3.NumDescriptors);
				Assert.AreEqual(2, s4.NumDescriptors);
				Assert.AreEqual(2, s5.NumDescriptors);

				Assert.AreEqual(1, s1.NumActivations);
				Assert.AreEqual(2, s2.NumActivations);
				Assert.AreEqual(3, s3.NumActivations);
				Assert.AreEqual(4, s4.NumActivations);
				Assert.AreEqual(5, s5.NumActivations);
			}
		}
	}
}
