﻿using System;
using Iridescence;
using Iridescence.Graphics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class FormatAccessorTests
	{
		#region Methods

		[TestMethod]
		public unsafe void WriterTest()
		{
			const int width = 4;
			const int height = 4;
			Format format = new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm,
				(FormatMappingChannel.Red, FormatMappingChannel.Red, FormatMappingChannel.Red, FormatMappingChannel.One),
				(FormatMappingChannel.Red, FormatMappingChannel.Red, FormatMappingChannel.Red, FormatMappingChannel.One));
			byte[] buffer = new byte[width * height * format.Size];

			fixed (byte* ptr = buffer)
			{
				Action<int, int, Color4> writer = format.CreateColorWriter<int>((IntPtr)ptr,
					new DataDimension(format.Size, DataDimensionFlags.CheckBounds, 0, width - 1),
					new DataDimension(format.Size * width, DataDimensionFlags.CheckBounds, 0, height - 1));

				writer(0, 0, new Color4(0.1f, 0.0f, 0.0f, 0.0f));
				writer(1, 0, new Color4(0.0f, 0.1f, 0.0f, 0.0f));
				writer(2, 0, new Color4(0.0f, 0.0f, 0.1f, 0.0f));
				writer(3, 0, new Color4(0.0f, 0.0f, 0.0f, 0.1f));
			}
		}
		
		#endregion
	}
}
