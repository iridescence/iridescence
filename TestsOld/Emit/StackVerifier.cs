﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Emit
{
	[TestClass]
	public class StackVerifier
	{
		private static readonly OpCode[] opCodes = typeof(OpCodes).GetFields(BindingFlags.Public | BindingFlags.Static).Select(f => (OpCode)f.GetValue(null)).ToArray();

		[TestMethod]
		public void VerifyBcl()
		{
			IEnumerable<MethodBase> methods = AppDomain.CurrentDomain
				.GetAssemblies()
				.SelectMany(a => a.GetTypes())
				.SelectMany(t => t.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static).Cast<MethodBase>()
					.Concat(t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static)));

			foreach (MethodBase method in methods)
			{
				MethodBody body = method.GetMethodBody();
				if (body == null)
					continue;

				StringBuilder str = new StringBuilder();

				try
				{
					byte[] il = body.GetILAsByteArray();
					using (MemoryStream stream = new MemoryStream(il))
					{
						for (;;)
						{
							long pos = stream.Position;
							int code = stream.ReadByte();

							if (code < 0)
								break;

							str.Append($"{pos:X4}: ");
						
							if (code == 0xFE)
							{
								int code2 = stream.ReadByte();
								if (code2 < 0)
									throw new EndOfStreamException();

								code = (code << 8) | code2;
							}

							OpCode op = opCodes.Single(o => o.Value == unchecked((short)code));
							str.Append(op);

							switch (op.OperandType)
							{
								case OperandType.InlineBrTarget:
									str.Append(" ");
									str.Append($"{stream.Position + BitConverter.ToInt32(read(stream, 4), 0):X4}");
									break;

								case OperandType.InlineField:
									str.Append(" ");
									str.Append(BitConverter.ToInt32(read(stream, 4), 0));
									break;

								case OperandType.InlineI:
									str.Append(" ");
									str.Append(BitConverter.ToInt32(read(stream, 4), 0));
									break;

								case OperandType.InlineI8:
									str.Append(" ");
									str.Append(BitConverter.ToInt64(read(stream, 8), 0));
									break;

								case OperandType.InlineMethod:
									str.Append(" ");
									str.Append(BitConverter.ToInt32(read(stream, 4), 0));
									break;

								case OperandType.InlineNone:
									break;

								case OperandType.InlineR:
									str.Append(" ");
									str.Append(BitConverter.ToDouble(read(stream, 8), 0));
									break;

								case OperandType.InlineSig:
									str.Append(" ");
									str.Append(BitConverter.ToInt32(read(stream, 4), 0));
									break;

								case OperandType.InlineString:
									str.Append(" ");
									str.Append(BitConverter.ToInt32(read(stream, 4), 0));
									break;

								case OperandType.InlineSwitch:
									int numLabels = BitConverter.ToInt32(read(stream, 4), 0);
									for (int i = 0; i < numLabels; ++i)
									{
										int switchCase = BitConverter.ToInt32(read(stream, 4), 0);
										str.Append(" ");
										str.Append(switchCase);
									}
									break;

								case OperandType.InlineTok:
									str.Append(" ");
									str.Append(BitConverter.ToInt32(read(stream, 4), 0));
									break;

								case OperandType.InlineType:
									str.Append(" ");
									str.Append(BitConverter.ToInt32(read(stream, 4), 0));
									break;

								case OperandType.InlineVar:
									str.Append(" ");
									str.Append(BitConverter.ToInt16(read(stream, 2), 0));
									break;

								case OperandType.ShortInlineBrTarget:
									str.Append(" ");
									byte target = checked((byte)stream.ReadByte());
									str.Append($"{(stream.Position + unchecked((sbyte)target)):X4}");
									break;

								case OperandType.ShortInlineI:
									str.Append(" ");
									str.Append($"{checked((byte)stream.ReadByte())}");
									break;

								case OperandType.ShortInlineR:
									str.Append(" ");
									str.Append(BitConverter.ToSingle(read(stream, 4), 0));
									break;

								case OperandType.ShortInlineVar:
									str.Append(" ");
									str.Append(stream.ReadByte());
									break;
							}

							str.AppendLine();
						}
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(method.DeclaringType?.FullName);
					Console.WriteLine(method.Name);
					Console.WriteLine(str);
					throw ex;
				}
			}
		}

		static byte[] read(Stream stream, int count)
		{
			byte[] b = new byte[count];
			int offset = 0;

			while (offset < count)
			{
				int n = stream.Read(b, offset, count - offset);
				if (n == 0)
					throw new EndOfStreamException();
				offset += n;
			}
			return b;
		}
	}
}
