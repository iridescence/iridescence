﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;
using Iridescence;
using Iridescence.Networking;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class SerializationCompiler
	{
		private static readonly StreamingContext streamingContext = new StreamingContext();

		private static readonly MethodBuilderConfiguration configStream = new MethodBuilderConfiguration()
				.WithArbiter(new SerializableAttributeArbiter())
				.WithContentModule(new SerializationMethodContentModule(streamingContext))
				.WithContentModule(new DeserializationCallbackContentModule())
				.WithContentModule(new PrimitiveContentModule())
				.WithContentModule(new StringContentModule())
				.WithContentModule(new NullableContentModule())
				.WithContentModule(new ArrayContentModule())
				.WithContentModule(new BlitContentModule())
				.WithContentModule(new ISerializableModule(streamingContext, new FormatterConverter()))
				.WithContentModule(new FieldContentModule())
				.WithMemberModule(new ValueTypeMemberModule())
				.WithMemberModule(new ReferencingMemberModule(typeof(int)))
			//.WithContentModule(new DebugContentModule())
			//.WithMemberModule(new DebugMemberModule())
			;

		private static readonly MethodBuilderConfiguration configBufferedStream = new MethodBuilderConfiguration()
				.WithArbiter(new SerializableAttributeArbiter())
				.WithStream(new BufferedStreamEmitFactory())
				.WithContentModule(new SerializationMethodContentModule(streamingContext))
				.WithContentModule(new DeserializationCallbackContentModule())
				.WithContentModule(new PrimitiveContentModule())
				.WithContentModule(new StringContentModule())
				.WithContentModule(new NullableContentModule())
				.WithContentModule(new ArrayContentModule())
				.WithContentModule(new BlitContentModule())
				.WithContentModule(new DelegateContentModule())
				.WithContentModule(new MetadataContentModule())
				.WithContentModule(new ISerializableModule(streamingContext, new FormatterConverter()))
				.WithContentModule(new FieldContentModule())
				.WithMemberModule(new ValueTypeMemberModule())
				.WithMemberModule(new ReferencingMemberModule(typeof(int)))
			//.WithContentModule(new DebugContentModule())
			//.WithMemberModule(new DebugMemberModule())
			;

		private static bool memberwiseEquals(object left, object right)
		{
			if (ReferenceEquals(left, right))
				return true;

			if (ReferenceEquals(left, null))
				return false;

			if (ReferenceEquals(right, null))
				return false;

			Type type = left.GetType();
			if (type != right.GetType())
				return false;

			if (type != typeof(object) && type == type.GetMethod("Equals", new [] {typeof(object)}).DeclaringType)
			{
				return left.Equals(right);
			}

			if (left is Delegate leftDel)
			{
				Delegate rightDel = (Delegate)right;

				Delegate[] a = leftDel.GetInvocationList();
				Delegate[] b = rightDel.GetInvocationList();

				if (a.Length != b.Length)
					return false;

				for (int i = 0; i < a.Length; ++i)
				{
					if (a[i].Method != b[i].Method)
						return false;

					if (!memberwiseEquals(a[i].Target, b[i].Target))
						return false;
				}

				return true;
			}

			if (left is IEnumerable lEnum)
			{
				IEnumerator rEnum = ((IEnumerable)right).GetEnumerator();
				foreach (object leftItem in lEnum)
				{
					if (!rEnum.MoveNext())
						return false;

					if (!memberwiseEquals(leftItem, rEnum.Current))
						return false;
				}
			}
			else
			{
				foreach (PropertyInfo info in type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
				{
					if (!memberwiseEquals(info.GetValue(left, null), info.GetValue(right, null)))
						return false;
				}

				foreach (FieldInfo info in type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
				{
					if (!memberwiseEquals(info.GetValue(left), info.GetValue(right)))
						return false;
				}
			}

			return true;
		}

		private static T testStream<T, TOutStream, TInStream>(T value, bool compare, bool testNonGeneric, MethodBuilderConfiguration config, Func<Stream, TOutStream> outStreamFactory, Func<Stream, TInStream> inStreamFactory, Action<TInStream> doCallbacks)
			where TOutStream : Stream
			where TInStream : Stream
		{
			WriterBuilder writerBuilder = new WriterBuilder(config, typeof(TOutStream));
			ReaderBuilder readerBuilder = new ReaderBuilder(config, typeof(TInStream));

			using (MemoryStream stream = new MemoryStream())
			//using (FileStream stream = new FileStream(@"Z:\SerTest.bin", FileMode.Create, FileAccess.ReadWrite))
			{
				ObjectWriter<TOutStream, object> writerObj = writerBuilder.GetMemberWriterNonGeneric<TOutStream>(typeof(T));
				ObjectReader<TInStream, object> readerObj = readerBuilder.GetMemberReaderDeferredNonGeneric<TInStream, object>(typeof(T)).UnDefer();

				using (TOutStream s = outStreamFactory(stream))
				{
					writerObj(s, value);
				}

				stream.Position = 0;

				object value2;
				using (TInStream s = inStreamFactory(stream))
				{
					readerObj(s, out value2);
					//Assert.AreEqual(0, s.Buffer.Length);
					doCallbacks(s);
				}

				Assert.AreEqual(stream.Position, stream.Length, "Stream not completely read.");
				
				if (compare)
				{
					Assert.IsTrue(memberwiseEquals(value, value2), "Original and deserialized objects are not equal.");	
				}
				
				if (!testNonGeneric)
					return (T)value2;
			}

			using (MemoryStream stream = new MemoryStream())
			{
				ObjectWriterRef<TOutStream,T> writer = writerBuilder.GetMemberWriter<TOutStream,T>();
				ObjectReader<TInStream,T> reader = readerBuilder.GetMemberReaderDeferred<TInStream,T, object>().UnDefer();

				using (TOutStream s = outStreamFactory(stream))
				{
					writer(s, ref value);
				}

				stream.Position = 0;

				T value2;
				using (TInStream s = inStreamFactory(stream))
				{
					reader(s, out value2);
					//Assert.AreEqual(0, s.Buffer.Length);
					doCallbacks(s);
				}
				
				Assert.AreEqual(stream.Position, stream.Length, "Stream not completely read.");

				if (compare)
				{
					Assert.IsTrue(memberwiseEquals(value, value2), "Original and deserialized objects are not equal.");
				}

				return value2;
			}
		}

		private static IEnumerable<T> test<T>(T value, bool compare = true, bool testNonGeneric = true)
		{
			yield return testStream(value, compare, testNonGeneric, configStream, s => new StreamWrapper(s), s => new StreamWrapper(s), s => s.DoCallbacks());
			yield return testStream(value, compare, testNonGeneric, configBufferedStream, s => new TestOutputStream(s, 1, true), s => new TestInputStream(s, 1, true), s => s.DoCallbacks());
		}

		private static void testAll<T>(T value)
		{
			foreach (T _ in test(value))
			{
				
			}
		}

		private static void testArray<T>(T element)
		{
			testAll<T[]>(null);
			testAll<T[]>(new T[0]);
			testAll<T[]>(new T[1]);
			testAll<T[]>(new T[] { element });
			testAll<T[]>(new T[42]);

			T[] a = new T[42];
			for (int i = 0; i < a.Length; ++i)
				a[i] = element;
			testAll<T[]>(a);
		}

		private static void testNullable<T>(T value) where T : struct
		{
			testAll<T?>(null);
			testAll<T?>(value);
		}

		[TestMethod]
		public void Null()
		{
			testAll<object>(null);
			testAll<EmptyClass>(null);
		}

		[TestMethod]
		public void Primitives()
		{
			testAll<sbyte>(1);
			testAll<byte>(2);

			testAll<short>(3);
			testAll<ushort>(4);

			testAll<int>(5);
			testAll<uint>(6);

			testAll<long>(7);
			testAll<ulong>(8);

			testAll<float>(9);
			testAll<double>(10);

			testAll<bool>(true);
			testAll<char>('A');

			testAll<IntPtr>(new IntPtr(-1337));
			testAll<UIntPtr>(new UIntPtr(1337));
		}

		[TestMethod]
		public unsafe void NullablePrimitives()
		{
			testNullable<sbyte>(1);
			testNullable<byte>(2);

			testNullable<short>(3);
			testNullable<ushort>(4);

			testNullable<int>(5);
			testNullable<uint>(6);

			testNullable<long>(7);
			testNullable<ulong>(8);

			testNullable<float>(9);
			testNullable<double>(10);

			testNullable<bool>(true);
			testNullable<char>('A');

			testNullable<IntPtr>(new IntPtr(-1337));
			testNullable<UIntPtr>(new UIntPtr(1337));
		}

		[TestMethod]
		public void String()
		{
			testAll<string>(null);
			testAll<string>("");
			testAll<string>("asdf");
			testAll<string>(string.Concat(Enumerable.Range(0, 65536).Select(i => Encoding.UTF32.GetString(BitConverter.GetBytes(i)))));
		}

		[TestMethod]
		public void EmptyClass()
		{
			testAll<EmptyClass>(new EmptyClass());
		}
		
		[TestMethod]
		public void Cyclic()
		{
			foreach (Cyclic c in test<Cyclic>(new Cyclic(), false))
			{
				Assert.AreSame(c, c.Self);	
			}

			foreach (Cyclic2 c2 in test<Cyclic2>(new Cyclic2(), false))
			{
				Assert.AreSame(c2, c2.Next.Next);
				Assert.AreSame(c2.Next, c2.Next.Next.Next);
			}
		}

		[TestMethod]
		public void Structs()
		{
			testAll(new SimpleStruct() {A = 1, B = 2, C = 3, D = true});
			testAll(new StructWithReference() {A = 1, B = 2, C = 3});
			testAll(new StructWithReference() {A = 1, B = 2, C = "Test"});
			testAll(new ComplexStruct1() {A = 1, B = 2, C = 3, D = {A = 4, B = 5, C = 6}});
			testAll(new ComplexStruct2() {A = 1, B = 2, C = '3', D = {A = 4, B = 5, C = "Test"}});
			testAll(new ComplexStruct3() {A = new IntPtr(1), B = {A = 2, B = 3, C = 4}, C = {A = 5, B = 6, C = '7', D = {A = 8, B = 9, C = "Test"}}, D = "ASDF"});
		}


		[TestMethod]
		public void PrimitiveArrays()
		{
			testArray<sbyte>(1);
			testArray<byte>(2);

			testArray<short>(3);
			testArray<ushort>(4);

			testArray<int>(5);
			testArray<uint>(6);

			testArray<long>(7);
			testArray<ulong>(8);

			testArray<float>(9);
			testArray<double>(10);

			testArray<bool>(true);
			testArray<char>('A');

			testArray<IntPtr>(new IntPtr(-1337));
			testArray<UIntPtr>(new UIntPtr(1337));
		}

		[TestMethod]
		public void ComplexArrays()
		{
			testArray<SimpleStruct>(new SimpleStruct {A = 1, B = 2, C = 3});
			testArray<ComplexStruct3?>(null);
			testArray<ComplexStruct3?>(new ComplexStruct3() {A = new IntPtr(1), B = {A = 2, B = 3, C = 4}, C = {A = 5, B = 6, C = '7', D = {A = 8, B = 9, C = "Test"}}, D = "ASDF"});

			testArray<string>("test");
			testArray<object>("test");
		}

		[TestMethod]
		public void Classes()
		{
			testAll(new ClassWithProperties() { A = "asdf", B = 1337, C = 42.0f });
			foreach (var c in test(new ClassWithNonSerialized() { A = "Test" }, false))
			{
				Assert.IsNull(c.A);
			}

			ClassWithCallbacks c1 = new ClassWithCallbacks();
			foreach (ClassWithCallbacks c2 in test(c1, false, false))
			{
				Assert.IsTrue(c1.OnSerializingCalled);
				Assert.IsTrue(c1.OnSerializedCalled);
				Assert.IsFalse(c1.OnDeserializingCalled);
				Assert.IsFalse(c1.OnDeserializedCalled);

				Assert.IsFalse(c2.OnSerializingCalled);
				Assert.IsFalse(c2.OnSerializedCalled);
				Assert.IsTrue(c2.OnDeserializingCalled);
				Assert.IsTrue(c2.OnDeserializedCalled);

				c1.Reset();
			}

			SerializationMember member = new SerializationMember(typeof(int));
			testAll(member);
		}

		static int testMethod(int a)
		{
			return 1;
		}

		static int testMethod(uint a)
		{
			return 2;
		}

		[TestMethod]
		public void Delegates()
		{
			{
				Delegate d1 = new Func<int, int>(testMethod);
				testAll<Delegate>(d1);
			}

			{
				Func<uint, int> d1 = testMethod;
				foreach (Func<uint, int> d2 in test(d1))
				{
					Assert.AreEqual(d1(1), d2(1));	
				}			
			}

			{
				ClassWithMethods c1 = new ClassWithMethods();
				Func<int> d1 = c1.Method;
				foreach (Func<int> d2 in test(d1))
				{
					Assert.AreEqual(d1(), d2());
				}
			}

			{
				ClassWithRedeclaredMethod c1 = new ClassWithRedeclaredMethod();
				Func<int> d1 = c1.Method;
				foreach (Func<int> d2 in test(d1))
				{
					Assert.AreEqual(d1(), d2());
				}
			}

			{
				MulticastTest mt = new MulticastTest();
				Func<int> d1 = mt.A;
				d1 += mt.B;
				foreach (Func<int> d2 in test(d1))
				{
					Assert.AreEqual(d1(), d2());
					MulticastTest mt2 = (MulticastTest)d2.Target;
					Assert.IsTrue(mt2.ACalled);
					Assert.IsTrue(mt2.BCalled);	
				}
			}
		}

		[TestMethod]
		public void BclClasses()
		{
			testAll(new List<object>() { null, 1, "2", new List<int>() { 1, 2, 3 }, new List<string>() { "a", "b", "c" } });
			testAll(new Dictionary<object, object>()
			{
				{1, "2"},
				{"3", 4.0f},
				{new object(), new Dictionary<int, int>() {{1, 2}, {3, 4}}},
			});

			testAll(new Dictionary<MemberInfo, PeerObjectIdentifier>()
			{
				{typeof(Cyclic).GetField("Self"), new PeerObjectIdentifier(Role.None, 1338)}
			});
			testAll(new List<PeerObjectIdentifier>() {new PeerObjectIdentifier(Role.None, 1337)});
		}

		[TestMethod]
		public void Metadata()
		{
			Assembly assembly = typeof(EmptyClass).Assembly;

			foreach (Type type in assembly.GetTypes())
			{
				testAll<Type>(type);

				foreach (MemberInfo member in type.GetMembers(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly))
					testAll<MemberInfo>(member);
			}
			
		}

		[TestMethod]
		public void SerializeAsValue()
		{
			{
				TestContainer t1 = new TestContainer();
				t1.A = new object();
				t1.B = t1.A;
				foreach (TestContainer t2 in test(t1))
				{
					Assert.IsTrue(ReferenceEquals(t2.A, t2.B));
				}
			}

			{
				TestContainerTreatAsValue t1 = new TestContainerTreatAsValue();
				t1.A = new object();
				t1.B = t1.A;
				foreach (TestContainerTreatAsValue t2 in test(t1))
				{
					Assert.IsFalse(ReferenceEquals(t2.A, t2.B));
				}
			}

			{
				TestContainerTreatAsValue2 t1 = new TestContainerTreatAsValue2();
				t1.A = new TreatAsValueTypeClass() {X = 42};
				t1.B = t1.A;
				foreach (TestContainerTreatAsValue2 t2 in test(t1))
				{
					Assert.IsFalse(ReferenceEquals(t2.A, t2.B));
				}
			}

			{
				TestContainerTreatTreeAsValue t1 = new TestContainerTreatTreeAsValue();
				t1.A = new TestContainer();
				t1.A.A = new object();
				t1.A.B = t1.A.A;
				foreach (TestContainerTreatTreeAsValue t2 in test(t1))
				{
					Assert.IsFalse(ReferenceEquals(t2.A.A, t2.A.B));
				}
			}
		}

		[TestMethod]
		public void Blit()
		{
			testAll(new BlittableStruct()
			{
				X = 1.0f,
				Y = 1.5f,
				Z = 2.0f,
			});
		}
	}

	[Serializable]
	class EmptyClass
	{
		
	}

	[Serializable]
	class Cyclic
	{
		public Cyclic Self;

		public Cyclic()
		{
			this.Self = this;
		}
	}

	[Serializable]
	class Cyclic2
	{
		public Cyclic2 Next;

		public Cyclic2()
		{
			this.Next = new Cyclic2(this);
		}

		public Cyclic2(Cyclic2 next)
		{
			this.Next = next;
		}
	}

	[Serializable]
	struct SimpleStruct
	{
		public byte A;
		public ushort B;
		public float C;
		public bool D;
	}

	[Serializable]
	struct StructWithReference
	{
		public sbyte A;
		public short B;
		public object C;
	}

	[Serializable]
	struct ComplexStruct1
	{
		public int A;
		public uint B;
		public double C;
		public SimpleStruct D;
	}

	[Serializable]
	struct ComplexStruct2
	{
		public long A;
		public ulong B;
		public char C;
		public StructWithReference D;
	}

	[Serializable]
	struct ComplexStruct3
	{
		public IntPtr A;
		public SimpleStruct B;
		public ComplexStruct2 C;
		public string D;
	}

	[Serializable]
	class ClassWithProperties
	{
		public string A { get; set; }
		public int B { get; set; }
		public float C { get; set; }
	}

	[Serializable]
	class ClassWithNonSerialized
	{
		[NonSerialized]
		public string A;
	}

	[Serializable]
	class ClassWithCallbacks
	{
		[NonSerialized] public bool OnSerializingCalled;
		[NonSerialized] public bool OnSerializedCalled;
		[NonSerialized] public bool OnDeserializingCalled;
		[NonSerialized] public bool OnDeserializedCalled;

		public void Reset()
		{
			this.OnSerializingCalled = false;
			this.OnSerializedCalled = false;
			this.OnDeserializingCalled = false;
			this.OnDeserializedCalled = false;
		}

		[OnSerializing]
		private void onSerializing(StreamingContext ctx)
		{
			Assert.IsFalse(this.OnSerializingCalled);
			Assert.IsFalse(this.OnSerializedCalled);
			this.OnSerializingCalled = true;
		}

		[OnSerialized]
		private void onSerialized(StreamingContext ctx)
		{
			Assert.IsTrue(this.OnSerializingCalled);
			Assert.IsFalse(this.OnSerializedCalled);
			this.OnSerializedCalled = true;
		}

		[OnDeserializing]
		private void onDeserializing(StreamingContext ctx)
		{
			Assert.IsFalse(this.OnDeserializingCalled);
			Assert.IsFalse(this.OnDeserializedCalled);
			this.OnDeserializingCalled = true;
		}

		[OnDeserialized]
		private void onDeserialized(StreamingContext ctx)
		{
			Assert.IsTrue(this.OnDeserializingCalled);
			Assert.IsFalse(this.OnDeserializedCalled);
			this.OnDeserializedCalled = true;
		}
	}

	[Serializable]
	class ClassWithMethods
	{
		private int a = 1;

		public int Method()
		{
			return this.a;
		}
	}

	[Serializable]
	class ClassWithRedeclaredMethod : ClassWithMethods
	{
		private int a = 2;

		public new int Method()
		{
			return this.a;
		}
	}

	[Serializable]
	class MulticastTest
	{
		public bool ACalled;
		public bool BCalled;

		public int A()
		{
			this.ACalled = true;
			return 1;
		}

		public int B()
		{
			this.BCalled = true;
			return 2;
		}
	}

	[Serializable]
	class TestContainer
	{
		public object A;
		public object B;
	}

	[Serializable]
	class TestContainerTreatAsValue
	{
		[SerializeAsValue]
		public object A;
		public object B;
	}

	[Serializable]
	class TestContainerTreatTreeAsValue
	{
		[SerializeTreeAsValues]
		public TestContainer A;
	}

	[Serializable]
	class TestContainerTreatAsValue2
	{
		public TreatAsValueTypeClass A;
		public TreatAsValueTypeClass B;
	}

	[Serializable]
	[SerializeAsValue]
	class TreatAsValueTypeClass
	{
		public int X;
	}

	[StructLayout(LayoutKind.Sequential)]
	[Serializable]
	[SerializeByBlit]
	struct BlittableStruct
	{
		public float X;
		public float Y;
		public float Z;
	}

	class StreamWrapper : Stream, IReferencingOutputStream<int>, IReferencingInputStream<int>, IDeserializationCallbackCollector
	{
		private readonly IReferenceProvider<object, int> refProvider;
		private readonly IDeferredReferenceResolver<object, int> refResolver;
		private readonly List<IDeserializationCallback> callbacks;

		public Stream Inner { get; }
		
		public StreamWrapper(Stream inner)
		{
			this.Inner = inner;
			this.refProvider = new DictionaryReferenceProvider<object>();
			this.refResolver = new DeferredResolver<object, int>(new DictionaryReferenceResolver<object>());
			this.callbacks = new List<IDeserializationCallback>();
		}

		public override void Flush() => this.Inner.Flush();
		public override long Seek(long offset, SeekOrigin origin) => this.Inner.Seek(offset, origin);
		public override void SetLength(long value) => this.Inner.SetLength(value);
		public override int Read(byte[] buffer, int offset, int count) => this.Inner.Read(buffer, offset, count);
		public override void Write(byte[] buffer, int offset, int count) => this.Inner.Write(buffer, offset, count);
		public override bool CanRead => this.Inner.CanRead;
		public override bool CanSeek => this.Inner.CanSeek;
		public override bool CanWrite => this.Inner.CanWrite;
		public override long Length => this.Inner.Length;
		public override long Position
		{
			get => this.Inner.Position;
			set => this.Inner.Position = value;
		}

		public void Add(IDeserializationCallback obj)
		{
			this.callbacks.Add(obj);
		}

		public void DoCallbacks()
		{
			foreach (IDeserializationCallback cb in this.callbacks)
			{
				cb.OnDeserialization(this);
			}
		}

		public void WriteIdentifier(int id)
		{
			this.WriteInt32V(id);
		}

		public int ReadIdentifier()
		{
			return this.ReadInt32V();
		}

		public int GetOrAdd(object obj, out bool wasAdded)
		{
			return this.refProvider.GetOrAdd(obj, out wasAdded);
		}

		public DeferredReferencingResult Get<TState>(int identifier, DeferredCallback<object, TState> callback, TState state)
		{
			return this.refResolver.Get(identifier, callback, state);
		}

		public void Set(int identifier, object value)
		{
			this.refResolver.Set(identifier, value);
		}
	}

	class TestOutputStream : CompactOutputStream, IReferencingOutputStream<int>
	{
		private readonly IReferenceProvider<object, int> refProvider;

		public TestOutputStream(Stream stream, int initialCapacity, bool leaveOpen)
			: base(stream, initialCapacity, leaveOpen)
		{
			this.refProvider = new DictionaryReferenceProvider<object>();
		}

		public int GetOrAdd(object obj, out bool wasAdded)
		{
			return this.refProvider.GetOrAdd(obj, out wasAdded);
		}

		public void WriteIdentifier(int id)
		{
			this.WriteUInt8((byte)'I');
			this.WriteUInt8((byte)'D');
			this.WriteUInt8((byte)'<');
			this.WriteInt32(id);
			this.WriteUInt8((byte)'>');
		}
	}

	class TestInputStream : CompactInputStream, IReferencingInputStream<int>, IDeserializationCallbackCollector
	{
		private readonly IDeferredReferenceResolver<object, int> refResolver;

		public readonly List<IDeserializationCallback> Callbacks;

		public TestInputStream(Stream stream, int initialCapacity, bool leaveOpen)
			: base(stream, initialCapacity, leaveOpen, Assembly.Load)
		{
			this.refResolver = new DeferredResolver<object, int>(new DictionaryReferenceResolver<object>());
			this.Callbacks = new List<IDeserializationCallback>();
		}

		public DeferredReferencingResult Get<TState>(int identifier, DeferredCallback<object, TState> callback, TState state)
		{
			return this.refResolver.Get(identifier, callback, state);
		}

		public void Set(int identifier, object value)
		{
			this.refResolver.Set(identifier, value);
		}

		public int ReadIdentifier()
		{
			Assert.AreEqual(this.ReadUInt8(), (byte)'I');
			Assert.AreEqual(this.ReadUInt8(), (byte)'D');
			Assert.AreEqual(this.ReadUInt8(), (byte)'<');
			int id = this.ReadInt32();
			Assert.AreEqual(this.ReadUInt8(), (byte)'>');
			return id;
		}

		public void Add(IDeserializationCallback obj)
		{
			this.Callbacks.Add(obj);
		}

		public void DoCallbacks()
		{
			foreach (IDeserializationCallback cb in this.Callbacks)
			{
				cb.OnDeserialization(this);
			}
		}
	}

}
