﻿using System;
using System.Linq;
using Iridescence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class CircularBufferTests
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CircularBufferTests"/> class. 
		/// </summary>
		public CircularBufferTests()
		{

		}

		#endregion

		#region Methods

		[TestMethod]
		public void TestReadWrite()
		{
			int[] buf = new int[5];
			CircularBuffer<int> cbuf = new CircularBuffer<int>(buf);

			Span<int> repeat(int value, int count)
			{
				int[] temp = new int[count];
				for (int i = 0; i < temp.Length; ++i)
					temp[i] = value;

				return temp;
			}

			Assert.AreEqual(0, cbuf.Count);
			Assert.AreEqual(0, buf[0]);
			Assert.AreEqual(0, buf[1]);
			Assert.AreEqual(0, buf[2]);
			Assert.AreEqual(0, buf[3]);
			Assert.AreEqual(0, buf[4]);

			// Write 3 x 1
			Assert.AreEqual(3, cbuf.Write(repeat(1, 3)));
			Assert.AreEqual(3, cbuf.Count);
			Assert.AreEqual(1, buf[0]);
			Assert.AreEqual(1, buf[1]);
			Assert.AreEqual(1, buf[2]);
			Assert.AreEqual(0, buf[3]);
			Assert.AreEqual(0, buf[4]);

			// Write 3 x 2, only writes 2 values.
			Assert.AreEqual(2, cbuf.Write(repeat(2, 3)));
			Assert.AreEqual(5, cbuf.Count);
			Assert.AreEqual(1, buf[0]);
			Assert.AreEqual(1, buf[1]);
			Assert.AreEqual(1, buf[2]);
			Assert.AreEqual(2, buf[3]);
			Assert.AreEqual(2, buf[4]);

			// Write 3 x 3, writes nothing.
			Assert.AreEqual(0, cbuf.Write(repeat(3, 3)));
			Assert.AreEqual(5, cbuf.Count);
			Assert.AreEqual(1, buf[0]);
			Assert.AreEqual(1, buf[1]);
			Assert.AreEqual(1, buf[2]);
			Assert.AreEqual(2, buf[3]);
			Assert.AreEqual(2, buf[4]);

			// Overwrite 3 x 4.
			Assert.AreEqual(3, cbuf.Write(repeat(4, 3), true));
			Assert.AreEqual(5, cbuf.Count);
			Assert.AreEqual(4, buf[0]);
			Assert.AreEqual(4, buf[1]);
			Assert.AreEqual(4, buf[2]);
			Assert.AreEqual(2, buf[3]);
			Assert.AreEqual(2, buf[4]);

			// Overwrite 3 x 5 (wraps).
			Assert.AreEqual(3, cbuf.Write(repeat(5, 3), true));
			Assert.AreEqual(5, cbuf.Count);
			Assert.AreEqual(5, buf[0]);
			Assert.AreEqual(4, buf[1]);
			Assert.AreEqual(4, buf[2]);
			Assert.AreEqual(5, buf[3]);
			Assert.AreEqual(5, buf[4]);

			// Overwrite 1,2,3,...10, keeps only last 5 items.
			Assert.AreEqual(10, cbuf.Write(Enumerable.Range(1, 10).ToArray(), true));
			Assert.AreEqual(5, cbuf.Count);
			Assert.AreEqual(10, buf[0]);
			Assert.AreEqual(6, buf[1]);
			Assert.AreEqual(7, buf[2]);
			Assert.AreEqual(8, buf[3]);
			Assert.AreEqual(9, buf[4]);

			// Write 10,11,12,...19, overwrites nothing.
			Assert.AreEqual(0, cbuf.Write(Enumerable.Range(10, 10).ToArray()));
			Assert.AreEqual(5, cbuf.Count);
			Assert.AreEqual(10, buf[0]);
			Assert.AreEqual(6, buf[1]);
			Assert.AreEqual(7, buf[2]);
			Assert.AreEqual(8, buf[3]);
			Assert.AreEqual(9, buf[4]);

			// Read 3 items.
			int[] b = new int[10];
			Assert.AreEqual(3, cbuf.Read(new Span<int>(b, 0, 3)));
			Assert.AreEqual(2, cbuf.Count);
			Assert.AreEqual(6, b[0]);
			Assert.AreEqual(7, b[1]);
			Assert.AreEqual(8, b[2]);

			// Read 3 items, only reads 2.
			Assert.AreEqual(2, cbuf.Read(new Span<int>(b, 0, 3)));
			Assert.AreEqual(0, cbuf.Count);
			Assert.AreEqual(9, b[0]);
			Assert.AreEqual(10, b[1]);

			// Read 3 items, reads nothing.
			Assert.AreEqual(0, cbuf.Read(new Span<int>(b, 0, 3)));
			Assert.AreEqual(0, cbuf.Count);
			
			// Write 1,2,3,4,5.
			Assert.AreEqual(5, cbuf.Write(Enumerable.Range(1, 5).ToArray()));
			Assert.AreEqual(5, cbuf.Count);
			Assert.AreEqual(5, buf[0]);
			Assert.AreEqual(1, buf[1]);
			Assert.AreEqual(2, buf[2]);
			Assert.AreEqual(3, buf[3]);
			Assert.AreEqual(4, buf[4]);

			// Read 10 items, returns only 5.
			Assert.AreEqual(5, cbuf.Read(new Span<int>(b, 0, 10)));
			Assert.AreEqual(0, cbuf.Count);
			Assert.AreEqual(1, b[0]);
			Assert.AreEqual(2, b[1]);
			Assert.AreEqual(3, b[2]);
			Assert.AreEqual(4, b[3]);
			Assert.AreEqual(5, b[4]);
		}

		#endregion
	}
}
