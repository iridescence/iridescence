﻿using System;
using System.IO;
using System.IO.Compression;
using Iridescence.FileSystem;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Directory = Iridescence.FileSystem.Directory;
using File = Iridescence.FileSystem.File;
using Path = Iridescence.FileSystem.Path;

namespace Tests
{
	[TestClass]
	public class FileSystemTest
	{
		[TestMethod]
		public void TestSystemDirectory()
		{
			System.IO.Directory.CreateDirectory("FSTEST");
			System.IO.Directory.CreateDirectory("FSTEST/A");
			System.IO.Directory.CreateDirectory("FSTEST/A/1");
			System.IO.Directory.CreateDirectory("FSTEST/B");
			System.IO.File.WriteAllText("FSTEST/a.txt", "test");
			System.IO.File.WriteAllText("FSTEST/b.txt", "test");
			System.IO.File.WriteAllText("FSTEST/c.txt", "test");

			SystemDirectory dir = new SystemDirectory(null, "", System.IO.Directory.GetCurrentDirectory());
			DumpDirectory(dir);

			Assert.IsTrue(dir.GetDirectory("FSTEST", out Directory dir2));
			Assert.IsNotNull(dir2);
			Assert.IsTrue(dir2.Exists);

			Assert.IsFalse(dir.GetFile("a.txt", out File file));
			Assert.IsNotNull(file);
			Assert.IsFalse(file.Exists);

			Assert.IsTrue(dir.GetFile("FSTEST/a.txt", out file));
			Assert.IsNotNull(file);
			Assert.IsTrue(file.Exists);

			Assert.IsTrue(dir2.GetFile("a.txt", out file));
			Assert.IsNotNull(file);
			Assert.IsTrue(file.Exists);

			using (Stream stream = file.Open(FileMode.Open, FileAccess.Read))
			using (TextReader reader = new StreamReader(stream))
			{
				Assert.AreEqual(reader.ReadLine(), "test");
			}

			//file = dir.GetFile("FSTEST", "a.txt");
			//Assert.IsNotNull(file);
			//Assert.IsTrue(file.Exists);
		}

		[TestMethod]
		public void FilePathParse()
		{
			Path path;
			try
			{
				path = Path.Parse(null);
				Assert.Fail();
			}
			catch (ArgumentNullException)
			{
				
			}

			path = Path.Parse("");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(0, path.Segments.Count);

			path = Path.Parse("/");
			Assert.IsTrue(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(0, path.Segments.Count);

			path = Path.Parse(".");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(0, path.Segments.Count);

			path = Path.Parse("/.");
			Assert.IsTrue(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(0, path.Segments.Count);

			path = Path.Parse("./././.");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(0, path.Segments.Count);

			path = Path.Parse("/./././.");
			Assert.IsTrue(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(0, path.Segments.Count);
			
			path = Path.Parse("a");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(1, path.Segments.Count);
			Assert.AreEqual("a", path.Segments[0]);
			
			path = Path.Parse("/a");
			Assert.IsTrue(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(1, path.Segments.Count);
			Assert.AreEqual("a", path.Segments[0]);

			path = Path.Parse("a/b");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(2, path.Segments.Count);
			Assert.AreEqual("a", path.Segments[0]);
			Assert.AreEqual("b", path.Segments[1]);
		
			path = Path.Parse("/a/b");
			Assert.IsTrue(path.IsAbsolute);
			Assert.AreEqual(0, path.UpSegments);
			Assert.AreEqual(2, path.Segments.Count);
			Assert.AreEqual("a", path.Segments[0]);
			Assert.AreEqual("b", path.Segments[1]);

			path = Path.Parse("..");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(1, path.UpSegments);
			Assert.AreEqual(0, path.Segments.Count);

			try
			{
				path = Path.Parse("/..");
				Assert.Fail();
			}
			catch (ArgumentException)
			{
				
			}

			path = Path.Parse("../a");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(1, path.UpSegments);
			Assert.AreEqual(1, path.Segments.Count);
			Assert.AreEqual("a", path.Segments[0]);

			path = Path.Parse("../../a");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(2, path.UpSegments);
			Assert.AreEqual(1, path.Segments.Count);
			Assert.AreEqual("a", path.Segments[0]);

			path = Path.Parse("../a/b");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(1, path.UpSegments);
			Assert.AreEqual(2, path.Segments.Count);
			Assert.AreEqual("a", path.Segments[0]);
			Assert.AreEqual("b", path.Segments[1]);
			
			path = Path.Parse("../a/../b");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(1, path.UpSegments);
			Assert.AreEqual(1, path.Segments.Count);
			Assert.AreEqual("b", path.Segments[0]);
			
			path = Path.Parse("../a/../b/..");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(1, path.UpSegments);
			Assert.AreEqual(0, path.Segments.Count);

			path = Path.Parse("../a/../b/../..");
			Assert.IsFalse(path.IsAbsolute);
			Assert.AreEqual(2, path.UpSegments);
			Assert.AreEqual(0, path.Segments.Count);
		}

		[TestMethod]
		public void MountTest()
		{
			if (System.IO.Directory.Exists("A"))
				System.IO.Directory.Delete("A", true);
			if (System.IO.Directory.Exists("B"))
				System.IO.Directory.Delete("B", true);

			System.IO.Directory.CreateDirectory("A");
			System.IO.Directory.CreateDirectory("A/1");
			System.IO.Directory.CreateDirectory("A/2/3");
			System.IO.Directory.CreateDirectory("A/4");

			System.IO.Directory.CreateDirectory("B");
			System.IO.Directory.CreateDirectory("B/1");
			System.IO.Directory.CreateDirectory("B/2");
			
			System.IO.File.WriteAllText("A/a.txt", "test");
			System.IO.File.WriteAllText("A/1/b.txt", "test");
			System.IO.File.WriteAllText("A/2/3/c.txt", "test");
			System.IO.File.WriteAllText("A/4/d.txt", "test");

			System.IO.File.WriteAllText("B/e.txt", "test");
			System.IO.File.WriteAllText("B/1/f.txt", "test");
			System.IO.File.WriteAllText("B/2/g.txt", "test");

			SystemDirectory a = new SystemDirectory(null, "A", System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "A"));
			SystemDirectory b = new SystemDirectory(null, "B", System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "B"));

			Assert.IsTrue(a.Exists);
			Assert.IsTrue(b.Exists);

			FileSystem fs = new FileSystem();

			Assert.IsFalse(fs.Root.Exists);

			fs.Mount(Path.Root, a);
			fs.Mount(Path.Absolute("Mnt"), b);

			Assert.IsTrue(fs.Root.Exists);

			DumpDirectory(fs.Root);
		}

		[TestMethod]
		[DeploymentItem("Resources/test.zip")]
		public void ZipTest()
		{
			using (FileStream fs = new FileStream("test.zip", FileMode.Open, FileAccess.Read))
			using (ZipArchive archive = new ZipArchive(fs, ZipArchiveMode.Read))
			{
				Directory root = archive.AsDirectory();
				DumpDirectory(root);
			}
		}

		public static void DumpDirectory(Directory dir)
		{
			string indent = new string(' ', dir.Path.Segments.Count * 3);

			foreach (Directory subDir in dir.Directories)
			{
				Console.WriteLine($"{indent}{subDir.Name}/");
				DumpDirectory(subDir);
			}

			foreach (File file in dir.Files)
			{
				Console.WriteLine($"{indent}{file.Name}");
			}
		}
	}
}

