﻿using System;
using System.Diagnostics;
using Iridescence.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class MathPerfTest
	{
		private const int numTests = 1_000_000;

		private static void testInline()
		{
			Matrix4x4 a = Matrix4x4.Identity;
			Matrix4x4 b = Matrix4x4.One;
			Matrix4x4 c = Matrix4x4.Zero;
			for (int i = 0; i < numTests; ++i)
			{
				c.M11 = a.M11 + b.M11;
				c.M12 = a.M12 + b.M12;
				c.M13 = a.M13 + b.M13;
				c.M14 = a.M14 + b.M14;
				c.M21 = a.M21 + b.M21;
				c.M22 = a.M22 + b.M22;
				c.M23 = a.M23 + b.M23;
				c.M24 = a.M24 + b.M24;
				c.M31 = a.M31 + b.M31;
				c.M32 = a.M32 + b.M32;
				c.M33 = a.M33 + b.M33;
				c.M34 = a.M34 + b.M34;
				c.M41 = a.M41 + b.M41;
				c.M42 = a.M42 + b.M42;
				c.M43 = a.M43 + b.M43;
				c.M44 = a.M44 + b.M44;
				b.M11 = c.M11 * a.M11 + c.M12 * a.M21 + c.M13 * a.M31 + c.M14 * a.M41;
				b.M12 = c.M11 * a.M12 + c.M12 * a.M22 + c.M13 * a.M32 + c.M14 * a.M42;
				b.M13 = c.M11 * a.M13 + c.M12 * a.M23 + c.M13 * a.M33 + c.M14 * a.M43;
				b.M14 = c.M11 * a.M14 + c.M12 * a.M24 + c.M13 * a.M34 + c.M14 * a.M44;
				b.M21 = c.M21 * a.M11 + c.M22 * a.M21 + c.M23 * a.M31 + c.M24 * a.M41;
				b.M22 = c.M21 * a.M12 + c.M22 * a.M22 + c.M23 * a.M32 + c.M24 * a.M42;
				b.M23 = c.M21 * a.M13 + c.M22 * a.M23 + c.M23 * a.M33 + c.M24 * a.M43;
				b.M24 = c.M21 * a.M14 + c.M22 * a.M24 + c.M23 * a.M34 + c.M24 * a.M44;
				b.M31 = c.M31 * a.M11 + c.M32 * a.M21 + c.M33 * a.M31 + c.M34 * a.M41;
				b.M32 = c.M31 * a.M12 + c.M32 * a.M22 + c.M33 * a.M32 + c.M34 * a.M42;
				b.M33 = c.M31 * a.M13 + c.M32 * a.M23 + c.M33 * a.M33 + c.M34 * a.M43;
				b.M34 = c.M31 * a.M14 + c.M32 * a.M24 + c.M33 * a.M34 + c.M34 * a.M44;
				b.M41 = c.M41 * a.M11 + c.M42 * a.M21 + c.M43 * a.M31 + c.M44 * a.M41;
				b.M42 = c.M41 * a.M12 + c.M42 * a.M22 + c.M43 * a.M32 + c.M44 * a.M42;
				b.M43 = c.M41 * a.M13 + c.M42 * a.M23 + c.M43 * a.M33 + c.M44 * a.M43;
				b.M44 = c.M41 * a.M14 + c.M42 * a.M24 + c.M43 * a.M34 + c.M44 * a.M44;
			}
		}

		private static void testMethod()
		{
			Matrix4x4 a = Matrix4x4.Identity;
			Matrix4x4 b = Matrix4x4.One;
			Matrix4x4 c = Matrix4x4.Zero;
			for (int i = 0; i < numTests; ++i)
			{
				Matrix4x4.Add(in a, in b, out c);
				Matrix4x4.Multiply(in c, in a, out b);
			}
		}

		private static void testOperator()
		{
			Matrix4x4 a = Matrix4x4.Identity;
			Matrix4x4 b = Matrix4x4.One;
			Matrix4x4 c = Matrix4x4.Zero;
			for (int i = 0; i < numTests; ++i)
			{
				c = a + b;
				b = c * a;
			}
		}

		[TestMethod]
		public void PerfTest()
		{
			Console.WriteLine($"64 bit: {Environment.Is64BitProcess}");

			Stopwatch watch = new Stopwatch();
			for (int i = 0; i < 5; ++i)
			{
				watch.Restart();
				testInline();
				watch.Stop();
				Console.WriteLine($"Inline: {watch.Elapsed.TotalMilliseconds}ms");

				watch.Restart();
				testMethod();
				watch.Stop();
				Console.WriteLine($"Method: {watch.Elapsed.TotalMilliseconds}ms");

				watch.Restart();
				testOperator();
				watch.Stop();
				Console.WriteLine($"Operator: {watch.Elapsed.TotalMilliseconds}ms");
			}
		}
	}
}
