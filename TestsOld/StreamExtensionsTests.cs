﻿using System.IO;
using Iridescence;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class StreamExtensionsTests
	{
		[TestMethod]
		public void LE_I16()
		{
			byte[] data = new byte[4];

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteInt16L(0x1122);
				s.WriteInt16L(0x3344);

				Assert.AreEqual(4, s.Position);
				Assert.AreEqual(4, s.Length);
				Assert.AreEqual(0x22, data[0]);
				Assert.AreEqual(0x11, data[1]);
				Assert.AreEqual(0x44, data[2]);
				Assert.AreEqual(0x33, data[3]);

				s.Position = 0;

				Assert.AreEqual(0x1122, s.ReadInt16L());
				Assert.AreEqual(0x3344, s.ReadInt16L());
				Assert.AreEqual(4, s.Position);

				s.Position = 4;
				Assert.IsFalse(s.TryReadInt16L(out _));
				s.Position = 3;
				Assert.IsFalse(s.TryReadInt16L(out _));
				s.Position = 2;
				Assert.IsTrue(s.TryReadInt16L(out short v));
				Assert.AreEqual(0x3344, v);
			}
		}
	}
}
