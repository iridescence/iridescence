﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Iridescence;
using Iridescence.Graphics;
using Iridescence.Graphics.IO;
using Iridescence.Graphics.OpenGL;
using Iridescence.Graphics.OpenGL.WGL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Buffer = Iridescence.Graphics.OpenGL.Buffer;

namespace Tests
{
	[TestClass]
	public class DdsTest
	{
		#region Methods

		[TestMethod]
		[DeploymentItem("Resources/DDS", "Images")]
		public void Test()
		{
			List<(string name, Image[] mips)> images = new List<(string name, Image[] mips)>();

			foreach (string fileName in Directory.EnumerateFiles("Images", "*.dds"))
			{
				using (FileStream file = new FileStream(fileName, FileMode.Open, FileAccess.Read))
				{
					try
					{
						Image[,] img = Dds.Read(file);
						Console.WriteLine($"{fileName} succeeded: {img}");

						Image[] face = new Image[img.GetLength(1)];

						for (int i = 0; i < img.GetLength(0); ++i)
						{
							for (int j = 0; j < img.GetLength(1); ++j)
							{
								face[j] = img[i, j];
							}

							images.Add(($"{fileName}, face {i}", face));
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine($"{fileName} failed to load: {ex.Message}");
					}
				}
			}

			//displayImage(images.ToArray());
		}

		private static void displayImage((string name, Image[] mips)[] images)
		{
			WGLForm form = new WGLForm();

			int currentTexture = 0;
			int currentLod = 0;
			
			WGLContext wglContext;
			GLContext context = null;

			Texture2D texture = null;
			VertexArray vertexArray = null;
			ProgramState programState = null;

			void loadTexture()
			{
				form.Text = images[currentTexture].name;

				try
				{
					texture = new Texture2D(context, images[currentTexture].mips[0].Format, images[currentTexture].mips[0].Width, images[currentTexture].mips[0].Height, images[currentTexture].mips.Length);
					for (int i = 0; i < images[currentTexture].mips.Length; ++i)
					{
						images[currentTexture].mips[i].CopyTo(texture.GetImage(i));
					}
					texture.Parameters.MinFilter = TextureMinFilter.NearestMipmapNearest;
					texture.Parameters.MagFilter = TextureMagFilter.Nearest;
					texture.Parameters.WrapX = TextureWrapMode.Clamp;
					texture.Parameters.WrapY = TextureWrapMode.Clamp;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.ToString());
					texture = null;
				}
			}

			form.Load += (s, e) =>
			{
				wglContext = form.CreateContext(PixelFormatDescriptor.Default, 3, 3, ContextFlags.DebugBit, ProfileMask.CoreProfileBit);
				form.MakeCurrent(wglContext);
				context = new GLContext(wglContext.GetProcAddress, new SynchronizationContextScheduler(SynchronizationContext.Current), true);

				Buffer vertices = Buffer.Create(context, new float[]
				{
					-1.0f, -1.0f, 0.0f, 0.0f,
					-1.0f, +1.0f, 0.0f, 1.0f,
					+1.0f, -1.0f, 1.0f, 0.0f,
					+1.0f, +1.0f, 1.0f, 1.0f,
				});
				
				vertexArray = new VertexArray(context);
				vertexArray.Bind(0, vertices, 2, VertexAttribPointerType.Float, false, 16, 0);
				vertexArray.Bind(1, vertices, 2, VertexAttribPointerType.Float, false, 16, 8);

				VertexShader vertexShader = new VertexShader(context, "#version 330 core\n" +
				                                                      "layout(location=0) in vec2 Position;" +
				                                                      "layout(location=1) in vec2 TexCoord;" +
				                                                      "out vec2 tc;" +
				                                                      "void main(void)" +
				                                                      "{" +
				                                                      "	gl_Position = vec4(Position, 0.0, 1.0);" +
				                                                      "	tc = TexCoord;" +
				                                                      "}");
				
				FragmentShader fragmentShader = new FragmentShader(context, "#version 330 core\n" +
				                                                            "uniform sampler2D Tex;" +
				                                                            "uniform float Lod;" +
				                                                            "in vec2 tc;" +
				                                                            "out vec4 color;" +
				                                                            "void main(void)" +
				                                                            "{" +
				                                                            "	ivec2 c = ivec2(tc * 32.0);" +
				                                                            "	if(((c.x + c.y) & 1) == 0)" +
				                                                            "		color = vec4(0.33, 0.33, 0.33, 1.0);" +
				                                                            "	else" +
				                                                            "		color = vec4(0.67, 0.67, 0.67, 1.0);" +
				                                                            "	vec4 t = textureLod(Tex, tc, Lod);" +
				                                                            "	color = mix(color, t, t.a);" +
				                                                            "}");

				Program program = new Program(context);
				program.Attach(vertexShader);
				program.Attach(fragmentShader);
				program.Link();
				
				programState = new ProgramState(program);
				programState.Set("Tex", 0);
				
				loadTexture();
			};

			form.KeyDown += (s, e) =>
			{
				if (e.KeyCode == Keys.Left)
				{
					if (++currentTexture >= images.Length)
						currentTexture = 0;
					loadTexture();
					form.Invalidate();
				}
				else if (e.KeyCode == Keys.Right)
				{
					if (--currentTexture < 0)
						currentTexture = images.Length - 1;
					loadTexture();
					form.Invalidate();
				}
				else if (e.KeyCode == Keys.Up)
				{
					currentLod++;
					form.Invalidate();
				}
				else if (e.KeyCode == Keys.Down)
				{
					if (--currentLod < 0)
						currentLod = 0;
					form.Invalidate();
				}
			};

			form.Paint += (s, e) =>
			{
				programState.Set("Lod", (float)currentLod);
				using (context.State
					.WithViewport(0, 0, form.ClientSize.Width, form.ClientSize.Height)
					.WithTexture(0, texture))
				{
					programState.Draw(vertexArray, new DrawArraysInfo(PrimitiveType.TriangleStrip, 0, 4));
				}

				form.SwapBuffers();
			};

			Application.Run(form);
		}

		#endregion
	}
}
