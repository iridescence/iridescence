﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Iridescence;
using Iridescence.Math;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class MemoryDataDescriptorTest
	{
		private const int width = 8;
		private const int height = 8;
		private const int depth = 1024;

		[TestMethod]
		public unsafe void ReadBytes()
		{
			byte[] buffer = new byte[width * height * depth];

			Random rng = new Random();
			rng.NextBytes(buffer);

			fixed (byte* ptr = buffer)
			{
				MemoryDataDescriptor desc = new MemoryDataDescriptor((IntPtr)ptr, typeof(byte),
					new DataDimension(1, DataDimensionFlags.None, 0, width - 1),
					new DataDimension(width, DataDimensionFlags.None, 0, height - 1),
					new DataDimension(width * height, DataDimensionFlags.None, 0, depth - 1));

				MemoryReader<byte> reader1 = desc.GetReader<byte>();
				Func<int, int, int, byte> reader2 = desc.CompileReader<Func<int, int, int, byte>>();

				for (int z = 0; z < depth; ++z)
				{
					for (int y = 0; y < height; ++y)
					{
						for (int x = 0; x < width; ++x)
						{
							byte v1 = reader1(x, y, z);
							Assert.AreEqual(buffer[z * width * height + y * width + x], v1);
							byte v2 = reader2(x, y, z);
							Assert.AreEqual(v1, v2);
						}
					}
				}
			}
		}
		
		[TestMethod]
		public unsafe void WriteBytes()
		{
			byte[] buffer1 = new byte[width * height * depth];
			byte[] buffer2 = new byte[width * height * depth];

			Random rng = new Random();
			rng.NextBytes(buffer1);
			rng.NextBytes(buffer2);

			fixed (byte* ptr1 = buffer1)
			fixed (byte* ptr2 = buffer2)
			{
				MemoryDataDescriptor desc1 = new MemoryDataDescriptor((IntPtr)ptr1, typeof(byte),
					new DataDimension(1, DataDimensionFlags.None, 0, width - 1),
					new DataDimension(width, DataDimensionFlags.None, 0, height - 1),
					new DataDimension(width * height, DataDimensionFlags.None, 0, depth - 1));

				MemoryDataDescriptor desc2 = new MemoryDataDescriptor((IntPtr)ptr2, desc1);

				MemoryWriter<byte> writer1 = desc1.GetWriter<byte>();
				Action<int, int, int, byte> writer2 = desc2.CompileWriter<Action<int, int, int, byte>>();

				for (int z = 0; z < depth; ++z)
				{
					for (int y = 0; y < height; ++y)
					{
						for (int x = 0; x < width; ++x)
						{
							byte v = (byte)rng.Next(0, 256);

							writer1(v, x, y, z);
							writer2(x, y, z, v);
							
							Assert.AreEqual(buffer1[z * width * height + y * width + x], v);
							Assert.AreEqual(buffer2[z * width * height + y * width + x], v);
						}
					}
				}
			}
		}

		[TestMethod]
		public unsafe void ReadFloats()
		{
			float[] buffer = new float[width * height * depth];

			Random rng = new Random();
			for (int i = 0; i < buffer.Length; ++i)
				buffer[i] = (float)rng.NextDouble();

			fixed (float* ptr = buffer)
			{
				MemoryDataDescriptor desc = new MemoryDataDescriptor((IntPtr)ptr, typeof(float),
					new DataDimension(4, DataDimensionFlags.None, 0, width - 1),
					new DataDimension(4 * width, DataDimensionFlags.None, 0, height - 1),
					new DataDimension(4 * width * height, DataDimensionFlags.None, 0, depth - 1));

				MemoryReader<float> reader1 = desc.GetReader<float>();
				Func<int, int, int, float> reader2 = desc.CompileReader<Func<int, int, int, float>>();

				for (int z = 0; z < depth; ++z)
				{
					for (int y = 0; y < height; ++y)
					{
						for (int x = 0; x < width; ++x)
						{
							float v1 = reader1(x, y, z);
							Assert.AreEqual(buffer[z * width * height + y * width + x], v1);
							float v2 = reader2(x, y, z);
							Assert.AreEqual(v1, v2);
						}
					}
				}
			}
		}

		[TestMethod]
		public unsafe void WriteFloats()
		{
			float[] buffer1 = new float[width * height * depth];
			float[] buffer2 = new float[width * height * depth];

			Random rng = new Random();
			for (int i = 0; i < buffer1.Length; ++i)
				buffer1[i] = (float)rng.NextDouble();
			for (int i = 0; i < buffer2.Length; ++i)
				buffer2[i] = (float)rng.NextDouble();

			fixed (float* ptr1 = buffer1)
			fixed (float* ptr2 = buffer2)
			{
				MemoryDataDescriptor desc1 = new MemoryDataDescriptor((IntPtr)ptr1, typeof(float),
					new DataDimension(4, DataDimensionFlags.None, 0, width - 1),
					new DataDimension(4 * width, DataDimensionFlags.None, 0, height - 1),
					new DataDimension(4 * width * height, DataDimensionFlags.None, 0, depth - 1));

				MemoryDataDescriptor desc2 = new MemoryDataDescriptor((IntPtr)ptr2, desc1);

				MemoryWriter<float> writer1 = desc1.GetWriter<float>();
				Action<int, int, int, float> writer2 = desc2.CompileWriter<Action<int, int, int, float>>();

				for (int z = 0; z < depth; ++z)
				{
					for (int y = 0; y < height; ++y)
					{
						for (int x = 0; x < width; ++x)
						{
							float v = (float)rng.Next();

							writer1(v, x, y, z);
							writer2(x, y, z, v);
							
							Assert.AreEqual(buffer1[z * width * height + y * width + x], v);
							Assert.AreEqual(buffer2[z * width * height + y * width + x], v);
						}
					}
				}
			}
		}
		
		[TestMethod]
		public unsafe void ReadBytesAsInt()
		{
			byte[] buffer = new byte[width * height * depth];

			Random rng = new Random();
			rng.NextBytes(buffer);

			fixed (byte* ptr = buffer)
			{
				MemoryDataDescriptor desc = new MemoryDataDescriptor((IntPtr)ptr, typeof(byte),
					new DataDimension(1, DataDimensionFlags.None, 0, width - 1),
					new DataDimension(width, DataDimensionFlags.None, 0, height - 1),
					new DataDimension(width * height, DataDimensionFlags.None, 0, depth - 1));

				MemoryReader<int> reader = desc.GetReader<int>();
				Func<int, int, int, int> reader2 = desc.CompileReader<Func<int, int, int, int>>();

				for (int z = 0; z < depth; ++z)
				{
					for (int y = 0; y < height; ++y)
					{
						for (int x = 0; x < width; ++x)
						{
							int v1 = reader(x, y, z);
							Assert.AreEqual(buffer[z * width * height + y * width + x], v1);
							int v2 = reader2(x, y, z);
							Assert.AreEqual(v1, v2);
						}
					}
				}
			}
		}

		[TestMethod]
		public unsafe void CheckBounds()
		{
			byte[] buffer = new byte[width * height * depth];

			Random rng = new Random();
			rng.NextBytes(buffer);

			fixed (byte* ptr = buffer)
			{
				MemoryDataDescriptor desc = new MemoryDataDescriptor((IntPtr)ptr, typeof(byte),
					new DataDimension(1, DataDimensionFlags.CheckBounds, 0, width - 1),
					new DataDimension(width, DataDimensionFlags.CheckBounds, 0, height - 1),
					new DataDimension(width * height, DataDimensionFlags.CheckBounds, 0, depth - 1));

				MemoryReader<byte> reader = desc.GetReader<byte>();
				Func<int, int, int, byte> reader2 = desc.CompileReader<Func<int, int, int, byte>>();

				for (int z = 0; z < depth; ++z)
				{
					for (int y = 0; y < height; ++y)
					{
						for (int x = 0; x < width; ++x)
						{
							byte v = reader(x, y, z);
							Assert.AreEqual(buffer[z * width * height + y * width + x], v);
						}
					}
				}

				try
				{
					reader(-1, 0, 0);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader(width, 0, 0);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader(0, -1, 0);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader(0, height, 0);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader(0, 0, -1);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader(0, 0, depth);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader2(-1, 0, 0);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader2(width, 0, 0);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader2(0, -1, 0);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader2(0, height, 0);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader2(0, 0, -1);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}

				try
				{
					reader2(0, 0, depth);
					Assert.Fail("Access should have failed.");
				}
				catch (IndexOutOfRangeException)
				{
					
				}
			}
		}

		[TestMethod]
		public unsafe void ComplexType()
		{
			byte[] buffer = new byte[width * height * depth * sizeof(Vector4)];

			Random rng = new Random();
			rng.NextBytes(buffer);

			fixed (byte* ptr = buffer)
			{
				MemoryDataDescriptor desc = new MemoryDataDescriptor((IntPtr)ptr, typeof(Vector4),
					new DataDimension(sizeof(Vector4), DataDimensionFlags.CheckBounds, 0, width - 1),
					new DataDimension(sizeof(Vector4) * width, DataDimensionFlags.CheckBounds, 0, height - 1),
					new DataDimension(sizeof(Vector4) * width * height, DataDimensionFlags.CheckBounds, 0, depth - 1));

				MemoryReader<Vector4> reader = desc.GetReader<Vector4>();
				Func<int, int, int, Vector4> reader2 = desc.CompileReader<Func<int, int, int, Vector4>>();

				for (int z = 0; z < depth; ++z)
				{
					for (int y = 0; y < height; ++y)
					{
						for (int x = 0; x < width; ++x)
						{
							Vector4 v = reader(x, y, z);
							Vector4 v2 = reader2(x, y, z);
							
							Assert.AreEqual(v.ToString(), v2.ToString());
						}
					}
				}
			}
		}

		[TestMethod]
		public unsafe void NonInt32Indices()
		{
			byte[] buffer = new byte[width * height * depth];

			Random rng = new Random();
			rng.NextBytes(buffer);

			fixed (byte* ptr = buffer)
			{
				MemoryDataDescriptor desc = new MemoryDataDescriptor((IntPtr)ptr, typeof(byte),
					new DataDimension(1, DataDimensionFlags.None, 0, width - 1),
					new DataDimension(width, DataDimensionFlags.None, 0, height - 1),
					new DataDimension(width * height, DataDimensionFlags.None, 0, depth - 1));

				Func<sbyte, ushort, long, byte> reader = desc.CompileReader<Func<sbyte, ushort, long, byte>>();

				for (int z = 0; z < depth; ++z)
				{
					for (int y = 0; y < height; ++y)
					{
						for (int x = 0; x < width; ++x)
						{
							int v = reader((sbyte)x, (ushort)y, z);
							Assert.AreEqual(buffer[z * width * height + y * width + x], v);
						}
					}
				}
			}
		}


		private static unsafe bool testCast<TFrom, TTo>()
		{
			Console.Write($"{typeof(TFrom)} -> {typeof(TTo)}: ");

			byte[] data = new byte[Marshal.SizeOf<TFrom>()];
			
			fixed (byte* ptr = data)
			{
				MemoryDataDescriptor descriptor = new MemoryDataDescriptor((IntPtr)ptr, typeof(TFrom), new DataDimension(8, DataDimensionFlags.CheckBounds, 0, 0));
				bool referenceReaderWorked = true;

				try
				{
					MemoryReader<TTo> reader = descriptor.GetReader<TTo>();
					TTo value = reader(0);

					Console.Write("Pass ");
				}
				catch (Exception ex)
				{
					Console.Write($"Fail {ex}");
					referenceReaderWorked = false;
				}

				try
				{
					Func<int, TTo> readerFunc = (Func<int, TTo>)descriptor.CompileReader(typeof(Func<int, TTo>));
					TTo value = readerFunc(0);

					Console.WriteLine("Pass");
				}
				catch (Exception ex)
				{
					Console.WriteLine($"Fail {ex}");
					if (referenceReaderWorked)
						return false;
				}
			}

			return true;
		}

		private static bool testPrimitiveCasts<TFrom>()
		{
			return testCast<TFrom, bool>() &
				   testCast<TFrom, sbyte>() &
				   testCast<TFrom, byte>() &
				   testCast<TFrom, short>() &
				   testCast<TFrom, ushort>() &
				   testCast<TFrom, int>() &
				   testCast<TFrom, uint>() &
				   testCast<TFrom, long>() &
				   testCast<TFrom, ulong>() &
				   testCast<TFrom, char>() &
				   testCast<TFrom, float>() &
				   testCast<TFrom, double>();
		}

		[TestMethod]
		public unsafe void Casts()
		{
			Assert.IsTrue(testPrimitiveCasts<sbyte>() &
						  testPrimitiveCasts<byte>() &
						  testPrimitiveCasts<short>() &
						  testPrimitiveCasts<ushort>() &
						  testPrimitiveCasts<int>() &
						  testPrimitiveCasts<uint>() &
						  testPrimitiveCasts<long>() &
						  testPrimitiveCasts<ulong>() &
						  testPrimitiveCasts<char>() &
						  testPrimitiveCasts<float>() &
						  testPrimitiveCasts<double>() &
						  testPrimitiveCasts<SNorm8>() &
						  testPrimitiveCasts<UNorm8>() &
						  testPrimitiveCasts<SNorm16>() & 
						  testPrimitiveCasts<UNorm16>());
		}

	}
}
