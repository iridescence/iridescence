﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Iridescence.FileSystem;
using Iridescence.Graphics;
using Iridescence.Graphics.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
	[TestClass]
	public class PngTest
	{
		[TestMethod]
		[DeploymentItem("Resources/PngSuite.zip")]
		public void TestSuite()
		{
			using (var zipStream = new FileStream("PngSuite.zip", FileMode.Open, FileAccess.Read))
			using (var zip = new ZipArchive(zipStream, ZipArchiveMode.Read))
			{
				foreach (ZipArchiveEntry entry in zip.Entries.Where(e => e.Name.EndsWith(".png")))
				{
					Console.WriteLine(entry.Name);
					using (Stream stream = entry.Open())
					{
						if (entry.Name.StartsWith("x"))
						{
							bool exceptionThrown = false;
							try
							{
								Image _ = Png.Read(stream);
							}
							catch (Exception ex)
							{
								Console.WriteLine(ex.Message);
								exceptionThrown = true;
							}

							if (!exceptionThrown)
								Assert.Fail("Corrupted file should have thrown exception.");
						}
						else
						{
							Image _ = Png.Read(stream);
						}
					}
				}
			}
		}
	}
}
