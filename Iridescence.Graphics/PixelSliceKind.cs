﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines directions of <see cref="PixelSlice"/>.
	/// </summary>
	public enum PixelSliceKind
	{
		X = 1,
		Y,
		Z
	}
}
