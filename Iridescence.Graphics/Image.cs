﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents an abstract image.
	/// Images are three-dimensional containers for arbitrary pixel data.
	/// The pixel data format is defined by a <see cref="Format"/>.
	/// </summary>
	[Serializable]
	public abstract class Image : ReadOnlyImage
	{
		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new image with the specified format and dimensions.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="depth"></param>
		protected Image(Format format, int width, int height, int depth)
			: base(format, width, height, depth)
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets a sub-region of the current image.
		/// </summary>
		/// <param name="x">The X offset in this box.</param>
		/// <param name="y">The Y offset in this box.</param>
		/// <param name="z">The Z offset in this box.</param>
		/// <param name="width">The width of the sub-region.</param>
		/// <param name="height">The height of the sub-region.</param>
		/// <param name="depth">The depth of the sub-region.</param>
		/// <returns></returns>
		public virtual Image Slice(int x, int y, int z, int width, int height, int depth)
		{
			this.ThrowIfOutOfBounds(x, y, z, width, height, depth);
			return new ImageSubBox(this, x, y, z, width, height, depth);
		}

		/// <summary>
		/// Writes pixel data to a region of this image.
		/// </summary>
		/// <param name="source">The source pixel box to read from.</param>
		/// <param name="destX">The destination X coordinate.</param>
		/// <param name="destY">The destination Y coordinate.</param>
		/// <param name="destZ">The destination Z coordinate.</param>
		public abstract void Write(MemoryImage source, int destX, int destY, int destZ);

		/// <summary>
		/// Writes pixel data to this image.
		/// </summary>
		/// <param name="source">The source pixel box to read from.</param>
		public void Write(MemoryImage source)
		{
			this.Write(source, 0, 0, 0);
		}

		/// <summary>
		/// Asynchronously writes pixel data to a region of this image.
		/// </summary>
		/// <param name="source">The source image to read from.</param>
		/// <param name="destX">The destination X coordinate.</param>
		/// <param name="destY">The destination Y coordinate.</param>
		/// <param name="destZ">The destination Z coordinate.</param>
		/// <returns></returns>
		public virtual Task WriteAsync(MemoryImage source, int destX, int destY, int destZ)
		{
			this.Write(source, destX, destY, destZ);
			return Task.CompletedTask;
		}

		/// <summary>
		/// Asynchronously writes pixel data to this image.
		/// </summary>
		/// <param name="source">The source pixel box to read from.</param>
		/// <returns></returns>
		public Task WriteAsync(MemoryImage source)
		{
			return this.WriteAsync(source, 0, 0, 0);
		}

		#endregion
	}
}
