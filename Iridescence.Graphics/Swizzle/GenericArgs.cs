﻿using Iridescence.Math.Swizzle;

namespace Iridescence.Graphics.Swizzle
{
	/// <summary>
	/// When used in an output mapping, specifies that a channel on the struct actually returns the red channel.
	/// When used in an input mapping, specifies that the value for a channel is set when the red channel on the structure is assigned.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct R : IOutputSwizzleParameter1, IInputSwizzleParameter { }

	/// <summary>
	/// When used in an output mapping, specifies that a channel on the struct actually returns the green channel.
	/// When used in an input mapping, specifies that the value for a channel is set when the green channel on the structure is assigned.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct G : IOutputSwizzleParameter2, IInputSwizzleParameter { }

	/// <summary>
	/// When used in an output mapping, specifies that a channel on the struct actually returns the blue channel.
	/// When used in an input mapping, specifies that the value for a channel is set when the blue channel on the structure is assigned.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct B : IOutputSwizzleParameter3, IInputSwizzleParameter { }

	/// <summary>
	/// When used in an output mapping, specifies that a channel on the struct actually returns the alpha channel.
	/// When used in an input mapping, specifies that the value for a channel is set when the alpha channel on the structure is assigned.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct A : IOutputSwizzleParameter4, IInputSwizzleParameter { }
}

