﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Iridescence.Math;
using Iridescence.Math.Swizzle;

namespace Iridescence.Graphics.Swizzle
{
	/// <summary>
	/// Allows remapping of the components of an underlying color of type <typeparamref name="TColor"/> (with component type <typeparamref name="T"/>) using generic type arguments.
	/// </summary>
	/// <typeparam name="T">The component type.</typeparam>
	/// <typeparam name="TColor">The color type.</typeparam>
	/// <typeparam name="RIn">Specifies which property on the swizzle structure must be assigned so that the underyling 'R' property is assigned. Can be either <see cref="T:R"/>, <see cref="T:G"/>, <see cref="T:B"/>, <see cref="T:A"/>, or <see cref="T:None"/>.</typeparam>
	/// <typeparam name="GIn">Specifies which property on the swizzle structure must be assigned so that the underyling 'G' property is assigned. Can be either <see cref="T:R"/>, <see cref="T:G"/>, <see cref="T:B"/>, <see cref="T:A"/>, or <see cref="T:None"/>.</typeparam>
	/// <typeparam name="ROut">Specifies which component of the underlying color is returned when the 'R' property of the swizzle structure is retrieved. Can be either <see cref="T:R"/>, <see cref="T:G"/>, <see cref="T:Zero"/>, or <see cref="T:One"/>.</typeparam>
	/// <typeparam name="GOut">Specifies which component of the underlying color is returned when the 'G' property of the swizzle structure is retrieved. Can be either <see cref="T:R"/>, <see cref="T:G"/>, <see cref="T:Zero"/>, or <see cref="T:One"/>.</typeparam>
	/// <typeparam name="BOut">Specifies which component of the underlying color is returned when the 'B' property of the swizzle structure is retrieved. Can be either <see cref="T:R"/>, <see cref="T:G"/>, <see cref="T:Zero"/>, or <see cref="T:One"/>.</typeparam>
	/// <typeparam name="AOut">Specifies which component of the underlying color is returned when the 'A' property of the swizzle structure is retrieved. Can be either <see cref="T:R"/>, <see cref="T:G"/>, <see cref="T:Zero"/>, or <see cref="T:One"/>.</typeparam>
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	[ColorStruct(4)]
	public struct ColorSwizzle2To4<T, TColor, RIn, GIn, ROut, GOut, BOut, AOut> : IColor4<T>
		where TColor : struct, IColor2<T>
		where RIn : struct, IInputSwizzleParameter
		where GIn : struct, IInputSwizzleParameter
		where ROut : struct, IOutputSwizzleParameter2
		where GOut : struct, IOutputSwizzleParameter2
		where BOut : struct, IOutputSwizzleParameter2
		where AOut : struct, IOutputSwizzleParameter2
	{
		#region Fields

		/// <summary>
		/// The actual underlying color (without swizzling applied).
		/// </summary>
		public TColor Value;
		
		#endregion
		
		#region Properties
	
		/// <summary>
		/// Gets or sets the first swizzled component.
		/// This property returns the internal value as specified by <typeparamref name="ROut"/> and sets the internal values where the 'In' generic arguments are <see cref="T:R"/>.
		/// </summary>
		public T R
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if(typeof(ROut) == typeof(R)) return this.Value.R;
				if(typeof(ROut) == typeof(G)) return this.Value.G;
				if(typeof(ROut) == typeof(One)) return GenericConstants.One<T>.Value;
				if(typeof(ROut) == typeof(Zero)) return default;
				throw new InvalidOperationException();
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set
			{
				if(typeof(RIn) == typeof(R)) this.Value.R = value;
				if(typeof(GIn) == typeof(R)) this.Value.G = value;
			}
		}

		/// <summary>
		/// Gets or sets the second swizzled component.
		/// This property returns the internal value as specified by <typeparamref name="GOut"/> and sets the internal values where the 'In' generic arguments are <see cref="T:G"/>.
		/// </summary>
		public T G
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if(typeof(GOut) == typeof(R)) return this.Value.R;
				if(typeof(GOut) == typeof(G)) return this.Value.G;
				if(typeof(GOut) == typeof(One)) return GenericConstants.One<T>.Value;
				if(typeof(GOut) == typeof(Zero)) return default;
				throw new InvalidOperationException();
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set
			{
				if(typeof(RIn) == typeof(G)) this.Value.R = value;
				if(typeof(GIn) == typeof(G)) this.Value.G = value;
			}
		}

		/// <summary>
		/// Gets or sets the third swizzled component.
		/// This property returns the internal value as specified by <typeparamref name="BOut"/> and sets the internal values where the 'In' generic arguments are <see cref="T:B"/>.
		/// </summary>
		public T B
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if(typeof(BOut) == typeof(R)) return this.Value.R;
				if(typeof(BOut) == typeof(G)) return this.Value.G;
				if(typeof(BOut) == typeof(One)) return GenericConstants.One<T>.Value;
				if(typeof(BOut) == typeof(Zero)) return default;
				throw new InvalidOperationException();
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set
			{
				if(typeof(RIn) == typeof(B)) this.Value.R = value;
				if(typeof(GIn) == typeof(B)) this.Value.G = value;
			}
		}

		/// <summary>
		/// Gets or sets the fourth swizzled component.
		/// This property returns the internal value as specified by <typeparamref name="AOut"/> and sets the internal values where the 'In' generic arguments are <see cref="T:A"/>.
		/// </summary>
		public T A
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if(typeof(AOut) == typeof(R)) return this.Value.R;
				if(typeof(AOut) == typeof(G)) return this.Value.G;
				if(typeof(AOut) == typeof(One)) return GenericConstants.One<T>.Value;
				if(typeof(AOut) == typeof(Zero)) return default;
				throw new InvalidOperationException();
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set
			{
				if(typeof(RIn) == typeof(A)) this.Value.R = value;
				if(typeof(GIn) == typeof(A)) this.Value.G = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ColorSwizzle2To4{T, TColor, RIn, GIn, ROut, GOut, BOut, AOut}"/> structure with the specified underlying value.
		/// </summary>
		/// <param name="value">The underlying value. No swizzling is applied in the constructor.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public ColorSwizzle2To4(in TColor value)
		{
			this.Value = value;
		}
		
		#endregion
		
		#region Methods

		public override int GetHashCode()
		{
			int hash = this.R.GetHashCode();
			hash = Utility.CombineHash(hash, this.G.GetHashCode());
			hash = Utility.CombineHash(hash, this.B.GetHashCode());
			hash = Utility.CombineHash(hash, this.A.GetHashCode());
			return hash;
		}
		
		/// <summary>
		/// Casts the specified swizzled structure into a 4-component value tuple.
		/// </summary>
		/// <param name="swizzled"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator (T x, T y, T z, T w)(in ColorSwizzle2To4<T, TColor, RIn, GIn, ROut, GOut, BOut, AOut> swizzled)
		{
			return (swizzled.R, swizzled.G, swizzled.B, swizzled.A);
		}
		
		/// <summary>
		/// Casts the specified tuple into a swizzled struct with the swizzling operations applied.
		/// </summary>
		/// <param name="color"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator ColorSwizzle2To4<T, TColor, RIn, GIn, ROut, GOut, BOut, AOut>(in (T x, T y, T z, T w) color)
		{
			ColorSwizzle2To4<T, TColor, RIn, GIn, ROut, GOut, BOut, AOut> swizzled = default;
			swizzled.R = color.x;
			swizzled.G = color.y;
			swizzled.B = color.z;
			swizzled.A = color.w;
			return swizzled;
		}

		/// <summary>
		/// Casts a swizzled color into a <see cref="Color4{T}"/> with all swizzling operations applied.
		/// This is the same as creating a color of the underlying type with the properties from this structure assigned to its components.
		/// </summary>
		/// <param name="swizzled"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator Color4<T>(ColorSwizzle2To4<T, TColor, RIn, GIn, ROut, GOut, BOut, AOut> swizzled)
		{
			Color4<T> color;
			color.R = swizzled.R;
			color.G = swizzled.G;
			color.B = swizzled.B;
			color.A = swizzled.A;
			return color;
		}

		/// <summary>
		/// Casts a <see cref="Color4{T}"/> into the swizzled color with all swizzling operations applied.
		/// This is the same as setting the properties of the swizzled color to the components of the underlying color.
		/// </summary>
		/// <param name="color"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator ColorSwizzle2To4<T, TColor, RIn, GIn, ROut, GOut, BOut, AOut>(in Color4<T> color)
		{
			ColorSwizzle2To4<T, TColor, RIn, GIn, ROut, GOut, BOut, AOut> swizzled = default;
			swizzled.R = color.R;
			swizzled.G = color.G;
			swizzled.B = color.B;
			swizzled.A = color.A;
			return swizzled;
		}

		#endregion
	}
}
