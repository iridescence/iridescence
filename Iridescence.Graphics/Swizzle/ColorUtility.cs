﻿using System;
using System.ComponentModel;
using Iridescence.Graphics.Swizzle;
using Iridescence.Math.Swizzle;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Color class utility functionality.
	/// </summary>
	public static class ColorUtility
	{
		/// <summary>
		/// Returns the channel mapping generic type argument for the specified <see cref="ColorMappingChannel"/>.
		/// </summary>
		/// <param name="ch"></param>
		/// <returns></returns>
		public static Type GetGenericArgument(ColorMappingChannel ch)
		{
			switch (ch)
			{
				case ColorMappingChannel.None:
					return typeof(None);
				case ColorMappingChannel.Zero:
					return typeof(Zero);
				case ColorMappingChannel.One:
					return typeof(One);
				case ColorMappingChannel.Red:
					return typeof(R);
				case ColorMappingChannel.Green:
					return typeof(G);
				case ColorMappingChannel.Blue:
					return typeof(B);
				case ColorMappingChannel.Alpha:
					return typeof(A);
				default:
					throw new InvalidEnumArgumentException(nameof(ch), (int)ch, typeof(ColorMappingChannel));
			}
		}

		public static Type ApplySwizzle(int elementCount, Type elementType, Type colorType, ColorMapping input, ColorMapping output)
		{
			if (elementType == null)
				throw new ArgumentNullException(nameof(elementType));

			if (colorType == null)
				throw new ArgumentNullException(nameof(colorType));
			
			if (input.IsIdentity && output.IsIdentity)
				return colorType;

			switch (elementCount)
			{
				case 1:
					return typeof(ColorSwizzle1To4<,,,,,>).MakeGenericType(
						elementType,
						GetGenericArgument(input.Red),
						GetGenericArgument(output.Red),
						GetGenericArgument(output.Green),
						GetGenericArgument(output.Blue),
						GetGenericArgument(output.Alpha));
				case 2:
					return typeof(ColorSwizzle2To4<,,,,,,,>).MakeGenericType(
						elementType,
						colorType,
						GetGenericArgument(input.Red),
						GetGenericArgument(input.Green),
						GetGenericArgument(output.Red),
						GetGenericArgument(output.Green),
						GetGenericArgument(output.Blue),
						GetGenericArgument(output.Alpha));
				case 3:
					return typeof(ColorSwizzle3To4<,,,,,,,,>).MakeGenericType(
						elementType,
						colorType,
						GetGenericArgument(input.Red),
						GetGenericArgument(input.Green),
						GetGenericArgument(input.Blue),
						GetGenericArgument(output.Red),
						GetGenericArgument(output.Green),
						GetGenericArgument(output.Blue),
						GetGenericArgument(output.Alpha));
				case 4:
					return typeof(ColorSwizzle4<,,,,,,,,,>).MakeGenericType(
						elementType,
						colorType,
						GetGenericArgument(input.Red),
						GetGenericArgument(input.Green),
						GetGenericArgument(input.Blue),
						GetGenericArgument(input.Alpha),
						GetGenericArgument(output.Red),
						GetGenericArgument(output.Green),
						GetGenericArgument(output.Blue),
						GetGenericArgument(output.Alpha));
				default:
					throw new ArgumentOutOfRangeException(nameof(elementCount));
			}
		}

		/// <summary>
		/// Returns the constructed generic type that represents the specified color specification.
		/// </summary>
		/// <param name="elementCount"></param>
		/// <param name="elementType"></param>
		/// <param name="input"></param>
		/// <param name="output"></param>
		/// <returns></returns>
		public static Type GetColorType(int elementCount, Type elementType, ColorMapping input, ColorMapping output)
		{
			if (elementCount < 1 || elementCount > 4)
				throw new ArgumentOutOfRangeException(nameof(elementCount));

			if (elementType == null)
				throw new ArgumentNullException(nameof(elementType));

			Exception ex = output.GetOutputValidationException();
			if(ex != null)
				throw ex;
			
			ex = input.GetInputValidationException();
			if(ex != null)
				throw ex;

			Type colorType;
			switch (elementCount)
			{
				case 1:
					colorType = elementType;
					break;
				case 2:
					colorType = typeof(Color2<>).MakeGenericType(elementType);
					break;
				case 3:
					colorType = typeof(Color3<>).MakeGenericType(elementType);
					break;
				case 4:
					colorType = typeof(Color4<>).MakeGenericType(elementType);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(elementCount));
			}

			return ApplySwizzle(elementCount, elementType, colorType, input, output);
		}
	}
}
