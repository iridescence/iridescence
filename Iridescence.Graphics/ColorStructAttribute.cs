﻿using System;

namespace Iridescence.Graphics
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public sealed class ColorStructAttribute : Attribute
	{
		#region Fields

		#endregion

		#region Properties

		public int Channels { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ColorStructAttribute"/> class. 
		/// </summary>
		public ColorStructAttribute(int channels = 4)
		{
			this.Channels = channels;
		}

		#endregion

		#region Methods

		#endregion
	}
}
