﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Interface for generic RGBA tuples.
	/// </summary>
	public interface IColor4<T> : IColor3<T>
	{
		/// <summary>
		/// Gets or sets the alpha channel.
		/// </summary>
		T A { get; set; }
	}
}
