﻿using System;
using Iridescence.Math;
using Iridescence.Memory;

namespace Iridescence.Graphics
{
	internal static class PixelHelper
	{
		public static readonly StructMapConversion ColorIdentityMapping = new StructMapConversion(
			("R", "R"),
			("G", "G"),
			("B", "B"),
			("A", "A"));

		/// <summary>
		/// Fill the specified span with the first N bytes by copying repeatedly. Each consecutive copy copies twice as much data.
		/// </summary>
		/// <param name="span"></param>
		/// <param name="count"></param>
		public static void FillLogN<T>(Span<T> span, int count)
		{
			int offset = count;

			while (offset < span.Length)
			{
				int n = Utility.Min(span.Length - offset, count);
				span.Slice(0, n).CopyTo(span.Slice(offset, n));

				offset += n;
				count *= 2;
			}
		}
	}
}
