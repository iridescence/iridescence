﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Interface for generic RGB tuples.
	/// </summary>
	public interface IColor3<T> : IColor2<T>
	{
		/// <summary>
		/// Gets or sets the blue channel.
		/// </summary>
		T B { get; set; }
	}
}
