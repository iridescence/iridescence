﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a method that copies part of a pixel row from a source pointer to a destination pointer.
	/// </summary>
	/// <typeparam name="TSource"></typeparam>
	/// <typeparam name="TDest"></typeparam>
	/// <param name="source">The managed pointer to the source data.</param>
	/// <param name="dest">The managed pointer to the destination data.</param>
	/// <param name="sourceX">The source offset.</param>
	/// <param name="destX">The destination offset.</param>
	/// <param name="countX">The number of pixels to copy.</param>
	public delegate void RowCopy<TSource, TDest>(ref TSource source, ref TDest dest, int sourceX, int destX, int countX);

	/// <summary>
	/// Represents a method that copies part of a pixel slice from a source pointer to a destination pointer.
	/// </summary>
	/// <typeparam name="TSource"></typeparam>
	/// <typeparam name="TDest"></typeparam>
	/// <param name="source">The managed pointer to the source data.</param>
	/// <param name="dest">The managed pointer to the destination data.</param>
	/// <param name="sourceX">The source X offset.</param>
	/// <param name="sourceY">The source Y offset.</param>
	/// <param name="destX">The destination X offset.</param>
	/// <param name="destY">The destination Y offset.</param>
	/// <param name="countX">The number of pixels along the X axis to copy.</param>
	/// <param name="countY">The number of pixels along the Y axis to copy.</param>
	public delegate void SliceCopy<TSource, TDest>(ref TSource source, ref TDest dest, int sourceX, int sourceY, int destX, int destY, int countX, int countY);

	/// <summary>
	/// Represents a method that copies part of a pixel box from a source pointer to a destination pointer.
	/// </summary>
	/// <typeparam name="TSource"></typeparam>
	/// <typeparam name="TDest"></typeparam>
	/// <param name="source">The managed pointer to the source data.</param>
	/// <param name="dest">The managed pointer to the destination data.</param>
	/// <param name="sourceX">The source X offset.</param>
	/// <param name="sourceY">The source Y offset.</param>
	/// <param name="sourceZ">The source Z offset.</param>
	/// <param name="destX">The destination X offset.</param>
	/// <param name="destY">The destination Y offset.</param>
	/// <param name="destZ">The destination Z offset.</param>
	/// <param name="countX">The number of pixels along the X axis to copy.</param>
	/// <param name="countY">The number of pixels along the Y axis to copy.</param>
	/// <param name="countZ">The number of pixels along the Z axis to copy.</param>
	public delegate void BoxCopy<TSource, TDest>(ref TSource source, ref TDest dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int countX, int countY, int countZ);
}
