﻿using System;
using System.Runtime.InteropServices;
using Iridescence.Memory;

namespace Iridescence.Graphics
{
	[StructLayout(LayoutKind.Sequential)]
	[Serializable]
	[CastExtensions(typeof(Color3Cast))]
	[ColorStruct(3)]
	public struct Color3<T> : IColor3<T>
	{
		#region Fields
		
		public T R;
		public T G;
		public T B;

		#endregion
		
		#region Properties
		
		T IColor2<T>.R
		{
			get => this.R;
			set => this.R = value;
		}

		T IColor2<T>.G
		{
			get => this.G;
			set => this.G = value;
		}

		T IColor3<T>.B
		{
			get => this.B;
			set => this.B = value;
		}

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Color3"/> class. 
		/// </summary>
		public Color3(T r, T g, T b)
		{
			this.R = r;
			this.G = g;
			this.B = b;
		}

		#endregion
		
		#region Methods

		#endregion
	}

	internal static class Color3Cast
	{
		public static Color3<UNorm8> UNorm8To16(in Color3<UNorm16> v)
		{
			Color3<UNorm8> result;
			result.R = (UNorm8)v.R.SingleValue;
			result.G = (UNorm8)v.G.SingleValue;
			result.B = (UNorm8)v.B.SingleValue;
			return result;
		}

		public static Color4<UNorm8> AddAlpha(in Color3<UNorm16> v)
		{
			Color4<UNorm8> result;
			result.R = (UNorm8)v.R.SingleValue;
			result.G = (UNorm8)v.G.SingleValue;
			result.B = (UNorm8)v.B.SingleValue;
			result.A.Value = 255;
			return result;
		}
	}
}
