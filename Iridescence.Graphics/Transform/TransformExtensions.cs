﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// <see cref="Transform"/> extension methods.
	/// </summary>
	public static class TransformExtensions
	{
		#region Methods

		/// <summary>
		/// Returns a transform that represents the combined transform of the two specified inputs.
		/// </summary>
		/// <param name="first"></param>
		/// <param name="second"></param>
		/// <returns></returns>
		public static Transform Combine(this Transform first, Transform second)
		{
			if (first == null)
				return second;

			if (second == null)
				return first;

			TranslateTransform trans1 = first as TranslateTransform;
			TranslateTransform trans2 = second as TranslateTransform;
			if (trans1 != null && trans2 != null)
			{
				return new TranslateTransform(trans1.Translation + trans2.Translation);
			}

			ScaleTransform scale1 = first as ScaleTransform;
			ScaleTransform scale2 = second as ScaleTransform;
			if (scale1 != null && scale2 != null)
			{
				return new ScaleTransform(scale1.Scale * scale2.Scale);
			}

			return new CompoundTransform(first, second);
		}

		public static void ToMatrixCombined(this Transform first, Transform second, out TransformMatrix result)
		{
			if (first == null)
			{
				if (second == null)
				{
					result = TransformMatrix.Identity;
					
				}
				else
				{
					second.ToMatrix(out result);
				}
			}
			else if (second == null)
			{
				first.ToMatrix(out result);
			}
			else
			{
				TransformMatrix.Multiply(second.ToMatrix(), first.ToMatrix(), out result);
			}
		}

		public static void ToMatrixCombined(this Transform first, in TransformMatrix second, out TransformMatrix result)
		{
			if (first == null)
			{
				result = second;
			}
			else
			{
				first.ToMatrix(out result);
				TransformMatrix.Multiply(second, result, out result);
			}
		}

		#endregion
	}
}
