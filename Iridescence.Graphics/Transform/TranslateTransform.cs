﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a translation transform.
	/// </summary>
	public class TranslateTransform : Transform
	{
		#region Fields

		private readonly Vector2D translation;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the translation.
		/// </summary>
		public Vector2D Translation
		{
			get => this.translation;
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TranslateTransform"/> class. 
		/// </summary>
		public TranslateTransform(Vector2D translation)
		{
			this.translation = translation;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TranslateTransform"/> class. 
		/// </summary>
		public TranslateTransform(double x, double y)
			: this(new Vector2D(x, y))
		{

		}

		#endregion

		#region Methods

		public sealed override void TransformPoint(in Vector2D point, out Vector2D result)
		{
			Vector2D.Add(point, this.translation, out result);
		}

		public sealed override void ToMatrix(out TransformMatrix matrix)
		{
			matrix = TransformMatrix.Identity;
			matrix.Translation = this.translation;
		}

		public override Transform Clone()
		{
			return new TranslateTransform(this.Translation);
		}

		#endregion
	}
}
