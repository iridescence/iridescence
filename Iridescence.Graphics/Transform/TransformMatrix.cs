﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a transform matrix, consisting of a 2x2 matrix with translation vector.
	/// </summary>
	public struct TransformMatrix : IEquatable<TransformMatrix>
	{
		#region Fields

		public Matrix2x2D Matrix;
		public Vector2D Translation;

		public static readonly TransformMatrix Identity = new TransformMatrix(Matrix2x2D.Identity, Vector2.Zero);

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TransformMatrix"/> class. 
		/// </summary>
		public TransformMatrix(Matrix2x2D matrix, Vector2D translation)
		{
			this.Matrix = matrix;
			this.Translation = translation;
		}

		#endregion

		#region Methods

		#region Transform

		public static void Transform(in TransformMatrix left, in Vector2D right, out Vector2D result)
		{
			Vector2D temp;
			temp.X = left.Matrix.M11 * right.X + left.Matrix.M12 * right.Y + left.Translation.X;
			temp.Y = left.Matrix.M21 * right.X + left.Matrix.M22 * right.Y + left.Translation.Y;
			result = temp;
		}

		public static Vector2D Transform(in TransformMatrix left, in Vector2D right)
		{
			Vector2D result;
			result.X = left.Matrix.M11 * right.X + left.Matrix.M12 * right.Y + left.Translation.X;
			result.Y = left.Matrix.M21 * right.X + left.Matrix.M22 * right.Y + left.Translation.Y;
			return result;
		}

		#endregion

		#region Multiply

		public static void Multiply(in TransformMatrix left, in TransformMatrix right, out TransformMatrix result)
		{
			TransformMatrix temp;
			temp.Matrix.M11 = left.Matrix.M11 * right.Matrix.M11 + left.Matrix.M12 * right.Matrix.M21;
			temp.Matrix.M12 = left.Matrix.M11 * right.Matrix.M12 + left.Matrix.M12 * right.Matrix.M22;
			temp.Translation.X = left.Matrix.M11 * right.Translation.X + left.Matrix.M12 * right.Translation.Y + left.Translation.X;
			temp.Matrix.M21 = left.Matrix.M21 * right.Matrix.M11 + left.Matrix.M22 * right.Matrix.M21;
			temp.Matrix.M22 = left.Matrix.M21 * right.Matrix.M12 + left.Matrix.M22 * right.Matrix.M22;
			temp.Translation.Y = left.Matrix.M21 * right.Translation.X + left.Matrix.M22 * right.Translation.Y + left.Translation.Y;
			result = temp;
		}

		public static TransformMatrix Multiply(in TransformMatrix left, in TransformMatrix right)
		{
			TransformMatrix result;
			result.Matrix.M11 = left.Matrix.M11 * right.Matrix.M11 + left.Matrix.M12 * right.Matrix.M21;
			result.Matrix.M12 = left.Matrix.M11 * right.Matrix.M12 + left.Matrix.M12 * right.Matrix.M22;
			result.Translation.X = left.Matrix.M11 * right.Translation.X + left.Matrix.M12 * right.Translation.Y + left.Translation.X;
			result.Matrix.M21 = left.Matrix.M21 * right.Matrix.M11 + left.Matrix.M22 * right.Matrix.M21;
			result.Matrix.M22 = left.Matrix.M21 * right.Matrix.M12 + left.Matrix.M22 * right.Matrix.M22;
			result.Translation.Y = left.Matrix.M21 * right.Translation.X + left.Matrix.M22 * right.Translation.Y + left.Translation.Y;
			return result;
		}
		
		public static TransformMatrix operator *(in TransformMatrix left, in TransformMatrix right)
		{
			TransformMatrix result;
			result.Matrix.M11 = left.Matrix.M11 * right.Matrix.M11 + left.Matrix.M12 * right.Matrix.M21;
			result.Matrix.M12 = left.Matrix.M11 * right.Matrix.M12 + left.Matrix.M12 * right.Matrix.M22;
			result.Translation.X = left.Matrix.M11 * right.Translation.X + left.Matrix.M12 * right.Translation.Y + left.Translation.X;
			result.Matrix.M21 = left.Matrix.M21 * right.Matrix.M11 + left.Matrix.M22 * right.Matrix.M21;
			result.Matrix.M22 = left.Matrix.M21 * right.Matrix.M12 + left.Matrix.M22 * right.Matrix.M22;
			result.Translation.Y = left.Matrix.M21 * right.Translation.X + left.Matrix.M22 * right.Translation.Y + left.Translation.Y;
			return result;
		}

		#endregion

		public bool Equals(TransformMatrix other)
		{
			return this.Matrix.Equals(other.Matrix) && this.Translation.Equals(other.Translation);
		}

		public bool Equals(in TransformMatrix other)
		{
			return this.Matrix.Equals(other.Matrix) && this.Translation.Equals(other.Translation);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is TransformMatrix && this.Equals((TransformMatrix)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (this.Matrix.GetHashCode() * 397) ^ this.Translation.GetHashCode();
			}
		}

		public static bool operator ==(TransformMatrix left, TransformMatrix right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(TransformMatrix left, TransformMatrix right)
		{
			return !left.Equals(right);
		}

		#endregion
	}
}
