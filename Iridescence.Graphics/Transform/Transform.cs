﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents an abstract 2D transform.
	/// </summary>
	public abstract class Transform : ICloneable
	{
		#region Properties

		/// <summary>
		/// An identity transform that has no effect.
		/// </summary>
		public static readonly Transform Identity = new IdentityTransform();

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Transform"/> class. 
		/// </summary>
		protected Transform()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Applies this <see cref="Transform"/> to a point.
		/// </summary>
		/// <param name="point">The input point.</param>
		/// <param name="result">The point after this <see cref="Transform"/> has been applied.</param>
		public abstract void TransformPoint(in Vector2D point, out Vector2D result);

		/// <summary>
		/// Applies this <see cref="Transform"/> to a point.
		/// </summary>
		/// <param name="point">The input point.</param>
		/// <result>The point after this <see cref="Transform"/> has been applied.</result>
		public Vector2D TransformPoint(in Vector2D point)
		{
			this.TransformPoint(point, out Vector2D result);
			return result;
		}
		
		/// <summary>
		/// Returns the transform matrix.
		/// </summary>
		public TransformMatrix ToMatrix()
		{
			this.ToMatrix(out TransformMatrix matrix);
			return matrix;
		}

		/// <summary>
		/// Returns the transform matrix.
		/// </summary>
		public abstract void ToMatrix(out TransformMatrix matrix);

		/// <summary>
		/// Returns a deep copy of this transform.
		/// </summary>
		/// <returns></returns>
		public abstract Transform Clone();

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion

		#region Nested Types

		private sealed class IdentityTransform : Transform
		{
			public override Transform Clone()
			{
				return this;
			}

			public override void ToMatrix(out TransformMatrix matrix)
			{
				matrix = TransformMatrix.Identity;
			}

			public override void TransformPoint(in Vector2D point, out Vector2D result)
			{
				result = point;
			}
		}

		#endregion
	}
}
