﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a rotate transform.
	/// </summary>
	public class RotateTransform : Transform
	{
		#region Fields

		private readonly double angle;
		private readonly Vector2D center;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the rotation angle.
		/// </summary>
		public double Angle => this.angle;

		/// <summary>
		/// Gets or sets the rotation center.
		/// </summary>
		public Vector2D Center => this.center;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="RotateTransform"/> class. 
		/// </summary>
		public RotateTransform(double angle)
		{
			this.angle = angle;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RotateTransform"/> class. 
		/// </summary>
		public RotateTransform(double angle, Vector2D center)
			: this(angle)
		{
			this.center = center;
		}

		#endregion

		#region Methods

		public sealed override void TransformPoint(in Vector2D point, out Vector2D result)
		{
			double sin = Utility.Sin(this.Angle);
			double cos = Utility.Cos(this.Angle);
			
			double x = this.center.X + cos * (point.X - this.center.X) - sin * (point.Y - this.center.Y);
			double y = this.center.Y + sin * (point.X - this.center.X) + cos * (point.Y - this.center.Y);

			result.X = x;
			result.Y = y;
		}

		public sealed override void ToMatrix(out TransformMatrix matrix)
		{
			double sin = Utility.Sin(this.Angle);
			double cos = Utility.Cos(this.Angle);
			matrix.Matrix.M11 = cos;
			matrix.Matrix.M12 = -sin;
			matrix.Translation.X = this.Center.X * (1.0 - cos) + this.Center.Y * sin;
			matrix.Matrix.M21 = sin;
			matrix.Matrix.M22 = cos;
			matrix.Translation.Y = this.Center.Y * (1.0 - cos) - this.Center.X * sin;
		}

		public override Transform Clone()
		{
			return new RotateTransform(this.Angle, this.Center);
		}

		#endregion
	}
}
