﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a 3x3 matrix transform.
	/// </summary>
	public class MatrixTransform : Transform
	{
		#region Fields

		private readonly TransformMatrix matrix;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the matrix.
		/// </summary>
		public TransformMatrix Matrix
		{
			get => this.matrix;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MatrixTransform"/> class. 
		/// </summary>
		public MatrixTransform(TransformMatrix matrix)
		{
			this.matrix = matrix;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MatrixTransform"/> class with an identity matrix.
		/// </summary>
		public MatrixTransform()
		{
			this.matrix = TransformMatrix.Identity;
		}
		
		#endregion

		#region Methods

		public sealed override void TransformPoint(in Vector2D point, out Vector2D result)
		{
			TransformMatrix.Transform(in this.matrix, in point, out result);
		}

		public sealed override void ToMatrix(out TransformMatrix matrix)
		{
			matrix = this.Matrix;
		}

		public override Transform Clone()
		{
			return new MatrixTransform(this.Matrix);
		}

		#endregion
	}
}
