﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a scale transform.
	/// </summary>
	public class ScaleTransform : Transform
	{
		#region Fields

		private readonly Vector2D scale;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the scaling factor.
		/// </summary>
		public Vector2D Scale
		{
			get => this.scale;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ScaleTransform"/> class. 
		/// </summary>
		public ScaleTransform(Vector2D scale)
		{
			this.scale = scale;
		}

		#endregion

		#region Methods
		
		public sealed override void TransformPoint(in Vector2D point, out Vector2D result)
		{
			Vector2D.Multiply(point, this.scale, out result);
		}

		public sealed override void ToMatrix(out TransformMatrix matrix)
		{
			matrix = TransformMatrix.Identity;
			matrix.Matrix.M11 = this.Scale.X;
			matrix.Matrix.M22 = this.Scale.Y;
		}

		public override Transform Clone()
		{
			return new ScaleTransform(this.Scale);
		}

		#endregion
	}
}
