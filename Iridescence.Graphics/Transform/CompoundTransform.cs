﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a compound of <see cref="Transform">transforms</see>.
	/// </summary>
	public class CompoundTransform : Transform, IReadOnlyList<Transform>
	{
		#region Fields

		private readonly IReadOnlyList<Transform> transforms;

		#endregion

		#region Properties

		public int Count => this.transforms.Count;

		public Transform this[int index]
		{
			get => this.transforms[index];
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CompoundTransform"/> class. 
		/// </summary>
		public CompoundTransform(IEnumerable<Transform> transforms)
		{
			this.transforms = transforms.ToReadOnlyList();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CompoundTransform"/> class. 
		/// </summary>
		public CompoundTransform(params Transform[] transforms)
			: this((IEnumerable<Transform>)transforms)
		{

		}

		#endregion

		#region Methods

		public sealed override void TransformPoint(in Vector2D point, out Vector2D result)
		{
			result = point;
			foreach (Transform t in this.transforms)
			{
				if (t == null)
					continue;
				
				t.TransformPoint(in result, out result);
			}
		}

		public sealed override void ToMatrix(out TransformMatrix matrix)
		{
			matrix = TransformMatrix.Identity;
			
			foreach (Transform t in this.transforms)
			{
				if (t == null)
					continue;
				
				TransformMatrix transformMatrix = t.ToMatrix();
				TransformMatrix.Multiply(transformMatrix, matrix, out matrix);
			}
		}

		public override Transform Clone()
		{
			return new CompoundTransform(this.transforms.Select(t => t?.Clone()));
		}
		
		public bool Contains(Transform item)
		{
			return this.transforms.Contains(item);
		}

		public IEnumerator<Transform> GetEnumerator()
		{
			return this.transforms.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)this.transforms).GetEnumerator();
		}
		
		#endregion
	}
}
