﻿using System;
using System.Runtime.CompilerServices;

namespace Iridescence.Graphics
{
	/// <summary>
	/// A collection of pixels in a <see cref="PixelRow"/>.
	/// </summary>
	public readonly ref struct PixelCollection
	{
		#region Fields

		private readonly Span<byte> span;
		private readonly PixelDimension dimX;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the span containing the pixel data.
		/// </summary>
		public Span<byte> Span => this.span;

		/// <summary>
		/// Gets the dimension specification.
		/// </summary>
		public PixelDimension XDimension => this.dimX;

		/// <summary>
		/// Gets the number of pixels in the collection.
		/// </summary>
		public int Count => this.dimX.Count;

		/// <summary>
		/// Gets the pixel data at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public Span<byte> this[int index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if (unchecked((uint)index) >= this.Count)
					throw new ArgumentOutOfRangeException(nameof(index));

				return this.span.Slice(index * this.dimX.Stride, this.dimX.ElementSize);
			}
		}

		/// <summary>
		/// Gets a value that indicates whether the pixels are contiguous.
		/// </summary>
		public bool IsConsecutive => this.dimX.IsConsecutive;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PixelCollection"/> class. 
		/// </summary>
		public PixelCollection(Span<byte> span, in PixelDimension pixels)
		{
			this.span = span;
			this.dimX = pixels;
		}

		#endregion

		#region Enumerator

		/// <summary>
		/// Returns an enumerator for the collection.
		/// </summary>
		/// <returns></returns>
		public Enumerator GetEnumerator() => new Enumerator(this);

		/// <summary>
		/// Enumerator implementation for <see cref="PixelCollection{T}"/>.
		/// </summary>
		public ref struct Enumerator
		{
			#region Fields

			private readonly PixelCollection collection;
			private Span<byte> current;
			private int index;

			#endregion

			#region Properties

			public Span<byte> Current => this.current;

			#endregion

			#region Constructors

			public Enumerator(in PixelCollection collection)
			{
				this.collection = collection;
				this.current = default;
				this.index = 0;
			}

			#endregion

			#region Methods
		
			public void Dispose()
			{
				this.current = default;
			}

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public bool MoveNext()
			{
				if (this.index < this.collection.dimX.Count)
				{
					this.current = this.collection.span.Slice(this.index * this.collection.dimX.Stride, this.collection.dimX.ElementSize);

					++this.index;
					return true;
				}

				return false;
			}

			public void Reset()
			{
				this.current = default;
				this.index = 0;
			}

			#endregion
		}

		#endregion
	}
}