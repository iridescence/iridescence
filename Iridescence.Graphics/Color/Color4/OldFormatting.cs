﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	public partial struct Color4
	{
		#region ToString

		#endregion

		#region Parse

		private static readonly Regex hexRegex = new Regex(@"#?\s*([0-9a-fA-F]{2})\s*([0-9a-fA-F]{2})\s*([0-9a-fA-F]{2})\s*([0-9a-fA-F]{2})?");

		/// <summary>
		/// Converts the string representation of a color in a specified style and culture-specific format to its single-precision floating-point color equivalent.
		/// A return value indicates whether the conversion succeeded or failed.
		/// </summary>
		/// <param name="str">A string representing a color to convert.</param>
		/// <param name="style">A bitwise combination of enumeration values that indicates the permitted format of <see cref="str"/>. A typical value to specify is <see cref="NumberStyles.Float"/> combined with <see cref="NumberStyles.AllowThousands" />.</param>
		/// <param name="provider">An object that supplies culture-specific formatting information about <see cref="str"/>.</param>
		/// <param name="result">When this method returns, contains the single-precision floating-point color equivalent to the numeric value or symbol contained in <see cref="str"/>, if the conversion succeeded, or <see cref="Matrix4x4.Zero"/> if the conversion failed. </param>
		/// <returns>true if <see cref="str"/> was converted successfully; otherwise, false.</returns>
		public static bool TryParse(string str, NumberStyles style, IFormatProvider provider, out Color4 result)
		{
			result = Color4.Transparency;

			if (Color4.GetColorByName(str, out result))
				return true;

			if (style.HasFlag(NumberStyles.HexNumber))
			{
				Match match = hexRegex.Match(str);
				if (match.Success)
				{
					byte r = byte.Parse(match.Groups[1].Value, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
					byte g = byte.Parse(match.Groups[2].Value, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
					byte b = byte.Parse(match.Groups[3].Value, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
					byte a = 0xFF;
					if (match.Groups[4].Success)
						a = byte.Parse(match.Groups[4].Value, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
					result = Color4.FromRgba8(r, g, b, a);
					return true;
				}
			}

			int numElements = ((IReadOnlyList<float>)result).Count;
			float[] components = new float[numElements];
			if (!Formatting.TryParse(str, (string input, out float output) => float.TryParse(input, style, provider, out output), components))
				return false;

			for (int i = 0; i < numElements; ++i)
				result[i] = components[i];

			return true;
		}


		/// <summary>
		/// Converts the string representation of a color to its single-precision floating-point color equivalent.
		/// A return value indicates whether the conversion succeeded or failed.
		/// </summary>
		/// <param name="str">A string representing a color to convert.</param>
		/// <param name="result">When this method returns, contains the single-precision floating-point color equivalent to the numeric value or symbol contained in <see cref="str"/>, if the conversion succeeded, or <see cref="Matrix4x4.Zero"/> if the conversion failed. </param>
		/// <returns>true if <see cref="str"/> was converted successfully; otherwise, false.</returns>
		public static bool TryParse(string str, out Color4 result)
		{
			return Color4.TryParse(str, Formatting.DefaultFloatStyles, CultureInfo.InvariantCulture.NumberFormat, out result);
		}

		/// <summary>
		/// Converts the string representation of a color to its single-precision floating-point color equivalent.
		/// </summary>
		/// <param name="str">A string that contains a color to convert.</param>
		/// <param name="style">A bitwise combination of enumeration values that indicates the permitted format of <see cref="str"/>. A typical value to specify is <see cref="NumberStyles.Float"/> combined with <see cref="NumberStyles.AllowThousands" />.</param>
		/// <param name="provider">An object that supplies culture-specific formatting information about <see cref="str"/>.</param>
		/// <returns>A single-precision floating-point color equivalent to the numeric value or symbol specified in <see cref="str"/>.</returns>
		public static Color4 Parse(string str, NumberStyles style, IFormatProvider provider)
		{
			Color4 result;
			if (!Color4.TryParse(str, style, CultureInfo.InvariantCulture.NumberFormat, out result))
				throw new FormatException();
			return result;
		}

		/// <summary>
		/// Converts the string representation of a color to its single-precision floating-point color equivalent.
		/// </summary>
		/// <param name="str">A string that contains a color to convert.</param>
		/// <returns>A single-precision floating-point color equivalent to the numeric value or symbol specified in <see cref="str"/>.</returns>
		public static Color4 Parse(string str)
		{
			return Color4.Parse(str, Formatting.DefaultFloatStyles, CultureInfo.InvariantCulture.NumberFormat);
		}
		
		#endregion
	}
}
