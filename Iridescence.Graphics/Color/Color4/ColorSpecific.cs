﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	public partial struct Color4
	{
		public Color4(Color4 rgba, float newA)
		{
			this.R = rgba.R;
			this.G = rgba.G;
			this.B = rgba.B;
			this.A = newA;
		}

		public Color4(float r, float g, float b)
		{
			this.R = r;
			this.G = g;
			this.B = b;
			this.A = 1.0f;
		}

		/// <summary>
		/// Gets the relative luminance of the color.
		/// </summary>
		public float Luminance => 0.2126f * this.R + 0.7152f * this.G + 0.0722f * this.B;

		/// <summary>
		/// Gets a color that is brighter than the current color.
		/// This adds a constant value of 0.1f to all channels.
		/// </summary>
		public Color4 Brighter
		{
			get
			{
				Color4 brighter;
				brighter.R = Utility.Clamp(this.R + 0.1f, 0.0f, 1.0f);
				brighter.G = Utility.Clamp(this.G + 0.1f, 0.0f, 1.0f);
				brighter.B = Utility.Clamp(this.B + 0.1f, 0.0f, 1.0f);
				brighter.A = this.A;
				return brighter;
			}
		}

		/// <summary>
		/// Gets a color that is darker than the current color.
		/// This subtracts a constant value of 0.1f to all channels.
		/// </summary>
		public Color4 Darker
		{
			get
			{
				Color4 brighter;
				brighter.R = Utility.Clamp(this.R - 0.1f, 0.0f, 1.0f);
				brighter.G = Utility.Clamp(this.G - 0.1f, 0.0f, 1.0f);
				brighter.B = Utility.Clamp(this.B - 0.1f, 0.0f, 1.0f);
				brighter.A = this.A;
				return brighter;
			}
		}

		/// <summary>
		/// Converts the color to its 32-bit R8G8B8A8 representation.
		/// </summary>
		/// <returns></returns>
		public uint ToRgba8()
		{
			uint r = (uint)Utility.Clamp(this.R * 255.0f, 0.0f, 255.0f);
			uint g = (uint)Utility.Clamp(this.G * 255.0f, 0.0f, 255.0f);
			uint b = (uint)Utility.Clamp(this.B * 255.0f, 0.0f, 255.0f);
			uint a = (uint)Utility.Clamp(this.A * 255.0f, 0.0f, 255.0f);
			return r | (g << 8) | (b << 16) | (a << 24);
		}

		/// <summary>
		/// Converts the color to its 32-bit R8G8B8A8 representation.
		/// </summary>
		/// <returns></returns>
		public void ToRgba8(out byte r, out byte g, out byte b, out byte a)
		{
			r = (byte)Utility.Clamp(this.R * 255.0f, 0.0f, 255.0f);
			g = (byte)Utility.Clamp(this.G * 255.0f, 0.0f, 255.0f);
			b = (byte)Utility.Clamp(this.B * 255.0f, 0.0f, 255.0f);
			a = (byte)Utility.Clamp(this.A * 255.0f, 0.0f, 255.0f);
		}

		/// <summary>
		/// Creates a new color from its RGBA byte representation.
		/// </summary>
		/// <param name="value">A 32-bit unsigned integer that contains the color channels with red in the lowest bits and alpha in the highest bits.</param>
		/// <returns>The color.</returns>
		public static Color4 FromRgba8(uint value)
		{
			byte r = (byte)value;
			byte g = (byte)(value >> 8);
			byte b = (byte)(value >> 16);
			byte a = (byte)(value >> 24);
			return Color4.FromRgba8(r, g, b, a);
		}

		/// <summary>
		/// Creates a new color from its RGBA byte representation.
		/// </summary>
		/// <param name="r">The red value.</param>
		/// <param name="g">The green value.</param>
		/// <param name="b">The blue value.</param>
		/// <returns>The color.</returns>
		public static Color4 FromRgb8(byte r, byte g, byte b)
		{
			return Color4.FromRgba8(r, g, b, 0xFF);
		}
		
		/// <summary>
		/// Creates a new color from its RGBA byte representation.
		/// </summary>
		/// <param name="r">The red value.</param>
		/// <param name="g">The green value.</param>
		/// <param name="b">The blue value.</param>
		/// <param name="a">The alpha value.</param>
		/// <returns>The color.</returns>
		public static Color4 FromRgba8(byte r, byte g, byte b, byte a)
		{
			const float factor = 1.0f / 255.0f;
			Color4 color;
			color.R = r * factor;
			color.G = g * factor;
			color.B = b * factor;
			color.A = a * factor;
			return color;
		}

		/// <summary>
		/// Applies the specified exponent to the color's channels.
		/// </summary>
		/// <param name="exponent">The gamma exponent.</param>
		/// <param name="transformAlpha">Whether to also transform the alpha channel.</param>
		/// <returns></returns>
		public Color4 GammaTransform(float exponent, bool transformAlpha = false)
		{
			Color4 color;
			color.R = Utility.Pow(this.R, exponent);
			color.G = Utility.Pow(this.G, exponent);
			color.B = Utility.Pow(this.B, exponent);
			color.A = transformAlpha ? Utility.Pow(this.A, exponent) : this.A;
			return color;
		}
	}
}
