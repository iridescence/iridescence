﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	public partial struct Color4
	{
		/// <summary>
		/// Converts the RGB color to its hue/saturation/value representation.
		/// </summary>
		/// <param name="h">The hue in the range [0 .. 360].</param>
		/// <param name="s">The saturation in the range [0 .. 1].</param>
		/// <param name="v">The value in the range [0 .. 1].</param>
		public void ToHsv(out float h, out float s, out float v)
		{
			float min = Utility.Min(Utility.Min(this.R, this.G), this.B);
			float max = Utility.Max(Utility.Max(this.R, this.G), this.B);

			v = max;

			float delta = max - min;
			if (delta <= float.Epsilon || max <= float.Epsilon)
			{
				s = 0.0f;
				h = 0.0f;
				return;
			}

			s = delta / max;

			delta *= (1.0f / 60.0f);

			if (this.R >= max)
				h = (this.G - this.B) / delta;
			else if (this.G >= max)
				h = (this.B - this.R) / delta + 120.0f;
			else
				h = (this.R - this.G) / delta + 240.0f;

			if (h < 0.0f)
				h += 360.0f;
		}

		/// <summary>
		/// Converts hue/saturation/value representation to an RGB color.
		/// </summary>
		/// <param name="h">The hue in the range [0 .. 360].</param>
		/// <param name="s">The saturation in the range [0 .. 1].</param>
		/// <param name="v">The value in the range [0 .. 1].</param>
		/// <returns>The color representing the input values.</returns>
		public static Color4 FromHsv(float h, float s, float v)
		{
			// http://www.cs.rit.edu/~ncs/color/t_convert.html

			Color4 color;
			color.A = 1.0f;

			if (s <= float.Epsilon)
			{
				// gray.
				color.R = color.G = color.B = v;
			}
			else
			{
				// put hue in the range [0 .. 6]
				h = (h / 60.0f) % 6.0f;

				int i = (int)h;
				float f = h - i;
				float p = v * (1.0f - s);
				float q = v * (1.0f - s * f);
				float t = v * (1.0f - s * (1.0f - f));

				switch (i)
				{
					case 0:
						color.R = v;
						color.G = t;
						color.B = p;
						break;
					case 1:
						color.R = q;
						color.G = v;
						color.B = p;
						break;
					case 2:
						color.R = p;
						color.G = v;
						color.B = t;
						break;
					case 3:
						color.R = p;
						color.G = q;
						color.B = v;
						break;
					case 4:
						color.R = t;
						color.G = p;
						color.B = v;
						break;
					default:
						color.R = v;
						color.G = p;
						color.B = q;
						break;
				}
			}

			return color;
		}

		/// <summary>
		/// Converts hue/saturation/value representation with alpha channel to an RGBA color.
		/// </summary>
		/// <param name="h">The hue in the range [0 .. 360].</param>
		/// <param name="s">The saturation in the range [0 .. 1].</param>
		/// <param name="v">The value in the range [0 .. 1].</param>
		/// <param name="a">The alpha channel in the range [0 .. 1].</param>
		/// <returns>The color representing the input values.</returns>
		public static Color4 FromHsva(float h, float s, float v, float a)
		{
			Color4 color = Color4.FromHsv(h, s, v);
			color.A = a;
			return color;
		}
	}
}
