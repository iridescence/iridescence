﻿using System;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Provides access to raw pixel data.
	/// </summary>
	public interface IPixelInfo
	{
		void Read<T>(ReadOnlySpan<byte> source, Span<T> dest, int sourceStrideX, int countX);
		void Write<T>(ReadOnlySpan<T> source, Span<byte> dest, int destStrideX, int countX);
	}
}
