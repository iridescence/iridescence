﻿using System;
using System.IO;
using Iridescence.FileSystem.Composition;
using File = Iridescence.FileSystem.File;

namespace Iridescence.Graphics.IO
{
	/// <summary>
	/// Exports <see cref="Image"/> instances from .dds files.
	/// </summary>
	public class DdsExportProvider : FileExportDescriptorProviderBase
	{
		#region Properties

		protected override Type PartType => typeof(Image);

		protected override FileExportSharingMode SharingMode => FileExportSharingMode.WeakReference;

		#endregion
	
		#region Methods

		protected override bool IsSupportedName(string name)
		{
			return name.EndsWith(".dds");
		}
		
		protected override object Load(File file)
		{
			using (Stream stream = file.Open(FileMode.Open, FileAccess.Read))
			{
				return Dds.Read(stream);
			}
		}

		#endregion
	}
}
