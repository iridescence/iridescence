﻿using System;
using System.IO;
using System.IO.Compression;
using Iridescence.Math;

namespace Iridescence.Graphics.IO
{
	/// <summary>
	/// Implements a decoder for portable network graphics (.png) images.
	/// </summary>
	public static class Png
	{
		#region Fields

		private static readonly byte[] scale1 = {0, 255};
		private static readonly byte[] scale2 = {0, 85, 170, 255};
		private static readonly byte[] scale4 = {0, 17, 34, 51, 68, 85, 102, 119, 136, 153, 170, 187, 204, 221, 238, 255};

		private static readonly Format gray8 = new Format(FormatType.R8, FormatSemantic.UNorm,
			(ColorMappingChannel.Red, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
			(ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.One));
		
		private static readonly Format gray16 = new Format(FormatType.R16, FormatSemantic.UNorm,
			(ColorMappingChannel.Red, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
			(ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.One));

		private static readonly Format gray8a8 = new Format(FormatType.R8G8, FormatSemantic.UNorm,
			(ColorMappingChannel.Red, ColorMappingChannel.Alpha, ColorMappingChannel.None, ColorMappingChannel.None),
			(ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Green));
		
		private static readonly Format gray16a16 = new Format(FormatType.R16G16, FormatSemantic.UNorm,
			(ColorMappingChannel.Red, ColorMappingChannel.Alpha, ColorMappingChannel.None, ColorMappingChannel.None),
			(ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Green));

		#endregion

		#region Methods

		/// <summary>
		/// Reads a PNG file from the specified stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static Image Read(Stream stream)
		{
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));

			ulong signature = stream.ReadUInt64B();
			if (signature != 0x89504E470D0A1A0A)
				throw new InvalidDataException("Invalid PNG signature.");

			using (PngChunkStream pngStream = new PngChunkStream(stream))
			{
				int bitDepth = 0;
				int colorType = 0;
				int width = 0;
				int height = 0;
				Format format = Format.None;
				int interlaceMethod = 0;
				MemoryImage image = null;

				byte[] palette = null;
				int bytesPerPixel = 0;

				bool idatFound = false;

				for(;;)
				{
					PngChunkType chunkType = pngStream.ReadNextChunk();
					if (chunkType == PngChunkType.IHDR)
					{
						if (width != 0 || height != 0 || format != Format.None)
							throw new InvalidDataException("More than one header chunk found.");

						width = pngStream.ReadInt32B();
						height = pngStream.ReadInt32B();
						bitDepth = pngStream.ReadByteOrThrow();
						colorType = pngStream.ReadByteOrThrow();
						
						switch (colorType)
						{
							case 0: // grayscale
								if (bitDepth == 1 || bitDepth == 2 || bitDepth == 4 || bitDepth == 8)
								{
									format = gray8;
									bytesPerPixel = 1;
								}
								else if (bitDepth == 16)
								{
									format = gray16;
									bytesPerPixel = 2;
								}
								else
								{
									throw new ArgumentException("Invalid bit depth.");
								}
								break;

							case 2: // rgb
								if (bitDepth == 8)
								{
									format = new Format(FormatType.R8G8B8, FormatSemantic.UNorm);
									bytesPerPixel = 3;
								}
								else if (bitDepth == 16)
								{
									format = new Format(FormatType.R16G16B16, FormatSemantic.UNorm);
									bytesPerPixel = 6;
								}
								else
								{
									throw new ArgumentException("Invalid bit depth.");
								}
								break;

							case 3: // indexed
								if (bitDepth == 1 || bitDepth == 2 || bitDepth == 4 || bitDepth == 8)
								{
									format = new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm);
									bytesPerPixel = 3;
								}
								else
								{
									throw new ArgumentException("Invalid bit depth.");
								}
								break;

							case 4: // grayscale + alpha
								if (bitDepth == 8)
								{
									format = gray8a8;
									bytesPerPixel = 2;
								}
								else if (bitDepth == 16)
								{
									format = gray16a16;
									bytesPerPixel = 4;
								}
								else
								{
									throw new ArgumentException("Invalid bit depth.");
								}
								break;

							case 6: // rgba
								if (bitDepth == 8)
								{
									format = new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm);
									bytesPerPixel = 4;
								}
								else if (bitDepth == 16)
								{
									format = new Format(FormatType.R16G16B16A16, FormatSemantic.UNorm);
									bytesPerPixel = 8;
								}
								else
								{
									throw new ArgumentException("Invalid bit depth.");
								}
								break;

							default:
								throw new ArgumentException("Invalid color type.");
						}

						int compressionMethod = pngStream.ReadByteOrThrow();
						if (compressionMethod != 0)
							throw new NotSupportedException("Unsupported compression method. DEFLATE is the only supported PNG compression method.");

						int filterMethod = pngStream.ReadByteOrThrow();
						if (filterMethod != 0)
							throw new NotSupportedException("Unsupported filter method.");

						interlaceMethod = pngStream.ReadByteOrThrow();
					}
					else if (chunkType == PngChunkType.PLTE)
					{
						palette = pngStream.ReadOrThrow(pngStream.ChunkLength);
					}
					else if (chunkType == PngChunkType.IDAT)
					{
						if (image != null)
							throw new InvalidDataException("Found IDAT chunk after data already parsed.");

						if (width == 0 || height == 0 || format == Format.None)
							throw new InvalidDataException("Invalid or missing header chunk before IDAT chunk.");

						// there can be multiple IDAT chunks which contain the concatenated, compressed data.
						pngStream.BeginSegmentedRead();

						// skip 2 bytes which contain the zlib headers.
						// .NET doesn't understand them, but ignoring them seems to work.
						pngStream.ReadUInt16L();

						int stride = width * bytesPerPixel;

						byte[] currentLine = new byte[stride];
						byte[] previousLine = new byte[stride];
						byte[] buffer = new byte[Format.GetSize(format.Type, width, height, 1)];

						using (DeflateStream dataStream = new DeflateStream(pngStream, CompressionMode.Decompress, true))
						{
							if (interlaceMethod == 0)
							{
								int offset = stride * height;
								for (int y = 0; y < height; y++)
								{
									offset -= width * bytesPerPixel;
									readLine(dataStream, currentLine, previousLine, width, colorType, bitDepth, palette);
									Array.Copy(currentLine, 0, buffer, offset, stride);
								}
							}
							else if (interlaceMethod == 1) // adam7
							{
								void adam7(int ystep, int yoffset, int xstep, int xoffset)
								{
									int numRows = (height + ystep - 1 - yoffset) / ystep;
									int numPixels = (width + xstep - 1 - xoffset) / xstep;

									if (numRows == 0 || numPixels == 0)
										return;

									// clear previous line.
									for (int i = numPixels * bytesPerPixel - 1; i >= 0; i--)
										previousLine[i] = 0;

									for (int y = 0; y < numRows; y++)
									{
										readLine(dataStream, currentLine, previousLine, numPixels, colorType, bitDepth, palette);
										interlaceCopy(currentLine, buffer, (y * ystep + yoffset) * stride + xoffset * bytesPerPixel, numPixels, bytesPerPixel, xstep * bytesPerPixel);
									}
								}

								adam7(8, 0, 8, 0); // pass 1
								adam7(8, 0, 8, 4); // pass 2
								adam7(8, 4, 4, 0); // pass 3
								adam7(4, 0, 4, 2); // pass 4
								adam7(4, 2, 2, 0); // pass 5
								adam7(2, 0, 2, 1); // pass 6
								adam7(2, 1, 1, 0); // pass 7

								// Flip the image.
								for (int y = 0; y < height / 2; y++)
								{
									Array.Copy(buffer, y * stride, currentLine, 0, stride);
									Array.Copy(buffer, (height - 1 - y) * stride, buffer, y * stride, stride);
									Array.Copy(currentLine, 0, buffer, (height - 1 - y) * stride, stride);
								}
							}
							else
							{
								throw new InvalidDataException("Invalid or unsupported interlace method.");
							}
						}

						// adler32 checksum from zlib.
						// we just ignore that here, the CRC-32 checksum of the chunk will make sure that there are no errors anyway.
						// .NET's deflate stream seems to be greedy and reads too much data, so we can't be sure that there will actually be 4 bytes left to read.
						int remaining = 0;
						while (remaining++ < 4 && pngStream.ReadByte() > 0) ;

						// create final image.
						image = new MemoryImage(buffer, format, width, height);

						idatFound = true;
					}
					else if (chunkType == PngChunkType.IEND)
					{
						break;
					}
				}

				if (!idatFound)
					throw new InvalidDataException("Missing IDAT chunk.");

				return image;
			}
		}

		private static void readLine(Stream stream, byte[] currentLine, byte[] previousLine, int numPixels, int colorType, int bitDepth, byte[] palette)
		{
			// Read filter type.
			int filterType = stream.ReadByteOrThrow();
			
			// Determine number of bytes to read and read the scanline.
			int bitsPerPixel = bitDepth;
			switch (colorType)
			{
				case 2:
					bitsPerPixel *= 3; // 3 channels, rgb.
					break;

				case 4:
					bitsPerPixel *= 2; // 2 channels, gray and alpha.
					break;

				case 6:
					bitsPerPixel *= 4; // 4 channels, rgba.
					break;
			}

			int count = (numPixels * bitsPerPixel + 7) / 8;
			stream.ReadOrThrow(currentLine, 0, count);

			// Undo the filter.
			int bytesPerPixel = (bitsPerPixel + 7) / 8;
			switch (filterType)
			{
				case 0:
					// Nothing to do.
					break;

				case 1:
					unfilterSub(currentLine, count, bytesPerPixel);
					break;

				case 2:
					unfilterUp(currentLine, previousLine, count);
					break;

				case 3:
					unfilterAverage(currentLine, previousLine, count, bytesPerPixel);
					break;

				case 4:
					unfilterPeath(currentLine, previousLine, count, bytesPerPixel);
					break;

				default:
					throw new InvalidDataException($"Invalid filter type {filterType}.");
			}

			Array.Copy(currentLine, previousLine, count);

			// Expand bits to bytes.
			if (bitDepth == 1)
				expand1(currentLine, numPixels, colorType != 3);
			else if (bitDepth == 2)
				expand2(currentLine, numPixels, colorType != 3);
			else if (bitDepth == 4)
				expand4(currentLine, numPixels, colorType != 3);
			else if (bitDepth == 16)
				swap16(currentLine, numPixels * bytesPerPixel);

			// Indexed.
			if (colorType == 3)
			{
				if (palette == null)
					throw new InvalidDataException("Missing palette for indexed colors.");

				// Copy palette entries into rows.
				for (int x = numPixels - 1; x >= 0; x--)
				{
					int palOffset = currentLine[x] * 3;
					int destOffset = x * 3;
					currentLine[destOffset] = palette[palOffset];
					currentLine[destOffset + 1] = palette[palOffset + 1];
					currentLine[destOffset + 2] = palette[palOffset + 2];
				}
			}
		}

		private static void unfilterSub(byte[] currentLine, int count, int reference)
		{
			for (int i = reference; i < count; i++)
				currentLine[i] += currentLine[i - reference];
		}

		private static void unfilterUp(byte[] currentLine, byte[] previousLine, int count)
		{
			for (int i = 0; i < count; i++)
				currentLine[i] += previousLine[i];
		}

		private static void unfilterAverage(byte[] currentLine, byte[] previousLine, int count, int reference)
		{
			for (int i = 0; i < count; i++)
			{
				if (i >= reference)
					currentLine[i] += (byte)((currentLine[i - reference] + previousLine[i]) >> 1);
				else
					currentLine[i] += (byte)(previousLine[i] >> 1);
			}
		}

		private static byte paethPredictor(byte a, byte b, byte c)
		{
			int p = a + b - c;
			int pa = Utility.Absolute(p - a);
			int pb = Utility.Absolute(p - b);
			int pc = Utility.Absolute(p - c);

			if (pa <= pb && pa <= pc)
				return a;

			if (pb <= pc)
				return b;

			return c;
		}

		private static void unfilterPeath(byte[] currentLine, byte[] previousLine, int count, int reference)
		{
			for (int i = 0; i < count; i++)
			{
				byte left, leftUp;
				if (i < reference)
				{
					left = 0;
					leftUp = 0;
				}
				else
				{
					left = currentLine[i - reference];
					leftUp = previousLine[i - reference];
				}

				currentLine[i] = unchecked((byte)(currentLine[i] + paethPredictor(left, previousLine[i], leftUp)));
			}
		}

		private static void expand1(byte[] buffer, int width, bool scale)
		{
			if (scale)
			{
				for (int i = width - 1; i >= 0; i--)
					buffer[i] = scale1[(buffer[i >> 3] >> (7 - (i & 7))) & 1];
			}
			else
			{
				for (int i = width - 1; i >= 0; i--)
					buffer[i] = (byte)((buffer[i >> 3] >> (7 - (i & 7))) & 1);
			}
		}

		private static void expand2(byte[] buffer, int width, bool scale)
		{
			if (scale)
			{
				for (int i = width - 1; i >= 0; i--)
					buffer[i] = scale2[(buffer[i >> 2] >> ((3 - (i & 3)) << 1)) & 3];
			}
			else
			{
				for (int i = width - 1; i >= 0; i--)
					buffer[i] = (byte)((buffer[i >> 2] >> ((3 - (i & 3)) << 1)) & 3);
			}
		}

		private static void expand4(byte[] buffer, int width, bool scale)
		{
			if ((width & 1) == 1)
			{
				// odd width.
				width--;
				if (scale)
					buffer[width] = scale4[buffer[width >> 1] >> 4];
				else
					buffer[width] = (byte)(buffer[width >> 1] >> 4);
			}

			if (scale)
			{
				for (int i = width - 1; i >= 0; i -= 2)
				{
					int data = buffer[i >> 1];
					buffer[i - 1] = scale4[data >> 4];
					buffer[i] = scale4[data & 15];
				}
			}
			else
			{
				for (int i = width - 1; i >= 0; i -= 2)
				{
					int data = buffer[i >> 1];
					buffer[i - 1] = (byte)(data >> 4);
					buffer[i] = (byte)(data & 15);
				}
			}
		}

		private static void swap16(byte[] buffer, int count)
		{
			for (int i = 0; i < count; i += 2)
			{
				byte temp = buffer[i];
				buffer[i] = buffer[i + 1];
				buffer[i + 1] = temp;
			}
		}

		private static void interlaceCopy(byte[] line, byte[] buffer, int offset, int count, int bpp, int step)
		{
			if (bpp == 1)
			{
				for (int i = 0; i < count; i++, offset += step)
				{
					buffer[offset] = line[i];
				}
			}
			else if (bpp == 2)
			{
				for (int i = 0, x = 0; i < count; i++, x += 2, offset += step)
				{
					buffer[offset] = line[x];
					buffer[offset + 1] = line[x + 1];
				}
			}
			else if (bpp == 3)
			{
				for (int i = 0, x = 0; i < count; i++, x += 3, offset += step)
				{
					buffer[offset] = line[x];
					buffer[offset + 1] = line[x + 1];
					buffer[offset + 2] = line[x + 2];
				}
			}
			else if (bpp == 4)
			{
				for (int i = 0, x = 0; i < count; i++, x += 4, offset += step)
				{
					buffer[offset] = line[x];
					buffer[offset + 1] = line[x + 1];
					buffer[offset + 2] = line[x + 2];
					buffer[offset + 3] = line[x + 3];
				}
			}
			else if (bpp == 6)
			{
				for (int i = 0, x = 0; i < count; i++, x += 6, offset += step)
				{
					buffer[offset] = line[x];
					buffer[offset + 1] = line[x + 1];
					buffer[offset + 2] = line[x + 2];
					buffer[offset + 3] = line[x + 3];
					buffer[offset + 4] = line[x + 4];
					buffer[offset + 5] = line[x + 5];
				}
			}
			else if (bpp == 8)
			{
				for (int i = 0, x = 0; i < count; i++, x += 8, offset += step)
				{
					buffer[offset] = line[x];
					buffer[offset + 1] = line[x + 1];
					buffer[offset + 2] = line[x + 2];
					buffer[offset + 3] = line[x + 3];
					buffer[offset + 4] = line[x + 4];
					buffer[offset + 5] = line[x + 5];
					buffer[offset + 6] = line[x + 6];
					buffer[offset + 7] = line[x + 7];
				}
			}
		}

		private enum PngChunkType : uint
		{
			IHDR = 0x49484452u,
			PLTE = 0x504C5445u,
			IDAT = 0x49444154u,
			IEND = 0x49454E44u,
		}

		private sealed class PngChunkStream : Stream
		{
			#region Fields

			private bool newChunk;
			private readonly Crc32Stream stream;
			private PngChunkType chunkType;
			private PngChunkType segmentType;
			private bool segmentEnd;
			private int chunkLength;

			#endregion

			#region Properties

			public override long Length
			{
				get { throw new NotSupportedException(); }
			}

			public override long Position
			{
				get { throw new NotSupportedException(); }
				set { throw new NotSupportedException(); }
			}

			public override bool CanRead => true;

			public override bool CanSeek => false;

			public override bool CanWrite => false;

			public PngChunkType ChunkType => this.chunkType;

			public int ChunkLength => this.chunkLength;

			#endregion

			#region Constructors

			public PngChunkStream(Stream stream)
			{
				this.stream = new Crc32Stream(Crc32.Default, stream, true);
				this.newChunk = true;
			}

			#endregion

			#region Methods

			public PngChunkType ReadNextChunk()
			{
				// happens when leaving segmented mode
				// the chunk header after the segmented part will already be read, but we don't want to skip it.
				if (this.segmentEnd)
				{
					this.segmentEnd = false;
					return this.chunkType;
				}

				// skip remaining data of current chunk.
				if (this.chunkLength > 0)
				{
					byte[] temp = new byte[4096];
					while (this.chunkLength > 0)
					{
						int numBytes = this.stream.Read(temp, 0, Utility.Min(temp.Length, this.chunkLength));
						if (numBytes == 0)
							throw new EndOfStreamException();
						this.chunkLength -= numBytes;
					}
				}

				// check CRC-32.
				if (this.chunkType != 0)
				{
					uint crc32 = this.stream.Checksum;
					uint actualCrc32 = this.stream.ReadUInt32B();
					if (crc32 != actualCrc32)
					{
						throw new InvalidDataException("Corrupt or invalid PNG chunk.");
					}
				}

				// read chunk length.
				this.chunkLength = this.stream.ReadInt32B();

				// reset crc and read chunk type.
				this.stream.ResetChecksum();
				this.chunkType = (PngChunkType)this.stream.ReadUInt32B();
				this.newChunk = false;

				return this.chunkType;
			}

			public override int Read(byte[] buffer, int offset, int count)
			{
				// do we need to read a new chunk header?
				if (this.newChunk)
				{
					// if we're in segmented mode, automatically read the next chunk.
					// this is useful for reading IDAT chunks.
					if (this.segmentType != 0)
					{
						// if the new chunk is of a different type than the last chunk, we've reached the end.
						if (this.ReadNextChunk() != this.segmentType)
						{
							this.segmentType = 0;
							this.segmentEnd = true;
							return 0;
						}
					}
					else
					{
						return 0;
					}
				}

				// will the current operation reach the end of the chunk?
				if (count >= this.chunkLength)
				{
					count = this.chunkLength; // limit the amount of data to read.
					this.newChunk = true; // read a new chunk next time.
				}

				// read chunk data.
				int numBytes = this.stream.Read(buffer, offset, count);
				this.chunkLength -= numBytes;

				return numBytes;
			}

			public void BeginSegmentedRead()
			{
				this.segmentType = this.chunkType;
				this.segmentEnd = false;
			}

			public override void Flush()
			{

			}

			public override void Close()
			{
				this.stream.Close();
				base.Close();
			}

			public override void Write(byte[] buffer, int offset, int count)
			{
				throw new NotSupportedException();
			}

			public override long Seek(long offset, SeekOrigin origin)
			{
				throw new NotSupportedException();
			}

			public override void SetLength(long value)
			{
				throw new NotSupportedException();
			}

			#endregion
		}

		#endregion
	}
}