﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.IO
{
	/// <summary>
	/// Implementation of DDS (direct draw surface).
	/// </summary>
	public static class Dds
	{
		#region Fields

		private static readonly uint dxt1 = toFourCC("DXT1");
		private static readonly uint dxt2 = toFourCC("DXT2");
		private static readonly uint dxt3 = toFourCC("DXT3");
		private static readonly uint dxt4 = toFourCC("DXT4");
		private static readonly uint dxt5 = toFourCC("DXT5");

		#endregion

		#region Constructors
		
		#endregion

		#region Methods

		private static uint toFourCC(string str)
		{
			return ((uint)str[0] << 0) |
			       ((uint)str[1] << 8) |
			       ((uint)str[2] << 16) |
			       ((uint)str[3] << 24);
		}

		/*
		private static FormatMappingChannel getMaskMapping8(uint mask)
		{
			if (mask == 0xFF)
				return FormatMappingChannel.Red;
			
			if (mask == 0x00)
				return FormatMappingChannel.Zero;

			throw new ArgumentException($"Invalid channel bit mask {mask:X8}");
		}

		private static FormatMappingChannel getMaskMapping332(uint mask)
		{
			if (mask == 0b000_000_11)
				return FormatMappingChannel.Blue;
			
			if (mask == 0b000_111_00)
				return FormatMappingChannel.Green;

			if (mask == 0b111_000_00)
				return FormatMappingChannel.Red;
			
			if (mask == 0b000_000_00)
				return FormatMappingChannel.Zero;

			throw new ArgumentException($"Invalid channel bit mask {mask:X8}");
		}
		
		private static FormatMappingChannel getMaskMapping565(uint mask)
		{
			if (mask == 0b00000_000000_11111)
				return FormatMappingChannel.Blue;
			
			if (mask == 0b00000_111111_00000)
				return FormatMappingChannel.Green;

			if (mask == 0b11111_000000_00000)
				return FormatMappingChannel.Red;
			
			if (mask == 0b00000_000000_00000)
				return FormatMappingChannel.Zero;

			throw new ArgumentException($"Invalid channel bit mask {mask:X8}");
		}
		*/
		
		private static ColorMappingChannel getMaskMapping5551(uint mask)
		{
			if (mask == 0b0_00000_00000_11111)
				return ColorMappingChannel.Red;
			
			if (mask == 0b0_00000_11111_00000)
				return ColorMappingChannel.Green;

			if (mask == 0b0_11111_00000_00000)
				return ColorMappingChannel.Blue;
			
			if (mask == 0b1_00000_00000_00000)
				return ColorMappingChannel.Alpha;

			if (mask == 0b0_00000_00000_00000)
				return ColorMappingChannel.Zero;

			throw new ArgumentException($"Invalid channel bit mask {mask:X8}");
		}

		private static ColorMappingChannel getMaskMapping4444(uint mask)
		{
			if (mask == 0b0000_0000_0000_1111)
				return ColorMappingChannel.Red;
			
			if (mask == 0b0000_0000_1111_0000)
				return ColorMappingChannel.Green;

			if (mask == 0b0000_1111_0000_0000)
				return ColorMappingChannel.Blue;
			
			if (mask == 0b1111_0000_0000_0000)
				return ColorMappingChannel.Alpha;

			if (mask == 0b0000_0000_0000_0000)
				return ColorMappingChannel.Zero;

			throw new ArgumentException($"Invalid channel bit mask {mask:X8}");
		}

		private static ColorMappingChannel getMaskMapping8888(uint mask)
		{
			if (mask == 0x000000FFu)
				return ColorMappingChannel.Red;
			
			if (mask == 0x0000FF00u)
				return ColorMappingChannel.Green;

			if (mask == 0x00FF0000u)
				return ColorMappingChannel.Blue;
			
			if (mask == 0xFF000000u)
				return ColorMappingChannel.Alpha;

			if (mask == 0x00000000u)
				return ColorMappingChannel.Zero;

			throw new ArgumentException($"Invalid channel bit mask {mask:X8}");
		}

		private static ColorMappingChannel getMaskMapping88(uint mask)
		{
			if (mask == 0x000000FFu)
				return ColorMappingChannel.Red;
			
			if (mask == 0x0000FF00u)
				return ColorMappingChannel.Green;

			if (mask == 0x00000000u)
				return ColorMappingChannel.Zero;

			throw new ArgumentException($"Invalid channel bit mask {mask:X8}");
		}

		private static ColorMappingChannel getMaskMapping1010102(uint mask)
		{
			if (mask == 0x000003FFu)
				return ColorMappingChannel.Red;
			
			if (mask == 0x000FFC00u)
				return ColorMappingChannel.Green;

			if (mask == 0x3FF00000u)
				return ColorMappingChannel.Blue;
			
			if (mask == 0xC0000000u)
				return ColorMappingChannel.Alpha;

			if (mask == 0x00000000u)
				return ColorMappingChannel.Zero;

			throw new ArgumentException($"Invalid channel bit mask {mask:X8}");
		}
		
		/// <summary>
		/// Reads a DDS texture from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns>
		/// A two-dimensional array of images. The first dimension is the array index or "face", the second dimension is the mip map level.
		/// For example, array[2, 4] is the fifth mip map level of the third image in the array.
		/// The number of faces and mip map levels depends on the file. For example, a cube map would have 6 faces.
		/// </returns>
		public static Image[,] Read(Stream stream)
		{
			if (stream.ReadInt32L() != 0x20534444) // "DDS "
			{
				throw new InvalidDataException("Invalid DDS header.");
			}

			Header header = stream.ReadStructure<Header>();
			if (header.Size != 124) // Header size, must be 124
			{
				throw new InvalidDataException("Invalid DDS header.");
			}
			
			// Compute number of faces.
			int faces = 1;
			if ((header.Caps2 & Caps2.CubeMap) > 0)
			{
				faces = 0;
				if (header.Caps2.HasFlag(Caps2.CubeMapPositiveX))
					faces++;
				if (header.Caps2.HasFlag(Caps2.CubeMapNegativeX))
					faces++;

				if (header.Caps2.HasFlag(Caps2.CubeMapPositiveY))
					faces++;
				if (header.Caps2.HasFlag(Caps2.CubeMapNegativeY))
					faces++;

				if (header.Caps2.HasFlag(Caps2.CubeMapPositiveZ))
					faces++;
				if (header.Caps2.HasFlag(Caps2.CubeMapNegativeZ))
					faces++;
			}

			// Find suitable format.
			Format format;
			if (header.PixelFormat.Flags.HasFlag(PixelFormatFlags.FourCC))
			{
				// TODO
				if (header.PixelFormat.FourCC == dxt1)
				{
					throw new NotImplementedException();
				}
				else if (header.PixelFormat.FourCC == dxt2 || header.PixelFormat.FourCC == dxt3)
				{
					throw new NotImplementedException();
				}
				else if (header.PixelFormat.FourCC == dxt4 || header.PixelFormat.FourCC == dxt5)
				{
					throw new NotImplementedException();
				}

				throw new NotSupportedException("Unsupported DDS pixel format.");
			}
			else if (header.PixelFormat.Flags.HasFlag(PixelFormatFlags.Alpha))
			{
				if (header.PixelFormat.RgbBitCount == 8 && header.PixelFormat.ABitMask == 0xFF)
				{
					format = new Format(FormatType.R8, FormatSemantic.UNorm,
						(ColorMappingChannel.Alpha, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
						(ColorMappingChannel.One, ColorMappingChannel.One, ColorMappingChannel.One, ColorMappingChannel.Red));
				}
				else if (header.PixelFormat.RgbBitCount == 16 && header.PixelFormat.ABitMask == 0xFFFF)
				{
					format = new Format(FormatType.R16, FormatSemantic.UNorm,
						(ColorMappingChannel.Alpha, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
						(ColorMappingChannel.One, ColorMappingChannel.One, ColorMappingChannel.One, ColorMappingChannel.Red));
				}
				else
				{
					throw new NotSupportedException("Unsupported DDS pixel format.");
				}
			}
			else if (header.PixelFormat.Flags.HasFlag(PixelFormatFlags.RGB))
			{
				FormatType type;
				ColorMapping mapping;
				FormatFlags flags = FormatFlags.None;
				if (header.PixelFormat.RgbBitCount == 8)
				{
					if (header.PixelFormat.TryGetMapping(0b111_000_00, 0b000_111_00, 0b000_000_11, 0, out mapping))
					{
						type = FormatType.R3G3B2;
					}
					else if (header.PixelFormat.TryGetMapping(0xFF, 0, 0, 0, out mapping))
					{
						type = FormatType.R8;
					}
					else
					{
						throw new NotSupportedException("Unsupported DDS pixel format. No fitting channel mapping found.");
					}
				}
				else if (header.PixelFormat.RgbBitCount == 16)
				{
					if (header.PixelFormat.TryGetMapping(0b11111_000000_00000, 0b00000_111111_00000, 0b00000_000000_11111, 0, out mapping))
					{
						type = FormatType.R5G6B5;
					}
					else if (header.PixelFormat.TryGetMapping(0b1111_0000_0000_0000, 0b0000_1111_0000_0000, 0b0000_0000_1111_0000, 0b0000_0000_0000_1111, out mapping))
					{
						type = FormatType.R4G4B4A4;
					}
					else if (header.PixelFormat.TryGetMapping(0b0_00000_00000_11111, 0b0_00000_11111_00000, 0b0_11111_00000_00000, 0b1_00000_00000_00000, out mapping))
					{
						type = FormatType.R5G5B5A1;
						flags = FormatFlags.Reversed;
					}
					else if (header.PixelFormat.TryGetMapping(0xFF00, 0x00FF, 0, 0, out mapping))
					{
						type = FormatType.R8G8;
					}
					else if (header.PixelFormat.TryGetMapping(0xFFFF, 0, 0, 0, out mapping))
					{
						type = FormatType.R16;
					}
					else
					{
						throw new NotSupportedException("Unsupported DDS pixel format. No fitting channel mapping found.");
					}
				}
				else if (header.PixelFormat.RgbBitCount == 24)
				{
					if (header.PixelFormat.TryGetMapping(0x000000FFu, 0x0000FF00u, 0x00FF0000u, 0, out mapping))
					{
						type = FormatType.R8G8B8;
					}
					else
					{
						throw new NotSupportedException("Unsupported DDS pixel format. No fitting channel mapping found.");
					}
				}
				else if (header.PixelFormat.RgbBitCount == 32)
				{
					if (header.PixelFormat.TryGetMapping(0x000000FFu, 0x0000FF00u, 0x00FF0000u, 0xFF000000u, out mapping))
					{
						type = FormatType.R8G8B8A8;
					}
					else if (header.PixelFormat.TryGetMapping(0x000003FFu, 0x000FFC00u, 0x3FF00000u, 0xC0000000u, out mapping))
					{
						type = FormatType.R10G10B10A2;
						flags = FormatFlags.Reversed;
					}
					else if (header.PixelFormat.TryGetMapping(0x0000FFFFu, 0xFFFF0000u, 0, 0, out mapping))
					{
						type = FormatType.R16G16;
					}
					else
					{
						throw new NotSupportedException("Unsupported DDS pixel format. No fitting channel mapping found.");
					}
				}
				else
				{
					throw new NotSupportedException("Unsupported DDS pixel format. Unsupported number of bits per pixel.");
				}

				if(!header.PixelFormat.Flags.HasFlag(PixelFormatFlags.AlphaPixels))
					mapping = new ColorMapping(mapping.Red, mapping.Green, mapping.Blue, ColorMappingChannel.One);
				format = new Format(type, FormatSemantic.UNorm, mapping, mapping, flags);
			}
			else if (header.PixelFormat.Flags.HasFlag(PixelFormatFlags.Luminance))
			{
				FormatType type;
				ColorMapping mapping;

				if (header.PixelFormat.RgbBitCount == 8)
				{
					if (header.PixelFormat.TryGetMapping(0xFF, 0, 0, 0, out mapping))
					{
						type = FormatType.R8;
					}
					else
					{
						throw new NotSupportedException("Unsupported DDS pixel format. No fitting channel mapping found.");
					}
				}
				else if (header.PixelFormat.RgbBitCount == 16)
				{
					if (header.PixelFormat.TryGetMapping(0x00FF, 0, 0, 0xFF00, out mapping))
					{
						type = FormatType.R8G8;
					}
					else if (header.PixelFormat.TryGetMapping(0xFFFF, 0, 0, 0, out mapping))
					{
						type = FormatType.R16;
					}
					else
					{
						throw new NotSupportedException("Unsupported DDS pixel format. No fitting channel mapping found.");
					}
				}
				else
				{
					throw new NotSupportedException("Unsupported DDS pixel format. Unsupported number of bits per pixel.");
				}

				ColorMapping inputMapping = new ColorMapping(ColorMappingChannel.Red, ColorMappingChannel.Alpha, ColorMappingChannel.None, ColorMappingChannel.None);
				if (mapping.Red == ColorMappingChannel.Alpha)
				{
					mapping = new ColorMapping(ColorMappingChannel.Green, ColorMappingChannel.Green, ColorMappingChannel.Green, mapping.Alpha);
					inputMapping = new ColorMapping(ColorMappingChannel.Alpha, ColorMappingChannel.Red, ColorMappingChannel.None, ColorMappingChannel.None);
				}
					
				if (mapping.Alpha == ColorMappingChannel.Alpha)
					mapping = new ColorMapping(mapping.Red, mapping.Green, mapping.Blue, ColorMappingChannel.Green);
				
				if(!header.PixelFormat.Flags.HasFlag(PixelFormatFlags.AlphaPixels))
					mapping = new ColorMapping(mapping.Red, mapping.Green, mapping.Blue, ColorMappingChannel.One);
				
				mapping = new ColorMapping(mapping.Red, mapping.Red, mapping.Red, mapping.Alpha);
				format = new Format(type, FormatSemantic.UNorm, inputMapping, mapping);

				/*
				if (header.PixelFormat.Flags.HasFlag(PixelFormatFlags.AlphaPixels))
				{
					if (header.PixelFormat.RgbBitCount == 16)
					{
						FormatMappingChannel luminance;
						FormatMappingChannel alpha;

						if (header.PixelFormat.RBitMask == 0x00FF &&
						    header.PixelFormat.ABitMask == 0xFF00)
						{
							luminance = FormatMappingChannel.Red;
							alpha = FormatMappingChannel.Green;
						}
						else if (header.PixelFormat.RBitMask == 0xFF00 &&
						         header.PixelFormat.ABitMask == 0x00FF)
						{
							luminance = FormatMappingChannel.Green;
							alpha = FormatMappingChannel.Red;
						}
						else
						{
							throw new NotSupportedException("Unsupported DDS pixel format.");
						}

						format = new Format(FormatType.R8G8, FormatSemantic.UNorm,
							(luminance, alpha, FormatMappingChannel.Zero, FormatMappingChannel.Zero),
							(luminance, luminance, luminance, alpha));
					}
					else if (header.PixelFormat.RgbBitCount == 32)
					{
						FormatMappingChannel luminance;
						FormatMappingChannel alpha;

						if (header.PixelFormat.RBitMask == 0x0000FFFF &&
						    header.PixelFormat.ABitMask == 0xFFFF0000)
						{
							luminance = FormatMappingChannel.Red;
							alpha = FormatMappingChannel.Green;
						}
						else if (header.PixelFormat.RBitMask == 0xFFFF0000 &&
						         header.PixelFormat.ABitMask == 0x0000FFFF)
						{
							luminance = FormatMappingChannel.Green;
							alpha = FormatMappingChannel.Red;
						}
						else
						{
							throw new NotSupportedException("Unsupported DDS pixel format.");
						}

						format = new Format(FormatType.R16G16, FormatSemantic.UNorm,
							(luminance, alpha, FormatMappingChannel.Zero, FormatMappingChannel.Zero),
							(luminance, luminance, luminance, alpha));
					}
					else
					{
						throw new NotSupportedException("Unsupported DDS pixel format.");
					}
				}
				else
				{
					if (header.PixelFormat.RgbBitCount == 8)
					{
						format = new Format(FormatType.R8, FormatSemantic.UNorm,
							(FormatMappingChannel.Red, FormatMappingChannel.Zero, FormatMappingChannel.Zero, FormatMappingChannel.Zero),
							(FormatMappingChannel.Red, FormatMappingChannel.Red, FormatMappingChannel.Red, FormatMappingChannel.One));
					}
					else if (header.PixelFormat.RgbBitCount == 16)
					{
						format = new Format(FormatType.R16, FormatSemantic.UNorm,
							(FormatMappingChannel.Red, FormatMappingChannel.Zero, FormatMappingChannel.Zero, FormatMappingChannel.Zero),
							(FormatMappingChannel.Red, FormatMappingChannel.Red, FormatMappingChannel.Red, FormatMappingChannel.One));
					}
					else
					{
						throw new NotSupportedException("Unsupported DDS pixel format.");
					}
				}
				*/
			}
			else
			{
				throw new NotSupportedException("Unsupported DDS pixel format.");
			}		

			// Load image data.
			Image[,] images = new Image[faces, header.MipMapCount];
			for (int face = 0; face < faces; ++face)
			{
				int mmWidth = (int)header.Width;
				int mmHeight = (int)header.Height;
				int mmDepth = (int)header.Depth;

				if (mmDepth == 0)
					mmDepth = 1;
				
				for (int mip = 0; mip < header.MipMapCount; ++mip)
				{
					int rowLength = mmWidth * format.Size;
					int sliceSize = rowLength * mmHeight;
					int size = sliceSize * mmDepth;
					byte[] data = new byte[size];

					// Read image flipped.
					for (int slice = 0; slice < mmDepth; ++slice)
					{
						for (int row = 0; row < mmHeight; ++row)
						{
							stream.ReadOrThrow(data, slice * sliceSize + (mmHeight - 1 - row) * rowLength, rowLength);
						}
					}
					
					MemoryImage pixelBox = MemoryImage.Create(data, format, mmWidth, mmHeight, mmDepth);
					images[face, mip] = pixelBox;
					
					if (mmWidth > 1) mmWidth /= 2;
					if (mmHeight > 1) mmHeight /= 2;
					if (mmDepth > 1) mmDepth /= 2;
				}
			}

			return images;
		}

		#endregion
		
		#region Nested Types
		
		[Flags]
		private enum HeaderFlags : uint
		{
			Caps = 0x1,
			Height = 0x2,
			Width = 0x4,
			Pitch = 0x8,
			PixelFormat = 0x1000,
			MipMapCount = 0x20000,
			LinearSize = 0x80000,
			Depth = 0x800000
		}

		[Flags]
		private enum PixelFormatFlags : uint
		{
			AlphaPixels = 0x1,
			Alpha = 0x2,
			FourCC = 0x4,
			RGB = 0x40,
			YUV = 0x200,
			Luminance = 0x20000
		}

		[Flags]
		private enum Caps : uint
		{
			Complex = 0x8,
			MipMap = 0x400000,
			Texture = 0x1000
		}

		[Flags]
		private enum Caps2 : uint
		{
			CubeMap = 0x200,
			CubeMapPositiveX = 0x400,
			CubeMapNegativeX = 0x800,
			CubeMapPositiveY = 0x1000,
			CubeMapNegativeY = 0x2000,
			CubeMapPositiveZ = 0x4000,
			CubeMapNegativeZ = 0x8000,
			Volume = 0x200000
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct PixelFormat
		{
			public uint Size;
			public PixelFormatFlags Flags;
			public uint FourCC;
			public uint RgbBitCount;
			public uint RBitMask;
			public uint GBitMask;
			public uint BBitMask;
			public uint ABitMask;

			private static bool tryGetMapping(uint mask, uint maskR, uint maskG, uint maskB, uint maskA, out ColorMappingChannel mapping)
			{
				if (maskR != 0 && mask == maskR)
				{
					mapping = ColorMappingChannel.Red;
					return true;
				}

				if (maskG != 0 && mask == maskG)
				{
					mapping = ColorMappingChannel.Green;
					return true;
				}

				if (maskB != 0 && mask == maskB)
				{
					mapping = ColorMappingChannel.Blue;
					return true;
				}

				if (maskA != 0 && mask == maskA)
				{
					mapping = ColorMappingChannel.Alpha;
					return true;
				}

				mapping = ColorMappingChannel.Zero;
				return mask == 0;
			}

			public bool TryGetMapping(uint maskR, uint maskG, uint maskB, uint maskA, out ColorMapping mapping)
			{
				mapping = ColorMapping.RGBA;
				if (!tryGetMapping(this.RBitMask, maskR, maskG, maskB, maskA, out ColorMappingChannel mappingR))
					return false;
				if (!tryGetMapping(this.GBitMask, maskR, maskG, maskB, maskA, out ColorMappingChannel mappingG))
					return false;
				if (!tryGetMapping(this.BBitMask, maskR, maskG, maskB, maskA, out ColorMappingChannel mappingB))
					return false;
				if (!tryGetMapping(this.ABitMask, maskR, maskG, maskB, maskA, out ColorMappingChannel mappingA))
					return false;

				mapping = new ColorMapping(mappingR, mappingG, mappingB, mappingA);
				return true;
			}

			public bool AnyMask(uint mask)
			{
				return this.RBitMask == mask || this.GBitMask == mask || this.BBitMask == mask || this.ABitMask == mask;
			}
		}
		
		[StructLayout(LayoutKind.Sequential)]
		private struct Header
		{
			public uint Size;
			public HeaderFlags Flags;
			public uint Height;
			public uint Width;
			public uint PitchOrLinearSize;
			public uint Depth;
			public uint MipMapCount;
			public uint Reserved0;
			public uint Reserved1;
			public uint Reserved2;
			public uint Reserved3;
			public uint Reserved4;
			public uint Reserved5;
			public uint Reserved6;
			public uint Reserved7;
			public uint Reserved8;
			public uint Reserved9;
			public uint Reserved10;
			public PixelFormat PixelFormat;
			public Caps Caps;
			public Caps2 Caps2;
			public uint Caps3;
			public uint Caps4;
			public uint Reserved11;
		}

		#endregion
	}
}
