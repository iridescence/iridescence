﻿using System;
using System.IO;

namespace Iridescence.Graphics.IO
{
	/// <summary>
	/// Implements Targa (.tga) image reading and writing functions.
	/// </summary>
	public static class Tga
	{
		#region Methods

		private static Format getPixelFormat(int bpp)
		{
			switch (bpp)
			{
				case 8:
					return new Format(FormatType.R8, FormatSemantic.UNorm,
						(ColorMappingChannel.Red, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
						(ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.One));

				case 16:
					return new Format(FormatType.R5G6B5, FormatSemantic.UNorm, 
						(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.None),
						(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.One));

				case 24:
					return new Format(FormatType.R8G8B8, FormatSemantic.UNorm,
						(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.None),
						(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.One));

				case 32:
					return new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm, 
						(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.Alpha),
						(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.Alpha));
			}

			throw new NotSupportedException($"A bit depth of {bpp} is not supported for TGA images.");
		}

		/// <summary>
		/// Reads an image from a stream in the TARGA format.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static Image Read(Stream stream)
		{
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));

			// read header.
			byte[] header = stream.ReadOrThrow(18);

			int idLength = header[0];
			int paletteType = header[1];
			int imageType = header[2];
			uint colorMapOffset = header.ReadUInt16L(3);
			int colorMapLength = header.ReadUInt16L(5);
			int colorMapBPP = header[7];
			int width = header.ReadUInt16L(12);
			int height = header.ReadUInt16L(14);
			int bpp = header[16];

			// skip image ID.
			stream.ReadOrThrow(idLength);

			Format format;
			byte[] colorMapData = null;
			if (paletteType == 0)
			{
				format = getPixelFormat(bpp);
			}
			else
			{
				format = getPixelFormat(colorMapBPP);

				colorMapData = new byte[colorMapLength * (colorMapBPP >> 3)];
				stream.Read(colorMapData, 0, colorMapData.Length);
			}

			int imageDataPixelSize = (bpp >> 3);
			byte[] imageData = new byte[width * height * (paletteType == 0 ? format.Size : (bpp >> 3))];

			switch (imageType)
			{
				case 0: // no image data is present
					break;

				case 1: // indexed.
				case 2: // uncompressed true-color image
				case 3: // b&w image
				{
					int rowLength = width * imageDataPixelSize;
					for (int y = 0; y < height; y++)
						stream.ReadOrThrow(imageData, y * rowLength, rowLength);

					break;
				}

				case 9: // indexed + RLE.
				case 10: // true-color + RLE.
				case 11: // b&w + RLE.
				{
					byte[] pixel = new byte[imageDataPixelSize];

					int rowLength = width * imageDataPixelSize;
					int y = 0;
					int x = 0;
					int numPixels = width * height;

					while (numPixels > 0)
					{
						int controlByte = stream.ReadByte();
						if (controlByte < 0)
							throw new EndOfStreamException();

						bool repeat = (controlByte & 0x80) != 0;
						int length = (controlByte & 0x7F) + 1;

						if (repeat)
						{
							stream.ReadOrThrow(pixel, 0, imageDataPixelSize);

							for (int i = 0; i < length; i++)
							{
								Array.Copy(pixel, 0, imageData, y * rowLength + x * imageDataPixelSize, imageDataPixelSize);

								if (++x >= width)
								{
									x = 0;
									y++;
								}
							}
						}
						else
						{
							for (int i = 0; i < length; i++)
							{
								stream.ReadOrThrow(imageData, y * rowLength + x * imageDataPixelSize, imageDataPixelSize);

								if (++x >= width)
								{
									x = 0;
									y++;
								}
							}
						}

						numPixels -= length;
					}

					break;
				}

				default:
					throw new NotSupportedException($"TGA image type {imageType} is not supported.");
			}

			if (paletteType != 0)
			{
				// create func to read indices.
				Func<uint, uint> readIndex;
				if (bpp == 8)
				{
					readIndex = i => imageData[i] - colorMapOffset;
				}
				else if (bpp == 16)
				{
					readIndex = i =>
					{
						i *= 2;
						return ((uint)imageData[i]) | ((uint)imageData[i + 1] << 8);
					};
				}
				else if (bpp == 24)
				{
					readIndex = i =>
					{
						i *= 3;
						return ((uint)imageData[i]) | ((uint)imageData[i + 1] << 8) | ((uint)imageData[i + 2] << 16);
					};
				}
				else
				{
					readIndex = i =>
					{
						i *= 4;
						return ((uint)imageData[i]) | ((uint)imageData[i + 1] << 8) | ((uint)imageData[i + 2] << 16) | ((uint)imageData[i + 3] << 24);
					};
				}

				byte[] colors;
				if (colorMapBPP == 8)
				{
					colors = new byte[imageData.Length];
					for (uint i = 0; i < imageData.Length; i++)
						colors[i] = colorMapData[readIndex(i)];
				}
				else if (colorMapBPP == 16)
				{
					colors = new byte[imageData.Length * 2];
					for (uint i = 0, j = 0; i < imageData.Length; i++)
					{
						uint k = readIndex(i) << 1;
						colors[j++] = colorMapData[k++];
						colors[j++] = colorMapData[k];
					}
				}
				else if (colorMapBPP == 24)
				{
					colors = new byte[imageData.Length * 3];
					for (uint i = 0, j = 0; i < imageData.Length; i++)
					{
						uint k = readIndex(i) * 3;
						colors[j++] = colorMapData[k++];
						colors[j++] = colorMapData[k++];
						colors[j++] = colorMapData[k];
					}
				}
				else
				{
					colors = new byte[imageData.Length * 4];
					for (uint i = 0, j = 0; i < imageData.Length; i++)
					{
						uint k = readIndex(i) << 2;
						colors[j++] = colorMapData[k++];
						colors[j++] = colorMapData[k++];
						colors[j++] = colorMapData[k++];
						colors[j++] = colorMapData[k];
					}
				}
				imageData = colors;
			}

			return new MemoryImage(imageData, format, width, height);
		}

		/// <summary>
		/// Writes an image to a stream in the TARGA format.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="image"></param>
		public static void Write(Stream stream, ReadOnlyImage image)
		{
			byte[] buffer = new byte[image.Width * image.Height * 4];
			MemoryImage box = new MemoryImage(buffer, new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm, 
				(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.Alpha),
				(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.Alpha)), image.Width, image.Height);
			image.Read(box);

			stream.WriteByte(0); // id length.
			stream.WriteByte(0); // palette type.
			stream.WriteByte(2); // image type.
			stream.WriteUInt16L(0); // color map offset.
			stream.WriteUInt16L(0); // color map length.
			stream.WriteByte(0); // color map bpp.
			stream.WriteUInt16L(0); // x origin.
			stream.WriteUInt16L(0); // y origin.
			stream.WriteUInt16L((ushort)image.Width); // width.
			stream.WriteUInt16L((ushort)image.Height); // height.
			stream.WriteByte(32); // bpp.
			stream.WriteByte(0);
			stream.Write(buffer, 0, buffer.Length);
		}

		#endregion
	}
}
