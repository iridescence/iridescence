﻿using System;
using System.IO;

namespace Iridescence.Graphics.IO
{
	/// <summary>
	/// Implements a decoder for Windows bitmap (.bmp) images.
	/// </summary>
	public static class Bmp
	{
		#region Methods

		/// <summary>
		/// Reads a BMP file from the specified stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static Image Read(Stream stream)
		{
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));

			if (stream.ReadUInt16L() != 0x4D42)
				throw new InvalidDataException("Input is not a bitmap file.");

			stream.ReadInt32L(); // file size, not important.
			stream.ReadInt32L(); // idk

			int dataOffset = stream.ReadInt32L();

			if (stream.ReadInt32L() != 40) // header size.
				throw new InvalidDataException("Invalid header size.");

			int width = stream.ReadInt32L();
			int height = stream.ReadInt32L();
			bool flip = false;

			if (height < 0)
			{
				height = -height;
				flip = true;
			}

			stream.ReadInt16L(); // planes - obsolete.

			int bpp = stream.ReadInt16L();
			int paletteEntries = 0;
			int rowSize;
			Format format;

			switch (bpp)
			{
				case 1:
					paletteEntries = 2;
					format = new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm);
					rowSize = width;
					break;

				case 4:
					paletteEntries = 16;
					format = new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm);
					rowSize = width;
					break;

				case 8:
					paletteEntries = 256;
					format = new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm);
					rowSize = width;
					break;

				case 16:
					format = new Format(FormatType.R5G5B5A1, FormatSemantic.UNorm);
					rowSize = width * 2;
					break;

				case 24:
					format = new Format(FormatType.R8G8B8, FormatSemantic.UNorm);
					rowSize = width * 3;
					break;

				case 32:
					format = new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm);
					rowSize = width * 4;
					break;

				default:
					throw new InvalidDataException("Invalid bitmap BPP.");
			}

			// TODO: maybe support for RLE compression.
			int compression = stream.ReadInt32L();
			if (compression != 0)
				throw new NotSupportedException("Compressed bitmaps are not supported.");

			stream.ReadInt32L(); // bitmap data size.
			stream.ReadInt32L(); // x DPI.
			stream.ReadInt32L(); // y DPI.

			int usedPaletteEntries = stream.ReadInt32L();
			if (usedPaletteEntries > 0)
				paletteEntries = usedPaletteEntries;

			stream.ReadInt32L(); // biClrImportant, whatever that is supposed to do.

			// decode image with palette.
			if (paletteEntries > 0)
			{
				// read palette.
				uint[] palette = new uint[paletteEntries];
				for (int i = 0; i < paletteEntries; i++)
					palette[i] = stream.ReadUInt32L() | 0xFF000000u;

				// just in case we can seek, do it, but it should work without.
				if (stream.CanSeek)
					stream.Position = dataOffset;

				byte[] row = new byte[rowSize];
				uint[] data = new uint[width * height];

				// read one row at a time and use the palette.
				for (int y = 0; y < height; y++)
				{
					int offset = (flip ? height - 1 - y : y) * rowSize;
					stream.Read(row, 0, rowSize);
					for (int x = 0; x < width; x++)
						data[offset + x] = palette[row[x]];
				}

				return MemoryImage.Create(data, format, width, height);
			}
			else
			{
				// just in case we can seek, do it, but it should work without.
				if (stream.CanSeek)
					stream.Position = dataOffset;

				// read data.
				byte[] data = new byte[Format.GetSize(format.Type, width, height, 1)];

				for (int y = 0; y < height; y++)
				{
					int offset = (flip ? height - 1 - y : y) * rowSize;
					stream.Read(data, offset, rowSize);
				}

				return MemoryImage.Create(data, format, width, height);
			}
		}
		
		#endregion
	}
}
