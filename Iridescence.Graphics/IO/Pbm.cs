﻿using System;
using System.IO;
using System.Text;

namespace Iridescence.Graphics.IO
{
	/// <summary>
	/// Implements the Netpbm image formats.
	/// </summary>
	public static class Pbm
	{
		#region Fields

		private static readonly Format grayscale8 = new Format(FormatType.R8, FormatSemantic.UNorm,
			(ColorMappingChannel.Red, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
			(ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.One));
		
		private static readonly Format grayscale16 = new Format(FormatType.R16, FormatSemantic.UNorm,
			(ColorMappingChannel.Red, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
			(ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.Red, ColorMappingChannel.One));
		
		#endregion

		#region Nested Types

		private sealed class Reader
		{
			private readonly Stream stream;
			private readonly StringBuilder stringBuilder;

			public Reader(Stream stream)
			{
				this.stream = stream;
				this.stringBuilder = new StringBuilder();
			}

			public void SkipToEndOfLine()
			{
				while (true)
				{
					int input = this.stream.ReadByte();
					if (input == -1)
						break;

					if (input == '\n')
						break;
				}
			}

			public bool ReadToken()
			{
				int count = 0;
				bool skippingWhitespaces = true;

				while (true)
				{
					int input = this.stream.ReadByte();
					if (input == -1)
						break;

					char c = (char)input;

					if (char.IsWhiteSpace(c))
					{
						if (skippingWhitespaces)
							continue;

						break;
					}

					if (c == '#')
					{
						this.SkipToEndOfLine();

						if (skippingWhitespaces)
							continue;

						break;
					}

					skippingWhitespaces = false;
					this.stringBuilder.Append((char)input);
					++count;
				}

				return count > 0;
			}

			public string ReadString()
			{
				this.stringBuilder.Clear();

				if (!this.ReadToken())
					return null;

				return this.stringBuilder.ToString();
			}

			public int ReadInteger()
			{
				this.stringBuilder.Clear();

				if (!this.ReadToken())
					throw new EndOfStreamException();

				int number = 0;
				int factor = 1;
				for (int i = this.stringBuilder.Length - 1; i >= 0; i--)
				{
					int x = this.stringBuilder[i] - '0';
					if(x < 0 || x > 9)
						throw new FormatException("Invalid character, expected number.");

					number += factor * x;
					factor *= 10;
				}

				return number;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Reads a Netpbm image from the specified stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static Image Read(Stream stream)
		{
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));

			Reader reader = new Reader(stream);

			string magic = reader.ReadString();

			if (magic == "P1") // ascii bitmap.
			{
				int width = reader.ReadInteger();
				int height = reader.ReadInteger();

				byte[] data = new byte[width * height];

				for (int y = 0; y < height; ++y)
				{
					int offset = (height - y - 1) * width;
					for (int x = 0; x < width; ++x)
					{
						int n = reader.ReadInteger();
						if(n != 0 && n != 1)
							throw new FormatException($"Expected either 0 or 1 in bitmap data, but found {n}.");

						data[offset++] = (byte)(n - 1);
					}
				}

				return MemoryImage.Create(data, grayscale8, width, height);
			}

			if (magic == "P2") // ascii grayscale.
			{
				int width = reader.ReadInteger();
				int height = reader.ReadInteger();
				int max = reader.ReadInteger();

				if (max <= byte.MaxValue)
				{
					byte[] data = new byte[width * height];

					for (int y = 0; y < height; ++y)
					{
						int offset = (height - y - 1) * width;
						for (int x = 0; x < width; ++x)
						{
							int n = reader.ReadInteger();
							if (n > max)
								throw new FormatException($"Pixel value is out of range. Maximum is {max}, pixel is {n}.");

							data[offset++] = (byte)((n * byte.MaxValue) / max);
						}
					}

					return MemoryImage.Create(data, grayscale8, width, height);
				}

				if (max <= ushort.MaxValue)
				{
					ushort[] data = new ushort[width * height];

					for (int y = 0; y < height; ++y)
					{
						int offset = (height - y - 1) * width;
						for (int x = 0; x < width; ++x)
						{
							int n = reader.ReadInteger();
							if (n > max)
								throw new FormatException($"Pixel value is out of range. Maximum is {max}, pixel is {n}.");

							data[offset++] = (byte)((n * ushort.MaxValue) / max);
						}
					}

					return MemoryImage.Create(data, grayscale16, width, height);
				}

				throw new NotSupportedException($"Maximum value of {max} is not supported.");
			}

			if (magic == "P3") // ascii rgb.
			{
				int width = reader.ReadInteger();
				int height = reader.ReadInteger();
				int max = reader.ReadInteger();

				if (max <= byte.MaxValue)
				{
					byte[] data = new byte[width * height * 3];

					for (int y = 0; y < height; ++y)
					{
						int offset = (height - y - 1) * width * 3;
						for (int x = 0; x < width; ++x)
						{
							for (int i = 0; i < 3; ++i)
							{
								int n = reader.ReadInteger();
								if (n > max)
									throw new FormatException($"Pixel value is out of range. Maximum is {max}, pixel is {n}.");

								data[offset++] = (byte)((n * byte.MaxValue) / max);
							}
						}
					}

					return MemoryImage.Create(data, new Format(FormatType.R8G8B8, FormatSemantic.UNorm), width, height);
				}

				if (max <= ushort.MaxValue)
				{
					ushort[] data = new ushort[width * height * 3];

					for (int y = 0; y < height; ++y)
					{
						int offset = (height - y - 1) * width * 3;
						for (int x = 0; x < width; ++x)
						{
							for (int i = 0; i < 3; ++i)
							{
								int n = reader.ReadInteger();
								if (n > max)
									throw new FormatException($"Pixel value is out of range. Maximum is {max}, pixel is {n}.");

								data[offset++] = (byte)((n * ushort.MaxValue) / max);
							}
						}
					}

					return MemoryImage.Create(data, new Format(FormatType.R16G16B16, FormatSemantic.UNorm), width, height);
				}

				throw new NotSupportedException($"Maximum value of {max} is not supported.");
			}

			if (magic == "P4") // binary bitmap.
			{
				int width = reader.ReadInteger();
				int height = reader.ReadInteger();

				byte[] data = new byte[width * height];

				for (int y = 0; y < height; ++y)
				{
					int offset = (height - y - 1) * width;
					byte currentByte = 0;
					for (int x = 0; x < width; ++x)
					{
						if ((x & 7) == 0)
							currentByte = stream.ReadByteOrThrow();
						
						int bit = 1 << (7 - (x & 7));
						if ((currentByte & bit) == 0)
							data[offset++] = 255;
						else
							data[offset++] = 0;
					}
				}

				return MemoryImage.Create(data, grayscale8, width, height);
			}

			if (magic == "P5") // binary grayscale.
			{
				int width = reader.ReadInteger();
				int height = reader.ReadInteger();
				int max = reader.ReadInteger();

				if (max <= byte.MaxValue)
				{
					byte[] data = new byte[width * height];

					for (int y = 0; y < height; ++y)
					{
						int offset = (height - y - 1) * width;
						stream.ReadOrThrow(data, offset, width);
					}

					if (max != byte.MaxValue)
					{
						for (int i = 0; i < data.Length; ++i)
						{
							data[i] = (byte)((data[i] * byte.MaxValue) / max);
						}
					}

					return MemoryImage.Create(data, grayscale8, width, height);
				}

				if (max <= ushort.MaxValue)
				{
					ushort[] data = new ushort[width * height];

					using (BufferStream temp = new BufferStream(data, false, true))
					{
						for (int y = 0; y < height; ++y)
						{
							int offset = (height - y - 1) * width;
							temp.Position = 2 * offset;
							stream.CopyTo(temp, 2 * width, 4096);
						}
					}

					if (max != ushort.MaxValue)
					{
						for (int i = 0; i < data.Length; ++i)
						{
							data[i] = (ushort)((data[i] * ushort.MaxValue) / max);
						}
					}

					return MemoryImage.Create(data, grayscale16, width, height);
				}
			}

			if (magic == "P6") // binary rgb.
			{
				int width = reader.ReadInteger();
				int height = reader.ReadInteger();
				int max = reader.ReadInteger();

				if (max <= byte.MaxValue)
				{
					byte[] data = new byte[width * height * 3];

					for (int y = 0; y < height; ++y)
					{
						int offset = (height - y - 1) * width * 3;
						stream.ReadOrThrow(data, offset, width * 3);
					}

					if (max != byte.MaxValue)
					{
						for (int i = 0; i < data.Length; ++i)
						{
							data[i] = (byte)((data[i] * byte.MaxValue) / max);
						}
					}

					return MemoryImage.Create(data, new Format(FormatType.R8G8B8, FormatSemantic.UNorm), width, height);
				}

				if (max <= ushort.MaxValue)
				{
					ushort[] data = new ushort[width * height * 3];

					using (BufferStream temp = new BufferStream(data, false, true))
					{
						for (int y = 0; y < height; ++y)
						{
							int offset = (height - y - 1) * width  * 3;
							temp.Position = 2 * offset;
							stream.CopyTo(temp, 2 * width * 3, 4096);
						}
					}

					if (max != ushort.MaxValue)
					{
						for (int i = 0; i < data.Length; ++i)
						{
							data[i] = (ushort)((data[i] * ushort.MaxValue) / max);
						}
					}

					return MemoryImage.Create(data, new Format(FormatType.R16G16B16, FormatSemantic.UNorm), width, height);
				}
			}

			throw new FormatException($"Invalid magic \"{magic}\". Supported types are P1, P2, P3, P4, P5 and P6.");
		}
		
		#endregion
	}
}
