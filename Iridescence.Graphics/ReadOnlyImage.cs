﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents an abstract read-only image.
	/// Images are three-dimensional containers for arbitrary pixel data.
	/// The pixel data format is defined by a <see cref="Format"/>.
	/// </summary>
	[Serializable]
	public abstract class ReadOnlyImage
	{
		#region Properties

		/// <summary>
		/// Gets the pixel format that best describes data in this image.
		/// </summary>
		/// <remarks>
		/// This might not be the exact format and can also be <see cref="Graphics.Format.None"/>, depending on the implementation.
		/// It is however supposed to be the closest match to make it possible to create compatible images, apply filters etc. without requiring the user to specify the format.
		/// </remarks>
		public Format Format { get; }

		/// <summary>
		/// Gets the width of the pixel buffer.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the height of the pixel buffer.
		/// </summary>
		public int Height { get; }

		/// <summary>
		/// Gets the depth of the pixel buffer.
		/// </summary>
		public int Depth { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new image with the specified format and dimensions.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="depth"></param>
		protected ReadOnlyImage(Format format, int width, int height, int depth)
		{
			if (width < 1 || height < 1 || depth < 1)
				throw new ArgumentException("Invalid image dimensions.");

			this.Format = format;
			this.Width = width;
			this.Height = height;
			this.Depth = depth;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Reads pixel data from a region of this image.
		/// </summary>
		/// <param name="dest">The destination pixel box to store the pixels in.</param>
		/// <param name="sourceX">The source X coordinate.</param>
		/// <param name="sourceY">The source Y coordinate.</param>
		/// <param name="sourceZ">The source Z coordinate.</param>
		public abstract void Read(MemoryImage dest, int sourceX, int sourceY, int sourceZ);

		/// <summary>
		/// Reads pixel data of the whole image.
		/// </summary>
		/// <param name="dest">The destination pixel box to store the pixels in.</param>
		public void Read(MemoryImage dest)
		{
			this.Read(dest, 0, 0, 0);
		}

		/// <summary>
		/// Asynchronously reads pixel data from a region of this image.
		/// </summary>
		/// <param name="dest">The destination pixel box to store the pixels in.</param>
		/// <param name="sourceX">The source X coordinate.</param>
		/// <param name="sourceY">The source Y coordinate.</param>
		/// <param name="sourceZ">The source Z coordinate.</param>
		/// <returns></returns>
		public virtual Task ReadAsync(MemoryImage dest, int sourceX, int sourceY, int sourceZ)
		{
			this.Read(dest, sourceX, sourceY, sourceZ);
			return Task.CompletedTask;
		}

		/// <summary>
		/// Asynchronously reads pixel data of the whole image.
		/// </summary>
		/// <param name="dest">The destination pixel box to store the pixels in.</param>
		/// <returns></returns>
		public Task ReadAsync(MemoryImage dest)
		{
			return this.ReadAsync(dest, 0, 0, 0);
		}
		
		/// <summary>
		/// Copies pixel data from a region of this image to a region of the destination image.
		/// </summary>
		/// <param name="dest">The destination image.</param>
		/// <param name="sourceX">The source X coordinate.</param>
		/// <param name="sourceY">The source Y coordinate.</param>
		/// <param name="sourceZ">The source Z coordinate.</param>
		/// <param name="destX">The destination X coordinate.</param>
		/// <param name="destY">The destination Y coordidate.</param>
		/// <param name="destZ">The destination Z coordinate.</param>
		/// <param name="width">The width of the region to copy.</param>
		/// <param name="height">The height of the region to copy.</param>
		/// <param name="depth">The depth of the region to copy.</param>
		public virtual void CopyTo(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			this.ThrowIfOutOfBounds(sourceX, sourceY, sourceZ, width, height, depth);
			dest.ThrowIfOutOfBounds(destX, destY, destZ, width, height, depth);

			MemoryImage myPixels = MemoryImage.Allocate(this.Format, width, height, depth);
			this.Read(myPixels, sourceX, sourceY, sourceZ);
			dest.Write(myPixels, destX, destY, destZ);
		}

		/// <summary>
		/// Copies the whole image to the specified image.
		/// </summary>
		/// <param name="dest">The destination image.</param>
		public void CopyTo(Image dest)
		{
			this.CopyTo(dest, 0, 0, 0, 0, 0, 0, this.Width, this.Height, this.Depth);
		}

		/// <summary>
		/// Asynchronously copies pixel data from a region of this image to a region of the destination image.
		/// </summary>
		/// <param name="dest">The destination image.</param>
		/// <param name="sourceX">The source X coordinate.</param>
		/// <param name="sourceY">The source Y coordinate.</param>
		/// <param name="sourceZ">The source Z coordinate.</param>
		/// <param name="destX">The destination X coordinate.</param>
		/// <param name="destY">The destination Y coordidate.</param>
		/// <param name="destZ">The destination Z coordinate.</param>
		/// <param name="width">The width of the region to copy.</param>
		/// <param name="height">The height of the region to copy.</param>
		/// <param name="depth">The depth of the region to copy.</param>
		public virtual async Task CopyToAsync(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			this.ThrowIfOutOfBounds(sourceX, sourceY, sourceZ, width, height, depth);
			dest.ThrowIfOutOfBounds(destX, destY, destZ, width, height, depth);

			MemoryImage myPixels = MemoryImage.Allocate(this.Format, width, height, depth);
			await this.ReadAsync(myPixels, sourceX, sourceY, sourceZ);
			await dest.WriteAsync(myPixels, destX, destY, destZ);
		}

		/// <summary>
		/// Asynchronously copies the whole image to the specified image.
		/// </summary>
		/// <param name="dest">The destination image.</param>
		public Task CopyToAsync(Image dest)
		{
			return this.CopyToAsync(dest, 0, 0, 0, 0, 0, 0, this.Width, this.Height, this.Depth);
		}

		/// <summary>
		/// Returns a pixel box that contains the pixel data of this image.
		/// </summary>
		/// <param name="alwaysCopy">
		/// Specified whether the image has to be copied, even if it already is a PixelBox or the data can be used as-is in a PixelBox.
		/// Setting this to false is useful if you plan to access the data read-only.
		/// In case the image is already a PixelBox, the input is simply casted to PixelBox and no copying is done.
		/// For images whose data is not readily available (e.g. GPU images), the data is copied even if alwaysCopy is false.
		/// </param>
		/// <returns>A new PixelBox with the pixel data of this image.</returns>
		public virtual MemoryImage ToMemoryImage(bool alwaysCopy)
		{
			MemoryImage box = MemoryImage.Allocate(this.Format, this.Width, this.Height, this.Depth);
			this.Read(box, 0, 0, 0);
			return box;
		}

		#endregion
	}
}
