﻿namespace Iridescence.Graphics
{
	internal delegate void BlockCopyFunc(ref byte src, ref byte dst, int srcX, int srcY, int srcZ, int dstX, int dstY, int dstZ, int countX, int countY, int countZ);
}
