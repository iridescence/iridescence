﻿using System;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a three-dimensional box of pixels.
	/// Provides access to raw pixel data (i.e. byte-based).
	/// </summary>
	public readonly ref struct PixelBox
	{
		#region Fields

		private readonly Span<byte> span;
		private readonly PixelDimension dimX;
		private readonly PixelDimension dimY;
		private readonly PixelDimension dimZ;
		private readonly IPixelInfo info;

		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the span of the slice.
		/// </summary>
		public Span<byte> Span => this.span;

		/// <summary>
		/// Gets the X dimension specification.
		/// </summary>
		public PixelDimension XDimension => this.dimX;

		/// <summary>
		/// Gets the Y dimension specification.
		/// </summary>
		public PixelDimension YDimension => this.dimY;

		/// <summary>
		/// Gets the Z dimension specification.
		/// </summary>
		public PixelDimension ZDimension => this.dimZ;

		/// <summary>
		/// Gets the width (in pixels) of the box.
		/// </summary>
		public int Width => this.dimX.Count;
		
		/// <summary>
		/// Gets the height (in pixels) of the box.
		/// </summary>
		public int Height => this.dimY.Count;
		
		/// <summary>
		/// Gets the depth (in pixels) of the box.
		/// </summary>
		public int Depth => this.dimZ.Count;

		/// <summary>
		/// Gets the slices of the box when sliced along the X axis.
		/// </summary>
		public PixelSliceCollection XSlices
		{
			get
			{
				PixelDimension x = new PixelDimension(this.dimX.Stride, this.dimX.ElementSize, this.dimX.Count);
				PixelDimension y = new PixelDimension(this.dimZ.Stride, x.Length, this.dimZ.Count);
				PixelDimension z = new PixelDimension(this.dimY.Stride, y.Length, this.dimY.Count);
				return new PixelSliceCollection(this.span, x, y, z, this.info);
			}
		}

		/// <summary>
		/// Gets the slices of the box when sliced along the Y axis.
		/// </summary>
		public PixelSliceCollection YSlices
		{
			get
			{
				PixelDimension dimX2 = new PixelDimension(this.dimZ.Stride, this.dimX.ElementSize, this.dimZ.Count);
				PixelDimension dimY2 = new PixelDimension(this.dimY.Stride, dimX2.Length, this.dimY.Count);
				PixelDimension dimZ2 = new PixelDimension(this.dimX.Stride, dimY2.Length, this.dimX.Count);
				return new PixelSliceCollection(this.span, dimX2, dimY2, dimZ2, this.info);
			}
		}

		/// <summary>
		/// Gets the slices of the box when sliced along the Z axis.
		/// </summary>
		public PixelSliceCollection ZSlices => new PixelSliceCollection(this.span, this.dimX, this.dimY, this.dimZ, this.info);

		public bool IsEmpty => this.dimX.IsEmpty || this.dimY.IsEmpty || this.dimZ.IsEmpty;

		public bool IsConsecutive => this.dimX.IsConsecutive && this.dimY.IsConsecutive && this.dimZ.IsConsecutive;

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PixelBox"/> class. 
		/// </summary>
		public PixelBox(Span<byte> span, in PixelDimension dimX, in PixelDimension dimY, in PixelDimension dimZ, IPixelInfo info = null)
		{
			this.span = span.Slice(0, dimZ.Length);
			this.dimX = dimX;
			this.dimY = dimY;
			this.dimZ = dimZ;
			this.info = info;
		}

		#endregion
		
		#region Methods

		/// <summary>
		/// Searches all three slicing kinds for a way to represent the box consecutively.
		/// </summary>
		/// <param name="slices"></param>
		/// <param name="kind"></param>
		/// <returns></returns>
		public bool TryGetConsecutiveSlices(out PixelSliceCollection slices, out PixelSliceKind kind)
		{
			slices = this.ZSlices;
			if (slices.IsConsecutive)
			{
				kind = PixelSliceKind.Z;
				return true;
			}

			slices = this.YSlices;
			if (slices.IsConsecutive)
			{
				kind = PixelSliceKind.Y;
				return true;
			}

			slices = this.XSlices;
			if (slices.IsConsecutive)
			{
				kind = PixelSliceKind.X;
				return true;
			}

			slices = default;
			kind = 0;
			return false;
		}

		/// <summary>
		/// Clears the contents of this pixel box.
		/// </summary>
		public void Clear()
		{
			if (this.IsConsecutive)
			{
				this.span.Clear();
			}
			else
			{
				if (!this.TryGetConsecutiveSlices(out PixelSliceCollection slices, out _))
					slices = this.ZSlices;

				foreach (PixelSlice slice in slices)
				{
					slice.Clear();
				}
			}
		}
		
		/// <summary>
		/// Sets each pixel in the box to the specified raw pixel data.
		/// The number of bytes in the specified <paramref name="pixel"/> must match the element size of the pixel data.
		/// </summary>
		/// <param name="pixel"></param>
		public void FillRaw(ReadOnlySpan<byte> pixel)
		{
			if (pixel.Length != this.dimX.ElementSize)
				throw new ArgumentException("Fill data length must match pixel size.", nameof(pixel));

			if (this.IsEmpty)
				return;

			if (this.IsConsecutive)
			{
				this.ZSlices[0].FillRaw(pixel);
				PixelHelper.FillLogN(this.span, this.dimZ.Length);
			}
			else
			{
				if (!this.TryGetConsecutiveSlices(out PixelSliceCollection slices, out _))
					slices = this.ZSlices;

				foreach (PixelSlice slice in slices)
				{
					slice.FillRaw(pixel);
				}
			}
		}
		
		/// <summary>
		/// Copies this <see cref="PixelBox"/> to the specified <see cref="PixelBox"/>.
		/// Performs no data conversion and copies pixel byte data as is.
		/// </summary>
		/// <param name="dest">The destination <see cref="PixelBox"/>. Must be at least as big as this <see cref="PixelBox"/>.</param>
		public void CopyToRaw(in PixelBox dest)
		{
			if (dest.Width < this.Width || dest.Height < this.Height || dest.Depth < this.Depth)
				throw new ArgumentException("Destination box is too small.", nameof(dest));
			
			if (dest.dimX.ElementSize != this.dimX.ElementSize)
				throw new ArgumentException("Pixels sizes do not match.", nameof(dest));

			if (this.IsConsecutive && dest.IsConsecutive)
			{
				// Fast path for when both boxews are consecutive and have no unused space between pixels.
				this.span.CopyTo(dest.span);
			}
			else
			{
				// Slow path, copies each row individually.
				PixelSliceCollection sourceSlices = this.ZSlices;
				PixelSliceCollection destSlices = dest.ZSlices;

				for (int i = 0; i < this.Depth; ++i)
				{
					sourceSlices[i].CopyToRaw(destSlices[i]);
				}
			}
		}

		/// <summary>
		/// Returns a sub-region of this <see cref="PixelSlice"/>, starting from the specified pixel position and containing the specified number of pixels in each dimension.
		/// </summary>
		/// <param name="startX">The new starting X pixel.</param>
		/// <param name="startY">The new starting Y pixel.</param>
		/// <param name="startZ">The new starting Z pixel.</param>
		/// <param name="lengthX">The number of pixels in X direction.</param>
		/// <param name="lengthY">The number of pixels in Y direction.</param>
		/// <param name="lengthZ">The number of pixels in Z direction.</param>
		/// <returns></returns>
		public PixelBox Slice(int startX, int startY, int startZ, int lengthX, int lengthY, int lengthZ)
		{
			if ((uint)startX > (uint)this.Width || (uint)lengthX > (uint)(this.Width - startX) ||
			    (uint)startY > (uint)this.Height || (uint)lengthY > (uint)(this.Height - startY) ||
				(uint)startZ > (uint)this.Depth || (uint)lengthZ > (uint)(this.Depth - startZ))
				throw new ArgumentOutOfRangeException();

			PixelDimension x = new PixelDimension(this.dimX.Stride, this.dimX.ElementSize, lengthX);
			PixelDimension y = new PixelDimension(this.dimY.Stride, x.Length, lengthY);
			PixelDimension z = new PixelDimension(this.dimZ.Stride, y.Length, lengthZ);
			return new PixelBox(this.span.Slice(x.Stride * startX + y.Stride * startY + z.Stride * startZ, z.Length), x, y, z);
		}

		/// <summary>
		/// Returns a sub-region of this <see cref="PixelSlice"/>, starting from the specified pixel position.
		/// </summary>
		/// <param name="startX">The new starting X pixel.</param>
		/// <param name="startY">The new starting Y pixel.</param>
		/// <param name="startZ">The new starting Z pixel.</param>
		/// <returns></returns>
		public PixelBox Slice(int startX, int startY, int startZ)
		{
			return this.Slice(startX, startY, startZ, this.Width - startX, this.Height - startY, this.Depth - startZ);
		}

		#endregion
	}
}
