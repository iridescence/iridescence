﻿using System;
using System.Runtime.InteropServices;
using Iridescence.Memory;

namespace Iridescence.Graphics
{
	[StructLayout(LayoutKind.Sequential)]
	[Serializable]
	[CastExtensions(typeof(Color4Cast))]
	[ColorStruct(4)]
	public struct Color4<T> : IColor4<T>
	{
		#region Fields
		
		public T R;
		public T G;
		public T B;
		public T A;

		#endregion
		
		#region Properties
		
		T IColor2<T>.R
		{
			get => this.R;
			set => this.R = value;
		}

		T IColor2<T>.G
		{
			get => this.G;
			set => this.G = value;
		}

		T IColor3<T>.B
		{
			get => this.B;
			set => this.B = value;
		}

		T IColor4<T>.A
		{
			get => this.A;
			set => this.A = value;
		}

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Color4"/> class. 
		/// </summary>
		public Color4(T r, T g, T b, T a)
		{
			this.R = r;
			this.G = g;
			this.B = b;
			this.A = a;
		}

		#endregion
		
		#region Methods
		
		#endregion
	}

	internal static class Color4Cast
	{
		public static Color4<UNorm8> UNorm8To16(in Color4<UNorm16> v)
		{
			Color4<UNorm8> result;
			result.R = (UNorm8)v.R.SingleValue;
			result.G = (UNorm8)v.G.SingleValue;
			result.B = (UNorm8)v.B.SingleValue;
			result.A = (UNorm8)v.A.SingleValue;
			return result;
		}
	}
}
