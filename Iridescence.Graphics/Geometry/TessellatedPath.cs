namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a tessellated sub-path of a <see cref="Tessellator"/>.
	/// </summary>
	public sealed class TessellatedPath
	{
		#region Fields

		internal int PointOffset;
		internal int PointCount;
		internal int LastPoint => this.PointOffset + this.PointCount - 1;

		internal int NumBevel;
		internal PathWinding Winding;
		internal bool Closed;
		internal bool Convex;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the tessellator.
		/// </summary>
		public PathTessellator Tessellator { get; }

		/// <summary>
		/// Gets the index of the first fill vertex.
		/// </summary>
		public int FillVertexOffset { get; internal set; }

		/// <summary>
		/// Gets the number of fill vertices.
		/// </summary>
		public int FillVertexCount { get; internal set; }

		/// <summary>
		/// Gets the index of the first stroke vertex.
		/// </summary>
		public int StrokeVertexOffset { get; internal set; }

		/// <summary>
		/// Gets the number of stroke vertices.
		/// </summary>
		public int StrokeVertexCount { get; internal set; }

		#endregion

		#region Constructors

		internal TessellatedPath(PathTessellator tessellator)
		{
			this.Tessellator = tessellator;
		}

		#endregion

		#region Methods

		#endregion
	}
}