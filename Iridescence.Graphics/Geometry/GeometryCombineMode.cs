﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines the different geometry combination modes.
	/// </summary>
	public enum GeometryCombineMode
	{
		/// <summary>
		/// The second geometry is excluded from the first, i.e. A-B.
		/// </summary>
		Exclude,

		/// <summary>
		/// The intersection of the two geometries is used.
		/// </summary>
		Intersect,

		/// <summary>
		/// The two geometries are merged into one, i.e. A+B.
		/// </summary>
		Union,

		/// <summary>
		/// The two geometries are combined by taking the first with out the second plus the second without the first, i.e. (A-B) + (B-A).
		/// </summary>
		Xor
	}
}
