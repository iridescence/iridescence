﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines sweep directions.
	/// </summary>
	public enum SweepDirection
	{
		Clockwise,
		CounterClockwise,
	}
}
