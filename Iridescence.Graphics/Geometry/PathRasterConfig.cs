namespace Iridescence.Graphics
{
	/// <summary>
	/// Specifies the path rasterization configuration.
	/// </summary>
	internal struct PathRasterConfig
	{
		#region Fields

		/// <summary>
		/// The number of pixels per unit.
		/// </summary>
		public readonly double PixelRatio;

		/// <summary>
		/// 
		/// </summary>
		public readonly double TessTol;

		/// <summary>
		/// The smallest distance.
		/// </summary>
		public readonly double DistTol;

		public readonly double FringeWidth;

		#endregion

		#region Constructors

		public PathRasterConfig(double ratio)
		{
			this.PixelRatio = ratio;
			this.TessTol = 1.0 / ratio;
			this.DistTol = 0.01 / ratio;
			this.FringeWidth = 1.0 / ratio;
		}

		#endregion
	}
}