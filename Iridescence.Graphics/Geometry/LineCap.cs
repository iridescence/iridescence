namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines line cap modes.
	/// </summary>
	public enum LineCap
	{
		Butt,
		Round,
		Square
	}
}