﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	[Flags]
	public enum PathTessellatorPointFlags : byte
	{
		Corner = 0x01,
		Left = 0x02,
		Bevel = 0x04,
		InnerBevel = 0x08,
	}

	/// <summary>
	/// Represents a path tessellator that can be used to retrieve vertices/triangles from a <see cref="PathFigure"/>.
	/// </summary>
	public sealed class PathTessellator
	{
		#region Nested Types

		private struct Point
		{
			public Vector2D Position;
			public Vector2D Delta;
			public double Length;
			public Vector2D Dm;
			public PathTessellatorPointFlags Flags;
		}

		#endregion

		#region Fields

		private readonly PathRasterConfig config;

		private Vector2D boundsMin;
		private Vector2D boundsMax;

		private readonly List<TessellatedPath> paths;
		private Point[] points;
		private int pointCount;
		private Vector2D[] vertices;
		private int vertexCount;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets a list that contains the tessellated paths.
		/// </summary>
		public IReadOnlyList<TessellatedPath> Paths => this.paths;

		/// <summary>
		/// Gets the position of the last point added to the tessellator.
		/// </summary>
		public Vector2D LastPoint
		{
			get
			{
				if (this.pointCount == 0)
					throw new InvalidOperationException("No previous point defined.");

				return this.points[this.pointCount - 1].Position;
			}
		}

		/// <summary>
		/// Gets the path vertex array.
		/// </summary>
		public ReadOnlySpan<Vector2D> Vertices => new ReadOnlySpan<Vector2D>(this.vertices, 0, this.vertexCount);
	
		/// <summary>
		/// Gets the bounding rectangle of the tessellation.
		/// </summary>
		public RectangleD Bounds => new RectangleD(this.boundsMin.X, this.boundsMin.Y, this.boundsMax.X - this.boundsMin.X, this.boundsMax.Y - this.boundsMin.Y);

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PathTessellator"/> class. 
		/// </summary>
		public PathTessellator(double pixelRatio)
		{
			this.config = new PathRasterConfig(pixelRatio);
			this.paths = new List<TessellatedPath>();
			this.points = new Point[32];

			this.Clear();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Resets the tessellator to the initial state.
		/// </summary>
		public void Clear()
		{
			this.boundsMin.X = double.MaxValue;
			this.boundsMin.Y = double.MaxValue;
			this.boundsMax.X = double.MinValue;
			this.boundsMax.Y = double.MinValue;
			this.paths.Clear();
			this.pointCount = 0;
			this.vertexCount = 0;
		}

		/// <summary>
		/// Computes the fill of the data added to the tessellator.
		/// </summary>
		public void Fill()
		{
			this.prepare();
			this.expandFill(0.0, LineJoin.Miter, 2.4);
		}

		/// <summary>
		/// Computes the stroke of the data added to the tessellator.
		/// </summary>
		public void Stroke(double strokeWidth, LineCap lineCap, LineJoin lineJoin, double miterLimit)
		{
			this.prepare();
			
			this.expandStroke(strokeWidth * 0.5, lineCap, lineJoin, miterLimit);

			this.boundsMin.X -= strokeWidth;
			this.boundsMin.Y -= strokeWidth;
			this.boundsMax.X += strokeWidth;
			this.boundsMax.Y += strokeWidth;
		}

		/// <summary>
		/// Adds a <see cref="Geometry"/>.
		/// Tries to use an optimized method for tessellating the specified <see cref="Geometry"/> and falls back to path conversion if the <see cref="Geometry"/> is not supported natively.
		/// </summary>
		/// <param name="geometry"></param>
		public void AddGeometry(Geometry geometry)
		{
			switch (geometry)
			{
				case RectangleGeometry rectangleGeometry:
					this.AddRectangleGeometry(rectangleGeometry);
					break;

				default:
					PathGeometry pathGeometry = geometry.ToPath();
					this.AddPathGeometry(pathGeometry);
					break;
			}
		}

		/// <summary>
		/// Adds all figures of the specified <see cref="PathGeometry"/>.
		/// </summary>
		/// <param name="geometry"></param>
		public void AddPathGeometry(PathGeometry geometry)
		{
			if (geometry == null)
				throw new ArgumentNullException(nameof(geometry));

			foreach (PathFigure figure in geometry.Figures)
			{
				this.AddFigure(figure);
			}
		}

		/// <summary>
		/// Adds a <see cref="RectangleGeometry"/>.
		/// </summary>
		/// <param name="geometry"></param>
		public void AddRectangleGeometry(RectangleGeometry geometry)
		{
			if (geometry == null)
				throw new ArgumentNullException(nameof(geometry));

			RectangleD rect = geometry.Rectangle;
			this.BeginFigure();
			this.AddPoint((rect.X, rect.Y), false);
			this.AddPoint((rect.X + rect.Width, rect.Y), false);
			this.AddPoint((rect.X + rect.Width, rect.Y + rect.Height), false);
			this.AddPoint((rect.X, rect.Y + rect.Height), false);
			this.CloseFigure();
		}

		/// <summary>
		/// Adds a figure.
		/// </summary>
		/// <param name="figure"></param>
		public void AddFigure(PathFigure figure)
		{
			if (figure == null)
				throw new ArgumentNullException(nameof(figure));

			this.BeginFigure();
			this.AddPoint(figure.StartPoint, figure.Segments.FirstOrDefault()?.IsSmoothJoin ?? false);

			foreach (PathSegment segment in figure.Segments)
			{
				segment.Tessellate(this);
			}

			if (figure.IsClosed)
				this.CloseFigure();
		}

		/// <summary>
		/// Starts a new figure.
		/// </summary>
		public void BeginFigure()
		{
			this.paths.Add(new TessellatedPath(this)
			{
				PointOffset = this.pointCount,
				Winding = PathWinding.CounterClockwise
			});
		}
		
		/// <summary>
		/// Closes the current path.
		/// </summary>
		public void CloseFigure()
		{
			if (this.paths.Count == 0)
				throw new InvalidOperationException("No figure defined.");

			TessellatedPath path = this.paths[this.paths.Count - 1];
			path.Closed = true;
		}

		/// <summary>
		/// Adds a point to the current figure.
		/// </summary>
		/// <param name="p"></param>
		public void AddPoint(Vector2D p)
		{
			this.AddPoint(p, true);
		}

		/// <summary>
		/// Adds a point to the current figure.
		/// </summary>
		/// <param name="p">The position.</param>
		/// <param name="smoothJoin">Whether the point is a corner (false) or a smooth join (true).</param>
		public void AddPoint(Vector2D p, bool smoothJoin)
		{
			if (this.paths.Count == 0)
				throw new InvalidOperationException("No figure defined.");

			TessellatedPath path = this.paths[this.paths.Count - 1];
			if (path.PointCount > 0 && this.pointCount > 0)
			{
				if (Vector2D.ApproximatelyEquals(p, this.points[this.pointCount - 1].Position, this.config.DistTol))
				{
					if (!smoothJoin)
						this.points[this.pointCount - 1].Flags |= PathTessellatorPointFlags.Corner;
					return;
				}
			}

			if (this.pointCount + 1 > this.points.Length)
			{
				Array.Resize(ref this.points, this.points.Length * 2);
			}

			this.points[this.pointCount] = new Point
			{
				Position = p,
				Flags = smoothJoin ? 0 : PathTessellatorPointFlags.Corner
			};

			++this.pointCount;
			++path.PointCount;
		}

		/// <summary>
		/// Adds a quadratic bezier curve.
		/// </summary>
		/// <param name="p2"></param>
		/// <param name="p3"></param>
		/// <param name="smoothJoin">Whether the point is a corner (false) or a smooth join (true).</param>
		public void AddQuadraticBezier(Vector2D p2, Vector2D p3, bool smoothJoin)
		{
			QuadraticBezier bezier;
			bezier.Point1 = this.LastPoint;
			bezier.Point2 = p2;
			bezier.Point3 = p3;
			this.AddQuadraticBezier(bezier, smoothJoin);
		}

		/// <summary>
		/// Adds a quadratic bezier curve.
		/// </summary>
		public void AddQuadraticBezier(QuadraticBezier bezier, bool smoothJoin)
		{
			int p0 = this.pointCount;
			bezier.Tessellate(this.config.TessTol, 16, this.AddPoint);

			if (!smoothJoin)
			{
				// Set type of first point.
				this.points[p0].Flags |= PathTessellatorPointFlags.Corner;
			}
		}

		/// <summary>
		/// Adds a cubic bezier curve.
		/// </summary>
		public void AddCubicBezier(Vector2D p2, Vector2D p3, Vector2D p4, bool smoothJoin)
		{
			if (this.pointCount == 0)
				throw new InvalidOperationException("No previous point defined.");

			CubicBezier bezier;
			bezier.Point1 = this.LastPoint;
			bezier.Point2 = p2;
			bezier.Point3 = p3;
			bezier.Point4 = p4;
			this.AddCubicBezier(bezier, smoothJoin);
		}

		/// <summary>
		/// Adds a cubic bezier curve.
		/// </summary>
		public void AddCubicBezier(CubicBezier bezier, bool smoothJoin)
		{
			int p0 = this.pointCount;
			bezier.Tessellate(this.config.TessTol, 16, this.AddPoint);

			if (!smoothJoin)
			{
				// Set type of first point.
				this.points[p0].Flags |= PathTessellatorPointFlags.Corner;
			}
		}

		/// <summary>
		/// Adds an elliptical arc.
		/// </summary>
		public void AddEllipticalArc(EllipticalArc arc, bool smoothJoin)
		{
			int p0 = this.pointCount;
			arc.Tessellate(this.config.TessTol, this.AddPoint);

			if (!smoothJoin)
			{
				// Set type of first point.
				this.points[p0].Flags |= PathTessellatorPointFlags.Corner;
			}
		}

		/// <summary>
		/// Sets the winding of the current path.
		/// </summary>
		/// <param name="winding"></param>
		public void SetWinding(PathWinding winding)
		{
			if (this.paths.Count == 0)
				throw new InvalidOperationException("No figure defined.");

			TessellatedPath path = this.paths[this.paths.Count - 1];
			path.Winding = winding;
		}

		private void chooseBevel(bool bevel, int p0, int p1, double w, out double x0, out double y0, out double x1, out double y1)
		{
			if (bevel)
			{
				x0 = this.points[p1].Position.X + this.points[p0].Delta.Y * w;
				y0 = this.points[p1].Position.Y - this.points[p0].Delta.X * w;
				x1 = this.points[p1].Position.X + this.points[p1].Delta.Y * w;
				y1 = this.points[p1].Position.Y - this.points[p1].Delta.X * w;
			}
			else
			{
				x0 = this.points[p1].Position.X + this.points[p1].Dm.X * w;
				y0 = this.points[p1].Position.Y + this.points[p1].Dm.Y * w;
				x1 = this.points[p1].Position.X + this.points[p1].Dm.X * w;
				y1 = this.points[p1].Position.Y + this.points[p1].Dm.Y * w;
			}
		}

		private void setVertex(int index, double x, double y, double u, double v)
		{
			this.vertices[index].X = x;
			this.vertices[index].Y = y;
		}

		private void calculateJoins(double w, LineJoin lineJoin, double miterLimit)
		{
			double iw = 0.0;

			if (w > 0.0)
				iw = 1.0 / w;

			// Calculate which joins needs extra vertices to append, and gather vertex count.
			for (int i = 0; i < this.paths.Count; ++i)
			{
				TessellatedPath path = this.paths[i];
				path.NumBevel = 0;

				int nLeft = 0;
				int p0 = path.LastPoint;
				int p1 = path.PointOffset;
				for (int j = 0; j < path.PointCount; ++j)
				{
					double dlx0 = this.points[p0].Delta.Y;
					double dly0 = -this.points[p0].Delta.X;
					double dlx1 = this.points[p1].Delta.Y;
					double dly1 = -this.points[p1].Delta.X;

					// Calculate extrusions
					this.points[p1].Dm.X = (dlx0 + dlx1) * 0.5;
					this.points[p1].Dm.Y = (dly0 + dly1) * 0.5;
					double dmr2 = this.points[p1].Dm.X * this.points[p1].Dm.X + this.points[p1].Dm.Y * this.points[p1].Dm.Y;
					if (dmr2 > 1e-6)
					{
						double scale = 1.0 / dmr2;
						if (scale > 600.0)
							scale = 600.0;

						this.points[p1].Dm.X *= scale;
						this.points[p1].Dm.Y *= scale;
					}

					// Clear flags, but keep the corner.
					this.points[p1].Flags = (this.points[p1].Flags & PathTessellatorPointFlags.Corner) != 0 ? PathTessellatorPointFlags.Corner : 0;

					// Keep track of left turns.
					double cross = this.points[p1].Delta.X * this.points[p0].Delta.Y - this.points[p0].Delta.X * this.points[p1].Delta.Y;
					if (cross > 0.0)
					{
						++nLeft;
						this.points[p1].Flags |= PathTessellatorPointFlags.Left;
					}

					// Calculate if we should use bevel or miter for inner join.
					double limit = Utility.Max(1.01, Utility.Min(this.points[p0].Length, this.points[p1].Length) * iw);
					if (dmr2 * limit * limit < 1.0)
						this.points[p1].Flags |= PathTessellatorPointFlags.InnerBevel;

					// Check to see if the corner needs to be beveled.
					if ((this.points[p1].Flags & PathTessellatorPointFlags.Corner) != 0)
					{
						if ((dmr2 * miterLimit * miterLimit) < 1.0 || lineJoin == LineJoin.Bevel || lineJoin == LineJoin.Round)
						{
							this.points[p1].Flags |= PathTessellatorPointFlags.Bevel;
						}
					}

					if ((this.points[p1].Flags & (PathTessellatorPointFlags.Bevel | PathTessellatorPointFlags.InnerBevel)) != 0)
					{
						++path.NumBevel;
					}

					p0 = p1++;
				}

				path.Convex = (nLeft == path.PointCount);
			}
		}

		/// <summary>
		/// Prepare the paths.
		/// Computes the bounding box and deltas of all points.
		/// </summary>
		private void prepare()
		{
			for (int j = 0; j < this.paths.Count; ++j)
			{
				TessellatedPath path = this.paths[j];

				// If the first and last points are the same, remove the last, mark as closed path.
				if (Vector2D.ApproximatelyEquals(this.points[path.LastPoint].Position, this.points[path.PointOffset].Position, this.config.DistTol))
				{
					--path.PointCount;
					path.Closed = true;
				}

				// Enforce winding.
				if (path.PointCount > 2)
				{
					double area = polyArea(this.points, path.PointOffset, path.PointCount);
					if (path.Winding == PathWinding.CounterClockwise && area < 0.0)
						this.polyReverse(path.PointOffset, path.PointCount);
					else if (path.Winding == PathWinding.Clockwise && area > 0.0)
						this.polyReverse(path.PointOffset, path.PointCount);
				}

				int p0 = path.LastPoint;
				int p1 = path.PointOffset;
				for (int i = 0; i < path.PointCount; ++i)
				{
					// Calculate segment direction and length
					this.points[p0].Delta.X = this.points[p1].Position.X - this.points[p0].Position.X;
					this.points[p0].Delta.Y = this.points[p1].Position.Y - this.points[p0].Position.Y;
					this.points[p0].Length = this.points[p0].Delta.Normalize(out this.points[p0].Delta);

					// Update bounds
					this.boundsMin.X = Utility.Min(this.boundsMin.X, this.points[p0].Position.X);
					this.boundsMin.Y = Utility.Min(this.boundsMin.Y, this.points[p0].Position.Y);
					this.boundsMax.X = Utility.Max(this.boundsMax.X, this.points[p0].Position.X);
					this.boundsMax.Y = Utility.Max(this.boundsMax.Y, this.points[p0].Position.Y);

					// Advance.
					p0 = p1++;
				}
			}
		}

		private void allocVertices(int numVertices)
		{
			if (this.vertices == null)
			{
				this.vertices = new Vector2D[numVertices];
				return;
			}

			if (this.vertices.Length < numVertices)
			{
				Array.Resize(ref this.vertices, numVertices);
			}
		}

		private void expandFill(double w, LineJoin lineJoin, double miterLimit)
		{
			double aa = this.config.FringeWidth;
			bool fringe = w > 0.0;

			this.calculateJoins(w, lineJoin, miterLimit);

			// Calculate max vertex usage.
			int numVertices = 0;
			for (int i = 0; i < this.paths.Count; ++i)
			{
				TessellatedPath path = this.paths[i];
				numVertices += path.PointCount + path.NumBevel + 1;
				if (fringe)
					numVertices += (path.PointCount + path.NumBevel * 5 + 1) * 2;
			}

			this.allocVertices(numVertices);
			bool convex = this.paths.Count == 1 && this.paths[0].Convex;
			int vertexIndex = 0;

			foreach (TessellatedPath path in this.paths)
			{
				// Calculate shape vertices.
				double woff = 0.5 * aa;
				path.FillVertexOffset = vertexIndex;

				if (fringe)
				{
					// Looping
					int p0 = path.LastPoint;
					int p1 = path.PointOffset;

					for (int j = 0; j < path.PointCount; ++j)
					{
						if (this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.Bevel))
						{
							if (this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.Left))
							{
								Vector2D l;
								l.X = this.points[p1].Position.X + this.points[p1].Dm.X * woff;
								l.Y = this.points[p1].Position.Y + this.points[p1].Dm.Y * woff;
								this.setVertex(vertexIndex++, l.X, l.Y, 0.5, 1.0);
							}
							else
							{
								Vector2D dl0;
								dl0.X = this.points[p0].Delta.Y;
								dl0.Y = -this.points[p0].Delta.X;

								Vector2D dl1;
								dl1.X = this.points[p1].Delta.Y;
								dl1.Y = -this.points[p1].Delta.X;

								Vector2D l0;
								l0.X = this.points[p1].Position.X + dl0.X * woff;
								l0.Y = this.points[p1].Position.Y + dl0.Y * woff;

								Vector2D l1;
								l1.X = this.points[p1].Position.X + dl1.X * woff;
								l1.Y = this.points[p1].Position.Y + dl1.Y * woff;

								this.setVertex(vertexIndex++, l0.X, l0.Y, 0.5, 1.0);
								this.setVertex(vertexIndex++, l1.X, l1.Y, 0.5, 1.0);
							}
						}
						else
						{
							this.setVertex(vertexIndex++, this.points[p1].Position.X + this.points[p1].Dm.X * woff, this.points[p1].Position.Y + this.points[p1].Dm.Y * woff, 0.5, 1.0);
						}
						p0 = p1++;
					}
				}
				else
				{
					int end = path.PointOffset + path.PointCount;
					for (int j = path.PointOffset; j < end; ++j)
					{
						this.setVertex(vertexIndex, this.points[j].Position.X, this.points[j].Position.Y, 0.5, 1.0);
						++vertexIndex;
					}
				}

				path.FillVertexCount = vertexIndex - path.FillVertexOffset;

				// Calculate fringe
				if (fringe)
				{
					double lw = w + woff;
					double rw = w - woff;
					double lu = 0.0;
					double ru = 1.0;
					path.StrokeVertexOffset = vertexIndex;

					// Create only half a fringe for convex shapes so that
					// the shape can be rendered without stenciling.
					if (convex)
					{
						lw = woff; // This should generate the same vertex as fill inset above.
						lu = 0.5f; // Set outline fade at middle.
					}

					// Looping
					int p0 = path.LastPoint;
					int p1 = path.PointOffset;

					for (int j = 0; j < path.PointCount; ++j)
					{
						if ((this.points[p1].Flags & (PathTessellatorPointFlags.Bevel | PathTessellatorPointFlags.InnerBevel)) != 0)
						{
							vertexIndex = this.bevelJoin(vertexIndex, p0, p1, lw, rw, lu, ru, this.config.FringeWidth);
						}
						else
						{
							this.setVertex(vertexIndex++, this.points[p1].Position.X + (this.points[p1].Dm.X * lw), this.points[p1].Position.Y + (this.points[p1].Dm.Y * lw), lu, 1.0f);
							this.setVertex(vertexIndex++, this.points[p1].Position.X - (this.points[p1].Dm.X * rw), this.points[p1].Position.Y - (this.points[p1].Dm.Y * rw), ru, 1.0f);
						}
						p0 = p1++;
					}

					// Loop it
					this.setVertex(vertexIndex++, this.vertices[path.StrokeVertexOffset].X, this.vertices[path.StrokeVertexOffset].Y, lu, 1.0f);
					this.setVertex(vertexIndex++, this.vertices[path.StrokeVertexOffset + 1].X, this.vertices[path.StrokeVertexOffset + 1].Y, ru, 1.0f);

					path.StrokeVertexCount = vertexIndex - path.StrokeVertexOffset;
				}
				else
				{
					path.StrokeVertexOffset = 0;
					path.StrokeVertexCount = 0;
				}
			}

			this.vertexCount = vertexIndex;
		}

		private void expandStroke(double w, LineCap lineCap, LineJoin lineJoin, double miterLimit)
		{
			double aa = this.config.FringeWidth;
			int ncap = curveDivs(w, ConstantsD.Pi, this.config.TessTol); // Calculate divisions per half circle.
			this.calculateJoins(w, lineJoin, miterLimit);

			// Calculate max vertex usage.
			int numVertices = 0;
			for (int i = 0; i < this.paths.Count; ++i)
			{
				TessellatedPath path = this.paths[i];

				if (lineJoin == LineJoin.Round)
				{
					numVertices += (path.PointCount + path.NumBevel * (ncap + 2) + 1) * 2; // plus one for loop
				}
				else
				{
					numVertices += (path.PointCount + path.NumBevel * 5 + 1) * 2; // plus one for loop
				}

				if (!path.Closed)
				{
					// space for caps
					if (lineCap == LineCap.Round)
					{
						numVertices += (ncap * 2 + 2) * 2;
					}
					else
					{
						numVertices += (3 + 3) * 2;
					}
				}
			}

			this.allocVertices(numVertices);
			int vertexIndex = 0;

			for (int i = 0; i < this.paths.Count; ++i)
			{
				TessellatedPath path = this.paths[i];
				path.FillVertexOffset = 0;
				path.FillVertexCount = 0;

				// Calculate fringe or stroke
				bool loop = path.Closed;
				path.StrokeVertexOffset = vertexIndex;

				int p0;
				int p1;
				int start;
				int end;

				if (loop)
				{
					// Looping
					p0 = path.LastPoint;
					p1 = path.PointOffset;
					start = 0;
					end = path.PointCount;
				}
				else
				{
					// Add cap
					p0 = path.PointOffset;
					p1 = path.PointOffset + 1;
					start = 1;
					end = path.PointCount - 1;

					Vector2D d;
					d.X = this.points[p1].Position.X - this.points[p0].Position.X;
					d.Y = this.points[p1].Position.Y - this.points[p0].Position.Y;
					d = d.Normalize();

					if (lineCap == LineCap.Butt)
					{
						vertexIndex = this.buttCapStart(vertexIndex, p0, d.X, d.Y, w, -aa * 0.5, aa);
					}
					else if (lineCap == LineCap.Square)
					{
						vertexIndex = this.buttCapStart(vertexIndex, p0, d.X, d.Y, w, w - aa, aa);
					}
					else if (lineCap == LineCap.Round)
					{
						vertexIndex = this.roundCapStart(vertexIndex, p0, d.X, d.Y, w, ncap, aa);
					}
				}

				for (int j = start; j < end; ++j)
				{
					if ((this.points[p1].Flags & (PathTessellatorPointFlags.Bevel | PathTessellatorPointFlags.InnerBevel)) != 0)
					{
						if (lineJoin == LineJoin.Round)
						{
							vertexIndex = this.roundJoin(vertexIndex, p0, p1, w, w, 0.0, 1.0, ncap, aa);
						}
						else
						{
							vertexIndex = this.bevelJoin(vertexIndex, p0, p1, w, w, 0.0, 1.0, aa);
						}
					}
					else
					{
						this.setVertex(vertexIndex++, this.points[p1].Position.X + (this.points[p1].Dm.X * w), this.points[p1].Position.Y + (this.points[p1].Dm.Y * w), 0, 1);
						this.setVertex(vertexIndex++, this.points[p1].Position.X - (this.points[p1].Dm.X * w), this.points[p1].Position.Y - (this.points[p1].Dm.Y * w), 1, 1);
					}

					p0 = p1++;
				}

				if (loop)
				{
					// Loop it
					this.setVertex(vertexIndex++, this.vertices[path.StrokeVertexOffset].X, this.vertices[path.StrokeVertexOffset].Y, 0.0, 1.0);
					this.setVertex(vertexIndex++, this.vertices[path.StrokeVertexOffset + 1].X, this.vertices[path.StrokeVertexOffset + 1].Y, 1.0, 1.0);
				}
				else
				{
					// Add cap
					Vector2D d;
					d.X = this.points[p1].Position.X - this.points[p0].Position.X;
					d.Y = this.points[p1].Position.Y - this.points[p0].Position.Y;
					d = d.Normalize();

					if (lineCap == LineCap.Butt)
					{
						vertexIndex = this.buttCapEnd(vertexIndex, p1, d.X, d.Y, w, -aa * 0.5, aa);
					}
					else if (lineCap == LineCap.Square)
					{
						vertexIndex = this.buttCapEnd(vertexIndex, p1, d.X, d.Y, w, w - aa, aa);
					}
					else if (lineCap == LineCap.Round)
					{
						vertexIndex = this.roundCapEnd(vertexIndex, p1, d.X, d.Y, w, ncap, aa);
					}
				}

				path.StrokeVertexCount = vertexIndex - path.StrokeVertexOffset;
			}

			this.vertexCount = vertexIndex;
		}

		private int buttCapStart(int dst, int p, double dx, double dy, double w, double d, double aa)
		{
			double px = this.points[p].Position.X - dx * d;
			double py = this.points[p].Position.Y - dy * d;
			double dlx = dy;
			double dly = -dx;
			this.setVertex(dst++, px + dlx * w - dx * aa, py + dly * w - dy * aa, 0, 0);
			this.setVertex(dst++, px - dlx * w - dx * aa, py - dly * w - dy * aa, 1, 0);
			this.setVertex(dst++, px + dlx * w, py + dly * w, 0, 1);
			this.setVertex(dst++, px - dlx * w, py - dly * w, 1, 1);
			return dst;
		}

		private int buttCapEnd(int dst, int p, double dx, double dy, double w, double d, double aa)
		{
			double px = this.points[p].Position.X + dx * d;
			double py = this.points[p].Position.Y + dy * d;
			double dlx = dy;
			double dly = -dx;
			this.setVertex(dst++, px + dlx * w, py + dly * w, 0, 1);
			this.setVertex(dst++, px - dlx * w, py - dly * w, 1, 1);
			this.setVertex(dst++, px + dlx * w + dx * aa, py + dly * w + dy * aa, 0, 0);
			this.setVertex(dst++, px - dlx * w + dx * aa, py - dly * w + dy * aa, 1, 0);
			return dst;
		}

		private int roundCapStart(int dst, int p, double dx, double dy, double w, int ncap, double aa)
		{
			double px = this.points[p].Position.X;
			double py = this.points[p].Position.Y;
			double dlx = dy;
			double dly = -dx;
			for (int i = 0; i < ncap; i++)
			{
				double a = i / (double)(ncap - 1) * ConstantsD.Pi;
				double ax = Utility.Cos(a) * w, ay = Utility.Sin(a) * w;
				this.setVertex(dst++, px - dlx * ax - dx * ay, py - dly * ax - dy * ay, 0, 1);
				this.setVertex(dst++, px, py, 0.5, 1);
			}
			this.setVertex(dst++, px + dlx * w, py + dly * w, 0, 1);
			this.setVertex(dst++, px - dlx * w, py - dly * w, 1, 1);
			return dst;
		}

		private int roundCapEnd(int dst, int p, double dx, double dy, double w, int ncap, double aa)
		{
			double px = this.points[p].Position.X;
			double py = this.points[p].Position.Y;
			double dlx = dy;
			double dly = -dx;
			this.setVertex(dst++, px + dlx * w, py + dly * w, 0, 1);
			this.setVertex(dst++, px - dlx * w, py - dly * w, 1, 1);
			for (int i = 0; i < ncap; i++)
			{
				double a = i / (double)(ncap - 1) * ConstantsD.Pi;
				double ax = Utility.Cos(a) * w, ay = Utility.Sin(a) * w;
				this.setVertex(dst++, px, py, 0.5, 1);
				this.setVertex(dst++, px - dlx * ax + dx * ay, py - dly * ax + dy * ay, 0, 1);
			}
			return dst;
		}

		private int roundJoin(int dst, int p0,int p1,double lw, double rw, double lu, double ru, int ncap, double fringe)
		{
			int i, n;
			double dlx0 = this.points[p0].Delta.Y;
			double dly0 = -this.points[p0].Delta.X;
			double dlx1 = this.points[p1].Delta.Y;
			double dly1 = -this.points[p1].Delta.X;

			if (this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.Left))
			{
				this.chooseBevel(this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.InnerBevel), p0, p1, lw, out double lx0, out double ly0, out double lx1, out double ly1);
				double a0 = Utility.Atan2(-dly0, -dlx0);
				double a1 = Utility.Atan2(-dly1, -dlx1);
				if (a1 > a0)
					a1 -= ConstantsD.Pi2;

				this.setVertex(dst++, lx0, ly0, lu, 1);
				this.setVertex(dst++, this.points[p1].Position.X - dlx0 * rw, this.points[p1].Position.Y - dly0 * rw, ru, 1);

				n = Utility.Clamp((int)Utility.Ceiling(((a0 - a1) / ConstantsD.Pi) * ncap), 2, ncap);
				for (i = 0; i < n; i++)
				{
					double u = i / (double)(n - 1);
					double a = a0 + u * (a1 - a0);
					double rx = this.points[p1].Position.X + Utility.Cos(a) * rw;
					double ry = this.points[p1].Position.Y + Utility.Sin(a) * rw;
					this.setVertex(dst++, this.points[p1].Position.X, this.points[p1].Position.Y, 0.5f, 1);
					this.setVertex(dst++, rx, ry, ru, 1);
				}

				this.setVertex(dst++, lx1, ly1, lu, 1);
				this.setVertex(dst++, this.points[p1].Position.X - dlx1 * rw, this.points[p1].Position.Y - dly1 * rw, ru, 1);
			}
			else
			{
				this.chooseBevel(this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.InnerBevel), p0, p1, -rw, out double rx0, out double ry0, out double rx1, out double ry1);
				double a0 = Utility.Atan2(dly0, dlx0);
				double a1 = Utility.Atan2(dly1, dlx1);
				if (a1 < a0)
					a1 += ConstantsD.Pi2;

				this.setVertex(dst++, this.points[p1].Position.X + dlx0 * rw, this.points[p1].Position.Y + dly0 * rw, lu, 1);
				this.setVertex(dst++, rx0, ry0, ru, 1);

				n = Utility.Clamp((int)Utility.Ceiling(((a1 - a0) / ConstantsD.Pi) * ncap), 2, ncap);
				for (i = 0; i < n; i++)
				{
					double u = i / (double)(n - 1);
					double a = a0 + u * (a1 - a0);
					double lx = this.points[p1].Position.X + Utility.Cos(a) * lw;
					double ly = this.points[p1].Position.Y + Utility.Sin(a) * lw;

					this.setVertex(dst++, lx, ly, lu, 1);
					this.setVertex(dst++, this.points[p1].Position.X, this.points[p1].Position.Y, 0.5f, 1);
				}

				this.setVertex(dst++, this.points[p1].Position.X + dlx1 * rw, this.points[p1].Position.Y + dly1 * rw, lu, 1);
				this.setVertex(dst++, rx1, ry1, ru, 1);
			}
			return dst;
		}

		private int bevelJoin(int dst, int p0, int p1, double lw, double rw, double lu, double ru, double fringe)
		{
			double rx0, ry0;
			double lx0, ly0;
			double dlx0 = this.points[p0].Delta.Y;
			double dly0 = -this.points[p0].Delta.X;
			double dlx1 = this.points[p1].Delta.Y;
			double dly1 = -this.points[p1].Delta.X;

			if (this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.Left))
			{
				this.chooseBevel(this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.InnerBevel), p0, p1, lw, out lx0, out ly0, out double lx1, out double ly1);

				this.setVertex(dst++, lx0, ly0, lu, 1);
				this.setVertex(dst++, this.points[p1].Position.X - dlx0 * rw, this.points[p1].Position.Y - dly0 * rw, ru, 1);

				if (this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.Bevel))
				{
					this.setVertex(dst++, lx0, ly0, lu, 1);
					this.setVertex(dst++, this.points[p1].Position.X - dlx0 * rw, this.points[p1].Position.Y - dly0 * rw, ru, 1);

					this.setVertex(dst++, lx1, ly1, lu, 1);
					this.setVertex(dst++, this.points[p1].Position.X - dlx1 * rw, this.points[p1].Position.Y - dly1 * rw, ru, 1);
				}
				else
				{
					rx0 = this.points[p1].Position.X - this.points[p1].Dm.X * rw;
					ry0 = this.points[p1].Position.Y - this.points[p1].Dm.Y * rw;

					this.setVertex(dst++, this.points[p1].Position.X, this.points[p1].Position.Y, 0.5, 1);
					this.setVertex(dst++, this.points[p1].Position.X - dlx0 * rw, this.points[p1].Position.Y - dly0 * rw, ru, 1);

					this.setVertex(dst++, rx0, ry0, ru, 1);
					this.setVertex(dst++, rx0, ry0, ru, 1);

					this.setVertex(dst++, this.points[p1].Position.X, this.points[p1].Position.Y, 0.5, 1);
					this.setVertex(dst++, this.points[p1].Position.X - dlx1 * rw, this.points[p1].Position.Y - dly1 * rw, ru, 1);
				}

				this.setVertex(dst++, lx1, ly1, lu, 1);
				this.setVertex(dst++, this.points[p1].Position.X - dlx1 * rw, this.points[p1].Position.Y - dly1 * rw, ru, 1);

			}
			else
			{
				this.chooseBevel(this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.InnerBevel), p0, p1, -rw, out rx0, out ry0, out double rx1, out double ry1);

				this.setVertex(dst++, this.points[p1].Position.X + dlx0 * lw, this.points[p1].Position.Y + dly0 * lw, lu, 1);
				this.setVertex(dst++, rx0, ry0, ru, 1);

				if (this.points[p1].Flags.HasFlag(PathTessellatorPointFlags.Bevel))
				{
					this.setVertex(dst++, this.points[p1].Position.X + dlx0 * lw, this.points[p1].Position.Y + dly0 * lw, lu, 1);
					this.setVertex(dst++, rx0, ry0, ru, 1);

					this.setVertex(dst++, this.points[p1].Position.X + dlx1 * lw, this.points[p1].Position.Y + dly1 * lw, lu, 1);
					this.setVertex(dst++, rx1, ry1, ru, 1);
				}
				else
				{
					lx0 = this.points[p1].Position.X + this.points[p1].Dm.X * lw;
					ly0 = this.points[p1].Position.Y + this.points[p1].Dm.Y * lw;

					this.setVertex(dst++, this.points[p1].Position.X + dlx0 * lw, this.points[p1].Position.Y + dly0 * lw, lu, 1);
					this.setVertex(dst++, this.points[p1].Position.X, this.points[p1].Position.Y, 0.5, 1);

					this.setVertex(dst++, lx0, ly0, lu, 1);
					this.setVertex(dst++, lx0, ly0, lu, 1);

					this.setVertex(dst++, this.points[p1].Position.X + dlx1 * lw, this.points[p1].Position.Y + dly1 * lw, lu, 1);
					this.setVertex(dst++, this.points[p1].Position.X, this.points[p1].Position.Y, 0.5, 1);
				}

				this.setVertex(dst++, this.points[p1].Position.X + dlx1 * lw, this.points[p1].Position.Y + dly1 * lw, lu, 1);
				this.setVertex(dst++, rx1, ry1, ru, 1);
			}

			return dst;
		}

		private static double triArea2(in Vector2D a, in Vector2D b, in Vector2D c)
		{
			return (c.X - a.X) * (b.Y - a.Y) - (b.X - a.X) * (c.Y - a.Y);
		}

		private static double polyArea(Point[] points, int offset, int count)
		{
			Vector2D a = points[offset].Position;
			double area = 0.0;
			for (int i = 2; i < count; ++i)
			{
				Vector2D b = points[offset + i - 1].Position;
				Vector2D c = points[offset + i].Position;
				area += triArea2(a, b, c);
			}
			return area * 0.5;
		}

		private void polyReverse(int offset, int count)
		{
			int i = offset;
			int j = offset + count - 1;
			while (i < j)
			{
				Point tmp = this.points[i];
				this.points[i] = this.points[j];
				this.points[j] = tmp;
				++i;
				--j;
			}
		}
		
		private static int curveDivs(double r, double arc, double tol)
		{
			double da = Utility.Acos(r / (r + tol));
			return Utility.Max(2, (int)Utility.Ceiling(arc / da));
		}

		#endregion
	}
}

