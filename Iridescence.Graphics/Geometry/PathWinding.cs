﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines path windings.
	/// </summary>
	public enum PathWinding
	{
		Clockwise,
		CounterClockwise
	}
}
