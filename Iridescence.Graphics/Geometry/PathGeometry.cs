﻿using System;
using System.Collections.Immutable;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents complex path geometry that may consist of lines, quadratic and cubic Bézier curves, arcs, etc.
	/// </summary>
	public class PathGeometry : Geometry
	{
		#region Properties

		/// <summary>
		/// Gets the geometry fill rule.
		/// </summary>
		public FillRule FillRule { get; }

		/// <summary>
		/// Gets the collection of <see cref="PathFigure">PathFigures</see>.
		/// </summary>
		public ImmutableArray<PathFigure> Figures { get; }

		/// <summary>
		/// Gets an empty <see cref="PathGeometry"/>.
		/// </summary>
		public static PathGeometry Empty { get; } = new PathGeometry();

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PathGeometry"/> class. 
		/// </summary>
		public PathGeometry(ImmutableArray<PathFigure> figures = default, FillRule fillRule = FillRule.EvenOdd, Transform transform = null)
			: base(transform)
		{
			this.FillRule = fillRule;
			this.Figures = figures.IsDefault ? ImmutableArray<PathFigure>.Empty : figures;

			if (this.Figures.Any(i => i == null))
				throw new ArgumentException("Figures array can not contain null.", nameof(figures));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PathGeometry"/> class. 
		/// </summary>
		public PathGeometry(PathFigure figure, FillRule fillRule = FillRule.EvenOdd, Transform transform = null)
			: this(ImmutableArray.Create(figure), fillRule, transform)
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a new <see cref="PathGeometry"/> with the specified <see cref="PathFigure"/> added.
		/// </summary>
		/// <param name="figure"></param>
		/// <returns></returns>
		public PathGeometry AddFigure(PathFigure figure)
		{
			if (figure == null)
				throw new ArgumentNullException(nameof(figure));

			return new PathGeometry(this.Figures.Add(figure), this.FillRule, this.Transform);
		}

		/// <summary>
		/// Returns a new <see cref="PathGeometry"/> with the specified fill rule.
		/// </summary>
		/// <param name="fillRule"></param>
		/// <returns></returns>
		public PathGeometry WithFillRule(FillRule fillRule)
		{
			return new PathGeometry(this.Figures, fillRule, this.Transform);
		}

		public override RectangleD GetBounds(Transform transform)
		{
			bool first = true;
			RectangleD bounds = RectangleD.Zero;

			transform = this.Transform.Combine(transform);
			
			foreach (PathFigure figure in this.Figures)
			{
				if (first)
				{
					bounds = figure.GetBounds(transform);
					first = false;
				}
				else
				{
					RectangleD figureBounds = figure.GetBounds(transform);
					RectangleD.Union(in bounds, in figureBounds, out bounds);
				}
			}

			return bounds;
		}

		public override RectangleD GetBounds(in TransformMatrix transform)
		{
			bool first = true;
			RectangleD bounds = RectangleD.Zero;

			this.Transform.ToMatrixCombined(transform, out TransformMatrix mat);
			
			foreach (PathFigure figure in this.Figures)
			{
				if (first)
				{
					bounds = figure.GetBounds(mat);
					first = false;
				}
				else
				{
					RectangleD figureBounds = figure.GetBounds(mat);
					RectangleD.Union(in bounds, in figureBounds, out bounds);
				}
			}

			return bounds;
		}

		public override Geometry Clone()
		{
			return new PathGeometry(this.Figures, this.FillRule, this.Transform);
		}

		public override PathGeometry ToPath()
		{
			return this;
		}

		#endregion
	}
}
