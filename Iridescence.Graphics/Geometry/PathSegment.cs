﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a path segment.
	/// </summary>
	public abstract class PathSegment : ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets a value that indicates whether the join between this <see cref="PathSegment"/> and the previous <see cref="PathSegment"/> is treated as a corner when it is stroked with a pen. 
		/// </summary>
		public bool IsSmoothJoin { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PathSegment"/> class. 
		/// </summary>
		protected PathSegment(bool? isSmoothJoin = null)
		{
			this.IsSmoothJoin = isSmoothJoin ?? false;
		}

		#endregion

		#region Methods
		
		/// <summary>
		/// Returns the end point of this path segment.
		/// </summary>
		/// <returns></returns>
		public abstract Vector2D GetEndPoint();

		/// <summary>
		/// Returns a bounding rectangle for this <see cref="PathSegment"/> with the specified <see cref="Transform"/> applied to it.
		/// </summary>
		/// <param name="startPoint">The end point of the previous <see cref="PathSegment"/>, without the transform applied to it.</param>
		/// <param name="transform">An optional <see cref="Transform"/>.</param>
		/// <returns></returns>
		public abstract RectangleD GetBounds(Vector2D startPoint, Transform transform);

		/// <summary>
		/// Returns a bounding rectangle for this <see cref="PathSegment"/> with the specified <see cref="Transform"/> applied to it.
		/// </summary>
		/// <param name="startPoint">The end point of the previous <see cref="PathSegment"/>, without the transform applied to it.</param>
		/// <param name="transform">An optional <see cref="TransformMatrix"/>.</param>
		/// <returns></returns>
		public abstract RectangleD GetBounds(Vector2D startPoint, in TransformMatrix transform);

		/// <summary>
		/// Returns a deep copy of this <see cref="PathSegment"/>.
		/// </summary>
		/// <returns></returns>
		public abstract PathSegment Clone();

		/// <summary>
		/// Tessellates the <see cref="PathSegment"/>.
		/// </summary>
		/// <param name="tessellator"></param>
		public abstract void Tessellate(PathTessellator tessellator);

		object ICloneable.Clone()
		{
			return this.Clone();
		}
		
		#endregion
	}
}
