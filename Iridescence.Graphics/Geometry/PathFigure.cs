﻿using System;
using System.Collections.Immutable;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a figure made out of individual path segments.
	/// </summary>
	public class PathFigure : ICloneable
	{
		#region Properties

		/// <summary>
		/// Gets the path start point.
		/// </summary>
		public Vector2D StartPoint { get; }

		/// <summary>
		/// Gets a value that indicates whether the path is closed, i.e. the first and the last point are connected with a line.
		/// </summary>
		public bool IsClosed { get; }

		/// <summary>
		/// Gets the collection of <see cref="PathSegment">segments</see>.
		/// </summary>
		public ImmutableArray<PathSegment> Segments { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PathFigure"/> class. 
		/// </summary>
		public PathFigure(Vector2D startPoint, ImmutableArray<PathSegment> segments = default, bool isClosed = false)
		{
			this.StartPoint = startPoint;
			this.IsClosed = isClosed;
			this.Segments = segments.IsDefault ? ImmutableArray<PathSegment>.Empty : segments;

			if (this.Segments.Any(i => i == null))
				throw new ArgumentException("Segments array can not contain null.", nameof(segments));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PathFigure"/> class. 
		/// </summary>
		public PathFigure(Vector2D startPoint, PathSegment segment, bool isClosed = false)
			: this(startPoint, ImmutableArray.Create(segment), isClosed)
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a new <see cref="PathFigure"/> with the specified <see cref="PathSegment"/> added.
		/// </summary>
		/// <param name="segment"></param>
		/// <returns></returns>
		public PathFigure AddSegment(PathSegment segment)
		{
			if (segment == null)
				throw new ArgumentNullException(nameof(segment));

			return new PathFigure(this.StartPoint, this.Segments.Add(segment), this.IsClosed);
		}

		/// <summary>
		/// Returns a bounding rectangle for this <see cref="PathFigure"/> with the specified <see cref="Transform"/> applied to it.
		/// </summary>
		/// <param name="transform"></param>
		/// <returns></returns>
		public RectangleD GetBounds(Transform transform)
		{
			RectangleD rectangle = new RectangleD(transform?.TransformPoint(this.StartPoint) ?? this.StartPoint, Vector2D.Zero);
			Vector2D startPoint = this.StartPoint;
			
			foreach (PathSegment segment in this.Segments)
			{
				RectangleD segmentBounds = segment.GetBounds(startPoint, transform);
				RectangleD.Union(in rectangle, in segmentBounds, out rectangle);
				startPoint = segment.GetEndPoint();
			}

			return rectangle;
		}

		/// <summary>
		/// Returns a bounding rectangle for this <see cref="PathFigure"/> with the specified <see cref="Transform"/> applied to it.
		/// </summary>
		/// <param name="transform"></param>
		/// <returns></returns>
		public RectangleD GetBounds(in TransformMatrix transform)
		{
			TransformMatrix.Transform(transform, this.StartPoint, out Vector2D start);
			RectangleD rectangle = new RectangleD(start, Vector2D.Zero);
			Vector2D startPoint = this.StartPoint;
			
			foreach (PathSegment segment in this.Segments)
			{
				RectangleD segmentBounds = segment.GetBounds(startPoint, transform);
				RectangleD.Union(in rectangle, in segmentBounds, out rectangle);
				startPoint = segment.GetEndPoint();
			}

			return rectangle;
		}

		public PathFigure Clone()
		{
			return new PathFigure(this.StartPoint, this.Segments, this.IsClosed);
		}

		object ICloneable.Clone()
		{
			return this.Clone();
		}
		
		#endregion
	}
}
