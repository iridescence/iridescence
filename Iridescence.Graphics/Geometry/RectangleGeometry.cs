﻿using System.Collections.Immutable;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a rectangular geometry.
	/// </summary>
	public class RectangleGeometry : Geometry
	{
		#region Properties

		/// <summary>
		/// Gets the rectangle.
		/// </summary>
		public RectangleD Rectangle { get; }

		#endregion

		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="RectangleGeometry"/> class. 
		/// </summary>
		public RectangleGeometry(RectangleD rectangle, Transform transform = null)
			: base(transform)
		{
			this.Rectangle = rectangle;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RectangleGeometry"/> class. 
		/// </summary>
		public RectangleGeometry(Vector2D position, Vector2D size, Transform transform = null)
			: this(new RectangleD(position, size), transform)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RectangleGeometry"/> class. 
		/// </summary>
		public RectangleGeometry(double x, double y, double width, double height, Transform transform = null)
			: this(new Vector2D(x, y), new Vector2D(width, height), transform)
		{

		}

		#endregion

		#region Methods

		public override RectangleD GetBounds(Transform transform)
		{
			return GetBounds(this.Rectangle, this.Transform.Combine(transform));
		}

		public override RectangleD GetBounds(in TransformMatrix transform)
		{
			this.Transform.ToMatrixCombined(transform, out TransformMatrix mat);
			return GetBounds(this.Rectangle, mat);
		}

		public override PathGeometry ToPath()
		{
			LinePathSegment seg1 = new LinePathSegment((this.Rectangle.X + this.Rectangle.Width, this.Rectangle.Y));
			LinePathSegment seg2 = new LinePathSegment((this.Rectangle.X + this.Rectangle.Width, this.Rectangle.Y + this.Rectangle.Height));
			LinePathSegment seg3 = new LinePathSegment((this.Rectangle.X, this.Rectangle.Y + this.Rectangle.Height));
			PathFigure figure = new PathFigure(this.Rectangle.Position, ImmutableArray.Create<PathSegment>(seg1, seg2, seg3), isClosed: true);
			PathGeometry path = new PathGeometry(figure, transform: this.Transform);
			return path;
		}

		public override Geometry Clone()
		{
			return new RectangleGeometry(this.Rectangle, this.Transform);
		}

		/// <summary>
		/// Returns the bounding rectangle for a transformed rectangle.
		/// </summary>
		/// <param name="rect"></param>
		/// <param name="transform"></param>
		/// <returns></returns>
		public static RectangleD GetBounds(RectangleD rect, Transform transform)
		{
			if (transform == null)
				return rect;

			if (transform is TranslateTransform translateTransform)
			{
				rect.Position += translateTransform.Translation;
				return rect;
			}

			if (transform is ScaleTransform scaleTransform)
			{
				rect.Size *= scaleTransform.Scale;
				return rect;
			}

			Vector2D p1, p2, p3, p4;
			p1.X = rect.X;
			p1.Y = rect.Y;
			p2.X = rect.X + rect.Width;
			p2.Y = rect.Y;
			p3.X = rect.X + rect.Width;
			p3.Y = rect.Y + rect.Height;
			p4.X = rect.X;
			p4.Y = rect.Y + rect.Height;

			p1 = transform.TransformPoint(p1);
			p2 = transform.TransformPoint(p2);
			p3 = transform.TransformPoint(p3);
			p4 = transform.TransformPoint(p4);

			Vector2D min = Vector2D.Min(p1, Vector2D.Min(p2, Vector2D.Min(p3, p4)));
			Vector2D max = Vector2D.Max(p1, Vector2D.Max(p2, Vector2D.Max(p3, p4)));

			return new RectangleD(min, max - min);
		}

		/// <summary>
		/// Returns the bounding rectangle for a transformed rectangle.
		/// </summary>
		/// <param name="rect"></param>
		/// <param name="transform"></param>
		/// <returns></returns>
		public static RectangleD GetBounds(RectangleD rect, in TransformMatrix transform)
		{
			Vector2D p1, p2, p3, p4;
			p1.X = rect.X;
			p1.Y = rect.Y;
			p2.X = rect.X + rect.Width;
			p2.Y = rect.Y;
			p3.X = rect.X + rect.Width;
			p3.Y = rect.Y + rect.Height;
			p4.X = rect.X;
			p4.Y = rect.Y + rect.Height;

			p1 = TransformMatrix.Transform(transform, p1);
			p2 = TransformMatrix.Transform(transform, p2);
			p3 = TransformMatrix.Transform(transform, p3);
			p4 = TransformMatrix.Transform(transform, p4);

			Vector2D min = Vector2D.Min(p1, Vector2D.Min(p2, Vector2D.Min(p3, p4)));
			Vector2D max = Vector2D.Max(p1, Vector2D.Max(p2, Vector2D.Max(p3, p4)));

			return new RectangleD(min, max - min);
		}

		#endregion
	}
}
