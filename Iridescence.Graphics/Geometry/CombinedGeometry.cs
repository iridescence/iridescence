﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a combination of two <see cref="Geometry">Geometries</see>.
	/// </summary>
	public class CombinedGeometry : Geometry
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the geometry combine mode.
		/// </summary>
		public GeometryCombineMode Mode { get; }

		/// <summary>
		/// Gets the first operand of the combine operation.
		/// </summary>
		public Geometry Geometry1 { get; }

		/// <summary>
		/// Gets the second operand of the combine operation.
		/// </summary>
		public Geometry Geometry2 { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CombinedGeometry"/> class. 
		/// </summary>
		public CombinedGeometry(GeometryCombineMode mode, Geometry geometry1, Geometry geometry2, Transform transform = null)
			: base(transform)
		{
			this.Mode = mode;
			this.Geometry1 = geometry1 ?? throw new ArgumentNullException(nameof(geometry1));
			this.Geometry2 = geometry2 ?? throw new ArgumentNullException(nameof(geometry2));
		}

		#endregion

		#region Methods

		public override RectangleD GetBounds(Transform transform)
		{
			// TODO
			transform = this.Transform.Combine(transform);
			return RectangleD.Union(this.Geometry1.GetBounds(transform), this.Geometry2.GetBounds(transform));
		}

		public override RectangleD GetBounds(in TransformMatrix transform)
		{
			// TODO
			this.Transform.ToMatrixCombined(transform, out TransformMatrix mat);
			return RectangleD.Union(this.Geometry1.GetBounds(mat), this.Geometry2.GetBounds(mat));		
		}

		/// <summary>
		/// Returns a flattened version of the combined geometry.
		/// </summary>
		/// <returns></returns>
		public Geometry Flatten()
		{
			Geometry a = this.Geometry1;
			Geometry b = this.Geometry2;

			if (a is CombinedGeometry ca)
			{
				a = ca.Flatten();
			}

			if (b is CombinedGeometry cb)
			{
				b = cb.Flatten();
			}

			throw new NotImplementedException();
		}

		public override PathGeometry ToPath()
		{
			return this.Flatten().ToPath();
		}

		public override Geometry Clone()
		{
			return new CombinedGeometry(this.Mode, this.Geometry1, this.Geometry2, this.Transform);
		}
		
		#endregion
	}
}
