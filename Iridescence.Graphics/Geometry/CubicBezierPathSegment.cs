﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a cubic Bézier curve path segment.
	/// </summary>
	public class CubicBezierPathSegment : PathSegment
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the first Bézier curve control point.
		/// </summary>
		public Vector2D ControlPoint1 { get; }
		
		/// <summary>
		/// Gets the second Bézier curve control point.
		/// </summary>
		public Vector2D ControlPoint2 { get; }

		/// <summary>
		/// Gets the end point of the Bézier curve.
		/// </summary>
		public Vector2D EndPoint { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CubicBezierPathSegment"/> class. 
		/// </summary>
		public CubicBezierPathSegment(Vector2D controlPoint1, Vector2D controlPoint2, Vector2D endPoint, bool? isSmoothJoin = null)
			: base(isSmoothJoin)
		{
			this.ControlPoint1 = controlPoint1;
			this.ControlPoint2 = controlPoint2;
			this.EndPoint = endPoint;
		}

		#endregion

		#region Methods

		public override RectangleD GetBounds(Vector2D startPoint, Transform transform)
		{
			CubicBezier bezier;

			if (transform == null)
			{
				bezier.Point1 = startPoint;
				bezier.Point2 = this.ControlPoint1;
				bezier.Point3 = this.ControlPoint2;
				bezier.Point4 = this.EndPoint;
			}
			else
			{
				bezier.Point1 = transform.TransformPoint(startPoint);
				bezier.Point2 = transform.TransformPoint(this.ControlPoint1);
				bezier.Point3 = transform.TransformPoint(this.ControlPoint2);
				bezier.Point4 = transform.TransformPoint(this.EndPoint);
			}

			return bezier.GetBounds();
		}

		public override RectangleD GetBounds(Vector2D startPoint, in TransformMatrix transform)
		{
			CubicBezier bezier;
			TransformMatrix.Transform(transform, startPoint, out bezier.Point1);
			TransformMatrix.Transform(transform, this.ControlPoint1, out bezier.Point2);
			TransformMatrix.Transform(transform, this.ControlPoint2, out bezier.Point3);
			TransformMatrix.Transform(transform, this.EndPoint, out bezier.Point4);
			return bezier.GetBounds();
		}

		public override Vector2D GetEndPoint()
		{
			return this.EndPoint;
		}

		public override void Tessellate(PathTessellator tessellator)
		{
			tessellator.AddCubicBezier(this.ControlPoint1, this.ControlPoint2, this.EndPoint, this.IsSmoothJoin);
		}

		public override PathSegment Clone()
		{
			return new CubicBezierPathSegment(this.ControlPoint1, this.ControlPoint2, this.EndPoint, this.IsSmoothJoin);
		}

		#endregion
	}

	public static class CubicBezierPathSegmentExtensions
	{
		/// <summary>
		/// Adds a <see cref="CubicBezierPathSegment"/> with the specified control and end points.
		/// </summary>
		/// <param name="figure">The figure to add the segment to.</param>
		/// <param name="controlPoint1">The first control point of the Bézier curve.</param>
		/// <param name="controlPoint2">The second control point of the Bézier curve.</param>
		/// <param name="endPoint">The end point of the Bézier curve.</param>
		/// <param name="isSmoothJoin">Whether the curve is joined smoothly.</param>
		public static PathFigure CubicTo(this PathFigure figure, Vector2D controlPoint1, Vector2D controlPoint2, Vector2D endPoint, bool? isSmoothJoin = null)
		{
			return figure.AddSegment(new CubicBezierPathSegment(controlPoint1, controlPoint2, endPoint, isSmoothJoin));
		}

		/// <summary>
		/// Adds a <see cref="CubicBezierPathSegment"/> with the specified control and end points.
		/// </summary>
		/// <param name="figure">The figure to add the segment to.</param>
		/// <param name="c1x">The X position of the first control point.</param>
		/// <param name="c1y">The Y position of the first control point.</param>
		/// <param name="c2x">The X position of the second control point.</param>
		/// <param name="c2y">The Y position of the second control point.</param>
		/// <param name="x">The X position of the end point.</param>
		/// <param name="y">The Y position of the end point.</param>
		/// /// <param name="isSmoothJoin">Whether the curve is joined smoothly.</param>
		public static PathFigure CubicTo(this PathFigure figure, double c1x, double c1y, double c2x, double c2y, double x, double y, bool? isSmoothJoin = null)
		{
			return figure.AddSegment(new CubicBezierPathSegment(new Vector2D(c1x, c1y), new Vector2D(c2x, c2y), new Vector2D(x, y), isSmoothJoin));
		}
	}
}
