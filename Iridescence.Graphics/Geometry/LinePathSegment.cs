﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a simple line segment.
	/// </summary>
	public class LinePathSegment : PathSegment
	{
		#region Properties

		/// <summary>
		/// Gets the line segment end point.
		/// </summary>
		public Vector2D Point { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="LinePathSegment"/> class. 
		/// </summary>
		public LinePathSegment(Vector2D point, bool? isSmoothJoin = null)
			: base(isSmoothJoin)
		{
			this.Point = point;
		}

		#endregion

		#region Methods

		public override RectangleD GetBounds(Vector2D startPoint, Transform transform)
		{
			Vector2D p1 = transform?.TransformPoint(startPoint) ?? startPoint;
			Vector2D p2 = transform?.TransformPoint(this.Point) ?? this.Point;

			Vector2D.Min(p1, p2, out Vector2D min);
			Vector2D.Max(p1, p2, out Vector2D max);
			Vector2D.Subtract(max, min, out max);
			return new RectangleD(min, max);
		}

		public override RectangleD GetBounds(Vector2D startPoint, in TransformMatrix transform)
		{
			TransformMatrix.Transform(transform, startPoint, out Vector2D p1);
			TransformMatrix.Transform(transform, this.Point, out Vector2D p2);

			Vector2D.Min(p1, p2, out Vector2D min);
			Vector2D.Max(p1, p2, out Vector2D max);
			Vector2D.Subtract(max, min, out max);
			return new RectangleD(min, max);
		}

		public override Vector2D GetEndPoint()
		{
			return this.Point;
		}

		public override void Tessellate(PathTessellator tessellator)
		{
			tessellator.AddPoint(this.Point, this.IsSmoothJoin);
		}

		public override PathSegment Clone()
		{
			return new LinePathSegment(this.Point, this.IsSmoothJoin);
		}

		#endregion
	}

	public static class LinePathSegmentExtensions
	{
		/// <summary>
		/// Adds a <see cref="LinePathSegment"/> with the specified point.
		/// </summary>
		/// <param name="figure">The figure to add the segment to.</param>
		/// <param name="point">The line end point.</param>
		/// <param name="isSmoothJoin">Whether the line is joined smoothly</param>
		public static PathFigure LineTo(this PathFigure figure, Vector2D point, bool? isSmoothJoin = null)
		{
			return figure.AddSegment(new LinePathSegment(point, isSmoothJoin));
		}

		/// <summary>
		/// Adds a <see cref="LinePathSegment"/> with the specified point.
		/// </summary>
		/// <param name="figure">The figure to add the segment to.</param>
		/// <param name="x">The X position of the end point.</param>
		/// <param name="y">The Y position of the end point.</param>
		/// <param name="isSmoothJoin">Whether the line is joined smoothly</param>
		public static PathFigure LineTo(this PathFigure figure, double x, double y, bool? isSmoothJoin = null)
		{
			return figure.AddSegment(new LinePathSegment(new Vector2D(x, y), isSmoothJoin));
		}
	}
}
