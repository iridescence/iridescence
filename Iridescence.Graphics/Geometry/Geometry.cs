﻿using System;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents abstract geometry.
	/// </summary>
	public abstract class Geometry : ICloneable
	{
		#region Properties

		#region Transform

		/// <summary>
		/// Gets the transform of the geometry.
		/// </summary>
		public Transform Transform { get; private set; }

		#endregion

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Geometry"/> class. 
		/// </summary>
		protected Geometry(Transform transform = null)
		{
			this.Transform = transform;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a new <see cref="Geometry"/> with the specified transform.
		/// </summary>
		/// <param name="transform"></param>
		/// <returns></returns>
		public Geometry WithTransform(Transform transform)
		{
			Geometry g = this.Clone();
			g.Transform = transform;
			return g;
		}

		/// <summary>
		/// Returns a bounding rectangle for the geometry.
		/// </summary>
		/// <returns></returns>
		public RectangleD GetBounds()
		{
			return this.GetBounds(null);
		}

		/// <summary>
		/// Returns a bounding rectangle for the geometry.
		/// </summary>
		/// <param name="transform">An additional transform, applied after the geometry's own <see cref="Transform"/>.</param>
		/// <returns></returns>
		public abstract RectangleD GetBounds(Transform transform);
		
		/// <summary>
		/// Returns a bounding rectangle for the geometry.
		/// </summary>
		/// <param name="transform">An additional transform, applied after the geometry's own <see cref="Transform"/>.</param>
		/// <returns></returns>
		public abstract RectangleD GetBounds(in TransformMatrix transform);
		
		/// <summary>
		/// Returns a <see cref="PathGeometry"/> representing this geometry or an approximation thereof.
		/// </summary>
		/// <returns></returns>
		public abstract PathGeometry ToPath();

		/// <summary>
		/// Returns a deep copy of the <see cref="Geometry"/>.
		/// </summary>
		/// <returns></returns>
		public abstract Geometry Clone();

		object ICloneable.Clone()
		{
			return this.Clone();
		}

		#endregion
	}
}
