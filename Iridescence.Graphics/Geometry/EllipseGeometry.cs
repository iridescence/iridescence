﻿using System.Collections.Immutable;
using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents ellipsoid geometry.
	/// </summary>
	public class EllipseGeometry : Geometry
	{
		#region Properties

		/// <summary>
		/// Gets the center of the ellipse.
		/// </summary>
		public Vector2D Center { get; }

		/// <summary>
		/// Gets the radius of the ellipse.
		/// </summary>
		public Vector2D Radius { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="EllipseGeometry"/> class. 
		/// </summary>
		public EllipseGeometry(Vector2D center, Vector2D radius, Transform transform = null)
			: base(transform)
		{
			this.Center = center;
			this.Radius = radius;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EllipseGeometry"/> class from a rectangle that defines the ellipse bounds.
		/// </summary>
		public EllipseGeometry(RectangleD rectangle, Transform transform = null)
			: base(transform)
		{
			this.Center = rectangle.Center;
			this.Radius = rectangle.Size * 0.5;
		}

		#endregion

		#region Methods

		public override RectangleD GetBounds(Transform transform)
		{
			return RectangleGeometry.GetBounds(new RectangleD(this.Center.X - this.Radius.X, this.Center.Y - this.Radius.Y, 2.0 * this.Radius.X, 2.0 * this.Radius.Y), this.Transform.Combine(transform));
		}

		public override RectangleD GetBounds(in TransformMatrix transform)
		{
			this.Transform.ToMatrixCombined(transform, out TransformMatrix mat);
			return RectangleGeometry.GetBounds(new RectangleD(this.Center.X - this.Radius.X, this.Center.Y - this.Radius.Y, 2.0 * this.Radius.X, 2.0 * this.Radius.Y), mat);
		}

		public override PathGeometry ToPath()
		{
			Vector2D p1;
			p1.X = this.Center.X;
			p1.Y = this.Center.Y - this.Radius.Y;

			Vector2D p2;
			p2.X = this.Center.X;
			p2.Y = this.Center.Y + this.Radius.Y;

			ArcPathSegment seg1 = new ArcPathSegment(p2, this.Radius);
			ArcPathSegment seg2 = new ArcPathSegment(p1, this.Radius);

			PathFigure figure = new PathFigure(p1, ImmutableArray.Create<PathSegment>(seg1, seg2), isClosed: true);
			return new PathGeometry(figure, transform: this.Transform);
		}

		public override Geometry Clone()
		{
			return new EllipseGeometry(this.Center, this.Radius, this.Transform);
		}

		#endregion
	}
}
