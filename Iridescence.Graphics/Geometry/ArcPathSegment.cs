﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents an elliptical arc between two points.
	/// </summary>
	public class ArcPathSegment : PathSegment
	{
		#region Properties

		/// <summary>
		/// Gets the arc's destination point.
		/// </summary>
		public Vector2D Point { get; }
		
		/// <summary>
		/// Gets the radius of the arc.
		/// </summary>
		public Vector2D Radius { get; }

		/// <summary>
		/// Gets the arc's rotation angle.
		/// </summary>
		public double RotationAngle { get; }

		/// <summary>
		/// Gets a value that indicates whether the arc is greater than 180 degrees.
		/// </summary>
		public bool IsLargeArc { get; }

		/// <summary>
		/// Gets the arc's sweep direction.
		/// </summary>
		public SweepDirection SweepDirection { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ArcPathSegment"/> class. 
		/// </summary>
		public ArcPathSegment(Vector2D point, Vector2D radius, double rotationAngle = 0.0, bool isLargeArc = false, SweepDirection sweepDirection = SweepDirection.CounterClockwise, bool? isSmoothJoin = null)
			: base(isSmoothJoin)
		{
			this.Point = point;
			this.Radius = radius;
			this.RotationAngle = rotationAngle;
			this.IsLargeArc = isLargeArc;
			this.SweepDirection = sweepDirection;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the <see cref="EllipticalArc"/> that represents this <see cref="ArcPathSegment"/> for the specified start point and transform, if there is any.
		/// </summary>
		/// <returns>True, if the arc has been computed. False if a straight line from the start point to the end point should be used instead.</returns>
		public bool TryGetEllipticalArc(in Vector2D startPoint, in TransformMatrix mat, out EllipticalArc arc)
		{
			TransformMatrix.Transform(mat, startPoint, out Vector2D p1);
			TransformMatrix.Transform(mat, this.Point, out Vector2D p2);
			Vector2D r;
			r.X = this.Radius.X * Utility.Sqrt(mat.Matrix.M11 * mat.Matrix.M11 + mat.Matrix.M21 * mat.Matrix.M21);
			r.Y = this.Radius.Y * Utility.Sqrt(mat.Matrix.M12 * mat.Matrix.M12 + mat.Matrix.M22 * mat.Matrix.M22);
			Vector2D rv = new Vector2D(Utility.Cos(this.RotationAngle), Utility.Sin(this.RotationAngle));
			Matrix2x2D.Multiply(mat.Matrix, rv, out rv);
			double rotAngle = Utility.Atan2(rv.Y, rv.X);

			return EllipticalArc.FromEndPointParameterization(p1, p2, r, rotAngle, this.IsLargeArc, this.SweepDirection == SweepDirection.CounterClockwise, out arc);
		}

		/// <summary>
		/// Returns the <see cref="EllipticalArc"/> that represents this <see cref="ArcPathSegment"/> for the specified start point and transform, if there is any.
		/// </summary>
		/// <returns>True, if the arc has been computed. False if a straight line from the start point to the end point should be used instead.</returns>
		public bool TryGetEllipticalArc(in Vector2D startPoint, Transform transform, out EllipticalArc arc)
		{
			if (transform == null)
			{
				return EllipticalArc.FromEndPointParameterization(startPoint, this.Point, this.Radius, this.RotationAngle, this.IsLargeArc, this.SweepDirection == SweepDirection.CounterClockwise, out arc);
			}

			transform.ToMatrix(out TransformMatrix mat);
			return this.TryGetEllipticalArc(startPoint, mat, out arc);
		}

		public override Vector2D GetEndPoint()
		{
			return this.Point;
		}

		public override RectangleD GetBounds(Vector2D startPoint, Transform transform)
		{
			if (!this.TryGetEllipticalArc(startPoint, transform, out EllipticalArc arc))
			{
				Vector2D min;
				Vector2D max;

				if (transform == null)
				{
					Vector2D.Min(startPoint, this.Point, out min);
					Vector2D.Max(startPoint, this.Point, out max);
				}
				else
				{
					transform.TransformPoint(startPoint, out Vector2D p1);
					transform.TransformPoint(this.Point, out Vector2D p2);

					Vector2D.Min(p1, p2, out min);
					Vector2D.Max(p1, p2, out max);
				}

				Vector2D.Subtract(max, min, out max);
				return new RectangleD(min, max);
			}

			return arc.GetBounds();
		}

		public override RectangleD GetBounds(Vector2D startPoint, in TransformMatrix transform)
		{
			if (!this.TryGetEllipticalArc(startPoint, transform, out EllipticalArc arc))
			{
				TransformMatrix.Transform(transform, startPoint, out Vector2D p1);
				TransformMatrix.Transform(transform, this.Point, out Vector2D p2);

				Vector2D.Min(p1, p2, out Vector2D min);
				Vector2D.Max(p1, p2, out Vector2D max);

				Vector2D.Subtract(max, min, out max);
				return new RectangleD(min, max);
			}

			return arc.GetBounds();
		}

		public override void Tessellate(PathTessellator tessellator)
		{
			if (this.TryGetEllipticalArc(tessellator.LastPoint, null, out EllipticalArc arc))
			{
				tessellator.AddEllipticalArc(arc, this.IsSmoothJoin);
			}
			else
			{
				tessellator.AddPoint(this.Point, this.IsSmoothJoin);
			}
		}

		public override PathSegment Clone()
		{
			return new ArcPathSegment(this.Point, this.Radius, this.RotationAngle, this.IsLargeArc, this.SweepDirection, this.IsSmoothJoin);
		}

		#endregion
	}

	public static class ArcPathSegmentExtensions
	{
		/// <summary>
		/// Adds an <see cref="ArcPathSegment"/> with the specified parameters.
		/// </summary>
		/// <param name="figure">The figure to add the segment to.</param>
		/// <param name="point">The arc end point</param>
		/// <param name="radius">The radius of the ellipse.</param>
		/// <param name="rotationAngle">The arc rotation angle.</param>
		/// <param name="isLargeArc">Whether the arc should be greater than 180 degrees.</param>
		/// <param name="sweepDirection">The arc sweep direction.</param>
		/// <param name="isSmoothJoin">Whether the arc is joined smoothly.</param>
		public static PathFigure ArcTo(this PathFigure figure, Vector2D point, Vector2D radius, double rotationAngle = 0.0, bool isLargeArc = false, SweepDirection sweepDirection = SweepDirection.CounterClockwise, bool? isSmoothJoin = null)
		{
			return figure.AddSegment(new ArcPathSegment(point, radius, rotationAngle, isLargeArc, sweepDirection, isSmoothJoin));
		}
	}
}
