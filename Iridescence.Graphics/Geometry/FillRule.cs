﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines the geometry fill rules.
	/// </summary>
	public enum FillRule
	{
		EvenOdd,
		NonZero
	}
}
