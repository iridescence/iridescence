﻿using Iridescence.Math;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a quadratic Bézier curve path segment.
	/// </summary>
	public class QuadraticBezierPathSegment : PathSegment
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the control point of the Bézier curve.
		/// </summary>
		public Vector2D ControlPoint { get; }

		/// <summary>
		/// Gets the end point of the Bézier curve.
		/// </summary>
		public Vector2D EndPoint { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="QuadraticBezierPathSegment"/> class. 
		/// </summary>
		public QuadraticBezierPathSegment(Vector2D controlPoint, Vector2D endPoint, bool? isSmoothJoin = null)
			: base(isSmoothJoin)
		{
			this.ControlPoint = controlPoint;
			this.EndPoint = endPoint;
		}

		#endregion

		#region Methods

		public override RectangleD GetBounds(Vector2D startPoint, Transform transform)
		{
			QuadraticBezier bezier;

			if (transform == null)
			{
				bezier.Point1 = startPoint;
				bezier.Point2 = this.ControlPoint;
				bezier.Point3 = this.EndPoint;
			}
			else
			{
				bezier.Point1 = transform.TransformPoint(startPoint);
				bezier.Point2 = transform.TransformPoint(this.ControlPoint);
				bezier.Point3 = transform.TransformPoint(this.EndPoint);
			}

			return bezier.GetBounds();
		}

		public override RectangleD GetBounds(Vector2D startPoint, in TransformMatrix transform)
		{
			QuadraticBezier bezier;
			TransformMatrix.Transform(transform, startPoint, out bezier.Point1);
			TransformMatrix.Transform(transform, this.ControlPoint, out bezier.Point2);
			TransformMatrix.Transform(transform, this.EndPoint, out bezier.Point3);
			return bezier.GetBounds();
		}


		public override Vector2D GetEndPoint()
		{
			return this.EndPoint;
		}

		public override void Tessellate(PathTessellator tessellator)
		{
			tessellator.AddQuadraticBezier(this.ControlPoint, this.EndPoint, this.IsSmoothJoin);
		}

		public override PathSegment Clone()
		{
			return new QuadraticBezierPathSegment(this.ControlPoint, this.EndPoint, this.IsSmoothJoin);
		}

		#endregion
	}

	public static class QuadraticBezierPathSegmentExtensions
	{
		/// <summary>
		/// Adds a <see cref="QuadraticBezierPathSegment"/> with the specified control and end point.
		/// </summary>
		/// <param name="figure">The figure to add the segment to.</param>
		/// <param name="controlPoint">The Bézier curve control point.</param>
		/// <param name="endPoint">The end point of the curve.</param>
		/// <param name="isSmoothJoin">Whether the curve is joined smoothly.</param>
		public static PathFigure QuadTo(this PathFigure figure, Vector2D controlPoint, Vector2D endPoint, bool? isSmoothJoin = null)
		{
			return figure.AddSegment(new QuadraticBezierPathSegment(controlPoint, endPoint));
		}

		/// <summary>
		/// Adds a <see cref="QuadraticBezierPathSegment"/> with the specified control and end point.
		/// </summary>
		/// <param name="figure">The figure to add the segment to.</param>
		/// <param name="cx">The X position of the Bézier curve control point.</param>
		/// <param name="cy">The Y position of the Bézier curve control point.</param>	
		/// <param name="x">The X position of the end point.</param>
		/// <param name="y">The Y position of the end point.</param>
		/// <param name="isSmoothJoin">Whether the curve is joined smoothly.</param>
		public static PathFigure QuadTo(this PathFigure figure, double cx, double cy, double x, double y, bool? isSmoothJoin = null)
		{
			return figure.AddSegment(new QuadraticBezierPathSegment(new Vector2D(cx, cy), new Vector2D(x, y)));
		}
	}
}
