﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines line join modes.
	/// </summary>
	public enum LineJoin
	{
		Miter,
		Round,
		Bevel
	}
}
