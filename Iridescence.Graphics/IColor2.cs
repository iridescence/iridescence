﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Interface for generic RG tuples.
	/// </summary>
	public interface IColor2<T>
	{
		/// <summary>
		/// Gets or sets the red channel.
		/// </summary>
		T R { get; set; }

		/// <summary>
		/// Gets or sets the green channel.
		/// </summary>
		T G { get; set; }
	}
}
