﻿using System;
using Iridescence.Memory;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Holds properties of a pixel data dimension.
	/// </summary>
	[Serializable]
	public readonly struct PixelDimension : IEquatable<PixelDimension>
	{
		#region Fields

		/// <summary>
		/// The stride (i.e. distance between two elements).
		/// </summary>
		public readonly int Stride;

		/// <summary>
		/// The size of one element.
		/// </summary>
		public readonly int ElementSize;

		/// <summary>
		/// The number of elements.
		/// </summary>
		public readonly int Count;

		/// <summary>
		/// Gets a value that indicates whether this pixel dimension is empty.
		/// </summary>
		public bool IsEmpty => this.Stride == 0 && this.ElementSize == 0 && this.Count == 0;
		
		/// <summary>
		/// Gets a value that indicates whether there is no unused space between elements (i.e. <see cref="Stride"/> equals <see cref="ElementSize"/>).
		/// </summary>
		public bool IsConsecutive => this.Stride == this.ElementSize;

		/// <summary>
		/// Gets the minimum data length required for this dimension.
		/// </summary>
		public int Length
		{
			get
			{
				if (this.Count == 0)
					return 0;

				return (this.Count - 1) * this.Stride + this.ElementSize;
			}
		}

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public PixelDimension(int stride, int elementSize, int elementCount)
		{
			if (elementSize < 0)
				throw new ArgumentOutOfRangeException(nameof(elementSize));

			if (elementCount < 0)
				throw new ArgumentOutOfRangeException(nameof(elementCount));

			this.Stride = stride;
			this.ElementSize = elementSize;
			this.Count = elementCount;
		}

		#endregion

		#region Methods

		public DataDimension GetDataDimension(string name = null, bool checkBounds = true)
		{
			DataDimensionFlags flags = checkBounds ? DataDimensionFlags.CheckBounds : DataDimensionFlags.None;
			return new DataDimension(flags, this.Stride, 0, this.Count, name);
		}
		
		public bool Equals(PixelDimension other)
		{
			return this.Stride == other.Stride && this.ElementSize == other.ElementSize && this.Count == other.Count;
		}

		public bool Equals(in PixelDimension other)
		{
			return this.Stride == other.Stride && this.ElementSize == other.ElementSize && this.Count == other.Count;
		}
		
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is PixelDimension specification && this.Equals(specification);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(this.Stride, this.ElementSize, this.Count);
		}

		public static bool operator ==(in PixelDimension left, in PixelDimension right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(in PixelDimension left, in PixelDimension right)
		{
			return !left.Equals(right);
		}

		public override string ToString()
		{
			return $"Size={this.ElementSize}, Stride={this.Stride}, Count={this.Count}";
		}

		#endregion
	}
}
