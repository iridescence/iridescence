﻿using System;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a dummy image that contains no real data.
	/// Reading from this image will result in zeroes, writing has no effect.
	/// </summary>
	[Serializable]
	public sealed class NullImage : Image
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NullImage class. 
		/// </summary>
		public NullImage(Format format, int width, int height, int depth)
			: base(format, width, height, depth)
		{

		}

		#endregion

		#region Methods

		public override void Read(MemoryImage dest, int sourceX, int sourceY, int sourceZ)
		{
			dest.PixelBox.Clear();
		}

		public override void Write(MemoryImage source, int destX, int destY, int destZ)
		{
			// do nothing.
		}

		#endregion

	}
}
