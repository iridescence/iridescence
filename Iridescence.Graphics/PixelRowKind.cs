﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines the direction of a pixel row.
	/// </summary>
	public enum PixelRowKind
	{
		Row = 1,
		Column,
	}
}
