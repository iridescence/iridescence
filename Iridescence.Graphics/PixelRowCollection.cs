﻿using System;
using System.Runtime.CompilerServices;

namespace Iridescence.Graphics
{
	/// <summary>
	/// A collection of pixel rows in a <see cref="PixelSlice"/>.
	/// </summary>
	public readonly ref struct PixelRowCollection
	{
		private readonly Span<byte> span;
		private readonly PixelDimension dimX;
		private readonly PixelDimension dimY;
		private readonly IPixelInfo info;

		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the span containing the pixel data.
		/// </summary>
		public Span<byte> Span => this.span;

		public PixelDimension XDimension => this.dimX;

		public PixelDimension YDimension => this.dimY;

		public int Width => this.dimX.Count;

		public int Count => this.dimY.Count;

		public PixelRow this[int index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if (unchecked((uint)index) >= this.Count)
					throw new ArgumentOutOfRangeException(nameof(index));

				return new PixelRow(this.span, this.dimX, this.info, index * this.dimY.Stride);
			}
		}

		public bool IsConsecutive => this.dimY.IsConsecutive && this.dimX.IsConsecutive;

		#endregion

		#region Constructors

		public PixelRowCollection(Span<byte> span, in PixelDimension dimX, in PixelDimension dimY, IPixelInfo info)
		{
			this.span = span;
			this.dimX = dimX;
			this.dimY = dimY;
			this.info = info;
		}

		#endregion

		#region Enumerator

		/// <summary>
		/// Returns an enumerator for the collection.
		/// </summary>
		/// <returns></returns>
		public Enumerator GetEnumerator() => new Enumerator(this);

		/// <summary>
		/// Enumerator implementation for <see cref="PixelRowCollection"/>.
		/// </summary>
		public ref struct Enumerator
		{
			#region Fields

			private readonly PixelRowCollection collection;
			private PixelRow current;
			private int index;

			#endregion

			#region Properties

			public PixelRow Current => this.current;

			#endregion

			#region Constructors

			public Enumerator(in PixelRowCollection collection)
			{
				this.collection = collection;
				this.current = default;
				this.index = 0;
			}

			#endregion

			#region Methods
		
			public void Dispose()
			{
				this.current = default;
			}

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public bool MoveNext()
			{
				if (this.index < this.collection.dimY.Count)
				{
					this.current = new PixelRow(
						this.collection.span, 
						this.collection.dimX, 
						this.collection.info,
						this.index * this.collection.dimY.Stride);

					++this.index;
					return true;
				}

				return false;
			}

			public void Reset()
			{
				this.current = default;
				this.index = 0;
			}

			#endregion
		}

		#endregion
	}
}
