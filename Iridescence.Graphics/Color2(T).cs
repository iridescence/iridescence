﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics
{
	[StructLayout(LayoutKind.Sequential)]
	[Serializable]
	[ColorStruct(2)]
	public struct Color2<T> : IColor2<T>
	{
		#region Fields
		
		public T R;
		public T G;

		#endregion
		
		#region Properties
		
		T IColor2<T>.R
		{
			get => this.R;
			set => this.R = value;
		}

		T IColor2<T>.G
		{
			get => this.G;
			set => this.G = value;
		}

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Color2"/> class. 
		/// </summary>
		public Color2(T r, T g)
		{
			this.R = r;
			this.G = g;
		}

		#endregion
		
		#region Methods
		
		#endregion
	}
}
