﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a single row of pixels.
	/// </summary>
	public readonly ref struct PixelRow
	{
		private readonly Span<byte> span;
		private readonly PixelDimension dimX;
		private readonly IPixelInfo info;

		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the span of the row.
		/// </summary>
		public Span<byte> Span => this.span;

		/// <summary>
		/// Gets the X dimension specification.
		/// </summary>
		public PixelDimension XDimension => this.dimX;

		/// <summary>
		/// Gets the length/width of the row (in pixels).
		/// </summary>
		public int Width => this.dimX.Count;

		/// <summary>
		/// Gets a collection that enumerates the spans of individual pixels.
		/// </summary>
		public PixelCollection Pixels => new PixelCollection(this.span, this.dimX);

		public bool IsEmpty => this.dimX.IsEmpty;
		
		public bool IsConsecutive => this.dimX.IsConsecutive;
		
		#endregion

		#region Constructors

		internal PixelRow(Span<byte> span, in PixelDimension dimX, IPixelInfo info, int offset)
		{
			this.span = span.Slice(offset, dimX.Length);
			this.dimX = dimX;
			this.info = info;
		}

		public PixelRow(Span<byte> span, in PixelDimension dimX, IPixelInfo info)
		{
			this.span = span.Slice(0, dimX.Length);
			this.dimX = dimX;
			this.info = info;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Clears the contents of this pixel row.
		/// </summary>
		public void Clear()
		{
			if (this.IsConsecutive)
			{
				this.span.Clear();
			}
			else
			{
				foreach (Span<byte> p in this.Pixels)
				{
					p.Clear();
				}
			}
		}

		/// <summary>
		/// Sets each pixel in the row to the specified raw pixel data.
		/// The number of bytes in the specified <paramref name="pixel"/> must match the element size of the pixel data.
		/// </summary>
		/// <param name="pixel"></param>
		public void FillRaw(ReadOnlySpan<byte> pixel)
		{
			if (pixel.Length != this.dimX.ElementSize)
				throw new ArgumentException("Fill data length must match pixel size.", nameof(pixel));

			if (this.IsEmpty)
				return;

			if (this.IsConsecutive)
			{
				Span<byte> rowData = this.span;
				pixel.CopyTo(rowData);
				PixelHelper.FillLogN(rowData, pixel.Length);
			}
			else
			{
				foreach (Span<byte> p in this.Pixels)
				{
					pixel.CopyTo(p);
				}
			}
		}
		
		/// <summary>
		/// Copies this <see cref="PixelRow"/> to the specified <see cref="PixelRow"/>.
		/// Performs no data conversion and copies pixel byte data as is.
		/// </summary>
		/// <param name="dest">The destination <see cref="PixelRow"/>. Must be at least as long as this <see cref="PixelRow"/>.</param>
		public void CopyToRaw(in PixelRow dest)
		{
			if (dest.Width < this.Width)
				throw new ArgumentException("Destination row is too short.", nameof(dest));

			if (dest.dimX.ElementSize != this.dimX.ElementSize)
				throw new ArgumentException("Pixels sizes do not match.", nameof(dest));

			if (this.IsConsecutive && dest.IsConsecutive)
			{
				// Fast path for when both rows are consecutive and have no unused space between pixels.
				this.span.CopyTo(dest.span);
			}
			else
			{
				// Slow path, copies each pixel individually.
				PixelCollection sourceSpans = this.Pixels;
				PixelCollection destSpans = dest.Pixels;

				for (int i = 0; i < this.Width; ++i)
				{
					sourceSpans[i].CopyTo(destSpans[i]);
				}
			}
		}

		/// <summary>
		/// Reads the pixel data of the row into the specified destination span.
		/// Performs conversion from the internal pixel format to the specified type <typeparamref name="T"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dest"></param>
		public void Read<T>(Span<T> dest)
		{
			if (this.info == null)
				throw new InvalidOperationException("The pixel row has no pixel data information associated with it. Conversion is not possible.");

			if (dest.Length < this.Width)
				throw new ArgumentException("Destination span is too short.", nameof(dest));

			this.info.Read(this.span, dest, this.dimX.Stride, this.dimX.Count);
		}

		/// <summary>
		/// Writes the pixel data from the specified source into this row.
		/// Performs conversion from the specified type <typeparamref name="T"/> to the internal pixel format.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		public void Write<T>(ReadOnlySpan<T> source)
		{
			if (this.info == null)
				throw new InvalidOperationException("The pixel row has no pixel data information associated with it. Conversion is not possible.");

			if (source.Length > this.Width)
				throw new ArgumentException("Source span is too long.", nameof(source));

			this.info.Write(source, this.span, this.dimX.Stride, this.dimX.Count);
		}
		
		/// <summary>
		/// Gets an enumerator that returns the pixels reinterpreted as a specific type <typeparamref name="T"/>.
		/// No cast is performed. The data is read directly from raw memory.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public PixelCollection<T> InterpretAs<T>()
			where T : struct
		{
			return new PixelCollection<T>(this.span, this.dimX);
		}

		/// <summary>
		/// Tries to get the pixel data as a <see cref="Span{T}"/> of the specified type.
		/// The raw data is reinterpreted as the specified type <typeparamref name="T"/>. No cast is performed.
		/// This does not work for non-contiguous memory and might also not be possible on platforms with stricter alignment rules.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="pixelSpan"></param>
		/// <returns></returns>
		public bool TryInterpretAsSpan<T>(out Span<T> pixelSpan)
			where T : struct
		{
			if (this.IsConsecutive)
			{
				// TODO: This does not work on platforms that have stricter alignment requirements than x86.
				// There is a TryCast method in the works.
				pixelSpan = MemoryMarshal.Cast<byte, T>(this.span);
				return true;
			}

			pixelSpan = Span<T>.Empty;
			return false;
		}

		/// <summary>
		/// Returns a sub-region of this <see cref="PixelRow"/>, starting from the specified pixel and containing the specified number of pixels.
		/// </summary>
		/// <param name="start">The new starting pixel.</param>
		/// <param name="length">The number of pixels.</param>
		/// <returns></returns>
		public PixelRow Slice(int start, int length)
		{
			if ((uint)start > (uint)this.Width || (uint)length > (uint)(this.Width - start))
				throw new ArgumentOutOfRangeException();

			PixelDimension x = new PixelDimension(this.dimX.Stride, this.dimX.ElementSize, length);
			return new PixelRow(this.span.Slice(x.Stride * start, x.Length), x, this.info);
		}

		/// <summary>
		/// Returns a sub-region of this <see cref="PixelRow"/>, starting from the specified pixel.
		/// </summary>
		/// <param name="start">The new starting pixel.</param>
		/// <returns></returns>
		public PixelRow Slice(int start)
		{
			return this.Slice(start, this.Width - start);
		}

		#endregion
	}
}