﻿using System;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a two-dimensional slice of pixels.
	/// </summary>
	public readonly ref struct PixelSlice
	{
		private readonly Span<byte> span;
		private readonly PixelDimension dimX;
		private readonly PixelDimension dimY;
		private readonly IPixelInfo info;

		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the span of the slice.
		/// </summary>
		public Span<byte> Span => this.span;

		/// <summary>
		/// Gets the X dimension specification.
		/// </summary>
		public PixelDimension XDimension => this.dimX;

		/// <summary>
		/// Gets the Y dimension specification.
		/// </summary>
		public PixelDimension YDimension => this.dimY;

		/// <summary>
		/// Gets the width (in pixels) of the slice.
		/// </summary>
		public int Width => this.dimX.Count;

		/// <summary>
		/// Gets the height (in pixels) of the slice.
		/// </summary>
		public int Height => this.dimY.Count;

		/// <summary>
		/// Gets the rows of the slice.
		/// </summary>
		public PixelRowCollection Rows => new PixelRowCollection(this.span, this.dimX, this.dimY, this.info);

		/// <summary>
		/// Gets the columns of the slice.
		/// </summary>
		public PixelRowCollection Columns
		{
			get
			{
				PixelDimension x = new PixelDimension(this.dimY.Stride, this.dimX.ElementSize, this.dimY.Count);
				PixelDimension y = new PixelDimension(this.dimX.Stride, x.Length, this.dimX.Count);
				return new PixelRowCollection(this.span, x, y, this.info);
			}
		}

		public bool IsEmpty => this.dimX.IsEmpty || this.dimY.IsEmpty;

		public bool IsConsecutive => this.dimX.IsConsecutive && this.dimY.IsConsecutive;

		#endregion

		#region Constructors

		internal PixelSlice(Span<byte> span, in PixelDimension dimX, in PixelDimension dimY, IPixelInfo info, int offset)
		{
			this.span = span.Slice(offset, dimY.Length);
			this.dimX = dimX;
			this.dimY = dimY;
			this.info = info;
		}

		public PixelSlice(Span<byte> span, in PixelDimension dimX, in PixelDimension dimY, IPixelInfo info)
		{
			this.span = span.Slice(0, dimY.Length);
			this.dimX = dimX;
			this.dimY = dimY;
			this.info = info;
		}

		#endregion

		#region Methods
		
		/// <summary>
		/// Tries to choose the consecutive representation out of <see cref="Rows"/> and <see cref="Columns"/>.
		/// </summary>
		/// <param name="rows"></param>
		/// <param name="kind"></param>
		/// <returns></returns>
		public bool TryGetConsecutiveRows(out PixelRowCollection rows, out PixelRowKind kind)
		{
			rows = this.Rows;
			if (rows.IsConsecutive)
			{
				kind = PixelRowKind.Row;
				return true;
			}

			rows = this.Columns;
			if (rows.IsConsecutive)
			{
				kind = PixelRowKind.Column;
				return true;
			}

			rows = default;
			kind = 0;
			return false;
		}
		
		/// <summary>
		/// Clears the contents of this pixel slice.
		/// </summary>
		public void Clear()
		{
			if (this.IsConsecutive)
			{
				this.span.Clear();
			}
			else
			{
				if (!this.TryGetConsecutiveRows(out PixelRowCollection rows, out _))
					rows = this.Rows;

				foreach (PixelRow row in rows)
				{
					row.Clear();
				}
			}
		}

		/// <summary>
		/// Sets each pixel in the slice to the specified raw pixel data.
		/// The number of bytes in the specified <paramref name="pixel"/> must match the element size of the pixel data.
		/// </summary>
		/// <param name="pixel"></param>
		public void FillRaw(ReadOnlySpan<byte> pixel)
		{
			if (pixel.Length != this.dimX.ElementSize)
				throw new ArgumentException("Fill data length must match pixel size.", nameof(pixel));

			if (this.IsEmpty)
				return;

			if (this.IsConsecutive)
			{
				this.Rows[0].FillRaw(pixel);
				PixelHelper.FillLogN(this.span, this.dimY.Length);
			}
			else
			{
				if (!this.TryGetConsecutiveRows(out PixelRowCollection rows, out _))
					rows = this.Rows;

				foreach (PixelRow row in rows)
				{
					row.FillRaw(pixel);
				}
			}
		}
		
		/// <summary>
		/// Copies this <see cref="PixelSlice"/> to the specified <see cref="PixelSlice"/>.
		/// Performs no data conversion and copies pixel byte data as is.
		/// </summary>
		/// <param name="dest">The destination <see cref="PixelSlice"/>. Must be at least as big as this <see cref="PixelSlice"/>.</param>
		public void CopyToRaw(in PixelSlice dest)
		{
			if (dest.Width < this.Width || dest.Height < this.Height)
				throw new ArgumentException("Destination slice is too small.", nameof(dest));

			if (dest.dimX.ElementSize != this.dimX.ElementSize)
				throw new ArgumentException("Pixels sizes do not match.", nameof(dest));

			if (this.IsConsecutive && dest.IsConsecutive)
			{
				// Fast path for when both slices are consecutive and have no unused space between pixels.
				this.span.CopyTo(dest.span);
			}
			else
			{
				// Slow path, copies each row individually.
				PixelRowCollection sourceRows = this.Rows;
				PixelRowCollection destRows = dest.Rows;

				for (int i = 0; i < this.Height; ++i)
				{
					sourceRows[i].CopyToRaw(destRows[i]);
				}
			}
		}
		
		/// <summary>
		/// Returns a sub-region of this <see cref="PixelSlice"/>, starting from the specified pixel position and containing the specified number of pixels in each dimension.
		/// </summary>
		/// <param name="startX">The new starting X pixel.</param>
		/// <param name="startY">The new starting Y pixel.</param>
		/// <param name="lengthX">The number of pixels in X direction.</param>
		/// <param name="lengthY">The number of pixels in Y direction.</param>
		/// <returns></returns>
		public PixelSlice Slice(int startX, int startY, int lengthX, int lengthY)
		{
			if ((uint)startX > (uint)this.Width || (uint)lengthX > (uint)(this.Width - startX) ||
			    (uint)startY > (uint)this.Height || (uint)lengthY > (uint)(this.Height - startY))
				throw new ArgumentOutOfRangeException();

			PixelDimension x = new PixelDimension(this.dimX.Stride, this.dimX.ElementSize, lengthX);
			PixelDimension y = new PixelDimension(this.dimY.Stride, x.Length, lengthY);
			return new PixelSlice(this.span.Slice(x.Stride * startX + y.Stride * startY, y.Length), x, y, this.info);
		}

		/// <summary>
		/// Returns a sub-region of this <see cref="PixelSlice"/>, starting from the specified pixel position.
		/// </summary>
		/// <param name="startX">The new starting X pixel.</param>
		/// <param name="startY">The new starting Y pixel.</param>
		/// <returns></returns>
		public PixelSlice Slice(int startX, int startY)
		{
			return this.Slice(startX, startY, this.Width - startX, this.Height - startY);
		}

		#endregion
	}
}