﻿using System;
using System.Runtime.CompilerServices;

namespace Iridescence.Graphics
{
	/// <summary>
	/// A collection of slices in a <see cref="MemoryImage"/>.
	/// </summary>
	public readonly ref struct PixelSliceCollection
	{
		#region Fields

		private readonly Span<byte> span;
		private readonly PixelDimension dimX;
		private readonly PixelDimension dimY;
		private readonly PixelDimension dimZ;
		private readonly IPixelInfo info;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the span containing the pixel data.
		/// </summary>
		public Span<byte> Span => this.span;

		public PixelDimension XDimension => this.dimX;

		public PixelDimension YDimension => this.dimY;

		public PixelDimension ZDimension => this.dimZ;

		public int Width => this.dimX.Count;

		public int Height => this.dimY.Count;

		public int Count => this.dimZ.Count;

		public PixelSlice this[int index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if (unchecked((uint)index) >= this.Count)
					throw new ArgumentOutOfRangeException(nameof(index));

				return new PixelSlice(this.span, this.dimX, this.dimY, this.info, index * this.dimZ.Stride);
			}
		}

		public bool IsConsecutive => this.dimZ.IsConsecutive && this.dimY.IsConsecutive && this.dimX.IsConsecutive;
		
		#endregion

		#region Constructors

		public PixelSliceCollection(Span<byte> span, in PixelDimension dimX, in PixelDimension dimY, in PixelDimension dimZ, IPixelInfo info)
		{
			this.span = span;
			this.dimZ = dimZ;
			this.dimY = dimY;
			this.dimX = dimX;
			this.info = info;
		}

		#endregion

		#region Enumerator

		/// <summary>
		/// Returns an enumerator for the collection.
		/// </summary>
		/// <returns></returns>
		public Enumerator GetEnumerator() => new Enumerator(this);

		/// <summary>
		/// Enumerator implementation for <see cref="PixelSliceCollection"/>.
		/// </summary>
		public ref struct Enumerator
		{
			#region Fields

			private readonly PixelSliceCollection collection;
			private PixelSlice current;
			private int index;

			#endregion

			#region Properties

			public PixelSlice Current => this.current;
			
			#endregion

			#region Constructors

			public Enumerator(in PixelSliceCollection collection)
			{
				this.collection = collection;
				this.current = default;
				this.index = 0;
			}

			#endregion

			#region Methods
		
			public void Dispose()
			{
				this.current = default;
			}

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public bool MoveNext()
			{
				if (this.index < this.collection.dimZ.Count)
				{
					this.current = new PixelSlice(
						this.collection.span,
						this.collection.dimX,
						this.collection.dimY,
						this.collection.info,
						this.index * this.collection.dimZ.Stride);

					++this.index;
					return true;
				}

				return false;
			}

			public void Reset()
			{
				this.current = default;
				this.index = 0;
			}

			#endregion
		}

		#endregion
	}
}