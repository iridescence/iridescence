﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using Iridescence.Math;
using Iridescence.Memory;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Caches block copy delegates.
	/// </summary>
	internal static class BlockCopyCache
	{
		#region Fields

		private static readonly ConcurrentDictionary<Key, BlockCopyFunc> cache;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		static BlockCopyCache()
		{
			cache = new ConcurrentDictionary<Key, BlockCopyFunc>();
		}

		#endregion

		#region Methods

		private static StructMapEntry makeEntry(Type type, string field, bool one)
		{
			Type channelType = type.GetFieldOrPropertyType(field);

			if (one)
			{
				return new StructMapStaticEntry(typeof(GenericConstants.One<>).MakeGenericType(channelType).GetField("Value", BindingFlags.Public | BindingFlags.Static), field);
			}

			return new StructMapDefaultEntry(field);
		}

		private static IMemoryConversionCodeGenerator makeCodeGen(int sourceChannels, int destChannels, Type destType)
		{
			switch (destChannels)
			{
				case 1:
				{
					switch (sourceChannels)
					{
						case 1:
						case 2:
						case 3:
						case 4: return new StructMapConversion(("R", "R"));
					}
					break;
				}

				case 2:
				{
					switch (sourceChannels)
					{
						case 1: return new StructMapConversion(new StructMapFieldEntry("R", "R"), makeEntry(destType, "G", false));
						case 2: 
						case 3:
						case 4: return new StructMapConversion(("R", "R"), ("G", "G"));
					}
					break;
				}

				case 3:
				{
					switch (sourceChannels)
					{
						case 1: return new StructMapConversion(new StructMapFieldEntry("R", "R"), makeEntry(destType, "G", false), makeEntry(destType, "B", false));
						case 2: return new StructMapConversion(new StructMapFieldEntry("R", "R"), new StructMapFieldEntry("G", "G"), makeEntry(destType, "B", false));
						case 3:
						case 4: return new StructMapConversion(("R", "R"), ("G", "G"), ("B", "B"));
					}
					break;
				}

				case 4:
				{
					switch (sourceChannels)
					{
						case 1: return new StructMapConversion(new StructMapFieldEntry("R", "R"), makeEntry(destType, "G", false), makeEntry(destType, "B", false), makeEntry(destType, "A", true));
						case 2: return new StructMapConversion(new StructMapFieldEntry("R", "R"), new StructMapFieldEntry("G", "G"), makeEntry(destType, "B", false), makeEntry(destType, "A", true));
						case 3: return new StructMapConversion(new StructMapFieldEntry("R", "R"), new StructMapFieldEntry("G", "G"), new StructMapFieldEntry("B", "B"), makeEntry(destType, "A", true));
						case 4: return new StructMapConversion(("R", "R"), ("G", "G"), ("B", "B"), ("A", "A"));
					}
					break;
				}
			}

			throw new ArgumentException("Invalid number of channels.");
		}

		public static BlockCopyFunc GetOrAdd(
			Type sType, PixelDimension sX, PixelDimension sY, PixelDimension sZ,
			Type dType, PixelDimension dX, PixelDimension dY, PixelDimension dZ)
		{
			IMemoryConversionCodeGenerator codeGen = null;

			Key k = new Key(sType, (sX.Stride, sY.Stride, sZ.Stride), dType, (dX.Stride, dY.Stride, dZ.Stride));

			return cache.GetOrAdd(k, _ =>
			{
				DataDescriptor sDesc = new DataDescriptor(sType, sX.GetDataDimension(checkBounds: false), sY.GetDataDimension(checkBounds: false), sZ.GetDataDimension(checkBounds: false));
				DataDescriptor dDesc = new DataDescriptor(dType, dX.GetDataDimension(checkBounds: false), dY.GetDataDimension(checkBounds: false), dZ.GetDataDimension(checkBounds: false));

				if (!MemoryAccessorBuilder.CanConvert(sType, dType))
				{
					ColorStructAttribute sColor = sType.GetCustomAttribute<ColorStructAttribute>(false);
					ColorStructAttribute dColor = dType.GetCustomAttribute<ColorStructAttribute>(false);

					if (sColor != null && dColor != null)
						codeGen = makeCodeGen(sColor.Channels, dColor.Channels, dType);

					if (codeGen == null)
						throw new InvalidOperationException($"Can not convert from {dType} to {dType}.");
				}

				BlockCopyFunc copy = (BlockCopyFunc)sDesc.CompileBlockCopy(dDesc, typeof(BlockCopyFunc), codeGen);
				return copy;
			});
		}

		#endregion

		#region Nested Types

		private readonly struct Key : IEquatable<Key>
		{
			public readonly Type SourceType;
			public readonly Vector3I SourceStride;
			public readonly Type DestType;
			public readonly Vector3I DestStride;

			public Key(Type sourceType, Vector3I sourceStride, Type destType, Vector3I destStride)
			{
				this.SourceType = sourceType;
				this.SourceStride = sourceStride;
				this.DestType = destType;
				this.DestStride = destStride;
			}

			public bool Equals(Key other)
			{
				return this.SourceType == other.SourceType && this.SourceStride.Equals(other.SourceStride) && 
				       this.DestType == other.DestType && this.DestStride.Equals(other.DestStride);
			}

			public override bool Equals(object obj)
			{
				if (ReferenceEquals(null, obj)) return false;
				return obj is Key other && this.Equals(other);
			}

			public override int GetHashCode()
			{
				return HashCode.Combine(
					this.SourceType, this.SourceStride.X, this.SourceStride.Y, this.SourceStride.Z,
					this.DestType, this.DestStride.X, this.DestStride.Y, this.DestStride.Z);
			}
		}

		#endregion
	}
}
