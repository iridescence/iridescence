﻿using System;
using System.Runtime.InteropServices;
using Iridescence.Memory;

namespace Iridescence.Graphics
{
	internal delegate void ReadFunc<T>(ref byte source, Span<T> dest, int sourceX, int destX, int countX);

	internal sealed class MemoryPixelInfo : IPixelInfo
	{
		#region Fields
		
		private readonly Type realType;

		private Delegate cachedReadFunc;
		private int cachedStrideX;

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MemoryPixelInfo"/> class. 
		/// </summary>
		public MemoryPixelInfo(Type realType)
		{
			this.realType = realType;
		}
		
		#endregion
		
		#region Methods
	
		public void Read<T>(ReadOnlySpan<byte> source, Span<T> dest, int sourceStrideX, int countX)
		{
			if (!(this.cachedReadFunc is ReadFunc<T> readFunc) || this.cachedStrideX != sourceStrideX)
			{
				DataDescriptor srcDesc = new DataDescriptor(this.realType, new DataDimension(DataDimensionFlags.None, sourceStrideX));
				DataDescriptor dstDesc = new DataDescriptor(typeof(T), new DataDimension(DataDimensionFlags.ElementStride, 1));
				readFunc = srcDesc.CompileBlockCopy<ReadFunc<T>>(dstDesc, PixelHelper.ColorIdentityMapping);

				this.cachedReadFunc = readFunc;
				this.cachedStrideX = sourceStrideX;
			}

			ref byte src = ref MemoryMarshal.GetReference(source);
			readFunc(ref src, dest, 0, 0, countX);
		}

		public void Write<T>(ReadOnlySpan<T> source, Span<byte> dest, int destStrideX, int countX)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
