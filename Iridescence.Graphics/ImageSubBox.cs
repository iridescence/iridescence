﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represenst a sub-box of an image.
	/// </summary>
	[Serializable]
	public sealed class ImageSubBox : Image
	{
		#region Fields

		private readonly Image parent;
		private readonly int x;
		private readonly int y;
		private readonly int z;
		
		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ImageSubBox.
		/// </summary>
		public ImageSubBox(Image parent, int x, int y, int z, int width, int height, int depth)
			: base(parent.Format, width, height, depth)
		{
			if (parent == null)
				throw new ArgumentNullException(nameof(parent));

			if (x < 0 || x + width > parent.Width)
				throw new ArgumentOutOfRangeException(nameof(x));

			if (y < 0 || y + height > parent.Height)
				throw new ArgumentOutOfRangeException(nameof(y));

			if (z < 0 || z + depth > parent.Depth)
				throw new ArgumentOutOfRangeException(nameof(z));

			this.parent = parent;
			this.x = x;
			this.y = y;
			this.z = z;
		}

		#endregion

		#region Methods

		public override void Read(MemoryImage dest, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, dest.Width, dest.Height, dest.Depth);
			this.parent.Read(dest, this.x + x, this.y + y, this.z + z);
		}

		public override Task ReadAsync(MemoryImage dest, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, dest.Width, dest.Height, dest.Depth);
			return this.parent.ReadAsync(dest, this.x + x, this.y + y, this.z + z);
		}

		public override void Write(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source.Width, source.Height, source.Depth);
			this.parent.Write(source, this.x + x, this.y + y, this.z + z);
		}

		public override Task WriteAsync(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source.Width, source.Height, source.Depth);
			return this.parent.WriteAsync(source, this.x + x, this.y + y, this.z + z);
		}

		public override void CopyTo(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			this.ThrowIfOutOfBounds(sourceX, sourceY, sourceZ, width, height, depth);
			this.parent.CopyTo(dest, this.x + sourceX, this.y + sourceY, this.z + sourceZ, destX, destY, destZ, width, height, depth);
		}

		public override Task CopyToAsync(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			this.ThrowIfOutOfBounds(sourceX, sourceY, sourceZ, width, height, depth);
			return this.parent.CopyToAsync(dest, this.x + sourceX, this.y + sourceY, this.z + sourceZ, destX, destY, destZ, width, height, depth);
		}

		#endregion
	}
}
