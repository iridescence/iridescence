﻿using System;
using System.Runtime.CompilerServices;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Enumerates pixels of a specific type.
	/// </summary>
	public readonly ref struct PixelCollection<T>
		where T : struct
	{
		private readonly Span<byte> span;
		private readonly PixelDimension dimX;

		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the span containing the pixel data.
		/// </summary>
		public Span<byte> Span => this.span;

		/// <summary>
		/// Gets the dimension specification.
		/// </summary>
		public PixelDimension XDimension => this.dimX;

		/// <summary>
		/// Gets the number of pixels in the collection.
		/// </summary>
		public int Count => this.dimX.Count;

		/// <summary>
		/// Gets the pixel data at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public T this[int index]
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if (unchecked((uint)index) >= this.Count)
					throw new ArgumentOutOfRangeException(nameof(index));

				return Unsafe.ReadUnaligned<T>(ref this.span[index * this.dimX.Stride]);
			}
		}

		/// <summary>
		/// Gets a value that indicates whether the pixels are contiguous.
		/// </summary>
		public bool IsConsecutive => this.dimX.IsConsecutive;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PixelCollection{T}"/> class. 
		/// </summary>
		public PixelCollection(Span<byte> span, in PixelDimension pixels)
		{
			if (pixels.ElementSize != Unsafe.SizeOf<T>())
				throw new ArgumentException("Size of type does not match pixel size.", nameof(pixels));

			this.span = span;
			this.dimX = pixels;
		}

		#endregion

		#region Enumerator

		/// <summary>
		/// Returns an enumerator for the collection.
		/// </summary>
		/// <returns></returns>
		public Enumerator GetEnumerator() => new Enumerator(this);

		/// <summary>
		/// Enumerator implementation for <see cref="PixelCollection{T}"/>.
		/// </summary>
		public ref struct Enumerator
		{
			#region Fields

			private readonly PixelCollection<T> collection;
			private T current;
			private int index;

			#endregion

			#region Properties

			public T Current => this.current;

			#endregion

			#region Constructors

			public Enumerator(in PixelCollection<T> collection)
			{
				this.collection = collection;
				this.index = 0;
				this.current = default;
			}

			#endregion

			#region Methods

			public void Dispose()
			{
				this.current = default;
			}

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			public bool MoveNext()
			{
				if (this.index < this.collection.dimX.Count)
				{
					this.current = Unsafe.ReadUnaligned<T>(ref this.collection.span[this.index * this.collection.dimX.Stride]);

					++this.index;
					return true;
				}

				return false;
			}

			public void Reset()
			{
				this.current = default;
				this.index = 0;
			}

			#endregion
		}

		#endregion
	}
}
