﻿using System;
using System.Buffers;
using System.Threading.Tasks;
using Iridescence.Memory;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Implements an <see cref="Image"/> that is backed by memory.
	/// </summary>
	[Serializable]
	public sealed class MemoryImage : Image
	{
		#region Fields

		[NonSerialized]
		private IPixelInfo pixelInfo;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the pixel box's memory.
		/// </summary>
		public Memory<byte> Memory { get; }

		/// <summary>
		/// Returns a <see cref="PixelSliceCollection"/> for the slices in this <see cref="MemoryImage"/>.
		/// </summary>
		public PixelBox PixelBox => new PixelBox(this.Memory.Span, this.XDimension, this.YDimension, this.ZDimension, this.getPixelInfo());

		/// <summary>
		/// Gets the X <see cref="PixelDimension"/> of this pixel box.
		/// </summary>
		public PixelDimension XDimension { get; }
		
		/// <summary>
		/// Gets the Y <see cref="PixelDimension"/> of this pixel box.
		/// </summary>
		public PixelDimension YDimension { get; }
		
		/// <summary>
		/// Gets the Z <see cref="PixelDimension"/> of this pixel box.
		/// </summary>
		public PixelDimension ZDimension { get; }

		#endregion

		#region Constructors

		public MemoryImage(Format format, in PixelDimension dimX, in PixelDimension dimY, in PixelDimension dimZ, Memory<byte> memory)
			: base(format, dimX.Count, dimY.Count, dimZ.Count)
		{
			this.Memory = memory;
			this.XDimension = dimX;
			this.YDimension = dimY;
			this.ZDimension = dimZ;

			verify(this.XDimension, this.YDimension, this.ZDimension);

			int minSize = this.getSize(this.Width, this.Height, this.Depth);
			if (this.Memory.Length < minSize)
				throw new ArgumentException("Memory is not big enough to hold all pixels.", nameof(this.Memory));
		}

		/// <summary>
		/// Creates a new pixel box.
		/// </summary>
		/// <param name="memory">The memory.</param>
		/// <param name="format">The format.</param>
		/// <param name="width">The width of the box.</param>
		/// <param name="height">The height of the box.</param>
		/// <param name="depth">The depth of the box.</param>
		/// <param name="rowLength">The number of pixels in one row.</param>
		/// <param name="rowCount">The number of rows in a slice.</param>
		public MemoryImage(Memory<byte> memory, Format format, int width, int height, int depth, int rowLength, int rowCount)
			: base(format, width, height, depth)
		{
			this.Memory = memory;
			this.XDimension = new PixelDimension(format.Size, format.Size, width);
			this.YDimension = new PixelDimension(format.Size * rowLength, format.Size * width, height);
			this.ZDimension = new PixelDimension(format.Size * rowLength * rowCount, format.Size * rowLength * height, depth);

			verify(this.XDimension, this.YDimension, this.ZDimension);

			int minSize = this.getSize(this.Width, this.Height, this.Depth);
			if (memory.Length < minSize)
				throw new ArgumentException("Memory is not big enough to hold all pixels.", nameof(memory));
		}

		/// <summary>
		/// Creates a new pixel box.
		/// </summary>
		/// <param name="memory">The memory.</param>
		/// <param name="format">The format.</param>
		/// <param name="width">The width of the box.</param>
		/// <param name="height">The height of the box.</param>
		/// <param name="depth">The depth of the box.</param>
		public MemoryImage(Memory<byte> memory, Format format, int width, int height = 1, int depth = 1)
			: this(memory, format, width, height, depth, width, height)
		{

		}

		#endregion

		#region Methods

		private static void verify(in PixelDimension pixels, in PixelDimension rows, in PixelDimension slices)
		{
			if (pixels.Count < 0)
				throw new ArgumentOutOfRangeException(nameof(pixels));

			if (rows.Count < 0)
				throw new ArgumentOutOfRangeException(nameof(rows));

			if (slices.Count < 0)
				throw new ArgumentOutOfRangeException(nameof(slices));

			if (pixels.ElementSize < 0)
				throw new ArgumentOutOfRangeException(nameof(pixels));

			if (rows.ElementSize < 0)
				throw new ArgumentOutOfRangeException(nameof(rows));

			if (slices.ElementSize < 0)
				throw new ArgumentOutOfRangeException(nameof(slices));

			if (rows.ElementSize < pixels.Length)
				throw new ArgumentException("Rows are not big enough to hold all pixels");

			if (slices.ElementSize < rows.Length)
				throw new ArgumentException("Slices are not big enough to hold all pixels");
		}

		private IPixelInfo getPixelInfo()
		{
			return this.pixelInfo ??= new MemoryPixelInfo(this.Format.GetElementType());
		}

		/// <summary>
		/// Creates a <see cref="MemoryImage"/> based on the specified data.
		/// </summary>
		/// <typeparam name="T">The data type. Must be blittable.</typeparam>
		/// <param name="data">The data buffer to use.</param>
		/// <param name="format">The format of the pixel box.</param>
		/// <param name="width">The width of the pixel box.</param>
		/// <param name="height">The height of the pixel box.</param>
		/// <param name="depth">The depth of the pixel box.</param>
		/// <returns></returns>
		public static MemoryImage Create<T>(T[] data, Format format, int width, int height = 1, int depth = 1) where T : struct
		{
			return new MemoryImage(new Memory<T>(data).AsBytes(), format, width, height, depth);
		}

		/// <summary>
		/// Allocates the required memory and creates a <see cref="MemoryImage"/> around it.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="depth"></param>
		/// <returns></returns>
		public static MemoryImage Allocate(Format format, int width, int height = 1, int depth = 1)
		{
			byte[] data = new byte[Format.GetSize(format.Type, width, height, depth)];
			return new MemoryImage(data, format, width, height, depth);
		}
		
		private int getSize(int w, int h, int d)
		{
			return (d - 1) * this.ZDimension.Stride + (h - 1) * this.YDimension.Stride + (w - 1) * this.XDimension.Stride + this.XDimension.ElementSize;
		}

		internal MemoryImage SliceInternal(int x, int y, int z, int width, int height, int depth)
		{
			this.ThrowIfOutOfBounds(x, y, z, width, height, depth);

			PixelDimension dimX = new PixelDimension(this.XDimension.Stride, this.XDimension.ElementSize, width);
			PixelDimension dimY = new PixelDimension(this.YDimension.Stride, dimX.Length, height);
			PixelDimension dimZ = new PixelDimension(this.ZDimension.Stride, dimY.Length, depth);
			return new MemoryImage(this.Format, dimX, dimY, dimZ, this.Memory.Slice(dimX.Stride * x + dimY.Stride * y + dimZ.Stride * z, dimZ.Length));
		}

		/// <inheritdoc />
		public override Image Slice(int x, int y, int z, int width, int height, int depth)
		{
			return this.SliceInternal(x, y, z, width, height, depth);
		}

		/// <summary>
		/// Sets the pixel box data.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="z"></param>
		/// <param name="source"></param>
		public override void Write(MemoryImage source, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, source.Width, source.Height, source.Depth);
			MemoryImage destBox = this.SliceInternal(x, y, z, source.Width, source.Height, source.Depth);
			MemoryImage.Copy(source, destBox);
		}

		/// <summary>
		/// Gets the pixel box data.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="z"></param>
		/// <param name="dest"></param>
		public override void Read(MemoryImage dest, int x, int y, int z)
		{
			this.ThrowIfOutOfBounds(x, y, z, dest.Width, dest.Height, dest.Depth);
			MemoryImage sourceBox = this.SliceInternal(x, y, z, dest.Width, dest.Height, dest.Depth);
			MemoryImage.Copy(sourceBox, dest);
		}

		/// <inheritdoc />
		public override void CopyTo(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			MemoryImage source = this.SliceInternal(sourceX, sourceY, sourceZ, width, height, depth);

			if (dest is MemoryImage destBox)
			{
				destBox = destBox.SliceInternal(destX, destY, destZ, width, height, depth);
				MemoryImage.Copy(source, destBox);
			}
			else
			{
				dest.Write(source, destX, destY, destZ);
			}
		}

		/// <inheritdoc />
		public override async Task CopyToAsync(Image dest, int sourceX, int sourceY, int sourceZ, int destX, int destY, int destZ, int width, int height, int depth)
		{
			MemoryImage source = this.SliceInternal(sourceX, sourceY, sourceZ, width, height, depth);

			if (dest is MemoryImage destBox)
			{
				// Run in background.
				await Task.Run(() =>
				{
					destBox = destBox.SliceInternal(destX, destY, destZ, width, height, depth);
					MemoryImage.Copy(source, destBox);
				});
			}
			else
			{
				await dest.WriteAsync(source, destX, destY, destZ);
			}
		}

		/// <summary>
		/// Returns a <see cref="MemoryImage"/> that contains the pixel data of this image.
		/// </summary>
		/// <param name="alwaysCopy">
		/// Specified whether the image has to be copied, even if it already is a PixelBox or the data can be used as-is in a PixelBox.
		/// Setting this to false is useful if you plan to access the data read-only.
		/// In case the image is already a PixelBox, the input is simply casted to PixelBox and no copying is done.
		/// For images whose data is not readily available (e.g. GPU images), the data is copied even if alwaysCopy is false.
		/// </param>
		/// <returns>A new PixelBox with the pixel data of this image.</returns>
		public override MemoryImage ToMemoryImage(bool alwaysCopy)
		{
			if (!alwaysCopy)
				return new MemoryImage(this.Format, this.XDimension, this.YDimension, this.ZDimension, this.Memory);

			MemoryImage box = Allocate(this.Format, this.Width, this.Height, this.Depth);
			this.Read(box, 0, 0, 0);
			return box;
		}


		/// <summary>
		/// Copies from the source image to the destination image using a dynamically emitted block copy function from <see cref="MemoryAccessorBuilder.CompileBlockCopy(DataDescriptor, DataDescriptor, Type, IMemoryConversionCodeGenerator)"/>
		/// </summary>
		/// <param name="source"></param>
		/// <param name="dest"></param>
		private static void blockCopy(MemoryImage source, MemoryImage dest)
		{
			Type sType = source.Format.GetElementType();
			Type dType = dest.Format.GetElementType();

			BlockCopyFunc copy = BlockCopyCache.GetOrAdd(sType, source.XDimension, source.YDimension, source.ZDimension, dType, dest.XDimension, dest.YDimension, dest.ZDimension);
			
			copy(ref source.Memory.Span[0], ref dest.Memory.Span[0], 0, 0, 0, 0, 0, 0, source.Width, source.Height, source.Depth);
		}

		/// <summary>
		/// Copies data from one box to another and converts the data if necessary.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="dest"></param>
		public static void Copy(MemoryImage source, MemoryImage dest)
		{
			if (source.Width != dest.Width) throw new ArgumentException("Source width and destination width do not match.");
			if (source.Height != dest.Height) throw new ArgumentException("Source height and destination height do not match.");
			if (source.Depth != dest.Depth) throw new ArgumentException("Source depth and destination depth do not match.");

			if (source.Format == dest.Format)
			{
				source.PixelBox.CopyToRaw(dest.PixelBox);
				return;
			}

			if (source.Format.IsDepthStencil && dest.Format.IsDepthStencil)
			{
				throw new NotImplementedException("Copying depth-stencil images is not implemented yet.");
			}

			if (!source.Format.IsDepthStencil && !dest.Format.IsDepthStencil)
			{
				blockCopy(source, dest);

				/*

				// Fallback: Slow floating-point conversion.
				int width = source.Width;
				int height = source.Height;
				int depth = source.Depth;

				using (source.CreateColorReader(out Func<int, int, int, Color4> reader))
				using (dest.CreateColorWriter(out Action<int, int, int, Color4> writer))
				{
					for (int z = 0; z < depth; z++)
					{
						for (int y = 0; y < height; y++)
						{
							for (int x = 0; x < width; x++)
							{
								writer(x, y, z, reader(x, y, z));
							}
						}
					}
				}*/
			}
			else
			{
				throw new InvalidOperationException("Can't copy from a color image to a depth-stencil image or vice versa.");
			}
		}
		
		#region Reader/Writer

		/// <summary>
		/// Pins the pixel box and returns one-dimensional readers for the four color channels.
		/// </summary>
		/// <typeparam name="TAddress"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="zero"></param>
		/// <param name="one"></param>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="b"></param>
		/// <param name="a"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateReaders<TAddress, TValue>(
			TValue zero,
			TValue one,
			out Func<TAddress, TValue> r,
			out Func<TAddress, TValue> g,
			out Func<TAddress, TValue> b,
			out Func<TAddress, TValue> a)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				this.Format.CreateReaders(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), zero, one, out r, out g, out b, out a);
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}

		/// <summary>
		/// Pins the pixel box and returns one-dimensional writers for the four color channels.
		/// </summary>
		/// <typeparam name="TAddress"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="zero"></param>
		/// <param name="one"></param>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="b"></param>
		/// <param name="a"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateWriters<TAddress, TValue>(
			TValue zero,
			TValue one,
			out Action<TAddress, TValue> r,
			out Action<TAddress, TValue> g,
			out Action<TAddress, TValue> b,
			out Action<TAddress, TValue> a)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				this.Format.CreateWriters(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), zero, one, out r, out g, out b, out a);
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}

		/// <summary>
		/// Pins the pixel box and returns two-dimensional readers for the four color channels.
		/// </summary>
		/// <typeparam name="TAddress"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="zero"></param>
		/// <param name="one"></param>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="b"></param>
		/// <param name="a"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateReaders<TAddress, TValue>(
			TValue zero,
			TValue one,
			out Func<TAddress, TAddress, TValue> r,
			out Func<TAddress, TAddress, TValue> g,
			out Func<TAddress, TAddress, TValue> b,
			out Func<TAddress, TAddress, TValue> a)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				this.Format.CreateReaders(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), this.YDimension.GetDataDimension(), zero, one, out r, out g, out b, out a);
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}

		/// <summary>
		/// Pins the pixel box and returns two-dimensional writers for the four color channels.
		/// </summary>
		/// <typeparam name="TAddress"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="zero"></param>
		/// <param name="one"></param>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="b"></param>
		/// <param name="a"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateWriters<TAddress, TValue>(
			TValue zero,
			TValue one,
			out Action<TAddress, TAddress, TValue> r,
			out Action<TAddress, TAddress, TValue> g,
			out Action<TAddress, TAddress, TValue> b,
			out Action<TAddress, TAddress, TValue> a)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				this.Format.CreateWriters(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), this.YDimension.GetDataDimension(), zero, one, out r, out g, out b, out a);
			}
			catch (Exception)
			{
				pin.Dispose();
				throw;
			}

			return pin;
		}

		
		/// <summary>
		/// Pins the pixel box and returns three-dimensional readers for the four color channels.
		/// </summary>
		/// <typeparam name="TAddress"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="zero"></param>
		/// <param name="one"></param>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="b"></param>
		/// <param name="a"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateReaders<TAddress, TValue>(
			TValue zero,
			TValue one,
			out Func<TAddress, TAddress, TAddress, TValue> r,
			out Func<TAddress, TAddress, TAddress, TValue> g,
			out Func<TAddress, TAddress, TAddress, TValue> b,
			out Func<TAddress, TAddress, TAddress, TValue> a)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				this.Format.CreateReaders(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), this.YDimension.GetDataDimension(), this.ZDimension.GetDataDimension(), zero, one, out r, out g, out b, out a);
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}

		/// <summary>
		/// Pins the pixel box and returns three-dimensional writers for the four color channels.
		/// </summary>
		/// <typeparam name="TAddress"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="zero"></param>
		/// <param name="one"></param>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="b"></param>
		/// <param name="a"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateWriters<TAddress, TValue>(
			TValue zero,
			TValue one,
			out Action<TAddress, TAddress, TAddress, TValue> r,
			out Action<TAddress, TAddress, TAddress, TValue> g,
			out Action<TAddress, TAddress, TAddress, TValue> b,
			out Action<TAddress, TAddress, TAddress, TValue> a)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				this.Format.CreateWriters(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), this.YDimension.GetDataDimension(), this.ZDimension.GetDataDimension(), zero, one, out r, out g, out b, out a);
			}
			catch (Exception)
			{
				pin.Dispose();
				throw;
			}

			return pin;
		}

		/// <summary>
		/// Pins the pixel box and returns a one-dimensional color reader.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateColorReader<TAddress>(out Func<TAddress, Color4> reader)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				reader = this.Format.CreateColorReader<TAddress>(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension());
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}

		/// <summary>
		/// Pins the pixel box and returns a one-dimensional color writer.
		/// </summary>
		/// <param name="writer"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateColorWriter<TAddress>(out Action<TAddress, Color4> writer)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				writer = this.Format.CreateColorWriter<TAddress>(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension());
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}

		/// <summary>
		/// Pins the pixel box and returns a two-dimensional color reader.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateColorReader<TAddress>(out Func<TAddress, TAddress, Color4> reader)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				reader = this.Format.CreateColorReader<TAddress>(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), this.YDimension.GetDataDimension());
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}
		
		/// <summary>
		/// Pins the pixel box and returns a two-dimensional color writer.
		/// </summary>
		/// <param name="writer"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateColorWriter<TAddress>(out Action<TAddress, TAddress, Color4> writer)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				writer = this.Format.CreateColorWriter<TAddress>(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), this.YDimension.GetDataDimension());
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}

		/// <summary>
		/// Pins the pixel box and returns a three-dimensional color reader.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateColorReader<TAddress>(out Func<TAddress, TAddress, TAddress, Color4> reader)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				reader = this.Format.CreateColorReader<TAddress>(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), this.YDimension.GetDataDimension(), this.ZDimension.GetDataDimension());
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}
		
		/// <summary>
		/// Pins the pixel box and returns a three-dimensional color writer.
		/// </summary>
		/// <param name="writer"></param>
		/// <returns></returns>
		public unsafe MemoryHandle CreateColorWriter<TAddress>(out Action<TAddress, TAddress, TAddress, Color4> writer)
		{
			MemoryHandle pin = this.Memory.Pin();

			try
			{
				writer = this.Format.CreateColorWriter<TAddress>(new IntPtr(pin.Pointer), this.XDimension.GetDataDimension(), this.YDimension.GetDataDimension(), this.ZDimension.GetDataDimension());
			}
			catch(Exception)
			{
				pin.Dispose();
				throw;
			}
			
			return pin;
		}

		#endregion

		#endregion
	}
}
