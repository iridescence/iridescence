﻿using System;
using System.Runtime.CompilerServices;
using Iridescence.Memory;

namespace Iridescence.Graphics
{
	public static class ImageExtensions
	{
		#region Methods

		/// <summary>
		/// Throws an exception if the specified region is not inside of this image's boundaries.
		/// </summary>
		public static void ThrowIfOutOfBounds(this ReadOnlyImage image, int x, int y, int z, int width, int height, int depth)
		{
			if (x < 0)
				throw new ArgumentOutOfRangeException(nameof(x), "X must not be negative.");

			if (y < 0)
				throw new ArgumentOutOfRangeException(nameof(y), "Y must not be negative.");

			if (z < 0)
				throw new ArgumentOutOfRangeException(nameof(z), "Z must not be negative.");

			if (x + width > image.Width)
				throw new ArgumentOutOfRangeException(nameof(width), "X+Width must not be greater than the image width.");

			if (y + height > image.Height)
				throw new ArgumentOutOfRangeException(nameof(height), "Y+Height must not be greater than the image height.");

			if (z + depth > image.Depth)
				throw new ArgumentOutOfRangeException(nameof(depth), "Z+Depth must not be greater than the image depth.");
		}


		/// <summary>
		/// Throws an exception if the specified region is not inside of this image's boundaries.
		/// </summary>
		public static void ThrowIfOutOfBounds(this ReadOnlyImage image, int x, int y, int z, Image other)
		{
			image.ThrowIfOutOfBounds(x, y, z, other.Width, other.Height, other.Depth);
		}

		/// <summary>
		/// Gets a copy function for rows of the specified image.
		/// The function converts from the image format to the specified output type.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="image"></param>
		/// <returns></returns>
		public static RowCopy<byte, T> GetRowColorCopy<T>(this MemoryImage image)
		{
			DataDescriptor srcDescriptor = new DataDescriptor(image.Format.GetElementType(), image.XDimension.GetDataDimension());
			DataDescriptor dstDescriptor = new DataDescriptor(typeof(T), new DataDimension(DataDimensionFlags.None, Unsafe.SizeOf<T>()));
			RowCopy<byte, T> copy = srcDescriptor.CompileBlockCopy<RowCopy<byte, T>>(dstDescriptor, PixelHelper.ColorIdentityMapping);
			return copy;
		}

		#endregion
	}
}
