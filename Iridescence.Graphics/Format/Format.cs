﻿using System;
using System.Text;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Describes a buffer format.
	/// </summary>
	[Serializable]
	public partial struct Format
	{
		#region Fields

		internal ulong Value;

		public static readonly Format None = new Format();
		internal const int DepthStencilTypeBit = 0x8000;
				
		#endregion

		#region Properties

		/// <summary>
		/// Gets the format's channels.
		/// </summary>
		public FormatType Type => (FormatType)(this.Value & 0xFFFF);

		/// <summary>
		/// Gets the semantic of the format.
		/// </summary>
		public FormatSemantic Semantic => (FormatSemantic)((this.Value >> 16) & 0xFF);

		/// <summary>
		/// Gets the input channel mappings.
		/// These are used when writing to data stored in this format.
		/// </summary>
		public ColorMapping Input => new ColorMapping(
			(ColorMappingChannel)((this.Value >> 32) & 0x0F),
			(ColorMappingChannel)((this.Value >> 36) & 0x0F),
			(ColorMappingChannel)((this.Value >> 40) & 0x0F),
			(ColorMappingChannel)((this.Value >> 44) & 0x0F));

		/// <summary>
		/// Gets the output channel mappings.
		/// These are used when reading from data in this format.
		/// </summary>
		public ColorMapping Output => new ColorMapping(
			(ColorMappingChannel)((this.Value >> 48) & 0x0F),
			(ColorMappingChannel)((this.Value >> 52) & 0x0F),
			(ColorMappingChannel)((this.Value >> 56) & 0x0F),
			(ColorMappingChannel)((this.Value >> 60) & 0x0F));

		/// <summary>
		/// Gets the flags of the format.
		/// </summary>
		public FormatFlags Flags => (FormatFlags)((this.Value >> 24) & 0xFF);
		
		/// <summary>
		/// Gets the size of one element.
		/// </summary>
		public int Size => GetSize(this.Type);

		/// <summary>
		/// Gets the number of components in one element.
		/// </summary>
		public int Components => GetComponents(this.Type);

		/// <summary>
		/// Gets a value that indicates whether the values are normalized (i.e. <see cref="Semantic"/> is <see cref="FormatSemantic.UNorm"/> or <see cref="FormatSemantic.SNorm"/>).
		/// </summary>
		public bool IsNormalized
		{
			get
			{
				FormatSemantic semantic = this.Semantic;
				return semantic == FormatSemantic.UNorm || semantic == FormatSemantic.SNorm;
			}
		}

		/// <summary>
		/// Gets a value that indicates whether this format is a depth-stencil format.
		/// </summary>
		/// <remarks>
		/// Depth stencil formats are treated a bit different: Swizzle masks have no effect on them and
		/// the stencil component always has UInt semantic, whereas the depth component semantic is
		/// defined by the Semantic property.
		/// </remarks>
		public bool IsDepthStencil => ((int)this.Type & DepthStencilTypeBit) == DepthStencilTypeBit;

		/// <summary>
		/// Gets a value that indicates whether the bit order of this format is reversed.
		/// </summary>
		public bool IsReversed => this.Flags.HasFlag(FormatFlags.Reversed);
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="Format"/>.
		/// </summary>
		public Format(FormatType type, FormatSemantic semantic, ColorMapping input, ColorMapping output, FormatFlags flags)
		{
			this.Value = ((ulong)type & 0xFFFF) |
			             (((ulong)semantic & 0xFF) << 16) |
			             (((ulong)flags & 0xFF) << 24) |
			             (((ulong)input.Red & 0x0F) << 32) |
			             (((ulong)input.Green & 0x0F) << 36) |
			             (((ulong)input.Blue & 0x0F) << 40) |
			             (((ulong)input.Alpha & 0x0F) << 44) |
			             (((ulong)output.Red & 0x0F) << 48) |
			             (((ulong)output.Green & 0x0F) << 52) |
			             (((ulong)output.Blue & 0x0F) << 56) |
			             (((ulong)output.Alpha & 0x0F) << 60);

			Exception ex = this.GetValidationException();
			if (ex != null)
				throw ex;
		}

		/// <summary>
		/// Creates a new <see cref="Format"/>.
		/// </summary>
		public Format(FormatType type, FormatSemantic semantic, ColorMapping input, ColorMapping output)
			: this(type, semantic, input, output, FormatFlags.None)
		{

		}

		/// <summary>
		/// Creates a new <see cref="Format"/>. 
		/// </summary>
		public Format(FormatType type, FormatSemantic semantic, FormatFlags flags)
			: this(type, semantic, ColorMapping.RGBA, ColorMapping.RGBA, flags)
		{

		}

		/// <summary>
		/// Creates a new <see cref="Format"/>. 
		/// </summary>
		public Format(FormatType type, FormatSemantic semantic)
			: this(type, semantic, ColorMapping.RGBA, ColorMapping.RGBA)
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Validates the format and returns an exception if validation failed.
		/// </summary>
		/// <returns></returns>
		internal Exception GetValidationException()
		{
			if (!Enum.IsDefined(typeof(FormatType), this.Type))
				return new InvalidFormatException("Invalid channels.");

			if (!Enum.IsDefined(typeof(FormatSemantic), this.Semantic))
				return new InvalidFormatException("Invalid semantic.");

			if (!IsValidCombination(this.Type, this.Semantic))
				return new InvalidFormatException($"Semantic \"{this.Semantic}\" can't be used in combination with \"{this.Type}\".");

			Exception ex = this.Output.GetOutputValidationException();
			if(ex != null)
				return ex;

			return this.Input.GetInputValidationException();
		}

		/// <summary>
		/// Returns a binary representation of this <see cref="Format"/>.
		/// </summary>
		/// <returns></returns>
		public ulong ToBinary()
		{
			return this.Value;
		}

		/// <summary>
		/// Returns a <see cref="Format" /> from a binary representation previously retrieved using <see cref="ToBinary"/>.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static Format FromBinary(ulong value)
		{
			Format f;
			f.Value = value;

			Exception ex = f.GetValidationException();
			if (ex != null)
				throw ex;

			return f;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is Format))
				return false;

			Format f = (Format)obj;
			return f.Value == this.Value;
		}

		public override int GetHashCode()
		{
			return this.Value.GetHashCode();
		}

		public Type GetElementType()
		{
			if(this.Semantic == FormatSemantic.None)
				return null;

			switch (this.Type)
			{
				case FormatType.None:
					return null;
				case FormatType.R8:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm: return ColorUtility.GetColorType(1, typeof(UNorm8), this.Input, this.Output);
						case FormatSemantic.SNorm: return ColorUtility.GetColorType(1, typeof(SNorm8), this.Input, this.Output);
						case FormatSemantic.UInt: return ColorUtility.GetColorType(1, typeof(byte), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(1, typeof(sbyte), this.Input, this.Output);
					}
					break;
				case FormatType.R8G8:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm: return ColorUtility.GetColorType(2, typeof(UNorm8), this.Input, this.Output);
						case FormatSemantic.SNorm: return ColorUtility.GetColorType(2, typeof(SNorm8), this.Input, this.Output);
						case FormatSemantic.UInt: return ColorUtility.GetColorType(2, typeof(byte), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(2, typeof(sbyte), this.Input, this.Output);
					}
					break;
				case FormatType.R8G8B8:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm: return ColorUtility.GetColorType(3, typeof(UNorm8), this.Input, this.Output);
						case FormatSemantic.SNorm: return ColorUtility.GetColorType(3, typeof(SNorm8), this.Input, this.Output);
						case FormatSemantic.UInt: return ColorUtility.GetColorType(3, typeof(byte), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(3, typeof(sbyte), this.Input, this.Output);
					}
					break;
				case FormatType.R8G8B8A8:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm: return ColorUtility.GetColorType(4, typeof(UNorm8), this.Input, this.Output);
						case FormatSemantic.SNorm: return ColorUtility.GetColorType(4, typeof(SNorm8), this.Input, this.Output);
						case FormatSemantic.UInt: return ColorUtility.GetColorType(4, typeof(byte), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(4, typeof(sbyte), this.Input, this.Output);
					}
					break;
				case FormatType.R16:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm: return ColorUtility.GetColorType(1, typeof(UNorm16), this.Input, this.Output);
						case FormatSemantic.SNorm: return ColorUtility.GetColorType(1, typeof(SNorm16), this.Input, this.Output);
						case FormatSemantic.UInt: return ColorUtility.GetColorType(1, typeof(ushort), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(1, typeof(short), this.Input, this.Output);
						case FormatSemantic.Float: return ColorUtility.GetColorType(1, typeof(Half), this.Input, this.Output);
					}
					break;
				case FormatType.R16G16:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm: return ColorUtility.GetColorType(2, typeof(UNorm16), this.Input, this.Output);
						case FormatSemantic.SNorm: return ColorUtility.GetColorType(2, typeof(SNorm16), this.Input, this.Output);
						case FormatSemantic.UInt: return ColorUtility.GetColorType(2, typeof(ushort), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(2, typeof(short), this.Input, this.Output);
						case FormatSemantic.Float: return ColorUtility.GetColorType(2, typeof(Half), this.Input, this.Output);
					}
					break;
				case FormatType.R16G16B16:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm: return ColorUtility.GetColorType(3, typeof(UNorm16), this.Input, this.Output);
						case FormatSemantic.SNorm: return ColorUtility.GetColorType(3, typeof(SNorm16), this.Input, this.Output);
						case FormatSemantic.UInt: return ColorUtility.GetColorType(3, typeof(ushort), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(3, typeof(short), this.Input, this.Output);
						case FormatSemantic.Float: return ColorUtility.GetColorType(3, typeof(Half), this.Input, this.Output);
					}
					break;
				case FormatType.R16G16B16A16:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm: return ColorUtility.GetColorType(4, typeof(UNorm16), this.Input, this.Output);
						case FormatSemantic.SNorm: return ColorUtility.GetColorType(4, typeof(SNorm16), this.Input, this.Output);
						case FormatSemantic.UInt: return ColorUtility.GetColorType(4, typeof(ushort), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(4, typeof(short), this.Input, this.Output);
						case FormatSemantic.Float: return ColorUtility.GetColorType(4, typeof(Half), this.Input, this.Output);
					}
					break;
				case FormatType.R32:
					switch (this.Semantic)
					{
						case FormatSemantic.UInt: return ColorUtility.GetColorType(1, typeof(uint), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(1, typeof(int), this.Input, this.Output);
						case FormatSemantic.Float: return ColorUtility.GetColorType(1, typeof(float), this.Input, this.Output);
					}
					break;
				case FormatType.R32G32:
					switch (this.Semantic)
					{
						case FormatSemantic.UInt: return ColorUtility.GetColorType(2, typeof(uint), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(2, typeof(int), this.Input, this.Output);
						case FormatSemantic.Float: return ColorUtility.GetColorType(2, typeof(float), this.Input, this.Output);
					}
					break;
				case FormatType.R32G32B32:
					switch (this.Semantic)
					{
						case FormatSemantic.UInt: return ColorUtility.GetColorType(3, typeof(uint), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(3, typeof(int), this.Input, this.Output);
						case FormatSemantic.Float: return ColorUtility.GetColorType(3, typeof(float), this.Input, this.Output);
					}
					break;
				case FormatType.R32G32B32A32:
					switch (this.Semantic)
					{
						case FormatSemantic.UInt: return ColorUtility.GetColorType(4, typeof(uint), this.Input, this.Output);
						case FormatSemantic.SInt: return ColorUtility.GetColorType(4, typeof(int), this.Input, this.Output);
						case FormatSemantic.Float: return ColorUtility.GetColorType(4, typeof(float), this.Input, this.Output);
					}
					break;
				case FormatType.R3G3B2:
					switch (this.Semantic)
					{
						case FormatSemantic.UInt: return ColorUtility.ApplySwizzle(3, typeof(byte), typeof(R3G3B2_UInt8), this.Input, this.Output);
						case FormatSemantic.UNorm: return ColorUtility.ApplySwizzle(3, typeof(UNorm8), typeof(R3G3B2_UNorm8), this.Input, this.Output);
					}
					break;
				case FormatType.R5G6B5:
					switch (this.Semantic)
					{
						case FormatSemantic.UInt: return ColorUtility.ApplySwizzle(3, typeof(byte), typeof(R5G6B5_UInt8), this.Input, this.Output);
						case FormatSemantic.UNorm: return ColorUtility.ApplySwizzle(3, typeof(UNorm8), typeof(R5G6B5_UNorm8), this.Input, this.Output);
					}
					break;
				case FormatType.R5G5B5A1:
					switch (this.Semantic)
					{
						case FormatSemantic.UInt: return ColorUtility.ApplySwizzle(4, typeof(byte), typeof(R5G5B5A1_UInt8), this.Input, this.Output);
						case FormatSemantic.UNorm: return ColorUtility.ApplySwizzle(4, typeof(UNorm8), typeof(R5G5B5A1_UNorm8), this.Input, this.Output);
					}
					break;
				case FormatType.R4G4B4A4:
					switch (this.Semantic)
					{
						case FormatSemantic.UInt: return ColorUtility.ApplySwizzle(4, typeof(byte), typeof(R4G4B4A4_UInt8), this.Input, this.Output);
						case FormatSemantic.UNorm: return ColorUtility.ApplySwizzle(4, typeof(UNorm8), typeof(R4G4B4A4_UNorm8), this.Input, this.Output);
					}
					break;
				case FormatType.R10G10B10A2:
				case FormatType.R11G11B10:
				case FormatType.R9G9B9E5:
					throw new NotImplementedException();
				case FormatType.D16:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm:
							return typeof(Depth16);
					}
					break;
				case FormatType.D24:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm:
							return typeof(Depth24);
					}
					break;
				case FormatType.D32:
					switch (this.Semantic)
					{
						case FormatSemantic.Float:
							return typeof(Depth32F);
					}
					break;
				case FormatType.D24S8:
					switch (this.Semantic)
					{
						case FormatSemantic.UNorm:
							return typeof(Depth24Stencil8);
					}
					break;
				case FormatType.D32S8:
					switch (this.Semantic)
					{
						case FormatSemantic.Float:
							return typeof(Depth32FStencil8);
					}
					break;
				case FormatType.S8:
					switch (this.Semantic)
					{
						case FormatSemantic.UInt:
							return typeof(Stencil8);
					}
					break;
			}

			throw new InvalidFormatException();
		}

		/// <summary>
		/// Returns a value that indicates whether the specified format channels and semantic form a valid combination.
		/// </summary>
		/// <returns></returns>
		public static bool IsValidCombination(FormatType type, FormatSemantic semantic)
		{
			if (!Enum.IsDefined(typeof(FormatType), type))
				return false;

			switch (semantic)
			{
				case FormatSemantic.None:
					return true;

				case FormatSemantic.UNorm:
					return type == FormatType.R8 ||
					       type == FormatType.R8G8 ||
					       type == FormatType.R8G8B8 ||
					       type == FormatType.R8G8B8A8 ||
					       type == FormatType.R16 ||
					       type == FormatType.R16G16 ||
					       type == FormatType.R16G16B16 ||
					       type == FormatType.R16G16B16A16 ||
					       type == FormatType.R3G3B2 ||
					       type == FormatType.R5G6B5 ||
					       type == FormatType.R5G5B5A1 ||
					       type == FormatType.R4G4B4A4 ||
					       type == FormatType.R10G10B10A2 ||
					       type == FormatType.D16 ||
					       type == FormatType.D24 ||
					       type == FormatType.D24S8;

				case FormatSemantic.SNorm:
					return type == FormatType.R8 ||
						   type == FormatType.R8G8 ||
						   type == FormatType.R8G8B8 ||
						   type == FormatType.R8G8B8A8 ||
						   type == FormatType.R16 ||
						   type == FormatType.R16G16 ||
						   type == FormatType.R16G16B16 ||
						   type == FormatType.R16G16B16A16;

				case FormatSemantic.UInt:
					return type == FormatType.R8 ||
						   type == FormatType.R8G8 ||
						   type == FormatType.R8G8B8 ||
						   type == FormatType.R8G8B8A8 ||
						   type == FormatType.R16 ||
						   type == FormatType.R16G16 ||
						   type == FormatType.R16G16B16 ||
						   type == FormatType.R16G16B16A16 ||
						   type == FormatType.R32 ||
						   type == FormatType.R32G32 ||
						   type == FormatType.R32G32B32 ||
						   type == FormatType.R32G32B32A32 ||
						   type == FormatType.R10G10B10A2 ||
						   type == FormatType.S8;

				case FormatSemantic.SInt:
					return type == FormatType.R8 ||
						   type == FormatType.R8G8 ||
						   type == FormatType.R8G8B8 ||
						   type == FormatType.R8G8B8A8 ||
						   type == FormatType.R16 ||
						   type == FormatType.R16G16 ||
						   type == FormatType.R16G16B16 ||
						   type == FormatType.R16G16B16A16 ||
						   type == FormatType.R32 ||
						   type == FormatType.R32G32 ||
						   type == FormatType.R32G32B32 ||
						   type == FormatType.R32G32B32A32;

				case FormatSemantic.Float:
					return type == FormatType.R16 ||
						   type == FormatType.R16G16 ||
						   type == FormatType.R16G16B16 ||
						   type == FormatType.R16G16B16A16 ||
						   type == FormatType.R32 ||
						   type == FormatType.R32G32 ||
						   type == FormatType.R32G32B32 ||
						   type == FormatType.R32G32B32A32 ||
						   type == FormatType.R11G11B10 ||
						   type == FormatType.R9G9B9E5 ||
						   type == FormatType.D32 ||
						   type == FormatType.D32S8;
			}

			return false;
		}

		/// <summary>
		/// Gets the number of bytes one element with this format type takes up.
		/// </summary>
		/// <returns></returns>
		public static int GetSize(FormatType type)
		{
			switch (type)
			{
				case FormatType.None: return 0;

				case FormatType.R8: return 1;
				case FormatType.R8G8: return 2;
				case FormatType.R8G8B8: return 3;
				case FormatType.R8G8B8A8: return 4;

				case FormatType.R16: return 2;
				case FormatType.R16G16: return 4;
				case FormatType.R16G16B16: return 6;
				case FormatType.R16G16B16A16: return 8;

				case FormatType.R32: return 4;
				case FormatType.R32G32: return 8;
				case FormatType.R32G32B32: return 12;
				case FormatType.R32G32B32A32: return 16;

				case FormatType.R3G3B2: return 1;
				case FormatType.R5G6B5: return 2;
				case FormatType.R5G5B5A1: return 2;
				case FormatType.R4G4B4A4: return 2;
				case FormatType.R11G11B10: return 4;
				case FormatType.R10G10B10A2: return 4;
				case FormatType.R9G9B9E5: return 4;

				case FormatType.D16: return 2;
				case FormatType.D24: return 3;
				case FormatType.D32: return 4;
				case FormatType.D24S8: return 4;
				case FormatType.D32S8: return 8;
				case FormatType.S8: return 1;
			}

			throw new ArgumentException($"Size of {type} can not be retreived because it is not a byte-aligned format.");
		}

		/// <summary>
		/// Returns the number of components in the specified format type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static int GetComponents(FormatType type)
		{
			switch (type)
			{
				case FormatType.None: return 0;

				case FormatType.R8: return 1;
				case FormatType.R8G8: return 2;
				case FormatType.R8G8B8: return 3;
				case FormatType.R8G8B8A8: return 4;

				case FormatType.R16: return 1;
				case FormatType.R16G16: return 2;
				case FormatType.R16G16B16: return 3;
				case FormatType.R16G16B16A16: return 4;

				case FormatType.R32: return 1;
				case FormatType.R32G32: return 2;
				case FormatType.R32G32B32: return 3;
				case FormatType.R32G32B32A32: return 4;

				case FormatType.R3G3B2: return 3;
				case FormatType.R5G6B5: return 3;
				case FormatType.R5G5B5A1: return 4;
				case FormatType.R4G4B4A4: return 4;
				case FormatType.R11G11B10: return 3;
				case FormatType.R10G10B10A2: return 4;
				case FormatType.R9G9B9E5: return 3;

				case FormatType.D16: return 1;
				case FormatType.D24: return 1;
				case FormatType.D32: return 1;
				case FormatType.D24S8: return 2;
				case FormatType.D32S8: return 2;
				case FormatType.S8: return 1;
			}

			throw new ArgumentException("Invalid format type.");
		}

		/// <summary>
		/// Gets the size of a 3D box stored with the specified format type.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="depth"></param>
		/// <returns></returns>
		public static int GetSize(FormatType type, int width, int height, int depth)
		{
			return width * height * depth * GetSize(type);
		}

		public static bool operator ==(Format a, Format b)
		{
			return a.Value == b.Value;
		}

		public static bool operator !=(Format a, Format b)
		{
			return a.Value != b.Value;
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			str.Append(this.Type);
			str.Append('-');
			str.Append(this.Semantic);

			if (!this.Input.IsIdentity)
			{
				str.Append(", In(");
				str.Append(this.Input);
				str.Append(")");
			}
			
			if (!this.Output.IsIdentity)
			{
				str.Append(", Out(");
				str.Append(this.Output);
				str.Append(")");
			}

			if (this.Flags != FormatFlags.None)
			{
				str.Append(", ");
				str.Append(this.Flags);
			}
			
			return str.ToString();
		}

		#endregion
	}
}
