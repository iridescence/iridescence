﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines the mapping of a format channel.
	/// </summary>
	public enum ColorMappingChannel
	{
		/// <summary>
		/// The channel is discarded. Only valid for input mappings.
		/// </summary>
		None,
		
		/// <summary>
		/// The channel produces a constant zero. Only valid for output mappings.
		/// </summary>
		Zero,

		/// <summary>
		/// The channel produces a constant one. Only valid for output mappings.
		/// </summary>
		One,

		/// <summary>
		/// The channel is mapped to the red channel.
		/// </summary>
		Red,

		/// <summary>
		/// The channel is mapped to the green channel.
		/// </summary>
		Green,

		/// <summary>
		/// The channel is mapped to the blue channel.
		/// </summary>
		Blue,

		/// <summary>
		/// The channel is mapped to the alpha channel.
		/// </summary>
		Alpha
	}
}
