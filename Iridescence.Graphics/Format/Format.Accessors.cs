﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Iridescence.Memory;

namespace Iridescence.Graphics
{
	public partial struct Format
	{
		#region Methods

		private static T map<T>(ColorMappingChannel swizzle, T zero, T one, T r, T g, T b, T a)
		{
			switch (swizzle)
			{
				case ColorMappingChannel.Zero:
					return zero;
				case ColorMappingChannel.One:
					return one;
				case ColorMappingChannel.Red:
					return r;
				case ColorMappingChannel.Green:
					return g;
				case ColorMappingChannel.Blue:
					return b;
				case ColorMappingChannel.Alpha:
					return a;
			}

			throw new ArgumentException("Invalid format channel mapping.");
		}

		private static Delegate constantWriter<TValue>(Delegate writer, TValue constant)
		{
			if (writer == null)
				return null;
			Type delegateType = writer.GetType();
			MethodInfo invokeMethod = delegateType.GetMethod("Invoke");
			ParameterInfo[] parameters = invokeMethod.GetParameters();
			
			ParameterExpression[] parameterExpressions = new ParameterExpression[parameters.Length];
			Expression[] callParameters = new Expression[parameters.Length];
			for (int i = 0; i < parameters.Length; ++i)
			{
				parameterExpressions[i] = Expression.Parameter(parameters[i].ParameterType, parameters[i].Name);
				callParameters[i] = parameterExpressions[i];
			}

			callParameters[callParameters.Length - 1] = Expression.Constant(constant);

			return Expression.Lambda(delegateType, Expression.Invoke(Expression.Constant(writer), callParameters), "<ConstantWriter>", parameterExpressions).Compile();
		}

		private static T emptyWriter<T>()
		{
			Type delegateType = typeof(T);
			MethodInfo invokeMethod = delegateType.GetMethod("Invoke");
			ParameterInfo[] parameters = invokeMethod.GetParameters();
			ParameterExpression[] parameterExpressions = new ParameterExpression[parameters.Length];
			for (int i = 0; i < parameters.Length; ++i)
			{
				parameterExpressions[i] = Expression.Parameter(parameters[i].ParameterType, parameters[i].Name);
			}
			return Expression.Lambda<T>(Expression.Empty(), "<EmptyWriter>", parameterExpressions).Compile();
		}

		private T combineWriter<T, TValue>(ColorMappingChannel channel, TValue zero, TValue one, T r, T g, T b, T a)
		{
			return (T)(object)this.combineWriter<TValue>(channel, zero, one, (Delegate)(object)r, (Delegate)(object)g, (Delegate)(object)b, (Delegate)(object)a);
		}

		private Delegate combineWriter<TValue>(ColorMappingChannel channel, TValue zero, TValue one, Delegate r, Delegate g, Delegate b, Delegate a)
		{
			Delegate result = null;

			if (this.Input.Red == channel)
			{
				result = Delegate.Combine(result, r);
			}
			else if (this.Input.Red == ColorMappingChannel.Zero && channel == ColorMappingChannel.Red)
			{
				result = Delegate.Combine(result, constantWriter(r, zero));
			}
			else if (this.Input.Red == ColorMappingChannel.One && channel == ColorMappingChannel.Red)
			{
				result = Delegate.Combine(result, constantWriter(r, one));
			}

			if (this.Input.Green == channel)
			{
				result = Delegate.Combine(result, g);
			}
			else if (this.Input.Green == ColorMappingChannel.Zero && channel == ColorMappingChannel.Green)
			{
				result = Delegate.Combine(result, constantWriter(g, zero));
			}
			else if (this.Input.Green == ColorMappingChannel.One && channel == ColorMappingChannel.Green)
			{
				result = Delegate.Combine(result, constantWriter(g, one));
			}
			
			if (this.Input.Blue == channel)
			{
				result = Delegate.Combine(result, b);
			}
			else if (this.Input.Blue == ColorMappingChannel.Zero && channel == ColorMappingChannel.Blue)
			{
				result = Delegate.Combine(result, constantWriter(b, zero));
			}
			else if (this.Input.Blue == ColorMappingChannel.One && channel == ColorMappingChannel.Blue)
			{
				result = Delegate.Combine(result, constantWriter(b, one));
			}
			
			if (this.Input.Alpha == channel)
			{
				result = Delegate.Combine(result, a);
			}
			else if (this.Input.Alpha == ColorMappingChannel.Zero && channel == ColorMappingChannel.Alpha)
			{
				result = Delegate.Combine(result, constantWriter(a, zero));
			}
			else if (this.Input.Alpha == ColorMappingChannel.One && channel == ColorMappingChannel.Alpha)
			{
				result = Delegate.Combine(result, constantWriter(a, one));
			}

			return result;
		}

		/// <summary>
		/// Gets the <see cref="MemoryDataDescriptor">data descriptors</see> for regular formats (i.e. Rn[Gn[Bn[An]]]).
		/// </summary>
		/// <param name="baseAddress"></param>
		/// <param name="dimensions"></param>
		/// <param name="r"></param>
		/// <param name="g"></param>
		/// <param name="b"></param>
		/// <param name="a"></param>
		private void getFields(
			out (int, Type) r,
			out (int, Type) g,
			out (int, Type) b,
			out (int, Type) a)
		{
			r = g = b = a = default;

			switch (this.Type)
			{
				case FormatType.R8:
					if (this.Semantic == FormatSemantic.UNorm)
					{
						r = (0, typeof(UNorm8));
					}
					else if (this.Semantic == FormatSemantic.SNorm)
					{
						r = (0, typeof(SNorm8));
					}
					else if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(byte));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(sbyte));
					}

					break;
				case FormatType.R8G8:
					if (this.Semantic == FormatSemantic.UNorm)
					{
						r = (0, typeof(UNorm8));
						g = (1, typeof(UNorm8));
					}
					else if (this.Semantic == FormatSemantic.SNorm)
					{
						r = (0, typeof(SNorm8));
						g = (1, typeof(SNorm8));
					}
					else if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(byte));
						g = (1, typeof(byte));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(sbyte));
						g = (1, typeof(sbyte));
					}

					break;
				case FormatType.R8G8B8:
					if (this.Semantic == FormatSemantic.UNorm)
					{
						r = (0, typeof(UNorm8));
						g = (1, typeof(UNorm8));
						b = (2, typeof(UNorm8));
					}
					else if (this.Semantic == FormatSemantic.SNorm)
					{
						r = (0, typeof(SNorm8));
						g = (1, typeof(SNorm8));
						b = (2, typeof(SNorm8));
					}
					else if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(byte));
						g = (1, typeof(byte));
						b = (2, typeof(byte));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(sbyte));
						g = (1, typeof(sbyte));
						b = (2, typeof(sbyte));
					}

					break;
				case FormatType.R8G8B8A8:
					if (this.Semantic == FormatSemantic.UNorm)
					{
						r = (0, typeof(UNorm8));
						g = (1, typeof(UNorm8));
						b = (2, typeof(UNorm8));
						a = (3, typeof(UNorm8));
					}
					else if (this.Semantic == FormatSemantic.SNorm)
					{
						r = (0, typeof(SNorm8));
						g = (1, typeof(SNorm8));
						b = (+2, typeof(SNorm8));
						a = (3, typeof(UNorm8));
					}
					else if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(byte));
						g = (1, typeof(byte));
						b = (2, typeof(byte));
						a = (3, typeof(byte));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(sbyte));
						g = (1, typeof(sbyte));
						b = (2, typeof(sbyte));
						a = (3, typeof(sbyte));
					}

					break;
				case FormatType.R16:
					if (this.Semantic == FormatSemantic.UNorm)
					{
						r = (0, typeof(UNorm16));
					}
					else if (this.Semantic == FormatSemantic.SNorm)
					{
						r = (0, typeof(SNorm16));
					}
					else if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(ushort));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(short));
					}
					else if (this.Semantic == FormatSemantic.Float)
					{
						r = (0, typeof(Half));
					}

					break;
				case FormatType.R16G16:
					if (this.Semantic == FormatSemantic.UNorm)
					{
						r = (0, typeof(UNorm16));
						g = (2, typeof(UNorm16));
					}
					else if (this.Semantic == FormatSemantic.SNorm)
					{
						r = (0, typeof(SNorm16));
						g = (2, typeof(SNorm16));
					}
					else if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(ushort));
						g = (2, typeof(ushort));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(short));
						g = (2, typeof(short));
					}
					else if (this.Semantic == FormatSemantic.Float)
					{
						r = (0, typeof(Half));
						g = (2, typeof(Half));
					}

					break;
				case FormatType.R16G16B16:
					if (this.Semantic == FormatSemantic.UNorm)
					{
						r = (0, typeof(UNorm16));
						g = (2, typeof(UNorm16));
						b = (4, typeof(UNorm16));
					}
					else if (this.Semantic == FormatSemantic.SNorm)
					{
						r = (0, typeof(SNorm16));
						g = (2, typeof(SNorm16));
						b = (4, typeof(SNorm16));
					}
					else if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(ushort));
						g = (2, typeof(ushort));
						b = (4, typeof(ushort));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(short));
						g = (2, typeof(short));
						b = (4, typeof(short));
					}
					else if (this.Semantic == FormatSemantic.Float)
					{
						r = (0, typeof(Half));
						g = (2, typeof(Half));
						b = (4, typeof(Half));
					}

					break;
				case FormatType.R16G16B16A16:
					if (this.Semantic == FormatSemantic.UNorm)
					{
						r = (0, typeof(UNorm16));
						g = (2, typeof(UNorm16));
						b = (4, typeof(UNorm16));
						a = (6, typeof(UNorm16));
					}
					else if (this.Semantic == FormatSemantic.SNorm)
					{
						r = (0, typeof(SNorm16));
						g = (2, typeof(SNorm16));
						b = (4, typeof(SNorm16));
						a = (6, typeof(UNorm16));
					}
					else if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(ushort));
						g = (2, typeof(ushort));
						b = (4, typeof(ushort));
						a = (6, typeof(ushort));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(short));
						g = (2, typeof(short));
						b = (4, typeof(short));
						a = (6, typeof(short));
					}
					else if (this.Semantic == FormatSemantic.Float)
					{
						r = (0, typeof(Half));
						g = (2, typeof(Half));
						b = (4, typeof(Half));
						a = (6, typeof(Half));
					}

					break;
				case FormatType.R32:
					if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(uint));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(int));
					}
					else if (this.Semantic == FormatSemantic.Float)
					{
						r = (0, typeof(float));
					}

					break;
				case FormatType.R32G32:
					if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(uint));
						g = (4, typeof(uint));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(int));
						g = (4, typeof(int));
					}
					else if (this.Semantic == FormatSemantic.Float)
					{
						r = (0, typeof(float));
						g = (4, typeof(float));
					}

					break;
				case FormatType.R32G32B32:
					if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(uint));
						g = (4, typeof(uint));
						b = (8, typeof(uint));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(int));
						g = (4, typeof(int));
						b = (8, typeof(int));
					}
					else if (this.Semantic == FormatSemantic.Float)
					{
						r = (0, typeof(float));
						g = (4, typeof(float));
						b = (8, typeof(float));
					}

					break;
				case FormatType.R32G32B32A32:
					if (this.Semantic == FormatSemantic.UInt)
					{
						r = (0, typeof(uint));
						g = (4, typeof(uint));
						b = (8, typeof(uint));
						a = (12, typeof(uint));
					}
					else if (this.Semantic == FormatSemantic.SInt)
					{
						r = (0, typeof(int));
						g = (4, typeof(int));
						b = (8, typeof(int));
						a = (12, typeof(int));
					}
					else if (this.Semantic == FormatSemantic.Float)
					{
						r = (0, typeof(float));
						g = (4, typeof(float));
						b = (8, typeof(float));
						a = (12, typeof(float));
					}

					break;
				default:
					throw new ArgumentException($"{this} is not a regular format");
			}
		}

		private static Expression createMaskAndShiftReader(Delegate elementAccessor, Type elementType, ParameterExpression[] parameters, int mask, int shift)
		{
			return
				Expression.LeftShift(
					Expression.And(
						Expression.Invoke(Expression.Constant(elementAccessor), parameters),
						Expression.Constant(mask, elementType)),
					Expression.Constant(shift));
		}

		#region Base

		/// <summary>
		/// Creates memory read accessors for the four color channels in the format.
		/// </summary>
		/// <typeparam name="T">The delegate type.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="dimensions">The specifications of the dimensions.</param>
		/// <param name="zero">The constant zero value accessor.</param>
		/// <param name="one">The constant one value accessor.</param>
		/// <param name="r">The reader for the red channel.</param>
		/// <param name="g">The reader for the green channel.</param>
		/// <param name="b">The reader for the blue channel.</param>
		/// <param name="a">The reader for the alpha channel.</param>
		public void CreateReaders<T>(IntPtr baseAddress, DataDimension[] dimensions, T zero, T one, out T r, out T g, out T b, out T a) where T : class
		{
			T readerR = null;
			T readerG = null;
			T readerB = null;
			T readerA = null;
			
			switch (this.Type)
			{
				case FormatType.None:
					break;
					
				case FormatType.R8:
				case FormatType.R8G8:
				case FormatType.R8G8B8:
				case FormatType.R8G8B8A8:
				case FormatType.R16:
				case FormatType.R16G16:
				case FormatType.R16G16B16:
				case FormatType.R16G16B16A16:
				case FormatType.R32:
				case FormatType.R32G32:
				case FormatType.R32G32B32:
				case FormatType.R32G32B32A32:
					this.getFields(out (int offset, Type t) descriptorR, out (int offset, Type t) descriptorG, out (int offset, Type t) descriptorB, out (int offset, Type t) descriptorA);
					readerR = descriptorR.t != null ? new DataDescriptor(descriptorR.t, dimensions).CompileReader<T>(baseAddress + descriptorR.offset) : null;
					readerG = descriptorG.t != null ? new DataDescriptor(descriptorG.t, dimensions).CompileReader<T>(baseAddress + descriptorG.offset) : null;
					readerB = descriptorB.t != null ? new DataDescriptor(descriptorB.t, dimensions).CompileReader<T>(baseAddress + descriptorB.offset) : null;
					readerA = descriptorA.t != null ? new DataDescriptor(descriptorA.t, dimensions).CompileReader<T>(baseAddress + descriptorA.offset) : null;
					break;
					
				case FormatType.R3G3B2:
					throw new NotImplementedException($"{this.Type} is not implemented yet.");
				case FormatType.R5G6B5:
				case FormatType.R5G5B5A1:
					// Get return type and indexer types for the actual accessors.
					MethodInfo invokeMethod = typeof(T).GetMethod("Invoke");
					Type[] indexerTypes = invokeMethod.GetParameters().Select(param => param.ParameterType).ToArray();
					Type returnType = invokeMethod.ReturnType;
					
					// Create an element accessor for the 16-bit source.
					Type elementAccessorType = ReflectionUtility.GetFuncType(typeof(ushort), indexerTypes);
					DataDescriptor elementDescriptor = new DataDescriptor(typeof(ushort), dimensions);
					Delegate elementAccessor = elementDescriptor.CompileReader(elementAccessorType, baseAddress);

					// Create parameters.
					ParameterExpression[] parameters = new ParameterExpression[indexerTypes.Length];
					for (int i = 0; i < parameters.Length; ++i)
					{
						parameters[i] = Expression.Parameter(indexerTypes[i], $"i{i}");
					}

					// Create expressions to access each component.
					Expression expressionR, expressionG, expressionB, expressionA = null;
					if (this.Type == FormatType.R5G5B5A1)
					{
						expressionR = createMaskAndShiftReader(elementAccessor, typeof(ushort), parameters, 0xF800, 11);
						expressionG = createMaskAndShiftReader(elementAccessor, typeof(ushort), parameters, 0x07C0, 6);
						expressionB = createMaskAndShiftReader(elementAccessor, typeof(ushort), parameters, 0x003E, 1);
						expressionA = createMaskAndShiftReader(elementAccessor, typeof(ushort), parameters, 0x0001, 0);

						if (this.IsNormalized)
						{
							expressionR = Expression.Multiply(expressionR, Expression.Constant(1.0f / 31.0f));
							expressionG = Expression.Multiply(expressionG, Expression.Constant(1.0f / 31.0f));
							expressionB = Expression.Multiply(expressionB, Expression.Constant(1.0f / 31.0f));
						}
					}
					else
					{
						expressionR = createMaskAndShiftReader(elementAccessor, typeof(ushort), parameters, 0xF800, 11);
						expressionG = createMaskAndShiftReader(elementAccessor, typeof(ushort), parameters, 0x07E0, 5);
						expressionB = createMaskAndShiftReader(elementAccessor, typeof(ushort), parameters, 0x001F, 0);

						if (this.IsNormalized)
						{
							expressionR = Expression.Multiply(expressionR, Expression.Constant(1.0f / 31.0f));
							expressionG = Expression.Multiply(expressionG, Expression.Constant(1.0f / 63.0f));
							expressionB = Expression.Multiply(expressionB, Expression.Constant(1.0f / 31.0f));
						}
					}

					// Cast to desired return type.
					expressionR = Expression.Convert(expressionR, returnType);
					expressionG = Expression.Convert(expressionG, returnType);
					expressionB = Expression.Convert(expressionB, returnType);
					if(expressionA != null)
						expressionA = Expression.Convert(expressionA, returnType);

					// Compile.
					readerR = Expression.Lambda<T>(expressionR, "R", parameters).Compile();
					readerG = Expression.Lambda<T>(expressionG, "G", parameters).Compile();
					readerB = Expression.Lambda<T>(expressionB, "B", parameters).Compile();
					if(expressionA != null)
						readerA = Expression.Lambda<T>(expressionA, "A", parameters).Compile();

					break;
				case FormatType.R4G4B4A4:
				case FormatType.R10G10B10A2:
				case FormatType.R11G11B10:
				case FormatType.R9G9B9E5:
					throw new NotImplementedException($"{this.Type} is not implemented yet.");
				case FormatType.D16:
				case FormatType.D24:
				case FormatType.D32:
				case FormatType.D24S8:
				case FormatType.D32S8:
				case FormatType.S8:
					break;
				default:
					throw new InvalidEnumArgumentException("Invalid FormatType.");
			}
			
			readerR = readerR ?? zero;
			readerG = readerG ?? zero;
			readerB = readerB ?? zero;
			readerA = readerA ?? one;

			r = map(this.Output.Red, zero, one, readerR, readerG, readerB, readerA);
			g = map(this.Output.Green, zero, one, readerR, readerG, readerB, readerA);
			b = map(this.Output.Blue, zero, one, readerR, readerG, readerB, readerA);
			a = map(this.Output.Alpha, zero, one, readerR, readerG, readerB, readerA);
		}

		/// <summary>
		/// Creates memory write accessors for the four color channels in the format.
		/// </summary>
		/// <typeparam name="T">The delegate type.</typeparam>
		/// <typeparam name="TValue">The value type.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="dimensions">The specifications of the dimensions.</param>
		/// <param name="zero">The constant zero value accessor.</param>
		/// <param name="one">The constant one value accessor.</param>
		/// <param name="r">The writer for the red channel.</param>
		/// <param name="g">The writer for the green channel.</param>
		/// <param name="b">The writer for the blue channel.</param>
		/// <param name="a">The writer for the alpha channel.</param>
		public void CreateWriters<T, TValue>(IntPtr baseAddress, DataDimension[] dimensions, TValue zero, TValue one, out T r, out T g, out T b, out T a) where T : class
		{
			T writerR = null;
			T writerG = null;
			T writerB = null;
			T writerA = null;

			switch (this.Type)
			{
				case FormatType.None:
					break;

				case FormatType.R8:
				case FormatType.R8G8:
				case FormatType.R8G8B8:
				case FormatType.R8G8B8A8:
				case FormatType.R16:
				case FormatType.R16G16:
				case FormatType.R16G16B16:
				case FormatType.R16G16B16A16:
				case FormatType.R32:
				case FormatType.R32G32:
				case FormatType.R32G32B32:
				case FormatType.R32G32B32A32:
					this.getFields(out (int offset, Type t) descriptorR, out (int offset, Type t) descriptorG, out (int offset, Type t) descriptorB, out (int offset, Type t) descriptorA);
					writerR = descriptorR.t != null ? new DataDescriptor(descriptorR.t, dimensions).CompileWriter<T>(baseAddress + descriptorR.offset) : null;
					writerG = descriptorG.t != null ? new DataDescriptor(descriptorG.t, dimensions).CompileWriter<T>(baseAddress + descriptorG.offset) : null;
					writerB = descriptorB.t != null ? new DataDescriptor(descriptorB.t, dimensions).CompileWriter<T>(baseAddress + descriptorB.offset) : null;
					writerA = descriptorA.t != null ? new DataDescriptor(descriptorA.t, dimensions).CompileWriter<T>(baseAddress + descriptorA.offset) : null;
					break;

				case FormatType.R3G3B2:
				case FormatType.R5G6B5:
				case FormatType.R5G5B5A1:
				case FormatType.R4G4B4A4:
				case FormatType.R10G10B10A2:
				case FormatType.R11G11B10:
				case FormatType.R9G9B9E5:
					throw new NotImplementedException($"{this.Type} is not implemented yet.");
				case FormatType.D16:
				case FormatType.D24:
				case FormatType.D32:
				case FormatType.D24S8:
				case FormatType.D32S8:
				case FormatType.S8:
					break;
				default:
					throw new InvalidEnumArgumentException("Invalid FormatType.");
			}

			T emptyWriter = emptyWriter<T>();
			r = this.combineWriter(ColorMappingChannel.Red, zero, one, writerR, writerG, writerB, writerA) ?? emptyWriter;
			g = this.combineWriter(ColorMappingChannel.Green, zero, one, writerR, writerG, writerB, writerA) ?? emptyWriter;
			b = this.combineWriter(ColorMappingChannel.Blue, zero, one, writerR, writerG, writerB, writerA) ?? emptyWriter;
			a = this.combineWriter(ColorMappingChannel.Alpha, zero, one, writerR, writerG, writerB, writerA) ?? emptyWriter;
		}

		#endregion

		#region 1D

		/// <summary>
		/// Creates memory read accessors for the four color channels in the format for one-dimensional data.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <typeparam name="TValue">The value type.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="zero">The constant zero value.</param>
		/// <param name="one">The constant one value.</param>
		/// <param name="r">The reader for the red channel.</param>
		/// <param name="g">The reader for the green channel.</param>
		/// <param name="b">The reader for the blue channel.</param>
		/// <param name="a">The reader for the alpha channel.</param>
		public void CreateReaders<TAddress, TValue>(IntPtr baseAddress, DataDimension x, TValue zero, TValue one,
			out Func<TAddress, TValue> r,
			out Func<TAddress, TValue> g,
			out Func<TAddress, TValue> b,
			out Func<TAddress, TValue> a)
		{
			this.CreateReaders(baseAddress, new[] {x}, (i0) => zero, (i0) => one, out r, out g, out b, out a);
		}

		/// <summary>
		/// Creates memory write accessors for the four color channels in the format for one-dimensional data.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <typeparam name="TValue">The value type.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="zero">The constant zero value.</param>
		/// <param name="one">The constant one value.</param>
		/// <param name="r">The writer for the red channel.</param>
		/// <param name="g">The writer for the green channel.</param>
		/// <param name="b">The writer for the blue channel.</param>
		/// <param name="a">The writer for the alpha channel.</param>
		public void CreateWriters<TAddress, TValue>(IntPtr baseAddress, DataDimension x, TValue zero, TValue one,
			out Action<TAddress, TValue> r,
			out Action<TAddress, TValue> g,
			out Action<TAddress, TValue> b,
			out Action<TAddress, TValue> a)
		{
			this.CreateWriters(baseAddress, new[] {x}, zero, one, out r, out g, out b, out a);
		}
		
		/// <summary>
		/// Creates a memory read accessor for one-dimensional color data stored in this format.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <returns>A read accessor that returns <see cref="Color4"/>.</returns>
		public Func<TAddress, Color4> CreateColorReader<TAddress>(IntPtr baseAddress, DataDimension x)
		{
			this.CreateReaders(baseAddress, x, 0.0f, 1.0f, 
				out Func<TAddress, float> r,
				out Func<TAddress, float> g, 
				out Func<TAddress, float> b,
				out Func<TAddress, float> a);

			return (i0) =>
			{
				Color4 color;
				color.R = r(i0);
				color.G = g(i0);
				color.B = b(i0);
				color.A = a(i0);
				return color;
			};
		}
		
		/// <summary>
		/// Creates a memory write accessor for one-dimensional color data stored in this format.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <returns>A write accessor that takes <see cref="Color4"/>.</returns>
		public Action<TAddress, Color4> CreateColorWriter<TAddress>(IntPtr baseAddress, DataDimension x)
		{
			this.CreateWriters(baseAddress, x, 0.0f, 1.0f, 
				out Action<TAddress, float> r,
				out Action<TAddress, float> g, 
				out Action<TAddress, float> b,
				out Action<TAddress, float> a);

			return (i0, v) =>
			{
				r(i0, v.R);
				g(i0, v.G);
				b(i0, v.B);
				a(i0, v.A);
			};
		}

		#endregion

		#region 2D

		/// <summary>
		/// Creates memory read accessors for the four color channels in the format for two-dimensional data.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <typeparam name="TValue">The value type.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="y">The specification of the Y dimension.</param>
		/// <param name="zero">The constant zero value.</param>
		/// <param name="one">The constant one value.</param>
		/// <param name="r">The reader for the red channel.</param>
		/// <param name="g">The reader for the green channel.</param>
		/// <param name="b">The reader for the blue channel.</param>
		/// <param name="a">The reader for the alpha channel.</param>
		public void CreateReaders<TAddress, TValue>(IntPtr baseAddress, DataDimension x, DataDimension y, TValue zero, TValue one,
			out Func<TAddress, TAddress, TValue> r,
			out Func<TAddress, TAddress, TValue> g,
			out Func<TAddress, TAddress, TValue> b,
			out Func<TAddress, TAddress, TValue> a)
		{
			this.CreateReaders(baseAddress, new[] {x, y}, (i0, i1) => zero, (i0, i1) => one, out r, out g, out b, out a);
		}

		/// <summary>
		/// Creates memory write accessors for the four color channels in the format for two-dimensional data.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <typeparam name="TValue">The value type.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="y">The specification of the Y dimension.</param>
		/// <param name="zero">The constant zero value.</param>
		/// <param name="one">The constant one value.</param>
		/// <param name="r">The writer for the red channel.</param>
		/// <param name="g">The writer for the green channel.</param>
		/// <param name="b">The writer for the blue channel.</param>
		/// <param name="a">The writer for the alpha channel.</param>
		public void CreateWriters<TAddress, TValue>(IntPtr baseAddress, DataDimension x, DataDimension y, TValue zero, TValue one,
			out Action<TAddress, TAddress, TValue> r,
			out Action<TAddress, TAddress, TValue> g,
			out Action<TAddress, TAddress, TValue> b,
			out Action<TAddress, TAddress, TValue> a)
		{
			this.CreateWriters(baseAddress, new[] {x, y}, zero, one, out r, out g, out b, out a);
		}

		/// <summary>
		/// Creates a memory read accessor for two-dimensional color data stored in this format.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="y">The specification of the Y dimension.</param>
		/// <returns>A read accessor that returns <see cref="Color4"/>.</returns>
		public Func<TAddress, TAddress, Color4> CreateColorReader<TAddress>(IntPtr baseAddress, DataDimension x, DataDimension y)
		{
			this.CreateReaders(baseAddress, x, y, 0.0f, 1.0f, 
				out Func<TAddress, TAddress, float> r,
				out Func<TAddress, TAddress, float> g, 
				out Func<TAddress, TAddress, float> b,
				out Func<TAddress, TAddress, float> a);

			return (i0, i1) =>
			{
				Color4 color;
				color.R = r(i0, i1);
				color.G = g(i0, i1);
				color.B = b(i0, i1);
				color.A = a(i0, i1);
				return color;
			};
		}

		/// <summary>
		/// Creates a memory write accessor for two-dimensional color data stored in this format.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="y">The specification of the Y dimension.</param>
		/// <returns>A write accessor that takes <see cref="Color4"/>.</returns>
		public Action<TAddress, TAddress, Color4> CreateColorWriter<TAddress>(IntPtr baseAddress, DataDimension x, DataDimension y)
		{
			this.CreateWriters(baseAddress, x, y, 0.0f, 1.0f,
				out Action<TAddress, TAddress, float> r,
				out Action<TAddress, TAddress, float> g,
				out Action<TAddress, TAddress, float> b,
				out Action<TAddress, TAddress, float> a);

			return (i0, i1, v) =>
			{
				r(i0, i1, v.R);
				g(i0, i1, v.G);
				b(i0, i1, v.B);
				a(i0, i1, v.A);
			};
		}

		#endregion

		#region 3D

		/// <summary>
		/// Creates memory read accessors for the four color channels in the format for three-dimensional data.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <typeparam name="TValue">The value type.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="y">The specification of the Y dimension.</param>
		/// <param name="z">The specification of the Z dimension.</param>
		/// <param name="zero">The constant zero value.</param>
		/// <param name="one">The constant one value.</param>
		/// <param name="r">The reader for the red channel.</param>
		/// <param name="g">The reader for the green channel.</param>
		/// <param name="b">The reader for the blue channel.</param>
		/// <param name="a">The reader for the alpha channel.</param>
		public void CreateReaders<TAddress, TValue>(IntPtr baseAddress, DataDimension x, DataDimension y, DataDimension z, TValue zero, TValue one, 
			out Func<TAddress, TAddress, TAddress, TValue> r,
			out Func<TAddress, TAddress, TAddress, TValue> g, 
			out Func<TAddress, TAddress, TAddress, TValue> b,
			out Func<TAddress, TAddress, TAddress, TValue> a)
		{
			this.CreateReaders(baseAddress, new[] {x, y, z}, (i0, i1, i2) => zero, (i0, i1, i2) => one, out r, out g, out b, out a);
		}

		/// <summary>
		/// Creates memory write accessors for the four color channels in the format for three-dimensional data.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <typeparam name="TValue">The value type.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="y">The specification of the Y dimension.</param>
		/// <param name="z">The specification of the Z dimension.</param>
		/// <param name="zero">The constant zero value.</param>
		/// <param name="one">The constant one value.</param>
		/// <param name="r">The writer for the red channel.</param>
		/// <param name="g">The writer for the green channel.</param>
		/// <param name="b">The writer for the blue channel.</param>
		/// <param name="a">The writer for the alpha channel.</param>
		public void CreateWriters<TAddress, TValue>(IntPtr baseAddress, DataDimension x, DataDimension y, DataDimension z, TValue zero, TValue one,
			out Action<TAddress, TAddress, TAddress, TValue> r,
			out Action<TAddress, TAddress, TAddress, TValue> g,
			out Action<TAddress, TAddress, TAddress, TValue> b,
			out Action<TAddress, TAddress, TAddress, TValue> a)
		{
			this.CreateWriters(baseAddress, new[] {x, y, z}, zero, one, out r, out g, out b, out a);
		}
		
		/// <summary>
		/// Creates a memory read accessor for three-dimensional color data stored in this format.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="y">The specification of the Y dimension.</param>
		/// <param name="z">The specification of the Z dimension.</param>
		/// <returns>A read accessor that returns <see cref="Color4"/>.</returns>
		public Func<TAddress, TAddress, TAddress, Color4> CreateColorReader<TAddress>(IntPtr baseAddress, DataDimension x, DataDimension y, DataDimension z)
		{
			this.CreateReaders(baseAddress, x, y, z, 0.0f, 1.0f, 
				out Func<TAddress, TAddress, TAddress, float> r,
				out Func<TAddress, TAddress, TAddress, float> g, 
				out Func<TAddress, TAddress, TAddress, float> b,
				out Func<TAddress, TAddress, TAddress, float> a);

			return (i0, i1, i2) =>
			{
				Color4 color;
				color.R = r(i0, i1, i2);
				color.G = g(i0, i1, i2);
				color.B = b(i0, i1, i2);
				color.A = a(i0, i1, i2);
				return color;
			};
		}
		
		/// <summary>
		/// Creates a memory write accessor for three-dimensional color data stored in this format.
		/// </summary>
		/// <typeparam name="TAddress">The type of the indices/addresses.</typeparam>
		/// <param name="baseAddress">The memory base address.</param>
		/// <param name="x">The specification of the X dimension.</param>
		/// <param name="y">The specification of the Y dimension.</param>
		/// <param name="z">The specification of the Z dimension.</param>
		/// <returns>A write accessor that returns <see cref="Color4"/>.</returns>
		public Action<TAddress, TAddress, TAddress, Color4> CreateColorWriter<TAddress>(IntPtr baseAddress, DataDimension x, DataDimension y, DataDimension z)
		{
			this.CreateWriters(baseAddress, x, y, z, 0.0f, 1.0f,
				out Action<TAddress, TAddress, TAddress, float> r,
				out Action<TAddress, TAddress, TAddress, float> g, 
				out Action<TAddress, TAddress, TAddress, float> b,
				out Action<TAddress, TAddress, TAddress, float> a);

			return (i0, i1, i2, v) =>
			{
				r(i0, i1, i2, v.R);
				g(i0, i1, i2, v.G);
				b(i0, i1, i2, v.B);
				a(i0, i1, i2, v.A);
			};
		}
		
		#endregion

		#endregion
	}
}
