﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Exception class for invalid formats.
	/// </summary>
	[Serializable]
	public sealed class InvalidFormatException : Exception
	{
		#region Constructors

		private InvalidFormatException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{

		}

		/// <summary>
		/// Creates a new InvalidFormatException.
		/// </summary>
		public InvalidFormatException()
			: base("Invalid format.")
		{

		}

		/// <summary>
		/// Creates a new InvalidFormatException.
		/// </summary>
		public InvalidFormatException(string message)
			: base(message)
		{

		}


		#endregion
	}
}
