﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics
{
	[StructLayout(LayoutKind.Sequential, Size = 1, Pack = 1)]
	[Serializable]
	internal struct Stencil8
	{
		public float D => 0.0f;
		public byte S;
	}

	[StructLayout(LayoutKind.Sequential, Size = 2, Pack = 2)]
	[Serializable]
	internal struct Depth16
	{
		public UNorm16 D;
		public byte S => 0;
	}

	[StructLayout(LayoutKind.Sequential, Size = 4, Pack = 4)]
	[Serializable]
	internal struct Depth24
	{
		public UNorm24 D;
		public byte S => 0;
	}

	[StructLayout(LayoutKind.Sequential, Size = 4, Pack = 4)]
	[Serializable]
	internal struct Depth24Stencil8
	{
		public UNorm24 D;
		public byte S;
	}

	[StructLayout(LayoutKind.Sequential, Size = 4, Pack = 4)]
	[Serializable]
	internal struct Depth32F
	{
		public float D;
		public byte S => 0;
	}

	[StructLayout(LayoutKind.Sequential, Size = 8, Pack = 8)]
	[Serializable]
	internal struct Depth32FStencil8
	{
		public float D;
		public byte S;
	}
}
