﻿using System;
using System.Runtime.InteropServices;


namespace Iridescence.Graphics
{
	
	[StructLayout(LayoutKind.Sequential, Size = 1, Pack = 1)]
	[Serializable]
	[ColorStruct(3)]
	internal struct R3G3B2_UInt8 : IColor3<byte>
	{
		public byte Value;

		
		public byte R
		{
			get
			{
				int v = (this.Value & 224) >> 5;
				byte value;
				value = unchecked((byte)(v * 255 / 7));
				return value;
			}
			set
			{
				int v = ((value) * 7 + 127) / 255;
				this.Value = unchecked((byte)((this.Value & -225) | (v << 5)));
			}
		}
		
		public byte G
		{
			get
			{
				int v = (this.Value & 28) >> 2;
				byte value;
				value = unchecked((byte)(v * 255 / 7));
				return value;
			}
			set
			{
				int v = ((value) * 7 + 127) / 255;
				this.Value = unchecked((byte)((this.Value & -29) | (v << 2)));
			}
		}
		
		public byte B
		{
			get
			{
				int v = (this.Value & 3) >> 0;
				byte value;
				value = unchecked((byte)(v * 255 / 3));
				return value;
			}
			set
			{
				int v = ((value) * 3 + 127) / 255;
				this.Value = unchecked((byte)((this.Value & -4) | (v << 0)));
			}
		}
		
	}
	
	[StructLayout(LayoutKind.Sequential, Size = 1, Pack = 1)]
	[Serializable]
	[ColorStruct(3)]
	internal struct R3G3B2_UNorm8 : IColor3<UNorm8>
	{
		public byte Value;

		
		public UNorm8 R
		{
			get
			{
				int v = (this.Value & 224) >> 5;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 7));
				return value;
			}
			set
			{
				int v = ((value.Value) * 7 + 127) / 255;
				this.Value = unchecked((byte)((this.Value & -225) | (v << 5)));
			}
		}
		
		public UNorm8 G
		{
			get
			{
				int v = (this.Value & 28) >> 2;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 7));
				return value;
			}
			set
			{
				int v = ((value.Value) * 7 + 127) / 255;
				this.Value = unchecked((byte)((this.Value & -29) | (v << 2)));
			}
		}
		
		public UNorm8 B
		{
			get
			{
				int v = (this.Value & 3) >> 0;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 3));
				return value;
			}
			set
			{
				int v = ((value.Value) * 3 + 127) / 255;
				this.Value = unchecked((byte)((this.Value & -4) | (v << 0)));
			}
		}
		
	}
	
	[StructLayout(LayoutKind.Sequential, Size = 2, Pack = 2)]
	[Serializable]
	[ColorStruct(3)]
	internal struct R5G6B5_UInt8 : IColor3<byte>
	{
		public ushort Value;

		
		public byte R
		{
			get
			{
				int v = (this.Value & 63488) >> 11;
				byte value;
				value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -63489) | (v << 11)));
			}
		}
		
		public byte G
		{
			get
			{
				int v = (this.Value & 2016) >> 5;
				byte value;
				value = unchecked((byte)(v * 255 / 63));
				return value;
			}
			set
			{
				int v = ((value) * 63 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -2017) | (v << 5)));
			}
		}
		
		public byte B
		{
			get
			{
				int v = (this.Value & 31) >> 0;
				byte value;
				value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -32) | (v << 0)));
			}
		}
		
	}
	
	[StructLayout(LayoutKind.Sequential, Size = 2, Pack = 2)]
	[Serializable]
	[ColorStruct(3)]
	internal struct R5G6B5_UNorm8 : IColor3<UNorm8>
	{
		public ushort Value;

		
		public UNorm8 R
		{
			get
			{
				int v = (this.Value & 63488) >> 11;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value.Value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -63489) | (v << 11)));
			}
		}
		
		public UNorm8 G
		{
			get
			{
				int v = (this.Value & 2016) >> 5;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 63));
				return value;
			}
			set
			{
				int v = ((value.Value) * 63 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -2017) | (v << 5)));
			}
		}
		
		public UNorm8 B
		{
			get
			{
				int v = (this.Value & 31) >> 0;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value.Value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -32) | (v << 0)));
			}
		}
		
	}
	
	[StructLayout(LayoutKind.Sequential, Size = 2, Pack = 2)]
	[Serializable]
	[ColorStruct(4)]
	internal struct R5G5B5A1_UInt8 : IColor4<byte>
	{
		public ushort Value;

		
		public byte R
		{
			get
			{
				int v = (this.Value & 63488) >> 11;
				byte value;
				value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -63489) | (v << 11)));
			}
		}
		
		public byte G
		{
			get
			{
				int v = (this.Value & 1984) >> 6;
				byte value;
				value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -1985) | (v << 6)));
			}
		}
		
		public byte B
		{
			get
			{
				int v = (this.Value & 62) >> 1;
				byte value;
				value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -63) | (v << 1)));
			}
		}
		
		public byte A
		{
			get
			{
				int v = (this.Value & 1) >> 0;
				byte value;
				value = unchecked((byte)(v * 255 / 1));
				return value;
			}
			set
			{
				int v = ((value) * 1 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -2) | (v << 0)));
			}
		}
		
	}
	
	[StructLayout(LayoutKind.Sequential, Size = 2, Pack = 2)]
	[Serializable]
	[ColorStruct(4)]
	internal struct R5G5B5A1_UNorm8 : IColor4<UNorm8>
	{
		public ushort Value;

		
		public UNorm8 R
		{
			get
			{
				int v = (this.Value & 63488) >> 11;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value.Value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -63489) | (v << 11)));
			}
		}
		
		public UNorm8 G
		{
			get
			{
				int v = (this.Value & 1984) >> 6;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value.Value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -1985) | (v << 6)));
			}
		}
		
		public UNorm8 B
		{
			get
			{
				int v = (this.Value & 62) >> 1;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 31));
				return value;
			}
			set
			{
				int v = ((value.Value) * 31 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -63) | (v << 1)));
			}
		}
		
		public UNorm8 A
		{
			get
			{
				int v = (this.Value & 1) >> 0;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 1));
				return value;
			}
			set
			{
				int v = ((value.Value) * 1 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -2) | (v << 0)));
			}
		}
		
	}
	
	[StructLayout(LayoutKind.Sequential, Size = 2, Pack = 2)]
	[Serializable]
	[ColorStruct(4)]
	internal struct R4G4B4A4_UInt8 : IColor4<byte>
	{
		public ushort Value;

		
		public byte R
		{
			get
			{
				int v = (this.Value & 61440) >> 12;
				byte value;
				value = unchecked((byte)(v * 255 / 15));
				return value;
			}
			set
			{
				int v = ((value) * 15 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -61441) | (v << 12)));
			}
		}
		
		public byte G
		{
			get
			{
				int v = (this.Value & 3840) >> 8;
				byte value;
				value = unchecked((byte)(v * 255 / 15));
				return value;
			}
			set
			{
				int v = ((value) * 15 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -3841) | (v << 8)));
			}
		}
		
		public byte B
		{
			get
			{
				int v = (this.Value & 240) >> 4;
				byte value;
				value = unchecked((byte)(v * 255 / 15));
				return value;
			}
			set
			{
				int v = ((value) * 15 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -241) | (v << 4)));
			}
		}
		
		public byte A
		{
			get
			{
				int v = (this.Value & 15) >> 0;
				byte value;
				value = unchecked((byte)(v * 255 / 15));
				return value;
			}
			set
			{
				int v = ((value) * 15 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -16) | (v << 0)));
			}
		}
		
	}
	
	[StructLayout(LayoutKind.Sequential, Size = 2, Pack = 2)]
	[Serializable]
	[ColorStruct(4)]
	internal struct R4G4B4A4_UNorm8 : IColor4<UNorm8>
	{
		public ushort Value;

		
		public UNorm8 R
		{
			get
			{
				int v = (this.Value & 61440) >> 12;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 15));
				return value;
			}
			set
			{
				int v = ((value.Value) * 15 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -61441) | (v << 12)));
			}
		}
		
		public UNorm8 G
		{
			get
			{
				int v = (this.Value & 3840) >> 8;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 15));
				return value;
			}
			set
			{
				int v = ((value.Value) * 15 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -3841) | (v << 8)));
			}
		}
		
		public UNorm8 B
		{
			get
			{
				int v = (this.Value & 240) >> 4;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 15));
				return value;
			}
			set
			{
				int v = ((value.Value) * 15 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -241) | (v << 4)));
			}
		}
		
		public UNorm8 A
		{
			get
			{
				int v = (this.Value & 15) >> 0;
				UNorm8 value;
				value.Value = unchecked((byte)(v * 255 / 15));
				return value;
			}
			set
			{
				int v = ((value.Value) * 15 + 127) / 255;
				this.Value = unchecked((ushort)((this.Value & -16) | (v << 0)));
			}
		}
		
	}
	
}

