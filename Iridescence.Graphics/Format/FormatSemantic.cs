﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines the semantic of format components, i.e. the way they're meant to be accessed.
	/// </summary>
	public enum FormatSemantic
	{
		/// <summary>
		/// Unspecified semantic.
		/// Elements stored without a semantic will only not be accessible because there is no way to interpret the data.
		/// </summary>
		None,

		/// <summary>
		/// The data is supposed to be accessed as floating-point values resulting from normalizing an unsigned input integer to the range [0..1].
		/// </summary>
		UNorm,

		/// <summary>
		/// The data is supposed to be accessed as floating-point values resulting from normalizing a signed input integer to the range [-1..1].
		/// </summary>
		SNorm,

		/// <summary>
		/// The data is supposed to be accessed as unsigned integer.
		/// </summary>
		UInt,

		/// <summary>
		/// The data is supposed to be accessed as signed integer.
		/// </summary>
		SInt,
		
		/// <summary>
		/// The data is supposed to be accessed as floating-point values.
		/// </summary>
		Float,
	}
}
