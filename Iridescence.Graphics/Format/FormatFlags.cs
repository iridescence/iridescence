﻿using System;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines the flags of a <see cref="Format"/>.
	/// </summary>
	[Flags]
	public enum FormatFlags
	{
		/// <summary>
		/// No flags.
		/// </summary>
		None = 0,
		
		/// <summary>
		/// Reversed bit order. This may be used with some of the packed format to indicate that the bit order is reversed.
		/// Usually, the first component (i.e. Red) is located in the most significant bits of the pixel value. When the <see cref="Reversed"/> flag is set, the first component is located in the least significant bits.
		/// </summary>
		Reversed = 1,

		/// <summary>
		/// Indicates that the Red, Green and Blue components of a color stored in this format are not to be interpreted as linear values in the range [0..1] but instead be gamma-(un)corrected as defined in the sRGB standard.
		/// </summary>
		Srgb = 2,
	}
}
