﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Exception class for invalid <see cref="ColorMapping"/>.
	/// </summary>
	[Serializable]
	public sealed class InvalidMappingException : Exception
	{
		#region Constructors

		private InvalidMappingException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{

		}

		/// <summary>
		/// Creates a new InvalidMappingException.
		/// </summary>
		public InvalidMappingException()
			: base($"Invalid {nameof(ColorMapping)}.")
		{

		}

		/// <summary>
		/// Creates a new InvalidMappingException.
		/// </summary>
		public InvalidMappingException(string message)
			: base(message)
		{

		}
		
		#endregion
	}
}
