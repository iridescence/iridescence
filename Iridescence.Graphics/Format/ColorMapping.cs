﻿using System;
using System.ComponentModel;

namespace Iridescence.Graphics
{
	/// <summary>
	/// Represents a color channel mapping or swizzle specification.
	/// </summary>
	[Serializable]
	public struct ColorMapping : IEquatable<ColorMapping>
	{
		#region Properties

		/// <summary>
		/// Gets the mapping of the red channel.
		/// </summary>
		public ColorMappingChannel Red { get; }

		/// <summary>
		/// Gets the mapping of the green channel.
		/// </summary>
		public ColorMappingChannel Green { get; }

		/// <summary>
		/// Gets the mapping of the blue channel.
		/// </summary>
		public ColorMappingChannel Blue { get; }

		/// <summary>
		/// Gets the mapping of the alpha channel.
		/// </summary>
		public ColorMappingChannel Alpha { get; }

		/// <summary>
		/// Gets a value that indicates whether this is the default identity mapping, i.e. <see cref="Red"/> is mapped to <see cref="ColorMappingChannel.Red"/>, 
		/// <see cref="Green"/> to <see cref="ColorMappingChannel.Green"/>, <see cref="Blue"/> to <see cref="ColorMappingChannel.Blue"/> and <see cref="Alpha"/> to <see cref="ColorMappingChannel.Alpha"/>.
		/// </summary>
		public bool IsIdentity => this.Red == ColorMappingChannel.Red && this.Green == ColorMappingChannel.Green && this.Blue == ColorMappingChannel.Blue && this.Alpha == ColorMappingChannel.Alpha;

		/// <summary>
		/// Gets the identity format mapping where <see cref="Red"/> is mapped to <see cref="ColorMappingChannel.Red"/>, <see cref="Green"/> to <see cref="ColorMappingChannel.Green"/>,
		/// <see cref="Blue"/> to <see cref="ColorMappingChannel.Blue"/> and <see cref="Alpha"/> to <see cref="ColorMappingChannel.Alpha"/>.
		/// </summary>
		public static ColorMapping RGBA => new ColorMapping(ColorMappingChannel.Red, ColorMappingChannel.Green, ColorMappingChannel.Blue, ColorMappingChannel.Alpha);

		/// <summary>
		/// Gets a RGB0 mapping (constant 0 in alpha channel).
		/// </summary>
		public static ColorMapping RGB0 => new ColorMapping(ColorMappingChannel.Red, ColorMappingChannel.Green, ColorMappingChannel.Blue, ColorMappingChannel.Zero);

		/// <summary>
		/// Gets a RGB1 mapping (constant 1 in alpha channel).
		/// </summary>
		public static ColorMapping RGB1 => new ColorMapping(ColorMappingChannel.Red, ColorMappingChannel.Green, ColorMappingChannel.Blue, ColorMappingChannel.One);

		/// <summary>
		/// Gets a RGB-None mapping (no alpha channel mapping). Useful for input only.
		/// </summary>
		public static ColorMapping RGBN => new ColorMapping(ColorMappingChannel.Red, ColorMappingChannel.Green, ColorMappingChannel.Blue, ColorMappingChannel.None);
		
		/// <summary>
		/// Gets a BGRA mapping (reversed color channels).
		/// </summary>
		public static ColorMapping BGRA => new ColorMapping(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.Alpha);

		/// <summary>
		/// Gets a BGR1 mapping (reversed color channels and constant 0 in alpha channel).
		/// </summary>
		public static ColorMapping BGR0 => new ColorMapping(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.Zero);

		/// <summary>
		/// Gets a BGR1 mapping (reversed color channels and constant 1 in alpha channel).
		/// </summary>
		public static ColorMapping BGR1 => new ColorMapping(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.One);
		
		/// <summary>
		/// Gets a BGR-None mapping (no alpha channel mapping). Useful for input only.
		/// </summary>
		public static ColorMapping BGRN => new ColorMapping(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.None);

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ColorMapping"/> class. 
		/// </summary>
		public ColorMapping(ColorMappingChannel red, ColorMappingChannel green, ColorMappingChannel blue, ColorMappingChannel alpha)
		{
			if (!EnumUtility<ColorMappingChannel, int>.IsDefined(red))
				throw new InvalidEnumArgumentException(nameof(red), (int)red, typeof(ColorMappingChannel));

			if (!EnumUtility<ColorMappingChannel, int>.IsDefined(green))
				throw new InvalidEnumArgumentException(nameof(green), (int)green, typeof(ColorMappingChannel));

			if (!EnumUtility<ColorMappingChannel, int>.IsDefined(blue))
				throw new InvalidEnumArgumentException(nameof(blue), (int)blue, typeof(ColorMappingChannel));

			if (!EnumUtility<ColorMappingChannel, int>.IsDefined(alpha))
				throw new InvalidEnumArgumentException(nameof(alpha), (int)alpha, typeof(ColorMappingChannel));

			this.Red = red;
			this.Green = green;
			this.Blue = blue;
			this.Alpha = alpha;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns whether the specified <see cref="ColorMappingChannel"/> would be valid for an output mapping.
		/// </summary>
		/// <param name="ch"></param>
		/// <returns></returns>
		public static bool IsValidForOutput(ColorMappingChannel ch)
		{
			return ch == ColorMappingChannel.Red || ch == ColorMappingChannel.Green || ch == ColorMappingChannel.Blue || ch == ColorMappingChannel.Alpha || ch == ColorMappingChannel.Zero || ch == ColorMappingChannel.One;
		}

		/// <summary>
		/// Returns whether the specified <see cref="ColorMappingChannel"/> would be valid for an input mapping.
		/// </summary>
		/// <param name="ch"></param>
		/// <returns></returns>
		public static bool IsValidForInput(ColorMappingChannel ch)
		{
			return ch == ColorMappingChannel.Red || ch == ColorMappingChannel.Green || ch == ColorMappingChannel.Blue || ch == ColorMappingChannel.Alpha || ch == ColorMappingChannel.None;
		}

		/// <summary>
		/// Returns a value that indicates whether this <see cref="ColorMapping"/> would be valid for an output mapping.
		/// </summary>
		/// <returns></returns>
		public bool IsValidForOutput()
		{
			return IsValidForOutput(this.Red) && IsValidForOutput(this.Green) && IsValidForOutput(this.Blue) && IsValidForOutput(this.Alpha);
		}

		/// <summary>
		/// Returns a value that indicates whether this <see cref="ColorMapping"/> would be valid for an input mapping.
		/// </summary>
		/// <returns></returns>
		public bool IsValidForInput()
		{
			return IsValidForInput(this.Red) && IsValidForInput(this.Green) && IsValidForInput(this.Blue) && IsValidForInput(this.Alpha);
		}

		private static Exception getOutputValidationException(string invalidChannelName, ColorMappingChannel invalidValue)
		{
			return new InvalidMappingException($"Invalid output channel mapping. The mapping for the {invalidChannelName} channel must be one of {ColorMappingChannel.Red}, {ColorMappingChannel.Green}, {ColorMappingChannel.Blue}, {ColorMappingChannel.Alpha}, {ColorMappingChannel.Zero} or {ColorMappingChannel.One}; found: {invalidValue}.");
		}

		private static Exception getInputValidationException(string invalidChannelName, ColorMappingChannel invalidValue)
		{
			return new InvalidMappingException($"Invalid input channel mapping. The mapping for the {invalidChannelName} channel must be one of {ColorMappingChannel.Red}, {ColorMappingChannel.Green}, {ColorMappingChannel.Blue}, {ColorMappingChannel.Alpha} or {ColorMappingChannel.None}; found: {invalidValue}.");
		}

		internal Exception GetOutputValidationException()
		{
			if (!IsValidForOutput(this.Red))
				return getOutputValidationException(nameof(this.Red), this.Red);

			if (!IsValidForOutput(this.Green))
				return getOutputValidationException(nameof(this.Green), this.Green);

			if (!IsValidForOutput(this.Blue))
				return getOutputValidationException(nameof(this.Blue), this.Blue);

			if (!IsValidForOutput(this.Alpha))
				return getOutputValidationException(nameof(this.Alpha), this.Alpha);

			return null;
		}

		internal Exception GetInputValidationException()
		{
			if (!IsValidForInput(this.Red))
				return getInputValidationException(nameof(this.Red), this.Red);

			if (!IsValidForInput(this.Green))
				return getInputValidationException(nameof(this.Green), this.Green);

			if (!IsValidForInput(this.Blue))
				return getInputValidationException(nameof(this.Blue), this.Blue);

			if (!IsValidForInput(this.Alpha))
				return getInputValidationException(nameof(this.Alpha), this.Alpha);

			return null;
		}

		public bool Equals(ColorMapping other)
		{
			return this.Red == other.Red && this.Green == other.Green && this.Blue == other.Blue && this.Alpha == other.Alpha;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is ColorMapping && this.Equals((ColorMapping)obj);
		}

		public override int GetHashCode()
		{
			return (int)this.Red |
			       ((int)this.Green << 4) |
			       ((int)this.Green << 8) |
			       ((int)this.Green << 12);
		}

		public static bool operator ==(ColorMapping left, ColorMapping right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(ColorMapping left, ColorMapping right)
		{
			return !left.Equals(right);
		}

		public override string ToString()
		{
			return $"Red:{this.Red}, Green:{this.Green}, Blue:{this.Blue}, Alpha:{this.Alpha}";
		}

		public static implicit operator ColorMapping((ColorMappingChannel r, ColorMappingChannel g, ColorMappingChannel b, ColorMappingChannel a) tuple)
		{
			return new ColorMapping(tuple.r, tuple.g, tuple.b, tuple.a);
		}

		public static implicit operator (ColorMappingChannel r, ColorMappingChannel g, ColorMappingChannel b, ColorMappingChannel a)(ColorMapping mapping)
		{
			return (mapping.Red, mapping.Green, mapping.Blue, mapping.Alpha);
		}

		#endregion
	}
}
