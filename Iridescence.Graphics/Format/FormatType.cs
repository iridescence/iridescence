﻿namespace Iridescence.Graphics
{
	/// <summary>
	/// Defines the channel format that specified the number of bits in a channel.
	/// Red, green, blue and alpha for colors are synonymous to X, Y, Z and W for vectors.
	/// </summary>
	/// <remarks>
	/// The value of an enum fields must not exceed 0xFF.
	/// The most significant bit (0x80) signifies that the format is compressed.
	/// The second most significant bit (0x40) signifies that the format is a depth-stencil format.
	/// </remarks>
	public enum FormatType
	{
		#region Base Formats
		
		/// <summary>
		/// No format.
		/// </summary>
		None,
		
		/// <summary>
		/// 8-bit red channel.
		/// </summary>
		R8,

		/// <summary>
		/// 8-bit red and green channels.
		/// </summary>
		R8G8,

		/// <summary>
		/// 8-bit red, green and blue channels.
		/// </summary>
		R8G8B8,

		/// <summary>
		/// 8-bit red, green, blue and alpha channels.
		/// </summary>
		R8G8B8A8,
		
		/// <summary>
		/// 16-bit red channel.
		/// </summary>
		R16,

		/// <summary>
		/// 16-bit red and green channels.
		/// </summary>
		R16G16,

		/// <summary>
		/// 16-bit red, green and blue channels.
		/// </summary>
		R16G16B16,

		/// <summary>
		/// 16-bit red, green, blue and alpha channels.
		/// </summary>
		R16G16B16A16,

		/// <summary>
		/// 32-bit red channel.
		/// </summary>
		R32,

		/// <summary>
		/// 32-bit red and green channel.
		/// </summary>
		R32G32,

		/// <summary>
		/// 32-bit red, green and blue channel.
		/// </summary>
		R32G32B32,

		/// <summary>
		/// 32-bit red, green, blue and alpha channel.
		/// </summary>
		R32G32B32A32,

		#endregion

		#region Special Formats
		
		/// <summary>
		/// 3-bit red channel, 3-bit green channel and 2-bit green channel packed into one byte.
		/// </summary>
		R3G3B2,
		
		/// <summary>
		/// 5-bit red channel, 6-bit green channel and 5-bit blue channel packed into one 16-bit integer.
		/// </summary>
		R5G6B5,

		/// <summary>
		/// 5-bit red, green and blue channel with 1-bit of alpha packed into one 16-bit integer.
		/// </summary>
		R5G5B5A1,
		
		/// <summary>
		/// 4-bit red, green, blue and alpha channels packed into one 16-bit integer.
		/// </summary>
		R4G4B4A4,

		/// <summary>
		/// 10-bit red, green and blue channel with 2-bit alpha channel into one 32-bit integer.
		/// </summary>
		R10G10B10A2,
		
		/// <summary>
		/// 11-bit red and green channel and 10-bit blue channel packed into one 32-bit integer, used in conjunction with the <see cref="FormatSemantic.Float"/> semantic.
		/// </summary>
		R11G11B10,

		/// <summary>
		/// 9-bit red, green and blue mantissa with a shared 5-bit exponent packed into one 32-bit integer, used in conjunction with the <see cref="FormatSemantic.Float"/> semantic.
		/// </summary>
		R9G9B9E5,
		
		#endregion

		#region Depth Stencil Formats

		/// <summary>
		/// 16-bit depth channel.
		/// </summary>
		D16 = Graphics.Format.DepthStencilTypeBit,

		/// <summary>
		/// 24-bit depth channel.
		/// </summary>
		D24,

		/// <summary>
		/// 32-bit depth channel.
		/// </summary>
		D32,

		/// <summary>
		/// 24-bit depth channel and 8-bit stencil channel.
		/// </summary>
		D24S8,

		/// <summary>
		/// 32-bit depth channel and 8-bit stencil channel.
		/// </summary>
		D32S8,

		/// <summary>
		/// 8-bit stencil channel.
		/// </summary>
		S8,

		#endregion
	}
}
