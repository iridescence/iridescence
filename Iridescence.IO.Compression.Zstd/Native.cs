﻿using System;
using System.Runtime.InteropServices;
using size_t = System.UIntPtr;

namespace Iridescence.IO.Compression.Zstd
{
	/// <summary>
	/// Native bindings for libzstd.
	/// </summary>
	internal static class Native
	{
		private const string lib = "libzstd.dll";
		private const CallingConvention callingConvention = CallingConvention.Cdecl;

		[StructLayout(LayoutKind.Sequential)]
		public struct Buffer
		{
			public IntPtr Data;
			public size_t Size;
			public size_t Position;
		}

		public enum EndDirective
		{
			Continue = 0,
			Flush = 1,
			End = 2
		}

		// Version

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern uint ZSTD_versionNumber();

		// Simple API

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_compress(IntPtr dst, size_t dstCapacity, IntPtr src, size_t srcSize, int compressionLevel);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_decompress(IntPtr dst, size_t dstCapacity, IntPtr src, size_t compressedSize);

		public const ulong ZSTD_CONTENTSIZE_UNKNOWN = unchecked(0UL - 1);
		public const ulong ZSTD_CONTENTSIZE_ERROR = unchecked(0UL - 2);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern ulong ZSTD_getFrameContentSize(IntPtr src, size_t srcSize);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern ulong ZSTD_getDecompressedSize(IntPtr src, size_t srcSize);

		// Helper

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_compressBound(size_t srcSize);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern bool ZSTD_isError(size_t code);

		[DllImport(lib, CallingConvention = callingConvention)]
		[return: MarshalAs(UnmanagedType.LPStr)]
		public static extern string ZSTD_getErrorName(size_t code);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern int ZSTD_maxCLevel();

		// ZSTD_CStream management functions

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern IntPtr ZSTD_createCStream();

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_freeCStream(IntPtr zcs);

		// Streaming compression functions

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_initCStream(IntPtr zcs, int compressionLevel);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_compressStream(IntPtr zcs, ref Buffer output, ref Buffer input);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_compressStream2(IntPtr zcs, ref Buffer output, ref Buffer input, EndDirective endOp);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_flushStream(IntPtr zcs, ref Buffer output);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_endStream(IntPtr zcs, ref Buffer output);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_CStreamInSize();

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_CStreamOutSize();

		// ZSTD_DStream management functions

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern IntPtr ZSTD_createDStream();

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_freeDStream(IntPtr zds);

		// Streaming decompression functions

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_initDStream(IntPtr zds);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_decompressStream(IntPtr zds, ref Buffer output, ref Buffer input);

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_DStreamInSize();

		[DllImport(lib, CallingConvention = callingConvention)]
		public static extern size_t ZSTD_DStreamOutSize();


	}
}
