﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence.IO.Compression.Zstd
{
	/// <summary>
	/// Implements a Zstandard decompression stream.
	/// </summary>
	public class ZstdDecoderStream : Stream
	{
		#region Fields

		private readonly Stream inner;
		private readonly bool leaveOpen;
		private readonly IntPtr zds;
		
		private bool isInitialized;
		
		private readonly byte[] inBuffer;
		private int inBufferPos;
		private int inBufferSize;

		private int disposed;

		#endregion

		#region Properties

		public override bool CanRead => this.inner.CanRead;

		public override bool CanSeek => false;

		public override bool CanWrite => false;

		public override long Length => throw new NotSupportedException();

		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ZstdDecoderStream"/> class. 
		/// </summary>
		public ZstdDecoderStream(Stream inner, bool leaveOpen = false)
		{
			this.inner = inner ?? throw new ArgumentNullException(nameof(inner));
			
			this.zds = Native.ZSTD_createDStream();
			if (this.zds == IntPtr.Zero)
				throw new ZstdException("Could not create ZSTD_DStream.");

			this.leaveOpen = leaveOpen;

			int inSize = checked((int)Native.ZSTD_DStreamInSize().ToUInt32());
			this.inBuffer = new byte[inSize];
		}

		#endregion

		#region Methods

		private void init()
		{
			if (this.isInitialized)
				return;

			ZstdException.ThrowOnError(Native.ZSTD_initDStream(this.zds));
			this.isInitialized = true;
		}

		public bool TryInitialize()
		{
			if (this.isInitialized)
				return false;

			this.init();
			return true;
		}

		public override void Flush()
		{

		}

		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			return Task.CompletedTask;
		}

		public override unsafe int Read(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));

			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			this.init();

			if (count == 0)
				return 0;

			fixed (byte* inPtr = this.inBuffer)
			fixed (byte* outPtr = &buffer[offset])
			{
				Native.Buffer outputBuffer;
				outputBuffer.Data = new IntPtr(outPtr);
				outputBuffer.Size = new UIntPtr((uint)count);
				outputBuffer.Position = UIntPtr.Zero;

				Native.Buffer inputBuffer;
				inputBuffer.Data = new IntPtr(inPtr);
				inputBuffer.Size = new UIntPtr((uint)this.inBufferSize);
				inputBuffer.Position = new UIntPtr((uint)this.inBufferPos);

				while (outputBuffer.Position.ToUInt64() == 0)
				//while (outputBuffer.Position.ToUInt64() < outputBuffer.Size.ToUInt64())
				{
					if (inputBuffer.Position.ToUInt64() >= inputBuffer.Size.ToUInt64())
					{
						int n = this.inner.Read(this.inBuffer, 0, this.inBuffer.Length);
						if (n == 0)
							break;

						inputBuffer.Size = new UIntPtr((uint)n);
						inputBuffer.Position = UIntPtr.Zero;
					}

					UIntPtr nextSize = Native.ZSTD_decompressStream(this.zds, ref outputBuffer, ref inputBuffer);
					ZstdException.ThrowOnError(nextSize);
				}

				this.inBufferPos = checked((int)inputBuffer.Position.ToUInt64());
				this.inBufferSize = checked((int)inputBuffer.Size.ToUInt64());

				return checked((int)outputBuffer.Position.ToUInt64());
			}
		}

		public override async Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));

			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			this.init();

			if (count == 0)
				return 0;

			GCHandle inHandle = GCHandle.Alloc(this.inBuffer, GCHandleType.Pinned);
			GCHandle outHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);

			try
			{
				Native.Buffer outputBuffer;
				outputBuffer.Data = outHandle.AddrOfPinnedObject();
				outputBuffer.Size = new UIntPtr((uint)count);
				outputBuffer.Position = UIntPtr.Zero;

				Native.Buffer inputBuffer;
				inputBuffer.Data = inHandle.AddrOfPinnedObject() + offset;
				inputBuffer.Size = new UIntPtr((uint)this.inBufferSize);
				inputBuffer.Position = new UIntPtr((uint)this.inBufferPos);

				bool canCancel = true;
				while (outputBuffer.Position.ToUInt64() == 0)
				//while (outputBuffer.Position.ToUInt64() < outputBuffer.Size.ToUInt64())
				{
					if (inputBuffer.Position.ToUInt64() >= inputBuffer.Size.ToUInt64())
					{
						int n = await this.inner.ReadAsync(this.inBuffer, 0, this.inBuffer.Length, canCancel ? cancellationToken : CancellationToken.None);
						if (n == 0)
							break;

						inputBuffer.Size = new UIntPtr((uint)n);
						inputBuffer.Position = UIntPtr.Zero;
						canCancel = false;
					}

					UIntPtr nextSize = Native.ZSTD_decompressStream(this.zds, ref outputBuffer, ref inputBuffer);
					ZstdException.ThrowOnError(nextSize);
				}

				this.inBufferPos = checked((int)inputBuffer.Position.ToUInt64());
				this.inBufferSize = checked((int)inputBuffer.Size.ToUInt64());

				return checked((int)outputBuffer.Position.ToUInt64());
			}
			finally
			{
				inHandle.Free();
				outHandle.Free();
			}
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		protected override void Dispose(bool disposing)
		{
			if (Interlocked.Exchange(ref this.disposed, 1) == 0)
			{
				ZstdException.ThrowOnError(Native.ZSTD_freeDStream(this.zds));

				if (!this.leaveOpen)
				{
					this.inner.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		#endregion
	}
}
