﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace Iridescence.IO.Compression.Zstd
{
	[Serializable]
	public class ZstdException : IOException
	{
		public ZstdException()
		{
		}

		public ZstdException(string message)
			: base(message)
		{
		}

		public ZstdException(string message, Exception inner)
			: base(message, inner)
		{
		}

		protected ZstdException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}

		public static void ThrowOnError(UIntPtr code)
		{
			if (!Native.ZSTD_isError(code))
				return;

			string msg = Native.ZSTD_getErrorName(code);
			throw new ZstdException(msg);
		}
	}
}
