﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence.IO.Compression.Zstd
{
	/// <summary>
	/// Implements a Zstandard compression stream.
	/// </summary>
	public class ZstdEncoderStream : Stream
	{
		#region Fields

		private readonly Stream inner;
		private readonly bool leaveOpen;
		private readonly IntPtr zcs;
		
		private bool isInitialized;
		private readonly int compressionLevel;

		private readonly byte[] outBuffer;

		private int writing;
		private int disposed;

		#endregion

		#region Properties

		public override bool CanRead => false;

		public override bool CanSeek => false;

		public override bool CanWrite => this.inner.CanWrite;

		public override long Length => throw new NotSupportedException();

		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ZstdEncoderStream"/> class. 
		/// </summary>
		public ZstdEncoderStream(Stream inner, int compressionLevel, bool leaveOpen = false)
		{
			this.inner = inner ?? throw new ArgumentNullException(nameof(inner));
			
			this.zcs = Native.ZSTD_createCStream();
			if (this.zcs == IntPtr.Zero)
				throw new ZstdException("Could not create ZSTD_CStream.");

			this.compressionLevel = compressionLevel;
			this.leaveOpen = leaveOpen;

			int outSize = checked((int)Native.ZSTD_CStreamOutSize().ToUInt32());
			this.outBuffer = new byte[outSize];
		}

		#endregion

		#region Methods

		private void compressBegin()
		{
			if (Interlocked.CompareExchange(ref this.writing, 1, 0) != 0)
				throw new InvalidOperationException("Writing is not thread-safe.");
		}

		private void compressEnd()
		{
			if (Interlocked.Exchange(ref this.writing, 0) == 0)
				throw new InvalidOperationException("Writing is not thread-safe.");
		}

		private void init()
		{
			if (this.isInitialized)
				return;

			ZstdException.ThrowOnError(Native.ZSTD_initCStream(this.zcs, this.compressionLevel));
			this.isInitialized = true;
		}

		public bool TryInitialize()
		{
			if (this.isInitialized)
				return false;

			this.init();
			return true;
		}

		private void flushOutBuffer(UIntPtr len)
		{
			if (len == UIntPtr.Zero)
				return;

			this.inner.Write(this.outBuffer, 0, checked((int)len.ToUInt32()));
		}

		private async Task flushOutBufferAsync(UIntPtr len, CancellationToken cancellationToken)
		{
			if (len == UIntPtr.Zero)
				return;

			await this.inner.WriteAsync(this.outBuffer, 0, checked((int)len.ToUInt32()), cancellationToken);
		}
		
		public override unsafe void Flush()
		{
			this.compressBegin();
			try
			{
				this.init();

				fixed (byte* outPtr = this.outBuffer)
				{
					Native.Buffer outputBuffer;
					outputBuffer.Data = new IntPtr(outPtr);
					outputBuffer.Size = new UIntPtr((uint)this.outBuffer.Length);

					for (;;)
					{
						outputBuffer.Position = UIntPtr.Zero;

						UIntPtr remaining = Native.ZSTD_flushStream(this.zcs, ref outputBuffer);
						ZstdException.ThrowOnError(remaining);

						this.flushOutBuffer(outputBuffer.Position);

						if (remaining == UIntPtr.Zero)
							break;
					}
				}

				this.inner.Flush();
			}
			finally
			{
				this.compressEnd();
			}
		}

		public override async Task FlushAsync(CancellationToken cancellationToken)
		{
			this.compressBegin();
			try
			{
				this.init();

				GCHandle outHandle = GCHandle.Alloc(this.outBuffer, GCHandleType.Pinned);

				try
				{
					Native.Buffer outputBuffer;
					outputBuffer.Data = outHandle.AddrOfPinnedObject();
					outputBuffer.Size = new UIntPtr((uint)this.outBuffer.Length);

					bool canCancel = true;

					for (;;)
					{
						outputBuffer.Position = UIntPtr.Zero;

						UIntPtr remaining = Native.ZSTD_flushStream(this.zcs, ref outputBuffer);
						ZstdException.ThrowOnError(remaining);

						await this.flushOutBufferAsync(outputBuffer.Position, canCancel ? cancellationToken : CancellationToken.None);

						if (remaining == UIntPtr.Zero)
							break;

						canCancel = false;
					}
				}
				finally
				{
					outHandle.Free();
				}

				await this.inner.FlushAsync(cancellationToken);
			}
			finally
			{
				this.compressEnd();
			}
		}

		private unsafe void close()
		{
			this.init();

			fixed (byte* outPtr = this.outBuffer)
			{
				Native.Buffer outputBuffer;
				outputBuffer.Data = new IntPtr(outPtr);
				outputBuffer.Size = new UIntPtr((uint)this.outBuffer.Length);

				for (; ; )
				{
					outputBuffer.Position = UIntPtr.Zero;

					UIntPtr remaining = Native.ZSTD_endStream(this.zcs, ref outputBuffer);
					ZstdException.ThrowOnError(remaining);

					this.flushOutBuffer(outputBuffer.Position);

					if (remaining == UIntPtr.Zero)
						break;
				}
			}
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		public override unsafe void Write(byte[] buffer, int offset, int count)
		{
			this.compressBegin();

			try
			{
				if (buffer == null)
					throw new ArgumentNullException(nameof(buffer));

				if (offset < 0)
					throw new ArgumentOutOfRangeException(nameof(offset));

				if (count < 0)
					throw new ArgumentOutOfRangeException(nameof(count));

				if (offset + count > buffer.Length)
					throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

				this.init();

				if (count == 0)
					return;
			
				fixed (byte* inPtr = &buffer[offset])
				fixed (byte* outPtr = this.outBuffer)
				{
					Native.Buffer inputBuffer;
					inputBuffer.Data = new IntPtr(inPtr);
					inputBuffer.Size = new UIntPtr((uint)count);
					inputBuffer.Position = UIntPtr.Zero;

					Native.Buffer outputBuffer;
					outputBuffer.Data = new IntPtr(outPtr);
					outputBuffer.Size = new UIntPtr((uint)this.outBuffer.Length);

					while (inputBuffer.Position.ToUInt64() < inputBuffer.Size.ToUInt64())
					{
						outputBuffer.Position = UIntPtr.Zero;

						UIntPtr nextSize = Native.ZSTD_compressStream2(this.zcs, ref outputBuffer, ref inputBuffer, Native.EndDirective.Continue);
						ZstdException.ThrowOnError(nextSize);

						this.flushOutBuffer(outputBuffer.Position);
					}
				}
			}
			finally
			{
				this.compressEnd();
			}
		}

		public override async Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			this.compressBegin();

			try
			{
				if (buffer == null)
					throw new ArgumentNullException(nameof(buffer));

				if (offset < 0)
					throw new ArgumentOutOfRangeException(nameof(offset));

				if (count < 0)
					throw new ArgumentOutOfRangeException(nameof(count));

				if (offset + count > buffer.Length)
					throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

				this.init();

				if (count == 0)
					return;
			
				GCHandle inHandle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
				GCHandle outHandle = GCHandle.Alloc(this.outBuffer, GCHandleType.Pinned);

				try
				{
					Native.Buffer inputBuffer;
					inputBuffer.Data = inHandle.AddrOfPinnedObject() + offset;
					inputBuffer.Size = new UIntPtr((uint)count);
					inputBuffer.Position = UIntPtr.Zero;

					Native.Buffer outputBuffer;
					outputBuffer.Data = outHandle.AddrOfPinnedObject();
					outputBuffer.Size = new UIntPtr((uint)this.outBuffer.Length);

					bool canCancel = true;

					while (inputBuffer.Position.ToUInt64() < inputBuffer.Size.ToUInt64())
					{
						outputBuffer.Position = UIntPtr.Zero;

						UIntPtr nextSize = Native.ZSTD_compressStream2(this.zcs, ref outputBuffer, ref inputBuffer, Native.EndDirective.Continue);
						ZstdException.ThrowOnError(nextSize);

						await this.flushOutBufferAsync(outputBuffer.Position, canCancel ? cancellationToken : CancellationToken.None);
						canCancel = false;
					}
				}
				finally
				{
					inHandle.Free();
					outHandle.Free();
				}
			}
			finally
			{
				this.compressEnd();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (Interlocked.Exchange(ref this.disposed, 1) == 0)
			{
				if (disposing)
				{
					this.close();
				}

				ZstdException.ThrowOnError(Native.ZSTD_freeCStream(this.zcs));

				if (!this.leaveOpen)
				{
					this.inner.Dispose();
				}
			}
			
			base.Dispose(disposing);
		}

		#endregion
	}
}
