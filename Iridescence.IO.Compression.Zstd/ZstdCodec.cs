﻿using System;

namespace Iridescence.IO.Compression.Zstd
{
	/// <summary>
	/// Zstd helper class.
	/// </summary>
	public static class ZstdCodec
	{
		/// <summary>
		/// Gets the maximum compression level supported.
		/// </summary>
		public static int MaximumCompressionLevel => Native.ZSTD_maxCLevel();

		/// <summary>
		/// Gets the version of libzstd.
		/// </summary>
		public static uint Version => Native.ZSTD_versionNumber();

		/// <summary>
		/// Returns the worst-case maximum compressed size for data of the specified length.
		/// </summary>
		/// <param name="length"></param>
		/// <returns></returns>
		public static long CompressBound(long length)
		{
			if (length < 0)
				throw new ArgumentOutOfRangeException(nameof(length));

			return checked((long)Native.ZSTD_compressBound(new UIntPtr((ulong)length)).ToUInt64());
		}

		/// <summary>
		/// Compresses the source span into the destination span with the specified compression level.
		/// </summary>
		/// <param name="source">A source span.</param>
		/// <param name="dest">A destination span.</param>
		/// <param name="compressionLevel">The Zstd compression level.</param>
		/// <returns>The compressed size written to <paramref name="dest"/></returns>
		public static unsafe int Compress(ReadOnlySpan<byte> source, Span<byte> dest, int compressionLevel)
		{
			fixed (byte* sourcePtr = source)
			fixed (byte* destPtr = dest)
			{
				UIntPtr res = Native.ZSTD_compress(new IntPtr(destPtr), new UIntPtr((uint)dest.Length), new IntPtr(sourcePtr), new UIntPtr((uint)source.Length), compressionLevel);
				ZstdException.ThrowOnError(res);

				return checked((int)res.ToUInt64());
			}
		}

		/// <summary>
		/// Decompresses the source span into the destination span.
		/// </summary>
		/// <param name="source">A source span.</param>
		/// <param name="dest">A destination span.</param>
		/// <returns>The decompressed size written to <paramref name="dest"/></returns>
		public static unsafe int Decompress(ReadOnlySpan<byte> source, Span<byte> dest)
		{
			fixed (byte* sourcePtr = source)
			fixed (byte* destPtr = dest)
			{
				UIntPtr res = Native.ZSTD_decompress(new IntPtr(destPtr), new UIntPtr((uint)dest.Length), new IntPtr(sourcePtr), new UIntPtr((uint)source.Length));
				ZstdException.ThrowOnError(res);

				return checked((int)res.ToUInt64());
			}
		}
	}
}
