﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Iridescence.Composition
{
	/// <summary>
	/// Defines the properties of a composition part so that it can be matched on import.
	/// </summary>
	[Serializable]
	public class CompositionContract : IEquatable<CompositionContract>
	{
		#region Fields

		[NonSerialized]
		private ExportKey? key;
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the type of the part that is requested.
		/// </summary>
		public Type Type { get; private set; }

		/// <summary>
		/// Gets the contract metadata.
		/// </summary>
		public ImmutableArray<CompositionMetadata> Metadata { get; private set; }
		
		#endregion
		
		#region Constructors

		private CompositionContract(CompositionContract other)
		{
			this.Type = other.Type;
			this.Metadata = other.Metadata;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContract"/> class. 
		/// </summary>
		public CompositionContract(Type type, ImmutableArray<CompositionMetadata> metadata = default)
		{
			this.Type = type ?? throw new ArgumentNullException(nameof(type));

			if (!metadata.IsDefault)
				this.Metadata = metadata;
			else
				this.Metadata = ImmutableArray<CompositionMetadata>.Empty;

			if (this.Metadata.Any(m => m == null))
				throw new ArgumentException("Metadata can not be null", nameof(metadata));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContract"/> class. 
		/// </summary>
		public CompositionContract(Type type, params CompositionMetadata[] metadata)
			: this(type, metadata.ToImmutableArray())
		{

		}


		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContract"/> class. 
		/// </summary>
		public CompositionContract(Type type, string name, ImmutableArray<CompositionMetadata> metadata = default)
			: this(type, addName(metadata, name))
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContract"/> class. 
		/// </summary>
		public CompositionContract(Type type, string name, params CompositionMetadata[] metadata)
			: this(type, addName(metadata.ToImmutableArray(), name))
		{

		}

		#endregion
		
		#region Methods

		private static ImmutableArray<CompositionMetadata> addName(ImmutableArray<CompositionMetadata> metadata, string name)
		{
			if (name == null)
			{
				if (metadata.IsDefault)
					return ImmutableArray<CompositionMetadata>.Empty;

				return metadata;
			}

			NameMetadata nameMeta = new NameMetadata(name);

			if (metadata.IsDefault)
				return ImmutableArray.Create<CompositionMetadata>(nameMeta);

			return metadata.Insert(0, nameMeta);
		}

		/// <summary>
		/// Returns a value that indicates whether an object that matches this contract can be assigned to a variable of the specified type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public bool IsAssignableTo(Type type)
		{
			return type.IsAssignableFrom(this.Type);
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();
			str.Append(this.Type.GetDisplayName());

			foreach (CompositionMetadata metadata in this.Metadata)
			{
				str.Append(' ');
				str.Append(metadata);
			}

			return str.ToString();
		}

		/// <summary>
		/// Returns all metadata of the specified type.
		/// </summary>
		/// <typeparam name="T">The type of metadata to look for.</typeparam>
		/// <returns>A list of metadata that matches the type.</returns>
		public IEnumerable<T> GetMetadata<T>() where T : CompositionMetadata
		{
			return this.Metadata.OfType<T>();
		}

		/// <summary>
		/// Returns all metadata of the specified type and constructs a contract without the metadata returned from this method.
		/// </summary>
		/// <typeparam name="T">The type of metadata to look for.</typeparam>
		/// <param name="remainingContract">The contract without the metadata.</param>
		/// <returns>A list of metadata that matches the type.</returns>
		public ImmutableArray<T> ExtractMetadata<T>(out CompositionContract remainingContract) where T : CompositionMetadata
		{
			var matchingMetadata = ImmutableArray.CreateBuilder<T>();
			var remainingMetadata = ImmutableArray.CreateBuilder<CompositionMetadata>();

			foreach (CompositionMetadata metadata in this.Metadata)
			{
				if (metadata is T m)
				{
					matchingMetadata.Add(m);
				}
				else
				{
					remainingMetadata.Add(metadata);
				}
			}

			remainingContract = new CompositionContract(this)
			{
				Metadata = remainingMetadata.ToImmutable()
			};

			return matchingMetadata.ToImmutable();
		}
	
		/// <summary>
		/// Returns a contract with the same properties, but a different type.
		/// </summary>
		/// <param name="newType"></param>
		/// <returns></returns>
		public CompositionContract ChangeType(Type newType)
		{
			if (newType == null)
				throw new ArgumentNullException(nameof(newType));

			return new CompositionContract(this)
			{
				Type = newType
			};
		}


		/// <summary>
		/// Returns a contract with the same properties with additional metadata.
		/// </summary>
		/// <param name="metadata"></param>
		/// <returns></returns>
		public CompositionContract WithMetadata(CompositionMetadata metadata)
		{
			if (metadata == null)
				throw new ArgumentNullException(nameof(metadata));

			return new CompositionContract(this)
			{
				Metadata = this.Metadata.Add(metadata)
			};
		}

		/// <summary>
		/// Returns a contract with the same properties with additional metadata.
		/// </summary>
		/// <param name="metadata"></param>
		/// <returns></returns>
		public CompositionContract WithMetadata(params CompositionMetadata[] metadata)
		{
			if (metadata == null)
				throw new ArgumentNullException(nameof(metadata));

			if (metadata.Any(m => m == null))
				throw new ArgumentNullException(nameof(metadata));

			return new CompositionContract(this)
			{
				Metadata = this.Metadata.AddRange(metadata)
			};
		}

		/// <summary>
		/// Returns a <see cref="ExportKey"/> that identifies the exports of this contract in a cache.
		/// </summary>
		/// <returns></returns>
		public ExportKey GetKey()
		{
			if (!this.key.HasValue)
			{
				ExportKeyBuilder builder = new ExportKeyBuilder();
				builder.Add(typeof(CompositionContract), this.Type);

				foreach (CompositionMetadata metadata in this.Metadata)
				{
					metadata.AddToKey(builder);
				}

				this.key = builder.Build();
			}

			return this.key.Value;
		}

		public bool Equals(CompositionContract other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;

			if (this.Type != other.Type)
				return false;

			return this.Metadata.MetadataEquals(other.Metadata);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((CompositionContract)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.Type.GetHashCode();

				foreach (CompositionMetadata m in this.Metadata)
				{
					hashCode = (hashCode * 397) ^ m.GetHashCode();
				}

				return hashCode;
			}
		}

		#endregion
	}
}
