﻿using System;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Implements <see cref="ImportInfo"/> for a reflected property.
	/// </summary>
	public class PropertyImportInfo : AssignableImportInfo
	{
		#region Properties
		
		/// <summary>
		/// Gets the target property.
		/// </summary>
		public PropertyInfo Property { get; }

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyImportInfo"/> class. 
		/// </summary>
		public PropertyImportInfo(CompositionContract contract, bool isOptional, PropertyInfo property)
			: base(contract, isOptional, property)
		{
			if (property == null)
				throw new ArgumentNullException(nameof(property));

			if (!property.CanWrite)
				throw new ArgumentException($"Property \"{property}\" has no setter.", nameof(property));

			this.Property = property;
		}
		
		#endregion
		
		#region Methods

		public override string ToString()
		{
			return this.Property.ToString();
		}

		public override void Set(object target, object value)
		{
			this.Property.SetValue(target, value);
		}

		#endregion
	}
}
