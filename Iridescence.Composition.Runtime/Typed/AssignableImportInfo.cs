﻿namespace Iridescence.Composition
{
	public abstract class AssignableImportInfo : ImportInfo
	{
		#region Constructors

		protected AssignableImportInfo(CompositionContract contract, bool isOptional, object target)
			: base(contract, isOptional, target)
		{
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sets the imported value on a target object instance.
		/// </summary>
		/// <param name="target">The target object.</param>
		/// <param name="value">The imported value.</param>
		public abstract void Set(object target, object value);

		#endregion
	}
}