﻿using System;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Implements <see cref="ImportInfo"/> for a reflected field.
	/// </summary>
	public class FieldImportInfo : AssignableImportInfo
	{
		#region Properties
		
		/// <summary>
		/// Gets the target field.
		/// </summary>
		public FieldInfo Field { get; }

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FieldImportInfo"/> class. 
		/// </summary>
		public FieldImportInfo(CompositionContract contract, bool isOptional, FieldInfo field)
			: base(contract, isOptional, field)
		{
			this.Field = field ?? throw new ArgumentNullException(nameof(field));
		}
		
		#endregion
		
		#region Methods

		public override string ToString()
		{
			return this.Field.ToString();
		}

		public override void Set(object target, object value)
		{
			this.Field.SetValue(target, value);
		}

		#endregion
	}
}
