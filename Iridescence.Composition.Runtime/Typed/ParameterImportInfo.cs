﻿using System;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Implements <see cref="ImportInfo"/> for a parameter of a method.
	/// </summary>
	public class ParameterImportInfo : ImportInfo
	{	
		#region Properties
		
		/// <summary>
		/// Gets the parameter.
		/// </summary>
		public ParameterInfo Parameter { get; }

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ParameterImportInfo"/> class. 
		/// </summary>
		public ParameterImportInfo(CompositionContract contract, bool isOptional, ParameterInfo parameter)
			: base(contract, isOptional, parameter)
		{
			this.Parameter = parameter ?? throw new ArgumentNullException(nameof(parameter));
		}
		
		#endregion
		
		#region Methods

		public override string ToString()
		{
			return this.Parameter.ToString();
		}

		#endregion
	}
}
