﻿using System;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides information about a typed export.
	/// </summary>
	public readonly struct ExportInfo
	{
		#region Fields

		private readonly Type type;
		private readonly ImmutableArray<CompositionMetadata> metadata;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the type of the export.
		/// </summary>
		public Type Type => this.type ?? typeof(object);

		/// <summary>
		/// Gets the composition metadata of the export.
		/// </summary>
		public ImmutableArray<CompositionMetadata> Metadata => this.metadata.IsDefault ? ImmutableArray<CompositionMetadata>.Empty : this.metadata;

		/// <summary>
		/// Gets a value that indicates whether the export is shared using the <see cref="ISharingManager"/>.
		/// </summary>
		public bool IsShared { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ExportInfo"/> class. 
		/// </summary>
		public ExportInfo(Type type, ImmutableArray<CompositionMetadata> metadata, bool isShared)
		{
			this.type = type ?? throw new ArgumentNullException(nameof(type));

			if (metadata.IsDefault)
				throw new ArgumentNullException(nameof(metadata));

			this.metadata = metadata;
			this.IsShared = isShared;
		}

		#endregion

		#region Methods

		#endregion
	}
}
