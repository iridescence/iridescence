﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents a composition target which provides a <see cref="CompositionContract"/> and the means to set it on an object instance.
	/// </summary>
	public abstract class ImportInfo
	{
		#region Fields

		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the import contract.
		/// </summary>
		public CompositionContract Contract { get; }

		/// <summary>
		/// Gets a value that indicates whether this import is optional (i.e. will not be set if no export matches).
		/// </summary>
		public bool IsOptional { get; }

		/// <summary>
		/// Gets the target object. Used to tell whether one target has multiple imports.
		/// </summary>
		public object Target { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportInfo"/> class. 
		/// </summary>
		protected ImportInfo(CompositionContract contract, bool isOptional, object target)
		{
			this.Contract = contract ?? throw new ArgumentNullException(nameof(contract));
			this.IsOptional = isOptional;
			this.Target = target ?? throw new ArgumentNullException(nameof(target));
		}

		#endregion

		#region Methods

		#endregion
	}
}
