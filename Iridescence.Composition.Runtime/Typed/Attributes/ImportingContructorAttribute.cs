﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks the contructor of a class as importing constructor.
	/// </summary>
	[AttributeUsage(AttributeTargets.Constructor)]
	public sealed class ImportingConstructorAttribute : Attribute
	{

	}
}
