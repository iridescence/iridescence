﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks an field, property or parameter as "multi" import.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public class ImportMultipleAttribute : Attribute, IImportAttribute, ICompositionMetadataAttribute
	{
		#region Properties

		public Type Type { get; }

		/// <summary>
		/// Gets the names.
		/// </summary>
		public IReadOnlyList<string> Names { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportMultipleAttribute"/> class. 
		/// </summary>
		public ImportMultipleAttribute(params string[] names)
			: this(null, names)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportMultipleAttribute"/> class. 
		/// </summary>
		public ImportMultipleAttribute(Type type, params string[] names)
		{
			this.Type = type;
			this.Names = names;
		}

		#endregion

		#region Methods

		public virtual IEnumerable<CompositionMetadata> GetMetadata(ICustomAttributeProvider target)
		{
			yield return new ImportMultipleMetadata(this.Names);
		}

		public virtual IEnumerable<ImportInfo> GetImports(ICustomAttributeProvider target, Lazy<ExportInfo> export)
		{
			yield return target.GetImportInfo(null, false, this.Type);
		}

		#endregion
	}
}
