﻿using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// An attribute interface that attaches metadata to an import.
	/// </summary>
	public interface ICompositionMetadataAttribute
	{
		/// <summary>
		/// Gets the <see cref="CompositionMetadata"/> attached to target.
		/// </summary>
		/// <param name="target">The thing this attribute is attached to.</param>
		/// <returns></returns>
		IEnumerable<CompositionMetadata> GetMetadata(ICustomAttributeProvider target);
	}
}
