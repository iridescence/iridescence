﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents a simple export attribute.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Parameter, AllowMultiple = true, Inherited = false)]
	public class ExportAttribute : Attribute, IExportAttribute
	{
		#region Fields

		private NameMetadata nameMetadata;

		#endregion
		
		#region Properties
		
		/// <summary>
		/// Gets the name under which to export the part.
		/// Can be <see langword="null"/> to export a part with the default name.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets the type of the export.
		/// Can be <see langword="null"/> to export the part using the type of the element this attribute is attached to.
		/// </summary>
		public Type Type { get; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ExportAttribute"/> class. 
		/// </summary>
		public ExportAttribute()
		{
		
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ExportAttribute"/> class. 
		/// </summary>
		public ExportAttribute(string name)
		{
			this.Name = name;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ExportAttribute"/> class. 
		/// </summary>
		public ExportAttribute(Type type)
		{
			this.Type = type;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ExportAttribute"/> class. 
		/// </summary>
		public ExportAttribute(string name, Type type)
		{
			this.Name = name;
			this.Type = type;
		}
		
		#endregion
		
		#region Methods

		public IEnumerable<ExportInfo> GetExports(ICustomAttributeProvider target, Lazy<CompositionContract> importContract)
		{
			ImmutableArray<CompositionMetadata> metadata = ExportMetadataCache.GetMetadata(target, this, out bool isShared);

			if (this.Name != null)
			{
				// Cache NameMetadata instance.
				if (this.nameMetadata == null)
					this.nameMetadata = new NameMetadata(this.Name);

				metadata = metadata.Add(this.nameMetadata);
			}

			switch (target)
			{
				case Type type:
					yield return new ExportInfo(this.Type ?? type, metadata, isShared);
					break;
				case FieldInfo field:
					yield return new ExportInfo(this.Type ?? field.FieldType, metadata, isShared);
					break;
				case PropertyInfo property:
					yield return new ExportInfo(this.Type ?? property.PropertyType, metadata, isShared);
					break;
				default:
					throw new NotSupportedException("The target is not supported.");
			}
		}

		#endregion
	}
}
