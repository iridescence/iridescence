﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks an field, property or parameter as "multi" import, specified a format string that is used to systematically name the parts.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public class ImportMultipleFormatAttribute : Attribute, IImportAttribute, ICompositionMetadataAttribute
	{
		#region Properties

		public Type Type { get; }

		public string FormatString { get; }

		public int StartIndex { get; }

		public int Count { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportMultipleFormatAttribute"/> class. 
		/// </summary>
		public ImportMultipleFormatAttribute(string formatString, int startIndex, int count)
			: this(null, formatString, startIndex, count)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportMultipleFormatAttribute"/> class. 
		/// </summary>
		public ImportMultipleFormatAttribute(Type type, string formatString, int startIndex, int count)
		{
			this.Type = type;
			this.FormatString = formatString;
			this.StartIndex = startIndex;
			this.Count = count;
		}

		#endregion

		#region Methods

		public virtual IEnumerable<CompositionMetadata> GetMetadata(ICustomAttributeProvider target)
		{
			yield return new ImportMultipleMetadata(Enumerable.Range(this.StartIndex, this.Count).Select(i => string.Format(this.FormatString, i)));
		}

		public virtual IEnumerable<ImportInfo> GetImports(ICustomAttributeProvider target, Lazy<ExportInfo> export)
		{
			yield return target.GetImportInfo(null, false, this.Type);
		}

		#endregion
	}

}
