﻿using System;
using System.Collections.Immutable;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Iridescence.Composition
{
	internal static class ExportMetadataCache
	{
		private static readonly ConditionalWeakTable<ICustomAttributeProvider, CacheEntry> cache = new ConditionalWeakTable<ICustomAttributeProvider, CacheEntry>();

		public static ImmutableArray<CompositionMetadata> GetMetadata(ICustomAttributeProvider target, object context, out bool isShared)
		{
			if (target == null)
				throw new ArgumentNullException(nameof(target));

			if (context == null)
				throw new ArgumentNullException(nameof(context));

			CacheEntry entry = cache.GetOrCreateValue(target);
			CachePair p = default;

			lock (entry)
			{
				if (!entry.Pairs.IsDefaultOrEmpty)
				{
					foreach (CachePair p2 in entry.Pairs)
					{
						if (ReferenceEquals(p2.Context, context))
						{
							p = p2;
							break;
						}
					}
				}

				if (p.Context == null)
				{
					p = new CachePair(context,
						target.GetCompositionMetadata().ToImmutableArray(),
						target.IsDefined(typeof(SharedAttribute), false));

					entry.Pairs = entry.Pairs.IsDefault ? ImmutableArray.Create(p) : entry.Pairs.Add(p);
				}
			}

			isShared = p.IsShared;
			return p.Metadata;
		}

		private readonly struct CachePair
		{
			public readonly object Context;
			public readonly ImmutableArray<CompositionMetadata> Metadata;
			public readonly bool IsShared;

			public CachePair(object context, ImmutableArray<CompositionMetadata> metadata, bool isShared)
			{
				this.Context = context;
				this.Metadata = metadata;
				this.IsShared = isShared;
			}
		}

		private sealed class CacheEntry
		{
			public ImmutableArray<CachePair> Pairs;
		}
	}
}