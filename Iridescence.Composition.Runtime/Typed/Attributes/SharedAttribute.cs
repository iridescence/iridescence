﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks an export as shared.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class SharedAttribute : Attribute
	{

	}
}
