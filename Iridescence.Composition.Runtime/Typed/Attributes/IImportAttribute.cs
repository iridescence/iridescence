﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Base import attribute.
	/// </summary>
	public interface IImportAttribute 
	{
		#region Methods

		/// <summary>
		/// Returns the <see cref="ImportInfo">contracts</see> for an import.
		/// </summary>
		/// <param name="target">The thing this attribute is attached to.</param>
		/// <param name="export">The export context. Only retrieve the value if the contracts returned by this method are dependent on this parameter.</param>
		/// <returns>An enumeration of imports.</returns>
		IEnumerable<ImportInfo> GetImports(ICustomAttributeProvider target, Lazy<ExportInfo> export);

		#endregion
	}
}
