﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks an field, property or parameter as "multi" import with a naming pattern.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public class ImportRangeAttribute : Attribute, IImportAttribute, ICompositionMetadataAttribute
	{
		#region Properties

		/// <summary>
		/// Gets the names.
		/// </summary>
		public string Format { get; }

		/// <summary>
		/// Gets the number of parts to import.
		/// </summary>
		public int Count { get; }

		/// <summary>
		/// Gets the start index.
		/// </summary>
		public int StartIndex { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportRangeAttribute"/> class. 
		/// </summary>
		public ImportRangeAttribute(string format, int count)
		{
			this.Format = format ?? throw new ArgumentNullException(nameof(format));
			this.Count = count;
		}

		#endregion

		#region Methods

		public virtual IEnumerable<CompositionMetadata> GetMetadata(ICustomAttributeProvider target)
		{
			yield return new ImportMultipleMetadata(Enumerable.Range(this.StartIndex, this.Count).Select(i => string.Format(this.Format, i)));
		}

		public virtual IEnumerable<ImportInfo> GetImports(ICustomAttributeProvider target, Lazy<ExportInfo> export)
		{
			yield return target.GetImportInfo(null, false);
		}

		#endregion
	}
}
