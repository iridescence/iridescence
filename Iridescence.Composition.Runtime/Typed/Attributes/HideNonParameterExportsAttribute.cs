﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Instructs the composition engine to hide all other (non-parameter) exports for the same contract when a requested part matches the parameter this attribute is applied to.
	/// </summary>
	[AttributeUsage(AttributeTargets.Parameter)]
	public class HideNonParameterExportsAttribute : Attribute
	{
		
	}
}
