﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks a method as import callback. The method is run when all imports have been successfully assigned.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class OnImportedAttribute : Attribute
	{

	}
}
