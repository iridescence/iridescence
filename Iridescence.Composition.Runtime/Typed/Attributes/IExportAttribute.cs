﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Base export attribute.
	/// </summary>
	public interface IExportAttribute
	{
		#region Methods

		/// <summary>
		/// Returns the <see cref="ExportInfo">infos</see> for an export.
		/// </summary>
		/// <param name="target">The thing this attribute is attached to.</param>
		/// <param name="importContract">The import contract. Only retrieve the value if the contracts returned by this method are dependent on this parameter.</param>
		/// <returns>The <see cref="CompositionContract">export contracts</see>.</returns>
		IEnumerable<ExportInfo> GetExports(ICustomAttributeProvider target, Lazy<CompositionContract> importContract);

		#endregion
	}
}
