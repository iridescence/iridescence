﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks an field, property or parameter as "many" import.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public class ImportManyAttribute : ImportAttribute, ICompositionMetadataAttribute
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportManyAttribute"/> class. 
		/// </summary>
		public ImportManyAttribute()
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportManyAttribute"/> class. 
		/// </summary>
		public ImportManyAttribute(string name)
			: base(name)
		{

		}

		#endregion

		#region Methods

		public IEnumerable<CompositionMetadata> GetMetadata(ICustomAttributeProvider target)
		{
			yield return ImportManyMetadata.Instance;
		}
		
		#endregion
	}
}
