﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	public static class ImportReflectionUtility
	{
		/// <summary>
		/// Gets the metadata applied to the specified member.
		/// </summary>
		/// <param name="provider"></param>
		/// <returns></returns>
		public static IEnumerable<CompositionMetadata> GetCompositionMetadata(this ICustomAttributeProvider provider)
		{
			return GetCompositionMetadata(provider.GetCustomAttributes(true).OfType<ICompositionMetadataAttribute>(), provider);
		}

		/// <summary>
		/// Gets the metadata from the specified <see cref="ICompositionMetadataAttribute">attributes</see>.
		/// </summary>
		/// <param name="metadataAttributes"></param>
		/// <param name="target"></param>
		/// <returns></returns>
		public static IEnumerable<CompositionMetadata> GetCompositionMetadata(IEnumerable<ICompositionMetadataAttribute> metadataAttributes, ICustomAttributeProvider target)
		{
			foreach (ICompositionMetadataAttribute metadataAttr in metadataAttributes)
			{
				foreach (CompositionMetadata metadata in metadataAttr.GetMetadata(target))
				{
					yield return metadata;
				}
			}
		}

		/// <summary>
		/// Returns the <see cref="ImportInfo"/> for the specified target.
		/// </summary>
		/// <param name="target">The target of the import.</param>
		/// <param name="name">The contract name.</param>
		/// <param name="isOptional">Whether this import is optional.</param>
		/// <param name="type">A type override. If not specified, the target type is used.</param>
		/// <returns></returns>
		public static ImportInfo GetImportInfo(this ICustomAttributeProvider target, string name, bool isOptional, Type type = null)
		{
			switch (target)
			{
				case FieldInfo field:
					return new FieldImportInfo(new CompositionContract(type ?? field.FieldType, name, field.GetCompositionMetadata().ToImmutableArray()), isOptional, field);
				case PropertyInfo property:
					return new PropertyImportInfo(new CompositionContract(type ?? property.PropertyType, name, property.GetCompositionMetadata().ToImmutableArray()), isOptional, property);
				case ParameterInfo parameter:
					return new ParameterImportInfo(new CompositionContract(type ?? parameter.ParameterType, name, parameter.GetCompositionMetadata().ToImmutableArray()), isOptional, parameter);
				default:
					throw new NotSupportedException("The target is not supported.");
			}
		}
	}
}
