﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks a field, property or parameter as composition import.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public class ImportAttribute : Attribute, IImportAttribute
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the name of the part to import.
		/// Can be <see langword="null"/> to import a part with the default name.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets or sets a value that determines whether the import is optional and can be left out if no export matches the import contract.
		/// </summary>
		public bool IsOptional { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportAttribute"/> class. 
		/// </summary>
		public ImportAttribute()
		{

		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ImportAttribute"/> class. 
		/// </summary>
		public ImportAttribute(string name)
		{
			this.Name = name;
		}

		#endregion

		#region Methods

		public virtual IEnumerable<ImportInfo> GetImports(ICustomAttributeProvider target, Lazy<ExportInfo> export)
		{
			yield return target.GetImportInfo(this.Name, this.IsOptional);
		}

		#endregion
	}
}
