﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks an interface as dynamically implementable export factory.
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface, AllowMultiple = false)]
	public class ExportFactoryAttribute : Attribute
	{
		#region Fields

		#endregion

		#region Properties

		/*
		/// <summary>
		/// Gets the type of the part created through the interface. Inferred from the creation method if null. 
		/// </summary>
		public Type PartType { get; }
		*/

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ExportFactoryAttribute"/> class. 
		/// </summary>
		public ExportFactoryAttribute(/*Type partType = null*/)
		{
			/*this.PartType = partType;*/
		}

		#endregion

		#region Methods

		#endregion
	}
}
