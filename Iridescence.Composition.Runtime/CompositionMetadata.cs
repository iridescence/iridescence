﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents abstract composition metadata.
	/// </summary>
	[Serializable]
	public abstract class CompositionMetadata
	{
		/// <summary>
		/// Adds this metadata to a <see cref="ExportKeyBuilder"/>.
		/// </summary>
		/// <param name="builder"></param>
		public virtual void AddToKey(ExportKeyBuilder builder)
		{
			builder.Add(this.GetType(), this);
		}

		public override string ToString()
		{
			string name = this.GetType().Name;
			if (name.EndsWith("Metadata"))
				return name.Substring(0, name.Length - 8);

			return name;
		}
	}
}

