﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace Iridescence.Composition
{
	/// <summary>
	/// Attaches a name to a part.
	/// </summary>
	[Serializable]
	public class NameMetadata : CompositionMetadata, IEquatable<NameMetadata>
	{
		#region Properties
		
		/// <summary>
		/// Gets the name.
		/// </summary>
		public string Name { get; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="NameMetadata"/> class. 
		/// </summary>
		public NameMetadata(string name)
		{
			this.Name = name ?? throw new ArgumentNullException(nameof(name));
		}
		
		#endregion
		
		#region Methods

		public override int GetHashCode()
		{
			return (this.Name != null ? this.Name.GetHashCode() : 0);
		}

		public bool Equals(NameMetadata other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return string.Equals(this.Name, other.Name);
		}

		public override bool Equals(object obj)
		{
			if (!(obj is NameMetadata other)) return false;
			return this.Equals(other);
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			str.Append("Name=");

			if (this.Name == null)
			{
				str.Append("null");
			}
			else
			{
				str.Append('"');
				str.Append(this.Name);
				str.Append('"');
			}

			return str.ToString();
		}

		#endregion
	}

	public static class NameMetadataExtensions
	{
		/// <summary>
		/// Returns the name assigned to a <see cref="CompositionContract"/> by calling <see cref="CompositionContract.ExtractMetadata{T}"/>.
		/// If there are multiple <see cref="NameMetadata"/> instances attached to the contract, the last one is used.
		/// </summary>
		/// <param name="contract"></param>
		/// <param name="name"></param>
		/// <param name="remainingContract"></param>
		/// <returns></returns>
		public static bool TryExtractName(this CompositionContract contract, out string name, out CompositionContract remainingContract)
		{
			IReadOnlyList<NameMetadata> meta = contract.ExtractMetadata<NameMetadata>(out remainingContract);
			if (meta.Count == 0)
			{
				name = null;
				return false;
			}

			name = meta.Last().Name;
			return true;
		}

		/// <summary>
		/// Returns the name assigned to a <see cref="CompositionContract"/> by calling <see cref="CompositionContract.GetMetadata{T}"/>.
		/// If there are multiple <see cref="NameMetadata"/> instances attached to the contract, the last one is used.
		/// </summary>
		/// <param name="contract"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public static bool TryGetName(this CompositionContract contract, out string name)
		{
			return contract.Metadata.TryGetName(out name);
		}

		/// <summary>
		/// Returns the name metadata from an array of <see cref="CompositionMetadata"/>.
		/// </summary>
		/// <param name="metadata"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public static bool TryGetName(this ImmutableArray<CompositionMetadata> metadata, out string name)
		{
			NameMetadata meta = metadata.OfType<NameMetadata>().LastOrDefault();
			if (meta == null)
			{
				name = null;
				return false;
			}

			name = meta.Name;
			return true;
		}
	}
}
