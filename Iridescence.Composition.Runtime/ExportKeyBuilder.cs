﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	/// <summary>
	/// Builds a <see cref="ExportKey"/>.
	/// </summary>
	public class ExportKeyBuilder
	{
		#region Fields

		private readonly List<(object, object)> data;
		private HashSet<object> maskedKeys;

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ExportKeyBuilder"/> class. 
		/// </summary>
		public ExportKeyBuilder()
		{
			this.data = new List<(object, object)>();
		}
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Adds an entry to the composition key builder.
		/// </summary>
		/// <param name="key">The key of the item. Has no effect on the resulting <see cref="ExportKey"/> and is only used to supported masking.</param>
		/// <param name="value">The value of the item. This is used to build the <see cref="ExportKey"/> if it was not masked.</param>
		public void Add(object key, object value)
		{
			this.data.Add((key, value));
		}

		/// <summary>
		/// Masks a entry key.
		/// </summary>
		/// <param name="key"></param>
		public void Mask(object key)
		{
			this.maskedKeys ??= new HashSet<object>();
			this.maskedKeys.Add(key);
		}

		/// <summary>
		/// Returns an <see cref="ExportKey"/> built from this instance.
		/// </summary>
		/// <returns></returns>
		public ExportKey Build()
		{
			var builder = ImmutableArray.CreateBuilder<object>();

			HashCode hash = new HashCode();
			
			foreach ((object key, object value) in this.data)
			{
				if (this.maskedKeys != null && this.maskedKeys.Contains(key))
					continue;

				if (value != null)
					hash.Add(value);
				else
					hash.Add(-1);
				
				builder.Add(value);
			}

			builder.Sort((a, b) =>
			{
				int h1 = a?.GetHashCode() ?? 0;
				int h2 = b?.GetHashCode() ?? 0;
				return h1.CompareTo(h2);
			});

			return new ExportKey(builder.ToImmutable(), hash.ToHashCode());
		}

		#endregion
	}
}
