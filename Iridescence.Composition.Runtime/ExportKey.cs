﻿using System;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents a caching key for composition parts that uniquely identifies a part even though the contracts for it might have been different.
	/// </summary>
	public readonly struct ExportKey : IEquatable<ExportKey>
	{
		#region Fields

		private readonly int hash;
		private readonly ImmutableArray<object> data;

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ExportKey"/> class. 
		/// </summary>
		internal ExportKey(ImmutableArray<object> data, int hash)
		{
			this.data = data;
			this.hash = hash;
		}
		
		#endregion
		
		#region Methods

		[Pure]
		public bool Equals(ExportKey other)
		{
			if (other.data.Length != this.data.Length)
				return false;

			for (int i = 0; i < this.data.Length; ++i)
			{
				if(!object.Equals(this.data[i], other.data[i]))
					return false;
			}

			return true;
		}

		[Pure]
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return this.Equals((ExportKey)obj);
		}

		[Pure]
		public override int GetHashCode()
		{
			return this.hash;
		}

		[Pure]
		public override string ToString()
		{
			return string.Join(", ", this.data);
		}

		#endregion
	}
}
