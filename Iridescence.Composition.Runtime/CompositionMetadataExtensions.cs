﻿using System.Collections.Generic;

namespace Iridescence.Composition
{
	public static class CompositionMetadataExtensions
	{
		/// <summary>
		/// Checks if two lists of <see cref="CompositionMetadata"/> match.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static bool MetadataEquals(this IReadOnlyList<CompositionMetadata> a, IReadOnlyList<CompositionMetadata> b)
		{
			if (a.Count != b.Count)
				return false;

			HashSet<CompositionMetadata> meta = new HashSet<CompositionMetadata>(a);
			foreach (CompositionMetadata m in b)
			{
				if (!meta.Remove(m))
					return false;
			}

			return meta.Count == 0;
		}
	}
}