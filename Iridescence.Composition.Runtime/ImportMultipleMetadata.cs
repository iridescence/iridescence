﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Composition
{
	/// <summary>
	/// Multi-import metadata that specifies the names of the parts to import.
	/// </summary>
	[Serializable]
	public class ImportMultipleMetadata : CompositionMetadata, IEquatable<ImportMultipleMetadata>
	{
		#region Properties
		
		/// <summary>
		/// Gets the names of the elements.
		/// </summary>
		public IEnumerable<string> Names { get; }

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ImportMultipleMetadata"/> class. 
		/// </summary>
		public ImportMultipleMetadata(IEnumerable<string> names)
		{
			this.Names = names ?? throw new ArgumentNullException(nameof(names));
		}

		#endregion

		#region Methods

		public bool Equals(ImportMultipleMetadata other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Names.SequenceEqual(other.Names);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((ImportMultipleMetadata)obj);
		}

		public override int GetHashCode()
		{
			HashCode h = new HashCode();

			foreach (string str in this.Names)
			{
				h.Add(str);
			}

			return h.ToHashCode();
		}

		#endregion
	}
}
