﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Marks an import contract as "many".
	/// </summary>
	[Serializable]
	public class ImportManyMetadata : CompositionMetadata, IEquatable<ImportManyMetadata>
	{
		#region Fields

		public static readonly ImportManyMetadata Instance = new ImportManyMetadata();
		
		#endregion

		#region Properties

		#endregion

		#region Constructors

		#endregion

		#region Methods

		public bool Equals(ImportManyMetadata other)
		{
			return other != null && other.GetType() == this.GetType();
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((ImportManyMetadata)obj);
		}

		public override int GetHashCode()
		{
			return 1;
		}

		#endregion
	}
}
