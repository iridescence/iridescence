using System.Runtime.CompilerServices;
using Iridescence.Math;
using Iridescence.Physics2D.Dynamics;

namespace Iridescence.Physics2D
{
	/// <summary>
	/// Math helper functions.
	/// </summary>
	public static class MathHelper
	{
		#region Methods

		/// <summary>
		/// Friction mixing law. The idea is to allow either fixture to drive the friction to zero.
		/// For example, anything slides on ice.
		/// </summary>
		/// <param name="friction1"></param>
		/// <param name="friction2"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float MixFriction(float friction1, float friction2)
		{
			return (float)System.Math.Sqrt(friction1 * friction2);
		}

		/// <summary>
		/// Restitution mixing law. The idea is allow for anything to bounce off an inelastic surface.
		/// For example, a superball bounces on anything.
		/// </summary>
		/// <param name="restitution1"></param>
		/// <param name="restitution2"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float MixRestitution(float restitution1, float restitution2)
		{
			return restitution1 > restitution2 ? restitution1 : restitution2;
		}

		public static bool IsValid(Rotation rot)
		{
			return rot.C >= -1.0f && rot.C <= 1.0 && rot.S >= -1.0f && rot.S <= 1.0f;
		}

		public static bool IsValid(Transform transform)
		{
			return transform.P.IsFinite && IsValid(transform.Q);
		}
		
		public static bool IsValid(BodyState state)
		{
			return state.Position.IsFinite && state.Angle.IsFinite() && state.LinearVelocity.IsFinite && state.AngularVelocity.IsFinite();
		}

		/// <summary>
		/// Returns the inverse of this matrix as a 2-by-2.
		/// Returns the zero matrix if singular.
		/// </summary>
		/// <param name="matrix"></param>
		/// <param name="M"></param>
		public static void GetInverse2x2(ref Matrix3x3 matrix, out Matrix3x3 M)
		{
			float a = matrix.M11;
			float b = matrix.M12;
			float c = matrix.M21;
			float d = matrix.M22;
			float det = a * d - b * c;
			if (det != 0.0f)
				det = 1.0f / det;

			M.M11 = det * d;
			M.M12 = -det * b;
			M.M31 = 0.0f;
			M.M21 = -det * c;
			M.M22 = det * a;
			M.M32 = 0.0f;
			M.M13 = 0.0f;
			M.M23 = 0.0f;
			M.M33 = 0.0f;
		}

		/// <summary>
		/// Returns the symmetric inverse of this matrix as a 3-by-3.
		/// Returns the zero matrix if singular.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="M"></param>
		public static void GetSymInverse3x3(ref Matrix3x3 a, out Matrix3x3 M)
		{
			Vector3 r1, r2, r3;
			r1.X = a.M11;
			r1.Y = a.M21;
			r1.Z = a.M31;
			r2.X = a.M12;
			r2.Y = a.M22;
			r2.Z = a.M32;
			r3.X = a.M13;
			r3.Y = a.M23;
			r3.Z = a.M33;

			Vector3.Cross(in r2, in r3, out Vector3 cross);
			float det = Vector3.Dot(r1, cross);
			if (det != 0.0f)
				det = 1.0f / det;

			float a11 = r1.X, a12 = r2.X, a13 = r3.X;
			float a22 = r2.Y, a23 = r3.Y;
			float a33 = r3.Z;

			M.M11 = det * (a22 * a33 - a23 * a23);
			M.M21 = det * (a13 * a23 - a12 * a33);
			M.M31 = det * (a12 * a23 - a13 * a22);

			M.M12 = M.M21;
			M.M22 = det * (a11 * a33 - a13 * a13);
			M.M32 = det * (a13 * a12 - a11 * a23);

			M.M13 = M.M31;
			M.M23 = M.M32;
			M.M33 = det * (a11 * a22 - a12 * a12);
		}
		
		#endregion
	}
}
