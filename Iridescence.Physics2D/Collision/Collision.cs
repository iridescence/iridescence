﻿using System;
using Iridescence.Math;
using Iridescence.Physics2D.Collision.Shapes;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// Structures and functions used for computing contact points, distance
	/// queries, and TOI queries.
	/// </summary>
	public static partial class Collisions
	{
		#region Methods

		/// <summary>
		/// Compute the point states given two manifolds. The states pertain to the transition from manifold1
		/// to manifold2. So state1 is either persist or remove while state2 is either add or persist.
		/// </summary>
		/// <param name="state1"></param>
		/// <param name="state2"></param>
		/// <param name="manifold1"></param>
		/// <param name="manifold2"></param>
		public static void GetPointStates(Span<PointState> state1, Span<PointState> state2, Manifold manifold1, Manifold manifold2)
		{
			state1.Clear();
			state2.Clear();
			
			// Detect persists and removes.
			for (int i = 0; i < manifold1.Points.CountInternal; ++i)
			{
				ContactFeature id = manifold1.Points[i].Id;

				state1[i] = PointState.Remove;

				for (int j = 0; j < manifold2.Points.CountInternal; ++j)
				{
					if (manifold2.Points[j].Id.Equals(in id))
					{
						state1[i] = PointState.Persist;
						break;
					}
				}
			}

			// Detect persists and adds.
			for (int i = 0; i < manifold2.Points.CountInternal; ++i)
			{
				ContactFeature id = manifold2.Points[i].Id;

				state2[i] = PointState.Add;

				for (int j = 0; j < manifold1.Points.CountInternal; ++j)
				{
					if (manifold1.Points[j].Id.Equals(in id))
					{
						state2[i] = PointState.Persist;
						break;
					}
				}
			}
		}

		/// <summary>
		/// Clipping for contact manifolds.
		/// </summary>
		/// <param name="vOut"></param>
		/// <param name="vIn"></param>
		/// <param name="normal"></param>
		/// <param name="offset"></param>
		/// <param name="vertexIndexA"></param>
		/// <returns></returns>
		internal static int ClipSegmentToLine(ref Array2<ClipVertex> vOut, in Array2<ClipVertex> vIn, in Vector2 normal, float offset, byte vertexIndexA)
		{
			// Start with no output points
			int numOut = 0;

			Vector2 v0 = vIn.A.Vertex;
			Vector2 v1 = vIn.B.Vertex;

			// Calculate the distance of end points to the line
			float distance0 = normal.X * v0.X + normal.Y * v0.Y - offset;
			float distance1 = normal.X * v1.X + normal.Y * v1.Y - offset;

			// If the points are behind the plane
			if (distance0 <= 0.0f) vOut[numOut++] = vIn[0];
			if (distance1 <= 0.0f) vOut[numOut++] = vIn[1];

			// If the points are on different sides of the plane
			if (distance0 * distance1 < 0.0f)
			{
				// Find intersection point of edge and plane
				float interp = distance0 / (distance0 - distance1);

				ClipVertex o;
				o.Vertex.X = v0.X + interp * (v1.X - v0.X);
				o.Vertex.Y = v0.Y + interp * (v1.Y - v0.Y);

				// VertexA is hitting edgeB.
				o.Feature.Hash = 0;
				o.Feature.IndexA = vertexIndexA;
				o.Feature.IndexB = vIn[0].Feature.IndexB;
				o.Feature.TypeA = ContactFeatureType.Vertex;
				o.Feature.TypeB = ContactFeatureType.Face;

				vOut[numOut] = o;

				++numOut;
			}

			return numOut;
		}

		/// <summary>
		/// Determine if two generic shapes overlap.
		/// </summary>
		/// <param name="shapeA"></param>
		/// <param name="indexA"></param>
		/// <param name="shapeB"></param>
		/// <param name="indexB"></param>
		/// <param name="xfA"></param>
		/// <param name="xfB"></param>
		/// <returns></returns>
		public static bool TestOverlap(Shape shapeA, int indexA, Shape shapeB, int indexB, in Transform xfA, in Transform xfB)
		{
			DistanceInput input;
			shapeA.GetDistanceProxy(indexA, out input.ProxyA);
			shapeB.GetDistanceProxy(indexB, out input.ProxyB);
			input.TransformA = xfA;
			input.TransformB = xfB;
			input.UseRadii = true;

			SimplexCache cache = new SimplexCache();

			Distance.ComputeDistance(out DistanceOutput output, ref cache, in input);

			return output.Distance < 10.0f * float.Epsilon;
		}

		#endregion
	}
}
