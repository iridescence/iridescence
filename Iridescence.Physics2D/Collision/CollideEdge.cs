﻿using System;
using Iridescence.Math;
using Iridescence.Physics2D.Collision.Shapes;

namespace Iridescence.Physics2D.Collision
{
	public static partial class Collisions
	{
		#region Methods

		/// <summary>
		/// Compute the collision manifold between an edge and a circle.
		/// </summary>
		/// <param name="manifold"></param>
		/// <param name="edgeA"></param>
		/// <param name="xfA"></param>
		/// <param name="circleB"></param>
		/// <param name="xfB"></param>
		public static void CollideEdgeAndCircle(Manifold manifold, EdgeShape edgeA, in Transform xfA, CircleShape circleB, in Transform xfB)
		{
			manifold.Points.CountInternal = 0;

			// Compute circle in frame of edge
			Transform.Multiply(in xfB, in circleB.position, out Vector2 Q);
			Transform.MultiplyTransposed(in xfA, in Q, out Q);

			Vector2 A = edgeA.vertex1;
			Vector2 B = edgeA.vertex2;

			Vector2.Subtract(B, A, out Vector2 e);

			// Barycentric coordinates
			Vector2.Subtract(B, Q, out Vector2 diff);
			float u = Vector2.Dot(e, diff);
			Vector2.Subtract(Q, A, out diff);
			float v = Vector2.Dot(e, diff);

			float radius = edgeA.radius + circleB.radius;

			ContactFeature cf = default;
			cf.IndexB = 0;
			cf.TypeB = ContactFeatureType.Vertex;

			// Region A
			if (v <= 0.0f)
			{
				Vector2 P = A;
				Vector2 d = Q - P;
				float dd = d.LengthSquared; //  b2Math.b2Dot(d, d);
				if (dd > radius * radius)
					return;

				// Is there an edge connected to A?
				if (edgeA.hasPrevVertex)
				{
					Vector2 a1 = edgeA.prevVertex;
					Vector2 b1 = A;
					Vector2 e1 = b1 - a1;
					diff = b1 - Q;
					float u1 = Vector2.Dot(e1, diff);

					// Is the circle in Region AB of the previous edge?
					if (u1 > 0.0f)
						return;
				}

				cf.IndexA = 0;
				cf.TypeA = ContactFeatureType.Vertex;
				manifold.Points.CountInternal = 1;
				manifold.Type = ManifoldType.Circles;
				manifold.LocalNormal = Vector2.Zero;
				manifold.LocalPoint = P;
				manifold.Points.Item0.Id = cf;
				manifold.Points.Item0.LocalPoint = circleB.position;
				return;
			}

			// Region B
			if (u <= 0.0f)
			{
				Vector2 P = B;
				Vector2 d = Q - P;
				float dd = d.LengthSquared; //  b2Math.b2Dot(d, d);
				if (dd > radius * radius)
				{
					return;
				}

				// Is there an edge connected to B?
				if (edgeA.hasNextVertex)
				{
					Vector2 B2 = edgeA.nextVertex;
					Vector2 A2 = B;
					Vector2 e2 = B2 - A2;
					diff = Q - A2;
					float v2 = Vector2.Dot(e2, diff);

					// Is the circle in Region AB of the next edge?
					if (v2 > 0.0f)
						return;
				}

				cf.IndexA = 1;
				cf.TypeA = ContactFeatureType.Vertex;
				manifold.Points.CountInternal = 1;
				manifold.Type = ManifoldType.Circles;
				manifold.LocalNormal = Vector2.Zero;
				manifold.LocalPoint = P;
				manifold.Points.Item0.Id = cf;
				manifold.Points.Item0.LocalPoint = circleB.position;
				return;
			}

			// Region AB
			float den = e.LengthSquared; // b2Math.b2Dot(e, e);
			System.Diagnostics.Debug.Assert(den > 0.0f);
			Vector2 xP = (1.0f / den) * (u * A + v * B);
			Vector2 xd = Q - xP;
			float xdd = xd.LengthSquared; //  b2Math.b2Dot(xd, xd);
			if (xdd > radius * radius)
			{
				return;
			}

			Vector2 n;
			n.X = -e.Y;
			n.Y = e.X;
			diff = Q - A;
			if (Vector2.Dot(n, diff) < 0.0f)
			{
				n.X = -n.X;
				n.Y = -n.Y;
			}
			Vector2.Normalize(n, out n);

			cf.IndexA = 0;
			cf.TypeA = ContactFeatureType.Face;
			manifold.Points.CountInternal = 1;
			manifold.Type = ManifoldType.FaceA;
			manifold.LocalNormal = n;
			manifold.LocalPoint = A;
			manifold.Points.Item0.Id = cf;
			manifold.Points.Item0.LocalPoint = circleB.position;
		}

		/// <summary>
		/// Compute the collision manifold between an edge and a circle.
		/// </summary>
		/// <param name="manifold"></param>
		/// <param name="edgeA"></param>
		/// <param name="xfA"></param>
		/// <param name="polygonB"></param>
		/// <param name="xfB"></param>
		public static void CollideEdgeAndPolygon(Manifold manifold, EdgeShape edgeA, in Transform xfA, PolygonShape polygonB, in Transform xfB)
		{
			const int capacity = 8;
			Span<Vector2> mem = stackalloc Vector2[2 * capacity];

			EPCollider b = new EPCollider(capacity, mem);
			b.Collide(manifold, edgeA, in xfA, polygonB, in xfB);
		}

		#endregion
	}
}
