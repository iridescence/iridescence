﻿using System;
using System.Diagnostics;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	internal struct SimplexVertex
	{
		/// <summary>
		/// support point in proxyA
		/// </summary>
		public Vector2 SupportA;

		/// <summary>
		/// support point in proxyB
		/// </summary>
		public Vector2 SupportB;

		/// <summary>
		/// wB - wA
		/// </summary>
		public Vector2 Distance;

		/// <summary>
		/// barycentric coordinate for closest point
		/// </summary>
		public float A;

		/// <summary>
		/// wA index
		/// </summary>
		public int IndexA;

		/// <summary>
		/// wB index
		/// </summary>
		public int IndexB;
	}
	
	internal ref struct Simplex
	{
		#region Fields

		internal readonly Span<SimplexVertex> Vertices;
		internal int Count;
	
		#endregion

		#region Constructors

		public Simplex(Span<SimplexVertex> vertices)
		{
			this.Vertices = vertices;
			this.Count = 0;
		}

		#endregion

		#region Methods

		public void ReadCache(in SimplexCache cache, in DistanceProxy proxyA, in Transform transformA, in DistanceProxy proxyB, in Transform transformB)
		{
			Debug.Assert(cache.Count <= 3);

			// Copy data from cache.
			this.Count = cache.Count;
			for (int i = 0; i < this.Count; ++i)
			{
				ref SimplexVertex v = ref this.Vertices[i];
				v.IndexA = cache.IndexA[i];
				v.IndexB = cache.IndexB[i];
				Vector2 wALocal = proxyA.vertices[v.IndexA];
				Vector2 wBLocal = proxyB.vertices[v.IndexB];
				Transform.Multiply(transformA, wALocal, out v.SupportA);
				Transform.Multiply(transformB, wBLocal, out v.SupportB);
				Vector2.Subtract(v.SupportB, v.SupportA, out v.Distance);
				v.A = 0.0f;
			}

			// Compute the new simplex metric, if it is substantially different than
			// old metric then flush the simplex.
			if (this.Count > 1)
			{
				float metric1 = cache.Metric;
				float metric2 = this.GetMetric();
				if (metric2 < 0.5f * metric1 || 2.0f * metric1 < metric2 || metric2 < float.Epsilon)
				{
					// Reset the simplex.
					this.Count = 0;
				}
			}

			// If the cache is empty or invalid ...
			if (this.Count == 0)
			{
				ref SimplexVertex v = ref this.Vertices[0];
				v.IndexA = 0;
				v.IndexB = 0;
				Vector2 wALocal = proxyA.vertices[0];
				Vector2 wBLocal = proxyB.vertices[0];
				Transform.Multiply(in transformA, in wALocal, out v.SupportA);
				Transform.Multiply(in transformB, in wBLocal, out v.SupportB);
				Vector2.Subtract(v.SupportB, v.SupportA, out v.Distance);
				v.A = 1.0f;
				this.Count = 1;
			}
		}

		public void WriteCache(ref SimplexCache cache)
		{
			cache.Metric = this.GetMetric();
			cache.Count = this.Count;
			for (int i = 0; i < this.Count; ++i)
			{
				cache.IndexA[i] = this.Vertices[i].IndexA;
				cache.IndexB[i] = this.Vertices[i].IndexB;
			}
		}

		public void GetSearchDirection(out Vector2 dir)
		{
			switch (this.Count)
			{
				case 1:
					Vector2.Negate(this.Vertices[0].Distance, out dir);
					return;

				case 2:
					Vector2.Subtract(this.Vertices[1].Distance, this.Vertices[0].Distance, out Vector2 e12);
					Vector2.Negate(this.Vertices[0].Distance, out Vector2 v0neg);
					float sgn = Vector2.Cross(e12, v0neg);

					if (sgn > 0.0f)
					{
						// Origin is left of e12.
						Vector2.Cross(1.0f, in e12, out dir);
					}
					else
					{
						// Origin is right of e12.
						Vector2.Cross(in e12, 1.0f, out dir);
					}
					return;

				default:
					throw new Exception();
			}
		}

		public void GetClosestPoint(out Vector2 point)
		{
			switch (this.Count)
			{
				case 1:
					point = this.Vertices[0].Distance;
					return;

				case 2:
					point.X = this.Vertices[0].A * this.Vertices[0].Distance.X + this.Vertices[1].A * this.Vertices[1].Distance.X;
					point.Y = this.Vertices[0].A * this.Vertices[0].Distance.Y + this.Vertices[1].A * this.Vertices[1].Distance.Y;
					return;

				case 3:
					point = Vector2.Zero;
					return;

				default:
					throw new Exception();
			}
		}

		public void GetWitnessPoints(out Vector2 pA, out Vector2 pB)
		{
			switch (this.Count)
			{
				case 1:
					pA = this.Vertices[0].SupportA;
					pB = this.Vertices[0].SupportB;
					break;

				case 2:
					pA.X = this.Vertices[0].A * this.Vertices[0].SupportA.X + this.Vertices[1].A * this.Vertices[1].SupportA.X;
					pA.Y = this.Vertices[0].A * this.Vertices[0].SupportA.Y + this.Vertices[1].A * this.Vertices[1].SupportA.Y;
					pB.X = this.Vertices[0].A * this.Vertices[0].SupportB.X + this.Vertices[1].A * this.Vertices[1].SupportB.X;
					pB.Y = this.Vertices[0].A * this.Vertices[0].SupportB.Y + this.Vertices[1].A * this.Vertices[1].SupportB.Y;
					break;

				case 3:
					pA.X = this.Vertices[0].A * this.Vertices[0].SupportA.X + this.Vertices[1].A * this.Vertices[1].SupportA.X + this.Vertices[2].A * this.Vertices[2].SupportA.X;
					pA.Y = this.Vertices[0].A * this.Vertices[0].SupportA.Y + this.Vertices[1].A * this.Vertices[1].SupportA.Y + this.Vertices[2].A * this.Vertices[2].SupportA.Y;
					pB = pA;
					break;

				default:
					throw new Exception("Invalid simplex count.");
			}
		}

		public float GetMetric()
		{
			switch (this.Count)
			{
				case 1:
					return 0.0f;

				case 2:
					float x = this.Vertices[0].Distance.X - this.Vertices[1].Distance.X;
					float y = this.Vertices[0].Distance.Y - this.Vertices[1].Distance.Y;
					return (float)System.Math.Sqrt(x * x + y * y);

				case 3:
					float ax = this.Vertices[1].Distance.X - this.Vertices[0].Distance.X;
					float ay = this.Vertices[1].Distance.Y - this.Vertices[0].Distance.Y;
					float bx = this.Vertices[2].Distance.X - this.Vertices[0].Distance.X;
					float by = this.Vertices[2].Distance.Y - this.Vertices[0].Distance.Y;
					return ax * by - ay * bx;

				default:
					throw new Exception("Invalid simplex count.");
			}
		}

		public void Solve2()
		{
			// Solve a line segment using barycentric coordinates.
			//
			// p = a1 * w1 + a2 * w2
			// a1 + a2 = 1
			//
			// The vector from the origin to the closest point on the line is
			// perpendicular to the line.
			// e12 = w2 - w1
			// dot(p, e) = 0
			// a1 * dot(w1, e) + a2 * dot(w2, e) = 0
			//
			// 2-by-2 linear system
			// [1      1     ][a1] = [1]
			// [w1.e12 w2.e12][a2] = [0]
			//
			// Define
			// d12_1 =  dot(w2, e12)
			// d12_2 = -dot(w1, e12)
			// d12 = d12_1 + d12_2
			//
			// Solution
			// a1 = d12_1 / d12
			// a2 = d12_2 / d12
			
			float w1x = this.Vertices[0].Distance.X;
			float w1y = this.Vertices[0].Distance.Y;
			float w2x = this.Vertices[1].Distance.X;
			float w2y = this.Vertices[1].Distance.Y;

			float e12x = w2x - w1x;
			float e12y = w2y - w1y;

			// w1 region
			float d12_2 = -(w1x * e12x + w1y * e12y);

			if (d12_2 <= 0.0f)
			{
				// a2 <= 0, so we clamp it to 0
				this.Vertices[0].A = 1.0f;
				this.Count = 1;
				return;
			}

			// w2 region
			float d12_1 = w2x * e12x + w2y * e12y;
			if (d12_1 <= 0.0f)
			{
				// a1 <= 0, so we clamp it to 0
				this.Vertices[1].A = 1.0f;
				this.Count = 1;

				this.Vertices[0].SupportA = this.Vertices[1].SupportA;
				this.Vertices[0].SupportB = this.Vertices[1].SupportB;
				this.Vertices[0].Distance = this.Vertices[1].Distance;
				this.Vertices[0].A = this.Vertices[1].A;
				this.Vertices[0].IndexA = this.Vertices[1].IndexA;
				this.Vertices[0].IndexB = this.Vertices[1].IndexB;

				return;
			}

			// Must be in e12 region.
			float inv_d12 = 1.0f / (d12_1 + d12_2);
			this.Vertices[0].A = d12_1 * inv_d12;
			this.Vertices[1].A = d12_2 * inv_d12;
			this.Count = 2;
		}

		public void Solve3()
		{
			// Possible regions:
			// - points[2]
			// - edge points[0]-points[2]
			// - edge points[1]-points[2]
			// - inside the triangle
			
			float w1x = this.Vertices[0].Distance.X;
			float w1y = this.Vertices[0].Distance.Y;
			float w2x = this.Vertices[1].Distance.X;
			float w2y = this.Vertices[1].Distance.Y;
			float w3x = this.Vertices[2].Distance.X;
			float w3y = this.Vertices[2].Distance.Y;

			// Edge12
			// [1      1     ][a1] = [1]
			// [w1.e12 w2.e12][a2] = [0]
			// a3 = 0
			float e12x = w2x - w1x;
			float e12y = w2y - w1y;

			float w1e12 = w1x * e12x + w1y * e12y;
			float w2e12 = w2x * e12x + w2y * e12y;
			float d12_1 = w2e12;
			float d12_2 = -w1e12;

			// Edge13
			// [1      1     ][a1] = [1]
			// [w1.e13 w3.e13][a3] = [0]
			// a2 = 0
			float e13x = w3x - w1x;
			float e13y = w3y - w1y;

			float w1e13 = w1x * e13x + w1y * e13y;
			float w3e13 = w3x * e13x + w3y * e13y;
			float d13_1 = w3e13;
			float d13_2 = -w1e13;

			// Edge23
			// [1      1     ][a2] = [1]
			// [w2.e23 w3.e23][a3] = [0]
			// a1 = 0
			float e23x = w3x - w2x;
			float e23y = w3y - w2y;

			float w2e23 = w2x * e23x + w2y * e23y;
			float w3e23 = w3x * e23x + w3y * e23y;
			float d23_1 = w3e23;
			float d23_2 = -w2e23;

			// Triangle123
			float n123 = e12x * e13y - e12y * e13x;

			float d123_1 = n123 * (w2x * w3y - w2y * w3x);
			float d123_2 = n123 * (w3x * w1y - w3y * w1x);
			float d123_3 = n123 * (w1x * w2y - w1y * w2x);

			// w1 region
			if (d12_2 <= 0.0f && d13_2 <= 0.0f)
			{
				this.Vertices[0].A = 1.0f;
				this.Count = 1;
				return;
			}

			// e12
			if (d12_1 > 0.0f && d12_2 > 0.0f && d123_3 <= 0.0f)
			{
				float inv_d12 = 1.0f / (d12_1 + d12_2);
				this.Vertices[0].A = d12_1 * inv_d12;
				this.Vertices[1].A = d12_2 * inv_d12;
				this.Count = 2;
				return;
			}

			// e13
			if (d13_1 > 0.0f && d13_2 > 0.0f && d123_2 <= 0.0f)
			{
				float inv_d13 = 1.0f / (d13_1 + d13_2);
				this.Vertices[0].A = d13_1 * inv_d13;
				this.Vertices[2].A = d13_2 * inv_d13;
				this.Count = 2;

				this.Vertices[1].SupportA = this.Vertices[2].SupportA;
				this.Vertices[1].SupportB = this.Vertices[2].SupportB;
				this.Vertices[1].Distance = this.Vertices[2].Distance;
				this.Vertices[1].A = this.Vertices[2].A;
				this.Vertices[1].IndexA = this.Vertices[2].IndexA;
				this.Vertices[1].IndexB = this.Vertices[2].IndexB;

				return;
			}

			// w2 region
			if (d12_1 <= 0.0f && d23_2 <= 0.0f)
			{
				this.Vertices[1].A = 1.0f;
				this.Count = 1;

				this.Vertices[0].SupportA = this.Vertices[1].SupportA;
				this.Vertices[0].SupportB = this.Vertices[1].SupportB;
				this.Vertices[0].Distance = this.Vertices[1].Distance;
				this.Vertices[0].A = this.Vertices[1].A;
				this.Vertices[0].IndexA = this.Vertices[1].IndexA;
				this.Vertices[0].IndexB = this.Vertices[1].IndexB;

				return;
			}

			// w3 region
			if (d13_1 <= 0.0f && d23_1 <= 0.0f)
			{
				this.Vertices[2].A = 1.0f;
				this.Count = 1;

				this.Vertices[0].SupportA = this.Vertices[2].SupportA;
				this.Vertices[0].SupportB = this.Vertices[2].SupportB;
				this.Vertices[0].Distance = this.Vertices[2].Distance;
				this.Vertices[0].A = this.Vertices[2].A;
				this.Vertices[0].IndexA = this.Vertices[2].IndexA;
				this.Vertices[0].IndexB = this.Vertices[2].IndexB;

				return;
			}

			// e23
			if (d23_1 > 0.0f && d23_2 > 0.0f && d123_1 <= 0.0f)
			{
				float inv_d23 = 1.0f / (d23_1 + d23_2);
				this.Vertices[1].A = d23_1 * inv_d23;
				this.Vertices[2].A = d23_2 * inv_d23;
				this.Count = 2;

				this.Vertices[0].SupportA = this.Vertices[2].SupportA;
				this.Vertices[0].SupportB = this.Vertices[2].SupportB;
				this.Vertices[0].Distance = this.Vertices[2].Distance;
				this.Vertices[0].A = this.Vertices[2].A;
				this.Vertices[0].IndexA = this.Vertices[2].IndexA;
				this.Vertices[0].IndexB = this.Vertices[2].IndexB;

				return;
			}

			// Must be in triangle123
			float inv_d123 = 1.0f / (d123_1 + d123_2 + d123_3);
			this.Vertices[0].A = d123_1 * inv_d123;
			this.Vertices[1].A = d123_2 * inv_d123;
			this.Vertices[2].A = d123_3 * inv_d123;
			this.Count = 3;
		}

		#endregion
	}
}
