﻿namespace Iridescence.Physics2D.Collision
{
	public enum ManifoldType
	{
		Circles,
		FaceA,
		FaceB
	}
}
