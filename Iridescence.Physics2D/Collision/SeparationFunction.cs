﻿using System;
using System.Diagnostics;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	public struct SeparationFunction
	{
		#region Nested Types

		private enum SeparationType
		{
			Points,
			FaceA,
			FaceB
		};

		#endregion

		#region Fields

		private DistanceProxy proxyA;
		private DistanceProxy proxyB;
		private Sweep sweepA;
		private Sweep sweepB;
		private SeparationType type;
		private Vector2 localPoint;
		private Vector2 axis;

		#endregion

		#region Constructors
		
		// TODO_ERIN might not need to return the separation
		public SeparationFunction(in SimplexCache cache, in DistanceProxy proxyA, in Sweep sweepA, in DistanceProxy proxyB, in Sweep sweepB, float t1, in Transform xfA, in Transform xfB)
		{
			this.proxyA = proxyA;
			this.proxyB = proxyB;
			int count = cache.Count;
			Debug.Assert(0 < count && count < 3);

			this.sweepA = sweepA;
			this.sweepB = sweepB;

			if (count == 1)
			{
				this.type = SeparationType.Points;
				Vector2 localPointA = this.proxyA.vertices[cache.IndexA[0]];
				Vector2 localPointB = this.proxyB.vertices[cache.IndexB[0]];

				float pointAx = (xfA.Q.C * localPointA.X - xfA.Q.S * localPointA.Y) + xfA.P.X;
				float pointAy = (xfA.Q.S * localPointA.X + xfA.Q.C * localPointA.Y) + xfA.P.Y;

				float pointBx = (xfB.Q.C * localPointB.X - xfB.Q.S * localPointB.Y) + xfB.P.X;
				float pointBy = (xfB.Q.S * localPointB.X + xfB.Q.C * localPointB.Y) + xfB.P.Y;

				this.axis.X = pointBx - pointAx;
				this.axis.Y = pointBy - pointAy;

				//float s = Vector2.Normalize(ref this.axis, out this.axis);
				//return s;
				this.localPoint = default;
			}
			else if (cache.IndexA[0] == cache.IndexA[1])
			{
				// Two points on B and one on A.
				this.type = SeparationType.FaceB;

				Vector2 localPointB1 = proxyB.vertices[cache.IndexB[0]];
				Vector2 localPointB2 = proxyB.vertices[cache.IndexB[1]];

				float b21x = localPointB2.X - localPointB1.X;
				float b21y = localPointB2.Y - localPointB1.Y;

				this.axis.X = -b21y;
				this.axis.Y = b21x;

				Vector2.Normalize(in this.axis, out this.axis);

				float normalx = xfB.Q.C * this.axis.X - xfB.Q.S * this.axis.Y;
				float normaly = xfB.Q.S * this.axis.X + xfB.Q.C * this.axis.Y;

				this.localPoint.X = 0.5f * (localPointB1.X + localPointB2.X);
				this.localPoint.Y = 0.5f * (localPointB1.Y + localPointB2.Y);

				float pointBx = (xfB.Q.C * this.localPoint.X - xfB.Q.S * this.localPoint.Y) + xfB.P.X;
				float pointBy = (xfB.Q.S * this.localPoint.X + xfB.Q.C * this.localPoint.Y) + xfB.P.Y;

				Vector2 localPointA = proxyA.vertices[cache.IndexA[0]];

				float pointAx = (xfA.Q.C * localPointA.X - xfA.Q.S * localPointA.Y) + xfA.P.X;
				float pointAy = (xfA.Q.S * localPointA.X + xfA.Q.C * localPointA.Y) + xfA.P.Y;

				float aminusbx = pointAx - pointBx;
				float aminusby = pointAy - pointBy;

				float s = aminusbx * normalx + aminusby * normaly;
				if (s < 0.0f)
				{
					this.axis.X = -this.axis.X;
					this.axis.Y = -this.axis.Y;
					//s = -s;
				}
				//return s;
			}
			else
			{
				// Two points on A and one or two points on B.
				this.type = SeparationType.FaceA;

				Vector2 localPointA1 = this.proxyA.vertices[cache.IndexA[0]];
				Vector2 localPointA2 = this.proxyA.vertices[cache.IndexA[1]];

				float a2minusa1x = localPointA2.X - localPointA1.X;
				float a2minusa1y = localPointA2.Y - localPointA1.Y;

				//m_axis = a2minusa1.UnitCross();// b2Math.b2Cross(localPointA2 - localPointA1, 1.0f);
				this.axis.X = a2minusa1y;
				this.axis.Y = -a2minusa1x;

				Vector2.Normalize(in this.axis, out this.axis);

				float normalx = xfA.Q.C * this.axis.X - xfA.Q.S * this.axis.Y;
				float normaly = xfA.Q.S * this.axis.X + xfA.Q.C * this.axis.Y;

				this.localPoint.X = 0.5f * (localPointA1.X + localPointA2.X);
				this.localPoint.Y = 0.5f * (localPointA1.Y + localPointA2.Y);

				float pointAx = (xfA.Q.C * this.localPoint.X - xfA.Q.S * this.localPoint.Y) + xfA.P.X;
				float pointAy = (xfA.Q.S * this.localPoint.X + xfA.Q.C * this.localPoint.Y) + xfA.P.Y;

				Vector2 localPointB = this.proxyB.vertices[cache.IndexB[0]];

				float pointBx = (xfB.Q.C * localPointB.X - xfB.Q.S * localPointB.Y) + xfB.P.X;
				float pointBy = (xfB.Q.S * localPointB.X + xfB.Q.C * localPointB.Y) + xfB.P.Y;

				float bminusax = pointBx - pointAx;
				float bminusay = pointBy - pointAy;

				float s = bminusax * normalx + bminusay * normaly;

				if (s < 0.0f)
				{
					this.axis.X = -this.axis.X;
					this.axis.Y = -this.axis.Y;
					//s = -s;
				}
				//return s;
			}
		}

		#endregion

		#region Methods

		public float FindMinSeparation(out int indexA, out int indexB, float t)
		{
			this.sweepA.GetTransform(t, out Transform xfA);
			this.sweepB.GetTransform(t, out Transform xfB);

			switch (this.type)
			{
				case SeparationType.Points:
				{
					Vector2 axisA;
					axisA.X = xfA.Q.C * this.axis.X + xfA.Q.S * this.axis.Y;
					axisA.Y = -xfA.Q.S * this.axis.X + xfA.Q.C * this.axis.Y;

					Vector2 axisB;
					axisB.X = xfB.Q.C * -this.axis.X + xfB.Q.S * -this.axis.Y;
					axisB.Y = -xfB.Q.S * -this.axis.X + xfB.Q.C * -this.axis.Y;

						indexA = this.proxyA.GetSupport(axisA, out Vector2 vA);
						indexB = this.proxyB.GetSupport(axisB, out Vector2 vB);

					float pointAx = (xfA.Q.C * vA.X - xfA.Q.S * vA.Y) + xfA.P.X;
					float pointAy = (xfA.Q.S * vA.X + xfA.Q.C * vA.Y) + xfA.P.Y;

					float pointBx = (xfB.Q.C * vB.X - xfB.Q.S * vB.Y) + xfB.P.X;
					float pointBy = (xfB.Q.S * vB.X + xfB.Q.C * vB.Y) + xfB.P.Y;

					float distx = pointBx - pointAx;
					float disty = pointBy - pointAy;

					float separation = distx * this.axis.X + disty * this.axis.Y;

					return separation;
				}

				case SeparationType.FaceA:
				{
					float normalx = xfA.Q.C * this.axis.X - xfA.Q.S * this.axis.Y;
					float normaly = xfA.Q.S * this.axis.X + xfA.Q.C * this.axis.Y;

					float pointAx = (xfA.Q.C * this.localPoint.X - xfA.Q.S * this.localPoint.Y) + xfA.P.X;
					float pointAy = (xfA.Q.S * this.localPoint.X + xfA.Q.C * this.localPoint.Y) + xfA.P.Y;

					Vector2 axisB;
					axisB.X = xfB.Q.C * -normalx + xfB.Q.S * -normaly;
					axisB.Y = -xfB.Q.S * -normalx + xfB.Q.C * -normaly;


						indexA = -1;
						indexB = this.proxyB.GetSupport(axisB, out Vector2 vB);

					float pointBx = (xfB.Q.C * vB.X - xfB.Q.S * vB.Y) + xfB.P.X;
					float pointBy = (xfB.Q.S * vB.X + xfB.Q.C * vB.Y) + xfB.P.Y;

					float distx = pointBx - pointAx;
					float disty = pointBy - pointAy;

					float separation = distx * normalx + disty * normaly;

					return separation;
				}

				case SeparationType.FaceB:
				{
					float normalx = xfB.Q.C * this.axis.X - xfB.Q.S * this.axis.Y;
					float normaly = xfB.Q.S * this.axis.X + xfB.Q.C * this.axis.Y;

					float pointBx = (xfB.Q.C * this.localPoint.X - xfB.Q.S * this.localPoint.Y) + xfB.P.X;
					float pointBy = (xfB.Q.S * this.localPoint.X + xfB.Q.C * this.localPoint.Y) + xfB.P.Y;

					Vector2 axisA;
					axisA.X = xfA.Q.C * -normalx + xfA.Q.S * -normaly;
					axisA.Y = -xfA.Q.S * -normalx + xfA.Q.C * -normaly;


						indexB = -1;
						indexA = this.proxyA.GetSupport(axisA, out Vector2 vA);

					float pointAx = (xfA.Q.C * vA.X - xfA.Q.S * vA.Y) + xfA.P.X;
					float pointAy = (xfA.Q.S * vA.X + xfA.Q.C * vA.Y) + xfA.P.Y;

					float distx = pointAx - pointBx;
					float disty = pointAy - pointBy;

					float separation = distx * normalx + disty * normaly;

					return separation;
				}

				default:
					throw new Exception();
			}
		}

		public float Evaluate(int indexA, int indexB, float t)
		{
			this.sweepA.GetTransform(t, out Transform xfA);
			this.sweepB.GetTransform(t, out Transform xfB);

			switch (this.type)
			{
				case SeparationType.Points:
				{
					var vA = this.proxyA.vertices[indexA];
					var vB = this.proxyB.vertices[indexB];

					float pointAx = (xfA.Q.C * vA.X - xfA.Q.S * vA.Y) + xfA.P.X;
					float pointAy = (xfA.Q.S * vA.X + xfA.Q.C * vA.Y) + xfA.P.Y;

					float pointBx = (xfB.Q.C * vB.X - xfB.Q.S * vB.Y) + xfB.P.X;
					float pointBy = (xfB.Q.S * vB.X + xfB.Q.C * vB.Y) + xfB.P.Y;

					float distx = pointBx - pointAx;
					float disty = pointBy - pointAy;

					float separation = distx * this.axis.X + disty * this.axis.Y;

					return separation;
				}

				case SeparationType.FaceA:
				{
					float normalx = xfA.Q.C * this.axis.X - xfA.Q.S * this.axis.Y;
					float normaly = xfA.Q.S * this.axis.X + xfA.Q.C * this.axis.Y;

					float pointAx = (xfA.Q.C * this.localPoint.X - xfA.Q.S * this.localPoint.Y) + xfA.P.X;
					float pointAy = (xfA.Q.S * this.localPoint.X + xfA.Q.C * this.localPoint.Y) + xfA.P.Y;

					var vB = this.proxyB.vertices[indexB];

					float pointBx = (xfB.Q.C * vB.X - xfB.Q.S * vB.Y) + xfB.P.X;
					float pointBy = (xfB.Q.S * vB.X + xfB.Q.C * vB.Y) + xfB.P.Y;

					float distx = pointBx - pointAx;
					float disty = pointBy - pointAy;

					float separation = distx * normalx + disty * normaly;

					return separation;
				}

				case SeparationType.FaceB:
				{
					float normalx = xfB.Q.C * this.axis.X - xfB.Q.S * this.axis.Y;
					float normaly = xfB.Q.S * this.axis.X + xfB.Q.C * this.axis.Y;

					float pointBx = (xfB.Q.C * this.localPoint.X - xfB.Q.S * this.localPoint.Y) + xfB.P.X;
					float pointBy = (xfB.Q.S * this.localPoint.X + xfB.Q.C * this.localPoint.Y) + xfB.P.Y;

					var vA = this.proxyA.vertices[indexA];

					float pointAx = (xfA.Q.C * vA.X - xfA.Q.S * vA.Y) + xfA.P.X;
					float pointAy = (xfA.Q.S * vA.X + xfA.Q.C * vA.Y) + xfA.P.Y;

					float distx = pointAx - pointBx;
					float disty = pointAy - pointBy;

					float separation = distx * normalx + disty * normaly;

					return separation;
				}

				default:
					throw new Exception();
			}
		}

		#endregion
	}
}
