using Iridescence.Math;
using Iridescence.Physics2D.Collision.Shapes;

namespace Iridescence.Physics2D.Collision
{
	public static partial class Collisions
	{
		#region Fields

		public static byte NullFeature = byte.MaxValue;

		#endregion

		#region Methods

		/// <summary>
		/// Compute the collision manifold between two polygons.
		/// </summary>
		/// <param name="manifold"></param>
		/// <param name="polyA"></param>
		/// <param name="xfA"></param>
		/// <param name="polyB"></param>
		/// <param name="xfB"></param>
		public static void CollidePolygons(Manifold manifold, PolygonShape polyA, in Transform xfA, PolygonShape polyB, in Transform xfB)
		{
			// Find edge normal of max separation on A - return if separating axis is found
			// Find edge normal of max separation on B - return if separation axis is found
			// Choose reference edge as min(minA, minB)
			// Find incident edge
			// Clip
			// The normal points from 1 to 2

			manifold.Points.CountInternal = 0;
			float totalRadius = polyA.radius + polyB.radius;

			float separationA = findMaxSeparation(out int edgeA, polyA, in xfA, polyB, in xfB);
			if (separationA > totalRadius)
				return;

			float separationB = findMaxSeparation(out int edgeB, polyB, in xfB, polyA, in xfA);
			if (separationB > totalRadius)
				return;

			PolygonShape poly1; // reference polygon
			PolygonShape poly2; // incident polygon
			Transform xf1, xf2;
			int edge1; // reference edge
			byte flip;
			const float k_tol = 0.1f * Settings.LinearSlop;

			if (separationB > separationA + k_tol)
			{
				poly1 = polyB;
				poly2 = polyA;
				xf1 = xfB;
				xf2 = xfA;
				edge1 = edgeB;
				manifold.Type = ManifoldType.FaceB;
				flip = 1;
			}
			else
			{
				poly1 = polyA;
				poly2 = polyB;
				xf1 = xfA;
				xf2 = xfB;
				edge1 = edgeA;
				manifold.Type = ManifoldType.FaceA;
				flip = 0;
			}

			Array2<ClipVertex> incidentEdge = new Array2<ClipVertex>();
			findIncidentEdge(out incidentEdge, poly1, in xf1, edge1, poly2, in xf2);

			int count1 = poly1.Vertices.Length;
			Vector2[] vertices1 = poly1.vertices;

			int iv1 = edge1;
			int iv2 = edge1 + 1 < count1 ? edge1 + 1 : 0;

			Vector2 v11 = vertices1[iv1];
			Vector2 v12 = vertices1[iv2];

			Vector2.Subtract(v12, v11, out Vector2 localTangent);
			Vector2.Normalize(in localTangent, out localTangent);

			Vector2 localNormal;
			localNormal.X = localTangent.Y;
			localNormal.Y = -localTangent.X;

			Vector2.Add(v11, v12, out Vector2 planePoint);
			Vector2.Multiply(planePoint, 0.5f, out planePoint);

			Rotation.Multiply(in xf1.Q, in localTangent, out Vector2 tangent);

			Vector2 normal;
			normal.X = tangent.Y;
			normal.Y = -tangent.X;

			Transform.Multiply(in xf1, in v11, out v11);
			Transform.Multiply(in xf1, in v12, out v12);

			// Face offset.
			float frontOffset = Vector2.Dot(normal, v11);

			// Side offsets, extended by polytope skin thickness.
			float sideOffset1 = -Vector2.Dot(tangent, v11) + totalRadius;
			float sideOffset2 = Vector2.Dot(tangent, v12) + totalRadius;

			// Clip incident edge against extruded edge1 side edges.
			Array2<ClipVertex> clipPoints1 = new Array2<ClipVertex>();
			Array2<ClipVertex> clipPoints2 = new Array2<ClipVertex>();

			// Clip to box side 1
			Vector2.Negate(tangent, out Vector2 tangentNeg);
			int np = Collisions.ClipSegmentToLine(ref clipPoints1, in incidentEdge, in tangentNeg, sideOffset1, (byte)iv1);
			if (np < 2)
				return;

			// Clip to negative box side 1
			np = Collisions.ClipSegmentToLine(ref clipPoints2, in clipPoints1, in tangent, sideOffset2, (byte)iv2);
			if (np < 2)
				return;

			// Now clipPoints2 contains the clipped points.
			manifold.LocalNormal = localNormal;
			manifold.LocalPoint = planePoint;

			int pointCount = 0;
			for (int i = 0; i < Settings.MaxManifoldPoints; ++i)
			{
				Vector2 v = clipPoints2[i].Vertex;
				float separation = Vector2.Dot(normal, v) - frontOffset;

				if (separation <= totalRadius)
				{
					ref ManifoldPoint cp = ref manifold.Points.Ref(pointCount);

					Transform.MultiplyTransposed(in xf2, in v, out cp.LocalPoint);
					cp.Id = clipPoints2[i].Feature;
					if (flip != 0)
					{
						// Swap features
						ContactFeature cf = cp.Id;
						cp.Id.IndexA = cf.IndexB;
						cp.Id.IndexB = cf.IndexA;
						cp.Id.TypeA = cf.TypeB;
						cp.Id.TypeB = cf.TypeA;
					}
					++pointCount;
				}
			}

			manifold.Points.CountInternal = pointCount;
		}

		/// <summary>
		/// Find the max separation between poly1 and poly2 using edge normals from poly1.
		/// </summary>
		/// <param name="edgeIndex"></param>
		/// <param name="poly1"></param>
		/// <param name="xf1"></param>
		/// <param name="poly2"></param>
		/// <param name="xf2"></param>
		/// <returns></returns>
		private static float findMaxSeparation(out int edgeIndex, PolygonShape poly1, in Transform xf1, PolygonShape poly2, in Transform xf2)
		{
			Vector2[] n1s = poly1.normals;
			Vector2[] v1s = poly1.vertices;
			Vector2[] v2s = poly2.vertices;
			Transform.MultiplyTransposed(in xf2, in xf1, out Transform xf);

			int bestIndex = 0;
			float maxSeparation = float.MinValue;
			for (int i = 0; i < poly1.vertices.Length; ++i)
			{
				// Get poly1 normal in frame2.
				Vector2 n = Rotation.Multiply(xf.Q, n1s[i]);
				Transform.Multiply(in xf, in v1s[i], out Vector2 v1);

				// Find deepest point for normal i.
				float si = float.MaxValue;
				for (int j = 0; j < poly2.vertices.Length; ++j)
				{
					float sij = Vector2.Dot(n, v2s[j] - v1);
					if (sij < si)
						si = sij;
				}

				if (si > maxSeparation)
				{
					maxSeparation = si;
					bestIndex = i;
				}
			}

			edgeIndex = bestIndex;
			return maxSeparation;
		}

		private static void findIncidentEdge(out Array2<ClipVertex> c, PolygonShape poly1, in Transform xf1, int edge1, PolygonShape poly2, in Transform xf2)
		{
#if false
			Vector2[] normals1 = poly1.Normals;

			int count2 = poly2.VertexCount;
			Vector2[] vertices2 = poly2.Vertices;
			Vector2[] normals2 = poly2.Normals;

			// Get the normal of the reference edge in poly2's frame.
			Vector2 normal1 = b2Math.b2MulT(xf2.q, b2Math.b2Mul(xf1.q, normals1[edge1]));

			// Find the incident edge on poly2.
			int index = 0;
			float minDot = b2Settings.b2_maxFloat;
			for (int i = 0; i < count2; ++i)
			{
				float dot = b2Math.b2Dot(ref normal1, ref normals2[i]);
				if (dot < minDot)
				{
					minDot = dot;
					index = i;
				}
			}

			// Build the clip vertices for the incident edge.
			int i1 = index;
			int i2 = i1 + 1 < count2 ? i1 + 1 : 0;

			c[0].v = b2Math.b2Mul(ref xf2, ref vertices2[i1]);
			c[0].id.indexA = (byte)edge1;
			c[0].id.indexB = (byte)i1;
			c[0].id.typeA = b2ContactFeatureType.e_face;
			c[0].id.typeB = b2ContactFeatureType.e_vertex;

			c[1].v = b2Math.b2Mul(ref xf2, ref vertices2[i2]);
			c[1].id.indexA = (byte)edge1;
			c[1].id.indexB = (byte)i2;
			c[1].id.typeA = b2ContactFeatureType.e_face;
			c[1].id.typeB = b2ContactFeatureType.e_vertex;
#else
			var edge = poly1.normals[edge1];

			int count2 = poly2.Vertices.Length;
			Vector2[] vertices2 = poly2.vertices;
			Vector2[] normals2 = poly2.normals;

			// Get the normal of the reference edge in poly2's frame.
			float bx = xf1.Q.C * edge.X - xf1.Q.S * edge.Y;
			float by = xf1.Q.S * edge.X + xf1.Q.C * edge.Y;

			float normal1x = xf2.Q.C * bx + xf2.Q.S * by;
			float normal1y = -xf2.Q.S * bx + xf2.Q.C * by;

			// Find the incident edge on poly2.
			int index = 0;
			float minDot = float.MaxValue;
			for (int i = 0; i < count2; ++i)
			{
				var normal = normals2[i];
				float dot = normal1x * normal.X + normal1y * normal.Y;
				if (dot < minDot)
				{
					minDot = dot;
					index = i;
				}
			}

			// Build the clip vertices for the incident edge.
			int i1 = index;
			int i2 = i1 + 1 < count2 ? i1 + 1 : 0;

			ClipVertex vertex;
			var vi1 = vertices2[i1];
			var vi2 = vertices2[i2];

			vertex.Vertex.X = (xf2.Q.C * vi1.X - xf2.Q.S * vi1.Y) + xf2.P.X;
			vertex.Vertex.Y = (xf2.Q.S * vi1.X + xf2.Q.C * vi1.Y) + xf2.P.Y;
			vertex.Feature.Hash = 0;
			vertex.Feature.IndexA = (byte)edge1;
			vertex.Feature.IndexB = (byte)i1;
			vertex.Feature.TypeA = ContactFeatureType.Face;
			vertex.Feature.TypeB = ContactFeatureType.Vertex;

			c.A = vertex;

			vertex.Vertex.X = (xf2.Q.C * vi2.X - xf2.Q.S * vi2.Y) + xf2.P.X;
			vertex.Vertex.Y = (xf2.Q.S * vi2.X + xf2.Q.C * vi2.Y) + xf2.P.Y;
			vertex.Feature.Hash = 0;
			vertex.Feature.IndexA = (byte)edge1;
			vertex.Feature.IndexB = (byte)i2;
			vertex.Feature.TypeA = ContactFeatureType.Face;
			vertex.Feature.TypeB = ContactFeatureType.Vertex;

			c.B = vertex;
#endif
		}

		#endregion
	}
}
