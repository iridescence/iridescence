/*
* Copyright (c) 2007-2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

using System.Diagnostics;

namespace Iridescence.Physics2D.Collision
{
	internal static class TimeOfImpact
	{
		#region Methods

		/// <summary>
		/// CCD via the local separating axis method. This seeks progression
		/// by computing the largest time at which separation is maintained.
		/// </summary>
		/// <param name="output"></param>
		/// <param name="input"></param>
		public static void Compute(in TimeOfImpactInput input, out TimeOfImpactOutput output)
		{
			output.State = ImpactState.Unknown;
			output.T = input.TMax;

			Sweep sweepA = input.SweepA;
			Sweep sweepB = input.SweepB;

			// Large rotations can make the root finder fail, so we normalize the
			// sweep angles.
			sweepA.Normalize();
			sweepB.Normalize();

			float tMax = input.TMax;

			float totalRadius = input.ProxyA.radius + input.ProxyB.radius;
			float target = System.Math.Max(Settings.LinearSlop, totalRadius - 3.0f * Settings.LinearSlop);
			float tolerance = 0.25f * Settings.LinearSlop;
			Debug.Assert(target > tolerance);

			float t1 = 0.0f;
			int k_maxIterations = 20; // TODO_ERIN b2Settings
			int iter = 0;

			// Prepare input for distance query.
			SimplexCache cache = new SimplexCache();
			DistanceInput distanceInput;
			distanceInput.ProxyA = input.ProxyA;
			distanceInput.ProxyB = input.ProxyB;
			distanceInput.UseRadii = false;

			// The outer loop progressively attempts to compute new separating axes.
			// This loop terminates when an axis is repeated (no progress is made).
			while (true)
			{
				// Get the distance between shapes. We can also use the results
				// to get a separating axis.
				sweepA.GetTransform(t1, out distanceInput.TransformA);
				sweepB.GetTransform(t1, out distanceInput.TransformB);

				Distance.ComputeDistance(out DistanceOutput distanceOutput, ref cache, in distanceInput);

				// If the shapes are overlapped, we give up on continuous collision.
				if (distanceOutput.Distance <= 0.0f)
				{
					// Failure!
					output.State = ImpactState.Overlapped;
					output.T = 0.0f;
					break;
				}

				if (distanceOutput.Distance < target + tolerance)
				{
					// Victory!
					output.State = ImpactState.Touching;
					output.T = t1;
					break;
				}

				// Initialize the separating axis.
				SeparationFunction fcn = new SeparationFunction(cache, input.ProxyA, sweepA, input.ProxyB, sweepB, t1, distanceInput.TransformA, distanceInput.TransformB);

				// Compute the TOI on the separating axis. We do this by successively
				// resolving the deepest point. This loop is bounded by the number of vertices.
				bool done = false;
				float t2 = tMax;
				int pushBackIter = System.Math.Max(input.ProxyA.vertices.Length, input.ProxyB.vertices.Length);
				while (true)
				{
					float s2 = fcn.FindMinSeparation(out int indexA, out int indexB, t2);

					// Is the final configuration separated?
					if (s2 > target + tolerance)
					{
						// Victory!
						output.State = ImpactState.Separated;
						output.T = tMax;
						done = true;
						break;
					}

					// Has the separation reached tolerance?
					if (s2 > target - tolerance)
					{
						// Advance the sweeps
						t1 = t2;
						break;
					}

					// Compute the initial separation of the witness points.
					float s1 = fcn.Evaluate(indexA, indexB, t1);

					// Check for initial overlap. This might happen if the root finder
					// runs out of iterations.
					if (s1 < target - tolerance)
					{
						output.State = ImpactState.Failed;
						output.T = t1;
						done = true;
						break;
					}

					// Check for touching
					if (s1 <= target + tolerance)
					{
						// Victory! t1 should hold the TOI (could be 0.0).
						output.State = ImpactState.Touching;
						output.T = t1;
						done = true;
						break;
					}

					// Compute 1D root of: f(x) - target = 0
					int rootIterCount = 0;
					float a1 = t1, a2 = t2;
					while (true)
					{
						// Use a mix of the secant rule and bisection.
						float t;
						if (rootIterCount % 1 == 1) // even/odd
						{
							// Secant rule to improve convergence.
							t = a1 + (target - s1) * (a2 - a1) / (s2 - s1);
						}
						else
						{
							// Bisection to guarantee progress.
							t = 0.5f * (a1 + a2);
						}

						float s = fcn.Evaluate(indexA, indexB, t);

						if (System.Math.Abs(s - target) < tolerance)
						{
							// t2 holds a tentative value for t1
							t2 = t;
							break;
						}

						// Ensure we continue to bracket the root.
						if (s > target)
						{
							a1 = t;
							s1 = s;
						}
						else
						{
							a2 = t;
							s2 = s;
						}

						if (++rootIterCount == 50)
							break;
					}


					--pushBackIter;

					if (pushBackIter == 0)
						break;
				}

				++iter;

				if (done)
					break;

				if (iter == k_maxIterations)
				{
					// Root finder got stuck. Semi-victory.
					output.State = ImpactState.Failed;
					output.T = t1;
					break;
				}
			}
		}

		#endregion
	}
}
