using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// Represents an axis aligned bounding box.
	/// </summary>
	public struct AABB : IEquatable<AABB>
	{
		#region Fields

		/// <summary>
		/// The lower vertex.
		/// </summary>
		public Vector2 LowerBound;

		/// <summary>
		/// The upper vertex.
		/// </summary>
		public Vector2 UpperBound;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the center of the AABB.
		/// </summary>
		public Vector2 Center => (this.LowerBound + this.UpperBound) * 0.5f;

		/// <summary>
		/// Gets the extents of the AABB (half-widths).
		/// </summary>
		public Vector2 Extents => (this.UpperBound - this.LowerBound) * 0.5f;

		/// <summary>
		/// Gets the size of the rectangle.
		/// </summary>
		public Vector2 Size => this.UpperBound - this.LowerBound;

		/// <summary>
		/// Gets the perimeter length.
		/// </summary>
		public float Perimeter => 2.0f * (this.UpperBound.X - this.LowerBound.X + this.UpperBound.Y - this.LowerBound.Y);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new AABB.
		/// </summary>
		/// <param name="lowerBound"></param>
		/// <param name="upperBound"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public AABB(in Vector2 lowerBound, in Vector2 upperBound)
		{
			this.LowerBound = lowerBound;
			this.UpperBound = upperBound;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Verifies that the bounds are sorted.
		/// </summary>
		/// <returns></returns>
		public bool IsValid()
		{
			Vector2 d = this.UpperBound - this.LowerBound;
			return d.X >= 0.0f && d.Y >= 0.0f && this.LowerBound.IsFinite && this.UpperBound.IsFinite;
		}

		/// <summary>
		/// Checks whether two AABBs overlap.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TestOverlap(in AABB a, in AABB b)
		{
			return b.LowerBound.X <= a.UpperBound.X &&
			       b.LowerBound.Y <= a.UpperBound.Y &&
			       a.LowerBound.X <= b.UpperBound.X &&
			       a.LowerBound.Y <= b.UpperBound.Y;
		}

		/// <summary>
		/// Combine an AABB into this one.
		/// </summary>
		/// <param name="aabb"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Combine(in AABB aabb)
		{
			this.LowerBound.X = aabb.LowerBound.X < this.LowerBound.X ? aabb.LowerBound.X : this.LowerBound.X;
			this.LowerBound.Y = aabb.LowerBound.Y < this.LowerBound.Y ? aabb.LowerBound.Y : this.LowerBound.Y;

			this.UpperBound.X = aabb.UpperBound.X > this.UpperBound.X ? aabb.UpperBound.X : this.UpperBound.X;
			this.UpperBound.Y = aabb.UpperBound.Y > this.UpperBound.Y ? aabb.UpperBound.Y : this.UpperBound.Y;
		}

		/// <summary>
		/// Combines two AABBs and stores the result in this AABB.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Combine(in AABB a, in AABB b)
		{
			this.LowerBound.X = a.LowerBound.X < b.LowerBound.X ? a.LowerBound.X : b.LowerBound.X;
			this.LowerBound.Y = a.LowerBound.Y < b.LowerBound.Y ? a.LowerBound.Y : b.LowerBound.Y;

			this.UpperBound.X = a.UpperBound.X > b.UpperBound.X ? a.UpperBound.X : b.UpperBound.X;
			this.UpperBound.Y = a.UpperBound.Y > b.UpperBound.Y ? a.UpperBound.Y : b.UpperBound.Y;
		}

		/// <summary>
		/// Combines two AABBs and stores the result in the output parameter.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Combine(in AABB a, in AABB b, out AABB result)
		{
			result.LowerBound.X = a.LowerBound.X < b.LowerBound.X ? a.LowerBound.X : b.LowerBound.X;
			result.LowerBound.Y = a.LowerBound.Y < b.LowerBound.Y ? a.LowerBound.Y : b.LowerBound.Y;

			result.UpperBound.X = a.UpperBound.X > b.UpperBound.X ? a.UpperBound.X : b.UpperBound.X;
			result.UpperBound.Y = a.UpperBound.Y > b.UpperBound.Y ? a.UpperBound.Y : b.UpperBound.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether this AABB contains the specified AABB.
		/// </summary>
		/// <param name="aabb"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public bool Contains(in AABB aabb)
		{
			return this.LowerBound.X <= aabb.LowerBound.X &&
			       this.LowerBound.Y <= aabb.LowerBound.Y &&
			       aabb.UpperBound.X <= this.UpperBound.X &&
			       aabb.UpperBound.Y <= this.UpperBound.Y;
		}

		/// <summary>
		/// Casts a ray against the bounding box.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="output"></param>
		/// <returns></returns>
		public bool RayCast(in RayCastInput input, out RayCastOutput output)
		{
			float tmin = -float.MaxValue;
			float tmax = float.MaxValue;

			Vector2 p = input.Point1;
			Vector2 d = input.Point2 - input.Point1;
			Vector2 absD;
			absD.X = System.Math.Abs(d.X);
			absD.Y = System.Math.Abs(d.Y);

			Vector2 normal = Vector2.Zero;

			for (int i = 0; i < 2; ++i)
			{
				float p_i = (i == 0 ? p.X : p.Y);
				float lb = (i == 0 ? this.LowerBound.X : this.LowerBound.Y);
				float ub = (i == 0 ? this.UpperBound.X : this.UpperBound.Y);
				float absd_i = (i == 0 ? absD.X : absD.Y);
				float d_i = (i == 0 ? d.X : d.Y);

				if (absd_i < float.Epsilon)
				{
					// Parallel.
					if (p_i < lb || ub < p_i)
					{
						output.Fraction = 0f;
						output.Normal = Vector2.Zero;
						return false;
					}
				}
				else
				{
					float inv_d = 1.0f / d_i;
					float t1 = (lb - p_i) * inv_d;
					float t2 = (ub - p_i) * inv_d;

					// Sign of the normal vector.
					float s = -1.0f;

					if (t1 > t2)
					{
						// swap t1 and t2.
						s = t1;
						t1 = t2;
						t2 = s;

						s = 1.0f;
					}

					// Push the min up
					if (t1 > tmin)
					{
						normal = Vector2.Zero;
						if (i == 0)
							normal.X = s;
						else
							normal.Y = s;
						tmin = t1;
					}

					// Pull the max down
					tmax = tmax < t2 ? tmax : t2;

					if (tmin > tmax)
					{
						output.Fraction = 0f;
						output.Normal = Vector2.Zero;
						return false;
					}
				}
			}

			// Does the ray start inside the box?
			// Does the ray intersect beyond the max fraction?
			if (tmin < 0.0f || input.MaxFraction < tmin)
			{
				output.Fraction = 0f;
				output.Normal = Vector2.Zero;
				return false;
			}

			// Intersection.
			output.Fraction = tmin;
			output.Normal = normal;
			return true;
		}

		/// <summary>
		/// Enlarges the bounding box in all directions by the specified amount.
		/// </summary>
		/// <param name="amount"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Fatten(float amount)
		{
			this.UpperBound.X += amount;
			this.UpperBound.Y += amount;
			this.LowerBound.X -= amount;
			this.LowerBound.Y -= amount;
		}

		/// <summary>
		/// Returns a bounding box for the specified vertices.
		/// </summary>
		/// <param name="vertices">An enumerable of vertices.</param>
		/// <returns></returns>
		public static AABB Create(IEnumerable<Vector2> vertices)
		{
			AABB aabb = new AABB();

			bool first = true;
			foreach (Vector2 v in vertices)
			{
				if (first)
				{
					aabb.LowerBound = v;
					aabb.UpperBound = v;
					first = false;
				}
				else
				{
					if (v.X > aabb.UpperBound.X) aabb.UpperBound.X = v.X;
					if (v.Y > aabb.UpperBound.Y) aabb.UpperBound.Y = v.Y;
					if (v.X < aabb.LowerBound.X) aabb.LowerBound.X = v.X;
					if (v.Y < aabb.LowerBound.Y) aabb.LowerBound.Y = v.Y;
				}
			}

			if (first)
				throw new ArgumentException("Set of vertices can not be empty.", nameof(vertices));

			return aabb;
		}

		/// <summary>
		/// Returns a bounding box for the specified vertices.
		/// </summary>
		/// <param name="vertices">The vertex array.</param>
		/// <returns></returns>
		public static AABB Create(ReadOnlySpan<Vector2> vertices)
		{
			if (vertices.IsEmpty)
				return default;

			AABB aabb;
			aabb.LowerBound = vertices[0];
			aabb.UpperBound = vertices[0];

			for (int i = 0; i < vertices.Length; ++i)
			{
				if (vertices[i].X > aabb.UpperBound.X) aabb.UpperBound.X = vertices[i].X;
				if (vertices[i].Y > aabb.UpperBound.Y) aabb.UpperBound.Y = vertices[i].Y;
				if (vertices[i].X < aabb.LowerBound.X) aabb.LowerBound.X = vertices[i].X;
				if (vertices[i].Y < aabb.LowerBound.Y) aabb.LowerBound.Y = vertices[i].Y;
			}

			return aabb;
		}

		public override string ToString()
		{
			return $"X:{this.LowerBound.X} Y:{this.LowerBound.Y} W:{this.Size.X} H:{this.Size.Y}";
		}

		public bool Equals(in AABB other)
		{
			return this.LowerBound.Equals(other.LowerBound) && this.UpperBound.Equals(other.UpperBound);
		}

		bool IEquatable<AABB>.Equals(AABB other)
		{
			return this.LowerBound.Equals(other.LowerBound) && this.UpperBound.Equals(other.UpperBound);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is AABB aabb && this.Equals(aabb);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (this.LowerBound.GetHashCode() * 397) ^ this.UpperBound.GetHashCode();
			}
		}

		public static bool operator ==(AABB left, AABB right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(AABB left, AABB right)
		{
			return !left.Equals(right);
		}

		#endregion
	}
}
