/*
* Copyright (c) 2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

using System;
using System.Diagnostics;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// A dynamic AABB tree broad-phase, inspired by Nathanael Presson's btDbvt.
	/// A dynamic tree arranges data in a binary tree to accelerate
	/// queries such as volume queries and ray casts. Leafs are proxies
	/// with an AABB. In the tree we expand the proxy AABB by b2_fatAABBFactor
	/// so that the proxy AABB is bigger than the client object. This allows the client
	/// object to move by small amounts without triggering a tree update.
	///
	/// Nodes are pooled and relocatable, so we use node indices rather than pointers.
	/// </summary>
	internal sealed class DynamicTree
	{
		#region Nested Types

		/// <summary>
		/// A node in the dynamic tree. The client does not interact with this directly.
		/// </summary>
		private struct TreeNode
		{
			public const int NullNode = -1;

			public bool IsLeaf => this.Child1 == NullNode;

			public int ParentOrNext;
			public int Child1;
			public int Child2;

			// leaf = 0, free node = -1
			public int Height;

			/// Enlarged AABB
			public AABB AABB;

			public object UserData;
		}
		
		#endregion

		#region Fields

		private int root;

		private TreeNode[] nodes;
		private int nodeCount;

		private int freeList;
		
		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Constructing the tree initializes the node pool.
		/// </summary>
		public DynamicTree()
		{
			this.root = TreeNode.NullNode;

			this.nodeCount = 0;
			this.nodes = new TreeNode[16];

			// Build a linked list for the free list.
			for (int i = 0; i < this.nodes.Length - 1; ++i)
			{
				this.nodes[i].ParentOrNext = i + 1;
				this.nodes[i].Height = -1;
			}

			this.nodes[this.nodes.Length - 1].ParentOrNext = TreeNode.NullNode;
			this.nodes[this.nodes.Length - 1].Height = -1;
			this.freeList = 0;
		}
		
		#endregion

		#region Methods
		
		/// <summary>
		/// Allocate a node from the pool. Grow the pool if necessary.
		/// </summary>
		/// <returns></returns>
		private int AllocateNode()
		{
			// Expand the node pool as needed.
			if (this.freeList == TreeNode.NullNode)
			{
				Debug.Assert(this.nodeCount == this.nodes.Length);

				Array.Resize(ref this.nodes, this.nodes.Length * 2);


				//// The free list is empty. Rebuild a bigger pool.
				//TreeNode[] oldNodes = this.nodes;
				//this.nodeCapacity *= 2;
				//this.nodes = new TreeNode[this.nodeCapacity];

				//// initialize new b2TreeNode
				//oldNodes.CopyTo(this.nodes, 0);

				// Build a linked list for the free list. The parent
				// pointer becomes the "next" pointer.
				for (int i = this.nodeCount; i < this.nodes.Length - 1; ++i)
				{
					this.nodes[i].ParentOrNext = i + 1;
					this.nodes[i].Height = -1;
				}

				this.nodes[this.nodes.Length - 1].ParentOrNext = TreeNode.NullNode;
				this.nodes[this.nodes.Length - 1].Height = -1;
				this.freeList = this.nodeCount;
			}

			// Peel a node off the free list.
			int nodeId = this.freeList;
			this.freeList = this.nodes[nodeId].ParentOrNext;
			this.nodes[nodeId].ParentOrNext = TreeNode.NullNode;
			this.nodes[nodeId].Child1 = TreeNode.NullNode;
			this.nodes[nodeId].Child2 = TreeNode.NullNode;
			this.nodes[nodeId].Height = 0;
			this.nodes[nodeId].UserData = null;
			++this.nodeCount;
			return nodeId;
		}

		/// <summary>
		/// Return a node to the pool.
		/// </summary>
		/// <param name="nodeId"></param>
		private void FreeNode(int nodeId)
		{
			Debug.Assert(0 <= nodeId && nodeId < this.nodes.Length);
			Debug.Assert(0 < this.nodeCount);
			this.nodes[nodeId].ParentOrNext = this.freeList;
			this.nodes[nodeId].Height = -1;
			this.freeList = nodeId;
			--this.nodeCount;
		}

		/// <summary>
		/// Perform a left or right rotation if node A is imbalanced.
		/// Returns the new root index.
		/// </summary>
		/// <param name="iA"></param>
		/// <returns></returns>
		private int Balance(int iA)
		{
			Debug.Assert(iA != TreeNode.NullNode);

			ref TreeNode A = ref this.nodes[iA];
			if (A.IsLeaf || A.Height < 2)
			{
				return iA;
			}

			int iB = A.Child1;
			int iC = A.Child2;
			Debug.Assert(0 <= iB && iB < this.nodes.Length);
			Debug.Assert(0 <= iC && iC < this.nodes.Length);

			ref TreeNode B = ref this.nodes[iB];
			ref TreeNode C = ref this.nodes[iC];

			int balance = C.Height - B.Height;

			// Rotate C up
			if (balance > 1)
			{
				int iF = C.Child1;
				int iG = C.Child2;
				ref TreeNode F = ref this.nodes[iF];
				ref TreeNode G = ref this.nodes[iG];
				Debug.Assert(0 <= iF && iF < this.nodes.Length);
				Debug.Assert(0 <= iG && iG < this.nodes.Length);

				// Swap A and C
				C.Child1 = iA;
				C.ParentOrNext = A.ParentOrNext;
				A.ParentOrNext = iC;

				// A's old parent should point to C
				if (C.ParentOrNext != TreeNode.NullNode)
				{
					if (this.nodes[C.ParentOrNext].Child1 == iA)
					{
						this.nodes[C.ParentOrNext].Child1 = iC;
					}
					else
					{
						Debug.Assert(this.nodes[C.ParentOrNext].Child2 == iA);
						this.nodes[C.ParentOrNext].Child2 = iC;
					}
				}
				else
				{
					this.root = iC;
				}

				// Rotate
				if (F.Height > G.Height)
				{
					C.Child2 = iF;
					A.Child2 = iG;
					G.ParentOrNext = iA;
					A.AABB.Combine(in B.AABB, in G.AABB);
					C.AABB.Combine(in A.AABB, in F.AABB);

					A.Height = 1 + System.Math.Max(B.Height, G.Height);
					C.Height = 1 + System.Math.Max(A.Height, F.Height);
				}
				else
				{
					C.Child2 = iG;
					A.Child2 = iF;
					F.ParentOrNext = iA;
					A.AABB.Combine(in B.AABB, in F.AABB);
					C.AABB.Combine(in A.AABB, in G.AABB);

					A.Height = 1 + System.Math.Max(B.Height, F.Height);
					C.Height = 1 + System.Math.Max(A.Height, G.Height);
				}

				return iC;
			}

			// Rotate B up
			if (balance < -1)
			{
				int iD = B.Child1;
				int iE = B.Child2;
				ref TreeNode D = ref this.nodes[iD];
				ref TreeNode E = ref this.nodes[iE];
				Debug.Assert(0 <= iD && iD < this.nodes.Length);
				Debug.Assert(0 <= iE && iE < this.nodes.Length);

				// Swap A and B
				B.Child1 = iA;
				B.ParentOrNext = A.ParentOrNext;
				A.ParentOrNext = iB;

				// A's old parent should point to B
				if (B.ParentOrNext != TreeNode.NullNode)
				{
					if (this.nodes[B.ParentOrNext].Child1 == iA)
					{
						this.nodes[B.ParentOrNext].Child1 = iB;
					}
					else
					{
						Debug.Assert(this.nodes[B.ParentOrNext].Child2 == iA);
						this.nodes[B.ParentOrNext].Child2 = iB;
					}
				}
				else
				{
					this.root = iB;
				}

				// Rotate
				if (D.Height > E.Height)
				{
					B.Child2 = iD;
					A.Child1 = iE;
					E.ParentOrNext = iA;
					A.AABB.Combine(in C.AABB, in E.AABB);
					B.AABB.Combine(in A.AABB, in D.AABB);

					A.Height = 1 + System.Math.Max(C.Height, E.Height);
					B.Height = 1 + System.Math.Max(A.Height, D.Height);
				}
				else
				{
					B.Child2 = iE;
					A.Child1 = iD;
					D.ParentOrNext = iA;
					A.AABB.Combine(in C.AABB, in D.AABB);
					B.AABB.Combine(in A.AABB, in E.AABB);

					A.Height = 1 + System.Math.Max(C.Height, D.Height);
					B.Height = 1 + System.Math.Max(A.Height, E.Height);
				}

				return iB;
			}

			return iA;
		}

		private void InsertLeaf(int leaf)
		{
			if (this.root == TreeNode.NullNode)
			{
				this.root = leaf;
				this.nodes[this.root].ParentOrNext = TreeNode.NullNode;
				return;
			}

			// Find the best sibling for this node
			AABB leafAABB = this.nodes[leaf].AABB;
			int index = this.root;
			while (this.nodes[index].IsLeaf == false)
			{
				int child1 = this.nodes[index].Child1;
				int child2 = this.nodes[index].Child2;

				float area = this.nodes[index].AABB.Perimeter;

				AABB.Combine(in this.nodes[index].AABB, in leafAABB, out AABB combinedAABB);
				float combinedArea = combinedAABB.Perimeter;

				// Cost of creating a new parent for this node and the new leaf
				float cost = 2.0f * combinedArea;

				// Minimum cost of pushing the leaf further down the tree
				float inheritanceCost = 2.0f * (combinedArea - area);

				// Cost of descending into child1
				float cost1;
				if (this.nodes[child1].IsLeaf)
				{
					AABB.Combine(in leafAABB, in this.nodes[child1].AABB, out AABB aabb);
					cost1 = aabb.Perimeter + inheritanceCost;
				}
				else
				{
					AABB.Combine(in leafAABB, in this.nodes[child1].AABB, out AABB aabb);
					float oldArea = this.nodes[child1].AABB.Perimeter;
					float newArea = aabb.Perimeter;
					cost1 = (newArea - oldArea) + inheritanceCost;
				}

				// Cost of descending into child2
				float cost2;
				if (this.nodes[child2].IsLeaf)
				{
					AABB.Combine(in leafAABB, in this.nodes[child2].AABB, out AABB aabb);
					cost2 = aabb.Perimeter + inheritanceCost;
				}
				else
				{
					AABB.Combine(in leafAABB, in this.nodes[child2].AABB, out AABB aabb);
					float oldArea = this.nodes[child2].AABB.Perimeter;
					float newArea = aabb.Perimeter;
					cost2 = newArea - oldArea + inheritanceCost;
				}

				// Descend according to the minimum cost.
				if (cost < cost1 && cost < cost2)
				{
					break;
				}

				// Descend
				if (cost1 < cost2)
				{
					index = child1;
				}
				else
				{
					index = child2;
				}
			}

			int sibling = index;


			// Create a new parent.
			int oldParent = this.nodes[sibling].ParentOrNext;
			int newParent = this.AllocateNode();
			this.nodes[newParent].ParentOrNext = oldParent;
			this.nodes[newParent].UserData = null;
			this.nodes[newParent].AABB.Combine(in leafAABB, in this.nodes[sibling].AABB);
			this.nodes[newParent].Height = this.nodes[sibling].Height + 1;

			if (oldParent != TreeNode.NullNode)
			{
				// The sibling was not the root.
				if (this.nodes[oldParent].Child1 == sibling)
				{
					this.nodes[oldParent].Child1 = newParent;
				}
				else
				{
					this.nodes[oldParent].Child2 = newParent;
				}

				this.nodes[newParent].Child1 = sibling;
				this.nodes[newParent].Child2 = leaf;
				this.nodes[sibling].ParentOrNext = newParent;
				this.nodes[leaf].ParentOrNext = newParent;
			}
			else
			{
				// The sibling was the root.
				this.nodes[newParent].Child1 = sibling;
				this.nodes[newParent].Child2 = leaf;
				this.nodes[sibling].ParentOrNext = newParent;
				this.nodes[leaf].ParentOrNext = newParent;
				this.root = newParent;
			}

			// Walk back up the tree fixing heights and AABBs
			index = this.nodes[leaf].ParentOrNext;
			while (index != TreeNode.NullNode)
			{
				index = this.Balance(index);

				int child1 = this.nodes[index].Child1;
				int child2 = this.nodes[index].Child2;

				Debug.Assert(child1 != TreeNode.NullNode);
				Debug.Assert(child2 != TreeNode.NullNode);

				this.nodes[index].Height = 1 + System.Math.Max(this.nodes[child1].Height, this.nodes[child2].Height);
				this.nodes[index].AABB.Combine(in this.nodes[child1].AABB, in this.nodes[child2].AABB);

				index = this.nodes[index].ParentOrNext;
			}

			//Validate();
		}

		private void RemoveLeaf(int leaf)
		{
			if (leaf == this.root)
			{
				this.root = TreeNode.NullNode;
				return;
			}

			int parent = this.nodes[leaf].ParentOrNext;
			int grandParent = this.nodes[parent].ParentOrNext;
			int sibling;
			if (this.nodes[parent].Child1 == leaf)
			{
				sibling = this.nodes[parent].Child2;
			}
			else
			{
				sibling = this.nodes[parent].Child1;
			}

			if (grandParent != TreeNode.NullNode)
			{
				// Destroy parent and connect sibling to grandParent.
				if (this.nodes[grandParent].Child1 == parent)
				{
					this.nodes[grandParent].Child1 = sibling;
				}
				else
				{
					this.nodes[grandParent].Child2 = sibling;
				}
				this.nodes[sibling].ParentOrNext = grandParent;
				this.FreeNode(parent);

				// Adjust ancestor bounds.
				int index = grandParent;
				while (index != TreeNode.NullNode)
				{
					index = this.Balance(index);

					int child1 = this.nodes[index].Child1;
					int child2 = this.nodes[index].Child2;

					this.nodes[index].AABB.Combine(in this.nodes[child1].AABB, in this.nodes[child2].AABB);
					this.nodes[index].Height = 1 + System.Math.Max(this.nodes[child1].Height, this.nodes[child2].Height);

					index = this.nodes[index].ParentOrNext;
				}
			}
			else
			{
				this.root = sibling;
				this.nodes[sibling].ParentOrNext = TreeNode.NullNode;
				this.FreeNode(parent);
			}

			//Validate();
		}

		/// <summary>
		/// Compute the height of a sub-tree.
		/// </summary>
		/// <param name="nodeId"></param>
		/// <returns></returns>
		private int ComputeHeight(int nodeId)
		{
			Debug.Assert(0 <= nodeId && nodeId < this.nodes.Length);
			ref TreeNode node = ref this.nodes[nodeId];

			if (node.IsLeaf)
			{
				return 0;
			}

			int height1 = this.ComputeHeight(node.Child1);
			int height2 = this.ComputeHeight(node.Child2);
			return 1 + System.Math.Max(height1, height2);
		}

		private int ComputeHeight()
		{
			int height = this.ComputeHeight(this.root);
			return height;
		}
		
		private void ValidateStructure(int index)
		{
			if (index == TreeNode.NullNode)
			{
				return;
			}

			if (index == this.root)
			{
				Debug.Assert(this.nodes[index].ParentOrNext == TreeNode.NullNode);
			}

			ref TreeNode node = ref this.nodes[index];

			int child1 = node.Child1;
			int child2 = node.Child2;

			if (node.IsLeaf)
			{
				Debug.Assert(child1 == TreeNode.NullNode);
				Debug.Assert(child2 == TreeNode.NullNode);
				Debug.Assert(node.Height == 0);
				return;
			}

			Debug.Assert(0 <= child1 && child1 < this.nodes.Length);
			Debug.Assert(0 <= child2 && child2 < this.nodes.Length);

			Debug.Assert(this.nodes[child1].ParentOrNext == index);
			Debug.Assert(this.nodes[child2].ParentOrNext == index);

			this.ValidateStructure(child1);
			this.ValidateStructure(child2);
		}

		private void ValidateMetrics(int index)
		{
			if (index == TreeNode.NullNode)
			{
				return;
			}

			ref TreeNode node = ref this.nodes[index];

			int child1 = node.Child1;
			int child2 = node.Child2;

			if (node.IsLeaf)
			{
				Debug.Assert(child1 == TreeNode.NullNode);
				Debug.Assert(child2 == TreeNode.NullNode);
				Debug.Assert(node.Height == 0);
				return;
			}

			Debug.Assert(0 <= child1 && child1 < this.nodes.Length);
			Debug.Assert(0 <= child2 && child2 < this.nodes.Length);
			Debug.Assert(node.Height == 1 + System.Math.Max(this.nodes[child1].Height, this.nodes[child2].Height));

			AABB.Combine(in this.nodes[child1].AABB, in this.nodes[child2].AABB, out AABB aabb);

			Debug.Assert(aabb.LowerBound == node.AABB.LowerBound);
			Debug.Assert(aabb.UpperBound == node.AABB.UpperBound);

			this.ValidateMetrics(child1);
			this.ValidateMetrics(child2);
		}

		/// <summary>
		/// Create a proxy. Provide a tight fitting AABB and a userData pointer.
		/// </summary>
		/// <param name="aabb"></param>
		/// <param name="userData"></param>
		/// <returns></returns>
		public int CreateProxy(in AABB aabb, object userData)
		{
			int proxyId = this.AllocateNode();

			// Fatten the aabb.
			this.nodes[proxyId].AABB = aabb;
			this.nodes[proxyId].AABB.Fatten(Settings.AABBExtension);
			this.nodes[proxyId].UserData = userData;
			this.nodes[proxyId].Height = 0;

			this.InsertLeaf(proxyId);

			return proxyId;
		}

		/// <summary>
		/// Destroy a proxy. This asserts if the id is invalid.
		/// </summary>
		/// <param name="proxyId"></param>
		public void DestroyProxy(int proxyId)
		{
			Debug.Assert(0 <= proxyId && proxyId < this.nodes.Length);
			Debug.Assert(this.nodes[proxyId].IsLeaf);

			this.RemoveLeaf(proxyId);
			this.FreeNode(proxyId);
		}

		/// <summary>
		/// Move a proxy with a swepted AABB. If the proxy has moved outside of its fattened AABB,
		/// then the proxy is removed from the tree and re-inserted. Otherwise
		/// the function returns immediately.
		/// </summary>
		/// <param name="proxyId"></param>
		/// <param name="aabb"></param>
		/// <param name="displacement"></param>
		/// <returns>True if the proxy was re-inserted</returns>
		public bool MoveProxy(int proxyId, in AABB aabb, in Vector2 displacement)
		{
			Debug.Assert(0 <= proxyId && proxyId < this.nodes.Length);

			Debug.Assert(this.nodes[proxyId].IsLeaf);

			if (this.nodes[proxyId].AABB.Contains(in aabb))
			{
				return false;
			}

			this.RemoveLeaf(proxyId);

			// Extend AABB.
			AABB b = aabb;
			b.Fatten(Settings.AABBExtension);

			// Predict AABB displacement.
			// d = b2Settings.b2_aabbMultiplier * displacement;
			float dx = Settings.AABBMultiplier * displacement.X;
			float dy = Settings.AABBMultiplier * displacement.Y;
			
			if (dx < 0.0f)
			{
				b.LowerBound.X += dx;
			}
			else
			{
				b.UpperBound.X += dx;
			}

			if (dy < 0.0f)
			{
				b.LowerBound.Y += dy;
			}
			else
			{
				b.UpperBound.Y += dy;
			}

			this.nodes[proxyId].AABB = b;

			this.InsertLeaf(proxyId);
			return true;
		}

		/// <summary>
		/// Returns proxy user data.
		/// </summary>
		/// <param name="proxyId"></param>
		/// <returns>The proxy user data or null if the id is invalid.</returns>
		public object GetUserData(int proxyId)
		{
			Debug.Assert(0 <= proxyId && proxyId < this.nodes.Length);
			return this.nodes[proxyId].UserData;
		}

		/// <summary>
		/// Stores the fat AABB for a proxy in the specified output.
		/// </summary>
		/// <param name="proxyId"></param>
		/// <param name="output"></param>
		public void GetFatAABB(int proxyId, out AABB output)
		{
			Debug.Assert(0 <= proxyId && proxyId < this.nodes.Length);
			output = this.nodes[proxyId].AABB;
		}
			
		/// <summary>
		/// Query an AABB for overlapping proxies. The callback function
		/// is called for each proxy that overlaps the supplied AABB.
		/// </summary>
		/// <param name="callback"></param>
		/// <param name="aabb"></param>
		public void Query(QueryCallback callback, AABB aabb)
		{
			SpanStack<int> stack = new SpanStack<int>(stackalloc int[256]);

			stack.Push(this.root);

			while (stack.Count > 0)
			{
				int nodeId = stack.Pop();
				if (nodeId == TreeNode.NullNode)
					continue;

				ref TreeNode node = ref this.nodes[nodeId];
				if (AABB.TestOverlap(node.AABB, aabb))
				{
					if (node.IsLeaf)
					{
						if (!callback(nodeId))
							break;
					}
					else
					{
						stack.Push(node.Child1);
						stack.Push(node.Child2);
					}
				}
			}
		}

		/// <summary>
		/// Ray-cast against the proxies in the tree. This relies on the callback
		/// to perform a exact ray-cast in the case were the proxy contains a shape.
		/// The callback also performs the any collision filtering. This has performance
		/// roughly equal to k * log(n), where k is the number of collisions and n is the
		/// number of proxies in the tree.
		/// </summary>
		/// <param name="input">The ray-cast input data. The ray extends from p1 to p1 + maxFraction * (p2 - p1).</param>
		/// <param name="callback">A callback class that is called for each proxy that is hit by the ray.</param>
		public void RayCast(in RayCastInput input, RayCastCallback callback)
		{
			Vector2 p1 = input.Point1;
			Vector2 p2 = input.Point2;
			Vector2 r = p2 - p1;
			Debug.Assert(r.LengthSquared > 0.0f);
			Vector2.Normalize(in r, out r);

			// v is perpendicular to the segment.
			Vector2 v; // b2Math.b2Cross(1.0f, r);
			v.X = -r.Y;
			v.Y = r.X;
			Vector2 absV;
			absV.X = System.Math.Abs(v.X);
			absV.Y = System.Math.Abs(v.Y);

			// Separating axis for segment (Gino, p80).
			// |dot(v, p1 - c)| > dot(|v|, h)

			float maxFraction = input.MaxFraction;

			// Build a bounding box for the segment.
			AABB segmentAABB;
			{
				Vector2 t = p1 + maxFraction * (p2 - p1);
				segmentAABB.LowerBound = Vector2.Min(p1, t);
				segmentAABB.UpperBound = Vector2.Max(p1, t);
			}

			Span<int> temp = stackalloc int[256];
			SpanStack<int> stack = new SpanStack<int>(temp);
			stack.Push(this.root);

			while (stack.Count > 0)
			{
				int nodeId = stack.Pop();
				if (nodeId == TreeNode.NullNode)
					continue;

				ref TreeNode node = ref this.nodes[nodeId];

				if (AABB.TestOverlap(in node.AABB, in segmentAABB) == false)
					continue;

				// Separating axis for segment (Gino, p80).
				// |dot(v, p1 - c)| > dot(|v|, h)
				Vector2 c = node.AABB.Center;
				Vector2 h = node.AABB.Extents;
				float separation = System.Math.Abs(Vector2.Dot(v, p1 - c)) - Vector2.Dot(absV, h);
				if (separation > 0.0f)
				{
					continue;
				}

				if (node.IsLeaf)
				{
					RayCastInput subInput;
					subInput.Point1 = input.Point1;
					subInput.Point2 = input.Point2;
					subInput.MaxFraction = maxFraction;

					float value = callback(in subInput, nodeId);

					if (value == 0.0f)
					{
						// The client has terminated the ray cast.
						return;
					}

					if (value > 0.0f)
					{
						// Update segment bounding box.
						maxFraction = value;
						Vector2 t = p1 + maxFraction * (p2 - p1);
						segmentAABB.LowerBound = Vector2.Min(p1, t);
						segmentAABB.UpperBound = Vector2.Max(p1, t);
					}
				}
				else
				{
					stack.Push(node.Child1);
					stack.Push(node.Child2);
				}
			}
		}

		/// <summary>
		/// Validate this tree. For testing.
		/// </summary>
		public void Validate()
		{
			this.ValidateStructure(this.root);
			this.ValidateMetrics(this.root);

			int freeCount = 0;
			int freeIndex = this.freeList;
			while (freeIndex != TreeNode.NullNode)
			{
				Debug.Assert(0 <= freeIndex && freeIndex < this.nodes.Length);
				freeIndex = this.nodes[freeIndex].ParentOrNext;
				++freeCount;
			}

			Debug.Assert(this.GetHeight() == this.ComputeHeight());

			Debug.Assert(this.nodeCount + freeCount == this.nodes.Length);
		}

		/// <summary>
		/// Compute the height of the binary tree in O(N) time. Should not be
		/// called often.
		/// </summary>
		/// <returns></returns>
		public int GetHeight()
		{
			if (this.root == TreeNode.NullNode)
				return 0;

			return this.nodes[this.root].Height;
		}

		/// <summary>
		/// Get the maximum balance of an node in the tree. The balance is the difference
		/// in height of the two children of a node.
		/// </summary>
		/// <returns></returns>
		public int GetMaxBalance()
		{
			int maxBalance = 0;
			for (int i = 0; i < this.nodes.Length; ++i)
			{
				ref TreeNode node = ref this.nodes[i];
				if (node.Height <= 1)
				{
					continue;
				}

				Debug.Assert(node.IsLeaf == false);

				int child1 = node.Child1;
				int child2 = node.Child2;
				int balance = System.Math.Abs(this.nodes[child2].Height - this.nodes[child1].Height);
				maxBalance = System.Math.Max(maxBalance, balance);
			}

			return maxBalance;
		}

		/// <summary>
		/// Get the ratio of the sum of the node areas to the root area.
		/// </summary>
		/// <returns></returns>
		public float GetAreaRatio()
		{
			if (this.root == TreeNode.NullNode)
			{
				return 0.0f;
			}

			ref TreeNode root = ref this.nodes[this.root];
			float rootArea = root.AABB.Perimeter;

			float totalArea = 0.0f;
			for (int i = 0; i < this.nodes.Length; ++i)
			{
				ref TreeNode node = ref this.nodes[i];
				if (node.Height < 0)
				{
					// Free node in pool
					continue;
				}

				totalArea += node.AABB.Perimeter;
			}

			return totalArea / rootArea;
		}

		/// <summary>
		/// Build an optimal tree. Very expensive. For testing.
		/// </summary>
		public void RebuildBottomUp()
		{
			int[] nodes = new int[this.nodeCount];
			int count = 0;

			// Build array of leaves. Free the rest.
			for (int i = 0; i < this.nodes.Length; ++i)
			{
				if (this.nodes[i].Height < 0)
				{
					// free node in pool
					continue;
				}

				if (this.nodes[i].IsLeaf)
				{
					this.nodes[i].ParentOrNext = TreeNode.NullNode;
					nodes[count] = i;
					++count;
				}
				else
				{
					this.FreeNode(i);
				}
			}

			while (count > 1)
			{
				float minCost = float.MaxValue;
				int iMin = -1, jMin = -1;
				for (int i = 0; i < count; ++i)
				{
					AABB aabbi = this.nodes[nodes[i]].AABB;

					for (int j = i + 1; j < count; ++j)
					{
						AABB aabbj = this.nodes[nodes[j]].AABB;
						AABB.Combine(in aabbi, in aabbj, out AABB b);
						float cost = b.Perimeter;
						if (cost < minCost)
						{
							iMin = i;
							jMin = j;
							minCost = cost;
						}
					}
				}

				int index1 = nodes[iMin];
				int index2 = nodes[jMin];
				ref TreeNode child1 = ref this.nodes[index1];
				ref TreeNode child2 = ref this.nodes[index2];

				int parentIndex = this.AllocateNode();
				ref TreeNode parent = ref this.nodes[parentIndex];
				parent.Child1 = index1;
				parent.Child2 = index2;
				parent.Height = 1 + System.Math.Max(child1.Height, child2.Height);
				parent.AABB.Combine(in child1.AABB, in child2.AABB);
				parent.ParentOrNext = TreeNode.NullNode;

				child1.ParentOrNext = parentIndex;
				child2.ParentOrNext = parentIndex;

				nodes[jMin] = nodes[count - 1];
				nodes[iMin] = parentIndex;
				--count;
			}

			this.root = nodes[0];

			this.Validate();
		}

		public void ShiftOrigin(Vector2 newOrigin)
		{
			for (int i = 0; i < this.nodes.Length; ++i)
			{
				this.nodes[i].AABB.LowerBound -= newOrigin;
				this.nodes[i].AABB.UpperBound -= newOrigin;
			}
		}

		#endregion
	}
}