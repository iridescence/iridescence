﻿namespace Iridescence.Physics2D.Collision
{
	/// Used to warm start b2Math.b2Distance.
	/// Set count to zero on first call.
	public struct SimplexCache
	{
		#region Fields

		/// <summary>
		/// Length or area.
		/// </summary>
		public float Metric;

		/// <summary>
		/// The number of vertices.
		/// </summary>
		public int Count;

		/// <summary>
		/// Vertices on shape A.
		/// </summary>
		public Array3<int> IndexA;

		/// <summary>
		/// Vertices on shape B.
		/// </summary>
		public Array3<int> IndexB;

		#endregion
	}
}
