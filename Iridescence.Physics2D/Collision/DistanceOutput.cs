﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// Output for b2Distance.
	/// </summary>
	public struct DistanceOutput
	{
		#region Fields

		/// <summary>
		/// Closest point on shapeA.
		/// </summary>
		public Vector2 PointA;

		/// <summary>
		/// Closest point on shapeB.
		/// </summary>
		public Vector2 PointB;

		/// <summary>
		/// The distance.
		/// </summary>
		public float Distance;

		/// <summary>
		/// Number of GJK iterations used.
		/// </summary>
		public int Iterations;

		#endregion
	}
}
