using System.Runtime.InteropServices;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// The features that intersect to form the contact point
	/// This must be 4 bytes or less.
	/// </summary>
	[StructLayout(LayoutKind.Explicit)]
	public struct ContactFeature
	{
		#region Fields

		[FieldOffset(0)] public uint Hash;

		/// <summary>
		/// Feature index on shapeA.
		/// </summary>
		[FieldOffset(0)] public byte IndexA;

		/// <summary>
		/// Feature index on shapeB.
		/// </summary>
		[FieldOffset(1)] public byte IndexB;

		/// <summary>
		/// The feature type on shapeA.
		/// </summary>
		[FieldOffset(2)] public ContactFeatureType TypeA;

		/// <summary>
		/// The feature type on shapeB.
		/// </summary>
		[FieldOffset(3)] public ContactFeatureType TypeB;

		#endregion

		#region Constructors

		public ContactFeature(byte iA, byte iB, ContactFeatureType tA, ContactFeatureType tB)
		{
			this.Hash = 0;
			this.IndexA = iA;
			this.IndexB = iB;
			this.TypeA = tA;
			this.TypeB = tB;
		}

		#endregion

		#region Methods

		public override int GetHashCode()
		{
			return unchecked((int)this.Hash);
		}

		public override bool Equals(object o)
		{
			ContactFeature bcf = (ContactFeature)o;
			return (this.IndexA == bcf.IndexA && this.IndexB == bcf.IndexB && this.TypeA == bcf.TypeA && this.TypeB == bcf.TypeB);
		}

		public bool Equals(in ContactFeature bcf)
		{
			return (this.IndexA == bcf.IndexA && this.IndexB == bcf.IndexB && this.TypeA == bcf.TypeA && this.TypeB == bcf.TypeB);
		}

		#endregion
	}
}
