using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// A distance proxy is used by the GJK algorithm.
	/// It encapsulates any shape.
	/// </summary>
	public struct DistanceProxy
	{
		#region Fields

		internal readonly Vector2[] vertices;
		internal readonly float radius;

		#endregion

		#region Properties

		public IReadOnlyList<Vector2> Vertices => this.vertices;
		
		public float Radius => this.radius;

		#endregion

		#region Constructors

		public DistanceProxy(Vector2[] vertices, float radius)
		{
			this.vertices = vertices;
			this.radius = radius;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Finds the support vertex and returns the index.
		/// The vertex is stored in "result."
		/// </summary>
		/// <param name="d"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public int GetSupport(in Vector2 d, out Vector2 result)
		{
			int bestIndex = 0;

			var v = this.vertices[0];
			float bestValue = v.X * d.X + v.Y * d.Y;

			for (int i = 1; i < this.vertices.Length; ++i)
			{
				v = this.vertices[i];

				float value = v.X * d.X + v.Y * d.Y;

				if (value > bestValue)
				{
					bestIndex = i;
					bestValue = value;
				}
			}

			result = this.vertices[bestIndex];
			return bestIndex;
		}

		#endregion
	}
}
