﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// Ray-cast input data. The ray extends from p1 to p1 + maxFraction * (p2 - p1).
	/// </summary>
	public struct RayCastInput
	{
		#region Fields

		public Vector2 Point1;
		public Vector2 Point2;
		public float MaxFraction;
		
		#endregion
	}
}
