using System;
using Iridescence.Math;
using Iridescence.Physics2D.Collision.Shapes;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// This class collides and edge and a polygon, taking into account edge adjacency.
	/// </summary>
	internal ref struct EPCollider
	{
		#region Nested Types

		private enum EPAxisType
		{
			Unknown,
			EdgeA,
			EdgeB
		}

		// This structure is used to keep track of the best separating axis.
		private struct EPAxis
		{
			public EPAxisType Type;
			public int Index;
			public float Separation;
		}

		// Reference face used for clipping
		private struct ReferenceFace
		{
			public int Index1, Index2;

			public Vector2 Vertex1, Vertex2;

			public Vector2 Normal;

			public Vector2 SideNormal1;
			public float SideOffset1;

			public Vector2 SideNormal2;
			public float SideOffset2;
		}

		// This holds polygon B expressed in frame A.
		private ref struct TempPolygon
		{
			public Span<Vector2> Vertices;
			public Span<Vector2> Normals;
			public int Capacity;
			public int Count;
		}
		
		#endregion

		#region Fields

		private TempPolygon polygonB;
		private Transform transform;
		private Vector2 centroidB;
		private Vector2 v0, v1, v2, v3;
		private Vector2 normal0, normal1, normal2;
		private Vector2 normal;
		private Vector2 lowerLimit, upperLimit;
		private float radius;
		private bool front;
		
		#endregion

		#region Properties

		#endregion

		#region Constructors

		public EPCollider(int capacity, Span<Vector2> mem)
		{
			this = default;
			this.polygonB.Capacity = capacity;
			this.polygonB.Vertices = mem.Slice(0, capacity);
			this.polygonB.Normals = mem.Slice(capacity, capacity);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Algorithm:
		/// 1. Classify v1 and v2
		/// 2. Classify polygon centroid as front or back
		/// 3. Flip normal if necessary
		/// 4. Initialize normal range to [-pi, pi] about face normal
		/// 5. Adjust normal range according to adjacent edges
		/// 6. Visit each separating axes, only accept axes within the range
		/// 7. Return if _any_ axis indicates separation
		/// 8. Clip
		/// </summary>
		/// <param name="manifold"></param>
		/// <param name="edgeA"></param>
		/// <param name="xfA"></param>
		/// <param name="polygonB"></param>
		/// <param name="xfB"></param>
		public void Collide(Manifold manifold, EdgeShape edgeA, in Transform xfA, PolygonShape polygonB, in Transform xfB)
		{
			Transform.MultiplyTransposed(xfA, xfB, out this.transform);

			Transform.Multiply(this.transform, polygonB.centroid, out this.centroidB);

			this.v0 = edgeA.prevVertex;
			this.v1 = edgeA.vertex1;
			this.v2 = edgeA.vertex2;
			this.v3 = edgeA.nextVertex;

			bool hasVertex0 = edgeA.hasPrevVertex;
			bool hasVertex3 = edgeA.hasNextVertex;

			Vector2.Subtract(this.v2, this.v1, out Vector2 edge1);
			Vector2.Normalize(edge1, out edge1);

			this.normal1.X = edge1.Y;
			this.normal1.Y = -edge1.X;

			Vector2.Subtract(this.centroidB, this.v1, out Vector2 cenMinusV1);

			float offset1 = Vector2.Dot(this.normal1, cenMinusV1);
			float offset0 = 0.0f, offset2 = 0.0f;
			bool convex1 = false, convex2 = false;

			// Is there a preceding edge?
			if (hasVertex0)
			{
				Vector2 edge0;// = m_v1 - m_v0;
				edge0.X = this.v1.X - this.v0.X;
				edge0.Y = this.v1.Y - this.v0.Y;
				Vector2.Normalize(edge0, out edge0);
				this.normal0 = new Vector2(edge0.Y, -edge0.X);
				convex1 = Vector2.Cross(edge0, edge1) >= 0.0f;
				Vector2 cenMinusV0;// = m_centroidB - m_v0;
				cenMinusV0.X = this.centroidB.X - this.v0.X;
				cenMinusV0.Y = this.centroidB.Y - this.v0.Y;
				offset0 = this.normal0.X * cenMinusV0.X + this.normal0.Y * cenMinusV0.Y; // b2Math.b2Dot(ref m_normal0, ref cenMinusV0);
			}

			// Is there a following edge?
			if (hasVertex3)
			{
				Vector2 edge2;// = m_v3 - m_v2;
				edge2.X = this.v3.X - this.v2.X;
				edge2.Y = this.v3.Y - this.v2.Y;
				Vector2.Normalize(edge2, out edge2);
				this.normal2 = new Vector2(edge2.Y, -edge2.X);
				convex2 = Vector2.Cross(edge1, edge2) > 0.0f;
				Vector2 tmp;
				tmp.X = this.centroidB.X - this.v2.X;
				tmp.Y = this.centroidB.Y - this.v2.Y;
				offset2 = this.normal2.X * tmp.X + this.normal2.Y * tmp.Y;// b2Math.b2Dot(m_normal2, m_centroidB - m_v2);
			}

			// Determine front or back collision. Determine collision normal limits.
			if (hasVertex0 && hasVertex3)
			{
				if (convex1 && convex2)
				{
					this.front = offset0 >= 0.0f || offset1 >= 0.0f || offset2 >= 0.0f;
					if (this.front)
					{
						this.normal = this.normal1;
						this.lowerLimit = this.normal0;
						this.upperLimit = this.normal2;
					}
					else
					{
						this.normal = -this.normal1;
						this.lowerLimit = -this.normal1;
						this.upperLimit = -this.normal1;
					}
				}
				else if (convex1)
				{
					this.front = offset0 >= 0.0f || (offset1 >= 0.0f && offset2 >= 0.0f);
					if (this.front)
					{
						this.normal = this.normal1;
						this.lowerLimit = this.normal0;
						this.upperLimit = this.normal1;
					}
					else
					{
						this.normal = -this.normal1;
						this.lowerLimit = -this.normal2;
						this.upperLimit = -this.normal1;
					}
				}
				else if (convex2)
				{
					this.front = offset2 >= 0.0f || (offset0 >= 0.0f && offset1 >= 0.0f);
					if (this.front)
					{
						this.normal = this.normal1;
						this.lowerLimit = this.normal1;
						this.upperLimit = this.normal2;
					}
					else
					{
						this.normal = -this.normal1;
						this.lowerLimit = -this.normal1;
						this.upperLimit = -this.normal0;
					}
				}
				else
				{
					this.front = offset0 >= 0.0f && offset1 >= 0.0f && offset2 >= 0.0f;
					if (this.front)
					{
						this.normal = this.normal1;
						this.lowerLimit = this.normal1;
						this.upperLimit = this.normal1;
					}
					else
					{
						this.normal = -this.normal1;
						this.lowerLimit = -this.normal2;
						this.upperLimit = -this.normal0;
					}
				}
			}
			else if (hasVertex0)
			{
				if (convex1)
				{
					this.front = offset0 >= 0.0f || offset1 >= 0.0f;
					if (this.front)
					{
						this.normal = this.normal1;
						this.lowerLimit = this.normal0;
						this.upperLimit = -this.normal1;
					}
					else
					{
						this.normal = -this.normal1;
						this.lowerLimit = this.normal1;
						this.upperLimit = -this.normal1;
					}
				}
				else
				{
					this.front = offset0 >= 0.0f && offset1 >= 0.0f;
					if (this.front)
					{
						this.normal = this.normal1;
						this.lowerLimit = this.normal1;
						this.upperLimit = -this.normal1;
					}
					else
					{
						this.normal = -this.normal1;
						this.lowerLimit = this.normal1;
						this.upperLimit = -this.normal0;
					}
				}
			}
			else if (hasVertex3)
			{
				if (convex2)
				{
					this.front = offset1 >= 0.0f || offset2 >= 0.0f;
					if (this.front)
					{
						this.normal = this.normal1;
						this.lowerLimit = -this.normal1;
						this.upperLimit = this.normal2;
					}
					else
					{
						this.normal = -this.normal1;
						this.lowerLimit = -this.normal1;
						this.upperLimit = this.normal1;
					}
				}
				else
				{
					this.front = offset1 >= 0.0f && offset2 >= 0.0f;
					if (this.front)
					{
						this.normal = this.normal1;
						this.lowerLimit = -this.normal1;
						this.upperLimit = this.normal1;
					}
					else
					{
						this.normal = -this.normal1;
						this.lowerLimit = -this.normal2;
						this.upperLimit = this.normal1;
					}
				}
			}
			else
			{
				this.front = offset1 >= 0.0f;
				if (this.front)
				{
					this.normal = this.normal1;
					this.lowerLimit = -this.normal1;
					this.upperLimit = -this.normal1;
				}
				else
				{
					this.normal = -this.normal1;
					this.lowerLimit = this.normal1;
					this.upperLimit = this.normal1;
				}
			}

			// Get polygonB frameA
			ReadOnlySpan<Vector2> vertices = polygonB.Vertices.Span;
			ReadOnlySpan<Vector2> normals = polygonB.Normals.Span;
			this.polygonB.Count = vertices.Length;

			if (this.polygonB.Count > this.polygonB.Capacity)
			{
				this.polygonB.Capacity = this.polygonB.Count;
				this.polygonB.Vertices = new Vector2[this.polygonB.Capacity];
				this.polygonB.Normals = new Vector2[this.polygonB.Capacity];
			}

			for (int i = 0; i < vertices.Length; ++i)
			{
				Transform.Multiply(this.transform, vertices[i], out this.polygonB.Vertices[i]);
				Rotation.Multiply(this.transform.Q, normals[i], out this.polygonB.Normals[i]);
			}

			this.radius = polygonB.radius + edgeA.radius;

			manifold.Points.CountInternal = 0;

			this.computeEdgeSeparation(out EPAxis edgeAxis);

			// If no valid normal can be found than this edge should not collide.
			if (edgeAxis.Type == EPAxisType.Unknown)
				return;

			if (edgeAxis.Separation > this.radius)
				return;

			this.computePolygonSeparation(out EPAxis polygonAxis);
			if (polygonAxis.Type != EPAxisType.Unknown && polygonAxis.Separation > this.radius)
				return;

			// Use hysteresis for jitter reduction.
			const float k_relativeTol = 0.98f;
			const float k_absoluteTol = 0.001f;

			EPAxis primaryAxis;
			if (polygonAxis.Type == EPAxisType.Unknown)
				primaryAxis = edgeAxis;
			else if (polygonAxis.Separation > k_relativeTol * edgeAxis.Separation + k_absoluteTol)
				primaryAxis = polygonAxis;
			else
				primaryAxis = edgeAxis;

			Array2<ClipVertex> ie = new Array2<ClipVertex>();
			ReferenceFace rf;
			if (primaryAxis.Type == EPAxisType.EdgeA)
			{
				manifold.Type = ManifoldType.FaceA;

				// Search for the polygon normal that is most anti-parallel to the edge normal.
				int bestIndex = 0;
				float bestValue = Vector2.Dot(this.normal, this.polygonB.Normals[0]);
				for (int i = 1; i < this.polygonB.Count; ++i)
				{
					float value = Vector2.Dot(this.normal, this.polygonB.Normals[i]);
					if (value < bestValue)
					{
						bestValue = value;
						bestIndex = i;
					}
				}

				int i1 = bestIndex;
				int i2 = i1 + 1 < this.polygonB.Count ? i1 + 1 : 0;

				ie.A.Vertex = this.polygonB.Vertices[i1];
				ie.A.Feature.IndexA = 0;
				ie.A.Feature.IndexB = (byte)i1;
				ie.A.Feature.TypeA = ContactFeatureType.Face;
				ie.A.Feature.TypeB = ContactFeatureType.Vertex;

				ie.B.Vertex = this.polygonB.Vertices[i2];
				ie.B.Feature.IndexA = 0;
				ie.B.Feature.IndexB = (byte)i2;
				ie.B.Feature.TypeA = ContactFeatureType.Face;
				ie.B.Feature.TypeB = ContactFeatureType.Vertex;

				if (this.front)
				{
					rf.Index1 = 0;
					rf.Index2 = 1;
					rf.Vertex1 = this.v1;
					rf.Vertex2 = this.v2;
					rf.Normal = this.normal1;
				}
				else
				{
					rf.Index1 = 1;
					rf.Index2 = 0;
					rf.Vertex1 = this.v2;
					rf.Vertex2 = this.v1;
					rf.Normal = -this.normal1;
				}
			}
			else
			{
				manifold.Type = ManifoldType.FaceB;

				ie.A.Vertex = this.v1;
				ie.A.Feature.IndexA = 0;
				ie.A.Feature.IndexB = (byte)primaryAxis.Index;
				ie.A.Feature.TypeA = ContactFeatureType.Vertex;
				ie.A.Feature.TypeB = ContactFeatureType.Face;

				ie.B.Vertex = this.v2;
				ie.B.Feature.IndexA = 0;
				ie.B.Feature.IndexB = (byte)primaryAxis.Index;
				ie.B.Feature.TypeA = ContactFeatureType.Vertex;
				ie.B.Feature.TypeB = ContactFeatureType.Face;

				rf.Index1 = primaryAxis.Index;
				rf.Index2 = rf.Index1 + 1 < this.polygonB.Count ? rf.Index1 + 1 : 0;
				rf.Vertex1 = this.polygonB.Vertices[rf.Index1];
				rf.Vertex2 = this.polygonB.Vertices[rf.Index2];
				rf.Normal = this.polygonB.Normals[rf.Index1];
			}

			rf.SideNormal1 = new Vector2(rf.Normal.Y, -rf.Normal.X);
			rf.SideNormal2 = -rf.SideNormal1;
			rf.SideOffset1 = Vector2.Dot(rf.SideNormal1, rf.Vertex1);
			rf.SideOffset2 = Vector2.Dot(rf.SideNormal2, rf.Vertex2);

			// Clip incident edge against extruded edge1 side edges.
			Array2<ClipVertex> clipPoints1 = new Array2<ClipVertex>();
			Array2<ClipVertex> clipPoints2 = new Array2<ClipVertex>();

			// Clip to box side 1
			int np = Collisions.ClipSegmentToLine(ref clipPoints1, ie, rf.SideNormal1, rf.SideOffset1, (byte)rf.Index1);
			if (np < Settings.MaxManifoldPoints)
				return;


			// Clip to negative box side 1
			np = Collisions.ClipSegmentToLine(ref clipPoints2, clipPoints1, rf.SideNormal2, rf.SideOffset2, (byte)rf.Index2);
			if (np < Settings.MaxManifoldPoints)
				return;

			// Now clipPoints2 contains the clipped points.
			if (primaryAxis.Type == EPAxisType.EdgeA)
			{
				manifold.LocalNormal = rf.Normal;
				manifold.LocalPoint = rf.Vertex1;
			}
			else
			{
				manifold.LocalNormal = polygonB.normals[rf.Index1];
				manifold.LocalPoint = polygonB.vertices[rf.Index1];
			}

			int pointCount = 0;
			for (int i = 0; i < Settings.MaxManifoldPoints; ++i)
			{
				Vector2 v = clipPoints2[i].Vertex;
				float separation = Vector2.Dot(rf.Normal, v - rf.Vertex1);
				if (separation <= this.radius)
				{
					ref ManifoldPoint cp = ref manifold.Points.Ref(pointCount);

					if (primaryAxis.Type == EPAxisType.EdgeA)
					{
						Transform.MultiplyTransposed(this.transform, v, out cp.LocalPoint);
						cp.Id = clipPoints2[i].Feature;
					}
					else
					{
						cp.LocalPoint = clipPoints2[i].Vertex;
						cp.Id.TypeA = clipPoints2[i].Feature.TypeB;
						cp.Id.TypeB = clipPoints2[i].Feature.TypeA;
						cp.Id.IndexA = clipPoints2[i].Feature.IndexB;
						cp.Id.IndexB = clipPoints2[i].Feature.IndexA;
					}

					++pointCount;
				}
			}

			manifold.Points.CountInternal = pointCount;
		}

		private void computeEdgeSeparation(out EPAxis axis)
		{
			axis.Type = EPAxisType.EdgeA;
			axis.Index = this.front ? 0 : 1;
			axis.Separation = float.MaxValue;

			for (int i = 0; i < this.polygonB.Count; ++i)
			{
				float s = Vector2.Dot(this.normal, this.polygonB.Vertices[i] - this.v1);
				if (s < axis.Separation)
					axis.Separation = s;
			}
		}

		private void computePolygonSeparation(out EPAxis axis)
		{
			axis.Type = EPAxisType.Unknown;
			axis.Index = -1;
			axis.Separation = -float.MaxValue;

			Vector2 perp = new Vector2(-this.normal.Y, this.normal.X);

			for (int i = 0; i < this.polygonB.Count; ++i)
			{
				Vector2 n = -this.polygonB.Normals[i];

				float s1 = Vector2.Dot(n, this.polygonB.Vertices[i] - this.v1);
				float s2 = Vector2.Dot(n, this.polygonB.Vertices[i] - this.v2);
				float s = System.Math.Min(s1, s2);

				if (s > this.radius)
				{
					// No collision
					axis.Type = EPAxisType.EdgeB;
					axis.Index = i;
					axis.Separation = s;
					return;
				}

				// Adjacency
				if (Vector2.Dot(n, perp) >= 0.0f)
				{
					if (Vector2.Dot(n - this.upperLimit, this.normal) < -Settings.AngularSlop)
						continue;
				}
				else
				{
					if (Vector2.Dot(n - this.lowerLimit, this.normal) < -Settings.AngularSlop)
						continue;
				}

				if (s > axis.Separation)
				{
					axis.Type = EPAxisType.EdgeB;
					axis.Index = i;
					axis.Separation = s;
				}
			}
		}

		#endregion
	}
}
