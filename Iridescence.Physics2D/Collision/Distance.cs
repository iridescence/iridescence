﻿using System;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	public static class Distance
	{
		#region Methods

		/// <summary>
		/// Compute the closest points between two shapes. Supports any combination of:
		/// b2CircleShape, b2PolygonShape, b2EdgeShape. The simplex cache is input/output.
		/// On the first call set b2SimplexCache.count to zero.
		/// </summary>
		/// <param name="output"></param>
		/// <param name="cache"></param>
		/// <param name="input"></param>
		public static void ComputeDistance(out DistanceOutput output, ref SimplexCache cache, in DistanceInput input)
		{
			// Initialize the simplex.
			Span<SimplexVertex> vertices = stackalloc SimplexVertex[3];
			Simplex simplex = new Simplex(vertices);
			simplex.ReadCache(in cache, in input.ProxyA, in input.TransformA, in input.ProxyB, in input.TransformB);

			const int maxIters = 20;

			// These store the vertices of the last simplex so that we
			// can check for duplicates and prevent cycling.
			Array3<int> saveA = new Array3<int>();
			Array3<int> saveB = new Array3<int>();

			// Main iteration loop.
			int iter = 0;
			while (iter < maxIters)
			{
				// Copy simplex so we can identify duplicates.
				int saveCount = simplex.Count;
				for (int i = 0; i < saveCount; ++i)
				{
					saveA[i] = vertices[i].IndexA;
					saveB[i] = vertices[i].IndexB;
				}

				switch (simplex.Count)
				{
					case 1:
						break;

					case 2:
						simplex.Solve2();
						break;

					case 3:
						simplex.Solve3();
						break;

					default:
						throw new Exception("Invalid simplex count.");
				}

				// If we have 3 points, then the origin is in the corresponding triangle.
				if (simplex.Count == 3)
				{
					break;
				}

				// Get search direction.
				simplex.GetSearchDirection(out Vector2 d);

				// Ensure the search direction is numerically fit.
				if ((d.X * d.X + d.Y * d.Y) < float.Epsilon)
				{
					// The origin is probably contained by a line segment
					// or triangle. Thus the shapes are overlapped.

					// We can't return zero here even though there may be overlap.
					// In case the simplex is a point, segment, or triangle it is difficult
					// to determine if the origin is contained in the CSO or very close to it.
					break;
				}

				// Compute a tentative new simplex vertex using support points.
				ref SimplexVertex vertex = ref vertices[simplex.Count];

				var q = input.TransformA.Q;

				Vector2 b;
				b.X = q.C * -d.X + q.S * -d.Y;
				b.Y = -q.S * -d.X + q.C * -d.Y;

				vertex.IndexA = input.ProxyA.GetSupport(b, out Vector2 vA);

				vertex.SupportA.X = (q.C * vA.X - q.S * vA.Y) + input.TransformA.P.X;
				vertex.SupportA.Y = (q.S * vA.X + q.C * vA.Y) + input.TransformA.P.Y;

				//                Vector2 wBLocal = new Vector2();
				q = input.TransformB.Q;
				b.X = q.C * d.X + q.S * d.Y;
				b.Y = -q.S * d.X + q.C * d.Y;

				vertex.IndexB = input.ProxyB.GetSupport(b, out Vector2 vB);

				vertex.SupportB.X = (input.TransformB.Q.C * vB.X - input.TransformB.Q.S * vB.Y) + input.TransformB.P.X;
				vertex.SupportB.Y = (input.TransformB.Q.S * vB.X + input.TransformB.Q.C * vB.Y) + input.TransformB.P.Y;

				vertex.Distance.X = vertex.SupportB.X - vertex.SupportA.X;
				vertex.Distance.Y = vertex.SupportB.Y - vertex.SupportA.Y;

				// Iteration count is equated to the number of support point calls.
				++iter;

				// Check for duplicate support points. This is the main termination criteria.
				bool duplicate = false;
				for (int i = 0; i < saveCount; ++i)
				{
					if (vertex.IndexA == saveA[i] && vertex.IndexB == saveB[i])
					{
						duplicate = true;
						break;
					}
				}

				// If we found a duplicate support point we must exit to avoid cycling.
				if (duplicate)
					break;

				// New vertex is ok and needed.
				++simplex.Count;
			}

			// Prepare output.
			simplex.GetWitnessPoints(out output.PointA, out output.PointB);
			output.Distance = Vector2.Distance(output.PointA, output.PointB);
			output.Iterations = iter;

			// Cache the simplex.
			simplex.WriteCache(ref cache);

			// Apply radii if requested.
			if (input.UseRadii)
			{
				float rA = input.ProxyA.radius;
				float rB = input.ProxyB.radius;

				if (output.Distance > rA + rB && output.Distance > float.Epsilon)
				{
					// Shapes are still not overlapped.
					// Move the witness points to the outer surface.
					output.Distance -= rA + rB;
					Vector2 normal;
					normal.X = output.PointB.X - output.PointA.X;
					normal.Y = output.PointB.Y - output.PointA.Y;

					Vector2.Normalize(in normal, out normal);

					output.PointA.X += rA * normal.X;
					output.PointA += rA * normal;

					output.PointB.X -= rB * normal.X;
					output.PointB.Y -= rB * normal.Y;
				}
				else
				{
					// Shapes are overlapped when radii are considered.
					// Move the witness points to the middle.
					Vector2 p;
					p.X = 0.5f * (output.PointA.X + output.PointB.X);
					p.Y = 0.5f * (output.PointA.Y + output.PointB.Y);

					output.PointA = p;
					output.PointB = p;
					output.Distance = 0.0f;
				}
			}
		}

		#endregion
	}
}
