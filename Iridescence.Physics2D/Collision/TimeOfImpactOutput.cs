﻿namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// Output parameters for b2TimeOfImpact.
	/// </summary>
	internal struct TimeOfImpactOutput
	{
		#region Fields

		public ImpactState State;
		public float T;

		#endregion
	}
}
