﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// Represents a collection of manifold points.
	/// </summary>
	public struct ManifoldPointCollection<T> : IReadOnlyList<T>
	{
		#region Fields

		internal int CountInternal;
		internal T Item0;
		internal T Item1;

		#endregion

		#region Properties

		public int Count => this.CountInternal;

		public T this[int index]
		{
			get
			{
				if (unchecked((uint)index) >= this.CountInternal)
					throw new IndexOutOfRangeException(nameof(index));

				switch (index)
				{
					case 0:
						return this.Item0;
					case 1:
						return this.Item1;
					default:
						throw new IndexOutOfRangeException(nameof(index));
				}
			}
			internal set
			{
				if (unchecked((uint)index) >= this.CountInternal)
					throw new IndexOutOfRangeException(nameof(index));

				switch (index)
				{
					case 0:
						this.Item0 = value;
						break;
					case 1:
						this.Item1 = value;
						break;
					default:
						throw new IndexOutOfRangeException(nameof(index));
				}
			}
		}

		#endregion
		
		#region Constructors
		
		#endregion
		
		#region Methods

		public IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < this.Count; ++i)
				yield return this[i];
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}

	internal static class ManifoldPointCollectionExtensions
	{
		internal static ref T Ref<T>(this ref ManifoldPointCollection<T> collection, int index)
		{
			Debug.Assert(index < Settings.MaxManifoldPoints);
			return ref Unsafe.Add(ref collection.Item0, index);
		}
	}
}
