﻿namespace Iridescence.Physics2D.Collision
{
	public delegate void PairCallback(object userDataA, object userDataB);

	public delegate float RayCastCallback(in RayCastInput input, int nodeId);

	public delegate bool QueryCallback(int nodeId);
}
