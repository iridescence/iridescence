using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// A manifold for two touching convex shapes.
	/// Box2D supports multiple types of contact:
	/// - clip point versus plane with radius
	/// - point versus point with radius (circles)
	/// We store contacts in this way so that position correction can account for movement, which is critical for continuous physics.
	/// All contact scenarios must be expressed in one of these types. This structure is stored across time steps, so we keep it small.
	/// </summary>
	public sealed class Manifold
	{
		#region Fields

		public ManifoldType Type;

		/// <summary>
		/// The points of contact
		/// </summary>
		public ManifoldPointCollection<ManifoldPoint> Points;

		/// <summary>
		/// The local normal. Depends on the <see cref="Type"/>:
		/// - <see cref="ManifoldType.Circles"/>: Not used.
		/// - <see cref="ManifoldType.FaceA"/>: the normal on polygonA
		/// - <see cref="ManifoldType.FaceB"/>: the normal on polygonB
		/// </summary>
		public Vector2 LocalNormal;

		/// <summary>
		/// The local point. Depends on the <see cref="Type"/>:
		/// - <see cref="ManifoldType.Circles"/>: the local center of circleA
		/// - <see cref="ManifoldType.FaceA"/>: the center of faceA
		/// - <see cref="ManifoldType.FaceB"/>: the center of faceB
		/// </summary>
		public Vector2 LocalPoint;
		
		#endregion

		#region Constructors

		public Manifold()
		{
			
		}

		#endregion

		#region Methods

		public void CopyFrom(Manifold other)
		{
			this.Type = other.Type;

			this.Points = other.Points;
			this.LocalNormal = other.LocalNormal;
			this.LocalPoint = other.LocalPoint;
		}

		#endregion
	}
}
