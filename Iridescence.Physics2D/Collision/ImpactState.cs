﻿namespace Iridescence.Physics2D.Collision
{
	internal enum ImpactState
	{
		Unknown,
		Failed,
		Overlapped,
		Touching,
		Separated
	}
}
