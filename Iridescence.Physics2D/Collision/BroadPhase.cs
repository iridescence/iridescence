using System;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// The broad-phase is used for computing pairs and performing volume queries and ray casts.
	/// This broad-phase does not persist pairs. Instead, this reports potentially new pairs.
	/// It is up to the client to consume the new pairs and to track subsequent overlap.
	/// </summary>
	public sealed class BroadPhase
	{
		#region Fields

		private readonly DynamicTree tree;

		private int proxyCount;
		private int queryProxyId;

		private int[] moveBuffer;
		private int moveCount;

		private ulong[] pairBuffer;
		private int pairCount;

		#endregion

		#region Properties

		/// <summary>
		/// Get the number of proxies.
		/// </summary>
		/// <returns></returns>
		public int GetProxyCount => this.proxyCount;

		/// <summary>
		/// Get the height of the embedded tree.
		/// </summary>
		/// <returns></returns>
		public int GetTreeHeight => this.tree.GetHeight();

		/// <summary>
		/// Get the balance of the embedded tree.
		/// </summary>
		/// <returns></returns>
		public int GetTreeBalance => this.tree.GetMaxBalance();

		/// <summary>
		/// Get the quality metric of the embedded tree.
		/// </summary>
		/// <returns></returns>
		public float GetTreeQuality => this.tree.GetAreaRatio();

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new broad phase.
		/// </summary>
		public BroadPhase()
		{
			this.tree = new DynamicTree();
			this.proxyCount = 0;

			this.pairCount = 0;
			this.pairBuffer = new ulong[16];

			this.moveCount = 0;
			this.moveBuffer = new int[16];
		}

		#endregion

		#region Methods
		
		private void bufferMove(int proxyId)
		{
			if (this.moveCount == this.moveBuffer.Length)
				Array.Resize(ref this.moveBuffer, this.moveBuffer.Length * 2);

			this.moveBuffer[this.moveCount] = proxyId;
			++this.moveCount;
		}

		private void unBufferMove(int proxyId)
		{
			for (int i = 0; i < this.moveCount; ++i)
			{
				if (this.moveBuffer[i] == proxyId)
					this.moveBuffer[i] = -1;
			}
		}

		// This is called from b2DynamicTreeQuery when we are gathering pairs.
		private bool queryCallback(int proxyId)
		{
			// A proxy cannot form a pair with itself.
			if (proxyId == this.queryProxyId)
				return true;

			// Grow the pair buffer as needed.
			if (this.pairCount == this.pairBuffer.Length)
				Array.Resize(ref this.pairBuffer, this.pairBuffer.Length * 2);

			int a = System.Math.Min(proxyId, this.queryProxyId);
			int b = System.Math.Max(proxyId, this.queryProxyId);
			this.pairBuffer[this.pairCount] = ((ulong)a << 32) | (uint)b;
			++this.pairCount;

			return true;
		}

		/// <summary>
		/// Create a proxy with an initial AABB. Pairs are not reported until
		/// UpdatePairs is called.
		/// </summary>
		/// <param name="aabb"></param>
		/// <param name="userData"></param>
		/// <returns></returns>
		public int CreateProxy(in AABB aabb, object userData)
		{
			int proxyId = this.tree.CreateProxy(in aabb, userData);
			this.proxyCount++;

			this.bufferMove(proxyId);

			return proxyId;
		}

		/// <summary>
		/// Destroy a proxy. It is up to the client to remove any pairs.
		/// </summary>
		/// <param name="proxyId"></param>
		public void DestroyProxy(int proxyId)
		{
			this.unBufferMove(proxyId);

			this.proxyCount--;

			this.tree.DestroyProxy(proxyId);
		}

		/// <summary>
		/// Call MoveProxy as many times as you like, then when you are done
		/// call UpdatePairs to finalized the proxy pairs (for your time step).
		/// </summary>
		/// <param name="proxyId"></param>
		/// <param name="aabb"></param>
		/// <param name="displacement"></param>
		public void MoveProxy(int proxyId, in AABB aabb, in Vector2 displacement)
		{
			bool buffer = this.tree.MoveProxy(proxyId, in aabb, in displacement);
			if (buffer)
				this.bufferMove(proxyId);
		}

		/// <summary>
		/// Call to trigger a re-processing of it's pairs on the next call to UpdatePairs.
		/// </summary>
		/// <param name="proxyId"></param>
		public void TouchProxy(int proxyId)
		{
			this.bufferMove(proxyId);
		}

		/// <summary>
		/// Get user data from a proxy. Returns NULL if the id is invalid.
		/// </summary>
		/// <param name="proxyId"></param>
		/// <returns></returns>
		public object GetUserData(int proxyId)
		{
			return this.tree.GetUserData(proxyId);
		}

		/// <summary>
		/// Tests overlap of fat AABBs.
		/// </summary>
		/// <param name="proxyIdA"></param>
		/// <param name="proxyIdB"></param>
		/// <returns></returns>
		public bool TestOverlap(int proxyIdA, int proxyIdB)
		{
			this.tree.GetFatAABB(proxyIdA, out AABB aabbA);
			this.tree.GetFatAABB(proxyIdB, out AABB aabbB);
			return AABB.TestOverlap(in aabbA, in aabbB);
		}

		/// <summary>
		/// Returns the fat AABB for a proxy.
		/// </summary>
		/// <param name="proxyId"></param>
		/// <param name="output"></param>
		public void GetFatAABB(int proxyId, out AABB output)
		{
			this.tree.GetFatAABB(proxyId, out output);
		}

		/// <summary>
		/// Update the pairs. This results in pair callbacks. This can only add pairs.
		/// </summary>
		/// <param name="callback"></param>
		public void UpdatePairs(PairCallback callback)
		{
			// Reset pair buffer
			this.pairCount = 0;

			// Perform tree queries for all moving proxies.
			int i;
			for (i = 0; i < this.moveCount; ++i)
			{
				this.queryProxyId = this.moveBuffer[i];
				if (this.queryProxyId < 0)
					continue;

				// We have to query the tree with the fat AABB so that
				// we don't fail to create a pair that may touch later.
				this.tree.GetFatAABB(this.queryProxyId, out AABB fatAABB);

				// Query tree, create pairs and add them pair buffer.
				this.tree.Query(this.queryCallback, fatAABB);
			}

			// Reset move buffer
			this.moveCount = 0;

			// Sort the pair buffer to expose duplicates.
			Array.Sort(this.pairBuffer, 0, this.pairCount);
						
			// Send the pairs back to the client.
			i = 0;
			while (i < this.pairCount)
			{
				ulong primaryPair = this.pairBuffer[i];
				int a = unchecked((int)(primaryPair >> 32));
				int b = unchecked((int)primaryPair);
				object userDataA = this.tree.GetUserData(a);
				object userDataB = this.tree.GetUserData(b);

				callback(userDataA, userDataB);
				++i;

				// Skip any duplicate pairs.
				while (i < this.pairCount)
				{
					if (this.pairBuffer[i] != primaryPair)
						break;

					++i;
				}
			}

			// Try to keep the tree balanced.
			//m_tree.Rebalance(4);
		}

		/// <summary>
		/// Query an AABB for overlapping proxies. The callback class
		/// is called for each proxy that overlaps the supplied AABB.
		/// </summary>
		/// <param name="callback"></param>
		/// <param name="aabb"></param>
		public void Query(QueryCallback callback, AABB aabb)
		{
			this.tree.Query(callback, aabb);
		}

		/// <summary>
		/// Ray-cast against the proxies in the tree. This relies on the callback
		/// to perform a exact ray-cast in the case were the proxy contains a shape.
		/// The callback also performs the any collision filtering. This has performance
		/// roughly equal to k * log(n), where k is the number of collisions and n is the
		/// number of proxies in the tree.
		/// </summary>
		/// <param name="input">The ray-cast input data. The ray extends from p1 to p1 + maxFraction * (p2 - p1).</param>
		/// <param name="callback">A callback class that is called for each proxy that is hit by the ray.</param>
		public void RayCast(in RayCastInput input, RayCastCallback callback)
		{
			this.tree.RayCast(input, callback);
		}

		/// <summary>
		/// Shift the world origin. Useful for large worlds.
		/// The shift formula is: position -= newOrigin
		/// </summary>
		/// <param name="newOrigin">The new origin with respect to the old origin</param>
		public void ShiftOrigin(Vector2 newOrigin)
		{
			this.tree.ShiftOrigin(newOrigin);
		}

		#endregion
	}
}