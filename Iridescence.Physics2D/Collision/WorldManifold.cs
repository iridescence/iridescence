﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// This is used to compute the current state of a contact manifold.
	/// </summary>
	public readonly struct WorldManifold
	{
		#region Fields

		/// <summary>
		/// Gets the world contact points (points of intersection).
		/// </summary>
		public readonly ManifoldPointCollection<Vector2> Points;

		/// <summary>
		/// World vector pointing from A to B.
		/// </summary>
		public readonly Vector2 Normal;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Evaluate the manifold with supplied transforms. This assumes
		/// modest motion from the original state. This does not change the
		/// point count, impulses, etc. The radii must come from the shapes
		/// that generated the manifold.
		/// </summary>
		/// <param name="manifold"></param>
		/// <param name="xfA"></param>
		/// <param name="radiusA"></param>
		/// <param name="xfB"></param>
		/// <param name="radiusB"></param>
		public WorldManifold(Manifold manifold, in Transform xfA, float radiusA, in Transform xfB, float radiusB)
		{
			this.Points = new ManifoldPointCollection<Vector2>();
			this.Points.CountInternal = manifold.Points.CountInternal;
			this.Normal = default;

			if (manifold.Points.CountInternal == 0)
				return;

			switch (manifold.Type)
			{
				case ManifoldType.Circles:
				{
					Transform.Multiply(xfA, manifold.LocalPoint, out Vector2 pointA);
					Transform.Multiply(xfB, manifold.Points.Item0.LocalPoint, out Vector2 pointB);

					if (Vector2.DistanceSquared(pointA, pointB) > float.Epsilon)
					{
						Vector2.Subtract(pointB, pointA, out this.Normal);
						Vector2.Normalize(this.Normal, out this.Normal);
					}
					else
					{
						this.Normal = Vector2.UnitX;
					}

					Vector2.Multiply(this.Normal, radiusA, out Vector2 cA);
					Vector2.Add(pointA, cA, out cA);

					Vector2.Multiply(this.Normal, radiusB, out Vector2 cB);
					Vector2.Subtract(pointB, cB, out cB);

					Vector2.Add(cA, cB, out cA);
					Vector2.Multiply(cA, 0.5f, out this.Points.Item0);

					break;
				}

				case ManifoldType.FaceA:
				{
					Rotation.Multiply(xfA.Q, manifold.LocalNormal, out this.Normal);

					Transform.Multiply(xfA, manifold.LocalPoint, out Vector2 planePoint);

					for (int i = 0; i < manifold.Points.CountInternal; ++i)
					{
						Transform.Multiply(xfB, manifold.Points.Ref(i).LocalPoint, out Vector2 clipPoint);

						Vector2.Subtract(clipPoint, planePoint, out Vector2 cA);
						Vector2.Multiply(this.Normal, radiusA - Vector2.Dot(cA, this.Normal), out cA);
						Vector2.Add(clipPoint, cA, out cA);

						Vector2.Multiply(this.Normal, radiusB, out Vector2 cB);
						Vector2.Subtract(clipPoint, cB, out cB);

						Vector2.Add(cA, cB, out cA);
						Vector2.Multiply(cA, 0.5f, out this.Points.Ref(i));
					}

					break;
				}

				case ManifoldType.FaceB:
				{
					Rotation.Multiply(xfB.Q, manifold.LocalNormal, out this.Normal);

					Transform.Multiply(xfB, manifold.LocalPoint, out Vector2 planePoint);

					for (int i = 0; i < manifold.Points.CountInternal; ++i)
					{
						Transform.Multiply(xfA, manifold.Points.Ref(i).LocalPoint, out Vector2 clipPoint);

						Vector2.Subtract(clipPoint, planePoint, out Vector2 cB);
						Vector2.Multiply(this.Normal, radiusB - Vector2.Dot(cB, this.Normal), out cB);
						Vector2.Add(clipPoint, cB, out cB);

						Vector2.Multiply(this.Normal, radiusA, out Vector2 cA);
						Vector2.Subtract(clipPoint, cA, out cA);

						Vector2.Add(cA, cB, out cA);
						Vector2.Multiply(cA, 0.5f, out this.Points.Ref(i));
					}

					// Ensure normal points from A to B.
					Vector2.Negate(this.Normal, out this.Normal);

					break;
				}
			}
		}

		#endregion

		#region Methods
		
		#endregion
	}
}
