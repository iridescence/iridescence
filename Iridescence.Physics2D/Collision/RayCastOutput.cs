﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// Ray-cast output data. The ray hits at p1 + fraction * (p2 - p1), where p1 and p2
	/// come from b2RayCastInput.
	/// </summary>
	public struct RayCastOutput
	{
		#region Fields

		public Vector2 Normal;
		public float Fraction;

		public static readonly RayCastOutput Zero = new RayCastOutput();

		#endregion

		#region Constructors

		public RayCastOutput(Vector2 normal, float fraction)
		{
			this.Normal = normal;
			this.Fraction = fraction;
		}
		
		#endregion
	}
}
