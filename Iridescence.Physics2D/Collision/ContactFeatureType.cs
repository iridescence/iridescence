﻿namespace Iridescence.Physics2D.Collision
{
	public enum ContactFeatureType : byte
	{
		Vertex = 0,
		Face = 1
	}
}
