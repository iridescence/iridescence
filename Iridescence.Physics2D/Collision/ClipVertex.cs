﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// Used for computing contact manifolds.
	/// </summary>
	internal struct ClipVertex
	{
		#region Fields

		public Vector2 Vertex;
		public ContactFeature Feature;

		#endregion
	}
}
