using System;
using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision.Shapes
{
	/// <summary>
	/// A shape is used for collision detection. You can create a shape however you like.
	/// Shapes used for simulation in b2World are created automatically when a b2Fixture
	/// is created. Shapes may encapsulate a one or more child shapes.
	/// </summary>
	[Serializable]
	public abstract class Shape
	{
		#region Fields

		internal readonly float radius;

		[NonSerialized]
		private int typeID;

		private static readonly Dictionary<Type, int> typeIDs = new Dictionary<Type, int>();

		#endregion

		#region Properties

		/// <summary>
		/// Gets the radius of the shape.
		/// The radius for non-circle shape is treated as the width of the surface, like a skin around the shape.
		/// </summary>
		public float Radius => this.radius;

		/// <summary>
		/// Gets the number of child primitives.
		/// </summary>
		internal abstract int ChildCount { get; }

		/// <summary>
		/// Gets a class identifier for fast lookup of shape types.
		/// </summary>
		internal int TypeID
		{
			get
			{
				if (this.typeID == 0)
				{
					this.typeID = Shape.GetTypeId(this.GetType());
				}

				return this.typeID;
			}
		}

		#endregion

		#region Constructors

		protected Shape(float radius)
		{
			this.radius = radius;

			// for fast contact type lookup.
			this.typeID = GetTypeId(this.GetType());
		}

		#endregion

		#region Methods

		/// <summary>
		/// Test a point for containment in this shape. This only works for convex shapes.
		/// </summary>
		/// <param name="xf">the shape world transform.</param>
		/// <param name="p">a point in world coordinates.</param>
		/// <returns></returns>
		public abstract bool TestPoint(in Transform xf, Vector2 p);

		/// <summary>
		/// Cast a ray against a child shape.
		/// </summary>
		/// <param name="input">the ray-cast input parameters.</param>
		/// <param name="transform">the transform to be applied to the shape.</param>
		/// <param name="childIndex">the child shape index</param>
		/// <param name="output">the ray-cast results.</param>
		public abstract bool RayCast(in RayCastInput input, in Transform transform, int childIndex, out RayCastOutput output);

		/// <summary>
		/// Given a transform, compute the associated axis aligned bounding box for a child shape.
		/// </summary>
		/// <param name="xf">the world transform of the shape.</param>
		/// <param name="childIndex">the child shape</param>
		/// <param name="output">returns the axis aligned box.</param>
		public abstract void ComputeBoundingBox(in Transform xf, int childIndex, out AABB output);

		/// <summary>
		/// Given a transform, compute the associated axis aligned bounding box for a child shape.
		/// </summary>
		/// <param name="transform"></param>
		/// <param name="childIndex"></param>
		/// <returns></returns>
		public AABB ComputeBoundingBox(in Transform transform, int childIndex)
		{
			this.ComputeBoundingBox(in transform, childIndex, out AABB aabb);
			return aabb;
		}

		/// Compute the mass properties of this shape using its dimensions and density.
		/// The inertia tensor is computed about the local origin.
		/// <param name="density">The density in kilograms per meter squared.</param>
		/// <returns>The mass data.</returns>
		public abstract MassData ComputeMass(float density);

		/// <summary>
		/// Returns the distance proxy for the specified child.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="proxy"></param>
		/// <returns></returns>
		public abstract void GetDistanceProxy(int index, out DistanceProxy proxy);

		/// <summary>
		/// Gets the type ID for a shape type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		internal static int GetTypeId(Type type)
		{
			int typeID;

			lock (typeIDs)
			{
				if (!typeIDs.TryGetValue(type, out typeID))
				{
					typeID = typeIDs.Count + 1;
					typeIDs.Add(type, typeID);
				}
			}

			return typeID;
		}

		#endregion
	}
}
