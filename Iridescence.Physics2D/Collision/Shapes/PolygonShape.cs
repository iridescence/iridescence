using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision.Shapes
{
	/// <summary>
	/// Represents a polygon shape.
	/// </summary>
	[Serializable]
	public sealed class PolygonShape : Shape, IDeserializationCallback
	{
		#region Fields

		internal Vector2[] vertices;

		[NonSerialized] internal Vector2[] normals;
		[NonSerialized] internal Vector2 centroid;
		[NonSerialized] private DistanceProxy distanceProxy;

		[NonSerialized] private int hash;
		[NonSerialized] private bool hashComputed;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the centroid of the polygon.
		/// </summary>
		public Vector2 Centroid => this.centroid;

		/// <summary>
		/// Gets the shape's vertices.
		/// </summary>
		public ReadOnlyMemory<Vector2> Vertices => this.vertices;

		/// <summary>
		/// Gets the shape's normals.
		/// </summary>
		public ReadOnlyMemory<Vector2> Normals => this.normals;

		internal override int ChildCount => 1;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new polygon based on the specified array of vertices.
		/// </summary>
		/// <param name="vertices">The vertices of the polygon.</param>
		/// <param name="radius">The "radius" of the polygon edges.</param>
		public PolygonShape(Vector2[] vertices, float radius)
			: base(radius)
		{
			if (vertices.Length < 3)
				throw new ArgumentException("A polygon needs at least 3 vertices.");

			// Perform welding and copy vertices into local buffer.
			List<Vector2> ps = new List<Vector2>();
			for (int i = 0; i < vertices.Length; ++i)
			{
				Vector2 v = vertices[i];

				bool unique = true;
				for (int j = 0; j < ps.Count; ++j)
				{
					if (Vector2.DistanceSquared(v, ps[j]) < (0.5f * Settings.LinearSlop) * (0.5f * Settings.LinearSlop))
					{
						unique = false;
						break;
					}
				}

				if (unique)
					ps.Add(v);
			}

			int n = ps.Count;
			if (n < 3)
				throw new InvalidPolygonException();

			// Create the convex hull using the Gift wrapping algorithm
			// http://en.wikipedia.org/wiki/Gift_wrapping_algorithm

			// Find the right most point on the hull
			int i0 = 0;
			float x0 = ps[0].X;
			for (int i = 1; i < n; ++i)
			{
				float x = ps[i].X;
				if (x > x0 || (x == x0 && ps[i].Y < ps[i0].Y))
				{
					i0 = i;
					x0 = x;
				}
			}

			int[] hull = new int[n];
			int m = 0;
			int ih = i0;

			do
			{
				hull[m] = ih;

				int ie = 0;
				for (int j = 1; j < n; ++j)
				{
					if (ie == ih)
					{
						ie = j;
						continue;
					}

					Vector2 r = ps[ie] - ps[hull[m]];
					Vector2 v = ps[j] - ps[hull[m]];
					float c = Vector2.Cross(in r, in v);
					if (c < 0.0f)
						ie = j;

					// Collinearity check
					if (c == 0.0f && v.LengthSquared > r.LengthSquared)
						ie = j;
				}

				++m;
				ih = ie;
			} while (ih != i0);


			if (m < 3)
				throw new Exception("Polygon is degenerate.");

			// Copy vertices.
			this.vertices = new Vector2[m];
			for (int i = 0; i < m; ++i)
				this.vertices[i] = ps[hull[i]];


#if DEBUG
			if (!this.validate())
				throw new InvalidPolygonException();
#endif

			this.initialize();
		}

		/// <summary>
		/// Creates a new polygon based on the specified array of vertices.
		/// </summary>
		/// <param name="vertices">The vertices of the polygon.</param>
		public PolygonShape(Vector2[] vertices)
			: this(vertices, Settings.PolygonRadius)
		{
			
		}

		#endregion

		#region Methods

		private void initialize()
		{
			// compute normals.
			this.normals = new Vector2[this.vertices.Length];
			for (int i = 0; i < this.vertices.Length; ++i)
			{
				int i1 = i;
				int i2 = i + 1 < this.vertices.Length ? i + 1 : 0;
				Vector2 edge = this.vertices[i2] - this.vertices[i1];
				
				// make sure edges have non-zero length.
				if (edge.LengthSquared < float.Epsilon)
					throw new InvalidPolygonException();

				Vector2.Normalize(Vector2.Cross(in edge, 1.0f), out this.normals[i]);
			}

			// create distance proxy.
			this.distanceProxy = new DistanceProxy(this.vertices, this.radius);

			// compute the polygon centroid.
			this.centroid = ComputeCentroid(this.vertices, this.vertices.Length);
		}

		private bool validate()
		{
			for (int i = 0; i < this.vertices.Length; ++i)
			{
				int i1 = i;
				int i2 = i < this.vertices.Length - 1 ? i1 + 1 : 0;
				Vector2 p = this.vertices[i1];
				Vector2 e = this.vertices[i2] - p;

				for (int j = 0; j < this.vertices.Length; ++j)
				{
					if (j == i1 || j == i2)
						continue;

					Vector2 v = this.vertices[j] - p;
					float c = Vector2.Cross(in e, in v);
					if (c < 0.0f)
						return false;
				}
			}

			return true;
		}

		void IDeserializationCallback.OnDeserialization(object sender)
		{
			this.initialize();
		}

		/// <summary>
		/// Creates a box.
		/// </summary>
		/// <param name="halfWidth"></param>
		/// <param name="halfHeight"></param>
		/// <returns></returns>
		public static PolygonShape CreateBox(float halfWidth, float halfHeight)
		{
			return new PolygonShape(new[]
			{
				new Vector2(-halfWidth, -halfHeight),
				new Vector2(halfWidth, -halfHeight),
				new Vector2(halfWidth, halfHeight),
				new Vector2(-halfWidth, halfHeight),
			});
		}

		/// <summary>
		/// Creates a box that is offset by a vector and rotated around its center by the specified angle.
		/// </summary>
		/// <param name="halfWidth"></param>
		/// <param name="halfHeight"></param>
		/// <param name="center"></param>
		/// <param name="angle"></param>
		/// <returns></returns>
		public static PolygonShape CreateBox(float halfWidth, float halfHeight, Vector2 center, float angle)
		{
			Vector2[] v =
			{
				new Vector2(-halfWidth, -halfHeight),
				new Vector2(halfWidth, -halfHeight),
				new Vector2(halfWidth, halfHeight),
				new Vector2(-halfWidth, halfHeight),
			};

			Transform transform = new Transform(center, new Rotation(angle));
			for (int i = 0; i < 4; i++)
				Transform.Multiply(in transform, in v[i], out v[i]);

			return new PolygonShape(v);
		}

		/// <summary>
		/// Computes the centroid of the specified vector array.
		/// </summary>
		/// <param name="vs"></param>
		/// <param name="count"></param>
		/// <returns></returns>
		public static Vector2 ComputeCentroid(Vector2[] vs, int count)
		{
			Vector2 c = new Vector2();
			float area = 0.0f;

			// pRef is the reference point for forming triangles.
			// It's location doesn't change the result (except for rounding error).
			Vector2 pRef = new Vector2(0.0f, 0.0f);

			float inv3 = 1.0f / 3.0f;

			for (int i = 0; i < count; ++i)
			{
				// Triangle vertices.
				Vector2 p1 = pRef;
				Vector2 p2 = vs[i];
				Vector2 p3 = i + 1 < count ? vs[i + 1] : vs[0];

				Vector2 e1 = p2 - p1;
				Vector2 e2 = p3 - p1;

				float D = Vector2.Cross(in e1, in e2);

				float triangleArea = 0.5f * D;
				area += triangleArea;

				// Area weighted centroid
				c += triangleArea * inv3 * (p1 + p2 + p3);
			}

			// Centroid
			if (area <= float.Epsilon)
				throw new ArgumentException("Centroid is not defined, area is zero.", nameof(vs));

			c *= 1.0f / area;
			return c;
		}

		public override bool TestPoint(in Transform xf, Vector2 p)
		{
			Vector2.Subtract(p, xf.P, out Vector2 pLocal);
			Rotation.MultiplyTransposed(in xf.Q, in pLocal, out pLocal);

			for (int i = 0; i < this.vertices.Length; ++i)
			{
				float dot = Vector2.Dot(this.normals[i], pLocal - this.vertices[i]);
				if (dot > 0.0f)
				{
					return false;
				}
			}

			return true;
		}

		public override bool RayCast(in RayCastInput input, in Transform xf, int childIndex, out RayCastOutput output)
		{
			output = RayCastOutput.Zero;

			// Put the ray into the polygon's frame of reference.
			Vector2.Subtract(input.Point1, xf.P, out Vector2 p1);
			Rotation.MultiplyTransposed(in xf.Q, in p1, out p1);

			Vector2.Subtract(input.Point2, xf.P, out Vector2 p2);
			Rotation.MultiplyTransposed(in xf.Q, in p2, out p2);

			Vector2.Subtract(p2, p1, out Vector2 d);

			float lower = 0.0f, upper = input.MaxFraction;

			int index = -1;

			for (int i = 0; i < this.vertices.Length; ++i)
			{
				// p = p1 + a * d
				// dot(normal, p - v) = 0
				// dot(normal, p1 - v) + a * dot(normal, d) = 0
				float numerator = Vector2.Dot(this.normals[i], this.vertices[i] - p1);
				float denominator = Vector2.Dot(this.normals[i], d);

				if (denominator == 0.0f)
				{
					if (numerator < 0.0f)
					{
						return false;
					}
				}
				else
				{
					// Note: we want this predicate without division:
					// lower < numerator / denominator, where denominator < 0
					// Since denominator < 0, we have to flip the inequality:
					// lower < numerator / denominator <==> denominator * lower > numerator.
					if (denominator < 0.0f && numerator < lower * denominator)
					{
						// Increase lower.
						// The segment enters this half-space.
						lower = numerator / denominator;
						index = i;
					}
					else if (denominator > 0.0f && numerator < upper * denominator)
					{
						// Decrease upper.
						// The segment exits this half-space.
						upper = numerator / denominator;
					}
				}

				// The use of epsilon here causes the assert on lower to trip
				// in some cases. Apparently the use of epsilon was to make edge
				// shapes work, but now those are handled separately.
				//if (upper < lower - b2_epsilon)
				if (upper < lower)
				{
					return false;
				}
			}

			//    Debug.Assert(0.0f <= lower && lower <= input.maxFraction);

			if (index >= 0)
			{
				output.Fraction = lower;
				output.Normal = Rotation.Multiply(in xf.Q, in this.normals[index]);
				return true;
			}

			return false;
		}

		public override void ComputeBoundingBox(in Transform xf, int childIndex, out AABB output)
		{
			Transform.Multiply(in xf, in this.vertices[0], out Vector2 lower);

			Vector2 upper = lower;
			for (int i = 1; i < this.vertices.Length; ++i)
			{
				Transform.Multiply(in xf, in this.vertices[i], out Vector2 v);
				Vector2.Min(lower, v, out lower);
				Vector2.Max(upper, v, out upper);
			}

			output.LowerBound.X = lower.X - this.radius;
			output.LowerBound.Y = lower.Y - this.radius;
			output.UpperBound.X = upper.X + this.radius;
			output.UpperBound.Y = upper.Y + this.radius;
		}

		public override MassData ComputeMass(float density)
		{
			// Polygon mass, centroid, and inertia.
			// Let rho be the polygon density in mass per unit area.
			// Then:
			// mass = rho * int(dA)
			// centroid.X = (1/mass) * rho * int(x * dA)
			// centroid.Y = (1/mass) * rho * int(y * dA)
			// I = rho * int((x*x + y*y) * dA)
			//
			// We can compute these integrals by summing all the integrals
			// for each triangle of the polygon. To evaluate the integral
			// for a single triangle, we make a change of variables to
			// the (u,v) coordinates of the triangle:
			// x = x0 + e1x * u + e2x * v
			// y = y0 + e1y * u + e2y * v
			// where 0 <= u && 0 <= v && u + v <= 1.
			//
			// We integrate u from [0,1-v] and then v from [0,1].
			// We also need to use the Jacobian of the transformation:
			// D = cross(e1, e2)
			//
			// Simplification: triangle centroid = (1/3) * (p1 + p2 + p3)
			//
			// The rest of the derivation is handled by computer algebra.

			Vector2 center = default;
			
			float area = 0.0f;
			float I = 0.0f;

			// s is the reference point for forming triangles.
			// It's location doesn't change the result (except for rounding error).
			Vector2 s = new Vector2(0.0f, 0.0f);

			// This code would put the reference point inside the polygon.
			for (int i = 0; i < this.vertices.Length; ++i)
			{
				s += this.vertices[i];
			}
			s *= 1.0f / this.vertices.Length;

			float k_inv3 = 1.0f / 3.0f;

			for (int i = 0; i < this.vertices.Length; ++i)
			{
				// Triangle vertices.
				Vector2 e1 = this.vertices[i] - s;
				Vector2 e2 = i + 1 < this.vertices.Length ? this.vertices[i + 1] - s : this.vertices[0] - s;

				float D = Vector2.Cross(in e1, in e2);

				float triangleArea = 0.5f * D;
				area += triangleArea;

				// Area weighted centroid
				center += triangleArea * k_inv3 * (e1 + e2);

				float ex1 = e1.X, ey1 = e1.Y;
				float ex2 = e2.X, ey2 = e2.Y;

				float intx2 = ex1 * ex1 + ex2 * ex1 + ex2 * ex2;
				float inty2 = ey1 * ey1 + ey2 * ey1 + ey2 * ey2;

				I += (0.25f * k_inv3 * D) * (intx2 + inty2);
			}

			// Total mass
			float mass = density * area;

			// Center of mass
			if (area <= float.Epsilon)
				throw new InvalidOperationException("Area is zero in mass calculation.");

			center *= 1.0f / area;

			Vector2 massCenter = center + s;

			// Inertia tensor relative to the local origin (point s).
			// Shift to center of mass then to original body origin.
			return new MassData(mass, massCenter, density * I + mass * (Vector2.Dot(massCenter, massCenter) - Vector2.Dot(center, center)));
		}

		public override void GetDistanceProxy(int index, out DistanceProxy proxy)
		{
			if (index != 0)
				throw new IndexOutOfRangeException();

			proxy = this.distanceProxy;
		}

		public override int GetHashCode()
		{
			if (!this.hashComputed)
			{
				// Compute hash.
				this.hash = 0;

				for (int i = 0; i < this.vertices.Length; i++)
				{
					this.hash = (23 * this.hash) + this.vertices[i].X.GetHashCode();
					this.hash = (23 * this.hash) + this.vertices[i].Y.GetHashCode();
				}

				this.hashComputed = true;
			}
			return this.hash;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is PolygonShape other))
				return false;

			if (other.GetHashCode() != this.GetHashCode() || other.vertices.Length != this.vertices.Length)
				return false;

			for (int i = 0; i < this.vertices.Length; ++i)
			{
				if (this.vertices[i].X != other.vertices[i].X ||
					this.vertices[i].Y != other.vertices[i].Y)
					return false;
			}

			return true;
		}

		#endregion
	}
}
