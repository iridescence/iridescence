using System;
using System.Runtime.Serialization;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision.Shapes
{
	/// <summary>
	/// Represents a simple edge shape that connects two vertices.
	/// Optionally provides support for continuous connected edges.
	/// </summary>
	[Serializable]
	public sealed class EdgeShape : Shape, IDeserializationCallback
	{
		#region Fields

		internal readonly Vector2 vertex1;
		internal readonly Vector2 vertex2;

		internal readonly Vector2 prevVertex;
		internal readonly bool hasPrevVertex;

		internal readonly Vector2 nextVertex;
		internal readonly bool hasNextVertex;

		[NonSerialized] private DistanceProxy distanceProxy;

		[NonSerialized] private int hash;
		[NonSerialized] private bool hashComputed;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the first vertex of the edge.
		/// </summary>
		public Vector2 Vertex1 => this.vertex1;

		/// <summary>
		/// Gets the second vertex of the edge.
		/// </summary>
		public Vector2 Vertex2 => this.vertex2;

		/// <summary>
		/// Gets the previous vertex position.
		/// </summary>
		public Vector2 PreviousVertex => this.prevVertex;

		/// <summary>
		/// Gets a value that indicates whether the edge has a previous vertex.
		/// </summary>
		public bool HasPreviousVertex => this.hasPrevVertex;

		/// <summary>
		/// Gets the next vertex position.
		/// </summary>
		public Vector2 NextVertex => this.nextVertex;

		/// <summary>
		/// Gets a value that indicates whether the edge has a next vertex.
		/// </summary>
		public bool HasNextVertex => this.hasNextVertex;

		internal override int ChildCount => 1;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new edge shape with optional continuity vertices.
		/// </summary>
		/// <param name="v1">The first vertex of the edge.</param>
		/// <param name="v2">The second vertex of the edge.</param>
		/// <param name="prevVertex">The previous vertex, if the edge is part of a connected set of edges.</param>
		/// <param name="hasPrevVertex">Determines whether the previous vertex is used.</param>
		/// <param name="nextVertex">The next vertex, if the edge is part of a connected set of edges.</param>
		/// <param name="hasNextVertex">Determines whether the next vertex is used.</param>
		/// <param name="radius">The "radius" of the edge.</param>
		public EdgeShape(Vector2 v1, Vector2 v2, Vector2 prevVertex, bool hasPrevVertex, Vector2 nextVertex, bool hasNextVertex, float radius)
			: base(radius)
		{
			this.vertex1 = v1;
			this.vertex2 = v2;

			this.prevVertex = prevVertex;
			this.hasPrevVertex = hasPrevVertex;

			this.nextVertex = nextVertex;
			this.hasNextVertex = hasNextVertex;

			this.initialize();
		}

		/// <summary>
		/// Creates a new edge shape with optional continuity vertices.
		/// </summary>
		/// <param name="v1">The first vertex of the edge.</param>
		/// <param name="v2">The second vertex of the edge.</param>
		/// <param name="prevVertex">The previous vertex, if the edge is part of a connected set of edges.</param>
		/// <param name="hasPrevVertex">Determines whether the previous vertex is used.</param>
		/// <param name="nextVertex">The next vertex, if the edge is part of a connected set of edges.</param>
		/// <param name="hasNextVertex">Determines whether the next vertex is used.</param>
		public EdgeShape(Vector2 v1, Vector2 v2, Vector2 prevVertex, bool hasPrevVertex, Vector2 nextVertex, bool hasNextVertex)
			: this(v1, v2, prevVertex, hasPrevVertex, nextVertex, hasNextVertex, Settings.PolygonRadius)
		{

		}

		/// <summary>
		/// Creates a simple edge shape.
		/// </summary>
		/// <param name="v1">The first vertex of the edge.</param>
		/// <param name="v2">The second vertex of the edge.</param>
		/// <param name="radius">The "radius" of the edge.</param>
		public EdgeShape(Vector2 v1, Vector2 v2, float radius)
			: this(v1, v2, Vector2.Zero, false, Vector2.Zero, false, radius)
		{
			
		}

		/// <summary>
		/// Creates a simple edge shape.
		/// </summary>
		/// <param name="v1">The first vertex of the edge.</param>
		/// <param name="v2">The second vertex of the edge.</param>
		public EdgeShape(Vector2 v1, Vector2 v2)
			: this(v1, v2, Settings.PolygonRadius)
		{
			
		}


		#endregion

		#region Methods

		private void initialize()
		{
			this.distanceProxy = new DistanceProxy(new[] {this.vertex1, this.vertex2}, this.radius);
		}

		void IDeserializationCallback.OnDeserialization(object sender)
		{
			this.initialize();
		}

		public override bool TestPoint(in Transform xf, Vector2 p)
		{
			return false;
		}

		public override bool RayCast(in RayCastInput input, in Transform xf, int childIndex, out RayCastOutput output)
		{
			// p = p1 + t * d
			// v = v1 + s * e
			// p1 + t * d = v1 + s * e
			// s * e - t * d = p1 - v1

			output = RayCastOutput.Zero;

			// Put the ray into the edge's frame of reference.
			Vector2.Subtract(input.Point1, xf.P, out Vector2 p1);
			Rotation.MultiplyTransposed(in xf.Q, in p1, out p1);

			Vector2.Subtract(input.Point2, xf.P, out Vector2 p2);
			Rotation.MultiplyTransposed(in xf.Q, in p2, out p2);

			Vector2.Subtract(p2, p1, out Vector2 d);

			Vector2 v1 = this.vertex1;
			Vector2 v2 = this.vertex2;
			Vector2.Subtract(v2, v1, out Vector2 e);
			Vector2 normal;
			normal.X = e.Y;
			normal.Y = -e.X;
			Vector2.Normalize(in normal, out normal);

			// q = p1 + t * d
			// dot(normal, q - v1) = 0
			// dot(normal, p1 - v1) + t * dot(normal, d) = 0
			Vector2.Subtract(v1, p1, out Vector2 diff);
			float numerator = Vector2.Dot(normal, diff);
			float denominator = Vector2.Dot(normal, d);

			if (denominator == 0.0f)
				return false;

			float t = numerator / denominator;
			if (t < 0.0f || input.MaxFraction < t)
				return false;

			Vector2 q = p1 + t * d;

			// q = v1 + s * r
			// s = dot(q - v1, r) / dot(r, r)
			float ee = e.LengthSquared; // b2Math.b2Dot(r, r);
			if (ee <= float.Epsilon)
				return false;

			diff = q - v1;
			float s = Vector2.Dot(diff, e) / ee;
			if (s < 0.0f || 1.0f < s)
				return false;

			output.Fraction = t;
			if (numerator > 0.0f)
				output.Normal = -Rotation.Multiply(in xf.Q, in normal);
			else
				output.Normal = Rotation.Multiply(in xf.Q, in normal);

			return true;
		}

		public override void ComputeBoundingBox(in Transform xf, int childIndex, out AABB output)
		{
			Vector2 v1;
			v1.X = (xf.Q.C * this.vertex1.X - xf.Q.S * this.vertex1.Y) + xf.P.X;
			v1.Y = (xf.Q.S * this.vertex1.X + xf.Q.C * this.vertex1.Y) + xf.P.Y;

			Vector2 v2;
			v2.X = (xf.Q.C * this.vertex2.X - xf.Q.S * this.vertex2.Y) + xf.P.X;
			v2.Y = (xf.Q.S * this.vertex2.X + xf.Q.C * this.vertex2.Y) + xf.P.Y;

			Vector2 lower;
			lower.X = v1.X < v2.X ? v1.X : v2.X;
			lower.Y = v1.Y < v2.Y ? v1.Y : v2.Y;

			//b2Math.b2Min(v1, v2);
			Vector2 upper;
			upper.X = v1.X > v2.X ? v1.X : v2.X;
			upper.Y = v1.Y > v2.Y ? v1.Y : v2.Y;
			// = b2Math.b2Max(v1, v2);

			output.LowerBound = lower;
			output.UpperBound = upper;
			output.Fatten(this.radius);
		}

		public override MassData ComputeMass(float density)
		{
			return new MassData(0.0f, 0.5f * (this.vertex1 + this.vertex2), 0.0f);
		}

		public override void GetDistanceProxy(int index, out DistanceProxy proxy)
		{
			if (index != 0)
				throw new IndexOutOfRangeException();

			proxy = this.distanceProxy;
		}

		public override int GetHashCode()
		{
			if (!this.hashComputed)
			{
				this.hash = 0;

				if (this.hasPrevVertex)
				{
					this.hash = (23 * this.hash) + this.prevVertex.X.GetHashCode();
					this.hash = (23 * this.hash) + this.prevVertex.Y.GetHashCode();
				}

				this.hash = (23 * this.hash) + this.vertex1.X.GetHashCode();
				this.hash = (23 * this.hash) + this.vertex1.Y.GetHashCode();

				this.hash = (23 * this.hash) + this.vertex2.X.GetHashCode();
				this.hash = (23 * this.hash) + this.vertex2.Y.GetHashCode();
				
				if (this.hasNextVertex)
				{
					this.hash = (23 * this.hash) + this.nextVertex.X.GetHashCode();
					this.hash = (23 * this.hash) + this.nextVertex.Y.GetHashCode();
				}

				this.hashComputed = true;
			}

			return this.hash;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is EdgeShape other))
				return false;

			if (other.GetHashCode() != this.GetHashCode())
				return false;

			if (this.vertex1 != other.vertex1 || this.vertex2 != other.vertex2)
				return false;

			if (other.hasNextVertex != this.hasNextVertex || other.hasPrevVertex != this.hasPrevVertex)
				return false;

			if (this.hasNextVertex && this.nextVertex != other.nextVertex)
				return false;

			if (this.hasPrevVertex && this.prevVertex != other.prevVertex)
				return false;

			return true;
		}

		#endregion
	}
}
