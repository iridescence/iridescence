﻿using System;

namespace Iridescence.Physics2D.Collision.Shapes
{
	/// <summary>
	/// An exception thrown when a polygon is not valid.
	/// </summary>
	public class InvalidPolygonException : Exception
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new InvalidPolygonException.
		/// </summary>
		public InvalidPolygonException()
			: base("Invalid polygon.")
		{

		}

		#endregion

		#region Methods

		#endregion
	}
}
