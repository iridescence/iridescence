﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Collision.Shapes
{
	/// <summary>
	/// This holds the mass data computed for a shape.
	/// </summary>
	public struct MassData
	{
		#region Fields

		/// <summary>
		/// The mass of the shape, usually in kilograms.
		/// </summary>
		public readonly float Mass;

		/// <summary>
		/// The position of the shape's centroid relative to the shape's origin.
		/// </summary>
		public readonly Vector2 Center;

		/// <summary>
		/// The rotational inertia of the shape about the local origin.
		/// </summary>
		public readonly float Inertia;
		
		#endregion

		#region Constructors

		public MassData(float mass, Vector2 center, float i)
		{
			this.Mass = mass;
			this.Center = center;
			this.Inertia = i;
		}
	
		#endregion
	}
}
