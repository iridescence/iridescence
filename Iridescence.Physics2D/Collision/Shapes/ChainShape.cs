using System;
using System.Runtime.Serialization;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision.Shapes
{
	/// <summary>
	/// Represents a chain of vertices, connected by edges. 
	/// </summary>
	[Serializable]
	public sealed class ChainShape : Shape, IDeserializationCallback
	{
		#region Fields

		internal readonly Vector2[] vertices;
		
		internal readonly bool hasPrevVertex;
		internal readonly Vector2 prevVertex;
		
		internal readonly bool hasNextVertex;
		internal readonly Vector2 nextVertex;

		[NonSerialized] private readonly DistanceProxy[] distanceProxies;

		[NonSerialized] private int hash;
		[NonSerialized] private bool hashComputed;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the vertices of the shape.
		/// </summary>
		public ReadOnlyMemory<Vector2> Vertices => this.vertices;

		/// <summary>
		/// Gets the previous vertex position.
		/// </summary>
		public Vector2 PreviousVertex => this.prevVertex;

		/// <summary>
		/// Gets a value that indicates whether the chain has a previous vertex.
		/// </summary>
		public bool HasPreviousVertex => this.hasPrevVertex;

		/// <summary>
		/// Gets the next vertex position.
		/// </summary>
		public Vector2 NextVertex => this.nextVertex;

		/// <summary>
		/// Gets a value that indicates whether the chain has a next vertex.
		/// </summary>
		public bool HasNextVertex => this.hasNextVertex;

		internal override int ChildCount => this.vertices.Length - 1;

		#endregion

		#region Constructors

		private ChainShape(int numVertices, float radius)
			: base(radius)
		{
			this.vertices = new Vector2[numVertices];
			this.distanceProxies = new DistanceProxy[numVertices];
		}

		/// <summary>
		/// Creates a new explicitly defined chain shape.
		/// </summary>
		/// <param name="vertices"></param>
		/// <param name="prevVertex"></param>
		/// <param name="hasPrevVertex"></param>
		/// <param name="nextVertex"></param>
		/// <param name="hasNextVertex"></param>
		/// <param name="radius"></param>
		public ChainShape(Vector2[] vertices, Vector2 prevVertex, bool hasPrevVertex, Vector2 nextVertex, bool hasNextVertex, float radius)
			: this(vertices.Length, radius)
		{
			for (int i = 1; i < vertices.Length; ++i)
			{
				if (Vector2.DistanceSquared(vertices[i - 1], vertices[i]) < Settings.LinearSlop * Settings.LinearSlop)
					throw new ArgumentException("Vertices are too close together.");
			}

			Array.Copy(vertices, this.vertices, vertices.Length);

			this.prevVertex = prevVertex;
			this.hasPrevVertex = hasPrevVertex;

			this.nextVertex = nextVertex;
			this.hasNextVertex = hasNextVertex;

			this.initialize();
		}
		
		/// <summary>
		/// Creates a new explicitly defined chain shape.
		/// </summary>
		/// <param name="vertices"></param>
		/// <param name="prevVertex"></param>
		/// <param name="hasPrevVertex"></param>
		/// <param name="nextVertex"></param>
		/// <param name="hasNextVertex"></param>
		public ChainShape(Vector2[] vertices, Vector2 prevVertex, bool hasPrevVertex, Vector2 nextVertex, bool hasNextVertex)
			: this(vertices, prevVertex, hasPrevVertex, nextVertex, hasNextVertex, Settings.PolygonRadius)
		{

		}

		/// <summary>
		/// Creates a new simple chain shape.
		/// </summary>
		/// <param name="vertices">The vertices.</param>
		/// <param name="loop">True, if the chain should loop.</param>
		/// <param name="radius">The "radius" of the edges.</param>
		public ChainShape(Vector2[] vertices, bool loop, float radius)
			: this(vertices.Length + (loop ? 1 : 0), radius)
		{
			Array.Copy(vertices, this.vertices, vertices.Length);

			if (loop)
			{
				this.vertices[vertices.Length] = this.vertices[0];

				this.prevVertex = this.vertices[this.vertices.Length - 2];
				this.hasPrevVertex = true;

				this.nextVertex = this.vertices[1];
				this.hasNextVertex = true;
			}

			this.initialize();
		}

		/// <summary>
		/// Creates a new simple chain shape.
		/// </summary>
		/// <param name="vertices">The vertices.</param>
		/// <param name="loop">True, if the chain should loop.</param>
		public ChainShape(Vector2[] vertices, bool loop)
			: this(vertices, loop, Settings.PolygonRadius)
		{

		}

		#endregion

		#region Methods

		private void initialize()
		{
			// create distance proxies.
			for (int i = 0; i < this.vertices.Length; i++)
			{
				this.distanceProxies[i] = new DistanceProxy(new[] {this.vertices[i], this.vertices[i >= this.vertices.Length - 1 ? 0 : i + 1]}, this.radius);
			}
		}
		
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			this.initialize();
		}

		/// <summary>
		/// Gets an edge shape that represents the edge at the specified index.
		/// </summary>
		/// <param name="index">The edge index.</param>
		/// <returns></returns>
		public EdgeShape GetChildEdge(int index)
		{
			if (index < 0 || index > this.vertices.Length - 2)
				throw new IndexOutOfRangeException();

			Vector2 vP, vN;
			bool hP, hN;

			if (index > 0)
			{
				vP = this.vertices[index - 1];
				hP = true;
			}
			else
			{
				vP = this.prevVertex;
				hP = this.hasPrevVertex;
			}

			if (index < this.vertices.Length - 2)
			{
				vN = this.vertices[index + 2];
				hN = true;
			}
			else
			{
				vN = this.nextVertex;
				hN = this.hasNextVertex;
			}

			return new EdgeShape(this.vertices[index], this.vertices[index + 1], vP, hP, vN, hN, this.radius);
		}

		public override bool TestPoint(in Transform xf, Vector2 p)
		{
			return false;
		}

		public override bool RayCast(in RayCastInput input, in Transform xf, int childIndex, out RayCastOutput output)
		{
			int i1 = childIndex;
			int i2 = childIndex + 1;
			if (i2 == this.vertices.Length)
				i2 = 0;

			EdgeShape edgeShape = new EdgeShape(this.vertices[i1], this.vertices[i2]);

			bool b = edgeShape.RayCast(input, in xf, 0, out RayCastOutput co);
			output = co;
			return b;
		}

		public override void ComputeBoundingBox(in Transform xf, int childIndex, out AABB output)
		{
			int i1 = childIndex;
			int i2 = childIndex + 1;
			if (i2 == this.vertices.Length)
				i2 = 0;
			
			Transform.Multiply(in xf, in this.vertices[i1], out Vector2 v1);
			Transform.Multiply(in xf, in this.vertices[i2], out Vector2 v2);

			Vector2.Min(v1, v2, out output.LowerBound);
			Vector2.Max(v1, v2, out output.UpperBound);
		}

		public override MassData ComputeMass(float density)
		{
			return new MassData(0.0f, Vector2.Zero, 0.0f);
		}

		public override void GetDistanceProxy(int index, out DistanceProxy proxy)
		{
			proxy = this.distanceProxies[index];
		}

		public override int GetHashCode()
		{
			if (!this.hashComputed)
			{
				// Compute hash.
				this.hash = 0;

				if (this.hasPrevVertex)
				{
					this.hash = (23 * this.hash) + this.prevVertex.X.GetHashCode();
					this.hash = (23 * this.hash) + this.prevVertex.Y.GetHashCode();
				}

				for (int i = 0; i < this.vertices.Length; i++)
				{
					this.hash = (23 * this.hash) + this.vertices[i].X.GetHashCode();
					this.hash = (23 * this.hash) + this.vertices[i].Y.GetHashCode();
				}

				if (this.hasNextVertex)
				{
					this.hash = (23 * this.hash) + this.nextVertex.X.GetHashCode();
					this.hash = (23 * this.hash) + this.nextVertex.Y.GetHashCode();
				}

				this.hashComputed = true;
			}

			return this.hash;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is ChainShape other))
				return false;

			if (other.GetHashCode() != this.GetHashCode())
				return false;

			if (other.vertices.Length != this.vertices.Length || other.hasNextVertex != this.hasNextVertex || other.hasPrevVertex != this.hasPrevVertex)
				return false;

			if (this.hasNextVertex && this.nextVertex != other.nextVertex)
				return false;

			if (this.hasPrevVertex && this.prevVertex != other.prevVertex)
				return false;

			for (int i = 0; i < this.vertices.Length; i++)
			{
				if (this.vertices[i] != other.vertices[i])
					return false;
			}

			return true;
		}

		#endregion
	}
}
