using System;
using System.Runtime.Serialization;
using Iridescence.Math;

namespace Iridescence.Physics2D.Collision.Shapes
{
	[Serializable]
	public sealed class CircleShape : Shape, IDeserializationCallback
	{
		#region Fields

		internal Vector2 position;

		[NonSerialized] private DistanceProxy distanceProxy;

		[NonSerialized] private int hash;
		[NonSerialized] private bool hashComputed;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the position/center of the circle.
		/// </summary>
		public Vector2 Position => this.position;

		internal override int ChildCount => 1;

		#endregion

		#region Constructors

		public CircleShape(float radius, Vector2 position)
			: base(radius)
		{
			this.position = position;
			this.initialize();
		}

		public CircleShape(float radius)
			: this(radius, Vector2.Zero)
		{
		}

		public CircleShape(CircleShape copy)
			: this(copy.radius, copy.position)
		{
		}

		#endregion

		#region Methods

		private void initialize()
		{
			this.distanceProxy = new DistanceProxy(new[] {this.position}, this.radius);
		}

		void IDeserializationCallback.OnDeserialization(object sender)
		{
			this.initialize();
		}

		public override bool TestPoint(in Transform transform, Vector2 p)
		{
			Vector2 temp = Rotation.Multiply(in transform.Q, in this.position);
			Vector2.Add(temp, transform.P, out temp);
			Vector2.Subtract(p, temp, out temp);
			return temp.LengthSquared <= this.radius * this.radius;
		}

		// Collision Detection in Interactive 3D Environments by Gino van den Bergen
		// From Section 3.1.2
		// x = s + a * r
		// norm(x) = radius
		public override bool RayCast(in RayCastInput input, in Transform transform, int childIndex, out RayCastOutput output)
		{
			Rotation.Multiply(in transform.Q, in this.position, out Vector2 s);
			Vector2.Add(s, transform.P, out s);
			Vector2.Subtract(input.Point1, s, out s);
			float b = Vector2.Dot(s, s) - this.radius * this.radius;

			Vector2.Subtract(input.Point2, input.Point1, out Vector2 r);
			float c = Vector2.Dot(s, r);
			float rr = Vector2.Dot(r, r);
			float sigma = c * c - rr * b;

			if (sigma < 0.0f || rr < float.Epsilon)
			{
				output = RayCastOutput.Zero;
				return false;
			}

			float a = -(c + Utility.Sqrt(sigma));

			if (0.0f <= a && a <= input.MaxFraction * rr)
			{
				a /= rr;
				output.Fraction = a;

				Vector2.Multiply(r, a, out output.Normal);
				Vector2.Add(output.Normal, s, out output.Normal);
				Vector2.Normalize(in output.Normal, out output.Normal);
				return true;
			}

			output = RayCastOutput.Zero;
			return false;
		}

		public override void ComputeBoundingBox(in Transform transform, int childIndex, out AABB output)
		{
			Rotation.Multiply(in transform.Q, in this.position, out Vector2 p);
			Vector2.Add(p, transform.P, out p);

			output.LowerBound.X = p.X - this.radius;
			output.LowerBound.Y = p.Y - this.radius;
			output.UpperBound.X = p.X + this.radius;
			output.UpperBound.Y = p.Y + this.radius;
		}

		public override MassData ComputeMass(float density)
		{
			float mass = density * (float)System.Math.PI * this.radius * this.radius;
			return new MassData(mass, this.position, mass * (0.5f * this.radius * this.radius + this.position.LengthSquared));
		}

		public override void GetDistanceProxy(int index, out DistanceProxy proxy)
		{
			if (index != 0)
				throw new ArgumentOutOfRangeException(nameof(index));

			proxy = this.distanceProxy;
		}

		public override int GetHashCode()
		{
			if (!this.hashComputed)
			{
				Vector3 temp;
				temp.X = this.position.X;
				temp.Y = this.position.Y;
				temp.Z = this.radius;
				this.hash = temp.GetHashCode();
				this.hashComputed = true;
			}
			return this.hash;
		}

		public override bool Equals(object obj)
		{
			CircleShape other = obj as CircleShape;

			if (other == null)
				return false;

			return other.GetHashCode() == this.GetHashCode() &&
				   other.radius == this.radius &&
				   other.position.X == this.position.X &&
				   other.position.Y == this.position.Y;
		}

		#endregion
	}
}
