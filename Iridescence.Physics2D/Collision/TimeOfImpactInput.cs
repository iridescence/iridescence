﻿namespace Iridescence.Physics2D.Collision
{
	/// <summary>
	/// Input parameters for b2TimeOfImpact
	/// </summary>
	public struct TimeOfImpactInput
	{
		#region Fields

		public DistanceProxy ProxyA;
		public DistanceProxy ProxyB;
		public Sweep SweepA;
		public Sweep SweepB;
		public float TMax;

		#endregion

		#region Constructors

		public TimeOfImpactInput(DistanceProxy proxyA, DistanceProxy proxyB, Sweep sweepA, Sweep sweepB, float tMax)
		{
			this.ProxyA = proxyA;
			this.ProxyB = proxyB;
			this.SweepA = sweepA;
			this.SweepB = sweepB;
			this.TMax = tMax;
		}

		#endregion
	}
}
