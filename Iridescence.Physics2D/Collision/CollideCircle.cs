﻿using Iridescence.Math;
using Iridescence.Physics2D.Collision.Shapes;

namespace Iridescence.Physics2D.Collision
{
	public static partial class Collisions
	{
		#region Methods

		/// <summary>
		/// Compute the collision manifold between two circles.
		/// </summary>
		/// <param name="manifold"></param>
		/// <param name="circleA"></param>
		/// <param name="xfA"></param>
		/// <param name="circleB"></param>
		/// <param name="xfB"></param>
		public static void CollideCircles(Manifold manifold, CircleShape circleA, in Transform xfA, CircleShape circleB, in Transform xfB)
		{
			manifold.Points.CountInternal = 0;

			float pAx = (xfA.Q.C * circleA.position.X - xfA.Q.S * circleA.position.Y) + xfA.P.X;
			float pAy = (xfA.Q.S * circleA.position.X + xfA.Q.C * circleA.position.Y) + xfA.P.Y;

			float pBx = (xfB.Q.C * circleB.position.X - xfB.Q.S * circleB.position.Y) + xfB.P.X;
			float pBy = (xfB.Q.S * circleB.position.X + xfB.Q.C * circleB.position.Y) + xfB.P.Y;

			float dx = pBx - pAx;
			float dy = pBy - pAy;

			float distSqr = dx * dx + dy * dy;
			float radius = circleA.radius + circleB.radius;

			if (distSqr > radius * radius)
			{
				return;
			}

			manifold.Type = ManifoldType.Circles;
			manifold.LocalPoint = circleA.position;
			manifold.LocalNormal = Vector2.Zero;
			manifold.Points.CountInternal = 1;

			manifold.Points.Item0.LocalPoint = circleB.position;
			manifold.Points.Item0.Id.Hash = 0;
		}

		/// <summary>
		/// Compute the collision manifold between a polygon and a circle.
		/// </summary>
		/// <param name="manifold"></param>
		/// <param name="polygonA"></param>
		/// <param name="xfA"></param>
		/// <param name="circleB"></param>
		/// <param name="xfB"></param>
		public static void CollidePolygonAndCircle(Manifold manifold, PolygonShape polygonA, in Transform xfA, CircleShape circleB, in Transform xfB)
		{
			manifold.Points.CountInternal = 0;

			// Compute circle position in the frame of the polygon.
			Vector2 c;
			c.X = (xfB.Q.C * circleB.position.X - xfB.Q.S * circleB.position.Y) + xfB.P.X;
			c.Y = (xfB.Q.S * circleB.position.X + xfB.Q.C * circleB.position.Y) + xfB.P.Y;

			Vector2 cLocal;
			float px = c.X - xfA.P.X;
			float py = c.Y - xfA.P.Y;
			cLocal.X = (xfA.Q.C * px + xfA.Q.S * py);
			cLocal.Y = (-xfA.Q.S * px + xfA.Q.C * py);

			// Find the min separating edge.
			int normalIndex = 0;
			float separation = float.MinValue;
			float radius = polygonA.radius + circleB.radius;
			Vector2[] vertices = polygonA.vertices;
			Vector2[] normals = polygonA.normals;

			for (int i = 0; i < polygonA.vertices.Length; ++i)
			{
				Vector2 tmp;
				tmp.X = cLocal.X - vertices[i].X;
				tmp.Y = cLocal.Y - vertices[i].Y;
				float s = normals[i].X * tmp.X + normals[i].Y * tmp.Y; // b2Math.b2Dot(normals[i], cLocal - vertices[i]);

				if (s > radius)
				{
					// Early out.
					return;
				}

				if (s > separation)
				{
					separation = s;
					normalIndex = i;
				}
			}

			// Vertices that subtend the incident face.
			int vertIndex1 = normalIndex;
			int vertIndex2 = vertIndex1 + 1 < polygonA.vertices.Length ? vertIndex1 + 1 : 0;
			Vector2 v1 = vertices[vertIndex1];
			Vector2 v2 = vertices[vertIndex2];

			// If the center is inside the polygon ...
			if (separation < float.Epsilon)
			{
				manifold.Points.CountInternal = 1;
				manifold.Type = ManifoldType.FaceA;
				manifold.LocalNormal = normals[normalIndex];
				manifold.LocalPoint.X = 0.5f * (v1.X + v2.X);
				manifold.LocalPoint.Y = 0.5f * (v1.Y + v2.Y);
				manifold.Points.Item0.LocalPoint = circleB.position;
				manifold.Points.Item0.Id.Hash = 0;
				return;
			}

			// Compute barycentric coordinates
			float ax = cLocal.X - v1.X;
			float ay = cLocal.Y - v1.Y;
			float bx = v2.X - v1.X;
			float by = v2.Y - v1.Y;
			float u1 = ax * bx + ay * by;

			ax = cLocal.X - v2.X;
			ay = cLocal.Y - v2.Y;
			bx = v1.X - v2.X;
			by = v1.Y - v2.Y;
			float u2 = ax * bx + ay * by;

			if (u1 <= 0.0f)
			{
				if (Vector2.DistanceSquared(cLocal, v1) > radius * radius)
				{
					return;
				}

				manifold.Points.CountInternal = 1;
				manifold.Type = ManifoldType.FaceA;
				manifold.LocalNormal.X = cLocal.X - v1.X;
				manifold.LocalNormal.Y = cLocal.Y - v1.Y;
				Vector2.Normalize(manifold.LocalNormal, out manifold.LocalNormal);
				manifold.LocalPoint = v1;
				manifold.Points.Item0.LocalPoint = circleB.position;
				manifold.Points.Item0.Id.Hash = 0;
			}
			else if (u2 <= 0.0f)
			{
				if (Vector2.DistanceSquared(cLocal, v2) > radius * radius)
				{
					return;
				}

				manifold.Points.CountInternal = 1;
				manifold.Type = ManifoldType.FaceA;
				manifold.LocalNormal.X = cLocal.X - v2.X;
				manifold.LocalNormal.Y = cLocal.Y - v2.Y;
				Vector2.Normalize(manifold.LocalNormal, out manifold.LocalNormal);
				manifold.LocalPoint = v2;
				manifold.Points.Item0.LocalPoint = circleB.position;
				manifold.Points.Item0.Id.Hash = 0;
			}
			else
			{
				Vector2 faceCenter;
				faceCenter.X = 0.5f * (v1.X + v2.X);
				faceCenter.Y = 0.5f * (v1.Y + v2.Y);

				Vector2 a;
				a.X = cLocal.X - faceCenter.X;
				a.Y = cLocal.Y - faceCenter.Y;

				separation = Vector2.Dot(a, normals[vertIndex1]);

				if (separation > radius)
				{
					return;
				}

				manifold.Points.CountInternal = 1;
				manifold.Type = ManifoldType.FaceA;
				manifold.LocalNormal = normals[vertIndex1];
				manifold.LocalPoint = faceCenter;
				manifold.Points.Item0.LocalPoint = circleB.position;
				manifold.Points.Item0.Id.Hash = 0;
			}
		}

		#endregion
	}
}
