using System.Diagnostics;
using Iridescence.Math;

namespace Iridescence.Physics2D
{
	/// <summary>
	/// This describes the motion of a body/shape for TOI computation.
	/// Shapes are defined with respect to the body origin, which may
	/// no coincide with the center of mass. However, to support dynamics
	/// we must interpolate the center of mass position.
	/// </summary>
	public struct Sweep
	{
		#region Fields
		
		/// <summary>
		/// Local center of mass position.
		/// </summary>
		public Vector2 LocalCenter;
  
		/// <summary>
		/// Center initial world position.
		/// </summary>
		public Vector2 Center0;

		/// <summary>
		/// Center world position.
		/// </summary>
		public Vector2 Center;

		/// <summary>
		/// The initial world angle.
		/// </summary>
		public float Angle0;

		/// <summary>
		/// The world angle.
		/// </summary>
		public float Angle;

		/// <summary>
		/// Fraction of the current time step in the range [0,1]
		/// c0 and a0 are the positions at alpha0.
		/// </summary>
		public float Alpha0;

		#endregion

		#region Methods
		
		/// <summary>
		/// Get the interpolated transform at a specific time.
		/// </summary>
		/// <param name="beta">A factor in [0,1], where 0 indicates alpha0</param>
		/// <param name="xfb"></param>
		public void GetTransform(float beta, out Transform xfb)
		{
			float x = (1.0f - beta) * this.Center0.X + beta * this.Center.X;
			float y = (1.0f - beta) * this.Center0.Y + beta * this.Center.Y;

			float angle = (1.0f - beta) * this.Angle0 + beta * this.Angle;

			float sin = (float)System.Math.Sin(angle);
			float cos = (float)System.Math.Cos(angle);

			// Shift to origin
			xfb.P.X = x - (cos * this.LocalCenter.X - sin * this.LocalCenter.Y);
			xfb.P.Y = y - (sin * this.LocalCenter.X + cos * this.LocalCenter.Y);
			
			xfb.Q.S = sin;
			xfb.Q.C = cos;
		}

		/// <summary>
		/// Advance the sweep forward, yielding a new initial state.
		/// </summary>
		/// <param name="alpha">The new initial time.</param>
		public void Advance(float alpha)
		{
			Debug.Assert(this.Alpha0 < 1.0f);
			float beta = (alpha - this.Alpha0) / (1.0f - this.Alpha0);
			this.Center0 += beta * (this.Center - this.Center0);
			this.Angle0 += beta * (this.Angle - this.Angle0);
			this.Alpha0 = alpha;
		}

		/// <summary>
		/// Normalize the angles.
		/// </summary>
		public void Normalize()
		{
			float d = 2.0f * Constants.Pi * Utility.Floor(this.Angle0 / (2.0f * Constants.Pi));
			this.Angle0 -= d;
			this.Angle -= d;
		}

		#endregion
	}
}
