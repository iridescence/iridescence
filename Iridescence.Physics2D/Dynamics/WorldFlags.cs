﻿using System;

namespace Iridescence.Physics2D.Dynamics
{
	[Flags]
	internal enum WorldFlags : short
	{
		NewFixture = 0x1,
		ClearForces = 0x4
	}
}
