﻿using System;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Exception thrown when the world is locked.
	/// </summary>
	public sealed class WorldLockedException : Exception
	{
		#region Constructors

		/// <summary>
		/// Creates a new WorldLockedException.
		/// </summary>
		public WorldLockedException()
			: base("World is locked.")
		{
		}

		#endregion
	}
}
