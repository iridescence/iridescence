using System;
using Iridescence.Physics2D.Collision.Shapes;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// A fixture descriptor is used to create a fixture.
	/// This class defines an abstract fixture definition.
	/// </summary>
	[Serializable]
	public sealed class FixtureDescriptor
	{
		#region Properties

		/// <summary>
		/// Gets or sets the shape of the fixture.
		/// </summary>
		public Shape Shape { get; set; }

		/// <summary>
		/// Gets or sets the density, usually in kg/m^2.
		/// </summary>
		public float Density { get; set; }

		/// <summary>
		/// Gets or sets the friction coefficient, usually in the range [0,1].
		/// </summary>
		public float Friction { get; set; }

		/// <summary>
		/// Gets or sets the restitution (elasticity), usually in the range [0,1].
		/// </summary>
		public float Restitution { get; set; }

		/// <summary>
		/// Gets or sets a value that determines whether the fixture is a sensor.
		/// A sensor shape collects contact information but never generates a collision response.
		/// </summary>
		public bool Sensor { get; set; }

		/// <summary>
		/// Gets or sets a user object.
		/// </summary>
		public object UserData { get; set; }

		#endregion

		#region Constructors

		public FixtureDescriptor(Shape shape, float density)
		{
			this.Shape = shape;
			this.Density = density;
			this.Friction = 0.2f;
			this.Restitution = 0.0f;
			this.Sensor = false;
			this.UserData = null;
		}

		public FixtureDescriptor(Shape shape)
			: this(shape, 0.0f)
		{

		}

		public FixtureDescriptor()
			: this(null)
		{

		}

		#endregion
	}
}
