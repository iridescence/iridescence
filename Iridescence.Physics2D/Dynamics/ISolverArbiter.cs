﻿namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// An interface that for objects that decide which objects are going to be solved in a world step.
	/// Can be implemented to limit simulation to certain bodies for stuff like prediction.
	/// </summary>
	public interface ISolverArbiter
	{
		#region Methods

		/// <summary>
		/// Returns whether to solve the specified body.
		/// </summary>
		/// <param name="body"></param>
		/// <returns>False if the body should not be solved. True otherwise.</returns>
		bool Solve(Body body);

		#endregion
	}
}
