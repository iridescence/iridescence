/*
* Copyright (c) 2006-2011 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/


/*
Position Correction Notes
=========================
I tried the several algorithms for position correction of the 2D revolute joint.
I looked at these systems:
- simple pendulum (1m diameter sphere on massless 5m stick) with initial angular velocity of 100 rad/s.
- suspension bridge with 30 1m long planks of length 1m.
- multi-link chain with 30 1m long links.

Here are the algorithms:

Baumgarte - A fraction of the position error is added to the velocity error. There is no
separate position solver.

Pseudo Velocities - After the velocity solver and position integration,
the position error, Jacobian, and effective mass are recomputed. Then
the velocity constraints are solved with pseudo velocities and a fraction
of the position error is added to the pseudo velocity error. The pseudo
velocities are initialized to zero and there is no warm-starting. After
the position solver, the pseudo velocities are added to the positions.
This is also called the First Order World method or the Position LCP method.

Modified Nonlinear Gauss-Seidel (NGS) - Like Pseudo Velocities except the
position error is re-computed for each constraint and the positions are updated
after the constraint is solved. The radius vectors (aka Jacobians) are
re-computed too (otherwise the algorithm has horrible instability). The pseudo
velocity states are not needed because they are effectively zero at the beginning
of each iteration. Since we have the current position error, we allow the
iterations to terminate early if the error becomes smaller than b2_linearSlop.

Full NGS or just NGS - Like Modified NGS except the effective mass are re-computed
each time a constraint is solved.

Here are the results:
Baumgarte - this is the cheapest algorithm but it has some stability problems,
especially with the bridge. The chain links separate easily close to the root
and they jitter as they struggle to pull together. This is one of the most common
methods in the field. The big drawback is that the position correction artificially
affects the momentum, thus leading to instabilities and false bounce. I used a
bias factor of 0.2. A larger bias factor makes the bridge less stable, a smaller
factor makes joints and contacts more spongy.

Pseudo Velocities - the is more stable than the Baumgarte method. The bridge is
stable. However, joints still separate with large angular velocities. Drag the
simple pendulum in a circle quickly and the joint will separate. The chain separates
easily and does not recover. I used a bias factor of 0.2. A larger value lead to
the bridge collapsing when a heavy cube drops on it.

Modified NGS - this algorithm is better in some ways than Baumgarte and Pseudo
Velocities, but in other ways it is worse. The bridge and chain are much more
stable, but the simple pendulum goes unstable at high angular velocities.

Full NGS - stable in all tests. The joints display good stiffness. The bridge
still sags, but this is better than infinite forces.

Recommendations
Pseudo Velocities are not really worthwhile because the bridge and chain cannot
recover from joint separation. In other cases the benefit over Baumgarte is small.

Modified NGS is not a robust method for the revolute joint due to the violent
instability seen in the simple pendulum. Perhaps it is viable with other constraint
types, especially scalar constraints where the effective mass is a scalar.

This leaves Baumgarte and Full NGS. Baumgarte has small, but manageable instabilities
and is very fast. I don't think we can escape Baumgarte, especially in highly
demanding cases where high constraint fidelity is not needed.

Full NGS is robust and easy on the eyes. I recommend this as an option for
higher fidelity simulation and certainly for suspension bridges and long chains.
Full NGS might be a good choice for ragdolls, especially motorized ragdolls where
joint separation can be problematic. The number of NGS iterations can be reduced
for better performance without harming robustness much.

Each joint in a can be handled differently in the position solver. So I recommend
a system where the user can select the algorithm on a per joint basis. I would
probably default to the slower Full NGS and let the user select the faster
Baumgarte method in performance critical scenarios.
*/

/*
Cache Performance

The Box2D solvers are dominated by cache misses. Data structures are designed
to increase the number of cache hits. Much of misses are due to random access
to body data. The constraint structures are iterated over linearly, which leads
to few cache misses.

The bodies are not accessed during iteration. Instead read only data, such as
the mass values are stored with the constraints. The mutable data are the constraint
impulses and the bodies velocities/positions. The impulses are held inside the
constraint structures. The body velocities/positions are held in compact, temporary
arrays to increase the number of cache hits. Linear and angular velocity are
stored in a single array since multiple arrays lead to multiple misses.
*/

/*
2D Rotation

R = [cos(theta) -sin(theta)]
	[sin(theta) cos(theta) ]

thetaDot = omega

Let q1 = cos(theta), q2 = sin(theta).
R = [q1 -q2]
	[q2  q1]

q1Dot = -thetaDot * q2
q2Dot = thetaDot * q1

q1_new = q1_old - dt * w * q2
q2_new = q2_old + dt * w * q1
then normalize.

This might be faster than computing sin+cos.
However, we can compute sin+cos of the same angle fast.
*/

using System;
using System.Buffers;
using System.Diagnostics;
using Iridescence.Math;
using Iridescence.Physics2D.Dynamics.Contacts;
using Iridescence.Physics2D.Dynamics.Controllers;
using Iridescence.Physics2D.Dynamics.Joints;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// An island is a group of independently solvable bodies, joints and contacts.
	/// </summary>
	internal sealed class Island
	{
		#region Fields

		private ContactManager contactManager;
		private ISolverArbiter solverArbiter;

		internal Body[] bodies;
		internal int bodyCount;

		internal Contact[] contacts;
		internal int contactCount;
		internal BodyContactCollection.Cache contactCache;

		internal Joint[] joints;
		internal int jointCount;

		private BodyState[] states;
		private readonly SolverData solverData;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public Island()
		{
			const int initialSize = 16;
			this.bodies = new Body[initialSize];
			this.contacts = new Contact[initialSize];
			this.joints = new Joint[initialSize];
			this.states = new BodyState[initialSize];
			this.solverData = new SolverData();
			this.contactCache = new BodyContactCollection.Cache();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Clears the island.
		/// </summary>
		public void Clear(bool clearReferences)
		{
			this.bodyCount = 0;
			this.contactCount = 0;
			this.jointCount = 0;

			if (clearReferences)
			{
				this.contactManager = null;
				this.solverArbiter = null;
				Array.Clear(this.bodies, 0, this.bodies.Length);
				Array.Clear(this.contacts, 0, this.contacts.Length);
				Array.Clear(this.joints, 0, this.joints.Length);
				this.contactCache.Clear();
			}
		}

		public void Add(Body body)
		{
			body.islandIndex = this.bodyCount;
			this.bodies[this.bodyCount++] = body;
		}

		public void Add(Contact contact)
		{
			this.contacts[this.contactCount++] = contact;
		}

		public void Add(Joint joint)
		{
			this.joints[this.jointCount++] = joint;
		}

		private static void ensureCapacity<T>(ref T[] array, int capacity)
		{
			if (array.Length >= capacity)
				return;

			int length = 16;
			while (length < capacity)
			{
				length *= 2;
			}

			Array.Resize(ref array, length);
		}

		public void Initialize(int bodyCapacity, int contactCapacity, int jointCapacity, ContactManager contactManager, ISolverArbiter solverArbiter)
		{
			this.contactManager = contactManager;
			this.solverArbiter = solverArbiter;
			
			ensureCapacity(ref this.bodies, bodyCapacity);
			ensureCapacity(ref this.contacts, contactCapacity);
			ensureCapacity(ref this.joints, jointCapacity);
			ensureCapacity(ref this.states, bodyCapacity);
			
			this.Clear(false);
		}

		public void Solve(in TimeStep step, bool allowSleep)
		{
			float h = step.DeltaT;

			// Solver data
			this.solverData.timeStep = step;
			this.solverData.states = this.states;

			// Integrate velocities and apply damping. Initialize the body state.
			for (int i = 0, count = this.bodyCount; i < count; ++i)
			{
				Body body = this.bodies[i];
				body.GetState(out BodyState state);

				// Store positions for continuous collision.
				body.sweep.Center0 = body.sweep.Center;
				body.sweep.Angle0 = body.sweep.Angle;

				this.states[i] = state;

				if (this.solverArbiter != null && !this.solverArbiter.Solve(body))
					continue;

				if (body.type == BodyType.Dynamic)
				{
					// Integrate velocities.
					Vector2.Multiply(body.force, h * body.invMass, out Vector2 temp);
					Vector2.Add(state.LinearVelocity, temp, out this.states[i].LinearVelocity);
					this.states[i].AngularVelocity += h * body.invInertia * body.torque;

					if (body.controllers != null)
					{
						// pre-solve controllers.
						foreach (Controller controller in body.controllers)
						{
							controller.PreSolve(this.solverData, body, i);
						}
					}
				}
			}

			// Initialize velocity constraints.
			ContactConstraint[] constraintsArray = ArrayPool<ContactConstraint>.Shared.Rent(this.contactCount);
			Span<ContactConstraint> constraints = new Span<ContactConstraint>(constraintsArray, 0, this.contactCount);
			ContactSolver contactSolver = new ContactSolver(step, new Span<Contact>(this.contacts, 0, this.contactCount), this.states, constraints);

			contactSolver.InitializeVelocityConstraints();

			if (step.WarmStarting)
			{
				contactSolver.WarmStart();
			}

			// Initialize velocity constraints.
			for (int i = 0; i < this.jointCount; ++i)
			{
				this.joints[i].InitVelocityConstraints(this.solverData);
			}

			// Solve velocity constraints
			for (int i = 0; i < step.VelocityIterations; ++i)
			{
				for (int j = 0; j < this.jointCount; ++j)
					this.joints[j].SolveVelocityConstraints(this.solverData);

				contactSolver.SolveVelocityConstraints();
			}

			// Store impulses for warm starting
			contactSolver.StoreImpulses();
			// Integrate positions
			for (int i = 0; i < this.bodyCount; ++i)
			{
				BodyState state = this.states[i];

				// Check for large velocities
				Vector2.Multiply(state.LinearVelocity, h, out Vector2 translation);
				if (translation.LengthSquared > Settings.MaxTranslationSquared)
				{
					float ratio = Settings.MaxTranslation / translation.Length;
					//v *= ratio;
					Vector2.Multiply(state.LinearVelocity, ratio, out state.LinearVelocity);
					Vector2.Multiply(state.LinearVelocity, h, out translation);
				}

				float rotation = h * state.AngularVelocity;
				if (rotation * rotation > Settings.MaxRotationSquared)
				{
					float ratio = Settings.MaxRotation / System.Math.Abs(rotation);
					state.AngularVelocity *= ratio;
					rotation = h * state.AngularVelocity;
				}

				// Integrate
				Vector2.Add(state.Position, translation, out state.Position);
				state.Angle += rotation;

				this.states[i] = state;
			}

			// Solve position constraints
			bool positionSolved = false;
			for (int i = 0; i < step.PositionIterations; ++i)
			{
				bool contactsOkay = contactSolver.SolvePositionConstraints();

				bool jointsOkay = true;
				for (int j = 0; j < this.jointCount; ++j)
				{
					bool jointOkay = this.joints[j].SolvePositionConstraints(this.solverData);
					jointsOkay = jointsOkay && jointOkay;
				}

				if (contactsOkay && jointsOkay)
				{
					// Exit early if the position errors are small.
					positionSolved = true;
					break;
				}
			}

			// Copy state buffers back to the bodies
			for (int i = 0; i < this.bodyCount; ++i)
			{
				Body body = this.bodies[i];

				if (this.solverArbiter != null && !this.solverArbiter.Solve(body))
					continue;
				
				if (body.controllers != null)
				{
					// post-solve controllers.
					foreach (Controller controller in body.controllers)
					{
						controller.PostSolve(this.solverData, body);
					}
				}

				body.sweep.Center = this.states[i].Position;
				body.sweep.Angle = this.states[i].Angle;
				body.LinearVelocity = this.states[i].LinearVelocity;
				body.AngularVelocity = this.states[i].AngularVelocity;
				body.SynchronizeTransform();
			}

			this.report(constraints);
			ArrayPool<ContactConstraint>.Shared.Return(constraintsArray);

			if (allowSleep)
			{
				float minSleepTime = float.MaxValue;

				float linTolSqr = Settings.LinearSleepTolerance * Settings.LinearSleepTolerance;
				float angTolSqr = Settings.AngularSleepTolerance * Settings.AngularSleepTolerance;

				for (int i = 0; i < this.bodyCount; ++i)
				{
					Body body = this.bodies[i];
					if (body.type == BodyType.Static)
						continue;

					if (this.solverArbiter != null && !this.solverArbiter.Solve(body))
						continue;

					if ((body.flags & BodyFlags.AutoSleep) == 0 ||
						body.AngularVelocity * body.AngularVelocity > angTolSqr ||
						Vector2.Dot(body.linearVelocity, body.linearVelocity) > linTolSqr)
					{
						body.sleepTime = 0.0f;
						minSleepTime = 0.0f;
					}
					else
					{
						body.sleepTime = body.sleepTime + h;
						minSleepTime = System.Math.Min(minSleepTime, body.sleepTime);
					}
				}

				if (minSleepTime >= Settings.TimeToSleep && positionSolved)
				{
					for (int i = 0; i < this.bodyCount; ++i)
					{
						Body body = this.bodies[i];
						body.Awake = false;
					}
				}
			}

		}

		public void SolveTOI(in TimeStep subStep, int toiIndexA, int toiIndexB)
		{
			Debug.Assert(toiIndexA < this.bodyCount);
			Debug.Assert(toiIndexB < this.bodyCount);

			// Initialize the body state.
			for (int i = 0; i < this.bodyCount; ++i)
			{
				Body b = this.bodies[i];
				this.states[i].Position = b.sweep.Center;
				this.states[i].Angle = b.sweep.Angle;
				this.states[i].LinearVelocity = b.LinearVelocity;
				this.states[i].AngularVelocity = b.AngularVelocity;
			}

			ContactConstraint[] constraintsArray = ArrayPool<ContactConstraint>.Shared.Rent(this.contactCount);
			Span<ContactConstraint> constraints = new Span<ContactConstraint>(constraintsArray, 0, this.contactCount);
			ContactSolver contactSolver = new ContactSolver(subStep, new Span<Contact>(this.contacts, 0, this.contactCount), this.states, constraints);

			// Solve position constraints.
			for (int i = 0; i < subStep.PositionIterations; ++i)
			{
				bool contactsOkay = contactSolver.SolveTOIPositionConstraints(toiIndexA, toiIndexB);
				if (contactsOkay)
				{
					break;
				}
			}

#if false
			// Is the new position really safe?
			for (int i = 0; i < m_contactCount; ++i)
			{
				b2Contact c = m_contacts[i];
				b2Fixture fA = c.GetFixtureA();
				b2Fixture fB = c.GetFixtureB();

				b2Body bA = fA.Body;
				b2Body bB = fB.Body;

				int indexA = c.GetChildIndexA();
				int indexB = c.GetChildIndexB();

				b2DistanceInput input = new b2DistanceInput();
				input.proxyA.Set(fA.Shape, indexA);
				input.proxyB.Set(fB.Shape, indexB);
				input.transformA = bA.Transform;
				input.transformB = bB.Transform;
				input.useRadii = false;

				b2DistanceOutput output;
				b2SimplexCache cache = new b2SimplexCache();
				cache.count = 0;
				output = b2Distance(cache, input);

				if (output.distance == 0 || cache.count == 3)
				{
					cache.count += 0;
				}
			}
#endif
			Body bodyA = this.bodies[toiIndexA];
			Body bodyB = this.bodies[toiIndexB];

			// Leap of faith to new safe state.
			bodyA.sweep.Center0 = this.states[toiIndexA].Position;
			bodyA.sweep.Angle0 = this.states[toiIndexA].Angle;
			bodyB.sweep.Center0 = this.states[toiIndexB].Position;
			bodyB.sweep.Angle0 = this.states[toiIndexB].Angle;

			// No warm starting is needed for TOI events because warm
			// starting impulses were applied in the discrete solver.
			contactSolver.InitializeVelocityConstraints();

			// Solve velocity constraints.
			for (int i = 0; i < subStep.VelocityIterations; ++i)
				contactSolver.SolveVelocityConstraints();

			// Don't store the TOI contact forces for warm starting
			// because they can be quite large.

			float h = subStep.DeltaT;

			// Integrate positions
			for (int i = 0, count = this.bodyCount; i < count; ++i)
			{
				Body body = this.bodies[i];
				BodyState state = this.states[i];

				// Check for large velocities
				Vector2 translation = h * state.LinearVelocity;
				if (Vector2.Dot(translation, translation) > Settings.MaxTranslationSquared)
				{
					float ratio = Settings.MaxTranslation / translation.Length;
					state.LinearVelocity *= ratio;
				}

				float rotation = h * state.AngularVelocity;
				if (rotation * rotation > Settings.MaxRotationSquared)
				{
					float ratio = Settings.MaxRotation / System.Math.Abs(rotation);
					state.AngularVelocity *= ratio;
				}

				// Integrate
				state.Position += h * state.LinearVelocity;
				state.Angle += h * state.AngularVelocity;

				this.states[i] = state;

				// Sync bodies
				body.sweep.Center = state.Position;
				body.sweep.Angle = state.Angle;
				body.LinearVelocity = state.LinearVelocity;
				body.AngularVelocity = state.AngularVelocity;
				body.SynchronizeTransform();
			}

			this.report(constraints);

			ArrayPool<ContactConstraint>.Shared.Return(constraintsArray);
		}

		private void report(Span<ContactConstraint> constraints)
		{
			if (this.contactManager == null)
				return;

			ContactImpulse impulse = ContactImpulse.Create();
			var normals = impulse.NormalImpulses;
			var tangens = impulse.TangentImpulses;

			for (int i = 0, count = this.contactCount; i < count; ++i)
			{
				Contact c = this.contacts[i];
				if (c.IsDestroyed)
					continue;

				ref ContactConstraint vc = ref constraints[i];

				impulse.Count = vc.Points.Count;
				for (int j = 0; j < vc.Points.Count; ++j)
				{
					normals[j] = vc.Points[j].NormalImpulse;
					tangens[j] = vc.Points[j].TangentImpulse;
				}

				this.contactManager.InternalPostSolve(c, ref impulse);
			}
		}

		#endregion
	}
}