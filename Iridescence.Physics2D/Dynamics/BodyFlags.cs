﻿using System;

namespace Iridescence.Physics2D.Dynamics
{
	[Flags]
	internal enum BodyFlags
	{
		Island = 0x0001,
		Awake = 0x0002,
		AutoSleep = 0x0004,
		Bullet = 0x0008,
		FixedRotation = 0x0010,
		Active = 0x0020,
		Toi = 0x0040
	}
}
