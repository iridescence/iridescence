namespace Iridescence.Physics2D.Dynamics
{
	/// This is an internal structure.
	public readonly struct TimeStep
	{
		#region Fields

		/// <summary>
		/// The time step.
		/// </summary>
		public readonly float DeltaT;

		/// <summary>
		/// Inverse time step (1.0 / DeltaT, or 0.0 if DeltaT == 0).
		/// </summary>
		public readonly float InvDeltaT;

		/// <summary>
		/// InvDeltaT of the previous step * DeltaT of this step.
		/// </summary>
		public readonly float DeltaTRatio;

		/// <summary>
		/// The number of position iterations.
		/// </summary>
		public readonly int PositionIterations;

		/// <summary>
		/// The number of velocity iterations.
		/// </summary>
		public readonly int VelocityIterations;

		public readonly bool WarmStarting;

		#endregion

		#region Constructor 

		public TimeStep(float deltaT, float deltaTRatio, int positionIterations, int velocityIterations, bool warmStarting)
		{
			this.DeltaT = deltaT;

			if (deltaT >= 0.0f)
				this.InvDeltaT = 1.0f / deltaT;
			else
				this.InvDeltaT = 0.0f;

			this.DeltaTRatio = deltaTRatio;
			this.PositionIterations = positionIterations;
			this.VelocityIterations = velocityIterations;
			this.WarmStarting = warmStarting;
		}

		#endregion
	}
}
