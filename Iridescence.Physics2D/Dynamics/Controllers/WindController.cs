﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Controllers
{
	/// <summary>
	/// Represents a wind controller that applies a force to the bodies.
	/// </summary>
	public sealed class WindController : Controller
	{
		#region Fields

		private Vector2 force;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the wind force of the controller.
		/// </summary>
		public Vector2 Force
		{
			get => this.force;
			set => this.force = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new WindController.
		/// </summary>
		public WindController(Vector2 force)
		{
			this.Force = force;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Applies the controller to a body.
		/// </summary>
		public override void PreSolve(SolverData data, Body body, int index)
		{
			body.ApplyForceToCenter(in this.force, false);
		}

		#endregion
	}
}
