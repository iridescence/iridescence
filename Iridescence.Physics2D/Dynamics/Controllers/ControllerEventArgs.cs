﻿using System;

namespace Iridescence.Physics2D.Dynamics.Controllers
{
	/// <summary>
	/// Controller event args.
	/// </summary>
	public class ControllerEventArgs : EventArgs
	{
		#region Properties

		/// <summary>
		/// Gets the controller.
		/// </summary>
		public Controller Controller { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ControllerEventArgs.
		/// </summary>
		public ControllerEventArgs(Controller controller)
		{
			this.Controller = controller;
		}

		#endregion
	}
}
