﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Controllers
{
	/// <summary>
	/// Represents a gravity controller that applies a constant force to the bodies.
	/// </summary>
	public sealed class GravityController : Controller
	{
		#region Fields

		private Vector2 gravity;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the gravity vector.
		/// </summary>
		public Vector2 Gravity
		{
			get => this.gravity;
			set => this.gravity = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new GravityController.
		/// </summary>
		public GravityController(Vector2 gravity)
		{
			this.gravity = gravity;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Applies the controller to a body.
		/// </summary>
		public override void PreSolve(SolverData data, Body body, int index)
		{
			Vector2 temp;
			temp.X = this.gravity.X * body.mass;
			temp.Y = this.gravity.Y * body.mass;
			body.ApplyForceToCenter(in temp, false);
		}

		#endregion
	}
}
