﻿namespace Iridescence.Physics2D.Dynamics.Controllers
{
	/// <summary>
	/// Represents an abstract controller.
	/// Controllers apply forces and other effects to bodies.
	/// </summary>
	public abstract class Controller
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets a user object.
		/// </summary>
		public object UserData { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Controller.
		/// </summary>
		protected Controller()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// This method is invoked before contacts, joints, etc. are solved.
		/// </summary>
		/// <param name="data">The solver data.</param>
		/// <param name="body">The body.</param>
		/// <param name="index">The index of the body in the solver data.</param>
		public virtual void PreSolve(SolverData data, Body body, int index)
		{
			
		}

		/// <summary>
		/// This method is invoked after contacts, joints, etc. have been solved.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="bdoy"></param>
		public virtual void PostSolve(SolverData data, Body bdoy)
		{
			
		}

		#endregion
	}
}
