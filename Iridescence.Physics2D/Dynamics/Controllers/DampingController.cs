﻿namespace Iridescence.Physics2D.Dynamics.Controllers
{
	/// <summary>
	/// Represents a velocity damping controller.
	/// </summary>
	public sealed class DampingController : Controller
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the linear damping.
		/// 0 means no damping.
		/// </summary>
		public float LinearDamping { get; set; }

		/// <summary>
		/// Gets or sets the angular damping.
		/// 0 means no damping.
		/// </summary>
		public float AngularDamping { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new DampingController.
		/// </summary>
		public DampingController(float linearDamping, float angularDamping)
		{
			this.LinearDamping = linearDamping;
			this.AngularDamping = angularDamping;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Applies the controller to a body.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="body"></param>
		public override void PreSolve(SolverData data, Body body, int index)
		{
			float factor = 1.0f / (1.0f + data.timeStep.DeltaT * this.LinearDamping);
			data.states[index].LinearVelocity.X *= factor;
			data.states[index].LinearVelocity.Y *= factor;

			factor = 1.0f / (1.0f + data.timeStep.DeltaT * this.AngularDamping);
			data.states[index].AngularVelocity *= factor;
		}

		#endregion
	}
}
