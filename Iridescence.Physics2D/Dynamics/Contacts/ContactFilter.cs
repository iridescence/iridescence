namespace Iridescence.Physics2D.Dynamics.Contacts
{
	/// <summary>
	/// Implement this class to provide collision filtering. In other words, you can implement
	/// this class if you want finer control over contact creation.
	/// </summary>
	public class ContactFilter
	{
		#region Methods

		/// <summary>
		/// Determines whether two fixtures are supposed to generate contacts.
		/// </summary>
		/// <param name="fixtureA">The first fixture.</param>
		/// <param name="fixtureB">The second fixture.</param>
		/// <returns>True, if the two fixtures are supposed to collide. False otherwise.</returns>
		public virtual bool ShouldCollide(Fixture fixtureA, Fixture fixtureB)
		{
			return true;
		}

		#endregion
	}
}
