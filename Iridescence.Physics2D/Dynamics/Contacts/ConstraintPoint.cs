﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Contacts
{
	internal struct ConstraintPoint
	{
		#region Fields

		//Position
		public Vector2 LocalPoint;

		//Velocity
		public Vector2 rA;
		public Vector2 rB;
		public float NormalImpulse;
		public float TangentImpulse;
		public float NormalMass;
		public float TangentMass;
		public float VelocityBias;
	
		#endregion
	}
}
