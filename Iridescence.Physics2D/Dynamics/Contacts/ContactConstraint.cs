﻿using Iridescence.Math;
using Iridescence.Physics2D.Collision;

namespace Iridescence.Physics2D.Dynamics.Contacts
{
	/// <summary>
	/// Contact position/velocity constraint data.
	/// </summary>
	internal struct ContactConstraint
	{
		#region Fields
		
		public int IndexA;
		public int IndexB;
		public float InvMassA;
		public float InvMassB;
		public float InvIA;
		public float InvIB;

		public ManifoldPointCollection<ConstraintPoint> Points;

		//Position
		public Vector2 LocalNormal;
		public Vector2 LocalPoint;
		public Vector2 LocalCenterA, LocalCenterB;
		public ManifoldType Type;
		public float RadiusA, RadiusB;

		//Velocity
		public Vector2 Normal;
		public Matrix2x2 NormalMass;
		public Matrix2x2 K;
		public float Friction;
		public float Restitution;
		public float TangentSpeed;

		#endregion
	}
}
