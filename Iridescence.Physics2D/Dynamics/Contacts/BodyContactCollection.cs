﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Physics2D.Dynamics.Contacts
{
	/// <summary>
	/// Collection for body contacts.
	/// </summary>
	internal sealed class BodyContactCollection
	{
		#region Fields

		private readonly Dictionary<Body, List<Contact>> dictionary;
		//private List<(Body body, Contact contact, bool isAdded)> postponedChanges;
		//private bool isLocked;
		private int bodyCount;
		private int contactCount;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BodyContactCollection.
		/// </summary>
		internal BodyContactCollection()
		{
			this.dictionary = new Dictionary<Body, List<Contact>>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a snapshot of the current contacts that can not be modified.
		/// </summary>
		/// <param name="cache">A cache so no new arrays have to be allocated.</param>
		/// <returns></returns>
		internal ReadOnlySpan<ContactRange> ToImmutable(Cache cache)
		{
			if (this.contactCount == 0)
				return ReadOnlySpan<ContactRange>.Empty;

			cache.EnsureCapacity(this.bodyCount, this.contactCount);

			int rangeIndex = 0;
			int contactsOffset = 0;
			foreach(KeyValuePair<Body, List<Contact>> pair in this.dictionary)
			{
				int count = pair.Value.Count;
				pair.Value.CopyTo(cache.contacts, contactsOffset);

				cache.ranges[rangeIndex++] = new ContactRange(pair.Key, cache, contactsOffset, count);
				contactsOffset += count;
			}

			return new ReadOnlySpan<ContactRange>(cache.ranges, 0, rangeIndex);
		}
	
		public IEnumerable<Contact> GetContacts()
		{
			return this.dictionary.Values.SelectMany(list => list);
		}

		public bool TryGetContacts(Body body, out IEnumerable<Contact> contacts)
		{
			if (!this.dictionary.TryGetValue(body, out List<Contact> list))
			{
				contacts = null;
				return false;
			}

			contacts = list;
			return true;
		}

		public void Add(Body body, Contact contact)
		{
			if (!this.dictionary.TryGetValue(body, out List<Contact> list))
			{
				list = new List<Contact>(new List<Contact>());
				this.dictionary.Add(body, list);
				++this.bodyCount;
			}

			list.Add(contact);
			++this.contactCount;
		}

		public bool Remove(Body body, Contact contact)
		{
			if (this.dictionary.TryGetValue(body, out List<Contact> list))
			{
				if (list.Remove(contact))
				{
					if (list.Count == 0)
					{
						this.dictionary.Remove(body);
						--this.bodyCount;
					}
					--this.contactCount;

					return true;
				}
			}

			return false;
		}

		#endregion

		#region Nested Types

		internal sealed class Cache
		{
			internal ContactRange[] ranges;
			internal Contact[] contacts;

			public Cache()
			{
				this.contacts = new Contact[16];
				this.ranges = new ContactRange[16];
			}

			internal void EnsureCapacity(int numBodies, int numContacts)
			{
				if (this.ranges.Length < numBodies)
				{
					numBodies = ((numBodies + 15) / 16) * 16;
					this.ranges = new ContactRange[numBodies];
				}

				if (this.contacts.Length < numContacts)
				{
					numContacts = ((numContacts + 15) / 16) * 16;
					this.contacts = new Contact[numContacts];
				}
			}

			internal void Clear()
			{
				new Span<ContactRange>(this.ranges).Clear();
				new Span<Contact>(this.contacts).Clear();
			}
		}

		internal readonly struct ContactRange
		{
			public readonly Body Body;
			private readonly Cache container;
			private readonly int start;
			private readonly int length;
			public ReadOnlySpan<Contact> Contacts => new ReadOnlySpan<Contact>(this.container.contacts, this.start, this.length);
			public ContactRange(Body body, Cache container, int start, int length)
			{
				this.Body = body;
				this.container = container;
				this.start = start;
				this.length = length;
			}
		}

		#endregion
	}
}
