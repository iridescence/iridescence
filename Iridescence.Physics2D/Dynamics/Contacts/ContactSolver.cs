/*
* Copyright (c) 2006-2011 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

using System;
using Iridescence.Math;
using Iridescence.Physics2D.Collision;
using Iridescence.Physics2D.Collision.Shapes;

namespace Iridescence.Physics2D.Dynamics.Contacts
{
	internal ref struct ContactSolver
	{
		#region Fields

		private readonly int count;
		private readonly ReadOnlySpan<Contact> contacts;
		private readonly Span<ContactConstraint> constraints;
		private Span<BodyState> states;

		#endregion

		#region Constructor

		public ContactSolver(in TimeStep step, ReadOnlySpan<Contact> contacts, Span<BodyState> states, Span<ContactConstraint> constraints)
		{
			this.count = contacts.Length;
			this.contacts = contacts;
			this.states = states;
			this.constraints = constraints;

			// Initialize position independent portions of the constraints.
			for (int i = 0; i < this.count; ++i)
			{
				Contact contact = contacts[i];

				Fixture fixtureA = contact.fixtureA;
				Fixture fixtureB = contact.fixtureB;
				Shape shapeA = fixtureA.shape;
				Shape shapeB = fixtureB.shape;
				float radiusA = shapeA.radius;
				float radiusB = shapeB.radius;
				Body bodyA = fixtureA.body;
				Body bodyB = fixtureB.body;
				Manifold manifold = contact.manifold;

				int pointCount = manifold.Points.CountInternal;

				ref ContactConstraint constraint = ref this.constraints[i];

				constraint.Friction = contact.friction;
				constraint.Restitution = contact.restitution;
				constraint.TangentSpeed = contact.tangentSpeed;
				constraint.IndexA = bodyA.islandIndex;
				constraint.IndexB = bodyB.islandIndex;
				constraint.InvMassA = bodyA.invMass;
				constraint.InvMassB = bodyB.invMass;
				constraint.InvIA = bodyA.invInertia;
				constraint.InvIB = bodyB.invInertia;
				constraint.Points.CountInternal = pointCount;
				constraint.K = Matrix2x2.Zero;
				constraint.NormalMass = Matrix2x2.Zero;

				constraint.LocalCenterA = bodyA.sweep.LocalCenter;
				constraint.LocalCenterB = bodyB.sweep.LocalCenter;
				constraint.LocalNormal = manifold.LocalNormal;
				constraint.LocalPoint = manifold.LocalPoint;
				constraint.RadiusA = radiusA;
				constraint.RadiusB = radiusB;
				constraint.Type = manifold.Type;

				for (int j = 0; j < pointCount; ++j)
				{
					ref ManifoldPoint cp = ref manifold.Points.Ref(j);
					ref ConstraintPoint vcp = ref constraint.Points.Ref(j);

					if (step.WarmStarting)
					{
						vcp.NormalImpulse = step.DeltaTRatio * cp.NormalImpulse;
						vcp.TangentImpulse = step.DeltaTRatio * cp.TangentImpulse;
					}
					else
					{
						vcp.NormalImpulse = 0.0f;
						vcp.TangentImpulse = 0.0f;
					}

					vcp.rA = Vector2.Zero;
					vcp.rB = Vector2.Zero;
					vcp.NormalMass = 0.0f;
					vcp.TangentMass = 0.0f;
					vcp.VelocityBias = 0.0f;

					constraint.Points.Ref(j).LocalPoint = cp.LocalPoint;
				}
			}
		}

		#endregion

		#region Methods

		// Initialize position dependent portions of the velocity constraints.
		public void InitializeVelocityConstraints()
		{
			for (int i = 0; i < this.count; ++i)
			{
				ref ContactConstraint constraint = ref this.constraints[i];

				float radiusA = constraint.RadiusA;
				float radiusB = constraint.RadiusB;
				Manifold manifold = this.contacts[i].manifold;

				int indexA = constraint.IndexA;
				int indexB = constraint.IndexB;
				
				float mA = constraint.InvMassA;
				float mB = constraint.InvMassB;
				float iA = constraint.InvIA;
				float iB = constraint.InvIB;
				Vector2 localCenterA = constraint.LocalCenterA;
				Vector2 localCenterB = constraint.LocalCenterB;

				Vector2 cA = this.states[indexA].Position;
				float aA = this.states[indexA].Angle;
				Vector2 vA = this.states[indexA].LinearVelocity;
				float wA = this.states[indexA].AngularVelocity;

				Vector2 cB = this.states[indexB].Position;
				float aB = this.states[indexB].Angle;
				Vector2 vB = this.states[indexB].LinearVelocity;
				float wB = this.states[indexB].AngularVelocity;

				Transform xfA, xfB;

				xfA.Q.S = (float)System.Math.Sin(aA);
				xfA.Q.C = (float)System.Math.Cos(aA);

				xfB.Q.S = (float)System.Math.Sin(aB);
				xfB.Q.C = (float)System.Math.Cos(aB);

				xfA.P.X = cA.X - (xfA.Q.C * localCenterA.X - xfA.Q.S * localCenterA.Y);
				xfA.P.Y = cA.Y - (xfA.Q.S * localCenterA.X + xfA.Q.C * localCenterA.Y);

				xfB.P.X = cB.X - (xfB.Q.C * localCenterB.X - xfB.Q.S * localCenterB.Y);
				xfB.P.Y = cB.Y - (xfB.Q.S * localCenterB.X + xfB.Q.C * localCenterB.Y);

				//b2WorldManifold worldManifold = new b2WorldManifold();
				WorldManifold worldManifold = new WorldManifold(manifold, xfA, radiusA, xfB, radiusB);

				Vector2 normal = worldManifold.Normal;
				constraint.Normal = normal;

				Vector2 tangent;
				tangent.X = normal.Y; //  b2Math.b2Cross(vc.normal, 1.0f);
				tangent.Y = -normal.X;

				int pointCount = constraint.Points.Count;
				for (int j = 0; j < pointCount; ++j)
				{
					ref ConstraintPoint vcp = ref constraint.Points.Ref(j);

					Vector2 point = worldManifold.Points[j];

					vcp.rA.X = point.X - cA.X;
					vcp.rA.Y = point.Y - cA.Y;

					vcp.rB.X = point.X - cB.X;
					vcp.rB.Y = point.Y - cB.Y;

					float rnA = vcp.rA.X * normal.Y - vcp.rA.Y * normal.X;
					float rnB = vcp.rB.X * normal.Y - vcp.rB.Y * normal.X;

					float kNormal = mA + mB + iA * rnA * rnA + iB * rnB * rnB;

					vcp.NormalMass = kNormal > 0.0f ? 1.0f / kNormal : 0.0f;

					float rtA = vcp.rA.X * tangent.Y - vcp.rA.Y * tangent.X;
					float rtB = vcp.rB.X * tangent.Y - vcp.rB.Y * tangent.X;

					float kTangent = mA + mB + iA * rtA * rtA + iB * rtB * rtB;

					vcp.TangentMass = kTangent > 0.0f ? 1.0f / kTangent : 0.0f;

					// Setup a velocity bias for restitution.
					vcp.VelocityBias = 0.0f;

					float bx = -wB * vcp.rB.Y;
					float by = wB * vcp.rB.X;
					float ax = -wA * vcp.rA.Y;
					float ay = wA * vcp.rA.X;

					float vRel = normal.X * (vB.X + bx - vA.X - ax) + normal.Y * (vB.Y + by - vA.Y - ay);
					if (vRel < -Settings.VelocityThreshold)
						vcp.VelocityBias = -constraint.Restitution * vRel;
				}

				// If we have two points, then prepare the block solver.
				if (constraint.Points.Count == 2)
				{
					ref ConstraintPoint vcp1 = ref constraint.Points.Item0;
					ref ConstraintPoint vcp2 = ref constraint.Points.Item1;

					float rn1A = vcp1.rA.X * normal.Y - vcp1.rA.Y * normal.X;
					float rn1B = vcp1.rB.X * normal.Y - vcp1.rB.Y * normal.X;
					float rn2A = vcp2.rA.X * normal.Y - vcp2.rA.Y * normal.X;
					float rn2B = vcp2.rB.X * normal.Y - vcp2.rB.Y * normal.X;

					float k11 = mA + mB + iA * rn1A * rn1A + iB * rn1B * rn1B;
					float k22 = mA + mB + iA * rn2A * rn2A + iB * rn2B * rn2B;
					float k12 = mA + mB + iA * rn1A * rn2A + iB * rn1B * rn2B;

					// Ensure a reasonable condition number.
					const float maxConditionNumber = 1000.0f;
					if (k11 * k11 < maxConditionNumber * (k11 * k22 - k12 * k12))
					{
						constraint.K.M11 = k11;
						constraint.K.M12 = k12;
						constraint.K.M21 = k12;
						constraint.K.M22 = k22;
						Matrix2x2.Invert(in constraint.K, out constraint.NormalMass);
					}
					else
					{
						// The constraints are redundant, just use one.
						// TODO_ERIN use deepest?
						constraint.Points.CountInternal = 1;
					}
				}
			}
		}

		public void WarmStart()
		{
			// Warm start.
			for (int i = 0; i < this.count; ++i)
			{
				ref ContactConstraint constraint = ref this.constraints[i];

				int indexA = constraint.IndexA;
				int indexB = constraint.IndexB;
				float mA = constraint.InvMassA;
				float iA = constraint.InvIA;
				float mB = constraint.InvMassB;
				float iB = constraint.InvIB;
				int pointCount = constraint.Points.Count;

				Vector2 vA = this.states[indexA].LinearVelocity;
				float wA = this.states[indexA].AngularVelocity;
				Vector2 vB =this.states[indexB].LinearVelocity;
				float wB = this.states[indexB].AngularVelocity;

				Vector2 normal = constraint.Normal;
				Vector2 tangent; //  b2Math.b2Cross(normal, 1.0f);
				tangent.X = normal.Y;
				tangent.Y = -normal.X;

				for (int j = 0; j < pointCount; ++j)
				{
					ref ConstraintPoint vcp = ref constraint.Points.Ref(j);

					Vector2 P;
					P.X = vcp.NormalImpulse * normal.X + vcp.TangentImpulse * tangent.X;
					P.Y = vcp.NormalImpulse * normal.Y + vcp.TangentImpulse * tangent.Y;

					wA -= iA * Vector2.Cross(in vcp.rA, in P);

					vA.X -= mA * P.X;
					vA.Y -= mA * P.Y;

					wB += iB * Vector2.Cross(in vcp.rB, in P);

					vB.X += mB * P.X;
					vB.Y += mB * P.Y;
				}

				this.states[indexA].LinearVelocity = vA;
				this.states[indexA].AngularVelocity = wA;
				this.states[indexB].LinearVelocity = vB;
				this.states[indexB].AngularVelocity = wB;
			}
		}

		public void SolveVelocityConstraints()
		{
			for (int i = 0; i < this.count; ++i)
			{
				ref ContactConstraint constraint = ref this.constraints[i];

				int indexA = constraint.IndexA;
				int indexB = constraint.IndexB;
				float mA = constraint.InvMassA;
				float iA = constraint.InvIA;
				float mB = constraint.InvMassB;
				float iB = constraint.InvIB;
				int pointCount = constraint.Points.Count;

				Vector2 vA =this.states[indexA].LinearVelocity;
				float wA = this.states[indexA].AngularVelocity;
				Vector2 vB = this.states[indexB].LinearVelocity;
				float wB = this.states[indexB].AngularVelocity;

				Vector2 normal = constraint.Normal;
				Vector2 tangent;
				tangent.X = normal.Y;
				tangent.Y = -normal.X;

				float friction = constraint.Friction;

				// Solve tangent constraints first because non-penetration is more important
				// than friction.
				for (int j = 0; j < pointCount; ++j)
				{
					ref ConstraintPoint vcp = ref constraint.Points.Ref(j);

					// Relative velocity at contact
					/*
						b.m_x = -s * a.m_y;
						b.m_y = s * a.m_x;
					 */

					// Vector2 dv = vB + b2Math.b2Cross(wB, ref vcp.rB) - vA - b2Math.b2Cross(wA, ref vcp.rA);
					Vector2 dv;
					dv.X = vB.X + (-wB * vcp.rB.Y) - vA.X - (-wA * vcp.rA.Y);
					dv.Y = vB.Y + (wB * vcp.rB.X) - vA.Y - (wA * vcp.rA.X);

					// Compute tangent force
					float vt = dv.X * tangent.X + dv.Y * tangent.Y - constraint.TangentSpeed; // b2Math.b2Dot(dv, tangent);
					float lambda = vcp.TangentMass * (-vt);

					// b2Math.b2Clamp the accumulated force
					float maxFriction = friction * vcp.NormalImpulse;
					float newImpulse = vcp.TangentImpulse + lambda;
					if (newImpulse < -maxFriction)
						newImpulse = -maxFriction;
					else if (newImpulse > maxFriction)
						newImpulse = maxFriction;

					lambda = newImpulse - vcp.TangentImpulse;
					vcp.TangentImpulse = newImpulse;

					// Apply contact impulse
					// P = lambda * tangent;
					Vector2 P;
					P.X = lambda * tangent.X;
					P.Y = lambda * tangent.Y;

					// vA -= mA * P;
					vA.X -= mA * P.X;
					vA.Y -= mA * P.Y;

					// wA -= iA * b2Math.b2Cross(vcp.rA, P);
					wA -= iA * (vcp.rA.X * P.Y - vcp.rA.Y * P.X);

					// vB += mB * P;
					vB.X += mB * P.X;
					vB.Y += mB * P.Y;

					// wB += iB * b2Math.b2Cross(vcp.rB, P);
					wB += iB * (vcp.rB.X * P.Y - vcp.rB.Y * P.X);
				}

				// Solve normal constraints
				if (constraint.Points.Count == 1)
				{
					ref ConstraintPoint vcp = ref constraint.Points.Item0;

					// Relative velocity at contact
					// Vector2 dv = vB + b2Math.b2Cross(wB, ref vcp.rB) - vA - b2Math.b2Cross(wA, ref vcp.rA);
					float dvx = vB.X + (-wB * vcp.rB.Y) - vA.X - (-wA * vcp.rA.Y);
					float dvy = vB.Y + (wB * vcp.rB.X) - vA.Y - (wA * vcp.rA.X);

					// Compute normal impulse
					float vn = dvx * normal.X + dvy * normal.Y; //b2Math.b2Dot(ref dv, ref normal);
					float lambda = -vcp.NormalMass * (vn - vcp.VelocityBias);

					// b2Math.b2Clamp the accumulated impulse
					float newImpulse = vcp.NormalImpulse + lambda;
					if (newImpulse < 0.0f)
						newImpulse = 0.0f;

					lambda = newImpulse - vcp.NormalImpulse;
					vcp.NormalImpulse = newImpulse;

					// Apply contact impulse
					//Vector2 P = lambda * normal;
					Vector2 P;
					P.X = lambda * normal.X;
					P.Y = lambda * normal.Y;

					// vA -= mA * P;
					vA.X -= mA * P.X;
					vA.Y -= mA * P.Y;

					// wA -= iA * b2Math.b2Cross(vcp.rA, P);
					wA -= iA * (vcp.rA.X * P.Y - vcp.rA.Y * P.X);

					// vB += mB * P;
					vB.X += mB * P.X;
					vB.Y += mB * P.Y;

					// wB += iB * b2Math.b2Cross(vcp.rB, P);
					wB += iB * (vcp.rB.X * P.Y - vcp.rB.Y * P.X);

					//vc.points[0] = vcp;
				}
				else
				{
					// Block solver developed in collaboration with Dirk Gregorius (back in 01/07 on Box2D_Lite).
					// Build the mini LCP for this contact patch
					//
					// vn = A * x + b, vn >= 0, , vn >= 0, x >= 0 and vn_i * x_i = 0 with i = 1..2
					//
					// A = J * W * JT and J = ( -n, -r1 x n, n, r2 x n )
					// b = vn0 - velocityBias
					//
					// The system is solved using the "Total enumeration method" (s. Murty). The complementary constraint vn_i * x_i
					// implies that we must have in any solution either vn_i = 0 or x_i = 0. So for the 2D contact problem the cases
					// vn1 = 0 and vn2 = 0, x1 = 0 and x2 = 0, x1 = 0 and vn2 = 0, x2 = 0 and vn1 = 0 need to be tested. The first valid
					// solution that satisfies the problem is chosen.
					// 
					// In order to account of the accumulated impulse 'a' (because of the iterative nature of the solver which only requires
					// that the accumulated impulse is clamped and not the incremental impulse) we change the impulse variable (x_i).
					//
					// Substitute:
					// 
					// x = a + d
					// 
					// a := old total impulse
					// x := new total impulse
					// d := incremental impulse 
					//
					// For the current iteration we extend the formula for the incremental impulse
					// to compute the new total impulse:
					//
					// vn = A * d + b
					//    = A * (x - a) + b
					//    = A * x + b - A * a
					//    = A * x + b'
					// b' = b - A * a;

					ref ConstraintPoint cp1 = ref constraint.Points.Item0;
					ref ConstraintPoint cp2 = ref constraint.Points.Item1;

					Vector2 a;
					a.X = cp1.NormalImpulse;
					a.Y = cp2.NormalImpulse;

					// Relative velocity at contact
					// vB + b2Math.b2Cross(wB, ref cp1.rB) - vA - b2Math.b2Cross(wA, ref cp1.rA);
					Vector2 dv1;
					dv1.X = vB.X + (-wB * cp1.rB.Y) - vA.X - (-wA * cp1.rA.Y);
					dv1.Y = vB.Y + (wB * cp1.rB.X) - vA.Y - (wA * cp1.rA.X);

					// vB + b2Math.b2Cross(wB, ref cp2.rB) - vA - b2Math.b2Cross(wA, ref cp2.rA);
					Vector2 dv2;
					dv2.X = vB.X + (-wB * cp2.rB.Y) - vA.X - (-wA * cp2.rA.Y);
					dv2.Y = vB.Y + (wB * cp2.rB.X) - vA.Y - (wA * cp2.rA.X);

					// Compute normal velocity
					float vn1 = dv1.X * normal.X + dv1.Y * normal.Y; // b2Math.b2Dot(ref dv1, ref normal);
					float vn2 = dv2.X * normal.X + dv2.Y * normal.Y; // b2Math.b2Dot(ref dv2, ref normal);

					Vector2 b;
					b.X = vn1 - cp1.VelocityBias;
					b.Y = vn2 - cp2.VelocityBias;

					// Compute b'
					// (A.ex.X * v.X + A.ey.X * v.Y, A.ex.Y * v.X + A.ey.Y * v.Y)
					b.X -= (constraint.K.M11 * a.X + constraint.K.M12 * a.Y);
					b.Y -= (constraint.K.M21 * a.X + constraint.K.M22 * a.Y);
					// b -= b2Math.b2Mul(vc.K, a);

					//            float k_errorTol = 1e-3f;

					#region Iteration

					for(;;)
					{
						//
						// Case 1: vn = 0
						//
						// 0 = A * x + b'
						//
						// Solve for x:
						//
						// x = - inv(A) * b'
						//
						Vector2 x;
						x.X = -(constraint.NormalMass.M11 * b.X + constraint.NormalMass.M12 * b.Y);
						x.Y = -(constraint.NormalMass.M21 * b.X + constraint.NormalMass.M22 * b.Y);

						if (x.X >= 0.0f && x.Y >= 0.0f)
						{
							// Get the incremental impulse
							Vector2 d;
							d.X = x.X - a.X;
							d.Y = x.Y - a.Y;

							// Apply incremental impulse
							Vector2 P1;
							P1.X = d.X * normal.X;
							P1.Y = d.X * normal.Y;

							Vector2 P2;
							P2.X = d.Y * normal.X;
							P2.Y = d.Y * normal.Y;

							Vector2 P12;
							P12.X = P1.X + P2.X;
							P12.Y = P1.Y + P2.Y;

							vA.X -= mA * P12.X;
							vA.Y -= mA * P12.Y;
							wA -= iA * (cp1.rA.X * P1.Y - cp1.rA.Y * P1.X + (cp2.rA.X * P2.Y - cp2.rA.Y * P2.X));

							vB.X += mB * P12.X;
							vB.Y += mB * P12.Y;
							wB += iB * (cp1.rB.X * P1.Y - cp1.rB.Y * P1.X + (cp2.rB.X * P2.Y - cp2.rB.Y * P2.X));

							// Accumulate
							cp1.NormalImpulse = x.X;
							cp2.NormalImpulse = x.Y;

							break;
						}

						//
						// Case 2: vn1 = 0 and x2 = 0
						//
						//   0 = a11 * x1 + a12 * 0 + b1' 
						// vn2 = a21 * x1 + a22 * 0 + b2'
						//
						x.X = -cp1.NormalMass * b.X;
						x.Y = 0.0f;

						//vn1 = 0.0f;
						//vn2 = constraint.K.M21 * x.X + b.Y;

						if (x.X >= 0.0f && vn2 >= 0.0f)
						{
							// Get the incremental impulse
							Vector2 d;
							d.X = x.X - a.X;
							d.Y = x.Y - a.Y;

							// Apply incremental impulse
							Vector2 P1;
							P1.X = d.X * normal.X;
							P1.Y = d.X * normal.Y;

							Vector2 P2;
							P2.X = d.Y * normal.X;
							P2.Y = d.Y * normal.Y;

							Vector2 P12;
							P12.X = P1.X + P2.X;
							P12.Y = P1.Y + P2.Y;

							vA.X -= mA * P12.X;
							vA.Y -= mA * P12.Y;
							wA -= iA * (cp1.rA.X * P1.Y - cp1.rA.Y * P1.X + (cp2.rA.X * P2.Y - cp2.rA.Y * P2.X));

							vB.X += mB * P12.X;
							vB.Y += mB * P12.Y;
							wB += iB * (cp1.rB.X * P1.Y - cp1.rB.Y * P1.X + (cp2.rB.X * P2.Y - cp2.rB.Y * P2.X));

							// Accumulate
							cp1.NormalImpulse = x.X;
							cp2.NormalImpulse = x.Y;

							break;
						}

						//
						// Case 3: vn2 = 0 and x1 = 0
						//
						// vn1 = a11 * 0 + a12 * x2 + b1' 
						//   0 = a21 * 0 + a22 * x2 + b2'
						//
						x.X = 0.0f;
						x.Y = -cp2.NormalMass * b.Y;

						//vn1 = constraint.K.M12 * x.Y + b.X;
						//vn2 = 0.0f;

						if (x.Y >= 0.0f && vn1 >= 0.0f)
						{
							// Resubstitute for the incremental impulse
							Vector2 d;
							d.X = x.X - a.X;
							d.Y = x.Y - a.Y;

							// Apply incremental impulse
							Vector2 P1;
							P1.X = d.X * normal.X;
							P1.Y = d.X * normal.Y;

							Vector2 P2;
							P2.X = d.Y * normal.X;
							P2.Y = d.Y * normal.Y;

							Vector2 P12;
							P12.X = P1.X + P2.X;
							P12.Y = P1.Y + P2.Y;

							vA.X -= mA * P12.X;
							vA.Y -= mA * P12.Y;
							wA -= iA * (cp1.rA.X * P1.Y - cp1.rA.Y * P1.X + (cp2.rA.X * P2.Y - cp2.rA.Y * P2.X));

							vB.X += mB * P12.X;
							vB.Y += mB * P12.Y;
							wB += iB * (cp1.rB.X * P1.Y - cp1.rB.Y * P1.X + (cp2.rB.X * P2.Y - cp2.rB.Y * P2.X));

							// Accumulate
							cp1.NormalImpulse = x.X;
							cp2.NormalImpulse = x.Y;

							break;
						}

						//
						// Case 4: x1 = 0 and x2 = 0
						// 
						// vn1 = b1
						// vn2 = b2;
						x.X = 0.0f;
						x.Y = 0.0f;
						vn1 = b.X;
						vn2 = b.Y;

						if (vn1 >= 0.0f && vn2 >= 0.0f)
						{
							// Resubstitute for the incremental impulse
							Vector2 d;
							d.X = x.X - a.X;
							d.Y = x.Y - a.Y;

							// Apply incremental impulse
							Vector2 P1;
							P1.X = d.X * normal.X;
							P1.Y = d.X * normal.Y;

							Vector2 P2;
							P2.X = d.Y * normal.X;
							P2.Y = d.Y * normal.Y;

							Vector2 P12;
							P12.X = P1.X + P2.X;
							P12.Y = P1.Y + P2.Y;

							vA.X -= mA * P12.X;
							vA.Y -= mA * P12.Y;
							wA -= iA * (cp1.rA.X * P1.Y - cp1.rA.Y * P1.X + (cp2.rA.X * P2.Y - cp2.rA.Y * P2.X));

							vB.X += mB * P12.X;
							vB.Y += mB * P12.Y;
							wB += iB * (cp1.rB.X * P1.Y - cp1.rB.Y * P1.X + (cp2.rB.X * P2.Y - cp2.rB.Y * P2.X));

							// Accumulate
							cp1.NormalImpulse = x.X;
							cp2.NormalImpulse = x.Y;
						}

						// No solution, give up. This is hit sometimes, but it doesn't seem to matter.
						break;
					}

					#endregion
				}

				this.states[indexA].LinearVelocity = vA;
				this.states[indexA].AngularVelocity = wA;
				this.states[indexB].LinearVelocity = vB;
				this.states[indexB].AngularVelocity = wB;
			}
		}

		public void StoreImpulses()
		{
			for (int i = 0; i < this.count; ++i)
			{
				ref ContactConstraint constraint = ref this.constraints[i];
				Manifold manifold = this.contacts[i].manifold;

				for (int j = 0; j < constraint.Points.Count; ++j)
				{
					ref ManifoldPoint p = ref manifold.Points.Ref(j);
					p.NormalImpulse = constraint.Points[j].NormalImpulse;
					p.TangentImpulse = constraint.Points[j].TangentImpulse;
				}
			}
		}
		
		// Sequential solver.
		public bool SolvePositionConstraints()
		{
			float minSeparation = 0.0f;

			for (int i = 0; i < this.count; ++i)
			{
				ref ContactConstraint constraint = ref this.constraints[i];

				int indexA = constraint.IndexA;
				int indexB = constraint.IndexB;
				Vector2 localCenterA = constraint.LocalCenterA;
				float mA = constraint.InvMassA;
				float iA = constraint.InvIA;
				Vector2 localCenterB = constraint.LocalCenterB;
				float mB = constraint.InvMassB;
				float iB = constraint.InvIB;
				int pointCount = constraint.Points.Count;

				Vector2 cA = this.states[indexA].Position;
				float aA = this.states[indexA].Angle;

				Vector2 cB = this.states[indexB].Position;
				float aB = this.states[indexB].Angle;

				// Solve normal constraints
				for (int j = 0; j < pointCount; ++j)
				{
					Transform xfA;
					xfA.Q.S = (float)System.Math.Sin(aA);
					xfA.Q.C = (float)System.Math.Cos(aA);

					Transform xfB;
					xfB.Q.S = (float)System.Math.Sin(aB);
					xfB.Q.C = (float)System.Math.Cos(aB);

					Vector2 b;
					b.X = xfA.Q.C * localCenterA.X - xfA.Q.S * localCenterA.Y;
					b.Y = xfA.Q.S * localCenterA.X + xfA.Q.C * localCenterA.Y;
					xfA.P.X = cA.X - b.X;
					xfA.P.Y = cA.Y - b.Y;

					b.X = xfB.Q.C * localCenterB.X - xfB.Q.S * localCenterB.Y;
					b.Y = xfB.Q.S * localCenterB.X + xfB.Q.C * localCenterB.Y;
					xfB.P.X = cB.X - b.X;
					xfB.P.Y = cB.Y - b.Y;

					PositionSolverManifold psm = new PositionSolverManifold(constraint, ref xfA, ref xfB, j);

					Vector2 normal;
					normal.X = psm.Normal.X;
					normal.Y = psm.Normal.Y;

					Vector2 point = psm.Point;
					float separation = psm.Separation;

					float rAx = point.X - cA.X;
					float rAy = point.Y - cA.Y;

					float rBx = point.X - cB.X;
					float rBy = point.Y - cB.Y;

					// Track max constraint error.
					minSeparation = System.Math.Min(minSeparation, separation);

					// Prevent large corrections and allow slop.
					float C = Settings.Baumgarte * (separation + Settings.LinearSlop);
					if (C < -Settings.MaxLinearCorrection)
					{
						C = -Settings.MaxLinearCorrection;
					}
					else if (C > 0)
					{
						C = 0;
					}

					// Compute the effective mass.
					float rnA = rAx * normal.Y - rAy * normal.X;
					float rnB = rBx * normal.Y - rBy * normal.X;
					float K = mA + mB + iA * rnA * rnA + iB * rnB * rnB;

					// Compute normal impulse
					float impulse = K > 0.0f ? -C / K : 0.0f;

					float Px = impulse * normal.X;
					float Py = impulse * normal.Y;

					cA.X -= mA * Px;
					cA.Y -= mA * Py;
					aA -= iA * (rAx * Py - rAy * Px);

					cB.X += mB * Px;
					cB.Y += mB * Py;
					aB += iB * (rBx * Py - rBy * Px);
				}

				this.states[indexA].Position = cA;
				this.states[indexA].Angle = aA;

				this.states[indexB].Position = cB;
				this.states[indexB].Angle = aB;
			}

			// We can't expect minSpeparation >= -b2_linearSlop because we don't
			// push the separation above -b2_linearSlop.
			return minSeparation >= -3.0f * Settings.LinearSlop;
		}

		// Sequential position solver for position constraints.
		public bool SolveTOIPositionConstraints(int toiIndexA, int toiIndexB)
		{
			float minSeparation = 0.0f;

			for (int i = 0; i < this.count; ++i)
			{
				ref ContactConstraint constraint = ref this.constraints[i];

				int indexA = constraint.IndexA;
				int indexB = constraint.IndexB;

				Vector2 localCenterA = constraint.LocalCenterA;
				Vector2 localCenterB = constraint.LocalCenterB;
				int pointCount = constraint.Points.Count;

				float mA = 0.0f;
				float iA = 0.0f;
				if (indexA == toiIndexA || indexA == toiIndexB)
				{
					mA = constraint.InvMassA;
					iA = constraint.InvIA;
				}

				float mB = 0.0f;
				float iB = 0.0f;
				if (indexB == toiIndexA || indexB == toiIndexB)
				{
					mB = constraint.InvMassB;
					iB = constraint.InvIB;
				}

				Vector2 cA = this.states[indexA].Position;
				float aA = this.states[indexA].Angle;

				Vector2 cB = this.states[indexB].Position;
				float aB = this.states[indexB].Angle;

				// Solve normal constraints
				for (int j = 0; j < pointCount; ++j)
				{
					Transform xfA, xfB;
					xfA.Q.S = (float)System.Math.Sin(aA);
					xfA.Q.C = (float)System.Math.Cos(aA);
					xfB.Q.S = (float)System.Math.Sin(aB);
					xfB.Q.C = (float)System.Math.Cos(aB);

					xfA.P.X = cA.X - (xfA.Q.C * localCenterA.X - xfA.Q.S * localCenterA.Y);
					xfA.P.Y = cA.Y - (xfA.Q.S * localCenterA.X + xfA.Q.C * localCenterA.Y);

					xfB.P.X = cB.X - (xfB.Q.C * localCenterB.X - xfB.Q.S * localCenterB.Y);
					xfB.P.Y = cB.Y - (xfB.Q.S * localCenterB.X + xfB.Q.C * localCenterB.Y);

					PositionSolverManifold psm = new PositionSolverManifold(constraint, ref xfA, ref xfB, j);
					Vector2 normal = psm.Normal;

					Vector2 point = psm.Point;
					float separation = psm.Separation;

					float rAx = point.X - cA.X;
					float rAy = point.Y - cA.Y;

					float rBx = point.X - cB.X;
					float rBy = point.Y - cB.Y;

					// Track max constraint error.
					minSeparation = System.Math.Min(minSeparation, separation);

					// Prevent large corrections and allow slop.
					float C = Utility.Clamp(Settings.TOIBaugarte * (separation + Settings.LinearSlop), -Settings.MaxLinearCorrection, 0.0f);

					// Compute the effective mass.
					float rnA = rAx * normal.Y - rAy * normal.X;
					float rnB = rBx * normal.Y - rBy * normal.X;

					float K = mA + mB + iA * rnA * rnA + iB * rnB * rnB;

					// Compute normal impulse
					float impulse = K > 0.0f ? -C / K : 0.0f;

					float Px = impulse * normal.X;
					float Py = impulse * normal.Y;

					cA.X -= mA * Px;
					cA.Y -= mA * Py;

					aA -= iA * (rAx * Py - rAy * Px);

					cB.X += mB * Px;
					cB.Y += mB * Py;

					aB += iB * (rBx * Py - rBy * Px);
				}

				this.states[indexA].Position = cA;
				this.states[indexA].Angle = aA;

				this.states[indexB].Position = cB;
				this.states[indexB].Angle = aB;
			}

			// We can't expect minSpeparation >= -b2_linearSlop because we don't
			// push the separation above -b2_linearSlop.
			return minSeparation >= -1.5f * Settings.LinearSlop;
		}

		#endregion
	}
}
