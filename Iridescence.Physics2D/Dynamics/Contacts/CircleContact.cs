/*
* Copyright (c) 2006-2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

using System;
using Iridescence.Physics2D.Collision;
using Iridescence.Physics2D.Collision.Shapes;

namespace Iridescence.Physics2D.Dynamics.Contacts
{
	internal sealed class CircleContact : Contact
	{
		#region Fields

#if CONTACT_POOL
		private static readonly Pool<CircleContact> pool = new Pool<CircleContact>(() => new CircleContact());
#endif

		#endregion

		#region Constructors

#if CONTACT_POOL
		public CircleContact()
		{
			// for use in pool.
		}
#endif

		public CircleContact(Fixture fixtureA, Fixture fixtureB)
			: base(fixtureA, 0, fixtureB, 0)
		{
			if (!((this.fixtureA.shape is CircleShape) && (this.fixtureB.shape is CircleShape)))
				throw new ArgumentException("Expected CircleShapes.");
		}

		#endregion

		#region Methods

		protected override void Evaluate(Manifold manifold, in Transform xfA, in Transform xfB)
		{
			Collisions.CollideCircles(manifold, (CircleShape)this.fixtureA.shape, in xfA, (CircleShape)this.fixtureB.shape, in xfB);
		}

#if CONTACT_POOL
		public new static Contact Create(Fixture fixtureA, int indexA, Fixture fixtureB, int indexB)
		{
			CircleContact contact = pool.Take();
			contact.Initialize(fixtureA, indexA, fixtureB, indexB);
			return contact;

			//return new CircleContact(fixtureA, fixtureB);
		}

		internal static void Destroy(Contact contact)
		{
			pool.Give((CircleContact)contact);
		}
#endif

		#endregion
	}
}
