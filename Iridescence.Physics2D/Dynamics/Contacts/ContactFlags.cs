﻿using System;

namespace Iridescence.Physics2D.Dynamics.Contacts
{
	[Flags]
	internal enum ContactFlags
	{
		// Used when crawling contact graph when forming islands.
		Island = 1 << 0,

		// Set when the shapes are touching.
		Touching = 1 << 1,

		// This contact can be disabled (by user)
		Enabled = 1 << 2,

		// This contact needs filtering because a fixture filter was changed.
		Filter = 1 << 3,

		// This bullet contact had a TOI event
		BulletHit = 1 << 4,

		// This contact has a valid TOI in m_toi
		Toi = 1 << 5,

		// Event states to keep track who had BeginContact and EndContact called on them.
		EventContactManager = 1 << 6,
		EventFixtureA = 1 << 7,
		EventFixtureB = 1 << 8,

		Initialized = 1 << 9,
		Destroyed = 1 << 10,
	}
}
