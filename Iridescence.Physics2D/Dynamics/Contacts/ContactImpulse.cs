﻿namespace Iridescence.Physics2D.Dynamics.Contacts
{
	/// <summary>
	/// Contact impulses for reporting. Impulses are used instead of forces because
	/// sub-step forces may approach infinity for rigid body collisions. These
	/// match up one-to-one with the contact points in b2Manifold.
	/// </summary>
	public struct ContactImpulse
	{
		#region Fields

		public int Count;
		public float[] NormalImpulses;
		public float[] TangentImpulses;

		#endregion

		#region Methods

		public static ContactImpulse Create()
		{
			ContactImpulse b;
			b.NormalImpulses = new float[Settings.MaxManifoldPoints];
			b.TangentImpulses = new float[Settings.MaxManifoldPoints];
			b.Count = 0;
			return b;
		}

		#endregion
	}
}
