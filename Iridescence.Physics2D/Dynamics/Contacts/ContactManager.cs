using System.Collections.Generic;
using System.Diagnostics;
using Iridescence.Physics2D.Collision;

namespace Iridescence.Physics2D.Dynamics.Contacts
{
	/// <summary>
	/// Contact event handler.
	/// </summary>
	/// <param name="contact"></param>
	public delegate void ContactEventHandler(Contact contact);

	/// <summary>
	/// Contact pre-solve event handler.
	/// </summary>
	/// <param name="contact"></param>
	/// <param name="manifold"></param>
	public delegate void ContactPreSolveEventHandler(Contact contact, Manifold manifold);

	/// <summary>
	/// Contact post-solve event handler.
	/// </summary>
	/// <param name="contact"></param>
	/// <param name="impulse"></param>
	public delegate void ContactPostSolveEventHandler(Contact contact, ref ContactImpulse impulse);

	/// <summary>
	/// Manages contacts.
	/// </summary>
	public sealed class ContactManager
	{
		#region Events

		/// <summary>
		/// Occurs when two fixtures begin to touch.
		/// </summary>
		public event ContactEventHandler BeginContact;

		/// <summary>
		/// Occurs when two fixtures cease to touch.
		/// </summary>
		public event ContactEventHandler EndContact;
		
		/// <summary>
		/// This is called after a contact is updated. This allows you to inspect a
		/// contact before it goes to the solver. If you are careful, you can modify the
		/// contact manifold (e.g. disable contact).
		/// A copy of the old manifold is provided so that you can detect changes.
		/// </summary>
		/// <remarks>
		/// This is called only for awake bodies.
		/// This is called even when the number of contact points is zero.
		/// This is not called for sensors.
		/// If you set the number of contact points to zero, you will not get an EndContact callback. However, you may get a BeginContact callback the next step.
		/// </remarks>
		public event ContactPreSolveEventHandler PreSolve;

		/// <summary>
		/// This lets you inspect a contact after the solver is finished. This is useful for inspecting impulses.
		/// </summary>
		/// <remarks>
		/// The contact manifold does not include time of impact impulses, which can be arbitrarily large if the sub-step is small.
		/// Hence the impulse is provided explicitly in a separate data structure.
		/// This is only called for contacts that are touching, solid, and awake.
		/// </remarks>
		public event ContactPostSolveEventHandler PostSolve;

		#endregion

		#region Fields

		internal readonly BroadPhase broadPhase;
		private readonly List<Contact> contacts;
		private ContactFilter contactFilter;
		private int collideIndex;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the contact filter.
		/// </summary>
		public ContactFilter ContactFilter
		{
			set => this.contactFilter = value;
			get => (this.contactFilter);
		}

		/// <summary>
		/// Gets the contacts of the contact manager.
		/// </summary>
		public IReadOnlyList<Contact> Contacts => this.contacts;

		#endregion

		#region Constructors
		
		public ContactManager()
		{
			this.contacts = new List<Contact>();
			this.contactFilter = new ContactFilter();
			this.broadPhase = new BroadPhase();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Raises the <see cref="BeginContact"/> event, keeping track of whether it has already been raised.
		/// </summary>
		internal void InternalBeginContact(Contact c)
		{
			Debug.Assert(!c.IsDestroyed);

			if ((c.flags & ContactFlags.EventContactManager) == 0)
			{
				c.flags |= ContactFlags.EventContactManager;
				this.BeginContact?.Invoke(c);
			}

			if ((c.flags & (ContactFlags.Destroyed | ContactFlags.EventFixtureA)) == 0)
			{
				c.flags |= ContactFlags.EventFixtureA;
				c.FixtureA?.InternalBeginContact(c);
			}
			
			if ((c.flags & (ContactFlags.Destroyed | ContactFlags.EventFixtureB)) == 0)
			{
				c.flags |= ContactFlags.EventFixtureB;
				c.FixtureB?.InternalBeginContact(c);
			}
		}

		/// <summary>
		/// Raises the <see cref="EndContact"/> event, keeping track of whether it has already been raised.
		/// </summary>
		internal void InternalEndContact(Contact c)
		{
			Debug.Assert(!c.IsDestroyed);

			if ((c.flags & ContactFlags.EventContactManager) != 0)
			{
				c.flags &= ~ContactFlags.EventContactManager;
				this.EndContact?.Invoke(c);
			}

			if ((c.flags & ContactFlags.EventFixtureA) != 0)
			{
				c.flags &= ~ContactFlags.EventFixtureA;
				c.FixtureA?.InternalEndContact(c);
			}
			
			if ((c.flags & ContactFlags.EventFixtureB) != 0)
			{
				c.flags &= ~ContactFlags.EventFixtureB;
				c.FixtureB?.InternalEndContact(c);
			}
		}

		/// <summary>
		/// Raises the <see cref="PreSolve"/> event.
		/// </summary>
		internal void InternalPreSolve(Contact c, Manifold m)
		{
			Debug.Assert(!c.IsDestroyed);

			this.PreSolve?.Invoke(c, m);

			c.FixtureA.InternalContactPreSolve(c, m);
			c.FixtureB?.InternalContactPreSolve(c, m);
		}

		/// <summary>
		/// Raises the <see cref="PostSolve"/> event.
		/// </summary>
		internal void InternalPostSolve(Contact c, ref ContactImpulse i)
		{
			Debug.Assert(!c.IsDestroyed);

			this.PostSolve?.Invoke(c, ref i);

			c.FixtureA.InternalContactPostSolve(c, ref i);
			c.FixtureB?.InternalContactPostSolve(c, ref i);
		}

		internal void Remove(Contact c)
		{
			Debug.Assert(!c.IsDestroyed);

			Fixture fixtureA = c.fixtureA;
			Fixture fixtureB = c.fixtureB;
			Body bodyA = fixtureA.body;
			Body bodyB = fixtureB.body;

			this.InternalEndContact(c);

			// Remove from the world.
			int contactIndex = this.contacts.IndexOf(c);
			if (contactIndex < 0)
				return; // The contact might have been deleted in InternalEndContact while already being removed.

			if (contactIndex <= this.collideIndex)
				--this.collideIndex;

			this.contacts.RemoveAt(contactIndex);
			
			// Remove from body 1
			//bodyA.contactEdges.Remove(c.nodeA);
			bool res1 = bodyA.contacts.Remove(bodyB, c);

			// Remove from body 2
			//bodyB.contactEdges.Remove(c.nodeB);
			bool res2 = bodyB.contacts.Remove(bodyA, c);

			Debug.Assert(res1 && res2);

			c.Destroy();
		}

		/// <summary>
		/// This is the top level collision call for the time step. Here
		/// all the narrow phase collision is processed for the world
		/// contact list.
		/// </summary>
		internal void Collide()
		{
			// Update awake contacts.
			for (this.collideIndex = 0; this.collideIndex < this.contacts.Count; ++this.collideIndex)
			{
				Contact c = this.contacts[this.collideIndex];
				Fixture fixtureA = c.fixtureA;
				Fixture fixtureB = c.fixtureB;
				int indexA = c.indexA;
				int indexB = c.indexB;
				Body bodyA = fixtureA.body;
				Body bodyB = fixtureB.body;

				// Is this contact flagged for filtering?
				if ((c.flags & ContactFlags.Filter) != 0)
				{
					// Should these bodies collide?
					if (bodyB.ShouldCollide(bodyA) == false)
					{
						this.Remove(c);
						continue;
					}

					// Check user filtering.
					if (this.contactFilter != null && !this.contactFilter.ShouldCollide(fixtureA, fixtureB))
					{
						this.Remove(c);
						continue;
					}

					// Clear the filtering flag.
					c.flags &= ~ContactFlags.Filter;
				}

				bool activeA = bodyA.Awake && bodyA.type != BodyType.Static;
				bool activeB = bodyB.Awake && bodyB.type != BodyType.Static;

				// At least one body must be awake and it must be dynamic or kinematic.
				if (activeA == false && activeB == false)
				{
					continue;
				}

				int proxyIdA = fixtureA.proxies[indexA].ProxyId;
				int proxyIdB = fixtureB.proxies[indexB].ProxyId;
				bool overlap = this.broadPhase.TestOverlap(proxyIdA, proxyIdB);

				// Here we destroy contacts that cease to overlap in the broad-phase.
				if (overlap == false)
				{
					this.Remove(c);
					continue;
				}

				// The contact persists.
				c.Update(this);
			}
		}

		public void FindNewContacts()
		{
			this.broadPhase.UpdatePairs(this.AddPair);
		}

		public void AddPair(object proxyUserDataA, object proxyUserDataB)
		{
			FixtureProxy proxyA = (FixtureProxy)proxyUserDataA;
			FixtureProxy proxyB = (FixtureProxy)proxyUserDataB;
			
			Fixture fixtureA = proxyA.Fixture;
			Fixture fixtureB = proxyB.Fixture;

			Body bodyA = fixtureA.body;
			Body bodyB = fixtureB.body;

			// Are the fixtures on the same body?
			if (bodyA == bodyB)
				return;

			int indexA = proxyA.ChildIndex;
			int indexB = proxyB.ChildIndex;

			// Does a contact already exist?
			if (bodyB.contacts.TryGetContacts(bodyA, out IEnumerable<Contact> contacts))
			{
				foreach (Contact contact in contacts)
				{
					Fixture fA = contact.fixtureA;
					Fixture fB = contact.fixtureB;
					int iA = contact.indexA;
					int iB = contact.indexB;

					if (fA == fixtureA && fB == fixtureB && iA == indexA && iB == indexB)
					{
						// A contact already exists.
						return;
					}

					if (fA == fixtureB && fB == fixtureA && iA == indexB && iB == indexA)
					{
						// A contact already exists.
						return;
					}
				}
			}

			// Does a joint override collision? Is at least one body dynamic?
			if (!bodyB.ShouldCollide(bodyA))
				return;

			// Check user filtering.
			if (this.contactFilter != null && !this.contactFilter.ShouldCollide(fixtureA, fixtureB))
				return;

			// Call the factory.
			Contact c = Contact.Create(fixtureA, indexA, fixtureB, indexB);
			if (c == null)
				return;

			// Contact creation may swap fixtures.
			bodyA = c.fixtureA.body;
			bodyB = c.fixtureB.body;

			// Insert into the world.
			this.contacts.Add(c);

			// Connect to island graph.

			// Connect to body A
			c.nodeA.Contact = c;
			c.nodeA.Other = bodyB;
			//bodyA.contactEdges.Add(c.nodeA);
			bodyA.contacts.Add(bodyB, c);

			// Connect to body B
			c.nodeB.Contact = c;
			c.nodeB.Other = bodyA;
			//bodyB.contactEdges.Add(c.nodeB);
			bodyB.contacts.Add(bodyA, c);

			// Wake up the bodies
			if (!fixtureA.Sensor && !fixtureB.Sensor)
			{
				bodyA.Awake = true;
				bodyB.Awake = true;
			}
		}

		#endregion
	}
}
