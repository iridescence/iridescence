/*
* Copyright (c) 2006-2009 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

using System;
using System.Diagnostics;
using Iridescence.Physics2D.Collision;
using Iridescence.Physics2D.Collision.Shapes;

namespace Iridescence.Physics2D.Dynamics.Contacts
{
	/// <summary>
	/// The class manages contact between two shapes. A contact exists for each overlapping
	/// AABB in the broad-phase (except if filtered). Therefore a contact object may exist
	/// that has no contact points.
	/// </summary>
	public abstract class Contact
	{
		#region Nested Types

		private delegate Contact CreateFunc(Fixture fixtureA, int indexA, Fixture fixtureB, int indexB);

		private delegate void DestroyFunc(Contact contact);

		private struct ContactRegister
		{
			public CreateFunc CreateFunc;
#if CONTACT_POOL
			public DestroyFunc DestroyFunc;
#endif
			public bool IsPrimary;
		}

		#endregion

		#region Fields

		internal ContactFlags flags;

		// Nodes for connecting bodies.
		internal ContactEdge nodeA;
		internal ContactEdge nodeB;

		internal Fixture fixtureA;
		internal Fixture fixtureB;

		internal int indexA;
		internal int indexB;

		internal readonly Manifold manifold;
		private readonly Manifold oldManifold;

		internal int toiCount;
		internal float toi;

		internal float friction;
		internal float restitution;
		internal float tangentSpeed;

		private static readonly ContactRegister[][] registers;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the contact manifold.
		/// Do not modify the manifold unless you understand the internals of Box2D.
		/// </summary>
		public Manifold Manifold => this.manifold;

		/// <summary>
		/// Gets or sets the restitution of the contact.
		/// </summary>
		public float Restitution
		{
			get => this.restitution;
			set => this.restitution = value;
		}

		/// <summary>
		/// Gets or sets the friction of the contact.
		/// </summary>
		public float Friction
		{
			get => this.friction;
			set => this.friction = value;
		}

		/// <summary>
		/// Enable/disable this contact. This can be used inside the pre-solve
		/// contact listener. The contact is only disabled for the current
		/// time step (or sub-step in continuous collisions).
		/// </summary>
		public bool Enabled
		{
			get => (this.flags & ContactFlags.Enabled) != 0;
			set
			{
				if (value)
					this.flags |= ContactFlags.Enabled;
				else
					this.flags &= ~ContactFlags.Enabled;
			}
		}

		/// <summary>
		/// Returns whether this contact is touching.
		/// </summary>
		/// <returns></returns>
		public bool IsTouching => (this.flags & ContactFlags.Touching) != 0;

		/// <summary>
		/// Gets a value that indicates whether the contact has been removed/destroyed.
		/// </summary>
		public bool IsDestroyed => (this.flags & ContactFlags.Destroyed) != 0;

		/// <summary>
		/// Gets fixture A in this contact.
		/// </summary>
		public Fixture FixtureA => this.fixtureA;

		/// <summary>
		/// Get the child primitive index for fixture A.
		/// </summary>
		public int ChildIndexA => this.indexA;

		/// <summary>
		/// Gets fixture B in this contact.
		/// </summary>
		public Fixture FixtureB => this.fixtureB;

		/// <summary>
		/// Get the child primitive index for fixture B.
		/// </summary>
		public int ChildIndexB => this.indexB;

		#endregion

		#region Constructors

		static Contact()
		{
			const int maxShapeTypes = 5;
			registers = new ContactRegister[maxShapeTypes][];
			for (int i = 0; i < registers.Length; i++)
				registers[i] = new ContactRegister[maxShapeTypes];

#if CONTACT_POOL
			addType(typeof(CircleShape), typeof(CircleShape), CircleContact.Create, CircleContact.Destroy);
			addType(typeof(PolygonShape), typeof(CircleShape), PolygonAndCircleContact.Create, PolygonAndCircleContact.Destroy);
			addType(typeof(PolygonShape), typeof(PolygonShape), PolygonContact.Create, PolygonContact.Destroy);
			addType(typeof(EdgeShape), typeof(CircleShape), EdgeAndCircleContact.Create, EdgeAndCircleContact.Destroy);
			addType(typeof(EdgeShape), typeof(PolygonShape), EdgeAndPolygonContact.Create, EdgeAndPolygonContact.Destroy);
			addType(typeof(ChainShape), typeof(CircleShape), ChainAndCircleContact.Create, ChainAndCircleContact.Destroy);
			addType(typeof(ChainShape), typeof(PolygonShape), ChainAndPolygonContact.Create, ChainAndPolygonContact.Destroy);
#else
			addType(typeof(CircleShape), typeof(CircleShape), (fa, ia, fb, ib) => new CircleContact(fa, fb));
			addType(typeof(PolygonShape), typeof(CircleShape), (fa, ia, fb, ib) => new PolygonAndCircleContact(fa, fb));
			addType(typeof(PolygonShape), typeof(PolygonShape), (fa, ia, fb, ib) => new PolygonContact(fa, fb));
			addType(typeof(EdgeShape), typeof(CircleShape), (fa, ia, fb, ib) => new EdgeAndCircleContact(fa, fb));
			addType(typeof(EdgeShape), typeof(PolygonShape), (fa, ia, fb, ib) => new EdgeAndPolygonContact(fa, fb));
			addType(typeof(ChainShape), typeof(CircleShape), (fa, ia, fb, ib) => new ChainAndCircleContact(fa, ia, fb, ib));
			addType(typeof(ChainShape), typeof(PolygonShape), (fa, ia, fb, ib) => new ChainAndPolygonContact(fa, ia, fb, ib));
#endif
		}

#if CONTACT_POOL
		protected Contact()
		{
			this.manifold = new Manifold();
			this.oldManifold = new Manifold();
		}
#endif

		protected Contact(Fixture fixtureA, int indexA, Fixture fixtureB, int indexB)
		{
			this.manifold = new Manifold();
			this.oldManifold = new Manifold();


#if CONTACT_POOL
			this.Initialize(fixtureA, indexA, fixtureB, indexB);
		}

		protected void Initialize(Fixture fixtureA, int indexA, Fixture fixtureB, int indexB)
		{
			Debug.Assert((this.flags & ContactFlags.Initialized) == 0);
#endif

			//Trace.WriteVerbose($"+ Contact: {fA.UserData} {fA.Body.UserData} {indexA} <=> {fB.UserData} {fB.Body.UserData} {indexB}");

			this.flags = ContactFlags.Enabled | ContactFlags.Initialized;

			this.fixtureA = fixtureA;
			this.fixtureB = fixtureB;

			this.indexA = indexA;
			this.indexB = indexB;

			this.manifold.Points.CountInternal = 0;

			this.nodeA = default;
			this.nodeB = default;

			this.toiCount = 0;

			this.friction = MathHelper.MixFriction(this.fixtureA.friction, this.fixtureB.friction);
			this.restitution = MathHelper.MixRestitution(this.fixtureA.restitution, this.fixtureB.restitution);
			this.tangentSpeed = 0.0f;
		}

		#endregion

		#region Methods

		private static void addType(Type type1, Type type2, CreateFunc create, DestroyFunc destroy = null)
		{
			int id1 = Shape.GetTypeId(type1);
			int id2 = Shape.GetTypeId(type2);

			registers[id1][id2].CreateFunc = create;
#if CONTACT_POOL
			registers[id1][id2].DestroyFunc = destroy;
#endif
			registers[id1][id2].IsPrimary = true;

			if (id1 != id2)
			{
				registers[id2][id1].CreateFunc = create;
#if CONTACT_POOL
				registers[id2][id1].DestroyFunc = destroy;
#endif
				registers[id2][id1].IsPrimary = false;
			}
		}

		/// <summary>
		/// Returns the world manifold.
		/// </summary>
		/// <returns></returns>
		public WorldManifold GetWorldManifold()
		{
			//Debug.Assert(this.IsValid);

			Body bodyA = this.fixtureA.body;
			Body bodyB = this.fixtureB.body;
			Shape shapeA = this.fixtureA.shape;
			Shape shapeB = this.fixtureB.shape;

			return new WorldManifold(this.manifold, in bodyA.transform, shapeA.radius, in bodyB.transform, shapeB.radius);
		}

		/// <summary>
		/// Evaluate this contact with your own manifold and transforms.
		/// </summary>
		/// <param name="manifold"></param>
		/// <param name="xfA"></param>
		/// <param name="xfB"></param>
		protected abstract void Evaluate(Manifold manifold, in Transform xfA, in Transform xfB);

		/// <summary>
		/// Creates a contact for the specified fixtures.
		/// </summary>
		/// <param name="fixtureA"></param>
		/// <param name="indexA"></param>
		/// <param name="fixtureB"></param>
		/// <param name="indexB"></param>
		/// <returns></returns>
		public static Contact Create(Fixture fixtureA, int indexA, Fixture fixtureB, int indexB)
		{
			int id1 = fixtureA.shape.TypeID;
			int id2 = fixtureB.shape.TypeID;

			CreateFunc create = registers[id1][id2].CreateFunc;

			if (create != null)
			{
				if (registers[id1][id2].IsPrimary)
					return create(fixtureA, indexA, fixtureB, indexB);

				return create(fixtureB, indexB, fixtureA, indexA);
			}

			return null;
		}

		/// <summary>
		/// Destroys the contact.
		/// </summary>
		internal void Destroy()
		{
			Debug.Assert((this.flags & ContactFlags.Initialized) != 0);
			Debug.Assert((this.flags & (ContactFlags.Destroyed | ContactFlags.EventFixtureA | ContactFlags.EventFixtureB | ContactFlags.EventContactManager)) == 0);

			Fixture fA = this.fixtureA;
			Fixture fB = this.fixtureB;

			//Trace.WriteVerbose($"- Contact: {fA.UserData} {fA.Body.UserData} {this.indexA} <=> {fB.UserData} {fB.Body.UserData} {this.indexB}");

			if (this.manifold.Points.CountInternal > 0 && !fA.Sensor && !fB.Sensor)
			{
				fA.body.Awake = true;
				fB.body.Awake = true;
			}

			int id1 = fA.shape.TypeID;
			int id2 = fB.shape.TypeID;

#if CONTACT_POOL
			registers[id1][id2].DestroyFunc?.Invoke(this);
#endif

			this.flags &= ~(ContactFlags.Enabled | ContactFlags.Touching);
			this.flags |= ContactFlags.Destroyed;
			//this.fixtureA = null;
			//this.fixtureB = null;
			//this.nodeA = default;
			//this.nodeB = default;
		}

		// Update the contact manifold and touching status.
		// Note: do not assume the fixture AABBs are overlapping or are valid.
		internal void Update(ContactManager manager)
		{
			Debug.Assert(!this.IsDestroyed);

			this.oldManifold.CopyFrom(this.manifold);

			// Re-enable this contact.
			// why?
			// this.flags |= ContactFlags.EnabledFlag;

			bool touching;
			bool wasTouching = this.IsTouching;

			bool sensor = this.fixtureA.sensor || this.fixtureB.sensor;

			Body bodyA = this.fixtureA.body;
			Body bodyB = this.fixtureB.body;
			Transform xfA = bodyA.transform;
			Transform xfB = bodyB.transform;

			// Is this contact a sensor?
			if (sensor)
			{
				Shape shapeA = this.fixtureA.shape;
				Shape shapeB = this.fixtureB.shape;
				touching = Collisions.TestOverlap(shapeA, this.indexA, shapeB, this.indexB, in xfA, in xfB);

				// Sensors don't generate manifolds.
				this.manifold.Points.CountInternal = 0;
			}
			else
			{
				this.Evaluate(this.manifold, in xfA, in xfB);
				touching = this.manifold.Points.CountInternal > 0;

				// Match old contact ids to new contact ids and copy the
				// stored impulses to warm start the solver.
				for (int i = 0; i < this.manifold.Points.CountInternal; ++i)
				{
					ref ManifoldPoint mp2 = ref this.manifold.Points.Ref(i);
					mp2.NormalImpulse = 0.0f;
					mp2.TangentImpulse = 0.0f;
					ContactFeature id2 = mp2.Id;

					for (int j = 0; j < this.oldManifold.Points.CountInternal; ++j)
					{
						ref ManifoldPoint mp1 = ref this.oldManifold.Points.Ref(j);

						if (mp1.Id.Hash == id2.Hash)
						{
							mp2.NormalImpulse = mp1.NormalImpulse;
							mp2.TangentImpulse = mp1.TangentImpulse;
							break;
						}
					}
				}

				if (touching != wasTouching)
				{
					bodyA.Awake = true;
					bodyB.Awake = true;
				}
			}

			if (touching)
				this.flags |= ContactFlags.Touching;
			else
				this.flags &= ~ContactFlags.Touching;

			if (touching)
			{
				manager.InternalBeginContact(this);

				if (!sensor && !this.IsDestroyed)
				{
					manager.InternalPreSolve(this, this.oldManifold);
				}
			}
			else
			{
				manager.InternalEndContact(this);
			}
		}

		public void FlagForFiltering()
		{
			this.flags |= ContactFlags.Filter;
		}

		public void ResetFriction()
		{
			this.friction = MathHelper.MixFriction(this.fixtureA.friction, this.fixtureB.friction);
		}

		public void ResetRestitution()
		{
			this.restitution = MathHelper.MixRestitution(this.fixtureA.restitution, this.fixtureB.restitution);
		}

		#endregion
	}
}
