﻿using System;
using Iridescence.Math;
using Iridescence.Physics2D.Collision;

namespace Iridescence.Physics2D.Dynamics.Contacts
{
	internal struct PositionSolverManifold
	{
		#region Fields

		public Vector2 Normal;
		public Vector2 Point;
		public float Separation;

		#endregion

		#region Constructors

		public PositionSolverManifold(in ContactConstraint pc, ref Transform xfA, ref Transform xfB, int index)
		{
			switch (pc.Type)
			{
				case ManifoldType.Circles:
				{
					Vector2 pointA;
					pointA.X = (xfA.Q.C * pc.LocalPoint.X - xfA.Q.S * pc.LocalPoint.Y) + xfA.P.X;
					pointA.Y = (xfA.Q.S * pc.LocalPoint.X + xfA.Q.C * pc.LocalPoint.Y) + xfA.P.Y;

					var lc = pc.Points[0].LocalPoint;
					Vector2 pointB;
					pointB.X = (xfB.Q.C * lc.X - xfB.Q.S * lc.Y) + xfB.P.X;
					pointB.Y = (xfB.Q.S * lc.X + xfB.Q.C * lc.Y) + xfB.P.Y;

					Vector2 a = pointB - pointA;
					if (a.LengthSquared > 0.0f)
					{
						Vector2.Normalize(a, out this.Normal);
					}
					else
					{
						this.Normal = default;
					}

					this.Point = 0.5f * (pointA + pointB);
					this.Separation = a.X * this.Normal.X + a.Y * this.Normal.Y - pc.RadiusA - pc.RadiusB;
					break;
				}

				case ManifoldType.FaceA:
				{
					this.Normal.X = xfA.Q.C * pc.LocalNormal.X - xfA.Q.S * pc.LocalNormal.Y;
					this.Normal.Y = xfA.Q.S * pc.LocalNormal.X + xfA.Q.C * pc.LocalNormal.Y;

					Vector2 planePoint;
					planePoint.X = (xfA.Q.C * pc.LocalPoint.X - xfA.Q.S * pc.LocalPoint.Y) + xfA.P.X;
					planePoint.Y = (xfA.Q.S * pc.LocalPoint.X + xfA.Q.C * pc.LocalPoint.Y) + xfA.P.Y;

					var lc = pc.Points[index].LocalPoint;
					Vector2 clipPoint;
					clipPoint.X = (xfB.Q.C * lc.X - xfB.Q.S * lc.Y) + xfB.P.X;
					clipPoint.Y = (xfB.Q.S * lc.X + xfB.Q.C * lc.Y) + xfB.P.Y;

					Vector2 rCP = clipPoint - planePoint;
					this.Separation = rCP.X * this.Normal.X + rCP.Y * this.Normal.Y - pc.RadiusA - pc.RadiusB;
					this.Point = clipPoint;
					break;
				}

				case ManifoldType.FaceB:
				{
					this.Normal.X = xfB.Q.C * pc.LocalNormal.X - xfB.Q.S * pc.LocalNormal.Y;
					this.Normal.Y = xfB.Q.S * pc.LocalNormal.X + xfB.Q.C * pc.LocalNormal.Y;

					Vector2 planePoint;
					planePoint.X = (xfB.Q.C * pc.LocalPoint.X - xfB.Q.S * pc.LocalPoint.Y) + xfB.P.X;
					planePoint.Y = (xfB.Q.S * pc.LocalPoint.X + xfB.Q.C * pc.LocalPoint.Y) + xfB.P.Y;

					var lc = pc.Points[index].LocalPoint;
					Vector2 clipPoint;
					clipPoint.X = (xfA.Q.C * lc.X - xfA.Q.S * lc.Y) + xfA.P.X;
					clipPoint.Y = (xfA.Q.S * lc.X + xfA.Q.C * lc.Y) + xfA.P.Y;

					Vector2 rCP = clipPoint - planePoint;
					this.Separation = rCP.X * this.Normal.X + rCP.Y * this.Normal.Y - pc.RadiusA - pc.RadiusB;
					this.Point = clipPoint;

					// Ensure normal points from A to B
					this.Normal = -this.Normal;
					break;
				}

				default:
					throw new ArgumentException();
			}
		}

		#endregion
	}
}
