using System;
using System.Collections.Generic;
using Iridescence.Math;
using Iridescence.Physics2D.Collision;
using Iridescence.Physics2D.Collision.Shapes;
using Iridescence.Physics2D.Dynamics.Contacts;
using Iridescence.Physics2D.Dynamics.Joints;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// The world class manages all physics entities, dynamic simulation,
	/// and asynchronous queries. The world also contains efficient memory
	/// management facilities.
	/// </summary>
	public sealed class World
	{
		#region Fields

		internal WorldFlags flags;
		internal readonly ContactManager contactManager;
		private ISolverArbiter solverArbiter;

		private readonly WorldBodyCollection bodies;
		private readonly WorldJointCollection joints;
		private Body[] bodiesCopy;

		// This is used to compute the time step ratio to
		// support a variable time step.
		private float invDeltaT0;

		private bool allowSleep;

		// These are for debugging the solver.
		private bool warmStarting;
		private bool continuousPhysics;
		private bool subStepping;

		private bool stepComplete;

		private static readonly Pool<Island> islandPool = new Pool<Island>(() => new Island());

		#endregion

		#region Properties

		/// <summary>
		/// Gets the world's contact manager.
		/// </summary>
		public ContactManager ContactManager => this.contactManager;

		/// <summary>
		/// Gets the bodies in this world.
		/// </summary>
		public WorldBodyCollection Bodies => this.bodies;

		/// <summary>
		/// Gets the joints in this world.
		/// </summary>
		public WorldJointCollection Joints => this.joints;

		/// <summary>
		/// Gets or sets a value that determines whether sleeping is allowed globally.
		/// </summary>
		public bool AllowSleep
		{
			get => this.allowSleep;
			set
			{
				if (value == this.allowSleep)
					return;

				this.allowSleep = value;
				if (this.allowSleep == false)
				{
					foreach (Body body in this.bodies)
						body.Awake = true;
				}
			}
		}

		public bool WarmStarting
		{
			get => this.warmStarting;
			set => this.warmStarting = value;
		}

		public bool ContinuousPhysics
		{
			get => this.continuousPhysics;
			set => this.continuousPhysics = value;
		}

		public bool SubStepping
		{
			get => this.subStepping;
			set => this.subStepping = value;
		}

		/// <summary>
		/// Gets or sets an object that decides which objects to solve.
		/// </summary>
		public ISolverArbiter SolverArbiter
		{
			get => this.solverArbiter;
			set => this.solverArbiter = value;
		}

		#endregion

		#region Constructors

		public World()
		{
			this.bodies = new WorldBodyCollection(this);
			this.joints = new WorldJointCollection(this);

			this.warmStarting = true;
			this.continuousPhysics = true;
			this.subStepping = false;

			this.stepComplete = true;

			this.allowSleep = true;
			//this.gravity = gravity;

			this.flags = WorldFlags.ClearForces;

			this.invDeltaT0 = 0.0f;

			// setup up our default Contact Manager 
			this.contactManager = new ContactManager();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Find islands, integrate and solve constraints, solve position constraints.
		/// </summary>
		/// <param name="step"></param>
		internal void Solve(in TimeStep step)
		{
			Island island = islandPool.Take();

			// Resize body copy container.
			if (this.bodiesCopy == null || this.bodiesCopy.Length < this.bodies.Count)
			{
				int n = 1;
				while (n < this.bodies.Count)
					n <<= 1;

				this.bodiesCopy = new Body[n];
			}

			// Copy bodies, clear island flags.
			Span<Body> bodies = new Span<Body>(this.bodiesCopy, 0, this.bodies.Count);
			for (int i = 0; i < bodies.Length; ++i)
			{
				bodies[i] = this.bodies[i];
				bodies[i].flags &= ~BodyFlags.Island;
			}

			// Size the island for the worst case.
			island.Initialize(bodies.Length, this.contactManager.Contacts.Count, this.joints.Count, this.contactManager, this.solverArbiter);

			// Clear contact island flags.
			foreach (Contact c in this.contactManager.Contacts)
				c.flags &= ~ContactFlags.Island;

			foreach (Joint joint in this.joints)
				joint.islandFlag = false;

			// Build and simulate all awake islands.
			Stack<Body> stack = new Stack<Body>(bodies.Length);
			foreach (Body seed in bodies)
			{
				if (this.solverArbiter != null && !this.solverArbiter.Solve(seed))
					continue;

				if ((seed.flags & BodyFlags.Island) != 0)
					continue;

				if (!seed.Awake || !seed.Active)
					continue;

				// The seed can be dynamic or kinematic.
				if (seed.type == BodyType.Static)
					continue;

				// Reset island and stack.
				island.Clear(false);
				stack.Push(seed);
				seed.flags |= BodyFlags.Island;

				// Perform a depth first search (DFS) on the constraint graph.
				while (stack.Count > 0)
				{
					// Grab the next body off the stack and add it to the island.
					Body b = stack.Pop();
					island.Add(b);

					// Make sure the body is awake (without resetting sleep timer).
					b.flags |= BodyFlags.Awake;

					// To keep islands as small as possible, we don't
					// propagate islands across static bodies.
					if (b.type == BodyType.Static)
						continue;

					// Search all contacts connected to this body.
					foreach (var bodyContacts in b.contacts.ToImmutable(island.contactCache))
					{
						Body other = bodyContacts.Body;
						foreach (Contact contact in bodyContacts.Contacts)
						{
							// Has this contact already been added to an island?
							if ((contact.flags & ContactFlags.Island) != 0)
								continue;

							// Is this contact solid and touching?
							if (!contact.Enabled || !contact.IsTouching)
								continue;

							// Skip sensors.
							if (contact.fixtureA.Sensor || contact.fixtureB.Sensor)
								continue;

							island.Add(contact);
							contact.flags |= ContactFlags.Island;

							// Was the other body already added to this island?
							if ((other.flags & BodyFlags.Island) > 0)
								continue;

							stack.Push(other);
							other.flags |= BodyFlags.Island;
						}
					}

					// Search all joints connect to this body.
					for (int j = 0; j < b.jointEdges.Count; j++)
					{
						JointEdge je = b.jointEdges[j];
						if (je.Joint.islandFlag)
							continue;

						Body other = je.Other;

						// Don't simulate joints connected to inactive bodies.
						if (!other.Active)
							continue;

						island.Add(je.Joint);
						je.Joint.islandFlag = true;

						if ((other.flags & BodyFlags.Island) > 0)
							continue;

						stack.Push(other);
						other.flags = other.flags | BodyFlags.Island;
					}
				}

				island.Solve(step, this.allowSleep);

				// Post solve cleanup.
				for (int i = 0; i < island.bodyCount; ++i)
				{
					// Allow static bodies to participate in other islands.
					Body b = island.bodies[i];
					if (b.type == BodyType.Static)
						b.flags &= ~BodyFlags.Island;
				}
			}

			island.Clear(true);
			islandPool.Give(island);

			// Synchronize fixtures, check for out of range bodies.
			foreach (Body b in bodies)
			{
				// If a body was not in an island then it did not move.
				if ((b.flags & BodyFlags.Island) == 0)
					continue;

				if (b.type == BodyType.Static)
					continue;

				// Update fixtures (for broad-phase).
				b.SynchronizeFixtures();
			}

			// Look for new contacts.
			this.contactManager.FindNewContacts();

			bodies.Clear();
		}

		/// <summary>
		/// Find TOI contacts and solve them.
		/// </summary>
		/// <param name="step"></param>
		internal void SolveTOI(in TimeStep step)
		{
			Island island = islandPool.Take();
			island.Initialize(2 * Settings.MaxTOIContacts, Settings.MaxTOIContacts, 0, this.contactManager, this.solverArbiter);

			if (this.stepComplete)
			{
				foreach (Body b in this.bodies)
				{
					b.flags = b.flags & ~BodyFlags.Island;
					b.sweep.Alpha0 = 0.0f;
				}

				foreach (Contact c in this.contactManager.Contacts)
				{
					// Invalidate TOI
					c.flags &= ~(ContactFlags.Toi | ContactFlags.Island);
					c.toiCount = 0;
					c.toi = 1.0f;
				}
			}

			// Find TOI events and solve them.
			for (;;)
			{
				// Find the first TOI.
				Contact minContact = null;
				float minAlpha = 1.0f;

				foreach (Contact c in this.contactManager.Contacts)
				{
					// Is this contact disabled?
					if ((c.flags & ContactFlags.Enabled) == 0)
						continue;

					// Prevent excessive sub-stepping.
					if (c.toiCount > Settings.MaxSubSteps)
						continue;

					float alpha;
					if ((c.flags & ContactFlags.Toi) != 0)
					{
						// This contact has a valid cached TOI.
						alpha = c.toi;
					}
					else
					{
						Fixture fA = c.fixtureA;
						Fixture fB = c.fixtureB;

						// Is there a sensor?
						if (fA.sensor || fB.sensor)
							continue;

						Body bA = fA.body;
						Body bB = fB.body;

						BodyType typeA = bA.type;
						BodyType typeB = bB.type;

						bool activeA = bA.Awake && typeA != BodyType.Static;
						bool activeB = bB.Awake && typeB != BodyType.Static;

						// Is at least one body active (awake and dynamic or kinematic)?
						if (activeA == false && activeB == false)
							continue;

						bool collideA = bA.IsBullet || typeA != BodyType.Dynamic;
						bool collideB = bB.IsBullet || typeB != BodyType.Dynamic;

						// Are these two non-bullet dynamic bodies?
						if (collideA == false && collideB == false)
							continue;

						// Compute the TOI for this contact.
						// Put the sweeps onto the same time interval.
						float alpha0 = bA.sweep.Alpha0;

						if (bA.sweep.Alpha0 < bB.sweep.Alpha0)
						{
							alpha0 = bB.sweep.Alpha0;
							bA.sweep.Advance(alpha0);
						}
						else if (bB.sweep.Alpha0 < bA.sweep.Alpha0)
						{
							alpha0 = bA.sweep.Alpha0;
							bB.sweep.Advance(alpha0);
						}

						int indexA = c.indexA;
						int indexB = c.indexB;

						// Compute the time of impact in interval [0, minTOI]
						TimeOfImpactInput input;
						fA.shape.GetDistanceProxy(indexA, out input.ProxyA);
						fB.shape.GetDistanceProxy(indexB, out input.ProxyB);
						input.SweepA = bA.sweep;
						input.SweepB = bB.sweep;
						input.TMax = 1.0f;

						TimeOfImpact.Compute(input, out TimeOfImpactOutput output);

						// Beta is the fraction of the remaining portion of the .
						float beta = output.T;
						if (output.State == ImpactState.Touching)
							alpha = System.Math.Min(alpha0 + (1.0f - alpha0) * beta, 1.0f);
						else
							alpha = 1.0f;

						c.toi = alpha;
						c.flags |= ContactFlags.Toi;
					}

					if (alpha < minAlpha)
					{
						// This is the minimum TOI found so far.
						minContact = c;
						minAlpha = alpha;
					}
				}

				if (minContact == null || Settings.AlphaEpsilon < minAlpha)
				{
					// No more TOI events. Done!
					this.stepComplete = true;
					break;
				}

				{
					// Advance the bodies to the TOI.
					Fixture fA = minContact.fixtureA;
					Fixture fB = minContact.fixtureB;
					Body bA = fA.body;
					Body bB = fB.body;

					Sweep backup1 = bA.sweep;
					Sweep backup2 = bB.sweep;

					bA.Advance(minAlpha);
					bB.Advance(minAlpha);

					// The TOI contact likely has some new contact points.
					minContact.Update(this.contactManager);
					minContact.flags &= ~ContactFlags.Toi;
					++minContact.toiCount;

					// Is the contact solid?
					if ((minContact.flags & ContactFlags.Enabled) == 0 || (minContact.flags & ContactFlags.Touching) == 0)
					{
						// Restore the sweeps.
						minContact.Enabled = false;
						bA.sweep = backup1;
						bB.sweep = backup2;
						bA.SynchronizeTransform();
						bB.SynchronizeTransform();
						continue;
					}

					bA.Awake = true;
					bB.Awake = true;

					// Build the island
					island.Clear(false);
					island.Add(bA);
					island.Add(bB);
					island.Add(minContact);

					bA.flags |= BodyFlags.Island;
					bB.flags |= BodyFlags.Island;
					minContact.flags |= ContactFlags.Island;

					// Get contacts on bodyA and bodyB.
					for (int i = 0; i < 2; ++i)
					{
						Body body = i == 0 ? bA : bB;
						if (body.type == BodyType.Dynamic)
						{
							foreach (var bodyContacts in body.contacts.ToImmutable(island.contactCache))
							{
								Body other = bodyContacts.Body;
								foreach (Contact contact in bodyContacts.Contacts)
								{
									if (island.bodyCount == island.bodies.Length || island.contactCount == island.contacts.Length)
										break;

									if (contact.IsDestroyed)
										continue;

									// Has this contact already been added to the island?
									if ((contact.flags & ContactFlags.Island) != 0)
										continue;

									// Only add static, kinematic, or bullet bodies.
									if (other.type == BodyType.Dynamic && !body.IsBullet && !body.IsBullet)
										continue;

									// Skip sensors.
									bool sensorA = contact.fixtureA.sensor;
									bool sensorB = contact.fixtureB.sensor;
									if (sensorA || sensorB)
										continue;

									// Tentatively advance the body to the TOI.
									Sweep backup = other.sweep;
									if ((other.flags & BodyFlags.Island) != 0)
										other.Advance(minAlpha);

									// Update the contact points
									contact.Update(this.contactManager);

									// Was the contact disabled by the user?
									if ((contact.flags & ContactFlags.Enabled) == 0)
									{
										other.sweep = backup;
										other.SynchronizeTransform();
										continue;
									}

									// Are there contact points?
									if ((contact.flags & ContactFlags.Touching) == 0)
									{
										other.sweep = backup;
										other.SynchronizeTransform();
										continue;
									}

									// Add the contact to the island
									contact.flags |= ContactFlags.Island;
									island.Add(contact);

									// Has the other body already been added to the island?
									if ((other.flags & BodyFlags.Island) != 0)
										continue;

									// Add the other body to the island.
									other.flags |= BodyFlags.Island;

									if (other.type != BodyType.Static)
										other.Awake = true;

									island.Add(other);
								}
							}
						}
					}

					TimeStep subStep = new TimeStep(
						deltaT: (1.0f - minAlpha) * step.DeltaT,
						deltaTRatio: 1.0f,
						positionIterations: 20,
						velocityIterations: step.VelocityIterations,
						warmStarting: true);

					island.SolveTOI(subStep, bA.islandIndex, bB.islandIndex);

					// Reset island flags and synchronize broad-phase proxies.
					for (int i = 0, count = island.bodyCount; i < count; ++i)
					{
						Body body = island.bodies[i];
						body.flags &= ~BodyFlags.Island;

						if (body.type != BodyType.Dynamic)
							continue;

						body.SynchronizeFixtures();

						// Invalidate all contact TOIs on this displaced body.
						foreach (Contact contact in body.contacts.GetContacts())
						{
							contact.flags &= ~(ContactFlags.Toi | ContactFlags.Island);
						}
					}

					// Commit fixture proxy movements to the broad-phase so that new contacts are created.
					// Also, some contacts can be destroyed.
					this.contactManager.FindNewContacts();

					if (this.subStepping)
					{
						this.stepComplete = false;
						break;
					}
				}
			}

			island.Clear(true);
			islandPool.Give(island);
		}

		/// <summary>
		/// Performs one step.
		/// </summary>
		/// <param name="dt">The time interval.</param>
		/// <param name="velocityIterations">The number of velocity iterations. Higher values lead to better results, but take more time.</param>
		/// <param name="positionIterations">The number of position iterations. Higher values lead to better results, but take more time.</param>
		public void Step(float dt, int velocityIterations, int positionIterations)
		{
			// If new fixtures were added, we need to find the new contacts.
			if ((this.flags & WorldFlags.NewFixture) != 0)
			{
				this.contactManager.FindNewContacts();
				this.flags &= ~WorldFlags.NewFixture;
			}

			TimeStep step = new TimeStep(
				deltaT: dt, 
				deltaTRatio: this.invDeltaT0 * dt,
				velocityIterations: velocityIterations,
				positionIterations: positionIterations,
				warmStarting: this.warmStarting);

			// Update contacts. This is where some contacts are destroyed.
			this.contactManager.Collide();

			//this.flags |= WorldFlags.Locked;

			// Integrate velocities, solve velocity constraints, and integrate positions.
			if (this.stepComplete && step.DeltaT > 0.0f)
				this.Solve(step);

			// Handle TOI events.
			if (this.continuousPhysics && step.DeltaT > 0.0f)
				this.SolveTOI(step);

			if (step.DeltaT > 0.0f)
				this.invDeltaT0 = step.InvDeltaT;

			if ((this.flags & WorldFlags.ClearForces) != 0)
				this.ClearForces();

			//this.flags &= ~WorldFlags.Locked;
		}

		/// <summary>
		/// Clears forces on all bodies.
		/// </summary>
		public void ClearForces()
		{
			foreach (Body body in this.bodies)
			{
				body.force = Vector2.Zero;
				body.torque = 0.0f;
			}
		}

		/// <summary>
		/// Calls the specified callback for all fixtures that intersect with the specified bounding box.
		/// </summary>
		/// <param name="aabb">The bounding box.</param>
		/// <param name="callback">The callback for fixtures. Can be used to cancel the query.</param>
		public void QueryAABB(in AABB aabb, QueryCallback callback)
		{
			this.contactManager.broadPhase.Query(proxyId =>
			{
				FixtureProxy proxy = (FixtureProxy)this.contactManager.broadPhase.GetUserData(proxyId);
				Fixture fixture = proxy.Fixture;
				return callback(fixture);
			}, aabb);
		}

		/// <summary>
		/// Calls the specified callback for all fixtures that intersect with the specified shape.
		/// </summary>
		/// <param name="shape"></param>
		/// <param name="transform"></param>
		/// <param name="callback"></param>
		public void QueryShape(Shape shape, in Transform transform, QueryCallback callback)
		{
			Transform xf = transform;
			for (int i = 0; i < shape.ChildCount; ++i)
			{
				shape.ComputeBoundingBox(xf, i, out AABB aabb);

				this.QueryAABB(aabb, fixture =>
				{
					Shape shape2 = fixture.shape;

					for (int j = 0; j < shape2.ChildCount; ++j)
					{
						if (Collisions.TestOverlap(shape, i, shape2, j, xf, fixture.body.transform))
						{
							if (!callback(fixture))
								return false;

							break;
						}
					}

					return true;
				});
			}
		}

		/// <summary>
		/// Casts a ray. The callback is called for all fixtures that intersect with the ray.
		/// </summary>
		/// <param name="point1">The starting point of the ray.</param>
		/// <param name="point2">The end point of the ray.</param>
		/// <param name="callback">The callback for fixtures. Can be used to cancel or modify the query.</param>
		public void RayCast(Vector2 point1, Vector2 point2, RayCastCallback callback)
		{
			RayCastInput input;
			input.MaxFraction = 1.0f;
			input.Point1 = point1;
			input.Point2 = point2;

			this.contactManager.broadPhase.RayCast(input, (in RayCastInput callbackInput, int proxyId) =>
			{
				FixtureProxy proxy = (FixtureProxy)this.contactManager.broadPhase.GetUserData(proxyId);
				Fixture fixture = proxy.Fixture;
				int index = proxy.ChildIndex;
				bool hit = fixture.RayCast(callbackInput, index, out RayCastOutput output);

				if (hit)
				{
					float fraction = output.Fraction;
					float fraction2 = 1.0f - fraction;

					//Vector2 point = (1.0f - fraction) * callbackInput.Point1 + fraction * callbackInput.Point2;

					Vector2 point;
					point.X = fraction2 * callbackInput.Point1.X + fraction * callbackInput.Point2.X;
					point.Y = fraction2 * callbackInput.Point1.Y + fraction * callbackInput.Point2.Y;

					return callback(fixture, point, output.Normal, fraction);
				}

				return callbackInput.MaxFraction;
			});
		}

		/// <summary>
		/// Moves the world origin.
		/// Can be used to improve precision.
		/// </summary>
		/// <param name="newOrigin">The position of the new origin (i.e. the point that will become 0,0).</param>
		public void ShiftOrigin(Vector2 newOrigin)
		{
			foreach (Body body in this.bodies)
			{
				body.transform.P -= newOrigin;
				body.sweep.Center0 -= newOrigin;
				body.sweep.Center -= newOrigin;
			}

			foreach (Joint joint in this.joints)
				joint.ShiftOrigin(newOrigin);

			this.contactManager.broadPhase.ShiftOrigin(newOrigin);
		}

		#endregion
	}
}
