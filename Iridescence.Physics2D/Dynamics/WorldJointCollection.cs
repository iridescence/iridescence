﻿using System;
using System.Collections;
using System.Collections.Generic;
using Iridescence.Physics2D.Dynamics.Contacts;
using Iridescence.Physics2D.Dynamics.Joints;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Contains the joints of a <see cref="World"/>.
	/// </summary>
	public sealed class WorldJointCollection : IReadOnlyList<Joint>, INotifyListChanged<Joint>
	{
		#region Events

		public event CollectionChangedEventHandler<ListItem<Joint>> CollectionChanged;

		#endregion

		#region Fields

		private readonly List<Joint> joints;
		
		#endregion
		
		#region Properties

		public World World { get; }
		
		public int Count => this.joints.Count;

		public Joint this[int index] => this.joints[index];

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="WorldJointCollection"/> class. 
		/// </summary>
		internal WorldJointCollection(World world)
		{
			this.World = world;
			this.joints = new List<Joint>();
		}
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Creates a new joint based on the specified joint descriptor and adds it to the world.
		/// </summary>
		/// <param name="def"></param>
		/// <returns></returns>
		public Joint Add(JointDescriptor def)
		{
			if (!ReferenceEquals(def.BodyA.World, this.World))
				throw new ArgumentException("BodyA is not part of the world.");

			if (!ReferenceEquals(def.BodyB.World, this.World))
				throw new ArgumentException("BodyB is not part of the world.");

			Joint joint = def.Create();

			// Connect to the world list.
			this.joints.Add(joint);

			// Connect to the bodies' doubly linked lists.
			joint.edgeA.Joint = joint;
			joint.edgeA.Other = joint.bodyB;
			joint.bodyA.jointEdges.Add(joint.edgeA);

			joint.edgeB.Joint = joint;
			joint.edgeB.Other = joint.bodyA;
			joint.bodyB.jointEdges.Add(joint.edgeB);
			
			Body bodyA = def.BodyA;
			Body bodyB = def.BodyB;

			// If the joint prevents collisions, then flag any contacts for filtering.
			if (!def.CollideConnected && bodyB.contacts.TryGetContacts(bodyA, out IEnumerable<Contact> contacts))
			{
				foreach (Contact c in contacts)
				{
					// Flag the contact for filtering at the next time step (where either body is awake).
					c.FlagForFiltering();
				}
			}


			this.CollectionChanged?.Invoke(this, ListChange.Add(this.joints.Count - 1, joint));

			// Note: creating a joint doesn't wake the bodies.
			return joint;
		}

		/// <summary>
		/// Removes the specified joint.
		/// </summary>
		/// <param name="joint"></param>
		public bool Remove(Joint joint)
		{
			int index = this.joints.IndexOf(joint);
			if (index < 0)
				return false;

			this.joints.RemoveAt(index);

			bool collideConnected = joint.collideConnected;
			
			// Disconnect from island graph.
			Body bodyA = joint.bodyA;
			Body bodyB = joint.bodyB;

			// Wake up connected bodies.
			bodyA.Awake = true;
			bodyB.Awake = true;

			// Remove from bodies.
			bodyA.jointEdges.Remove(joint.edgeA);
			bodyB.jointEdges.Remove(joint.edgeB);

			// If the joint prevents collisions, then flag any contacts for filtering.
			if (collideConnected == false && bodyB.contacts.TryGetContacts(bodyA, out IEnumerable<Contact> contacts))
			{
				foreach (Contact contact in contacts)
				{
					// Flag the contact for filtering at the next time step (where either body is awake).
					contact.FlagForFiltering();
				}
			}

			this.CollectionChanged?.Invoke(this, ListChange.Remove(index, joint));

			return true;
		}

		public IEnumerator<Joint> GetEnumerator()
		{
			return this.joints.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)this.joints).GetEnumerator();
		}

		#endregion
	}
}
