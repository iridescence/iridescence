using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// Wheel joint definition. This requires defining a line of
	/// motion using an axis and an anchor point. The definition uses local
	/// anchor points and a local axis so that the initial configuration
	/// can violate the constraint slightly. The joint translation is zero
	/// when the local anchor points coincide in world space. Using local
	/// anchors and a local axis helps when saving and loading a game.
	/// </summary>
	public sealed class WheelJointDescriptor : JointDescriptor
	{
		#region Properties

		/// <summary>
		/// The local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA { get; set; }

		/// <summary>
		/// The local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB { get; set; }

		/// <summary>
		/// The local translation axis in bodyA.
		/// </summary>
		public Vector2 LocalAxisA { get; set; }

		/// <summary>
		/// Enable/disable the joint motor.
		/// </summary>
		public bool MotorEnabled { get; set; }

		/// <summary>
		/// The maximum motor torque, usually in N-m.
		/// </summary>
		public float MaxMotorTorque { get; set; }

		/// <summary>
		/// The desired motor speed in radians per second.
		/// </summary>
		public float MotorSpeed { get; set; }

		/// <summary>
		/// Suspension frequency, zero indicates no suspension
		/// </summary>
		public float Frequency { get; set; }

		/// <summary>
		/// Suspension damping ratio, one indicates critical damping
		/// </summary>
		public float DampingRatio { get; set; }

		#endregion

		#region Constructors

		public WheelJointDescriptor()
		{
			this.LocalAnchorA = Vector2.Zero;
			this.LocalAnchorB = Vector2.Zero;
			this.LocalAxisA = Vector2.UnitX;
			this.MotorEnabled = false;
			this.MaxMotorTorque = 0.0f;
			this.MotorSpeed = 0.0f;
			this.Frequency = 2.0f;
			this.DampingRatio = 0.7f;
		}

		/// <summary>
		/// Initialize the bodies, anchors, axis, and reference angle using the world
		/// anchor and world axis.
		/// </summary>
		/// <param name="bA"></param>
		/// <param name="bB"></param>
		/// <param name="anchor"></param>
		/// <param name="axis"></param>
		public WheelJointDescriptor(Body bA, Body bB, Vector2 anchor, Vector2 axis)
			: this()
		{
			this.BodyA = bA;
			this.BodyB = bB;
			this.LocalAnchorA = this.BodyA.GetLocalPoint(anchor);
			this.LocalAnchorB = this.BodyB.GetLocalPoint(anchor);
			this.LocalAxisA = this.BodyA.GetLocalVector(axis);
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new WheelJoint(this);
		}

		#endregion
	}
}
