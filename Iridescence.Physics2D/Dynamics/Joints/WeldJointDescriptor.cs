using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// Weld joint definition. You need to specify local anchor points
	/// where they are attached and the relative body angle. The position
	/// of the anchor points is important for computing the reaction torque.
	/// </summary>
	public sealed class WeldJointDescriptor : JointDescriptor
	{
		#region Properties

		/// <summary>
		/// The local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA { get; set; }

		/// <summary>
		/// The local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB { get; set; }

		/// <summary>
		/// The bodyB angle minus bodyA angle in the reference state (radians).
		/// </summary>
		public float ReferenceAngle { get; set; }

		/// <summary>
		/// The mass-spring-damper frequency in Hertz. Rotation only.
		/// Disable softness with a value of 0.
		/// </summary>
		public float Frequency { get; set; }

		/// <summary>
		/// The damping ratio. 0 = no damping, 1 = critical damping.
		/// </summary>
		public float DampingRatio { get; set; }

		#endregion

		#region Constructors

		public WeldJointDescriptor()
		{
			this.LocalAnchorA = Vector2.Zero;
			this.LocalAnchorB = Vector2.Zero;
			this.ReferenceAngle = 0.0f;
			this.Frequency = 0.0f;
			this.DampingRatio = 0.0f;
		}

		/// Initialize the bodies, anchors, and reference angle using a world
		/// anchor point.
		public WeldJointDescriptor(Body bA, Body bB, Vector2 anchor)
			: this()
		{
			this.BodyA = bA;
			this.BodyB = bB;
			this.LocalAnchorA = this.BodyA.GetLocalPoint(anchor);
			this.LocalAnchorB = this.BodyB.GetLocalPoint(anchor);
			this.ReferenceAngle = this.BodyB.Angle - this.BodyA.Angle;
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new WeldJoint(this);
		}

		#endregion
	}
}
