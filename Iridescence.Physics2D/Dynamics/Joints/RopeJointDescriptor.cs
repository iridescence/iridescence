using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// Rope joint definition. This requires two body anchor points and  a maximum lengths.
	/// Note: by default the connected objects will not collide. See CollideConnected in JointDescriptor.
	/// </summary>
	public sealed class RopeJointDescriptor : JointDescriptor
	{
		#region Properties

		/// <summary>
		/// Gets or sets the local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA { get; set; }

		/// <summary>
		/// Gets or sets the local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB { get; set; }

		/// <summary>
		/// Gets or sets the maximum length of the rope.
		/// Warning: this must be larger than b2_linearSlop or the joint will have no effect.
		/// </summary>
		public float Length { get; set; }

		#endregion

		#region Constructors

		public RopeJointDescriptor(Body bodyA, Body bodyB, float length, Vector2 localAnchorA, Vector2 localAnchorB)
		{
			this.BodyA = bodyA;
			this.BodyB = bodyB;
			this.Length = length;
			this.LocalAnchorA = localAnchorA;
			this.LocalAnchorB = localAnchorB;
		}

		public RopeJointDescriptor(Body bodyA, Body bodyB, float maxLength)
			: this(bodyA, bodyB, maxLength, Vector2.Zero, Vector2.Zero)
		{
		}

		public RopeJointDescriptor(Body bodyA, Body bodyB)
			: this(bodyA, bodyB, 1.0f)
		{
		}

		public RopeJointDescriptor()
			: this(null, null)
		{
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new RopeJoint(this);
		}

		#endregion
	}
}
