/*
* Copyright (c) 2006-2007 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

// p = attached point, m = mouse point
// C = p - m
// Cdot = v
//      = v + cross(w, r)
// J = [I r_skew]
// Identity used:
// w k % (rx i + ry j) = w * (-ry i + rx j)

using System;
using System.Diagnostics;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// A mouse joint is used to make a point on a body track a
	/// specified world point. This a soft constraint with a maximum
	/// force. This allows the constraint to stretch and without
	/// applying huge forces.
	/// </summary>
	public sealed class MouseJoint : Joint
	{
		#region Fields

		private readonly Vector2 localAnchorB;
		private Vector2 target;
		private float frequency;
		private float dampingRatio;
		private float beta;

		// Solver shared
		private Vector2 impulse;
		private float maxForce;
		private float gamma;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 rB;
		private Vector2 localCenterB;
		private float invMassB;
		private float invIB;
		private Matrix2x2 mass;
		private Vector2 c;
		
		#endregion

		#region Properties

		public override Vector2 AnchorA => this.target;

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		public Vector2 Target
		{
			get => this.target;
			set
			{
				if (this.target != value)
				{
					this.bodyB.Awake = true;
					this.target = value;
				}
			}
		}

		public float MaxForce
		{
			get => this.maxForce;
			set => this.maxForce = value;
		}

		public float DampingRatio
		{
			get => this.dampingRatio;
			set => this.dampingRatio = value;
		}

		public float Frequency
		{
			get => this.frequency;
			set => this.frequency = value;
		}

		#endregion

		#region Constructors

		public MouseJoint(MouseJointDescriptor def)
			: base(def)
		{
			if (def.MaxForce < 0.0f)
				throw new ArgumentException("Invalid MaxForce.");

			if (def.Frequency < 0.0f)
				throw new ArgumentException("Invalid Frequency.");

			if (def.DampingRatio < 0.0f)
				throw new ArgumentException("Invalid DampingRatio.");

			this.target = def.Target;
			Transform.MultiplyTransposed(in this.bodyB.transform, in this.target, out this.localAnchorB);

			this.maxForce = def.MaxForce;
			this.impulse = Vector2.Zero;

			this.frequency = def.Frequency;
			this.dampingRatio = def.DampingRatio;

			this.beta = 0.0f;
			this.gamma = 0.0f;
		}

		#endregion

		#region Methods
		
		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassB = this.bodyB.invMass;
			this.invIB = this.bodyB.invInertia;

			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qB = new Rotation(aB);

			float mass = this.bodyB.mass;

			// Frequency
			float omega = 2.0f * Constants.Pi * this.frequency;

			// Damping coefficient
			float d = 2.0f * mass * this.dampingRatio * omega;

			// Spring stiffness
			float k = mass * (omega * omega);

			// magic formulas
			// gamma has units of inverse mass.
			// beta has units of inverse time.
			float h = data.timeStep.DeltaT;
			Debug.Assert(d + h * k > float.Epsilon);
			this.gamma = h * (d + h * k);
			if (this.gamma != 0.0f)
			{
				this.gamma = 1.0f / this.gamma;
			}
			this.beta = h * k * this.gamma;

			// Compute the effective mass matrix.
			this.rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);

			// K    = [(1/m1 + 1/m2) * eye(2) - skew(r1) * invI1 * skew(r1) - skew(r2) * invI2 * skew(r2)]
			//      = [1/m1+1/m2     0    ] + invI1 * [r1.Y*r1.Y -r1.X*r1.Y] + invI2 * [r1.Y*r1.Y -r1.X*r1.Y]
			//        [    0     1/m1+1/m2]           [-r1.X*r1.Y r1.X*r1.X]           [-r1.X*r1.Y r1.X*r1.X]
			Matrix2x2 K;
			K.M11 = this.invMassB + this.invIB * this.rB.Y * this.rB.Y + this.gamma;
			K.M12 = -this.invIB * this.rB.X * this.rB.Y;
			K.M21 = K.M12;
			K.M22 = this.invMassB + this.invIB * this.rB.X * this.rB.X + this.gamma;

			Matrix2x2.Invert(in K, out this.mass);

			this.c = cB + this.rB - this.target;
			this.c *= this.beta;

			// Cheat with some damping
			wB *= 0.98f;

			if (data.timeStep.WarmStarting)
			{
				this.impulse *= data.timeStep.DeltaTRatio;
				vB += this.invMassB * this.impulse;
				wB += this.invIB * Vector2.Cross(in this.rB, in this.impulse);
			}
			else
			{
				this.impulse = Vector2.Zero;
			}

			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			// Cdot = v + cross(w, r)
			Vector2 Cdot = vB + Vector2.Cross(wB, in this.rB);
			Matrix2x2.Multiply(this.mass, -(Cdot + this.c + this.gamma * this.impulse), out Vector2 impulse);

			Vector2 oldImpulse = this.impulse;
			this.impulse += impulse;
			float maxImpulse = data.timeStep.DeltaT * this.maxForce;
			if (this.impulse.LengthSquared > maxImpulse * maxImpulse)
			{
				this.impulse *= maxImpulse / this.impulse.Length;
			}
			impulse = this.impulse - oldImpulse;

			vB += this.invMassB * impulse;
			wB += this.invIB * Vector2.Cross(in this.rB, in impulse);

			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			return true;
		}

		public override Vector2 GetReactionForce(float inv_dt)
		{
			return inv_dt * this.impulse;
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return inv_dt * 0.0f;
		}

		public override void ShiftOrigin(Vector2 newOrigin)
		{
			this.target -= newOrigin;
		}

		#endregion
	}
}