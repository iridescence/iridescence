namespace Iridescence.Physics2D.Dynamics.Joints
{
	public sealed class GearJointDescriptor : JointDescriptor
	{
		#region Properties

		/// <summary>
		/// The first revolute/prismatic joint attached to the gear joint.
		/// </summary>
		public Joint JointA { get; set; }

		/// <summary>
		/// The second revolute/prismatic joint attached to the gear joint.
		/// </summary>
		public Joint JointB { get; set; }

		/// <summary>
		/// The gear ratio.
		/// See <see cref="GearJoint.Ratio">GearJoint.Ratio</see> for explanation.
		/// </summary>
		public float Ratio { get; set; }

		#endregion

		#region Constructors

		public GearJointDescriptor()
		{
			this.JointA = null;
			this.JointB = null;
			this.Ratio = 1.0f;
		}

		public GearJointDescriptor(Joint jointA, Joint jointB, float ratio)
		{
			this.JointA = jointA;
			this.JointB = jointB;
			this.Ratio = ratio;
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new GearJoint(this);
		}

		#endregion
	}
}
