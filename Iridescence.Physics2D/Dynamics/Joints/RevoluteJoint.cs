/*
* Copyright (c) 2006-2011 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/


// Point-to-point constraint
// C = p2 - p1
// Cdot = v2 - v1
//      = v2 + cross(w2, r2) - v1 - cross(w1, r1)
// J = [-I -r1_skew I r2_skew ]
// Identity used:
// w k % (rx i + ry j) = w * (-ry i + rx j)

// Motor constraint
// Cdot = w2 - w1
// J = [0 0 -1 0 0 1]
// K = invI1 + invI2

using System;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// A revolute joint constrains two bodies to share a common point while they
	/// are free to rotate about the point. The relative rotation about the shared
	/// point is the joint angle. You can limit the relative rotation with
	/// a joint limit that specifies a lower and upper angle. You can use a motor
	/// to drive the relative rotation about the shared point. A maximum motor torque
	/// is provided so that infinite forces are not generated.
	/// </summary>
	public sealed class RevoluteJoint : Joint
	{
		#region Fields

		// Solver shared
		private readonly Vector2 localAnchorA;
		private readonly Vector2 localAnchorB;
		private Vector3 impulse;
		private float motorImpulse;

		private bool motorEnabled;
		private float maxMotorTorque;
		private float motorSpeed;

		private bool limitEnabled;
		private readonly float referenceAngle;
		private float lowerAngle;
		private float upperAngle;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 rA;
		private Vector2 rB;
		private Vector2 localCenterA;
		private Vector2 localCenterB;
		private float invMassA;
		private float invMassB;
		private float invIA;
		private float invIB;
		private Matrix3x3 mass; // effective mass for point-to-point constraint.
		private float motorMass; // effective mass for motor/limit angular constraint.
		private LimitState limitState;
		
		#endregion

		#region Properties

		public override Vector2 AnchorA => this.bodyA.GetWorldPoint(this.localAnchorA);

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		/// <summary>
		/// Gets the local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA => this.localAnchorA;

		/// <summary>
		/// Gets the local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB => this.localAnchorB;

		/// <summary>
		/// Gets the reference angle.
		/// </summary>
		public float ReferenceAngle => this.referenceAngle;

		/// <summary>
		/// Gets or sets a value that determines whether the joint limit is enabled.
		/// </summary>
		public bool LimitEnabled 
		{
			get => this.limitEnabled;
			set
			{
				if (value != this.limitEnabled)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;
					this.limitEnabled = value;
					this.impulse.Z = 0.0f;
				}
			}
		}

		/// <summary>
		/// Gets or sets the lower limit.
		/// </summary>
		public float LowerLimit
		{
			get => this.lowerAngle;
			set => this.SetLimits(value, this.upperAngle);
		}

		/// <summary>
		/// Gets or sets the upper limit.
		/// </summary>
		public float UpperLimit
		{
			get => this.upperAngle;
			set => this.SetLimits(this.lowerAngle, value);
		}

		/// <summary>
		/// Gets or sets a value that determines whether the motor is enabled.
		/// </summary>
		public bool MotorEnabled
		{
			get => this.motorEnabled;
			set
			{
				if (value != this.motorEnabled)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;
					this.motorEnabled = value;	
				}
			}
		}

		/// <summary>
		/// Gets or sets the speed of the motor in radians per second.
		/// </summary>
		public float MotorSpeed
		{
			get => this.motorSpeed;
			set
			{
				if (value != this.motorSpeed)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;
					this.motorSpeed = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the maximum motor torque in N-m.
		/// </summary>
		public float MaxMotorTorque
		{
			get => this.maxMotorTorque;
			set
			{
				if (value != this.maxMotorTorque)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;	
					this.maxMotorTorque = value;
				}
			}
		}
		
		#endregion

		#region Constructors

		public RevoluteJoint(RevoluteJointDescriptor def)
			: base(def)
		{
			this.localAnchorA = def.LocalAnchorA;
			this.localAnchorB = def.LocalAnchorB;
			this.referenceAngle = def.ReferenceAngle;

			this.impulse = Vector3.Zero;
			this.motorImpulse = 0.0f;

			this.lowerAngle = def.LowerLimit;
			this.upperAngle = def.UpperLimit;
			this.maxMotorTorque = def.MaxMotorTorque;
			this.motorSpeed = def.MotorSpeed;
			this.limitEnabled = def.LimitEnabled;
			this.motorEnabled = def.MotorEnabled;
			this.limitState = LimitState.Inactive;
		}
		
		#endregion

		#region Methods
		
		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterA = this.bodyA.sweep.LocalCenter;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassA = this.bodyA.invMass;
			this.invMassB = this.bodyB.invMass;
			this.invIA = this.bodyA.invInertia;
			this.invIB = this.bodyB.invInertia;

			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			this.rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			this.rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);

			// J = [-I -r1_skew I r2_skew]
			//     [ 0       -1 0       1]
			// r_skew = [-ry; rx]

			// Matlab
			// K = [ mA+r1y^2*iA+mB+r2y^2*iB,  -r1y*iA*r1x-r2y*iB*r2x,          -r1y*iA-r2y*iB]
			//     [  -r1y*iA*r1x-r2y*iB*r2x, mA+r1x^2*iA+mB+r2x^2*iB,           r1x*iA+r2x*iB]
			//     [          -r1y*iA-r2y*iB,           r1x*iA+r2x*iB,                   iA+iB]

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			bool fixedRotation = (iA + iB == 0.0f);
			
			this.mass.M11 = mA + mB + this.rA.Y * this.rA.Y * iA + this.rB.Y * this.rB.Y * iB;
			this.mass.M12 = -this.rA.Y * this.rA.X * iA - this.rB.Y * this.rB.X * iB;
			this.mass.M13 = -this.rA.Y * iA - this.rB.Y * iB;
			this.mass.M21 = this.mass.M12;
			this.mass.M22 = mA + mB + this.rA.X * this.rA.X * iA + this.rB.X * this.rB.X * iB;
			this.mass.M23 = this.rA.X * iA + this.rB.X * iB;
			this.mass.M31 = this.mass.M13;
			this.mass.M32 = this.mass.M23;
			this.mass.M33 = iA + iB;

			this.motorMass = iA + iB;
			if (this.motorMass > 0.0f)
			{
				this.motorMass = 1.0f / this.motorMass;
			}

			if (this.motorEnabled == false || fixedRotation)
			{
				this.motorImpulse = 0.0f;
			}

			if (this.limitEnabled && fixedRotation == false)
			{
				float jointAngle = aB - aA - this.referenceAngle;
				if (System.Math.Abs(this.upperAngle - this.lowerAngle) < 2.0f * Settings.AngularSlop)
				{
					this.limitState = LimitState.EqualLimits;
				}
				else if (jointAngle <= this.lowerAngle)
				{
					if (this.limitState != LimitState.AtLowerLimit)
					{
						this.impulse.Z = 0.0f;
					}
					this.limitState = LimitState.AtLowerLimit;
				}
				else if (jointAngle >= this.upperAngle)
				{
					if (this.limitState != LimitState.AtUpperLimit)
					{
						this.impulse.Z = 0.0f;
					}
					this.limitState = LimitState.AtUpperLimit;
				}
				else
				{
					this.limitState = LimitState.Inactive;
					this.impulse.Z = 0.0f;
				}
			}
			else
			{
				this.limitState = LimitState.Inactive;
			}

			if (data.timeStep.WarmStarting)
			{
				// Scale impulses to support a variable time step.
				this.impulse *= data.timeStep.DeltaTRatio;
				this.motorImpulse *= data.timeStep.DeltaTRatio;

				Vector2 P = new Vector2(this.impulse.X, this.impulse.Y);

				vA -= mA * P;
				wA -= iA * (Vector2.Cross(in this.rA, in P) + this.motorImpulse + this.impulse.Z);

				vB += mB * P;
				wB += iB * (Vector2.Cross(in this.rB, in P) + this.motorImpulse + this.impulse.Z);
			}
			else
			{
				this.impulse = Vector3.Zero;
				this.motorImpulse = 0.0f;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			bool fixedRotation = (iA + iB == 0.0f);

			// Solve motor constraint.
			if (this.motorEnabled && this.limitState != LimitState.EqualLimits && fixedRotation == false)
			{
				float Cdot = wB - wA - this.motorSpeed;
				float impulse = -this.motorMass * Cdot;
				float oldImpulse = this.motorImpulse;
				float maxImpulse = data.timeStep.DeltaT * this.maxMotorTorque;
				this.motorImpulse = Utility.Clamp(this.motorImpulse + impulse, -maxImpulse, maxImpulse);
				impulse = this.motorImpulse - oldImpulse;

				wA -= iA * impulse;
				wB += iB * impulse;
			}

			// Solve limit constraint.
			if (this.limitEnabled && this.limitState != LimitState.Inactive && fixedRotation == false)
			{
				Vector2 Cdot1 = vB + Vector2.Cross(wB, in this.rB) - vA - Vector2.Cross(wA, in this.rA);
				float Cdot2 = wB - wA;
				Vector3 Cdot = new Vector3(Cdot1.X, Cdot1.Y, Cdot2);

				Matrix3x3.Solve(ref this.mass, ref Cdot, out Vector3 impulse);
				Vector3.Negate(impulse, out impulse);

				if (this.limitState == LimitState.EqualLimits)
				{
					this.impulse += impulse;
				}
				else if (this.limitState == LimitState.AtLowerLimit)
				{
					float newImpulse = this.impulse.Z + impulse.Z;
					if (newImpulse < 0.0f)
					{
						Vector2 rhs = -Cdot1 + this.impulse.Z * (new Vector2(this.mass.M13, this.mass.M23));
						Matrix3x3.Solve(ref this.mass, ref rhs, out Vector2 reduced);

						impulse.X = reduced.X;
						impulse.Y = reduced.Y;
						impulse.Z = -this.impulse.Z;
						this.impulse.X += reduced.X;
						this.impulse.Y += reduced.Y;
						this.impulse.Z = 0.0f;
					}
					else
					{
						this.impulse += impulse;
					}
				}
				else if (this.limitState == LimitState.AtUpperLimit)
				{
					float newImpulse = this.impulse.Z + impulse.Z;
					if (newImpulse > 0.0f)
					{
						Vector2 rhs = -Cdot1 + this.impulse.Z * (new Vector2(this.mass.M13, this.mass.M23));
						Matrix3x3.Solve(ref this.mass, ref rhs, out Vector2 reduced);

						impulse.X = reduced.X;
						impulse.Y = reduced.Y;
						impulse.Z = -this.impulse.Z;
						this.impulse.X += reduced.X;
						this.impulse.Y += reduced.Y;
						this.impulse.Z = 0.0f;
					}
					else
					{
						this.impulse += impulse;
					}
				}

				Vector2 P = new Vector2(impulse.X, impulse.Y);

				vA -= mA * P;
				wA -= iA * (Vector2.Cross(in this.rA, in P) + impulse.Z);

				vB += mB * P;
				wB += iB * (Vector2.Cross(in this.rB, in P) + impulse.Z);
			}
			else
			{
				// Solve point-to-point constraint
				Vector2 Cdot = -(vB + Vector2.Cross(wB, in this.rB) - vA - Vector2.Cross(wA, in this.rA));
				Matrix3x3.Solve(ref this.mass, ref Cdot, out Vector2 impulse);

				this.impulse.X += impulse.X;
				this.impulse.Y += impulse.Y;

				vA -= mA * impulse;
				wA -= iA * Vector2.Cross(in this.rA, in impulse);

				vB += mB * impulse;
				wB += iB * Vector2.Cross(in this.rB, in impulse);
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			float angularError = 0.0f;
			float positionError = 0.0f;

			bool fixedRotation = (this.invIA + this.invIB == 0.0f);

			// Solve angular limit constraint.
			if (this.limitEnabled && this.limitState != LimitState.Inactive && fixedRotation == false)
			{
				float angle = aB - aA - this.referenceAngle;
				float limitImpulse = 0.0f;

				if (this.limitState == LimitState.EqualLimits)
				{
					// Prevent large angular corrections
					float C = Utility.Clamp(angle - this.lowerAngle, -Settings.MaxAngularCorrection, Settings.MaxAngularCorrection);
					limitImpulse = -this.motorMass * C;
					angularError = System.Math.Abs(C);
				}
				else if (this.limitState == LimitState.AtLowerLimit)
				{
					float C = angle - this.lowerAngle;
					angularError = -C;

					// Prevent large angular corrections and allow some slop.
					C = Utility.Clamp(C + Settings.AngularSlop, -Settings.MaxAngularCorrection, 0.0f);
					limitImpulse = -this.motorMass * C;
				}
				else if (this.limitState == LimitState.AtUpperLimit)
				{
					float C = angle - this.upperAngle;
					angularError = C;

					// Prevent large angular corrections and allow some slop.
					C = Utility.Clamp(C - Settings.AngularSlop, 0.0f, Settings.MaxAngularCorrection);
					limitImpulse = -this.motorMass * C;
				}

				aA -= this.invIA * limitImpulse;
				aB += this.invIB * limitImpulse;
			}

			// Solve point-to-point constraint.
			{
				qA.Angle = aA;
				qB.Angle = aB;
				Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
				Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);

				Vector2 C = cB + rB - cA - rA;
				positionError = C.Length;

				float mA = this.invMassA, mB = this.invMassB;
				float iA = this.invIA, iB = this.invIB;

				Matrix2x2 K;
				K.M11 = mA + mB + iA * rA.Y * rA.Y + iB * rB.Y * rB.Y;
				K.M12 = -iA * rA.X * rA.Y - iB * rB.X * rB.Y;
				K.M21 = K.M12;
				K.M22 = mA + mB + iA * rA.X * rA.X + iB * rB.X * rB.X;

				// TODO
				Vector2 __C; __C.X = C.X; __C.Y = C.Y;
				Matrix2x2.Negate(K, out K);
				Matrix2x2.Solve(ref K, ref __C, out Vector2 __impulse);
				Vector2 impulse;
				impulse.X = __impulse.X;
				impulse.Y = __impulse.Y;

				cA -= mA * impulse;
				aA -= iA * Vector2.Cross(in rA, in impulse);

				cB += mB * impulse;
				aB += iB * Vector2.Cross(in rB, in impulse);
			}

			data.states[this.indexA].Position = cA;
			data.states[this.indexA].Angle = aA;
			data.states[this.indexB].Position = cB;
			data.states[this.indexB].Angle = aB;

			return positionError <= Settings.LinearSlop && angularError <= Settings.AngularSlop;
		}

		public override Vector2 GetReactionForce(float inv_dt)
		{
			Vector2 P = new Vector2(this.impulse.X, this.impulse.Y);
			return inv_dt * P;
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return inv_dt * this.impulse.Z;
		}

		public float GetJointAngle()
		{
			Body bA = this.bodyA;
			Body bB = this.bodyB;
			return bB.sweep.Angle - bA.sweep.Angle - this.referenceAngle;
		}

		public float GetJointSpeed()
		{
			Body bA = this.bodyA;
			Body bB = this.bodyB;
			return bB.AngularVelocity - bA.AngularVelocity;
		}

		public float GetMotorTorque(float inv_dt)
		{
			return inv_dt * this.motorImpulse;
		}
		
		public void SetLimits(float lower, float upper)
		{
			if (lower > upper)
				throw new ArgumentException("The lower limit must not be greater than the upper limit.");

			if (lower != this.lowerAngle ||
				upper != this.upperAngle)
			{
				this.bodyA.Awake = true;
				this.bodyB.Awake = true;
				this.lowerAngle = lower;
				this.upperAngle = upper;
				this.impulse.Z = 0.0f;
			}
		}	
		
		#endregion
	}
}
