namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// Represents an abstract joint descriptor.
	/// </summary>
	public abstract class JointDescriptor
	{
		#region Fields

		/// <summary>
		/// Gets a value that indicates whether the two attached bodies can collide with eachother.
		/// If this is false, the bodies will pass through each other without collision.
		/// </summary>
		public bool CollideConnected;

		#endregion

		#region Properties

		/// <summary>
		/// The first attached body.
		/// </summary>
		public Body BodyA { get; set; }

		/// <summary>
		/// The second attached body.
		/// </summary>
		public Body BodyB { get; set; }

		/// <summary>
		/// Gets or sets a user object.
		/// </summary>
		public object UserData { get; set; }

		#endregion

		#region Methods

		/// <summary>
		/// Creates a joint instance based on this descriptor.
		/// </summary>
		/// <returns></returns>
		protected internal abstract Joint Create();

		#endregion
	}
}
