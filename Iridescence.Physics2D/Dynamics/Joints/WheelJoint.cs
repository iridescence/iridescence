/*
* Copyright (c) 2006-2007 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

// Linear raint (point-to-line)
// d = pB - pA = xB + rB - xA - rA
// C = dot(ay, d)
// Cdot = dot(d, cross(wA, ay)) + dot(ay, vB + cross(wB, rB) - vA - cross(wA, rA))
//      = -dot(ay, vA) - dot(cross(d + rA, ay), wA) + dot(ay, vB) + dot(cross(rB, ay), vB)
// J = [-ay, -cross(d + rA, ay), ay, cross(rB, ay)]

// Spring linear raint
// C = dot(ax, d)
// Cdot = = -dot(ax, vA) - dot(cross(d + rA, ax), wA) + dot(ax, vB) + dot(cross(rB, ax), vB)
// J = [-ax -cross(d+rA, ax) ax cross(rB, ax)]

// Motor rotational raint
// Cdot = wB - wA
// J = [0 0 -1 0 0 1]

using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// A wheel joint. This joint provides two degrees of freedom: translation
	/// along an axis fixed in bodyA and rotation in the plane. In other words, it is a point to
	/// line constraint with a rotational motor and a linear spring/damper.
	/// </summary>
	public sealed class WheelJoint : Joint
	{
		#region Fields

		private float frequency;
		private float dampingRatio;

		// Solver shared
		private readonly Vector2 localAnchorA;
		private readonly Vector2 localAnchorB;
		private readonly Vector2 localXAxisA;
		private readonly Vector2 localYAxisA;

		private float impulse;
		private float motorImpulse;
		private float springImpulse;

		private float maxMotorTorque;
		private float motorSpeed;
		private bool motorEnabled;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 localCenterA;
		private Vector2 localCenterB;
		private float invMassA;
		private float invMassB;
		private float invIA;
		private float invIB;

		private Vector2 ax, ay;
		private float sAx, sBx;
		private float sAy, sBy;

		private float mass;
		private float motorMass;
		private float springMass;

		private float bias;
		private float gamma;
		
		#endregion

		#region Properties

		public override Vector2 AnchorA => this.bodyA.GetWorldPoint(this.localAnchorA);

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		/// <summary>
		/// Gets or sets a value that determines whether the motor is enabled.
		/// </summary>
		public bool MotorEnabled
		{
			get => this.motorEnabled;
			set
			{
				this.bodyA.Awake = true;
				this.bodyB.Awake = true;
				this.motorEnabled = value;
			}
		}

		/// <summary>
		/// Gets or sets the motor speed in radians per second.
		/// </summary>
		public float MotorSpeed
		{
			get => this.motorSpeed;
			set
			{
				this.bodyA.Awake = true;
				this.bodyB.Awake = true;
				this.motorSpeed = value;
			}
		}

		/// <summary>
		/// Gets or sets the maximum motor torque in N-m.
		/// </summary>
		public float MaxMotorTorque
		{
			get => this.maxMotorTorque;
			set
			{
				this.bodyA.Awake = true;
				this.bodyB.Awake = true;
				this.maxMotorTorque = value;
			}
		}

		/// <summary>
		/// Gets or sets the spring frequency in Hertz.
		/// Setting the frequency to zero disables the spring.
		/// </summary>
		public float Frequency
		{
			get => this.frequency;
			set => this.frequency = value;
		}

		/// <summary>
		/// Gets or sets the spring damping ratio.
		/// </summary>
		public float DampingRatio
		{
			get => this.dampingRatio;
			set => this.dampingRatio = value;
		}
		
		#endregion

		#region Constructors

		public WheelJoint(WheelJointDescriptor def)
			: base(def)
		{
			this.localAnchorA = def.LocalAnchorA;
			this.localAnchorB = def.LocalAnchorB;
			this.localXAxisA = def.LocalAxisA;
			Vector2 b;
			b.X = -this.localXAxisA.Y;
			b.Y = this.localXAxisA.X;
			this.localYAxisA = b; // b2Math.b2Cross(1.0f, m_localXAxisA);

			this.mass = 0.0f;
			this.impulse = 0.0f;
			this.motorMass = 0.0f;
			this.motorImpulse = 0.0f;
			this.springMass = 0.0f;
			this.springImpulse = 0.0f;

			this.maxMotorTorque = def.MaxMotorTorque;
			this.motorSpeed = def.MotorSpeed;
			this.motorEnabled = def.MotorEnabled;

			this.frequency = def.Frequency;
			this.dampingRatio = def.DampingRatio;

			this.bias = 0.0f;
			this.gamma = 0.0f;

			this.ax = Vector2.Zero;
			this.ay = Vector2.Zero;
		}
		
		#endregion

		#region Methods

		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterA = this.bodyA.sweep.LocalCenter;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassA = this.bodyA.invMass;
			this.invMassB = this.bodyB.invMass;
			this.invIA = this.bodyA.invInertia;
			this.invIB = this.bodyB.invInertia;

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			// Compute the effective masses.
			Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);
			Vector2 d = cB + rB - cA - rA;

			// Point to line raint
			{
				this.ay = Rotation.Multiply(qA, this.localYAxisA);
				this.sAy = Vector2.Cross(d + rA, in this.ay);
				this.sBy = Vector2.Cross(in rB, in this.ay);

				this.mass = mA + mB + iA * this.sAy * this.sAy + iB * this.sBy * this.sBy;

				if (this.mass > 0.0f)
				{
					this.mass = 1.0f / this.mass;
				}
			}

			// Spring raint
			this.springMass = 0.0f;
			this.bias = 0.0f;
			this.gamma = 0.0f;
			if (this.frequency > 0.0f)
			{
				this.ax = Rotation.Multiply(qA, this.localXAxisA);
				this.sAx = Vector2.Cross(d + rA, in this.ax);
				this.sBx = Vector2.Cross(in rB, in this.ax);

				float invMass = mA + mB + iA * this.sAx * this.sAx + iB * this.sBx * this.sBx;

				if (invMass > 0.0f)
				{
					this.springMass = 1.0f / invMass;

					float C = Vector2.Dot(d, this.ax);

					// Frequency
					float omega = 2.0f * (float)System.Math.PI * this.frequency;

					// Damping coefficient
					float dx = 2.0f * this.springMass * this.dampingRatio * omega;

					// Spring stiffness
					float k = this.springMass * omega * omega;

					// magic formulas
					float h = data.timeStep.DeltaT;
					this.gamma = h * (dx + h * k);
					if (this.gamma > 0.0f)
					{
						this.gamma = 1.0f / this.gamma;
					}

					this.bias = C * h * k * this.gamma;

					this.springMass = invMass + this.gamma;
					if (this.springMass > 0.0f)
					{
						this.springMass = 1.0f / this.springMass;
					}
				}
			}
			else
			{
				this.springImpulse = 0.0f;
			}

			// Rotational motor
			if (this.motorEnabled)
			{
				this.motorMass = iA + iB;
				if (this.motorMass > 0.0f)
				{
					this.motorMass = 1.0f / this.motorMass;
				}
			}
			else
			{
				this.motorMass = 0.0f;
				this.motorImpulse = 0.0f;
			}

			if (data.timeStep.WarmStarting)
			{
				// Account for variable time step.
				this.impulse *= data.timeStep.DeltaTRatio;
				this.springImpulse *= data.timeStep.DeltaTRatio;
				this.motorImpulse *= data.timeStep.DeltaTRatio;

				Vector2 P = this.impulse * this.ay + this.springImpulse * this.ax;
				float LA = this.impulse * this.sAy + this.springImpulse * this.sAx + this.motorImpulse;
				float LB = this.impulse * this.sBy + this.springImpulse * this.sBx + this.motorImpulse;

				vA -= this.invMassA * P;
				wA -= this.invIA * LA;

				vB += this.invMassB * P;
				wB += this.invIB * LB;
			}
			else
			{
				this.impulse = 0.0f;
				this.springImpulse = 0.0f;
				this.motorImpulse = 0.0f;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			// Solve spring raint
			{
				float Cdot = Vector2.Dot(this.ax, vB - vA) + this.sBx * wB - this.sAx * wA;
				float impulse = -this.springMass * (Cdot + this.bias + this.gamma * this.springImpulse);
				this.springImpulse += impulse;

				Vector2 P = impulse * this.ax;
				float LA = impulse * this.sAx;
				float LB = impulse * this.sBx;

				vA -= mA * P;
				wA -= iA * LA;

				vB += mB * P;
				wB += iB * LB;
			}

			// Solve rotational motor raint
			{
				float Cdot = wB - wA - this.motorSpeed;
				float impulse = -this.motorMass * Cdot;

				float oldImpulse = this.motorImpulse;
				float maxImpulse = data.timeStep.DeltaT * this.maxMotorTorque;
				this.motorImpulse = Utility.Clamp(this.motorImpulse + impulse, -maxImpulse, maxImpulse);
				impulse = this.motorImpulse - oldImpulse;

				wA -= iA * impulse;
				wB += iB * impulse;
			}

			// Solve point to line raint
			{
				float Cdot = Vector2.Dot(this.ay, vB - vA) + this.sBy * wB - this.sAy * wA;
				float impulse = -this.mass * Cdot;
				this.impulse += impulse;

				Vector2 P = impulse * this.ay;
				float LA = impulse * this.sAy;
				float LB = impulse * this.sBy;

				vA -= mA * P;
				wA -= iA * LA;

				vB += mB * P;
				wB += iB * LB;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);
			Vector2 d = (cB - cA) + rB - rA;

			Vector2 ay = Rotation.Multiply(qA, this.localYAxisA);

			float sAy = Vector2.Cross(d + rA, in ay);
			float sBy = Vector2.Cross(in rB, in ay);

			float C = Vector2.Dot(d, ay);

			float k = this.invMassA + this.invMassB + this.invIA * this.sAy * this.sAy + this.invIB * this.sBy * this.sBy;

			float impulse;
			if (k != 0.0f)
				impulse = -C / k;
			else
				impulse = 0.0f;

			Vector2 P = impulse * ay;
			float LA = impulse * sAy;
			float LB = impulse * sBy;

			cA -= this.invMassA * P;
			aA -= this.invIA * LA;
			cB += this.invMassB * P;
			aB += this.invIB * LB;

			data.states[this.indexA].Position = cA;
			data.states[this.indexA].Angle = aA;
			data.states[this.indexB].Position = cB;
			data.states[this.indexB].Angle = aB;

			return System.Math.Abs(C) <= Settings.LinearSlop;
		}

		public override Vector2 GetReactionForce(float inv_dt)
		{
			return inv_dt * (this.impulse * this.ay + this.springImpulse * this.ax);
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return inv_dt * this.motorImpulse;
		}
			
		public float GetJointTranslation()
		{
			Body bA = this.bodyA;
			Body bB = this.bodyB;

			Vector2 pA = bA.GetWorldPoint(this.localAnchorA);
			Vector2 pB = bB.GetWorldPoint(this.localAnchorB);
			Vector2 d = pB - pA;
			Vector2 axis = bA.GetWorldVector(this.localXAxisA);

			float translation = Vector2.Dot(d, axis);
			return translation;
		}

		public float GetJointLinearSpeed() 
		{
			Body bA = this.bodyA;
			Body bB = this.bodyB;

			Vector2 rA = Rotation.Multiply(bA.transform.Q, this.localAnchorA - bA.sweep.LocalCenter);
			Vector2 rB = Rotation.Multiply(bB.transform.Q, this.localAnchorB - bB.sweep.LocalCenter);
			Vector2 p1 = bA.sweep.Center + rA;
			Vector2 p2 = bB.sweep.Center + rB;
			Vector2 d = p2 - p1;
			Vector2 axis = Rotation.Multiply(bA.transform.Q, this.localXAxisA);

			Vector2 vA = bA.linearVelocity;
			Vector2 vB = bB.linearVelocity;
			float wA = bA.angularVelocity;
			float wB = bB.angularVelocity;

			return Vector2.Dot(d, Vector2.Cross(wA, axis)) + Vector2.Dot(axis, vB + Vector2.Cross(wB, rB) - vA - Vector2.Cross(wA, rA));
		}

		public float GetJointAngle() 
		{
			return this.bodyB.sweep.Angle - this.bodyA.sweep.Angle;
		}

		public float GetJointAngularSpeed() 
		{
			return this.bodyB.angularVelocity - this.bodyA.angularVelocity;
		}

		public float GetMotorTorque(float inv_dt)
		{
			return inv_dt * this.motorImpulse;
		}

		#endregion
	}
}
