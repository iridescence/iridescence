using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// Pulley joint definition. This requires two ground anchors,
	/// two dynamic body anchor points, and a pulley ratio.
	public class PulleyJointDescriptor : JointDescriptor
	{
		#region Properties

		/// <summary>
		/// The first ground anchor in world coordinates. This point never moves.
		/// </summary>
		public Vector2 GroundAnchorA { get; set; }

		/// <summary>
		/// The second ground anchor in world coordinates. This point never moves.
		/// </summary>
		public Vector2 GroundAnchorB { get; set; }

		/// <summary>
		/// The local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA { get; set; }

		/// <summary>
		/// The local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB { get; set; }

		/// <summary>
		/// The a reference length for the segment attached to bodyA.
		/// </summary>
		public float LengthA { get; set; }

		/// <summary>
		/// The a reference length for the segment attached to bodyB.
		/// </summary>
		public float LengthB { get; set; }

		/// <summary>
		/// The pulley ratio, used to simulate a block-and-tackle.
		/// </summary>
		public float Ratio { get; set; }

		#endregion

		#region Constructors

		public PulleyJointDescriptor()
		{
			this.GroundAnchorA = new Vector2(-1.0f, 1.0f);
			this.GroundAnchorB = new Vector2(1.0f, 1.0f);
			this.LocalAnchorA = new Vector2(-1.0f, 0.0f);
			this.LocalAnchorB = new Vector2(1.0f, 0.0f);
			this.LengthA = 0.0f;
			this.LengthB = 0.0f;
			this.Ratio = 1.0f;
			this.CollideConnected = true;
		}

		/// <summary>
		/// Initialize the bodies, anchors, lengths, max lengths, and ratio using the world anchors.
		/// </summary>
		/// <param name="bA"></param>
		/// <param name="bB"></param>
		/// <param name="groundA"></param>
		/// <param name="groundB"></param>
		/// <param name="anchorA"></param>
		/// <param name="anchorB"></param>
		/// <param name="r"></param>
		public PulleyJointDescriptor(Body bA, Body bB, Vector2 groundA, Vector2 groundB, Vector2 anchorA, Vector2 anchorB, float r)
			: this()
		{
			this.BodyA = bA;
			this.BodyB = bB;
			this.GroundAnchorA = groundA;
			this.GroundAnchorB = groundB;
			this.LocalAnchorA = this.BodyA.GetLocalPoint(anchorA);
			this.LocalAnchorB = this.BodyB.GetLocalPoint(anchorB);
			Vector2 dA = anchorA - groundA;
			this.LengthA = dA.Length;
			Vector2 dB = anchorB - groundB;
			this.LengthB = dB.Length;
			this.Ratio = r;
			System.Diagnostics.Debug.Assert(this.Ratio > float.Epsilon);
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new PulleyJoint(this);
		}

		#endregion
	}
}
