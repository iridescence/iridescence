﻿using System;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	[Flags]
	public enum LimitState : short
	{
		Inactive = 1,
		AtLowerLimit = 1 << 1,
		AtUpperLimit = 1 << 2,
		EqualLimits = 1 << 3
	}
}
