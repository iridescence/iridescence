﻿using System;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// Joint event args.
	/// </summary>
	public class JointEventArgs : EventArgs
	{
		#region Properties

		/// <summary>
		/// Gets the joint.
		/// </summary>
		public Joint Joint { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new JointEventArgs.
		/// </summary>
		public JointEventArgs(Joint joint)
		{
			this.Joint = joint;
		}

		#endregion
	}
}
