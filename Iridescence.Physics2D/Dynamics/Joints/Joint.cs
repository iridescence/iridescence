/*
* Copyright (c) 2006-2007 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

using System;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	public abstract class Joint
	{
		#region Fields

		internal JointEdge edgeA;
		internal JointEdge edgeB;
		internal Body bodyA;
		internal Body bodyB;

		protected int index;

		internal bool islandFlag;
		internal bool collideConnected;
		
		#endregion

		#region Properties

		/// <summary>
		/// Short-cut function to determine if either body is inactive.
		/// </summary>
		public bool Active => this.bodyA.Active && this.bodyB.Active;

		/// <summary>
		/// Get the first body attached to this joint.
		/// </summary>
		public Body BodyA => this.bodyA;

		/// <summary>
		/// Get the second body attached to this joint.
		/// </summary>
		public Body BodyB => this.bodyB;

		/// <summary>
		/// Gets a value that indicates whether the two attached bodies can collide with eachother.
		/// If this is false, the bodies will pass through each other without collision.
		/// </summary>
		public bool CollideConnected => this.collideConnected;

		/// <summary>
		/// Gets the joint's first anchor in world coordinates.
		/// </summary>
		public virtual Vector2 AnchorA => Vector2.Zero;

		/// <summary>
		/// Gets the joint's second anchor in world coordinates.
		/// </summary>
		public virtual Vector2 AnchorB => Vector2.Zero;

		/// <summary>
		/// Gets or sets a user object.
		/// </summary>
		public object UserData { get; set; }

		#endregion

		#region Constructors

		protected Joint(JointDescriptor descriptor)
		{
			this.bodyA = descriptor.BodyA;
			this.bodyB = descriptor.BodyB;

			if (!ReferenceEquals(this.bodyA.World, this.bodyB.World))
				throw new ArgumentException("Can't connect bodies of different worlds.");

			this.index = 0;
			this.collideConnected = descriptor.CollideConnected;
			this.islandFlag = false;
			this.edgeA = new JointEdge();
			this.edgeB = new JointEdge();

			this.UserData = descriptor.UserData;
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Initializes velocity constraints for iteration.
		/// </summary>
		/// <param name="data"></param>
		protected internal virtual void InitVelocityConstraints(SolverData data)
		{

		}

		/// <summary>
		/// Iteratively solves the velocity constraints.
		/// </summary>
		/// <param name="data"></param>
		protected internal virtual void SolveVelocityConstraints(SolverData data)
		{

		}

		/// <summary>
		/// This returns true if the position errors are within tolerance.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		protected internal virtual bool SolvePositionConstraints(SolverData data)
		{
			return false;
		}

		/// <summary>
		/// Returns the reaction force on bodyB at the joint anchor in Newtons.
		/// </summary>
		/// <param name="invDeltaT"></param>
		/// <returns></returns>
		public virtual Vector2 GetReactionForce(float invDeltaT)
		{
			return Vector2.Zero;
		}

		/// <summary>
		/// Returns the reaction torque on bodyB in N*m.
		/// </summary>
		/// <param name="invDeltaT"></param>
		/// <returns></returns>
		public virtual float GetReactionTorque(float invDeltaT)
		{
			return 0.0f;
		}

		/// <summary>
		/// Shift the origin for any points stored in world coordinates.
		/// </summary>
		/// <param name="newOrigin"></param>
		public virtual void ShiftOrigin(Vector2 newOrigin)
		{

		}

		#endregion
	}
}