/*
* Copyright (c) 2007-2011 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

// Limit:
// C = norm(pB - pA) - L
// u = (pB - pA) / norm(pB - pA)
// Cdot = dot(u, vB + cross(wB, rB) - vA - cross(wA, rA))
// J = [-u -cross(rA, u) u cross(rB, u)]
// K = J * invM * JT
//   = invMassA + invIA * cross(rA, u)^2 + invMassB + invIB * cross(rB, u)^2

using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// A rope joint enforces a maximum distance between two points
	/// on two bodies. It has no other effect.
	/// Warning: if you attempt to change the maximum length during
	/// the simulation you will get some non-physical behavior.
	/// A model that would allow you to dynamically modify the length
	/// would have some sponginess, so I chose not to implement it
	/// that way. See b2DistanceJoint if you want to dynamically
	/// control length.
	/// </summary>
	public sealed class RopeJoint : Joint
	{
		#region Fields

		// Solver shared
		private readonly Vector2 localAnchorA;
		private readonly Vector2 localAnchorB;
		private float maxLength;
		private float length;
		private float impulse;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 u;
		private Vector2 rA;
		private Vector2 rB;
		private Vector2 localCenterA;
		private Vector2 localCenterB;
		private float invMassA;
		private float invMassB;
		private float invIA;
		private float invIB;
		private float mass;
		private LimitState state;

		#endregion

		#region Properties

		public override Vector2 AnchorA => this.bodyA.GetWorldPoint(this.localAnchorA);

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		/// <summary>
		/// Gets the local anchor point relative to bodyA's origin.
		/// </summary>
		/// <returns></returns>
		public Vector2 LocalAnchorA => this.localAnchorA;

		/// <summary>
		/// Gets the local anchor point relative to bodyB's origin.
		/// </summary>
		/// <returns></returns>
		public Vector2 LocalAnchorB => this.localAnchorB;

		/// <summary>
		/// Gets or sets the maximum length of the rope.
		/// </summary>
		public float MaxLength
		{
			get => this.maxLength;
			set => this.maxLength = value;
		}

		public LimitState LimitState => this.state;

		#endregion

		#region Constructors

		public RopeJoint(RopeJointDescriptor def)
			: base(def)
		{
			this.localAnchorA = def.LocalAnchorA;
			this.localAnchorB = def.LocalAnchorB;

			this.maxLength = def.Length;

			this.mass = 0.0f;
			this.impulse = 0.0f;
			this.state = LimitState.Inactive;
			this.length = 0.0f;
		}
		
		#endregion

		#region Methods

		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterA = this.bodyA.sweep.LocalCenter;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassA = this.bodyA.invMass;
			this.invMassB = this.bodyB.invMass;
			this.invIA = this.bodyA.invInertia;
			this.invIB = this.bodyB.invInertia;

			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			this.rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			this.rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);
			this.u = cB + this.rB - cA - this.rA;

			this.length = this.u.Length;

			float C = this.length - this.maxLength;
			if (C > 0.0f)
			{
				this.state = LimitState.AtUpperLimit;
			}
			else
			{
				this.state = LimitState.Inactive;
			}

			if (this.length > Settings.LinearSlop)
			{
				this.u *= 1.0f / this.length;
			}
			else
			{
				this.u = Vector2.Zero;
				this.mass = 0.0f;
				this.impulse = 0.0f;
				return;
			}

			// Compute effective mass.
			float crA = Vector2.Cross(in this.rA, in this.u);
			float crB = Vector2.Cross(in this.rB, in this.u);
			float invMass = this.invMassA + this.invIA * crA * crA + this.invMassB + this.invIB * crB * crB;

			this.mass = invMass != 0.0f ? 1.0f / invMass : 0.0f;

			if (data.timeStep.WarmStarting)
			{
				// Scale the impulse to support a variable time step.
				this.impulse *= data.timeStep.DeltaTRatio;

				Vector2 P = this.impulse * this.u;
				vA -= this.invMassA * P;
				wA -= this.invIA * Vector2.Cross(in this.rA, in P);
				vB += this.invMassB * P;
				wB += this.invIB * Vector2.Cross(in this.rB, in P);
			}
			else
			{
				this.impulse = 0.0f;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			// Cdot = dot(u, v + cross(w, r))
			Vector2 vpA = vA + Vector2.Cross(wA, in this.rA);
			Vector2 vpB = vB + Vector2.Cross(wB, in this.rB);
			float C = this.length - this.maxLength;
			float Cdot = Vector2.Dot(this.u, vpB - vpA);

			// Predictive constraint.
			if (C < 0.0f)
			{
				Cdot += data.timeStep.InvDeltaT * C;
			}

			float impulse = -this.mass * Cdot;
			float oldImpulse = this.impulse;
			this.impulse = System.Math.Min(0.0f, this.impulse + impulse);
			impulse = this.impulse - oldImpulse;

			Vector2 P = impulse * this.u;
			vA -= this.invMassA * P;
			wA -= this.invIA * Vector2.Cross(in this.rA, in P);
			vB += this.invMassB * P;
			wB += this.invIB * Vector2.Cross(in this.rB, in P);

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);
			Vector2 u = cB + rB - cA - rA;

			float length = Vector2.Normalize(in u, out u);
			float C = length - this.maxLength;

			C = Utility.Clamp(C, 0.0f, Settings.MaxLinearCorrection);

			float impulse = -this.mass * C;
			Vector2 P = impulse * u;

			cA -= this.invMassA * P;
			aA -= this.invIA * Vector2.Cross(in rA, in P);
			cB += this.invMassB * P;
			aB += this.invIB * Vector2.Cross(in rB, in P);

			data.states[this.indexA].Position = cA;
			data.states[this.indexA].Angle = aA;
			data.states[this.indexB].Position = cB;
			data.states[this.indexB].Angle = aB;

			return length - this.maxLength < Settings.LinearSlop;
		}

		public override Vector2 GetReactionForce(float inv_dt)
		{
			return (inv_dt * this.impulse) * this.u;
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return 0.0f;
		}

		#endregion
	}
}