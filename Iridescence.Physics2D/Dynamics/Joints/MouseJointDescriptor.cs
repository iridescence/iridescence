using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	public sealed class MouseJointDescriptor : JointDescriptor
	{
		#region Properties

		/// <summary>
		/// The initial world target point. This is assumed
		/// to coincide with the body anchor initially.
		/// </summary>
		public Vector2 Target { get; set; }

		/// <summary>
		/// The maximum constraint force that can be exerted
		/// to move the candidate body. Usually you will express
		/// as some multiple of the weight (multiplier * mass * gravity).
		/// </summary>
		public float MaxForce { get; set; }

		/// <summary>
		/// The response speed in Hertz.
		/// </summary>
		public float Frequency { get; set; }

		/// <summary>
		/// The damping ratio.
		/// 0 = no damping, 1 = critical damping.
		/// </summary>
		public float DampingRatio { get; set; }

		#endregion

		#region Constructors

		public MouseJointDescriptor()
		{
			this.Target = Vector2.Zero;
			this.MaxForce = 0.0f;
			this.Frequency = 5.0f;
			this.DampingRatio = 0.7f;
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new MouseJoint(this);
		}

		#endregion
	}
}
