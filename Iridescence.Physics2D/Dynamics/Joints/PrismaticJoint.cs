/*
* Copyright (c) 2006-2011 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

// Linear constraint (point-to-line)
// d = p2 - p1 = x2 + r2 - x1 - r1
// C = dot(perp, d)
// Cdot = dot(d, cross(w1, perp)) + dot(perp, v2 + cross(w2, r2) - v1 - cross(w1, r1))
//      = -dot(perp, v1) - dot(cross(d + r1, perp), w1) + dot(perp, v2) + dot(cross(r2, perp), v2)
// J = [-perp, -cross(d + r1, perp), perp, cross(r2,perp)]
//
// Angular constraint
// C = a2 - a1 + a_initial
// Cdot = w2 - w1
// J = [0 0 -1 0 0 1]
//
// K = J * invM * JT
//
// J = [-a -s1 a s2]
//     [0  -1  0  1]
// a = perp
// s1 = cross(d + r1, a) = cross(p2 - x1, a)
// s2 = cross(r2, a) = cross(p2 - x2, a)


// Motor/Limit linear constraint
// C = dot(ax1, d)
// Cdot = = -dot(ax1, v1) - dot(cross(d + r1, ax1), w1) + dot(ax1, v2) + dot(cross(r2, ax1), v2)
// J = [-ax1 -cross(d+r1,ax1) ax1 cross(r2,ax1)]

// Block Solver
// We develop a block solver that includes the joint limit. This makes the limit stiff (inelastic) even
// when the mass has poor distribution (leading to large torques about the joint anchor points).
//
// The Jacobian has 3 rows:
// J = [-uT -s1 uT s2] // linear
//     [0   -1   0  1] // angular
//     [-vT -a1 vT a2] // limit
//
// u = perp
// v = axis
// s1 = cross(d + r1, u), s2 = cross(r2, u)
// a1 = cross(d + r1, v), a2 = cross(r2, v)

// M * (v2 - v1) = JT * df
// J * v2 = bias
//
// v2 = v1 + invM * JT * df
// J * (v1 + invM * JT * df) = bias
// K * df = bias - J * v1 = -Cdot
// K = J * invM * JT
// Cdot = J * v1 - bias
//
// Now solve for f2.
// df = f2 - f1
// K * (f2 - f1) = -Cdot
// f2 = invK * (-Cdot) + f1
//
// Clamp accumulated limit impulse.
// lower: f2(3) = max(f2(3), 0)
// upper: f2(3) = min(f2(3), 0)
//
// Solve for correct f2(1:2)
// K(1:2, 1:2) * f2(1:2) = -Cdot(1:2) - K(1:2,3) * f2(3) + K(1:2,1:3) * f1
//                       = -Cdot(1:2) - K(1:2,3) * f2(3) + K(1:2,1:2) * f1(1:2) + K(1:2,3) * f1(3)
// K(1:2, 1:2) * f2(1:2) = -Cdot(1:2) - K(1:2,3) * (f2(3) - f1(3)) + K(1:2,1:2) * f1(1:2)
// f2(1:2) = invK(1:2,1:2) * (-Cdot(1:2) - K(1:2,3) * (f2(3) - f1(3))) + f1(1:2)
//
// Now compute impulse to be applied:
// df = f2 - f1

using System;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// A prismatic joint. This joint provides one degree of freedom: translation
	/// along an axis fixed in bodyA. Relative rotation is prevented. You can
	/// use a joint limit to restrict the range of motion and a joint motor to
	/// drive the motion or to model joint friction.
	/// </summary>
	public sealed class PrismaticJoint : Joint
	{
		#region Fields

		// Solver shared
		private readonly Vector2 localAnchorA;
		private readonly Vector2 localAnchorB;
		private readonly Vector2 localXAxisA;
		private readonly Vector2 localYAxisA;
		private readonly float referenceAngle;
		private Vector3 impulse;
		private float motorImpulse;
		private float lowerTranslation;
		private float upperTranslation;
		private float maxMotorForce;
		private float motorSpeed;
		private bool limitEnabled;
		private bool motorEnabled;
		private LimitState limitState;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 localCenterA;
		private Vector2 localCenterB;
		private float invMassA;
		private float invMassB;
		private float invIA;
		private float invIB;
		private Vector2 axis, perp;
		private float s1, s2;
		private float a1, a2;
		private Matrix3x3 K;
		private float motorMass;
		
		#endregion

		#region Properties

		public override Vector2 AnchorA => this.bodyA.GetWorldPoint(this.localAnchorA);

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		/// <summary>
		/// Gets the local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA => this.localAnchorA;

		/// <summary>
		/// Gets the local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB => this.localAnchorB;

		/// <summary>
		/// Gets the local joint axis relative to bodyA.
		/// </summary>
		public Vector2 LocalAxisA => this.localXAxisA;

		/// <summary>
		/// Gets the reference angle.
		/// </summary>
		public float ReferenceAngle => this.referenceAngle;

		/// <summary>
		/// Gets or sets a value that determines whether the joint limit is enabled.
		/// </summary>
		public bool LimitEnabled
		{
			get => this.limitEnabled;
			set
			{
				if (value != this.limitEnabled)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;
					this.limitEnabled = value;
					this.impulse.Z = 0.0f;
				}
			}
		}

		/// <summary>
		/// Gets or sets the lower limit.
		/// </summary>
		public float LowerLimit
		{
			get => this.lowerTranslation;
			set => this.SetLimits(value, this.upperTranslation);
		}

		/// <summary>
		/// Gets or sets the upper limit.
		/// </summary>
		public float UpperLimit
		{
			get => this.upperTranslation;
			set => this.SetLimits(this.lowerTranslation, value);
		}

		/// <summary>
		/// Gets or sets a value that determines whether the motor is enabled.
		/// </summary>
		public bool MotorEnabled
		{
			get => this.motorEnabled;
			set
			{
				if (value != this.motorEnabled)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;
					this.motorEnabled = value;	
				}
			}
		}

		/// <summary>
		/// Gets or sets the motor speed in m/s.
		/// </summary>
		public float MotorSpeed
		{
			get => this.motorSpeed;
			set
			{
				if (value != this.motorSpeed)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;
					this.motorSpeed = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the maximum motor force in N.
		/// </summary>
		public float MaxMotorForce
		{
			get => this.maxMotorForce;
			set
			{
				if (value != this.maxMotorForce)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;
					this.maxMotorForce = value;
				}
			}
		}

		#endregion

		#region Constructors

		public PrismaticJoint(PrismaticJointDescriptor def)
			: base(def)
		{
			this.localAnchorA = def.LocalAnchorA;
			this.localAnchorB = def.LocalAnchorB;
			Vector2.Normalize(def.LocalAxisA, out this.localXAxisA);
			Vector2 b;
			b.X = -this.localXAxisA.Y;
			b.Y = this.localXAxisA.X;
			this.localYAxisA = b; // b2Math.b2Cross(1.0f, localXAxisA);
			this.referenceAngle = def.ReferenceAngle;

			this.impulse = Vector3.Zero;
			this.motorMass = 0.0f;
			this.motorImpulse = 0.0f;

			this.limitEnabled = def.LimitEnabled;
			this.limitState = LimitState.Inactive;
			this.lowerTranslation = def.LowerLimit;
			this.upperTranslation = def.UpperLimit;

			this.maxMotorForce = def.MaxMotorForce;
			this.motorSpeed = def.MotorSpeed;
			this.motorEnabled = def.MotorEnabled;

			this.axis = Vector2.Zero;
			this.perp = Vector2.Zero;
		}
		
		#endregion

		#region Methods

		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterA = this.bodyA.sweep.LocalCenter;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassA = this.bodyA.invMass;
			this.invMassB = this.bodyB.invMass;
			this.invIA = this.bodyA.invInertia;
			this.invIB = this.bodyB.invInertia;

			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			// Compute the effective masses.
			Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);
			Vector2 d = (cB - cA) + rB - rA;

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			// Compute motor Jacobian and effective mass.
			{
				this.axis = Rotation.Multiply(qA, this.localXAxisA);
				this.a1 = Vector2.Cross(d + rA, in this.axis);
				this.a2 = Vector2.Cross(in rB, in this.axis);

				this.motorMass = mA + mB + iA * this.a1 * this.a1 + iB * this.a2 * this.a2;
				if (this.motorMass > 0.0f)
				{
					this.motorMass = 1.0f / this.motorMass;
				}
			}

			// Prismatic constraint.
			{
				this.perp = Rotation.Multiply(qA, this.localYAxisA);

				this.s1 = Vector2.Cross(d + rA, this.perp);
				this.s2 = Vector2.Cross(rB, this.perp);

				float k11 = mA + mB + iA * this.s1 * this.s1 + iB * this.s2 * this.s2;
				float k12 = iA * this.s1 + iB * this.s2;
				float k13 = iA * this.s1 * this.a1 + iB * this.s2 * this.a2;
				float k22 = iA + iB;
				if (k22 == 0.0f)
				{
					// For bodies with fixed rotation.
					k22 = 1.0f;
				}
				float k23 = iA * this.a1 + iB * this.a2;
				float k33 = mA + mB + iA * this.a1 * this.a1 + iB * this.a2 * this.a2;

				this.K.Column1 = new Vector3(k11, k12, k13);
				this.K.Column2 = new Vector3(k12, k22, k23);
				this.K.Column3 = new Vector3(k13, k23, k33);
			}

			// Compute motor and limit terms.
			if (this.limitEnabled)
			{
				float jointTranslation = Vector2.Dot(this.axis, d);
				if (System.Math.Abs(this.upperTranslation - this.lowerTranslation) < 2.0f * Settings.LinearSlop)
				{
					this.limitState = LimitState.EqualLimits;
				}
				else if (jointTranslation <= this.lowerTranslation)
				{
					if (this.limitState != LimitState.AtLowerLimit)
					{
						this.limitState = LimitState.AtLowerLimit;
						this.impulse.Z = 0.0f;
					}
				}
				else if (jointTranslation >= this.upperTranslation)
				{
					if (this.limitState != LimitState.AtUpperLimit)
					{
						this.limitState = LimitState.AtUpperLimit;
						this.impulse.Z = 0.0f;
					}
				}
				else
				{
					this.limitState = LimitState.Inactive;
					this.impulse.Z = 0.0f;
				}
			}
			else
			{
				this.limitState = LimitState.Inactive;
				this.impulse.Z = 0.0f;
			}

			if (this.motorEnabled == false)
			{
				this.motorImpulse = 0.0f;
			}

			if (data.timeStep.WarmStarting)
			{
				// Account for variable time step.
				this.impulse *= data.timeStep.DeltaTRatio;
				this.motorImpulse *= data.timeStep.DeltaTRatio;

				Vector2 P = this.impulse.X * this.perp + (this.motorImpulse + this.impulse.Z) * this.axis;
				float LA = this.impulse.X * this.s1 + this.impulse.Y + (this.motorImpulse + this.impulse.Z) * this.a1;
				float LB = this.impulse.X * this.s2 + this.impulse.Y + (this.motorImpulse + this.impulse.Z) * this.a2;
			
				vA -= mA * P;
				wA -= iA * LA;

				vB += mB * P;
				wB += iB * LB;
			}
			else
			{
				this.impulse = Vector3.Zero;
				this.motorImpulse = 0.0f;
			}
		
			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			// Solve linear motor constraint.
			if (this.motorEnabled && this.limitState != LimitState.EqualLimits)
			{
				float Cdot = Vector2.Dot(this.axis, vB - vA) + this.a2 * wB - this.a1 * wA;
				float impulse = this.motorMass * (this.motorSpeed - Cdot);
				float oldImpulse = this.motorImpulse;
				float maxImpulse = data.timeStep.DeltaT * this.maxMotorForce;
				this.motorImpulse = Utility.Clamp(this.motorImpulse + impulse, -maxImpulse, maxImpulse);
				impulse = this.motorImpulse - oldImpulse;

				Vector2 P = impulse * this.axis;
				float LA = impulse * this.a1;
				float LB = impulse * this.a2;

				vA -= mA * P;
				wA -= iA * LA;

				vB += mB * P;
				wB += iB * LB;
			}

			Vector2 Cdot1;
			Cdot1.X = -(Vector2.Dot(this.perp, vB - vA) + this.s2 * wB - this.s1 * wA);
			Cdot1.Y = wA - wB;

			if (this.limitEnabled && this.limitState != LimitState.Inactive)
			{
				// Solve prismatic and limit constraint in block form.
				Vector3 Cdot;
				Cdot.X = Cdot1.X;
				Cdot.Y = Cdot1.Y;
				Cdot.Z = -(Vector2.Dot(this.axis, vB - vA) + this.a2 * wB - this.a1 * wA);

				Vector3 f1 = this.impulse;
				Matrix3x3.Solve(ref this.K, ref Cdot, out Vector3 df);

				this.impulse += df;

				if (this.limitState == LimitState.AtLowerLimit)
					this.impulse.Z = System.Math.Max(this.impulse.Z, 0.0f);
				else if (this.limitState == LimitState.AtUpperLimit)
					this.impulse.Z = System.Math.Min(this.impulse.Z, 0.0f);

				// f2(1:2) = invK(1:2,1:2) * (-Cdot(1:2) - K(1:2,3) * (f2(3) - f1(3))) + f1(1:2)
				Vector2 b = Cdot1 - (this.impulse.Z - f1.Z) * (new Vector2(this.K.M13, this.K.M23));
				Matrix3x3.Solve(ref this.K, ref b, out Vector2 f2r);
				this.impulse.X = f2r.X + f1.X;
				this.impulse.Y = f2r.Y + f1.Y;

				df = this.impulse - f1;

				Vector2 P = df.X * this.perp + df.Z * this.axis;
				float LA = df.X * this.s1 + df.Y + df.Z * this.a1;
				float LB = df.X * this.s2 + df.Y + df.Z * this.a2;

				vA -= mA * P;
				wA -= iA * LA;

				vB += mB * P;
				wB += iB * LB;
			}
			else
			{
				// Limit is inactive, just solve the prismatic constraint in block form.
				Matrix3x3.Solve(ref this.K, ref Cdot1, out Vector2 df);
				this.impulse.X += df.X;
				this.impulse.Y += df.Y;

				Vector2 P = df.X * this.perp;
				float LA = df.X * this.s1 + df.Y;
				float LB = df.X * this.s2 + df.Y;

				vA -= mA * P;
				wA -= iA * LA;

				vB += mB * P;
				wB += iB * LB;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			// Compute fresh Jacobians
			Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);
			Vector2 d = cB + rB - cA - rA;

			Vector2 axis = Rotation.Multiply(qA, this.localXAxisA);
			float a1 = Vector2.Cross(d + rA, axis);
			float a2 = Vector2.Cross(rB, axis);
			Vector2 perp = Rotation.Multiply(qA, this.localYAxisA);

			float s1 = Vector2.Cross(d + rA, perp);
			float s2 = Vector2.Cross(rB, perp);

			Vector3 impulse;
			Vector2 C1;
			C1.X = Vector2.Dot(perp, d);
			C1.Y = aB - aA - this.referenceAngle;

			float linearError = System.Math.Abs(C1.X);
			float angularError = System.Math.Abs(C1.Y);

			bool active = false;
			float C2 = 0.0f;
			if (this.limitEnabled)
			{
				float translation = Vector2.Dot(axis, d);
				if (System.Math.Abs(this.upperTranslation - this.lowerTranslation) < 2.0f * Settings.LinearSlop)
				{
					// Prevent large angular corrections
					C2 = Utility.Clamp(translation, -Settings.MaxLinearCorrection, Settings.MaxLinearCorrection);
					linearError = System.Math.Max(linearError, System.Math.Abs(translation));
					active = true;
				}
				else if (translation <= this.lowerTranslation)
				{
					// Prevent large linear corrections and allow some slop.
					C2 = Utility.Clamp(translation - this.lowerTranslation + Settings.LinearSlop, -Settings.MaxLinearCorrection, 0.0f);
					linearError = System.Math.Max(linearError, this.lowerTranslation - translation);
					active = true;
				}
				else if (translation >= this.upperTranslation)
				{
					// Prevent large linear corrections and allow some slop.
					C2 = Utility.Clamp(translation - this.upperTranslation - Settings.LinearSlop, 0.0f, Settings.MaxLinearCorrection);
					linearError = System.Math.Max(linearError, translation - this.upperTranslation);
					active = true;
				}
			}

			if (active)
			{
				float k11 = mA + mB + iA * s1 * s1 + iB * s2 * s2;
				float k12 = iA * s1 + iB * s2;
				float k13 = iA * s1 * a1 + iB * s2 * a2;
				float k22 = iA + iB;
				if (k22 == 0.0f)
				{
					// For fixed rotation
					k22 = 1.0f;
				}
				float k23 = iA * a1 + iB * a2;
				float k33 = mA + mB + iA * a1 * a1 + iB * a2 * a2;

				Matrix3x3 K;
				K.M11 = k11;
				K.M12 = K.M21 = k12;
				K.M13 = K.M31 = k13;
				K.M22 = k22;
				K.M23 = K.M32 = k23;
				K.M33 = k33;
				
				Vector3 C;
				C.X = C1.X;
				C.Y = C1.Y;
				C.Z = C2;

				impulse = Matrix3x3.Solve(K, -C);
			}
			else
			{
				float k11 = mA + mB + iA * s1 * s1 + iB * s2 * s2;
				float k12 = iA * s1 + iB * s2;
				float k22 = iA + iB;
				if (k22 == 0.0f)
					k22 = 1.0f;

				Matrix2x2 K;
				K.M11 = k11;
				K.M12 = K.M21 = k12;
				K.M22 = k22;

				Vector2 impulse1 = Matrix2x2.Solve(K, -C1);
				impulse.X = impulse1.X;
				impulse.Y = impulse1.Y;
				impulse.Z = 0.0f;
			}

			Vector2 P = impulse.X * perp + impulse.Z * axis;
			float LA = impulse.X * s1 + impulse.Y + impulse.Z * a1;
			float LB = impulse.X * s2 + impulse.Y + impulse.Z * a2;

			cA -= mA * P;
			aA -= iA * LA;
			cB += mB * P;
			aB += iB * LB;

			data.states[this.indexA].Position = cA;
			data.states[this.indexA].Angle = aA;
			data.states[this.indexB].Position = cB;
			data.states[this.indexB].Angle = aB;

			return linearError <= Settings.LinearSlop && angularError <= Settings.AngularSlop;
		}

		public override Vector2 GetReactionForce(float inv_dt)
		{
			return inv_dt * (this.impulse.X * this.perp + (this.motorImpulse + this.impulse.Z) * this.axis);
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return inv_dt * this.impulse.Y;
		}

		public float GetMotorForce(float inv_dt)
		{
			return inv_dt * this.motorImpulse;
		}
	
		public float GetJointTranslation()
		{
			Vector2 pA = this.bodyA.GetWorldPoint(this.localAnchorA);
			Vector2 pB = this.bodyB.GetWorldPoint(this.localAnchorB);
			Vector2 d = pB - pA;
			Vector2 axis = this.bodyA.GetWorldVector(this.localXAxisA);

			float translation = Vector2.Dot(d, axis);
			return translation;
		}

		public float GetJointSpeed()
		{
			Body bA = this.bodyA;
			Body bB = this.bodyB;

			Vector2 rA = Rotation.Multiply(bA.Transform.Q, this.localAnchorA - bA.sweep.LocalCenter);
			Vector2 rB = Rotation.Multiply(bB.Transform.Q, this.localAnchorB - bB.sweep.LocalCenter);
			Vector2 p1 = bA.sweep.Center + rA;
			Vector2 p2 = bB.sweep.Center + rB;
			Vector2 d = p2 - p1;
			Vector2 axis = Rotation.Multiply(bA.Transform.Q, this.localXAxisA);

			Vector2 vA = bA.LinearVelocity;
			Vector2 vB = bB.LinearVelocity;
			float wA = bA.AngularVelocity;
			float wB = bB.AngularVelocity;

			float speed = Vector2.Dot(d, Vector2.Cross(wA, in axis)) + Vector2.Dot(axis, vB + Vector2.Cross(wB, rB) - vA - Vector2.Cross(wA, in rA));
			return speed;
		}
		
		/// <summary>
		/// Sets the joint's limits.
		/// </summary>
		/// <param name="lower"></param>
		/// <param name="upper"></param>
		public void SetLimits(float lower, float upper)
		{
			if (lower > upper)
				throw new ArgumentException("The lower limit must not be greater than the upper limit.");

			if (lower != this.lowerTranslation ||
				upper != this.upperTranslation)
			{
				this.bodyA.Awake = true;
				this.bodyB.Awake = true;
				this.lowerTranslation = lower;
				this.upperTranslation = upper;
				this.impulse.Z = 0.0f;
			}
		}

		#endregion
	}
}