using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// Revolute joint definition. This requires defining an
	/// anchor point where the bodies are joined. The definition
	/// uses local anchor points so that the initial configuration
	/// can violate the constraint slightly. You also need to
	/// specify the initial relative angle for joint limits. This
	/// helps when saving and loading a game.
	/// The local anchor points are measured from the body's origin
	/// rather than the center of mass because:
	/// 1. you might not know where the center of mass will be.
	/// 2. if you add/remove shapes from a body and recompute the mass,
	///    the joints will be broken.
	/// </summary>
	public sealed class RevoluteJointDescriptor : JointDescriptor
	{
		#region Properties

		/// <summary>
		/// The local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA { get; set; }

		/// <summary>
		/// The local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB { get; set; }

		/// <summary>
		/// The bodyB angle minus bodyA angle in the reference state (radians).
		/// </summary>
		public float ReferenceAngle { get; set; }

		/// <summary>
		/// A flag to enable joint limits.
		/// </summary>
		public bool LimitEnabled { get; set; }

		/// <summary>
		/// The lower angle for the joint limit (radians).
		/// </summary>
		public float LowerLimit { get; set; }

		/// <summary>
		/// The upper angle for the joint limit (radians).
		/// </summary>
		public float UpperLimit { get; set; }

		/// <summary>
		/// A flag to enable the joint motor.
		/// </summary>
		public bool MotorEnabled { get; set; }

		/// <summary>
		/// The desired motor speed. Usually in radians per second.
		/// </summary>
		public float MotorSpeed { get; set; }

		/// <summary>
		/// The maximum motor torque used to achieve the desired motor speed.
		/// Usually in N-m.
		/// </summary>
		public float MaxMotorTorque { get; set; }

		#endregion

		#region Constructors

		public RevoluteJointDescriptor()
		{
			this.LocalAnchorA = Vector2.Zero;
			this.LocalAnchorB = Vector2.Zero;
			this.ReferenceAngle = 0.0f;
			this.LowerLimit = 0.0f;
			this.UpperLimit = 0.0f;
			this.MaxMotorTorque = 0.0f;
			this.MotorSpeed = 0.0f;
			this.LimitEnabled = false;
			this.MotorEnabled = false;
		}

		/// <summary>
		/// Initialize the bodies, anchors, and reference angle using a world
		/// anchor point.
		/// </summary>
		/// <param name="bA"></param>
		/// <param name="bB"></param>
		/// <param name="anchor"></param>
		public RevoluteJointDescriptor(Body bA, Body bB, Vector2 anchor)
			: this()
		{
			this.BodyA = bA;
			this.BodyB = bB;
			this.LocalAnchorA = this.BodyA.GetLocalPoint(anchor);
			this.LocalAnchorB = this.BodyB.GetLocalPoint(anchor);
			this.ReferenceAngle = this.BodyB.Angle - this.BodyA.Angle;
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new RevoluteJoint(this);
		}

		#endregion
	}
}
