﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// Motor joint definition.
	/// </summary>
	public sealed class MotorJointDescriptor : JointDescriptor
	{
		#region Properties

		/// <summary>
		/// Position of bodyB minus the position of bodyA, in bodyA's frame, in meters.
		/// </summary>
		public Vector2 LinearOffset { get; set; }

		/// <summary>
		/// The bodyB angle minus bodyA angle in radians.
		/// </summary>
		public float AngularOffset { get; set; }

		/// <summary>
		/// The maximum motor force in N.
		/// </summary>
		public float MaxForce { get; set; }

		/// <summary>
		/// The maximum motor torque in N-m.
		/// </summary>
		public float MaxTorque { get; set; }

		/// <summary>
		/// Position correction factor in the range [0,1].
		/// </summary>
		public float CorrectionFactor { get; set; }

		#endregion

		#region Constructors

		public MotorJointDescriptor()
		{
			this.LinearOffset = Vector2.Zero;
			this.AngularOffset = 0.0f;
			this.MaxForce = 1.0f;
			this.MaxTorque = 1.0f;
			this.CorrectionFactor = 0.3f;
		}

		/// <summary>
		/// Initialize the bodies and offsets using the current transforms.
		/// </summary>
		/// <param name="bA"></param>
		/// <param name="bB"></param>
		public MotorJointDescriptor(Body bA, Body bB)
			: this()
		{
			this.BodyA = bA;
			this.BodyB = bB;
			Vector2 xB = this.BodyB.Position;
			this.LinearOffset = this.BodyA.GetLocalPoint(xB);

			float angleA = this.BodyA.Angle;
			float angleB = this.BodyB.Angle;
			this.AngularOffset = angleB - angleA;
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new MotorJoint(this);
		}

		#endregion
	}
}
