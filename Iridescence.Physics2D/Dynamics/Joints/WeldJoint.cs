/*
* Copyright (c) 2006-2011 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

// Point-to-point constraint
// C = p2 - p1
// Cdot = v2 - v1
//      = v2 + cross(w2, r2) - v1 - cross(w1, r1)
// J = [-I -r1_skew I r2_skew ]
// Identity used:
// w k % (rx i + ry j) = w * (-ry i + rx j)

// Angle constraint
// C = angle2 - angle1 - referenceAngle
// Cdot = w2 - w1
// J = [0 0 -1 0 0 1]
// K = invI1 + invI2

using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// A weld joint essentially glues two bodies together. A weld joint may
	/// distort somewhat because the island constraint solver is approximate.
	/// </summary>
	public sealed class WeldJoint : Joint
	{
		#region Fields

		private float frequency;
		private float dampingRatio;
		private float bias;

		// Solver shared
		private Vector2 localAnchorA;
		private Vector2 localAnchorB;
		private float referenceAngle;
		private float gamma;
		private Vector3 impulse;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 rA;
		private Vector2 rB;
		private Vector2 localCenterA;
		private Vector2 localCenterB;
		private float invMassA;
		private float invMassB;
		private float invIA;
		private float invIB;
		private Matrix3x3 mass;
		
		#endregion

		#region Properties

		public override Vector2 AnchorA => this.bodyA.GetWorldPoint(this.localAnchorA);

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		/// <summary>
		/// The local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA
		{
			get => this.localAnchorA;
			set => this.localAnchorA = value;
		}

		/// <summary>
		/// The local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB
		{
			get => this.localAnchorB;
			set => this.localAnchorB = value;
		}

		/// <summary>
		/// Gets or sets the reference angle.
		/// </summary>
		public float ReferenceAngle
		{
			get => this.referenceAngle;
			set => this.referenceAngle = value;
		}

		/// <summary>
		/// Gets or sets the frequency in Hertz.
		/// </summary>
		public float Frequency
		{
			get => this.frequency;
			set => this.frequency = value;
		}

		/// <summary>
		/// Gets or sets the damping ratio.
		/// </summary>
		public float DampingRatio
		{
			get => this.dampingRatio;
			set => this.dampingRatio = value;
		}

		#endregion

		#region Constructors
		
		public WeldJoint(WeldJointDescriptor def)
			: base(def)
		{
			this.localAnchorA = def.LocalAnchorA;
			this.localAnchorB = def.LocalAnchorB;
			this.referenceAngle = def.ReferenceAngle;
			this.frequency = def.Frequency;
			this.dampingRatio = def.DampingRatio;

			this.impulse = Vector3.Zero;
		}
		
		#endregion

		#region Methods
		
		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterA = this.bodyA.sweep.LocalCenter;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassA = this.bodyA.invMass;
			this.invMassB = this.bodyB.invMass;
			this.invIA = this.bodyA.invInertia;
			this.invIB = this.bodyB.invInertia;

			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			this.rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			this.rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);

			// J = [-I -r1_skew I r2_skew]
			//     [ 0       -1 0       1]
			// r_skew = [-ry; rx]

			// Matlab
			// K = [ mA+r1y^2*iA+mB+r2y^2*iB,  -r1y*iA*r1x-r2y*iB*r2x,          -r1y*iA-r2y*iB]
			//     [  -r1y*iA*r1x-r2y*iB*r2x, mA+r1x^2*iA+mB+r2x^2*iB,           r1x*iA+r2x*iB]
			//     [          -r1y*iA-r2y*iB,           r1x*iA+r2x*iB,                   iA+iB]

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			Matrix3x3 K;
			K.M11 = mA + mB + this.rA.Y * this.rA.Y * iA + this.rB.Y * this.rB.Y * iB;
			K.M12 = -this.rA.Y * this.rA.X * iA - this.rB.Y * this.rB.X * iB;
			K.M13 = -this.rA.Y * iA - this.rB.Y * iB;
			K.M21 = K.M12;
			K.M22 = mA + mB + this.rA.X * this.rA.X * iA + this.rB.X * this.rB.X * iB;
			K.M23 = this.rA.X * iA + this.rB.X * iB;
			K.M31 = K.M13;
			K.M32 = K.M23;
			K.M33 = iA + iB;
			
			if (this.frequency > 0.0f)
			{
				MathHelper.GetInverse2x2(ref K, out this.mass);

				float invM = iA + iB;
				float m = invM > 0.0f ? 1.0f / invM : 0.0f;

				float C = aB - aA - this.referenceAngle;

				// Frequency
				float omega = 2.0f * Constants.Pi * this.frequency;

				// Damping coefficient
				float d = 2.0f * m * this.dampingRatio * omega;

				// Spring stiffness
				float k = m * omega * omega;

				// magic formulas
				float h = data.timeStep.DeltaT;
				this.gamma = h * (d + h * k);
				this.gamma = this.gamma != 0.0f ? 1.0f / this.gamma : 0.0f;
				this.bias = C * h * k * this.gamma;

				invM += this.gamma;
				this.mass.M33 = invM != 0.0f ? 1.0f / invM : 0.0f;
			}
			else if (K.M33 == 0.0f)
			{
				MathHelper.GetInverse2x2(ref K, out this.mass);
				this.gamma = 0.0f;
				this.bias = 0.0f;
			}
			else
			{
				MathHelper.GetSymInverse3x3(ref K, out this.mass);
				this.gamma = 0.0f;
				this.bias = 0.0f;
			}

			if (data.timeStep.WarmStarting)
			{
				// Scale impulses to support a variable time step.
				this.impulse *= data.timeStep.DeltaTRatio;

				Vector2 P = new Vector2(this.impulse.X, this.impulse.Y);

				vA -= mA * P;
				wA -= iA * (Vector2.Cross(in this.rA, in P) + this.impulse.Z);

				vB += mB * P;
				wB += iB * (Vector2.Cross(in this.rB, in P) + this.impulse.Z);
			}
			else
			{
				this.impulse = Vector3.Zero;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			if (this.frequency > 0.0f)
			{
				float Cdot2 = wB - wA;

				float impulse2 = -this.mass.M33 * (Cdot2 + this.bias + this.gamma * this.impulse.Z);
				this.impulse.Z += impulse2;

				wA -= iA * impulse2;
				wB += iB * impulse2;

				Vector2 Cdot1 = vB + Vector2.Cross(wB, in this.rB) - vA - Vector2.Cross(wA, in this.rA);

				Vector2 P;
				P.X = -this.mass.M11 * Cdot1.X - this.mass.M21 * Cdot1.Y;
				P.Y = -this.mass.M12 * Cdot1.X - this.mass.M22 * Cdot1.Y;

				this.impulse.X += P.X;
				this.impulse.Y += P.Y;

				vA -= mA * P;
				wA -= iA * Vector2.Cross(in this.rA, in P);

				vB += mB * P;
				wB += iB * Vector2.Cross(in this.rB, in P);
			}
			else
			{
				Vector2 Cdot1 = vB + Vector2.Cross(wB, in this.rB) - vA - Vector2.Cross(wA, in this.rA);
				float Cdot2 = wB - wA;
				Vector3 Cdot = new Vector3(Cdot1.X, Cdot1.Y, Cdot2);

				Matrix3x3.Multiply(this.mass, Cdot, out Vector3 impulse);
				Vector3.Negate(impulse, out impulse);

				this.impulse += impulse;

				Vector2 P = new Vector2(impulse.X, impulse.Y);

				vA -= mA * P;
				wA -= iA * (Vector2.Cross(in this.rA, in P) + impulse.Z);

				vB += mB * P;
				wB += iB * (Vector2.Cross(in this.rB, in P) + impulse.Z);
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);

			float positionError, angularError;

			Matrix3x3 K;
			K.M11 = mA + mB + rA.Y * rA.Y * iA + rB.Y * rB.Y * iB;
			K.M12 = -rA.Y * rA.X * iA - rB.Y * rB.X * iB;
			K.M13 = -rA.Y * iA - rB.Y * iB;
			K.M21 = K.M12;
			K.M22 = mA + mB + rA.X * rA.X * iA + rB.X * rB.X * iB;
			K.M23 = rA.X * iA + rB.X * iB;
			K.M31 = K.M13;
			K.M32 = K.M23;
			K.M33 = iA + iB;

			if (this.frequency > 0.0f)
			{
				Vector2 C1 = cB + rB - cA - rA;

				positionError = C1.Length;
				angularError = 0.0f;

				Matrix3x3.Solve(ref K, ref C1, out Vector2 P);
				Vector2.Negate(P, out P);

				cA -= mA * P;
				aA -= iA * Vector2.Cross(in rA, in P);

				cB += mB * P;
				aB += iB * Vector2.Cross(in rB, in P);
			}
			else
			{
				Vector2 C1 = cB + rB - cA - rA;
				float C2 = aB - aA - this.referenceAngle;

				positionError = C1.Length;
				angularError = System.Math.Abs(C2);

				Vector3 C = new Vector3(C1.X, C1.Y, C2);

				Vector3 impulse;
				if (K.M33 > 0.0f)
				{
					Matrix3x3.Solve(ref K, ref C, out impulse);
					Vector3.Negate(impulse, out impulse);
				}
				else
				{
					Matrix3x3.Solve(ref K, ref C1, out Vector2 impulse2);
					impulse.X = -impulse2.X;
					impulse.Y = -impulse2.Y;
					impulse.Z = 0.0f;
				} 
				
				Vector2 P = new Vector2(impulse.X, impulse.Y);

				cA -= mA * P;
				aA -= iA * (Vector2.Cross(in rA, in P) + impulse.Z);

				cB += mB * P;
				aB += iB * (Vector2.Cross(in rB, in P) + impulse.Z);
			}

			data.states[this.indexA].Position = cA;
			data.states[this.indexA].Angle = aA;
			data.states[this.indexB].Position = cB;
			data.states[this.indexB].Angle = aB;

			return positionError <= Settings.LinearSlop && angularError <= Settings.AngularSlop;
		}

		public override Vector2 GetReactionForce(float inv_dt)
		{
			Vector2 P = new Vector2(this.impulse.X, this.impulse.Y);
			return inv_dt * P;
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return inv_dt * this.impulse.Z;
		}
		
		#endregion
	}
}
