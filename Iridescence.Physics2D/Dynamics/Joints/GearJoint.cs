/*
* Copyright (c) 2007-2011 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/
// Gear Joint:
// C0 = (coordinate1 + ratio * coordinate2)_initial
// C = (coordinate1 + ratio * coordinate2) - C0 = 0
// J = [J1 ratio * J2]
// K = J * invM * JT
//   = J1 * invM1 * J1T + ratio * ratio * J2 * invM2 * J2T
//
// Revolute:
// coordinate = rotation
// Cdot = angularVelocity
// J = [0 0 1]
// K = J * invM * JT = invI
//
// Prismatic:
// coordinate = dot(p - pg, ug)
// Cdot = dot(v + cross(w, r), ug)
// J = [ug cross(r, ug)]
// K = J * invM * JT = invMass + invI * cross(r, ug)^2

using System;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// A gear joint is used to connect two joints together. Either joint
	/// can be a revolute or prismatic joint. You specify a gear ratio
	/// to bind the motions together:
	/// coordinate1 + ratio * coordinate2 = constant
	/// The ratio can be negative or positive. If one joint is a revolute joint
	/// and the other joint is a prismatic joint, then the ratio will have units
	/// of length or units of 1/length.
	/// </summary>
	/// <remarks>
	/// You have to manually destroy the gear joint if joint1 or joint2 is destroyed.
	/// </remarks>
	public sealed class GearJoint : Joint
	{
		#region Fields

		private readonly Joint jointA;
		private readonly Joint jointB;

		// Body A is connected to body C
		// Body B is connected to body D
		private readonly Body bodyC;
		private readonly Body bodyD;

		// Solver shared
		private readonly Vector2 localAnchorA;
		private readonly Vector2 localAnchorB;
		private readonly Vector2 localAnchorC;
		private readonly Vector2 localAnchorD;

		private Vector2 localAxisC;
		private Vector2 localAxisD;

		private readonly float referenceAngleA;
		private readonly float referenceAngleB;

		private readonly float constant;
		private float ratio;

		private float impulse;

		// Solver temp
		private int indexA;
		private int indexB;
		private int indexC;
		private int indexD;
		private Vector2 lcA, lcB, lcC, lcD;
		private float mA, mB, mC, mD;
		private float iA, iB, iC, iD;
		private Vector2 JvAC, JvBD;
		private float JwA, JwB, JwC, JwD;
		private float mass;
		
		#endregion

		#region Properties
	
		public override Vector2 AnchorA => this.bodyA.GetWorldPoint(this.localAnchorA);

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		/// <summary>
		/// Gets the first joint.
		/// </summary>
		public Joint JointA => this.jointA;

		/// <summary>
		/// Gets the second joint.
		/// </summary>
		public Joint JointB => this.jointB;

		public float Ratio
		{
			get => this.ratio;
			set => this.ratio = value;
		}

		#endregion

		#region Constructors

		public GearJoint(GearJointDescriptor def)
			: base(def)
		{
			this.jointA = def.JointA;
			this.jointB = def.JointB;

			if (!(this.jointA is RevoluteJoint || this.jointA is PrismaticJoint))
				throw new ArgumentException("Invalid joints. Expected RevoluteJoint or PrismaticJoint");

			if (!(this.jointB is RevoluteJoint || this.jointB is PrismaticJoint))
				throw new ArgumentException("Invalid joints. Expected RevoluteJoint or PrismaticJoint");

			float coordinateA, coordinateB;

			// TODO_ERIN there might be some problem with the joint edges in b2Joint.

			this.bodyC = this.jointA.bodyA;
			this.bodyA = this.jointA.bodyB;

			// Get geometry of joint1
			Transform xfA = this.bodyA.Transform;
			float aA = this.bodyA.sweep.Angle;
			Transform xfC = this.bodyC.Transform;
			float aC = this.bodyC.sweep.Angle;

			if (this.jointA is RevoluteJoint)
			{
				RevoluteJoint revolute = (RevoluteJoint)def.JointA;
				this.localAnchorC = revolute.LocalAnchorA;
				this.localAnchorA = revolute.LocalAnchorB;
				this.referenceAngleA = revolute.ReferenceAngle;
				this.localAxisC = Vector2.Zero;

				coordinateA = aA - aC - this.referenceAngleA;
			}
			else
			{
				PrismaticJoint prismatic = (PrismaticJoint)def.JointA;
				this.localAnchorC = prismatic.LocalAnchorA;
				this.localAnchorA = prismatic.LocalAnchorB;
				this.referenceAngleA = prismatic.ReferenceAngle;
				this.localAxisC = prismatic.LocalAxisA;

				Vector2 pC = this.localAnchorC;
				Rotation.Multiply(in xfA.Q, in this.localAnchorA, out Vector2 pA);
				Vector2.Add(pA, xfA.P, out pA);
				Vector2.Subtract(pA, xfC.P, out pA);
				Rotation.MultiplyTransposed(in xfC.Q, in pA, out pA);
				coordinateA = Vector2.Dot(pA - pC, this.localAxisC);
			}

			this.bodyD = this.jointB.BodyA;
			this.bodyB = this.jointB.BodyB;

			// Get geometry of joint2
			Transform xfB = this.bodyB.Transform;
			float aB = this.bodyB.sweep.Angle;
			Transform xfD = this.bodyD.Transform;
			float aD = this.bodyD.sweep.Angle;

			if (this.jointB is RevoluteJoint)
			{
				RevoluteJoint revolute = (RevoluteJoint)def.JointB;
				this.localAnchorD = revolute.LocalAnchorA;
				this.localAnchorB = revolute.LocalAnchorB;
				this.referenceAngleB = revolute.ReferenceAngle;
				this.localAxisD = Vector2.Zero;

				coordinateB = aB - aD - this.referenceAngleB;
			}
			else
			{
				PrismaticJoint prismatic = (PrismaticJoint)def.JointB;
				this.localAnchorD = prismatic.LocalAnchorA;
				this.localAnchorB = prismatic.LocalAnchorB;
				this.referenceAngleB = prismatic.ReferenceAngle;
				this.localAxisD = prismatic.LocalAxisA;

				Vector2 pD = this.localAnchorD;
				Rotation.Multiply(in xfB.Q, in this.localAnchorB, out Vector2 pB);
				Vector2.Add(pB, xfB.P, out pB);
				Vector2.Subtract(pB, xfD.P, out pB);
				Rotation.MultiplyTransposed(in xfD.Q, in pB, out pB);
				coordinateB = Vector2.Dot(pB - pD, this.localAxisD);
			}

			this.ratio = def.Ratio;

			this.constant = coordinateA + this.ratio * coordinateB;

			this.impulse = 0.0f;
		}

		#endregion

		#region Methods

		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.indexC = this.bodyC.islandIndex;
			this.indexD = this.bodyD.islandIndex;
			this.lcA = this.bodyA.sweep.LocalCenter;
			this.lcB = this.bodyB.sweep.LocalCenter;
			this.lcC = this.bodyC.sweep.LocalCenter;
			this.lcD = this.bodyD.sweep.LocalCenter;
			this.mA = this.bodyA.invMass;
			this.mB = this.bodyB.invMass;
			this.mC = this.bodyC.invMass;
			this.mD = this.bodyD.invMass;
			this.iA = this.bodyA.invInertia;
			this.iB = this.bodyB.invInertia;
			this.iC = this.bodyC.invInertia;
			this.iD = this.bodyD.invInertia;

			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			float aC = data.states[this.indexC].Angle;
			Vector2 vC = data.states[this.indexC].LinearVelocity;
			float wC = data.states[this.indexC].AngularVelocity;

			float aD = data.states[this.indexD].Angle;
			Vector2 vD = data.states[this.indexD].LinearVelocity;
			float wD = data.states[this.indexD].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);
			Rotation qC = new Rotation(aC);
			Rotation qD = new Rotation(aD);

			this.mass = 0.0f;

			if (this.jointA is RevoluteJoint)
			{
				this.JvAC = Vector2.Zero;
				this.JwA = 1.0f;
				this.JwC = 1.0f;
				this.mass += this.iA + this.iC;
			}
			else
			{
				Vector2 u = Rotation.Multiply(qC, this.localAxisC);
				Vector2 rC = Rotation.Multiply(qC, this.localAnchorC - this.lcC);
				Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.lcA);
				this.JvAC = u;
				this.JwC = Vector2.Cross(in rC, in u);
				this.JwA = Vector2.Cross(in rA, in u);
				this.mass += this.mC + this.mA + this.iC * this.JwC * this.JwC + this.iA * this.JwA * this.JwA;
			}

			if (this.jointB is RevoluteJoint)
			{
				this.JvBD = Vector2.Zero;
				this.JwB = this.ratio;
				this.JwD = this.ratio;
				this.mass += this.ratio * this.ratio * (this.iB + this.iD);
			}
			else
			{
				Vector2 u = Rotation.Multiply(qD, this.localAxisD);
				Vector2 rD = Rotation.Multiply(qD, this.localAnchorD - this.lcD);
				Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.lcB);
				this.JvBD = this.ratio * u;
				this.JwD = this.ratio * Vector2.Cross(in rD, in u);
				this.JwB = this.ratio * Vector2.Cross(in rB, in u);
				this.mass += this.ratio * this.ratio * (this.mD + this.mB) + this.iD * this.JwD * this.JwD + this.iB * this.JwB * this.JwB;
			}

			// Compute effective mass.
			this.mass = this.mass > 0.0f ? 1.0f / this.mass : 0.0f;

			if (data.timeStep.WarmStarting)
			{
				vA += (this.mA * this.impulse) * this.JvAC;
				wA += this.iA * this.impulse * this.JwA;
				vB += (this.mB * this.impulse) * this.JvBD;
				wB += this.iB * this.impulse * this.JwB;
				vC -= (this.mC * this.impulse) * this.JvAC;
				wC -= this.iC * this.impulse * this.JwC;
				vD -= (this.mD * this.impulse) * this.JvBD;
				wD -= this.iD * this.impulse * this.JwD;
			}
			else
			{
				this.impulse = 0.0f;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
			data.states[this.indexC].LinearVelocity = vC;
			data.states[this.indexC].AngularVelocity = wC;
			data.states[this.indexD].LinearVelocity = vD;
			data.states[this.indexD].AngularVelocity = wD;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;
			Vector2 vC = data.states[this.indexC].LinearVelocity;
			float wC = data.states[this.indexC].AngularVelocity;
			Vector2 vD = data.states[this.indexD].LinearVelocity;
			float wD = data.states[this.indexD].AngularVelocity;

			float Cdot = Vector2.Dot(this.JvAC, vA - vC) + Vector2.Dot(this.JvBD, vB - vD);
			Cdot += (this.JwA * wA - this.JwC * wC) + (this.JwB * wB - this.JwD * wD);

			float impulse = -this.mass * Cdot;
			this.impulse += impulse;

			vA += (this.mA * impulse) * this.JvAC;
			wA += this.iA * impulse * this.JwA;
			vB += (this.mB * impulse) * this.JvBD;
			wB += this.iB * impulse * this.JwB;
			vC -= (this.mC * impulse) * this.JvAC;
			wC -= this.iC * impulse * this.JwC;
			vD -= (this.mD * impulse) * this.JvBD;
			wD -= this.iD * impulse * this.JwD;

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
			data.states[this.indexC].LinearVelocity = vC;
			data.states[this.indexC].AngularVelocity = wC;
			data.states[this.indexD].LinearVelocity = vD;
			data.states[this.indexD].AngularVelocity = wD;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;
			Vector2 cC = data.states[this.indexC].Position;
			float aC = data.states[this.indexC].Angle;
			Vector2 cD = data.states[this.indexD].Position;
			float aD = data.states[this.indexD].Angle;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);
			Rotation qC = new Rotation(aC);
			Rotation qD = new Rotation(aD);

			float linearError = 0.0f;

			float coordinateA, coordinateB;

			Vector2 JvAC = Vector2.Zero, JvBD = Vector2.Zero;
			float JwA, JwB, JwC, JwD;
			float mass = 0.0f;

			if (this.jointA is RevoluteJoint)
			{
				JvAC = Vector2.Zero;
				JwA = 1.0f;
				JwC = 1.0f;
				mass += this.iA + this.iC;

				coordinateA = aA - aC - this.referenceAngleA;
			}
			else
			{
				Vector2 u = Rotation.Multiply(in qC, in this.localAxisC);
				Vector2 rC = Rotation.Multiply(qC, this.localAnchorC - this.lcC);
				Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.lcA);
				JvAC = u;
				JwC = Vector2.Cross(in rC, in u);
				JwA = Vector2.Cross(in rA, in u);
				mass += this.mC + this.mA + this.iC * JwC * JwC + this.iA * JwA * JwA;

				Vector2 pC = this.localAnchorC - this.lcC;
				Vector2.Subtract(cA, cC, out Vector2 pA);
				Vector2.Add(pA, rA, out pA);
				Rotation.MultiplyTransposed(in qC, in pA, out pA);
				coordinateA = Vector2.Dot(pA - pC, this.localAxisC);
			}

			if (this.jointB is RevoluteJoint)
			{
				JvBD = Vector2.Zero;
				JwB = this.ratio;
				JwD = this.ratio;
				mass += this.ratio * this.ratio * (this.iB + this.iD);

				coordinateB = aB - aD - this.referenceAngleB;
			}
			else
			{
				Vector2 u = Rotation.Multiply(in qD, in this.localAxisD);
				Vector2 rD = Rotation.Multiply(qD, this.localAnchorD - this.lcD);
				Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.lcB);
				JvBD = this.ratio * u;
				JwD = this.ratio * Vector2.Cross(in rD, in u);
				JwB = this.ratio * Vector2.Cross(in rB, in u);
				mass += this.ratio * this.ratio * (this.mD + this.mB) + this.iD * JwD * JwD + this.iB * JwB * JwB;

				Vector2 pD = this.localAnchorD - this.lcD;
				Vector2.Subtract(cB, cD, out Vector2 pB);
				Vector2.Add(pB, rB, out pB);
				Rotation.MultiplyTransposed(in qD, in pB, out pB);
				coordinateB = Vector2.Dot(pB - pD, this.localAxisD);
			}

			float C = (coordinateA + this.ratio * coordinateB) - this.constant;

			float impulse = 0.0f;
			if (mass > 0.0f)
			{
				impulse = -C / mass;
			}

			cA += this.mA * impulse * JvAC;
			aA += this.iA * impulse * JwA;
			cB += this.mB * impulse * JvBD;
			aB += this.iB * impulse * JwB;
			cC -= this.mC * impulse * JvAC;
			aC -= this.iC * impulse * JwC;
			cD -= this.mD * impulse * JvBD;
			aD -= this.iD * impulse * JwD;

			data.states[this.indexA].Position = cA;
			data.states[this.indexA].Angle = aA;
			data.states[this.indexB].Position = cB;
			data.states[this.indexB].Angle = aB;
			data.states[this.indexC].Position = cC;
			data.states[this.indexC].Angle = aC;
			data.states[this.indexD].Position = cD;
			data.states[this.indexD].Angle = aD;

			// TODO_ERIN not implemented
			return linearError < Settings.LinearSlop;
		}

		public override Vector2 GetReactionForce(float inv_dt)
		{
			Vector2 P = this.impulse * this.JvAC;
			return inv_dt * P;
		}

		public override float GetReactionTorque(float inv_dt)
		{
			float L = this.impulse * this.JwA;
			return inv_dt * L;
		}

		#endregion
	}
}