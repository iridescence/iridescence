/*
* Copyright (c) 2006-2011 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

// 1-D constrained system
// m (v2 - v1) = lambda
// v2 + (beta/h) * x1 + gamma * lambda = 0, gamma has units of inverse mass.
// x2 = x1 + h * v2

// 1-D mass-damper-spring system
// m (v2 - v1) + h * d * v2 + h * k * 

// C = norm(p2 - p1) - L
// u = (p2 - p1) / norm(p2 - p1)
// Cdot = dot(u, v2 + cross(w2, r2) - v1 - cross(w1, r1))
// J = [-u -cross(r1, u) u cross(r2, u)]
// K = J * invM * JT
//   = invMass1 + invI1 * cross(r1, u)^2 + invMass2 + invI2 * cross(r2, u)^2

using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	public sealed class DistanceJoint : Joint
	{
		#region Fields

		private readonly float frequency;
		private readonly float dampingRatio;
		private float bias;

		// Solver shared
		private readonly Vector2 localAnchorA;
		private readonly Vector2 localAnchorB;
		private float gamma;
		private float impulse;
		private readonly float length;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 u;
		private Vector2 rA;
		private Vector2 rB;
		private Vector2 localCenterA;
		private Vector2 localCenterB;
		private float invMassA;
		private float invMassB;
		private float invIA;
		private float invIB;
		private float mass;
		
		#endregion

		#region Properties

		public override Vector2 AnchorA => this.bodyA.GetWorldPoint(this.localAnchorA);

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		#endregion

		#region Constructors

		public DistanceJoint(DistanceJointDescriptor def)
			: base(def)
		{
			this.localAnchorA = def.LocalAnchorA;
			this.localAnchorB = def.LocalAnchorB;
			this.length = def.Length;
			this.frequency = def.Frequency;
			this.dampingRatio = def.DampingRatio;
			this.impulse = 0.0f;
			this.gamma = 0.0f;
			this.bias = 0.0f;
		}
		
		#endregion

		#region Methods

		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterA = this.bodyA.sweep.LocalCenter;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassA = this.bodyA.invMass;
			this.invMassB = this.bodyB.invMass;
			this.invIA = this.bodyA.invInertia;
			this.invIB = this.bodyB.invInertia;

			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			this.rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			this.rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);
			this.u = cB + this.rB - cA - this.rA;

			// Handle singularity.
			float length = this.u.Length;
			if (length > Settings.LinearSlop)
				this.u *= 1.0f / length;
			else
				this.u = Vector2.Zero;

			float crAu = Vector2.Cross(in this.rA, in this.u);
			float crBu = Vector2.Cross(in this.rB, in this.u);
			float invMass = this.invMassA + this.invIA * crAu * crAu + this.invMassB + this.invIB * crBu * crBu;

			// Compute the effective mass matrix.
			this.mass = invMass != 0.0f ? 1.0f / invMass : 0.0f;

			if (this.frequency > 0.0f)
			{
				float C = length - this.length;

				// Frequency
				float omega = 2.0f * (float)System.Math.PI * this.frequency;

				// Damping coefficient
				float d = 2.0f * this.mass * this.dampingRatio * omega;

				// Spring stiffness
				float k = this.mass * omega * omega;

				// magic formulas
				float h = data.timeStep.DeltaT;
				this.gamma = h * (d + h * k);
				this.gamma = this.gamma != 0.0f ? 1.0f / this.gamma : 0.0f;
				this.bias = C * h * k * this.gamma;

				invMass += this.gamma;
				this.mass = invMass != 0.0f ? 1.0f / invMass : 0.0f;
			}
			else
			{
				this.gamma = 0.0f;
				this.bias = 0.0f;
			}

			if (data.timeStep.WarmStarting)
			{
				// Scale the impulse to support a variable time step.
				this.impulse *= data.timeStep.DeltaTRatio;

				Vector2 P = this.impulse * this.u;
				vA -= this.invMassA * P;
				wA -= this.invIA * Vector2.Cross(in this.rA, in P);
				vB += this.invMassB * P;
				wB += this.invIB * Vector2.Cross(in this.rB, in P);
			}
			else
			{
				this.impulse = 0.0f;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			// Cdot = dot(u, v + cross(w, r))
			Vector2 vpA = vA + Vector2.Cross(wA, in this.rA);
			Vector2 vpB = vB + Vector2.Cross(wB, in this.rB);
			float Cdot = Vector2.Dot(this.u, vpB - vpA);

			float impulse = -this.mass * (Cdot + this.bias + this.gamma * this.impulse);
			this.impulse += impulse;

			Vector2 P = impulse * this.u;
			vA -= this.invMassA * P;
			wA -= this.invIA * Vector2.Cross(in this.rA, in P);
			vB += this.invMassB * P;
			wB += this.invIB * Vector2.Cross(in this.rB, in P);

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			// There is no position correction for soft distance constraints.
			if (this.frequency > 0.0f)
				return true;

			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);
			Vector2 u = cB + rB - cA - rA;

			float length = Vector2.Normalize(in u, out u);
			float C = length - this.length;
			C = Utility.Clamp(C, -Settings.MaxLinearCorrection, Settings.MaxLinearCorrection);

			float impulse = -this.mass * C;
			Vector2 P = impulse * u;

			cA -= this.invMassA * P;
			aA -= this.invIA * Vector2.Cross(in rA, in P);
			cB += this.invMassB * P;
			aB += this.invIB * Vector2.Cross(in rB, in P);

			data.states[this.indexA].Position = cA;
			data.states[this.indexA].Angle = aA;
			data.states[this.indexB].Position = cB;
			data.states[this.indexB].Angle = aB;

			return System.Math.Abs(C) < Settings.LinearSlop;
		}
		
		public override Vector2 GetReactionForce(float inv_dt)
		{
			return (inv_dt * this.impulse) * this.u;
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return 0.0f;
		}
		
		#endregion
	}
}
