﻿using System;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// A motor joint is used to control the relative motion
	/// between two bodies. A typical usage is to control the movement
	/// of a dynamic body with respect to the ground.
	/// </summary>
	public sealed class MotorJoint : Joint
	{
		#region Fields

		// Solver shared
		private Vector2 linearOffset;
		private float angularOffset;
		private Vector2 linearImpulse;
		private float angularImpulse;
		private float maxForce;
		private float maxTorque;
		private float correctionFactor;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 rA;
		private Vector2 rB;
		private Vector2 localCenterA;
		private Vector2 localCenterB;
		private Vector2 linearError;
		private float angularError;
		private float invMassA;
		private float invMassB;
		private float invIA;
		private float invIB;
		private Matrix2x2 linearMass;
		private float angularMass;

		#endregion

		#region Properties

		public override Vector2 AnchorA => this.bodyA.Position;

		public override Vector2 AnchorB => this.bodyB.Position;

		/// <summary>
		/// Gets or sets the target linear offset.
		/// </summary>
		public Vector2 LinearOffset
		{
			get => this.linearOffset;
			set
			{
				if (value.X != this.linearOffset.X ||
					value.Y != this.linearOffset.Y)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;
					this.linearOffset = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the angular offset in radians.
		/// </summary>
		public float AngularOffset
		{
			get => this.angularOffset;
			set
			{
				if (value != this.angularOffset)
				{
					this.bodyA.Awake = true;
					this.bodyB.Awake = true;
					this.angularOffset = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the maximum friction force in N.
		/// </summary>
		public float MaxForce
		{
			get => this.maxForce;
			set
			{
				if (value < 0.0f)
					throw new ArgumentException("Invalid force.");

				this.maxForce = value;
			}
		}

		/// <summary>
		/// Gets or sets the maximum friction torque in N*m.
		/// </summary>
		public float MaxTorque
		{
			get => this.maxTorque;
			set
			{
				if (value < 0.0f)
					throw new ArgumentException("Invalid torque.");

				this.maxTorque = value;
			}
		}

		/// <summary>
		/// Gets or sets the position correction factor in the range [0, 1].
		/// </summary>
		public float CorrectionFactor
		{
			get => this.correctionFactor;
			set
			{
				if (value < 0.0f || value > 1.0f)
					throw new ArgumentException("Invalid factor.");

				this.correctionFactor = value;
			}
		}

		#endregion

		#region Constructors

		public MotorJoint(MotorJointDescriptor def)
			: base(def)
		{
			this.linearOffset = def.LinearOffset;
			this.angularOffset = def.AngularOffset;

			this.linearImpulse = Vector2.Zero;
			this.angularImpulse = 0.0f;

			this.maxForce = def.MaxForce;
			this.maxTorque = def.MaxTorque;
			this.correctionFactor = def.CorrectionFactor;
		}
		
		#endregion

		#region Methods

		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterA = this.bodyA.sweep.LocalCenter;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassA = this.bodyA.invMass;
			this.invMassB = this.bodyB.invMass;
			this.invIA = this.bodyA.invInertia;
			this.invIB = this.bodyB.invInertia;

			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			// Compute the effective mass matrix.
			this.rA = Rotation.Multiply(qA, this.linearOffset - this.localCenterA);
			this.rB = Rotation.Multiply(qB, -this.localCenterB);

			// J = [-I -r1_skew I r2_skew]
			//     [ 0       -1 0       1]
			// r_skew = [-ry; rx]

			// Matlab
			// K = [ mA+r1y^2*iA+mB+r2y^2*iB,  -r1y*iA*r1x-r2y*iB*r2x,          -r1y*iA-r2y*iB]
			//     [  -r1y*iA*r1x-r2y*iB*r2x, mA+r1x^2*iA+mB+r2x^2*iB,           r1x*iA+r2x*iB]
			//     [          -r1y*iA-r2y*iB,           r1x*iA+r2x*iB,                   iA+iB]

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			Matrix2x2 K;
			K.M11 = mA + mB + iA * this.rA.Y * this.rA.Y + iB * this.rB.Y * this.rB.Y;
			K.M12 = -iA * this.rA.X * this.rA.Y - iB * this.rB.X * this.rB.Y;
			K.M21 = K.M12;
			K.M22 = mA + mB + iA * this.rA.X * this.rA.X + iB * this.rB.X * this.rB.X;

			Matrix2x2.Invert(in K, out this.linearMass);

			this.angularMass = iA + iB;
			if (this.angularMass > 0.0f)
			{
				this.angularMass = 1.0f / this.angularMass;
			}

			this.linearError = cB + this.rB - cA - this.rA;
			this.angularError = aB - aA - this.angularOffset;

			if (data.timeStep.WarmStarting)
			{
				// Scale impulses to support a variable time step.
				this.linearImpulse *= data.timeStep.DeltaTRatio;
				this.angularImpulse *= data.timeStep.DeltaTRatio;

				Vector2 P = new Vector2(this.linearImpulse.X, this.linearImpulse.Y);
				vA -= mA * P;
				wA -= iA * (Vector2.Cross(this.rA, in P) + this.angularImpulse);
				vB += mB * P;
				wB += iB * (Vector2.Cross(this.rB, in P) + this.angularImpulse);
			}
			else
			{
				this.linearImpulse = Vector2.Zero;
				this.angularImpulse = 0.0f;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			float h = data.timeStep.DeltaT;
			float inv_h = data.timeStep.InvDeltaT;

			// Solve angular friction
			{
				float Cdot = wB - wA + inv_h * this.correctionFactor * this.angularError;
				float impulse = -this.angularMass * Cdot;

				float oldImpulse = this.angularImpulse;
				float maxImpulse = h * this.maxTorque;
				this.angularImpulse = Utility.Clamp(this.angularImpulse + impulse, -maxImpulse, maxImpulse);
				impulse = this.angularImpulse - oldImpulse;

				wA -= iA * impulse;
				wB += iB * impulse;
			}

			// Solve linear friction
			{
				Vector2 Cdot = vB + Vector2.Cross(wB, in this.rB) - vA - Vector2.Cross(wA, in this.rA) + inv_h * this.correctionFactor * this.linearError;

				Matrix2x2.Multiply(this.linearMass, Cdot, out Vector2 impulse);
				Vector2.Negate(impulse, out impulse);

				Vector2 oldImpulse = this.linearImpulse;
				this.linearImpulse += impulse;

				float maxImpulse = h * this.maxForce;

				if (this.linearImpulse.LengthSquared > maxImpulse * maxImpulse)
				{
					Vector2.Normalize(in this.linearImpulse, out this.linearImpulse);
					Vector2.Multiply(this.linearImpulse, maxImpulse, out this.linearImpulse);
				}

				impulse = this.linearImpulse - oldImpulse;

				vA -= mA * impulse;
				wA -= iA * Vector2.Cross(in this.rA, in impulse);

				vB += mB * impulse;
				wB += iB * Vector2.Cross(in this.rB, in impulse);
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			return true;
		}
		
		public override Vector2 GetReactionForce(float inv_dt)
		{
			return inv_dt * this.linearImpulse;
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return inv_dt * this.angularImpulse;
		}
		
		#endregion
	}
}
