/*
* Copyright (c) 2006-2011 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

// Point-to-point constraint
// Cdot = v2 - v1
//      = v2 + cross(w2, r2) - v1 - cross(w1, r1)
// J = [-I -r1_skew I r2_skew ]
// Identity used:
// w k % (rx i + ry j) = w * (-ry i + rx j)

// Angle constraint
// Cdot = w2 - w1
// J = [0 0 -1 0 0 1]
// K = invI1 + invI2

using System;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// Friction joint. This is used for top-down friction.
	/// It provides 2D translational friction and angular friction.
	/// </summary>
	public sealed class FrictionJoint : Joint
	{
		#region Fields

		private readonly Vector2 localAnchorA;
		private readonly Vector2 localAnchorB;

		// Solver shared
		private Vector2 linearImpulse;
		private float angularImpulse;
		private float maxForce;
		private float maxTorque;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 rA;
		private Vector2 rB;
		private Vector2 localCenterA;
		private Vector2 localCenterB;
		private float invMassA;
		private float invMassB;
		private float invIA;
		private float invIB;
		private Matrix2x2 linearMass;
		private float angularMass;

		#endregion

		#region Properties

		public override Vector2 AnchorA => this.bodyA.GetWorldPoint(this.localAnchorA);

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		public float MaxForce
		{
			get => this.maxForce;
			set
			{
				if (value < 0.0f)
					throw new ArgumentException("Invalid MaxForce.");
				this.maxForce = value;
			}
		}

		public float MaxTorque
		{
			get => this.maxTorque;
			set
			{
				if (value < 0.0f)
					throw new ArgumentException("Invalid MaxTorque.");
				this.maxTorque = value;
			}
		}

		#endregion

		#region Constructors

		public FrictionJoint(FrictionJointDescriptor def)
			: base(def)
		{
			this.localAnchorA = def.LocalAnchorA;
			this.localAnchorB = def.LocalAnchorB;

			this.linearImpulse = Vector2.Zero;
			this.angularImpulse = 0.0f;

			this.maxForce = def.MaxForce;
			this.maxTorque = def.MaxTorque;
		}

		#endregion

		#region Methods

		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterA = this.bodyA.sweep.LocalCenter;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassA = this.bodyA.invMass;
			this.invMassB = this.bodyB.invMass;
			this.invIA = this.bodyA.invInertia;
			this.invIB = this.bodyB.invInertia;

			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			// Compute the effective mass matrix.
			this.rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			this.rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);

			// J = [-I -r1_skew I r2_skew]
			//     [ 0       -1 0       1]
			// r_skew = [-ry; rx]

			// Matlab
			// K = [ mA+r1y^2*iA+mB+r2y^2*iB,  -r1y*iA*r1x-r2y*iB*r2x,          -r1y*iA-r2y*iB]
			//     [  -r1y*iA*r1x-r2y*iB*r2x, mA+r1x^2*iA+mB+r2x^2*iB,           r1x*iA+r2x*iB]
			//     [          -r1y*iA-r2y*iB,           r1x*iA+r2x*iB,                   iA+iB]

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			Matrix2x2 K;
			K.M11 = mA + mB + iA * this.rA.Y * this.rA.Y + iB * this.rB.Y * this.rB.Y;
			K.M12 = -iA * this.rA.X * this.rA.Y - iB * this.rB.X * this.rB.Y;
			K.M21 = K.M12;
			K.M22 = mA + mB + iA * this.rA.X * this.rA.X + iB * this.rB.X * this.rB.X;

			Matrix2x2.Invert(in K, out this.linearMass);

			this.angularMass = iA + iB;
			if (this.angularMass > 0.0f)
			{
				this.angularMass = 1.0f / this.angularMass;
			}

			if (data.timeStep.WarmStarting)
			{
				// Scale impulses to support a variable time step.
				this.linearImpulse *= data.timeStep.DeltaTRatio;
				this.angularImpulse *= data.timeStep.DeltaTRatio;

				Vector2 P = new Vector2(this.linearImpulse.X, this.linearImpulse.Y);
				vA -= mA * P;
				wA -= iA * (Vector2.Cross(in this.rA, in P) + this.angularImpulse);
				vB += mB * P;
				wB += iB * (Vector2.Cross(in this.rB, in P) + this.angularImpulse);
			}
			else
			{
				this.linearImpulse = Vector2.Zero;
				this.angularImpulse = 0.0f;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			float mA = this.invMassA, mB = this.invMassB;
			float iA = this.invIA, iB = this.invIB;

			float h = data.timeStep.DeltaT;

			// Solve angular friction
			{
				float Cdot = wB - wA;
				float impulse = -this.angularMass * Cdot;

				float oldImpulse = this.angularImpulse;
				float maxImpulse = h * this.maxTorque;
				this.angularImpulse = Utility.Clamp(this.angularImpulse + impulse, -maxImpulse, maxImpulse);
				impulse = this.angularImpulse - oldImpulse;

				wA -= iA * impulse;
				wB += iB * impulse;
			}

			// Solve linear friction
			{
				Vector2 Cdot = vB + Vector2.Cross(wB, in this.rB) - vA - Vector2.Cross(wA, in this.rA);

				Vector2.Multiply(this.linearImpulse, Cdot, out Vector2 impulse);
				Vector2.Negate(impulse, out impulse);

				Vector2 oldImpulse = this.linearImpulse;
				this.linearImpulse += impulse;

				float maxImpulse = h * this.maxForce;

				if (this.linearImpulse.LengthSquared > maxImpulse * maxImpulse)
				{
					Vector2.Normalize(in this.linearImpulse, out this.linearImpulse);
					Vector2.Multiply(this.linearImpulse, maxImpulse, out this.linearImpulse);
				}

				impulse = this.linearImpulse - oldImpulse;

				vA -= mA * impulse;
				wA -= iA * Vector2.Cross(in this.rA, in impulse);

				vB += mB * impulse;
				wB += iB * Vector2.Cross(in this.rB, in impulse);
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			return true;
		}

		public override Vector2 GetReactionForce(float inv_dt)
		{
			return inv_dt * this.linearImpulse;
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return inv_dt * this.angularImpulse;
		}

		#endregion

	}
}
