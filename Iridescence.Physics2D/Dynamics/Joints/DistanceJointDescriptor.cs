using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	public sealed class DistanceJointDescriptor : JointDescriptor
	{
		#region Properties

		/// <summary>
		/// The local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA { get; set; }

		/// <summary>
		/// The local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB { get; set; }

		/// <summary>
		/// The natural length between the anchor points.
		/// </summary>
		public float Length { get; set; }

		/// <summary>
		/// The mass-spring-damper frequency in Hertz. A value of 0 disables softness.
		/// </summary>
		public float Frequency { get; set; }

		/// <summary>
		/// The damping ratio.
		/// 0 = no damping, 1 = critical damping.
		/// </summary>
		public float DampingRatio { get; set; }

		#endregion

		#region Constructors

		public DistanceJointDescriptor()
		{
			this.LocalAnchorA = Vector2.Zero;
			this.LocalAnchorB = Vector2.Zero;
			this.Length = 1.0f;
			this.Frequency = 0.0f;
			this.DampingRatio = 0.0f;
		}

		public DistanceJointDescriptor(Body b1, Body b2, Vector2 anchor1, Vector2 anchor2)
		{
			this.BodyA = b1;
			this.BodyB = b2;
			this.LocalAnchorA = this.BodyA.GetLocalPoint(anchor1);
			this.LocalAnchorB = this.BodyB.GetLocalPoint(anchor2);
			Vector2 d = anchor2 - anchor1;
			this.Length = d.Length;
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new DistanceJoint(this);
		}

		#endregion
	}
}
