/*
* Copyright (c) 2007 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/

// Pulley:
// length1 = norm(p1 - s1)
// length2 = norm(p2 - s2)
// C0 = (length1 + ratio * length2)_initial
// C = C0 - (length1 + ratio * length2)
// u1 = (p1 - s1) / norm(p1 - s1)
// u2 = (p2 - s2) / norm(p2 - s2)
// Cdot = -dot(u1, v1 + cross(w1, r1)) - ratio * dot(u2, v2 + cross(w2, r2))
// J = -[u1 cross(r1, u1) ratio * u2  ratio * cross(r2, u2)]
// K = J * invM * JT
//   = invMass1 + invI1 * cross(r1, u1)^2 + ratio^2 * (invMass2 + invI2 * cross(r2, u2)^2)

using System.Diagnostics;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	/// <summary>
	/// The pulley joint is connected to two bodies and two fixed ground points.
	/// The pulley supports a ratio such that:
	/// length1 + ratio * length2 &lt;= constant
	/// Yes, the force transmitted is scaled by the ratio.
	/// Warning: the pulley joint can get a bit squirrelly by itself. They often
	/// work better when combined with prismatic joints. You should also cover the
	/// the anchor points with static shapes to prevent one side from going to
	/// zero length.
	/// </summary>
	public sealed class PulleyJoint : Joint
	{
		#region Fields

		private Vector2 groundAnchorA;
		private Vector2 groundAnchorB;
		private readonly float lengthA;
		private readonly float lengthB;

		// Solver shared
		private readonly Vector2 localAnchorA;
		private readonly Vector2 localAnchorB;
		private readonly float constant;
		private readonly float ratio;
		private float impulse;

		// Solver temp
		private int indexA;
		private int indexB;
		private Vector2 uA;
		private Vector2 uB;
		private Vector2 rA;
		private Vector2 rB;
		private Vector2 localCenterA;
		private Vector2 localCenterB;
		private float invMassA;
		private float invMassB;
		private float invIA;
		private float invIB;
		private float mass;

		#endregion

		#region Properties

		public override Vector2 AnchorA => this.bodyA.GetWorldPoint(this.localAnchorA);

		public override Vector2 AnchorB => this.bodyB.GetWorldPoint(this.localAnchorB);

		public Vector2 GroundAnchorA => this.groundAnchorA;

		public Vector2 GroundAnchorB => this.groundAnchorB;

		public float LengthA => this.lengthA;

		public float LengthB => this.lengthB;

		public float Ratio => this.ratio;

		public float CurrentLengthA
		{
			get
			{
				Vector2 p = this.bodyA.GetWorldPoint(this.localAnchorA);
				Vector2 s = this.groundAnchorA;
				Vector2 d = p - s;
				return d.Length;
			}
		}

		public float CurrentLengthB
		{
			get
			{
				Vector2 p = this.bodyB.GetWorldPoint(this.localAnchorB);
				Vector2 s = this.groundAnchorB;
				Vector2 d = p - s;
				return d.Length;
			}
		}

		#endregion

		#region Constructors

		public PulleyJoint(PulleyJointDescriptor def)
			: base(def)
		{
			this.groundAnchorA = def.GroundAnchorA;
			this.groundAnchorB = def.GroundAnchorB;
			this.localAnchorA = def.LocalAnchorA;
			this.localAnchorB = def.LocalAnchorB;

			this.lengthA = def.LengthA;
			this.lengthB = def.LengthB;

			Debug.Assert(def.Ratio != 0.0f);
			this.ratio = def.Ratio;

			this.constant = def.LengthA + this.ratio * def.LengthB;

			this.impulse = 0.0f;
		}

		#endregion

		#region Methods

		protected internal override void InitVelocityConstraints(SolverData data)
		{
			this.indexA = this.bodyA.islandIndex;
			this.indexB = this.bodyB.islandIndex;
			this.localCenterA = this.bodyA.sweep.LocalCenter;
			this.localCenterB = this.bodyB.sweep.LocalCenter;
			this.invMassA = this.bodyA.invMass;
			this.invMassB = this.bodyB.invMass;
			this.invIA = this.bodyA.invInertia;
			this.invIB = this.bodyB.invInertia;

			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;

			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			this.rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			this.rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);

			// Get the pulley axes.
			this.uA = cA + this.rA - this.groundAnchorA;
			this.uB = cB + this.rB - this.groundAnchorB;

			float lengthA = this.uA.Length;
			float lengthB = this.uB.Length;

			if (lengthA > 10.0f * Settings.LinearSlop)
				this.uA *= 1.0f / lengthA;
			else
				this.uA = Vector2.Zero;

			if (lengthB > 10.0f * Settings.LinearSlop)
				this.uB *= 1.0f / lengthB;
			else
				this.uB = Vector2.Zero;

			// Compute effective mass.
			float ruA = Vector2.Cross(in this.rA, in this.uA);
			float ruB = Vector2.Cross(in this.rB, in this.uB);

			float mA = this.invMassA + this.invIA * ruA * ruA;
			float mB = this.invMassB + this.invIB * ruB * ruB;

			this.mass = mA + this.ratio * this.ratio * mB;

			if (this.mass > 0.0f)
				this.mass = 1.0f / this.mass;

			if (data.timeStep.WarmStarting)
			{
				// Scale impulses to support variable time steps.
				this.impulse *= data.timeStep.DeltaTRatio;

				// Warm starting.
				Vector2 PA = -(this.impulse) * this.uA;
				Vector2 PB = (-this.ratio * this.impulse) * this.uB;

				vA += this.invMassA * PA;
				wA += this.invIA * Vector2.Cross(in this.rA, in PA);
				vB += this.invMassB * PB;
				wB += this.invIB * Vector2.Cross(in this.rB, in PB);
			}
			else
			{
				this.impulse = 0.0f;
			}

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override void SolveVelocityConstraints(SolverData data)
		{
			Vector2 vA = data.states[this.indexA].LinearVelocity;
			float wA = data.states[this.indexA].AngularVelocity;
			Vector2 vB = data.states[this.indexB].LinearVelocity;
			float wB = data.states[this.indexB].AngularVelocity;

			Vector2 vpA = vA + Vector2.Cross(wA, in this.rA);
			Vector2 vpB = vB + Vector2.Cross(wB, in this.rB);

			float Cdot = -Vector2.Dot(this.uA, vpA) - this.ratio * Vector2.Dot(this.uB, vpB);
			float impulse = -this.mass * Cdot;
			this.impulse += impulse;

			Vector2 PA = -impulse * this.uA;
			Vector2 PB = -this.ratio * impulse * this.uB;
			vA += this.invMassA * PA;
			wA += this.invIA * Vector2.Cross(in this.rA, in PA);
			vB += this.invMassB * PB;
			wB += this.invIB * Vector2.Cross(in this.rB, in PB);

			data.states[this.indexA].LinearVelocity = vA;
			data.states[this.indexA].AngularVelocity = wA;
			data.states[this.indexB].LinearVelocity = vB;
			data.states[this.indexB].AngularVelocity = wB;
		}

		protected internal override bool SolvePositionConstraints(SolverData data)
		{
			Vector2 cA = data.states[this.indexA].Position;
			float aA = data.states[this.indexA].Angle;
			Vector2 cB = data.states[this.indexB].Position;
			float aB = data.states[this.indexB].Angle;

			Rotation qA = new Rotation(aA);
			Rotation qB = new Rotation(aB);

			Vector2 rA = Rotation.Multiply(qA, this.localAnchorA - this.localCenterA);
			Vector2 rB = Rotation.Multiply(qB, this.localAnchorB - this.localCenterB);

			// Get the pulley axes.
			Vector2 uA = cA + rA - this.groundAnchorA;
			Vector2 uB = cB + rB - this.groundAnchorB;

			float lengthA = uA.Length;
			float lengthB = uB.Length;

			if (lengthA > 10.0f * Settings.LinearSlop)
				uA *= 1.0f / lengthA;
			else
				uA = Vector2.Zero;

			if (lengthB > 10.0f * Settings.LinearSlop)
				uB *= 1.0f / lengthB;
			else
				uB = Vector2.Zero;

			// Compute effective mass.
			float ruA = Vector2.Cross(in rA, in uA);
			float ruB = Vector2.Cross(in rB, in uB);

			float mA = this.invMassA + this.invIA * ruA * ruA;
			float mB = this.invMassB + this.invIB * ruB * ruB;

			float mass = mA + this.ratio * this.ratio * mB;

			if (mass > 0.0f)
				mass = 1.0f / mass;

			float C = this.constant - lengthA - this.ratio * lengthB;
			float linearError = System.Math.Abs(C);

			float impulse = -mass * C;

			Vector2 PA = -impulse * uA;
			Vector2 PB = -this.ratio * impulse * uB;

			cA += this.invMassA * PA;
			aA += this.invIA * Vector2.Cross(in rA, in PA);
			cB += this.invMassB * PB;
			aB += this.invIB * Vector2.Cross(in rB, in PB);

			data.states[this.indexA].Position = cA;
			data.states[this.indexA].Angle = aA;
			data.states[this.indexB].Position = cB;
			data.states[this.indexB].Angle = aB;

			return linearError < Settings.LinearSlop;
		}

		public override Vector2 GetReactionForce(float inv_dt)
		{
			Vector2 P = this.impulse * this.uB;
			return inv_dt * P;
		}

		public override float GetReactionTorque(float inv_dt)
		{
			return 0.0f;
		}

		public override void ShiftOrigin(Vector2 newOrigin)
		{
			this.groundAnchorA -= newOrigin;
			this.groundAnchorB -= newOrigin;
		}

		#endregion
	}
}