using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics.Joints
{
	public sealed class FrictionJointDescriptor : JointDescriptor
	{
		#region Fields

		/// <summary>
		/// The local anchor point relative to bodyB's origin.
		/// </summary>
		public Vector2 LocalAnchorB;

		/// <summary>
		/// The maximum friction force in N.
		/// </summary>
		public float MaxForce;

		/// <summary>
		/// The maximum friction torque in N-m.
		/// </summary>
		public float MaxTorque;

		#endregion

		#region Properties

		/// <summary>
		/// The local anchor point relative to bodyA's origin.
		/// </summary>
		public Vector2 LocalAnchorA { get; set; }

		#endregion

		#region Constructors

		public FrictionJointDescriptor()
		{
			this.LocalAnchorA = Vector2.Zero;
			this.LocalAnchorB = Vector2.Zero;
			this.MaxForce = 0.0f;
			this.MaxTorque = 0.0f;
		}

		public FrictionJointDescriptor(Body bA, Body bB, Vector2 anchor)
			: this()
		{
			this.BodyA = bA;
			this.BodyB = bB;
			this.LocalAnchorA = this.BodyA.GetLocalPoint(anchor);
			this.LocalAnchorB = this.BodyB.GetLocalPoint(anchor);
		}

		#endregion

		#region Methods

		protected internal override Joint Create()
		{
			return new FrictionJoint(this);
		}

		#endregion
	}
}
