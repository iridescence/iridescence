﻿namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Defines the body types.
	/// </summary>
	public enum BodyType
	{
		/// <summary>
		/// A static body. Can't move around and does not respond to impulses.
		/// </summary>
		Static,

		/// <summary>
		/// A kinematic body. Can be moved but has no mass. Useful for things like projectiles or other impact-only objects.
		/// </summary>
		Kinematic,

		/// <summary>
		/// A completely dynamic body.
		/// </summary>
		Dynamic
	}
}
