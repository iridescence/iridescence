﻿using System;
using System.Collections;
using System.Collections.Generic;
using Iridescence.Physics2D.Dynamics.Controllers;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Contains the controlls of a <see cref="Body"/>.
	/// </summary>
	public sealed class BodyControllerCollection : IReadOnlyList<Controller>, INotifyListChanged<Controller>
	{
		#region Events

		public event CollectionChangedEventHandler<ListItem<Controller>> CollectionChanged;

		#endregion

		#region Fields

		private readonly List<Controller> controllers;
		
		#endregion
		
		#region Properties

		public Body Body { get; }

		public World World => this.Body.World;

		public int Count => this.controllers.Count;

		public Controller this[int index] => this.controllers[index];
		
		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="BodyControllerCollection"/> class. 
		/// </summary>
		internal BodyControllerCollection(Body body)
		{
			this.Body = body;
			this.controllers = new List<Controller>();
		}

		#endregion
		
		#region Methods

		/// <summary>
		/// Adds a controller to this body.
		/// </summary>
		/// <param name="controller"></param>
		public void Add(Controller controller)
		{
			if(this.controllers.Contains(controller))
				throw new ArgumentException("Controller has already been added to this body.", nameof(controller));

			this.controllers.Add(controller);

			this.CollectionChanged?.Invoke(this, ListChange.Add(this.controllers.Count - 1, controller));
		}

		/// <summary>
		/// Removes a controller from this body.
		/// </summary>
		/// <param name="controller"></param>
		/// <returns></returns>
		public bool Remove(Controller controller)
		{
			int index = this.controllers.IndexOf(controller);
			if(index < 0)
				return false;

			this.controllers.RemoveAt(index);

			this.CollectionChanged?.Invoke(this, ListChange.Remove(index, controller));
			return true;
		}

		public IEnumerator<Controller> GetEnumerator()
		{
			return this.controllers.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		#endregion
	}
}
