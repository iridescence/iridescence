using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Iridescence.Math;
using Iridescence.Physics2D.Collision;
using Iridescence.Physics2D.Collision.Shapes;
using Iridescence.Physics2D.Dynamics.Contacts;
using Iridescence.Physics2D.Dynamics.Joints;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// A rigid body. These are created via World.CreateBody.
	/// </summary>
	public sealed class Body : IComparable<Body>
	{
		#region Fields

		private readonly World world;

		internal BodyType type;
		internal BodyFlags flags;
		internal int islandIndex;

		internal Transform transform;
		internal Sweep sweep;

		internal Vector2 linearVelocity;
		internal float angularVelocity;

		internal Vector2 force;
		internal float torque;

		private readonly BodyFixtureCollection fixtures;
		internal BodyControllerCollection controllers;

		//internal readonly List<Fixture> fixtures;
		internal readonly List<JointEdge> jointEdges;
		internal readonly BodyContactCollection contacts;

		internal float mass, invMass;
		internal float inertia, invInertia;
		internal float sleepTime;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the world that this body belongs to.
		/// </summary>
		public World World => this.world;

		/// <summary>
		/// Gets or sets the type of the body.
		/// </summary>
		public BodyType Type
		{
			get => this.type;
			set
			{
				if (this.type == value)
					return;

				this.type = value;

				this.ResetMassData();

				if (this.type == BodyType.Static)
				{
					this.linearVelocity = Vector2.Zero;
					this.angularVelocity = 0.0f;
					this.sweep.Angle0 = this.sweep.Angle;
					this.sweep.Center0 = this.sweep.Center;
					this.SynchronizeFixtures();
				}

				this.Awake = true;

				this.force = Vector2.Zero;
				this.torque = 0.0f;

				// Delete the attached contacts.
				this.DestroyContacts();

				// Touch the proxies so that new contacts will be created (when appropriate)
				BroadPhase broadPhase = this.world.contactManager.broadPhase;
				foreach (Fixture f in this.fixtures)
				{
					if (!f.proxiesCreated)
						continue;

					for (int i = 0; i < f.proxies.Length; ++i)
					{
						broadPhase.TouchProxy(f.proxies[i].ProxyId);
					}
				}
			}
		}

		/// <summary>
		/// Gets the body's fixtures.
		/// </summary>
		public BodyFixtureCollection Fixtures => this.fixtures;

		/// <summary>
		/// Gets the body's controllers.
		/// </summary>
		public BodyControllerCollection Controllers => this.controllers ??= new BodyControllerCollection(this);

		/// <summary>
		/// Gets or sets the position of the body's origin and rotation.
		/// Manipulating a body's transform may cause non-physical behavior.
		/// Note: contacts are updated on the next call to b2World::Step.
		/// </summary>
		public Transform Transform
		{
			get => this.transform;
			set
			{
				Debug.Assert(MathHelper.IsValid(value));

				this.transform = value;

				Transform.Multiply(in this.transform, in this.sweep.LocalCenter, out this.sweep.Center);
				this.sweep.Center0 = this.sweep.Center;
				this.sweep.Angle0 = this.sweep.Angle = this.transform.Q.Angle;

				BroadPhase broadPhase = this.world.contactManager.broadPhase;
				foreach (Fixture f in this.fixtures)
				{
					f.Synchronize(broadPhase, in this.transform, in this.transform);
				}
			}
		}

		/// <summary>
		/// Gets the body's position in the world.
		/// </summary>
		public Vector2 Position
		{
			get => this.transform.P;
			set
			{
				Debug.Assert(value.IsFinite);

				Transform t = this.transform;
				t.P = value;
				this.Transform = t;
			}
		}

		/// <summary>
		/// Gets the angle of the body.
		/// </summary>
		public float Angle
		{
			get => this.sweep.Angle;
			set
			{
				Debug.Assert(value.IsFinite());

				Transform t = this.transform;
				t.Q.Angle = value;
				this.Transform = t;
			}
		}

		/// <summary>
		/// Gets the world position of the center of mass.
		/// </summary>
		public Vector2 WorldCenter => this.sweep.Center;

		/// <summary>
		/// Gets the local position of the center of mass.
		/// </summary>
		public Vector2 LocalCenter => this.sweep.LocalCenter;

		/// <summary>
		/// Gets or sets the linear velocity of the center of mass.
		/// </summary>
		public Vector2 LinearVelocity
		{
			get => this.linearVelocity;
			set
			{
				Debug.Assert(value.IsFinite);

				if (this.type == BodyType.Static)
					return;

				float l = value.LengthSquared;
				if (l > 0.0f)
					this.Awake = true;

				this.linearVelocity = value;
			}
		}

		/// <summary>
		/// Gets or sets the angular velocity.
		/// </summary>
		public float AngularVelocity
		{
			get => this.angularVelocity;
			set
			{
				Debug.Assert(value.IsFinite());

				if (this.type == BodyType.Static)
					return;

				if (value * value > 0.0f)
					this.Awake = true;

				this.angularVelocity = value;
			}
		}

		/// <summary>
		/// Gets or sets the body's state.
		/// </summary>
		public BodyState State
		{
			get
			{
				BodyState state;
				state.Position = this.transform.P;
				state.Angle = this.sweep.Angle;
				state.LinearVelocity = this.linearVelocity;
				state.AngularVelocity = this.angularVelocity;
				return state;
			}
			set
			{
				Debug.Assert(MathHelper.IsValid(value));

				Transform t;
				t.P = value.Position;
				t.Q = new Rotation(value.Angle);
				this.Transform = t;

				if (this.type == BodyType.Static)
					return;

				float l = value.LinearVelocity.LengthSquared;
				if (l > 0.0f || value.AngularVelocity * value.AngularVelocity > 0.0f)
					this.Awake = true;

				this.linearVelocity = value.LinearVelocity;
				this.angularVelocity = value.AngularVelocity;
			}
		}

		/// <summary>
		/// Gets the mass of the body.
		/// </summary>
		public float Mass => this.mass;

		/// <summary>
		/// Gets the rotational inertia of the body about the local origin.
		/// </summary>
		public float Inertia => this.inertia + this.mass * this.sweep.LocalCenter.LengthSquared;

		/// <summary>
		/// Gets the mass data of the body.
		/// </summary>
		public MassData MassData
		{
			get => new MassData(this.mass, this.sweep.LocalCenter, this.inertia + this.mass * this.sweep.LocalCenter.LengthSquared);
			set
			{
				if (this.type != BodyType.Dynamic)
					return;

				this.invMass = 0.0f;
				this.inertia = 0.0f;
				this.invInertia = 0.0f;

				this.mass = value.Mass;
				if (this.mass <= 0.0f)
					this.mass = 1.0f;

				this.invMass = 1.0f / this.mass;

				if (value.Inertia > 0.0f && (this.flags & BodyFlags.FixedRotation) == 0)
				{
					this.inertia = value.Inertia - this.mass * Vector2.Dot(value.Center, value.Center);
					Debug.Assert(this.inertia > 0.0f);
					this.invInertia = 1.0f / this.inertia;
				}

				// Move center of mass.
				Vector2 oldCenter = this.sweep.Center;
				this.sweep.LocalCenter = value.Center;
				Transform.Multiply(in this.transform, in this.sweep.LocalCenter, out this.sweep.Center);
				this.sweep.Center0 = this.sweep.Center;

				// Update center of mass velocity.
				Vector2 diff = this.sweep.Center - oldCenter;
				this.linearVelocity += Vector2.Cross(this.angularVelocity, in diff);
			}
		}

		/// <summary>
		/// Gets or sets whether the body is currently awake.
		/// </summary>
		public bool Awake
		{
			get => (this.flags & BodyFlags.Awake) != 0;
			set
			{
				if (value)
				{
					this.flags |= BodyFlags.Awake;
					this.sleepTime = 0.0f;
				}
				else
				{
					this.flags &= ~BodyFlags.Awake;
					this.sleepTime = 0.0f;
					this.linearVelocity = Vector2.Zero;
					this.angularVelocity = 0.0f;
					this.force = Vector2.Zero;
					this.torque = 0.0f;
				}
			}
		}

		/// <summary>
		/// Gets or sets the active state of the body. An inactive body is not
		/// simulated and cannot be collided with or woken up.
		/// If you pass a flag of true, all fixtures will be added to the
		/// broad-phase.
		/// If you pass a flag of false, all fixtures will be removed from
		/// the broad-phase and all contacts will be destroyed.
		/// Fixtures and joints are otherwise unaffected. You may continue
		/// to create/destroy fixtures and joints on inactive bodies.
		/// Fixtures on an inactive body are implicitly inactive and will
		/// not participate in collisions, ray-casts, or queries.
		/// Joints connected to an inactive body are implicitly inactive.
		/// An inactive body is still owned by a World object and remains
		/// in the body list.
		/// </summary>
		public bool Active
		{
			get => (this.flags & BodyFlags.Active) != 0;
			set
			{
				if (value == this.Active)
					return;

				BroadPhase broadPhase = this.world.contactManager.broadPhase;

				if (value)
				{
					this.flags |= BodyFlags.Active;

					// Create all proxies.
					foreach (Fixture f in this.fixtures)
					{
						f.CreateProxies(broadPhase, this.transform);
					}

					// Contacts are created the next time step.
				}
				else
				{
					this.flags &= ~BodyFlags.Active;

					// Destroy all proxies.
					foreach (Fixture f in this.fixtures)
					{
						f.DestroyProxies(broadPhase);
					}

					// Destroy the attached contacts.
					this.DestroyContacts();
				}
			}
		}

		/// <summary>
		/// Gets or sets a value that determines whether this body is treated as a bullet for continuous collision detection.
		/// </summary>
		public bool IsBullet
		{
			get => (this.flags & BodyFlags.Bullet) != 0;
			set
			{
				if (value)
					this.flags |= BodyFlags.Bullet;
				else
					this.flags &= ~BodyFlags.Bullet;
			}
		}

		/// <summary>
		/// Gets or sets whether the body has fixed rotation.
		/// Setting this property causes the mass to be reset.
		/// </summary>
		public bool FixedRotation
		{
			get => (this.flags & BodyFlags.FixedRotation) == BodyFlags.FixedRotation;
			set
			{
				bool status = (this.flags & BodyFlags.FixedRotation) == BodyFlags.FixedRotation;
				if (status == value)
					return;

				if (value)
					this.flags |= BodyFlags.FixedRotation;
				else
					this.flags &= ~BodyFlags.FixedRotation;

				this.ResetMassData();
			}
		}

		/// <summary>
		/// Gets or sets a value that determines whether the body is allowed to sleep.
		/// </summary>
		public bool AllowSleep
		{
			get => (this.flags & BodyFlags.AutoSleep) == BodyFlags.AutoSleep;
			set
			{
				if (value)
				{
					this.flags |= BodyFlags.AutoSleep;
				}
				else
				{
					this.flags &= ~BodyFlags.AutoSleep;
					this.Awake = true;
				}
			}
		}

		/// <summary>
		/// Gets or set a user object.
		/// </summary>
		public object UserData { get; set; }

		#endregion

		#region Constructors

		internal Body(World world, BodyDescriptor descriptor)
		{
			this.world = world;

			this.flags = 0;

			if (descriptor.IsBullet)
				this.flags |= BodyFlags.Bullet;

			if (descriptor.FixedRotation)
				this.flags |= BodyFlags.FixedRotation;

			if (descriptor.AllowSleep)
				this.flags |= BodyFlags.AutoSleep;

			if (descriptor.Awake)
				this.flags |= BodyFlags.Awake;

			if (descriptor.Active)
				this.flags |= BodyFlags.Active;

			this.transform.P = descriptor.Position;
			this.transform.Q.Angle = descriptor.Angle;

			this.sweep.LocalCenter = Vector2.Zero;
			this.sweep.Center0 = this.transform.P;
			this.sweep.Center = this.transform.P;
			this.sweep.Angle0 = descriptor.Angle;
			this.sweep.Angle = descriptor.Angle;
			this.sweep.Alpha0 = 0.0f;

			this.fixtures = new BodyFixtureCollection(this);
			this.jointEdges = new List<JointEdge>();
			this.contacts = new BodyContactCollection();

			this.linearVelocity = descriptor.LinearVelocity;
			this.angularVelocity = descriptor.AngularVelocity;

			//this.linearDamping = descriptor.LinearDamping;
			//this.angularDamping = descriptor.AngularDamping;
			//this.gravityScale = descriptor.GravityScale;

			this.force = Vector2.Zero;
			this.torque = 0.0f;

			this.sleepTime = 0.0f;

			this.type = descriptor.Type;

			if (this.type == BodyType.Dynamic)
			{
				this.mass = 1.0f;
				this.invMass = 1.0f;
			}
			else
			{
				this.mass = 0.0f;
				this.invMass = 0.0f;
			}

			this.inertia = 0.0f;
			this.invInertia = 0.0f;

			this.UserData = descriptor.UserData;
		}

		#endregion

		#region Methods

		#region IComparable Members

		public int CompareTo(Body b2)
		{
			if (this.type != b2.type)
				return this.type - b2.type;
			return 0;
		}

		#endregion

		#region World<->Local

		/// <summary>
		/// Returns the world coordinates of a point given the local coordinates.
		/// </summary>
		/// <param name="localPoint">A point on the body measured relative the the body's origin.</param>
		/// <returns>The corresponding point expressed in world coordinates.</returns>
		public Vector2 GetWorldPoint(Vector2 localPoint)
		{
			if (!localPoint.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(localPoint));

			Transform.Multiply(in this.transform, in localPoint, out Vector2 worldPoint);
			return worldPoint;
		}

		/// <summary>
		/// Returns the world coordinates of a vector given the local coordinates.
		/// </summary>
		/// <param name="localVector">A vector fixed in the body.</param>
		/// <returns>The corresponding vector expressed in world coordinates.</returns>
		public Vector2 GetWorldVector(Vector2 localVector)
		{
			if (!localVector.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(localVector));

			return Rotation.Multiply(in this.transform.Q, in localVector);
		}

		/// <summary>
		/// Returns a local point relative to the body's origin given a world point.
		/// </summary>
		/// <param name="worldPoint">A point in world coordinates.</param>
		/// <returns>The corresponding local point relative to the body's origin.</returns>
		public Vector2 GetLocalPoint(Vector2 worldPoint)
		{
			if (!worldPoint.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(worldPoint));

			Transform.MultiplyTransposed(in this.transform, in worldPoint, out Vector2 localPoint);
			return localPoint;
		}

		/// <summary>
		/// Returns a local vector given a world vector.
		/// </summary>
		/// <param name="worldVector">A vector in world coordinates.</param>
		/// <returns>The corresponding local vector.</returns>
		public Vector2 GetLocalVector(Vector2 worldVector)
		{
			if (!worldVector.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(worldVector));

			return Rotation.MultiplyTransposed(in this.transform.Q, in worldVector);
		}

		/// <summary>
		/// Returns the world linear velocity of a world point attached to this body.
		/// </summary>
		/// <param name="worldPoint">A point in world coordinates.</param>
		/// <returns>The world velocity of the point.</returns>
		public Vector2 GetLinearVelocityFromWorldPoint(Vector2 worldPoint)
		{
			if (!worldPoint.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(worldPoint));

			Vector2 diff = worldPoint - this.sweep.Center;
			return this.linearVelocity + Vector2.Cross(this.angularVelocity, in diff);
		}

		/// <summary>
		/// Gets the world velocity of a local point.
		/// </summary>
		/// <param name="localPoint">A point in local coordinates.</param>
		/// <returns>The world velocity of the point.</returns>
		public Vector2 GetLinearVelocityFromLocalPoint(Vector2 localPoint)
		{
			if (!localPoint.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(localPoint));

			return this.GetLinearVelocityFromWorldPoint(this.GetWorldPoint(localPoint));
		}

		#endregion

		#region Forces

		/// <summary>
		/// Apply a force at a world point. If the force is not
		/// applied at the center of mass, it will generate a torque and
		/// affect the angular velocity. Optionally wakes up the body.
		/// </summary>
		/// <param name="force">The world force vector, usually in Newtons (N).</param>
		/// <param name="point">The world position of the point of application.</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyForce(in Vector2 force, in Vector2 point, bool wake)
		{
			if (!force.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(force));

			if (!point.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(point));

			if (this.type != BodyType.Dynamic)
				return;

			if (wake || this.Awake)
			{
				this.Awake = true;
				Vector2.Add(this.force, force, out this.force);
				this.torque += Vector2.Cross(point - this.sweep.Center, force);
			}
		}

		/// <summary>
		/// Apply a force at a world point. If the force is not
		/// applied at the center of mass, it will generate a torque and
		/// affect the angular velocity. Optionally wakes up the body.
		/// </summary>
		/// <param name="force">The world force vector, usually in Newtons (N).</param>
		/// <param name="point">The world position of the point of application.</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyForce(Vector2 force, Vector2 point, bool wake)
		{
			this.ApplyForce(in force, in point, wake);
		}

		/// <summary>
		/// Apply a force to the center of mass. Optionally wakes up the body.
		/// </summary>
		/// <param name="force">The world force vector, usually in Newtons (N).</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyForceToCenter(in Vector2 force, bool wake)
		{
			if (!force.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(force));

			if (this.type != BodyType.Dynamic)
				return;

			if (wake || this.Awake)
			{
				this.Awake = true;
				Vector2.Add(this.force, force, out this.force);
			}
		}

		/// <summary>
		/// Apply a force to the center of mass. Optionally wakes up the body.
		/// </summary>
		/// <param name="force">The world force vector, usually in Newtons (N).</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyForceToCenter(Vector2 force, bool wake)
		{
			this.ApplyForceToCenter(in force, wake);
		}

		/// <summary>
		/// Apply a torque. This affects the angular velocity
		/// without affecting the linear velocity of the center of mass.
		/// Optionally wakes up the body.
		/// </summary>
		/// <param name="torque">Torque about the z-axis (out of the screen), usually in N-m.</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyTorque(float torque, bool wake)
		{
			if (!torque.IsFinite())
				throw new ArgumentException("Invalid value.", nameof(torque));

			if (this.type != BodyType.Dynamic)
				return;

			if (wake || this.Awake)
			{
				this.Awake = true;
				this.torque += torque;
			}
		}

		/// <summary>
		/// Apply an impulse at a point. This immediately modifies the velocity.
		/// It also modifies the angular velocity if the point of application
		/// is not at the center of mass. Optionally wakes up the body.
		/// </summary>
		/// <param name="impulse">The world impulse vector, usually in N-seconds or kg-m/s.</param>
		/// <param name="point">The world position of the point of application.</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyLinearImpulse(in Vector2 impulse, in Vector2 point, bool wake)
		{
			if (!impulse.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(impulse));

			if (!point.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(point));

			if (this.type != BodyType.Dynamic)
				return;

			if (wake || this.Awake)
			{
				this.Awake = true;
				this.linearVelocity += this.invMass * impulse;
				this.angularVelocity += this.invInertia * Vector2.Cross(point - this.sweep.Center, in impulse);
			}
		}

		/// <summary>
		/// Apply an impulse at a point. This immediately modifies the velocity.
		/// It also modifies the angular velocity if the point of application
		/// is not at the center of mass. Optionally wakes up the body.
		/// </summary>
		/// <param name="impulse">The world impulse vector, usually in N-seconds or kg-m/s.</param>
		/// <param name="point">The world position of the point of application.</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyLinearImpulse(Vector2 impulse, Vector2 point, bool wake)
		{
			this.ApplyForce(in impulse, in point, wake);
		}

		/// <summary>
		/// Apply an angular impulse.
		/// </summary>
		/// <param name="impulse">The angular impulse in units of kg*m*m/s</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyAngularImpulse(float impulse, bool wake)
		{
			if (!impulse.IsFinite())
				throw new ArgumentException("Invalid value.", nameof(impulse));

			if (this.type != BodyType.Dynamic)
				return;

			if (wake || this.Awake)
			{
				this.Awake = true;
				this.angularVelocity += this.invInertia * impulse;
			}
		}

		/// <summary>
		/// Apply an impulse to the center of mass. This immediately modifies the velocity.
		/// </summary>
		/// <param name="impulse">The world impulse vector, usually in N-seconds or kg-m/s.</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyLinearImpulseToCenter(in Vector2 impulse, bool wake)
		{
			if (!impulse.IsFinite)
				throw new ArgumentException("Invalid value.", nameof(impulse));

			if (this.type != BodyType.Dynamic)
				return;

			if (wake || this.Awake)
			{
				this.Awake = true;
				this.linearVelocity += this.invMass * impulse;
			}
		}

		/// <summary>
		/// Apply an impulse to the center of mass. This immediately modifies the velocity.
		/// </summary>
		/// <param name="impulse">The world impulse vector, usually in N-seconds or kg-m/s.</param>
		/// <param name="wake">Determines whether the body should be woken up.</param>
		public void ApplyLinearImpulseToCenter(Vector2 impulse, bool wake)
		{
			this.ApplyLinearImpulseToCenter(in impulse, wake);
		}

		#endregion

		#region Fixture

		/// <summary>
		/// This resets the mass properties to the sum of the mass properties of the fixtures.
		/// This normally does not need to be called unless you called SetMassData to override
		/// the mass and you later want to reset the mass.
		/// </summary>
		public void ResetMassData()
		{
			// Compute mass data from shapes. Each shape has its own density.
			this.mass = 0.0f;
			this.invMass = 0.0f;
			this.inertia = 0.0f;
			this.invInertia = 0.0f;
			this.sweep.LocalCenter = Vector2.Zero;

			// Static and kinematic bodies have zero mass.
			if (this.type == BodyType.Static || this.type == BodyType.Kinematic)
			{
				this.sweep.Center0 = this.transform.P;
				this.sweep.Center = this.transform.P;
				this.sweep.Angle0 = this.sweep.Angle;
				return;
			}

			// Accumulate mass over all fixtures.
			Vector2 localCenter = Vector2.Zero;
			foreach (Fixture f in this.fixtures)
			{
				if (f.density <= float.Epsilon)
					continue;

				MassData massData = f.MassData;
				this.mass += massData.Mass;
				localCenter += massData.Mass * massData.Center;
				this.inertia += massData.Inertia;
			}

			// Compute center of mass.
			if (this.mass > 0.0f)
			{
				this.invMass = 1.0f / this.mass;
				localCenter *= this.invMass;
			}
			else
			{
				// Force all dynamic bodies to have a positive mass.
				this.mass = 1.0f;
				this.invMass = 1.0f;
			}

			if (this.inertia > 0.0f && (this.flags & BodyFlags.FixedRotation) == 0)
			{
				// Center the inertia about the center of mass.
				this.inertia -= this.mass * Vector2.Dot(localCenter, localCenter);
				Debug.Assert(this.inertia > 0.0f);
				this.invInertia = 1.0f / this.inertia;
			}
			else
			{
				this.inertia = 0.0f;
				this.invInertia = 0.0f;
			}

			// Move center of mass.
			Vector2 oldCenter = this.sweep.Center;
			this.sweep.LocalCenter = localCenter;
			Transform.Multiply(in this.transform, in this.sweep.LocalCenter, out this.sweep.Center);
			this.sweep.Center0 = this.sweep.Center;

			// Update center of mass velocity.
			Vector2 diff = this.sweep.Center - oldCenter;
			this.linearVelocity += Vector2.Cross(this.angularVelocity, in diff);
		}

		#endregion

		#region Internal

		/// <summary>
		/// Gets the body state.
		/// </summary>
		/// <param name="state"></param>
		internal void GetState(out BodyState state)
		{
			state.Position = this.transform.P;
			state.Angle = this.sweep.Angle;
			state.LinearVelocity = this.linearVelocity;
			state.AngularVelocity = this.angularVelocity;
		}

		internal void DestroyContacts()
		{
			Contact[] allContacts = this.contacts.GetContacts().ToArray();
			foreach (Contact contact in allContacts)
			{
				// A contact might get destroyed from an event fired by a different contract.
				// Since we create a copy of all contacts in the line above, the removal is not reflected in this loop,
				// so we have to check for this condition manually.
				if (contact.IsDestroyed)
					continue;

				this.world.contactManager.Remove(contact);
			}
		}

		internal void Advance(float alpha)
		{
			// Advance to the new safe time. This doesn't sync the broad-phase.
			this.sweep.Advance(alpha);

			this.sweep.Center = this.sweep.Center0;
			this.sweep.Angle = this.sweep.Angle0;

			this.SynchronizeTransform();
		}

		internal bool ShouldCollide(Body other)
		{
			// At least one body should be dynamic.
			if (this.type != BodyType.Dynamic && other.type != BodyType.Dynamic)
				return false;

			// Does a joint prevent collision?
			for (int i = 0; i < this.jointEdges.Count; i++)
			{
				if (this.jointEdges[i].Other == other)
				{
					if (!this.jointEdges[i].Joint.collideConnected)
						return false;
				}
			}

			return true;
		}

		internal void SynchronizeTransform()
		{
			this.transform.Q.S = (float)System.Math.Sin(this.sweep.Angle);
			this.transform.Q.C = (float)System.Math.Cos(this.sweep.Angle);
			this.transform.P.X = this.sweep.Center.X - (this.transform.Q.C * this.sweep.LocalCenter.X - this.transform.Q.S * this.sweep.LocalCenter.Y);
			this.transform.P.Y = this.sweep.Center.Y - (this.transform.Q.S * this.sweep.LocalCenter.X + this.transform.Q.C * this.sweep.LocalCenter.Y);
		}

		internal void SynchronizeFixtures()
		{
			Transform xf1;
			xf1.Q.S = (float)System.Math.Sin(this.sweep.Angle0);
			xf1.Q.C = (float)System.Math.Cos(this.sweep.Angle0);
			xf1.P.X = this.sweep.Center0.X - (xf1.Q.C * this.sweep.LocalCenter.X - xf1.Q.S * this.sweep.LocalCenter.Y);
			xf1.P.Y = this.sweep.Center0.Y - (xf1.Q.S * this.sweep.LocalCenter.X + xf1.Q.C * this.sweep.LocalCenter.Y);

			BroadPhase broadPhase = this.world.contactManager.broadPhase;
			foreach (Fixture f in this.fixtures)
			{
				f.Synchronize(broadPhase, in xf1, in this.transform);
			}
		}

		#endregion

		#endregion
	}
}
