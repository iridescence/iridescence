﻿using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Called for each fixture found in the query. You control how the ray cast using the return value.
	/// </summary>
	/// <param name="fixture">the fixture hit by the ray.</param>
	/// <param name="point">the point of initial intersection.</param>
	/// <param name="normal">the normal vector at the point of intersection.</param>
	/// <param name="fraction">The fraction at which the fixture was hit.</param>
	/// <returns>
	/// -1.0: ignore this fixture and continue.
	/// 0.0: terminate the ray cast.
	/// between 0.0 and 1.0: clip the ray to this point.
	/// 1.0: don't clip the ray and continue.
	/// </returns>
	public delegate float RayCastCallback(Fixture fixture, Vector2 point, Vector2 normal, float fraction);

	/// <summary>
	/// Called for each fixture found in the query AABB.
	/// </summary>
	/// <param name="fixture"></param>
	/// <returns>False to terminate the query. True to continue.</returns>
	public delegate bool QueryCallback(Fixture fixture);
}
