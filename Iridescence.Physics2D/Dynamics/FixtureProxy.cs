﻿using Iridescence.Physics2D.Collision;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// This proxy is used internally to connect fixtures to the broad-phase.
	/// </summary>
	internal sealed class FixtureProxy
	{
		#region Fields

		public AABB AABB;
		public Fixture Fixture;
		public int ChildIndex;
		public int ProxyId;

		#endregion

		#region Constructors

		public FixtureProxy()
		{
			this.ProxyId = -1;
		}

		#endregion
	}
}
