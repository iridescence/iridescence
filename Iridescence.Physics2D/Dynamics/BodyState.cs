using System;
using System.Diagnostics;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Represents the state of a body, consisting of it's position and angle as well as it's linear and angular velocity.
	/// </summary>
	[Serializable]
	[DebuggerDisplay("P: {Position.X} {Position.Y} {Angle} V: {LinearVelocity.X} {LinearVelocity.Y} {AngularVelocity}")]
	public struct BodyState : IEquatable<BodyState>
	{
		#region Fields

		public Vector2 Position;
		public float Angle;

		public Vector2 LinearVelocity;
		public float AngularVelocity;

		#endregion

		#region Constructors

		public BodyState(Vector2 position, float angle)
		{
			this.Position = position;
			this.Angle = angle;
			this.LinearVelocity = Vector2.Zero;
			this.AngularVelocity = 0.0f;
		}

		public BodyState(Vector2 position, float angle, Vector2 linearVelocity, float angularVelocity)
		{
			this.Position = position;
			this.Angle = angle;
			this.LinearVelocity = linearVelocity;
			this.AngularVelocity = angularVelocity;
		}

		#endregion

		#region Methods

		public static bool ApproximatelyEquals(in BodyState a, in BodyState b, float positionTolerance, float angleTolerance, float linearVelocityTolerance, float angularVelocityTolerance)
		{
			return a.Position.ApproximatelyEquals(b.Position, positionTolerance) &&
			       Utility.ApproximatelyEquals(a.Angle, b.Angle, angleTolerance) &&
				   a.LinearVelocity.ApproximatelyEquals(b.LinearVelocity, linearVelocityTolerance) &&
			       Utility.ApproximatelyEquals(a.AngularVelocity, b.AngularVelocity, angularVelocityTolerance);
		}

		public static bool Equals(in BodyState a, in BodyState b)
		{
			return a.Position.X == b.Position.X &&
				   a.Position.Y == b.Position.Y &&
				   a.Angle == b.Angle &&
				   a.LinearVelocity.X == b.LinearVelocity.X &&
				   a.LinearVelocity.Y == b.LinearVelocity.Y &&
				   a.AngularVelocity == b.AngularVelocity;
		}

		public bool Equals(in BodyState other)
		{
			return Equals(this, other);
		}
		
		bool IEquatable<BodyState>.Equals(BodyState other) => this.Equals(other);

		public override bool Equals(object obj)
		{
			if (!(obj is BodyState))
				return false;

			BodyState other = (BodyState)obj;
			return Equals(this, other);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(this.Position.X, this.Position.Y, this.Angle, this.LinearVelocity.X, this.LinearVelocity.Y, this.AngularVelocity);
		}
		
		/// <summary>
		/// Interpolates two body states.
		/// </summary>
		/// <param name="a">The first state.</param>
		/// <param name="b">The second state.</param>
		/// <param name="t">The interpolation amount (0 .. 1).</param>
		/// <param name="result">The result.</param>
		public static void Lerp(in BodyState a, in BodyState b, float t, out BodyState result)
		{
			float t2 = 1.0f - t;
			result.Position.X = a.Position.X * t2 + b.Position.X * t;
			result.Position.Y = a.Position.Y * t2 + b.Position.Y * t;
			result.Angle = a.Angle * t2 + b.Angle * t;
			result.LinearVelocity.X = a.LinearVelocity.X * t2 + b.LinearVelocity.X * t;
			result.LinearVelocity.Y = a.LinearVelocity.Y * t2 + b.LinearVelocity.Y * t;
			result.AngularVelocity = a.AngularVelocity * t2 + b.AngularVelocity * t;
		}

		public override string ToString()
		{
			return $"Position: {this.Position}, Angle: {this.Angle}, LinVel: {this.LinearVelocity}, AngVel: {this.AngularVelocity}";
		}
		
		#endregion
	}
}