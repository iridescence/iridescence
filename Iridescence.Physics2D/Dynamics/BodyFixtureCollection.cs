﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Physics2D.Collision;
using Iridescence.Physics2D.Dynamics.Contacts;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Contains the fixtures of a body.
	/// </summary>
	public sealed class BodyFixtureCollection : IReadOnlyList<Fixture>, INotifyListChanged<Fixture>
	{
		#region Events

		public event CollectionChangedEventHandler<ListItem<Fixture>> CollectionChanged;

		#endregion

		#region Fields

		private readonly List<Fixture> fixtures;
		
		#endregion
		
		#region Properties
		
		public Body Body { get; }

		public World World => this.Body.World;

		public int Count => this.fixtures.Count;

		public Fixture this[int index] => this.fixtures[index];

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="BodyFixtureCollection"/> class. 
		/// </summary>
		internal BodyFixtureCollection(Body body)
		{
			this.fixtures = new List<Fixture>();
			this.Body = body;
		}

		#endregion
		
		#region Methods

		/// <summary>
		/// Creates a fixture and attach it to this body. Use this function if you need
		/// to set some fixture parameters, like friction. Otherwise you can create the
		/// fixture directly from a shape. If the density is non-zero, this function 
		/// automatically updates the mass of the body. Contacts are not created until
		/// the next time step.
		/// </summary>
		/// <param name="def">The fixture descriptor.</param>
		/// <returns></returns>
		public Fixture Add(FixtureDescriptor def)
		{
			Fixture fixture = new Fixture(this.Body, def);

			if ((this.Body.flags & BodyFlags.Active) != 0)
			{
				BroadPhase broadPhase = this.World.contactManager.broadPhase;
				fixture.CreateProxies(broadPhase, this.Body.transform);
			}

			this.fixtures.Add(fixture);

			// Adjust mass properties if needed.
			if (fixture.density > 0.0f)
				this.Body.ResetMassData();

			// Let the world know we have a new fixture. This will cause new contacts
			// to be created at the beginning of the next time step.
			this.World.flags |= WorldFlags.NewFixture;

			this.CollectionChanged?.Invoke(this, ListChange.Add(this.fixtures.Count - 1, fixture));

			return fixture;
		}

		/// <summary>
		/// Destroy a fixture. This removes the fixture from the broad-phase and
		/// destroys all contacts associated with this fixture. This will
		/// automatically adjust the mass of the body if the body is dynamic and the
		/// fixture has positive density. All fixtures attached to a body are
		/// implicitly destroyed when the body is destroyed.
		/// </summary>
		/// <param name="fixture">The fixture.</param>
		public bool Remove(Fixture fixture)
		{
			int index = this.fixtures.IndexOf(fixture);
			if(index < 0)
				return false;

			this.fixtures.RemoveAt(index);

			// Destroy any contacts associated with the fixture.
			foreach (Contact c in this.Body.contacts.GetContacts().Where(c => fixture == c.fixtureA || fixture == c.fixtureB).ToArray())
			{
				// This destroys the contact and removes it from this body's contact list.
				this.World.contactManager.Remove(c);
			}

			if ((this.Body.flags & BodyFlags.Active) != 0 && fixture.proxiesCreated)
			{
				BroadPhase broadPhase = this.World.contactManager.broadPhase;
				fixture.DestroyProxies(broadPhase);
			}

			// Reset the mass data.
			this.Body.ResetMassData();

			this.CollectionChanged?.Invoke(this, ListChange.Remove(index, fixture));
			
			return true;
		}

		/// <summary>
		/// Removes all fixtures from this body.
		/// </summary>
		public void Clear()
		{
			Fixture[] oldFixtures = this.fixtures.ToArray();

			this.fixtures.Clear();

			foreach (Contact c in this.Body.contacts.GetContacts().ToArray())
			{
				// This destroys the contact and removes it from this body's contact list.
				this.World.contactManager.Remove(c);
			}

			if ((this.Body.flags & BodyFlags.Active) != 0)
			{
				BroadPhase broadPhase = this.World.contactManager.broadPhase;
				foreach (Fixture fixture in oldFixtures)
				{
					fixture.DestroyProxies(broadPhase);
				}
			}

			// Reset the mass data.
			this.Body.ResetMassData();

			this.CollectionChanged?.Invoke(this, ListChange.Clear(oldFixtures));
		}
		
		public IEnumerator<Fixture> GetEnumerator()
		{
			return this.fixtures.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		#endregion
	}
}
