using System.Diagnostics;
using Iridescence.Math;
using Iridescence.Physics2D.Collision;
using Iridescence.Physics2D.Collision.Shapes;
using Iridescence.Physics2D.Dynamics.Contacts;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// A fixture is used to attach a shape to a body for collision detection. A fixture
	/// inherits its transform from its parent. Fixtures hold additional non-geometric data
	/// such as friction, collision filters, etc.
	/// </summary>
	public sealed class Fixture
	{
		#region Events

		/// <summary>
		/// Occurs when this fixture begins to touch another fixture.
		/// </summary>
		public event ContactEventHandler BeginContact;

		/// <summary>
		/// Occurs when this fixture ceases to touch another fixture.
		/// </summary>
		public event ContactEventHandler EndContact;

		/// <summary>
		/// Occurs before a contact manifold is solved.
		/// </summary>
		public event ContactPreSolveEventHandler ContactPreSolve;

		/// <summary>
		/// Occurs after a contact manifold ist solved.
		/// </summary>
		public event ContactPostSolveEventHandler ContactPostSolve;

		#endregion

		#region Fields

		internal readonly Body body;
		internal readonly Shape shape;

		internal float density;
		internal float friction;
		internal float restitution;

		internal bool proxiesCreated;
		internal FixtureProxy[] proxies;
		
		internal bool sensor;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the parent body of this fixture. This is NULL if the fixture is not attached.
		/// </summary>
		public Body Body => this.body;

		/// <summary>
		/// Gets the fixture's shape.
		/// </summary>
		public Shape Shape => this.shape;

		/// <summary>
		/// Returns the shape's computed mass data.
		/// </summary>
		public MassData MassData => this.shape.ComputeMass(this.density);

		/// <summary>
		/// Gets or sets the fixture's density.
		/// </summary>
		public float Density
		{
			get => this.density;
			set => this.density = value;
		}

		/// <summary>
		/// Gets or sets the fixture's friction.
		/// </summary>
		public float Friction
		{
			get => this.friction;
			set => this.friction = value;
		}

		/// <summary>
		/// Gets or sets the restituation (elasticity) of the fixture.
		/// </summary>
		public float Restitution
		{
			get => this.restitution;
			set => this.restitution = value;
		}

		/// <summary>
		/// Gets or sets a value that determines whether this fixture is a sensor.
		/// </summary>
		public bool Sensor
		{
			get => (this.sensor);
			set
			{
				if (value != this.sensor)
				{
					this.body.Awake = true;
					this.sensor = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets a user object.
		/// </summary>
		public object UserData { get; set; }
		
		#endregion

		#region Constructors
		
		internal Fixture(Body body, FixtureDescriptor def)
		{
			this.UserData = def.UserData;
			this.friction = def.Friction;
			this.restitution = def.Restitution;

			this.body = body;

			this.sensor = def.Sensor;

			this.shape = def.Shape;

			// Reserve proxy space
			int childCount = this.shape.ChildCount;

			this.proxies = new FixtureProxy[childCount];
			for (int i = 0; i < childCount; ++i)
				this.proxies[i] = new FixtureProxy();

			this.density = def.Density;
		}

		#endregion

		#region Methods

		internal void InternalBeginContact(Contact c)
		{
			this.BeginContact?.Invoke(c);
		}

		internal void InternalEndContact(Contact c)
		{
			this.EndContact?.Invoke(c);
		}

		internal void InternalContactPreSolve(Contact c, Manifold m)
		{
			this.ContactPreSolve?.Invoke(c, m);
		}

		internal void InternalContactPostSolve(Contact c, ref ContactImpulse i)
		{
			this.ContactPostSolve?.Invoke(c, ref i);
		}

		/// <summary>
		/// Returns a value that indicates whether the specified point lies within the fixture.
		/// </summary>
		/// <param name="p">The point in world coordinates.</param>
		/// <returns></returns>
		public bool TestPoint(Vector2 p)
		{
			return this.shape.TestPoint(in this.body.transform, p);
		}

		/// <summary>
		/// Casts a ray against this fixture.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="childIndex"></param>
		/// <param name="output"></param>
		/// <returns></returns>
		public bool RayCast(in RayCastInput input, int childIndex, out RayCastOutput output)
		{
			return this.shape.RayCast(in input, in this.body.transform, childIndex, out output);
		}

		/// <summary>
		/// Gets the bounding bot of the specified child shape transform with this fixture's transform.
		/// </summary>
		/// <param name="childIndex"></param>
		/// <returns></returns>
		public AABB GetBoundingBox(int childIndex)
		{
			return this.proxies[childIndex].AABB;
		}

		internal void CreateProxies(BroadPhase broadPhase, Transform xf)
		{
			Debug.Assert(!this.proxiesCreated);

			// Create proxies in the broad-phase.
			for (int i = 0; i < this.proxies.Length; ++i)
			{
				FixtureProxy proxy = this.proxies[i];
				this.shape.ComputeBoundingBox(in xf, i, out proxy.AABB);
				proxy.Fixture = this;
				proxy.ChildIndex = i;
				proxy.ProxyId = broadPhase.CreateProxy(in proxy.AABB, proxy);
				this.proxies[i] = proxy;
			}

			this.proxiesCreated = true;
		}

		internal void DestroyProxies(BroadPhase broadPhase)
		{
			Debug.Assert(this.proxiesCreated);

			// Destroy proxies in the broad-phase.
			for (int i = 0; i < this.proxies.Length; ++i)
			{
				broadPhase.DestroyProxy(this.proxies[i].ProxyId);
				this.proxies[i].ProxyId = -1;
			}

			this.proxiesCreated = false;
		}

		internal void Synchronize(BroadPhase broadPhase, in Transform transform1, in Transform transform2)
		{
			if (!this.proxiesCreated)
				return;

			for (int i = 0; i < this.proxies.Length; ++i)
			{
				FixtureProxy proxy = this.proxies[i];

				this.shape.ComputeBoundingBox(in transform1, proxy.ChildIndex, out AABB aabb1);
				this.shape.ComputeBoundingBox(in transform2, proxy.ChildIndex, out AABB aabb2);

				proxy.AABB.Combine(in aabb1, in aabb2);

				Vector2 displacement;
				displacement.X = transform2.P.X - transform1.P.X;
				displacement.Y = transform2.P.Y - transform1.P.Y;

				broadPhase.MoveProxy(proxy.ProxyId, in proxy.AABB, in displacement);
			}
		}

		/// <summary>
		/// Updates the contact filters for this fixture.
		/// </summary>
		public void Refilter()
		{
			if (this.body == null)
				return;

			// Flag associated contacts for filtering.
			foreach (Contact contact in this.body.contacts.GetContacts())
			{
				if (contact.fixtureA == this || contact.fixtureB == this)
					contact.FlagForFiltering();
			}

			/*
			for(int i = 0; i < this.body.contactEdges.Count; i++)
			{
				Contact contact = this.body.contactEdges[i].Contact;
				if (contact.fixtureA == this || contact.fixtureB == this)
					contact.FlagForFiltering();
			}
			*/

			World world = this.body.World;
			if (world == null)
				return;

			if (this.proxiesCreated)
			{
				// Touch each proxy so that new pairs may be created
				BroadPhase broadPhase = world.contactManager.broadPhase;
				for (int i = 0; i < this.proxies.Length; ++i)
				{
					broadPhase.TouchProxy(this.proxies[i].ProxyId);
				}
			}
		}

		#endregion
	}
}
