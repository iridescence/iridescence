﻿using System;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Fixture event args.
	/// </summary>
	public sealed class FixtureEventArgs : EventArgs
	{
		#region Fields

		private readonly Fixture fixture;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the fixture.
		/// </summary>
		public Fixture Fixture => this.fixture;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FixtureEventArgs.
		/// </summary>
		public FixtureEventArgs(Fixture fixture)
		{
			this.fixture = fixture;
		}

		#endregion
	}
}
