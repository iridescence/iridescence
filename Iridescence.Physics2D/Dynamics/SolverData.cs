﻿namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Solver data.
	/// </summary>
	public sealed class SolverData
	{
		#region Fields

		internal TimeStep timeStep;
		internal BodyState[] states;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the time step.
		/// </summary>
		public TimeStep TimeStep => this.timeStep;

		/// <summary>
		/// Gets the body states.
		/// </summary>
		public BodyState[] States => this.states;

		#endregion

		#region Constructors

		internal SolverData()
		{

		}

		public SolverData(in TimeStep timeStep, int numStates)
		{
			this.timeStep = timeStep;
			this.states = new BodyState[numStates];
		}

		#endregion

		#region Methods

		#endregion
	}
}
