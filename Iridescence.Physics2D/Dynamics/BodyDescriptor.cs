using System;
using Iridescence.Math;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// A body descriptor holds all the data needed to construct a rigid body.
	/// You can safely re-use body definitions. Shapes are added to a body after construction.
	/// </summary>
	[Serializable]
	public sealed class BodyDescriptor
	{
		#region Properties

		/// <summary>
		/// Gets or sets the initial body type.
		/// </summary>
		public BodyType Type { get; set; }

		/// <summary>
		/// Gets or sets the position at which the body is placed into the world.
		/// </summary>
		public Vector2 Position { get; set; }

		/// <summary>
		/// Gets or sets the initial angle of the body.
		/// </summary>
		public float Angle { get; set; }

		/// <summary>
		/// Gets or sets the initial linear velocity of the body.
		/// </summary>
		public Vector2 LinearVelocity { get; set; }

		/// <summary>
		/// Gets or sets the initial angular velocity of the body.
		/// </summary>
		public float AngularVelocity { get; set; }

		/// <summary>
		/// Gets or sets a value that determines whether sleeping is enabled for the body.
		/// This can be changed later.
		/// </summary>
		public bool AllowSleep { get; set; }

		/// <summary>
		/// Gets or sets a value that determines whether the body is initially awake.
		/// </summary>
		public bool Awake { get; set; }

		/// <summary>
		/// Gets or sets a value that determines whether the rotation of the body is locked.
		/// This can be changed later.
		/// </summary>
		public bool FixedRotation { get; set; }

		/// <summary>
		/// Gets or sets a value that determines whether the body starts out as a bullet.
		/// Bullet bodies are prevented from tunneling through dynamic bodies. Note that all
		/// bodies are by default prevented from tunneling through static and kinematic bodies,
		/// but preventing them from tunneling though dynamic objects is more computationally
		/// intensive, so use this property only when needed.
		/// </summary>
		public bool IsBullet { get; set; }

		/// <summary>
		/// Gets or sets a value that determines whether the body starts out as active.
		/// </summary>
		public bool Active { get; set; }

		/// <summary>
		/// Gets or sets a user object.
		/// </summary>
		public object UserData { get; set; }

		#endregion

		#region Constructors

		public BodyDescriptor(BodyType type, BodyState state)
		{
			this.Type = type;
			this.Position = state.Position;
			this.Angle = state.Angle;
			this.LinearVelocity = state.LinearVelocity;
			this.AngularVelocity = state.AngularVelocity;
			this.AllowSleep = true;
			this.Awake = true;
			this.FixedRotation = false;
			this.IsBullet = false;
			this.Active = true;
			this.UserData = null;
		}

		public BodyDescriptor(BodyType type, Vector2 position, float angle)
			: this(type, new BodyState(position, angle))
		{

		}

		public BodyDescriptor(BodyType type)
			: this(type, Vector2.Zero, 0.0f)
		{
		}

		public BodyDescriptor()
			: this(BodyType.Static)
		{
		}

		#endregion
	}
}
