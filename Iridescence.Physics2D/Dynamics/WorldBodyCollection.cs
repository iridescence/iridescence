﻿using Iridescence.Physics2D.Dynamics.Joints;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Contains the bodies of a <see cref="World"/>.
	/// </summary>
	public sealed class WorldBodyCollection : IReadOnlyList<Body>, INotifyListChanged<Body>
	{
		#region Events

		public event CollectionChangedEventHandler<ListItem<Body>> CollectionChanged;

		#endregion

		#region Fields

		private readonly List<Body> bodies;

		#endregion
		
		#region Properties

		public World World { get; }

		public int Count => this.bodies.Count;
		
		public Body this[int index] => this.bodies[index];

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="WorldBodyCollection"/> class. 
		/// </summary>
		internal WorldBodyCollection(World world)
		{
			this.World = world;
			this.bodies = new List<Body>();
		}

		#endregion
		
		#region Methods

		/// <summary>
		/// Creates a body based on the descriptor and adds it to the world.
		/// </summary>
		/// <param name="def"></param>
		/// <returns></returns>
		public Body Add(BodyDescriptor def)
		{
			Body body = new Body(this.World, def);
			this.bodies.Add(body);

			this.CollectionChanged?.Invoke(this, ListChange.Add(this.bodies.Count - 1, body));

			return body;
		}

		/// <summary>
		/// Removes a body from the world.
		/// </summary>
		/// <param name="body"></param>
		public bool Remove(Body body)
		{
			int index = this.bodies.IndexOf(body);
			if (index < 0)
				return false;

			this.bodies.RemoveAt(index);

			// Delete the attached joints.
			for (int i = body.jointEdges.Count - 1; i >= 0; i--)
			{
				JointEdge je = body.jointEdges[i];
				this.World.Joints.Remove(je.Joint);
			}

			// Delete the attached contacts.
			body.DestroyContacts();

			// Delete the attached fixtures. This destroys broad-phase proxies.
			foreach (Fixture f in body.Fixtures)
			{
				f.DestroyProxies(this.World.contactManager.broadPhase);
			}

			this.CollectionChanged?.Invoke(this, ListChange.Remove(index, body));

			return true;
		}

		public IEnumerator<Body> GetEnumerator()
		{
			return this.bodies.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		#endregion


	}
}
