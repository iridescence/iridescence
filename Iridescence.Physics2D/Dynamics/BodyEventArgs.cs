﻿using System;

namespace Iridescence.Physics2D.Dynamics
{
	/// <summary>
	/// Body event args.
	/// </summary>
	public sealed class BodyEventArgs : EventArgs
	{
		#region Fields

		private readonly Body body;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the body.
		/// </summary>
		public Body Body => this.body;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BodyEventArgs.
		/// </summary>
		public BodyEventArgs(Body body)
		{
			this.body = body;
		}

		#endregion
	}
}
