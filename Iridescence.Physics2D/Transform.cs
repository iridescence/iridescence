using System;
using System.Runtime.CompilerServices;
using Iridescence.Math;

namespace Iridescence.Physics2D
{
	/// <summary>
	/// A transform contains translation and rotation. It is used to represent
	/// the position and orientation of rigid frames.
	/// </summary>
	public struct Transform : IEquatable<Transform>
	{
		#region Fields

		/// <summary>
		/// The identity transform. Does nothing when multiplied with another transform or a vector.
		/// </summary>
		public static readonly Transform Identity = new Transform(Vector2.Zero, Rotation.Identity);

		/// <summary>
		/// The translation.
		/// </summary>
		public Vector2 P;

		/// <summary>
		/// The rotation.
		/// </summary>
		public Rotation Q;

		#endregion

		#region Constructors

		/// <summary>
		/// Initialize using a position vector and a rotation.
		/// </summary>
		/// <param name="position"></param>
		/// <param name="rotation"></param>
		public Transform(in Vector2 position, in Rotation rotation)
		{
			this.P = position;
			this.Q = rotation;
		}

		/// <summary>
		/// Initializes the <see cref="Transform"/> using a position and angle (in radians).
		/// </summary>
		/// <param name="position"></param>
		/// <param name="angle"></param>
		public Transform(in Vector2 position, float angle)
		{
			this.P = position;
			this.Q = new Rotation(angle);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Set this based on the position and angle.
		/// </summary>
		/// <param name="position"></param>
		/// <param name="angle"></param>
		public void Set(Vector2 position, float angle)
		{
			this.P = position;
			this.Q.Angle = angle;
		}

		/// <summary>
		/// Multiplies (i.e. concatenates) the two transforms.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Multiply(in Transform a, in Transform b, out Transform result)
		{
			Rotation.Multiply(in a.Q, in b.Q, out Rotation q);
			Rotation.Multiply(in a.Q, in b.P, out Vector2 p);
			Vector2.Add(p, a.P, out result.P);
			result.Q = q;
		}

		/// <summary>
		/// Multiplies A transposed with B.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="result"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void MultiplyTransposed(in Transform a, in Transform b, out Transform result)
		{
			// v2 = A.q' * (B.q * v1 + B.p - A.p)
			//    = A.q' * B.q * v1 + A.q' * (B.p - A.p)
			Rotation.MultiplyTransposed(in a.Q, in b.Q, out Rotation q);
			Vector2.Subtract(b.P, a.P, out result.P);
			Rotation.MultiplyTransposed(in a.Q, in result.P, out result.P);
			result.Q = q;
		}

		/// <summary>
		/// Multiplies (i.e. applies) this transform to the specified vector.
		/// </summary>
		/// <param name="transform"></param>
		/// <param name="vector"></param>
		/// <param name="result"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Multiply(in Transform transform, in Vector2 vector, out Vector2 result)
		{
			float x = (transform.Q.C * vector.X - transform.Q.S * vector.Y) + transform.P.X;
			result.Y = (transform.Q.S * vector.X + transform.Q.C * vector.Y) + transform.P.Y;
			result.X = x;
		}

		/// <summary>
		/// Multiplies (i.e. applies) the transposed version of the transform to the specified vector.
		/// </summary>
		/// <param name="transform"></param>
		/// <param name="vector"></param>
		/// <param name="result"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void MultiplyTransposed(in Transform transform, in Vector2 vector, out Vector2 result)
		{
			float px = vector.X - transform.P.X;
			float py = vector.Y - transform.P.Y;
			result.X = (transform.Q.C * px + transform.Q.S * py);
			result.Y = (-transform.Q.S * px + transform.Q.C * py);
		}

		public bool Equals(in Transform other)
		{
			return this.P.Equals(other.P) && this.Q.Equals(other.Q);
		}

		bool IEquatable<Transform>.Equals(Transform other) => this.Equals(other);

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is Transform transform && this.Equals(transform);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(this.P.X, this.P.Y, this.Q.S, this.Q.C);
		}

		public static bool operator ==(in Transform left, in Transform right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(in Transform left, in Transform right)
		{
			return !left.Equals(right);
		}

		#endregion

	}
}
