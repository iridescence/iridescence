﻿namespace Iridescence.Serialization
{
	/// <summary>
	/// Defines the results of <see cref="IDeferredReferenceResolver{T,TIdentifier}.Get{TState}"/>.
	/// </summary>
	public enum DeferredReferencingResult
	{
		/// <summary>
		/// The object was retreived from the reference resolver.
		/// </summary>
		Retreived,

		/// <summary>
		/// The object could not be retreived.
		/// The callback is the first registered callback waiting for the object.
		/// </summary>
		Initial,

		/// <summary>
		/// The object could not be retreived.
		/// The callback was added to the other callback(s) waiting for the object.
		/// </summary>
		Enqueued
	}
}
