﻿using System.Collections.Generic;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents an abstract metadata attribute for <see cref="SerializationMember"/>.
	/// </summary>
	public interface ISerializationMemberAttribute
	{
		#region Methods

		/// <summary>
		/// Returns the attributes for the specified child member.
		/// </summary>
		/// <param name="parent">The parent member.</param>
		/// <param name="child">The child member of the parent.</param>
		/// <returns>An enumeration of attributes that the parent member applies to this child.</returns>
		IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child);

		#endregion
	}
}
