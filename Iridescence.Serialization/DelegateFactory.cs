﻿using System;
using System.Collections.Concurrent;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents a delegation creation function.
	/// </summary>
	/// <param name="target">The target object. If the method is static this parameter is ignored.</param>
	/// <returns>The delegate.</returns>
	public delegate TDelegate DelegateCreationFunc<out TDelegate>(object target) 
		where TDelegate : class /* Delegate */;
	
	/// <summary>
	/// Implements a factory for <see cref="Delegate">delegates</see>.
	/// </summary>
	public static class DelegateFactory
	{
		#region Fields

		private static readonly ConcurrentDictionary<Key, Value> cache = new ConcurrentDictionary<Key, Value>();

		#endregion

		#region Methods

		private static void emitCode(Type delegateType, MethodInfo method, ILBuilder emit)
		{
			Type declType = method.DeclaringType;
			if (declType == null)
				throw new ArgumentException("Method does not have a declaring type.", nameof(method));

			ConstructorInfo delegateConstructor = delegateType.GetConstructor(new[] {typeof(object), typeof(IntPtr)});
			if (delegateConstructor == null)
				throw new ArgumentException("Delegate does not have a constructor that takes a function pointer.", nameof(delegateType));

			if (method.IsStatic)
			{
				emit.LoadNull();
				emit.LoadFunctionPointer(method);
			}
			else if (!method.IsVirtual)
			{
				emit.LoadArgument(0);
				emit.CastClass(declType);
				emit.LoadFunctionPointer(method);
			}
			else
			{
				emit.LoadArgument(0);
				emit.CastClass(declType);
				emit.Duplicate();
				emit.LoadVirtualFunctionPointer(method);
			}
			emit.NewObject(delegateConstructor);
			emit.Return();
		}

		private static Value create(Type delegateType, MethodInfo method)
		{
			if (delegateType == null)
				throw new ArgumentNullException(nameof(delegateType));

			if (method == null)
				throw new ArgumentNullException(nameof(method));

			Type factoryType = typeof(DelegateCreationFunc<>).MakeGenericType(delegateType);

			DynamicMethodBuilder genericEmit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(factoryType), $"CreateDelegate<{delegateType.Name}>({method.Name})");
			emitCode(delegateType, method, genericEmit);
			Delegate generic = genericEmit.CreateDelegate(factoryType);

			DynamicMethodBuilder nonGenericEmit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(typeof(DelegateCreationFunc<Delegate>)), $"CreateDelegate({delegateType.Name},{method.Name})");
			emitCode(delegateType, method, nonGenericEmit);
			DelegateCreationFunc<Delegate> nonGeneric = (DelegateCreationFunc<Delegate>)nonGenericEmit.CreateDelegate(typeof(DelegateCreationFunc<Delegate>));
			
			return new Value(generic, nonGeneric);
		}

		/// <summary>
		/// Returns a factory that creates delegates of the specified type for the specified method.
		/// </summary>
		/// <param name="delegateType"></param>
		/// <param name="method"></param>
		/// <returns></returns>
		public static DelegateCreationFunc<Delegate> GetFactory(Type delegateType, MethodInfo method)
		{
			return cache.GetOrAdd(new Key(delegateType, method), key => create(key.DelegateType, key.Method)).NonGeneric;
		}

		/// <summary>
		/// Returns a factory that creates delegates of the specified type for the specified method.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		public static DelegateCreationFunc<TDelegate> GetFactory<TDelegate>(MethodInfo method)
			where TDelegate : class /* Delegate */
		{
			return (DelegateCreationFunc<TDelegate>)cache.GetOrAdd(new Key(typeof(TDelegate), method), key => create(key.DelegateType, key.Method)).Generic;
		}

		#endregion

		#region Nested Types

		private struct Key : IEquatable<Key>
		{
			public readonly Type DelegateType;
			public readonly MethodInfo Method;

			public Key(Type delegateType, MethodInfo method)
			{
				this.DelegateType = delegateType;
				this.Method = method;
			}

			public override int GetHashCode()
			{
				return this.DelegateType.GetHashCode() ^ this.Method.GetHashCode();
			}

			public bool Equals(Key other)
			{
				return other.DelegateType == this.DelegateType && other.Method == this.Method;
			}

			public override bool Equals(object obj)
			{
				if (!(obj is Key))
					return false;

				return this.Equals((Key)obj);
			}
		}

		private struct Value
		{
			public readonly Delegate Generic;
			public readonly DelegateCreationFunc<Delegate> NonGeneric;

			public Value(Delegate generic, DelegateCreationFunc<Delegate> nonGeneric)
			{
				this.Generic = generic;
				this.NonGeneric = nonGeneric;
			}
		}

		#endregion
	}
}
