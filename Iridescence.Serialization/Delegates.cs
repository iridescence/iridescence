﻿using System.IO;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents a method that writes a generic object to the specified <see cref="TOutput"/>.
	/// </summary>
	/// <typeparam name="TOutput">The type of the destination to write to, e.g. a <see cref="Stream"/>.</typeparam>
	/// <typeparam name="TObj">The type of the object to write.</typeparam>
	/// <param name="output">The destination to write to.</param>
	/// <param name="obj">The object to write.</param>
	public delegate void ObjectWriter<in TOutput, in TObj>(TOutput output, TObj obj);

	/// <summary>
	/// Represents a method that writes a generic object to the specified <see cref="TOutput"/>.
	/// The input is passed by-reference using an "in" parameter. This yields better performance when handling large structs.
	/// </summary>
	/// <typeparam name="TOutput">The type of the destination to write to, e.g. a <see cref="Stream"/>.</typeparam>
	/// <typeparam name="TObj">The type of the object to write.</typeparam>
	/// <param name="output">The destination to write to.</param>
	/// <param name="obj">The object to write.</param>
	public delegate void ObjectWriterRef<in TOutput, TObj>(TOutput output, ref TObj obj);

	/// <summary>
	/// Represents a method that reads a generic object from the specified <see cref="TSource"/>.
	/// </summary>
	/// <typeparam name="TInput">The type of the source to read from, e.g. a <see cref="Stream"/>.</typeparam>
	/// <typeparam name="TObj">The type of the resulting object..</typeparam>
	/// <param name="input">The source to read from.</param>
	/// <param name="obj">The resulting object.</param>
	public delegate void ObjectReader<in TInput, TObj>(TInput input, out TObj obj);

	/// <summary>
	/// Represents a callback for deferred object retrieval.
	/// The object and an optional user argument/state is passed to the function.
	/// </summary>
	/// <typeparam name="TObj">The type of the resulting object.</typeparam>
	/// <typeparam name="TState">The type of the user state argument.</typeparam>
	/// <param name="obj">The object.</param>
	/// <param name="state">A user argument.</param>
	public delegate void DeferredCallback<in TObj, in TState>(TObj obj, TState state);

	/// <summary>
	/// Represents a method that reads an object from the specified context in a deferred mode of execution, i.e. the result is returned by invoking a callback, not via the return value of the method.
	/// </summary>
	/// <typeparam name="TInput">The type of the source to read from, e.g. a <see cref="Stream"/>.</typeparam>
	/// <typeparam name="TObj">The type of the resulting object..</typeparam>
	/// <typeparam name="TState">The type of a user argument.</typeparam>
	/// <param name="input">The source to read from.</param>
	/// <param name="callback">
	/// The callback to invoke once the object becomes available.
	/// This callback is usually invoked as soon as the object reference is final.
	/// The object is not guaranteed to be completely deserialized. No assumptions about the contents of the object must be made before the whole read operation is complete.
	/// </param>
	/// <param name="state">A user argument to pass to the callback. Can be used to eliminate the need to allocate closures and delegates.</param>
	public delegate void ObjectReaderDeferred<in TInput, out TObj, TState>(TInput input, DeferredCallback<TObj, TState> callback, TState state);

	public readonly struct EmptyDeferredState
	{

	}

	public static class DelegateExtensions
	{
		/// <summary>
		/// Returns a by-value wrapper around a by-reference object writer.
		/// </summary>
		/// <typeparam name="TDest">The type of the destination to write to, e.g. a <see cref="Stream"/>.</typeparam>
		/// <typeparam name="TInput">The type of the object to write.</typeparam>
		/// <param name="writer"></param>
		/// <returns></returns>
		public static ObjectWriter<TDest, TInput> ByVal<TDest, TInput>(this ObjectWriterRef<TDest, TInput> writer) 
		{
			return (s, obj) => { writer(s, ref obj); };
		}
		
		/// <summary>
		/// Returns a <see cref="ObjectReader{TStream, T}"/> that wraps the specified <see cref="ObjectReaderDeferred{TSource,TResult,TState}"/>
		/// by waiting for the deferred callback to be invoked and returning the object that was supplied to it. If the deferred callback was not
		/// invoked at all, a <see cref="DeferException"/> is thrown.
		/// </summary>
		/// <typeparam name="TSource">The type of the source to read from, e.g. a <see cref="Stream"/>.</typeparam>
		/// <typeparam name="TResult">The type of the resulting object..</typeparam>
		/// <typeparam name="TState">The type of a user argument.</typeparam>
		/// <param name="deferredReader"></param>
		/// <returns>A "linear" reader.</returns>
		/// <exception cref="DeferException">The deferred reader did not invoke the supplied callback.</exception>
		public static ObjectReader<TSource, TResult> UnDefer<TSource, TResult, TState>(this ObjectReaderDeferred<TSource, TResult, TState> deferredReader)
		{
			return (TSource stream, out TResult obj) =>
			{
				bool isSet = false;
				TResult result = default;

				deferredReader(stream, (obj2, _) =>
				{
					result = obj2;
					isSet = true;
				}, default);

				if (!isSet)
				{
					throw new DeferException();
				}

				obj = result;
			};
		}
	}
}
