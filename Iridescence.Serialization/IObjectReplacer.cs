﻿namespace Iridescence.Serialization
{
	/// <summary>
	/// Interface that allows objects being replaced before they are written.
	/// </summary>
	public interface IObjectReplacer
	{
		/// <summary>
		/// Replaces an object.
		/// </summary>
		/// <param name="obj">The object to be written.</param>
		/// <returns>The object that should be written instead.</returns>
		object Replace(object obj);
	}
}
