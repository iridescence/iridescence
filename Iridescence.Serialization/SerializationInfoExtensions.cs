﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Iridescence.Serialization
{
	public static class SerializationInfoExtensions
	{
		/// <summary>
		/// Retreives a value from a <see cref="SerializationInfo"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="info"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public static T GetValue<T>(this SerializationInfo info, string name)
		{
			return (T)info.GetValue(name, typeof(T));
		}

		/// <summary>
		/// Returns an <see cref="IEnumerable{T}"/> that enumerates the specified <see cref="SerializationInfo"/>.
		/// </summary>
		/// <param name="info"></param>
		/// <returns></returns>
		public static IEnumerable<SerializationEntry> AsEnumerable(this SerializationInfo info)
		{
			foreach (SerializationEntry entry in info)
			{
				yield return entry;
			}
		}
	}
}