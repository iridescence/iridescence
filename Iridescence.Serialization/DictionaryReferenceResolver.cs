﻿using System;
using System.Collections.Generic;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Simple object reference provider implementation based on a dictionary.
	/// </summary>
	public sealed class DictionaryReferenceResolver<T> : IReferenceResolver<T, int>
		where T : class
	{
		#region Fields

		private readonly IDictionary<int, T> dictionary;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new DictionaryReferenceResolver.
		/// </summary>
		public DictionaryReferenceResolver()
			: this(new Dictionary<int, T>())
		{
			
		}
		
		/// <summary>
		/// Creates a new DictionaryReferenceResolver.
		/// </summary>
		public DictionaryReferenceResolver(IDictionary<int, T> dictionary)
		{
			this.dictionary = dictionary ?? throw new ArgumentNullException(nameof(dictionary));
		}

		#endregion

		#region Methods

		public void Add(int key, T obj)
		{
			if (key < 0)
				throw new ArgumentException(nameof(key));

			this.dictionary.Add(key, obj);
		}

		public bool TryGetValue(int key, out T value)
		{
			if (key == -1)
			{
				value = null;
				return true;
			}

			return this.dictionary.TryGetValue(key, out value);
		}
		
		#endregion
	}
}
