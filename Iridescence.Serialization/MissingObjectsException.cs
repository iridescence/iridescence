﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Exception thrown when one or more objects are missing.
	/// </summary>
	[Serializable]
	public class MissingObjectsException<TIdentifier> : DeferException
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the list of identifiers of the missing objects.
		/// </summary>
		public IReadOnlyList<TIdentifier> Identifiers { get; }

		#endregion

		#region Constructors

		protected MissingObjectsException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			this.Identifiers = info.GetValue<TIdentifier[]>("Identifiers");
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MissingObjectsException{TIdentifier}"/> class. 
		/// </summary>
		public MissingObjectsException(TIdentifier identifier)
			: base($"Object reference could not be resolved. Missing object: {identifier}")
		{
			this.Identifiers = new[] {identifier};
		}

		
		/// <summary>
		/// Initializes a new instance of the <see cref="MissingObjectsException{TIdentifier}"/> class. 
		/// </summary>
		public MissingObjectsException(IEnumerable<TIdentifier> identifiers)
			: base($"Object references could not be resolved. Missing objects: {string.Join(", ", identifiers)}")
		{
			this.Identifiers = identifiers.ToArray();
		}

		#endregion

		#region Methods

		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("MissingObjects", this.Identifiers.ToArray());
		}

		#endregion
	}
}
