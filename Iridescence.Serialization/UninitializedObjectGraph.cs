﻿using System;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents an uninitialized object graph returned by a deserializer.
	/// </summary>
	public class UninitializedObjectGraph : Lazy<object>
	{
		#region Fields

		/// <summary>
		/// A <see cref="UninitializedObjectGraph"/> that yields a null reference.
		/// </summary>
		public static readonly UninitializedObjectGraph Null = new UninitializedObjectGraph(null, () => null);

		#endregion

		#region Properties

		/// <summary>
		/// Gets the root object that has been deserialized.
		/// </summary>
		/// <remarks>
		/// Depending on whether the lazy value was created, the object graph might not be fully initialized.
		/// To initialize it, use the <see cref="Lazy{T}.Value"/> property instead.
		/// </remarks>
		public object UninitializedRoot { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="UninitializedObjectGraph"/> class. 
		/// </summary>
		public UninitializedObjectGraph(object root, Func<object> valueFactory)
			: base(valueFactory)
		{
			this.UninitializedRoot = root;
		}

		#endregion
	}
}
