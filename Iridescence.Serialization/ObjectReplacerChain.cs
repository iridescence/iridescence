﻿using System.Collections.Generic;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Chains multiple object replacers.
	/// </summary>
	public class ObjectReplacerChain : List<IObjectReplacer>, IObjectReplacer
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ObjectReplacerChain.
		/// </summary>
		public ObjectReplacerChain()
		{

		}

		/// <summary>
		/// Creates a new ObjectReplacerChain.
		/// </summary>
		public ObjectReplacerChain(IEnumerable<IObjectReplacer> replacers)
			: base(replacers)
		{

		}

		#endregion

		#region Methods

		public object Replace(object obj)
		{
			foreach (IObjectReplacer replacer in this)
			{
				obj = replacer.Replace(obj);
			}

			return obj;
		}
		
		#endregion
	}
}
