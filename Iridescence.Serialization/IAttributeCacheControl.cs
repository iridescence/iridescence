﻿namespace Iridescence.Serialization
{
	/// <summary>
	/// Can be used to control caching of a <see cref="ISerializationMemberAttribute"/>.
	/// </summary>
	public interface IAttributeCacheControl
	{
		/// <summary>
		/// Gets a value that determines whether this attribute should be included in a caching key.
		/// </summary>
		/// <remarks>
		/// This should be false for attributes that are purely metadata and do not ever change how a member is serialized.
		/// If this is false, a member that has a particular <see cref="ISerializationMemberAttribute"/> and a member that does not will look as if they are the same member to the serializer and use the same (de)serializer code.
		/// </remarks>
		bool IncludeInCache { get; }
	}
}
