﻿namespace Iridescence.Serialization
{
	/// <summary>
	/// Interface for classes that map objects to identifiers.
	/// </summary>
	public interface IReferenceProvider<in T, out TIdentifier>
	{
		#region Methods

		/// <summary>
		/// Returns the identifier for the specified object instance.
		/// </summary>
		/// <param name="obj">An object. Can be null, in which case a null identifier should be returned.</param>
		/// <param name="wasAdded">A value that indicates whether this object was known beforehand or had to be added and a new ID was assigned. Must be false if obj is null.</param>
		/// <returns>The identifier for the object.</returns>
		TIdentifier GetOrAdd(T obj, out bool wasAdded);

		#endregion
	}
}
