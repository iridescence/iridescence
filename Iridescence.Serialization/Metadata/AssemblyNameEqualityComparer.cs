﻿using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Compares <see cref="AssemblyName"/> instances for equality.
	/// </summary>
	public class AssemblyNameEqualityComparer : IEqualityComparer<AssemblyName>
	{
		public static bool AreEqual(AssemblyName x, AssemblyName y)
		{
			if (ReferenceEquals(x, y))
				return true;

			if (ReferenceEquals(x, null))
				return false;

			if (ReferenceEquals(y, null))
				return false;

			return x.FullName == y.FullName;
		}

		public bool Equals(AssemblyName x, AssemblyName y)
		{
			return AreEqual(x, y);
		}

		public int GetHashCode(AssemblyName obj)
		{
			return obj.FullName.GetHashCode();
		}
	}
}
