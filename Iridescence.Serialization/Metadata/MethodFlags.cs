﻿using System;

namespace Iridescence.Serialization
{
	[Flags]
	internal enum MethodFlags
	{
		None = 0,
		Static = 0x01,
	}
}
