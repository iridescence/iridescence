﻿using System;
using System.IO;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Adds metadata writing and reading capabilities to a <see cref="Stream"/>.
	/// </summary>
	public class CompactMetadataStream : StreamWrapper, IMetadataWriter, IMetadataReader
	{
		#region Fields

		private readonly CompactMetadataWriter writer;
		private readonly CompactMetadataReader reader;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CompactMetadataStream"/> class. 
		/// </summary>
		/// <param name="baseStream">The base stream.</param>
		/// <param name="leaveOpen">Whether to leave the base stream open when this stream is closed.</param>
		/// <param name="writer">The metadata writer. Can be null if writing is not supported.</param>
		/// <param name="reader">The metadata reader. Can be null if reading is not supported.</param>
		public CompactMetadataStream(Stream baseStream, bool leaveOpen, CompactMetadataWriter writer, CompactMetadataReader reader)
			: base(baseStream, leaveOpen)
		{
			this.writer = writer;
			this.reader = reader;
		}

		#endregion

		#region Methods

		public void WriteAssemblyName(AssemblyName name)
		{
			if (this.writer == null)
				throw new NotSupportedException("Writing metadata is not supported.");

			this.writer.WriteAssemblyName(this, name);
		}

		public void WriteMember(MemberInfo member)
		{
			if (this.writer == null)
				throw new NotSupportedException("Writing metadata is not supported.");

			this.writer.WriteMember(this, member);
		}

		public AssemblyName ReadAssemblyName()
		{
			if (this.reader == null)
				throw new NotSupportedException("Reading metadata is not supported.");

			return this.reader.ReadAssemblyName(this);
		}

		public MemberInfo ReadMember()
		{
			if (this.reader == null)
				throw new NotSupportedException("Reading metadata is not supported.");

			return this.reader.ReadMember(this);
		}

		#endregion
	}
}
