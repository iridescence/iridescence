﻿using System;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents a generic type parameter of a method.
	/// </summary>
	public class SignatureMethodGenericParameter : SignatureType, IEquatable<SignatureMethodGenericParameter>
	{
		#region Fields

		#endregion

		#region Properties

		public int Position { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignatureMethodGenericParameter"/> class. 
		/// </summary>
		public SignatureMethodGenericParameter(int position)
		{
			if (position < 0) throw new ArgumentOutOfRangeException(nameof(position));
			this.Position = position;
		}

		#endregion

		#region Methods

		public override bool TryResolve(MethodBase method, out Type type)
		{
			type = null;
			if (method == null)
				return false;

			Type[] genericArgs = method.GetGenericArguments();

			if (this.Position >= genericArgs.Length)
				return false;

			type = genericArgs[this.Position];
			return true;
		}

		public override string ToString()
		{
			return $"!!{this.Position}";
		}

		public bool Equals(SignatureMethodGenericParameter other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Position == other.Position;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((SignatureMethodGenericParameter)obj);
		}

		public override int GetHashCode()
		{
			return this.Position;
		}
		
		#endregion
	}
}
