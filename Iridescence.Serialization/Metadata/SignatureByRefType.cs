﻿using System;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents the by-ref type of a <see cref="SignatureType"/>.
	/// </summary>
	public class SignatureByRefType : SignatureType, IEquatable<SignatureByRefType>
	{
		#region Properties

		public SignatureType ElementType { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignatureByRefType"/> class. 
		/// </summary>
		public SignatureByRefType(SignatureType elementType)
		{
			this.ElementType = elementType ?? throw new ArgumentNullException(nameof(elementType));
		}

		#endregion

		#region Methods

		public override bool TryResolve(MethodBase method, out Type type)
		{
			type = null;
			if (!this.ElementType.TryResolve(method, out Type elementType))
				return false;

			type = elementType.MakeByRefType();
			return true;
		}

		public override string ToString()
		{
			return $"{this.ElementType}&";
		}

		public bool Equals(SignatureByRefType other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.ElementType.Equals(other.ElementType);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((SignatureByRefType)obj);
		}

		public override int GetHashCode()
		{
			return this.ElementType.GetHashCode();
		}
		
		#endregion
	}
}
