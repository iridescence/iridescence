﻿namespace Iridescence.Serialization
{
	internal enum AssemblyCode
	{
		Null,
		Reference,
		New,
	}
}
