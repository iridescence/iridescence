﻿using System;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents a simple <see cref="SignatureType"/> that wraps a normal <see cref="Type"/>.
	/// </summary>
	public class SignatureSimpleType : SignatureType, IEquatable<SignatureSimpleType>
	{
		#region Properties

		/// <summary>
		/// Gets the underlying <see cref="Type"/>.
		/// </summary>
		public Type Type { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignatureSimpleType"/> class. 
		/// </summary>
		public SignatureSimpleType(Type type)
		{
			this.Type = type ?? throw new ArgumentNullException(nameof(type));
		}

		#endregion

		#region Methods

		public override bool TryResolve(MethodBase method, out Type type)
		{
			type = this.Type;
			return true;
		}

		public override string ToString()
		{
			return this.Type.FullName ?? this.Type.Name;
		}

		public bool Equals(SignatureSimpleType other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Type == other.Type;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((SignatureSimpleType)obj);
		}

		public override int GetHashCode()
		{
			return this.Type.GetHashCode();
		}
		
		#endregion
	}
}
