﻿namespace Iridescence.Serialization
{
	internal enum SignatureCode
	{
		Null,
		SimpleType,
		ByRefType,
		PointerType,
		SzArrayType,
		MdArrayType,
		GenericType,
		TypeGenericParameter,
		MethodGenericParameter,
	}
}
