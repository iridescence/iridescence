﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents the signature of a method.
	/// </summary>
	public class Signature : IEquatable<Signature>
	{
		#region Properties

		/// <summary>
		/// Gets the method's return type.
		/// </summary>
		public SignatureType ReturnType { get; }

		/// <summary>
		/// Gets the method's parameter types.
		/// </summary>
		public IReadOnlyList<SignatureType> ParameterTypes { get; }

		/// <summary>
		/// Gets the number of type arguments the method has.
		/// </summary>
		public IReadOnlyList<SignatureType> TypeArguments { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Signature"/> that represents the signature of the specified <see cref="MethodBase" />.
		/// </summary>
		public Signature(MethodBase method)
		{
			if (method is MethodInfo info)
			{
				this.ReturnType = CreateSignatureType(info.ReturnType);
			}

			this.ParameterTypes = method.GetParameters().Select(p => CreateSignatureType(p.ParameterType)).ToArray();

			if (method.IsGenericMethod)
			{
				this.TypeArguments = method.GetGenericArguments().Select(CreateSignatureType).ToArray();	
			}
			else
			{
				this.TypeArguments = Array.Empty<SignatureType>();
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="Signature"/>.
		/// </summary>
		public Signature(SignatureType returnType, IEnumerable<SignatureType> parameterTypes, IEnumerable<SignatureType> typeArguments)
		{
			this.ReturnType = returnType;

			this.ParameterTypes = parameterTypes?.ToArray() ?? Array.Empty<SignatureType>();
			if (this.ParameterTypes.Any(t => t == null))
				throw new ArgumentException("Parameter type can not be null.", nameof(parameterTypes));

			this.TypeArguments = typeArguments?.ToArray() ?? Array.Empty<SignatureType>();
			if (this.TypeArguments.Any(t => t == null))
				throw new ArgumentException("Type argument can not be null.", nameof(typeArguments));
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a simplified <see cref="Signature"/> that contains as many <see cref="SignatureSimpleType">simple types</see> as possible.
		/// </summary>
		/// <returns></returns>
		public Signature Simplify()
		{
			return new Signature(
				this.ReturnType?.Simplify(), 
				this.ParameterTypes.Select(t => t.Simplify()),
				this.TypeArguments.Select(t => t.Simplify()));
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			str.Append('<');

			bool first = true;
			foreach (SignatureType type in this.TypeArguments)
			{
				if (first)
					first = false;
				else
					str.Append(", ");
				str.Append(type);
			}

			str.Append('>');

			str.Append('(');

			first = true;
			foreach (SignatureType type in this.ParameterTypes)
			{
				if (first)
					first = false;
				else
					str.Append(", ");
				str.Append(type);
			}

			str.Append(')');

			if (this.ReturnType != null)
			{
				str.Append(" => ");
				str.Append(this.ReturnType);
			}

			return str.ToString();
		}

		public bool Equals(Signature other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;

			if (other.ParameterTypes.Count != this.ParameterTypes.Count)
				return false;

			for (int i = 0; i < this.ParameterTypes.Count; ++i)
			{
				if (!Equals(this.ParameterTypes[i], other.ParameterTypes[i]))
					return false;
			}

			if (other.TypeArguments.Count != this.TypeArguments.Count)
				return false;
			
			for (int i = 0; i < this.TypeArguments.Count; ++i)
			{
				if (!Equals(this.TypeArguments[i], other.TypeArguments[i]))
					return false;
			}

			return Equals(this.ReturnType, other.ReturnType);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((Signature)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = this.ParameterTypes.Count * 397 + this.TypeArguments.Count;

				foreach (SignatureType type in this.ParameterTypes)
				{
					hash = (hash * 397) ^ type.GetHashCode();
				}

				foreach (SignatureType type in this.TypeArguments)
				{
					hash = (hash * 397) ^ type.GetHashCode();
				}

				hash = (hash * 397) ^ this.ReturnType?.GetHashCode() ?? 0;

				return hash;
			}
		}

		/// <summary>
		/// Returns the <see cref="SignatureType"/> for the specified <see cref="Type"/>.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static SignatureType CreateSignatureType(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (type.IsByRef)
			{
				return new SignatureByRefType(CreateSignatureType(type.GetElementType()));
			}

			if (type.IsPointer)
			{
				return new SignaturePointerType(CreateSignatureType(type.GetElementType()));
			}

			if (type.IsArray)
			{
				if(type.IsSzArray())
					return new SignatureSzArrayType(CreateSignatureType(type.GetElementType()));

				return new SignatureMdArrayType(CreateSignatureType(type.GetElementType()), type.GetArrayRank());
			}

			if (type.IsConstructedGenericType)
			{
				return new SignatureGenericType(CreateSignatureType(type.GetGenericTypeDefinition()), type.GetGenericArguments().Select(CreateSignatureType));
			}

			if (type.IsGenericParameter)
			{
				if (type.DeclaringMethod != null)
				{
					return new SignatureMethodGenericParameter(type.GenericParameterPosition);
				}

				return new SignatureTypeGenericParameter(CreateSignatureType(type.DeclaringType), type.GenericParameterPosition);
			}

			return new SignatureSimpleType(type);
		}

		#endregion
	}
}
