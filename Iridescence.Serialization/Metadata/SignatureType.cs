﻿using System;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents a type in a <see cref="Signature"/>.
	/// </summary>
	public abstract class SignatureType
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignatureType"/> class. 
		/// </summary>
		protected SignatureType()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Tries to resolve this <see cref="SignatureType"/> to the actual <see cref="Type"/>, based on the specified method.
		/// </summary>
		/// <param name="method">The method to use as a base for the resolve. Can be null to attempt a method-less resolve.</param>
		/// <param name="type">The resulting <see cref="Type"/>.</param>
		/// <returns>True, if the resolve succeeded. False otherwise.</returns>
		public abstract bool TryResolve(MethodBase method, out Type type);

		/// <summary>
		/// Returns a simplified version of this <see cref="SignatureType"/>.
		/// </summary>
		/// <returns></returns>
		public SignatureType Simplify()
		{
			if (this is SignatureSimpleType)
				return this;

			if (!this.TryResolve(null, out Type underlyingType))
				return this;

			return new SignatureSimpleType(underlyingType);
		}

		#endregion
	}
}
