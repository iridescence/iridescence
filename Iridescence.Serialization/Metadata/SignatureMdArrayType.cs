﻿using System;
using System.Reflection;
using System.Text;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents a multi-dimensional array of a <see cref="SignatureType"/>.
	/// </summary>
	public class SignatureMdArrayType : SignatureType, IEquatable<SignatureMdArrayType>
	{
		#region Properties

		public SignatureType ElementType { get; }

		public int Rank { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignatureMdArrayType"/> class. 
		/// </summary>
		public SignatureMdArrayType(SignatureType elementType, int rank)
		{
			if (rank <= 0)
				throw new ArgumentOutOfRangeException(nameof(rank));

			this.ElementType = elementType ?? throw new ArgumentNullException(nameof(elementType));
			this.Rank = rank;
		}

		#endregion

		#region Methods

		public override bool TryResolve(MethodBase method, out Type type)
		{
			type = null;
			if (!this.ElementType.TryResolve(method, out Type elementType))
				return false;

			type = elementType.MakeArrayType(this.Rank);
			return true;
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();
			str.Append(this.ElementType);
			str.Append('[');
			if (this.Rank == 1)
			{
				str.Append('*');
			}
			else
			{
				for (int i = 1; i < this.Rank; ++i)
				{
					str.Append(',');
				}
			}
			str.Append(']');
			return str.ToString();
		}


		public bool Equals(SignatureMdArrayType other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.ElementType.Equals(other.ElementType) && this.Rank == other.Rank; 
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((SignatureMdArrayType)obj);
		}

		public override int GetHashCode()
		{
			return this.ElementType.GetHashCode() ^ (this.Rank * 397);
		}

		#endregion
	}
}
