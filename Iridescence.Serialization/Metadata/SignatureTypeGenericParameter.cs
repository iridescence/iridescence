﻿using System;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents a generic type parameter of a method.
	/// </summary>
	public class SignatureTypeGenericParameter : SignatureType, IEquatable<SignatureTypeGenericParameter>
	{
		#region Properties

		public SignatureType Type { get; }

		public int Position { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignatureTypeGenericParameter"/> class. 
		/// </summary>
		public SignatureTypeGenericParameter(SignatureType type, int position)
		{
			if (position < 0) throw new ArgumentOutOfRangeException(nameof(position));
			this.Type = type ?? throw new ArgumentNullException(nameof(type));
			this.Position = position;
		}

		#endregion

		#region Methods

		public override bool TryResolve(MethodBase method, out Type type)
		{
			type = null;

			if (!this.Type.TryResolve(method, out Type declType))
				return false;

			Type[] genericArgs = declType.GetGenericArguments();
			if (this.Position >= genericArgs.Length)
				return false;

			type = genericArgs[this.Position];
			return true;
		}

		public override string ToString()
		{
			return $"!{this.Position}";
		}

		public bool Equals(SignatureTypeGenericParameter other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Type.Equals(other.Type) && this.Position == other.Position;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((SignatureTypeGenericParameter)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return this.Type.GetHashCode() ^ (this.Position * 397);
			}
		}

		#endregion
	}
}
