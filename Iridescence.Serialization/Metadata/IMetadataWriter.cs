﻿using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Interface for metadata writing.
	/// </summary>
	public interface IMetadataWriter
	{
		#region Methods

		/// <summary>
		/// Writes an assembly name.
		/// </summary>
		/// <param name="name"></param>
		void WriteAssemblyName(AssemblyName name);

		/// <summary>
		/// Writes any member (i.e. type, method, field, property, event).
		/// </summary>
		/// <param name="member"></param>
		void WriteMember(MemberInfo member);

		#endregion
	}
}
