﻿using System;
using System.Configuration.Assemblies;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Reads reflection metadata previously stored using <see cref="CompactMetadataReader"/>.
	/// </summary>
	public class CompactMetadataReader
	{
		#region Fields

		// Limits for sanity checking.
		private const int maxNameLength = 8192;
		private const int maxPublicKeyLength = 512;
		private const int maxPublicKeyTokenLength = 16;
		private const int maxTypeArguments = 128;
		private const int maxParameters = 1024;

		private readonly CompactMetadataOptions options;
		private readonly Func<AssemblyName, Assembly> loadAssembly;
		private readonly IReferenceResolver<AssemblyName, int> assemblyReferenceProvider;
		private readonly IReferenceResolver<MemberInfo, int> memberReferenceProvider;

		private const BindingFlags allFlags =
			BindingFlags.Public |
			BindingFlags.NonPublic |
			BindingFlags.Static |
			BindingFlags.Instance |
			BindingFlags.DeclaredOnly;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CompactMetadataReader"/> class. 
		/// </summary>
		public CompactMetadataReader(CompactMetadataOptions options, Func<AssemblyName, Assembly> assemblyResolver)
		{
			this.options = options;
			this.loadAssembly = assemblyResolver ?? throw new ArgumentNullException(nameof(assemblyResolver));
			this.assemblyReferenceProvider = new DictionaryReferenceResolver<AssemblyName>();
			this.memberReferenceProvider = new DictionaryReferenceResolver<MemberInfo>();
		}

		#endregion

		#region

		private static Exception paramMustBeZeroException<T>(T code)
		{
			return new InvalidDataException($"The op-code parameter must be zero for {typeof(T).Name} \"{code}\".");
		}

		private static Exception invalidOpCodeException<T>(T code)
		{
			return new InvalidDataException($"Invalid {typeof(T).Name} \"{code}\".");
		}

		public AssemblyName ReadAssemblyName(Stream stream)
		{
			stream.ReadCode(out AssemblyCode code, out int id);
			switch (code)
			{
				case AssemblyCode.Null:
					if (id != 0) throw paramMustBeZeroException(AssemblyCode.Null);
					return null;

				case AssemblyCode.Reference:
					return this.assemblyReferenceProvider.Get(id);

				case AssemblyCode.New:
					AssemblyName name = new AssemblyName();

					// Read name.
					name.Name = stream.ReadStringV(maxNameLength);

					// Read culture.
					int lcid = stream.ReadInt32V() - 1;
					if (lcid != -1)
					{
						name.CultureInfo = CultureInfo.GetCultureInfo(lcid);
					}

					// Read public key (token).
					int pktLen = stream.ReadInt32V() - 1;
					if (pktLen != -1)
					{
						if (pktLen < 0 || pktLen > maxPublicKeyTokenLength)
							throw new LimitExceededException();

						byte[] pkt = stream.ReadOrThrow(pktLen);
						if (pkt != null)
							name.SetPublicKeyToken(pkt);

					}

					if ((this.options & CompactMetadataOptions.FullAssemblyNames) != 0)
					{
						int pkLen = stream.ReadInt32V() - 1;
						if (pkLen != -1)
						{
							if (pkLen < 0 || pkLen > maxPublicKeyLength)
								throw new LimitExceededException();

							byte[] pk = stream.ReadOrThrow(pkLen);
							if (pk != null)
								name.SetPublicKey(pk);
						}
					}
					
					// Read version.
					int major = stream.ReadInt32V();
					int minor = stream.ReadInt32V();
					int build = stream.ReadInt32V();
					int revision = stream.ReadInt32V();
					name.Version = new Version(major, minor, build, revision);

					// Reads flags and other properties.
					name.HashAlgorithm = (System.Configuration.Assemblies.AssemblyHashAlgorithm)stream.ReadInt32V();
					name.VersionCompatibility = (AssemblyVersionCompatibility)stream.ReadInt32V();
					name.Flags = (AssemblyNameFlags)stream.ReadInt32V();
					name.ContentType = (AssemblyContentType)stream.ReadInt32V();

					// Add to reference resolver.
					this.assemblyReferenceProvider.Add(id, name);
					return name;

				default:
					throw invalidOpCodeException(code);
			}
		}

		public Assembly ReadAssembly(Stream stream)
		{
			AssemblyName name = this.ReadAssemblyName(stream);
			Assembly assembly = this.loadAssembly(name);
			if (assembly == null)
				throw new SerializationException($"Assembly \"{name}\" could not be loaded.");

			return assembly;
		}

		private SignatureType readSignatureType(Stream stream)
		{
			stream.ReadCode(out SignatureCode code, out int param);

			switch (code)
			{
				case SignatureCode.Null:
					if (param != 0) throw paramMustBeZeroException(SignatureCode.Null);
					return null;

				case SignatureCode.SimpleType:
					if (param != 0) throw paramMustBeZeroException(SignatureCode.Null);
					return new SignatureSimpleType(this.ReadType(stream));

				case SignatureCode.ByRefType:
					if (param != 0) throw paramMustBeZeroException(SignatureCode.Null);
					return new SignatureByRefType(this.readSignatureType(stream));

				case SignatureCode.PointerType:
					if (param != 0) throw paramMustBeZeroException(SignatureCode.Null);
					return new SignaturePointerType(this.readSignatureType(stream));

				case SignatureCode.SzArrayType:
					if (param != 0) throw paramMustBeZeroException(SignatureCode.Null);
					return new SignatureSzArrayType(this.readSignatureType(stream));

				case SignatureCode.MdArrayType:
					return new SignatureMdArrayType(this.readSignatureType(stream), param);

				case SignatureCode.GenericType:
					SignatureType typeDef = this.readSignatureType(stream);
					SignatureType[] args = new SignatureType[param];
					for (int i = 0; i < args.Length; ++i)
						args[i] = this.readSignatureType(stream);
					return new SignatureGenericType(typeDef, args);

				case SignatureCode.TypeGenericParameter:
					return new SignatureTypeGenericParameter(this.readSignatureType(stream), param);

				case SignatureCode.MethodGenericParameter:
					return new SignatureMethodGenericParameter(param);

				default:
					throw invalidOpCodeException(code);
			}
		}

		public Signature ReadSignature(Stream stream)
		{
			SignatureType returnType = this.readSignatureType(stream);

			int numParameters = stream.ReadInt32V();
			if (numParameters < 0 || numParameters > maxParameters)
				throw new LimitExceededException();

			SignatureType[] parameterTypes = new SignatureType[numParameters];
			for (int i = 0; i < parameterTypes.Length; ++i)
			{
				parameterTypes[i] = this.readSignatureType(stream);
			}

			int numTypeArgs = stream.ReadInt32V();
			if (numTypeArgs < 0 || numTypeArgs > maxTypeArguments)
				throw new LimitExceededException();

			SignatureType[] typeArguments = new SignatureType[numTypeArgs];
			for (int i = 0; i < typeArguments.Length; ++i)
			{
				typeArguments[i] = this.readSignatureType(stream);
			}

			return new Signature(returnType, parameterTypes, typeArguments);
		}

		private static BindingFlags getBindingFlags(MethodFlags flags)
		{
			BindingFlags b = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;

			if ((flags & MethodFlags.Static) != 0)
				b |= BindingFlags.Static;
			else
				b |= BindingFlags.Instance;

			return b;
		}

		public MemberInfo ReadMember(Stream stream)
		{
			stream.ReadCode(out MemberCode code, out int id);
			MemberInfo member;
			
			switch (code)
			{
				case MemberCode.Null:
					if (id != 0) throw paramMustBeZeroException(MemberCode.Null);
					return null;

				case MemberCode.Reference:
					return this.memberReferenceProvider.Get(id);

				case MemberCode.Type:
				{
					string ns = stream.ReadStringV(maxNameLength);
					string name = stream.ReadStringV(maxNameLength);
					Assembly assembly = this.ReadAssembly(stream);
					member = assembly.GetType($"{ns}.{name}", false, false);
					if (member == null || member.DeclaringType != null)
					{
						// Fall back in case .NET doesn't find the type. Should probably never occur.
						member = assembly.GetTypes().Single(t => t.DeclaringType == null && t.Namespace == ns && t.Name == name);	
					}
					break;
				}

				case MemberCode.TypeNested:
				{
					string ns = stream.ReadStringV(maxNameLength);
					string name = stream.ReadStringV(maxNameLength);
					Type declType = this.ReadType(stream);
					member = declType.GetNestedTypes(allFlags).Single(t => t.Namespace == ns && t.Name == name);
					break;
				}

				case MemberCode.TypeGeneric:
				{
					Type typeDef = this.ReadType(stream);

					// Read generic arguments.
					Type[] genericArgs = new Type[typeDef.GetGenericArguments().Length];
					for (int i = 0; i < genericArgs.Length; i++)
						genericArgs[i] = this.ReadType(stream);

					member = typeDef.MakeGenericType(genericArgs);
					break;
				}

				case MemberCode.TypeGenericParameterOfType:
				{
					Type declType = this.ReadType(stream);
					int index = stream.ReadInt32V();
					member = declType.GetGenericArguments()[index];
					break;
				}

				case MemberCode.TypeGenericParameterOfMethod:
				{
					MethodBase declMethod = this.ReadMethod(stream);
					int index = stream.ReadInt32V();
					member = declMethod.GetGenericArguments()[index];
					break;
				}

				case MemberCode.TypeByRef:
				{
					member = this.ReadType(stream).MakeByRefType();
					break;
				}

				case MemberCode.TypePointer:
				{
					member = this.ReadType(stream).MakePointerType();
					break;
				}

				case MemberCode.TypeSzArray:
				{
					member = this.ReadType(stream).MakeArrayType();
					break;
				}

				case MemberCode.TypeMdArray:
				{
					int arrayRank = stream.ReadInt32V();
					Type elementType = this.ReadType(stream);

					member = elementType.MakeArrayType(arrayRank);
					break;
				}
				
				case MemberCode.Method:
				{
					string name = stream.ReadStringV(maxNameLength);
					Type declType = this.ReadType(stream);
					MethodFlags flags = (MethodFlags)stream.ReadByteOrThrow();
					Signature sig = this.ReadSignature(stream).Simplify();
					member = declType
							.GetMethods(getBindingFlags(flags))
							.Single(m => m.Name == name && new Signature(m).Simplify().Equals(sig));
					break;
				}

				case MemberCode.MethodConstructor:
				{
					Type declType = this.ReadType(stream);
					MethodFlags flags = (MethodFlags)stream.ReadByteOrThrow();
					Signature sig = this.ReadSignature(stream).Simplify();
					member = declType
							.GetConstructors(getBindingFlags(flags))
							.Single(m => new Signature(m).Simplify().Equals(sig));
					break;
				}

				case MemberCode.Field:
				{
					string name = stream.ReadStringV(maxNameLength);
					Type declType = this.ReadType(stream);
					member = declType.GetField(name, allFlags);
					break;
				}

				case MemberCode.Property:
				{
					string name = stream.ReadStringV(maxNameLength);
					Type declType = this.ReadType(stream);
					Type returnType = this.ReadType(stream);

					int numIndexArgs = stream.ReadInt32V();
					if (numIndexArgs < 0 || numIndexArgs > maxParameters)
						throw new LimitExceededException();

					Type[] indexTypes = new Type[numIndexArgs];
					for (int i = 0; i < indexTypes.Length; ++i)
						indexTypes[i] = this.ReadType(stream);

					member = declType.GetProperty(name, allFlags, null, returnType, indexTypes, null);
					break;
				}

				case MemberCode.Event:
				{
					string name = stream.ReadStringV(maxNameLength);
					Type declType = this.ReadType(stream);
					member = declType.GetEvent(name, allFlags);
					break;
				}

				default:
					throw invalidOpCodeException(code);
			}

			if (member == null)
			{
				throw new MissingMemberException($"Could not find member. ({code}, {id})");
			}

			this.memberReferenceProvider.Add(id, member);

			return member;

		}

		public Type ReadType(Stream stream)
		{
			return (Type)this.ReadMember(stream);
		}
		
		public MethodBase ReadMethod(Stream stream)
		{
			return (MethodBase)this.ReadMember(stream);
		}

		public FieldInfo ReadField(Stream stream)
		{
			return (FieldInfo)this.ReadMember(stream);
		}

		public PropertyInfo ReadProperty(Stream stream)
		{
			return (PropertyInfo)this.ReadMember(stream);
		}

		public EventInfo ReadEvent(Stream stream)
		{
			return (EventInfo)this.ReadMember(stream);
		}

		#endregion
	}
}
