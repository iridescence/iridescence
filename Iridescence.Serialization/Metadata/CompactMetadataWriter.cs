﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Stores common reflection metadata in a compact binary representation.
	/// </summary>
	public class CompactMetadataWriter
	{
		#region Fields

		private readonly CompactMetadataOptions options;
		private readonly IReferenceProvider<AssemblyName, int> assemblyReferenceProvider;
		private readonly IReferenceProvider<MemberInfo, int> memberReferenceProvider;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CompactMetadataWriter"/> class. 
		/// </summary>
		public CompactMetadataWriter(CompactMetadataOptions options)
		{
			this.options = options;
			this.assemblyReferenceProvider = new DictionaryReferenceProvider<AssemblyName>(new Dictionary<AssemblyName, int>(new AssemblyNameEqualityComparer()));
			this.memberReferenceProvider = new DictionaryReferenceProvider<MemberInfo>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Writes an <see cref="AssemblyName"/> to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="assemblyName"></param>
		public void WriteAssemblyName(Stream stream, AssemblyName assemblyName)
		{
			if (ReferenceEquals(assemblyName, null))
			{
				// Emit "Null" opcode.
				stream.WriteCode(AssemblyCode.Null);
				return;
			}

			int id = this.assemblyReferenceProvider.GetOrAdd(assemblyName, out bool wasAdded);
			if (!wasAdded)
			{
				// Emit "Reference" opcode with ID.
				stream.WriteCode(AssemblyCode.Reference, id);
				return;
			}

			// Emit "New" opcode with ID.
			// Store assembly name based on what's relevant to AssemblyName.FullName.
			stream.WriteCode(AssemblyCode.New, id);
			stream.WriteStringV(assemblyName.Name);
			stream.WriteInt32V(assemblyName.CultureInfo?.LCID + 1 ?? 0);

			byte[] pkt = assemblyName.GetPublicKeyToken();
			int pktLen = pkt?.Length + 1 ?? 0;
			stream.WriteInt32V(pktLen);
			if (pkt != null) stream.Write(pkt, 0, pkt.Length);

			if ((this.options & CompactMetadataOptions.FullAssemblyNames) != 0)
			{
				byte[] pk = assemblyName.GetPublicKey();
				int pkLen = pk?.Length + 1 ?? 0;
				stream.WriteInt32V(pkLen);
				if (pk != null) stream.Write(pk, 0, pk.Length);
			}

			stream.WriteInt32V(assemblyName.Version.Major);
			stream.WriteInt32V(assemblyName.Version.Minor);
			stream.WriteInt32V(assemblyName.Version.Build);
			stream.WriteInt32V(assemblyName.Version.Revision);

			stream.WriteInt32V((int)assemblyName.HashAlgorithm);
			stream.WriteInt32V((int)assemblyName.VersionCompatibility);
			stream.WriteInt32V((int)assemblyName.Flags);
			stream.WriteInt32V((int)assemblyName.ContentType);
		}

		/// <summary>
		/// Writes the name of the specified <see cref="Assembly"/> to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="assembly"></param>
		public void WriteAssembly(Stream stream, Assembly assembly)
		{
			this.WriteAssemblyName(stream, assembly.GetName());
		}

		/// <summary>
		/// Writes a <see cref="SignatureType"/> to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="sigType"></param>
		private void writeSignatureType(Stream stream, SignatureType sigType)
		{
			switch (sigType)
			{
				case null:
					stream.WriteCode(AssemblyCode.Null);
					break;

				case SignatureSimpleType simpleType:
					stream.WriteCode(SignatureCode.SimpleType);
					this.WriteType(stream, simpleType.Type);
					break;

				case SignatureByRefType byRefType:
					stream.WriteCode(SignatureCode.ByRefType);
					this.writeSignatureType(stream, byRefType.ElementType);
					break;

				case SignaturePointerType pointerType:
					stream.WriteCode(SignatureCode.PointerType);
					this.writeSignatureType(stream, pointerType.ElementType);
					break;

				case SignatureSzArrayType szArrayType:
					stream.WriteCode(SignatureCode.SzArrayType);
					this.writeSignatureType(stream, szArrayType.ElementType);
					break;

				case SignatureMdArrayType mdArrayType:
					stream.WriteCode(SignatureCode.MdArrayType, mdArrayType.Rank);
					this.writeSignatureType(stream, mdArrayType.ElementType);
					break;

				case SignatureGenericType genericType:
					stream.WriteCode(SignatureCode.GenericType, genericType.TypeArguments.Count);
					this.writeSignatureType(stream, genericType.TypeDefinition);
					foreach(SignatureType arg in genericType.TypeArguments)
						this.writeSignatureType(stream, arg);
					break;

				case SignatureTypeGenericParameter typeGenericParameter:
					stream.WriteCode(SignatureCode.TypeGenericParameter, typeGenericParameter.Position);
					this.writeSignatureType(stream, typeGenericParameter.Type);
					break;

				case SignatureMethodGenericParameter methodGenericParameter:
					stream.WriteCode(SignatureCode.MethodGenericParameter, methodGenericParameter.Position);
					break;

				default:
					throw new NotSupportedException();
			}
		}

		/// <summary>
		/// Writes a <see cref="Signature"/> to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="signature"></param>
		public void WriteSignature(Stream stream, Signature signature)
		{
			this.writeSignatureType(stream, signature.ReturnType);
			stream.WriteInt32V(signature.ParameterTypes.Count);
			foreach (SignatureType type in signature.ParameterTypes)
			{
				this.writeSignatureType(stream, type);
			}
			stream.WriteInt32V(signature.TypeArguments.Count);
			foreach (SignatureType type in signature.TypeArguments)
			{
				this.writeSignatureType(stream, type);
			}
		}

		/// <summary>
		/// Writes a member represented by any supported <see cref="MemberInfo"/> to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="member"></param>
		public void WriteMember(Stream stream, MemberInfo member)
		{
			if (ReferenceEquals(member, null))
			{
				// Emit "Null" opcode.
				stream.WriteCode(MemberCode.Null);
				return;
			}

			int id = this.memberReferenceProvider.GetOrAdd(member, out bool wasAdded);

			if (!wasAdded)
			{
				// Emit "Reference" opcode with ID.
				stream.WriteCode(MemberCode.Reference, id);
				return;
			}

			if (member is Type type)
			{
				if (type.IsByRef)
				{
					// "ByRef" + element type.
					stream.WriteCode(MemberCode.TypeByRef, id);
					this.WriteType(stream, type.GetElementType());
				}
				else if (type.IsPointer)
				{
					// "Pointer" + element type.
					stream.WriteCode(MemberCode.TypePointer, id);
					this.WriteType(stream, type.GetElementType());
				}
				else if (type.IsArray)
				{
					if (type.IsSzArray())
					{
						// "SzArray" + element type.
						stream.WriteCode(MemberCode.TypeSzArray, id);
						this.WriteType(stream, type.GetElementType());
					}
					else
					{
						// "Array" + Rank + element type.
						stream.WriteCode(MemberCode.TypeMdArray, id);
						stream.WriteInt32V(type.GetArrayRank());
						this.WriteType(stream, type.GetElementType());
					}
				}
				else if (type.IsConstructedGenericType)
				{
					// "Generic" + definition + type arguments.
					Type[] genericArgs = type.GetGenericArguments();
					stream.WriteCode(MemberCode.TypeGeneric, id);

					this.WriteType(stream, type.GetGenericTypeDefinition());
					foreach (Type arg in genericArgs)
					{
						this.WriteType(stream, arg);
					}
				}
				else if (type.IsGenericParameter)
				{
					if (type.DeclaringMethod != null)
					{
						// "TypeGenericParameterOfMethod" + declaring method + index.
						stream.WriteCode(MemberCode.TypeGenericParameterOfMethod, id);
						this.WriteMethod(stream, type.DeclaringMethod);
						stream.WriteInt32V(type.GenericParameterPosition);
					}
					else if (type.DeclaringType != null)
					{
						// "TypeGenericParameter" + declaring type + index.
						stream.WriteCode(MemberCode.TypeGenericParameterOfType, id);
						this.WriteType(stream, type.DeclaringType);
						stream.WriteInt32V(type.GenericParameterPosition);
					}
					else
					{
						throw new NotSupportedException();
					}
				}
				else if (type.DeclaringType != null)
				{
					// "Nested" + namespace + name + declaring type.
					stream.WriteCode(MemberCode.TypeNested, id);
					stream.WriteStringV(type.Namespace);
					stream.WriteStringV(type.Name);
					this.WriteType(stream, type.DeclaringType);
				}
				else
				{
					// "Type" + namespace + name + assembly name.
					stream.WriteCode(MemberCode.Type, id);
					stream.WriteStringV(type.Namespace);
					stream.WriteStringV(type.Name);
					this.WriteAssembly(stream, type.Assembly);
				}
			}
			else if (member is MethodBase methodBase)
			{
				if (methodBase is ConstructorInfo)
				{
					// "MethodConstructor" + declaring type + signature.
					stream.WriteCode(MemberCode.MethodConstructor, id);
				}
				else
				{
					// "Method" + name + declaring type + signature.
					stream.WriteCode(MemberCode.Method, id);
					stream.WriteStringV(methodBase.Name);
				}
				
				this.WriteType(stream, methodBase.DeclaringType);

				MethodFlags flags = MethodFlags.None;
				if (methodBase.IsStatic)
					flags |= MethodFlags.Static;
				stream.WriteByte((byte)flags);
				
				Signature signature = new Signature(methodBase);
				this.WriteSignature(stream, signature);
			}
			else if (member is FieldInfo field)
			{
				// "Field" + declaring type + name.
				stream.WriteCode(MemberCode.Field, id);
				stream.WriteStringV(field.Name);
				this.WriteType(stream, field.DeclaringType);
			}
			else if (member is PropertyInfo property)
			{
				// "Property" + declaring type + name + return type + index types.
				stream.WriteCode(MemberCode.Property, id);
				stream.WriteStringV(property.Name);
				this.WriteType(stream, property.DeclaringType);
				this.WriteType(stream, property.PropertyType);

				ParameterInfo[] parameters = property.GetIndexParameters();
				stream.WriteInt32V(parameters.Length);
				foreach (ParameterInfo p in parameters)
				{
					this.WriteType(stream, p.ParameterType);
				}
			}
			else if (member is EventInfo ev)
			{
				// "Event" + declaring type + name.
				stream.WriteCode(MemberCode.Event, id);
				stream.WriteStringV(ev.Name);
				this.WriteType(stream, ev.DeclaringType);
			}
			else
			{
				throw new NotSupportedException();
			}
		}

		/// <summary>
		/// Writes the specified <see cref="Type"/> to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="type"></param>
		public void WriteType(Stream stream, Type type)
		{
			this.WriteMember(stream, type);
		}

		public void WriteMethod(Stream stream, MethodBase method)
		{
			this.WriteMember(stream, method);
		}

		public void WriteField(Stream stream, FieldInfo field)
		{
			this.WriteMember(stream, field);
		}

		public void WriteProperty(Stream stream, PropertyInfo property)
		{
			this.WriteMember(stream, property);
		}
		
		public void WriteEvent(Stream stream, EventInfo ev)
		{
			this.WriteMember(stream, ev);
		}

		#endregion
	}
}
