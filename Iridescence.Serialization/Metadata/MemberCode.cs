﻿namespace Iridescence.Serialization
{
	internal enum MemberCode
	{
		Null,
		Reference,
		Type,
		TypeNested,
		TypeByRef,
		TypePointer,
		TypeSzArray,
		TypeMdArray,
		TypeGeneric,
		TypeGenericParameterOfType,
		TypeGenericParameterOfMethod,
		Method,
		MethodConstructor,
		Field,
		Property,
		Event,
	}
}
