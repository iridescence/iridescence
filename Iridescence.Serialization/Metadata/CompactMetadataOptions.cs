﻿using System;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Options for <see cref="CompactMetadataReader"/> and <see cref="CompactMetadataWriter"/>.
	/// </summary>
	[Flags]
	public enum CompactMetadataOptions
	{
		None = 0,

		/// <summary>
		/// Full assembly names are stored. This includes the public key, which is otherwise not stored.
		/// </summary>
		FullAssemblyNames = 1,
	}
}
