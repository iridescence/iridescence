﻿using System.IO;

namespace Iridescence.Serialization
{
	internal static class CompactMetadataUtility
	{
		#region Methods

		public static void WriteCode(this Stream stream, AssemblyCode code, int parameter = 0)
		{
			ulong max = (ulong)EnumUtility<AssemblyCode, int>.MaxValue + 1;
			stream.WriteUInt64V((ulong)code + (ulong)parameter * max);
		}

		public static void ReadCode(this Stream stream, out AssemblyCode code, out int parameter)
		{
			ulong max = (ulong)EnumUtility<AssemblyCode, int>.MaxValue + 1;
			ulong codeWithParameter = stream.ReadUInt64V();
			code = (AssemblyCode)(codeWithParameter % max);
			parameter = checked((int)(codeWithParameter / max));
		}

		public static void WriteCode(this Stream stream, MemberCode code, int parameter = 0)
		{
			ulong max = (ulong)EnumUtility<MemberCode, int>.MaxValue + 1;
			stream.WriteUInt64V((ulong)code + (ulong)parameter * max);
		}

		public static void ReadCode(this Stream stream, out MemberCode code, out int parameter)
		{
			ulong max = (ulong)EnumUtility<MemberCode, int>.MaxValue + 1;
			ulong codeWithParameter = stream.ReadUInt64V();
			code = (MemberCode)(codeWithParameter % max);
			parameter = checked((int)(codeWithParameter / max));
		}

		public static void WriteCode(this Stream stream, SignatureCode code, int parameter = 0)
		{
			ulong max = (ulong)EnumUtility<SignatureCode, int>.MaxValue + 1;
			stream.WriteUInt64V((ulong)code + (ulong)parameter * max);
		}

		public static void ReadCode(this Stream stream, out SignatureCode code, out int parameter)
		{
			ulong max = (ulong)EnumUtility<SignatureCode, int>.MaxValue + 1;
			ulong codeWithParameter = stream.ReadUInt64V();
			code = (SignatureCode)(codeWithParameter % max);
			parameter = checked((int)(codeWithParameter / max));
		}
		
		#endregion
	}
}
