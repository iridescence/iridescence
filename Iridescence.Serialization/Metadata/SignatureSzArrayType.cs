﻿using System;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents an SZ array of the specified element <see cref="SignatureType"/>.
	/// </summary>
	public class SignatureSzArrayType : SignatureType, IEquatable<SignatureSzArrayType>
	{
		#region Properties

		public SignatureType ElementType { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignatureSzArrayType"/> class. 
		/// </summary>
		public SignatureSzArrayType(SignatureType elementType)
		{
			this.ElementType = elementType ?? throw new ArgumentNullException(nameof(elementType));
		}

		#endregion

		#region Methods

		public override bool TryResolve(MethodBase method, out Type type)
		{
			type = null;
			if (!this.ElementType.TryResolve(method, out Type elementType))
				return false;

			type = elementType.MakeArrayType();
			return true;
		}

		public override string ToString()
		{
			return $"{this.ElementType}[]";
		}


		public bool Equals(SignatureSzArrayType other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.ElementType.Equals(other.ElementType);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((SignatureSzArrayType)obj);
		}

		public override int GetHashCode()
		{
			return this.ElementType.GetHashCode();
		}
		
		#endregion
	}
}
