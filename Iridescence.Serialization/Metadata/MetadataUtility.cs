﻿using System;
using System.Reflection;

namespace Iridescence.Serialization
{
	public static class MetadataUtility
	{
		/// <summary>
		/// Returns a value that indicates whether the specified type represents a runtime reflection metadata class such as RtFieldInfo, MonoField or the like.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static bool IsRuntimeMetadata(Type type)
		{
			if (type.IsSubclassOf(typeof(MemberInfo)))
			{
				// There is no way to identify the internal runtime reflection implementations across runtimes.
				// e.g. in .NET Framework there is RtFieldInfo, which also exists in .NET Core, but it is not serializable in the latter.
				// on Mono the class is MonoField.
				return type.Assembly == typeof(RuntimeTypeHandle).Assembly;
			}

			return false;
		}
	}
}
