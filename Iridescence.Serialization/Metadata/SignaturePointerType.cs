﻿using System;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents the pointer type of a <see cref="SignatureType"/>.
	/// </summary>
	public class SignaturePointerType : SignatureType, IEquatable<SignaturePointerType>
	{
		#region Properties

		public SignatureType ElementType { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignaturePointerType"/> class. 
		/// </summary>
		public SignaturePointerType(SignatureType elementType)
		{
			this.ElementType = elementType ?? throw new ArgumentNullException(nameof(elementType));
		}

		#endregion

		#region Methods

		public override bool TryResolve(MethodBase method, out Type type)
		{
			type = null;
			if (!this.ElementType.TryResolve(method, out Type elementType))
				return false;

			type = elementType.MakePointerType();
			return true;
		}

		public override string ToString()
		{
			return $"{this.ElementType}*";
		}


		public bool Equals(SignaturePointerType other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.ElementType.Equals(other.ElementType);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((SignaturePointerType)obj);
		}

		public override int GetHashCode()
		{
			return this.ElementType.GetHashCode();
		}
		
		#endregion
	}
}
