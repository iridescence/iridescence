﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Represents a generic type.
	/// </summary>
	public class SignatureGenericType : SignatureType, IEquatable<SignatureGenericType>
	{
		#region Fields

		#endregion

		#region Properties

		public SignatureType TypeDefinition { get; }

		public IReadOnlyList<SignatureType> TypeArguments { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignatureGenericType"/> class. 
		/// </summary>
		public SignatureGenericType(SignatureType typeDefinition, IEnumerable<SignatureType> typeArguments)
		{
			this.TypeDefinition = typeDefinition ?? throw new ArgumentNullException();
			this.TypeArguments = typeArguments.ToArray();
		}

		#endregion

		#region Methods

		public override bool TryResolve(MethodBase method, out Type type)
		{
			type = null;

			if (!this.TypeDefinition.TryResolve(method, out Type typeDef))
				return false;

			Type[] args = new Type[this.TypeArguments.Count];
			for (int i = 0; i < args.Length; ++i)
			{
				if (!this.TypeArguments[i].TryResolve(method, out args[i]))
					return false;
			}

			type = typeDef.MakeGenericType(args);
			return true;
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();
			str.Append(this.TypeDefinition);
			str.Append('<');

			bool first = true;
			foreach (SignatureType arg in this.TypeArguments)
			{
				if (first)
					first = false;
				else
					str.Append(", ");
				
				str.Append(arg);
			}

			str.Append('>');

			return str.ToString();
		}

		public bool Equals(SignatureGenericType other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;

			if (!Equals(this.TypeDefinition, other.TypeDefinition))
				return false;

			if (this.TypeArguments.Count != other.TypeArguments.Count)
				return false;

			for (int i = 0; i < this.TypeArguments.Count; ++i)
			{
				if (!Equals(this.TypeArguments[i], other.TypeArguments[i]))
					return false;
			}

			return true;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((SignatureGenericType)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = this.TypeDefinition.GetHashCode();

				foreach (SignatureType arg in this.TypeArguments)
				{
					hash = (hash * 397) ^ arg.GetHashCode();
				}

				return hash;
			}
		}
		
		#endregion
	}
}
