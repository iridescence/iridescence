﻿using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Interface for metadata reading.
	/// </summary>
	public interface IMetadataReader
	{
		#region Methods

		/// <summary>
		/// Reads an assembly name.
		/// </summary>
		/// <returns></returns>
		AssemblyName ReadAssemblyName();

		/// <summary>
		/// Reads any member (i.e. type, method, field, property, event).
		/// </summary>
		/// <returns></returns>
		MemberInfo ReadMember();

		#endregion
	}
}
