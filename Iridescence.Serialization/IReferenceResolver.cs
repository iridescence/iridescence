﻿namespace Iridescence.Serialization
{
	/// <summary>
	/// Interface for classes that map identifiers to objects.
	/// </summary>
	public interface IReferenceResolver<T, in TIdentifier>
	{
		#region Methods

		/// <summary>
		/// Tries to map an object identifier to its object instance.
		/// </summary>
		/// <param name="identifier">The object identifier.</param>
		/// <param name="obj">The object that has the specified ID.</param>
		/// <returns>True, if the object exists. False otherwise.</returns>
		bool TryGetValue(TIdentifier identifier, out T obj);

		/// <summary>
		/// Adds a new object with the specified identifier for later re-use.
		/// </summary>
		/// <param name="identifier">The object identifier.</param>
		/// <param name="obj">An object.</param>
		void Add(TIdentifier identifier, T obj);

		#endregion
	}

	public static class ReferenceResolverExtensions
	{
		/// <summary>
		/// Returns the object with the specified identifier.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TIdentifier"></typeparam>
		/// <param name="resolver"></param>
		/// <param name="identifier"></param>
		/// <returns></returns>
		public static T Get<T, TIdentifier>(this IReferenceResolver<T, TIdentifier> resolver, TIdentifier identifier)
		{
			if (!resolver.TryGetValue(identifier, out var obj))
			{
				throw new MissingObjectsException<TIdentifier>(identifier);
			}

			return obj;
		}
	}
}
