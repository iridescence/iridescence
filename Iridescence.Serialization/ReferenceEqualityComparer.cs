﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Compares objects by their reference instead of equality.
	/// </summary>
	public sealed class ReferenceEqualityComparer<T> : IEqualityComparer<T>
	{
		#region Methods

		bool IEqualityComparer<T>.Equals(T x, T y)
		{
			return ReferenceEquals(x, y);
		}

		int IEqualityComparer<T>.GetHashCode(T obj)
		{
			return RuntimeHelpers.GetHashCode(obj);
		}

		#endregion
	}
}
