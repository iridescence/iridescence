﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Stores serialization-related information about a member.
	/// </summary>
	[Serializable]
	public class SerializationMember
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the type of the member.
		/// </summary>
		public Type Type { get; private set; }

		/// <summary>
		/// Gets the attributes applied to the <see cref="Type"/>.
		/// </summary>
		public ImmutableArray<ISerializationMemberAttribute> TypeAttributes { get; private set; }
		
		/// <summary>
		/// Gets the attributes applied to the member.
		/// </summary>
		public ImmutableArray<ISerializationMemberAttribute> MemberAttributes { get; private set; }

		/// <summary>
		/// Gets the member's attributes.
		/// This is a flattened representation containing the member's own attributes an attributes that were passed down by parent members.
		/// </summary>
		public IEnumerable<ISerializationMemberAttribute> Attributes => this.TypeAttributes.Concat(this.MemberAttributes);

		/// <summary>
		/// Gets the name of the member.
		/// </summary>
		public string Name { get; private set; }

		/// <summary>
		/// Gets a value that indicates whether the current member should be serialized as if it were a value type.
		/// </summary>
		/// <returns></returns>
		public bool TreatAsValue => this.Attributes.Any(attr => attr is SerializeAsValueAttribute);

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SerializationMember"/> class with the specified parameters.
		/// </summary>
		/// <param name="type">The type of the member.</param>
		/// <param name="typeAttributes">The attributes applied to the type.</param>
		/// <param name="memberAttributes">The attributes applied to the member.</param>
		/// <param name="name">The name of the child member.</param>
		public SerializationMember(Type type, ImmutableArray<ISerializationMemberAttribute> typeAttributes, ImmutableArray<ISerializationMemberAttribute> memberAttributes, string name = null)
		{
			this.Type = type ?? throw new ArgumentNullException(nameof(type));

			if (typeAttributes.IsDefault)
				throw new ArgumentNullException(nameof(typeAttributes));

			if (typeAttributes.Any(a => a == null))
				throw new ArgumentException("An attribute can not be null.", nameof(typeAttributes));

			if (memberAttributes.IsDefault)
				throw new ArgumentNullException(nameof(memberAttributes));

			if (memberAttributes.Any(a => a == null))
				throw new ArgumentException("An attribute can not be null.", nameof(memberAttributes));

			this.TypeAttributes = typeAttributes;
			this.MemberAttributes = memberAttributes;
			this.Name = name;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SerializationMember"/> class with the specified parameters.
		/// Retrieves the type attributes from the type's custom attributes.
		/// </summary>
		/// <param name="type">The type of the member.</param>
		public SerializationMember(Type type)
			: this(type, type.GetSerializationAttributes(), ImmutableArray<ISerializationMemberAttribute>.Empty)
		{
			
		}

		private SerializationMember()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a member with the same properties, but a different type.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public SerializationMember ChangeType(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (type == this.Type)
				return this;

			return this.ChangeType(type, type.GetSerializationAttributes());
		}

		/// <summary>
		/// Returns a member with the same properties, but a different type.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="typeAttributes">The attribute applied to the type. If set to null, the attributes are retrieved from the custom attributes applied to the type.</param>
		/// <returns></returns>
		public SerializationMember ChangeType(Type type, ImmutableArray<ISerializationMemberAttribute> typeAttributes)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			return new SerializationMember(type, typeAttributes, this.MemberAttributes, this.Name);
		}

		/// <summary>
		/// Goes from this <see cref="SerializationMember"/> to a child, applying attributes from this member that affect children.
		/// </summary>
		/// <param name="type">The type of the child member.</param>
		/// <param name="name">The name of the child member.</param>
		/// <returns>A <see cref="SerializationMember"/> that has a flattened list of attributes containing the original member attributes and attributes from this member that have an effect on the child.</returns>
		public SerializationMember Descend(Type type, string name)
		{
			return this.Descend(type, name, type.GetSerializationAttributes(), ImmutableArray<ISerializationMemberAttribute>.Empty);
		}

		/// <summary>
		/// Goes from this <see cref="SerializationMember"/> to a child, applying attributes from this member that affect children.
		/// </summary>
		/// <param name="type">The type of the child member.</param>
		/// <param name="typeAttributes">The attributes applied to the type. If set to null, the attributes are retrieved from the custom attributes applied to the type.</param>
		/// <param name="memberAttributes">The attributes applied to the member. If set to null, only the inherited attributes from this member are used.</param>
		/// <param name="name">The name of the child member.</param>
		/// <returns>A <see cref="SerializationMember"/> that has a flattened list of attributes containing the original member attributes and attributes from this member that have an effect on the child.</returns>
		public SerializationMember Descend(Type type, string name, ImmutableArray<ISerializationMemberAttribute> typeAttributes, ImmutableArray<ISerializationMemberAttribute> memberAttributes)
		{
			SerializationMember member = new SerializationMember
			{
				Type = type,
				TypeAttributes = typeAttributes,
				MemberAttributes = memberAttributes,
				Name = name,
			};

			// Derive attributes.
			ImmutableArray<ISerializationMemberAttribute> childTypeAttrs = typeAttributes;
			ImmutableArray<ISerializationMemberAttribute> childMemberAttrs = memberAttributes;

			foreach (ISerializationMemberAttribute attribute in this.TypeAttributes)
			{
				childTypeAttrs = childTypeAttrs.AddRange(attribute.GetChildAttributes(this, member));
			}

			foreach (ISerializationMemberAttribute attribute in this.MemberAttributes)
			{
				childMemberAttrs = childMemberAttrs.AddRange(attribute.GetChildAttributes(this, member));
			}

			if (childTypeAttrs.Length != typeAttributes.Length || childMemberAttrs.Length != memberAttributes.Length)
			{
				// Re-create member with inherited extra attributes.
				member = new SerializationMember(type, childTypeAttrs, childMemberAttrs, name);
			}
			
			return member;
		}

		/// <summary>
		/// Goes from this <see cref="SerializationMember"/> to a child field, applying attributes from this member that affect children.
		/// </summary>
		/// <param name="field">The <see cref="FieldInfo"/> of the field.</param>
		/// <param name="nameAlias">An alternative name. If set to null, the field name is used.</param>
		/// <returns>A <see cref="SerializationMember"/> that has a flattened list of attributes containing the original member attributes and attributes from this member that have an effect on the child.</returns>
		public SerializationMember Descend(FieldInfo field, string nameAlias = null)
		{
			return this.Descend(field, field.FieldType.GetSerializationAttributes(), nameAlias);
		}

		/// <summary>
		/// Goes from this <see cref="SerializationMember"/> to a child field, applying attributes from this member that affect children.
		/// </summary>
		/// <param name="field">The <see cref="FieldInfo"/> of the field.</param>
		/// <param name="typeAttributes">The attributes applied to the field type. If set to null, the attributes are retrieved from the custom attributes applied to the type.</param>
		/// <param name="nameAlias">An alternative name. If set to null, the field name is used.</param>
		/// <returns>A <see cref="SerializationMember"/> that has a flattened list of attributes containing the original member attributes and attributes from this member that have an effect on the child.</returns>
		public SerializationMember Descend(FieldInfo field, ImmutableArray<ISerializationMemberAttribute> typeAttributes, string nameAlias = null)
		{
			return this.Descend(field, typeAttributes, field.GetSerializationAttributes(), nameAlias);
		}

		/// <summary>
		/// Goes from this <see cref="SerializationMember"/> to a child field, applying attributes from this member that affect children.
		/// </summary>
		/// <param name="field">The <see cref="FieldInfo"/> of the field.</param>
		/// <param name="typeAttributes">The attributes applied to the field type. If set to null, the attributes are retrieved from the custom attributes applied to the type.</param>
		/// <param name="memberAttributes">The attributes applied to the field. If set to null, the attributes are retrieved from the custom attributes applied to the field.</param>
		/// <param name="nameAlias">An alternative name. If set to null, the field name is used.</param>
		/// <returns>A <see cref="SerializationMember"/> that has a flattened list of attributes containing the original member attributes and attributes from this member that have an effect on the child.</returns>
		public SerializationMember Descend(FieldInfo field, ImmutableArray<ISerializationMemberAttribute> typeAttributes, ImmutableArray<ISerializationMemberAttribute> memberAttributes, string nameAlias = null)
		{
			string name = nameAlias ?? field.Name;
			return this.Descend(field.FieldType, name, typeAttributes, memberAttributes);
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			str.Append(this.Type.GetDisplayName());

			if (this.Name != null)
			{
				str.Append(' ');
				str.Append('"');
				str.Append(this.Name);
				str.Append('"');
			}

			foreach (ISerializationMemberAttribute attr in this.MemberAttributes)
			{
				str.Append(" M:");
				str.Append(attr);
			}

			foreach (ISerializationMemberAttribute attr in this.TypeAttributes)
			{
				str.Append(" T:");
				str.Append(attr);
			}

			return str.ToString();
		}

		#endregion
	}

	internal static class SerializationMemberExtensions
	{
		private static readonly ConcurrentDictionary<MemberInfo, ImmutableArray<ISerializationMemberAttribute>> attributeCache = new ConcurrentDictionary<MemberInfo, ImmutableArray<ISerializationMemberAttribute>>();

		/// <summary>
		/// Returns a collection of <see cref="ISerializationMemberAttribute"/> that are applied to the specified member.
		/// </summary>
		/// <param name="member"></param>
		/// <returns></returns>
		public static ImmutableArray<ISerializationMemberAttribute> GetSerializationAttributes(this MemberInfo member)
		{
			return attributeCache.GetOrAdd(member, m =>
			{
				var builder = ImmutableArray.CreateBuilder<ISerializationMemberAttribute>();

				foreach (Attribute attr in m.GetCustomAttributes())
				{
					if (attr is ISerializationMemberAttribute serAttr)
						builder.Add(serAttr);
				}

				if (builder.Count == 0)
					return ImmutableArray<ISerializationMemberAttribute>.Empty;

				return builder.ToImmutable();
			});
		}
	}
}
