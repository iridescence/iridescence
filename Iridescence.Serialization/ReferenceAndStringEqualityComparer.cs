﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Compares objects by their reference instead of equality.
	/// Strings are still compared by value.
	/// </summary>
	public sealed class ReferenceAndStringEqualityComparer : IEqualityComparer<object>
	{
		#region Methods

		bool IEqualityComparer<object>.Equals(object x, object y)
		{
			if (x is string xs && y is string ys)
				return string.Equals(xs, ys, StringComparison.Ordinal);

			return ReferenceEquals(x, y);
		}

		int IEqualityComparer<object>.GetHashCode(object obj)
		{
			if (obj is string s)
				return s.GetHashCode();
			return RuntimeHelpers.GetHashCode(obj);
		}

		#endregion
	}
}