﻿namespace Iridescence.Serialization
{
	/// <summary>
	/// An interface for deferred reference resolving.
	/// </summary>
	/// <typeparam name="T">The base class of the values resolved by this resolver.</typeparam>
	/// <typeparam name="TIdentifier">The type of the identifier.</typeparam>
	public interface IDeferredReferenceResolver<in T, in TIdentifier>
	{
		/// <summary>
		/// Requests the object with the specified identifier from the reference resolver.
		/// </summary>
		/// <typeparam name="TValue"></typeparam>
		/// <typeparam name="TState"></typeparam>
		/// <param name="identifier">The identifier of the object.</param>
		/// <param name="callback">The callback to invoke when the object was retreived.</param>
		/// <param name="state">An argument to pass to the callback.</param>
		/// <returns>The result.</returns>
		DeferredReferencingResult Get<TValue, TState>(TIdentifier identifier, DeferredCallback<TValue, TState> callback, TState state) where TValue : T;

		/// <summary>
		/// Sets the object with the specified identifier.
		/// </summary>
		/// <param name="identifier"></param>
		/// <param name="value"></param>
		void Set(TIdentifier identifier, T value);
	}
}
