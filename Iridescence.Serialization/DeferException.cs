﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Exception that occurs when a deferred read operation does not complete properly.
	/// </summary>
	[Serializable]
	public class DeferException : SerializationException
	{
		#region Constructors

		public DeferException()
			: this("Deferred read operation did not return an object. This may indicate that there are unresolvable or missing object references.")
		{

		}

		public DeferException(string message) : base(message)
		{

		}

		public DeferException(string message, Exception innerException) : base(message, innerException)
		{

		}

		protected DeferException(SerializationInfo info, StreamingContext context) : base(info, context)
		{

		}
		
		#endregion
	}
}
