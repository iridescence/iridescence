﻿using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.Serialization;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Implements a more compact delegate surrogate than the one .NET provides.
	/// </summary>
	public sealed class LightDelegateSurrogate : ISerializationSurrogate
	{
		[Serializable]
		private struct DelegateInfo
		{
			public object Target;
			public MethodInfo Method;
		}

		public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
		{
			Delegate d = (Delegate)obj;
			info.AddValue("Type", d.GetType());

			Delegate[] invokationList = d.GetInvocationList();
			info.AddValue("Count", invokationList.Length);

			for (int i = 0; i < invokationList.Length; i++)
			{
				info.AddValue(i.ToString(CultureInfo.InvariantCulture), new DelegateInfo
				{
					Target = invokationList[i].Target,
					Method = invokationList[i].Method
				});
			}
		}

		public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
		{
			Type type = info.GetValue<Type>("Type");
			int count = info.GetInt32("Count");
			Delegate d = null;
			
			for (int i = 0; i < count; i++)
			{
				DelegateInfo delegateInfo = info.GetValue<DelegateInfo>(i.ToString(CultureInfo.InvariantCulture));
				DelegateCreationFunc<Delegate> creator = DelegateFactory.GetFactory(type, delegateInfo.Method);
				Delegate currentDelegate = creator(delegateInfo.Target);

				if (d == null)
					d = currentDelegate;
				else
					d = Delegate.Combine(d, currentDelegate);
			}

			return d;
		}
	}
}