﻿using System;

namespace Iridescence.Serialization
{
	/// <summary>
	/// An interface that can be used to decide whether a type may be (de)serialized or not.
	/// </summary>
	public interface ISerializationArbiter
	{
		/// <summary>
		/// Verifies that the specified type can be used in the serialization process.
		/// Can be used to throw exceptions for types that do not support serialization or are not allowed.
		/// </summary>
		/// <param name="type"></param>
		void VerifyType(Type type);
	}
}
