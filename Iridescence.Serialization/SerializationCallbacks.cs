﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Contains delegates for the four different serialization callbacks.
	/// </summary>
	internal sealed class SerializationCallbacks
	{
		#region Fields

		public delegate void Callback<in T>(T obj, StreamingContext context);

		public readonly Delegate OnSerializing;
		public readonly Delegate OnSerialized;
		public readonly Delegate OnDeserializing;
		public readonly Delegate OnDeserialized;

		public static readonly SerializationCallbacks Empty = new SerializationCallbacks();

		#endregion

		#region Properties

		public bool IsEmpty => this.OnSerializing == null && this.OnSerialized == null && this.OnDeserializing == null && this.OnDeserialized == null;

		#endregion

		#region Constructors
		
		private SerializationCallbacks()
		{

		}

		/// <summary>
		/// Creates a new callback collection by scanning the specified type for marked methods.
		/// </summary>
		/// <param name="type"></param>
		public SerializationCallbacks(Type type)
		{
			this.OnSerializing = createDelegate(type, typeof(OnSerializingAttribute));
			this.OnSerialized = createDelegate(type, typeof(OnSerializedAttribute));
			this.OnDeserializing = createDelegate(type, typeof(OnDeserializingAttribute));
			this.OnDeserialized = createDelegate(type, typeof(OnDeserializedAttribute));
		}

		#endregion

		#region Methods

		private static Delegate createDelegate(Type declType, Type attribType)
		{
			Delegate callbacks = null;
			Type callbackType = null;

			foreach (MethodInfo method in declType.GetMethodsWithAttribute(attribType))
			{
				if (callbackType == null)
					callbackType = typeof(Callback<>).MakeGenericType(declType);

				Delegate d = Delegate.CreateDelegate(callbackType, null, method, true);

				callbacks = Delegate.Combine(callbacks, d);
			}

			return callbacks;
		}

		#endregion
	}
}