﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Flags for the serialization arbiter.
	/// </summary>
	[Flags]
	public enum SerializationArbiterFlags
	{
		/// <summary>
		/// Whether runtime reflection metadata (e.g. runtime types, fields, methods) should be allowed.
		/// </summary>
		AllowRuntimeMetadata = 1,

		/// <summary>
		/// Whether <see cref="Memory{T}"/> and <see cref="ReadOnlyMemory{T}"/> should be allowed. This most likely requires special support by the serializer.
		/// </summary>
		AllowMemory = 2,
	}

	/// <summary>
	/// Determines whether a type serializable by checking if it has a <see cref="SerializableAttribute"/> attached to it.
	/// </summary>
	public class SerializableAttributeArbiter : ISerializationArbiter
	{
		#region Fields

		private readonly SerializationArbiterFlags flags;
		private readonly HashSet<Type> whitelist;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public SerializableAttributeArbiter(SerializationArbiterFlags flags)
		{
			this.flags = flags;
		}

		public SerializableAttributeArbiter(SerializationArbiterFlags flags, params Type[] whitelist)
			: this(flags)
		{
			if (whitelist.Length > 0)
			{
				this.whitelist = new HashSet<Type>(whitelist);
			}
		}

		#endregion

		#region Methods

		protected virtual bool IsSerializable(Type type)
		{
			if (type.IsSerializable)
				return true;

			if (this.whitelist != null && this.whitelist.Contains(type))
				return true;

			if ((this.flags & SerializationArbiterFlags.AllowMemory) != 0)
			{
				Type memoryType = type;
				if (memoryType.IsConstructedGenericType)
					memoryType = type.GetGenericTypeDefinition();

				if (memoryType == typeof(Memory<>) || memoryType == typeof(ReadOnlyMemory<>))
					return true;
			}

			if ((this.flags & SerializationArbiterFlags.AllowRuntimeMetadata) != 0)
			{
				if (MetadataUtility.IsRuntimeMetadata(type))
					return true;
			}
			
			return false;
		}

		public void VerifyType(Type type)
		{
			if (!this.IsSerializable(type))
				throw new SerializationException($"\"{type}\" is not serializable.");
		}

		#endregion
	}
}
