﻿using System;
using System.Collections.Concurrent;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Static cache for serialization callback methods on types.
	/// </summary>
	internal static class SerializationCallbackCache
	{
		#region Fields

		private static readonly ConcurrentDictionary<Type, SerializationCallbacks> dictionary = new ConcurrentDictionary<Type, SerializationCallbacks>();

		#endregion
		
		#region Methods

		/// <summary>
		/// Returns a serialization callback wrapper for the specified type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static SerializationCallbacks GetSerializationCallbacks(this Type type)
		{
			if (!dictionary.TryGetValue(type, out SerializationCallbacks entry))
			{
				entry = new SerializationCallbacks(type);

				// many types do not have callbacks, use null instead of an empty entry to reduce memory footprint.
				if (entry.IsEmpty)
					entry = null;

				dictionary.TryAdd(type, entry);
			}

			return entry ?? SerializationCallbacks.Empty;
		}

		#endregion
	}
}
