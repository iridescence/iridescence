﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Wraps a <see cref="IReferenceResolver{T,TIdentifier}"/> in a <see cref="IDeferredReferenceResolver{T,TIdentifier}"/>.
	/// </summary>
	public sealed class DeferredResolver<T, TIdentifier> : IDeferredReferenceResolver<T, TIdentifier>
	{
		#region Fields

		private readonly IReferenceResolver<T, TIdentifier> referenceResolver;
		private readonly Dictionary<TIdentifier, Action<T>> callbacks;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the objects currently missing.
		/// </summary>
		public IEnumerable<TIdentifier> MissingObjects => this.callbacks.Keys;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DeferredResolver{T,TIdentifier}"/> class. 
		/// </summary>
		public DeferredResolver(IReferenceResolver<T, TIdentifier> referenceResolver)
		{
			this.referenceResolver = referenceResolver;
			this.callbacks = new Dictionary<TIdentifier, Action<T>>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Lazily retrieves an object with the specified ID.
		/// The reference provider is used to resolve the object ID to its real value and the callback is called immediately with the result.
		/// If the reference provider does not know the object, the callback is enqueued for later invocation once the object is made known using the Set method.
		/// </summary>
		/// <param name="id">The object ID.</param>
		/// <param name="callback">A callback that is invoked with the result.</param>
		/// <param name="state">an argument for the callback.</param>
		/// <returns>True, if the object was retrieved. False otherwise.</returns>
		public DeferredReferencingResult Get<TValue, TState>(TIdentifier id, DeferredCallback<TValue, TState> callback, TState state) where TValue : T
		{
			// Ask the reference resolver for that object.
			if (!this.referenceResolver.TryGetValue(id, out T current))
			{
				// If there is none, add the callback for future use.
				// Sadly, we need to put the callback argument into a closure.
				void newCallback(T o) => callback((TValue)o, state);

				// Combine with existing delegate, if any.
				if (this.callbacks.TryGetValue(id, out Action<T> currentCallback))
				{
					this.callbacks[id] = currentCallback + newCallback;
					return DeferredReferencingResult.Enqueued;
				}

				// Add new item to dictionary.
				this.callbacks[id] = newCallback;
				return DeferredReferencingResult.Initial;
			}

			Debug.Assert(current == null || current is TValue);

			// Return the object.
			callback((TValue)current, state);
			return DeferredReferencingResult.Retreived;
		}

		/// <summary>
		/// Sets an object with the specified identifier.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="value"></param>
		public void Set(TIdentifier id, T value)
		{
			// Call any callbacks for that object.
			if (this.callbacks.TryGetValue(id, out Action<T> callback))
			{
				callback(value);
				this.callbacks.Remove(id);
			}

			// Tell the reference provider about the new object.
			this.referenceResolver.Add(id, value);
		}

		/// <summary>
		/// Clears the object callback cache.
		/// </summary>
		public void Clear()
		{
			this.callbacks.Clear();
		}

		#endregion
	}
}
