﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Marks a class or structure as container for a method invocation. The parameter that contains the method to call is dynamically evaluated by name.
	/// </summary>
	[Serializable]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public sealed class DynamicMethodInvocationAttribute : Attribute, ISerializationMemberAttribute
	{
		#region Fields
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the name of the property or field that contains the <see cref="MethodBase"/>.
		/// </summary>
		public string MethodMember { get; }
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DynamicMethodInvocationAttribute"/> class. 
		/// </summary>
		public DynamicMethodInvocationAttribute(string methodMember)
		{
			this.MethodMember = methodMember;
		}
		
		#endregion
		
		#region Methods

		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			yield break;
		}
		
		#endregion
	}
}
