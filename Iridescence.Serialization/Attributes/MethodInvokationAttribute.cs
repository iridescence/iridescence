﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Specifies that a <see cref="SerializationMember"/> represents the container of parameters to a method call. The members of that container each represent one parameter and have to be marked with 
	/// </summary>
	[Serializable]
	public sealed class MethodInvokationAttribute : Attribute, ISerializationMemberAttribute, IEquatable<MethodInvokationAttribute>
	{
		#region Properties
		
		/// <summary>
		/// Gets the method to call.
		/// </summary>
		public MethodBase Method { get; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MethodInvokationAttribute"/> class. 
		/// </summary>
		public MethodInvokationAttribute(MethodBase method)
		{
			this.Method = method ?? throw new ArgumentNullException(nameof(method));
		}
		
		#endregion
		
		#region Methods

		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			MethodParameterAttribute mp = (MethodParameterAttribute)child.Attributes.SingleOrDefault(attr => attr is MethodParameterAttribute);
			if (mp == null)
				yield break;

			ParameterInfo[] parameters = this.Method.GetParameters();

			if (mp.ParameterIndex < 0 || mp.ParameterIndex >= parameters.Length)
				throw new ArgumentOutOfRangeException(nameof(MethodParameterAttribute.ParameterIndex), $"The parameter index {mp.ParameterIndex} is out of range. The method has {parameters.Length} parameters.");

			ParameterInfo parameter = parameters[mp.ParameterIndex];

			// Return this attribute so that the parameter members know what methdo they belong to.
			yield return this;

			// Copy parameter attributes to the member.
			foreach (Attribute attr in parameter.GetCustomAttributes())
			{
				if (attr is ISerializationMemberAttribute sma)
				{
					yield return sma;
				}
			}
		}

		public bool Equals(MethodInvokationAttribute other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(this.Method, other.Method);
		}

		public override bool Equals(object obj)
		{
			return ReferenceEquals(this, obj) || obj is MethodInvokationAttribute other && this.Equals(other);
		}

		public override int GetHashCode()
		{
			return this.Method.GetHashCode();
		}

		#endregion
	}
}
