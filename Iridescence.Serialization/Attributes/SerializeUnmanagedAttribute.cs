﻿using System;
using System.Collections.Generic;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Indicates that a type or member is to be serialized by copying the memory contents into the stream, if possible.
	/// This is only valid for blittable types. See https://docs.microsoft.com/en-us/dotnet/framework/interop/blittable-and-non-blittable-types
	/// </summary>
	/// <remarks>
	/// Note that <see cref="IntPtr"/> and <see cref="UIntPtr"/> are blittable, but since their size depends on the CPU architecture, it is not a good idea to blit types containing pointers.
	/// </remarks>
	[Serializable]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public class SerializeUnmanagedAttribute : Attribute, ISerializationMemberAttribute
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="SerializeUnmanagedAttribute"/> class. 
		/// </summary>
		public SerializeUnmanagedAttribute()
		{
		
		}
		
		#endregion
		
		#region Methods
		
		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			yield break;
		}
		
		#endregion
	}
}
