﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Marks a member of a type as corresponding to the parameter of a method, addressed by its parameter index.
	/// </summary>
	[Serializable]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public sealed class MethodParameterAttribute : Attribute, ISerializationMemberAttribute, IAttributeCacheControl, IEquatable<MethodParameterAttribute>
	{
		#region Properties

		/// <summary>
		/// Gets the parameter index.
		/// </summary>
		public int ParameterIndex { get; }

		bool IAttributeCacheControl.IncludeInCache => false;

		#endregion

		#region Constructors

		public MethodParameterAttribute(int parameterIndex)
		{
			this.ParameterIndex = parameterIndex;
		}

		#endregion

		#region Methods
		
		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			return Enumerable.Empty<ISerializationMemberAttribute>();
		}
		
		public bool Equals(MethodParameterAttribute other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.ParameterIndex == other.ParameterIndex;
		}

		public override bool Equals(object obj)
		{
			return ReferenceEquals(this, obj) || obj is MethodParameterAttribute other && this.Equals(other);
		}

		public override int GetHashCode()
		{
			return this.ParameterIndex;
		}

		#endregion
	}
}