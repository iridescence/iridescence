﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Indicates that a member should be serialized as if it were a value type (i.e. no referencing logic should apply).
	/// </summary>
	[Serializable]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public sealed class SerializeAsValueAttribute : Attribute, ISerializationMemberAttribute
	{
		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SerializeAsValueAttribute"/> class. 
		/// </summary>
		public SerializeAsValueAttribute()
		{

		}

		#endregion

		#region Methods

		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			return Enumerable.Empty<ISerializationMemberAttribute>();
		}
		
		#endregion
	}
}
