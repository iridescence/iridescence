﻿using System;
using System.Collections.Generic;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Indicates that all child members (recursively) of the current member should be serialized as if they were value types (i.e. no referencing logic should apply).
	/// </summary>
	[Serializable]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public sealed class SerializeTreeAsValuesAttribute : Attribute, ISerializationMemberAttribute
	{
		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SerializeTreeAsValuesAttribute"/> class. 
		/// </summary>
		public SerializeTreeAsValuesAttribute()
		{

		}

		#endregion

		#region Methods
		
		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			yield return new SerializeAsValueAttribute();
			yield return new SerializeTreeAsValuesAttribute();
		}
	
		#endregion
	}
}
