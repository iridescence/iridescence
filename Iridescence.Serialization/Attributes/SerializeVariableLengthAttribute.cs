﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Adds the <see cref="MemberFlags.VariableLengthEncoding"/> flag to a member.
	/// </summary>
	[Serializable]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public sealed class SerializeVariableLengthAttribute : Attribute, ISerializationMemberAttribute
	{
		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SerializeVariableLengthAttribute"/> class. 
		/// </summary>
		public SerializeVariableLengthAttribute()
		{

		}

		#endregion

		#region Methods

		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			return Enumerable.Empty<ISerializationMemberAttribute>();
		}
		
		#endregion
	}
}
