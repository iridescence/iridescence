﻿using System;
using System.Collections.Generic;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Indicates that the child members of the current member should be serialized as if they were value types (i.e. no referencing logic should apply).
	/// </summary>
	[Serializable]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
	public sealed class SerializeChildrenAsValuesAttribute : Attribute, ISerializationMemberAttribute
	{
		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SerializeChildrenAsValuesAttribute"/> class. 
		/// </summary>
		public SerializeChildrenAsValuesAttribute()
		{

		}

		#endregion

		#region Methods
		
		public IEnumerable<ISerializationMemberAttribute> GetChildAttributes(SerializationMember parent, SerializationMember child)
		{
			yield return new SerializeAsValueAttribute();
		}
		
		#endregion
	}
}
