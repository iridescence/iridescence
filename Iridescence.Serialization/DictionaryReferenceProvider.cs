﻿using System;
using System.Collections.Generic;

namespace Iridescence.Serialization
{
	/// <summary>
	/// Simple object ID provider implementation based on a dictionary.
	/// </summary>
	public sealed class DictionaryReferenceProvider<T> : IReferenceProvider<T, int>
	{
		#region Fields

		private readonly IDictionary<T, int> dictionary;

		#endregion

		#region Properties
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="DictionaryReferenceProvider{T}"/> that creates a new dictionary as backing storage.
		/// </summary>
		public DictionaryReferenceProvider()
			: this(new Dictionary<T, int>(new ReferenceEqualityComparer<T>()))
		{

		}

		/// <summary>
		/// Creates a new <see cref="DictionaryReferenceProvider{T}"/>.
		/// </summary>
		public DictionaryReferenceProvider(IDictionary<T, int> dictionary)
		{
			this.dictionary = dictionary ?? throw new ArgumentNullException();
		}

		#endregion

		#region Methods

		public int GetOrAdd(T obj, out bool wasAdded)
		{
			if (obj == null)
			{
				wasAdded = false;
				return -1;
			}

			wasAdded = !this.dictionary.TryGetValue(obj, out int id);

			if (wasAdded)
			{
				id = this.dictionary.Count;
				this.dictionary.Add(obj, id);
			}

			return id;
		}

		#endregion
	}
}
