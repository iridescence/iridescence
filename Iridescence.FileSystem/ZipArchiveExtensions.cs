﻿using System.IO.Compression;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Extension methods for <see cref="ZipArchive"/>
	/// </summary>
	public static class ZipArchiveExtensions
	{
		#region Methods

		/// <summary>
		/// Returns a <see cref="Directory"/> that represents the root of the specified <see cref="ZipArchive"/>.
		/// </summary>
		/// <param name="archive"></param>
		/// <returns></returns>
		public static Directory AsDirectory(this ZipArchive archive)
		{
			return new ZipDirectory(null, "", archive);
		}
		
		#endregion
	}
}
