﻿using System;
using System.IO;
using System.Reflection;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents an embedded assembly resource as a <see cref="File"/>.
	/// </summary>
	public class AssemblyResourceFile : File
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the assembly that contains the file.
		/// </summary>
		public Assembly Assembly { get; }
		
		/// <summary>
		/// Gets the name of the resource within the assembly. Does not necessary match the file <see cref="P:Name"/>.
		/// </summary>
		public string ResourceName { get; }

		public override bool Exists => this.Assembly.GetManifestResourceInfo(this.ResourceName) != null;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AssemblyResourceFile"/> class. 
		/// </summary>
		public AssemblyResourceFile(Directory directory, string fileName, Assembly assembly, string resourceName)
			: base(directory, fileName)
		{
			this.Assembly = assembly ?? throw new ArgumentNullException(nameof(assembly));
			this.ResourceName = resourceName ?? throw new ArgumentNullException(nameof(resourceName));
		}

		#endregion

		#region Methods

		public override Stream Open(FileMode mode, FileAccess access, FileShare share = FileShare.Read)
		{
			if (access != FileAccess.Read)
				throw new NotSupportedException("Writing to assembly resources is not supported.");

			if (mode != FileMode.Open && mode != FileMode.OpenOrCreate)
				throw new NotSupportedException("FileModes other than FileMode.Open[OrCreate] are not supported.");

			Stream stream = this.Assembly.GetManifestResourceStream(this.ResourceName);

			if (stream == null)
			{
				if (mode == FileMode.OpenOrCreate)
				{
					throw new NotSupportedException("Creating assembly resource is not supported.");
				}
				else
				{
					throw new FileNotFoundException($"Could not open assembly resource '{this.ResourceName}'.", this.ResourceName);
				}
			}

			return stream;
		}

		#endregion
	}
}
