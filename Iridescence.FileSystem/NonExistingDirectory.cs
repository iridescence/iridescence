﻿using System.Collections.Generic;
using System.Linq;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents a non-existing directory.
	/// </summary>
	internal sealed class NonExistingDirectory : Directory
	{
		#region Fields

		#endregion

		#region Properties

		public override bool Exists => false;

		public override IEnumerable<Directory> Directories => Enumerable.Empty<Directory>();

		public override IEnumerable<File> Files => Enumerable.Empty<File>();

		#endregion

		#region Constructors

		public NonExistingDirectory(Directory directory, string name) 
			: base(directory, name)
		{

		}

		#endregion

		#region Methods

		protected override File GetFileInternal(string name)
		{
			return new NonExistingFile(this, name);
		}

		protected override Directory GetDirectoryInternal(string name)
		{
			return new NonExistingDirectory(this, name);
		}

		#endregion
	}
}
