﻿using System;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// EventArgs for <see cref="FileSystem"/>.
	/// </summary>
	public class MountEventArgs : EventArgs
	{
		#region Fields
		
		#endregion
		
		#region Properties

		public Path Path { get; }

		public Directory Directory { get; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MountEventArgs"/> class. 
		/// </summary>
		public MountEventArgs(Path path, Directory directory)
		{
			this.Path = path;
			this.Directory = directory;
		}
		
		#endregion
		
		#region Methods
		
		#endregion
	}
}
