﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents the abstract base object for directories and files.
	/// </summary>
	public abstract class FileSystemObject
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the parent directory.
		/// </summary>
		/// <remarks>
		/// Can be null in case this is a root object.
		/// </remarks>
		public Directory Directory { get; }

		/// <summary>
		/// Gets the name of the file.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets a value that indicates whether this file system object exists.
		/// </summary>
		public abstract bool Exists { get; }

		#endregion

		#region ObjectPath

		/// <summary>
		/// Gets the absolute <see cref="FileSystemObject"/> path of this <see cref="FileSystemObject"/>, including this.
		/// </summary>
		public IReadOnlyList<FileSystemObject> ObjectPath
		{
			get
			{
				List<FileSystemObject> objects = new List<FileSystemObject>();

				FileSystemObject current = this;
				while(current != null)
				{
					objects.Add(current);
					current = current.Directory;
				}

				objects.Reverse();
				return objects;
			}
		}

		#endregion

		#region Path

		/// <summary>
		/// Gets the absolute <see cref="Iridescence.FileSystem.Path"/> of this <see cref="FileSystemObject"/>, including this.
		/// </summary>
		public Path Path
		{
			get
			{
				List<string> segments = new List<string>();

				FileSystemObject current = this;
				while(current.Directory != null)
				{
					segments.Add(current.Name);
					current = current.Directory;
				}

				segments.Reverse();
				return Path.Absolute(segments);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="FileSystemObject"/> class. 
		/// </summary>
		protected FileSystemObject(Directory directory, string name)
		{
			this.Directory = directory;
			this.Name = name ?? throw new ArgumentNullException(nameof(name));
		}
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Returns the root directory.
		/// </summary>
		/// <returns></returns>
		public Directory GetRoot()
		{
			if (this is Directory me && me.Directory == null)
				return me;

			Directory dir = this.Directory;

			while (dir.Directory != null)
			{
				dir = dir.Directory;
			}

			return dir;
		}

		/// <summary>
		/// Deletes this <see cref="FileSystemObject"/>.
		/// </summary>
		public virtual void Delete()
		{
			throw new NotSupportedException($"{this.GetType().Name}: Deleting is not supported.");
		}

		/// <summary>
		/// Deletes this <see cref="FileSystemObject"/>.
		/// </summary>
		public virtual Task DeleteAsync()
		{
			return Task.Run(this.Delete);
		}

		/// <summary>
		/// Copies/moves the <see cref="FileSystemObject"/>.
		/// </summary>
		/// <param name="newName">The new name of the file.</param>
		/// <param name="flags">The copy flags.</param>
		/// <param name="destination">The destination directory.</param>
		public void Copy(string newName, FileSystemObjectCopyFlags flags = FileSystemObjectCopyFlags.None, Directory destination = null)
		{
			if (newName == null)
				throw new ArgumentNullException(nameof(newName));

			if (destination == null)
				destination = this.Directory;

			this.CopyInternalAsync(newName, flags, destination);
		}

		protected virtual void CopyInternal(string newName, FileSystemObjectCopyFlags flags, Directory destination)
		{
			throw new NotSupportedException($"{this.GetType().Name}: Copying/moving is not supported.");
		}

		/// <summary>
		/// Copies/moves the <see cref="FileSystemObject"/>.
		/// </summary>
		/// <param name="newName">The new name of the file.</param>
		/// <param name="flags">The copy flags.</param>
		/// <param name="destination">The destination directory.</param>
		public async Task CopyAsync(string newName, FileSystemObjectCopyFlags flags = FileSystemObjectCopyFlags.None, Directory destination = null)
		{
			if (newName == null)
				throw new ArgumentNullException(nameof(newName));
			
			if (destination == null)
				destination = this.Directory;

			await this.CopyInternalAsync(newName, flags, destination);
		}

		protected virtual Task CopyInternalAsync(string newName, FileSystemObjectCopyFlags flags, Directory destination)
		{
			return Task.Run(() => this.CopyInternal(newName, flags, destination));
		}

		public override string ToString()
		{
			return this.Name;
		}

		#endregion
	}
}
