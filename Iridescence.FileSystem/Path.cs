﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents a file system path.
	/// </summary>
	[Serializable]
	public struct Path : IEquatable<Path>
	{
		#region Constants

		/// <summary>
		/// The default path separator character.
		/// </summary>
		public const char PathSeparator = '/';

		/// <summary>
		/// The default identifier for the current directory.
		/// </summary>
		public const string CurrentDirectory = ".";

		/// <summary>
		/// The default identifier for the parent directory.
		/// </summary>
		public const string ParentDirectory = "..";

		#endregion

		#region Fields

		private readonly int upSegments;
		private readonly IReadOnlyList<string> segments;

		private static readonly IReadOnlyList<string> emptyList = EmptyReadOnlyList<string>.Instance;
		private static readonly PathParseOptions defaultParseOptions = new PathParseOptions();

		#endregion
		
		#region Properties

		/// <summary>
		/// Gets a value that indicates whether the file path is absolute.
		/// </summary>
		public bool IsAbsolute => this.upSegments < 0;

		/// <summary>
		/// If the path is not absolute, specifies the number of steps to go upwards in the hierarchy.
		/// </summary>
		public int UpSegments => this.IsAbsolute ? 0 : this.upSegments;

		/// <summary>
		/// Gets the segments of the path.
		/// </summary>
		public IReadOnlyList<string> Segments => this.segments ?? emptyList;

		/// <summary>
		/// Returns the last segment in the path.
		/// </summary>
		public string LastSegment => this.segments == null || this.segments.Count == 0 ? null : this.segments[this.segments.Count - 1];
		
		/// <summary>
		/// Gets an empty relative path.
		/// </summary>
		public static Path Empty { get; } = Relative();

		/// <summary>
		/// Gets the absolute root path (i.e. an empty absolute path).
		/// </summary>
		public static Path Root { get; } = Absolute();
		
		#endregion
		
		#region Constructors

		/// <summary>
		/// Creates a new path with the specified properties.
		/// </summary>
		/// <param name="absolute">Whether the path is absolute. An absolute path must be no <see cref="UpSegments"/>.</param>
		/// <param name="upSegments">The number of levels a relative path goes "upwards" in the hierarchy.</param>
		/// <param name="segments">The path segments, as strings.</param>
		public Path(bool absolute, int upSegments, IEnumerable<string> segments)
		{
			if (absolute && upSegments != 0)
				throw new ArgumentException("An absolute path can not go further up.", nameof(upSegments));

			if (upSegments < 0)
				throw new ArgumentOutOfRangeException(nameof(upSegments));

			if (segments == null)
				throw new ArgumentNullException(nameof(segments));

			this.upSegments = absolute ? -1 : upSegments;

			if (segments is IReadOnlyCollection<string> collection && collection.Count == 0)
			{
				this.segments = null;
			}
			else
			{
				this.segments = segments.ToArray();

				if (this.segments.Any(string.IsNullOrEmpty))
				{
					throw new ArgumentException("Segments can not be null or empty.");
				}
			}
		}

		#endregion
		
		#region Methods

		/// <summary>
		/// Returns a new absolute path with the specified segments.
		/// </summary>
		/// <param name="segments">The path segments, as strings.</param>
		/// <returns></returns>
		public static Path Absolute(IEnumerable<string> segments)
		{
			return new Path(true, 0, segments);
		}

		/// <summary>
		/// Returns a new absolute path with the specified segments.
		/// </summary>
		/// <param name="segments">The path segments, as strings.</param>
		/// <returns></returns>
		public static Path Absolute(params string[] segments)
		{
			return new Path(true, 0, segments);
		}

		/// <summary>
		/// Returns a new relative path with the specified segments.
		/// </summary>
		/// <param name="segments">The path segments, as strings.</param>
		/// <returns></returns>
		public static Path Relative(IEnumerable<string> segments)
		{
			return new Path(false, 0, segments);
		}

		/// <summary>
		/// Returns a new relative path with the specified segments.
		/// </summary>
		/// <param name="segments">The path segments, as strings.</param>
		/// <returns></returns>
		public static Path Relative(params string[] segments)
		{
			return new Path(false, 0, segments);
		}


		/// <summary>
		/// Returns a new relative path with the specified segments.
		/// </summary>
		/// <param name="upSegments">The number of levels a relative path goes "upwards" in the hierarchy.</param>
		/// <param name="segments">The path segments, as strings.</param>
		/// <returns></returns>
		public static Path Relative(int upSegments, IEnumerable<string> segments)
		{
			return new Path(false, upSegments, segments);
		}

		/// <summary>
		/// Returns a new relative path with the specified segments.
		/// </summary>
		/// <param name="upSegments">The number of levels a relative path goes "upwards" in the hierarchy.</param>
		/// <param name="segments">The path segments, as strings.</param>
		/// <returns></returns>
		public static Path Relative(int upSegments, params string[] segments)
		{
			return new Path(false, upSegments, segments);
		}

		public override bool Equals(object obj)
		{
			if (!(obj is Path other))
				return false;

			return this.Equals(other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = this.upSegments;

				if (this.segments != null)
				{
					foreach (string segment in this.segments)
					{
						hash = (hash * 397) ^ segment.GetHashCode();
					}
				}

				return hash;
			}
		}

		public bool Equals(Path other)
		{
			if (other.upSegments != this.upSegments)
				return false;

			if (other.segments == null && this.segments == null)
				return true;

			if (other.segments == null || this.segments == null)
				return false;

			if (other.segments.Count != this.segments.Count)
				return false;

			for (int i = 0; i < this.segments.Count; ++i)
			{
				if (!String.Equals(this.segments[i], other.segments[i], StringComparison.Ordinal))
					return false;
			}

			return true;
		}

		/// <summary>
		/// For absolute paths, returns whether this path starts with the segments from the specified path.
		/// </summary>
		/// <param name="start"></param>
		/// <returns></returns>
		public bool StartsWith(Path start)
		{
			if (!this.IsAbsolute || !start.IsAbsolute)
				throw new ArgumentException("Path must be absolute.", nameof(start));

			if (start.Segments.Count > this.Segments.Count)
				return false;

			for (int i = 0; i < start.Segments.Count; ++i)
			{
				if (!String.Equals(this.Segments[i], start.Segments[i], StringComparison.Ordinal))
					return false;
			}

			return true;
		}

		public static bool TryParse(string str, out Path path)
		{
			return TryParse(str, null, out path);
		}

		/// <summary>
		/// Tries parsing a <see cref="Path"/> from a string.
		/// </summary>
		/// <param name="str">The path string.</param>
		/// <param name="options">Additional parsing options.</param>
		/// <param name="path">The resultng path.</param>
		/// <returns>True, if parsing succeeded. False otherwise.</returns>
		public static bool TryParse(string str, PathParseOptions options, out Path path)
		{
			if(str == null)
				throw new ArgumentNullException(nameof(str));

			try
			{
				path = Parse(str, options);
			}
			catch (ArgumentException)
			{
				path = default(Path);
				return false;
			}

			return true;
		}

		/// <summary>
		/// Parses a <see cref="Path"/> from a string.
		/// </summary>
		/// <param name="str">The path string.</param>
		/// <param name="options">Additional parsing options.</param>
		/// <returns></returns>
		public static Path Parse(string str, PathParseOptions options = null)
		{
			if(str == null)
				throw new ArgumentNullException(nameof(str));

			options = options ?? defaultParseOptions;

			int upSegments = 0;
			StringReader reader = new StringReader(str);

			List<string> segments = new List<string>();
			StringBuilder currentSegment = new StringBuilder();

			void addSegment()
			{
				if (currentSegment.Length == 0)
				{
					if (segments.Count == 0 && upSegments == 0)
					{
						// Path separator at the beginning, make path absolute.
						upSegments = -1;
						return;
					}

					throw new ArgumentException("Path segment can not be empty.", nameof(str));
				}

				string segmentStr = currentSegment.ToString();
				currentSegment.Clear();

				if (segmentStr == options.CurrentDirectorySegment)
				{
					// Nothing to do for "current directory".
					return;
				}
				
				if (segmentStr == options.ParentDirectorySegment)
				{
					// Go up when encountering "parent directory".
					if (segments.Count == 0)
					{
						if (upSegments < 0)
						{
							throw new ArgumentException("An absolute path can't navigate past the root directory.", nameof(str));
						}

						++upSegments;
					}
					else
					{
						segments.RemoveAt(segments.Count - 1);
					}
					return;
				}

				segments.Add(segmentStr);
			}

			int c;
			while ((c = reader.Read()) >= 0)
			{
				currentSegment.Append((char)c);

				// Search for separator.
				foreach (string separator in options.PathSeparators)
				{
					if (currentSegment.Length < separator.Length)
						continue;

					bool match = true;
					for (int i = 0, j = currentSegment.Length - separator.Length; i < separator.Length; ++i, ++j)
					{
						if (separator[i] != currentSegment[j])
						{
							match = false;
							break;
						}
					}

					if (match)
					{
						currentSegment.Length -= separator.Length;
						addSegment();
						break;
					}
				}
			}

			if(currentSegment.Length > 0)
				addSegment();

			return upSegments < 0 ? Absolute(segments) : Relative(upSegments, segments);
		}

		/// <summary>
		/// Combines the two paths.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static Path Combine(Path a, Path b)
		{
			if (b.IsAbsolute)
				return b;

			if (b.UpSegments > a.Segments.Count)
			{
				if(a.IsAbsolute)
					throw new ArgumentException("An absolute path can't navigate past the root directory.", nameof(b));

				return Relative(a.UpSegments + b.UpSegments - a.Segments.Count, b.Segments);
			}

			List<string> segments = new List<string>(a.Segments.Take(a.Segments.Count - b.UpSegments));
			segments.AddRange(b.Segments);

			if (a.IsAbsolute)
			{
				return Absolute(segments);
			}

			return Relative(a.UpSegments, segments);	
		}

		/// <summary>
		/// Appends the specified segment to a path.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="segment"></param>
		/// <returns></returns>
		public static Path Combine(Path path, string segment)
		{
			return Combine(path, Relative(segment));
		}

		/// <summary>
		/// Combines this path with another path.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public Path Combine(Path other)
		{
			return Combine(this, other);
		}

		/// <summary>
		/// Appends the specified segment to the path.
		/// </summary>
		/// <param name="segment"></param>
		/// <returns></returns>
		public Path Combine(string segment)
		{
			return Combine(this, segment);
		}

		/// <summary>
		/// Returns a new path that represents the parent of the current path. Removes the last segment or increments <see cref="UpSegments"/> if there none.
		/// </summary>
		/// <returns></returns>
		public Path Up()
		{
			return Combine(this, Relative(1));
		}

		/// <summary>
		/// Assuming the path identifies a file, returns a path without the file's extension by stripping everything after the last dot in the name, including the dot.
		/// </summary>
		/// <returns></returns>
		public Path WithoutFileExtension()
		{
			string last = this.LastSegment;
			if (last == null)
				throw new InvalidOperationException("Path is empty.");

			int lastDot = last.LastIndexOf('.');
			if (lastDot < 0)
				return this;

			var segments = this.Segments
				.Take(this.Segments.Count - 1)
				.Append(last.Substring(0, lastDot));

			if (this.IsAbsolute)
				return Absolute(segments);
			
			return Relative(this.UpSegments, segments);
		}

		/// <summary>
		/// Returns just the file extension, including the dot. (e.g. "Image.png" would return ".png").
		/// </summary>
		/// <returns></returns>
		public string GetFileExtension()
		{
			string last = this.LastSegment;
			if (last == null)
				throw new InvalidOperationException("Path is empty.");

			int lastDot = last.LastIndexOf('.');
			if (lastDot < 0)
				return string.Empty;

			return last.Substring(lastDot);
		}

		public static bool operator ==(Path a, Path b)
		{
			return a.Equals(b);
		}

		public static bool operator !=(Path a, Path b)
		{
			return !a.Equals(b);
		}

		public static Path operator +(Path a, Path b)
		{
			return Path.Combine(a, b);
		}

		public static Path operator +(Path a, string b)
		{
			return Path.Combine(a, b);
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			bool needSeparator = false;

			if (this.IsAbsolute)
			{
				str.Append(PathSeparator);
			}
			else
			{
				for (int i = 0; i < this.UpSegments; ++i)
				{
					if (needSeparator)
					{
						str.Append(PathSeparator);
					}

					str.Append(ParentDirectory);
					needSeparator = true;
				}
			}
			
			foreach (string segment in this.Segments)
			{
				if (needSeparator)
				{
					str.Append(PathSeparator);
				}

				str.Append(segment);
				needSeparator = true;
			}

			return str.ToString();
		}

		#endregion
	}
}
