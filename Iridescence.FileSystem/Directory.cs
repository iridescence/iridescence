﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents an abstract directory that may contain sub-directories and files.
	/// </summary>
	public abstract class Directory : FileSystemObject
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets all sub-directories of this <see cref="Directory"/>.
		/// </summary>
		/// <returns></returns>
		public abstract IEnumerable<Directory> Directories { get; }

		/// <summary>
		/// Gets all files in this <see cref="Directory"/>.
		/// </summary>
		/// <returns></returns>
		public abstract IEnumerable<File> Files { get; }

		/// <summary>
		/// Gets all child <see cref="FileSystemObject">objects</see> of the directory. Combines directories and files.
		/// </summary>
		public IEnumerable<FileSystemObject> Objects => ((IEnumerable<FileSystemObject>)this.Directories).Concat(this.Files);

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Directory"/> class. 
		/// </summary>
		protected Directory(Directory directory, string name)
			: base(directory, name)
		{
			
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates the directory.
		/// </summary>
		public virtual void Create()
		{
			throw new NotSupportedException($"{this.GetType().Name}: Creating is not supported.");
		}

		/// <summary>
		/// Creates the directory.
		/// </summary>
		public virtual Task CreateAsync()
		{
			return Task.Run(this.Create);
		}

		/// <summary>
		/// Deletes an empty directory.
		/// </summary>
		public sealed override void Delete()
		{
			this.Delete(false);
		}

		/// <summary>
		/// Deletes an empty directory asynchronously.
		/// </summary>
		public sealed override Task DeleteAsync()
		{
			return this.DeleteAsync(false);
		}

		/// <summary>
		/// Deletes a directory.
		/// </summary>
		/// <param name="recursive">True, if the directory should be deleted recursively if it is not empty.</param>
		public virtual void Delete(bool recursive)
		{
			throw new NotSupportedException($"{this.GetType().Name}: Deleting is not supported.");
		}

		/// <summary>
		/// Deletes a directory asynchronously.
		/// </summary>
		/// <param name="recursive">True, if the directory should be deleted recursively if it is not empty.</param>
		public virtual Task DeleteAsync(bool recursive)
		{
			return Task.Run(() => this.Delete(recursive));
		}

		/// <summary>
		/// Returns the direct sub-directory with the specified name.
		/// </summary>
		/// <param name="name">The name of the directory (never a path).</param>
		/// <returns>The directory. An object is always returned, no matter if the directory exists or not.</returns>
		protected virtual Directory GetDirectoryInternal(string name)
		{
			return this.Directories.FirstOrDefault(d => d.Name == name) ?? new NonExistingDirectory(this, name);
		}

		/// <summary>
		/// Returns the file with the specified name.
		/// </summary>
		/// <param name="name">The name of the file (never a path).</param>
		/// <returns>The file. An object is always returned, no matter if the file exists or not.</returns>
		protected virtual File GetFileInternal(string name)
		{
			return this.Files.FirstOrDefault(d => d.Name == name) ?? new NonExistingFile(this, name);
		}

		/// <summary>
		/// Returns the <see cref="Directory"/> at the specified path.
		/// </summary>
		/// <param name="path">The path to the directory.</param>
		/// <returns>The directory. An object is always returned, no matter if the directory exists or not.</returns>
		public Directory GetDirectory(Path path)
		{
			Directory currentDirectory = path.IsAbsolute ? this.GetRoot() : this;

			int upSegments = path.UpSegments;

			while (upSegments > 0)
			{
				currentDirectory = currentDirectory.Directory;

				if (currentDirectory == null)
					throw new ArgumentException("The specified path is not valid for the directory because it extends past the root directory.", nameof(path));

				--upSegments;
			}

			for (int i = 0; i < path.Segments.Count; ++i)
			{
				currentDirectory = currentDirectory.GetDirectoryInternal(path.Segments[i]);
				if (currentDirectory == null)
					throw new InvalidOperationException($"{nameof(this.GetDirectoryInternal)} must not return null.");
			}

			return currentDirectory;
		}

		/// <summary>
		/// Returns the <see cref="Directory"/> with the specified name.
		/// If the file does not exists, a new directory is returned which has <see cref="P:Directory.Exists"/> set to false.
		/// </summary>
		/// <param name="path">The path to the directory.</param>
		/// <returns>The directory. An object is always returned, no matter if the directory exists or not.</returns>
		public Directory GetDirectory(string path)
		{
			return this.GetDirectory(Path.Parse(path));
		}

		/// <summary>
		/// Gets the <see cref="Directory"/> at the specified path.
		/// </summary>
		/// <param name="path">The path to the directory.</param>
		/// <param name="directory">The directory. An object is always returned, no matter if the directory exists or not.</param>
		/// <returns>True, if the directory exists. False otherwise.</returns>
		public bool GetDirectory(Path path, out Directory directory)
		{
			directory = this.GetDirectory(path);
			return directory.Exists;
		}

		/// <summary>
		/// Gets the <see cref="Directory"/> with the specified name.
		/// If the file does not exists, a new directory is returned which has <see cref="P:Directory.Exists"/> set to false.
		/// </summary>
		/// <param name="path">The path to the directory.</param>
		/// <param name="directory">The directory. An object is always returned, no matter if the directory exists or not.</param>
		/// <returns>True, if the directory exists. False otherwise.</returns>
		public bool GetDirectory(string path, out Directory directory)
		{
			directory = this.GetDirectory(path);
			return directory.Exists;
		}
		
		/// <summary>
		/// Returns the <see cref="File"/> at the specified path.
		/// </summary>
		/// <param name="path">The path to the file.</param>
		/// <returns>The file. An object is always returned, no matter if the file exists or not.</returns>
		public File GetFile(Path path)
		{
			if (path.Segments.Count == 0)
				throw new ArgumentException("Path can not be empty.", nameof(path));

			if (!path.IsAbsolute && path.UpSegments == 0 && path.Segments.Count == 1)
				return this.GetFileInternal(path.Segments[0]);

			Directory dir = this.GetDirectory(path.Up());
			File file = dir.GetFileInternal(path.Segments[path.Segments.Count - 1]);
			if (file == null)
				throw new InvalidOperationException($"{nameof(this.GetFileInternal)} must not return null.");

			return file;
		}

		/// <summary>
		/// Returns the <see cref="File"/> with the specified name.
		/// If the file does not exists, a new file is returned which has <see cref="P:File.Exists"/> set to false.
		/// </summary>
		/// <param name="path">The path to the file.</param>
		/// <returns>The file. An object is always returned, no matter if the file exists or not.</returns>
		public File GetFile(string path)
		{
			return this.GetFile(Path.Parse(path));
		}
		
		/// <summary>
		/// Gets the <see cref="File"/> at the specified path.
		/// </summary>
		/// <param name="path">The path to the file.</param>
		/// <param name="file">The file. An object is always returned, no matter if the file exists or not.</param>
		/// <returns>True, if the file exists. False otherwise.</returns>
		public bool GetFile(Path path, out File file)
		{
			file = this.GetFile(path);
			return file.Exists;
		}
		
		/// <summary>
		/// Gets the <see cref="File"/> with the specified name.
		/// If the file does not exists, a new file is returned which has <see cref="P:File.Exists"/> set to false.
		/// </summary>
		/// <param name="path">The path to the file.</param>
		/// <param name="file">The file. An object is always returned, no matter if the file exists or not.</param>
		/// <returns>True, if the file exists. False otherwise.</returns>
		public bool GetFile(string path, out File file)
		{
			file = this.GetFile(path);
			return file.Exists;
		}
		
		/// <summary>
		/// Opens a file in this directory.
		/// </summary>
		/// <param name="path">The path to the file.</param>
		/// <param name="mode">The file mode.</param>
		/// <param name="access">The file access.</param>
		/// <returns>A stream for the file.</returns>
		public Stream Open(string path, FileMode mode, FileAccess access)
		{
			return this.GetFile(path).Open(mode, access);
		}

		/// <summary>
		/// Opens a file relative to this directory.
		/// </summary>
		/// <param name="path">The path to the file.</param>
		/// <param name="mode">The file mode.</param>
		/// <param name="access">The file access.</param>
		/// <returns>A stream for the file.</returns>
		public Stream Open(Path path, FileMode mode, FileAccess access)
		{
			return this.GetFile(path).Open(mode, access);
		}
		
		#endregion
	}
}
