﻿using System;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// <see cref="FileSystemObject"/> copy operation flags.
	/// </summary>
	[Flags]
	public enum FileSystemObjectCopyFlags
	{
		/// <summary>
		/// No flags.
		/// </summary>
		None = 0,

		/// <summary>
		/// Whether to overwrite the destination if it already exists.
		/// </summary>
		Overwrite = 1,

		/// <summary>
		/// Whether to delete the source after successful copy.
		/// </summary>
		DeleteSource = 2,
	}
}
