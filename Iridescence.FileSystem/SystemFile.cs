﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents a system file.
	/// </summary>
	public class SystemFile : File
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the real name of the file.
		/// </summary>
		public string RealName { get; }

		/// <inheritdoc />
		public override bool Exists => System.IO.File.Exists(this.RealName);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SystemFile.
		/// </summary>
		public SystemFile(Directory directory, string name, string realName)
			: base(directory, name)
		{
			FileInfo info = new FileInfo(realName);
			this.RealName = info.FullName;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a <see cref="SystemFile"/> that wraps the specified real file.
		/// </summary>
		/// <param name="realPath"></param>
		/// <returns></returns>
		public static SystemFile FromRealPath(string realPath)
		{
			string name = System.IO.Path.GetFileName(realPath);
			return new SystemFile(null, name, realPath);
		}

		/// <summary>
		/// Opens the file.
		/// </summary>
		/// <returns></returns>
		public override Stream Open(FileMode mode, FileAccess access, FileShare share = FileShare.Read)
		{
			return new FileStream(this.RealName, mode, access, share);
		}

		public override void Delete()
		{
			System.IO.File.Delete(this.RealName);
		}

		public override Task DeleteAsync()
		{
			return Task.Run(() => System.IO.File.Delete(this.RealName));
		}

		protected override async Task CopyInternalAsync(string newName, FileSystemObjectCopyFlags flags, Directory destination)
		{
			if (!(destination is SystemDirectory sysDir))
			{
				await base.CopyInternalAsync(newName, flags, destination);
				return;
			}

			if (newName.Length == 0 || !SystemUtil.IsValidFileName(newName))
				throw new ArgumentException("Invalid file name.", nameof(newName));

			string parent = sysDir.RealName;
			if (string.IsNullOrEmpty(parent))
				throw new InvalidOperationException("Invalid destination directory.");

			string newPath = System.IO.Path.Combine(parent, newName);
			bool overwrite = (flags & FileSystemObjectCopyFlags.Overwrite) != 0;

			if ((flags & FileSystemObjectCopyFlags.DeleteSource) != 0)
			{
				try
				{
					await Task.Run(() => { System.IO.File.Move(this.RealName, newPath); });
				}
				catch (IOException) when (overwrite)
				{
					// .NET standard is missing Move(string name, bool overwrite) method, so we emulate it here.
					await Task.Run(() =>
					{
						System.IO.File.Copy(this.RealName, newPath, true);
						System.IO.File.Delete(this.RealName);
					});
				}
			}
			else
			{
				await Task.Run(() => { System.IO.File.Copy(this.RealName, newPath, overwrite); });
			}
		}
	
		#endregion
	}
}
