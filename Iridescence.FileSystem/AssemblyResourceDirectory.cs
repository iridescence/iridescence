﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Implements a <see cref="Directory"/> that creates a file-system view for assembly resources.
	/// </summary>
	public class AssemblyResourceDirectory : Directory
	{
		#region Properties

		/// <summary>
		/// Gets the assembly that contains the file.
		/// </summary>
		public Assembly Assembly { get; }

		/// <summary>
		/// Gets the resource name prefix of this directory.
		/// </summary>
		public string Prefix { get; }

		/// <summary>
		/// Gets the path separator used for sub-directories and files.
		/// </summary>
		public ImmutableArray<string> Separators { get; }

		public override bool Exists
		{
			get
			{
				if (this.Prefix == null)
					return true; // No prefix means root, the root is the assembly which does exist.

				// Check if there is anything starting with the prefix.
				return this.Assembly.GetManifestResourceNames().Any(str => str.StartsWith(this.Prefix));
			}
		}

		public override IEnumerable<Directory> Directories
		{
			get
			{
				HashSet<string> returnedDirs = new HashSet<string>();
				foreach (string resName in this.Assembly.GetManifestResourceNames())
				{
					int offset = 0;

					if (this.Prefix != null)
					{
						if (!resName.StartsWith(this.Prefix))
							continue;

						offset = this.Prefix.Length;
					}

					int separatorIndex = this.indexOfSeparator(resName, offset, out string separator);
					if (separatorIndex < 0) // Not a directory.
						continue;

					string dirName = resName.Substring(offset, separatorIndex - offset);
					if (returnedDirs.Add(dirName))
					{
						string dirPrefix = resName.Substring(0, separatorIndex + separator.Length);
						yield return new AssemblyResourceDirectory(this, dirName, dirPrefix);
					}
				}
			}
		}

		public override IEnumerable<File> Files
		{
			get
			{
				foreach (string resName in this.Assembly.GetManifestResourceNames())
				{
					int offset = 0;

					if (this.Prefix != null)
					{
						if (!resName.StartsWith(this.Prefix))
							continue;

						offset = this.Prefix.Length;
					}

					int separatorIndex = this.indexOfSeparator(resName, offset, out _);
					if (separatorIndex >= 0) // File is in a sub-directory.
						continue;

					yield return new AssemblyResourceFile(this, resName.Substring(offset), this.Assembly, resName);
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AssemblyResourceDirectory"/> class. 
		/// </summary>
		public AssemblyResourceDirectory(AssemblyResourceDirectory directory, string name, string prefix)
			: base(directory, name)
		{
			this.Assembly = directory.Assembly;
			this.Prefix = prefix;
			this.Separators = directory.Separators;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AssemblyResourceDirectory"/> class. 
		/// </summary>
		public AssemblyResourceDirectory(Directory directory, string name, Assembly assembly, string prefix, ImmutableArray<string> separators)
			: base(directory, name)
		{
			this.Assembly = assembly;
			this.Prefix = prefix;
			this.Separators = separators;
		}

		#endregion

		#region Methods

		private int indexOfSeparator(string str, int startIndex, out string separator)
		{
			foreach (string s in this.Separators)
			{
				int index = str.IndexOf(s, startIndex, StringComparison.Ordinal);
				if (index >= 0)
				{
					separator = s;
					return index;
				}
			}

			separator = null;
			return -1;
		}

		/// <summary>
		/// Returns a <see cref="AssemblyResourceDirectory"/> that represents the root directory of the specified assembly's resources.
		/// </summary>
		/// <param name="assembly"></param>
		/// <param name="name"></param>
		/// <param name="separators"></param>
		/// <returns></returns>
		public static AssemblyResourceDirectory AsRoot(Assembly assembly, string name = "", params string[] separators)
		{
			return new AssemblyResourceDirectory(null, name, assembly, null, separators.ToImmutableArray());
		}

		#endregion
	}
}
