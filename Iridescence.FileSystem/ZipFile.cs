﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Implements a <see cref="File"/> based on an <see cref="ZipArchiveEntry"/>.
	/// </summary>
	internal sealed class ZipFile : File
	{
		#region Fields

		private readonly ZipArchive archive;
		
		#endregion

		#region Properties

		private ZipArchiveEntry entry => this.archive.GetEntry(Path.Relative(this.Path.Segments).ToString());

		public override bool Exists => this.entry != null;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ZipFile"/> class. 
		/// </summary>
		public ZipFile(Directory directory, string name, ZipArchive archive)
			: base(directory, name)
		{
			this.archive = archive;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ZipFile"/> class. 
		/// </summary>
		public ZipFile(Directory directory, ZipArchiveEntry entry)
			: base(directory, entry.Name)
		{
			this.archive = entry.Archive;
		}

		#endregion

		#region Methods

		public override void Delete()
		{
			this.entry?.Delete();
		}

		public override Task DeleteAsync()
		{
			this.entry?.Delete();
			return Task.CompletedTask;
		}

		private void create()
		{
			this.archive.CreateEntry(Path.Relative(this.Path.Segments).ToString());
		}

		public override Stream Open(FileMode mode, FileAccess access, FileShare share = FileShare.Read)
		{
			switch (this.archive.Mode)
			{
				case ZipArchiveMode.Create:
					switch (mode)
					{
						case FileMode.Create:
						case FileMode.CreateNew:
						case FileMode.OpenOrCreate:
							if ((access & FileAccess.Read) != 0)
								throw new ArgumentException("Read access is not possible because the archive is in 'Create' mode.", nameof(access));

							if (this.Exists)
								throw new IOException($"Can't re-create zip entry '{this.Path}' because the archive is in 'Create' mode.");

							this.create();
							return this.entry.Open();

						default:
							throw new ArgumentException($"File mode '{mode}' is not valid because the archive is in 'Create' mode.", nameof(mode));
					}

				case ZipArchiveMode.Read:
					switch (mode)
					{
						case FileMode.Open:
						case FileMode.OpenOrCreate:
							if (!this.Exists)
								throw new FileNotFoundException("Zip entry does not exist.", this.Path.ToString());

							if ((access & FileAccess.Write) != 0)
								throw new InvalidOperationException("Zip entry can not be opened with write access because the archive is in 'Read' mode.");

							return this.entry.Open();

						default:
							throw new InvalidOperationException($"File mode {mode} is not valid because the archive is in 'Read' mode.");
					}

				case ZipArchiveMode.Update:
					switch (mode)
					{
						case FileMode.Append:
						{
							if (!this.Exists)
							{
								this.create();
								return this.entry.Open();
							}

							Stream stream = this.entry.Open();
							stream.Position = stream.Length;
							return stream;
						}

						case FileMode.Create:
						{
							if (!this.Exists)
							{
								this.create();
								return this.entry.Open();
							}

							Stream stream = this.entry.Open();
							stream.SetLength(0);
							return stream;
						}

						case FileMode.CreateNew:
						{
							if (this.Exists)
							{
								throw new IOException($"The file '{this.Path}' already exists.");
							}

							this.create();
							return this.entry.Open();
						}

						case FileMode.Open:
						{
							if (!this.Exists)
								throw new FileNotFoundException("Zip entry does not exist.", this.Path.ToString());

							return this.entry.Open();
						}

						case FileMode.OpenOrCreate:
						{
							if (this.Exists)
								return this.entry.Open();

							this.create();
							return this.entry.Open();
						}

						case FileMode.Truncate:
						{
							if (!this.Exists)
								throw new FileNotFoundException("Zip entry does not exist.", this.Path.ToString());

							Stream stream = this.entry.Open();
							stream.SetLength(0);
							return stream;
						}

						default:
							throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
					}

				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		#endregion
	}
}
