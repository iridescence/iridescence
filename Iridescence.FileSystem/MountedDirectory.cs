﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents a directory in a file-system defined by a <see cref="FileSystem"/>.
	/// </summary>
	public class MountedDirectory : Directory
	{
		#region Fields

		private readonly FileSystem fileSystem;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the real directory that is backing this mounted directory, if there is one. Otherwise, this property is null.
		/// </summary>
		public Directory RealDirectory
		{
			get
			{
				Path currentPath = this.Path;
				int up = 0;
				Directory dir;

				// Go up the hierarchy until we find a mount point.
				for(;;)
				{
					if (this.fileSystem.TryGetValue(currentPath, out dir))
						break;

					dir = null;
					++up;

					if (currentPath == Path.Root)
						break;

					currentPath = currentPath.Up();
				}

				dir = dir ?? new NonExistingDirectory(null, "");

				// The mount point is already the directory we are looking for.
				if (up == 0)
					return dir;

				// Otherwise, get the directory relative to the mount point.
				Path relPath = Path.Relative(this.Path.Segments.Skip(this.Path.Segments.Count - up));
				return dir.GetDirectory(relPath);
			}
		}

		public override IEnumerable<Directory> Directories
		{
			get
			{
				Directory realDir = this.RealDirectory;
				IReadOnlyDictionary<string, Directory> mountedDirs = this.fileSystem.GetMountedDirectories(this.Path);

				foreach (string name in realDir.Directories.Select(d => d.Name).Union(mountedDirs.Keys).Distinct())
				{
					yield return new MountedDirectory(this, this.fileSystem, name);
				}
			}
		}

		public override IEnumerable<File> Files => this.RealDirectory.Files.Select(f => new MountedFile(this, f.Name));

		public override bool Exists => this.RealDirectory?.Exists ?? false;

		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="MountedDirectory"/> class. 
		/// </summary>
		internal MountedDirectory(Directory parent, FileSystem fileSystem, string name)
			: base(parent, name)
		{
			this.fileSystem = fileSystem ?? throw new ArgumentNullException(nameof(fileSystem));
		}

		#endregion

		#region Methods
		
		protected override Directory GetDirectoryInternal(string name)
		{
			return new MountedDirectory(this, this.fileSystem, name);
		}

		protected override File GetFileInternal(string name)
		{
			return new MountedFile(this, name);
		}

		protected override void CopyInternal(string newName, FileSystemObjectCopyFlags flags, Directory destination)
		{
			this.RealDirectory.Copy(newName, flags, destination);
		}

		protected override Task CopyInternalAsync(string newName, FileSystemObjectCopyFlags flags, Directory destination)
		{
			return this.RealDirectory.CopyAsync(newName, flags, destination);
		}

		public override void Create()
		{
			this.RealDirectory.Create();
		}

		public override Task CreateAsync()
		{
			return this.RealDirectory.CreateAsync();
		}

		public override void Delete(bool recursive)
		{
			this.RealDirectory.Delete(recursive);
		}

		public override Task DeleteAsync(bool recursive)
		{
			return this.RealDirectory.DeleteAsync(recursive);
		}

		#endregion
	}
}
