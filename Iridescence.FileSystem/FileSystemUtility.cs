﻿using System.IO;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// File system utility methods.
	/// </summary>
	public static class FileSystemUtility
	{
		public static bool TryGetRealPath(this File file, out string realPath)
		{
			if (file is SystemFile sysFile)
			{
				realPath = sysFile.RealName;
				return true;
			}

			if (file is MountedFile mntFile)
			{
				return mntFile.RealFile.TryGetRealPath(out realPath);
			}

			if (file is NonExistingFile nxFile)
			{
				Directory parent = nxFile.Directory;
				if (parent != null && TryGetRealPath(parent, out string parentRealPath))
				{
					realPath = System.IO.Path.Combine(parentRealPath, nxFile.Name);
					return true;
				}
			}

			realPath = null;
			return false;
		}

		public static bool TryGetRealPath(this Directory dir, out string realPath)
		{
			if (dir is SystemDirectory sysDir)
			{
				realPath = sysDir.RealName;
				return true;
			}

			if (dir is MountedDirectory mntDir)
			{
				return mntDir.RealDirectory.TryGetRealPath(out realPath);
			}

			if (dir is NonExistingDirectory nxDir)
			{
				Directory parent = nxDir.Directory;
				if (parent != null && TryGetRealPath(parent, out string parentRealPath))
				{
					realPath = System.IO.Path.Combine(parentRealPath, nxDir.Name);
					return true;
				}
			}

			realPath = null;
			return false;
		}

		public static Stream OpenRead(this File file)
		{
			return file.Open(FileMode.Open, FileAccess.Read);
		}

		public static Stream OpenWrite(this File file)
		{
			return file.Open(FileMode.OpenOrCreate, FileAccess.Write);
		}

		public static Stream OpenCreate(this File file)
		{
			return file.Open(FileMode.Create, FileAccess.Write);
		}
	}
}
