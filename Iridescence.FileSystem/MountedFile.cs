﻿using System.IO;
using System.Threading.Tasks;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents a virtual mounted file.
	/// </summary>
	public class MountedFile : File
	{
		#region Fields

		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the real file that is backing this mounted file.
		/// </summary>
		public File RealFile => ((MountedDirectory)this.Directory).RealDirectory.GetFile(this.Name);

		public override bool Exists => this.RealFile.Exists;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MountedFile"/> class. 
		/// </summary>
		internal MountedFile(MountedDirectory parent, string name)
			: base(parent, name)
		{
			
		}

		#endregion

		#region Methods

		public override void Delete()
		{
			this.RealFile.Delete();
		}

		public override Task DeleteAsync()
		{
			return this.RealFile.DeleteAsync();
		}

		protected override void CopyInternal(string newName, FileSystemObjectCopyFlags flags, Directory destination)
		{
			this.RealFile.Copy(newName, flags, destination);
		}

		protected override Task CopyInternalAsync(string newName, FileSystemObjectCopyFlags flags, Directory destination)
		{
			return this.RealFile.CopyAsync(newName, flags, destination);
		}

		public override Stream Open(FileMode mode, FileAccess access, FileShare share = FileShare.Read)
		{
			return this.RealFile.Open(mode, access, share);
		}

		#endregion
	}
}
