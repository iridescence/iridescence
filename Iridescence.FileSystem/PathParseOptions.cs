﻿namespace Iridescence.FileSystem
{
	/// <summary>
	/// Contains path parsing options.
	/// </summary>
	public class PathParseOptions
	{
		#region Properties

		/// <summary>
		/// Gets or sets the collections of path separator strings. The default is "/" only.
		/// </summary>
		public string[] PathSeparators { get; set; } = {"/"};

		/// <summary>
		/// Gets or sets the "current directory" segment. The default is ".", which is used on virtually all operating systems.
		/// </summary>
		public string CurrentDirectorySegment { get; set; } = Path.CurrentDirectory;

		/// <summary>
		/// Gets or sets the "parent directory" segment. The default is "..", which is used on virtually all operating systems.
		/// </summary>
		public string ParentDirectorySegment { get; set; } = Path.ParentDirectory;

		#endregion
	}
}
