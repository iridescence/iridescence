﻿using System.Collections.Generic;
using System.Linq;

namespace Iridescence.FileSystem
{
	internal static class SystemUtil
	{
		#region Fields

		private static readonly HashSet<char> invalidFileChars = new HashSet<char>(System.IO.Path.GetInvalidFileNameChars());
		
		#endregion

		#region Methods

		public static bool IsValidFileName(string name)
		{
			return !name.Any(c => invalidFileChars.Contains(c));
		}

		#endregion
	}
}
