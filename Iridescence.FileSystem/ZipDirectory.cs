﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents a directory inside of a <see cref="ZipArchive"/>.
	/// </summary>
	internal sealed class ZipDirectory : Directory
	{
		#region Fields

		private readonly ZipArchive archive;

		private static readonly PathParseOptions zipPathOptions = new PathParseOptions() {CurrentDirectorySegment = null, ParentDirectorySegment = null};

		#endregion

		#region Properties

		public override bool Exists
		{
			get
			{
				if (this.Directory == null)
					return true; // Root directory in a zip archive always exists.

				return this.Directory.Directories.Any(d => d.Name == this.Name); // Check if parent contains a directory with this name.
			}
		}

		public override IEnumerable<Directory> Directories
		{
			get
			{
				Path myPath = this.Path;
				HashSet<Path> returnedPaths = new HashSet<Path>();

				// Since zip archives do not really have directories, we construct a list of directories from the file names.
				foreach (ZipArchiveEntry entry in this.archive.Entries)
				{
					Path entryPath = Path.Absolute(Path.Parse(entry.FullName, zipPathOptions).Segments);

					if (entryPath.Segments.Count > myPath.Segments.Count + 1)
						continue;

					Path dirPath = Path.Absolute(entryPath.Segments.Take(myPath.Segments.Count + 1));
					if (returnedPaths.Add(dirPath))
					{
						yield return new ZipDirectory(this, dirPath.LastSegment, this.archive);
					}
				}
			}
		}

		private IEnumerable<ZipArchiveEntry> entries
		{
			get
			{
				Path myPath = this.Path;
				foreach (ZipArchiveEntry entry in this.archive.Entries)
				{
					Path entryPath = Path.Absolute(Path.Parse(entry.FullName, zipPathOptions).Segments);

					if (entryPath.Up().Equals(myPath))
						yield return entry;
				}
			}
		}

		public override IEnumerable<File> Files => this.entries.Select(e => new ZipFile(this, e));

		#endregion

		#region Constructors

		public ZipDirectory(Directory directory, string name, ZipArchive archive)
			: base(directory, name)
		{
			this.archive = archive;
		}

		#endregion

		#region Methods

		public override void Delete(bool recursive)
		{
			foreach (ZipArchiveEntry entry in this.entries)
			{
				if(!recursive)
					throw new IOException("Directory is not empty.");

				entry.Delete();
			}
		}

		public override Task DeleteAsync(bool recursive)
		{
			this.Delete(recursive);
			return Task.CompletedTask;
		}

		public override void Create()
		{
			
		}

		public override Task CreateAsync()
		{
			return Task.CompletedTask;
		}

		protected override Directory GetDirectoryInternal(string name)
		{
			return new ZipDirectory(this, name, this.archive);
		}

		protected override File GetFileInternal(string name)
		{
			ZipArchiveEntry entry = this.archive.GetEntry(Path.Relative(this.Path.Segments).Combine(name).ToString());

			if(entry != null)
				return new ZipFile(this, entry);

			return new ZipFile(this, name, this.archive);
		}
		
		#endregion
	}
}
