﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents a system directory.
	/// </summary>
	public class SystemDirectory : Directory
	{
		#region Fields


		#endregion

		#region Properties

		/// <summary>
		/// Gets the real name of the directory.
		/// </summary>
		public string RealName { get; }

		/// <inheritdoc />
		public override IEnumerable<Directory> Directories
		{
			get
			{
				DirectoryInfo info = new DirectoryInfo(this.RealName);
				foreach (DirectoryInfo dir in info.GetDirectories())
				{
					yield return new SystemDirectory(this, dir.Name, dir.FullName);
				}
			}
		}

		/// <inheritdoc />
		public override IEnumerable<File> Files
		{
			get
			{
				DirectoryInfo info = new DirectoryInfo(this.RealName);
				foreach (FileInfo file in info.GetFiles())
				{
					yield return new SystemFile(this, file.Name, file.FullName);
				}
			}
		}

		/// <inheritdoc />
		public override bool Exists => System.IO.Directory.Exists(this.RealName);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SystemDirectory.
		/// </summary>
		public SystemDirectory(Directory directory, string name, string realName)
			: base(directory, name)
		{
			DirectoryInfo info = new DirectoryInfo(realName);
			this.RealName = info.FullName;
			//this.Exists = info.Exists;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the specified real directory without a parent directory.
		/// It will appear as if the directory is a root directory.
		/// </summary>
		/// <param name="realName"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public static SystemDirectory AsRoot(string realName, string name = "")
		{
			return new SystemDirectory(null, name, realName);
		}

		public override void Create()
		{
			System.IO.Directory.CreateDirectory(this.RealName);
		}

		public override Task CreateAsync()
		{
			return Task.Run(() => System.IO.Directory.CreateDirectory(this.RealName));
		}

		public override void Delete(bool recursive)
		{
			System.IO.Directory.Delete(this.RealName, recursive);
		}

		public override Task DeleteAsync(bool recursive)
		{
			return Task.Run(() => System.IO.Directory.Delete(this.RealName, recursive));
		}

		protected override File GetFileInternal(string name)
		{
			if (!SystemUtil.IsValidFileName(name))
			{
				return new NonExistingFile(this, name);
			}

			FileInfo info = new FileInfo(System.IO.Path.Combine(this.RealName, name));
			return new SystemFile(this, info.Name, info.FullName);
		}

		protected override Directory GetDirectoryInternal(string name)
		{
			if (!SystemUtil.IsValidFileName(name))
			{
				return new NonExistingDirectory(this, name);
			}

			DirectoryInfo info = new DirectoryInfo(System.IO.Path.Combine(this.RealName, name));
			return new SystemDirectory(this, info.Name, info.FullName);
		}


		#endregion
	}
}
