﻿using System;
using System.IO;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents a file that throws <see cref="NotSupportedException"/> when trying to access it.
	/// </summary>
	internal sealed class NonExistingFile : File
	{
		#region Fields

		#endregion

		#region Properties

		public override bool Exists => false;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="NonExistingFile"/> class. 
		/// </summary>
		public NonExistingFile(Directory directory, string name) 
			: base(directory, name)
		{
		
		}
		
		#endregion
		
		#region Methods
		
		public override Stream Open(FileMode mode, FileAccess access, FileShare share = FileShare.Read)
		{
			throw new NotSupportedException($"{this.GetType().Name}: Opening is not supported.");
		}

		#endregion
	}
}
