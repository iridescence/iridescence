﻿using System.IO;
using System.Xml;
using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Exports an <see cref="XmlTextReader"/> from a <see cref="TextReader"/>
	/// </summary>
	public class XmlTextReaderExportProvider : TransformProvider<TextReader, XmlTextReader>
	{	
		#region Properties

		protected override string InputName => "Reader";

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="XmlTextReaderExportProvider"/> class. 
		/// </summary>
		public XmlTextReaderExportProvider()
		{
		
		}
		
		#endregion
		
		#region Methods

		public override XmlTextReader Transform(TextReader input, CompositionContext context)
		{
			return new XmlTextReader(input);
		}

		#endregion
	}
}
