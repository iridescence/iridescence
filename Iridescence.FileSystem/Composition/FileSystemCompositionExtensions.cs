﻿using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// File system composition extension methods.
	/// </summary>
	public static class FileSystemCompositionExtensions
	{
		/// <summary>
		/// Adds file system composition support based on the specified root directory.
		/// </summary>
		/// <param name="builder"></param>
		/// <param name="rootDirectory"></param>
		/// <returns></returns>
		public static CompositionContainerBuilder WithFileSystem(this CompositionContainerBuilder builder, Directory rootDirectory)
		{
			return builder.WithProvider(new FileSystemExportDescriptorProvider(rootDirectory));
		}

		/// <summary>
		/// Returns the path of a <see cref="CompositionContract"/>.
		/// </summary>
		/// <param name="contract"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		public static bool TryGetPath(this CompositionContract contract, out Path path)
		{
			path = Path.Empty;
			if (!contract.TryGetName(out string name))
				return false;

			return Path.TryParse(name, out path);
		}

		/// <summary>
		/// Returns the path of a <see cref="CompositionContract"/>.
		/// </summary>
		/// <param name="contract"></param>
		/// <param name="path"></param>
		/// <param name="remainingContract"></param>
		/// <returns></returns>
		public static bool TryExtractPath(this CompositionContract contract, out Path path, out CompositionContract remainingContract)
		{
			path = Path.Empty;
			if (!contract.TryExtractName(out string name, out remainingContract))
				return false;

			return Path.TryParse(name, out path);
		}
	}
}
