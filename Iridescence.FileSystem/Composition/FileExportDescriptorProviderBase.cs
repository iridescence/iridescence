﻿using System;
using System.Collections.Generic;
using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Represents an abstract file-based export provider which loads its instances from a file.
	/// </summary>
	public abstract class FileExportDescriptorProviderBase : IExportDescriptorProvider
	{
		#region Properties

		protected abstract Type PartType { get; }

		protected virtual FileExportSharingMode SharingMode => FileExportSharingMode.None;

		#endregion
		
		#region Methods

		protected abstract bool IsSupportedName(string name);

		protected abstract object Load(File file);

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			if (!contract.TryExtractName(out string name, out CompositionContract contractWithoutName) || name == null || !this.IsSupportedName(name))
				yield break;

			if (!contractWithoutName.Type.IsAssignableFrom(this.PartType))
				yield break;

			foreach (Dependency dep in dependencyAccessor.ResolveDependencies("File", contract.ChangeType(typeof(File))))
			{
				yield return new ExportDescriptorPromise(
					contract,
					$"{this.GetType().Name} for {name}",
					this.SharingMode != FileExportSharingMode.None,
					() => EnumerableExtensions.Once(dep),
					dependencies =>
					{
						ExportDescriptorPromise filePromise = dependencies.GetDependency(dep.Site);
						WeakReference<object> weakRef = null;
						bool hardRefSet = false;
						object hardRef = null;

						return ExportDescriptor.Create((ctx, op) =>
						{
							if (this.SharingMode == FileExportSharingMode.WeakReference && weakRef != null && weakRef.TryGetTarget(out object weakRefTarget))
							{
								return weakRefTarget;
							}

							if (this.SharingMode == FileExportSharingMode.HardReference && hardRefSet)
							{
								return hardRef;
							}

							File file = (File)filePromise.GetDescriptor().Activator(ctx, op);
							object result = this.Load(file);

							if (this.SharingMode == FileExportSharingMode.WeakReference)
							{
								weakRef = new WeakReference<object>(result);
							}
							else if (this.SharingMode == FileExportSharingMode.HardReference)
							{
								hardRef = result;
								hardRefSet = true;
							}

							return result;
						});
					});
			}
		}

		#endregion
	}

	/// <summary>
	/// Defines how an export retreived from a file should be shared.
	/// </summary>
	public enum FileExportSharingMode
	{
		/// <summary>
		/// No sharing. The file is loaded every time the <see cref="CompositeActivator"/> is invoked.
		/// </summary>
		None,

		/// <summary>
		/// The object returned from the Load method is stored using a weak reference. The Load method is not invoked again for as long as the weak reference target can be reteived.
		/// </summary>
		WeakReference,

		/// <summary>
		/// The file is loaded once and the result is stored using a hard reference. It will not be loaded again.
		/// </summary>
		HardReference
	}
}
