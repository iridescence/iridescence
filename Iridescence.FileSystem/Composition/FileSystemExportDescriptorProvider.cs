﻿using System.Collections.Generic;
using System.Linq;
using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Exports <see cref="File">files</see> and <see cref="Directory">directories</see>.
	/// </summary>
	public class FileSystemExportDescriptorProvider : IExportDescriptorProvider
	{
		#region Fields

		private readonly Directory root;
		
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="FileSystemExportDescriptorProvider"/> class. 
		/// </summary>
		public FileSystemExportDescriptorProvider(Directory root)
		{
			this.root = root;
		}
		
		#endregion
		
		#region Methods

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			bool isFile = contract.IsAssignableTo(typeof(File));
			bool isDirectory = contract.IsAssignableTo(typeof(Directory));

			if (!isFile && !isDirectory)
				yield break;

			// Get name.
			if (!contract.TryExtractName(out string name, out CompositionContract unwrapped) || name == null)
				yield break;
			
			// Parse NameMetadata into Path.
			if (!Path.TryParse(name, out Path filePath))
				yield break;

			// Check whether non-existing files/dirs are allowed to be returned here.
			bool allowNonExisting = unwrapped.ExtractMetadata<FileSystemExistsMetadata>(out unwrapped).LastOrDefault()?.AllowNonExisting ?? false;

			// Get the object.
			FileSystemObject obj;
			if (isFile)
			{
				obj = this.root.GetFile(filePath);
			}
			else
			{
				obj = this.root.GetDirectory(filePath);
			}
			
			if (!obj.Exists && !allowNonExisting)
				yield break;

			yield return new ExportDescriptorPromise(
				contract,
				$"{(obj is Directory ? "directory" : "file")} {filePath}",
				false,
				Dependency.NoDependencies,
				_ =>
				{
					return ExportDescriptor.Create((context, operation) => obj, unwrapped.Metadata);
				});
		}

		#endregion
	}
}
