﻿using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Composition extensions that provides working directory/relative name functionality.
	/// </summary>
	public static class RelativeNameExtensions
	{
		/// <summary>
		/// Returns a <see cref="CompositionContext"/> with the specified working directory.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="newDirectory"></param>
		/// <returns></returns>
		public static CompositionContext ChangeDirectory(this CompositionContext context, Path newDirectory)
		{
			if (context is CompositionContextWithWorkingDirectory cwd)
			{
				newDirectory = Path.Combine(cwd.WorkingDirectory, newDirectory);
				context = cwd.BaseContext;
			}

			return new CompositionContextWithWorkingDirectory(context, newDirectory);
		}

		/// <summary>
		/// Returns a <see cref="CompositionContext"/> with the specified working directory.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="newDirectory"></param>
		/// <returns></returns>
		public static CompositionContext ChangeDirectory(this CompositionContext context, string newDirectory)
		{
			return context.ChangeDirectory(Path.Parse(newDirectory));
		}

		/// <summary>
		/// Returns a <see cref="CompositionContext"/> with a working directory relative to the specified file name.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static CompositionContext RelativeToFile(this CompositionContext context, Path fileName)
		{
			Path directory = fileName;
			if (!fileName.IsAbsolute || fileName.Segments.Count > 0)
				directory = fileName.Up();

			return context.ChangeDirectory(directory);
		}

		/// <summary>
		/// Returns a <see cref="CompositionContext"/> with a working directory relative to the specified file name.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static CompositionContext RelativeToFile(this CompositionContext context, string fileName)
		{
			return context.RelativeToFile(Path.Parse(fileName));
		}

		/// <summary>
		/// Returns a <see cref="CompositionContext"/> with a working directory relative to the file name specified in a contract.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="contract"></param>
		/// <returns></returns>
		public static CompositionContext RelativeToFile(this CompositionContext context, CompositionContract contract)
		{
			if (!contract.TryGetPath(out Path fileName))
				return context;

			return context.RelativeToFile(fileName);
		}
	}
}