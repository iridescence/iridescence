﻿using System.Collections.Generic;
using System.IO;
using Iridescence.Composition;
using System.Json;
using System.Linq;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Exports JSON values from <see cref="TextReader"/> instances.
	/// </summary>
	public class JsonExportProvider : IExportDescriptorProvider
	{
		#region Methods

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			if (contract.Type != typeof(JsonValue) &&
			    contract.Type != typeof(JsonObject) &&
			    contract.Type != typeof(JsonArray) &&
			    contract.Type != typeof(JsonPrimitive))
				yield break;

			CompositionContract inputContract = contract.ChangeType(typeof(TextReader));

			foreach (Dependency dep in dependencyAccessor.ResolveDependencies("Reader", inputContract))
			{
				yield return new ExportDescriptorPromise(
					contract,
					this.ToString(),
					true,
					() => EnumerableExtensions.Once(dep),
					deps =>
					{
						ExportDescriptor fileDescriptor = deps.First().Promise.GetDescriptor();

						return ExportDescriptor.Create((context, operation) =>
						{
							using (TextReader reader = (TextReader)CompositionOperation.Run(context, fileDescriptor.Activator))
							{
								JsonValue value = JsonValue.Load(reader);

								if (contract.Type == typeof(JsonValue))
									return value;

								if (contract.Type == typeof(JsonObject) && value is JsonObject obj)
									return obj;

								if (contract.Type == typeof(JsonArray) && value is JsonArray arr)
									return arr;

								if (contract.Type == typeof(JsonPrimitive) && value is JsonPrimitive prim)
									return prim;

								throw new InvalidDataException($"A {contract.Type} was expected, but the parser returned a {value?.GetType()}.");
							}
						}, contract.Metadata);
					});
			}
		}

		#endregion
	}
}
