﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Implements an <see cref="ImportAttribute"/> that supports relative path logic.
	/// </summary>
	public class RelativeImportAttribute : ImportAttribute
	{
		public RelativeImportAttribute(string name)
			: base(name)
		{

		}

		private static bool tryGetPath(ImmutableArray<CompositionMetadata> metadata, out Path path)
		{
			path = Path.Empty;

			NameMetadata meta = metadata.OfType<NameMetadata>().LastOrDefault();
			if (meta == null)
				return false;
			
			return Path.TryParse(meta.Name, out path);
		}

		public override IEnumerable<ImportInfo> GetImports(ICustomAttributeProvider target, Lazy<ExportInfo> export)
		{
			if (this.Name == null || 
			    !Path.TryParse(this.Name, out Path myPath) ||
			    !tryGetPath(export.Value.Metadata, out Path exportPath))
				return base.GetImports(target, export);

			Path absolute = Path.Combine(exportPath.Up(), myPath);
			return EnumerableExtensions.Once(target.GetImportInfo(absolute.ToString(), this.IsOptional));
		}
	}
}
