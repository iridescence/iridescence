﻿using System;
using System.Collections.Generic;
using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Wraps a <see cref="CompositionContext"/> but adds a working directory property.
	/// </summary>
	internal sealed class CompositionContextWithWorkingDirectory : CompositionContext
	{	
		#region Properties

		public CompositionContext BaseContext { get; }

		public Path WorkingDirectory { get; }
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContextWithWorkingDirectory"/> class. 
		/// </summary>
		public CompositionContextWithWorkingDirectory(CompositionContext baseContext, Path workingDirectory)
		{
			this.BaseContext = baseContext;
			this.WorkingDirectory = workingDirectory;
		}
		
		#endregion
		
		#region Methods

		public override void AddDisposable(IDisposable obj)
		{
			this.BaseContext.AddDisposable(obj);
		}

		public override IEnumerable<ExportDescriptor> GetExportDescriptors(CompositionContract contract)
		{
			if (contract.TryExtractPath(out Path path, out CompositionContract namelessContract))
			{
				path = Path.Combine(this.WorkingDirectory, path);
				contract = namelessContract.WithMetadata(new NameMetadata(path.ToString()));
			}

			return this.BaseContext.GetExportDescriptors(contract);
		}

		public override string ToString()
		{
			return this.WorkingDirectory.ToString();
		}

		#endregion
	}
}
