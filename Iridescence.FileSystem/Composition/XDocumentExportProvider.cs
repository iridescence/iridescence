﻿using System.Xml;
using System.Xml.Linq;
using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Exports <see cref="XDocument"/> instances from an <see cref="XmlReader"/>.
	/// </summary>
	public class XDocumentExportProvider : TransformProvider<XmlReader, XDocument>
	{	
		#region Properties

		protected override string InputName => "Reader";

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="XDocumentExportProvider"/> class. 
		/// </summary>
		public XDocumentExportProvider()
		{
		
		}

		#endregion

		#region Methods	

		public override XDocument Transform(XmlReader input, CompositionContext context)
		{
			using (input)
			{
				return XDocument.Load(input);
			}
		}

		#endregion
	}
}
