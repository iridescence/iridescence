﻿using System.IO;
using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Exports <see cref="Stream"/> instances from files.
	/// </summary>
	public class StreamExportProvider : TransformProvider<File, Stream>
	{
		#region Fields
		
		#endregion
		
		#region Properties

		protected override string InputName => "File";

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="StreamExportProvider"/> class. 
		/// </summary>
		public StreamExportProvider()
		{
		
		}
		
		#endregion
		
		#region Methods

		public override Stream Transform(File input, CompositionContext context)
		{
			return input.Open(FileMode.Open, FileAccess.Read);
		}

		#endregion
	}
}
