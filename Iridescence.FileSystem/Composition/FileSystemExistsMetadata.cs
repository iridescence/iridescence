﻿using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Composition metadata that specifies whether a file or directory may be non-existing to fulfill the contract.
	/// The default is false (if this metadata is not declared).
	/// </summary>
	public class FileSystemExistsMetadata : CompositionMetadata
	{
		#region Properties
		
		public bool AllowNonExisting { get; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="FileSystemExistsMetadata"/> class. 
		/// </summary>
		public FileSystemExistsMetadata(bool allowNonExisting)
		{
			this.AllowNonExisting = allowNonExisting;
		}
		
		#endregion
	}
}
