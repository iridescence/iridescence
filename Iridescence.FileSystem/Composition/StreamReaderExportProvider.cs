﻿using System;
using System.IO;
using System.Text;
using Iridescence.Composition;

namespace Iridescence.FileSystem.Composition
{
	/// <summary>
	/// Exports <see cref="StreamReader"/> instances from a <see cref="Stream"/>.
	/// </summary>
	public class StreamReaderExportProvider : TransformProvider<Stream, StreamReader>
	{
		#region Fields
		
		#endregion
		
		#region Properties

		protected override string InputName => "Stream";

		public Encoding Encoding { get; }

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamReaderExportProvider"/> class. 
		/// </summary>
		public StreamReaderExportProvider()
			: this(Encoding.UTF8)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamReaderExportProvider"/> class. 
		/// </summary>
		public StreamReaderExportProvider(Encoding encoding)
		{
			this.Encoding = encoding ?? throw new ArgumentNullException(nameof(encoding));
		}
		
		#endregion
		
		#region Methods
		
		public override StreamReader Transform(Stream input, CompositionContext context)
		{
			return new StreamReader(input, this.Encoding);
		}
		
		#endregion
	}
}
