﻿using System.IO;
using System.Threading.Tasks;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents an abstract file.
	/// </summary>
	public abstract class File : FileSystemObject
	{
		#region Properties
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="File"/> class. 
		/// </summary>
		protected File(Directory directory, string name)
			: base(directory, name)
		{
			
		}

		#endregion
		
		#region Methods

		/// <summary>
		/// Opens the file.
		/// </summary>
		/// <param name="mode"></param>
		/// <param name="access"></param>
		/// <param name="share"></param>
		/// <returns></returns>
		public abstract Stream Open(FileMode mode, FileAccess access, FileShare share = FileShare.Read);

		protected override void CopyInternal(string newName, FileSystemObjectCopyFlags flags, Directory destination)
		{
			File destFile = destination.GetFile(newName);

			using (Stream source = this.Open(FileMode.Open, FileAccess.Read))
			{
				FileMode openMode;

				if ((flags & FileSystemObjectCopyFlags.Overwrite) != 0)
					openMode = FileMode.Create;
				else
					openMode = FileMode.CreateNew;

				using (Stream dest = destFile.Open(openMode, FileAccess.Write))
				{
					source.CopyTo(dest);
				}
			}

			if ((flags & FileSystemObjectCopyFlags.DeleteSource) != 0)
			{
				this.Delete();
			}
		}

		protected override async Task CopyInternalAsync(string newName, FileSystemObjectCopyFlags flags, Directory destination)
		{
			File destFile = destination.GetFile(newName);

			using (Stream source = this.Open(FileMode.Open, FileAccess.Read))
			{
				FileMode openMode;

				if ((flags & FileSystemObjectCopyFlags.Overwrite) != 0)
					openMode = FileMode.Create;
				else
					openMode = FileMode.CreateNew;

				using (Stream dest = destFile.Open(openMode, FileAccess.Write))
				{
					await source.CopyToAsync(dest);
				}
			}

			if ((flags & FileSystemObjectCopyFlags.DeleteSource) != 0)
			{
				await this.DeleteAsync();
			}
		}

		#endregion
	}
}
