﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.FileSystem
{
	/// <summary>
	/// Represents a file system with mount table and root directory.
	/// </summary>
	public class FileSystem : IReadOnlyDictionary<Path, Directory>
	{
		#region Events

		/// <summary>
		/// Occurs when a directory is mounted.
		/// </summary>
		public event EventHandler<MountEventArgs> Mounted;

		/// <summary>
		/// Occurs when a directory is unmounted.
		/// </summary>
		public event EventHandler<MountEventArgs> Unmounted;

		#endregion

		#region Fields

		private readonly Dictionary<Path, Directory> directories;
		private readonly MountedDirectory root;

		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the root directory of the file system that is created by this mount table.
		/// </summary>
		public Directory Root => this.root;

		Directory IReadOnlyDictionary<Path, Directory>.this[Path key] => this.directories[key];

		int IReadOnlyCollection<KeyValuePair<Path, Directory>>.Count => this.directories.Count;

		IEnumerable<Path> IReadOnlyDictionary<Path, Directory>.Keys => ((IReadOnlyDictionary<Path, Directory>)this.directories).Keys;

		IEnumerable<Directory> IReadOnlyDictionary<Path, Directory>.Values => ((IReadOnlyDictionary<Path, Directory>)this.directories).Values;

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="FileSystem"/> class. 
		/// </summary>
		public FileSystem()
		{
			this.directories = new Dictionary<Path, Directory>();
			this.root = new MountedDirectory(null, this, "");
		}
		
		#endregion
		
		#region Methods
		
		/// <summary>
		/// Mounts the specified directory at an absolute path.
		/// </summary>
		/// <param name="path">The path to mount the directory at. Must be absolute.</param>
		/// <param name="directory">The directory to mount.</param>
		public void Mount(Path path, Directory directory)
		{
			if (!path.IsAbsolute)
				throw new ArgumentException("Path must be absolute.", nameof(path));

			if (this.directories.ContainsKey(path))
				throw new InvalidOperationException("There is already a directory mounted at \"{path}\".");

			this.directories.Add(path, directory);
			this.Mounted?.Invoke(this, new MountEventArgs(path, directory));
		}

		/// <summary>
		/// Unmounts the directory mounted at the specified path.
		/// </summary>
		/// <param name="path">The path to unmount. Must be absolute.</param>
		/// <returns>True, if a directory was unmounted. False if there was no directory mounted at the specified path.</returns>
		public bool Unmount(Path path)
		{
			if (!path.IsAbsolute)
				throw new ArgumentException("Path must be absolute.", nameof(path));

			if (!this.directories.TryGetValue(path, out Directory dir))
				return false;

			this.directories.Remove(path);
			this.Unmounted?.Invoke(this, new MountEventArgs(path, dir));

			return true;
		}

		/// <summary>
		/// Returns whether the specified path is mounted in the file system. Only returns true for mount points, not any directory that exists.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool ContainsKey(Path key)
		{
			return this.directories.ContainsKey(key);
		}

		/// <summary>
		/// Returns the directory mounted at the specified path.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool TryGetValue(Path key, out Directory value)
		{
			return this.directories.TryGetValue(key, out value);
		}

		internal IReadOnlyDictionary<string, Directory> GetMountedDirectories(Path basePath)
		{
			Dictionary<string, Directory> dict = new Dictionary<string, Directory>();
			foreach (KeyValuePair<Path, Directory> mnt in this.directories)
			{
				Path mntPath = mnt.Key;
				if (mntPath.Segments.Count != basePath.Segments.Count + 1 || !mntPath.StartsWith(basePath))
					continue;

				dict.Add(mntPath.Segments[mntPath.Segments.Count - 1], mnt.Value);
			}
			return dict;
		}
		
		internal Directory GetMountBase(Path path, out Path relativePath)
		{
			if (!path.IsAbsolute)
				throw new ArgumentException("Path must be absolute.", nameof(path));

			int longestMatch = -1;
			Directory mntDir = null;

			foreach (KeyValuePair<Path, Directory> mnt in this.directories)
			{
				Path mntPath = mnt.Key;
				if (!mntPath.StartsWith(path))
					continue;
				
				if (mntPath.Segments.Count > longestMatch)
				{
					longestMatch = mntPath.Segments.Count;
					mntDir = mnt.Value;
				}
			}

			if (mntDir == null)
			{
				relativePath = Path.Empty;
				return null;
			}

			relativePath = Path.Relative(path.Segments.Skip(longestMatch));
			return mntDir;
		}

		public IEnumerator<KeyValuePair<Path, Directory>> GetEnumerator()
		{
			return this.directories.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)this.directories).GetEnumerator();
		}
		
		#endregion
	}
}
