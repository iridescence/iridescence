﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;

namespace Iridescence.Math
{
	public partial struct Vector2I : IEquatable<Vector2I>
	{
		[System.Diagnostics.Contracts.Pure]
		public override bool Equals(object obj)
		{
			if (!(obj is Vector2I)) return false;
			Vector2I other = (Vector2I)obj;
			return this.X == other.X
			    && this.Y == other.Y;
		}
		
		[System.Diagnostics.Contracts.Pure]
		bool IEquatable<Vector2I>.Equals(Vector2I other)
		{
			return this.X == other.X
			    && this.Y == other.Y;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public bool Equals(in Vector2I value)
		{
			return this.X == value.X
			    && this.Y == value.Y;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool Equals(in Vector2I left, in Vector2I right)
		{
			return left.X == right.X
			    && left.Y == right.Y;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool operator ==(in Vector2I left, in Vector2I right)
		{
			return left.X == right.X
			    && left.Y == right.Y;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool operator !=(in Vector2I left, in Vector2I right)
		{
			return left.X != right.X
			    || left.Y != right.Y;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public bool ApproximatelyEquals(in Vector2I value, int tolerance)
		{
			return System.Math.Abs(this.X - value.X) < tolerance
			    && System.Math.Abs(this.Y - value.Y) < tolerance;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool ApproximatelyEquals(in Vector2I left, in Vector2I right, int tolerance)
		{
			return System.Math.Abs(left.X - right.X) < tolerance
			    && System.Math.Abs(left.Y - right.Y) < tolerance;
		}
		
	}
}
