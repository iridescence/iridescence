﻿using System;
using System.Collections.Generic;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a rectangle with integer position and size.
	/// </summary>
	[Serializable]
	public struct RectangleI : IEquatable<RectangleI>
	{
		#region Fields

		/// <summary>
		/// The X position of the rectangle.
		/// </summary>
		public int X;

		/// <summary>
		/// The Y position of the rectangle.
		/// </summary>
		public int Y;

		/// <summary>
		/// The width of the rectangle.
		/// </summary>
		public int Width;

		/// <summary>
		/// The height of the rectangle.
		/// </summary>
		public int Height;

		/// <summary>
		/// Gets a rectangle with all values set to 0.
		/// </summary>
		public static readonly RectangleI Zero = new RectangleI(0, 0, 0, 0);

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the position of the rectangle.
		/// </summary>
		public Vector2I Position
		{
			get
			{
				Vector2I v;
				v.X = this.X;
				v.Y = this.Y;
				return v;
			}
			set
			{
				this.X = value.X;
				this.Y = value.Y;
			}
		}

		/// <summary>
		/// Gets or sets the size of the rectangle.
		/// </summary>
		public Vector2I Size
		{
			get
			{
				Vector2I v;
				v.X = this.Width;
				v.Y = this.Height;
				return v;
			}
			set
			{
				this.Width = value.X;
				this.Height = value.Y;
			}
		}

		/// <summary>
		/// Gets the area of the rectangle.
		/// </summary>
		public int Area => this.Width * this.Height;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new rectangle.
		/// </summary>
		public RectangleI(int x, int y, int width, int height)
		{
			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;
		}

		/// <summary>
		/// Creates a new rectangle.
		/// </summary>
		public RectangleI(Vector2I position, Vector2I size)
		{
			this.X = position.X;
			this.Y = position.Y;
			this.Width = size.X;
			this.Height = size.Y;
		}

		#endregion

		#region Methods

		private static bool test(int x, int min, int max)
		{
			return x >= min && x < max;
		}

		/// <summary>
		/// Returns a value that indicates whether a point lies within the rectangle.
		/// </summary>
		public static bool IsPointInside(in RectangleI rect, int x, int y)
		{
			return test(x, rect.X, rect.X + rect.Width) && test(y, rect.Y, rect.Y + rect.Height);
		}

		/// <summary>
		/// Returns a value that indicates whether a point lies within the rectangle.
		/// </summary>
		public static bool IsPointInside(in RectangleI rect, in Vector2I v)
		{
			return test(v.X, rect.X, rect.X + rect.Width) && test(v.Y, rect.Y, rect.Y + rect.Height);
		}

		/// <summary>
		/// Returns a value that indicates whether the specified inner rectangle lies completely within the outer rectangle.
		/// </summary>
		public static bool IsRectangleInside(in RectangleI outer, in RectangleI inner)
		{
			return outer.X <= inner.X && outer.Y <= inner.Y && (outer.X + outer.Width) >= (inner.X + inner.Width) && (outer.Y + outer.Height) >= (inner.Y + inner.Height);
		}

		/// <summary>
		/// Returns a value that indicates whether the two rectangles intersect.
		/// </summary>
		public static bool IsIntersecting(in RectangleI a, in RectangleI b)
		{
			return (test(a.X, b.X, b.X + b.Width) || test(b.X, a.X, a.X + a.Width)) && (test(a.Y, b.Y, b.Y + b.Height) || test(b.Y, a.Y, a.Y + a.Height));
		}

		/// <summary>
		/// Returns the intersection rectangle of two rectangles.
		/// </summary>
		public static void Intersect(in RectangleI a, in RectangleI b, out RectangleI result)
		{
			int x1 = Utility.Max(a.X, b.X);
			int x2 = Utility.Min(a.X + a.Width, b.X + b.Width);
			int y1 = Utility.Max(a.Y, b.Y);
			int y2 = Utility.Min(a.Y + a.Height, b.Y + b.Height);

			if (x2 < x1 || y2 < y1)
			{
				result = default;
				return;
			}

			result.X = x1;
			result.Y = y1;
			result.Width = x2 - x1;
			result.Height = y2 - y1;
		}


		/// <summary>
		/// Returns the intersection rectangle of two rectangles.
		/// </summary>
		public static RectangleI Intersect(in RectangleI a, in RectangleI b)
		{
			RectangleI result;
			result.X = Utility.Max(a.X, b.X);
			result.Width = Utility.Min(a.X + a.Width, b.X + b.Width) - result.X;
			result.Y = Utility.Max(a.Y, b.Y);
			result.Height = Utility.Min(a.Y + a.Height, b.Y + b.Height) - result.Y;

			if (result.Width < 0 || result.Height < 0)
			{
				return default;
			}

			return result;
		}

		/// <summary>
		/// Returns the union of the two specified rectangles.
		/// </summary>
		public static void Union(in RectangleI a, in RectangleI b, out RectangleI result)
		{
			int x1 = Utility.Min(a.X, b.X);
			int x2 = Utility.Max(a.X + a.Width, b.X + b.Width);
			int y1 = Utility.Min(a.Y, b.Y);
			int y2 = Utility.Max(a.Y + a.Height, b.Y + b.Height);
			result.X = x1;
			result.Y = y1;
			result.Width = x2 - x1;
			result.Height = y2 - y1;
		}

		/// <summary>
		/// Returns the union of the two specified rectangles.
		/// </summary>
		public static RectangleI Union(in RectangleI a, in RectangleI b)
		{
			RectangleI result;
			result.X = Utility.Min(a.X, b.X);
			result.Width = Utility.Max(a.X + a.Width, b.X + b.Width) - result.X;
			result.Y = Utility.Min(a.Y, b.Y);
			result.Height = Utility.Max(a.Y + a.Height, b.Y + b.Height) - result.Y;
			return result;
		}
	
		/// <summary>
		/// Returns the smallest rectangle that contains the specified rectangle and the specified point.
		/// </summary>
		public static void Union(in RectangleI rect, in Vector2I p, out RectangleI result)
		{
			Vector2I min;
			min.X = rect.X;
			min.Y = rect.Y;
			
			Vector2I max;
			max.X = rect.X + rect.Width;
			max.Y = rect.Y + rect.Height;
			
			Vector2I.Min(min, p, out min);
			Vector2I.Max(max, p, out max);
			
			result.X = min.X;
			result.Y = min.Y;
			result.Width = max.X - min.X;
			result.Height = max.Y - min.Y;
		}
		
		/// <summary>
		/// Returns the smallest rectangle that contains the specified rectangle and the specified point.
		/// </summary>
		public static RectangleI Union(in RectangleI rect, in Vector2I p)
		{
			Vector2I min;
			min.X = rect.X;
			min.Y = rect.Y;
			
			Vector2I max;
			max.X = rect.X + rect.Width;
			max.Y = rect.Y + rect.Height;
			
			Vector2I.Min(min, p, out min);
			Vector2I.Max(max, p, out max);
			
			RectangleI result;
			result.X = min.X;
			result.Y = min.Y;
			result.Width = max.X - min.X;
			result.Height = max.Y - min.Y;
			return result;
		}

		/// <summary>
		/// Inflates the rectangle. This subtracts the offset from the position and adds the doubled offset to the width and height.
		/// </summary>
		public static void Inflate(in RectangleI rect, int dx, int dy, out RectangleI result)
		{
			result.X = rect.X - dx;
			result.Y = rect.Y - dy;
			result.Width = rect.Width + 2 * dx;
			result.Height = rect.Height + 2 * dy;
		}

		/// <summary>
		/// Inflates the rectangle. This subtracts the offset from the position and adds the doubled offset to the width and height.
		/// </summary>
		public static RectangleI Inflate(in RectangleI rect, int dx, int dy)
		{
			RectangleI result;
			result.X = rect.X - dx;
			result.Y = rect.Y - dy;
			result.Width = rect.Width + 2 * dx;
			result.Height = rect.Height + 2 * dy;
			return result;
		}
		
		/// <summary>
		/// Returns up to 4 border rectangles resulting from subtracting the smaller rectangle from the larger rectangle.
		/// </summary>
		/// <param name="larger"></param>
		/// <param name="smaller"></param>
		/// <returns></returns>
		public static IEnumerable<RectangleI> GetBorders(RectangleI larger, RectangleI smaller)
		{
			if (larger.X < smaller.X)
			{
				yield return new RectangleI(larger.X, smaller.Y, smaller.X - larger.X, smaller.Height);
			}

			if (larger.X + larger.Width > smaller.X + smaller.Width)
			{
				yield return new RectangleI(smaller.X + smaller.Width, smaller.Y, (larger.X + larger.Width) - (smaller.X + smaller.Width), smaller.Height);
			}

			if (larger.Y < smaller.Y)
			{
				yield return new RectangleI(larger.X, larger.Y, larger.Width, smaller.Y - larger.Y);
			}

			if (larger.Y + larger.Height > smaller.Y + smaller.Height)
			{
				yield return new RectangleI(larger.X, smaller.Y + smaller.Height, larger.Width, (larger.Y + larger.Height) - (smaller.Y + smaller.Height));
			}
		}

		/// <summary>
		/// Returns a value that indicates whether this rectangle and the specified rectangle are equal.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public bool Equals(in RectangleI other)
		{
			return this.X == other.X && this.Y == other.Y && this.Width == other.Width && this.Height == other.Height;
		}

		bool IEquatable<RectangleI>.Equals(RectangleI other) => this.Equals(other);

		/// <summary>
		/// Returns a value that indicates whether this rectangle and the specified object.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if (!(obj is RectangleI))
				return false;

			RectangleI other = (RectangleI)obj;
			return this.X == other.X && this.Y == other.Y && this.Width == other.Width && this.Height == other.Height;
		}

		/// <summary>
		/// Returns a value that indicates whether the two rectangles are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(in RectangleI left, in RectangleI right)
		{
			return left.X == right.X && left.Y == right.Y && left.Width == right.Width && left.Height == right.Height;
		}

		public override int GetHashCode()
		{
			int hashCode = this.X;
			hashCode = (hashCode * 397) ^ this.Y;
			hashCode = (hashCode * 397) ^ this.Width;
			hashCode = (hashCode * 397) ^ this.Height;
			return hashCode;
		}

		public override string ToString()
		{
			return $"{this.Position} {this.Size}";
		}

		/// <summary>
		/// Returns a value that indicates whether the two rectangles are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool operator ==(in RectangleI left, in RectangleI right)
		{
			return left.X == right.X && left.Y == right.Y && left.Width == right.Width && left.Height == right.Height;
		}
		
		/// <summary>
		/// Returns a value that indicates whether the two rectangles are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool operator !=(in RectangleI left, in RectangleI right)
		{
			return left.X != right.X || left.Y != right.Y || left.Width != right.Width || left.Height != right.Height;
		}

		public static explicit operator RectangleI(in Rectangle rect)
		{
			RectangleI result;
			result.X = (int)rect.X;
			result.Y = (int)rect.Y;
			result.Width = (int)rect.Width;
			result.Height = (int)rect.Height;
			return result;
		}

		public static explicit operator RectangleI(in RectangleD rect)
		{
			RectangleI result;
			result.X = (int)rect.X;
			result.Y = (int)rect.Y;
			result.Width = (int)rect.Width;
			result.Height = (int)rect.Height;
			return result;
		}

		#endregion
	}
}
