﻿namespace Iridescence.Math
{
	public partial struct Vector3I
	{
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static void Cross(ref Vector3I left, ref Vector3I right, out Vector3I result)
		{
			int x = left.Y * right.Z - left.Z * right.Y;
			int y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			result.X = x;
			result.Y = y;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static void Cross(Vector3I left, ref Vector3I right, out Vector3I result)
		{
			int x = left.Y * right.Z - left.Z * right.Y;
			int y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			result.X = x;
			result.Y = y;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static void Cross(ref Vector3I left, Vector3I right, out Vector3I result)
		{
			int x = left.Y * right.Z - left.Z * right.Y;
			int y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			result.X = x;
			result.Y = y;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static void Cross(Vector3I left, Vector3I right, out Vector3I result)
		{
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static Vector3I Cross(ref Vector3I left, ref Vector3I right)
		{
			Vector3I result;
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			return result;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static Vector3I Cross(Vector3I left, ref Vector3I right)
		{
			Vector3I result;
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			return result;
		}

		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static Vector3I Cross(ref Vector3I left, Vector3I right)
		{
			Vector3I result;
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			return result;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static Vector3I Cross(Vector3I left, Vector3I right)
		{
			Vector3I result;
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			return result;
		}

		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public Vector3I Cross(ref Vector3I vector)
		{
			Vector3I result;
			result.X = this.Y * vector.Z - this.Z * vector.Y;
			result.Y = this.Z * vector.X - this.X * vector.Z;
			result.Z = this.X * vector.Y - this.Y * vector.X;
			return result;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors. For performance reasons, it is recommended to use the methods with reference parameters.
		/// </summary>
		public Vector3I Cross(Vector3I vector)
		{
			Vector3I result;
			result.X = this.Y * vector.Z - this.Z * vector.Y;
			result.Y = this.Z * vector.X - this.X * vector.Z;
			result.Z = this.X * vector.Y - this.Y * vector.X;
			return result;
		}
	}
}
