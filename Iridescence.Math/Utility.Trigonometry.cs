﻿using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	public static partial class Utility
	{
		#region Sin

		/// <summary>
		/// Returns the sine of the value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The sine of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Sin(float x)
		{
			return (float)System.Math.Sin(x);
		}

		/// <summary>
		/// Returns the sine of the value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The sine of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Sin(double x)
		{
			return System.Math.Sin(x);
		}

		#endregion

		#region Asin

		/// <summary>
		/// Returns the inverse sine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse sine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Asin(float angle)
		{
			return (float)System.Math.Asin(angle);
		}

		/// <summary>
		/// Returns the inverse sine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse sine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Asin(double angle)
		{
			return System.Math.Asin(angle);
		}

		#endregion

		#region Cos

		/// <summary>
		/// Returns the cosine of the value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The cosine of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Cos(float x)
		{
			return (float)System.Math.Cos(x);
		}

		/// <summary>
		/// Returns the cosine of the value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The cosine of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Cos(double x)
		{
			return System.Math.Cos(x);
		}

		#endregion

		#region Acos

		/// <summary>
		/// Returns the inverse cosine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse cosine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Acos(float angle)
		{
			return (float)System.Math.Acos(angle);
		}

		/// <summary>
		/// Returns the inverse cosine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse cosine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Acos(double angle)
		{
			return System.Math.Acos(angle);
		}

		#endregion

		#region Tan

		/// <summary>
		/// Returns the tangent of the value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The tangent of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Tan(float x)
		{
			return (float)System.Math.Tan(x);
		}

		/// <summary>
		/// Returns the tangent of the value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The tangent of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Tan(double x)
		{
			return System.Math.Tan(x);
		}

		#endregion

		#region Atan

		/// <summary>
		/// Returns the inverse tangent of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The arc tangent of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Atan(float angle)
		{
			return (float)System.Math.Atan(angle);
		}

		/// <summary>
		/// Returns the inverse tangent of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The arc tangent of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Atan(double angle)
		{
			return System.Math.Atan(angle);
		}

		#endregion

		#region Atan2

		/// <summary>
		/// Returns the inverse tangent of the quotient of the two values.
		/// </summary>
		/// <param name="y">The first value.</param>
		/// <param name="x">The second value.</param>
		/// <returns>The inverse tangent of the two values.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Atan2(float y, float x)
		{
			return (float)System.Math.Atan2(y, x);
		}

		/// <summary>
		/// Returns the inverse tangent of the quotient of the two values.
		/// </summary>
		/// <param name="y">The first value.</param>
		/// <param name="x">The second value.</param>
		/// <returns>The inverse tangent of the two values.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Atan2(double y, double x)
		{
			return System.Math.Atan2(y, x);
		}

		#endregion

		#region Sinh

		/// <summary>
		/// Returns the hyperbolic sine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The hyperbolic sine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Sinh(float angle)
		{
			return (float)System.Math.Sinh(angle);
		}

		/// <summary>
		/// Returns the hyperbolic sine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The hyperbolic sine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Sinh(double angle)
		{
			return System.Math.Sinh(angle);
		}

		#endregion

		#region Asinh

		/// <summary>
		/// Returns the inverse hyperbolic sine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse hyperbolic sine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Asinh(float angle)
		{
			return Utility.Ln(angle + Utility.Sqrt(angle * angle + 1.0f));
		}

		/// <summary>
		/// Returns the inverse hyperbolic sine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse hyperbolic sine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Asinh(double angle)
		{
			return Utility.Ln(angle + Utility.Sqrt(angle * angle + 1.0d));
		}

		#endregion

		#region Cosh

		/// <summary>
		/// Returns the hyperbolic cosine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The hyperbolic cosine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Cosh(float angle)
		{
			return (float)System.Math.Cosh(angle);
		}

		/// <summary>
		/// Returns the hyperbolic cosine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The hyperbolic cosine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Cosh(double angle)
		{
			return System.Math.Cosh(angle);
		}

		#endregion

		#region Acosh

		/// <summary>
		/// Returns the inverse hyperbolic cosine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse hyperbolic cosine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Acosh(float angle)
		{
			return Utility.Ln(angle + Utility.Sqrt(angle * angle - 1.0f));
		}

		/// <summary>
		/// Returns the inverse hyperbolic cosine of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse hyperbolic cosine of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Acosh(double angle)
		{
			return Utility.Ln(angle + Utility.Sqrt(angle * angle - 1.0f));
		}

		#endregion

		#region Tanh

		/// <summary>
		/// Returns the hyperbolic tangent of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The hyperbolic tangent of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Tanh(float angle)
		{
			return (float)System.Math.Tanh(angle);
		}

		/// <summary>
		/// Returns the hyperbolic tangent of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The hyperbolic tangent of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Tanh(double angle)
		{
			return System.Math.Tanh(angle);
		}

		#endregion

		#region Atanh

		/// <summary>
		/// Returns the inverse hyperbolic tangent of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse hyperbolic tangent of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Atanh(float angle)
		{
			return 0.5f * Utility.Ln((1.0f + angle) / (1.0f - angle));
		}

		/// <summary>
		/// Returns the inverse hyperbolic tangent of the angle.
		/// </summary>
		/// <param name="angle">The angle in radians.</param>
		/// <returns>The inverse hyperbolic tangent of the angle.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Atanh(double angle)
		{
			return 0.5d * Utility.Ln((1.0d + angle) / (1.0d - angle));
		}

		#endregion

		#region Sinc

		/// <summary>
		/// Calculates the sinc function for the value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Sinc(float x)
		{
			float v = Constants.Pi * x;
			return Utility.Sin(v) / v;
		}

		/// <summary>
		/// Calculates the sinc function for the value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Sinc(double x)
		{
			double v = ConstantsD.Pi * x;
			return Utility.Sin(v) / v;
		}

		#endregion

		#region Lanczos

		/// <summary>
		/// Calculates the lanczos function for the value.
		/// </summary>
		/// <param name="a">The size.</param>
		/// <param name="x">The value.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Lanczos(float a, float x)
		{
			if (x == 0.0f)
				return 1.0f;
			if (x > -a && x < a)
				return Utility.Sinc(x) * Utility.Sinc(x / a);
			return 0.0f;
		}

		/// <summary>
		/// Calculates the lanczos function for the value.
		/// </summary>
		/// <param name="a">The size.</param>
		/// <param name="x">The value.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Lanczos(double a, double x)
		{
			if (x == 0.0d)
				return 1.0d;
			if (x > -a && x < a)
				return Utility.Sinc(x) * Utility.Sinc(x / a);
			return 0.0d;
		}

		#endregion

		#region Angle

		/// <summary>
		/// Given the triangle (a, b, c), returns the inner angle at point a.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		/// <returns></returns>
		public static float Angle(Vector2 a, Vector2 b, Vector2 c)
		{
			Vector2 d1, d2;
			Vector2.Subtract(b, a, out d1);
			Vector2.Subtract(c, a, out d2);

			float cross = d1.X * d2.Y - d1.Y * d2.X;
			float dot = d1.X * d2.X + d1.Y * d2.Y;

			return Utility.Atan2(cross, dot);
		}

		/// <summary>
		/// Given the triangle (a, b, c), returns the inner angle at point a.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		/// <returns></returns>
		public static double Angle(Vector2D a, Vector2D b, Vector2D c)
		{
			Vector2D d1, d2;
			Vector2D.Subtract(b, a, out d1);
			Vector2D.Subtract(c, a, out d2);

			double cross = d1.X * d2.Y - d1.Y * d2.X;
			double dot = d1.X * d2.X + d1.Y * d2.Y;

			return Utility.Atan2(cross, dot);
		}

		#endregion
	}
}
