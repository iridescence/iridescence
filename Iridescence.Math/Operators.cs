﻿using System;
using System.Linq.Expressions;

namespace Iridescence.Math
{
	/// <summary>
	/// Provides operator functions for generic types.
	/// </summary>
	/// <typeparam name="T">The type of the operands/result.</typeparam>
	public static class Operators<T>
	{
		private static Func<T, T, T> add;
		public static Func<T, T, T> Add => add ?? (add = Operators<T, T, T>.CompileBinaryOp(Expression.Add));

		private static Func<T, T, T> subtract;
		public static Func<T, T, T> Subtract => subtract ?? (subtract = Operators<T, T, T>.CompileBinaryOp(Expression.Subtract));

		private static Func<T, T, T> multiply;
		public static Func<T, T, T> Multiply => multiply ?? (multiply = Operators<T, T, T>.CompileBinaryOp(Expression.Multiply));

		private static Func<T, T, T> divide;
		public static Func<T, T, T> Divide => divide ?? (divide = Operators<T, T, T>.CompileBinaryOp(Expression.Divide));

		private static Func<T, T> negate;
		public static Func<T, T> Negate => negate ?? (negate = Operators<T, T>.CompileUnaryOp(Expression.Negate));
	}

	/// <summary>
	/// Provides unary operator functions for generic types.
	/// </summary>
	/// <typeparam name="T">The type of the operand.</typeparam>
	/// <typeparam name="TResult">The type of the result.</typeparam>
	public static class Operators<T, TResult>
	{
		#region Operators

		private static Func<T, TResult> negate;
		public static Func<T, TResult> Negate => negate ?? (negate = CompileUnaryOp(Expression.Negate));

		#endregion

		#region Methods

		internal static Func<T, TResult> CompileUnaryOp(Func<ParameterExpression, UnaryExpression> exprFunc)
		{
			ParameterExpression param = Expression.Parameter(typeof(T));
			UnaryExpression op = exprFunc(param);
			return Expression.Lambda<Func<T, TResult>>(op, param).Compile();
		}

		#endregion
	}

	/// <summary>
	/// Provides binary operator functions for generic types.
	/// </summary>
	/// <typeparam name="T1">The type of the left operand.</typeparam>
	/// <typeparam name="T2">The type of the right operand.</typeparam>
	/// <typeparam name="TResult">The type of the result.</typeparam>
	public static class Operators<T1, T2, TResult>
	{
		#region Operators

		private static Func<T1, T2, TResult> add;
		public static Func<T1, T2, TResult> Add => add ?? (add = CompileBinaryOp(Expression.Add));

		private static Func<T1, T2, TResult> subtract;
		public static Func<T1, T2, TResult> Subtract => subtract ?? (subtract = CompileBinaryOp(Expression.Subtract));

		private static Func<T1, T2, TResult> multiply;
		public static Func<T1, T2, TResult> Multiply => multiply ?? (multiply = CompileBinaryOp(Expression.Multiply));

		private static Func<T1, T2, TResult> divide;
		public static Func<T1, T2, TResult> Divide => divide ?? (divide = CompileBinaryOp(Expression.Divide));

		#endregion

		#region Methods

		internal static Func<T1, T2, TResult> CompileBinaryOp(Func<ParameterExpression, ParameterExpression, BinaryExpression> exprFunc)
		{
			ParameterExpression paramA = Expression.Parameter(typeof(T1));
			ParameterExpression paramB = Expression.Parameter(typeof(T2));
			BinaryExpression op = exprFunc(paramA, paramB);
			return Expression.Lambda<Func<T1, T2, TResult>>(op, paramA, paramB).Compile();
		}

		#endregion
	}
}
