﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence.Math
{
	/// <summary>
	/// Holds the solutions of an equation solver.
	/// </summary>
	public struct SolveSolutions : IReadOnlyList<double>
	{
		#region Fields

		private readonly double x0;
		private readonly double x1;
		private readonly double x2;
		private readonly int count;

		public static readonly SolveSolutions NoSolution = new SolveSolutions();

		#endregion

		#region Properties

		/// <summary>
		/// Gets the number of solutions.
		/// </summary>
		public int Count => this.count;

		/// <summary>
		/// Gets the specified solution.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public double this[int index]
		{
			get
			{
				if (index < 0 || index >= this.count)
					throw new ArgumentOutOfRangeException(nameof(index));

				switch (index)
				{
					case 0: return this.x0;
					case 1: return this.x1;
					case 2: return this.x2;
				}

				throw new ArgumentOutOfRangeException(nameof(index));
			}
		}
		
		#endregion

		#region Constructors

		public SolveSolutions(double x0)
		{
			this.x0 = x0;
			this.x1 = double.NaN;
			this.x2 = double.NaN;
			this.count = 1;
		}
		
		public SolveSolutions(double x0, double x1)
		{
			this.x0 = x0;
			this.x1 = x1;
			this.x2 = double.NaN;
			this.count = 2;
		}

		public SolveSolutions(double x0, double x1, double x2)
		{
			this.x0 = x0;
			this.x1 = x1;
			this.x2 = x2;
			this.count = 3;
		}

		#endregion

		#region Methods

		public IEnumerator<double> GetEnumerator()
		{
			if (this.count > 0)
			{
				yield return this.x0;
				if (this.count > 1)
				{
					yield return this.x1;
					if (this.count > 2)
					{
						yield return this.x2;
					}
				}
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}

	public static partial class Utility
	{
		private const double epsilon = 1e-14;

		/// <summary>
		/// Solves the quadratic equation ax² + bx + c = 0.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		/// <returns>The solutions.</returns>
		public static SolveSolutions SolveQuadratic(double a, double b, double c)
		{
			if (Utility.Absolute(a) < epsilon)
			{
				if (Utility.Absolute(b) < epsilon)
				{
					if (c == 0.0)
						return SolveSolutions.NoSolution; // TODO

					return SolveSolutions.NoSolution;
				}

				return new SolveSolutions(-c / b);
			}

			double dscr = b * b - 4.0 * a * c;

			if (dscr > 0.0)
			{
				dscr = Utility.Sqrt(dscr);

				if (b >= 0)
				{
					return new SolveSolutions(-b - dscr / (2.0 * a), (2.0 * c) / (-b - dscr));
				}

				return new SolveSolutions((2.0 * c) / (-b + dscr), (-b + dscr) / (2.0 * a));
			}

			if (dscr == 0.0)
			{
				return new SolveSolutions(-b / (2.0 * a));
			}

			return SolveSolutions.NoSolution;
		}

		private static SolveSolutions solveCubicNormed(double a, double b, double c)
		{
			double a2 = a * a;
			double q = (a2 - 3 * b) / 9;
			double r = (a * (2 * a2 - 9 * b) + 27 * c) / 54;
			double r2 = r * r;
			double q3 = q * q * q;
			double A, B;
			if (r2 < q3)
			{
				double t = Utility.Clamp(r / Utility.Sqrt(q3), -1.0, 1.0);

				t = Utility.Acos(t);
				a /= 3;
				q = -2 * Utility.Sqrt(q);

				return new SolveSolutions(
					q * Utility.Cos(t / 3) - a,
					q * Utility.Cos((t + 2 * ConstantsD.Pi) / 3) - a,
					q * Utility.Cos((t - 2 * ConstantsD.Pi) / 3) - a);
			}

			A = -Utility.Pow(Utility.Absolute(r) + Utility.Sqrt(r2 - q3), 1.0 / 3.0);

			if (r < 0.0)
				A = -A;

			B = A == 0.0 ? 0.0 : q / A;
			a /= 3;

			double s0 = (A + B) - a;
			double s1 = -0.5 * (A + B) - a;
			double s2 = 0.5 * Utility.Sqrt(3.0) * (A - B);

			if (Utility.Absolute(s2) < epsilon)
				return new SolveSolutions(s0, s1);

			return new SolveSolutions(s0);
		}

		/// <summary>
		/// Solves the cubic equation ax³ + bx² + cx + d = 0.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <param name="c"></param>
		/// <param name="d"></param>
		/// <returns>The solutions.</returns>
		public static SolveSolutions SolveCubic(double a, double b, double c, double d)
		{
			if (Utility.Absolute(a) < epsilon)
				return SolveQuadratic(b, c, d);

			return solveCubicNormed(b / a, c / a, d / a);
		}
	}
}
