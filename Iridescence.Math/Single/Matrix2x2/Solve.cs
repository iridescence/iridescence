﻿namespace Iridescence.Math
{
	public partial struct Matrix2x2
	{
		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix2x2 a, ref Vector2 b, out Vector2 x)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix2x2 a, Vector2 b, out Vector2 x)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix2x2 a, ref Vector2 b, out Vector2 x)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix2x2 a, Vector2 b, out Vector2 x)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2 Solve(ref Matrix2x2 a, ref Vector2 b)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			Vector2 x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2 Solve(ref Matrix2x2 a, Vector2 b)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			Vector2 x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}


		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2 Solve(Matrix2x2 a, ref Vector2 b)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			Vector2 x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2 Solve(Matrix2x2 a, Vector2 b)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			Vector2 x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}
	}
}
