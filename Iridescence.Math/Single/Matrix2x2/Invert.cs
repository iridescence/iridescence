﻿using System;

namespace Iridescence.Math
{
	public partial struct Matrix2x2
	{
		public Matrix2x2 Invert()
		{
			Matrix2x2.Invert(this, out Matrix2x2 result);
			return result;
		}

		public void Invert(out Matrix2x2 result)
		{
			Matrix2x2.Invert(this, out result);
		}

		public static void Invert(in Matrix2x2 matrix, out Matrix2x2 result)
		{
			float d = matrix.Determinant;
			if (d >= -float.Epsilon && d <= float.Epsilon)
				throw new InvalidOperationException("This matrix can't be inverted.");
			d = 1.0f / d;

			float m11 = matrix.M22 * d;
			float m12 = -matrix.M12 * d;

			float m21 = -matrix.M21 * d;
			float m22 = matrix.M11 * d;

			result.M11 = m11;
			result.M12 = m12;
			result.M21 = m21;
			result.M22 = m22;
		}
	}
}
