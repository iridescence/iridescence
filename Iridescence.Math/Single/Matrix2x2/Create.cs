﻿namespace Iridescence.Math
{
	public partial struct Matrix2x2
	{
		#region Rotation

		/// <summary>
		/// Creates a rotation matrix.
		/// </summary>
		/// <param name="angle"></param>
		/// <param name="matrix"></param>
		public static void CreateRotation(float angle, out Matrix2x2 matrix)
		{
			float cos = Utility.Cos(angle);
			float sin = Utility.Sin(angle);

			matrix.M11 = cos;
			matrix.M12 = -sin;
			matrix.M21 = sin;
			matrix.M22 = cos;
		}

		/// <summary>
		/// Creates a rotation matrix.
		/// </summary>
		/// <param name="angle"></param>
		public static Matrix2x2 CreateRotation(float angle)
		{
			float cos = Utility.Cos(angle);
			float sin = Utility.Sin(angle);

			Matrix2x2 matrix;
			matrix.M11 = cos;
			matrix.M12 = -sin;
			matrix.M21 = sin;
			matrix.M22 = cos;
			return matrix;
		}

		#endregion

		#region Scale

		/// <summary>
		/// Creates a scaling matrix.
		/// </summary>
		public static void CreateScale(float scale, out Matrix2x2 matrix)
		{
			matrix = Matrix2x2.Identity;
			matrix.M11 = scale;
			matrix.M22 = scale;
		}

		/// <summary>
		/// Creates a scaling matrix.
		/// </summary>
		public static Matrix2x2 CreateScale(float scale)
		{
			Matrix2x2 matrix = Matrix2x2.Identity;
			matrix.M11 = scale;
			matrix.M22 = scale;
			return matrix;
		}

		/// <summary>
		/// Creates a scaling matrix.
		/// </summary>
		public static void CreateScale(Vector2 scale, out Matrix2x2 matrix)
		{
			matrix = Matrix2x2.Identity;
			matrix.M11 = scale.X;
			matrix.M22 = scale.Y;
		}

		/// <summary>
		/// Creates a scaling matrix.
		/// </summary>
		public static Matrix2x2 CreateScale(Vector2 scale)
		{
			Matrix2x2 matrix = Matrix2x2.Identity;
			matrix.M11 = scale.X;
			matrix.M22 = scale.Y;
			return matrix;
		}

		#endregion
	}
}
