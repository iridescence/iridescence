﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Iridescence.Math
{
	public partial struct Vector4
	{
		[System.Diagnostics.Contracts.Pure]
		public Vector4 Normalize()
		{
			Vector4 result;
			float length = Utility.Sqrt(Vector4.Dot(this, this));
			float v = 1 / length;
			result.X = v * this.X;
			result.Y = v * this.Y;
			result.Z = v * this.Z;
			result.W = v * this.W;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public float Normalize(out Vector4 result)
		{
			float length = Utility.Sqrt(Vector4.Dot(this, this));
			float v = 1 / length;
			result.X = v * this.X;
			result.Y = v * this.Y;
			result.Z = v * this.Z;
			result.W = v * this.W;
			return length;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static float Normalize(in Vector4 value, out Vector4 result)
		{
			float length = Utility.Sqrt(Vector4.Dot(value, value));
			float v = 1 / length;
			result.X = v * value.X;
			result.Y = v * value.Y;
			result.Z = v * value.Z;
			result.W = v * value.W;
			return length;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static float Normalize(Vector4 value, out Vector4 result)
		{
			float length = Utility.Sqrt(Vector4.Dot(value, value));
			float v = 1 / length;
			result.X = v * value.X;
			result.Y = v * value.Y;
			result.Z = v * value.Z;
			result.W = v * value.W;
			return length;
		}
		
	}
}
