﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Iridescence.Math
{
	public partial struct Matrix4x2
	{
		[System.Diagnostics.Contracts.Pure]
		public static Matrix2x4 Transpose(in Matrix4x2 value)
		{
			Matrix2x4 result;
			result.M11 = value.M11;
			result.M21 = value.M12;
			result.M12 = value.M21;
			result.M22 = value.M22;
			result.M13 = value.M31;
			result.M23 = value.M32;
			result.M14 = value.M41;
			result.M24 = value.M42;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Transpose(in Matrix4x2 value, out Matrix2x4 result)
		{
			result.M11 = value.M11;
			result.M21 = value.M12;
			result.M12 = value.M21;
			result.M22 = value.M22;
			result.M13 = value.M31;
			result.M23 = value.M32;
			result.M14 = value.M41;
			result.M24 = value.M42;
		}
		
	}
}
