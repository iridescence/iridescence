﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Iridescence.Math
{
	public partial struct Matrix4x2
	{
		[System.Diagnostics.Contracts.Pure]
		public static Matrix4x2 Multiply(float left, in Matrix4x2 right)
		{
			Matrix4x2 result;
			result.M11 = left * right.M11;
			result.M12 = left * right.M12;
			result.M21 = left * right.M21;
			result.M22 = left * right.M22;
			result.M31 = left * right.M31;
			result.M32 = left * right.M32;
			result.M41 = left * right.M41;
			result.M42 = left * right.M42;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Multiply(float left, in Matrix4x2 right, out Matrix4x2 result)
		{
			result.M11 = left * right.M11;
			result.M12 = left * right.M12;
			result.M21 = left * right.M21;
			result.M22 = left * right.M22;
			result.M31 = left * right.M31;
			result.M32 = left * right.M32;
			result.M41 = left * right.M41;
			result.M42 = left * right.M42;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Matrix4x2 operator *(float left, in Matrix4x2 right)
		{
			Matrix4x2 result;
			result.M11 = left * right.M11;
			result.M12 = left * right.M12;
			result.M21 = left * right.M21;
			result.M22 = left * right.M22;
			result.M31 = left * right.M31;
			result.M32 = left * right.M32;
			result.M41 = left * right.M41;
			result.M42 = left * right.M42;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Matrix4x2 Multiply(in Matrix4x2 left, float right)
		{
			Matrix4x2 result;
			result.M11 = left.M11 * right;
			result.M12 = left.M12 * right;
			result.M21 = left.M21 * right;
			result.M22 = left.M22 * right;
			result.M31 = left.M31 * right;
			result.M32 = left.M32 * right;
			result.M41 = left.M41 * right;
			result.M42 = left.M42 * right;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Multiply(in Matrix4x2 left, float right, out Matrix4x2 result)
		{
			result.M11 = left.M11 * right;
			result.M12 = left.M12 * right;
			result.M21 = left.M21 * right;
			result.M22 = left.M22 * right;
			result.M31 = left.M31 * right;
			result.M32 = left.M32 * right;
			result.M41 = left.M41 * right;
			result.M42 = left.M42 * right;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Matrix4x2 operator *(in Matrix4x2 left, float right)
		{
			Matrix4x2 result;
			result.M11 = left.M11 * right;
			result.M12 = left.M12 * right;
			result.M21 = left.M21 * right;
			result.M22 = left.M22 * right;
			result.M31 = left.M31 * right;
			result.M32 = left.M32 * right;
			result.M41 = left.M41 * right;
			result.M42 = left.M42 * right;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public Vector4 Multiply(in Vector2 value)
		{
			Vector4 result;
			result.X = this.M11 * value.X + this.M12 * value.Y;
			result.Y = this.M21 * value.X + this.M22 * value.Y;
			result.Z = this.M31 * value.X + this.M32 * value.Y;
			result.W = this.M41 * value.X + this.M42 * value.Y;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Vector4 Multiply(in Matrix4x2 left, in Vector2 right)
		{
			Vector4 result;
			result.X = left.M11 * right.X + left.M12 * right.Y;
			result.Y = left.M21 * right.X + left.M22 * right.Y;
			result.Z = left.M31 * right.X + left.M32 * right.Y;
			result.W = left.M41 * right.X + left.M42 * right.Y;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Multiply(in Matrix4x2 left, in Vector2 right, out Vector4 result)
		{
			Vector4 temp;
			temp.X = left.M11 * right.X + left.M12 * right.Y;
			temp.Y = left.M21 * right.X + left.M22 * right.Y;
			temp.Z = left.M31 * right.X + left.M32 * right.Y;
			temp.W = left.M41 * right.X + left.M42 * right.Y;
			result = temp;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Vector4 operator *(in Matrix4x2 left, in Vector2 right)
		{
			Vector4 result;
			result.X = left.M11 * right.X + left.M12 * right.Y;
			result.Y = left.M21 * right.X + left.M22 * right.Y;
			result.Z = left.M31 * right.X + left.M32 * right.Y;
			result.W = left.M41 * right.X + left.M42 * right.Y;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public Matrix4x2 Multiply(in Matrix2x2 value)
		{
			Matrix4x2 result;
			result.M11 = this.M11 * value.M11 + this.M12 * value.M21;
			result.M12 = this.M11 * value.M12 + this.M12 * value.M22;
			result.M21 = this.M21 * value.M11 + this.M22 * value.M21;
			result.M22 = this.M21 * value.M12 + this.M22 * value.M22;
			result.M31 = this.M31 * value.M11 + this.M32 * value.M21;
			result.M32 = this.M31 * value.M12 + this.M32 * value.M22;
			result.M41 = this.M41 * value.M11 + this.M42 * value.M21;
			result.M42 = this.M41 * value.M12 + this.M42 * value.M22;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Matrix4x2 Multiply(in Matrix4x2 left, in Matrix2x2 right)
		{
			Matrix4x2 result;
			result.M11 = left.M11 * right.M11 + left.M12 * right.M21;
			result.M12 = left.M11 * right.M12 + left.M12 * right.M22;
			result.M21 = left.M21 * right.M11 + left.M22 * right.M21;
			result.M22 = left.M21 * right.M12 + left.M22 * right.M22;
			result.M31 = left.M31 * right.M11 + left.M32 * right.M21;
			result.M32 = left.M31 * right.M12 + left.M32 * right.M22;
			result.M41 = left.M41 * right.M11 + left.M42 * right.M21;
			result.M42 = left.M41 * right.M12 + left.M42 * right.M22;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Multiply(in Matrix4x2 left, in Matrix2x2 right, out Matrix4x2 result)
		{
			Matrix4x2 temp;
			temp.M11 = left.M11 * right.M11 + left.M12 * right.M21;
			temp.M12 = left.M11 * right.M12 + left.M12 * right.M22;
			temp.M21 = left.M21 * right.M11 + left.M22 * right.M21;
			temp.M22 = left.M21 * right.M12 + left.M22 * right.M22;
			temp.M31 = left.M31 * right.M11 + left.M32 * right.M21;
			temp.M32 = left.M31 * right.M12 + left.M32 * right.M22;
			temp.M41 = left.M41 * right.M11 + left.M42 * right.M21;
			temp.M42 = left.M41 * right.M12 + left.M42 * right.M22;
			result = temp;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Matrix4x2 operator *(in Matrix4x2 left, in Matrix2x2 right)
		{
			Matrix4x2 result;
			result.M11 = left.M11 * right.M11 + left.M12 * right.M21;
			result.M12 = left.M11 * right.M12 + left.M12 * right.M22;
			result.M21 = left.M21 * right.M11 + left.M22 * right.M21;
			result.M22 = left.M21 * right.M12 + left.M22 * right.M22;
			result.M31 = left.M31 * right.M11 + left.M32 * right.M21;
			result.M32 = left.M31 * right.M12 + left.M32 * right.M22;
			result.M41 = left.M41 * right.M11 + left.M42 * right.M21;
			result.M42 = left.M41 * right.M12 + left.M42 * right.M22;
			return result;
		}
		
	}
}
