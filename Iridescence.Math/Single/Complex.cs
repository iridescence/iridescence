﻿using System;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a number in the complex plane.
	/// </summary>
	[Serializable]
	public struct Complex : IEquatable<Complex>
	{
		#region Fields

		#region Instance

		/// <summary>
		/// Real part of the complex number.
		/// </summary>
		public float Real;

		/// <summary>
		/// Imaginary part of the complex number
		/// </summary>
		public float Imaginary;

		#endregion

		#region Static

		/// <summary>
		/// Real number zero.
		/// </summary>
		public static readonly Complex Zero = new Complex(0.0f, 0.0f);

		/// <summary>
		/// Real number one.
		/// </summary>
		public static readonly Complex One = new Complex(1.0f, 0.0f);

		/// <summary>
		/// Imaginary number I.
		/// </summary>
		public static readonly Complex I = new Complex(0.0f, 1.0f);

		/// <summary>
		/// Real positive infinity.
		/// </summary>
		public static readonly Complex PositiveInfinity = new Complex(float.PositiveInfinity, 0.0f);

		/// <summary>
		/// Real negative infinity.
		/// </summary>
		public static readonly Complex NegativeInfinity = new Complex(float.NegativeInfinity, 0.0f);

		#endregion

		#endregion

		#region Properties

		#region Instance

		/// <summary>
		/// Gets the magnitude of the complex number.
		/// </summary>
		public float Magnitude => Utility.Sqrt(this.Real * this.Real + this.Imaginary + this.Imaginary);

		/// <summary>
		/// Gets the squared magnitude of the complex number.
		/// </summary>
		public float MagnitudeSquared => this.Real * this.Real + this.Imaginary * this.Imaginary;

		/// <summary>
		/// Gets the phase of the complex number.
		/// </summary>
		public float Phase => Utility.Atan2(this.Imaginary, this.Real);

		/// <summary>
		/// Gets the complex conjugate of the complex number.
		/// </summary>
		public Complex Conjugate
		{
			get
			{
				Complex c;
				c.Real = this.Real;
				c.Imaginary = -this.Imaginary;
				return c;
			}
		}

		#endregion

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new complex number.
		/// </summary>
		public Complex(float real)
		{
			this.Real = real;
			this.Imaginary = 0.0f;
		}

		/// <summary>
		/// Creates a new complex number.
		/// </summary>
		/// <param name="real">The real part of the complex number.</param>
		/// <param name="imaginary">The imaginary part of the complex number.</param>
		public Complex(float real, float imaginary)
		{
			this.Real = real;
			this.Imaginary = imaginary;
		}

		#endregion

		#region Methods

		#region Instance

		public bool Equals(Complex other)
		{
			return this.Real.Equals(other.Real) && 
			       this.Imaginary.Equals(other.Imaginary);
		}
		
		public override bool Equals(object obj)
		{
			if (!(obj is Complex other))
				return false;

			return this.Equals(other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (this.Real.GetHashCode() * 397) ^ this.Imaginary.GetHashCode();
			}
		}

		public override string ToString()
		{
			return $"{this.Real} + {this.Imaginary}i";
		}

		public Complex AddMult(float v, Complex c)
		{
			Complex result;
			result.Real = this.Real + v * c.Real;
			result.Imaginary = this.Imaginary + v * c.Imaginary;
			return result;
		}
		
		#endregion

		#region Static

		public static Complex Polar(float rho, float theta)
		{
			Complex c;
			c.Real = rho * Utility.Cos(theta);
			c.Imaginary = rho * Utility.Sin(theta);
			return c;
		}

		private static float MagnitudeAndScaleFactor(Complex a, out int exp)
		{
			exp = 0;

			if (float.IsInfinity(a.Real) || float.IsInfinity(a.Imaginary))
				return float.PositiveInfinity; // at least one component is INF

			if (float.IsNaN(a.Real))
				return (a.Real); //	real component is NaN

			if (float.IsNaN(a.Imaginary))
				return (a.Imaginary); // imaginary component is NaN

			// neither component is NaN or INF
			if (a.Real < 0.0f)
				a.Real = -a.Real;

			if (a.Imaginary < 0.0f)
				a.Imaginary = -a.Imaginary;

			if (a.Real < a.Imaginary)
			{
				// ensure that |a.Imaginary| <= |a.Real|
				float _Tmp1 = a.Real;
				a.Real = a.Imaginary;
				a.Imaginary = _Tmp1;
			}

			if (a.Real == 0.0f)
				return (a.Real); // |0| == 0
			
			if (1.0f <= a.Real)
			{
				exp = 2;
				a.Real = a.Real * 0.25f;
				a.Imaginary = a.Imaginary * 0.25f;
			}
			else
			{
				exp = -2;
				a.Real = a.Real * 4.0f;
				a.Imaginary = a.Imaginary * 4.0f;
			}

			float _Tmp = a.Real - a.Imaginary;
			
			if (_Tmp == a.Real)
				return (a.Real); // a.Imaginary unimportant

			if (a.Imaginary < _Tmp)
			{
				// use simple approximation
				float _Qv = a.Real / a.Imaginary;
				return (a.Real + a.Imaginary / (_Qv + Utility.Sqrt(_Qv * _Qv + 1.0f)));
			}
			else
			{
				// use 1 1/2 precision to preserve bits
				const float _Root2 = (float)1.4142135623730950488016887242096981;
				const float _Oneplusroot2high = (float)(10125945.0 / 4194304.0); // exact if prec >= 24 bits
				const float _Oneplusroot2low = (float)1.4341252375973918872420969807856967e-7;

				float _Qv = _Tmp / a.Imaginary;
				float _Rv = (_Qv + 2.0f) * _Qv;
				float _Sv = _Rv / (_Root2 + Utility.Sqrt(_Rv + 2.0f)) + _Oneplusroot2low + _Qv + _Oneplusroot2high;
				return (a.Real + a.Imaginary / _Sv);
			}
		}

		public static Complex Sqrt(Complex a)
		{
			// return sqrt(complex)
			int exp;
			float _Rho = MagnitudeAndScaleFactor(a, out exp); // get magnitude and scale factor

			if (exp == 0.0f)
				return (new Complex(_Rho, _Rho)); // argument is zero, INF, or NaN
			
			// compute in safest quadrant
			float _Realmag = (a.Real < 0.0f ? -a.Real : a.Real) * Utility.Pow(2.0f, -exp);
			_Rho = (Utility.Sqrt(2.0f * (_Realmag + _Rho))) * Utility.Pow(2.0f, exp / 2.0f - 1.0f);

			if (0.0f <= a.Real)
				return (new Complex(_Rho, a.Imaginary / (2.0f * _Rho)));

			if (a.Imaginary < 0.0f)
				return (new Complex(-a.Imaginary / (2.0f * _Rho), -_Rho));

			return (new Complex(a.Imaginary / (2.0f * _Rho), _Rho));
		}

		#endregion

		#endregion

		#region Operators

		public static bool operator ==(Complex a, Complex b)
		{
			return a.Real == b.Real && a.Imaginary == b.Imaginary;
		}

		public static bool operator !=(Complex a, Complex b)
		{
			return a.Real != b.Real || a.Imaginary != b.Imaginary;
		}

		public static Complex operator +(Complex a, Complex b)
		{
			Complex c;
			c.Real = a.Real + b.Real;
			c.Imaginary = a.Imaginary + b.Imaginary;
			return c;
		}

		public static Complex operator +(float a, Complex b)
		{
			Complex c;
			c.Real = a + b.Real;
			c.Imaginary = b.Imaginary;
			return c;
		}


		public static Complex operator +(Complex a, float b)
		{
			Complex c;
			c.Real = a.Real + b;
			c.Imaginary = a.Imaginary;
			return c;
		}

		public static Complex operator +(in Complex a)
		{
			Complex c;
			c.Real = +a.Real;
			c.Imaginary = +a.Imaginary;
			return c;
		}

		public static Complex operator -(Complex a, Complex b)
		{
			Complex c;
			c.Real = a.Real - b.Real;
			c.Imaginary = a.Imaginary - b.Imaginary;
			return c;
		}

		public static Complex operator -(Complex a, float b)
		{
			Complex c;
			c.Real = a.Real - b;
			c.Imaginary = a.Imaginary;
			return c;
		}

		public static Complex operator -(float a, Complex b)
		{
			Complex c;
			c.Real = a - b.Real;
			c.Imaginary = -b.Imaginary;
			return c;
		}

		public static Complex operator -(Complex a)
		{
			Complex c;
			c.Real = -a.Real;
			c.Imaginary = -a.Imaginary;
			return c;
		}

		public static Complex operator *(Complex a, Complex b)
		{
			Complex c;
			c.Real = a.Real * b.Real - a.Imaginary * b.Imaginary;
			c.Imaginary = a.Real * b.Imaginary + a.Imaginary * b.Real;
			return c;
		}

		public static Complex operator *(Complex a, float b)
		{
			Complex c;
			c.Real = a.Real * b;
			c.Imaginary = a.Imaginary * b;
			return c;
		}

		public static Complex operator /(Complex a, Complex b)
		{
			float magSq = b.MagnitudeSquared;
			magSq = 1.0f / magSq;
			Complex c;
			c.Real = (a.Real * b.Real + a.Imaginary * b.Imaginary) * magSq;
			c.Imaginary = (a.Imaginary * b.Real - a.Real * b.Imaginary) * magSq;
			return c;
		}

		public static Complex operator /(Complex a, float b)
		{
			Complex c;
			c.Real = a.Real / b;
			c.Imaginary = a.Imaginary / b;
			return c;
		}

		public static Complex operator /(float a, Complex b)
		{
			Complex c;
			c.Real = a / b.Real;
			c.Imaginary = a / b.Imaginary;
			return c;
		}

		public static implicit operator Complex(float a)
		{
			Complex c;
			c.Real = a;
			c.Imaginary = 0.0f;
			return c;
		}

		#endregion
	}
}
