﻿namespace Iridescence.Math
{
	public partial struct Matrix3x3
	{
		#region Translation

		/// <summary>
		/// Creates a translation matrix.
		/// </summary>
		/// <param name="translation">The translation vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateTranslation(ref Vector2 translation, out Matrix3x3 result)
		{
			result = Matrix3x3.Identity;
			result.M13 = translation.X;
			result.M23 = translation.Y;
		}

		/// <summary>
		/// Creates a translation matrix.
		/// </summary>
		/// <param name="translation">The translation vector.</param>
		/// <returns>The resulting matrix.</returns>
		public static Matrix3x3 CreateTranslation(ref Vector2 translation)
		{
			Matrix3x3 result = Matrix3x3.Identity;
			result.M13 = translation.X;
			result.M23 = translation.Y;
			return result;
		}
		
		/// <summary>
		/// Creates a translation matrix.
		/// </summary>
		/// <param name="translation">The translation vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateTranslation(Vector2 translation, out Matrix3x3 result)
		{
			result = Matrix3x3.Identity;
			result.M13 = translation.X;
			result.M23 = translation.Y;
		}

		/// <summary>
		/// Creates a translation matrix.
		/// </summary>
		/// <param name="translation">The translation vector.</param>
		/// <returns>The resulting matrix.</returns>
		public static Matrix3x3 CreateTranslation(Vector2 translation)
		{
			Matrix3x3 result = Matrix3x3.Identity;
			result.M13 = translation.X;
			result.M23 = translation.Y;
			return result;
		}

		#endregion

		#region Rotation

		/// <summary>
		/// Creates a rotation matrix based on an axis/angle rotation and stores the result in the specified matrix.
		/// </summary>
		/// <param name="axis">The axis to rotate about.</param>
		/// <param name="angle">The angle in radians (counter-clockwise).</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(Vector3 axis, float angle, out Matrix3x3 result)
		{
			// http://en.wikipedia.org/wiki/Rotation_matrix#Axis_and_angle
			axis = axis.Normalize();
			
			float c = Utility.Cos(angle);
			float s = Utility.Sin(angle);
			float C = 1.0f - c;
			
			float xxC = axis.X * axis.X * C;
			float xyC = axis.X * axis.Y * C;
			float xzC = axis.X * axis.Z * C;
			float yyC = axis.Y * axis.Y * C;
			float yzC = axis.Y * axis.Z * C;
			float zzC = axis.Z * axis.Z * C;
			
			float xs = s * axis.X;
			float ys = s * axis.Y;
			float zs = s * axis.Z;
			
			result.M11 = xxC + c;
			result.M12 = xyC - zs;
			result.M13 = xzC + ys;
			
			result.M21 = xyC + zs;
			result.M22 = yyC + c;
			result.M23 = yzC - xs;
			
			result.M31 = xzC - ys;
			result.M32 = yzC + xs;
			result.M33 = zzC + c;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on an axis/angle rotation and returns the result.
		/// </summary>
		/// <param name="axis">The axis to rotate about.</param>
		/// <param name="angle">The angle in radians (counter-clockwise).</param>
		public static Matrix3x3 CreateRotation(Vector3 axis, float angle)
		{
			Matrix3x3 result;
			Matrix3x3.CreateRotation(axis, angle, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and stores the result in the specified matrix.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(ref Quaternion q, out Matrix3x3 result)
		{
			Quaternion qn = q.Normalize();
		
			float xx = qn.X * qn.X;
			float yy = qn.Y * qn.Y;
			float zz = qn.Z * qn.Z;
			float xy = qn.X * qn.Y;
			float zw = qn.Z * qn.W;
			float zx = qn.Z * qn.X;
			float yw = qn.Y * qn.W;
			float yz = qn.Y * qn.Z;
			float xw = qn.X * qn.W;
		
			result.M11 = 1f - (2f * (yy + zz));
			result.M21 = 2f * (xy + zw);
			result.M31 = 2f * (zx - yw);
		
			result.M12 = 2f * (xy - zw);
			result.M22 = 1f - (2f * (zz + xx));
			result.M32 = 2f * (yz + xw);
		
			result.M13 = 2f * (zx + yw);
			result.M23 = 2f * (yz - xw);
			result.M33 = 1f - (2f * (yy + xx));
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and stores the result in the specified matrix.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(Quaternion q, out Matrix3x3 result)
		{
			q = q.Normalize();
		
			float xx = q.X * q.X;
			float yy = q.Y * q.Y;
			float zz = q.Z * q.Z;
			float xy = q.X * q.Y;
			float zw = q.Z * q.W;
			float zx = q.Z * q.X;
			float yw = q.Y * q.W;
			float yz = q.Y * q.Z;
			float xw = q.X * q.W;
		
			result.M11 = 1f - (2f * (yy + zz));
			result.M21 = 2f * (xy + zw);
			result.M31 = 2f * (zx - yw);
		
			result.M12 = 2f * (xy - zw);
			result.M22 = 1f - (2f * (zz + xx));
			result.M32 = 2f * (yz + xw);
		
			result.M13 = 2f * (zx + yw);
			result.M23 = 2f * (yz - xw);
			result.M33 = 1f - (2f * (yy + xx));
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and returns the result.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		public static Matrix3x3 CreateRotation(ref Quaternion q)
		{
			Matrix3x3 result;
			Matrix3x3.CreateRotation(ref q, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and returns the result.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		public static Matrix3x3 CreateRotation(Quaternion q)
		{
			Matrix3x3 result;
			Matrix3x3.CreateRotation(ref q, out result);
			return result;
		}

		#endregion

		#region Scale

		/// <summary>
		/// Creates a scale matrix.
		/// </summary>
		/// <param name="scale">The scale vector.</param>
		public static void CreateScale(ref Vector3 scale, out Matrix3x3 result)
		{
			result = Matrix3x3.Zero;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
		}

		/// <summary>
		/// Creates a scale matrix.
		/// </summary>
		/// <param name="scale">The scale vector.</param>
		public static void CreateScale(Vector3 scale, out Matrix3x3 result)
		{
			result = Matrix3x3.Zero;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
		}
		
		/// <summary>
		/// Creates a scale matrix.
		/// </summary>
		/// <param name="scale">The scale vector.</param>
		/// <returns></returns>
		public static Matrix3x3 CreateScale(ref Vector3 scale)
		{
			Matrix3x3 result = Matrix3x3.Zero;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
			return result;
		}

		/// <summary>
		/// Creates a scale matrix.
		/// </summary>
		/// <param name="scale">The scale vector.</param>
		/// <returns></returns>
		public static Matrix3x3 CreateScale(Vector3 scale)
		{
			Matrix3x3 result = Matrix3x3.Zero;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
			return result;
		}

		#endregion

		#region LookAt

		/// <summary>
		/// Creates a view matrix that looks towards the target position and stores the result in the specified matrix.
		/// </summary>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateLookAt(ref Vector3 target, ref Vector3 up, out Matrix3x3 result)
		{
			Vector3 x, y, z;
			
			Vector3.Normalize(target, out z);
			Vector3.Negate(z, out z);
			
			Vector3.Cross(up, z, out x);
			Vector3.Normalize(x, out x);
			
			Vector3.Cross(z, x, out y);
			Vector3.Normalize(y, out y);
			
			result.M11 = x.X;
			result.M12 = x.Y;
			result.M13 = x.Z;
			
			result.M21 = y.X;
			result.M22 = y.Y;
			result.M23 = y.Z;
			
			result.M31 = z.X;
			result.M32 = z.Y;
			result.M33 = z.Z;
		}
		
		/// <summary>
		/// Creates a view matrix that looks towards the target position and stores the result in the specified matrix.
		/// </summary>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateLookAt(Vector3 target, Vector3 up, out Matrix3x3 result)
		{
			Matrix3x3.CreateLookAt(ref target, ref up, out result);
		}
		
		/// <summary>
		/// Creates a view matrix that looks towards the target position and returns the result.
		/// </summary>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		public static Matrix3x3 CreateLookAt(ref Vector3 target, ref Vector3 up)
		{
			Matrix3x3 result;
			Matrix3x3.CreateLookAt(ref target, ref up, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a view matrix that looks towards the target position and returns the result.
		/// </summary>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		public static Matrix3x3 CreateLookAt(Vector3 target, Vector3 up)
		{
			Matrix3x3 result;
			Matrix3x3.CreateLookAt(ref target, ref up, out result);
			return result;
		}

		#endregion
	}
}
