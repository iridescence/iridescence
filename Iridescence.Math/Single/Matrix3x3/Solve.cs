﻿namespace Iridescence.Math
{
	public partial struct Matrix3x3
	{
		#region Vector2

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix3x3 a, ref Vector2 b, out Vector2 x)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix3x3 a, Vector2 b, out Vector2 x)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix3x3 a, ref Vector2 b, out Vector2 x)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix3x3 a, Vector2 b, out Vector2 x)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2 Solve(ref Matrix3x3 a, ref Vector2 b)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			Vector2 x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2 Solve(ref Matrix3x3 a, Vector2 b)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			Vector2 x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2 Solve(Matrix3x3 a, ref Vector2 b)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			Vector2 x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2 Solve(Matrix3x3 a, Vector2 b)
		{
			float det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0f)
				det = 1.0f / det;

			Vector2 x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		#endregion

		#region Vector3

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix3x3 a, ref Vector3 b, out Vector3 x)
		{
			Vector3 c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;
			
			Vector3 cx;
			Vector3.Cross(in c2, in c3, out cx);
			float det = Vector3.Dot(c1, cx);
			if (det != 0.0f)
				det = 1.0f / det;

			Vector3.Cross(in c2, in c3, out cx);
			x.X = det * Vector3.Dot(b, cx);

			Vector3.Cross(in b, in c3, out cx);
			x.Y = det * Vector3.Dot(c1, cx);

			Vector3.Cross(in c2, in b, out cx);
			x.Z = det * Vector3.Dot(c1, cx);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix3x3 a, Vector3 b, out Vector3 x)
		{
			Vector3 c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3 cx;
			Vector3.Cross(in c2, in c3, out cx);
			float det = Vector3.Dot(c1, cx);
			if (det != 0.0f)
				det = 1.0f / det;

			Vector3.Cross(in c2, in c3, out cx);
			x.X = det * Vector3.Dot(b, cx);

			Vector3.Cross(in b, in c3, out cx);
			x.Y = det * Vector3.Dot(c1, cx);

			Vector3.Cross(in c2, in b, out cx);
			x.Z = det * Vector3.Dot(c1, cx);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix3x3 a, ref Vector3 b, out Vector3 x)
		{
			Vector3 c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3 cx;
			Vector3.Cross(in c2, in c3, out cx);
			float det = Vector3.Dot(c1, cx);
			if (det != 0.0f)
				det = 1.0f / det;

			Vector3.Cross(in c2, in c3, out cx);
			x.X = det * Vector3.Dot(b, cx);

			Vector3.Cross(in b, in c3, out cx);
			x.Y = det * Vector3.Dot(c1, cx);

			Vector3.Cross(in c2, in b, out cx);
			x.Z = det * Vector3.Dot(c1, cx);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix3x3 a, Vector3 b, out Vector3 x)
		{
			Vector3 c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3 cx;
			Vector3.Cross(in c2, in c3, out cx);
			float det = Vector3.Dot(c1, cx);
			if (det != 0.0f)
				det = 1.0f / det;

			Vector3.Cross(in c2, in c3, out cx);
			x.X = det * Vector3.Dot(b, cx);

			Vector3.Cross(in b, in c3, out cx);
			x.Y = det * Vector3.Dot(c1, cx);

			Vector3.Cross(in c2, in b, out cx);
			x.Z = det * Vector3.Dot(c1, cx);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector3 Solve(ref Matrix3x3 a, ref Vector3 b)
		{
			Vector3 x;

			Vector3 c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3 cx;
			Vector3.Cross(in c2, in c3, out cx);
			float det = Vector3.Dot(c1, cx);
			if (det != 0.0f)
				det = 1.0f / det;

			Vector3.Cross(in c2, in c3, out cx);
			x.X = det * Vector3.Dot(b, cx);

			Vector3.Cross(in b, in c3, out cx);
			x.Y = det * Vector3.Dot(c1, cx);

			Vector3.Cross(in c2, in b, out cx);
			x.Z = det * Vector3.Dot(c1, cx);

			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector3 Solve(ref Matrix3x3 a, Vector3 b)
		{
			Vector3 x;

			Vector3 c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3 cx;
			Vector3.Cross(in c2, in c3, out cx);
			float det = Vector3.Dot(c1, cx);
			if (det != 0.0f)
				det = 1.0f / det;

			Vector3.Cross(in c2, in c3, out cx);
			x.X = det * Vector3.Dot(b, cx);

			Vector3.Cross(in b, in c3, out cx);
			x.Y = det * Vector3.Dot(c1, cx);

			Vector3.Cross(in c2, in b, out cx);
			x.Z = det * Vector3.Dot(c1, cx);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector3 Solve(Matrix3x3 a, ref Vector3 b)
		{
			Vector3 x;

			Vector3 c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3 cx;
			Vector3.Cross(in c2, in c3, out cx);
			float det = Vector3.Dot(c1, cx);
			if (det != 0.0f)
				det = 1.0f / det;

			Vector3.Cross(in c2, in c3, out cx);
			x.X = det * Vector3.Dot(b, cx);

			Vector3.Cross(in b, in c3, out cx);
			x.Y = det * Vector3.Dot(c1, cx);

			Vector3.Cross(in c2, in b, out cx);
			x.Z = det * Vector3.Dot(c1, cx);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector3 Solve(Matrix3x3 a, Vector3 b)
		{
			Vector3 x;

			Vector3 c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3 cx;
			Vector3.Cross(in c2, in c3, out cx);
			float det = Vector3.Dot(c1, cx);
			if (det != 0.0f)
				det = 1.0f / det;

			Vector3.Cross(in c2, in c3, out cx);
			x.X = det * Vector3.Dot(b, cx);

			Vector3.Cross(in b, in c3, out cx);
			x.Y = det * Vector3.Dot(c1, cx);

			Vector3.Cross(in c2, in b, out cx);
			x.Z = det * Vector3.Dot(c1, cx);
			return x;
		}

		#endregion
	}
}
