﻿namespace Iridescence.Math
{
	/// <summary>
	/// Single-precision floating-point constants.
	/// </summary>
	public static class Constants
	{
		/// <summary>
		/// The natural constant pi.
		/// </summary>
		public const float Pi = 3.1415926535897932384626433832795f;

		/// <summary>
		/// 0.5 * <see cref="Pi"/>.
		/// </summary>
		public const float PiOver2 = 1.5707963267948966192313216916398f;

		/// <summary>
		/// 2 * <see cref="Pi"/>. 
		/// </summary>
		public const float Pi2 = 6.283185307179586476925286766559f;

		/// <summary>
		/// Euler's number.
		/// </summary>
		public const double E = 2.7182818284590452353602874713526624977572470936999595f;

		/// <summary>
		/// Gets the amount of radians per degree = pi / 180.
		/// </summary>
		public const float RadiansPerDegree = Pi / 180.0f;

		/// <summary>
		/// Gets the amount of degrees per radians = 180 / pi.
		/// </summary>
		public const float DegreesPerRadian = 180.0f / Pi;
	}
}
