using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a 2D rotation using the sine and cosine of the angle.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public struct Rotation
	{
		#region Fields

		public static Rotation Identity = new Rotation(0.0f);

		/// <summary>
		/// Cosine of the angle.
		/// </summary>
		public float C;

		/// <summary>
		/// Sine of the angle.
		/// </summary>
		public float S;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the angle.
		/// </summary>
		public float Angle
		{
			get => (float)System.Math.Atan2(this.S, this.C);
			set
			{
				this.S = (float)System.Math.Sin(value);
				this.C = (float)System.Math.Cos(value);
			}
		}

		/// <summary>
		/// Gets the local X axis.
		/// </summary>
		public Vector2 XAxis => new Vector2(this.C, this.S);

		/// <summary>
		/// Gets the local y axis.
		/// </summary>
		public Vector2 YAxis => new Vector2(-this.S, this.C);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new rotation using the specified angle.
		/// </summary>
		/// <param name="angle">The angle.</param>
		public Rotation(float angle)
		{
			this.S = (float)System.Math.Sin(angle);
			this.C = (float)System.Math.Cos(angle);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Multiplies two rotations.
		/// </summary>
		/// <param name="q"></param>
		/// <param name="r"></param>
		/// <param name="qr"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Multiply(in Rotation q, in Rotation r, out Rotation qr)
		{
			float s = q.S * r.C + q.C * r.S;
			qr.C = q.C * r.C - q.S * r.S;
			qr.S = s;
		}

		/// <summary>
		/// Multiplies the transposed of Q with R.
		/// </summary>
		/// <param name="q"></param>
		/// <param name="r"></param>
		/// <param name="qr"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void MultiplyTransposed(in Rotation q, in Rotation r, out Rotation qr)
		{
			float s = q.C * r.S - q.S * r.C;
			qr.C = q.C * r.C + q.S * r.S;
			qr.S = s;
		}

		/// <summary>
		/// Multiplies the rotation with a vector.
		/// </summary>
		/// <param name="q"></param>
		/// <param name="v"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Vector2 Multiply(in Rotation q, in Vector2 v)
		{
			Vector2 b;
			b.X = q.C * v.X - q.S * v.Y;
			b.Y = q.S * v.X + q.C * v.Y;
			return b;
		}

		/// <summary>
		/// Multiplies the rotation with a vector.
		/// </summary>
		/// <param name="q"></param>
		/// <param name="v"></param>
		/// <param name="b"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Multiply(in Rotation q, in Vector2 v, out Vector2 b)
		{
			float x = q.C * v.X - q.S * v.Y;
			b.Y = q.S * v.X + q.C * v.Y;
			b.X = x;
		}

		/// <summary>
		/// Multiplies the transposed rotation with a vector.
		/// </summary>
		/// <param name="q"></param>
		/// <param name="v"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Vector2 MultiplyTransposed(in Rotation q, in Vector2 v)
		{
			Vector2 b;
			b.X = q.C * v.X + q.S * v.Y;
			b.Y = -q.S * v.X + q.C * v.Y;
			return b;
		}

		/// <summary>
		/// Multiplies the transposed rotation with a vector.
		/// </summary>
		/// <param name="q"></param>
		/// <param name="v"></param>
		/// <param name="b"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void MultiplyTransposed(in Rotation q, in Vector2 v, out Vector2 b)
		{
			float x = q.C * v.X + q.S * v.Y;
			b.Y = -q.S * v.X + q.C * v.Y;
			b.X = x;
		}

		/// <summary>
		/// Returns the matrix that represents the same rotation.
		/// </summary>
		/// <returns></returns>
		public Matrix2x2 ToMatrix2x2()
		{
			Matrix2x2 result;
			result.M11 = this.C;
			result.M12 = -this.S;
			result.M21 = this.S;
			result.M22 = this.C;
			return result;
		}

		/// <summary>
		/// Returns the matrix that represents the same rotation.
		/// </summary>
		/// <returns></returns>
		public void ToMatrix2x2(out Matrix2x2 result)
		{
			result.M11 = this.C;
			result.M12 = -this.S;
			result.M21 = this.S;
			result.M22 = this.C;
		}

		/// <summary>
		/// Returns the matrix that represents the same rotation.
		/// </summary>
		/// <returns></returns>
		public Matrix3x3 ToMatrix3x3()
		{
			Matrix3x3 result;
			result.M11 = this.C;
			result.M12 = -this.S;
			result.M13 = 0.0f;
			result.M21 = this.S;
			result.M22 = this.C;
			result.M23 = 0.0f;
			result.M31 = 0.0f;
			result.M32 = 0.0f;
			result.M33 = 1.0f;
			return result;
		}

		/// <summary>
		/// Returns the matrix that represents the same rotation.
		/// </summary>
		/// <returns></returns>
		public void ToMatrix3x3(out Matrix3x3 result)
		{
			result.M11 = this.C;
			result.M12 = -this.S;
			result.M13 = 0.0f;
			result.M21 = this.S;
			result.M22 = this.C;
			result.M23 = 0.0f;
			result.M31 = 0.0f;
			result.M32 = 0.0f;
			result.M33 = 1.0f;
		}
		
		/// <summary>
		/// Returns a vector where X = C and Y = S.
		/// </summary>
		public static implicit operator Vector2(in Rotation rotation)
		{
			Vector2 vec;
			vec.X = rotation.C;
			vec.Y = rotation.S;
			return vec;
		}

		/// <summary>
		/// Returns a rotation where C = X and S = Y.
		/// </summary>
		public static explicit operator Rotation(in Vector2 rotation)
		{
			Rotation rot;
			rot.C = rotation.X;
			rot.S = rotation.Y;
			return rot;
		}

		public override string ToString()
		{
			return $"{Constants.DegreesPerRadian * this.Angle}�";
		}

		#endregion
	}
}
