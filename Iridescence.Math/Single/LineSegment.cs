﻿using System;
using System.Diagnostics.Contracts;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a 2-dimensional line segment.
	/// </summary>
	[Serializable]
	public struct LineSegment : IEquatable<LineSegment>, ICsgComponent<LineSegment>
	{
		#region Fields

		/// <summary>
		/// The start point of the segment.
		/// </summary>
		public Vector2 Start;

		/// <summary>
		/// The end point of the segment.
		/// </summary>
		public Vector2 End;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the length of the line segment.
		/// </summary>
		public float Length => Vector2.Distance(this.Start, this.End);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="LineSegment"/>.
		/// </summary>
		public LineSegment(Vector2 start, Vector2 end)
		{
			this.Start = start;
			this.End = end;
		}

		/// <summary>
		/// Creates a new <see cref="LineSegment"/>.
		/// </summary>
		public LineSegment(float x1, float y1, float x2, float y2)
		{
			this.Start.X = x1;
			this.Start.Y = y1;
			this.End.X = x2;
			this.End.Y = y2;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Inverts the <see cref="LineSegment"/> (i.e. swaps start and end points).
		/// </summary>
		/// <returns></returns>
		[Pure]
		public LineSegment Invert()
		{
			LineSegment inverted;
			inverted.Start = this.End;
			inverted.End = this.Start;
			return inverted;
		}

		/// <summary>
		/// Splits the <see cref="LineSegment"/> at the specified position.
		/// </summary>
		/// <param name="t">The position at which to split the line segment, in the interval [0..1].</param>
		/// <param name="first"></param>
		/// <param name="second"></param>
		[Pure]
		public void Split(float t, out LineSegment first, out LineSegment second)
		{
			if (t < 0.0f || t > 1.0f)
				throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

			first.Start = this.Start;
			first.End = this.Start + t * (this.End - this.Start);
			second.Start = first.End;
			second.End = this.End;
		}

		/// <summary>
		/// Splits the <see cref="LineSegment"/>.
		/// </summary>
		/// <param name="other"></param>
		/// <param name="front"></param>
		/// <param name="back"></param>
		[Pure]
		public void Split(LineSegment other, out LineSegment front, out LineSegment back)
		{
			Vector2 d;
			Vector2.Subtract(other.End, other.Start, out d);
			//Vector2.Normalize(ref d, out d);

			Vector2 v1;
			Vector2.Subtract(other.Start, this.Start, out v1);

			Vector2 v2;
			Vector2.Subtract(this.End, this.Start, out v2);

			Vector2 v3;
			v3.X = -d.Y;
			v3.Y = d.X;

			float v1Dotv3 = Vector2.Dot(v1, v3);
			float v2Dotv3 = Vector2.Dot(v2, v3);
			float t = v1Dotv3 / v2Dotv3;

			if (t <= 0.0f)
			{
				front = new LineSegment(this.Start, this.Start);
				back = this;
			}
			else if (t >= 1.0f)
			{
				front = this;
				back = new LineSegment(this.End, this.End);
			}
			else 
			{
				float t2 = 1.0f - t;
				front.Start = this.Start;
				front.End.X = this.Start.X * t2 + this.End.X * t;
				front.End.Y = this.Start.Y * t2 + this.End.Y * t;

				back.Start = front.End;
				back.End = this.End;

				if (v2Dotv3 >= 0.0f)
				{
					Utility.Swap(ref front, ref back);
				}
			}
		}

		/// <summary>
		/// Returns what side of the <see cref="LineSegment"/> the specified point is on.
		/// </summary>
		/// <param name="point"></param>
		/// <returns></returns>
		[Pure]
		public int Side(Vector2 point)
		{
			return System.Math.Sign((this.End.X - this.Start.X) * (point.Y - this.Start.Y) - (this.End.Y - this.Start.Y) * (point.X - this.Start.X));
		}
		
		/// <summary>
		/// Returns what side of the <see cref="LineSegment"/> the specified point is on.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static int Side(Vector2 start, Vector2 end, Vector2 point)
		{
			return System.Math.Sign((end.X - start.X) * (point.Y - start.Y) - (end.Y - start.Y) * (point.X - start.X));
		}

		[Pure]
		CsgComponentRelation ICsgComponent<LineSegment>.GetRelationTo(LineSegment other)
		{
			int sideA = this.Side(other.Start);
			int sideB = this.Side(other.End);

			CsgComponentRelation relation = 0;
			if (sideA < 0 || sideB < 0)
				relation |= CsgComponentRelation.Back;

			if (sideA > 0 || sideB > 0)
				relation |= CsgComponentRelation.Front;

			if (sideA == 0 && sideB == 0)
				relation |= CsgComponentRelation.Coplanar;

			return relation;
		}
		
		[Pure]
		LineSegment ICsgComponent<LineSegment>.Clone()
		{
			return this;
		}

		/// <summary>
		/// Returns the point of intersection between two lines.
		/// </summary>
		/// <param name="p1">The first point on the first line.</param>
		/// <param name="p2">The second point on the first line.</param>
		/// <param name="q1">The first point on the second line.</param>
		/// <param name="q2">The second point on the second line.</param>
		/// <returns>The point of intersection.</returns>
		/// <remarks>
		/// This actually returns the intersection between two infinite lines that go through the specified points.
		/// Use <see cref="IsIntersecting(Vector2,Vector2,Vector2,Vector2)"/> to determine whether two line segments are intersecting.
		/// </remarks>
		public static Vector2 Intersect(Vector2 p1, Vector2 p2, Vector2 q1, Vector2 q2)
		{
			float a1 = p2.Y - p1.Y;
			float b1 = p1.X - p2.X;
			float c1 = a1 * p1.X + b1 * p1.Y;
			float a2 = q2.Y - q1.Y;
			float b2 = q1.X - q2.X;
			float c2 = a2 * q1.X + b2 * q1.Y;
			float det = a1 * b2 - a2 * b1;
			Vector2 result;
			result.X = (b2 * c1 - b1 * c2) / det;
			result.Y = (a1 * c2 - a2 * c1) / det;
			return result;
		}

		/// <summary>
		/// Returns the point of intersection between two lines.
		/// </summary>
		/// <param name="p">The first line segment.</param>
		/// <param name="q">The second line segment.</param>
		/// <returns>The point of intersection.</returns>
		/// <remarks>
		/// This actually returns the intersection between two infinite lines that go through the specified points.
		/// Use <see cref="IsIntersecting(LineSegment,LineSegment)"/> to determine whether two line segments are intersecting.
		/// </remarks>
		public static Vector2 Intersect(LineSegment p, LineSegment q)
		{
			return Intersect(p.Start, p.End, q.Start, q.End);
		}

		/// <summary>
		/// Given three colinear points p, q, r, the function checks if point q lies on line segment 'pr'
		/// </summary>
		/// <param name="p"></param>
		/// <param name="q"></param>
		/// <param name="r"></param>
		/// <returns></returns>
		private static bool onSegment(Vector2 p, Vector2 q, Vector2 r)
		{
			return q.X <= Utility.Max(p.X, r.X) && q.X >= Utility.Min(p.X, r.X) &&
			       q.Y <= Utility.Max(p.Y, r.Y) && q.Y >= Utility.Min(p.Y, r.Y);
		}

		/// <summary>
		/// Returns a value that indicates whether two line segments are intersecting.
		/// </summary>
		/// <param name="p1">The first point on the first line.</param>
		/// <param name="p2">The second point on the first line.</param>
		/// <param name="q1">The first point on the second line.</param>
		/// <param name="q2">The second point on the second line.</param>
		/// <returns>True, if the specified segments are intersecting. False otherwise.</returns>
		public static bool IsIntersecting(Vector2 p1, Vector2 p2, Vector2 q1, Vector2 q2)
		{
			int o1 = Side(p1, p2, q1);
			int o2 = Side(p1, p2, q2);
			int o3 = Side(q1, q2, p1);
			int o4 = Side(q1, q2, p2);
 
			if (o1 != o2 && o3 != o4)
				return true;

			return o1 == 0 && onSegment(p1, q1, p2) ||
			       o2 == 0 && onSegment(p1, q2, p2) ||
			       o3 == 0 && onSegment(q1, p1, q2) ||
			       o4 == 0 && onSegment(q1, p2, q2);
		}

		/// <summary>
		/// Returns a value that indicates whether two line segments are intersecting.
		/// </summary>
		/// <param name="p">The first line segment.</param>
		/// <param name="q">The second line segment.</param>
		/// <returns>True, if the specified segments are intersecting. False otherwise.</returns>
		public static bool IsIntersecting(LineSegment p, LineSegment q)
		{
			return IsIntersecting(p.Start, p.End, q.Start, q.End);
		}

		[Pure]
		public Rectangle GetBounds()
		{
			Vector2.Min(this.Start, this.End, out Vector2 min);
			Vector2.Max(this.Start, this.End, out Vector2 max);
			Vector2.Subtract(max, min, out max);
			return new Rectangle(min, max);
		}
		
		[Pure]
		public override string ToString()
		{
			return $"({this.Start}) - ({this.End})";
		}

		/// <summary>
		/// Returns a value that indicates whether the this <see cref="LineSegment"/> and the specified <see cref="LineSegment"/> are equal.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		[Pure]
		public bool Equals(LineSegment other)
		{
			return this.Start.Equals(other.Start) && this.End.Equals(other.End);
		}

		/// <summary>
		/// Returns a value that indicates whether the this <see cref="LineSegment"/> and the specified object are equal.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		[Pure]
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is LineSegment && this.Equals((LineSegment)obj);
		}

		/// <summary>
		/// Returns the hash code of this <see cref="LineSegment"/>.
		/// </summary>
		/// <returns></returns>
		[Pure]
		public override int GetHashCode()
		{
			unchecked
			{
				return (this.Start.GetHashCode() * 397) ^ this.End.GetHashCode();
			}
		}

		public static bool operator ==(LineSegment left, LineSegment right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(LineSegment left, LineSegment right)
		{
			return !left.Equals(right);
		}
		
		#endregion
	}
}
