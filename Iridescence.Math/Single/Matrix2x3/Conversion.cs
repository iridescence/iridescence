﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;

namespace Iridescence.Math
{
	public partial struct Matrix2x3
	{
		[System.Diagnostics.Contracts.Pure]
		public static implicit operator (float m11, float m12, float m13, float m21, float m22, float m23)(in Matrix2x3 value)
		{
			return (value.M11, value.M12, value.M13, value.M21, value.M22, value.M23);
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static implicit operator Matrix2x3((float m11, float m12, float m13, float m21, float m22, float m23) value)
		{
			Matrix2x3 result;
			result.M11 = value.m11;
			result.M12 = value.m12;
			result.M13 = value.m13;
			result.M21 = value.m21;
			result.M22 = value.m22;
			result.M23 = value.m23;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static implicit operator Matrix2x3D(in Matrix2x3 value)
		{
			Matrix2x3D result;
			result.M11 = value.M11;
			result.M12 = value.M12;
			result.M13 = value.M13;
			result.M21 = value.M21;
			result.M22 = value.M22;
			result.M23 = value.M23;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static implicit operator (double m11, double m12, double m13, double m21, double m22, double m23)(in Matrix2x3 value)
		{
			return (value.M11, value.M12, value.M13, value.M21, value.M22, value.M23);
		}
		
		public void ToArray(Span<float> destination)
		{
			if (destination.Length < 6)
				throw new ArgumentException("Destination array is too small.", nameof(destination));
			
			destination[0] = this.M11;
			destination[1] = this.M12;
			destination[2] = this.M13;
			destination[3] = this.M21;
			destination[4] = this.M22;
			destination[5] = this.M23;
		}
		
		public void ToArray(float[] destination, int offset = 0)
		{
			this.ToArray(new Span<float>(destination, offset, 6));
		}
		
		[System.Diagnostics.Contracts.Pure]
		public float[] ToArray()
		{
			float[] array = new float[6];
			this.ToArray(array);
			return array;
		}
		
	}
}
