﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;

namespace Iridescence.Math
{
	public partial struct Matrix2x3 : IEquatable<Matrix2x3>
	{
		[System.Diagnostics.Contracts.Pure]
		public override bool Equals(object obj)
		{
			if (!(obj is Matrix2x3)) return false;
			Matrix2x3 other = (Matrix2x3)obj;
			return this.M11 == other.M11
			    && this.M12 == other.M12
			    && this.M13 == other.M13
			    && this.M21 == other.M21
			    && this.M22 == other.M22
			    && this.M23 == other.M23;
		}
		
		[System.Diagnostics.Contracts.Pure]
		bool IEquatable<Matrix2x3>.Equals(Matrix2x3 other)
		{
			return this.M11 == other.M11
			    && this.M12 == other.M12
			    && this.M13 == other.M13
			    && this.M21 == other.M21
			    && this.M22 == other.M22
			    && this.M23 == other.M23;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public bool Equals(in Matrix2x3 value)
		{
			return this.M11 == value.M11
			    && this.M12 == value.M12
			    && this.M13 == value.M13
			    && this.M21 == value.M21
			    && this.M22 == value.M22
			    && this.M23 == value.M23;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool Equals(in Matrix2x3 left, in Matrix2x3 right)
		{
			return left.M11 == right.M11
			    && left.M12 == right.M12
			    && left.M13 == right.M13
			    && left.M21 == right.M21
			    && left.M22 == right.M22
			    && left.M23 == right.M23;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool operator ==(in Matrix2x3 left, in Matrix2x3 right)
		{
			return left.M11 == right.M11
			    && left.M12 == right.M12
			    && left.M13 == right.M13
			    && left.M21 == right.M21
			    && left.M22 == right.M22
			    && left.M23 == right.M23;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool operator !=(in Matrix2x3 left, in Matrix2x3 right)
		{
			return left.M11 != right.M11
			    || left.M12 != right.M12
			    || left.M13 != right.M13
			    || left.M21 != right.M21
			    || left.M22 != right.M22
			    || left.M23 != right.M23;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public bool ApproximatelyEquals(in Matrix2x3 value, float tolerance)
		{
			return System.Math.Abs(this.M11 - value.M11) < tolerance
			    && System.Math.Abs(this.M12 - value.M12) < tolerance
			    && System.Math.Abs(this.M13 - value.M13) < tolerance
			    && System.Math.Abs(this.M21 - value.M21) < tolerance
			    && System.Math.Abs(this.M22 - value.M22) < tolerance
			    && System.Math.Abs(this.M23 - value.M23) < tolerance;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool ApproximatelyEquals(in Matrix2x3 left, in Matrix2x3 right, float tolerance)
		{
			return System.Math.Abs(left.M11 - right.M11) < tolerance
			    && System.Math.Abs(left.M12 - right.M12) < tolerance
			    && System.Math.Abs(left.M13 - right.M13) < tolerance
			    && System.Math.Abs(left.M21 - right.M21) < tolerance
			    && System.Math.Abs(left.M22 - right.M22) < tolerance
			    && System.Math.Abs(left.M23 - right.M23) < tolerance;
		}
		
	}
}
