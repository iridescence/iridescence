﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Iridescence.Math
{
	public partial struct Matrix2x3
	{
		public static readonly Matrix2x3 Zero = new Matrix2x3(0, 0, 0, 0, 0, 0);
		
		public static readonly Matrix2x3 One = new Matrix2x3(1, 1, 1, 1, 1, 1);
		
	}
}
