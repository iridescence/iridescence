﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Math
{
	/// <summary>
	/// <para>
	/// A structure representing a Quaternion,
	/// which can be used to represent orientation, i.e. rotation, of objects.
	/// </para>
	/// <para>
	/// Quaternions usually have the form a + bi + cj + dk, where a, b, c and d are real numbers, 
	/// of which a is called the real coefficient and b, c and d the 3 imaginary coefficients.
	/// While i, j and k are imaginary numbers, they've the following properties:
	/// i² = j² = k² = -1, i*j = k, j*k = i, k*i = j, j*i = -k, k*j = -i and i*k = -j.
	/// </para>
	/// <para>
	/// In this representation of Quaternions, they just consist of X, Y, Z and W, where W is 'a' and 
	/// X, Y and Z are bi, cj and dk, so we'll ignore the fact that there are imaginery portions of the quaternion.
	/// All the same we can easily refer to quaternions as (q, p) where q is w and p a three-vector of X, Y and Z.
	/// </para>
	/// <para>
	/// Quaternions in this representation are usually meant to be unit-quaternions with its euclidian norm 
	/// |q| = Sqrt(X² + Y² + Z² + W²) = 1. Unit quaternions offer us 3 degrees of freedom, rather than 4 (we actually don't need 4 degrees of freedom).
	/// </para>
	/// <para>
	/// The main advantages of quaternions over other rotation representations are:
	/// - They only consist of 4 values, on contrary to rotation matrices with at least 9 components.
	/// - They prevent gimbal lock, which may occur when rotating around different axis and with which you loose one degree of freedom.
	/// - They offer a great reliability in interpolation.
	/// </para>
	/// <para>
	/// While quaternion multiplication is associative (q1*(q2*q3)=(q1*q2)*q3), but not commutative (q1*q2!=q2*q1),
	/// multiplicating two quaternions just means to combine their rotation, so rotating a vector v by a quaternion q1 and
	/// rotating the rotated vector v' again by another quaternion q2 is the same, as if we just rotate v by q, with q = q1*q2.
	/// </para>
	/// <para>
	/// In this representation of quaternions vectors are rotated by v' = q * v * q^-1, with q^-1 being the inverse of q.
	/// </para>
	/// <para>
	/// | W X Y Z |
	/// </para>
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	[Serializable]
	public partial struct Quaternion
	{
		#region Fields

		/// <summary>
		/// The x-component of the quaternion, called an imaginary coefficient.
		/// </summary>
		public float X;

		/// <summary>
		/// The y-component of the quaternion, called an imaginary coefficient.
		/// </summary>
		public float Y;

		/// <summary>
		/// The z-component of the quaternion, called an imaginary coefficient.
		/// </summary>
		public float Z;

		/// <summary>
		/// The w-component of the quaternion, called the real coefficient.
		/// </summary>
		public float W;

		#region Static
		
		/// <summary>
		/// The identity quaternion.
		/// </summary>
		/// <remarks>q * I = I * q = q, where q is a quaternion and I is the identity quaternion.</remarks>
		public static readonly Quaternion Identity = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);

		/// <summary>
		/// A quaternion with all components set to 0.
		/// </summary>
		public static readonly Quaternion Zero = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);

		#endregion
		
		#endregion

		#region Properties

		#region Instance

		/// <summary>
		/// Checks whether all components of the quaternion are 0.
		/// </summary>
		public bool IsZero
		{
			get { return this.W == 0f && this.XYZ == Vector3.Zero; }
		}

		/// <summary>
		/// Checks whether this is the identity quaternion.
		/// </summary>
		public bool IsIdentity
		{
			get { return this.W == 1f && this.XYZ == Vector3.Zero; }
		}

		/// <summary>
		/// Checks whether the quaternion is normalized, i.e. its norm is 1.
		/// </summary>
		public bool IsNormalized
		{
			get { return Utility.Absolute(this.NormSquared - 1f) <= 0.0001f; }
		}

		/// <summary>
		/// Returns the euclidian norm |q| of the quaternion.
		/// </summary>
		/// <remarks>
		/// <para>
		/// As we work with unit-quaternions here,
		/// the norm should usually be 1. If not, use Normalize() to normalize the quaternion.
		/// </para>
		/// <para>
		/// Note that the computation of the norm is quite expensive, 
		/// as we use the square root for this.
		/// </para>
		/// <para>To compare the norms of two quaternions use NormSquared instead.</para>
		/// <para>|q| = Sqrt(w² + x² + y² + z²).</para>
		/// </remarks>
		public float Norm
		{
			get { return Utility.Sqrt(this.W * this.W + this.X * this.X + this.Y * this.Y + this.Z * this.Z); }
		}

		/// <summary>
		/// Returns the squared euclidian norm |q|² of the quaternion.
		/// </summary>
		/// <remarks>
		/// <para>
		/// As we work with unit-quaternions here,
		/// the squared norm should usually be 1 (in fact the norm should be, but as 1² = 1, the squared norm is the same). 
		/// If not, use Normalize() to normalize the quaternion.
		/// </para>
		/// <para>
		/// The norm of the quaternion is the square root of this, but as square roots are quite expensive, use this
		/// to compare the norms of two quaternions.
		/// </para>
		/// <para>|p|² = w² + x² + y² + z².</para>
		/// </remarks>
		public float NormSquared
		{
			get { return this.W * this.W + this.X * this.X + this.Y * this.Y + this.Z * this.Z; }
		}

		/// <summary>
		/// Returns the local or relative x-axis of the quaternion.
		/// </summary>
		public Vector3 XAxis
		{
			get
			{
				Vector3 result;
				result.X = 1f - (2f * this.Y * this.Y + 2f * this.Z * this.Z);
				result.Y = 2f * this.X * this.Y + 2f * this.Z * this.W;
				result.Z = 2f * this.X * this.Z - 2f * this.Y * this.W;
				return result;
			}
		}

		/// <summary>
		/// Returns the local or relative y-axis of the quaternion.
		/// </summary>
		public Vector3 YAxis
		{
			get
			{
				Vector3 result;
				result.X = 2f * this.X * this.Y - 2f * this.Z * this.W;
				result.Y = 1f - (2f * this.X * this.X + 2f * this.Z * this.Z);
				result.Z = 2f * this.Y * this.Z + 2f * this.X * this.W;
				return result;
			}
		}

		/// <summary>
		/// Returns the local or relative z-axis of the quaternion.
		/// </summary>
		public Vector3 ZAxis
		{
			get
			{
				Vector3 result;
				result.X = 2f * this.X * this.Z + 2f * this.Y * this.W;
				result.Y = 2f * this.Y * this.Z - 2f * this.X * this.W;
				result.Z = 1f - (2f * this.X * this.X + 2f * this.Y * this.Y);
				return result;
			}
		}

		/// <summary>
		/// <para>Gets or sets the rotation of the quaternion around the x-axis in radians.</para>
		/// <para>The yaw value is part of the euler-angle representation of this rotation.</para>
		/// <para>
		/// As there are several ways to perform the same rotation with rotations around several axis,
		/// this is just one possible rotation representation.
		/// </para>
		/// </summary>
		public float Yaw
		{
			get
			{
				float pole = this.X * this.Y + this.Z * this.W;
				if (Utility.Absolute(pole - 0.5f) <= float.Epsilon)
					return 2.0f * Utility.Atan2(this.X, this.W);
				if (Utility.Absolute(pole + 0.5f) <= float.Epsilon)
					return -2.0f * Utility.Atan2(this.X, this.W);
				return Utility.Atan2(2.0f * (this.W * this.Y + this.X * this.Y), 1.0f - 2.0f * (this.Y * this.Y + this.Z * this.Z));
			}
			set { this = Quaternion.FromEulerAngles(value, this.Pitch, this.Roll); }
		}

		/// <summary>
		/// <para>Gets or sets the rotation of the quaternion around the y-axis in radians.</para>
		/// <para>The pitch value is part of the euler-angle representation of this rotation.</para>
		/// <para>
		/// As there are several ways to perform the same rotation with rotations around several axis,
		/// this is just one possible rotation representation.
		/// </para>
		/// </summary>
		public float Pitch
		{
			get { return Utility.Asin(2.0f * (this.X * this.Y + this.Z * this.W)); }
			set { this = Quaternion.FromEulerAngles(this.Yaw, value, this.Roll); }
		}

		/// <summary>
		/// <para>Gets or sets the rotation of the quaternion around the z-axis in radians.</para>
		/// <para>The roll value is part of the euler-angle representation of this rotation.</para>
		/// <para>
		/// As there are several ways to perform the same rotation with rotations around several axis,
		/// this is just one possible rotation representation.
		/// </para>
		/// </summary>
		public float Roll
		{
			get
			{
				float pole = this.X * this.Y + this.Z * this.W;
				if (Utility.Absolute(pole - 0.5f) <= float.Epsilon || Utility.Absolute(pole + 0.5f) <= float.Epsilon)
					return 0.0f;	
				return Utility.Atan2(2.0f * (this.W * this.X + this.Y * this.Z), 1.0f - 2.0f * (this.X * this.X + this.Z * this.Z));
			}
			set { this = Quaternion.FromEulerAngles(this.Yaw, this.Pitch, value); }
		}

		/// <summary>
		/// <para>Gets or sets an array containg the quaternion's values.</para>
		/// <para>{W, X, Y, Z}</para>
		/// </summary>
		public float[] Array
		{
			get { return new[] {this.W, this.X, this.Y, this.Z}; }
			set
			{
				for (int i = 0; i < value.Length; i++)
				{
					if (i > 3) return;
					this[i] = value[i];
				}
			}
		}

		/// <summary>
		/// Gets or sets the component at the given index.
		/// </summary>
		/// <param name="index">Index 0 for W, 1 for X, 2 for Y and 3 for Z.</param>
		/// <returns>The value of the component at the given index.</returns>
		public float this[int index]
		{
			get
			{
				if (index == 0) return this.W;
				if (index == 1) return this.X;
				if (index == 2) return this.Y;
				if (index == 3) return this.Z;
				throw new ArgumentOutOfRangeException();
			}
			set
			{
				if (index == 0) this.W = value;
				else if (index == 1) this.X = value;
				else if (index == 2) this.Y = value;
				else if (index == 3) this.Z = value;
				else throw new ArgumentOutOfRangeException();
			}
		}

		/// <summary>
		/// Gets or sets a three dimensional vector containing the imaginary part of the quaternion.
		/// </summary>
		public Vector3 XYZ
		{
			get 
			{
				Vector3 result;
				result.X = this.X;
				result.Y = this.Y;
				result.Z = this.Z;
				return result;
			}
			set
			{
				this.X = value.X;
				this.Y = value.Y;
				this.Z = value.Z;
			}
		}

		/// <summary>
		/// Gets or sets a four dimensional vector containing the components of the quaternion.
		/// </summary>
		public Vector4 XYZW
		{
			get 
			{
				Vector4 result;
				result.X = this.X;
				result.Y = this.Y;
				result.Z = this.Z;
				result.W = this.W;
				return result;
			}
			set
			{
				this.X = value.X;
				this.Y = value.Y;
				this.Z = value.Z;
				this.W = value.W;
			}
		}

		#endregion

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a quaternion.
		/// </summary>
		/// <param name="w">The w-component of the quaternion.</param>
		/// <param name="x">The x-component of the quaternion.</param>
		/// <param name="y">The y-component of the quaternion.</param>
		/// <param name="z">The z-component of the quaternion.</param>
		public Quaternion(float x, float y, float z, float w)
		{
			this.W = w;
			this.X = x;
			this.Y = y;
			this.Z = z;
		}

		/// <summary>
		/// Creates a quaternion.
		/// </summary>
		/// <param name="q">The w- and real component of the quaternion.</param>
		/// <param name="p">The imaginary components x, y and z of the quaternion.</param>
		public Quaternion(Vector3 p, float q)
		{
			this.W = q;
			this.X = p.X;
			this.Y = p.Y;
			this.Z = p.Z;
		}

		/// <summary>
		/// Creates a quaternion.
		/// </summary>
		/// <param name="xyzw">The components of the quaternion, stored in a four-vector.</param>
		public Quaternion(Vector4 xyzw)
		{
			this.W = xyzw.W;
			this.X = xyzw.X;
			this.Y = xyzw.Y;
			this.Z = xyzw.Z;
		}

		/// <summary>
		/// Creates a quaternion.
		/// </summary>
		/// <param name="q">The components of the quaternion, stored in another quaternion.</param>
		public Quaternion(Quaternion q)
		{
			this.W = q.W;
			this.X = q.X;
			this.Y = q.Y;
			this.Z = q.Z;
		}

		/// <summary>
		/// Creates a quaternion.
		/// </summary>
		/// <param name="values">The components of the quaternion, stored in an array. {W, X, Y, Z}</param>
		public Quaternion(float[] values)
		{
			if (values.Length > 0) this.W = values[0];
			else this.W = 1f;
			if (values.Length > 1) this.X = values[1];
			else this.X = 0f;
			if (values.Length > 2) this.Y = values[2];
			else this.Y = 0f;
			if (values.Length > 3) this.Z = values[3];
			else this.Z = 0f;
		}

		#endregion

		#region Methods

		#region Static

		/// <summary>
		/// Builds a quaternion representing the rotation around the given axis about the given angle.
		/// </summary>
		/// <param name="angle">The angle to rotate about in radians.</param>
		/// <param name="axis">The axis to rotate around. This should be normalized vector.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void FromAxisAngle(ref Vector3 axis, float angle, out Quaternion result)
		{
			float sin = Utility.Sin(angle * 0.5f);
			result.X = axis.X * sin;
			result.Y = axis.Y * sin;
			result.Z = axis.Z * sin;
			result.W = Utility.Cos(angle * 0.5f);
		}

		/// <summary>
		/// Builds a quaternion representing the rotation around the given axis about the given angle.
		/// </summary>
		/// <param name="angle">The angle to rotate about in radians.</param>
		/// <param name="axis">The axis to rotate around. This should be normalized vector.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void FromAxisAngle(Vector3 axis, float angle, out Quaternion result)
		{
			float sin = Utility.Sin(angle * 0.5f);
			result.X = axis.X * sin;
			result.Y = axis.Y * sin;
			result.Z = axis.Z * sin;
			result.W = Utility.Cos(angle * 0.5f);
		}

		/// <summary>
		/// Builds a quaternion representing the rotation around the given axis about the given angle.
		/// </summary>
		/// <param name="angle">The angle to rotate about in radians.</param>
		/// <param name="axis">The axis to rotate around. This should be a normalized vector.</param>
		/// <returns>The quaternion representation of the axis-angle rotation.</returns>
		public static Quaternion FromAxisAngle(Vector3 axis, float angle)
		{
			Quaternion result;
			Quaternion.FromAxisAngle(axis, angle, out result);
			return result;
		}

		/// <summary>
		/// Builds a quaternion representing the rotation around the given axis about the given angle.
		/// </summary>
		/// <param name="angle">The angle to rotate about in radians.</param>
		/// <param name="x">The x-component of the axis to rotate around.</param>
		/// <param name="y">The y-component of the axis to rotate around.</param>
		/// <param name="z">The z-component of the axis to rotate around.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void FromAxisAngle(float x, float y, float z, float angle, out Quaternion result)
		{
			float sin = Utility.Sin(angle * 0.5f);
			result.X = x * sin;
			result.Y = y * sin;
			result.Z = z * sin;
			result.W = Utility.Cos(angle * 0.5f);
		}

		/// <summary>
		/// Builds a quaternion representing the rotation around the given axis about the given angle.
		/// </summary>
		/// <param name="angle">The angle to rotate about in radians.</param>
		/// <param name="x">The x-component of the axis to rotate around.</param>
		/// <param name="y">The y-component of the axis to rotate around.</param>
		/// <param name="z">The z-component of the axis to rotate around.</param>
		/// <returns>The quaternion representation of the axis-angle rotation.</returns>
		public static Quaternion FromAxisAngle(float x, float y, float z, float angle)
		{
			Quaternion result;
			Quaternion.FromAxisAngle(angle, x, y, z, out result);
			return result;
		}

		/// <summary>
		/// Converts a direction vector to a quaternion
		/// </summary>
		/// <param name="direction">The direction.</param>
		/// <param name="up">The up vector.</param>
		/// <param name="result">The resutling quaternion.</param>
		/// <returns></returns>
		public static void FromDirection(ref Vector3 direction, ref Vector3 up, out Quaternion result)
		{
			Vector3 right;
			Vector3.Cross(in up, in direction, out right);
			Vector3 realUp;
			Vector3.Cross(in direction, in right, out realUp);
			result.W = Utility.Sqrt(1.0f + right.X + realUp.Y + direction.Z) * 0.5f;
			float scale = 1.0f / (result.W * 4.0f);
			result.X = ((direction.Y - realUp.Y) * scale);
			result.Y = ((right.Z - direction.X) * scale);
			result.Z = ((realUp.X - right.Y) * scale);
		}


		/// <summary>
		/// Converts a direction vector to a quaternion
		/// </summary>
		/// <param name="direction">The direction.</param>
		/// <param name="up">The up vector.</param>
		/// <param name="result">The resutling quaternion.</param>
		/// <returns></returns>
		public static void FromDirection(Vector3 direction, Vector3 up, out Quaternion result)
		{
			Vector3 right;
			Vector3.Cross(in up, in direction, out right);
			Vector3 realUp;
			Vector3.Cross(in direction, in right, out realUp);
			result.W = Utility.Sqrt(1.0f + right.X + realUp.Y + direction.Z) * 0.5f;
			float scale = 1.0f / (result.W * 4.0f);
			result.X = ((direction.Y - realUp.Y) * scale);
			result.Y = ((right.Z - direction.X) * scale);
			result.Z = ((realUp.X - right.Y) * scale);
		}

		/// <summary>
		/// Converts a direction vector to a quaternion
		/// </summary>
		/// <param name="direction">The direction.</param>
		/// <param name="up">The up vector.</param>
		/// <returns>The resutling quaternion.</returns>
		public static Quaternion FromDirection(ref Vector3 direction, ref Vector3 up)
		{
			Quaternion result;
			Vector3 right;
			Vector3.Cross(in up, in direction, out right);
			Vector3 realUp;
			Vector3.Cross(in direction, in right, out realUp);
			result.W = Utility.Sqrt(1.0f + right.X + realUp.Y + direction.Z) * 0.5f;
			float scale = 1.0f / (result.W * 4.0f);
			result.X = ((direction.Y - realUp.Y) * scale);
			result.Y = ((right.Z - direction.X) * scale);
			result.Z = ((realUp.X - right.Y) * scale);
			return result;
		}

		/// <summary>
		/// Converts a direction vector to a quaternion
		/// </summary>
		/// <param name="direction">The direction.</param>
		/// <param name="up">The up vector.</param>
		/// <returns>The resutling quaternion.</returns>
		public static Quaternion FromDirection(Vector3 direction, Vector3 up)
		{
			Quaternion result;
			Vector3 right;
			Vector3.Cross(in up, in direction, out right);
			Vector3 realUp;
			Vector3.Cross(in direction, in right, out realUp);
			result.W = Utility.Sqrt(1.0f + right.X + realUp.Y + direction.Z) * 0.5f;
			float scale = 1.0f / (result.W * 4.0f);
			result.X = ((direction.Y - realUp.Y) * scale);
			result.Y = ((right.Z - direction.X) * scale);
			result.Z = ((realUp.X - right.Y) * scale);
			return result;
		}

		#region FromMatrix3

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the 3x3 rotation matrix.
		/// </summary>
		/// <param name="matrix">The 3x3 rotation matrix.</param>
		/// <param name="result">The quaternion representation of the matrix3 rotation.</param>
		public static void FromMatrix(ref Matrix3x3 matrix, out Quaternion result)
		 {
			float scale = matrix.M11 + matrix.M22 + matrix.M33;

			if (scale > 0.0f)
			{
				float sqrt = Utility.Sqrt(scale + 1.0f);
				float halfReciprocal = 0.5f / sqrt;

				result.X = (matrix.M32 - matrix.M23) * halfReciprocal;
				result.Y = (matrix.M13 - matrix.M31) * halfReciprocal;
				result.Z = (matrix.M21 - matrix.M12) * halfReciprocal;
				result.W = 0.5f * sqrt;
			}
			else if (matrix.M11 >= matrix.M22 && matrix.M11 >= matrix.M33)
			{
				float sqrt = (float)(System.Math.Sqrt(1.0f + matrix.M11 - matrix.M22 - matrix.M33));
				float halfReciprocal = 0.5f / sqrt;

				result.X = 0.5f * sqrt;
				result.Y = (matrix.M12 + matrix.M21) * halfReciprocal;
				result.Z = (matrix.M13 + matrix.M31) * halfReciprocal;
				result.W = (matrix.M32 - matrix.M23) * halfReciprocal;
			}
			else if (matrix.M22 > matrix.M33)
			{
				float sqrt = (float)(System.Math.Sqrt(1.0f + matrix.M22 - matrix.M11 - matrix.M33));
				float halfReciprocal = 0.5f / sqrt;

				result.X = (matrix.M21 + matrix.M12) * halfReciprocal;
				result.Y = 0.5f * sqrt;
				result.Z = (matrix.M32 + matrix.M23) * halfReciprocal;
				result.W = (matrix.M13 - matrix.M31) * halfReciprocal;
			}
			else
			{
				float sqrt = (float)(System.Math.Sqrt(1.0f + matrix.M33 - matrix.M11 - matrix.M22));
				float halfReciprocal = 0.5f / sqrt;

				result.X = (matrix.M31 + matrix.M13) * halfReciprocal;
				result.Y = (matrix.M32 + matrix.M23) * halfReciprocal;
				result.Z = 0.5f * sqrt;
				result.W = (matrix.M21 - matrix.M12) * halfReciprocal;
			}
		}

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the 3x3 rotation matrix.
		/// </summary>
		/// <param name="matrix">The 3x3 rotation matrix.</param>
		/// <returns>The quaternion representation of the rotation matrix.</returns>
		public static Quaternion FromMatrix(ref Matrix3x3 matrix)
		{
			Quaternion result;
			Quaternion.FromMatrix(ref matrix, out result);
			return result;
		}

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the 3x3 rotation matrix. It is recommended to use the method with the reference parameter for performance reasons.
		/// </summary>
		/// <param name="matrix">The 3x3 rotation matrix.</param>
		/// <returns>The quaternion representation of the rotation matrix.</returns>
		public static Quaternion FromMatrix(Matrix3x3 matrix)
		{
			Quaternion result;
			Quaternion.FromMatrix(ref matrix, out result);
			return result;
		}

		#endregion

		#region FromMatrix4

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the 4x4 transformation matrix.
		/// </summary>
		/// <param name="matrix">The 4x4 transformation matrix.</param>
		/// <param name="result">The quaternion representation of the matrix4 rotation.</param>
		public static void FromMatrix(ref Matrix4x4 matrix, out Quaternion result)
		{
			float scale = matrix.M11 + matrix.M22 + matrix.M33;

			if (scale > 0.0f)
			{
				float sqrt = Utility.Sqrt(scale + 1.0f);
				float halfReciprocal = 0.5f / sqrt;

				result.X = (matrix.M32 - matrix.M23) * halfReciprocal;
				result.Y = (matrix.M13 - matrix.M31) * halfReciprocal;
				result.Z = (matrix.M21 - matrix.M12) * halfReciprocal;
				result.W = 0.5f * sqrt;
			}
			else if (matrix.M11 >= matrix.M22 && matrix.M11 >= matrix.M33)
			{
				float sqrt = (float)(System.Math.Sqrt(1.0f + matrix.M11 - matrix.M22 - matrix.M33));
				float halfReciprocal = 0.5f / sqrt;

				result.X = 0.5f * sqrt;
				result.Y = (matrix.M12 + matrix.M21) * halfReciprocal;
				result.Z = (matrix.M13 + matrix.M31) * halfReciprocal;
				result.W = (matrix.M32 - matrix.M23) * halfReciprocal;
			}
			else if (matrix.M22 > matrix.M33)
			{
				float sqrt = (float)(System.Math.Sqrt(1.0f + matrix.M22 - matrix.M11 - matrix.M33));
				float halfReciprocal = 0.5f / sqrt;

				result.X = (matrix.M21 + matrix.M12) * halfReciprocal;
				result.Y = 0.5f * sqrt;
				result.Z = (matrix.M32 + matrix.M23) * halfReciprocal;
				result.W = (matrix.M13 - matrix.M31) * halfReciprocal;
			}
			else
			{
				float sqrt = (float)(System.Math.Sqrt(1.0f + matrix.M33 - matrix.M11 - matrix.M22));
				float halfReciprocal = 0.5f / sqrt;

				result.X = (matrix.M31 + matrix.M13) * halfReciprocal;
				result.Y = (matrix.M32 + matrix.M23) * halfReciprocal;
				result.Z = 0.5f * sqrt;
				result.W = (matrix.M21 - matrix.M12) * halfReciprocal;
			}
		}

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the 4x4 transformation matrix.
		/// </summary>
		/// <param name="matrix">The transformation matrix.</param>
		/// <returns>The quaternion representation of the transformation matrix rotation.</returns>
		public static Quaternion FromMatrix(ref Matrix4x4 matrix)
		{
			Quaternion result;
			Quaternion.FromMatrix(ref matrix, out result);
			return result;
		}

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the 4x4 transformation matrix. It is recommended to use the method with the reference parameter for performance reasons.
		/// </summary>
		/// <param name="matrix">The transformation matrix.</param>
		/// <returns>The quaternion representation of the transformation matrix rotation.</returns>
		public static Quaternion FromMatrix(Matrix4x4 matrix)
		{
			Quaternion result;
			Quaternion.FromMatrix(ref matrix, out result);
			return result;
		}

		#endregion

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the values around the x-, y- and z-axis.
		/// </summary>
		/// <param name="yaw">The rotation around the x-axis in radians.</param>
		/// <param name="pitch">The rotation around the y-axis in radians.</param>
		/// <param name="roll">The rotation around the z-axis in radians.</param>
		/// <returns>The quaternion representation of the euler angle rotation.</returns>
		public static Quaternion FromEulerAngles(float yaw, float pitch, float roll)
		{
			Quaternion result;

			yaw *= 0.5f;
			pitch *= 0.5f;
			roll *= 0.5f;

			float c1 = Utility.Cos(yaw);
			float s1 = Utility.Sin(yaw);
			float c2 = Utility.Cos(pitch);
			float s2 = Utility.Sin(pitch);
			float c3 = Utility.Cos(roll);
			float s3 = Utility.Sin(roll);
			
			float c1c2 = c1 * c2;
			float s1s2 = s1 * s2;

			result.W = c1c2 * c3 - s1s2 * s3;
			result.X = c1c2 * s3 + s1s2 * c3;
			result.Y = s1 * c2 * c3 + c1 * s2 * s3;
			result.Z = c1 * s2 * c3 - s1 * c2 * s3;

			return result;
		}

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the values around the x-, y- and z-axis.
		/// </summary>
		/// <param name="angles">The euler angles.</param>
		/// <returns>The quaternion representation of the euler angle rotation.</returns>
		public static Quaternion FromEulerAngles(Vector3 angles)
		{
			Quaternion result;

			angles *= 0.5f;

			float c1 = Utility.Cos(angles.Y); // rotation around Y is yaw
			float s1 = Utility.Sin(angles.Y);
			float c2 = Utility.Cos(angles.Z); // rotation around Z is pitch
			float s2 = Utility.Sin(angles.Z);
			float c3 = Utility.Cos(angles.X); // rotaiton around X is roll
			float s3 = Utility.Sin(angles.X);

			float c1c2 = c1 * c2;
			float s1s2 = s1 * s2;

			result.W = c1c2 * c3 - s1s2 * s3;
			result.X = c1c2 * s3 + s1s2 * c3;
			result.Y = s1 * c2 * c3 + c1 * s2 * s3;
			result.Z = c1 * s2 * c3 - s1 * c2 * s3;

			return result;
		}

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the values around the x-, y- and z-axis.
		/// </summary>
		/// <param name="yaw">The rotation around the x-axis in radians.</param>
		/// <param name="pitch">The rotation around the y-axis in radians.</param>
		/// <param name="roll">The rotation around the z-axis in radians.</param>
		/// <param name="result">The quaternion representation of the euler angle rotation.</param>
		public static void FromEulerAngles(float yaw, float pitch, float roll, out Quaternion result)
		{
			yaw *= 0.5f;
			pitch *= 0.5f;
			roll *= 0.5f;

			float c1 = Utility.Cos(yaw);
			float s1 = Utility.Sin(yaw);
			float c2 = Utility.Cos(pitch);
			float s2 = Utility.Sin(pitch);
			float c3 = Utility.Cos(roll);
			float s3 = Utility.Sin(roll);

			float c1c2 = c1 * c2;
			float s1s2 = s1 * s2;

			result.W = c1c2 * c3 - s1s2 * s3;
			result.X = c1c2 * s3 + s1s2 * c3;
			result.Y = s1 * c2 * c3 + c1 * s2 * s3;
			result.Z = c1 * s2 * c3 - s1 * c2 * s3;
		}

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the values around the x-, y- and z-axis.
		/// </summary>
		/// <param name="angles">The euler angles.</param>
		/// <param name="result">The quaternion representation of the euler angle rotation.</param>
		public static void FromEulerAngles(ref Vector3 angles, out Quaternion result)
		{
			angles *= 0.5f;

			float c1 = Utility.Cos(angles.Y); // rotation around Y is yaw
			float s1 = Utility.Sin(angles.Y);
			float c2 = Utility.Cos(angles.Z); // rotation around Z is pitch
			float s2 = Utility.Sin(angles.Z);
			float c3 = Utility.Cos(angles.X); // rotaiton around X is roll
			float s3 = Utility.Sin(angles.X);

			float c1c2 = c1 * c2;
			float s1s2 = s1 * s2;

			result.W = c1c2 * c3 - s1s2 * s3;
			result.X = c1c2 * s3 + s1s2 * c3;
			result.Y = s1 * c2 * c3 + c1 * s2 * s3;
			result.Z = c1 * s2 * c3 - s1 * c2 * s3;
		}

		/// <summary>
		/// Builds a quaternion representing the rotation performed by the values around the x-, y- and z-axis.
		/// </summary>
		/// <param name="angles">The euler angles.</param>
		/// <param name="result">The quaternion representation of the euler angle rotation.</param>
		public static void FromEulerAngles(Vector3 angles, out Quaternion result)
		{
			angles *= 0.5f;

			float c1 = Utility.Cos(angles.Y); // rotation around Y is yaw
			float s1 = Utility.Sin(angles.Y);
			float c2 = Utility.Cos(angles.Z); // rotation around Z is pitch
			float s2 = Utility.Sin(angles.Z);
			float c3 = Utility.Cos(angles.X); // rotaiton around X is roll
			float s3 = Utility.Sin(angles.X);

			float c1c2 = c1 * c2;
			float s1s2 = s1 * s2;

			result.W = c1c2 * c3 - s1s2 * s3;
			result.X = c1c2 * s3 + s1s2 * c3;
			result.Y = s1 * c2 * c3 + c1 * s2 * s3;
			result.Z = c1 * s2 * c3 - s1 * c2 * s3;
		}

		/// <summary>
		/// Creates a quaternion that "looks" into the specified direction.
		/// </summary>
		/// <param name="eye">The position of the eye.</param>
		/// <param name="dest">The point to look at.</param>
		/// <param name="up">The up vector.</param>
		/// <returns>The 'lookat' quaternion.</returns>
		public static Quaternion LookAt(Vector3 eye, Vector3 dest, Vector3 up)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// <para>Returns the inverse of the quaternion.</para>
		/// <para>The inverse of a quaternion represents the inverted rotation of the quaternion.</para>
		/// <para>q * q^-1 = I, where q^-1 is the inverse of the quaternion q and I the identity quaternion.</para>
		/// <para>For normalizied quaternion q^-1 = q', where q' is the conjugate.</para>
		/// </summary>
		public static void Invert(ref Quaternion quaternion, out Quaternion result)
		{
			float norm = quaternion.NormSquared;
			//if (norm > Single.Epsilon)
			//{
			norm = 1.0f / norm;
			result.X = -quaternion.X * norm;
			result.Y = -quaternion.Y * norm;
			result.Z = -quaternion.Z * norm;
			result.W = quaternion.W * norm;
			//}
			//else
			//{
			//	result = quaternion;
			//}
		}

		/// <summary>
		/// <para>Returns the inverse of the quaternion.</para>
		/// <para>The inverse of a quaternion represents the inverted rotation of the quaternion.</para>
		/// <para>q * q^-1 = I, where q^-1 is the inverse of the quaternion q and I the identity quaternion.</para>
		/// <para>For normalizied quaternion q^-1 = q', where q' is the conjugate.</para>
		/// </summary>
		public static void Invert(Quaternion quaternion, out Quaternion result)
		{
			float norm = quaternion.NormSquared;
			//if (norm > Single.Epsilon)
			//{
			norm = 1.0f / norm;
			result.X = -quaternion.X * norm;
			result.Y = -quaternion.Y * norm;
			result.Z = -quaternion.Z * norm;
			result.W = quaternion.W * norm;
			//}
			//else
			//{
			//	result = quaternion;
			//}
		}

		/// <summary>
		/// Returns the inverse of the quaternion.
		/// This is a faster method than <see cref="Invert"/> because it assumes that the quaternion is normalized.
		/// Do not use this method if you're working with potentially unnormalized quaternions.
		/// </summary>
		/// <returns>The inverse of this quaternion.</returns>
		public static void InvertFast(Quaternion quaternion, out Quaternion result)
		{
			result.X = -quaternion.X;
			result.Y = -quaternion.Y;
			result.Z = -quaternion.Z;
			result.W = quaternion.W;
		}

		public static void ToAxisAngle(ref Quaternion quaternion, out Vector3 axis, out float angle)
		{
			Quaternion qn;
			Quaternion.Normalize(ref quaternion, out qn);
			angle = 2.0f * Utility.Acos(qn.W);
			float s = Utility.Sqrt(1.0f - qn.W * qn.W);

			if (s < 0.001f)
			{
				axis.X = qn.X;
				axis.Y = qn.Y;
				axis.Z = qn.Z;
			}
			else
			{
				axis.X = qn.X / s;
				axis.Y = qn.Y / s;
				axis.Z = qn.Z / s;
			}
		}


		private static Quaternion rotX = Quaternion.FromAxisAngle(Vector3.UnitX, Constants.Pi * 0.5f);
		private static Quaternion rotY = Quaternion.FromAxisAngle(Vector3.UnitY, Constants.Pi * 0.5f);

		/// <summary>
		/// Gets the rotation around the specified axis performed by this quaternion.
		/// </summary>
		/// <param name="q"></param>
		/// <param name="axis"></param>
		/// <returns></returns>
		public static float GetAxisAngle(ref Quaternion q, ref Vector3 axis)
		{	// http://stackoverflow.com/questions/3684269/component-of-a-quaternion-rotation-around-an-axis
			Vector3.Normalize(in axis, out axis);
			Vector3 orthonormal1, orthonormal2;
			
			// get the plane the axis is a normal of
			Vector3 w;
			Quaternion.Rotate(ref rotX, ref axis, out w);
			float dot = Vector3.Dot(axis, w);
			if (System.Math.Abs(dot) > 0.6f)
				Quaternion.Rotate(ref rotY, ref axis, out w);
			Vector3.Normalize(in w, out w);

			Vector3.Cross(in axis, in w, out orthonormal1);
			Vector3.Normalize(in orthonormal1, out orthonormal1);
			Vector3.Cross(in axis, in orthonormal1, out orthonormal2);
			Vector3.Normalize(in orthonormal2, out orthonormal2);

			Vector3 transformed;
			Quaternion.Rotate(ref q, ref orthonormal1, out transformed);

			// project transformed vector onto plane
			Vector3 flattened = transformed - (Vector3.Dot(transformed, axis) * axis);
			Vector3.Normalize(in flattened, out flattened);

			// get angle between original vector and projected transform to get angle around normal
			return (float)System.Math.Acos(Vector3.Dot(orthonormal1, flattened));
		}

		/// <summary>
		/// Gets the rotation around the specified axis performed by this quaternion.
		/// </summary>
		/// <param name="q"></param>
		/// <param name="axis"></param>
		/// <returns></returns>
		public static float GetAxisAngle(Quaternion q, Vector3 axis)
		{
			return GetAxisAngle(ref q, ref axis);
		}

		/// <summary>
		/// Adds the two quaternions.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void Add(ref Quaternion left, ref Quaternion right, out Quaternion result)
		{
			result.W = left.W + right.W;
			result.X = left.X + right.X;
			result.Y = left.Y + right.Y;
			result.Z = left.Z + right.Z;
		}

		/// <summary>
		/// Substracts the two quaternions.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void Subtract(ref Quaternion left, ref Quaternion right, out Quaternion result)
		{
			result.W = left.W - right.W;
			result.X = left.X - right.X;
			result.Y = left.Y - right.Y;
			result.Z = left.Z - right.Z;
		}

		/// <summary>
		/// <para>Multiplicates the two quaternions.</para>
		/// <para>The product of two quaternions is the concatenated rotation of two quaternions.</para>
		/// <para>
		/// Quaternion multiplication isn't commutative, so the product represents the 
		/// rotation of the left quaternion, followed by the rotation of the right quaternion.
		/// </para>
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void Multiply(ref Quaternion left, ref Quaternion right, out Quaternion result)
		{
			float w, x, y;
			w = left.W * right.W - left.X * right.X - left.Y * right.Y - left.Z * right.Z;
			x = left.W * right.X + left.X * right.W + left.Y * right.Z - left.Z * right.Y;
			y = left.W * right.Y + left.Y * right.W + left.Z * right.X - left.X * right.Z;
			result.Z = left.W * right.Z + left.Z * right.W + left.X * right.Y - left.Y * right.X;
			result.W = w;
			result.X = x;
			result.Y = y;
		}

		/// <summary>
		/// Adds the scalar to the quaternion.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void Add(ref Quaternion q, float scalar, out Quaternion result)
		{
			result.W = q.W + scalar;
			result.X = q.X + scalar;
			result.Y = q.Y + scalar;
			result.Z = q.Z + scalar;
		}

		/// <summary>
		/// Multiplicates the scalar to the quaternion.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void Multiply(ref Quaternion q, float scalar, out Quaternion result)
		{
			result.W = q.W * scalar;
			result.X = q.X * scalar;
			result.Y = q.Y * scalar;
			result.Z = q.Z * scalar;
		}

		/// <summary>
		/// Substracts the scalar from the quaternion.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void Subtract(Quaternion q, float scalar, out Quaternion result)
		{
			result.W = q.W - scalar;
			result.X = q.X - scalar;
			result.Y = q.Y - scalar;
			result.Z = q.Z - scalar;
		}

		/// <summary>
		/// Divides the quaternion by the scalar.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void Divide(ref Quaternion q, float scalar, out Quaternion result)
		{
			result.W = q.W / scalar;
			result.X = q.X / scalar;
			result.Y = q.Y / scalar;
			result.Z = q.Z / scalar;
		}

		/// <summary>
		/// <para>Negates the quaternion.</para>
		/// <para>The negated quaternion represents the same rotation as the original quaternion.</para>
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="result">The resulting quaternion.</param>
		public static void Negate(ref Quaternion q, out Quaternion result)
		{
			result.W = -q.W;
			result.X = -q.X;
			result.Y = -q.Y;
			result.Z = -q.Z;
		}

		/// <summary>
		/// <para>Rotates the vector v by the quaternion q.</para>
		/// <para>We use vector rotation in the form of v' = q*v*q^-1.</para>
		/// </summary>
		/// <param name="q">The rotation as a quaternion.</param>
		/// <param name="v">The vector to be rotated.</param>
		/// <param name="result">The resulting vector.</param>
		public static void Rotate(ref Quaternion q, ref Vector3 v, out Vector3 result)
		{
			// thanks to ryg / farbrausch
			Vector3 temp;

			temp.X = q.Y * v.Z - q.Z * v.Y + q.W * v.X;
			temp.Y = q.Z * v.X - q.X * v.Z + q.W * v.Y;
			temp.Z = q.X * v.Y - q.Y * v.X + q.W * v.Z;

			result.X = v.X + 2.0f * (q.Y * temp.Z - q.Z * temp.Y);
			result.Y = v.Y + 2.0f * (q.Z * temp.X - q.X * temp.Z);
			result.Z = v.Z + 2.0f * (q.X * temp.Y - q.Y * temp.X);
		}

		/// <summary>
		/// <para>Rotates the vector v by the quaternion q.</para>
		/// <para>We use vector rotation in the form of v' = q*v*q^-1.</para>
		/// </summary>
		/// <param name="q">The rotation as a quaternion.</param>
		/// <param name="v">The vector to be rotated.</param>
		/// <param name="result">The resulting vector.</param>
		public static void Rotate(Quaternion q, Vector3 v, out Vector3 result)
		{
			// thanks to ryg / farbrausch
			Vector3 temp;

			temp.X = q.Y * v.Z - q.Z * v.Y + q.W * v.X;
			temp.Y = q.Z * v.X - q.X * v.Z + q.W * v.Y;
			temp.Z = q.X * v.Y - q.Y * v.X + q.W * v.Z;

			result.X = v.X + 2.0f * (q.Y * temp.Z - q.Z * temp.Y);
			result.Y = v.Y + 2.0f * (q.Z * temp.X - q.X * temp.Z);
			result.Z = v.Z + 2.0f * (q.X * temp.Y - q.Y * temp.X);
		}


		/// <summary>
		/// Rotates the vector v by the inverse quaternion q^-1.
		/// </summary>
		/// <param name="q">The rotation as a quaternion.</param>
		/// <param name="v">The vector to be rotated.</param>
		/// <param name="result">The resulting vector.</param>
		public static void RotateInverse(ref Quaternion q, ref Vector3 v, out Vector3 result)
		{
			// thanks to ryg / farbrausch
			Vector3 temp;

			temp.X = q.Z * v.Y - q.Y * v.Z + q.W * v.X;
			temp.Y = q.X * v.Z - q.Z * v.X + q.W * v.Y;
			temp.Z = q.Y * v.X - q.X * v.Y + q.W * v.Z;

			result.X = v.X + 2.0f * (q.Z * temp.Y - q.Y * temp.Z);
			result.Y = v.Y + 2.0f * (q.X * temp.Z - q.Z * temp.X);
			result.Z = v.Z + 2.0f * (q.Y * temp.X - q.X * temp.Y);
		}

		/// <summary>
		/// Rotates the vector v by the inverse quaternion q^-1.
		/// </summary>
		/// <param name="q">The rotation as a quaternion.</param>
		/// <param name="v">The vector to be rotated.</param>
		/// <param name="result">The resulting vector.</param>
		public static void RotateInverse(Quaternion q, Vector3 v, out Vector3 result)
		{
			// thanks to ryg / farbrausch
			Vector3 temp;

			temp.X = q.Z * v.Y - q.Y * v.Z + q.W * v.X;
			temp.Y = q.X * v.Z - q.Z * v.X + q.W * v.Y;
			temp.Z = q.Y * v.X - q.X * v.Y + q.W * v.Z;

			result.X = v.X + 2.0f * (q.Z * temp.Y - q.Y * temp.Z);
			result.Y = v.Y + 2.0f * (q.X * temp.Z - q.Z * temp.X);
			result.Z = v.Z + 2.0f * (q.Y * temp.X - q.X * temp.Y);
		}

		/// <summary>
		/// Normalized the input quaternion and stores the result in the specified output quaternion.
		/// </summary>
		public static void Normalize(ref Quaternion q, out Quaternion result)
		{
			float norm = q.Norm;
			if (norm > float.Epsilon)
			{
				norm = 1.0f / norm;
				result.W = q.W * norm;
				result.X = q.X * norm;
				result.Y = q.Y * norm;
				result.Z = q.Z * norm;
			}
			else
			{
				result.W = q.W;
				result.X = q.X;
				result.Y = q.Y;
				result.Z = q.Z;
			}
		}

		/// <summary>
		/// Calculates spherical interpolation between two quaternions.
		/// </summary>
		/// <param name="quaternion1"></param>
		/// <param name="quaternion2"></param>
		/// <param name="amount"></param>
		public static Quaternion Slerp(Quaternion quaternion1, Quaternion quaternion2, float amount)
		{
			Quaternion result;
			Quaternion.Slerp(ref quaternion1, ref quaternion2, amount, out result);
			return result;
		}

		/// <summary>
		/// Calculates spherical interpolation between two quaternions.
		/// </summary>
		/// <param name="quaternion1"></param>
		/// <param name="quaternion2"></param>
		/// <param name="amount"></param>
		public static Quaternion Slerp(ref Quaternion quaternion1, ref Quaternion quaternion2, float amount)
		{
			Quaternion result;
			Quaternion.Slerp(ref quaternion1, ref quaternion2, amount, out result);
			return result;
		}

		/// <summary>
		/// Calculates spherical interpolation between two quaternions.
		/// </summary>
		/// <param name="quaternion1"></param>
		/// <param name="quaternion2"></param>
		/// <param name="amount"></param>
		/// <param name="result"></param>
		public static void Slerp(Quaternion quaternion1, Quaternion quaternion2, float amount, out Quaternion result)
		{
			float norm = quaternion1.X * quaternion2.X + quaternion1.Y * quaternion2.Y + quaternion1.Z * quaternion2.Z + quaternion1.W * quaternion2.W;
			float t1, t2;
			bool invert = false;

			if (norm < 0f)
			{
				invert = true;
				norm = -norm;
			}

			if (norm > 0.999999f)
			{
				t1 = 1.0f - amount;
				t2 = invert ? -amount : amount;
			}
			else
			{
				float normAcos = (float)System.Math.Acos(norm);
				float invSin = (float)(1.0d / System.Math.Sin(normAcos));
				t1 = (float)System.Math.Sin((1f - amount) * normAcos) * invSin;
				t2 = invert ?
					(float)System.Math.Sin(amount * normAcos) * -invSin :
					(float)System.Math.Sin(amount * normAcos) * invSin;
			}

			result.X = (t1 * quaternion1.X) + (t2 * quaternion2.X);
			result.Y = (t1 * quaternion1.Y) + (t2 * quaternion2.Y);
			result.Z = (t1 * quaternion1.Z) + (t2 * quaternion2.Z);
			result.W = (t1 * quaternion1.W) + (t2 * quaternion2.W);
		}

		/// <summary>
		/// Calculates spherical interpolation between two quaternions.
		/// </summary>
		/// <param name="quaternion1"></param>
		/// <param name="quaternion2"></param>
		/// <param name="amount"></param>
		/// <param name="result"></param>
		public static void Slerp(ref Quaternion quaternion1, ref Quaternion quaternion2, float amount, out Quaternion result)
		{
			float norm = quaternion1.X * quaternion2.X + quaternion1.Y * quaternion2.Y + quaternion1.Z * quaternion2.Z + quaternion1.W * quaternion2.W;
			float t1, t2;
			bool invert = false;

			if (norm < 0f)
			{
				invert = true;
				norm = -norm;
			}

			if (norm > 0.999999f)
			{
				t1 = 1.0f - amount;
				t2 = invert ? -amount : amount;
			}
			else
			{
				float normAcos = (float)System.Math.Acos(norm);
				float invSin = (float)(1.0d / System.Math.Sin(normAcos));
				t1 = (float)System.Math.Sin((1f - amount) * normAcos) * invSin;
				t2 = invert ?
					(float)System.Math.Sin(amount * normAcos) * -invSin :
					(float)System.Math.Sin(amount * normAcos) * invSin;
			}

			result.X = (t1 * quaternion1.X) + (t2 * quaternion2.X);
			result.Y = (t1 * quaternion1.Y) + (t2 * quaternion2.Y);
			result.Z = (t1 * quaternion1.Z) + (t2 * quaternion2.Z);
			result.W = (t1 * quaternion1.W) + (t2 * quaternion2.W);
		}

		public static void LookAt(ref Vector3 target, ref Vector3 up, out Quaternion result)
		{
			Matrix3x3 mat;
			Matrix3x3.CreateLookAt(ref target, ref up, out mat);
			Quaternion.FromMatrix(ref mat, out result);
		}

		public static void LookAt(Vector3 target, Vector3 up, out Quaternion result)
		{
			Matrix3x3 mat;
			Matrix3x3.CreateLookAt(ref target, ref up, out mat);
			Quaternion.FromMatrix(ref mat, out result);
		}

		public static Quaternion LookAt(ref Vector3 target, ref Vector3 up)
		{
			Quaternion result;
			Matrix3x3 mat;
			Matrix3x3.CreateLookAt(ref target, ref up, out mat);
			Quaternion.FromMatrix(ref mat, out result);
			return result;
		}

		public static Quaternion LookAt(Vector3 target, Vector3 up)
		{
			Quaternion result;
			Matrix3x3 mat;
			Matrix3x3.CreateLookAt(ref target, ref up, out mat);
			Quaternion.FromMatrix(ref mat, out result);
			return result;
		}
		
		#endregion

		#region Instance

		/// <summary>
		/// Returns the local axes. Same as Quaternion.Rotate(Vector3.Unit[X|Y|Z]).
		/// </summary>
		/// <param name="xAxis"></param>
		/// <param name="yAxis"></param>
		/// <param name="zAxis"></param>
		public void GetAxes(out Vector3 xAxis, out Vector3 yAxis, out Vector3 zAxis)
		{
			float xx2 = 2.0f * this.X * this.X;
			float yy2 = 2.0f * this.Y * this.Y;
			float zz2 = 2.0f * this.Z * this.Z;

			xAxis.X = 1f - (yy2 + zz2);
			xAxis.Y = 2f * (this.X * this.Y + this.Z * this.W);
			xAxis.Z = 2f * (this.X * this.Z - this.Y * this.W);

			yAxis.X = 2f * (this.X * this.Y - this.Z * this.W);
			yAxis.Y = 1f - (xx2 + zz2);
			yAxis.Z = 2f * (this.Y * this.Z + this.X * this.W);

			zAxis.X = 2f * (this.X * this.Z + this.Y * this.W);
			zAxis.Y = 2f * (this.Y * this.Z - this.X * this.W);
			zAxis.Z = 1f - (xx2 + yy2);
		}


		/// <summary>
		/// Returns the inverse local axes. Same as Quaternion.RotateInverse(Vector3.Unit[X|Y|Z]).
		/// </summary>
		/// <param name="xAxis"></param>
		/// <param name="yAxis"></param>
		/// <param name="zAxis"></param>
		public void GetAxesInverse(out Vector3 xAxis, out Vector3 yAxis, out Vector3 zAxis)
		{
			float xx2 = 2.0f * this.X * this.X;
			float yy2 = 2.0f * this.Y * this.Y;
			float zz2 = 2.0f * this.Z * this.Z;

			xAxis.X = 1f - (yy2 + zz2);
			xAxis.Y = -2f * (this.Z * this.W - this.X * this.Y);
			xAxis.Z = 2f * (this.X * this.Z + this.Y * this.W);

			yAxis.X = 2f * (this.X * this.Y + this.Z * this.W);
			yAxis.Y = 1f - (xx2 + zz2);
			yAxis.Z = -2f * (this.X * this.W - this.Y * this.Z);

			zAxis.X = -2f * (this.Y * this.W - this.X * this.Z);
			zAxis.Y = 2f * (this.Y * this.Z + this.X * this.W);
			zAxis.Z = 1f - (xx2 + yy2);
		}

		/// <summary>
		/// <para>Returns the inverse of the quaternion.</para>
		/// <para>The inverse of a quaternion represents the inverted rotation of the quaternion.</para>
		/// <para>q * q^-1 = I, where q^-1 is the inverse of the quaternion q and I the identity quaternion.</para>
		/// <para>For normalizied quaternion q^-1 = q', where q' is the conjugate.</para>
		/// </summary>
		/// <returns>The inverse of this quaternion.</returns>
		public Quaternion Invert()
		{
			float norm = this.NormSquared;
			//if (norm > Single.Epsilon)
			//{
				Quaternion result;
				norm = 1.0f / norm;
				result.X = -this.X * norm;
				result.Y = -this.Y * norm;
				result.Z = -this.Z * norm;
				result.W = this.W * norm;
				return result;
			//}
			//return this;
		}

		/// <summary>
		/// <para>Returns the conjugate of the quaternion.</para>
		/// <para>The conjugate of a quaternion is defined as (q, -p).</para>
		/// </summary>
		/// <returns>The conjugate q'.</returns>
		public Quaternion Conjugate()
		{
			Quaternion result;
			result.X = -this.X;
			result.Y = -this.Y;
			result.Z = -this.Z;
			result.W = this.W;
			return result;
		}

		/// <summary>
		/// <para>Returns the conjugate of the quaternion.</para>
		/// <para>The conjugate of a quaternion is defined as (q, -p).</para>
		/// </summary>
		/// <param name="result">The conjugate q'.</param>
		public void Conjugate(out Quaternion result)
		{
			result.X = -this.X;
			result.Y = -this.Y;
			result.Z = -this.Z;
			result.W = this.W;
		}

		/// <summary>
		/// <para>Computes the dot or inner product of the quaternions.</para>
		/// <para>
		/// The dot or inner product of the quaternion equals the cosine of the angle between the directions
		/// of the two quaternions on the hypersphere. q1.Dot(q2) = cos(alpha).
		/// </para>
		/// </summary>
		/// <param name="q">The other quaternion.</param>
		/// <returns>The dot product of the quaternions.</returns>
		public float Dot(Quaternion q)
		{
			return this.XYZ.Dot(q.XYZ) + this.W * q.W;
		}

		/// <summary>
		/// <para>Computes the dot or inner product of the quaternions.</para>
		/// <para>
		/// The dot or inner product of the quaternion equals the cosine of the angle between the directions
		/// of the two quaternions on the hypersphere. q1.Dot(q2) = cos(alpha).
		/// </para>
		/// </summary>
		/// <param name="q">The other quaternion.</param>
		/// <returns>The dot product of the quaternions.</returns>
		public float Dot(ref Quaternion q)
		{
			return this.XYZ.Dot(q.XYZ) + this.W * q.W;
		}

		/// <summary>
		/// Returns the angle between the directions of the two quaternions in radians.
		/// </summary>
		/// <param name="q">The other quaternion.</param>
		/// <returns>The angle in radians.</returns>
		public float Angle(Quaternion q)
		{
			return Utility.Acos(this.Dot(ref q));
		}

		/// <summary>
		/// Returns the angle between the directions of the two quaternions in radians.
		/// </summary>
		/// <param name="q">The other quaternion.</param>
		/// <returns>The angle in radians.</returns>
		public float Angle(ref Quaternion q)
		{
			return Utility.Acos(this.Dot(ref q));
		}

		/// <summary>
		/// <para> Returns the normalized quaternion. Normalized quaternions have the norm 1.</para>
		/// <para>
		/// In this representation we usually work with normalized quaternions,
		/// as they offer us 3 degrees of freedom, rather than 4 (we only need 3).
		/// </para>
		/// </summary>
		/// <returns>The normalized quaternion with |q| = 1.</returns>
		public Quaternion Normalize()
		{
			float norm = this.Norm;
			if (norm > float.Epsilon)
			{
				norm = 1.0f / norm;
				Quaternion qn;
				qn.X = this.X * norm;
				qn.Y = this.Y * norm;
				qn.Z = this.Z * norm;
				qn.W = this.W * norm;
				return qn;
			}
			return this;
		}

		/// <summary>
		/// <para> Returns the normalized quaternion. Normalized quaternions have the norm 1.</para>
		/// <para>
		/// In this representation we usually work with normalized quaternions,
		/// as they offer us 3 degrees of freedom, rather than 4 (we only need 3).
		/// </para>
		/// </summary>
		/// <returns>The normalized quaternion with |q| = 1.</returns>
		public void Normalize(out Quaternion result)
		{
			float norm = this.Norm;
			if (norm > float.Epsilon)
			{
				norm = 1.0f / norm;
				result.X = this.X * norm;
				result.Y = this.Y * norm;
				result.Z = this.Z * norm;
				result.W = this.W * norm;
			}
			else
			{
				result = this;
			}
		}

		/// <summary>
		/// Returns the rotated quaternion.
		/// </summary>
		/// <param name="q">The rotation represented by a quaternion.</param>
		/// <returns>The rotated quaternion.</returns>
		public Quaternion Rotate(Quaternion q)
		{
			return this * q;
		}

		/// <summary>
		/// Returns the rotated quaternion.
		/// </summary>
		/// <param name="matrix">The rotation represented by a rotation matrix.</param>
		/// <returns>The rotated quaternion.</returns>
		public Quaternion Rotate(ref Matrix3x3 matrix)
		{
			return this * FromMatrix(ref matrix);
		}

		/// <summary>
		/// Returns the rotated quaternion.
		/// </summary>
		/// <param name="matrix">The rotation represented by a transformation matrix.</param>
		/// <returns>The rotated quaternion.</returns>
		public Quaternion Rotate(ref Matrix4x4 matrix)
		{
			return this * FromMatrix(ref matrix);
		}

		/// <summary>
		/// Returns the rotated quaternion.
		/// </summary>
		/// <param name="angle">The angle to rotate about in radians.</param>
		/// <param name="axis">The axis to rotate around.</param>
		/// <returns>The rotated quaternion.</returns>
		public Quaternion Rotate(Vector3 axis, float angle)
		{
			return this * Quaternion.FromAxisAngle(axis, angle);
		}

		/// <summary>
		/// Returns the rotated quaternion.
		/// </summary>
		/// <param name="x">The x-component of the axis to rotate around.</param>
		/// <param name="y">The y-component of the axis to rotate around.</param>
		/// <param name="z">The z-component of the axis to rotate around.</param>
		/// <param name="angle">The angle to rotate around in radians.</param>
		/// <returns>The rotated quaternion.</returns>
		public Quaternion Rotate(float x, float y, float z, float angle)
		{
			return this * Quaternion.FromAxisAngle(angle, x, y, z);
		}

		/// <summary>
		/// Returns the rotated quaternion.
		/// </summary>
		/// <param name="yaw">The rotation around the x-axis in radians.</param>
		/// <param name="pitch">The rotation around the y-axis in radians.</param>
		/// <param name="roll">The rotation around the z-axis in radians.</param>
		/// <returns>The rotated quaternion.</returns>
		public Quaternion Rotate(float yaw, float pitch, float roll)
		{
			return this * Quaternion.FromEulerAngles(yaw, pitch, roll);
		}

		/// <summary>
		/// Clones the quaternion.
		/// </summary>
		/// <returns>A copy of this quaternion.</returns>
		public Quaternion Clone()
		{
			return new Quaternion(this.X, this.Y, this.Z, this.W);
		}

		/// <summary>
		/// Converts the quaternion to euler angles.
		/// </summary>
		/// <returns></returns>
		public void ToEulerAngles(out float yaw, out float pitch, out float roll)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Checks whether the two quaternions are equal within a tolerance of 0.001.
		/// </summary>
		/// <param name="q">The quaternion of compare with.</param>
		/// <returns>True if both quaternions are equal within a tolerance of 0.001, false if not.</returns>
		public bool Compare(Quaternion q)
		{
			return this.Compare(q, 0.001f);
		}

		/// <summary>
		/// Checks whether the two quaternions are equal within the given tolerance.
		/// </summary>
		/// <param name="q">The quaternion to compare with.</param>
		/// <param name="tolerance">The tolerance.</param>
		/// <returns>True if both quaternions are equal within the tolerance, false if not.</returns>
		public bool Compare(Quaternion q, float tolerance)
		{
			return Utility.Absolute(this.W - q.W) <= tolerance && Utility.Absolute(this.X - q.X) <= tolerance &&
			       Utility.Absolute(this.Y - q.Y) <= tolerance && Utility.Absolute(this.Z - q.Z) <= tolerance;
		}

		#endregion

		#endregion

		#region Operators

		/// <summary>
		/// Adds the two quaternions.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <returns>The sum of the two quaternions.</returns>
		public static Quaternion operator +(Quaternion left, Quaternion right)
		{
			left.W += right.W;
			left.X += right.X;
			left.Y += right.Y;
			left.Z += right.Z;
			return left;
		}

		/// <summary>
		/// Substracts the two quaternions.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <returns>The difference of the two quaternions.</returns>
		public static Quaternion operator -(Quaternion left, Quaternion right)
		{
			left.W -= right.W;
			left.X -= right.X;
			left.Y -= right.Y;
			left.Z -= right.Z;
			return left;
		}

		/// <summary>
		/// <para>Multiplicates the two quaternions.</para>
		/// <para>The product of two quaternions is the concatenated rotation of two quaternions.</para>
		/// <para>
		/// Quaternion multiplication isn't commutative, so the product represents the 
		/// rotation of the left quaternion, followed by the rotation of the right quaternion.
		/// </para>
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <returns>The product of the two quaternions.</returns>
		public static Quaternion operator *(Quaternion left, Quaternion right)
		{
			Quaternion result;
			result.W = left.W * right.W - left.X * right.X - left.Y * right.Y - left.Z * right.Z;
			result.X = left.W * right.X + left.X * right.W + left.Y * right.Z - left.Z * right.Y;
			result.Y = left.W * right.Y + left.Y * right.W + left.Z * right.X - left.X * right.Z;
			result.Z = left.W * right.Z + left.Z * right.W + left.X * right.Y - left.Y * right.X;
			return result;
		}

		/// <summary>
		/// Adds the scalar to the quaternion.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <returns>The sum of the scalar and the quaternion.</returns>
		public static Quaternion operator +(Quaternion q, float scalar)
		{
			q.W += scalar;
			q.X += scalar;
			q.Y += scalar;
			q.Z += scalar;
			return q;
		}

		/// <summary>
		/// Adds the scalar to the quaternion.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <returns>The sum of the scalar and the quaternion.</returns>
		public static Quaternion operator +(float scalar, Quaternion q)
		{
			q.W += scalar;
			q.X += scalar;
			q.Y += scalar;
			q.Z += scalar;
			return q;
		}

		/// <summary>
		/// Multiplicates the scalar to the quaternion.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <returns>The product of the quaternion and the scalar.</returns>
		public static Quaternion operator *(Quaternion q, float scalar)
		{
			q.W *= scalar;
			q.X *= scalar;
			q.Y *= scalar;
			q.Z *= scalar;
			return q;
		}

		/// <summary>
		/// Multiplicates the scalar to the quaternion.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <returns>The product of the quaternion and the scalar.</returns>
		public static Quaternion operator *(float scalar, Quaternion q)
		{
			q.W *= scalar;
			q.X *= scalar;
			q.Y *= scalar;
			q.Z *= scalar;
			return q;
		}

		/// <summary>
		/// Substracts the scalar from the quaternion.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <returns>The difference of the scalar and the quaternion.</returns>
		public static Quaternion operator -(Quaternion q, float scalar)
		{
			q.W -= scalar;
			q.X -= scalar;
			q.Y -= scalar;
			q.Z -= scalar;
			return q;
		}

		/// <summary>
		/// Divides the quaternion by the scalar.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="scalar">The real scalar.</param>
		/// <returns>The quotient of the scalar and the quaternion.</returns>
		public static Quaternion operator /(Quaternion q, float scalar)
		{
			q.W /= scalar;
			q.X /= scalar;
			q.Y /= scalar;
			q.Z /= scalar;
			return q;
		}

		/// <summary>
		/// <para>Negates the quaternion.</para>
		/// <para>The negated quaternion represents the same rotation as the original quaternion.</para>
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <returns>The negated quaternion.</returns>
		public static Quaternion operator -(Quaternion q)
		{
			q.W = -q.W;
			q.X = -q.X;
			q.Y = -q.Y;
			q.Z = -q.Z;
			return q;
		}

		/// <summary>
		/// <para>Rotates the vector v by the quaternion q.</para>
		/// <para>We use vector rotation in the form of v' = q*v*q^-1.</para>
		/// </summary>
		/// <param name="q">The rotation as a quaternion.</param>
		/// <param name="v">The vector to be rotated.</param>
		/// <returns>The rotated vector v'.</returns>
		public static Vector3 operator *(Quaternion q, Vector3 v)
		{
			// thanks to ryg / farbrausch
			//return v + 2.0f * q.XYZ.Cross(q.XYZ.Cross(v) + q.W * v);

			Vector3 result;
			Vector3 temp;

			temp.X = q.Y * v.Z - q.Z * v.Y + q.W * v.X;
			temp.Y = q.Z * v.X - q.X * v.Z + q.W * v.Y;
			temp.Z = q.X * v.Y - q.Y * v.X + q.W * v.Z;

			result.X = v.X + 2.0f * (q.Y * temp.Z - q.Z * temp.Y);
			result.Y = v.Y + 2.0f * (q.Z * temp.X - q.X * temp.Z);
			result.Z = v.Z + 2.0f * (q.X * temp.Y - q.Y * temp.X);
			return result;
		}

		/// <summary>
		/// Rotates the vector v by the inverse quaternion q^-1.
		/// </summary>
		/// <param name="q">The rotation as a quaternion.</param>
		/// <param name="v">The vector to be rotated.</param>
		/// <returns>The rotated vector v'.</returns>
		public static Vector3 operator *(Vector3 v, Quaternion q)
		{
			// thanks to ryg / farbrausch
			//return v + 2.0f * (-q.XYZ).Cross((-q.XYZ).Cross(v) + q.W * v);

			Vector3 result;
			Vector3 temp;

			temp.X = q.Z * v.Y - q.Y * v.Z + q.W * v.X;
			temp.Y = q.X * v.Z - q.Z * v.X + q.W * v.Y;
			temp.Z = q.Y * v.X - q.X * v.Y + q.W * v.Z;

			result.X = v.X + 2.0f * (q.Z * temp.Y - q.Y * temp.Z);
			result.Y = v.Y + 2.0f * (q.X * temp.Z - q.Z * temp.X);
			result.Z = v.Z + 2.0f * (q.Y * temp.X - q.X * temp.Y);
			return result;
		}

		/// <summary>
		/// Checks whether every component in the left quaternion is greater than those in the right one.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <returns>
		/// True if each component in the left quaternion is greater 
		/// than the corresponding component in the right one, false if not.
		/// </returns>
		public static bool operator >(Quaternion left, Quaternion right)
		{
			return left.W > right.W && left.X > right.X && left.Y > right.Y && left.Z > right.Z;
		}

		/// <summary>
		/// Checks whether every component in the left quaternion is greater than or equal to those in the right one.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <returns>
		/// True if each component in the left quaternion is greater 
		/// than or equal to the corresponding component in the right one, false if not.
		/// </returns>
		public static bool operator >=(Quaternion left, Quaternion right)
		{
			return left.W >= right.W && left.X >= right.X && left.Y >= right.Y && left.Z >= right.Z;
		}

		/// <summary>
		/// Checks whether every component in the left quaternion is lower than those in the right one.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <returns>
		/// True if each component in the left quaternion is lower 
		/// than the corresponding component in the right one, false if not.
		/// </returns>
		public static bool operator <(Quaternion left, Quaternion right)
		{
			return left.W < right.W && left.X < right.X && left.Y < right.Y && left.Z < right.Z;
		}

		/// <summary>
		/// Checks whether every component in the left quaternion is lower than or equal to those in the right one.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <returns>
		/// True if each component in the left quaternion is lower 
		/// than or equal to the corresponding component in the right one, false if not.
		/// </returns>
		public static bool operator <=(Quaternion left, Quaternion right)
		{
			return left.W <= right.W && left.X <= right.X && left.Y <= right.Y && left.Z <= right.Z;
		}

		/// <summary>
		/// Checks whether the two quaternions are equal.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <returns>
		/// True if the quaternions are equal by value, false if not.
		/// </returns>
		public static bool operator ==(Quaternion left, Quaternion right)
		{
			return left.W == right.W && left.X == right.X && left.Y == right.Y && left.Z == right.Z;
		}

		/// <summary>
		/// Checks whether the two quaternions are unequal.
		/// </summary>
		/// <param name="left">The left quaternion.</param>
		/// <param name="right">The right quaternion.</param>
		/// <returns>
		/// True if the quaternions are unequal by value, false if not.
		/// </returns>
		public static bool operator !=(Quaternion left, Quaternion right)
		{
			return !(left == right);
		}

		#endregion

		#region Overrides

		/// <summary>
		/// Returns whether the given quaternion and this quaternion are equal.
		/// </summary>
		/// <param name="obj">The other quaternion.</param>
		/// <returns>
		/// False if any component of the two quaternions are unequal or the given object is no quaternion.
		/// True if each component of the two quaternions are equal.  
		/// </returns>
		public override bool Equals(object obj)
		{
			if (obj is Quaternion)
				return this == (Quaternion)obj;
			return false;
		}

		/// <summary>
		/// Gets an unique HashCode of this object.
		/// </summary>
		/// <returns>The hashcode.</returns>
		public override int GetHashCode()
		{
			int hash = this.X.GetHashCode();
			hash = Utility.CombineHash(hash, this.Y.GetHashCode());
			hash = Utility.CombineHash(hash, this.Z.GetHashCode());
			hash = Utility.CombineHash(hash, this.W.GetHashCode());
			return hash;
		}

		#endregion
	}
}
