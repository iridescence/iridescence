﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Math
{
	public partial struct Quaternion : IReadOnlyList<float>
	{
		#region Properties
		
		int IReadOnlyCollection<float>.Count => 4;

		#endregion

		#region Methods

		IEnumerator<float> IEnumerable<float>.GetEnumerator()
		{
			return ((IEnumerable<float>)this.ToArray()).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable<float>)this.ToArray()).GetEnumerator();
		}

		#endregion
	}
}
