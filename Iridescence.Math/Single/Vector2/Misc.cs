﻿namespace Iridescence.Math
{
	public partial struct Vector2
	{
		/// <summary>
		/// Returns a vector with the specified length that points in the specified direction.
		/// </summary>
		/// <param name="angle">The angle.</param>
		/// <param name="length">The length of the vector.</param>
		/// <param name="vector">The resulting vector.</param>
		public static void GetDirectionVector(float angle, float length, out Vector2 vector)
		{
			vector.X = Utility.Cos(angle) * length;
			vector.Y = Utility.Sin(angle) * length;
		}

		/// <summary>
		/// Returns a vector with the specified length that points in the specified direction.
		/// </summary>
		/// <param name="angle">The angle.</param>
		/// <param name="length">The length of the vector.</param>
		/// <returns>The resulting vector.</returns>
		public static Vector2 GetDirectionVector(float angle, float length)
		{
			Vector2 vector;
			vector.X = Utility.Cos(angle) * length;
			vector.Y = Utility.Sin(angle) * length;
			return vector;
		}
	}
}
