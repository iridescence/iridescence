﻿namespace Iridescence.Math
{
	public partial struct Vector2
	{
		/// <summary>
		/// Returns a.X * b.Y - a.Y * b.X.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static float Cross(in Vector2 a, in Vector2 b)
		{
			return a.X * b.Y - a.Y * b.X;
		}

		/// <summary>
		/// Returns (s * a.Y, -s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <param name="result"></param>
		public static void Cross(in Vector2 a, float s, out Vector2 result)
		{
			float aX = a.X;
			result.X = s * a.Y;
			result.Y = -s * aX;
		}

		/// <summary>
		/// Returns (s * a.Y, -s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <returns></returns>
		public static Vector2 Cross(in Vector2 a, float s)
		{
			Vector2 result; 
			result.X = s * a.Y;
			result.Y = -s * a.X;
			return result;
		}

		/// <summary>
		/// Returns (-s * a.Y, s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <param name="result"></param>
		public static void Cross(float s, in Vector2 a, out Vector2 result)
		{
			float aX = a.X;
			result.X = -s * a.Y;
			result.Y = s * aX;
		}

		/// <summary>
		/// Returns (-s * a.Y, s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <returns></returns>
		public static Vector2 Cross(float s, in Vector2 a)
		{
			Vector2 result;
			result.X = -s * a.Y;
			result.Y = s * a.X;
			return result;
		}

		/// <summary>
		/// Returns this.X * b.Y - this.Y * b.X.
		/// </summary>
		/// <param name="b"></param>
		/// <returns></returns>
		public float Cross(in Vector2 b)
		{
			return this.X * b.Y - this.Y * b.X;
		}

		/// <summary>
		/// Returns (s * this.Y, -s * this.X).
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public Vector2 Cross(float s)
		{
			Vector2 result;
			result.X = s * this.Y;
			result.Y = -s * this.X;
			return result;
		}
	}
}
