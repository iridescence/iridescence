﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a 2D polygon.
	/// </summary>
	[Serializable]
	public class Polygon2 : List<Vector2>
	{
		#region Properties

		/// <summary>
		/// Gets a value that indicates whether the polygon is (mostly) clockwise.
		/// </summary>
		public bool IsClockwise
		{
			get
			{
				float sum = 0.0f;
				for (int i = 0; i < this.Count; i++)
				{
					int j = i + 1;
					if (j == this.Count)
						j = 0;

					sum += (this[j].X - this[i].X) * (this[j].Y + this[i].Y);
				}

				return sum >= 0.0f;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Polygon2.
		/// </summary>
		public Polygon2()
		{

		}

		/// <summary>
		/// Creates a new Polygon2.
		/// </summary>
		public Polygon2(IEnumerable<Vector2> vertices)
			: base(vertices)
		{
			
		}

		/// <summary>
		/// Creates a new Polygon2.
		/// </summary>
		public Polygon2(int capacity)
			: base(capacity)
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds a new vertex.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		public void Add(float x, float y)
		{
			Vector2 v;
			v.X = x;
			v.Y = y;
			this.Add(v);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private Vector2 wrap(int i)
		{
			if (this.Count == 1)
				return this[0];

			if (i < 0)
				return this[(i % this.Count) + this.Count];
			return this[i % this.Count];
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static float area(Vector2 a, Vector2 b, Vector2 c)
		{
			return ((b.X - a.X) * (c.Y - a.Y)) - ((c.X - a.X) * (b.Y - a.Y));
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static bool left(Vector2 a, Vector2 b, Vector2 c)
		{
			return area(a, b, c) > 0.0f;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static bool leftOn(Vector2 a, Vector2 b, Vector2 c)
		{
			return area(a, b, c) >= 0.0f;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static bool right(Vector2 a, Vector2 b, Vector2 c)
		{
			return area(a, b, c) < 0.0f;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static bool rightOn(Vector2 a, Vector2 b, Vector2 c)
		{
			return area(a, b, c) <= 0.0f;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static bool isReflex(Polygon2 poly, int i)
		{
			return right(poly.wrap(i - 1), poly[i], poly.wrap(i + 1));
		}

		private static void insert(List<Vector2> dest, List<Vector2> source, int startIndex, int endIndex)
		{
			for (int i = startIndex; i < endIndex; i++)
				dest.Add(source[i]);
		}

		private static void decompose(Polygon2 poly, List<Polygon2> polys, List<Vector2> reflexVertices)
		{
			Vector2 upperInt = Vector2.Zero, lowerInt = Vector2.Zero;
			int upperIndex = 0, lowerIndex = 0, closestIndex = 0;

			for (int i = 0; i < poly.Count; ++i)
			{
				if (isReflex(poly, i))
				{
					reflexVertices.Add(poly[i]);
					float lowerDist;
					var upperDist = lowerDist = float.MaxValue;
					float d;
					Vector2 p;
					for (int j = 0; j < poly.Count; ++j)
					{
						if (left(poly.wrap(i - 1), poly.wrap(i), poly.wrap(j)) && rightOn(poly.wrap(i - 1), poly.wrap(i), poly.wrap(j - 1)))
						{
							// if line intersects with an edge
							p = LineSegment.Intersect(poly.wrap(i - 1), poly.wrap(i), poly.wrap(j), poly.wrap(j - 1)); // find the point of intersection
							if (right(poly.wrap(i + 1), poly.wrap(i), p))
							{
								// make sure it's inside the poly
								d = Vector2.DistanceSquared(poly[i], p);
								if (d < lowerDist)
								{
									// keep only the closest intersection
									lowerDist = d;
									lowerInt = p;
									lowerIndex = j;
								}
							}
						}

						if (left(poly.wrap(i + 1), poly.wrap(i), poly.wrap(j + 1)) && rightOn(poly.wrap(i + 1), poly.wrap(i), poly.wrap(j)))
						{
							p = LineSegment.Intersect(poly.wrap(i + 1), poly.wrap(i), poly.wrap(j), poly.wrap(j + 1));
							if (left(poly.wrap(i - 1), poly.wrap(i), p))
							{
								d = Vector2.DistanceSquared(poly[i], p);
								if (d < upperDist)
								{
									upperDist = d;
									upperInt = p;
									upperIndex = j;
								}
							}
						}
					}

					Polygon2 lowerPoly = new Polygon2();
					Polygon2 upperPoly = new Polygon2();
					
					// if there are no vertices to connect to, choose a point in the middle
					if (lowerIndex == (upperIndex + 1) % poly.Count)
					{
						//Console.WriteLine("Case 1: Vertex({0}), lowerIndex({1}), upperIndex({2}), poly.size({3})", i, lowerIndex, upperIndex, poly.Count);
						p.X = (lowerInt.X + upperInt.X) / 2;
						p.Y = (lowerInt.Y + upperInt.Y) / 2;
						//steinerPoints.push_back(p);

						if (i < upperIndex)
						{
							insert(lowerPoly, poly, i, upperIndex + 1);

							lowerPoly.Add(p);
							upperPoly.Add(p);

							if (lowerIndex != 0)
								insert(upperPoly, poly, lowerIndex, poly.Count);

							insert(upperPoly, poly, 0, i + 1);
						}
						else
						{
							if (i != 0)
								insert(lowerPoly, poly, i, poly.Count);

							insert(lowerPoly, poly, 0, upperIndex + 1);

							lowerPoly.Add(p);
							upperPoly.Add(p);

							insert(upperPoly, poly, lowerIndex, i + 1);
						}
					}
					else
					{
						// connect to the closest point within the triangle
						//Console.WriteLine("Case 2: Vertex({0}), closestIndex({1}), poly.size({2})\n", i, closestIndex, poly.Count);

						if (lowerIndex > upperIndex)
						{
							upperIndex += poly.Count;
						}

						var closestDist = float.MaxValue;
						for (int j = lowerIndex; j <= upperIndex; ++j)
						{
							if (leftOn(poly.wrap(i - 1), poly.wrap(i), poly.wrap(j))
							    && rightOn(poly.wrap(i + 1), poly.wrap(i), poly.wrap(j)))
							{
								d = Vector2.DistanceSquared(poly.wrap(i), poly.wrap(j));
								if (d < closestDist)
								{
									closestDist = d;
									poly.wrap(j);
									closestIndex = j % poly.Count;
								}
							}
						}

						if (i < closestIndex)
						{
							insert(lowerPoly, poly, i, closestIndex + 1);

							if (closestIndex != 0)
								insert(upperPoly, poly, closestIndex, poly.Count);

							insert(upperPoly, poly, 0, i + 1);
						}
						else
						{
							if (i != 0)
								insert(lowerPoly, poly, i, poly.Count);

							insert(lowerPoly, poly, 0, closestIndex + 1);

							insert(upperPoly, poly, closestIndex, i + 1);
						}
					}

					// solve smallest poly first
					if (lowerPoly.Count < upperPoly.Count)
					{
						decompose(lowerPoly, polys, reflexVertices);
						decompose(upperPoly, polys, reflexVertices);
					}
					else
					{
						decompose(upperPoly, polys, reflexVertices);
						decompose(lowerPoly, polys, reflexVertices);
					}
					return;
				}
			}
			polys.Add(poly);
		}

		/// <summary>
		/// Decomposes the polygon into convex sub-polygons.
		/// </summary>
		/// <returns>An array of convex polygons.</returns>
		public Polygon2[] Decompose()
		{
			List<Polygon2> polys = new List<Polygon2>();

			if (this.IsClockwise)
			{
				Polygon2 reverse = new Polygon2(this);
				reverse.Reverse();
				decompose(reverse, polys, new List<Vector2>());
			}
			else
			{
				decompose(this, polys, new List<Vector2>());	
			}

			return polys.ToArray();
		}

		/// <summary>
		/// Gets an array of line segments.
		/// </summary>
		/// <returns></returns>
		public LineSegment[] GetLineSegments()
		{
			LineSegment[] segments = new LineSegment[this.Count];

			for (int i = 0; i < this.Count; i++)
			{
				int j = i + 1;
				if (j == this.Count)
					j = 0;

				segments[i].Start = this[i];
				segments[i].End = this[j];
			}

			return segments;
		}

		#endregion
	}
}
