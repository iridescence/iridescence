﻿using System;

namespace Iridescence.Math
{
	public partial struct Matrix4x4
	{
		#region Translation

		/// <summary>
		/// Creates a translation matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="x">X-axis translation.</param>
		/// <param name="y">Y-axis translation.</param>
		/// <param name="z">Z-axis translation.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateTranslation(float x, float y, float z, out Matrix4x4 result)
		{
			result = Matrix4x4.Identity;
			result.M14 = x;
			result.M24 = y;
			result.M34 = z;
		}
		
		/// <summary>
		/// Creates a translation matrix and returns the result.
		/// </summary>
		/// <param name="x">X-axis translation.</param>
		/// <param name="y">Y-axis translation.</param>
		/// <param name="z">Z-axis translation.</param>
		public static Matrix4x4 CreateTranslation(float x, float y, float z)
		{
			Matrix4x4 result = Matrix4x4.Identity;
			result.M14 = x;
			result.M24 = y;
			result.M34 = z;
			return result;
		}
		
		/// <summary>
		/// Creates a translation matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="vector">The translation vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateTranslation(ref Vector3 vector, out Matrix4x4 result)
		{
			result = Matrix4x4.Identity;
			result.M14 = vector.X;
			result.M24 = vector.Y;
			result.M34 = vector.Z;
		}
		
		/// <summary>
		/// Creates a translation matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="vector">The translation vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateTranslation(Vector3 vector, out Matrix4x4 result)
		{
			result = Matrix4x4.Identity;
			result.M14 = vector.X;
			result.M24 = vector.Y;
			result.M34 = vector.Z;
		}
		
		/// <summary>
		/// Creates a translation matrix and returns the result.
		/// </summary>
		/// <param name="vector">The translation vector.</param>
		public static Matrix4x4 CreateTranslation(ref Vector3 vector)
		{
			Matrix4x4 result = Matrix4x4.Identity;
			result.M14 = vector.X;
			result.M24 = vector.Y;
			result.M34 = vector.Z;
			return result;
		}
		
		/// <summary>
		/// Creates a translation matrix and returns the result.
		/// </summary>
		/// <param name="vector">The translation vector.</param>
		public static Matrix4x4 CreateTranslation(Vector3 vector)
		{
			Matrix4x4 result = Matrix4x4.Identity;
			result.M14 = vector.X;
			result.M24 = vector.Y;
			result.M34 = vector.Z;
			return result;
		}

		#endregion

		#region Rotation

		/// <summary>
		/// Creates a rotation matrix based on an axis/angle rotation and stores the result in the specified matrix.
		/// </summary>
		/// <param name="axis">The axis to rotate about.</param>
		/// <param name="angle">The angle in radians (counter-clockwise).</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(ref Vector3 axis, float angle, out Matrix4x4 result)
		{
			// http://en.wikipedia.org/wiki/Rotation_matrix#Axis_and_angle
			axis = axis.Normalize();
			
			float c = Utility.Cos(angle);
			float s = Utility.Sin(angle);
			float C = 1.0f - c;
			
			float xxC = axis.X * axis.X * C;
			float xyC = axis.X * axis.Y * C;
			float xzC = axis.X * axis.Z * C;
			float yyC = axis.Y * axis.Y * C;
			float yzC = axis.Y * axis.Z * C;
			float zzC = axis.Z * axis.Z * C;
			
			float xs = s * axis.X;
			float ys = s * axis.Y;
			float zs = s * axis.Z;
			
			result.M11 = xxC + c;
			result.M12 = xyC - zs;
			result.M13 = xzC + ys;
			result.M14 = 0.0f;
			
			result.M21 = xyC + zs;
			result.M22 = yyC + c;
			result.M23 = yzC - xs;
			result.M24 = 0.0f;
			
			result.M31 = xzC - ys;
			result.M32 = yzC + xs;
			result.M33 = zzC + c;
			result.M34 = 0.0f;
			
			result.M41 = 0.0f;
			result.M42 = 0.0f;
			result.M43 = 0.0f;
			result.M44 = 1.0f;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on an axis/angle rotation and stores the result in the specified matrix.
		/// </summary>
		/// <param name="axis">The axis to rotate about.</param>
		/// <param name="angle">The angle in radians (counter-clockwise).</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(Vector3 axis, float angle, out Matrix4x4 result)
		{
			// http://en.wikipedia.org/wiki/Rotation_matrix#Axis_and_angle
			axis = axis.Normalize();
			
			float c = Utility.Cos(angle);
			float s = Utility.Sin(angle);
			float C = 1.0f - c;
			
			float xxC = axis.X * axis.X * C;
			float xyC = axis.X * axis.Y * C;
			float xzC = axis.X * axis.Z * C;
			float yyC = axis.Y * axis.Y * C;
			float yzC = axis.Y * axis.Z * C;
			float zzC = axis.Z * axis.Z * C;
			
			float xs = s * axis.X;
			float ys = s * axis.Y;
			float zs = s * axis.Z;
			
			result.M11 = xxC + c;
			result.M12 = xyC - zs;
			result.M13 = xzC + ys;
			result.M14 = 0.0f;
			
			result.M21 = xyC + zs;
			result.M22 = yyC + c;
			result.M23 = yzC - xs;
			result.M24 = 0.0f;
			
			result.M31 = xzC - ys;
			result.M32 = yzC + xs;
			result.M33 = zzC + c;
			result.M34 = 0.0f;
			
			result.M41 = 0.0f;
			result.M42 = 0.0f;
			result.M43 = 0.0f;
			result.M44 = 1.0f;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on an axis/angle rotation and returns the result.
		/// </summary>
		/// <param name="axis">The axis to rotate about.</param>
		/// <param name="angle">The angle in radians (counter-clockwise).</param>
		public static Matrix4x4 CreateRotation(ref Vector3 axis, float angle)
		{
			Matrix4x4 result;
			Matrix4x4.CreateRotation(ref axis, angle, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on an axis/angle rotation and returns the result.
		/// </summary>
		/// <param name="axis">The axis to rotate about.</param>
		/// <param name="angle">The angle in radians (counter-clockwise).</param>
		public static Matrix4x4 CreateRotation(Vector3 axis, float angle)
		{
			Matrix4x4 result;
			Matrix4x4.CreateRotation(ref axis, angle, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and stores the result in the specified matrix.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(ref Quaternion q, out Matrix4x4 result)
		{
			Quaternion qn;
			Quaternion.Normalize(ref q, out qn);
			
			float xx = qn.X * qn.X;
			float yy = qn.Y * qn.Y;
			float zz = qn.Z * qn.Z;
			float xy = qn.X * qn.Y;
			float zw = qn.Z * qn.W;
			float zx = qn.Z * qn.X;
			float yw = qn.Y * qn.W;
			float yz = qn.Y * qn.Z;
			float xw = qn.X * qn.W;
		
			result.M11 = 1f - (2f * (yy + zz));
			result.M21 = 2f * (xy + zw);
			result.M31 = 2f * (zx - yw);
			result.M41 = 0f;
		
			result.M12 = 2f * (xy - zw);
			result.M22 = 1f - (2f * (zz + xx));
			result.M32 = 2f * (yz + xw);
			result.M42 = 0f;
		
			result.M13 = 2f * (zx + yw);
			result.M23 = 2f * (yz - xw);
			result.M33 = 1f - (2f * (yy + xx));
			result.M43 = 0f;
			
			result.M14 = 0f;
			result.M24 = 0f;
			result.M34 = 0f;
			result.M44 = 1f;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and stores the result in the specified matrix.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(Quaternion q, out Matrix4x4 result)
		{
			Quaternion.Normalize(ref q, out q);
		
			float xx = q.X * q.X;
			float yy = q.Y * q.Y;
			float zz = q.Z * q.Z;
			float xy = q.X * q.Y;
			float zw = q.Z * q.W;
			float zx = q.Z * q.X;
			float yw = q.Y * q.W;
			float yz = q.Y * q.Z;
			float xw = q.X * q.W;
		
			result.M11 = 1f - (2f * (yy + zz));
			result.M21 = 2f * (xy + zw);
			result.M31 = 2f * (zx - yw);
			result.M41 = 0f;
		
			result.M12 = 2f * (xy - zw);
			result.M22 = 1f - (2f * (zz + xx));
			result.M32 = 2f * (yz + xw);
			result.M42 = 0f;
		
			result.M13 = 2f * (zx + yw);
			result.M23 = 2f * (yz - xw);
			result.M33 = 1f - (2f * (yy + xx));
			result.M43 = 0f;
		
			result.M14 = 0f;
			result.M24 = 0f;
			result.M34 = 0f;
			result.M44 = 1f;
		}

		/// <summary>
		/// Creates a rotation matrix based on a quaternion and returns the result.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		public static Matrix4x4 CreateRotation(ref Quaternion q)
		{
			Matrix4x4 result;
			Matrix4x4.CreateRotation(ref q, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and returns the result.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		public static Matrix4x4 CreateRotation(Quaternion q)
		{
			Matrix4x4 result;
			Matrix4x4.CreateRotation(ref q, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a 3x3 rotation matrix.
		/// </summary>
		/// <param name="matrix">The rotation matrix.</param>
		public static Matrix4x4 CreateRotation(ref Matrix3x3 matrix)
		{
			Matrix4x4 result;
			result.M11 = matrix.M11;
			result.M12 = matrix.M12;
			result.M13 = matrix.M13;
			result.M14 = 0.0f;
		
			result.M21 = matrix.M21;
			result.M22 = matrix.M22;
			result.M23 = matrix.M23;
			result.M24 = 0.0f;
		
			result.M31 = matrix.M31;
			result.M32 = matrix.M32;
			result.M33 = matrix.M33;
			result.M34 = 0.0f;
		
			result.M41 = 0.0f;
			result.M42 = 0.0f;
			result.M43 = 0.0f;
			result.M44 = 1.0f;
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a 3x3 rotation matrix.
		/// </summary>
		/// <param name="matrix">The rotation matrix.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(ref Matrix3x3 matrix, out Matrix4x4 result)
		{
			result.M11 = matrix.M11;
			result.M12 = matrix.M12;
			result.M13 = matrix.M13;
			result.M14 = 0.0f;
		
			result.M21 = matrix.M21;
			result.M22 = matrix.M22;
			result.M23 = matrix.M23;
			result.M24 = 0.0f;
		
			result.M31 = matrix.M31;
			result.M32 = matrix.M32;
			result.M33 = matrix.M33;
			result.M34 = 0.0f;
		
			result.M41 = 0.0f;
			result.M42 = 0.0f;
			result.M43 = 0.0f;
			result.M44 = 1.0f;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a 3x3 rotation matrix and returns the result.
		/// </summary>
		/// <param name="matrix">The rotation matrix.</param>
		public static Matrix4x4 CreateRotation(Matrix3x3 matrix)
		{
			Matrix4x4 result;
			Matrix4x4.CreateRotation(ref matrix, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a 3x3 rotation matrix and returns the result.
		/// </summary>
		/// <param name="matrix">The rotation matrix.</param>
		/// <param name="result">The result.</param>
		public static void CreateRotation(Matrix3x3 matrix, out Matrix4x4 result)
		{
			Matrix4x4.CreateRotation(ref matrix, out result);
		}

		#endregion

		#region Scale

		/// <summary>
		/// Creates a scaling matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="x">X-axis scaling factor.</param>
		/// <param name="y">Y-axis scaling factor.</param>
		/// <param name="z">Z-axis scaling factor.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateScale(float x, float y, float z, out Matrix4x4 result)
		{
			result = Matrix4x4.Identity;
			result.M11 = x;
			result.M22 = y;
			result.M33 = z;
		}
		
		/// <summary>
		/// Creates a scaling matrix and returns the result.
		/// </summary>
		/// <param name="x">X-axis scaling factor.</param>
		/// <param name="y">Y-axis scaling factor.</param>
		/// <param name="z">Z-axis scaling factor.</param>
		public static Matrix4x4 CreateScale(float x, float y, float z)
		{
			Matrix4x4 result = Matrix4x4.Identity;
			result.M11 = x;
			result.M22 = y;
			result.M33 = z;
			return result;
		}
		
		/// <summary>
		/// Creates a scaling matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="scale">The scaling factor.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateScale(ref Vector3 scale, out Matrix4x4 result)
		{
			result = Matrix4x4.Identity;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
		}
		
		/// <summary>
		/// Creates a scaling matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="scale">The scaling factor.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateScale(Vector3 scale, out Matrix4x4 result)
		{
			result = Matrix4x4.Identity;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
		}
		
		/// <summary>
		/// Creates a scaling matrix and returns the result.
		/// </summary>
		/// <param name="scale">The scaling factor.</param>
		public static Matrix4x4 CreateScale(ref Vector3 scale)
		{
			Matrix4x4 result = Matrix4x4.Identity;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
			return result;
		}
		
		/// <summary>
		/// Creates a scaling matrix and returns the result.
		/// </summary>
		/// <param name="scale">The scaling factor.</param>
		public static Matrix4x4 CreateScale(Vector3 scale)
		{
			Matrix4x4 result = Matrix4x4.Identity;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
			return result;
		}
		
		/// <summary>
		/// Creates a scaling matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="scale">The scaling factor.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateScale(float scale, out Matrix4x4 result)
		{
			result = Matrix4x4.Identity;
			result.M11 = scale;
			result.M22 = scale;
			result.M33 = scale;
		}
		
		/// <summary>
		/// Creates a scaling matrix and returns the result.
		/// </summary>
		/// <param name="scale">The scaling factor.</param>
		public static Matrix4x4 CreateScale(float scale)
		{
			Matrix4x4 result = Matrix4x4.Identity;
			result.M11 = scale;
			result.M22 = scale;
			result.M33 = scale;
			return result;
		}

		#endregion

		#region LookAt

		/// <summary>
		/// Creates a view matrix that looks from the eye position towards the target position and stores the result in the specified matrix.
		/// </summary>
		/// <param name="eye">The eye or camera position from where to look from.</param>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateLookAt(ref Vector3 eye, ref Vector3 target, ref Vector3 up, out Matrix4x4 result)
		{
			Vector3 x, y, z;
		
			Vector3.Subtract(eye, target, out z);
			Vector3.Normalize(in z, out z);
		
			Vector3.Cross(in up, in z, out x);
			Vector3.Normalize(in x, out x);
			
			Vector3.Cross(in z, in x, out y);
			Vector3.Normalize(in y, out y);
			
			result.M11 = x.X;
			result.M12 = x.Y;
			result.M13 = x.Z;
			result.M14 = -Vector3.Dot(x, eye);
			
			result.M21 = y.X;
			result.M22 = y.Y;
			result.M23 = y.Z;
			result.M24 = -Vector3.Dot(y, eye);
			
			result.M31 = z.X;
			result.M32 = z.Y;
			result.M33 = z.Z;
			result.M34 = -Vector3.Dot(z, eye);
			
			result.M41 = 0.0f;
			result.M42 = 0.0f;
			result.M43 = 0.0f;
			result.M44 = 1.0f;
		}
		
		/// <summary>
		/// Creates a view matrix that looks from the eye position towards the target position and stores the result in the specified matrix.
		/// </summary>
		/// <param name="eye">The eye or camera position from where to look from.</param>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateLookAt(Vector3 eye, Vector3 target, Vector3 up, out Matrix4x4 result)
		{
			Matrix4x4.CreateLookAt(ref eye, ref target, ref up, out result);
		}
		
		/// <summary>
		/// Creates a view matrix that looks from the eye position towards the target position and returns the result.
		/// </summary>
		/// <param name="eye">The eye or camera position from where to look from.</param>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		public static Matrix4x4 CreateLookAt(ref Vector3 eye, ref Vector3 target, ref Vector3 up)
		{
			Matrix4x4 result;
			Matrix4x4.CreateLookAt(ref eye, ref target, ref up, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a view matrix that looks from the eye position towards the target position and returns the result.
		/// </summary>
		/// <param name="eye">The eye or camera position from where to look from.</param>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		public static Matrix4x4 CreateLookAt(Vector3 eye, Vector3 target, Vector3 up)
		{
			Matrix4x4 result;
			Matrix4x4.CreateLookAt(ref eye, ref target, ref up, out result);
			return result;
		}

		#endregion

		#region Orthographic

		/// <summary>
		/// Creates an orthographic projection matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="left">The left edge of the projection plane.</param>
		/// <param name="right">The right edge of the projection plane</param>
		/// <param name="bottom">The bottom edge of the projection plane</param>
		/// <param name="top">The top edge of the projection plane</param>
		/// <param name="near">The near value of the projection plane.</param>
		/// <param name="far">The far value of the projection plane.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateOrthographic(float left, float right, float bottom, float top, float near, float far, out Matrix4x4 result)
		{
			float invX = 1f / (right - left);
			float invY = 1f / (top - bottom);
			float invZ = 1f / (far - near);
			
			result = Matrix4x4.Identity;
			
			result.M11 = 2f * invX;
			result.M22 = 2f * invY;
			result.M33 = -2f * invZ;
			
			result.M14 = -(right + left) * invX;
			result.M24 = -(top + bottom) * invY;
			result.M34 = -(far + near) * invZ;
		}
		
		/// <summary>
		/// Creates an orthographic projection matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="left">The left edge of the projection plane.</param>
		/// <param name="right">The right edge of the projection plane</param>
		/// <param name="bottom">The bottom edge of the projection plane</param>
		/// <param name="top">The top edge of the projection plane</param>
		/// <param name="near">The near value of the projection plane.</param>
		/// <param name="far">The far value of the projection plane.</param>
		public static Matrix4x4 CreateOrthographic(float left, float right, float bottom, float top, float near, float far)
		{
			Matrix4x4 result;
			Matrix4x4.CreateOrthographic(left, right, bottom, top, near, far, out result);
			return result;
		}
		
		/// <summary>
		/// Creates an orthographic projection matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="width">The width of the projection plane.</param>
		/// <param name="height">The height of the projection plane.</param>
		/// <param name="near">The near value of the projection plane.</param>
		/// <param name="far">The far value of the projection plane.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateOrthographic(float width, float height, float near, float far, out Matrix4x4 result)
		{
			Matrix4x4.CreateOrthographic(width * -0.5f, width * 0.5f, height * -0.5f, height * 0.5f, near, far, out result);
		}
		
		/// <summary>
		/// Creates an orthographic projection matrix and returns the result.
		/// </summary>
		/// <param name="width">The width of the projection plane.</param>
		/// <param name="height">The height of the projection plane.</param>
		/// <param name="near">The near value of the projection plane.</param>
		/// <param name="far">The far value of the projection plane.</param>
		public static Matrix4x4 CreateOrthographic(float width, float height, float near, float far)
		{
			return Matrix4x4.CreateOrthographic(width * -0.5f, width * 0.5f, height * -0.5f, height * 0.5f, near, far);
		}

		#endregion

		#region Perspective

		/// <summary>
		/// Creates a perspective projection matrix and stores the result in the specified matrix.
		/// </summary>
		/// <param name="width">The width of the view volume at the near plane.</param>
		/// <param name="height">The height of the view volume at the near plane.</param>
		/// <param name="near">The near plane of the projection.</param>
		/// <param name="far">The far plane of the projection.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreatePerspective(float width, float height, float near, float far, out Matrix4x4 result)
		{
			if (near <= 0.0f)
				throw new ArgumentOutOfRangeException("near");

			if (far <= 0.0f)
				throw new ArgumentOutOfRangeException("far");

			if (near >= far)
				throw new ArgumentOutOfRangeException("near");
		
			result = Matrix4x4.Zero;
			result.M11 = 2.0f * near / width;
			result.M22 = 2.0f * near / height;
			result.M33 = far / (near - far);
			result.M34 = (near * far) / (near - far);
			result.M43 = -1f;
		}
		
		/// <summary>
		/// Creates a perspective projection matrix and returns the result.
		/// </summary>
		/// <param name="width">The width of the view volume at the near plane.</param>
		/// <param name="height">The height of the view volume at the near plane.</param>
		/// <param name="near">The near plane of the projection.</param>
		/// <param name="far">The far plane of the projection.</param>
		public static Matrix4x4 CreatePerspective(float width, float height, float near, float far)
		{
			Matrix4x4 result;
			Matrix4x4.CreatePerspective(width, height, near, far, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a perspective projection matrix.
		/// </summary>
		/// <param name="fovY">The vertical field of view in radians.</param>
		/// <param name="aspect">The aspect ratio of the view (width/height).</param>
		/// <param name="near">The near value of the projection.</param>
		/// <param name="far">The far value of the projection.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreatePerspectiveFOV(float fovY, float aspect, float near, float far, out Matrix4x4 result)
		{
			if (fovY <= 0.0f || fovY > Constants.Pi) throw new ArgumentOutOfRangeException("fovY");
			if (aspect <= 0.0f) throw new ArgumentOutOfRangeException("aspect");
			
			float f = 1.0f / Utility.Tan(0.5f * fovY);
		
			result = Matrix4x4.Zero;
			result.M11 = f / aspect;
			result.M22 = f;
			result.M33 = (near + far) / (near - far);
			result.M34 = (2f * near * far) / (near - far);
			result.M43 = -1f;
		}
		
		/// <summary>
		/// Creates a perspective projection matrix and returns the result.
		/// </summary>
		/// <param name="fovY">The vertical field of view in radians.</param>
		/// <param name="aspect">The aspect ratio of the view (width/height).</param>
		/// <param name="near">The near value of the projection.</param>
		/// <param name="far">The far value of the projection.</param>
		public static Matrix4x4 CreatePerspectiveFOV(float fovY, float aspect, float near, float far)
		{
			Matrix4x4 result;
			Matrix4x4.CreatePerspectiveFOV(fovY, aspect, near, far, out result);
			return result;
		}

		#endregion
	}
}
