﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Globalization;

namespace Iridescence.Math
{
	public partial struct Matrix4x4
	{
		public static bool TryParse(string str, NumberStyles style, IFormatProvider provider, out Matrix4x4 result)
		{
			result = default;
			Span<float> values = stackalloc float[16];
			if(!Formatting.TryParse(str, (string input, out float output) => float.TryParse(input, style, provider, out output), values))
				return false;
			result.M11 = values[0];
			result.M12 = values[1];
			result.M13 = values[2];
			result.M14 = values[3];
			result.M21 = values[4];
			result.M22 = values[5];
			result.M23 = values[6];
			result.M24 = values[7];
			result.M31 = values[8];
			result.M32 = values[9];
			result.M33 = values[10];
			result.M34 = values[11];
			result.M41 = values[12];
			result.M42 = values[13];
			result.M43 = values[14];
			result.M44 = values[15];
			return true;
		}
		
		public static bool TryParse(string str, out Matrix4x4 result)
		{
			return TryParse(str, Formatting.DefaultFloatStyles, NumberFormatInfo.CurrentInfo, out result);
		}
		
		public static Matrix4x4 Parse(string str, NumberStyles style, IFormatProvider provider)
		{
			if(!TryParse(str, style, provider, out Matrix4x4 result))
				throw new FormatException();
			
			return result;
		}
		
		public static Matrix4x4 Parse(string str)
		{
			if(!TryParse(str, out Matrix4x4 result))
				throw new FormatException();
			
			return result;
		}
		
	}
}
