﻿namespace Iridescence.Math
{
	public partial struct Vector3
	{
		/// <summary>
		/// Returns the angle between the two vectors.
		/// </summary>
		/// <param name="vector">The second vector.</param>
		/// <returns>The angle between the two vectors in radians.</returns>
		public float Angle(ref Vector3 vector)
		{
			float lp = this.Length * vector.Length;
			if (lp > 0.0f)
				return Utility.Acos(this.Dot(vector) / lp);
			return float.NaN;
		}

		/// <summary>
		/// Returns the angle between the two vectors. For performance reasons, it is recommended to use the methods with reference parameters.
		/// </summary>
		/// <param name="vector">The second vector.</param>
		/// <returns>The angle between the two vectors in radians.</returns>
		public float Angle(Vector3 vector)
		{
			float lp = this.Length * vector.Length;
			if (lp > 0.0f)
				return Utility.Acos(this.Dot(vector) / lp);
			return float.NaN;
		}

		/// <summary>
		/// Checks whether this and the specified vector are perpendicular.
		/// </summary>
		/// <param name="vector">The other vector.</param>
		/// <param name="epsilon">The precision threshold.</param>
		/// <returns>True if they are perpendicular, false if not.</returns>
		public bool IsPerpendicular(ref Vector3 vector, float epsilon)
		{
			return Utility.Absolute(this.Dot(vector)) <= epsilon;
		}

		/// <summary>
		/// Checks whether this and the specified vector are perpendicular.
		/// </summary>
		/// <param name="vector">The other vector.</param>
		/// <param name="epsilon">The precision threshold.</param>
		/// <returns>True if they are perpendicular, false if not.</returns>
		/// <remarks>
		/// For performance reasons, it is recommended to use the methods with reference parameters.
		/// </remarks>
		public bool IsPerpendicular(Vector3 vector, float epsilon)
		{
			return Utility.Absolute(this.Dot(vector)) <= epsilon;
		}

		/// <summary>
		/// Checks whether this and the specified vector are perpendicular.
		/// </summary>
		/// <param name="vector">The other vector.</param>
		/// <returns>True if they are perpendicular, false if not.</returns>
		public bool IsPerpendicular(ref Vector3 vector)
		{
			return Utility.Absolute(this.Dot(vector)) <= float.Epsilon;
		}

		/// <summary>
		/// Checks whether this and the specified vector are perpendicular.
		/// </summary>
		/// <param name="vector">The other vector.</param>
		/// <returns>True if they are perpendicular, false if not.</returns>
		/// <remarks>
		/// For performance reasons, it is recommended to use the methods with reference parameters.
		/// </remarks>
		public bool IsPerpendicular(Vector3 vector)
		{
			return Utility.Absolute(this.Dot(vector)) <= float.Epsilon;
		}

		/// <summary>
		/// Computes the shortest quaternion-rotation to rotate the vector to the destination.
		/// </summary>
		/// <param name="destination">The destination to rotate to.</param>
		/// <param name="fallBackAxis">The axis to fall back at.</param>
		/// <returns>The shortest arc rotation.</returns>
		public Quaternion GetRotationTo(Vector3 destination, Vector3 fallBackAxis)
		{
			Quaternion q = Quaternion.Identity;
			Vector3 v0;
			Vector3 v1;
			this.Normalize(out v0);
			destination.Normalize(out v1);

			Vector3 cross = v0.Cross(v1);
			float dot = v0.Dot(v1);

			if (dot >= 1.0f)
				return q;

			if (dot < (0.000001f - 1.0f))
			{
				if (fallBackAxis == Vector3.Zero)
				{
					fallBackAxis = Vector3.Cross(Vector3.UnitX, this);
					if (fallBackAxis.LengthSquared <= float.Epsilon)
						fallBackAxis = Vector3.UnitY.Cross(this);

					fallBackAxis = fallBackAxis.Normalize();
				}
				q = Quaternion.FromAxisAngle(fallBackAxis, Constants.Pi);
			}
			else
			{
				float s = Utility.Sqrt((1.0f + dot) * 2.0f);
				q.XYZ = cross * (1.0f / s);
				q.W = s * 0.5f;
			}
			return q;
		}

		/// <summary>
		/// Computes the shortest quaternion-rotation to rotate the vector to the destination.
		/// </summary>
		/// <param name="destination">The destination to rotate to.</param>
		/// <returns>The shortest arc rotation.</returns>
		public Quaternion GetRotationTo(Vector3 destination)
		{
			return this.GetRotationTo(destination, Vector3.Zero);
		}
	}
}
