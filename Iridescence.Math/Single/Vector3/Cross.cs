﻿namespace Iridescence.Math
{
	public partial struct Vector3
	{
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static void Cross(in Vector3 left, in Vector3 right, out Vector3 result)
		{
			float x = left.Y * right.Z - left.Z * right.Y;
			float y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			result.X = x;
			result.Y = y;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static void Cross(Vector3 left, in Vector3 right, out Vector3 result)
		{
			float x = left.Y * right.Z - left.Z * right.Y;
			float y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			result.X = x;
			result.Y = y;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static void Cross(in Vector3 left, Vector3 right, out Vector3 result)
		{
			float x = left.Y * right.Z - left.Z * right.Y;
			float y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			result.X = x;
			result.Y = y;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static void Cross(Vector3 left, Vector3 right, out Vector3 result)
		{
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static Vector3 Cross(in Vector3 left, in Vector3 right)
		{
			Vector3 result;
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			return result;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static Vector3 Cross(Vector3 left, in Vector3 right)
		{
			Vector3 result;
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			return result;
		}

		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static Vector3 Cross(in Vector3 left, Vector3 right)
		{
			Vector3 result;
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			return result;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public static Vector3 Cross(Vector3 left, Vector3 right)
		{
			Vector3 result;
			result.X = left.Y * right.Z - left.Z * right.Y;
			result.Y = left.Z * right.X - left.X * right.Z;
			result.Z = left.X * right.Y - left.Y * right.X;
			return result;
		}

		/// <summary>
		/// Returns the cross product between the vectors.
		/// </summary>
		public Vector3 Cross(in Vector3 vector)
		{
			Vector3 result;
			result.X = this.Y * vector.Z - this.Z * vector.Y;
			result.Y = this.Z * vector.X - this.X * vector.Z;
			result.Z = this.X * vector.Y - this.Y * vector.X;
			return result;
		}
		
		/// <summary>
		/// Returns the cross product between the vectors. For performance reasons, it is recommended to use the methods with reference parameters.
		/// </summary>
		public Vector3 Cross(Vector3 vector)
		{
			Vector3 result;
			result.X = this.Y * vector.Z - this.Z * vector.Y;
			result.Y = this.Z * vector.X - this.X * vector.Z;
			result.Z = this.X * vector.Y - this.Y * vector.X;
			return result;
		}
	}
}
