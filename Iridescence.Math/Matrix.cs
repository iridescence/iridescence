﻿using System;
using System.Collections.Generic;

namespace Iridescence.Math
{
	/// <summary>
	/// Generic matrix methods and operations.
	/// </summary>
	public static class Matrix
	{
		#region Get

		/// <summary>
		/// Copies values from a portion of a matrix to an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceMatrix">The source matrix.</param>
		/// <param name="rowOffset">A row offset that specifies the first row in the matrix to set.</param>
		/// <param name="columnOffset">A column offset that specifies the first column in the matrix to set.</param>
		/// <param name="destArray">The destination array.</param>
		/// <param name="offset1">An offset applied to the first dimension of the source array.</param>
		/// <param name="offset2">An offset applied to the second dimension of the source array.</param>
		/// <param name="rowCount">The number of rows to set.</param>
		/// <param name="columnCount">The number of columns to set.</param>
		/// <param name="rowIsFirstDimension">Specifies the meaning of the dimensions of the source array. Specifying false means that the first dimension is the row and the second dimension is the column. True means the opposite.</param>
		public static void Get<T>(this IMatrix<T> sourceMatrix, int rowOffset, int columnOffset, T[,] destArray, int offset1, int offset2, int rowCount, int columnCount, bool rowIsFirstDimension)
		{
			int nRows = sourceMatrix.Rows;
			int nCols = sourceMatrix.Columns;

			if (rowOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(rowOffset));
			if (rowOffset + rowCount > nRows)
				throw new ArgumentOutOfRangeException(nameof(rowCount));
			if (columnOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(columnOffset));
			if (columnOffset + columnCount > nCols)
				throw new ArgumentOutOfRangeException(nameof(columnCount));

			if (offset1 < 0)
				throw new ArgumentOutOfRangeException(nameof(offset1));
			if (offset1 + rowCount > destArray.GetLength(rowIsFirstDimension ? 0 : 1))
				throw new ArgumentOutOfRangeException(nameof(rowCount));
			if (offset2 < 0)
				throw new ArgumentOutOfRangeException(nameof(offset2));
			if (offset2 + columnCount > destArray.GetLength(rowIsFirstDimension ? 1 : 0))
				throw new ArgumentOutOfRangeException(nameof(columnCount));
			
			for (int row = 0; row < rowCount; ++row)
			{
				for (int col = 0; col < columnCount; ++col)
				{
					if (rowIsFirstDimension)
						destArray[offset1 + row, offset2 + col] = sourceMatrix[rowOffset + row, columnOffset + col];
					else
						destArray[offset1 + col, offset2 + row] = sourceMatrix[rowOffset + row, columnOffset + col];
				}
			}
		}

		/// <summary>
		/// Copies values from a portion of a matrix to an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceMatrix">The source matrix.</param>
		/// <param name="rowOffset">A row offset that specifies the first row in the matrix to set.</param>
		/// <param name="columnOffset">A column offset that specifies the first column in the matrix to set.</param>
		/// <param name="destArray">The destination array.</param>
		/// <param name="offset1">An offset applied to the first dimension of the source array.</param>
		/// <param name="offset2">An offset applied to the second dimension of the source array.</param>
		/// <param name="rowCount">The number of rows to set.</param>
		/// <param name="columnCount">The number of columns to set.</param>
		public static void Get<T>(this IMatrix<T> sourceMatrix, int rowOffset, int columnOffset, T[,] destArray, int offset1, int offset2, int rowCount, int columnCount)
		{
			Get(sourceMatrix, rowOffset, columnOffset, destArray, offset1, offset2, rowCount, columnCount, true);
		}

		/// <summary>
		/// Copies values from a matrix to an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceMatrix">The source matrix.</param>
		/// <param name="destArray">The destination array.</param>
		public static void Get<T>(this IMatrix<T> sourceMatrix, T[,] destArray)
		{
			Get(sourceMatrix, 0, 0, destArray, 0, 0, sourceMatrix.Rows, sourceMatrix.Columns);
		}

		/// <summary>
		/// Copies values from an array to a portion of a matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceMatrix">The source matrix.</param>
		/// <param name="rowOffset">A row offset that specifies the first row in the matrix to set.</param>
		/// <param name="columnOffset">A column offset that specifies the first column in the matrix to set.</param>
		/// <param name="destArray">The destination array.</param>
		/// <param name="offset">An offset into the source array.</param>
		/// <param name="stride">The stride in the source array (i.e. the number of elements between the beginning of the major dimension).</param>
		/// <param name="rowCount">The number of rows to set.</param>
		/// <param name="columnCount">The number of columns to set.</param>
		/// <param name="layout">Specifies how the source array is layed out.</param>
		public static void Get<T>(this IMatrix<T> sourceMatrix, int rowOffset, int columnOffset, T[] destArray, int offset, int stride, int rowCount, int columnCount, MultidimensionalLayout layout)
		{
			int nRows = sourceMatrix.Rows;
			int nCols = sourceMatrix.Columns;

			if (layout != MultidimensionalLayout.RowMajor && layout != MultidimensionalLayout.ColumnMajor)
				throw new ArgumentException("Invalid layout.", nameof(layout));

			if (rowOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(rowOffset));
			if (rowOffset + rowCount > nRows)
				throw new ArgumentOutOfRangeException(nameof(rowCount));
			if (columnOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(columnOffset));
			if (columnOffset + columnCount > nCols)
				throw new ArgumentOutOfRangeException(nameof(columnCount));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));

			if (layout == MultidimensionalLayout.RowMajor)
			{
				if (offset + (rowCount - 1) * stride + columnCount > destArray.Length)
					throw new ArgumentOutOfRangeException(nameof(rowCount));
			}
			else
			{
				if (offset + (columnCount - 1) * stride + rowCount > destArray.Length)
					throw new ArgumentOutOfRangeException(nameof(rowCount));
			}

			for (int row = 0; row < rowCount; ++row)
			{
				for (int col = 0; col < columnCount; ++col)
				{
					int index;
					if (layout == MultidimensionalLayout.RowMajor)
						index = row * stride + col;
					else
						index = col * stride + row;

					destArray[offset + index] = sourceMatrix[rowOffset + row, columnOffset + col];
				}
			}
		}

		/// <summary>
		/// Copies values from a portion of a matrix to an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceMatrix">The source matrix.</param>
		/// <param name="rowOffset">A row offset that specifies the first row in the matrix to set.</param>
		/// <param name="columnOffset">A column offset that specifies the first column in the matrix to set.</param>
		/// <param name="destArray">The destination array.</param>
		/// <param name="offset">An offset into the source array.</param>
		/// <param name="stride">The stride in the source array (i.e. the number of elements between the beginning of the major dimension).</param>
		/// <param name="rowCount">The number of rows to set.</param>
		/// <param name="columnCount">The number of columns to set.</param>
		public static void Get<T>(this IMatrix<T> sourceMatrix, int rowOffset, int columnOffset, T[] destArray, int offset, int stride, int rowCount, int columnCount)
		{
			Get(sourceMatrix, rowOffset, columnOffset, destArray, offset, stride, rowCount, columnCount, MultidimensionalLayout.RowMajor);
		}

		/// <summary>
		/// Copies values from a portion of a matrix to an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceMatrix">The source matrix.</param>
		/// <param name="destArray">The destination array.</param>
		/// <param name="offset">An offset into the source array.</param>
		/// <param name="stride">The stride in the source array (i.e. the number of elements between the beginning of the major dimension).</param>
		public static void Get<T>(this IMatrix<T> sourceMatrix, T[] destArray, int offset, int stride)
		{
			Get(sourceMatrix, 0, 0, destArray, offset, stride, sourceMatrix.Rows, sourceMatrix.Columns);
		}

		/// <summary>
		/// Copies values from a matrix to an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceMatrix">The source matrix.</param>
		/// <param name="destArray">The destination array.</param>
		public static void Get<T>(this IMatrix<T> sourceMatrix, T[] destArray)
		{
			Get(sourceMatrix, 0, 0, destArray, 0, sourceMatrix.Columns, sourceMatrix.Rows, sourceMatrix.Columns);
		}

		#endregion

		#region Set

		/// <summary>
		/// Copies values from an array to a portion of a matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix to write to.</param>
		/// <param name="rowOffset">A row offset that specifies the first row in the matrix to set.</param>
		/// <param name="columnOffset">A column offset that specifies the first column in the matrix to set.</param>
		/// <param name="values">The source array.</param>
		/// <param name="offset1">An offset applied to the first dimension of the source array.</param>
		/// <param name="offset2">An offset applied to the second dimension of the source array.</param>
		/// <param name="rowCount">The number of rows to set.</param>
		/// <param name="columnCount">The number of columns to set.</param>
		/// <param name="rowIsFirstDimension">
		/// Determines the meaning of the dimensions of the source array.
		/// Specifying true determines that the first dimension addresses the row and the second dimension addresses the column.
		/// False means the opposite: The first dimension is the column, the second dimension is the row.
		/// </param>
		public static void Set<T>(this IMatrix<T> matrix, int rowOffset, int columnOffset, T[,] values, int offset1, int offset2, int rowCount, int columnCount, bool rowIsFirstDimension)
		{
			int nRows = matrix.Rows;
			int nCols = matrix.Columns;

			if (rowOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(rowOffset));
			if (rowOffset + rowCount > nRows)
				throw new ArgumentOutOfRangeException(nameof(rowCount));
			if (columnOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(columnOffset));
			if (columnOffset + columnCount > nCols)
				throw new ArgumentOutOfRangeException(nameof(columnCount));

			if (offset1 < 0)
				throw new ArgumentOutOfRangeException(nameof(offset1));
			if (offset1 + rowCount > values.GetLength(rowIsFirstDimension ? 0 : 1))
				throw new ArgumentOutOfRangeException(nameof(rowCount));
			if (offset2 < 0)
				throw new ArgumentOutOfRangeException(nameof(offset2));
			if (offset2 + columnCount > values.GetLength(rowIsFirstDimension ? 1 : 0))
				throw new ArgumentOutOfRangeException(nameof(columnCount));
			
			for (int row = 0; row < rowCount; ++row)
			{
				for (int col = 0; col < columnCount; ++col)
				{
					matrix[rowOffset + row, columnOffset + col] = rowIsFirstDimension ? values[offset1 + row, offset2 + col] : values[offset1 + col, offset2 + row];
				}
			}
		}

		/// <summary>
		/// Copies values from an array to a portion of a matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix to write to.</param>
		/// <param name="rowOffset">A row offset that specifies the first row in the matrix to set.</param>
		/// <param name="columnOffset">A column offset that specifies the first column in the matrix to set.</param>
		/// <param name="values">The source array.</param>
		/// <param name="offset1">An offset applied to the first dimension of the source array.</param>
		/// <param name="offset2">An offset applied to the second dimension of the source array.</param>
		/// <param name="rowCount">The number of rows to set.</param>
		/// <param name="columnCount">The number of columns to set.</param>
		public static void Set<T>(this IMatrix<T> matrix, int rowOffset, int columnOffset, T[,] values, int offset1, int offset2, int rowCount, int columnCount)
		{
			Set(matrix, rowOffset, columnOffset, values, offset1, offset2, rowCount, columnCount, true);
		}

		/// <summary>
		/// Copies values from an array to a a matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix to write to.</param>
		/// <param name="values">The source array.</param>
		public static void Set<T>(this IMatrix<T> matrix, T[,] values)
		{
			Set(matrix, 0, 0, values, 0, 0, matrix.Rows, matrix.Columns);
		}

		/// <summary>
		/// Copies values from an array to a portion of a matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix to write to.</param>
		/// <param name="rowOffset">A row offset that specifies the first row in the matrix to set.</param>
		/// <param name="columnOffset">A column offset that specifies the first column in the matrix to set.</param>
		/// <param name="values">The source array.</param>
		/// <param name="offset">An offset into the source array.</param>
		/// <param name="stride">The stride in the source array (i.e. the number of elements between the beginning of the major dimension).</param>
		/// <param name="rowCount">The number of rows to set.</param>
		/// <param name="columnCount">The number of columns to set.</param>
		/// <param name="layout">Specifies how the source array is layed out.</param>
		public static void Set<T>(this IMatrix<T> matrix, int rowOffset, int columnOffset, T[] values, int offset, int stride, int rowCount, int columnCount, MultidimensionalLayout layout)
		{
			int nRows = matrix.Rows;
			int nCols = matrix.Columns;

			if (layout != MultidimensionalLayout.RowMajor && layout != MultidimensionalLayout.ColumnMajor)
				throw new ArgumentException("Invalid layout.", nameof(layout));

			if (rowOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(rowOffset));
			if (rowOffset + rowCount > nRows)
				throw new ArgumentOutOfRangeException(nameof(rowCount));
			if (columnOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(columnOffset));
			if (columnOffset + columnCount > nCols)
				throw new ArgumentOutOfRangeException(nameof(columnCount));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));

			if (layout == MultidimensionalLayout.RowMajor)
			{
				if (offset + (rowCount - 1) * stride + columnCount > values.Length)
					throw new ArgumentOutOfRangeException(nameof(rowCount));
			}
			else
			{
				if (offset + (columnCount - 1) * stride + rowCount > values.Length)
					throw new ArgumentOutOfRangeException(nameof(rowCount));
			}

			for (int row = 0; row < rowCount; ++row)
			{
				for (int col = 0; col < columnCount; ++col)
				{
					int index;
					if (layout == MultidimensionalLayout.RowMajor)
						index = row * stride + col;
					else
						index = col * stride + row;

					matrix[rowOffset + row, columnOffset + col] = values[offset + index];
				}
			}
		}

		/// <summary>
		/// Copies values from an array to a portion of a matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix to write to.</param>
		/// <param name="rowOffset">A row offset that specifies the first row in the matrix to set.</param>
		/// <param name="columnOffset">A column offset that specifies the first column in the matrix to set.</param>
		/// <param name="values">The source array.</param>
		/// <param name="offset">An offset into the source array.</param>
		/// <param name="stride">The stride in the source array (i.e. the number of elements between the beginning of the major dimension).</param>
		/// <param name="rowCount">The number of rows to set.</param>
		/// <param name="columnCount">The number of columns to set.</param>
		public static void Set<T>(this IMatrix<T> matrix, int rowOffset, int columnOffset, T[] values, int offset, int stride, int rowCount, int columnCount)
		{
			Set(matrix, rowOffset, columnOffset, values, offset, stride, rowCount, columnCount, MultidimensionalLayout.RowMajor);
		}

		/// <summary>
		/// Copies values from an array to a matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix to write to.</param>
		/// <param name="values">The source array.</param>
		/// <param name="offset">An offset into the source array.</param>
		/// <param name="stride">The stride in the source array (i.e. the number of elements between the beginning of the major dimension).</param>
		public static void Set<T>(this IMatrix<T> matrix, T[] values, int offset, int stride)
		{
			Set(matrix, 0, 0, values, offset, stride, matrix.Rows, matrix.Columns);
		}

		/// <summary>
		/// Copies values from an array to a matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix to write to.</param>
		/// <param name="values">The source array.</param>
		public static void Set<T>(this IMatrix<T> matrix, T[] values)
		{
			Set(matrix, 0, 0, values, 0, matrix.Columns, matrix.Rows, matrix.Columns);
		}

		#endregion

		#region ForEach

		/// <summary>
		/// Performs an operation for each entry in the source matrix.
		/// </summary>
		/// <param name="matrix">The matrix.</param>
		/// <param name="action">An action invoked for each entry.</param>
		public static void ForEach<T>(this IMatrix<T> matrix, Action<T> action)
		{
			for (int row = 0; row < matrix.Rows; ++row)
			{
				for (int col = 0; col < matrix.Columns; ++col)
				{
					action(matrix[row, col]);
				}
			}
		}

		/// <summary>
		/// Performs an operation for each entry in the source matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix.</param>
		/// <param name="action">An action invoked for each entry. The row and column of the current entry are provided.</param>
		public static void ForEach<T>(this IMatrix<T> matrix, Action<T, int, int> action)
		{
			for (int row = 0; row < matrix.Rows; ++row)
			{
				for (int col = 0; col < matrix.Columns; ++col)
				{
					action(matrix[row, col], row, col);
				}
			}
		}
		
		/// <summary>
		/// Performs an operation for each entry in the source matrix and stores the result in another matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The source matrix.</param>
		/// <param name="func">A function invoked for each entry.</param>
		/// <param name="result">The destination matrix.</param>
		public static void ForEach<T>(this IMatrix<T> matrix, Func<T, T> func, IMatrix<T> result)
		{
			int rows = matrix.Rows;
			int cols = matrix.Columns;
			if (rows != result.Rows ||
			    cols != result.Columns)
				throw new ArgumentException("The matrices must be equal in size.");

			for (int row = 0; row < rows; ++row)
			{
				for (int col = 0; col < cols; ++col)
				{
					result[row, col] = func(matrix[row, col]);
				}
			}
		}

		/// <summary>
		/// Performs an operation for each entry in the source matrix and stores the result in another matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The source matrix.</param>
		/// <param name="func">A function invoked for each entry. The row and column of the current entry are provided.</param>
		/// <param name="result">The destination matrix.</param>
		public static void ForEach<T>(this IMatrix<T> matrix, Func<T, int, int, T> func, IMatrix<T> result)
		{
			int rows = matrix.Rows;
			int cols = matrix.Columns;
			if (rows != result.Rows ||
			    cols != result.Columns)
				throw new ArgumentException("The matrices must be equal in size.");

			for (int row = 0; row < rows; ++row)
			{
				for (int col = 0; col < cols; ++col)
				{
					result[row, col] = func(matrix[row, col], row, col);
				}
			}
		}

		/// <summary>
		/// Performs an operation for each entry in the two matrices and stores the result in another matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix1">The first operand.</param>
		/// <param name="matrix2">The second operand.</param>
		/// <param name="func">A function invoked for each entry.</param>
		/// <param name="result">The destination matrix.</param>
		public static void ForEach<T>(this IMatrix<T> matrix1, IMatrix<T> matrix2, Func<T, T, T> func, IMatrix<T> result) 
		{
			int rows = matrix1.Rows;
			int cols = matrix1.Columns;
			if (rows != matrix2.Rows ||
			    cols != matrix2.Columns ||
			    rows != result.Rows ||
			    cols != result.Columns)
				throw new ArgumentException("The matrices must be equal in size.");

			for (int row = 0; row < rows; ++row)
			{
				for (int col = 0; col < cols; ++col)
				{
					result[row, col] = func(matrix1[row, col], matrix2[row, col]);
				}
			}
		}

		/// <summary>
		/// Performs an operation for each entry in the two matrices and stores the result in another matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix1">The first operand.</param>
		/// <param name="matrix2">The second operand.</param>
		/// <param name="func">A function invoked for each entry. The row and column of the current entry are provided.</param>
		/// <param name="result">The destination matrix.</param>
		public static void ForEach<T>(this IMatrix<T> matrix1, IMatrix<T> matrix2, Func<T, T, int, int, T> func, IMatrix<T> result)
		{
			int rows = matrix1.Rows;
			int cols = matrix1.Columns;
			if (rows != matrix2.Rows ||
			    cols != matrix2.Columns ||
			    rows != result.Rows ||
			    cols != result.Columns)
				throw new ArgumentException("The matrices must be equal in size.");

			for (int row = 0; row < rows; ++row)
			{
				for (int col = 0; col < cols; ++col)
				{
					result[row, col] = func(matrix1[row, col], matrix2[row, col], row, col);
				}
			}
		}

		#endregion

		#region Equality

		/// <summary>
		/// Returns a value that indicates whether the two matrices are equal.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix1">The first matrix.</param>
		/// <param name="matrix2">The second matrix.</param>
		/// <param name="comparer">The equality comparer.</param>
		/// <returns>True, if the two matrices are equal. False otherwise.</returns>
		public static bool Equals<T>(IMatrix<T> matrix1, IMatrix<T> matrix2, IEqualityComparer<T> comparer)
		{
			int rows = matrix1.Rows;
			int cols = matrix1.Columns;
			if (rows != matrix2.Rows ||
			    cols != matrix2.Columns)
				return false;

			for (int row = 0; row < rows; ++row)
			{
				for (int col = 0; col < cols; ++col)
				{
					if (!comparer.Equals(matrix1[row, col], matrix2[row, col]))
						return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Returns a value that indicates whether the two matrices are equal.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix1">The first matrix.</param>
		/// <param name="matrix2">The second matrix.</param>
		/// <returns>True, if the two matrices are equal. False otherwise.</returns>
		public static bool Equals<T>(IMatrix<T> matrix1, IMatrix<T> matrix2)
		{
			return Equals(matrix1, matrix2, EqualityComparer<T>.Default);
		}

		#endregion

		#region + - * /

		/// <summary>
		/// Adds two matrices (entrywise).
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix1">The first operand.</param>
		/// <param name="matrix2">The second operand.</param>
		/// <param name="result">The result.</param>
		public static void Add<T>(IMatrix<T> matrix1, IMatrix<T> matrix2, IMatrix<T> result) 
		{
			ForEach(matrix1, matrix2, Operators<T>.Add, result);
		}

		/// <summary>
		/// Adds a value to each entry.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix.</param>
		/// <param name="value">The value to add.</param>
		/// <param name="result">The result.</param>
		public static void Add<T>(IMatrix<T> matrix, T value, IMatrix<T> result)
		{
			ForEach(matrix, entry => Operators<T>.Add(entry, value), result);
		}

		/// <summary>
		/// Subtracts two matrices (entrywise).
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix1">The first operand.</param>
		/// <param name="matrix2">The second operand.</param>
		/// <param name="result">The result.</param>
		public static void Subtract<T>(IMatrix<T> matrix1, IMatrix<T> matrix2, IMatrix<T> result)
		{
			ForEach(matrix1, matrix2, Operators<T>.Subtract, result);
		}

		/// <summary>
		/// Subtracts a value from each entry.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix.</param>
		/// <param name="value">The value to subtract.</param>
		/// <param name="result">The result.</param>
		public static void Subtract<T>(IMatrix<T> matrix, T value, IMatrix<T> result)
		{
			ForEach(matrix, entry => Operators<T>.Subtract(entry, value), result);
		}

		/// <summary>
		/// Subtracts a value from each entry.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value">The value to subtract.</param>
		/// <param name="matrix">The matrix.</param>
		/// <param name="result">The result.</param>
		public static void Subtract<T>(T value, IMatrix<T> matrix, IMatrix<T> result)
		{
			ForEach(matrix, entry => Operators<T>.Subtract(value, entry), result);
		}
		
		/// <summary>
		/// Multiplies each entry with a value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix.</param>
		/// <param name="value">The factor.</param>
		/// <param name="result">The result.</param>
		public static void Multiply<T>(IMatrix<T> matrix, T value, IMatrix<T> result)
		{
			ForEach(matrix, entry => Operators<T>.Multiply(entry, value), result);
		}

		/// <summary>
		/// Multiplies two matrices.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <param name="result"></param>
		public static void Multiply<T>(IMatrix<T> left, IMatrix<T> right, IMatrix<T> result)
		{
			int lRows = left.Rows;
			int lCols = left.Columns;
			int rRows = right.Rows;
			int rCols = right.Columns;

			if (lCols != rRows)
				throw new ArgumentException("The number of columns of the left matrix must match the number of rows on the right matrix.");

			if (result.Rows != lRows || result.Columns != rCols)
				throw new ArgumentException("The result matrix is too small", nameof(result));

			T[,] temp = new T[lRows, rCols];

			for (int row = 0; row < lRows; ++row)
			{
				for (int col = 0; col < rCols; ++col)
				{
					// Compute dot(left row, right col)
					T dot = default(T);
					bool first = true;
					for (int i = 0; i < lCols; ++i)
					{
						T a = left[row, i];
						T b = right[i, col];
						T product = Operators<T>.Multiply(a, b);

						if (first)
						{
							dot = product;
							first = false;
						}
						else
						{
							dot = Operators<T>.Add(dot, product);
						}
					}

					temp[row, col] = dot;
				}
			}

			Set(result, temp);
		}

		/// <summary>
		/// Divides each entry by a value.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix.</param>
		/// <param name="value">The divisor.</param>
		/// <param name="result">The result.</param>
		public static void Divide<T>(IMatrix<T> matrix, T value, IMatrix<T> result)
		{
			ForEach(matrix, entry => Operators<T>.Divide(entry, value), result);
		}

		/// <summary>
		/// Divides the value by each entry.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix">The matrix.</param>
		/// <param name="value">The divisor.</param>
		/// <param name="result">The result.</param>
		public static void Divide<T>(T value, IMatrix<T> matrix, IMatrix<T> result)
		{
			ForEach(matrix, entry => Operators<T>.Divide(value, entry), result);
		}

		#endregion

		#region Transpose

		/// <summary>
		/// Transposes a matrix.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="matrix"></param>
		/// <param name="result"></param>
		public static void Transpose<T>(this IMatrix<T> matrix, IMatrix<T> result)
		{
			int rows = matrix.Rows;
			int cols = matrix.Columns;

			if (result.Rows != cols || result.Columns != rows)
				throw new ArgumentException("The result matrix is incompatible with the specified input matrix.");

			if (rows == cols)
			{
				// Different algorithm in case result and matrix are the same instance.
				for (int row = 0; row < rows; ++row)
				{
					result[row, row] = matrix[row, row];
					for (int col = 0; col < row; ++col)
					{
						T temp = matrix[row, col];
						result[row, col] = matrix[col, row];
						result[col, row] = temp;
					}
				}
			}
			else
			{
				for (int row = 0; row < rows; ++row)
				{
					for (int col = 0; col < cols; ++col)
					{
						result[col, row] = matrix[row, col];
					}
				}
			}
		}

		#endregion
	}
}
