﻿using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	public static partial class Utility
	{
		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static sbyte Median(sbyte a, sbyte b, sbyte c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static byte Median(byte a, byte b, byte c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static short Median(short a, short b, short c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ushort Median(ushort a, ushort b, ushort c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Median(int a, int b, int c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static uint Median(uint a, uint b, uint c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long Median(long a, long b, long c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ulong Median(ulong a, ulong b, ulong c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Median(float a, float b, float c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Median(double a, double b, double c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}

		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static char Median(char a, char b, char c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}
		
		/// <summary>
		/// Returns the middle out of three values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static decimal Median(decimal a, decimal b, decimal c)
		{
			return Max(Min(a, b), Min(Max(a, b), c));
		}
	}
}
