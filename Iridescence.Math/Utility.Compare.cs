﻿using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	public static partial class Utility
	{
		/// <summary>
		/// Compares two floating-point numbers with tolerance.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="tolerance"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Compare(float x, float y, float tolerance)
		{
			if (Absolute(x) - Absolute(y) <= tolerance)
				return 0;
			
			if (x > y)
				return 1;

			return -1;
		}

		/// <summary>
		/// Compares two floating-point numbers with tolerance.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="tolerance"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Compare(double x, double y, double tolerance)
		{
			if (Absolute(x) - Absolute(y) <= tolerance)
				return 0;
			
			if (x > y)
				return 1;

			return -1;
		}

		/// <summary>
		/// Compares two floating-point numbers and returns whether their difference lies within the specified tolerance.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="tolerance"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool ApproximatelyEquals(float x, float y, float tolerance)
		{
			return Absolute(x) - Absolute(y) <= tolerance;
		}
		
		/// <summary>
		/// Compares two floating-point numbers and returns whether their difference lies within the specified tolerance.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="tolerance"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool ApproximatelyEquals(double x, double y, double tolerance)
		{
			return Absolute(x) - Absolute(y) <= tolerance;
		}
	}
}
