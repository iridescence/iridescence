﻿using System;

namespace Iridescence.Math
{
	/// <summary>
	/// Provides generic constants.
	/// If an implementor wants to use a custom type with the channel mapping infrastructure, the constants in this static class should be assigned first.
	/// </summary>
	public static class GenericConstants
	{
		/// <summary>
		/// Provides a constant 1 of type <typeparamref name="T"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		public static class One<T>
		{
			/// <summary>
			/// The value of the constant.
			/// </summary>
			public static T Value;
		}

		static GenericConstants()
		{
			One<bool>.Value = true;
			One<sbyte>.Value = 1;
			One<byte>.Value = 1;
			One<short>.Value = 1;
			One<ushort>.Value = 1;
			One<char>.Value = (char)1;
			One<int>.Value = 1;
			One<uint>.Value = 1;
			One<Int24>.Value = (Int24)1;
			One<UInt24>.Value = (UInt24)1;
			One<long>.Value = 1;
			One<ulong>.Value = 1;
			One<Half>.Value = (Half)1;
			One<float>.Value = 1;
			One<double>.Value = 1;
			One<SNorm8>.Value = SNorm8.One;
			One<UNorm8>.Value = UNorm8.One;
			One<SNorm16>.Value = SNorm16.One;
			One<UNorm16>.Value = UNorm16.One;
			One<SNorm24>.Value = SNorm24.One;
			One<UNorm24>.Value = UNorm24.One;
		}
	}
}