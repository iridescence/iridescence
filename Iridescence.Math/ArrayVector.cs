﻿using System;
using System.Globalization;
using System.Text;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a generic vector implemented as one-dimensional array.
	/// </summary>
	public class ArrayVector<T> : IVector<T>, IMatrix<T>, IEquatable<IVector<T>>, IFormattable
	{
		#region Properties

		/// <summary>
		/// Gets the array.
		/// </summary>
		public T[] Array { get; }

		/// <summary>
		/// Gets the number of elements in the vector.
		/// </summary>
		public int Count => this.Array.Length;

		public T this[int index]
		{
			get => this.Array[index];
			set => this.Array[index] = value;
		}

		int IMatrix<T>.Rows => this.Array.Length;

		int IMatrix<T>.Columns => 1;

		public T this[int row, int column]
		{
			get
			{
				if (column != 0)
					throw new ArgumentOutOfRangeException(nameof(column));
				return this.Array[row];
			}
			set
			{
				if (column != 0)
					throw new ArgumentOutOfRangeException(nameof(column));
				this.Array[row] = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ArrayVector{T}"/> class. 
		/// </summary>
		public ArrayVector(int count)
		{
			this.Array = new T[count];
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ArrayVector{T}"/> class. 
		/// </summary>
		public ArrayVector(T[] array)
		{
			this.Array = array;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ArrayVector{T}"/> class by copying an existing vector.
		/// </summary>
		/// <param name="vector">A vector to copy.</param>
		public ArrayVector(IVector<T> vector)
		{
			this.Array = new T[vector.Count];

			for (int i = 0; i < this.Count; ++i)
			{
				this.Array[i] = vector[i];
			}
		}

		#endregion

		#region Methods

		public bool Equals(IVector<T> other)
		{
			if (other == null)
				return false;

			if (other.Count != this.Count)
				return false;

			for (int i = 0; i < this.Count; ++i)
			{
				if (!object.Equals(this[i], other[i]))
					return false;
			}

			return true;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;

			IVector<T> other = obj as IVector<T>;
			return this.Equals(other);
		}

		public override int GetHashCode()
		{
			return (this.Array != null ? this.Array.GetHashCode() : 0);
		}

		public static bool operator ==(ArrayVector<T> left, ArrayVector<T> right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(ArrayVector<T> left, ArrayVector<T> right)
		{
			return !Equals(left, right);
		}

		/// <summary>
		/// Returns a string representation of the matrix.
		/// </summary>
		/// <param name="entryFormat">The format for entries (only applies if <see cref="T"/> implements <see cref="IFormattable"/>).</param>
		/// <param name="formatProvider">The format provider.</param>
		/// <returns></returns>
		public string ToString(string entryFormat, IFormatProvider formatProvider)
		{
			StringBuilder str = new StringBuilder();
			
			str.Append("{ ");

			for (int i = 0; i < this.Count; ++i)
			{
				T val = this.Array[i];
				IFormattable formattable = val as IFormattable;
				if (formattable != null)
					str.Append(formattable.ToString(entryFormat, formatProvider));
				else
					str.Append(val);
				str.Append(' ');
			}

			str.Append('}');
			return str.ToString();
		}

		/// <summary>
		/// Returns a string representation of the matrix.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return this.ToString(null, NumberFormatInfo.CurrentInfo);
		}

		#endregion
	}
}
