﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a cubic bezier curve.
	/// </summary>
	public struct CubicBezier : IEquatable<CubicBezier>, IParametricCurve, ISplittable<CubicBezier>
	{
		#region Fields

		/// <summary>
		/// The start point.
		/// </summary>
		public Vector2D Point1;

		/// <summary>
		/// The first control point.
		/// </summary>
		public Vector2D Point2;

		/// <summary>
		/// The second control point.
		/// </summary>
		public Vector2D Point3;

		/// <summary>
		/// The end point.
		/// </summary>
		public Vector2D Point4;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the point at the specified position.
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public Vector2D this[double t] => this.GetValue(t);

		/// <summary>
		/// Gets the bezier's scaling origin, which is the intersection of the start- and end-point normals.
		/// </summary>
		public Vector2D Origin
		{
			get
			{
				Vector2D tangent1;
				tangent1.X = this.Point2.Y - this.Point1.Y;
				tangent1.Y = this.Point2.X - this.Point1.X;
				Vector2D tangent2;
				tangent2.X = this.Point4.Y - this.Point3.Y;
				tangent2.Y = this.Point4.X - this.Point3.X;
				double c1 = tangent1.Y * this.Point1.X + tangent1.X * this.Point1.Y;
				double c2 = tangent2.Y * this.Point4.X + tangent2.X * this.Point4.Y;
				double det = tangent1.Y * tangent2.X - tangent2.Y * tangent1.X;
				Vector2D result;
				if (det <= double.Epsilon && det >= -double.Epsilon)
				{
					result.X = 0.5 * (this.Point1.X + this.Point4.X);
					result.Y = 0.5 * (this.Point1.Y + this.Point4.Y);
				}
				else
				{
					result.X = (tangent2.X * c1 - tangent1.X * c2) / det;
					result.Y = (tangent1.Y * c2 - tangent2.Y * c1) / det;
				}
				return result;
			}
		}

		/// <summary>
		/// Gets the derivative at <see cref="Point1"/>.
		/// </summary>
		public Vector2D Derivative1
		{
			get
			{
				Vector2D v;
				v.X = this.Point2.X - this.Point1.X;
				v.Y = this.Point2.Y - this.Point1.Y;
				return v;
			}
		}

		/// <summary>
		/// Gets the tangent at <see cref="Point1"/>.
		/// </summary>
		public Vector2D Tangent1
		{
			get
			{
				Vector2D v;
				v.X = this.Point2.X - this.Point1.X;
				v.Y = this.Point2.Y - this.Point1.Y;
				Vector2D.Normalize(in v, out v);
				return v;
			}
		}

		/// <summary>
		/// Gets the normal at <see cref="Point1"/>.
		/// </summary>
		public Vector2D Normal1
		{
			get
			{
				Vector2D v;
				v.X = this.Point1.Y - this.Point2.Y;
				v.Y = this.Point2.X - this.Point1.X;
				Vector2D.Normalize(in v, out v);
				return v;
			}
		}

		/// <summary>
		/// Gets the derivative at <see cref="Point4"/>.
		/// </summary>
		public Vector2D Derivative4
		{
			get
			{
				Vector2D v;
				v.X = this.Point4.X - this.Point3.X;
				v.Y = this.Point4.Y - this.Point3.Y;
				return v;
			}
		}
		
		/// <summary>
		/// Gets the tangent at <see cref="Point4"/>.
		/// </summary>
		public Vector2D Tangent4
		{
			get
			{
				Vector2D v;
				v.X = this.Point4.X - this.Point3.X;
				v.Y = this.Point4.Y - this.Point3.Y;
				Vector2D.Normalize(in v, out v);
				return v;
			}
		}
		
		/// <summary>
		/// Gets the normal at <see cref="Point4"/>.
		/// </summary>
		public Vector2D Normal4
		{
			get
			{
				Vector2D v;
				v.X = this.Point3.Y - this.Point4.Y;
				v.Y = this.Point4.X - this.Point3.X;
				Vector2D.Normalize(in v, out v);
				return v;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CubicBezier"/> class.
		/// </summary>
		public CubicBezier(Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
		{
			this.Point1 = p1;
			this.Point2 = p2;
			this.Point3 = p3;
			this.Point4 = p4;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the point at the specified position.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetValue(double t)
		{
			if (t < 0.0 || t > 1.0)
				throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

			Vector2D p12 = this.Point1 + t * (this.Point2 - this.Point1);
			Vector2D p23 = this.Point2 + t * (this.Point3 - this.Point2);
			Vector2D p34 = this.Point3 + t * (this.Point4 - this.Point3);
			Vector2D p123 = p12 + t * (p23 - p12);
			Vector2D p234 = p23 + t * (p34 - p23);
			return p123 + t * (p234 - p123);
		}

		/// <summary>
		/// Returns the point at the specified position, offset (i.e. moved along the normal) by the specified amount.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <param name="offset">The distance from the curve (along the normal at <see cref="t"/>).</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetOffsetValue(double t, double offset)
		{
			if (t < 0.0 || t > 1.0)
				throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

			Vector2D p12 = this.Point1 + t * (this.Point2 - this.Point1);
			Vector2D p23 = this.Point2 + t * (this.Point3 - this.Point2);
			Vector2D p34 = this.Point3 + t * (this.Point4 - this.Point3);
			Vector2D p123 = p12 + t * (p23 - p12);
			Vector2D p234 = p23 + t * (p34 - p23);
			Vector2D n = p234 - p123;
			Vector2D p = p123 + t * n;
			n.X = -n.Y;
			n.Y = n.X;
			Vector2D.Normalize(in n, out n);
			p += n * offset;
			return p;
		}

		/// <summary>
		/// Returns the derivative value at the specified position.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetDerivative(double t)
		{
			if (t < 0.0 || t > 1.0)
				throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

			Vector2D p12 = this.Point1 + t * (this.Point2 - this.Point1);
			Vector2D p23 = this.Point2 + t * (this.Point3 - this.Point2);
			Vector2D p34 = this.Point3 + t * (this.Point4 - this.Point3);
			Vector2D p123 = p12 + t * (p23 - p12);
			Vector2D p234 = p23 + t * (p34 - p23);
			return p234 - p123;
		}
		
		/// <summary>
		/// Returns the tangent at the specified position.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetTangent(double t)
		{
			Vector2D tangent = this.GetDerivative(t);
			tangent.Normalize(out tangent);
			return tangent;
		}

		/// <summary>
		/// Returns the tangent at the specified position.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetNormal(double t)
		{
			Vector2D tangent = this.GetTangent(t);
			return new Vector2D(-tangent.Y, tangent.X);
		}

		/// <summary>
		/// Splits this <see cref="CubicBezier"/> into two curves at the specified position.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <param name="first"></param>
		/// <param name="second"></param>
		[Pure]
		public void Split(double t, out CubicBezier first, out CubicBezier second)
		{
			if (t < 0.0 || t > 1.0)
				throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

			Vector2D p1 = this.Point1;
			Vector2D p4 = this.Point4;
			Vector2D p12 = p1 + t * (this.Point2 - p1);
			Vector2D p23 = this.Point2 + t * (this.Point3 - this.Point2);
			Vector2D p34 = this.Point3 + t * (p4 - this.Point3);
			Vector2D p123 = p12 + t * (p23 - p12);
			Vector2D p234 = p23 + t * (p34 - p23);
			Vector2D p1234 = p123 + t * (p234 - p123);

			first.Point1 = p1;
			first.Point2 = p12;
			first.Point3 = p123;
			first.Point4 = p1234;

			second.Point1 = p1234;
			second.Point2 = p234;
			second.Point3 = p34;
			second.Point4 = p4;
		}

		/// <summary>
		/// Splits this <see cref="CubicBezier"/> into multiple curves at the specified positions.
		/// </summary>
		/// <param name="positions">The curve positions in the range [0..1].</param>
		[Pure]
		public IEnumerable<CubicBezier> Split(IEnumerable<double> positions)
		{
			CubicBezier second = this;

			double tPrev = 0.0;
			foreach (double t in positions.OrderBy(t => t))
			{
				if (t < 0.0 || t > 1.0)
					throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

				if (t == tPrev)
				{
					yield return second;
					continue;
				}

				double relT = (t - tPrev) / (1.0 - tPrev);
				second.Split(relT, out CubicBezier first, out second);
				yield return first;
				tPrev = t;
			}

			yield return second;
		}

		/// <summary>
		/// Projects the specified point onto the curve.
		/// </summary>
		/// <param name="p">The point to project.</param>
		/// <returns>The closest 't' value.</returns>
		[Pure]
		public double Project(Vector2D p)
		{
			return this.Project(p, 4);
		}

		/// <summary>
		/// Projects the specified point onto the curve.
		/// </summary>
		/// <param name="p">The point to project.</param>
		/// <param name="iterations">The number of iterations. Each iteration improves the precision of the result.</param>
		/// <returns>The closest 't' value.</returns>
		[Pure]
		public double Project(Vector2D p, int iterations)
		{
			return this.Project(p, iterations, out _, out _);
		}

		/// <summary>
		/// Projects the specified point onto the curve.
		/// </summary>
		/// <param name="p">The point to project.</param>
		/// <param name="iterations">The number of iterations. Each iteration improves the precision of the result.</param>
		/// <param name="projected">The projected point.</param>
		/// <param name="distance">The distance between the specified point and the projected point.</param>
		/// <returns>The closest 't' value.</returns>
		[Pure]
		public double Project(Vector2D p, int iterations, out Vector2D projected, out double distance)
		{
			const int divisions = 64;
			const double step = 1.0 / divisions;

			double t = 0.0;
			double tMin = 0.0;
			double tRange = 1.0;

			projected = this.Point1;
			distance = double.MaxValue;

			for (int i = 0; i < iterations; ++i)
			{
				for (int j = 0; j <= divisions; ++j)
				{
					double newT = tMin + j * step * tRange;
					Vector2D newP = this.GetValue(newT);
					double newDistance = Vector2D.DistanceSquared(p, newP);
					if (newDistance < distance)
					{
						distance = newDistance;
						t = newT;
						projected = newP;
					}
				}

				tRange *= step;
				tMin = Utility.Min(Utility.Max(t - step * 0.5, 0.0), 1.0 - tRange);
			}

			return t;
		}

		private static void addPoints(Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4, double tolerance, int maxDepth, Action<Vector2D> callback)
		{
			if (maxDepth <= 0)
				return;

			// Check distance.
			Vector2D d;
			d.X = p4.X - p1.X;
			d.Y = p4.Y - p1.Y;
			double d2 = Utility.Absolute((p2.X - p4.X) * d.Y - (p2.Y - p4.Y) * d.X);
			double d3 = Utility.Absolute((p3.X - p4.X) * d.Y - (p3.Y - p4.Y) * d.X);
			double d23 = d2 + d3;
			if (d23 * d23 < tolerance * d.LengthSquared)
			{
				callback(p4);
				return;
			}

			// Split in the middle.
			Vector2D p12;
			p12.X = (p1.X + p2.X) * 0.5;
			p12.Y = (p1.Y + p2.Y) * 0.5;

			Vector2D p23;
			p23.X = (p2.X + p3.X) * 0.5;
			p23.Y = (p2.Y + p3.Y) * 0.5;

			Vector2D p34;
			p34.X = (p3.X + p4.X) * 0.5;
			p34.Y = (p3.Y + p4.Y) * 0.5;

			Vector2D p123;
			p123.X = (p12.X + p23.X) * 0.5;
			p123.Y = (p12.Y + p23.Y) * 0.5;

			Vector2D p234;
			p234.X = (p23.X + p34.X) * 0.5;
			p234.Y = (p23.Y + p34.Y) * 0.5;

			Vector2D p1234;
			p1234.X = (p123.X + p234.X) * 0.5;
			p1234.Y = (p123.Y + p234.Y) * 0.5;

			// Add left and right points.
			addPoints(p1, p12, p123, p1234, tolerance, maxDepth - 1, callback);
			addPoints(p1234, p234, p34, p4, tolerance, maxDepth - 1, callback);
		}
		
		/// <summary>
		/// Tessellates the curve recursively.
		/// </summary>
		/// <param name="tolerance">The approximate maximum distance between two tessellated points.</param>
		/// <param name="maxDepth">The maximum recursion depth.</param>
		/// <param name="callback">A callback that is called for each computed point.</param>
		public void Tessellate(double tolerance, int maxDepth, Action<Vector2D> callback)
		{
			addPoints(this.Point1, this.Point2, this.Point3, this.Point4, tolerance, maxDepth, callback);
		}

		private void offset(double offset, double tolerance, int depth, Action<CubicBezier, CubicBezier> callback)
		{
			const int maxDepth = 4;

			Vector2D origin = this.Origin;

			// Check whether this makes for a good candidate by checking what sides the control points are on.
			int side1 = LineSegmentD.Side(this.Point1, this.Point4, this.Point2);
			int side2 = LineSegmentD.Side(this.Point1, this.Point4, this.Point3);
			int side3 = LineSegmentD.Side(origin, this.Point1, this.Point2);
			int side4 = LineSegmentD.Side(this.Point1, this.Point2, this.Point4);
			int side5 = LineSegmentD.Side(origin, this.Point4, this.Point3);
			int side6 = LineSegmentD.Side(this.Point4, this.Point3, this.Point1);
			if ((side1 == side2 && side3 == side4 && side5 == side6) || depth > maxDepth)
			{
				// Check whether the center of the P1-P2-P3-P4 polygon and the curve mid are "close-enough".
				Vector2D polyMid = 0.25 * (this.Point1 + this.Point2 + this.Point3 + this.Point4);
				Vector2D curveMid = this.GetValue(0.5);
				if (Vector2D.DistanceSquared(polyMid, curveMid) <= tolerance * tolerance || depth > maxDepth)
				{
					// Scale the curve.
					CubicBezier scaled;
					scaled.Point1 = this.Point1 + this.Normal1 * offset;
					scaled.Point4 = this.Point4 + this.Normal4 * offset;
					scaled.Point2 = LineSegmentD.Intersect(scaled.Point1, scaled.Point1 + this.GetDerivative(0), origin, this.Point2);
					scaled.Point3 = LineSegmentD.Intersect(scaled.Point4, scaled.Point4 + this.GetDerivative(1), origin, this.Point3);

					// Completely degenerate case:
					if (double.IsInfinity(scaled.Point1.X) || double.IsInfinity(scaled.Point1.Y) ||
					    double.IsInfinity(scaled.Point2.X) || double.IsInfinity(scaled.Point2.Y) ||
					    double.IsInfinity(scaled.Point3.X) || double.IsInfinity(scaled.Point3.Y) ||
					    double.IsInfinity(scaled.Point4.X) || double.IsInfinity(scaled.Point4.Y))
						return;

					// Check for degenerate cases where the scaled curve has a very different relation between P1, P2 and P3, P4, in which case it is simply split again.
					double u1 = (this.Point2 - this.Point1).LengthSquared;
					double u2 = (this.Point3 - this.Point4).LengthSquared;
					double v1 = (scaled.Point2 - scaled.Point1).LengthSquared;
					double v2 = (scaled.Point3 - scaled.Point4).LengthSquared;
					double w1 = u1 / v1;
					double w2 = u2 / v2;

					// Compute a reasonable error tolerance that decreases the deeper we go.
					double minTol = 1.0;
					for (int i = 0; i <= depth; ++i)
					{
						minTol *= 0.75;
					}
					double maxTol = 1.0 / minTol;

					if (w1 > minTol && w1 < maxTol && 
						w2 > minTol && w2 < maxTol || depth > maxDepth)
					{
						if (depth > maxDepth)
						{
							// Correct some degenerate cases in case we can't split any further.
							// This limits how far out the control point can go to fix some issues at pinched parts of a curve.
							Vector2D limit = LineSegmentD.Intersect(scaled.Point1, scaled.Point2, scaled.Point3, scaled.Point4);
							if ((limit - scaled.Point1).LengthSquared < (scaled.Point2 - scaled.Point1).LengthSquared)
							{
								scaled.Point2 = limit;
							}
							else if ((limit - scaled.Point4).LengthSquared < (scaled.Point3 - scaled.Point4).LengthSquared)
							{
								scaled.Point3 = limit;
							}
						}

						callback(this, scaled);
						return;
					}
				}
			}

			// Split curve and try again.
			this.Split(0.5, out CubicBezier c1, out CubicBezier c2);
			c1.offset(offset, tolerance, depth + 1, callback);
			c2.offset(offset, tolerance, depth + 1, callback);
		}

		/// <summary>
		/// Offsets this curve by the specified amount and returns the resulting approximation.
		/// </summary>
		/// <param name="offset"></param>
		/// <param name="tolerance"></param>
		/// <returns></returns>
		public void Offset(double offset, double tolerance, Action<CubicBezier, CubicBezier> callback)
		{
			if (Utility.Absolute(offset) <= double.Epsilon)
			{
				callback(this, this);
				return;
			}

			foreach (CubicBezier bezier in this.Split(this.GetExtrema().Select(ex => ex.Value)))
			{
				bezier.offset(offset, tolerance, 0, callback);
			}
		}

		/// <summary>
		/// Returns the derivative curve as a <see cref="QuadraticBezier"/>.
		/// </summary>
		/// <returns></returns>
		[Pure]
		public QuadraticBezier GetDerivative()
		{
			return new QuadraticBezier(
				3.0 * (this.Point2 - this.Point1),
				3.0 * (this.Point3 - this.Point2),
				3.0 * (this.Point4 - this.Point3));
		}

		/// <summary>
		/// Returns the extremeties of the curve.
		/// </summary>
		[Pure]
		public IEnumerable<Extremum<double>> GetExtrema()
		{
			return this.GetExtrema(true);
		}

		/// <summary>
		/// Returns the extremeties of the curve.
		/// </summary>
		/// <param name="discardOutOfBounds">True to discard extrema that are not within the range [0..1].</param>
		[Pure]
		public IEnumerable<Extremum<double>> GetExtrema(bool discardOutOfBounds)
		{
			QuadraticBezier derivative = this.GetDerivative();

			IEnumerable<Extremum<double>> roots(double a, double b, double c)
			{
				double d = a - 2.0 * b + c;
				if (d != 0.0)
				{
					double m1 = -Utility.Sqrt(b * b - a * c);
					double m2 = -a + b;

					double t = -(m1 + m2) / d;
					double td = 2.0 * d * t + 2.0 * (b - a);
					if (!discardOutOfBounds || (t >= 0.0 && t <= 1.0))
						yield return new Extremum<double>(t, td < 0.0 ? ExtremumType.Maximum : td > 0.0 ? ExtremumType.Minimum : ExtremumType.Saddle);

					t = -(-m1 + m2) / d;
					td = 2.0 * d * t + 2.0 * (b - a);
					if (!discardOutOfBounds || (t >= 0.0 && t <= 1.0))
						yield return new Extremum<double>(t, td < 0.0 ? ExtremumType.Maximum : td > 0.0 ? ExtremumType.Minimum : ExtremumType.Saddle);
				}
				else if (b != c)
				{
					double t = (2.0 * b - c) / (2.0 * (b - c));
					double td = 2.0 * d * t + 2.0 * (b - a);
					if (!discardOutOfBounds || (t >= 0.0 && t <= 1.0))
						yield return new Extremum<double>(t, td < 0.0 ? ExtremumType.Maximum : td > 0.0 ? ExtremumType.Minimum : ExtremumType.Saddle);
				}
			}

			foreach (Extremum<double> ex in roots(derivative.Point1.X, derivative.Point2.X, derivative.Point3.X))
			{
				yield return ex;
			}

			foreach (Extremum<double> ex in roots(derivative.Point1.Y, derivative.Point2.Y, derivative.Point3.Y))
			{
				yield return ex;
			}
		}

		/// <summary>
		/// Returns an axis-aligned bounding rectangle of this bezier curve.
		/// </summary>
		/// <returns></returns>
		[Pure]
		public RectangleD GetBounds()
		{
			Vector2D min = new Vector2D(double.MaxValue);
			Vector2D max = new Vector2D(double.MinValue);

			Vector2D.Min(min, this.Point1, out min);
			Vector2D.Max(max, this.Point1, out max);

			Vector2D.Min(min, this.Point4, out min);
			Vector2D.Max(max, this.Point4, out max);

			foreach (Extremum<double> ex in this.GetExtrema())
			{
				Vector2D p = this[ex.Value];
				Vector2D.Min(min, p, out min);
				Vector2D.Max(max, p, out max);
			}

			return new RectangleD(min, max - min);
		}

		/// <summary>
		/// Returns a value that indicates whether the two specified <see cref="CubicBezier">CubicBeziers</see> are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(ref CubicBezier left, ref CubicBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y &&
			       left.Point4.X == right.Point4.X && left.Point4.Y == right.Point4.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether the two specified <see cref="CubicBezier">CubicBeziers</see> are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(ref CubicBezier left, CubicBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y &&
			       left.Point4.X == right.Point4.X && left.Point4.Y == right.Point4.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether the two specified <see cref="CubicBezier">CubicBeziers</see> are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(CubicBezier left, ref CubicBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y &&
			       left.Point4.X == right.Point4.X && left.Point4.Y == right.Point4.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether the two specified <see cref="CubicBezier">CubicBeziers</see> are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(CubicBezier left, CubicBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y &&
			       left.Point4.X == right.Point4.X && left.Point4.Y == right.Point4.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether this <see cref="CubicBezier"/> and the specified <see cref="CubicBezier"/> are equal.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		[Pure]
		public bool Equals(CubicBezier other)
		{
			return this.Point1.X == other.Point1.X && this.Point1.Y == other.Point1.Y &&
			       this.Point2.X == other.Point2.X && this.Point2.Y == other.Point2.Y &&
			       this.Point3.X == other.Point3.X && this.Point3.Y == other.Point3.Y &&
			       this.Point4.X == other.Point4.X && this.Point4.Y == other.Point4.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether this <see cref="CubicBezier"/> and the specified object are equal.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		[Pure]
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
				return false;
			return obj is CubicBezier && Equals((CubicBezier)obj);
		}

		/// <summary>
		/// Returns a hash code for this <see cref="CubicBezier"/>.
		/// </summary>
		/// <returns></returns>
		[Pure]
		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = this.Point1.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Point2.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Point3.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Point4.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(CubicBezier left, CubicBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y &&
			       left.Point4.X == right.Point4.X && left.Point4.Y == right.Point4.Y;
		}

		public static bool operator !=(CubicBezier left, CubicBezier right)
		{
			return left.Point1.X != right.Point1.X || left.Point1.Y != right.Point1.Y ||
			       left.Point2.X != right.Point2.X || left.Point2.Y != right.Point2.Y ||
			       left.Point3.X != right.Point3.X || left.Point3.Y != right.Point3.Y ||
			       left.Point4.X != right.Point4.X || left.Point4.Y != right.Point4.Y;
		}

		#endregion
	}
}
