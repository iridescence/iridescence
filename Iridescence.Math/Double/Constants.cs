﻿namespace Iridescence.Math
{
	/// <summary>
	/// Double-precision floating-point constants.
	/// </summary>
	public static class ConstantsD
	{
		/// <summary>
		/// Gets the natural constant pi with floating point precision.
		/// </summary>
		public const double Pi = 3.1415926535897932384626433832795d;

		/// <summary>
		/// Gets 1/2 of pi with floating point precision. 
		/// </summary>
		public const double PiOver2 = 1.5707963267948966192313216916398d;

		/// <summary>
		/// Gets 2 * pi with floating point precision. 
		/// </summary>
		public const double Pi2 = 6.283185307179586476925286766559d;

		/// <summary>
		/// Gets Euler's number as a double-precision floating point number.
		/// </summary>
		public const double E = 2.7182818284590452353602874713526624977572470936999595d;

		/// <summary>
		/// Gets the amount of radians per degree = pi / 180.
		/// </summary>
		public const double RadiansPerDegree = Pi / 180.0;

		/// <summary>
		/// Gets the amount of degrees per radians = 180 / pi.
		/// </summary>
		public const double DegreesPerRadian = 180.0 / Pi;
	}
}
