﻿namespace Iridescence.Math
{
	public partial struct Matrix3x3D
	{
		#region Translation

		/// <summary>
		/// Creates a translation matrix.
		/// </summary>
		/// <param name="translation">The translation vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateTranslation(ref Vector2D translation, out Matrix3x3D result)
		{
			result = Matrix3x3D.Identity;
			result.M13 = translation.X;
			result.M23 = translation.Y;
		}

		/// <summary>
		/// Creates a translation matrix.
		/// </summary>
		/// <param name="translation">The translation vector.</param>
		/// <returns>The resulting matrix.</returns>
		public static Matrix3x3D CreateTranslation(ref Vector2D translation)
		{
			Matrix3x3D result = Matrix3x3D.Identity;
			result.M13 = translation.X;
			result.M23 = translation.Y;
			return result;
		}
		
		/// <summary>
		/// Creates a translation matrix.
		/// </summary>
		/// <param name="translation">The translation vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateTranslation(Vector2D translation, out Matrix3x3D result)
		{
			result = Matrix3x3D.Identity;
			result.M13 = translation.X;
			result.M23 = translation.Y;
		}

		/// <summary>
		/// Creates a translation matrix.
		/// </summary>
		/// <param name="translation">The translation vector.</param>
		/// <returns>The resulting matrix.</returns>
		public static Matrix3x3D CreateTranslation(Vector2D translation)
		{
			Matrix3x3D result = Matrix3x3D.Identity;
			result.M13 = translation.X;
			result.M23 = translation.Y;
			return result;
		}

		#endregion

		#region Rotation

		/// <summary>
		/// Creates a rotation matrix based on an axis/angle rotation and stores the result in the specified matrix.
		/// </summary>
		/// <param name="axis">The axis to rotate about.</param>
		/// <param name="angle">The angle in radians (counter-clockwise).</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(Vector3D axis, double angle, out Matrix3x3D result)
		{
			// http://en.wikipedia.org/wiki/Rotation_matrix#Axis_and_angle
			axis = axis.Normalize();
			
			double c = Utility.Cos(angle);
			double s = Utility.Sin(angle);
			double C = 1.0 - c;
			
			double xxC = axis.X * axis.X * C;
			double xyC = axis.X * axis.Y * C;
			double xzC = axis.X * axis.Z * C;
			double yyC = axis.Y * axis.Y * C;
			double yzC = axis.Y * axis.Z * C;
			double zzC = axis.Z * axis.Z * C;
			
			double xs = s * axis.X;
			double ys = s * axis.Y;
			double zs = s * axis.Z;
			
			result.M11 = xxC + c;
			result.M12 = xyC - zs;
			result.M13 = xzC + ys;
			
			result.M21 = xyC + zs;
			result.M22 = yyC + c;
			result.M23 = yzC - xs;
			
			result.M31 = xzC - ys;
			result.M32 = yzC + xs;
			result.M33 = zzC + c;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on an axis/angle rotation and returns the result.
		/// </summary>
		/// <param name="axis">The axis to rotate about.</param>
		/// <param name="angle">The angle in radians (counter-clockwise).</param>
		public static Matrix3x3D CreateRotation(Vector3D axis, double angle)
		{
			Matrix3x3D result;
			Matrix3x3D.CreateRotation(axis, angle, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and stores the result in the specified matrix.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(ref Quaternion q, out Matrix3x3D result)
		{
			Quaternion qn = q.Normalize();
		
			double xx = qn.X * qn.X;
			double yy = qn.Y * qn.Y;
			double zz = qn.Z * qn.Z;
			double xy = qn.X * qn.Y;
			double zw = qn.Z * qn.W;
			double zx = qn.Z * qn.X;
			double yw = qn.Y * qn.W;
			double yz = qn.Y * qn.Z;
			double xw = qn.X * qn.W;
		
			result.M11 = 1.0 - (2.0 * (yy + zz));
			result.M21 = 2.0 * (xy + zw);
			result.M31 = 2.0 * (zx - yw);
		
			result.M12 = 2.0 * (xy - zw);
			result.M22 = 1.0 - (2.0 * (zz + xx));
			result.M32 = 2.0 * (yz + xw);
		
			result.M13 = 2.0 * (zx + yw);
			result.M23 = 2.0 * (yz - xw);
			result.M33 = 1.0 - (2.0 * (yy + xx));
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and stores the result in the specified matrix.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateRotation(Quaternion q, out Matrix3x3D result)
		{
			q = q.Normalize();
		
			double xx = q.X * q.X;
			double yy = q.Y * q.Y;
			double zz = q.Z * q.Z;
			double xy = q.X * q.Y;
			double zw = q.Z * q.W;
			double zx = q.Z * q.X;
			double yw = q.Y * q.W;
			double yz = q.Y * q.Z;
			double xw = q.X * q.W;
		
			result.M11 = 1.0 - (2.0 * (yy + zz));
			result.M21 = 2.0 * (xy + zw);
			result.M31 = 2.0 * (zx - yw);
		
			result.M12 = 2.0 * (xy - zw);
			result.M22 = 1.0 - (2.0 * (zz + xx));
			result.M32 = 2.0 * (yz + xw);
		
			result.M13 = 2.0 * (zx + yw);
			result.M23 = 2.0 * (yz - xw);
			result.M33 = 1.0 - (2.0 * (yy + xx));
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and returns the result.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		public static Matrix3x3D CreateRotation(ref Quaternion q)
		{
			Matrix3x3D result;
			Matrix3x3D.CreateRotation(ref q, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a rotation matrix based on a quaternion and returns the result.
		/// </summary>
		/// <param name="q">The quaternion.</param>
		public static Matrix3x3D CreateRotation(Quaternion q)
		{
			Matrix3x3D result;
			Matrix3x3D.CreateRotation(ref q, out result);
			return result;
		}

		#endregion

		#region Scale

		/// <summary>
		/// Creates a scale matrix.
		/// </summary>
		/// <param name="scale">The scale vector.</param>
		public static void CreateScale(ref Vector3D scale, out Matrix3x3D result)
		{
			result = Matrix3x3D.Zero;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
		}

		/// <summary>
		/// Creates a scale matrix.
		/// </summary>
		/// <param name="scale">The scale vector.</param>
		public static void CreateScale(Vector3D scale, out Matrix3x3D result)
		{
			result = Matrix3x3D.Zero;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
		}
		
		/// <summary>
		/// Creates a scale matrix.
		/// </summary>
		/// <param name="scale">The scale vector.</param>
		/// <returns></returns>
		public static Matrix3x3D CreateScale(ref Vector3D scale)
		{
			Matrix3x3D result = Matrix3x3D.Zero;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
			return result;
		}

		/// <summary>
		/// Creates a scale matrix.
		/// </summary>
		/// <param name="scale">The scale vector.</param>
		/// <returns></returns>
		public static Matrix3x3D CreateScale(Vector3D scale)
		{
			Matrix3x3D result = Matrix3x3D.Zero;
			result.M11 = scale.X;
			result.M22 = scale.Y;
			result.M33 = scale.Z;
			return result;
		}

		#endregion

		#region LookAt

		/// <summary>
		/// Creates a view matrix that looks towards the target position and stores the result in the specified matrix.
		/// </summary>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateLookAt(ref Vector3D target, ref Vector3D up, out Matrix3x3D result)
		{
			Vector3D x, y, z;
			
			Vector3D.Normalize(target, out z);
			Vector3D.Negate(z, out z);
			
			Vector3D.Cross(up, z, out x);
			Vector3D.Normalize(x, out x);
			
			Vector3D.Cross(z, x, out y);
			Vector3D.Normalize(y, out y);
			
			result.M11 = x.X;
			result.M12 = x.Y;
			result.M13 = x.Z;
			
			result.M21 = y.X;
			result.M22 = y.Y;
			result.M23 = y.Z;
			
			result.M31 = z.X;
			result.M32 = z.Y;
			result.M33 = z.Z;
		}
		
		/// <summary>
		/// Creates a view matrix that looks towards the target position and stores the result in the specified matrix.
		/// </summary>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		/// <param name="result">The resulting matrix.</param>
		public static void CreateLookAt(Vector3D target, Vector3D up, out Matrix3x3D result)
		{
			Matrix3x3D.CreateLookAt(ref target, ref up, out result);
		}
		
		/// <summary>
		/// Creates a view matrix that looks towards the target position and returns the result.
		/// </summary>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		public static Matrix3x3D CreateLookAt(ref Vector3D target, ref Vector3D up)
		{
			Matrix3x3D result;
			Matrix3x3D.CreateLookAt(ref target, ref up, out result);
			return result;
		}
		
		/// <summary>
		/// Creates a view matrix that looks towards the target position and returns the result.
		/// </summary>
		/// <param name="target">The target position to look at.</param>
		/// <param name="up">The up vector.</param>
		public static Matrix3x3D CreateLookAt(Vector3D target, Vector3D up)
		{
			Matrix3x3D result;
			Matrix3x3D.CreateLookAt(ref target, ref up, out result);
			return result;
		}

		#endregion
	}
}
