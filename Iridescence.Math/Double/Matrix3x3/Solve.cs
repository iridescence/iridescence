﻿namespace Iridescence.Math
{
	public partial struct Matrix3x3D
	{
		#region Vector2D

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix3x3D a, ref Vector2D b, out Vector2D x)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix3x3D a, Vector2D b, out Vector2D x)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix3x3D a, ref Vector2D b, out Vector2D x)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix3x3D a, Vector2D b, out Vector2D x)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2D Solve(ref Matrix3x3D a, ref Vector2D b)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			Vector2D x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2D Solve(ref Matrix3x3D a, Vector2D b)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			Vector2D x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2D Solve(Matrix3x3D a, ref Vector2D b)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			Vector2D x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A. Only the upper-left 2x2 sub-matrix is used.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2D Solve(Matrix3x3D a, Vector2D b)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			Vector2D x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		#endregion

		#region Vector3D

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix3x3D a, ref Vector3D b, out Vector3D x)
		{
			Vector3D c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;
			
			Vector3D cx;
			Vector3D.Cross(in c2, in c3, out cx);
			double det = Vector3D.Dot(c1, cx);
			if (det != 0.0)
				det = 1.0 / det;

			Vector3D.Cross(in c2, in c3, out cx);
			x.X = det * Vector3D.Dot(b, cx);

			Vector3D.Cross(in b, in c3, out cx);
			x.Y = det * Vector3D.Dot(c1, cx);

			Vector3D.Cross(in c2, in b, out cx);
			x.Z = det * Vector3D.Dot(c1, cx);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix3x3D a, Vector3D b, out Vector3D x)
		{
			Vector3D c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3D cx;
			Vector3D.Cross(in c2, in c3, out cx);
			double det = Vector3D.Dot(c1, cx);
			if (det != 0.0)
				det = 1.0 / det;

			Vector3D.Cross(in c2, in c3, out cx);
			x.X = det * Vector3D.Dot(b, cx);

			Vector3D.Cross(in b, in c3, out cx);
			x.Y = det * Vector3D.Dot(c1, cx);

			Vector3D.Cross(in c2, in b, out cx);
			x.Z = det * Vector3D.Dot(c1, cx);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix3x3D a, ref Vector3D b, out Vector3D x)
		{
			Vector3D c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3D cx;
			Vector3D.Cross(in c2, in c3, out cx);
			double det = Vector3D.Dot(c1, cx);
			if (det != 0.0)
				det = 1.0 / det;

			Vector3D.Cross(in c2, in c3, out cx);
			x.X = det * Vector3D.Dot(b, cx);

			Vector3D.Cross(in b, in c3, out cx);
			x.Y = det * Vector3D.Dot(c1, cx);

			Vector3D.Cross(in c2, in b, out cx);
			x.Z = det * Vector3D.Dot(c1, cx);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix3x3D a, Vector3D b, out Vector3D x)
		{
			Vector3D c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3D cx;
			Vector3D.Cross(in c2, in c3, out cx);
			double det = Vector3D.Dot(c1, cx);
			if (det != 0.0)
				det = 1.0 / det;

			Vector3D.Cross(in c2, in c3, out cx);
			x.X = det * Vector3D.Dot(b, cx);

			Vector3D.Cross(in b, in c3, out cx);
			x.Y = det * Vector3D.Dot(c1, cx);

			Vector3D.Cross(in c2, in b, out cx);
			x.Z = det * Vector3D.Dot(c1, cx);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector3D Solve(ref Matrix3x3D a, ref Vector3D b)
		{
			Vector3D x;

			Vector3D c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3D cx;
			Vector3D.Cross(in c2, in c3, out cx);
			double det = Vector3D.Dot(c1, cx);
			if (det != 0.0)
				det = 1.0 / det;

			Vector3D.Cross(in c2, in c3, out cx);
			x.X = det * Vector3D.Dot(b, cx);

			Vector3D.Cross(in b, in c3, out cx);
			x.Y = det * Vector3D.Dot(c1, cx);

			Vector3D.Cross(in c2, in b, out cx);
			x.Z = det * Vector3D.Dot(c1, cx);

			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector3D Solve(ref Matrix3x3D a, Vector3D b)
		{
			Vector3D x;

			Vector3D c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3D cx;
			Vector3D.Cross(in c2, in c3, out cx);
			double det = Vector3D.Dot(c1, cx);
			if (det != 0.0)
				det = 1.0 / det;

			Vector3D.Cross(in c2, in c3, out cx);
			x.X = det * Vector3D.Dot(b, cx);

			Vector3D.Cross(in b, in c3, out cx);
			x.Y = det * Vector3D.Dot(c1, cx);

			Vector3D.Cross(in c2, in b, out cx);
			x.Z = det * Vector3D.Dot(c1, cx);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector3D Solve(Matrix3x3D a, ref Vector3D b)
		{
			Vector3D x;

			Vector3D c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3D cx;
			Vector3D.Cross(in c2, in c3, out cx);
			double det = Vector3D.Dot(c1, cx);
			if (det != 0.0)
				det = 1.0 / det;

			Vector3D.Cross(in c2, in c3, out cx);
			x.X = det * Vector3D.Dot(b, cx);

			Vector3D.Cross(in b, in c3, out cx);
			x.Y = det * Vector3D.Dot(c1, cx);

			Vector3D.Cross(in c2, in b, out cx);
			x.Z = det * Vector3D.Dot(c1, cx);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector3D Solve(Matrix3x3D a, Vector3D b)
		{
			Vector3D x;

			Vector3D c1, c2, c3;
			c1.X = a.M11; c2.X = a.M21; c3.X = a.M31;
			c1.Y = a.M12; c2.Y = a.M22; c3.Y = a.M32;
			c1.Z = a.M13; c2.Z = a.M23; c3.Z = a.M33;

			Vector3D cx;
			Vector3D.Cross(in c2, in c3, out cx);
			double det = Vector3D.Dot(c1, cx);
			if (det != 0.0)
				det = 1.0 / det;

			Vector3D.Cross(in c2, in c3, out cx);
			x.X = det * Vector3D.Dot(b, cx);

			Vector3D.Cross(in b, in c3, out cx);
			x.Y = det * Vector3D.Dot(c1, cx);

			Vector3D.Cross(in c2, in b, out cx);
			x.Z = det * Vector3D.Dot(c1, cx);
			return x;
		}

		#endregion
	}
}
