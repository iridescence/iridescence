﻿using System;

namespace Iridescence.Math
{
	public partial struct Matrix3x3D
	{
		public Matrix3x3D Invert()
		{
			Matrix3x3D result;
			Matrix3x3D.Invert(in this, out result);
			return result;
		}

		public void Invert(out Matrix3x3D result)
		{
			Matrix3x3D.Invert(in this, out result);
		}

		public static void Invert(in Matrix3x3D matrix, out Matrix3x3D result)
		{
			double d = matrix.Determinant;
			if (d >= -double.Epsilon && d <= double.Epsilon)
				throw new InvalidOperationException("This matrix can't be inverted.");

			d = 1.0 / d;
			
			double m11 = (matrix.M22 * matrix.M33 - matrix.M23 * matrix.M32) * d;
			double m12 = (matrix.M13 * matrix.M32 - matrix.M12 * matrix.M33) * d;
			double m13 = (matrix.M12 * matrix.M23 - matrix.M22 * matrix.M13) * d;
		
			double m21 = (matrix.M23 * matrix.M31 - matrix.M33 * matrix.M21) * d;
			double m22 = (matrix.M11 * matrix.M33 - matrix.M31 * matrix.M13) * d;
			double m23 = (matrix.M13 * matrix.M21 - matrix.M23 * matrix.M11) * d;
		
			double m31 = (matrix.M21 * matrix.M32 - matrix.M31 * matrix.M22) * d;
			double m32 = (matrix.M12 * matrix.M31 - matrix.M32 * matrix.M11) * d;
			double m33 = (matrix.M11 * matrix.M22 - matrix.M21 * matrix.M12) * d;

			result.M11 = m11; result.M12 = m12; result.M13 = m13;
			result.M21 = m21; result.M22 = m22; result.M23 = m23;
			result.M31 = m31; result.M32 = m32; result.M33 = m33;
		}
	}
}
