﻿using System;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a number in the ComplexD plane.
	/// </summary>
	[Serializable]
	public struct ComplexD : IEquatable<ComplexD>
	{
		#region Fields

		#region Instance

		/// <summary>
		/// Real part of the ComplexD number.
		/// </summary>
		public double Real;

		/// <summary>
		/// Imaginary part of the ComplexD number
		/// </summary>
		public double Imaginary;

		#endregion

		#region Static

		/// <summary>
		/// Real number zero.
		/// </summary>
		public static readonly ComplexD Zero = new ComplexD(0.0, 0.0);

		/// <summary>
		/// Real number one.
		/// </summary>
		public static readonly ComplexD One = new ComplexD(1.0, 0.0);

		/// <summary>
		/// Imaginary number I.
		/// </summary>
		public static readonly ComplexD I = new ComplexD(0.0, 1.0);

		/// <summary>
		/// Real positive infinity.
		/// </summary>
		public static readonly ComplexD PositiveInfinity = new ComplexD(double.PositiveInfinity, 0.0);

		/// <summary>
		/// Real negative infinity.
		/// </summary>
		public static readonly ComplexD NegativeInfinity = new ComplexD(double.NegativeInfinity, 0.0);

		#endregion

		#endregion

		#region Properties

		#region Instance

		/// <summary>
		/// Gets the magnitude of the ComplexD number.
		/// </summary>
		public double Magnitude => Utility.Sqrt(this.Real * this.Real + this.Imaginary + this.Imaginary);

		/// <summary>
		/// Gets the squared magnitude of the ComplexD number.
		/// </summary>
		public double MagnitudeSquared => this.Real * this.Real + this.Imaginary * this.Imaginary;

		/// <summary>
		/// Gets the phase of the ComplexD number.
		/// </summary>
		public double Phase => Utility.Atan2(this.Imaginary, this.Real);

		/// <summary>
		/// Gets the ComplexD conjugate of the ComplexD number.
		/// </summary>
		public ComplexD Conjugate
		{
			get
			{
				ComplexD c;
				c.Real = this.Real;
				c.Imaginary = -this.Imaginary;
				return c;
			}
		}

		#endregion

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ComplexD number.
		/// </summary>
		public ComplexD(double real)
		{
			this.Real = real;
			this.Imaginary = 0.0;
		}

		/// <summary>
		/// Creates a new ComplexD number.
		/// </summary>
		/// <param name="real">The real part of the ComplexD number.</param>
		/// <param name="imaginary">The imaginary part of the ComplexD number.</param>
		public ComplexD(double real, double imaginary)
		{
			this.Real = real;
			this.Imaginary = imaginary;
		}

		#endregion

		#region Methods

		#region Instance

		public bool Equals(ComplexD other)
		{
			return this.Real.Equals(other.Real) && 
			       this.Imaginary.Equals(other.Imaginary);
		}
		
		public override bool Equals(object obj)
		{
			if (!(obj is ComplexD other))
				return false;

			return this.Equals(other);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (this.Real.GetHashCode() * 397) ^ this.Imaginary.GetHashCode();
			}
		}

		public override string ToString()
		{
			return $"{this.Real} + {this.Imaginary}i";
		}

		public ComplexD AddMult(double v, in ComplexD c)
		{
			ComplexD result;
			result.Real = this.Real + v * c.Real;
			result.Imaginary = this.Imaginary + v * c.Imaginary;
			return result;
		}

		#endregion

		#region Static

		public static ComplexD Polar(double rho, double theta)
		{
			ComplexD c;
			c.Real = rho * Utility.Cos(theta);
			c.Imaginary = rho * Utility.Sin(theta);
			return c;
		}

		private static double MagnitudeAndScaleFactor(ComplexD a, out int exp)
		{
			exp = 0;

			if (double.IsInfinity(a.Real) || double.IsInfinity(a.Imaginary))
				return double.PositiveInfinity; // at least one component is INF

			if (double.IsNaN(a.Real))
				return (a.Real); //	real component is NaN

			if (double.IsNaN(a.Imaginary))
				return (a.Imaginary); // imaginary component is NaN

			// neither component is NaN or INF
			if (a.Real < 0.0)
				a.Real = -a.Real;

			if (a.Imaginary < 0.0)
				a.Imaginary = -a.Imaginary;

			if (a.Real < a.Imaginary)
			{
				// ensure that |a.Imaginary| <= |a.Real|
				double _Tmp1 = a.Real;
				a.Real = a.Imaginary;
				a.Imaginary = _Tmp1;
			}

			if (a.Real == 0.0)
				return (a.Real); // |0| == 0
			
			if (1.0 <= a.Real)
			{
				exp = 2;
				a.Real = a.Real * 0.25;
				a.Imaginary = a.Imaginary * 0.25;
			}
			else
			{
				exp = -2;
				a.Real = a.Real * 4.0;
				a.Imaginary = a.Imaginary * 4.0;
			}

			double _Tmp = a.Real - a.Imaginary;
			
			if (_Tmp == a.Real)
				return (a.Real); // a.Imaginary unimportant

			if (a.Imaginary < _Tmp)
			{
				// use simple approximation
				double _Qv = a.Real / a.Imaginary;
				return (a.Real + a.Imaginary / (_Qv + Utility.Sqrt(_Qv * _Qv + 1.0f)));
			}
			else
			{
				// use 1 1/2 precision to preserve bits
				const double _Root2 = 1.4142135623730950488016887242096981;
				const double _Oneplusroot2high = (10125945.0 / 4194304.0); // exact if prec >= 24 bits
				const double _Oneplusroot2low = 1.4341252375973918872420969807856967e-7;

				double _Qv = _Tmp / a.Imaginary;
				double _Rv = (_Qv + 2.0) * _Qv;
				double _Sv = _Rv / (_Root2 + Utility.Sqrt(_Rv + 2.0)) + _Oneplusroot2low + _Qv + _Oneplusroot2high;
				return (a.Real + a.Imaginary / _Sv);
			}
		}

		public static ComplexD Sqrt(in ComplexD a)
		{
			// return sqrt(ComplexD)
			int exp;
			double _Rho = MagnitudeAndScaleFactor(a, out exp); // get magnitude and scale factor

			if (exp == 0.0)
				return (new ComplexD(_Rho, _Rho)); // argument is zero, INF, or NaN
			
			// compute in safest quadrant
			double _Realmag = (a.Real < 0.0 ? -a.Real : a.Real) * Utility.Pow(2.0, -exp);
			_Rho = (Utility.Sqrt(2.0 * (_Realmag + _Rho))) * Utility.Pow(2.0, exp / 2.0 - 1.0);

			if (0.0 <= a.Real)
				return (new ComplexD(_Rho, a.Imaginary / (2.0 * _Rho)));

			if (a.Imaginary < 0.0)
				return (new ComplexD(-a.Imaginary / (2.0 * _Rho), -_Rho));

			return (new ComplexD(a.Imaginary / (2.0 * _Rho), _Rho));
		}

		#endregion

		#endregion

		#region Operators

		public static bool operator ==(in ComplexD a, in ComplexD b)
		{
			return a.Real == b.Real && a.Imaginary == b.Imaginary;
		}

		public static bool operator !=(in ComplexD a, in ComplexD b)
		{
			return a.Real != b.Real || a.Imaginary != b.Imaginary;
		}

		public static ComplexD operator +(in ComplexD a, in ComplexD b)
		{
			ComplexD c;
			c.Real = a.Real + b.Real;
			c.Imaginary = a.Imaginary + b.Imaginary;
			return c;
		}

		public static ComplexD operator +(double a, in ComplexD b)
		{
			ComplexD c;
			c.Real = a + b.Real;
			c.Imaginary = b.Imaginary;
			return c;
		}


		public static ComplexD operator +(in ComplexD a, double b)
		{
			ComplexD c;
			c.Real = a.Real + b;
			c.Imaginary = a.Imaginary;
			return c;
		}

		public static ComplexD operator +(in ComplexD a)
		{
			ComplexD c;
			c.Real = +a.Real;
			c.Imaginary = +a.Imaginary;
			return c;
		}

		public static ComplexD operator -(in ComplexD a, in ComplexD b)
		{
			ComplexD c;
			c.Real = a.Real - b.Real;
			c.Imaginary = a.Imaginary - b.Imaginary;
			return c;
		}

		public static ComplexD operator -(in ComplexD a, double b)
		{
			ComplexD c;
			c.Real = a.Real - b;
			c.Imaginary = a.Imaginary;
			return c;
		}

		public static ComplexD operator -(double a, in ComplexD b)
		{
			ComplexD c;
			c.Real = a - b.Real;
			c.Imaginary = -b.Imaginary;
			return c;
		}

		public static ComplexD operator -(in ComplexD a)
		{
			ComplexD c;
			c.Real = -a.Real;
			c.Imaginary = -a.Imaginary;
			return c;
		}

		public static ComplexD operator *(in ComplexD a, in ComplexD b)
		{
			ComplexD c;
			c.Real = a.Real * b.Real - a.Imaginary * b.Imaginary;
			c.Imaginary = a.Real * b.Imaginary + a.Imaginary * b.Real;
			return c;
		}

		public static ComplexD operator *(in ComplexD a, double b)
		{
			ComplexD c;
			c.Real = a.Real * b;
			c.Imaginary = a.Imaginary * b;
			return c;
		}

		public static ComplexD operator /(in ComplexD a, in ComplexD b)
		{
			double magSq = b.MagnitudeSquared;
			magSq = 1.0 / magSq;
			ComplexD c;
			c.Real = (a.Real * b.Real + a.Imaginary * b.Imaginary) * magSq;
			c.Imaginary = (a.Imaginary * b.Real - a.Real * b.Imaginary) * magSq;
			return c;
		}

		public static ComplexD operator /(in ComplexD a, double b)
		{
			ComplexD c;
			c.Real = a.Real / b;
			c.Imaginary = a.Imaginary / b;
			return c;
		}

		public static ComplexD operator /(double a, in ComplexD b)
		{
			ComplexD c;
			c.Real = a / b.Real;
			c.Imaginary = a / b.Imaginary;
			return c;
		}

		public static implicit operator ComplexD(double a)
		{
			ComplexD c;
			c.Real = a;
			c.Imaginary = 0.0;
			return c;
		}

		#endregion
	}
}
