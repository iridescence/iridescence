﻿namespace Iridescence.Math
{
	/// <summary>
	/// Interface for curves that can be split at a specific position.
	/// </summary>
	public interface ISplittable<T>
	{
		/// <summary>
		/// Splits the curve into two sub-curves at the specified position.
		/// </summary>
		/// <param name="t"></param>
		/// <param name="first"></param>
		/// <param name="second"></param>
		void Split(double t, out T first, out T second);
	}
}
