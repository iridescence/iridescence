﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Iridescence.Math
{
	public partial struct Vector3D
	{
		[System.Diagnostics.Contracts.Pure]
		public double Dot(in Vector3D value)
		{
			return this.X * value.X + this.Y * value.Y + this.Z * value.Z;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static double Dot(in Vector3D left, in Vector3D right)
		{
			return left.X * right.X + left.Y * right.Y + left.Z * right.Z;
		}
		
	}
}
