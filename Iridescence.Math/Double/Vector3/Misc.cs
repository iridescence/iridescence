﻿namespace Iridescence.Math
{
	public partial struct Vector3D
	{
		/// <summary>
		/// Returns the angle between the two vectors.
		/// </summary>
		/// <param name="vector">The second vector.</param>
		/// <returns>The angle between the two vectors in radians.</returns>
		public double Angle(ref Vector3D vector)
		{
			double lp = this.Length * vector.Length;
			if (lp > 0.0)
				return Utility.Acos(this.Dot(vector) / lp);
			return double.NaN;
		}

		/// <summary>
		/// Returns the angle between the two vectors. For performance reasons, it is recommended to use the methods with reference parameters.
		/// </summary>
		/// <param name="vector">The second vector.</param>
		/// <returns>The angle between the two vectors in radians.</returns>
		public double Angle(Vector3D vector)
		{
			double lp = this.Length * vector.Length;
			if (lp > 0.0)
				return Utility.Acos(this.Dot(vector) / lp);
			return double.NaN;
		}

		/// <summary>
		/// Checks whether this and the specified vector are perpendicular.
		/// </summary>
		/// <param name="vector">The other vector.</param>
		/// <param name="epsilon">The precision threshold.</param>
		/// <returns>True if they are perpendicular, false if not.</returns>
		public bool IsPerpendicular(ref Vector3D vector, double epsilon)
		{
			return Utility.Absolute(this.Dot(vector)) <= epsilon;
		}

		/// <summary>
		/// Checks whether this and the specified vector are perpendicular.
		/// </summary>
		/// <param name="vector">The other vector.</param>
		/// <param name="epsilon">The precision threshold.</param>
		/// <returns>True if they are perpendicular, false if not.</returns>
		/// <remarks>
		/// For performance reasons, it is recommended to use the methods with reference parameters.
		/// </remarks>
		public bool IsPerpendicular(Vector3D vector, double epsilon)
		{
			return Utility.Absolute(this.Dot(vector)) <= epsilon;
		}

		/// <summary>
		/// Checks whether this and the specified vector are perpendicular.
		/// </summary>
		/// <param name="vector">The other vector.</param>
		/// <returns>True if they are perpendicular, false if not.</returns>
		public bool IsPerpendicular(ref Vector3D vector)
		{
			return Utility.Absolute(this.Dot(vector)) <= double.Epsilon;
		}

		/// <summary>
		/// Checks whether this and the specified vector are perpendicular.
		/// </summary>
		/// <param name="vector">The other vector.</param>
		/// <returns>True if they are perpendicular, false if not.</returns>
		/// <remarks>
		/// For performance reasons, it is recommended to use the methods with reference parameters.
		/// </remarks>
		public bool IsPerpendicular(Vector3D vector)
		{
			return Utility.Absolute(this.Dot(vector)) <= double.Epsilon;
		}
	}
}
