﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;

namespace Iridescence.Math
{
	public partial struct Vector3D
	{
		[System.Diagnostics.Contracts.Pure]
		public static implicit operator (double x, double y, double z)(in Vector3D value)
		{
			return (value.X, value.Y, value.Z);
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static implicit operator Vector3D((double x, double y, double z) value)
		{
			Vector3D result;
			result.X = value.x;
			result.Y = value.y;
			result.Z = value.z;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static explicit operator Vector3I(in Vector3D value)
		{
			Vector3I result;
			result.X = (int)value.X;
			result.Y = (int)value.Y;
			result.Z = (int)value.Z;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static explicit operator Vector3(in Vector3D value)
		{
			Vector3 result;
			result.X = (float)value.X;
			result.Y = (float)value.Y;
			result.Z = (float)value.Z;
			return result;
		}
		
		public void ToArray(Span<double> destination)
		{
			if (destination.Length < 3)
				throw new ArgumentException("Destination array is too small.", nameof(destination));
			
			destination[0] = this.X;
			destination[1] = this.Y;
			destination[2] = this.Z;
		}
		
		public void ToArray(double[] destination, int offset = 0)
		{
			this.ToArray(new Span<double>(destination, offset, 3));
		}
		
		[System.Diagnostics.Contracts.Pure]
		public double[] ToArray()
		{
			double[] array = new double[3];
			this.ToArray(array);
			return array;
		}
		
	}
}
