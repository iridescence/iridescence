﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Globalization;

namespace Iridescence.Math
{
	public partial struct Matrix3x2D : IFormattable
	{
		public string ToString(string format, IFormatProvider formatProvider)
		{
			return Formatting.ToString(this, format, formatProvider);
		}
		
		public string ToString(IFormatProvider formatProvider)
		{
			return Formatting.ToString(this, null, formatProvider);
		}
		
		public string ToString(string format)
		{
			return Formatting.ToString(this, format, NumberFormatInfo.CurrentInfo);
		}
		
		public override string ToString()
		{
			return Formatting.ToString(this, null, NumberFormatInfo.CurrentInfo);
		}
		
	}
}
