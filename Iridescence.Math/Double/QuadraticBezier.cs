﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a quadratic bezier curve.
	/// </summary>
	public struct QuadraticBezier : IEquatable<QuadraticBezier>, IParametricCurve, ISplittable<QuadraticBezier>
	{
		#region Fields

		/// <summary>
		/// The start point.
		/// </summary>
		public Vector2D Point1;

		/// <summary>
		/// The control point.
		/// </summary>
		public Vector2D Point2;

		/// <summary>
		/// The end point.
		/// </summary>
		public Vector2D Point3;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the point at the specified position.
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public Vector2D this[double t] => this.GetValue(t);

		/// <summary>
		/// Gets the bezier's scaling origin, which is the intersection of the start- and end-point normals.
		/// </summary>
		public Vector2D Origin
		{
			get
			{
				Vector2D tangent1;
				tangent1.X = this.Point2.Y - this.Point1.Y;
				tangent1.Y = this.Point2.X - this.Point1.X;
				Vector2D tangent2;
				tangent2.X = this.Point3.Y - this.Point2.Y;
				tangent2.Y = this.Point3.X - this.Point2.X;
				double c1 = tangent1.Y * this.Point1.X + tangent1.X * this.Point1.Y;
				double c2 = tangent2.Y * this.Point3.X + tangent2.X * this.Point3.Y;
				double det = tangent1.Y * tangent2.X - tangent2.Y * tangent1.X;
				Vector2D result;
				if (det <= double.Epsilon && det >= -double.Epsilon)
				{
					result.X = 0.5 * (this.Point1.X + this.Point3.X);
					result.Y = 0.5 * (this.Point1.Y + this.Point3.Y);
				}
				else
				{
					result.X = (tangent2.X * c1 - tangent1.X * c2) / det;
					result.Y = (tangent1.Y * c2 - tangent2.Y * c1) / det;
				}
				return result;
			}
		}
		
		/// <summary>
		/// Gets the derivative at <see cref="Point1"/>.
		/// </summary>
		public Vector2D Derivative1
		{
			get
			{
				Vector2D v;
				v.X = this.Point2.X - this.Point1.X;
				v.Y = this.Point2.Y - this.Point1.Y;
				return v;
			}
		}

		/// <summary>
		/// Gets the tangent at <see cref="Point1"/>.
		/// </summary>
		public Vector2D Tangent1
		{
			get
			{
				Vector2D v;
				v.X = this.Point2.X - this.Point1.X;
				v.Y = this.Point2.Y - this.Point1.Y;
				Vector2D.Normalize(in v, out v);
				return v;
			}
		}

		/// <summary>
		/// Gets the normal at <see cref="Point1"/>.
		/// </summary>
		public Vector2D Normal1
		{
			get
			{
				Vector2D v;
				v.X = this.Point1.Y - this.Point2.Y;
				v.Y = this.Point2.X - this.Point1.X;
				Vector2D.Normalize(in v, out v);
				return v;
			}
		}

		/// <summary>
		/// Gets the derivative at <see cref="Point3"/>.
		/// </summary>
		public Vector2D Derivative3
		{
			get
			{
				Vector2D v;
				v.X = this.Point3.X - this.Point2.X;
				v.Y = this.Point3.Y - this.Point2.Y;
				return v;
			}
		}
		
		/// <summary>
		/// Gets the tangent at <see cref="Point3"/>.
		/// </summary>
		public Vector2D Tangent3
		{
			get
			{
				Vector2D v;
				v.X = this.Point3.X - this.Point2.X;
				v.Y = this.Point3.Y - this.Point2.Y;
				Vector2D.Normalize(in v, out v);
				return v;
			}
		}
		
		/// <summary>
		/// Gets the normal at <see cref="Point3"/>.
		/// </summary>
		public Vector2D Normal3
		{
			get
			{
				Vector2D v;
				v.X = this.Point2.Y - this.Point3.Y;
				v.Y = this.Point3.X - this.Point2.X;
				Vector2D.Normalize(in v, out v);
				return v;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="QuadraticBezier"/> class.
		/// </summary>
		public QuadraticBezier(Vector2D p1, Vector2D p2, Vector2D p3)
		{
			this.Point1 = p1;
			this.Point2 = p2;
			this.Point3 = p3;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the point at the specified position.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetValue(double t)
		{
			if (t < 0.0 || t > 1.0)
				throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

			Vector2D p12 = this.Point1 + t * (this.Point2 - this.Point1);
			Vector2D p23 = this.Point2 + t * (this.Point3 - this.Point2);
			return p12 + t * (p23 - p12);
		}

		/// <summary>
		/// Returns the point at the specified position, offset (i.e. moved along the normal) by the specified amount.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <param name="offset">The distance from the curve (along the normal at <see cref="t"/>).</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetOffsetValue(double t, double offset)
		{
			Vector2D p12 = this.Point1 + t * (this.Point2 - this.Point1);
			Vector2D p23 = this.Point2 + t * (this.Point3 - this.Point2);
			Vector2D n = p23 - p12;
			Vector2D p = p12 + t * n;
			n.X = -n.Y;
			n.Y = n.X;
			Vector2D.Normalize(in n, out n);
			p += n * offset;
			return p;
		}

		/// <summary>
		/// Returns the derivative value at the specified position.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetDerivative(double t)
		{
			if (t < 0.0 || t > 1.0)
				throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

			Vector2D p12 = this.Point1 + t * (this.Point2 - this.Point1);
			Vector2D p23 = this.Point2 + t * (this.Point3 - this.Point2);
			return p23 - p12;
		}

		/// <summary>
		/// Returns the tangent at the specified position.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetTangent(double t)
		{
			Vector2D tangent = this.GetDerivative(t);
			tangent.Normalize(out tangent);
			return tangent;
		}

		/// <summary>
		/// Returns the tangent at the specified position.
		/// </summary>
		/// <param name="t">The curve position in the range [0..1].</param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetNormal(double t)
		{
			Vector2D tangent = this.GetTangent(t);
			return new Vector2D(-tangent.Y, tangent.X);
		}

		/// <summary>
		/// Splits this <see cref="QuadraticBezier"/> into two curves at the specified position.
		/// </summary>
		/// <param name="t">The split position in the range [0..1].</param>
		/// <param name="first"></param>
		/// <param name="second"></param>
		[Pure]
		public void Split(double t, out QuadraticBezier first, out QuadraticBezier second)
		{
			if (t < 0.0 || t > 1.0)
				throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

			Vector2D p1 = this.Point1;
			Vector2D p3 = this.Point3;
			Vector2D p12 = p1 + t * (this.Point2 - p1);
			Vector2D p23 = this.Point2 + t * (p3 - this.Point2);
			Vector2D p123 = p12 + t * (p23 - p12);

			first.Point1 = p1;
			first.Point2 = p12;
			first.Point3 = p123;

			second.Point1 = p123;
			second.Point2 = p23;
			second.Point3 = p3;
		}

		/// <summary>
		/// Splits this <see cref="CubicBezier"/> into multiple curves at the specified positions.
		/// </summary>
		/// <param name="positions">The curve positions in the range [0..1].</param>
		[Pure]
		public IEnumerable<QuadraticBezier> Split(IEnumerable<double> positions)
		{
			QuadraticBezier second = this;

			double tPrev = 0.0;
			foreach (double t in positions.OrderBy(t => t))
			{
				if (t < 0.0 || t > 1.0)
					throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

				if (t == tPrev)
				{
					yield return second;
					continue;
				}

				double relT = (t - tPrev) / (1.0 - tPrev);
				second.Split(relT, out QuadraticBezier first, out second);
				yield return first;
				tPrev = t;
			}

			yield return second;
		}

		/// <summary>
		/// Projects the specified point onto the curve.
		/// </summary>
		/// <param name="p">The point to project.</param>
		/// <returns>The closest 't' value.</returns>
		[Pure]
		public double Project(Vector2D p)
		{
			return this.Project(p, 4);
		}

		/// <summary>
		/// Projects the specified point onto the curve.
		/// </summary>
		/// <param name="p">The point to project.</param>
		/// <param name="iterations">The number of iterations. Each iteration improves the precision of the result.</param>
		/// <returns>The closest 't' value.</returns>
		[Pure]
		public double Project(Vector2D p, int iterations)
		{
			return this.Project(p, iterations, out _, out _);
		}

		/// <summary>
		/// Projects the specified point onto the curve.
		/// </summary>
		/// <param name="p">The point to project.</param>
		/// <param name="iterations">The number of iterations. Each iteration improves the precision of the result.</param>
		/// <param name="projected">The projected point.</param>
		/// <param name="distance">The distance between the specified point and the projected point.</param>
		/// <returns>The closest 't' value.</returns>
		[Pure]
		public double Project(Vector2D p, int iterations, out Vector2D projected, out double distance)
		{
			const int divisions = 64;
			const double step = 1.0 / divisions;

			double t = 0.0;
			double tMin = 0.0;
			double tRange = 1.0;

			projected = this.Point1;
			distance = double.MaxValue;

			for (int i = 0; i < iterations; ++i)
			{
				for (int j = 0; j <= divisions; ++j)
				{
					double newT = tMin + j * step * tRange;
					Vector2D newP = this.GetValue(newT);
					double newDistance = Vector2D.DistanceSquared(p, newP);
					if (newDistance < distance)
					{
						distance = newDistance;
						t = newT;
						projected = newP;
					}
				}

				tRange *= step;
				tMin = Utility.Min(Utility.Max(t - step * 0.5, 0.0), 1.0 - tRange);
			}

			return t;
		}

		/// <summary>
		/// Returns a <see cref="CubicBezier"/> that represents the same curve as this <see cref="QuadraticBezier"/>.
		/// </summary>
		/// <returns></returns>
		[Pure]
		public CubicBezier ToCubic()
		{
			CubicBezier c;
			c.Point1 = this.Point1;
			c.Point2 = this.Point1 + (2.0 / 3.0) * (this.Point2 - this.Point1);
			c.Point3 = this.Point3 + (2.0 / 3.0) * (this.Point2 - this.Point3);
			c.Point4 = this.Point3;
			return c;
		}

		/// <summary>
		/// Tessellates the curve recursively.
		/// </summary>
		/// <param name="tolerance">The approximate maximum distance between two tessellated points.</param>
		/// <param name="maxDepth">The maximum recursion depth.</param>
		/// <param name="callback">A callback that is called for each computed point.</param>
		public void Tessellate(double tolerance, int maxDepth, Action<Vector2D> callback)
		{
			// TODO
			this.ToCubic().Tessellate(tolerance, maxDepth, callback);
		}

		/// <summary>
		/// Returns the extremeties of the curve.
		/// </summary>
		[Pure]
		public IEnumerable<Extremum<double>> GetExtrema()
		{
			return this.GetExtrema(true);
		}

		/// <summary>
		/// Returns the extremeties of the curve.
		/// </summary>
		/// <param name="discardOutOfBounds">True to discard extrema that are not within the range [0..1].</param>
		[Pure]
		public IEnumerable<Extremum<double>> GetExtrema(bool discardOutOfBounds)
		{
			Vector2D a = 2.0 * (this.Point2 - this.Point1);
			Vector2D b = 2.0 * (this.Point3 - this.Point2);

			if (a.X != b.X)
			{
				double t = a.X / (a.X - b.X);
				double td = b.X - a.X;
				if (!discardOutOfBounds || (t >= 0.0 && t <= 1.0))
					yield return new Extremum<double>(t, td < 0.0 ? ExtremumType.Maximum : td > 0.0 ? ExtremumType.Minimum : ExtremumType.Saddle);
			}

			if (a.Y != b.Y)
			{
				double t = a.Y / (a.Y - b.Y);
				double td = b.Y - a.Y;
				if (!discardOutOfBounds || (t >= 0.0 && t <= 1.0))
					yield return new Extremum<double>(t, td < 0.0 ? ExtremumType.Maximum : td > 0.0 ? ExtremumType.Minimum : ExtremumType.Saddle);
			}
		}

		/// <summary>
		/// Returns an axis-aligned bounding rectangle of this bezier curve.
		/// </summary>
		/// <returns></returns>
		[Pure]
		public RectangleD GetBounds()
		{
			Vector2D min = new Vector2D(double.MaxValue);
			Vector2D max = new Vector2D(double.MinValue);

			Vector2D.Min(min, this.Point1, out min);
			Vector2D.Max(max, this.Point1, out max);

			Vector2D.Min(min, this.Point3, out min);
			Vector2D.Max(max, this.Point3, out max);

			foreach (Extremum<double> ex in this.GetExtrema())
			{
				Vector2D p = this[ex.Value];
				Vector2D.Min(min, p, out min);
				Vector2D.Max(max, p, out max);
			}

			return new RectangleD(min, max - min);
		}

		/// <summary>
		/// Returns a value that indicates whether the two specified <see cref="QuadraticBezier">QuadraticBeziers</see> are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(ref QuadraticBezier left, ref QuadraticBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether the two specified <see cref="QuadraticBezier">QuadraticBeziers</see> are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(QuadraticBezier left, ref QuadraticBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y;
		}


		/// <summary>
		/// Returns a value that indicates whether the two specified <see cref="QuadraticBezier">QuadraticBeziers</see> are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(ref QuadraticBezier left, QuadraticBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether the two specified <see cref="QuadraticBezier">QuadraticBeziers</see> are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(QuadraticBezier left, QuadraticBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether this <see cref="QuadraticBezier"/> and the specified <see cref="QuadraticBezier"/> are equal.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		[Pure]
		public bool Equals(QuadraticBezier other)
		{
			return this.Point1.X == other.Point1.X && this.Point1.Y == other.Point1.Y &&
			       this.Point2.X == other.Point2.X && this.Point2.Y == other.Point2.Y &&
			       this.Point3.X == other.Point3.X && this.Point3.Y == other.Point3.Y;
		}

		/// <summary>
		/// Returns a value that indicates whether this <see cref="QuadraticBezier"/> and the specified object are equal.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		[Pure]
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
				return false;
			return obj is QuadraticBezier && this.Equals((QuadraticBezier)obj);
		}

		/// <summary>
		/// Returns a hash code for this <see cref="CubicBezier"/>.
		/// </summary>
		/// <returns></returns>
		[Pure]
		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = this.Point1.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Point2.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Point3.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(QuadraticBezier left, QuadraticBezier right)
		{
			return left.Point1.X == right.Point1.X && left.Point1.Y == right.Point1.Y &&
			       left.Point2.X == right.Point2.X && left.Point2.Y == right.Point2.Y &&
			       left.Point3.X == right.Point3.X && left.Point3.Y == right.Point3.Y;
		}

		public static bool operator !=(QuadraticBezier left, QuadraticBezier right)
		{
			return left.Point1.X != right.Point1.X || left.Point1.Y != right.Point1.Y ||
			       left.Point2.X != right.Point2.X || left.Point2.Y != right.Point2.Y ||
			       left.Point3.X != right.Point3.X || left.Point3.Y != right.Point3.Y;
		}

		#endregion
	}
}
