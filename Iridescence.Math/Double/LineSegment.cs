﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a 2-dimensional line segment.
	/// </summary>
	[Serializable]
	public struct LineSegmentD : IEquatable<LineSegmentD>, ICsgComponent<LineSegmentD>, IParametricCurve, ISplittable<LineSegmentD>, IIntersectable<LineSegmentD>
	{
		#region Fields

		/// <summary>
		/// The start point of the segment.
		/// </summary>
		public Vector2D Start;

		/// <summary>
		/// The end point of the segment.
		/// </summary>
		public Vector2D End;

		#endregion

		#region Properties

		/// <summary>
		/// Gets an interpolated point on the line.
		/// </summary>
		/// <param name="t">The position in the interval [0..1].</param>
		/// <returns></returns>
		public Vector2D this[double t] => this.GetValue(t);

		/// <summary>
		/// Gets the length of the line segment.
		/// </summary>
		public double Length => Vector2D.Distance(this.Start, this.End);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="LineSegmentD"/>.
		/// </summary>
		public LineSegmentD(Vector2D start, Vector2D end)
		{
			this.Start = start;
			this.End = end;
		}

		/// <summary>
		/// Creates a new <see cref="LineSegmentD"/>.
		/// </summary>
		public LineSegmentD(double x1, double y1, double x2, double y2)
		{
			this.Start.X = x1;
			this.Start.Y = y1;
			this.End.X = x2;
			this.End.Y = y2;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the interpolated value at the specified position.
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		[Pure]
		public Vector2D GetValue(double t)
		{
			Vector2D result;
			result.X = this.Start.X + t * (this.End.X - this.Start.X);
			result.Y = this.Start.Y + t * (this.End.Y - this.Start.Y);
			return result;
		}

		/// <summary>
		/// Inverts the <see cref="LineSegmentD"/> (i.e. swaps start and end points).
		/// </summary>
		/// <returns></returns>
		[Pure]
		public LineSegmentD Invert()
		{
			LineSegmentD inverted;
			inverted.Start = this.End;
			inverted.End = this.Start;
			return inverted;
		}

		/// <summary>
		/// Splits the <see cref="LineSegmentD"/> at the specified position.
		/// </summary>
		/// <param name="t">The position at which to split the line segment, in the interval [0..1].</param>
		/// <param name="first"></param>
		/// <param name="second"></param>
		[Pure]
		public void Split(double t, out LineSegmentD first, out LineSegmentD second)
		{
			if (t < 0.0 || t > 1.0)
				throw new ArgumentOutOfRangeException(nameof(t), "t must be in the range [0..1].");

			first.Start = this.Start;
			first.End = this.Start + t * (this.End - this.Start);
			second.Start = first.End;
			second.End = this.End;
		}

		/// <summary>
		/// Splits the <see cref="LineSegmentD"/>.
		/// </summary>
		/// <param name="other"></param>
		/// <param name="front"></param>
		/// <param name="back"></param>
		[Pure]
		public void Split(LineSegmentD other, out LineSegmentD front, out LineSegmentD back)
		{
			Vector2D d;
			Vector2D.Subtract(other.End, other.Start, out d);
			//Vector2D.Normalize(ref d, out d);

			Vector2D v1;
			Vector2D.Subtract(other.Start, this.Start, out v1);

			Vector2D v2;
			Vector2D.Subtract(this.End, this.Start, out v2);

			Vector2D v3;
			v3.X = -d.Y;
			v3.Y = d.X;

			double v1Dotv3 = Vector2D.Dot(v1, v3);
			double v2Dotv3 = Vector2D.Dot(v2, v3);
			double t = v1Dotv3 / v2Dotv3;

			if (t <= 0.0)
			{
				front = new LineSegmentD(this.Start, this.Start);
				back = this;
			}
			else if (t >= 1.0)
			{
				front = this;
				back = new LineSegmentD(this.End, this.End);
			}
			else 
			{
				double t2 = 1.0 - t;
				front.Start = this.Start;
				front.End.X = this.Start.X * t2 + this.End.X * t;
				front.End.Y = this.Start.Y * t2 + this.End.Y * t;

				back.Start = front.End;
				back.End = this.End;

				if (v2Dotv3 >= 0.0)
				{
					Utility.Swap(ref front, ref back);
				}
			}
		}

		/// <summary>
		/// Returns what side of the <see cref="LineSegmentD"/> the specified point is on.
		/// </summary>
		/// <param name="point"></param>
		/// <returns></returns>
		[Pure]
		public int Side(Vector2D point)
		{
			return System.Math.Sign((this.End.X - this.Start.X) * (point.Y - this.Start.Y) - (this.End.Y - this.Start.Y) * (point.X - this.Start.X));
		}
		
		/// <summary>
		/// Returns what side of the <see cref="LineSegmentD"/> the specified point is on.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="point"></param>
		/// <returns></returns>
		public static int Side(Vector2D start, Vector2D end, Vector2D point)
		{
			return System.Math.Sign((end.X - start.X) * (point.Y - start.Y) - (end.Y - start.Y) * (point.X - start.X));
		}

		[Pure]
		CsgComponentRelation ICsgComponent<LineSegmentD>.GetRelationTo(LineSegmentD other)
		{
			int sideA = this.Side(other.Start);
			int sideB = this.Side(other.End);

			CsgComponentRelation relation = 0;
			if (sideA < 0 || sideB < 0)
				relation |= CsgComponentRelation.Back;

			if (sideA > 0 || sideB > 0)
				relation |= CsgComponentRelation.Front;

			if (sideA == 0 && sideB == 0)
				relation |= CsgComponentRelation.Coplanar;

			return relation;
		}
		
		[Pure]
		LineSegmentD ICsgComponent<LineSegmentD>.Clone()
		{
			return this;
		}

		/// <summary>
		/// Returns the point of intersection between two lines.
		/// </summary>
		/// <param name="p1">The first point on the first line.</param>
		/// <param name="p2">The second point on the first line.</param>
		/// <param name="q1">The first point on the second line.</param>
		/// <param name="q2">The second point on the second line.</param>
		/// <returns>The point of intersection.</returns>
		/// <remarks>
		/// This actually returns the intersection between two infinite lines that go through the specified points.
		/// Use <see cref="IsIntersecting(Vector2D,Vector2D,Vector2D,Vector2D)"/> to determine whether two line segments are intersecting.
		/// </remarks>
		public static Vector2D Intersect(Vector2D p1, Vector2D p2, Vector2D q1, Vector2D q2)
		{
			double a1 = p2.Y - p1.Y;
			double b1 = p1.X - p2.X;
			double c1 = a1 * p1.X + b1 * p1.Y;
			double a2 = q2.Y - q1.Y;
			double b2 = q1.X - q2.X;
			double c2 = a2 * q1.X + b2 * q1.Y;
			double det = a1 * b2 - a2 * b1;
			Vector2D result;
			result.X = (b2 * c1 - b1 * c2) / det;
			result.Y = (a1 * c2 - a2 * c1) / det;
			return result;
		}

		/// <summary>
		/// Returns the point of intersection between two lines.
		/// </summary>
		/// <param name="p">The first line segment.</param>
		/// <param name="q">The second line segment.</param>
		/// <returns>The point of intersection.</returns>
		/// <remarks>
		/// This actually returns the intersection between two infinite lines that go through the specified points.
		/// Use <see cref="IsIntersecting(LineSegmentD,LineSegmentD)"/> to determine whether two line segments are intersecting.
		/// </remarks>
		public static Vector2D Intersect(LineSegmentD p, LineSegmentD q)
		{
			return Intersect(p.Start, p.End, q.Start, q.End);
		}

		/// <summary>
		/// Given three colinear points p, q, r, the function checks if point q lies on line segment 'pr'
		/// </summary>
		/// <param name="p"></param>
		/// <param name="q"></param>
		/// <param name="r"></param>
		/// <returns></returns>
		private static bool onSegment(Vector2D p, Vector2D q, Vector2D r)
		{
			return q.X <= Utility.Max(p.X, r.X) && q.X >= Utility.Min(p.X, r.X) &&
			       q.Y <= Utility.Max(p.Y, r.Y) && q.Y >= Utility.Min(p.Y, r.Y);
		}

		/// <summary>
		/// Returns a value that indicates whether two line segments are intersecting.
		/// </summary>
		/// <param name="p1">The first point on the first line.</param>
		/// <param name="p2">The second point on the first line.</param>
		/// <param name="q1">The first point on the second line.</param>
		/// <param name="q2">The second point on the second line.</param>
		/// <returns>True, if the specified segments are intersecting. False otherwise.</returns>
		public static bool IsIntersecting(Vector2D p1, Vector2D p2, Vector2D q1, Vector2D q2)
		{
			int o1 = Side(p1, p2, q1);
			int o2 = Side(p1, p2, q2);
			int o3 = Side(q1, q2, p1);
			int o4 = Side(q1, q2, p2);
 
			if (o1 != o2 && o3 != o4)
				return true;

			return o1 == 0 && onSegment(p1, q1, p2) ||
			       o2 == 0 && onSegment(p1, q2, p2) ||
			       o3 == 0 && onSegment(q1, p1, q2) ||
			       o4 == 0 && onSegment(q1, p2, q2);
		}

		/// <summary>
		/// Returns a value that indicates whether two line segments are intersecting.
		/// </summary>
		/// <param name="p">The first line segment.</param>
		/// <param name="q">The second line segment.</param>
		/// <returns>True, if the specified segments are intersecting. False otherwise.</returns>
		public static bool IsIntersecting(LineSegmentD p, LineSegmentD q)
		{
			return IsIntersecting(p.Start, p.End, q.Start, q.End);
		}
		
		[Pure]
		public IEnumerable<double> GetIntersections(LineSegmentD other)
		{
			Vector2D s1, s2;
			s1.X = this.End.X - this.Start.X;
			s1.Y = this.End.Y - this.Start.Y;
			s2.X = other.End.X - other.Start.X;
			s2.Y = other.End.Y - other.Start.Y;
			double det = s1.X * s2.Y - s2.X * s1.Y;
			if (Utility.Absolute(det) < double.Epsilon)
			{
				int side1 = System.Math.Sign(s1.X * (other.Start.Y - this.Start.Y) - s1.Y * (other.Start.X - this.Start.X));
				int side2 = System.Math.Sign(s1.X * (other.End.Y - this.Start.Y) - s1.Y * (other.End.X - this.Start.X));

				if (side1 == 0 && side2 == 0)
				{
					yield return 0.0;
				}
			}
			else
			{
				double s = (s1.X * (this.Start.Y - other.Start.Y) - s1.Y * (this.Start.X - other.Start.X)) / det;
				if (s >= 0.0 && s <= 1.0)
				{
					double t = (s2.X * (this.Start.Y - other.Start.Y) - s2.Y * (this.Start.X - other.Start.X)) / det;
					if (t >= 0.0 && t <= 1.0)
					{
						yield return t;
					}
				}
			}
		}

		[Pure]
		IEnumerable<double> IIntersectable<LineSegmentD>.GetIntersections(LineSegmentD other, double tolerance)
		{
			return this.GetIntersections(other);
		}

		[Pure]
		public RectangleD GetBounds()
		{
			Vector2D.Min(this.Start, this.End, out Vector2D min);
			Vector2D.Max(this.Start, this.End, out Vector2D max);
			Vector2D.Subtract(max, min, out max);
			return new RectangleD(min, max);
		}
		
		[Pure]
		public override string ToString()
		{
			return $"({this.Start}) - ({this.End})";
		}

		/// <summary>
		/// Returns a value that indicates whether the this <see cref="LineSegmentD"/> and the specified <see cref="LineSegmentD"/> are equal.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		[Pure]
		public bool Equals(LineSegmentD other)
		{
			return this.Start.Equals(other.Start) && this.End.Equals(other.End);
		}

		/// <summary>
		/// Returns a value that indicates whether the this <see cref="LineSegmentD"/> and the specified object are equal.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		[Pure]
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is LineSegmentD && this.Equals((LineSegmentD)obj);
		}

		/// <summary>
		/// Returns the hash code of this <see cref="LineSegmentD"/>.
		/// </summary>
		/// <returns></returns>
		[Pure]
		public override int GetHashCode()
		{
			unchecked
			{
				return (this.Start.GetHashCode() * 397) ^ this.End.GetHashCode();
			}
		}

		public static bool operator ==(LineSegmentD left, LineSegmentD right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(LineSegmentD left, LineSegmentD right)
		{
			return !left.Equals(right);
		}
		
		#endregion
	}
}
