﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Iridescence.Math
{
	public partial struct Matrix4x2D
	{
		[System.Diagnostics.Contracts.Pure]
		public Matrix4x2D Lerp(in Matrix4x2D value, double amount)
		{
			double amount2 = 1 - amount;
			Matrix4x2D result;
			result.M11 = this.M11 * amount2 + value.M11 * amount;
			result.M12 = this.M12 * amount2 + value.M12 * amount;
			result.M21 = this.M21 * amount2 + value.M21 * amount;
			result.M22 = this.M22 * amount2 + value.M22 * amount;
			result.M31 = this.M31 * amount2 + value.M31 * amount;
			result.M32 = this.M32 * amount2 + value.M32 * amount;
			result.M41 = this.M41 * amount2 + value.M41 * amount;
			result.M42 = this.M42 * amount2 + value.M42 * amount;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Matrix4x2D Lerp(in Matrix4x2D left, in Matrix4x2D right, double amount)
		{
			double amount2 = 1 - amount;
			Matrix4x2D result;
			result.M11 = left.M11 * amount2 + right.M11 * amount;
			result.M12 = left.M12 * amount2 + right.M12 * amount;
			result.M21 = left.M21 * amount2 + right.M21 * amount;
			result.M22 = left.M22 * amount2 + right.M22 * amount;
			result.M31 = left.M31 * amount2 + right.M31 * amount;
			result.M32 = left.M32 * amount2 + right.M32 * amount;
			result.M41 = left.M41 * amount2 + right.M41 * amount;
			result.M42 = left.M42 * amount2 + right.M42 * amount;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Lerp(in Matrix4x2D left, in Matrix4x2D right, double amount, out Matrix4x2D result)
		{
			double amount2 = 1 - amount;
			result.M11 = left.M11 * amount2 + right.M11 * amount;
			result.M12 = left.M12 * amount2 + right.M12 * amount;
			result.M21 = left.M21 * amount2 + right.M21 * amount;
			result.M22 = left.M22 * amount2 + right.M22 * amount;
			result.M31 = left.M31 * amount2 + right.M31 * amount;
			result.M32 = left.M32 * amount2 + right.M32 * amount;
			result.M41 = left.M41 * amount2 + right.M41 * amount;
			result.M42 = left.M42 * amount2 + right.M42 * amount;
		}
		
	}
}
