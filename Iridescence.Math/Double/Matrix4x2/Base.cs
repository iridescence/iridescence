﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Runtime.InteropServices;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a 4x2 double-precision floating point matrix.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	[Serializable]
	public partial struct Matrix4x2D : IMatrix<double>
	{
		/// <summary>
		/// The element at the first row and first column of the matrix.
		/// </summary>
		public double M11;
		
		/// <summary>
		/// The element at the first row and second column of the matrix.
		/// </summary>
		public double M12;
		
		/// <summary>
		/// The element at the second row and first column of the matrix.
		/// </summary>
		public double M21;
		
		/// <summary>
		/// The element at the second row and second column of the matrix.
		/// </summary>
		public double M22;
		
		/// <summary>
		/// The element at the third row and first column of the matrix.
		/// </summary>
		public double M31;
		
		/// <summary>
		/// The element at the third row and second column of the matrix.
		/// </summary>
		public double M32;
		
		/// <summary>
		/// The element at the fourth row and first column of the matrix.
		/// </summary>
		public double M41;
		
		/// <summary>
		/// The element at the fourth row and second column of the matrix.
		/// </summary>
		public double M42;
		
		int IMatrix<double>.Rows => 4;
		
		int IMatrix<double>.Columns => 2;
		
		/// <summary>
		/// Gets or sets the element at the specified row and column.
		/// </summary>
		public double this[int row, int column]
		{
			get
			{
				if (unchecked((uint)row) >= 4)
					throw new ArgumentOutOfRangeException(nameof(row));
				
				if (unchecked((uint)column) >= 2)
					throw new ArgumentOutOfRangeException(nameof(column));
				
				unsafe
				{
					fixed (Matrix4x2D* p = &this)
					{
						return ((double*)p)[row * 2 + column];
					}
				}
			}
			set
			{
				if (unchecked((uint)row) >= 4)
					throw new ArgumentOutOfRangeException(nameof(row));
				
				if (unchecked((uint)column) >= 2)
					throw new ArgumentOutOfRangeException(nameof(column));
				
				unsafe
				{
					fixed (Matrix4x2D* p = &this)
					{
						((double*)p)[row * 2 + column] = value;
					}
				}
			}
		}
		
		/// <summary>
		/// Gets or sets a component of the matrix, addressed by the zero-based index in row-major order.
		/// </summary>
		public double this[int index]
		{
			get
			{
				if (unchecked((uint)index) >= 8)
					throw new ArgumentOutOfRangeException(nameof(index));
				
				unsafe
				{
					fixed (Matrix4x2D* p = &this)
					{
						return ((double*)p)[index];
					}
				}
			}
			set
			{
				if (unchecked((uint)index) >= 8)
					throw new ArgumentOutOfRangeException(nameof(index));
				
				unsafe
				{
					fixed (Matrix4x2D* p = &this)
					{
						((double*)p)[index] = value;
					}
				}
			}
		}
		
		public bool IsFinite => this.M11.IsFinite() && this.M12.IsFinite() && this.M21.IsFinite() && this.M22.IsFinite() && this.M31.IsFinite() && this.M32.IsFinite() && this.M41.IsFinite() && this.M42.IsFinite();
	}
}
