﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Diagnostics;

namespace Iridescence.Math
{
	public partial struct Vector2D
	{
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector2D XX
		{
			get
			{
				Vector2D value;
				value.X = this.X;
				value.Y = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector2D XY
		{
			get
			{
				Vector2D value;
				value.X = this.X;
				value.Y = this.Y;
				return value;
			}
			set
			{
				this.X = value.X;
				this.Y = value.Y;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector2D YX
		{
			get
			{
				Vector2D value;
				value.X = this.Y;
				value.Y = this.X;
				return value;
			}
			set
			{
				this.Y = value.X;
				this.X = value.Y;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector2D YY
		{
			get
			{
				Vector2D value;
				value.X = this.Y;
				value.Y = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector3D XXX
		{
			get
			{
				Vector3D value;
				value.X = this.X;
				value.Y = this.X;
				value.Z = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector3D XXY
		{
			get
			{
				Vector3D value;
				value.X = this.X;
				value.Y = this.X;
				value.Z = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector3D XYX
		{
			get
			{
				Vector3D value;
				value.X = this.X;
				value.Y = this.Y;
				value.Z = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector3D XYY
		{
			get
			{
				Vector3D value;
				value.X = this.X;
				value.Y = this.Y;
				value.Z = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector3D YXX
		{
			get
			{
				Vector3D value;
				value.X = this.Y;
				value.Y = this.X;
				value.Z = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector3D YXY
		{
			get
			{
				Vector3D value;
				value.X = this.Y;
				value.Y = this.X;
				value.Z = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector3D YYX
		{
			get
			{
				Vector3D value;
				value.X = this.Y;
				value.Y = this.Y;
				value.Z = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector3D YYY
		{
			get
			{
				Vector3D value;
				value.X = this.Y;
				value.Y = this.Y;
				value.Z = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D XXXX
		{
			get
			{
				Vector4D value;
				value.X = this.X;
				value.Y = this.X;
				value.Z = this.X;
				value.W = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D XXXY
		{
			get
			{
				Vector4D value;
				value.X = this.X;
				value.Y = this.X;
				value.Z = this.X;
				value.W = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D XXYX
		{
			get
			{
				Vector4D value;
				value.X = this.X;
				value.Y = this.X;
				value.Z = this.Y;
				value.W = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D XXYY
		{
			get
			{
				Vector4D value;
				value.X = this.X;
				value.Y = this.X;
				value.Z = this.Y;
				value.W = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D XYXX
		{
			get
			{
				Vector4D value;
				value.X = this.X;
				value.Y = this.Y;
				value.Z = this.X;
				value.W = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D XYXY
		{
			get
			{
				Vector4D value;
				value.X = this.X;
				value.Y = this.Y;
				value.Z = this.X;
				value.W = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D XYYX
		{
			get
			{
				Vector4D value;
				value.X = this.X;
				value.Y = this.Y;
				value.Z = this.Y;
				value.W = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D XYYY
		{
			get
			{
				Vector4D value;
				value.X = this.X;
				value.Y = this.Y;
				value.Z = this.Y;
				value.W = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D YXXX
		{
			get
			{
				Vector4D value;
				value.X = this.Y;
				value.Y = this.X;
				value.Z = this.X;
				value.W = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D YXXY
		{
			get
			{
				Vector4D value;
				value.X = this.Y;
				value.Y = this.X;
				value.Z = this.X;
				value.W = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D YXYX
		{
			get
			{
				Vector4D value;
				value.X = this.Y;
				value.Y = this.X;
				value.Z = this.Y;
				value.W = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D YXYY
		{
			get
			{
				Vector4D value;
				value.X = this.Y;
				value.Y = this.X;
				value.Z = this.Y;
				value.W = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D YYXX
		{
			get
			{
				Vector4D value;
				value.X = this.Y;
				value.Y = this.Y;
				value.Z = this.X;
				value.W = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D YYXY
		{
			get
			{
				Vector4D value;
				value.X = this.Y;
				value.Y = this.Y;
				value.Z = this.X;
				value.W = this.Y;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D YYYX
		{
			get
			{
				Vector4D value;
				value.X = this.Y;
				value.Y = this.Y;
				value.Z = this.Y;
				value.W = this.X;
				return value;
			}
		}
		
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public Vector4D YYYY
		{
			get
			{
				Vector4D value;
				value.X = this.Y;
				value.Y = this.Y;
				value.Z = this.Y;
				value.W = this.Y;
				return value;
			}
		}
		
	}
}
