﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Iridescence.Math
{
	public partial struct Vector2D
	{
		[System.Diagnostics.Contracts.Pure]
		public static Vector2D Min(in Vector2D left, double right)
		{
			Vector2D result;
			result.X = left.X < right ? left.X : right;
			result.Y = left.Y < right ? left.Y : right;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Min(in Vector2D left, double right, out Vector2D result)
		{
			result.X = left.X < right ? left.X : right;
			result.Y = left.Y < right ? left.Y : right;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Vector2D Min(double left, in Vector2D right)
		{
			Vector2D result;
			result.X = left < right.X ? left : right.X;
			result.Y = left < right.Y ? left : right.Y;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Min(double left, in Vector2D right, out Vector2D result)
		{
			result.X = left < right.X ? left : right.X;
			result.Y = left < right.Y ? left : right.Y;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public Vector2D Min(in Vector2D value)
		{
			Vector2D result;
			result.X = this.X < value.X ? this.X : value.X;
			result.Y = this.Y < value.Y ? this.Y : value.Y;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Vector2D Min(in Vector2D left, in Vector2D right)
		{
			Vector2D result;
			result.X = left.X < right.X ? left.X : right.X;
			result.Y = left.Y < right.Y ? left.Y : right.Y;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Min(in Vector2D left, in Vector2D right, out Vector2D result)
		{
			result.X = left.X < right.X ? left.X : right.X;
			result.Y = left.Y < right.Y ? left.Y : right.Y;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Vector2D Max(in Vector2D left, double right)
		{
			Vector2D result;
			result.X = left.X > right ? left.X : right;
			result.Y = left.Y > right ? left.Y : right;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Max(in Vector2D left, double right, out Vector2D result)
		{
			result.X = left.X > right ? left.X : right;
			result.Y = left.Y > right ? left.Y : right;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Vector2D Max(double left, in Vector2D right)
		{
			Vector2D result;
			result.X = left > right.X ? left : right.X;
			result.Y = left > right.Y ? left : right.Y;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Max(double left, in Vector2D right, out Vector2D result)
		{
			result.X = left > right.X ? left : right.X;
			result.Y = left > right.Y ? left : right.Y;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public Vector2D Max(in Vector2D value)
		{
			Vector2D result;
			result.X = this.X > value.X ? this.X : value.X;
			result.Y = this.Y > value.Y ? this.Y : value.Y;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static Vector2D Max(in Vector2D left, in Vector2D right)
		{
			Vector2D result;
			result.X = left.X > right.X ? left.X : right.X;
			result.Y = left.Y > right.Y ? left.Y : right.Y;
			return result;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static void Max(in Vector2D left, in Vector2D right, out Vector2D result)
		{
			result.X = left.X > right.X ? left.X : right.X;
			result.Y = left.Y > right.Y ? left.Y : right.Y;
		}
		
	}
}
