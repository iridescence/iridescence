﻿namespace Iridescence.Math
{
	public partial struct Vector2D
	{
		/// <summary>
		/// Returns a vector with the specified length that points in the specified direction.
		/// </summary>
		/// <param name="angle">The angle.</param>
		/// <param name="length">The length of the vector.</param>
		/// <param name="vector">The resulting vector.</param>
		public static void GetDirectionVector(double angle, double length, out Vector2D vector)
		{
			vector.X = Utility.Cos(angle) * length;
			vector.Y = Utility.Sin(angle) * length;
		}

		/// <summary>
		/// Returns a vector with the specified length that points in the specified direction.
		/// </summary>
		/// <param name="angle">The angle.</param>
		/// <param name="length">The length of the vector.</param>
		/// <returns>The resulting vector.</returns>
		public static Vector2D GetDirectionVector(double angle, double length)
		{
			Vector2D vector;
			vector.X = Utility.Cos(angle) * length;
			vector.Y = Utility.Sin(angle) * length;
			return vector;
		}
	}
}
