﻿namespace Iridescence.Math
{
	public partial struct Vector2D
	{
		/// <summary>
		/// Returns a.X * b.Y - a.Y * b.X.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static double Cross(in Vector2D a, in Vector2D b)
		{
			return a.X * b.Y - a.Y * b.X;
		}

		/// <summary>
		/// Returns a.X * b.Y - a.Y * b.X.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static double Cross(in Vector2D a, Vector2D b)
		{
			return a.X * b.Y - a.Y * b.X;
		}

		/// <summary>
		/// Returns a.X * b.Y - a.Y * b.X.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static double Cross(Vector2D a, in Vector2D b)
		{
			return a.X * b.Y - a.Y * b.X;
		}

		/// <summary>
		/// Returns a.X * b.Y - a.Y * b.X.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static double Cross(Vector2D a, Vector2D b)
		{
			return a.X * b.Y - a.Y * b.X;
		}

		/// <summary>
		/// Returns (s * a.Y, -s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <param name="result"></param>
		public static void Cross(in Vector2D a, double s, out Vector2D result)
		{
			double aX = a.X;
			result.X = s * a.Y;
			result.Y = -s * aX;
		}

		/// <summary>
		/// Returns (s * a.Y, -s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <param name="result"></param>
		public static void Cross(Vector2D a, double s, out Vector2D result)
		{
			result.X = s * a.Y;
			result.Y = -s * a.X;
		}

		/// <summary>
		/// Returns (s * a.Y, -s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <returns></returns>
		public static Vector2D Cross(in Vector2D a, double s)
		{
			Vector2D result; 
			result.X = s * a.Y;
			result.Y = -s * a.X;
			return result;
		}

		/// <summary>
		/// Returns (s * a.Y, -s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <returns></returns>
		public static Vector2D Cross(Vector2D a, double s)
		{
			Vector2D result;
			result.X = s * a.Y;
			result.Y = -s * a.X;
			return result;
		}

		/// <summary>
		/// Returns (-s * a.Y, s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <param name="result"></param>
		public static void Cross(double s, in Vector2D a, out Vector2D result)
		{
			double aX = a.X;
			result.X = -s * a.Y;
			result.Y = s * aX;
		}

		/// <summary>
		/// Returns (-s * a.Y, s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <param name="result"></param>
		public static void Cross(double s, Vector2D a, out Vector2D result)
		{
			result.X = -s * a.Y;
			result.Y = s * a.X;
		}

		/// <summary>
		/// Returns (-s * a.Y, s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <returns></returns>
		public static Vector2D Cross(double s, in Vector2D a)
		{
			Vector2D result;
			result.X = -s * a.Y;
			result.Y = s * a.X;
			return result;
		}

		/// <summary>
		/// Returns (-s * a.Y, s * a.X).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="s"></param>
		/// <returns></returns>
		public static Vector2D Cross(double s, Vector2D a)
		{
			Vector2D result;
			result.X = -s * a.Y;
			result.Y = s * a.X;
			return result;
		}

		/// <summary>
		/// Returns this.X * b.Y - this.Y * b.X.
		/// </summary>
		/// <param name="b"></param>
		/// <returns></returns>
		public double Cross(Vector2D b)
		{
			return this.X * b.Y - this.Y * b.X;
		}
		
		/// <summary>
		/// Returns this.X * b.Y - this.Y * b.X.
		/// </summary>
		/// <param name="b"></param>
		/// <returns></returns>
		public double Cross(in Vector2D b)
		{
			return this.X * b.Y - this.Y * b.X;
		}

		/// <summary>
		/// Returns (s * this.Y, -s * this.X).
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public Vector2D Cross(double s)
		{
			Vector2D result;
			result.X = s * this.Y;
			result.Y = -s * this.X;
			return result;
		}
	}
}
