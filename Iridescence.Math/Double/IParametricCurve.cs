﻿namespace Iridescence.Math
{
	/// <summary>
	/// Interface for a two-dimensional parametric curve.
	/// </summary>
	public interface IParametricCurve
	{
		/// <summary>
		/// Gets the value at the specified position.
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		Vector2D GetValue(double t);

		/// <summary>
		/// Returns an axis-aligned bounding rectangle for the curve.
		/// </summary>
		RectangleD GetBounds();
	}
}
