﻿using System.Collections.Generic;

namespace Iridescence.Math
{
	public interface IIntersectable<in T>
	{
		IEnumerable<double> GetIntersections(T other, double tolerance);
	}
}
