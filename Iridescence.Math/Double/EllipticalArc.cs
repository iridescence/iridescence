﻿using System;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents an elliptic arc.
	/// </summary>
	public struct EllipticalArc : IEquatable<EllipticalArc>, IParametricCurve, ISplittable<EllipticalArc>
	{
		#region Fields

		/// <summary>
		/// The center of the ellipse.
		/// </summary>
		public Vector2D Center;
		
		/// <summary>
		/// The radii of the ellipse.
		/// </summary>
		public Vector2D Radius;
		
		/// <summary>
		/// The start angle (in radians) of the elliptical arc.
		/// </summary>
		public double StartAngle;
		
		/// <summary>
		/// The delta between the end and start angle (in radians) of the elliptical arc.
		/// </summary>
		public double AngleDelta;
		
		/// <summary>
		/// The angle from the x-axis of the current coordinate system to the x-axis of the ellipse.
		/// </summary>
		public double XAngle;
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the end angle (in radians) of the elliptical arc.
		/// </summary>
		public double EndAngle => this.StartAngle + this.AngleDelta;

		public Vector2D this[double t] => this.GetValue(t);
			
		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="EllipticalArc"/> class. 
		/// </summary>
		public EllipticalArc(Vector2D center, Vector2D radius, double startAngle, double angleDelta)
		{
			this.Center = center;
			this.Radius = radius;
			this.StartAngle = startAngle;
			this.AngleDelta = angleDelta;
			this.XAngle = 0.0;
		}

		#endregion
		
		#region Methods

		private Vector2D getValueFromAngle(double angle)
		{
			double cosX = Utility.Cos(this.XAngle);
			double sinX = Utility.Sin(this.XAngle);
			
			Vector2D r;
			r.X = Utility.Cos(angle) * this.Radius.X;
			r.Y = Utility.Sin(angle) * this.Radius.Y;

			Vector2D result;
			result.X = this.Center.X + cosX * r.X - sinX * r.Y;
			result.Y = this.Center.Y + sinX * r.X + cosX * r.Y;
			return result;
		}

		public Vector2D GetValue(double t)
		{
			return this.getValueFromAngle(this.StartAngle + this.AngleDelta * t);
		}

		public RectangleD GetBounds()
		{
			Vector2D p1 = this.GetValue(0.0);
			Vector2D p2 = this.GetValue(1.0);

			Vector2D.Min(p1, p2, out Vector2D min);
			Vector2D.Max(p1, p2, out Vector2D max);
			Vector2D.Subtract(max, min, out max);
			RectangleD bounds = new RectangleD(min, max);

			double cosX = Utility.Cos(this.XAngle);
			double sinX = Utility.Sin(this.XAngle);

			Vector4D angles;
			angles.X = Utility.Atan2(-this.Radius.Y * sinX, this.Radius.X * cosX);
			angles.Y = Utility.WrapAngle(angles.X + ConstantsD.Pi);
			angles.Z = Utility.Atan2(this.Radius.Y * cosX, this.Radius.X * sinX);
			angles.W = Utility.WrapAngle(angles.Z + ConstantsD.Pi);

			double startAngle = Utility.WrapAngle(this.StartAngle);
			double angleDelta = this.AngleDelta;

			if (angleDelta < 0.0)
			{
				startAngle += angleDelta;
				angleDelta = -angleDelta;
			}
			
			for (int i = 0; i < 4; ++i)
			{
				double angle = angles[i];
				if (angle >= startAngle && angle <= startAngle + angleDelta)
				{
					Vector2D r;
					r.X = Utility.Cos(angle) * this.Radius.X;
					r.Y = Utility.Sin(angle) * this.Radius.Y;

					Vector2D p;
					p.X = this.Center.X + cosX * r.X - sinX * r.Y;
					p.Y = this.Center.Y + sinX * r.X + cosX * r.Y;

					RectangleD.Union(in bounds, in p, out bounds);
				}
			}

			return bounds;
		}

		public void Split(double t, out EllipticalArc first, out EllipticalArc second)
		{
			first = this;
			second = this;

			first.AngleDelta *= t;
			second.StartAngle = first.EndAngle;
			second.AngleDelta *= (1.0 - t);
		}

		/// <summary>
		/// Tessellates the arc.
		/// </summary>
		/// <param name="tolerance">The approximate maximum distance between two tessellated points.</param>
		/// <param name="callback">A callback that is called for each computed point.</param>
		public void Tessellate(double tolerance, Action<Vector2D> callback)
		{
			const double minStep = 1e-6;
			const double maxStep = ConstantsD.PiOver2;

			double toleranceSq = tolerance * tolerance;
			bool reverse = this.AngleDelta < 0.0;
			double startAngle = this.StartAngle;
			double angleDelta = Utility.Absolute(this.AngleDelta);

			// Start point.
			Vector2D previous = this.getValueFromAngle(startAngle);
			//callback(previous);
			
			double angle = 0.0;
			double step = maxStep;
			
			while(angle < angleDelta)
			{
				// Compute next angle.
				double nextAngle = angle + step;

				if (nextAngle > angleDelta)
				{
					nextAngle = angleDelta;
				}

				// Get point on ellipse.
				double actualAngle = reverse ? startAngle - nextAngle : startAngle + nextAngle;
				Vector2D next = this.getValueFromAngle(actualAngle);

				// Calculate distance to previous point.
				double distanceSq = Vector2D.DistanceSquared(previous, next);

				if (distanceSq > toleranceSq && step > minStep)
				{
					// Too far away.
					step = Utility.Max(step * 0.5, minStep);
					continue;
				}

				if (distanceSq < toleranceSq * 0.125 && step < maxStep)
				{
					// Too close.
					step = Utility.Min(step * 2.0, maxStep);
					continue;
				}

				// New point found.
				callback(next);
				previous = next;
				angle = nextAngle;
			}
		}

		public bool Equals(EllipticalArc other)
		{
			return this.Center.Equals(other.Center) && this.Radius.Equals(other.Radius) && this.StartAngle.Equals(other.StartAngle) && this.AngleDelta.Equals(other.AngleDelta) && this.XAngle.Equals(other.XAngle);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is EllipticalArc && Equals((EllipticalArc)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = this.Center.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Radius.GetHashCode();
				hashCode = (hashCode * 397) ^ this.StartAngle.GetHashCode();
				hashCode = (hashCode * 397) ^ this.AngleDelta.GetHashCode();
				hashCode = (hashCode * 397) ^ this.XAngle.GetHashCode();
				return hashCode;
			}
		}

		/// <summary>
		/// Converts an elliptical arc from end-point parameterization to the center-radius parameterization used by the <see cref="EllipticalArc"/> structure.
		/// </summary>
		/// <param name="p1">The start point of the arc.</param>
		/// <param name="p2">The end point of the arc.</param>
		/// <param name="radius">The arc bounding box or X- and Y-radius.</param>
		/// <param name="xAngle">The angle from the x-axis of the current coordinate system to the x-axis of the ellipse.</param>
		/// <param name="isLargeArc">If true, an arc spanning less than or equal to 180 degrees is chosen. If false, an arc spanning greater than 180 degrees is chosen.</param>
		/// <param name="sweepCcw">Specifies whether the sweep direction is counter-clockwise (true) or clockwise (false).</param>
		/// <param name="arc">The resulting <see cref="EllipticalArc"/>.</param>
		/// <returns>True, if the conversion was successful. False otherwise.</returns>
		/// <remarks>In the event that false is returned, a straight line between <see cref="p1"/> and <see cref="p2"/> should be used instead.</remarks>
		public static bool FromEndPointParameterization(Vector2D p1, Vector2D p2, Vector2D radius, double xAngle, bool isLargeArc, bool sweepCcw, out EllipticalArc arc)
		{
			// This is pretty much 1:1 taken from https://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes
			arc.XAngle = xAngle;
			arc.Radius.X = Utility.Absolute(radius.X);
			arc.Radius.Y = Utility.Absolute(radius.Y);

			double rx2 = arc.Radius.X * arc.Radius.X;
			double ry2 = arc.Radius.Y * arc.Radius.Y;

			if (rx2 < double.Epsilon || ry2 < double.Epsilon)
			{
				arc = default(EllipticalArc);
				return false;
			}

			double phi = xAngle;
			double cosPhi = Utility.Cos(phi);
			double sinPhi = Utility.Sin(phi);

			double mx = (p1.X - p2.X) * 0.5;
			double my = (p1.Y - p2.Y) * 0.5;

			double x1s = cosPhi * mx + sinPhi * my;
			double y1s = -sinPhi * mx + cosPhi * my;
			double x1s2 = x1s * x1s;
			double y1s2 = y1s * y1s;

			double d = x1s2 / rx2 + y1s2 / ry2;
			if (d > 1.0)
			{
				d = Utility.Sqrt(d);
				arc.Radius.X *= d;
				arc.Radius.Y *= d;
				rx2 = arc.Radius.X * arc.Radius.X;
				ry2 = arc.Radius.Y * arc.Radius.Y;
			}

			double s = (rx2 * ry2 - rx2 * y1s2 - ry2 * x1s2) / (rx2 * y1s2 + ry2 * x1s2);
			if (s > double.Epsilon)
				s = Utility.Sqrt(s);
			else
				s = 0.0;
			
			if (isLargeArc ^ sweepCcw)
				s = -s;

			double csx = arc.Radius.X * y1s / arc.Radius.Y * s;
			double csy = -arc.Radius.Y * x1s / arc.Radius.X * s;

			arc.Center.X = cosPhi * csx - sinPhi * csy + (p1.X + p2.X) * 0.5;
			arc.Center.Y = sinPhi * csx + cosPhi * csy + (p1.Y + p2.Y) * 0.5;
			
			double p1x = (x1s - csx) / arc.Radius.X;
			double p1y = (y1s - csy) / arc.Radius.Y;
			double p2x = (-x1s - csx) / arc.Radius.X;
			double p2y = (-y1s - csy) / arc.Radius.Y;

			double angle(double ux, double uy, double vx, double vy)
			{
				double dot = ux * vx + uy * vy;
				double u = ux * ux + uy * uy;
				double v = vx * vx + vy * vy;
				double w = dot / Utility.Sqrt(u * v);

				if (w <= -1.0)
					w = ConstantsD.Pi;
				else if (w >= 1.0)
					w = 0.0;
				else
					w = Utility.Acos(w);

				if (ux * vy - uy * vx > 0.0)
					return w;

				return -w;
			}

			arc.StartAngle = angle(1.0, 0.0, p1x, p1y);
			arc.AngleDelta = angle(p1x, p1y, p2x, p2y);

			return true;
		}
		
		public static bool operator ==(EllipticalArc left, EllipticalArc right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(EllipticalArc left, EllipticalArc right)
		{
			return !left.Equals(right);
		}

		#endregion
	}
}
