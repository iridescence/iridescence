﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;

namespace Iridescence.Math
{
	public partial struct Matrix4x4D : IEquatable<Matrix4x4D>
	{
		[System.Diagnostics.Contracts.Pure]
		public override bool Equals(object obj)
		{
			if (!(obj is Matrix4x4D)) return false;
			Matrix4x4D other = (Matrix4x4D)obj;
			return this.M11 == other.M11
			    && this.M12 == other.M12
			    && this.M13 == other.M13
			    && this.M14 == other.M14
			    && this.M21 == other.M21
			    && this.M22 == other.M22
			    && this.M23 == other.M23
			    && this.M24 == other.M24
			    && this.M31 == other.M31
			    && this.M32 == other.M32
			    && this.M33 == other.M33
			    && this.M34 == other.M34
			    && this.M41 == other.M41
			    && this.M42 == other.M42
			    && this.M43 == other.M43
			    && this.M44 == other.M44;
		}
		
		[System.Diagnostics.Contracts.Pure]
		bool IEquatable<Matrix4x4D>.Equals(Matrix4x4D other)
		{
			return this.M11 == other.M11
			    && this.M12 == other.M12
			    && this.M13 == other.M13
			    && this.M14 == other.M14
			    && this.M21 == other.M21
			    && this.M22 == other.M22
			    && this.M23 == other.M23
			    && this.M24 == other.M24
			    && this.M31 == other.M31
			    && this.M32 == other.M32
			    && this.M33 == other.M33
			    && this.M34 == other.M34
			    && this.M41 == other.M41
			    && this.M42 == other.M42
			    && this.M43 == other.M43
			    && this.M44 == other.M44;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public bool Equals(in Matrix4x4D value)
		{
			return this.M11 == value.M11
			    && this.M12 == value.M12
			    && this.M13 == value.M13
			    && this.M14 == value.M14
			    && this.M21 == value.M21
			    && this.M22 == value.M22
			    && this.M23 == value.M23
			    && this.M24 == value.M24
			    && this.M31 == value.M31
			    && this.M32 == value.M32
			    && this.M33 == value.M33
			    && this.M34 == value.M34
			    && this.M41 == value.M41
			    && this.M42 == value.M42
			    && this.M43 == value.M43
			    && this.M44 == value.M44;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool Equals(in Matrix4x4D left, in Matrix4x4D right)
		{
			return left.M11 == right.M11
			    && left.M12 == right.M12
			    && left.M13 == right.M13
			    && left.M14 == right.M14
			    && left.M21 == right.M21
			    && left.M22 == right.M22
			    && left.M23 == right.M23
			    && left.M24 == right.M24
			    && left.M31 == right.M31
			    && left.M32 == right.M32
			    && left.M33 == right.M33
			    && left.M34 == right.M34
			    && left.M41 == right.M41
			    && left.M42 == right.M42
			    && left.M43 == right.M43
			    && left.M44 == right.M44;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool operator ==(in Matrix4x4D left, in Matrix4x4D right)
		{
			return left.M11 == right.M11
			    && left.M12 == right.M12
			    && left.M13 == right.M13
			    && left.M14 == right.M14
			    && left.M21 == right.M21
			    && left.M22 == right.M22
			    && left.M23 == right.M23
			    && left.M24 == right.M24
			    && left.M31 == right.M31
			    && left.M32 == right.M32
			    && left.M33 == right.M33
			    && left.M34 == right.M34
			    && left.M41 == right.M41
			    && left.M42 == right.M42
			    && left.M43 == right.M43
			    && left.M44 == right.M44;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool operator !=(in Matrix4x4D left, in Matrix4x4D right)
		{
			return left.M11 != right.M11
			    || left.M12 != right.M12
			    || left.M13 != right.M13
			    || left.M14 != right.M14
			    || left.M21 != right.M21
			    || left.M22 != right.M22
			    || left.M23 != right.M23
			    || left.M24 != right.M24
			    || left.M31 != right.M31
			    || left.M32 != right.M32
			    || left.M33 != right.M33
			    || left.M34 != right.M34
			    || left.M41 != right.M41
			    || left.M42 != right.M42
			    || left.M43 != right.M43
			    || left.M44 != right.M44;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public bool ApproximatelyEquals(in Matrix4x4D value, double tolerance)
		{
			return System.Math.Abs(this.M11 - value.M11) < tolerance
			    && System.Math.Abs(this.M12 - value.M12) < tolerance
			    && System.Math.Abs(this.M13 - value.M13) < tolerance
			    && System.Math.Abs(this.M14 - value.M14) < tolerance
			    && System.Math.Abs(this.M21 - value.M21) < tolerance
			    && System.Math.Abs(this.M22 - value.M22) < tolerance
			    && System.Math.Abs(this.M23 - value.M23) < tolerance
			    && System.Math.Abs(this.M24 - value.M24) < tolerance
			    && System.Math.Abs(this.M31 - value.M31) < tolerance
			    && System.Math.Abs(this.M32 - value.M32) < tolerance
			    && System.Math.Abs(this.M33 - value.M33) < tolerance
			    && System.Math.Abs(this.M34 - value.M34) < tolerance
			    && System.Math.Abs(this.M41 - value.M41) < tolerance
			    && System.Math.Abs(this.M42 - value.M42) < tolerance
			    && System.Math.Abs(this.M43 - value.M43) < tolerance
			    && System.Math.Abs(this.M44 - value.M44) < tolerance;
		}
		
		[System.Diagnostics.Contracts.Pure]
		public static bool ApproximatelyEquals(in Matrix4x4D left, in Matrix4x4D right, double tolerance)
		{
			return System.Math.Abs(left.M11 - right.M11) < tolerance
			    && System.Math.Abs(left.M12 - right.M12) < tolerance
			    && System.Math.Abs(left.M13 - right.M13) < tolerance
			    && System.Math.Abs(left.M14 - right.M14) < tolerance
			    && System.Math.Abs(left.M21 - right.M21) < tolerance
			    && System.Math.Abs(left.M22 - right.M22) < tolerance
			    && System.Math.Abs(left.M23 - right.M23) < tolerance
			    && System.Math.Abs(left.M24 - right.M24) < tolerance
			    && System.Math.Abs(left.M31 - right.M31) < tolerance
			    && System.Math.Abs(left.M32 - right.M32) < tolerance
			    && System.Math.Abs(left.M33 - right.M33) < tolerance
			    && System.Math.Abs(left.M34 - right.M34) < tolerance
			    && System.Math.Abs(left.M41 - right.M41) < tolerance
			    && System.Math.Abs(left.M42 - right.M42) < tolerance
			    && System.Math.Abs(left.M43 - right.M43) < tolerance
			    && System.Math.Abs(left.M44 - right.M44) < tolerance;
		}
		
	}
}
