﻿using System;

namespace Iridescence.Math
{
	public partial struct Matrix4x4D
	{
		public Matrix4x4D Invert()
		{
			Matrix4x4D.Invert(this, out Matrix4x4D result);
			return result;
		}

		public void Invert(out Matrix4x4D result)
		{
			Matrix4x4D.Invert(this, out result);
		}

		public static void Invert(in Matrix4x4D matrix, out Matrix4x4D result)
		{
			// Compute 2x2 determinants.
			double s0 = matrix.M11 * matrix.M22 - matrix.M12 * matrix.M21;
			double s1 = matrix.M11 * matrix.M32 - matrix.M12 * matrix.M31;
			double s2 = matrix.M11 * matrix.M42 - matrix.M12 * matrix.M41;
			double s3 = matrix.M21 * matrix.M32 - matrix.M22 * matrix.M31;
			double s4 = matrix.M21 * matrix.M42 - matrix.M22 * matrix.M41;
			double s5 = matrix.M31 * matrix.M42 - matrix.M32 * matrix.M41;
			double c5 = matrix.M33 * matrix.M44 - matrix.M34 * matrix.M43;
			double c4 = matrix.M23 * matrix.M44 - matrix.M24 * matrix.M43;
			double c3 = matrix.M23 * matrix.M34 - matrix.M24 * matrix.M33;
			double c2 = matrix.M13 * matrix.M44 - matrix.M14 * matrix.M43;
			double c1 = matrix.M13 * matrix.M34 - matrix.M14 * matrix.M33;
			double c0 = matrix.M13 * matrix.M24 - matrix.M14 * matrix.M23;
		
			// Compute 4x4 determinant.
			double d = (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0);
			if (d >= -double.Epsilon && d <= double.Epsilon)
				throw new InvalidOperationException("This matrix can't be inverted.");

			d = 1.0 / d;
		
			// Compute new values.
			double m11 = (matrix.M22 * c5 - matrix.M32 * c4 + matrix.M42 * c3) * d;
			double m21 = (-matrix.M21 * c5 + matrix.M31 * c4 - matrix.M41 * c3) * d;
			double m31 = (matrix.M24 * s5 - matrix.M34 * s4 + matrix.M44 * s3) * d;
			double m41 = (-matrix.M23 * s5 + matrix.M33 * s4 - matrix.M43 * s3) * d;
		
			double m12 = (-matrix.M12 * c5 + matrix.M32 * c2 - matrix.M42 * c1) * d;
			double m22 = (matrix.M11 * c5 - matrix.M31 * c2 + matrix.M41 * c1) * d;
			double m32 = (-matrix.M14 * s5 + matrix.M34 * s2 - matrix.M44 * s1) * d;
			double m42 = (matrix.M13 * s5 - matrix.M33 * s2 + matrix.M43 * s1) * d;
		
			double m13 = (matrix.M12 * c4 - matrix.M22 * c2 + matrix.M42 * c0) * d;
			double m23 = (-matrix.M11 * c4 + matrix.M21 * c2 - matrix.M41 * c0) * d;
			double m33 = (matrix.M14 * s4 - matrix.M24 * s2 + matrix.M44 * s0) * d;
			double m43 = (-matrix.M13 * s4 + matrix.M23 * s2 - matrix.M43 * s0) * d;
		
			double m14 = (-matrix.M12 * c3 + matrix.M22 * c1 - matrix.M32 * c0) * d;
			double m24 = (matrix.M11 * c3 - matrix.M21 * c1 + matrix.M31 * c0) * d;
			double m34 = (-matrix.M14 * s3 + matrix.M24 * s1 - matrix.M34 * s0) * d;
			double m44 = (matrix.M13 * s3 - matrix.M23 * s1 + matrix.M33 * s0) * d;

			result.M11 = m11; result.M12 = m12; result.M13 = m13; result.M14 = m14;
			result.M21 = m21; result.M22 = m22; result.M23 = m23; result.M24 = m24;
			result.M31 = m31; result.M32 = m32; result.M33 = m33; result.M34 = m34;
			result.M41 = m41; result.M42 = m42; result.M43 = m43; result.M44 = m44;
		}
	}
}
