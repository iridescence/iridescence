﻿using System;
using System.Collections.Generic;

namespace Iridescence.Math
{
	/// <summary>
	/// Curve methods.
	/// </summary>
	public static class Curve
	{
		#region Methods

		private readonly struct SubCurve<T> where T : IParametricCurve
		{
			public readonly T Curve;
			public readonly RectangleD Bounds;
			public readonly double Min;
			public readonly double Max;
			public double Mid => 0.5 * (this.Min + this.Max);
			
			public SubCurve(T curve, double min, double max)
			{
				this.Curve = curve;
				this.Bounds = curve.GetBounds();
				this.Min = min;
				this.Max = max;
			}
		}
		
		private readonly struct Pair<T1, T2> 
			where T1 : IParametricCurve
			where T2 : IParametricCurve
		{
			public readonly SubCurve<T1> First;
			public readonly SubCurve<T2> Second;

			public Pair(T1 first, double firstMin, double firstMax, T2 second, double secondMin, double secondMax)
			{
				this.First = new SubCurve<T1>(first, firstMin, firstMax);
				this.Second = new SubCurve<T2>(second, secondMin, secondMax);
			}

			public bool IsWithinTolerance(double tolerance)
			{
				return this.First.Bounds.Area + this.Second.Bounds.Area < tolerance;
			}

			private static bool test(double x, double min, double max)
			{
				return x >= min && x <= max;
			}

			public bool IsIntersecting()
			{
				return (test(this.First.Bounds.X, this.Second.Bounds.X, this.Second.Bounds.X + this.Second.Bounds.Width) ||
				        test(this.Second.Bounds.X, this.First.Bounds.X, this.First.Bounds.X + this.First.Bounds.Width)) &&
				       (test(this.First.Bounds.Y, this.Second.Bounds.Y, this.Second.Bounds.Y + this.Second.Bounds.Height) ||
				        test(this.Second.Bounds.Y, this.First.Bounds.Y, this.First.Bounds.Y + this.First.Bounds.Height));
			}
		}

		/// <summary>
		/// Merge results that are very close together (less than the specified tolerance).
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="curve"></param>
		/// <param name="positions"></param>
		/// <param name="tolerance"></param>
		/// <returns></returns>
		private static List<double> merge<T>(T curve, List<double> positions, double tolerance) where T : IParametricCurve
		{
			List<double> merged = new List<double>();
			List<int> mergedIndices = new List<int>();
			double toleranceSq = tolerance * tolerance;
			while (positions.Count > 0)
			{
				double t = positions[0];
				Vector2D p = curve.GetValue(t);
				positions.RemoveAt(0);

				double tCum = t;
				Vector2D pCum = p;
				double factor = 1.0;

				int num = 1;

				mergedIndices.Clear();
				for (int i = 0; i < positions.Count; ++i)
				{
					double t2 = positions[i];
					Vector2D p2 = curve.GetValue(t2);

					if (Vector2D.DistanceSquared(pCum * factor, p2) < toleranceSq)
					{
						tCum += t2;
						pCum += p2;
						++num;
						factor = 1.0 / num;
						mergedIndices.Add(i);
					}
				}

				tCum *= factor;
				pCum = curve.GetValue(tCum);

				if (Vector2D.DistanceSquared(p, pCum) > toleranceSq * 2.0)
				{
					merged.Add(t);
				}
				else
				{
					for (int i = mergedIndices.Count - 1; i >= 0; --i)
						positions.RemoveAt(mergedIndices[i]);

					merged.Add(tCum);
				}
			}

			return merged;
		}

		/// <summary>
		/// Returns all intersections between two curves.
		/// </summary>
		/// <typeparam name="T1"></typeparam>
		/// <typeparam name="T2"></typeparam>
		/// <param name="first"></param>
		/// <param name="second"></param>
		/// <param name="tolerance"></param>
		/// <returns>The intersections, expressed as positions on the first curve.</returns>
		public static IEnumerable<double> GetIntersections<T1, T2>(T1 first, T2 second, double tolerance/*, int debugStep, out Pair<T>[] debugPairs*/) 
			where T1 : IParametricCurve, ISplittable<T1>
			where T2 : IParametricCurve, ISplittable<T2>
		{
			if (first is IIntersectable<T2> inter1)
				return inter1.GetIntersections(second, tolerance);

			if (second is IIntersectable<T1> inter2)
				return inter2.GetIntersections(first, tolerance);

			double toleranceSq = 0.25 * tolerance * tolerance;

			Pair<T1, T2>[] pairs = new Pair<T1, T2>[8];

			void ensureCapacity(int capacity)
			{
				int newSize = pairs.Length;
				while (newSize < capacity)
				{
					newSize *= 2;
				}
				Array.Resize(ref pairs, newSize);
			}
			
			List<double> results = new List<double>();

			// Add first pair that spans the whole curve.
			pairs[0] = new Pair<T1, T2>(first, 0.0, 1.0, second, 0.0, 1.0);
			int numPairs = 1;
			
			while (numPairs > 0 /*&& debugStep-- > 0*/)
			{
				int numNewPairs = 0;
				for (int i = 0; i < numPairs; ++i)
				{
					// Check if that pair is intersecting at all.
					if (!pairs[i].IsIntersecting())
						continue;

					// Check if the pair is within the specified tolerance.
					if (pairs[i].IsWithinTolerance(toleranceSq))
					{
						// Check whether lines through the start and end points the sub curves intersect.
						if (LineSegmentD.IsIntersecting(
							pairs[i].First.Curve.GetValue(0.0), pairs[i].First.Curve.GetValue(1.0),
							pairs[i].Second.Curve.GetValue(0.0), pairs[i].Second.Curve.GetValue(1.0)))
						{
							// Add result.
							results.Add(pairs[i].First.Mid);
						}
					}
					else
					{
						// Split first and second sub curves of the pair into 4 new pairs.
						pairs[i].First.Curve.Split(0.5, out T1 first1, out T1 first2);
						pairs[i].Second.Curve.Split(0.5, out T2 second1, out T2 second2);

						ensureCapacity(numPairs + numNewPairs + 4);
						pairs[numPairs + numNewPairs++] = new Pair<T1, T2>(first1, pairs[i].First.Min, pairs[i].First.Mid, second1, pairs[i].Second.Min, pairs[i].Second.Mid);
						pairs[numPairs + numNewPairs++] = new Pair<T1, T2>(first2, pairs[i].First.Mid, pairs[i].First.Max, second1, pairs[i].Second.Min, pairs[i].Second.Mid);
						pairs[numPairs + numNewPairs++] = new Pair<T1, T2>(first1, pairs[i].First.Min, pairs[i].First.Mid, second2, pairs[i].Second.Mid, pairs[i].Second.Max);
						pairs[numPairs + numNewPairs++] = new Pair<T1, T2>(first2, pairs[i].First.Mid, pairs[i].First.Max, second2, pairs[i].Second.Mid, pairs[i].Second.Max);
					}
				}

				// Move new pairs to the beginning of the array for the next loop.
				Array.Copy(pairs, numPairs, pairs, 0, numNewPairs);
				numPairs = numNewPairs;
			}

			if (results.Count > 1)
				results = merge(first, results, tolerance);
			
			//debugPairs = new Pair<T>[numPairs];
			//Array.Copy(pairs, 0, debugPairs, 0, numPairs);
			
			return results;
		}

		#endregion
	}
}
