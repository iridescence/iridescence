﻿namespace Iridescence.Math
{
	public partial struct Matrix2x2D
	{
		#region Matrix2x2D^T * Matrix2x2D

		/// <summary>
		/// Multiplies a transposed matrix with a matrix.
		/// </summary>
		/// <param name="leftT">The left matrix. The transposed matrix will be used.</param>
		/// <param name="right">The right matrix</param>
		/// <param name="result">left^T * right</param>
		public static void MultiplyTransposed(ref Matrix2x2D leftT, ref Matrix2x2D right, out Matrix2x2D result)
		{
			double m11 = leftT.M11 * right.M11 + leftT.M21 * right.M21;
			double m12 = leftT.M11 * right.M12 + leftT.M21 * right.M22;
			double m21 = leftT.M12 * right.M11 + leftT.M22 * right.M21;
			double m22 = leftT.M12 * right.M12 + leftT.M22 * right.M22;
			result.M11 = m11; result.M12 = m12;
			result.M21 = m21; result.M22 = m22;
		}

		/// <summary>
		/// Multiplies a transposed matrix with a matrix.
		/// </summary>
		/// <param name="leftT">The left matrix. The transposed matrix will be used.</param>
		/// <param name="right">The right matrix</param>
		/// <param name="result">left^T * right</param>
		public static void MultiplyTransposed(Matrix2x2D leftT, ref Matrix2x2D right, out Matrix2x2D result)
		{
			double m11 = leftT.M11 * right.M11 + leftT.M21 * right.M21;
			double m12 = leftT.M11 * right.M12 + leftT.M21 * right.M22;
			double m21 = leftT.M12 * right.M11 + leftT.M22 * right.M21;
			double m22 = leftT.M12 * right.M12 + leftT.M22 * right.M22;
			result.M11 = m11; result.M12 = m12;
			result.M21 = m21; result.M22 = m22;
		}

		/// <summary>
		/// Multiplies a transposed matrix with a matrix.
		/// </summary>
		/// <param name="leftT">The left matrix. The transposed matrix will be used.</param>
		/// <param name="right">The right matrix</param>
		/// <param name="result">left^T * right</param>
		public static void MultiplyTransposed(ref Matrix2x2D leftT, Matrix2x2D right, out Matrix2x2D result)
		{
			double m11 = leftT.M11 * right.M11 + leftT.M21 * right.M21;
			double m12 = leftT.M11 * right.M12 + leftT.M21 * right.M22;
			double m21 = leftT.M12 * right.M11 + leftT.M22 * right.M21;
			double m22 = leftT.M12 * right.M12 + leftT.M22 * right.M22;
			result.M11 = m11; result.M12 = m12;
			result.M21 = m21; result.M22 = m22;
		}

		/// <summary>
		/// Multiplies a transposed matrix with a matrix.
		/// </summary>
		/// <param name="leftT">The left matrix. The transposed matrix will be used.</param>
		/// <param name="right">The right matrix</param>
		/// <param name="result">left^T * right</param>
		public static void MultiplyTransposed(Matrix2x2D leftT, Matrix2x2D right, out Matrix2x2D result)
		{
			result.M11 = leftT.M11 * right.M11 + leftT.M21 * right.M21;
			result.M12 = leftT.M11 * right.M12 + leftT.M21 * right.M22;
			result.M21 = leftT.M12 * right.M11 + leftT.M22 * right.M21;
			result.M22 = leftT.M12 * right.M12 + leftT.M22 * right.M22;
		}

		#endregion
	}
}
