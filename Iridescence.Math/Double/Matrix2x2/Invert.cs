﻿using System;

namespace Iridescence.Math
{
	public partial struct Matrix2x2D
	{
		public Matrix2x2D Invert()
		{
			Matrix2x2D.Invert(in this, out Matrix2x2D result);
			return result;
		}

		public void Invert(out Matrix2x2D result)
		{
			Matrix2x2D.Invert(in this, out result);
		}

		public static void Invert(in Matrix2x2D matrix, out Matrix2x2D result)
		{
			double d = matrix.Determinant;
			if (d >= -double.Epsilon && d <= double.Epsilon)
				throw new InvalidOperationException("This matrix can't be inverted.");
			d = 1.0 / d;

			double m11 = matrix.M22 * d;
			double m12 = -matrix.M12 * d;
			
			double m21 = -matrix.M21 * d;
			double m22 = matrix.M11 * d;

			result.M11 = m11; result.M12 = m12;
			result.M21 = m21; result.M22 = m22;
		}
	}
}
