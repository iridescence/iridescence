﻿namespace Iridescence.Math
{
	public partial struct Matrix2x2D
	{
		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix2x2D a, ref Vector2D b, out Vector2D x)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(ref Matrix2x2D a, Vector2D b, out Vector2D x)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix2x2D a, ref Vector2D b, out Vector2D x)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <param name="x">The resulting vector x.</param>
		public static void Solve(Matrix2x2D a, Vector2D b, out Vector2D x)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2D Solve(ref Matrix2x2D a, ref Vector2D b)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			Vector2D x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2D Solve(ref Matrix2x2D a, Vector2D b)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			Vector2D x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}


		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2D Solve(Matrix2x2D a, ref Vector2D b)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			Vector2D x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}

		/// <summary>
		/// Solves A * x = b.
		/// </summary>
		/// <param name="a">The matrix A.</param>
		/// <param name="b">The vector b.</param>
		/// <returns>The resulting vector x.</returns>
		public static Vector2D Solve(Matrix2x2D a, Vector2D b)
		{
			double det = a.M11 * a.M22 - a.M12 * a.M21;
			if (det != 0.0)
				det = 1.0 / det;

			Vector2D x;
			x.X = det * (a.M22 * b.X - a.M12 * b.Y);
			x.Y = det * (a.M11 * b.Y - a.M21 * b.X);
			return x;
		}
	}
}
