﻿using System;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a rectangle with double-precision floating-point position and size.
	/// </summary>
	[Serializable]
	public struct RectangleD : IEquatable<RectangleD>
	{
		#region Fields

		/// <summary>
		/// The X position of the rectangle.
		/// </summary>
		public double X;

		/// <summary>
		/// The Y position of the rectangle.
		/// </summary>
		public double Y;

		/// <summary>
		/// The width of the rectangle.
		/// </summary>
		public double Width;

		/// <summary>
		/// The height of the rectangle.
		/// </summary>
		public double Height;

		/// <summary>
		/// Gets a rectangle with all values set to 0.
		/// </summary>
		public static readonly RectangleD Zero = new RectangleD(0.0, 0.0, 0.0, 0.0);

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the position of the rectangle.
		/// </summary>
		public Vector2D Position
		{
			get
			{
				Vector2D v;
				v.X = this.X;
				v.Y = this.Y;
				return v;
			}
			set
			{
				this.X = value.X;
				this.Y = value.Y;
			}
		}

		/// <summary>
		/// Gets or sets the size of the rectangle.
		/// </summary>
		public Vector2D Size
		{
			get
			{
				Vector2D v;
				v.X = this.Width;
				v.Y = this.Height;
				return v;
			}
			set
			{
				this.Width = value.X;
				this.Height = value.Y;
			}
		}

		/// <summary>
		/// Gets the area of the rectangle.
		/// </summary>
		public double Area => this.Width * this.Height;

		/// <summary>
		/// Gets the position of the rectangle's center.
		/// </summary>
		public Vector2D Center
		{
			get
			{
				Vector2D v;
				v.X = this.X + this.Width * 0.5;
				v.Y = this.Y + this.Height * 0.5;
				return v;
			}
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new rectangle.
		/// </summary>
		public RectangleD(double x, double y, double width, double height)
		{
			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;
		}

		/// <summary>
		/// Creates a new rectangle.
		/// </summary>
		public RectangleD(Vector2D position, Vector2D size)
		{
			this.X = position.X;
			this.Y = position.Y;
			this.Width = size.X;
			this.Height = size.Y;
		}

		#endregion

		#region Methods

		private static bool test(double x, double min, double max)
		{
			return x >= min && x < max;
		}

		/// <summary>
		/// Returns a value that indicates whether a point lies within the rectangle.
		/// </summary>
		public static bool IsPointInside(in RectangleD rect, double x, double y)
		{
			return test(x, rect.X, rect.X + rect.Width) && test(y, rect.Y, rect.Y + rect.Height);
		}

		/// <summary>
		/// Returns a value that indicates whether a point lies within the rectangle.
		/// </summary>
		public static bool IsPointInside(in RectangleD rect, in Vector2D v)
		{
			return test(v.X, rect.X, rect.X + rect.Width) && test(v.Y, rect.Y, rect.Y + rect.Height);
		}

		/// <summary>
		/// Returns a value that indicates whether the specified inner rectangle lies completely within the outer rectangle.
		/// </summary>
		public static bool IsRectangleInside(in RectangleD outer, in RectangleD inner)
		{
			return outer.X <= inner.X && outer.Y <= inner.Y && (outer.X + outer.Width) >= (inner.X + inner.Width) && (outer.Y + outer.Height) >= (inner.Y + inner.Height);
		}

		/// <summary>
		/// Returns a value that indicates whether the two rectangles intersect.
		/// </summary>
		public static bool IsIntersecting(in RectangleD a, in RectangleD b)
		{
			return (test(a.X, b.X, b.X + b.Width) || test(b.X, a.X, a.X + a.Width)) && (test(a.Y, b.Y, b.Y + b.Height) || test(b.Y, a.Y, a.Y + a.Height));
		}

		/// <summary>
		/// Returns the intersection rectangle of two rectangles.
		/// </summary>
		public static void Intersect(in RectangleD a, in RectangleD b, out RectangleD result)
		{
			double x1 = Utility.Max(a.X, b.X);
			double x2 = Utility.Min(a.X + a.Width, b.X + b.Width);
			double y1 = Utility.Max(a.Y, b.Y);
			double y2 = Utility.Min(a.Y + a.Height, b.Y + b.Height);

			if (x2 < x1 || y2 < y1)
			{
				result = default;
				return;
			}

			result.X = x1;
			result.Y = y1;
			result.Width = x2 - x1;
			result.Height = y2 - y1;
		}

		/// <summary>
		/// Returns the intersection rectangle of two rectangles.
		/// </summary>
		public static RectangleD Intersect(in RectangleD a, in RectangleD b)
		{
			RectangleD result;
			result.X = Utility.Max(a.X, b.X);
			result.Width = Utility.Min(a.X + a.Width, b.X + b.Width) - result.X;
			result.Y = Utility.Max(a.Y, b.Y);
			result.Height = Utility.Min(a.Y + a.Height, b.Y + b.Height) - result.Y;

			if (result.Width < 0.0 || result.Height < 0.0)
			{
				return default;
			}

			return result;
		}

		/// <summary>
		/// Returns the union of the two specified rectangles.
		/// </summary>
		public static void Union(in RectangleD a, in RectangleD b, out RectangleD result)
		{
			double x1 = Utility.Min(a.X, b.X);
			double x2 = Utility.Max(a.X + a.Width, b.X + b.Width);
			double y1 = Utility.Min(a.Y, b.Y);
			double y2 = Utility.Max(a.Y + a.Height, b.Y + b.Height);
			result.X = x1;
			result.Y = y1;
			result.Width = x2 - x1;
			result.Height = y2 - y1;
		}

		/// <summary>
		/// Returns the union of the two specified rectangles.
		/// </summary>
		public static RectangleD Union(in RectangleD a, in RectangleD b)
		{
			RectangleD result;
			result.X = Utility.Min(a.X, b.X);
			result.Width = Utility.Max(a.X + a.Width, b.X + b.Width) - result.X;
			result.Y = Utility.Min(a.Y, b.Y);
			result.Height = Utility.Max(a.Y + a.Height, b.Y + b.Height) - result.Y;
			return result;
		}

		/// <summary>
		/// Returns the smallest rectangle that contains the specified rectangle and the specified point.
		/// </summary>
		public static void Union(in RectangleD rect, in Vector2D p, out RectangleD result)
		{
			Vector2D min;
			min.X = rect.X;
			min.Y = rect.Y;
			
			Vector2D max;
			max.X = rect.X + rect.Width;
			max.Y = rect.Y + rect.Height;
			
			Vector2D.Min(min, p, out min);
			Vector2D.Max(max, p, out max);
			
			result.X = min.X;
			result.Y = min.Y;
			result.Width = max.X - min.X;
			result.Height = max.Y - min.Y;
		}
		
		/// <summary>
		/// Returns the smallest rectangle that contains the specified rectangle and the specified point.
		/// </summary>
		public static RectangleD Union(in RectangleD rect, in Vector2D p)
		{
			Vector2D min;
			min.X = rect.X;
			min.Y = rect.Y;
			
			Vector2D max;
			max.X = rect.X + rect.Width;
			max.Y = rect.Y + rect.Height;
			
			Vector2D.Min(min, p, out min);
			Vector2D.Max(max, p, out max);
			
			RectangleD result;
			result.X = min.X;
			result.Y = min.Y;
			result.Width = max.X - min.X;
			result.Height = max.Y - min.Y;
			return result;
		}

		/// <summary>
		/// Inflates the rectangle. This subtracts the offset from the position and adds the doubled offset to the width and height.
		/// </summary>
		public static void Inflate(in RectangleD rect, double dx, double dy, out RectangleD result)
		{
			result.X = rect.X - dx;
			result.Y = rect.Y - dy;
			result.Width = rect.Width + 2 * dx;
			result.Height = rect.Height + 2 * dy;
		}

		/// <summary>
		/// Inflates the rectangle. This subtracts the offset from the position and adds the doubled offset to the width and height.
		/// </summary>
		public static RectangleD Inflate(in RectangleD rect, double dx, double dy)
		{
			RectangleD result;
			result.X = rect.X - dx;
			result.Y = rect.Y - dy;
			result.Width = rect.Width + 2 * dx;
			result.Height = rect.Height + 2 * dy;
			return result;
		}
		
		/// <summary>
		/// Returns a value that indicates whether this rectangle and the specified rectangle are equal.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		public bool Equals(in RectangleD other)
		{
			return this.X == other.X && this.Y == other.Y && this.Width == other.Width && this.Height == other.Height;
		}

		bool IEquatable<RectangleD>.Equals(RectangleD other) => this.Equals(other);

		/// <summary>
		/// Returns a value that indicates whether this rectangle and the specified object.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(object obj)
		{
			if (!(obj is RectangleD))
				return false;

			RectangleD other = (RectangleD)obj;
			return this.X == other.X && this.Y == other.Y && this.Width == other.Width && this.Height == other.Height;
		}

		/// <summary>
		/// Returns a value that indicates whether the two rectangles are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool Equals(in RectangleD left, in RectangleD right)
		{
			return left.X == right.X && left.Y == right.Y && left.Width == right.Width && left.Height == right.Height;
		}

		public override int GetHashCode()
		{
			int hashCode = this.X.GetHashCode();
			hashCode = (hashCode * 397) ^ this.Y.GetHashCode();
			hashCode = (hashCode * 397) ^ this.Width.GetHashCode();
			hashCode = (hashCode * 397) ^ this.Height.GetHashCode();
			return hashCode;
		}

		public override string ToString()
		{
			return $"{this.Position} {this.Size}";
		}

		/// <summary>
		/// Returns a value that indicates whether the two rectangles are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool operator ==(in RectangleD left, in RectangleD right)
		{
			return left.X == right.X && left.Y == right.Y && left.Width == right.Width && left.Height == right.Height;
		}
		
		/// <summary>
		/// Returns a value that indicates whether the two rectangles are equal.
		/// </summary>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		public static bool operator !=(in RectangleD left, in RectangleD right)
		{
			return left.X != right.X || left.Y != right.Y || left.Width != right.Width || left.Height != right.Height;
		}

		public static implicit operator RectangleD(in RectangleI rect)
		{
			RectangleD result;
			result.X = rect.X;
			result.Y = rect.Y;
			result.Width = rect.Width;
			result.Height = rect.Height;
			return result;
		}

		public static implicit operator RectangleD(in Rectangle rect)
		{
			RectangleD result;
			result.X = rect.X;
			result.Y = rect.Y;
			result.Width = rect.Width;
			result.Height = rect.Height;
			return result;
		}
		
		#endregion
	}
}
