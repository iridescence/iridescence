﻿using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	/// <summary>
	/// Common math variables, constants and methods.
	/// </summary>
	public static partial class Utility
	{
		#region Methods

		/// <summary>
		/// Returns whether the specified number is finite (i.e. not infinity or NaN).
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsFinite(this float x)
		{
			return !(float.IsInfinity(x) || float.IsNaN(x));
		}
		
		/// <summary>
		/// Returns whether the specified number is finite (i.e. not infinity or NaN).
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsFinite(this double x)
		{
			return !(double.IsInfinity(x) || double.IsNaN(x));
		}

		/// <summary>
		/// Adds two values.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Add(double x, double y)
		{
			return x + y;
		}

		/// <summary>
		/// Subtracts two values.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Subtract(double x, double y)
		{
			return x - y;
		}

		/// <summary>
		/// Multiples two values.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Multiply(double x, double y)
		{
			return x * y;
		}

		/// <summary>
		/// Divides two values.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Divide(double x, double y)
		{
			return x / y;
		}

		/// <summary>
		/// Adds two values.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Add(int x, int y)
		{
			return unchecked(x + y);
		}

		/// <summary>
		/// Subtracts two values.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Subtract(int x, int y)
		{
			return unchecked(x - y);
		}

		/// <summary>
		/// Multiples two values.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Multiply(int x, int y)
		{
			return unchecked(x * y);
		}

		/// <summary>
		/// Divides two values.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Divide(int x, int y)
		{
			return unchecked(x / y);
		}

		/// <summary>
		/// Divides two values and returns the remainder.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Mod(float x, float y)
		{
			return x % y;
		}

		/// <summary>
		/// Divides two values and returns the remainder.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Mod(double x, double y)
		{
			return x % y;
		}
		
		/// <summary>
		/// Divides two values and returns the canonical modulus.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double CanonicalMod(double x, double y)
		{
			return (x % y + y) % y;
		}

		/// <summary>
		/// Divides two values and returns the canonical modulus.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float CanonicalMod(float x, float y)
		{
			return (x % y + y) % y;
		}

		/// <summary>
		/// Divides two values and returns the canonical modulus.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int CanonicalMod(int x, int y)
		{
			return (x % y + y) % y;
		}

		/// <summary>
		/// Converts the given angle in radians to degrees.
		/// </summary>
		/// <param name="radians">The angle in radians.</param>
		/// <returns>The angle in degrees.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float RadiansToDegrees(float radians)
		{
			return radians * Constants.DegreesPerRadian;
		}

		/// <summary>
		/// Converts the given angle in radians to degrees.
		/// </summary>
		/// <param name="radians">The angle in radians.</param>
		/// <returns>The angle in degrees.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double RadiansToDegrees(double radians)
		{
			return radians * ConstantsD.DegreesPerRadian;
		}

		/// <summary>
		/// Converts the given angle in degrees in radians.
		/// </summary>
		/// <param name="degrees">The angle in degrees.</param>
		/// <returns>The angle in radians.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float DegreesToRadians(float degrees)
		{
			return degrees * Constants.RadiansPerDegree;
		}

		/// <summary>
		/// Converts the given angle in degrees in radians.
		/// </summary>
		/// <param name="degrees">The angle in degrees.</param>
		/// <returns>The angle in radians.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double DegreesToRadians(double degrees)
		{
			return degrees * ConstantsD.RadiansPerDegree;
		}

		/// <summary>
		/// Returns -x.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The negative value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Negate(float x)
		{
			return -x;
		}

		/// <summary>
		/// Returns -x.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The negative value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Negate(double x)
		{
			return -x;
		}

		/// <summary>
		/// Returns -x.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The negative value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Negate(int x)
		{
			return -x;
		}
		
		/// <summary>
		/// Returns the logarithm of a specified number in a specified base.
		/// </summary>
		/// <param name="x">The number whose logarithm is to be found.</param>
		/// <param name="y">The base of the logarithm.</param>
		/// <returns>The logarithm.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Log(float x, float y)
		{
			return (float)System.Math.Log(x, y);
		}

		/// <summary>
		/// Returns the logarithm of a specified number in a specified base.
		/// </summary>
		/// <param name="a">The number whose logarithm to be found.</param>
		/// <param name="b">The base of the logarithm.</param>
		/// <returns>The logarithm.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Log(double x, double b)
		{
			return System.Math.Log(x, b);
		}


		/// <summary>
		/// Returns the natural logarithm of a specified number in a specified base.
		/// </summary>
		/// <param name="x">The number whose logarithm is to be found.</param>
		/// <returns>The logarithm.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Ln(float x)
		{
			return (float)System.Math.Log(x, Constants.E);
		}

		/// <summary>
		/// Returns the natural logarithm of a specified number in a specified base.
		/// </summary>
		/// <param name="a">The number whose logarithm is to be found.</param>
		/// <returns>The logarithm.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Ln(double x)
		{
			return System.Math.Log(x, ConstantsD.E);
		}

		/// <summary>
		/// Swaps the two values.
		/// </summary>
		/// <param name="first">The first value.</param>
		/// <param name="second">The second value.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Swap<T>(ref T first, ref T second)
		{
			T temp = first;
			first = second;
			second = temp;
		}

		/// <summary>
		/// Computes the square root of the given value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The square root.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Sqrt(float x)
		{
			return (float)System.Math.Sqrt(x);
		}

		/// <summary>
		/// Computes the square root of the given value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The square root.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Sqrt(double x)
		{
			return System.Math.Sqrt(x);
		}

		/// <summary>
		/// Computes the reciprocal of the square root of the given value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The square root.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float InverseSqrt(float x)
		{
			return (float)(1.0 / System.Math.Sqrt(x));
		}

		/// <summary>
		/// Computes the square root of the given value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The square root.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double InverseSqrt(double x)
		{
			return 1.0 / System.Math.Sqrt(x);
		}
		
		/// <summary>
		/// Computes the cube root of the given value.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The cube root.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float CubeRoot(float x)
		{
			if (x < 0)
				return -(float)System.Math.Pow(-x, 1.0 / 3.0);
			return (float)System.Math.Pow(x, 1.0 / 3.0);
		}

		/// <summary>
		/// Computes the cube root of the given value. Only the real roots are returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The cube root.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double CubeRoot(double x)
		{
			if (x < 0)
				return -System.Math.Pow(-x, 1.0 / 3.0);
			return System.Math.Pow(x, 1.0 / 3.0);
		}
		
		/// <summary>
		/// Returns e raised to the specified power.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>e raised to the specified power.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Exp(float x)
		{
			return (float)System.Math.Exp(x);
		}

		/// <summary>
		/// Returns e raised to the specified power
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>e raised to the specified power.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Exp(double x)
		{
			return System.Math.Exp(x);
		}

		/// <summary>
		/// Computes the triangle function.
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Triangle(float x)
		{
			x /= (Constants.Pi * 0.5f);
			x -= Utility.Floor(x / 4.0f) * 4.0f;

			if (x <= 1.0f)
				return x;
			if (x <= 3.0f)
				return 2.0f - x;
			return -4.0f + x;
		}

		/// <summary>
		/// Computes the triangle function.
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Triangle(double x)
		{
			x /= ConstantsD.PiOver2;
			x -= Utility.Floor(x / 4.0d) * 4.0d;

			if (x <= 1.0d)
				return x;
			if (x <= 3.0d)
				return 2.0d - x;
			return -4.0d + x;
		}

		/// <summary>
		/// Computes the square function.
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Square(float x)
		{
			if (Utility.Sin(x) > 0.0f)
				return 1.0f;
			return -1.0f;
		}

		/// <summary>
		/// Computes the square function.
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Square(double x)
		{
			if (Utility.Sin(x) > 0.0d)
				return 1.0d;
			return -1.0d;
		}

		/// <summary>
		/// Computes the sawtooth function.
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Sawtooth(float x)
		{
			return -1.0f + (x / (2.0f * Constants.Pi) - Utility.Floor(x / (2.0f * Constants.Pi))) * 2.0f;
		}

		/// <summary>
		/// Computes the sawtooth function.
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Sawtooth(double x)
		{
			return -1.0d + (x / ConstantsD.Pi2 - Utility.Floor(x / ConstantsD.Pi2)) * 2.0d;
		}


		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Floor(float x)
		{
			return (float)System.Math.Floor(x);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Floor(double x)
		{
			return System.Math.Floor(x);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Ceiling(float x)
		{
			return (float)System.Math.Ceiling(x);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Ceiling(double x)
		{
			return System.Math.Ceiling(x);
		}

		/// <summary>
		/// Gets the nearest power of two for the specified value that is greater or equal to the value.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static uint GetNearestPowerOfTwo(uint value)
		{
			value -= 1;
			value |= value >> 16;
			value |= value >> 8;
			value |= value >> 4;
			value |= value >> 2;
			value |= value >> 1;
			value += 1;
			return value;
		}

		/// <summary>
		///	Calculate a face normal, including the w component which is the offset from the origin.
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <param name="v3"></param>
		/// <returns></returns>
		public static Vector4 CalculateFaceNormal(Vector3 v1, Vector3 v2, Vector3 v3)
		{
			var normal = Utility.CalculateBasicFaceNormal(v1, v2, v3);

			// Now set up the w (distance of tri from origin
			return new Vector4(normal.X, normal.Y, normal.Z, -(normal.Dot(v1)));
		}

		/// <summary>
		///	Calculate a face normal, no w-information.
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <param name="v3"></param>
		/// <returns></returns>
		public static Vector3 CalculateBasicFaceNormal(Vector3 v1, Vector3 v2, Vector3 v3)
		{
			return (v2 - v1).Cross(v3 - v1).Normalize();
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Pow2(int power)
		{
			return ((power >= 0) && (power <= 30)) ? (1 << power) : 0;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsPowerOf2(int x)
		{
			return (x > 0) && ((x & (x - 1)) == 0);
		}

		public static int Log2(int x)
		{
			if (x <= 65536)
			{
				if (x <= 256)
				{
					if (x <= 16)
					{
						if (x <= 4)
						{
							if (x <= 2)
							{
								if (x <= 1)
									return 0;
								return 1;
							}
							return 2;
						}
						if (x <= 8)
							return 3;
						return 4;
					}
					if (x <= 64)
					{
						if (x <= 32)
							return 5;
						return 6;
					}
					if (x <= 128)
						return 7;
					return 8;
				}
				if (x <= 4096)
				{
					if (x <= 1024)
					{
						if (x <= 512)
							return 9;
						return 10;
					}
					if (x <= 2048)
						return 11;
					return 12;
				}
				if (x <= 16384)
				{
					if (x <= 8192)
						return 13;
					return 14;
				}
				if (x <= 32768)
					return 15;
				return 16;
			}

			if (x <= 16777216)
			{
				if (x <= 1048576)
				{
					if (x <= 262144)
					{
						if (x <= 131072)
							return 17;
						return 18;
					}
					if (x <= 524288)
						return 19;
					return 20;
				}
				if (x <= 4194304)
				{
					if (x <= 2097152)
						return 21;
					return 22;
				}
				if (x <= 8388608)
					return 23;
				return 24;
			}
			if (x <= 268435456)
			{
				if (x <= 67108864)
				{
					if (x <= 33554432)
						return 25;
					return 26;
				}
				if (x <= 134217728)
					return 27;
				return 28;
			}
			if (x <= 1073741824)
			{
				if (x <= 536870912)
					return 29;
				return 30;
			}
			return 31;
		}

		/// <summary>
		/// Wraps an angle between negative Pi and positive Pi.
		/// </summary>
		/// <param name="w"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double WrapAngle(double w)
		{
			return w + (2.0f * Constants.Pi) * Floor((ConstantsD.Pi - w) / (2.0f * Constants.Pi));
		}

		/// <summary>
		/// Wraps an angle between negative Pi and positive Pi.
		/// </summary>
		/// <param name="w"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float WrapAngle(float w)
		{
			return w + (2.0f * Constants.Pi) * Floor((Constants.Pi - w) / (2.0f * Constants.Pi));
		}

		/// <summary>
		/// Given two angles in radians (a and b), returns the shortest angle between them so that
		/// WrapAngle(b) = WrapAngle(a + ShortestAngle(a, b)).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static double ShortestAngle(double a, double b)
		{
			double diff = WrapAngle(a) - WrapAngle(b);
			if (diff > ConstantsD.Pi)
				diff = -(2.0 * ConstantsD.Pi) + diff;
			else if (diff < -ConstantsD.Pi)
				diff = (2.0 * ConstantsD.Pi) + diff;
			return diff;
		}

		/// <summary>
		/// Given two angles in radians (a and b), returns the shortest angle between them so that
		/// WrapAngle(b) = WrapAngle(a + ShortestAngle(a, b)).
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static float ShortestAngle(float a, float b)
		{
			float diff = WrapAngle(b) - WrapAngle(a);
			if (diff > Constants.Pi)
				diff = -(2.0f * Constants.Pi) + diff;
			else if (diff < -Constants.Pi)
				diff = (2.0f * Constants.Pi) + diff;
			return diff;
		}

		/// <summary>
		/// Returns whether the specified angle is in the angle range defined by the specified start and end angle.
		/// </summary>
		/// <param name="angle"></param>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns></returns>
		public static bool AngleInRange(float angle, float start, float end)
		{
			if (end < start)
			{
				float t = end;
				end = start;
				start = t;
			}

			float delta = end - start;
			if (delta >= Constants.Pi2)
				return true;

			angle = WrapAngle(angle);
			start = WrapAngle(start);
			end = start + delta;

			if (angle >= start && angle <= end)
				return true;
			
			if (angle <= angle - Constants.Pi2)
				return true;

			return false;
		}

		/// <summary>
		/// Returns whether the specified angle is in the angle range defined by the specified start and end angle.
		/// </summary>
		/// <param name="angle"></param>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns></returns>
		public static bool AngleInRange(double angle, double start, double end)
		{
			if (end < start)
			{
				double t = end;
				end = start;
				start = t;
			}

			double delta = end - start;
			if (delta >= ConstantsD.Pi2)
				return true;

			angle = WrapAngle(angle);
			start = WrapAngle(start);
			end = start + delta;

			if (angle >= start && angle <= end)
				return true;
			
			if (angle <= angle - ConstantsD.Pi2)
				return true;

			return false;
		}
		
		/// <summary>
		/// Increments a float by the smallest amount possible.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static unsafe float Increment(float value)
		{
			int result = *(int*)&value;
			if (value > 0.0f)
				result++;
			else if (value < 0.0f)
				result--;
			else if (value == 0.0f)
				return float.Epsilon;
			return *(float*)&result;
		}

		/// <summary>
		/// Decrements a float by the smallest amount possible.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static unsafe float Decrement(float value)
		{
			int result = *(int*)&value;
			if (value > 0.0f)
				result--;
			else if (value < 0.0f)
				result++;
			else if (value == 0.0f)
				return -float.Epsilon; // thanks to Sebastian Negraszus
			return *(float*)&result;
		}


		/// <summary>
		/// Repeatedly increments or decrements a float by the smallest amount possible.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static unsafe float Offset(float value, int n)
		{
			if (n > 0)
			{
				while (n-- > 0)
				{
					int result = *(int*)&value;
					if (value > 0.0f)
						result++;
					else if (value < 0.0f)
						result--;
					else if (value == 0.0f)
						return float.Epsilon;
					value = *(float*)&result;
				}
			}
			else if (n < 0)
			{
				while (n++ < 0)
				{
					int result = *(int*)&value;
					if (value > 0.0f)
						result--;
					else if (value < 0.0f)
						result++;
					else if (value == 0.0f)
						return -float.Epsilon;
					value = *(float*)&result;
				}
			}

			return value;
		}

		/// <summary>
		/// Combines two hashes.
		/// </summary>
		/// <param name="h1"></param>
		/// <param name="h2"></param>
		/// <returns></returns>
		public static int CombineHash(int h1, int h2)
		{
			unchecked
			{
				// RyuJIT optimizes this to use the ROL instruction
				// Related GitHub pull request: dotnet/coreclr#1830
				uint rol5 = ((uint)h1 << 5) | ((uint)h1 >> 27);
				return ((int)rol5 + h1) ^ h2;
			}
		}

		/// <summary>
		/// Returns the linear interpolation of two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static byte Lerp(byte value1, byte value2, float amount)
		{
			float amount2 = 1.0f - amount;
			return (byte)(value1 * amount2 + value2 * amount);
		}

		/// <summary>
		/// Returns the linear interpolation of two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Lerp(float value1, float value2, float amount)
		{
			float amount2 = 1.0f - amount;
			return value1 * amount2 + value2 * amount;
		}

		/// <summary>
		/// Returns the linear interpolation of two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Lerp(double value1, double value2, double amount)
		{
			double amount2 = 1.0 - amount;
			return value1 * amount2 + value2 * amount;
		}

		#endregion
	}
}
