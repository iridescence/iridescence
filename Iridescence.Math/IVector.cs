﻿namespace Iridescence.Math
{
	/// <summary>
	/// Abstract, generic vector interface.
	/// </summary>
	public interface IVector<T>
	{
		#region Properties

		/// <summary>
		/// Gets the number of elements in the vector.
		/// </summary>
		int Count { get; }

		/// <summary>
		/// Gets or sets the value of the element at the specified index.
		/// The index is zero-based, meaning 0 is the first element.
		/// </summary>
		/// <param name="index">The index of the element.</param>
		/// <returns>The value at the specified position in the vector.</returns>	
		T this[int index] { get; set; }

		#endregion
	}
}
