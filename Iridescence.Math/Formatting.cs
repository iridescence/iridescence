﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Iridescence.Math
{
	public delegate bool TryParseFunction<T>(string s, out T result);

	/// <summary>
	/// Formatting options for math classes.
	/// </summary>
	public static class Formatting
	{
		#region Fields

		private static readonly char[] separators = {'\r', '\n', '\t', ' ', ',', ';', '|'};
		private static readonly char[,] groups =
		{
			{'(', ')'},
			{'[', ']'},
			{'{', '}'},
			{'<', '>'}
		};

		public const NumberStyles DefaultIntegerStyles = NumberStyles.Integer;
		public const NumberStyles DefaultFloatStyles = NumberStyles.AllowThousands | NumberStyles.Float;

		#endregion

		#region Properties

		
		#endregion

		#region Methods

		public static string ToString<T>(IEnumerable<T> values, string formatString, IFormatProvider provider)
			where T : IFormattable
		{
			if (formatString != null)
			{
				object[] args = values.Cast<object>().ToArray();
				return string.Format(formatString, args);
			}

			StringBuilder s = new StringBuilder();

			bool first = true;

			foreach (T value in values)
			{
				if (first)
					first = false;
				else
					s.Append(" ");

				s.Append(value.ToString(null, provider));
			}

			return s.ToString();
		}

		private static bool tryRemoveGroups(string input, ref int index, int limit, int endChar, StringBuilder output)
		{
			int numGroups = groups.GetLength(0);

			while(index < limit)
			{
				bool groupFound = false;

				char c = input[index];

				if (c == endChar)
				{
					// found the end char.
					++index;
					return true; 
				}

				for (int j = 0; j < numGroups; ++j)
				{
					if (c == groups[j, 0])
					{
						// start of a new group.
						groupFound = true;
						++index;

						// recursively remove groups from that substring.
						if (!tryRemoveGroups(input, ref index, limit, groups[j, 1], output))
							return false;
					}
					else if (c == groups[j, 1])
					{
						// end of a different group.
						return false; 
					}
				}

				if (!groupFound)
				{
					// normal character.
					output.Append(c);
					++index;
				}

			}

			return endChar == -1;
		}

		private static bool tryRemoveGroups(string input, out string output)
		{
			StringBuilder outputBuilder = new StringBuilder(input.Length);
			int offset = 0;
			if (!tryRemoveGroups(input, ref offset, input.Length, -1, outputBuilder))
			{
				output = null;
				return false;
			}

			output = outputBuilder.ToString();
			return true;
		}

		public static bool TryParse<T>(string str, TryParseFunction<T> tryParse, Span<T> values)
		{
			if (!tryRemoveGroups(str, out str))
				return false;

			string[] split = str.Split(separators, StringSplitOptions.RemoveEmptyEntries);

			if (split.Length != values.Length)
				return false;

			for (int i = 0; i < split.Length; ++i)
			{
				if (!tryParse(split[i], out values[i]))
					return false;
			}

			return true;
		}

		#endregion
	}
}
