﻿namespace Iridescence.Math
{
	/// <summary>
	/// Interface for generic 3-element vectors.
	/// </summary>
	public interface IVector3<T> : IVector2<T>
	{
		/// <summary>
		/// Gets or sets the third component of the vector.
		/// </summary>
		T Z { get; set; }
	}
}
