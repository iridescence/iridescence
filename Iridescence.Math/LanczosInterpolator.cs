﻿using System;

namespace Iridescence.Math
{
	/// <summary>
	/// Implements lanczos interpolation.
	/// </summary>
	public sealed class LanczosInterpolator
	{
		#region Fields

		private readonly int width;
		private readonly int resolution;
		private readonly double[] lanczosTable;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the width (half the kernel size) of the interpolator.
		/// </summary>
		public int Width => this.width;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="LanczosInterpolator"/> class. 
		/// </summary>
		/// <param name="width">The width of one "wing" of the interpolator in samples, i.e. half the kernel size.</param>
		/// <param name="resolution">The resolution of the internal lookup table.</param>
		public LanczosInterpolator(int width = 8, int resolution = 256)
		{
			if (width < 1)
				throw new ArgumentException("Width must be at least 1.", nameof(width));

			if (resolution < 1)
				throw new ArgumentException("Resolution must be at least 1");

			this.width = width;
			this.resolution = resolution;

			// Generate lookup table for "f(x) = sinc(x) * sinc(x / width)" with the specified resolution.
			this.lanczosTable = new double[(width + 1) * resolution + 1];
			this.lanczosTable[0] = 1.0f;

			for (int i = 1; i <= width * resolution; ++i)
			{
				double x = i / (double)resolution;
				this.lanczosTable[i] = Utility.Sinc(x) * Utility.Sinc(x / this.width);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Interpolates between the specified samples. Note that the sample array should include at least n samples before and after the specified position (where n is the width specified in the constructor).
		/// </summary>
		/// <param name="samples">The sample buffer.</param>
		/// <param name="position">The floating-point position of the sample to return.</param>
		/// <returns>The interpolated sample at the specified position.</returns>
		public double Interpolate(ReadOnlySpan<double> samples, double position)
		{
			int kernelSize = this.width;
			int mid = (int)position;

			double tablePos = (mid - kernelSize) * this.resolution - position * this.resolution;
			int tableIndex = (int)Utility.Floor(tablePos);
			double t = tablePos - tableIndex;

			double sum = 0.0;
			for (int kernelPos = -kernelSize; kernelPos <= kernelSize; ++kernelPos)
			{
				// Get sample.
				int sampleIndex = mid + kernelPos;
				double sample = sampleIndex >= 0 && sampleIndex < samples.Length ? samples[sampleIndex] : 0.0;

				// Approximate function from lookup table.
				int t0 = Utility.Absolute(tableIndex);
				int t1 = Utility.Absolute(tableIndex + 1);
				double lanczos = this.lanczosTable[t0] * (1.0 - t) + this.lanczosTable[t1] * t;

				// Sum.
				sum += sample * lanczos;

				// Advance lookup table index by one position.
				tableIndex += this.resolution;
			}

			return sum;
		}

		#endregion
	}

}
