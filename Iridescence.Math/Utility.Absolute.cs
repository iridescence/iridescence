﻿using System;
using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	public static partial class Utility
	{
		/// <summary>
		/// Returns the absolute of the value, i.e. its distance to zero.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The absolute value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static sbyte Absolute(sbyte x)
		{
			if (x < 0)
			{
				x = (sbyte)-x;
				if (x < 0)
				{
					throw new OverflowException();
				}
			}

			return x;
		}

		/// <summary>
		/// Returns the absolute of the value, i.e. its distance to zero.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The absolute value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static short Absolute(short x)
		{
			if (x < 0)
			{
				x = (short)-x;
				if (x < 0)
				{
					throw new OverflowException();
				}
			}

			return x;
		}

		/// <summary>
		/// Returns the absolute of the value, i.e. its distance to zero.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The absolute value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Absolute(int x)
		{
			if (x < 0)
			{
				x = -x;
				if (x < 0)
				{
					throw new OverflowException();
				}
			}

			return x;
		}

		/// <summary>
		/// Returns the absolute of the value, i.e. its distance to zero.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The absolute value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long Absolute(long x)
		{
			if (x < 0)
			{
				x = -x;
				if (x < 0)
				{
					throw new OverflowException();
				}
			}

			return x;
		}

		/// <summary>
		/// Returns the absolute of the value, i.e. its distance to zero.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The absolute value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Absolute(float x)
		{
			return System.Math.Abs(x);
		}

		/// <summary>
		/// Returns the absolute of the value, i.e. its distance to zero.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The absolute value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Absolute(double x)
		{
			return System.Math.Abs(x);
		}

		/// <summary>
		/// Returns the absolute of the value, i.e. its distance to zero.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The absolute value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static decimal Absolute(decimal x)
		{
			return System.Math.Abs(x);
		}
	}
}
