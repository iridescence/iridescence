﻿using System;
using System.Linq.Expressions;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a Bézier surface.
	/// </summary>
	public class BezierPatch<T>
	{
		#region Fields

		private readonly int width;
		private readonly int height;
		private readonly T[,] controlPoints;

		private readonly Func<T, T, T> add;
		private readonly Func<T, float, T> mult;
		
		#endregion

		#region Properties

		public int Width => this.width;

		public int Height => this.height;

		public T this[int x, int y]
		{
			get => this.controlPoints[x, y];
			set => this.controlPoints[x, y] = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BezierPatch.
		/// </summary>
		public BezierPatch(int width, int height)
		{
			this.width = width;
			this.height = height;
			this.controlPoints = new T[width, height];

			// generic operators

			ParameterExpression paramA = Expression.Parameter(typeof(T));
			ParameterExpression paramB = Expression.Parameter(typeof(T));
			BinaryExpression op = Expression.Add(paramA, paramB);
			this.add = Expression.Lambda<Func<T, T, T>>(op, paramA, paramB).Compile();

			paramA = Expression.Parameter(typeof(T));
			paramB = Expression.Parameter(typeof(float));
			op = Expression.Multiply(paramA, paramB);
			this.mult = Expression.Lambda<Func<T, float, T>>(op, paramA, paramB).Compile();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Tesselates the patch using the specified resolution.
		/// </summary>
		/// <param name="resX">X resolution.</param>
		/// <param name="resY">Y resolution.</param>
		/// <returns></returns>
		public T[,] Tesselate(int resX, int resY)
		{
			T[,] output = new T[resX,resY];

			for (int x = 0; x < resX; x++)
			{
				Vector2 mu;
				mu.X = (float)x / (float)(resX - 1);
				for (int y = 0; y < resY; y++)
				{
					mu.Y = (float)y / (float)(resY - 1);

					for (int kx = 0; kx < this.width; kx++)
					{
						Vector2 b;
						b.X = this.blend(kx, mu.X, this.width - 1);
						for (int ky = 0; ky < this.height; ky++)
						{
							b.Y = this.blend(ky, mu.Y, this.height - 1);
							output[x, y] = this.add(output[x, y], this.mult(this.controlPoints[kx, ky], b.X * b.Y));
						}
					}
				}
			}

			return output;
		}

		private float blend(int k, float mu, int n)
		{
			int nn = n;
			int kn = k;
			int nkn = n - k;
			float blend = 1.0f;

			while (nn >= 1)
			{
				blend *= nn;
				nn--;
				if (kn > 1)
				{
					blend /= (float)kn;
					kn--;
				}
				if (nkn > 1)
				{
					blend /= (float)nkn;
					nkn--;
				}
			}

			if (k > 0)
				blend *= Utility.Pow((float)mu, (float)k);

			if (n - k > 0)
				blend *= Utility.Pow((float)(1 - mu), (float)(n - k));

			return blend;
		}

		#endregion
	}
}
