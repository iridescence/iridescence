﻿namespace Iridescence.Math
{
	/// <summary>
	/// Interface for generic 2-element vectors.
	/// </summary>
	public interface IVector2<T>
	{
		/// <summary>
		/// Gets or sets the first component of the vector.
		/// </summary>
		T X { get; set; }

		/// <summary>
		/// Gets or sets the second component of the vector.
		/// </summary>
		T Y { get; set; }
	}
}
