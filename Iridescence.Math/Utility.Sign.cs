﻿using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	public static partial class Utility
	{
		#region Sign

		/// <summary>
		/// Returns the sign of the value. If the value is negative, -1 is returned; If the value is 0, 0 is returned; If the value is positive, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Sign(sbyte x)
		{
			return System.Math.Sign(x);
		}	
		
		/// <summary>
		/// Returns the sign of the value. If the value is negative, -1 is returned; If the value is 0, 0 is returned; If the value is positive, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Sign(short x)
		{
			return System.Math.Sign(x);
		}	
		/// <summary>
		/// Returns the sign of the value. If the value is negative, -1 is returned; If the value is 0, 0 is returned; If the value is positive, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Sign(int x)
		{
			return System.Math.Sign(x);
		}

		/// <summary>
		/// Returns the sign of the value. If the value is negative, -1 is returned; If the value is 0, 0 is returned; If the value is positive, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Sign(long x)
		{
			return System.Math.Sign(x);
		}

		/// <summary>
		/// Returns the sign of the value. If the value is negative, -1 is returned; If the value is 0, 0 is returned; If the value is positive, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Sign(float x)
		{
			return System.Math.Sign(x);
		}

		/// <summary>
		/// Returns the sign of the value. If the value is negative, -1 is returned; If the value is 0, 0 is returned; If the value is positive, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Sign(double x)
		{
			return System.Math.Sign(x);
		}

		/// <summary>
		/// Returns the sign of the value. If the value is negative, -1 is returned; If the value is 0, 0 is returned; If the value is positive, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Sign(decimal x)
		{
			return System.Math.Sign(x);
		}

		#endregion

		#region NonZeroSign

		/// <summary>
		/// Returns the non-zero sign of the value. If the value is negative, -1 is returned; Otherwise, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The non-zero sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int NonZeroSign(sbyte x)
		{
			return x < 0 ? -1 : 1;
		}	
		
		/// <summary>
		/// Returns the non-zero sign of the value. If the value is negative, -1 is returned; Otherwise, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The non-zero sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int NonZeroSign(short x)
		{
			return x < 0 ? -1 : 1;
		}	

		/// <summary>
		/// Returns the non-zero sign of the value. If the value is negative, -1 is returned; Otherwise, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The non-zero sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int NonZeroSign(int x)
		{
			return x < 0 ? -1 : 1;
		}	

		/// <summary>
		/// Returns the non-zero sign of the value. If the value is negative, -1 is returned; Otherwise, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The non-zero sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int NonZeroSign(long x)
		{
			return x < 0L ? -1 : 1;
		}	

		/// <summary>
		/// Returns the non-zero sign of the value. If the value is negative, -1 is returned; Otherwise, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The non-zero sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int NonZeroSign(float x)
		{
			return x < 0.0f ? -1 : 1;
		}	

		/// <summary>
		/// Returns the non-zero sign of the value. If the value is negative, -1 is returned; Otherwise, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The non-zero sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int NonZeroSign(double x)
		{
			return x < 0.0 ? -1 : 1;
		}	

		/// <summary>
		/// Returns the non-zero sign of the value. If the value is negative, -1 is returned; Otherwise, 1 is returned.
		/// </summary>
		/// <param name="x">The value.</param>
		/// <returns>The non-zero sign of the value.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int NonZeroSign(decimal x)
		{
			return x < 0.0m ? -1 : 1;
		}	

		#endregion

	}
}
