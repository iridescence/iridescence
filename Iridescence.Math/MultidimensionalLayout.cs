﻿namespace Iridescence.Math
{
	/// <summary>
	/// Defines the memory layout of a multi-dimensional array.
	/// </summary>
	public enum MultidimensionalLayout
	{
		/// <summary>
		/// Row-major order. The consecutive elements of a row reside next to each other.
		/// </summary>
		RowMajor,

		/// <summary>
		/// Column-major order. The consecutive elements of a column reside next to each other.
		/// </summary>
		ColumnMajor,
	}
}
