﻿using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	/// <summary>
	/// The module initializer for the library.
	/// </summary>
	internal static class ModuleInitializer
	{
		public static void Initialize()
		{
			RuntimeHelpers.RunClassConstructor(typeof(GenericConstants).TypeHandle);
		}
	}
}
