﻿namespace Iridescence.Math
{
	public static partial class Utility
	{
		// normal vector encoding/decoding
		// thanks to http://www.geometrictools.com for the idea (whose code did not work though).

		/// <summary>
		/// Encodes a normal with 3 floating point numbers to a 32-bit integer.
		/// Stores spherical coordinates in 29 bits. The remaining 3 bits are used for signs.
		/// </summary>
		/// <param name="x">The x component.</param>
		/// <param name="y">The y component.</param>
		/// <param name="z">The z component.</param>
		/// <param name="result">The encoded normal.</param>
		/// <remarks>
		/// The input is expected to be normalized.
		/// Some precision may be lost.
		/// </remarks>
		public static void EncodeNormal(double x, double y, double z, out uint result)
		{
			const double twoDivPi = 2.0d / ConstantsD.Pi;
			const double factor = 0x7FFE * twoDivPi;

			result = 0;

			if (x < 0.0f)
			{
				result |= (1u << 29); // x sign.
				x = -x;
			}

			if (y < 0.0f)
			{
				result |= (1u << 30); // y sign.
				y = -y;
			}

			if (z < 0.0f)
				result |= (1u << 31); // z sign.

			uint s = (uint)(factor * Asin(Sqrt(x * x + y * y)));
			uint a = (uint)(s * twoDivPi * Atan2(y, x));
			result |= a + s * (s + 1u) / 2u; // mantissa.
		}

		/// <summary>
		/// Encodes a normal with 3 floating point numbers to a 32-bit integer.
		/// Stores spherical coordinates in 29 bits. The remaining 3 bits are used for signs.
		/// </summary>
		/// <param name="x">The x component.</param>
		/// <param name="y">The y component.</param>
		/// <param name="z">The z component.</param>
		/// <param name="result">The encoded normal.</param>
		/// <remarks>
		/// The input is expected to be normalized.
		/// Some precision may be lost.
		/// </remarks>
		public static void EncodeNormal(float x, float y, float z, out uint result)
		{
			EncodeNormal((double)x, (double)y, (double)z, out result); // TODO
		}
		
		/// <summary>
		/// Decodes a previously encoded normal.
		/// </summary>
		/// <param name="value">The encoded normal.</param>
		/// <param name="x">The restored X component.</param>
		/// <param name="y">The restored Y component.</param>
		/// <param name="z">The restored Z component.</param>
		public static void DecodeNormal(uint value, out double x, out double y, out double z)
		{
			const double piDivTwo = ConstantsD.Pi / 2.0d;
			const double invFactor = ConstantsD.Pi / (2.0d * 0x7FFE);

			uint i = value & 0x1FFFFFFFu; // mantissa.
			uint s = (uint)(0.5d * (Sqrt(1.0d + 8.0d * i) - 1.0d));
			uint a = (s * (s + 1u) / 2u - i);

			if (s == 0x7FFE)
			{
				if (a == 0)
				{
					// unit x.
					x = 1.0d;
					y = 0.0d;
					z = 0.0d;
				}
				else
				{
					// unit y.
					x = 0.0d;
					y = 1.0d;
					z = 0.0d;
				}
			}
			else if (s == 0)
			{
				// unit z.
				x = 0.0d;
				y = 0.0d;
				z = 1.0d;
			}
			else
			{
				double sf = (double)s;
				double theta = a == 0u ? 0.0d : piDivTwo * (double)(a ^ ~1u) / sf;
				double phi = invFactor * sf;
				double sinPhi = Sin(phi);

				x = Cos(theta) * sinPhi;
				y = Sin(theta) * sinPhi;
				z = Cos(phi);
			}

			if ((value & (1u << 29)) != 0u) x = -x; // x sign.
			if ((value & (1u << 30)) != 0u) y = -y; // y sign.
			if ((value & (1u << 31)) != 0u) z = -z; // z sign.
		}

		/// <summary>
		/// Decodes a previously encoded normal.
		/// </summary>
		/// <param name="value">The encoded normal.</param>
		/// <param name="x">The restored X component.</param>
		/// <param name="y">The restored Y component.</param>
		/// <param name="z">The restored Z component.</param>
		public static void DecodeNormal(uint value, out float x, out float y, out float z)
		{
			const float piDivTwo = Constants.Pi / 2.0f;
			const float invFactor = Constants.Pi / (2.0f * 0x7FFE);

			uint i = value & 0x1FFFFFFFu; // mantissa.
			uint s = (uint)(0.5f * (Sqrt(1.0f + 8.0f * i) - 1.0f));
			uint a = (s * (s + 1u) / 2u - i);

			if (s == 0x7FFE)
			{
				if (a == 0)
				{
					// unit x.
					x = 1.0f;
					y = 0.0f;
					z = 0.0f;
				}
				else
				{
					// unit y.
					x = 0.0f;
					y = 1.0f;
					z = 0.0f;
				}
			}
			else if (s == 0)
			{
				// unit z.
				x = 0.0f;
				y = 0.0f;
				z = 1.0f;
			}
			else
			{
				float sf = (float)s;
				float theta = a == 0u ? 0.0f : piDivTwo * (float)(a ^ ~1u) / sf;
				float phi = invFactor * sf;
				float sinPhi = Sin(phi);

				x = Cos(theta) * sinPhi;
				y = Sin(theta) * sinPhi;
				z = Cos(phi);
			}

			if ((value & (1u << 29)) != 0u) x = -x; // x sign.
			if ((value & (1u << 30)) != 0u) y = -y; // y sign.
			if ((value & (1u << 31)) != 0u) z = -z; // z sign.
		}
	}
}
