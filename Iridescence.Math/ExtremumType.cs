﻿namespace Iridescence.Math
{
	/// <summary>
	/// Defines the types of extrema.
	/// </summary>
	public enum ExtremumType
	{
		Minimum,
		Maximum,
		Saddle,
	}
}
