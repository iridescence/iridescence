﻿using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	public static partial class Utility
	{
		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static sbyte Min(sbyte a, sbyte b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static sbyte Max(sbyte a, sbyte b)
		{
			return a > b ? a : b;
		}
		
		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static byte Min(byte a, byte b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static byte Max(byte a, byte b)
		{
			return a > b ? a : b;
		}

		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static short Min(short a, short b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static short Max(short a, short b)
		{
			return a > b ? a : b;
		}

		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ushort Min(ushort a, ushort b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ushort Max(ushort a, ushort b)
		{
			return a > b ? a : b;
		}

		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Min(int a, int b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Max(int a, int b)
		{
			return a > b ? a : b;
		}
		
		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static uint Min(uint a, uint b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static uint Max(uint a, uint b)
		{
			return a > b ? a : b;
		}

		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long Min(long a, long b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long Max(long a, long b)
		{
			return a > b ? a : b;
		}
		
		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ulong Min(ulong a, ulong b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ulong Max(ulong a, ulong b)
		{
			return a > b ? a : b;
		}

		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Min(float a, float b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Max(float a, float b)
		{
			return a > b ? a : b;
		}
		
		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Min(double a, double b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Max(double a, double b)
		{
			return a > b ? a : b;
		}
		
		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static char Min(char a, char b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static char Max(char a, char b)
		{
			return a > b ? a : b;
		}

		/// <summary>
		/// Returns the smaller of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static decimal Min(decimal a, decimal b)
		{
			return a < b ? a : b;
		}

		/// <summary>
		/// Returns the larger of the two values.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static decimal Max(decimal a, decimal b)
		{
			return a > b ? a : b;
		}
	}
}
