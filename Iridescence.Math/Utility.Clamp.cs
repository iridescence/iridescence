﻿using System;
using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	public static partial class Utility
	{
		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static sbyte Clamp(sbyte x, sbyte min, sbyte max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static byte Clamp(byte x, byte min, byte max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}
		
		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static short Clamp(short x, short min, short max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ushort Clamp(ushort x, ushort min, ushort max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}
		
		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Clamp(int x, int min, int max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static uint Clamp(uint x, uint min, uint max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long Clamp(long x, long min, long max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ulong Clamp(ulong x, ulong min, ulong max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Clamp(float x, float min, float max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Clamp(double x, double min, double max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static char Clamp(char x, char min, char max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}
		
		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static decimal Clamp(decimal x, decimal min, decimal max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static TimeSpan Clamp(TimeSpan x, TimeSpan min, TimeSpan max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}

		/// <summary>
		/// Clamps the specified value between a minimum and a maximum.
		/// </summary>
		/// <param name="x">The input value.</param>
		/// <param name="min">The minumum value.</param>
		/// <param name="max">The maximum value.</param>
		/// <returns>min if x is less than min; max if x is greater than max; x otherwise.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static DateTime Clamp(DateTime x, DateTime min, DateTime max)
		{
			if (x < min) return min;
			if (x > max) return max;
			return x;
		}
	}
}
