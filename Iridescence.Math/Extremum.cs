﻿using System;
using System.Collections.Generic;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents an extremum of a function.
	/// </summary>
	public struct Extremum<T> : IEquatable<Extremum<T>>
	{
		#region Properties

		/// <summary>
		/// Gets the value of the extremum.
		/// </summary>
		public T Value { get; }

		/// <summary>
		/// Gets the type of the extremum.
		/// </summary>
		public ExtremumType Type { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Extremum{T}"/> class. 
		/// </summary>
		public Extremum(T value, ExtremumType type)
		{
			this.Value = value;
			this.Type = type;
		}

		#endregion

		#region Methods

		public bool Equals(Extremum<T> other)
		{
			return EqualityComparer<T>.Default.Equals(this.Value, other.Value) && this.Type == other.Type;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is Extremum<T> && this.Equals((Extremum<T>)obj);
		}

		public override int GetHashCode()
		{
			return EqualityComparer<T>.Default.GetHashCode(this.Value) ^ (int)this.Type;
		}

		public static bool operator ==(Extremum<T> left, Extremum<T> right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Extremum<T> left, Extremum<T> right)
		{
			return !left.Equals(right);
		}
		
		#endregion
	}
}
