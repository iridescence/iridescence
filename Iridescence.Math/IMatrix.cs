﻿namespace Iridescence.Math
{
	/// <summary>
	/// Abstract, generic matrix interface.
	/// </summary>
	public interface IMatrix<T>
	{
		#region Properties

		/// <summary>
		/// Gets the number of rows in the matrix..
		/// </summary>
		int Rows { get; }

		/// <summary>
		/// Gets the number of columns in the matrix..
		/// </summary>
		int Columns { get; }

		/// <summary>
		/// Gets or sets the value of the entry at the specified row and column.
		/// The row and column index are zero-based, meaning [0, 0] is M11.
		/// </summary>
		/// <param name="row">The matrix row.</param>
		/// <param name="column">The matrix column.</param>
		/// <returns>The value of the entry at the specified position in the matrix.</returns>	
		T this[int row, int column] { get; set; }

		#endregion
	}
}
