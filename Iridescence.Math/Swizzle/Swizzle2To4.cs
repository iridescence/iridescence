﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Iridescence.Math.Swizzle
{
	/// <summary>
	/// Allows remapping of the components of an underlying vector of type <typeparamref name="TVector"/> (with component type <typeparamref name="T"/>) using generic type arguments.
	/// </summary>
	/// <typeparam name="T">The component type.</typeparam>
	/// <typeparam name="TVector">The vector type.</typeparam>
	/// <typeparam name="XIn">Specifies which property on the swizzle structure must be assigned so that the underyling 'X' property is assigned. Can be either <see cref="T:X"/>, <see cref="T:Y"/>, <see cref="T:Z"/>, <see cref="T:W"/>, or <see cref="T:None"/>.</typeparam>
	/// <typeparam name="YIn">Specifies which property on the swizzle structure must be assigned so that the underyling 'Y' property is assigned. Can be either <see cref="T:X"/>, <see cref="T:Y"/>, <see cref="T:Z"/>, <see cref="T:W"/>, or <see cref="T:None"/>.</typeparam>
	/// <typeparam name="XOut">Specifies which component of the underlying vector is returned when the 'X' property of the swizzle structure is retrieved. Can be either <see cref="T:X"/>, <see cref="T:Y"/>, <see cref="T:Zero"/>, or <see cref="T:One"/>.</typeparam>
	/// <typeparam name="YOut">Specifies which component of the underlying vector is returned when the 'Y' property of the swizzle structure is retrieved. Can be either <see cref="T:X"/>, <see cref="T:Y"/>, <see cref="T:Zero"/>, or <see cref="T:One"/>.</typeparam>
	/// <typeparam name="ZOut">Specifies which component of the underlying vector is returned when the 'Z' property of the swizzle structure is retrieved. Can be either <see cref="T:X"/>, <see cref="T:Y"/>, <see cref="T:Zero"/>, or <see cref="T:One"/>.</typeparam>
	/// <typeparam name="WOut">Specifies which component of the underlying vector is returned when the 'W' property of the swizzle structure is retrieved. Can be either <see cref="T:X"/>, <see cref="T:Y"/>, <see cref="T:Zero"/>, or <see cref="T:One"/>.</typeparam>
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public struct Swizzle2To4<T, TVector, XIn, YIn, XOut, YOut, ZOut, WOut> : IVector4<T>
		where TVector : struct, IVector2<T>
		where XIn : struct, IInputSwizzleParameter
		where YIn : struct, IInputSwizzleParameter
		where XOut : struct, IOutputSwizzleParameter2
		where YOut : struct, IOutputSwizzleParameter2
		where ZOut : struct, IOutputSwizzleParameter2
		where WOut : struct, IOutputSwizzleParameter2
	{
		#region Fields

		/// <summary>
		/// The actual underlying vector (without swizzling applied).
		/// </summary>
		public TVector Value;
		
		#endregion
		
		#region Properties
	
		/// <summary>
		/// Gets or sets the first swizzled component.
		/// This property returns the internal value as specified by <typeparamref name="XOut"/> and sets the internal values where the 'In' generic arguments are <see cref="T:X"/>.
		/// </summary>
		public T X
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if(typeof(XOut) == typeof(X)) return this.Value.X;
				if(typeof(XOut) == typeof(Y)) return this.Value.Y;
				if(typeof(XOut) == typeof(One)) return GenericConstants.One<T>.Value;
				if(typeof(XOut) == typeof(Zero)) return default;
				throw new InvalidOperationException();
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set
			{
				if(typeof(XIn) == typeof(X)) this.Value.X = value;
				if(typeof(YIn) == typeof(X)) this.Value.Y = value;
			}
		}

		/// <summary>
		/// Gets or sets the second swizzled component.
		/// This property returns the internal value as specified by <typeparamref name="YOut"/> and sets the internal values where the 'In' generic arguments are <see cref="T:Y"/>.
		/// </summary>
		public T Y
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if(typeof(YOut) == typeof(X)) return this.Value.X;
				if(typeof(YOut) == typeof(Y)) return this.Value.Y;
				if(typeof(YOut) == typeof(One)) return GenericConstants.One<T>.Value;
				if(typeof(YOut) == typeof(Zero)) return default;
				throw new InvalidOperationException();
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set
			{
				if(typeof(XIn) == typeof(Y)) this.Value.X = value;
				if(typeof(YIn) == typeof(Y)) this.Value.Y = value;
			}
		}

		/// <summary>
		/// Gets or sets the third swizzled component.
		/// This property returns the internal value as specified by <typeparamref name="ZOut"/> and sets the internal values where the 'In' generic arguments are <see cref="T:Z"/>.
		/// </summary>
		public T Z
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if(typeof(ZOut) == typeof(X)) return this.Value.X;
				if(typeof(ZOut) == typeof(Y)) return this.Value.Y;
				if(typeof(ZOut) == typeof(One)) return GenericConstants.One<T>.Value;
				if(typeof(ZOut) == typeof(Zero)) return default;
				throw new InvalidOperationException();
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set
			{
				if(typeof(XIn) == typeof(Z)) this.Value.X = value;
				if(typeof(YIn) == typeof(Z)) this.Value.Y = value;
			}
		}

		/// <summary>
		/// Gets or sets the fourth swizzled component.
		/// This property returns the internal value as specified by <typeparamref name="WOut"/> and sets the internal values where the 'In' generic arguments are <see cref="T:W"/>.
		/// </summary>
		public T W
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get
			{
				if(typeof(WOut) == typeof(X)) return this.Value.X;
				if(typeof(WOut) == typeof(Y)) return this.Value.Y;
				if(typeof(WOut) == typeof(One)) return GenericConstants.One<T>.Value;
				if(typeof(WOut) == typeof(Zero)) return default;
				throw new InvalidOperationException();
			}
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			set
			{
				if(typeof(XIn) == typeof(W)) this.Value.X = value;
				if(typeof(YIn) == typeof(W)) this.Value.Y = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Swizzle2To4{T, TVector, XIn, YIn, XOut, YOut, ZOut, WOut}"/> structure with the specified underlying value.
		/// </summary>
		/// <param name="value">The underlying value. No swizzling is applied in the constructor.</param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Swizzle2To4(in TVector value)
		{
			this.Value = value;
		}
		
		#endregion
		
		#region Methods

		public override int GetHashCode()
		{
			int hash = this.X.GetHashCode();
			hash = Utility.CombineHash(hash, this.Y.GetHashCode());
			hash = Utility.CombineHash(hash, this.Z.GetHashCode());
			hash = Utility.CombineHash(hash, this.W.GetHashCode());
			return hash;
		}
		
		/// <summary>
		/// Casts the specified swizzled structure into a 4-component value tuple.
		/// </summary>
		/// <param name="swizzled"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator (T x, T y, T z, T w)(in Swizzle2To4<T, TVector, XIn, YIn, XOut, YOut, ZOut, WOut> swizzled)
		{
			return (swizzled.X, swizzled.Y, swizzled.Z, swizzled.W);
		}
		
		/// <summary>
		/// Casts the specified tuple into a swizzled struct with the swizzling operations applied.
		/// </summary>
		/// <param name="vector"></param>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator Swizzle2To4<T, TVector, XIn, YIn, XOut, YOut, ZOut, WOut>(in (T x, T y, T z, T w) vector)
		{
			Swizzle2To4<T, TVector, XIn, YIn, XOut, YOut, ZOut, WOut> swizzled = default;
			swizzled.X = vector.x;
			swizzled.Y = vector.y;
			swizzled.Z = vector.z;
			swizzled.W = vector.w;
			return swizzled;
		}


		#endregion
	}
}
