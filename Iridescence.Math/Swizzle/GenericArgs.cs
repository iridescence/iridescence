﻿namespace Iridescence.Math.Swizzle
{
	/// <summary>
	/// Interface for a swizzle parameter.
	/// </summary>
	public interface ISwizzleParameter { }

	/// <summary>
	/// Interface for swizzle parameters that can be used for mapping the output of a vector with at least 1 component.
	/// </summary>
	public interface IOutputSwizzleParameter1 : IOutputSwizzleParameter2 { }

	/// <summary>
	/// Interface for swizzle parameters that can be used for mapping the output of a vector with at least 2 components.
	/// </summary>
	public interface IOutputSwizzleParameter2 : IOutputSwizzleParameter3 { }

	/// <summary>
	/// Interface for swizzle parameters that can be used for mapping the output of a vector with at least 3 components.
	/// </summary>
	public interface IOutputSwizzleParameter3 : IOutputSwizzleParameter4 { }

	/// <summary>
	/// Interface for swizzle parameters that can be used for mapping the output of a vector with at least 4 components.
	/// </summary>
	public interface IOutputSwizzleParameter4 : ISwizzleParameter { }

	/// <summary>
	/// Interface for swizzle parameters that can be used for mapping inputs to a structure.
	/// </summary>
	public interface IInputSwizzleParameter : ISwizzleParameter { }

	/// <summary>
	/// When used in an output mapping, specifies that a vector component on the swizzled struct returns the X component of the underlying vector.
	/// When used in an input mapping, specifies that the underlying value of a vector component is set when the X component on the swizzled struct is assigned.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct X : IOutputSwizzleParameter1, IInputSwizzleParameter { }

	/// <summary>
	/// When used in an output mapping, specifies that a vector component on the swizzled struct returns the Y component of the underlying vector.
	/// When used in an input mapping, specifies that the underlying value of a vector component is set when the Y component on the swizzled struct is assigned.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct Y : IOutputSwizzleParameter2, IInputSwizzleParameter { }

	/// <summary>
	/// When used in an output mapping, specifies that a vector component on the swizzled struct returns the Z component of the underlying vector.
	/// When used in an input mapping, specifies that the underlying value of a vector component is set when the Z component on the swizzled struct is assigned.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct Z : IOutputSwizzleParameter3, IInputSwizzleParameter { }

	/// <summary>
	/// When used in an output mapping, specifies that a vector component on the swizzled struct returns the W component of the underlying vector.
	/// When used in an input mapping, specifies that the underlying value of a vector component is set when the W component on the swizzled struct is assigned.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct W : IOutputSwizzleParameter4, IInputSwizzleParameter { }

	/// <summary>
	/// Specifies that a vector component returns a constant 0 (or default(T)).
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct Zero : IOutputSwizzleParameter1 { } 

	/// <summary>
	/// Specifies that a vector component returns a constant 1.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct One : IOutputSwizzleParameter1 { } 

	/// <summary>
	/// Specifies that the input for a vector component is to be ignored/discarded.
	/// This class should only be used for generic constraints.
	/// </summary>
	public struct None : IInputSwizzleParameter { }
}

