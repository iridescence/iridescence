﻿using System;
using System.Collections.Generic;

namespace Iridescence.Math
{
	[Flags]
	public enum CsgComponentRelation
	{
		Front = 1,
		Back = 2,
		Coplanar = 4,
	}
	
	/// <summary>
	/// Represents a CSG BSP node.
	/// </summary>
	public sealed class CsgNode<T> : IEnumerable<T> where T : ICsgComponent<T>
	{
		#region Interfaces

		#endregion

		#region Fields

		private List<T> components;
		private CsgNode<T> front;
		private CsgNode<T> back;

		#endregion

		#region Properties

		public IEnumerable<T> Components
		{
			get { return this.components; }
		}

		public CsgNode<T> Front
		{
			get { return this.front; }
		}

		public CsgNode<T> Back
		{
			get { return this.back; }
		}

		#endregion

		#region Constructors

		private CsgNode()
		{
			this.components = new List<T>();
		}

		public CsgNode(IEnumerable<T> components)
		{
			this.components = new List<T>();
			this.add(components);
		}

		#endregion

		#region Methods

		private void add(IEnumerable<T> components)
		{
			IEnumerator<T> enumerator = components.GetEnumerator();
			if (!enumerator.MoveNext())
				return;

			List<T> front = new List<T>();
			List<T> back = new List<T>();

			if (this.components.Count == 0)
			{
				this.components.Add(enumerator.Current);
				if (!enumerator.MoveNext())
					return;
			}

			do
			{
				this.split(enumerator.Current, this.components, this.components, front, back);
			} while (enumerator.MoveNext());
				

			if (front.Count > 0)
			{
				if(this.front == null)
					this.front = new CsgNode<T>();
				this.front.add(front);
			}

			if (back.Count > 0)
			{
				if (this.back == null)
					this.back = new CsgNode<T>();
				this.back.add(back);
			}
		}

		private List<T> clipComponents(List<T> clipComponents)
		{
			List<T> front = new List<T>();
			List<T> back = new List<T>();

			if (this.components.Count > 0)
			{
				for (int i = 0; i < clipComponents.Count; i++)
					this.split(clipComponents[i], front, back, front, back);	
			}
			
			if (this.front != null)
				front = this.front.clipComponents(front);

			if (this.back != null)
				back = this.back.clipComponents(back);
			else
				back.Clear();

			front.AddRange(back);
			return front;

		}

		private void split(T component, List<T> coplanarFront, List<T> coplanarBack, List<T> front, List<T> back)
		{
			T myComponent = this.components[0];

			CsgComponentRelation relation = myComponent.GetRelationTo(component);

			if ((relation & CsgComponentRelation.Coplanar) != 0)
			{
				if ((relation & CsgComponentRelation.Front) != 0)
					coplanarFront.Add(component);
				else
					coplanarBack.Add(component);
			}
			else
			{
				bool isFront = (relation & CsgComponentRelation.Front) != 0;
				bool isBack = (relation & CsgComponentRelation.Back) != 0;

				if (isFront && isBack)
				{
					T frontPoly;
					T backPoly;
					component.Split(myComponent, out frontPoly, out backPoly);

					front.Add(frontPoly);
					back.Add(backPoly);
				}
				else if (isFront)
				{
					front.Add(component);
				}
				else if (isBack)
				{
					back.Add(component);
				}
			}
		}

		/// <summary>
		/// Returns a deep copy of this node.
		/// </summary>
		/// <returns></returns>
		public CsgNode<T> Clone()
		{
			CsgNode<T> clone = new CsgNode<T>();

			clone.components = new List<T>(this.components.Count);

			foreach (T component in this.components)
				clone.components.Add(component.Clone());

			if (this.front != null)
				clone.front = this.front.Clone();

			if (this.back != null)
				clone.back = this.back.Clone();

			return clone;
		}

		/// <summary>
		/// Inverts this node.
		/// </summary>
		public void Invert()
		{
			for (int i = 0; i < this.components.Count; i++)
				this.components[i] = this.components[i].Invert();

			if (this.front != null)
				this.front.Invert();

			if (this.back != null)
				this.back.Invert();

			CsgNode<T> temp = this.front;
			this.front = this.back;
			this.back = temp;
		}
		
		/// <summary>
		/// Removes everything from this node that lies within the specified node.
		/// </summary>
		/// <param name="node"></param>
		public void Remove(CsgNode<T> node)
		{
			this.components = node.clipComponents(this.components);

			if (this.front != null)
			{
				this.front.Remove(node);
			}

			if (this.back != null)
			{
				this.back.Remove(node);
			}

			if (this.components.Count == 0)
			{
				CsgNode<T> front = this.front;
				CsgNode<T> back = this.back;

				this.front = null;
				this.back = null;

				if (front != null)
					this.add(front);

				if (back != null)
					this.add(back);
			}
		}


		/// <summary>
		/// Merges this node with the specified node.
		/// </summary>
		/// <param name="node">The other node. Will be useless after this operation, so make sure to clone it.</param>
		public void Union(CsgNode<T> node)
		{
			CsgNode<T> a = this;
			CsgNode<T> b = node;
			a.Remove(b); 
			b.Remove(a);
			b.Invert();
			b.Remove(a);
			b.Invert();
			a.add(b);
		}

		/// <summary>
		/// Subtracts the specified node from this node.
		/// </summary>
		/// <param name="node">The other node. Will be useless after this operation, so make sure to clone it.</param>
		public void Subtract(CsgNode<T> node)
		{
			CsgNode<T> a = this;
			CsgNode<T> b = node.Clone();
			a.Invert();
			a.Remove(b);
			b.Remove(a);
			b.Invert();
			b.Remove(a);
			b.Invert();
			a.add(b);
			a.Invert();
		}

		/// <summary>
		/// Intersects the specified node with this node.
		/// </summary>
		/// <param name="node">The other node. Will be useless after this operation, so make sure to clone it.</param>
		public void Intersect(CsgNode<T> node)
		{
			CsgNode<T> a = this;
			CsgNode<T> b = node.Clone();
			a.Invert();
			b.Remove(a);
			b.Invert();
			a.Remove(b);
			b.Remove(a);
			a.add(b);
			a.Invert();
		}

		public IEnumerator<T> GetEnumerator()
		{
			foreach (T component in this.components)
				yield return component;

			if (this.front != null)
			{
				foreach (T component in this.front)
					yield return component;
			}

			if (this.back != null)
			{
				foreach (T component in this.back)
					yield return component;
			}
		}

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}

	/// <summary>
	/// Interface for abstract CSG components.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface ICsgComponent<T> where T : ICsgComponent<T>
	{
		/// <summary>
		/// Returns a copy of the component.
		/// </summary>
		/// <returns></returns>
		T Clone();

		/// <summary>
		/// Inverts the component.
		/// </summary>
		/// <returns></returns>
		T Invert();

		/// <summary>
		/// Splits the component.
		/// </summary>
		/// <param name="other"></param>
		/// <param name="front"></param>
		/// <param name="back"></param>
		void Split(T other, out T front, out T back);

		/// <summary>
		/// Returns the relation to another component.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		CsgComponentRelation GetRelationTo(T other);
	}
}

