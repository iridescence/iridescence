﻿using System;
using System.Globalization;
using System.Text;

namespace Iridescence.Math
{
	/// <summary>
	/// Represents a generic matrix implemented as two-dimensional array.
	/// </summary>
	public class ArrayMatrix<T> : IMatrix<T>, IEquatable<IMatrix<T>>, IFormattable
	{
		#region Properties

		/// <summary>
		/// Gets the array.
		/// </summary>
		public T[,] Array { get; }

		/// <summary>
		/// Gets the number of rows in the matrix.
		/// </summary>
		public int Rows => this.Array.GetLength(0);

		/// <summary>
		/// Gets the number of columns in the matrix.
		/// </summary>
		public int Columns => this.Array.GetLength(1);

		public T this[int row, int column]
		{
			get => this.Array[row, column];
			set => this.Array[row, column] = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ArrayMatrix{T}"/> class. 
		/// </summary>
		public ArrayMatrix(int rows, int columns)
		{
			this.Array = new T[rows, columns];
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ArrayMatrix{T}"/> class. 
		/// </summary>
		public ArrayMatrix(T[,] array)
		{
			this.Array = array;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ArrayMatrix{T}"/> class by copying an existing matrix.
		/// </summary>
		/// <param name="matrix">A matrix to copy.</param>
		public ArrayMatrix(IMatrix<T> matrix)
		{
			this.Array = new T[matrix.Rows, matrix.Columns];

			for (int row = 0; row < this.Rows; ++row)
			{
				for (int col = 0; col < this.Columns; ++col)
				{
					this.Array[row, col] = matrix[row, col];
				}
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a one-dimensional array that contains the matrix entries in the specified layout.
		/// </summary>
		/// <param name="layout">The array layout.</param>
		/// <returns></returns>
		public T[] ToArray(MultidimensionalLayout layout)
		{
			T[] values = new T[this.Rows * this.Columns];

			for (int row = 0; row < this.Rows; ++row)
			{
				for (int col = 0; col < this.Columns; ++col)
				{
					if (layout == MultidimensionalLayout.RowMajor)
						values[row * this.Rows + col] = this[row, col];
					else
						values[col * this.Columns + row] = this[row, col];
				}
			}

			return values;
		}

		public bool Equals(IMatrix<T> other)
		{
			if (other == null)
				return false;

			if (other.Rows != this.Rows || other.Columns != this.Columns)
				return false;

			for (int row = 0; row < this.Rows; ++row)
			{
				for (int col = 0; col < this.Columns; ++col)
				{
					if (!object.Equals(this[row, col], other[row, col]))
						return false;
				}
			}

			return true;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;

			IMatrix<T> other = obj as IMatrix<T>;
			return this.Equals(other);
		}

		public override int GetHashCode()
		{
			return (this.Array != null ? this.Array.GetHashCode() : 0);
		}

		public static bool operator ==(ArrayMatrix<T> left, ArrayMatrix<T> right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(ArrayMatrix<T> left, ArrayMatrix<T> right)
		{
			return !Equals(left, right);
		}

		/// <summary>
		/// Returns a string representation of the matrix.
		/// </summary>
		/// <param name="entryFormat">The format for entries (only applies if <see cref="T"/> implements <see cref="IFormattable"/>).</param>
		/// <param name="formatProvider">The format provider.</param>
		/// <returns></returns>
		public string ToString(string entryFormat, IFormatProvider formatProvider)
		{
			StringBuilder str = new StringBuilder();
			
			str.Append("{ ");

			for (int row = 0; row < this.Rows; ++row)
			{
				str.Append("{ ");

				for (int col = 0; col < this.Columns; ++col)
				{
					T val = this.Array[row, col];
					IFormattable formattable = val as IFormattable;
					if (formattable != null)
						str.Append(formattable.ToString(entryFormat, formatProvider));
					else
						str.Append(val);
					str.Append(' ');
				}

				str.Append("} ");
			}

			str.Append('}');
			return str.ToString();
		}

		/// <summary>
		/// Returns a string representation of the matrix.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return this.ToString(null, NumberFormatInfo.CurrentInfo);
		}

		#endregion
	}
}
