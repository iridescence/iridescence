﻿namespace Iridescence.Math
{
	/// <summary>
	/// Interface for generic 4-element vectors.
	/// </summary>
	public interface IVector4<T> : IVector3<T>
	{
		/// <summary>
		/// Gets or sets the fourth component of the vector.
		/// </summary>
		T W { get; set; }
	}
}
