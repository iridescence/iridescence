﻿using System.Runtime.CompilerServices;

namespace Iridescence.Math
{
	public static partial class Utility
	{
		/// <summary>
		/// Returns a specified number raised to the specified power.
		/// </summary>
		/// <param name="x">The number to be raised to the power.</param>
		/// <param name="y">The power.</param>
		/// <returns>The number x raised to the power y.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static float Pow(float x, float y)
		{
			return (float)System.Math.Pow(x, y);
		}

		/// <summary>
		/// Returns a specified number raised to the specified power.
		/// </summary>
		/// <param name="x">The number to be raised to the power.</param>
		/// <param name="y">The power.</param>
		/// <returns>The number x raised to the power y.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static double Pow(double x, double y)
		{
			return System.Math.Pow(x, y);
		}

		/// <summary>
		/// Returns a specified number raised to the specified power.
		/// </summary>
		/// <param name="x">The number to be raised to the power.</param>
		/// <param name="y">The power.</param>
		/// <returns>The number x raised to the power y.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Pow(int x, int y)
		{
			if (y < 0)
				return 0;

			int result = 1;
			while (y != 0)
			{
				if ((y & 1) != 0)
					result *= x;
				y >>= 1;
				x *= x;
			}

			return result;
		}

		/// <summary>
		/// Returns a specified number raised to the specified power.
		/// </summary>
		/// <param name="x">The number to be raised to the power.</param>
		/// <param name="y">The power.</param>
		/// <returns>The number x raised to the power y.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static uint Pow(uint x, int y)
		{
			if (y < 0)
				return 0;

			uint result = 1;
			while (y != 0)
			{
				if ((y & 1) != 0)
					result *= x;
				y >>= 1;
				x *= x;
			}

			return result;
		}

		/// <summary>
		/// Returns a specified number raised to the specified power.
		/// </summary>
		/// <param name="x">The number to be raised to the power.</param>
		/// <param name="y">The power.</param>
		/// <returns>The number x raised to the power y.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static long Pow(long x, int y)
		{
			if (y < 0)
				return 0;

			long result = 1;
			while (y != 0)
			{
				if ((y & 1) != 0)
					result *= x;
				y >>= 1;
				x *= x;
			}

			return result;
		}

		/// <summary>
		/// Returns a specified number raised to the specified power.
		/// </summary>
		/// <param name="x">The number to be raised to the power.</param>
		/// <param name="y">The power.</param>
		/// <returns>The number x raised to the power y.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static ulong Pow(ulong x, int y)
		{
			if (y < 0)
				return 0;

			ulong result = 1;
			while (y != 0)
			{
				if ((y & 1) != 0)
					result *= x;
				y >>= 1;
				x *= x;
			}

			return result;
		}
	}
}
