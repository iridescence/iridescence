﻿using System;

namespace Iridescence.Math
{
	/// <summary>
	/// Generic vector methods and operations.
	/// </summary>
	public static class Vector
	{
		#region Get

		/// <summary>
		/// Copies values from a vector to an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceVector">The source vector.</param>
		/// <param name="sourceOffset">An offset in the source vector.</param>
		/// <param name="destArray">The destination array.</param>
		/// <param name="destOffset">An offset in the destination array.</param>
		/// <param name="count">The number of elements to copy.</param>
		public static void Get<T>(this IVector<T> sourceVector, int sourceOffset, T[] destArray, int destOffset, int count)
		{
			if (sourceOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(sourceOffset));

			if (sourceOffset + count >= sourceVector.Count)
				throw new ArgumentOutOfRangeException(nameof(count));

			if (destOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(destOffset));

			if (destOffset + count >= destArray.Length)
				throw new ArgumentOutOfRangeException(nameof(count));

			for (int i = 0; i < count; ++i)
			{
				destArray[destOffset + i] = sourceVector[sourceOffset + i];
			}
		}

		/// <summary>
		/// Copies values from a vector to an array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="sourceVector">The source vector.</param>
		/// <param name="destArray">The destination array.</param>
		public static void Get<T>(this IVector<T> sourceVector, T[] destArray)
		{
			Get(sourceVector, 0, destArray, 0, sourceVector.Count);
		}
		
		#endregion

		#region Set

		/// <summary>
		/// Copies values from an array to a vector.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="destVector">The destination vector.</param>
		/// <param name="destOffset">An offset in the destination vector.</param>
		/// <param name="sourceArray">The source array.</param>
		/// <param name="sourceOffset">An offset in the source array.</param>
		/// <param name="count">The number of elements to copy.</param>
		public static void Set<T>(this IVector<T> destVector, int destOffset, T[] sourceArray, int sourceOffset, int count)
		{
			if (destOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(destOffset));

			if (destOffset + count >= destVector.Count)
				throw new ArgumentOutOfRangeException(nameof(count));

			if (sourceOffset < 0)
				throw new ArgumentOutOfRangeException(nameof(sourceOffset));

			if (sourceOffset + count >= sourceArray.Length)
				throw new ArgumentOutOfRangeException(nameof(count));

			for (int i = 0; i < count; ++i)
			{
				destVector[destOffset + i] = sourceArray[sourceOffset + i];
			}
		}

		/// <summary>
		/// Copies values from an array to a vector.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="destVector">The destination vector.</param>
		/// <param name="sourceArray">The source array.</param>
		public static void Set<T>(this IVector<T> destVector, T[] sourceArray)
		{
			Set(destVector, 0, sourceArray, 0, destVector.Count);
		}
		
		#endregion
	}
}
