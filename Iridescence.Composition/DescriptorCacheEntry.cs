﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	internal readonly struct DescriptorCacheEntry : IEnumerable<ExportDescriptor>
	{
		public readonly ImmutableArray<ExportDescriptor> Descriptors;

		public DescriptorCacheEntry(ImmutableArray<ExportDescriptor> descriptors)
		{
			this.Descriptors = descriptors;
		}

		public DescriptorCacheEntry(IReadOnlyList<ExportDescriptor> descriptors)
		{
			if (descriptors.Count == 0)
				this.Descriptors = ImmutableArray<ExportDescriptor>.Empty;
			else
				this.Descriptors = descriptors.ToImmutableArray();
		}

		public ImmutableArray<ExportDescriptor>.Enumerator GetEnumerator() => this.Descriptors.GetEnumerator();

		IEnumerator<ExportDescriptor> IEnumerable<ExportDescriptor>.GetEnumerator() => ((IEnumerable<ExportDescriptor>)this.Descriptors).GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)this.Descriptors).GetEnumerator();
	}
}