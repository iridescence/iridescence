﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents a composition operation for a graph. Supports completion events.
	/// </summary>
	public class CompositionOperation : IDisposable
	{
		#region Fields

		private List<Action> completing;
		private List<Action> completed;
		private Dictionary<object, object> instances;
		private object sharingLock;

		#endregion

		#region Properties

		public CompositionOperationState State { get; private set; }
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionOperation"/> class. 
		/// </summary>
		private CompositionOperation()
		{
			this.State = CompositionOperationState.Running;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Runs a composition operation.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="activator"></param>
		/// <returns></returns>
		public static object Run(CompositionContext context, CompositeActivator activator)
		{
			if (activator == null)
				throw new ArgumentNullException(nameof(activator));

			using (CompositionOperation operation = new CompositionOperation())
			{
				object result = activator(context, operation);
				operation.complete();
				return result;
			}
		}

		internal void EnterSharingLock(object sharingLock)
		{
			if (sharingLock == null)
				throw new ArgumentNullException(nameof(sharingLock));

			if (this.sharingLock == null)
			{
				this.sharingLock = sharingLock;
				Monitor.Enter(sharingLock);
			}

			if (this.sharingLock != sharingLock)
				throw new InvalidOperationException("Sharing lock taken");
		}
		
		/// <summary>
		/// Gets or adds an object instance with the specified key to the operation.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="valueFactory"></param>
		/// <returns></returns>
		public object GetOrAdd(object key, Func<object, object> valueFactory)
		{
			object value;

			if (this.instances == null)
			{
				this.instances = new Dictionary<object, object>();
			}
			else if (this.instances.TryGetValue(key, out value))
			{
				return value;
			}

			value = valueFactory(key);
			this.instances.Add(key, value);

			return value;
		}

		private void complete()
		{
			this.State = CompositionOperationState.Completing;
			if (this.completing != null)
			{
				List<Action> list = this.completing;
				this.completing = null;

				foreach (Action action in list)
				{
					action();
				}
			}

			this.State = CompositionOperationState.Completed;
			if (this.completed != null)
			{
				List<Action> list = this.completed;
				this.completed = null;

				foreach (Action action in list)
				{
					action();
				}
			}

			this.instances = null;
		}

		/// <summary>
		/// Occurs after all prerequisite parts have been activated.
		/// </summary>
		public event Action Completing
		{
			add
			{
				if (this.State >= CompositionOperationState.Completing)
				{
					value();
					return;
				}

				if (this.completing == null)
				{
					this.completing = new List<Action>();
				}

				this.completing.Add(value);
			}
			remove => this.completing?.Remove(value);
		}

		/// <summary>
		/// Occurs when the composition operation has been completed.
		/// </summary>
		public event Action Completed
		{
			add
			{
				if (this.State >= CompositionOperationState.Completed)
				{
					value();
					return;
				}

				if (this.completed == null)
				{
					this.completed = new List<Action>();
				}

				this.completed.Add(value);
			}
			remove => this.completed?.Remove(value);
		}

		public void Dispose()
		{
			if (this.sharingLock != null)
			{
				Monitor.Exit(this.sharingLock);
			}
		}
		
		#endregion
	}
}
