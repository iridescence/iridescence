﻿using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Composition
{
	public abstract class TransformProvider<TIn, TOut> : IExportDescriptorProvider
	{
		/// <summary>
		/// Gets the name of the input dependency.
		/// This is shown in error messages when the input dependency is missing.
		/// </summary>
		protected virtual string InputName => "Input";

		/// <summary>
		/// Transforms an input object into an output object.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="context"></param>
		/// <returns></returns>
		public abstract TOut Transform(TIn input, CompositionContext context);

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			if (!contract.Type.IsAssignableFrom(typeof(TOut)))
				yield break;

			CompositionContract inputContract = contract.ChangeType(typeof(TIn));

			foreach (Dependency dep in dependencyAccessor.ResolveDependencies(this.InputName, inputContract))
			{
				yield return new ExportDescriptorPromise(
					contract,
					this.ToString(),
					true,
					() => EnumerableExtensions.Once(dep),
					deps =>
					{
						ExportDescriptor fileDescriptor = deps.First().Promise.GetDescriptor();

						return ExportDescriptor.Create((context, operation) =>
						{
							TIn input = (TIn)CompositionOperation.Run(context, fileDescriptor.Activator);
							return this.Transform(input, context);
						}, contract.Metadata);
					});
			}
		}
	}
}
