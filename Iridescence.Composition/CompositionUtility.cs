﻿using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Composition
{
	/// <summary>
	/// Utility methods.
	/// </summary>
	public static class CompositionUtility
	{
		/// <summary>
		/// Tries to get the dependency with the specified site from a collection of dependencies.
		/// </summary>
		/// <param name="dependencies"></param>
		/// <param name="site"></param>
		/// <param name="dependencyPromise"></param>
		/// <returns></returns>
		public static bool TryGetDependency(this IEnumerable<Dependency> dependencies, object site, out ExportDescriptorPromise dependencyPromise)
		{
			Dependency dependency = dependencies.FirstOrDefault(dep => object.ReferenceEquals(dep.Site, site));

			if (dependency == null)
			{
				dependencyPromise = null;
				return false;
			}

			dependencyPromise = dependency.Promise;
			return true;
		}

		/// <summary>
		/// Returns the dependency with the specified site from a collection of dependencies.
		/// </summary>
		/// <param name="dependencies"></param>
		/// <param name="site"></param>
		/// <param name="throwIfMissing"></param>
		/// <returns></returns>
		public static ExportDescriptorPromise GetDependency(this IEnumerable<Dependency> dependencies, object site, bool throwIfMissing = true)
		{
			if (dependencies.TryGetDependency(site, out ExportDescriptorPromise promise))
				return promise;

			if (throwIfMissing)
				throw new CompositionException($"Missing dependency for site \"{site}\".");

			return null;
		}
	}
}
