// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Collections.Generic;

namespace Iridescence.Composition
{
	internal class DirectExportDescriptor : ExportDescriptor
	{
		public override CompositeActivator Activator { get; }

		public override IReadOnlyList<CompositionMetadata> Metadata { get; }

		public DirectExportDescriptor(CompositeActivator activator, IReadOnlyList<CompositionMetadata> metadata)
		{
			this.Activator = activator;
			this.Metadata = metadata;
		}
	}
}
