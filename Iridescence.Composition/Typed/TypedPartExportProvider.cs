﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Exports types marked with <see cref="IExportAttribute"/>.
	/// </summary>
	public class TypedPartExportProvider : IExportDescriptorProvider
	{
		#region Fields

		private readonly ImmutableArray<DiscoveredPart> parts;

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TypedPartExportProvider"/> class. 
		/// </summary>
		public TypedPartExportProvider(IEnumerable<Type> types, ITypeModelProvider attributeProvider = null, ISharingManager sharingManager = null)
		{
			attributeProvider ??= DefaultModelProvider.Instance;
			sharingManager ??= new DictionarySharingManager();
			//this.types = types.Select(t => new TypeCompositionInfo(t, attributeProvider, sharingManager)).ToArray();
			
			TypeInspector inspector = new TypeInspector(attributeProvider, sharingManager, TypedUtility.DefaultActivationFeatures);
			var list = ImmutableArray.CreateBuilder<DiscoveredPart>();
			foreach (Type type in types)
			{
				if (inspector.TryGetPart(type, out DiscoveredPart part))
					list.Add(part);
			}

			this.parts = list.ToImmutable();
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TypedPartExportProvider"/> class. 
		/// </summary>
		public TypedPartExportProvider(Assembly assembly)
			: this(assembly.ExportedTypes)
		{
			
		}
		
		#endregion
		
		#region Methods

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			foreach (DiscoveredPart part in this.parts)
			{
				foreach (ExportDescriptorPromise promise in part.GetPromises(contract, dependencyAccessor))
				{
					yield return promise;
				}
			}
		}

		#endregion
	}
}
