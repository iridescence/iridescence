﻿using System.Collections.Generic;

namespace Iridescence.Composition
{
	public static class ImportInfoExtensions
	{
		public static Dependency Resolve(this ImportInfo import, DependencyAccessor dependencyAccessor)
		{
			if (import.IsOptional)
			{
				if (dependencyAccessor.TryResolveOptionalDependency(import, import.Contract, DependencyFlags.None, out Dependency dep))
					return dep;
			}
			else
			{
				return dependencyAccessor.ResolveRequiredDependency(import, import.Contract);
			}

			return null;
		}

		public static IEnumerable<Dependency> Resolve(this IEnumerable<ImportInfo> imports, DependencyAccessor dependencyAccessor)
		{
			foreach (ImportInfo info in imports)
			{
				Dependency dep = info.Resolve(dependencyAccessor);
				if (dep != null)
					yield return dep;
			}
		}
	}
}