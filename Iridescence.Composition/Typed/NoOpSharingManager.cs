﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Implements a dummy sharing manager that does not share instances.
	/// </summary>
	internal sealed class NoOpSharingManager : ISharingManager
	{
		public static NoOpSharingManager Instance { get; } = new NoOpSharingManager();

		public object GetOrAdd(object key, Func<object> factory) => factory();
	}
}
