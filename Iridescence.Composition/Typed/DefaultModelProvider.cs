﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Default type model provider that simply resolves attributes using reflection.
	/// </summary>
	public class DefaultModelProvider : ITypeModelProvider
	{
		public static readonly DefaultModelProvider Instance = new DefaultModelProvider();

		#region Methods
		
		public virtual IEnumerable<Attribute> GetTypeAttributes(Type type)
		{
			return type.GetCustomAttributes();
		}

		public virtual IEnumerable<Attribute> GetParameterAttributes(ParameterInfo parameter)
		{
			return parameter.GetCustomAttributes();
		}

		public IEnumerable<Attribute> GetMemberAttributes(Type type, MemberInfo member)
		{
			return member.GetCustomAttributes();
		}

		public virtual ConstructorInfo GetImportingConstructor(Type type)
		{
			ConstructorInfo[] constructors = type
				.GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly)
				.Where(ctor => ctor.GetCustomAttribute<ImportingConstructorAttribute>() != null)
				.ToArray();

			if (constructors.Length == 0)
				return null;

			if (constructors.Length == 1)
				return constructors[0];

			throw new CompositionException($"There is more than one importing constructor declared in {type}.");
		}

		public virtual IEnumerable<MethodInfo> GetImportedCallbacks(Type type)
		{
			foreach (MethodInfo method in type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
			{
				if (method.GetCustomAttribute<OnImportedAttribute>() != null)
				{
					if (method.ReturnType != typeof(void) || method.GetParameters().Length > 0 || method.IsGenericMethodDefinition)
						throw new CompositionException($"Method \"{method}\" on type \"{type}\" is marked as import callback, but does not have the right signature.");

					yield return method;
				}
			}
		}

		#endregion
	}
}
