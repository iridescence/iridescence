﻿namespace Iridescence.Composition
{
	/// <summary>
	/// Base class for runtime-generated factory implementations.
	/// </summary>
	public abstract class EmittedFactoryBase
	{
		#region Properties

		/*
		/// <summary>
		/// Gets the contract of the product produced by the factory.
		/// </summary>
		public abstract CompositionContract ProductContract { get; }
		*/

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="EmittedFactoryBase"/> class. 
		/// </summary>
		public EmittedFactoryBase()
		{

		}

		#endregion
	}
}
