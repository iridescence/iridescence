﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Composition
{
	internal class DiscoveredPropertyExport : DiscoveredExport
	{
		#region Fields

		private static readonly MethodInfo activatorInvoke = typeof(CompositeActivator).GetInvokeMethod();

		#endregion

		#region Properties

		public PropertyInfo Property { get; }

		public IExportAttribute Attribute { get; }

		protected override string Name => $"{this.Property.DeclaringType?.GetDisplayName()}.{this.Property.Name}";

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscoveredPropertyExport"/> class. 
		/// </summary>
		public DiscoveredPropertyExport(PropertyInfo property, IExportAttribute attribute)
		{
			this.Property = property;
			this.Attribute = attribute;
		}

		#endregion

		#region Methods

		protected override IEnumerable<ExportInfo> GetExports(CompositionContract importContract)
		{
			Lazy<CompositionContract> contractLazy = new Lazy<CompositionContract>(() => importContract);
			foreach (ExportInfo export in this.Attribute.GetExports(this.Property, contractLazy))
			{
				yield return export;
			}
		}

		protected override ExportDescriptor GetDescriptor(CompositeActivator partActivator)
		{
			MethodInfo propGetter = this.Property.GetGetMethod();

			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(typeof(CompositeActivator)).WithThis(typeof(CompositeActivator)));
			emit.LoadArgument(0);
			emit.LoadArgument(1);
			emit.LoadArgument(2);
			emit.CallVirtual(activatorInvoke);
			emit.CastClass(this.Property.DeclaringType);
			emit.CallVirtual(propGetter);
			emit.Return();
			CompositeActivator propActivator = emit.CreateDelegate<CompositeActivator>(partActivator);

			return ExportDescriptor.Create(propActivator);
		}

		#endregion
	}
}
