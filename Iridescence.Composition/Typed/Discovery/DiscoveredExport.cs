﻿using System.Collections.Generic;

namespace Iridescence.Composition
{
	internal abstract class DiscoveredExport : IExportDescriptorProvider
	{
		#region Properties

		public DiscoveredPart Part { get; internal set; }

		protected abstract string Name { get; }

		#endregion

		#region Methods

		protected abstract IEnumerable<ExportInfo> GetExports(CompositionContract importContract);

		protected abstract ExportDescriptor GetDescriptor(CompositeActivator partActivator);

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract importContract, DependencyAccessor dependencyAccessor)
		{
			foreach (ExportInfo exportInfo in this.GetExports(importContract))
			{
				if (!importContract.Type.IsAssignableFrom(exportInfo.Type))
					continue;

				if (!exportInfo.Metadata.MetadataEquals(importContract.Metadata))
					continue;

				yield return new ExportDescriptorPromise(
					importContract,
					this.Name,
					exportInfo.IsShared,
					() => this.Part.GetDependencies(exportInfo, dependencyAccessor),
					deps =>
					{
						CompositeActivator partActivator = this.Part.GetActivator(exportInfo, deps);
						return this.GetDescriptor(partActivator);
					});
			}
		}

		#endregion
	}
}
