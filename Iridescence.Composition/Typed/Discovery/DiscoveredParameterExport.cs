﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents a method parameter as export during a call to the method.
	/// </summary>
	/// <remarks>
	/// The <see cref="CompositionContext"/> passed to the activator method of an <see cref="ExportDescriptor"/> retrieved from this provider must implement <see cref="IParameterProvider"/>.
	/// </remarks>
	internal class DiscoveredParameterExport : IExportDescriptorProvider
	{
		#region Properties

		public ParameterInfo Parameter { get; }

		public IExportAttribute Attribute { get; }

		public bool HideOtherExports { get; }

		#endregion
			   
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscoveredInstanceExport"/> class. 
		/// </summary>
		public DiscoveredParameterExport(ParameterInfo parameter, IExportAttribute attribute, bool hideOtherExports)
		{
			this.Parameter = parameter;
			this.Attribute = attribute;
			this.HideOtherExports = hideOtherExports;
		}

		#endregion

		#region Methods

		public IEnumerable<ExportInfo> GetExports(CompositionContract importContract)
		{
			if (this.Attribute == null)
			{
				// Default export for parameters.
				yield return new ExportInfo(this.Parameter.ParameterType, ImmutableArray<CompositionMetadata>.Empty, true);
			}
			else
			{
				Lazy<CompositionContract> contractLazy = new Lazy<CompositionContract>(() => importContract);
				foreach (ExportInfo export in this.Attribute.GetExports(this.Parameter, contractLazy))
				{
					yield return export;
				}
			}
		}

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract importContract, DependencyAccessor dependencyAccessor)
		{
			foreach (ExportInfo exportInfo in this.GetExports(importContract))
			{
				if (!importContract.Type.IsAssignableFrom(exportInfo.Type))
					continue;

				if (!exportInfo.Metadata.MetadataEquals(importContract.Metadata))
					continue;

				yield return new ExportDescriptorPromise(
					importContract,
					this.Parameter.Name,
					exportInfo.IsShared,
					Dependency.NoDependencies,
					deps =>
					{
						return ExportDescriptor.Create((c, o) =>
						{
							IParameterProvider parameterProvider = (IParameterProvider)c;
							return parameterProvider.GetParameterValue(this.Parameter);
						});
					});
			}
		}

		#endregion

	}
}
