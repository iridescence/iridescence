﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Iridescence.Composition
{
	/// <summary>
	/// TODO
	/// </summary>
	internal class DiscoveredPart
	{
		#region Fields

		private readonly ImmutableArray<IActivationFeature> activationFeatures;
		private CompositeActivator activator;

		#endregion

		#region Properties

		public Type Type { get; }

		public ITypeModelProvider Model { get; }

		public ISharingManager SharingManager { get; }

		public ImmutableArray<DiscoveredExport> Exports { get; internal set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscoveredPart"/> class. 
		/// </summary>
		public DiscoveredPart(Type type, ITypeModelProvider model, ISharingManager sharingManager, ImmutableArray<IActivationFeature> activationFeatures)
		{
			this.Type = type;
			this.Model = model;
			this.SharingManager = sharingManager;
			this.activationFeatures = activationFeatures;
		}

		#endregion

		#region Methods

		public IEnumerable<Dependency> GetDependencies(ExportInfo export, DependencyAccessor dependencyAccessor)
		{
			DiscoveredPartExportInfo info = new DiscoveredPartExportInfo(this, export);
			return this.activationFeatures.SelectMany(f => f.GetDependencies(info, dependencyAccessor));
		}

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract importContract, DependencyAccessor dependencyAccessor)
		{
			foreach (DiscoveredExport export in this.Exports)
			{
				foreach (ExportDescriptorPromise promise in export.GetPromises(importContract, dependencyAccessor))
				{
					yield return promise;
				}
			}
		}

		public CompositeActivator GetActivator(ExportInfo export, ImmutableArray<Dependency> dependencies)
		{
			if (this.activator != null)
				return this.activator;

			CompositeActivator current = null;

			DiscoveredPartExportInfo info = new DiscoveredPartExportInfo(this, export);

			foreach (IActivationFeature feature in this.activationFeatures)
			{
				current = feature.Apply(info, current, dependencies);
			}

			this.activator = current ?? throw new CompositionException($"Could not find activator for part \"{this}\".");
			return current;
		}

		public override string ToString()
		{
			return this.Type.GetDisplayName();
		}

		#endregion
	}
}
