﻿using Iridescence.Emit;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents a dynamically implementable export factory discovered for a <see cref="TypedFactoryExportProvider"/>.
	/// </summary>
	internal class DiscoveredFactory
	{
		#region Fields

		private static readonly MethodInfo getPromisesMethod = typeof(DiscoveredFactory).GetMethod(nameof(getPromises), BindingFlags.Instance | BindingFlags.NonPublic);

		private Type compiledType;
		private Delegate createFactory;
		private readonly Type partType;
		private readonly bool isWrappedExport;

		#endregion

		#region Properties

		public Type FactoryType { get; }

		public MethodInfo FactoryMethod { get; }

		public ImmutableArray<DiscoveredParameterExport> ParameterExports { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscoveredFactory"/> class. 
		/// </summary>
		public DiscoveredFactory(Type factoryType, MethodInfo factoryMethod, ImmutableArray<DiscoveredParameterExport> parameterExports)
		{
			this.FactoryType = factoryType;
			this.FactoryMethod = factoryMethod;
			this.ParameterExports = parameterExports;

			this.partType = factoryMethod.ReturnType;

			if(this.partType.IsConstructedGenericType && this.partType.GetGenericTypeDefinition() == typeof(Export<>))
			{
				this.isWrappedExport = true;
				this.partType = this.partType.GetGenericArguments()[0];
			}
		}

		#endregion

		#region Methods

		private void implementInterface()
		{
			Type createDelType = typeof(CreateFactoryFunc<>).MakeGenericType(this.FactoryMethod.ReturnType);
			Type invokerDelType = typeof(Func<,>).MakeGenericType(typeof(object[]), this.FactoryMethod.ReturnType);

			TypeBuilder typeBuilder = DynamicModule.Module.DefineType($"{this.FactoryType.Name}-Impl-{Guid.NewGuid()}", TypeAttributes.Public | TypeAttributes.Class | TypeAttributes.Sealed, typeof(EmittedFactoryBase));
			typeBuilder.AddInterfaceImplementation(this.FactoryType);

			FieldBuilder callbackField = typeBuilder.DefineField("invoker", invokerDelType, FieldAttributes.Private);
			//FieldBuilder contractField = typeBuilder.DefineField("contract", typeof(CompositionContract), FieldAttributes.Private);

			MethodILBuilder il;

			// Empty parameterless constructor.
			ConstructorBuilder ctor = typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);

			// Allocate an object[] array and store all parameter values in it, then call the callback stored in the 'callbackField' with that object.
			il = MethodILBuilder.CreateOverride(typeBuilder, this.FactoryMethod);
			ParameterInfo[] parameters = this.FactoryMethod.GetParameters();
			Local paramArray = il.DeclareLocal(typeof(object[]), "params");
			il.LoadConstant(parameters.Length);
			il.NewArray(typeof(object));
			il.StoreLocal(paramArray);
			for (int i = 0; i < parameters.Length; ++i)
			{
				il.LoadLocal(paramArray);
				il.LoadConstant(i);
				il.LoadArgument(1 + i);
				if (parameters[i].ParameterType.IsValueType)
				{
					il.Box(parameters[i].ParameterType);
				}
				il.StoreElement(typeof(object));
			}
			il.LoadArgument(0);
			il.LoadField(callbackField);
			il.LoadLocal(paramArray);
			il.InvokeDelegate(invokerDelType);
			il.Return();
			typeBuilder.DefineMethodOverride(il.Compile(), this.FactoryMethod);

			/*
			// Getter for ProductContract property.
			PropertyInfo contractProperty = typeof(EmittedFactoryBase).GetProperty(nameof(EmittedFactoryBase.ProductContract), BindingFlags.Public | BindingFlags.Instance);
			MethodInfo contractPropertyGetter = contractProperty.GetGetMethod();
			il = ILBuilder.BuildMethodOverride(typeBuilder, contractPropertyGetter);
			il.LoadArgument(0);
			il.LoadField(contractField);
			il.Return();
			il.CreateMethod();
			typeBuilder.DefineMethodOverride(il.CreateMethod(), contractPropertyGetter);
			*/

			// Static constructor method, takes a contract and stores it in the 'contractField' and a callback and stores it in the 'callbackField'.
			il = MethodILBuilder.Create(MethodSignature.FromDelegate(createDelType), typeBuilder, "<CreateFactory>");
			Local objLocal = il.DeclareLocal(typeBuilder, "obj");
			il.NewObject(ctor, Array.Empty<Type>());
			il.StoreLocal(objLocal);
			il.LoadLocal(objLocal);
			/*
			il.LoadArgument(0);
			il.StoreField(contractField);
			il.LoadLocal(objLocal);
			*/
			il.LoadArgument(0);
			il.StoreField(callbackField);
			il.LoadLocal(objLocal);
			il.Return();
			MethodBuilder createMethod = il.Compile();

			// Compile class.
			this.compiledType = typeBuilder.CreateTypeInfo();
			MethodInfo activatorMethod = this.compiledType.GetMethod(createMethod.Name, BindingFlags.Public | BindingFlags.Static);
			this.createFactory = activatorMethod.CreateDelegate(createDelType);
		}

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract importContract, DependencyAccessor dependencyAccessor)
		{
			if (!importContract.Type.IsAssignableFrom(this.FactoryType))
				return Enumerable.Empty<ExportDescriptorPromise>();

			GetPromisesFunc func = (GetPromisesFunc)getPromisesMethod.MakeGenericMethod(this.partType).CreateDelegate(typeof(GetPromisesFunc), this);
			return func(importContract, dependencyAccessor);
		}

		private IEnumerable<ExportDescriptorPromise> getPromises<T>(CompositionContract importContract, DependencyAccessor dependencyAccessor)
		{
			CallAccessor accessor = new CallAccessor(dependencyAccessor, this.ParameterExports);
			CompositionContract productContract = importContract.ChangeType(this.partType);

			// Find all potential products and create factories for them.
			foreach (Dependency d in accessor.ResolveDependencies("product", productContract))
			{
				if (this.createFactory == null)
				{
					this.implementInterface();
				}

				yield return new ExportDescriptorPromise(
					importContract,
					this.FactoryType.GetDisplayName(),
					false,
					() => EnumerableExtensions.Once(d),
					_ =>
					{
						ExportDescriptor descriptor = d.Promise.GetDescriptor();
						CompositeActivator activator = descriptor.Activator;
						if (this.isWrappedExport)
						{
							CreateFactoryFunc<Export<T>> createFactory = (CreateFactoryFunc<Export<T>>)this.createFactory;

							// Return an Export<T> that disposes a lifetime contact.
							return ExportDescriptor.Create((c, o) =>
							{
								return createFactory(parameterValues =>
								{
									MethodCallLifetimeContext context = new MethodCallLifetimeContext(c, parameterValues);
									return new Export<T>((T)CompositionOperation.Run(context, activator), context.Dispose);
								});
							});
						}
						else
						{
							CreateFactoryFunc<T> createFactory = (CreateFactoryFunc<T>)this.createFactory;

							// Return T directly and bind disposable objects to the requesting context.
							return ExportDescriptor.Create((c, o) =>
							{
								return createFactory(parameterValues =>
								{
									MethodCallContext context = new MethodCallContext(c, parameterValues);
									return (T)CompositionOperation.Run(context, activator);
								});
							});
						}
					});
			}
		}

		#endregion

		#region Nested Types

		private delegate IEnumerable<ExportDescriptorPromise> GetPromisesFunc(CompositionContract contract, DependencyAccessor dependencyAccessor);

		private delegate object CreateFactoryFunc<T>(/*CompositionContract productContract,*/Func<object[], T> invoker);

		private class CallAccessor : DependencyAccessor
		{
			private readonly DependencyAccessor inner;
			private readonly ImmutableArray<DiscoveredParameterExport> exports;

			public CallAccessor(DependencyAccessor inner, ImmutableArray<DiscoveredParameterExport> exports)
			{
				this.inner = inner;
				this.exports = exports;
			}

			public override IEnumerable<ExportDescriptorPromise> GetPromises(DependencyQuery query)
			{
				bool continueWithInner = true;

				foreach (DiscoveredParameterExport export in this.exports)
				{
					foreach (ExportDescriptorPromise promise in query.GetPromises(export))
					{
						yield return promise;

						if (export.HideOtherExports)
							continueWithInner = false;
					}
				}

				if (!continueWithInner)
					yield break;

				foreach (ExportDescriptorPromise promise in this.inner.GetPromises(query))
				{
					yield return promise;
				}
			}
		}

		#endregion
	}
}
