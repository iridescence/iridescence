﻿using System;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	internal readonly struct DiscoveredPartExportInfo
	{
		public DiscoveredPart Part { get; }

		public ExportInfo ExportInfo { get; }

		public Type PartType => this.Part.Type;
		
		public ITypeModelProvider Model => this.Part.Model;

		public ISharingManager SharingManager => this.Part.SharingManager;

		public ImmutableArray<CompositionMetadata> ExportMetadata => this.ExportInfo.Metadata;

		public Type ExportType => this.ExportInfo.Type;
		
		public bool IsShared => this.ExportInfo.IsShared;

		public DiscoveredPartExportInfo(DiscoveredPart part, ExportInfo exportInfo)
		{
			this.Part = part;
			this.ExportInfo = exportInfo;
		}
	}
}
