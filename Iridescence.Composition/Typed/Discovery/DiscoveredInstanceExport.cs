﻿using System;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	internal class DiscoveredInstanceExport : DiscoveredExport
	{
		#region Properties

		public Type Type { get; }

		public IExportAttribute Attribute { get; }

		protected override string Name => this.Type.GetDisplayName();

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscoveredInstanceExport"/> class. 
		/// </summary>
		public DiscoveredInstanceExport(Type type, IExportAttribute attribute)
		{
			this.Type = type;
			this.Attribute = attribute;
		}

		#endregion

		#region Methods

		protected override IEnumerable<ExportInfo> GetExports(CompositionContract importContract)
		{
			Lazy<CompositionContract> contractLazy = new Lazy<CompositionContract>(() => importContract);
			foreach (ExportInfo export in this.Attribute.GetExports(this.Type, contractLazy))
			{
				yield return export;
			}
		}

		protected override ExportDescriptor GetDescriptor(CompositeActivator partActivator)
		{
			return ExportDescriptor.Create(partActivator);
		}

		#endregion
	}
}
