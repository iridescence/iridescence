﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	internal sealed class TypeInspector
	{
		#region Fields

		private readonly ITypeModelProvider model;
		private readonly ISharingManager sharingManager;
		private readonly ImmutableArray<IActivationFeature> activationFeatures;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TypeInspector"/> class. 
		/// </summary>
		public TypeInspector(ITypeModelProvider model, ISharingManager sharingManager, ImmutableArray<IActivationFeature> activationFeatures)
		{
			this.model = model;
			this.sharingManager = sharingManager;
			this.activationFeatures = activationFeatures;
		}

		#endregion

		#region Methods

		public bool TryGetPart(Type type, out DiscoveredPart part)
		{
			part = null;

			if (type.IsAbstract)
				return false;

			var exports = ImmutableArray.CreateBuilder<DiscoveredExport>();

			foreach (DiscoveredExport export in this.getExports(type))
			{
				part ??= new DiscoveredPart(type, this.model, this.sharingManager, this.activationFeatures);

				exports.Add(export);
				export.Part = part;
			}

			if (part == null)
				return false;

			part.Exports = exports.ToImmutable();
			return true;
		}
		
		private IEnumerable<DiscoveredExport> getExports(Type type)
		{
			foreach (DiscoveredExport export in this.getInstanceExports(type))
			{
				yield return export;
			}

			foreach (DiscoveredExport export in this.getPropertyExports(type))
			{
				yield return export;
			}
		}

		private IEnumerable<DiscoveredExport> getInstanceExports(Type type)
		{
			foreach (IExportAttribute attr in this.model.GetTypeAttributes(type).OfType<IExportAttribute>())
			{
				yield return new DiscoveredInstanceExport(type, attr);
			}
		}

		private IEnumerable<DiscoveredExport> getPropertyExports(Type type)
		{
			foreach (PropertyInfo prop in type.GetRuntimeProperties())
			{
				MethodInfo getter = prop.GetMethod;

				if (getter == null || !getter.IsPublic || getter.IsStatic)
					continue;

				foreach (IExportAttribute attr in this.model.GetMemberAttributes(type, prop).OfType<IExportAttribute>())
				{
					yield return new DiscoveredPropertyExport(prop, attr);
				}
			}
		}
		
		#endregion
	}
}
