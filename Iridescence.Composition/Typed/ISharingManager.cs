﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides the sharing mechanics for shared instances.
	/// </summary>
	public interface ISharingManager
	{
		/// <summary>
		/// Returns an existing object instance for the specified key - or, if none exists, calls the factory to retrieve a new instance.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="factory"></param>
		/// <returns></returns>
		object GetOrAdd(object key, Func<object> factory);
	}
}
