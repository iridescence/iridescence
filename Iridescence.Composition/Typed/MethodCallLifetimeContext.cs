﻿using System;
using System.Reflection;

namespace Iridescence.Composition
{
	internal class MethodCallLifetimeContext : LifetimeContext, IParameterProvider
	{
		private readonly object[] parameterValues;

		public MethodCallLifetimeContext(CompositionContext parent, object[] parameterValues)
			: base(parent)
		{
			this.parameterValues = parameterValues ?? throw new ArgumentNullException(nameof(parameterValues));
		}

		public object GetParameterValue(ParameterInfo parameter)
		{
			return this.parameterValues[parameter.Position];
		}
	}
}
