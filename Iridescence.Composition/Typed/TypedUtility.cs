﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	/// <summary>
	/// Utility methods for typed parts.
	/// </summary>
	public static class TypedUtility
	{
		internal static readonly ImmutableArray<IActivationFeature> DefaultActivationFeatures = ImmutableArray.Create<IActivationFeature>(
			new ImportingConstructorFeature(),
			new ParameterlessConstructorFeature(),
			new DisposableFeature(),
			new ImportFeature(),
			new ImportCallbackFeature(),
			new SharingFeature());

		/// <summary>
		/// Satisfies all imports declared in an object instance.
		/// </summary>
		public static void SatisfyImports(this CompositionContext context, object target, ITypeModelProvider model = null)
		{
			if (target == null)
				throw new ArgumentNullException(nameof(target));

			Type type = target.GetType();
			model ??= DefaultModelProvider.Instance;

			Dictionary<AssignableImportInfo, object> values = new Dictionary<AssignableImportInfo, object>();
			
			foreach (AssignableImportInfo import in ImportFeature.GetImports(model, type, default))
			{
				if (context.TryGetExport(import.Contract, out object value))
				{
					values.Add(import, value);
				}
				else
				{
					if (!import.IsOptional)
					{
						throw new DependencyException($"No export for \"{import}\" was found.");
					}
				}
			}

			foreach (KeyValuePair<AssignableImportInfo, object> pair in values)
			{
				pair.Key.Set(target, pair.Value);
			}

			ImportCallbackFeature.CombineImportCallbacks(type, model.GetImportedCallbacks(type))?.Invoke(target);
		}
	}
}
