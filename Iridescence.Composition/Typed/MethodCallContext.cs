﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Composition
{
	internal class MethodCallContext : CompositionContext, IParameterProvider
	{
		private readonly CompositionContext parent;
		private readonly object[] parameterValues;

		public MethodCallContext(CompositionContext parent, object[] parameterValues)
		{
			this.parent = parent ?? throw new ArgumentNullException(nameof(parent));
			this.parameterValues = parameterValues ?? throw new ArgumentNullException(nameof(parameterValues));
		}

		public override IEnumerable<ExportDescriptor> GetExportDescriptors(CompositionContract contract)
		{
			return this.parent.GetExportDescriptors(contract);
		}

		public override void AddDisposable(IDisposable obj)
		{
			this.parent.AddDisposable(obj);
		}

		public object GetParameterValue(ParameterInfo parameter)
		{
			return this.parameterValues[parameter.Position];
		}
	}
}
