﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Implements an export promise provider that dynamically generates implementations for factory interfaces.
	/// </summary>
	public class TypedFactoryExportProvider : IExportDescriptorProvider
	{
		#region Fields

		private readonly ImmutableArray<DiscoveredFactory> factories;
		private readonly ITypeModelProvider model;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="TypedFactoryExportProvider"/> class. 
		/// </summary>
		public TypedFactoryExportProvider(IEnumerable<Type> types, ITypeModelProvider attributeProvider = null)
		{
			this.model = attributeProvider ?? DefaultModelProvider.Instance;

			var list = ImmutableArray.CreateBuilder<DiscoveredFactory>();
			foreach (Type type in types)
			{
				if (this.tryGetFactory(type, out DiscoveredFactory factory))
					list.Add(factory);
			}

			this.factories = list.ToImmutable();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TypedPartExportProvider"/> class. 
		/// </summary>
		public TypedFactoryExportProvider(Assembly assembly)
			: this(assembly.ExportedTypes)
		{

		}

		#endregion

		#region Methods

		private bool tryGetFactory(Type type, out DiscoveredFactory factory)
		{
			factory = null;

			if (!type.IsInterface)
				return false;

			ExportFactoryAttribute attr = this.model.GetTypeAttributes(type).OfType<ExportFactoryAttribute>().FirstOrDefault();
			if (attr == null)
				return false;

			MethodInfo[] methods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance).Union(type.GetInterfaces().SelectMany(t => t.GetMethods(BindingFlags.Public | BindingFlags.Instance))).ToArray();
			if (methods.Length != 1)
				return false;

			MethodInfo factoryMethod = methods[0];

			if (factoryMethod.ReturnType == typeof(void))
				return false;

			ImmutableArray<DiscoveredParameterExport> parameterExports = this.getParameterExports(factoryMethod).ToImmutableArray();
			factory = new DiscoveredFactory(type, factoryMethod, parameterExports);

			return true;
		}

		private IEnumerable<DiscoveredParameterExport> getParameterExports(MethodInfo method)
		{
			foreach (ParameterInfo parameter in method.GetParameters())
			{
				bool hideOtherExports = this.model.GetParameterAttributes(parameter).Any(a => a is HideNonParameterExportsAttribute);

				bool anyAttributes = false;
				foreach (IExportAttribute attr in this.model.GetParameterAttributes(parameter).OfType<IExportAttribute>())
				{
					// Make the parameter avialable through its export attributes.
					yield return new DiscoveredParameterExport(parameter, attr, hideOtherExports);
					anyAttributes = true;
				}

				if (!anyAttributes)
				{
					// If it has no attributes, make it available through a default export.
					yield return new DiscoveredParameterExport(parameter, null, hideOtherExports);
				}
			}
		}

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			foreach(DiscoveredFactory factory in this.factories)
			{
				foreach (ExportDescriptorPromise promise in factory.GetPromises(contract, dependencyAccessor))
				{
					yield return promise;
				}
			}

			yield break;
		}

		#endregion
	}
}
