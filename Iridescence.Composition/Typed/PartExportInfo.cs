﻿using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Wraps an <see cref="ExportInfo"/> with extra information about the source of that export.
	/// </summary>
	public readonly struct PartExportInfo
	{
		#region Properties

		/// <summary>
		/// Gets the member the export stems from.
		/// </summary>
		public MemberInfo Member { get; }

		/// <summary>
		/// Gets the export info.
		/// </summary>
		public ExportInfo ExportInfo { get; }
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PartExportInfo"/> class. 
		/// </summary>
		public PartExportInfo(MemberInfo member, ExportInfo exportInfo)
		{
			this.Member = member;
			this.ExportInfo = exportInfo;
		}

		#endregion
	}
}
