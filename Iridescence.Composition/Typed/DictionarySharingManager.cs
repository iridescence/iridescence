﻿using System;
using System.Collections.Concurrent;

namespace Iridescence.Composition
{
	/// <summary>
	/// Implements a dictionary-based <see cref="ISharingManager"/>
	/// </summary>
	public class DictionarySharingManager : ISharingManager
	{
		private readonly ConcurrentDictionary<object, object> instances;

		/// <summary>
		/// Initializes a new instance of the <see cref="DictionarySharingManager"/> class. 
		/// </summary>
		public DictionarySharingManager()
		{
			this.instances = new ConcurrentDictionary<object, object>();
		}

		public object GetOrAdd(object key, Func<object> factory) => this.instances.GetOrAdd(key, _ => factory());
	}
}
