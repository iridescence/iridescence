﻿using System.Reflection;

namespace Iridescence.Composition
{
	internal interface IParameterProvider
	{
		object GetParameterValue(ParameterInfo parameter);
	}
}
