﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Composition
{
	internal sealed class ImportCallbackFeature : IActivationFeature
	{
		public static Action<object> CombineImportCallbacks(Type type, IEnumerable<MethodInfo> methods)
		{
			MethodInfo[] array = methods.ToArray();
			if (array.Length == 0)
				return null;

			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(new MethodSignature(typeof(void), typeof(object)));

			Local local = emit.DeclareLocal(type);
			emit.LoadArgument(0);
			emit.CastClass(type);
			emit.StoreLocal(local);

			foreach (MethodInfo method in array)
			{
				emit.LoadLocal(local);
				emit.CallVirtual(method);
			}

			emit.Return();

			return emit.CreateDelegate<Action<object>>();
		}

		public CompositeActivator Apply(in DiscoveredPartExportInfo export, CompositeActivator inner, ImmutableArray<Dependency> dependencies)
		{
			if (inner == null)
				return null;

			Action<object> callback = CombineImportCallbacks(export.PartType, export.Model.GetImportedCallbacks(export.Part.Type));
			if (callback == null)
				return inner;

			return (c, o) =>
			{
				object instance = inner(c, o);
				o.Completed += () => { callback(instance); };
				return instance;
			};
		}

		public IEnumerable<Dependency> GetDependencies(in DiscoveredPartExportInfo export, DependencyAccessor dependencyAccessor)
		{
			return Enumerable.Empty<Dependency>();
		}
	}
}