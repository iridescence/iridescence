﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	internal sealed class ImportFeature : IActivationFeature
	{
		public static IEnumerable<AssignableImportInfo> GetImports(ITypeModelProvider model, Type type, ExportInfo export)
		{
			return getImports(model, type, type, export);
		}

		private static IEnumerable<AssignableImportInfo> getImports(ITypeModelProvider model, Type declType, Type currentType, ExportInfo export)
		{
			Type baseType = currentType.BaseType;
			if (baseType != null && baseType != typeof(object))
			{
				foreach (AssignableImportInfo baseContract in getImports(model, declType, baseType, export))
				{
					yield return baseContract;
				}
			}

			HashSet<object> sites = new HashSet<object>();
			foreach (MemberInfo member in currentType.GetMembers(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly))
			{
				foreach (Attribute attr in model.GetMemberAttributes(declType, member))
				{
					if (!(attr is IImportAttribute importAttr))
						continue;

					Lazy<ExportInfo> exportLazy = new Lazy<ExportInfo>(() => export);
					foreach (AssignableImportInfo import in importAttr.GetImports(member, exportLazy).OfType<AssignableImportInfo>())
					{
						if (!sites.Add(import.Target))
							throw new CompositionException($"{import.Target} is targeted by more than one import.");

						yield return import;
					}
				}
			}
		}

		public CompositeActivator Apply(in DiscoveredPartExportInfo export, CompositeActivator inner, ImmutableArray<Dependency> dependencies)
		{
			if (inner == null)
				return null;

			return (c, o) =>
			{
				object instance = inner(c, o);
				o.Completing += () =>
				{
					foreach (Dependency dep in dependencies)
					{
						if (!(dep.Site is AssignableImportInfo import))
							continue;

						ExportDescriptor descriptor = dep.Promise.GetDescriptor();
						object importValue = descriptor.Activator(c, o);
						import.Set(instance, importValue);
					}
				};
				return instance;
			};
		}

		public IEnumerable<Dependency> GetDependencies(in DiscoveredPartExportInfo export, DependencyAccessor dependencyAccessor)
		{
			return GetImports(export.Model, export.PartType, export.ExportInfo).Resolve(dependencyAccessor);
		}
	}
}
