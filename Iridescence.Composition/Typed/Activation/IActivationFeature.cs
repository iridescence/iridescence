﻿using System.Collections.Generic;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	internal interface IActivationFeature
	{
		CompositeActivator Apply(in DiscoveredPartExportInfo export, CompositeActivator inner, ImmutableArray<Dependency> dependencies);

		IEnumerable<Dependency> GetDependencies(in DiscoveredPartExportInfo export, DependencyAccessor dependencyAccessor);
	}
}
