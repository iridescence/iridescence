﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Composition
{
	internal class ImportingConstructorFeature : IActivationFeature
	{
		private static readonly FieldInfo activatorField = typeof(ParameterValue).GetField(nameof(ParameterValue.Activator), BindingFlags.Instance | BindingFlags.Public) ?? throw new MissingFieldException(nameof(ParameterValue), nameof(ParameterValue.Activator));
		private static readonly FieldInfo valueField = typeof(ParameterValue).GetField(nameof(ParameterValue.Value), BindingFlags.Instance | BindingFlags.Public) ?? throw new MissingFieldException(nameof(ParameterValue), nameof(ParameterValue.Value));
		private static readonly MethodInfo activatorInvoke = typeof(CompositeActivator).GetInvokeMethod();

		private static IEnumerable<ImportInfo> getImports(ITypeModelProvider model, MethodBase method, ExportInfo export)
		{
			foreach (ParameterInfo parameter in method.GetParameters())
			{
				bool dependencyFromAttribute = false;
				foreach (IImportAttribute attr in model.GetParameterAttributes(parameter).OfType<IImportAttribute>())
				{
					Lazy<ExportInfo> exportLazy = new Lazy<ExportInfo>(() => export);
					foreach (ImportInfo import in attr.GetImports(parameter, exportLazy))
					{
						dependencyFromAttribute = true;
						yield return import;
					}
				}

				if (!dependencyFromAttribute)
				{
					yield return new ParameterImportInfo(new CompositionContract(parameter.ParameterType), parameter.IsOptional, parameter);
				}
			}
		}

		private struct ParameterValue
		{
			public CompositeActivator Activator;
			public object Value;
		}

		public CompositeActivator Apply(in DiscoveredPartExportInfo export, CompositeActivator inner, ImmutableArray<Dependency> dependencies)
		{
			if (inner != null)
				return inner;

			ConstructorInfo ctor = export.Model.GetImportingConstructor(export.PartType);
			if (ctor == null)
				return null;

			ParameterInfo[] parameters = ctor.GetParameters();
			ParameterValue[] values = new ParameterValue[parameters.Length];

			// Assign default values.
			foreach (ParameterInfo parameter in parameters)
			{
				if (parameter.HasDefaultValue)
				{
					values[parameter.Position].Value = parameter.DefaultValue;
				}
			}

			// Assign activators.
			foreach (Dependency dep in dependencies)
			{
				if (!(dep.Site is ParameterImportInfo paramImport))
					continue;

				values[paramImport.Parameter.Position].Activator = dep.Promise.GetDescriptor().Activator;
			}

			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(typeof(CompositeActivator)).WithThis(values.GetType()));
			Local activatorLocal = emit.DeclareLocal(typeof(CompositeActivator));

			for (int i = 0; i < parameters.Length; ++i)
			{
				// activator = this[i].Activator
				emit.LoadArgument(0);
				emit.LoadConstant(i);
				emit.LoadElementAddress(typeof(ParameterValue));
				emit.LoadField(activatorField);
				emit.StoreLocal(activatorLocal);

				// if (activator != null) activator(c,o);
				Label end = emit.DefineLabel("end");
				Label noActivatorLabel = emit.DefineLabel("noActivator");
				emit.LoadLocal(activatorLocal);
				emit.BranchIfFalse(noActivatorLabel);
				emit.LoadLocal(activatorLocal);
				emit.LoadArgument(1);
				emit.LoadArgument(2);
				emit.CallVirtual(activatorInvoke);
				emit.CastClass(parameters[i].ParameterType);
				emit.Branch(end);

				// this[i].Value
				emit.MarkLabel(noActivatorLabel);
				emit.LoadArgument(0);
				emit.LoadConstant(i);
				emit.LoadElementAddress(typeof(ParameterValue));
				emit.LoadField(valueField);
				emit.CastClass(parameters[i].ParameterType);

				emit.MarkLabel(end);
			}

			emit.NewObject(ctor);
			emit.Return();

			return emit.CreateDelegate<CompositeActivator>(values);
		}

		public IEnumerable<Dependency> GetDependencies(in DiscoveredPartExportInfo export, DependencyAccessor dependencyAccessor)
		{
			ConstructorInfo ctor = export.Part.Model.GetImportingConstructor(export.Part.Type);
			if (ctor == null)
				return Enumerable.Empty<Dependency>();

			return getImports(export.Part.Model, ctor, export.ExportInfo).Resolve(dependencyAccessor);
		}
	}
}