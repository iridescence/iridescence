﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Iridescence.Composition
{
	internal sealed class SharingFeature : IActivationFeature
	{
		public CompositeActivator Apply(in DiscoveredPartExportInfo export, CompositeActivator inner, ImmutableArray<Dependency> dependencies)
		{
			if (inner == null)
				return null;

			object key = export.Part;

			if (!export.IsShared)
			{
				return (c, o) => o.GetOrAdd(key, _ => inner(c, o));
			}

			ISharingManager sharingManager = export.SharingManager;

			return (c, o) =>
			{
				o.EnterSharingLock(sharingManager);
				return sharingManager.GetOrAdd(key, () => inner(c, o));
			};
		}

		public IEnumerable<Dependency> GetDependencies(in DiscoveredPartExportInfo export, DependencyAccessor dependencyAccessor)
		{
			return Enumerable.Empty<Dependency>();
		}
	}
}
