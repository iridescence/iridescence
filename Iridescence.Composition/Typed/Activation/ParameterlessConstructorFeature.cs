﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Composition
{
	internal class ParameterlessConstructorFeature : IActivationFeature
	{
		public CompositeActivator Apply(in DiscoveredPartExportInfo export, CompositeActivator inner, ImmutableArray<Dependency> dependencies)
		{
			if (inner != null)
				return inner;

			if (export.PartType.IsValueType)
			{
				// Return default(T)
				DynamicMethodBuilder emit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(typeof(CompositeActivator)));
				Local local = emit.DeclareLocal(export.Part.Type);
				emit.LoadLocalAddress(local);
				emit.InitializeObject(export.Part.Type);
				emit.LoadLocal(local);
				emit.Return();
				return emit.CreateDelegate<CompositeActivator>();
			}
			else
			{
				ConstructorInfo ctor = export.Part.Type.GetConstructor(Array.Empty<Type>());
				if (ctor == null)
					return null;

				// Invoke constructor.
				DynamicMethodBuilder emit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(typeof(CompositeActivator)));
				emit.NewObject(ctor);
				emit.Return();
				return emit.CreateDelegate<CompositeActivator>();
			}
		}

		public IEnumerable<Dependency> GetDependencies(in DiscoveredPartExportInfo export, DependencyAccessor dependencyAccessor)
		{
			return Enumerable.Empty<Dependency>();
		}
	}
}
