﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Iridescence.Composition
{
	internal sealed class DisposableFeature : IActivationFeature
	{
		public CompositeActivator Apply(in DiscoveredPartExportInfo export, CompositeActivator inner, ImmutableArray<Dependency> dependencies)
		{
			if (inner == null)
				return null;

			if (!typeof(IDisposable).IsAssignableFrom(export.PartType))
				return inner;

			return (c, o) =>
			{
				IDisposable instance = (IDisposable)inner(c, o);
				o.Completed += () => { c.AddDisposable(instance); };
				return instance;
			};
		}

		public IEnumerable<Dependency> GetDependencies(in DiscoveredPartExportInfo export, DependencyAccessor dependencyAccessor)
		{
			return Enumerable.Empty<Dependency>();
		}
	}
}
