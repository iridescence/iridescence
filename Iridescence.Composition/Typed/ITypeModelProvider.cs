﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides composition information for various members.
	/// </summary>
	public interface ITypeModelProvider
	{
		/// <summary>
		/// Returns the attributes of the specified type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		IEnumerable<Attribute> GetTypeAttributes(Type type);

		/// <summary>
		/// Returns the attributes of a parameter.
		/// </summary>
		/// <param name="parameter"></param>
		/// <returns></returns>
		IEnumerable<Attribute> GetParameterAttributes(ParameterInfo parameter);

		/// <summary>
		/// Returns the attributes of a member.
		/// </summary>
		/// <param name="type">The type that requests the member attributes. Can be the declaring type of the member or any derived type.</param>
		/// <param name="member">The member.</param>
		/// <returns></returns>
		IEnumerable<Attribute> GetMemberAttributes(Type type, MemberInfo member);

		/// <summary>
		/// Returns the importing constructor of the specified type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		ConstructorInfo GetImportingConstructor(Type type);

		/// <summary>
		/// Returns the methods that need to be invoked when the imports of the type are assigned.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		IEnumerable<MethodInfo> GetImportedCallbacks(Type type);
	}
}
