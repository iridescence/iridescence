﻿using System;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Builds an array of type T.
	/// </summary>
	internal class ArrayBuilder<T> : ICollectionBuilder
	{
		private List<T> list;
		
		public Type ItemType => typeof(T);

		public void Add(object value)
		{
			if (this.list == null)
				this.list = new List<T>();

			this.list.Add((T)value);
		}

		public object Build()
		{
			return this.list?.ToArray() ?? Array.Empty<T>();
		}
	}
}
