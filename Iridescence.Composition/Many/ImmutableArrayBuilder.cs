﻿using System;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	/// <summary>
	/// Builds an <see cref="ImmutableArray{T}" />
	/// </summary>
	internal class ImmutableArrayBuilder<T> : ICollectionBuilder
	{
		private ImmutableArray<T>.Builder builder;

		public Type ItemType => typeof(T);

		public void Add(object value)
		{
			if (this.builder == null)
				this.builder = ImmutableArray.CreateBuilder<T>();

			this.builder.Add((T)value);
		}

		public object Build()
		{
			return this.builder?.ToImmutable() ?? ImmutableArray<T>.Empty;
		}
	}
}