﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides a way to import all elements of collection.
	/// </summary>
	public class ImportManyExportProvider : IExportDescriptorProvider
	{	
		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			// Check if this is a "many" import.
			if (!contract.ExtractMetadata<ImportManyMetadata>(out contract).Any())
				yield break;

			// Check if we can build that collection.
			Func<ICollectionBuilder> builderFactory = CollectionBuilderFactory.GetFactory(contract.Type, out Type elementType);
			if (builderFactory == null)
				yield break;

			CompositionContract elementContract = contract.ChangeType(elementType);
			yield return new ExportDescriptorPromise(
				contract,
				$"{elementType.Name}[]",
				false,
				() => dependencyAccessor.ResolveDependencies("item", elementContract, DependencyFlags.Prerequisite),
				d =>
				{
					ExportDescriptor[] elements = d.Select(e => e.Promise.GetDescriptor()).ToArray();
					return ExportDescriptor.Create((ctx, op) =>
					{
						ICollectionBuilder builder = builderFactory();
						foreach (object value in elements.Select(e => e.Activator(ctx, op)))
						{
							builder.Add(value);
						}

						object obj = builder.Build();
						if (obj is IDisposable disposable)
							ctx.AddDisposable(disposable);

						return obj;
					});
				});
		}
	}
}
