﻿using System;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Builds an <see cref="List{T}"/>.
	/// </summary>
	internal class ListBuilder<T> : ICollectionBuilder
	{
		private readonly List<T> list;
		
		public Type ItemType => typeof(T);

		public ListBuilder()
		{
			this.list = new List<T>();
		}

		public void Add(object value)
		{
			this.list.Add((T)value);
		}

		public object Build() => this.list;
	}
}
