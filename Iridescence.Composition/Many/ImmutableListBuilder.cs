﻿using System;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	/// <summary>
	/// Builds an <see cref="ImmutableList{T}" />
	/// </summary>
	internal class ImmutableListBuilder<T> : ICollectionBuilder
	{
		private ImmutableList<T>.Builder builder;

		public Type ItemType => typeof(T);

		public void Add(object value)
		{
			if (this.builder == null)
				this.builder = ImmutableList.CreateBuilder<T>();

			this.builder.Add((T)value);
		}

		public object Build()
		{
			return this.builder?.ToImmutable() ?? ImmutableList<T>.Empty;
		}
	}
}