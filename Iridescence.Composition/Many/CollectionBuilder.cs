﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Builds an <see cref="ICollection"/>.
	/// </summary>
	internal class CollectionBuilder<T> : ICollectionBuilder
	{
		private readonly ICollection<T> collection;
		
		public Type ItemType => typeof(T);

		public CollectionBuilder(Type collectionType)
		{
			this.collection = (ICollection<T>)Activator.CreateInstance(collectionType);
		}
		
		public void Add(object value)
		{
			this.collection.Add((T)value);
		}

		public object Build() => this.collection;
	}
}
