﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Builds collections for "Many" imports.
	/// </summary>
	internal interface ICollectionBuilder
	{
		#region Properties
		
		/// <summary>
		/// Gets the collection item type.
		/// </summary>
		Type ItemType { get; }

		#endregion
		
		#region Methods
		
		/// <summary>
		/// Adds the specified item to the collection.
		/// </summary>
		/// <param name="value"></param>
		void Add(object value);

		/// <summary>
		/// Returns the completed collection.
		/// </summary>
		/// <returns></returns>
		object Build();

		#endregion
	}
}
