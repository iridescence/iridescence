﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	internal static class CollectionBuilderFactory
	{
		public static Func<ICollectionBuilder> GetFactory(Type collectionType, out Type elementType)
		{
			elementType = null;

			if (collectionType.IsSzArray())
			{
				Type arrayElementType = collectionType.GetElementType();
				elementType = arrayElementType;
				return () => (ICollectionBuilder)Activator.CreateInstance(typeof(ArrayBuilder<>).MakeGenericType(arrayElementType));
			}

			if (collectionType.IsConstructedGenericType)
			{
				Type def = collectionType.GetGenericTypeDefinition();

				if (def == typeof(IList<>) || def == typeof(IReadOnlyList<>) || def == typeof(ICollection<>) || def == typeof(IEnumerable<>))
				{
					Type genericArg = collectionType.GetGenericArguments()[0];
					elementType = genericArg;
					return () => (ICollectionBuilder)Activator.CreateInstance(typeof(ListBuilder<>).MakeGenericType(genericArg));
				}

				if (def == typeof(ImmutableArray<>))
				{
					Type genericArg = collectionType.GetGenericArguments()[0];
					elementType = genericArg;
					return () => (ICollectionBuilder)Activator.CreateInstance(typeof(ImmutableArrayBuilder<>).MakeGenericType(genericArg));
				}

				if (def == typeof(ImmutableList<>))
				{
					Type genericArg = collectionType.GetGenericArguments()[0];
					elementType = genericArg;
					return () => (ICollectionBuilder)Activator.CreateInstance(typeof(ImmutableListBuilder<>).MakeGenericType(genericArg));
				}
			}

			if (!collectionType.IsAbstract)
			{
				Type[] interfaces = collectionType.GetInterfaces();

				foreach (Type iface in interfaces)
				{
					if (iface.IsConstructedGenericType && iface.GetGenericTypeDefinition() == typeof(ICollection<>))
					{
						Type genericArg = collectionType.GetGenericArguments()[0];
						elementType = genericArg;
						return () => (ICollectionBuilder)Activator.CreateInstance(typeof(CollectionBuilder<>).MakeGenericType(genericArg), collectionType);
					}
				}
			}
			
			return null;
		}
	}
}