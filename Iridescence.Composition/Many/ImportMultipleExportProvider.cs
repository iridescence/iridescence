﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides a way to import multiple, distinctly named parts into one collection.
	/// </summary>
	public class ImportMultipleExportProvider : IExportDescriptorProvider
	{
		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			// Check if this is a "multi" import.
			IReadOnlyList<ImportMultipleMetadata> meta = contract.ExtractMetadata<ImportMultipleMetadata>(out CompositionContract singleContract);
			if (meta.Count == 0)
				yield break;

			// Check if we can build that collection.
			Func<ICollectionBuilder> builderFactory = CollectionBuilderFactory.GetFactory(singleContract.Type, out Type elementType);
			if (builderFactory == null)
				yield break;

			yield return new ExportDescriptorPromise(
				contract,
				$"{elementType.Name}[]",
				false,
				() => meta.SelectMany(m => m.Names).Select(name => dependencyAccessor.ResolveRequiredDependency(name, new CompositionContract(elementType, name, singleContract.Metadata), DependencyFlags.Prerequisite)),
				d =>
				{
					ExportDescriptor[] elements = d.Select(e => e.Promise.GetDescriptor()).ToArray();
					return ExportDescriptor.Create((ctx, op) =>
					{
						ICollectionBuilder builder = builderFactory();
						foreach (object value in elements.Select(e => e.Activator(ctx, op)))
						{
							builder.Add(value);
						}

						object obj = builder.Build();
						if (obj is IDisposable disposable)
							ctx.AddDisposable(disposable);

						return obj;
					});
				});
		}
	}
}
