﻿using System;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Builds a call to a method.
	/// </summary>
	public class CallBuilder
	{
		#region Fields

		private readonly ParameterValue[] values;
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the method to call.
		/// </summary>
		public MethodBase Method { get; }

		/// <summary>
		/// Gets a value that indicates whether all parameters have a value assigned.
		/// </summary>
		public bool IsComplete => this.values.All(v => v.IsSet);

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CallBuilder"/> class. 
		/// </summary>
		public CallBuilder(MethodBase method)
		{
			this.Method = method ?? throw new ArgumentNullException(nameof(method));

			ParameterInfo[] infos = method.GetParameters();
			this.values = new ParameterValue[infos.Length];

			foreach (ParameterInfo info in infos)
			{
				if (info.HasDefaultValue)
				{
					this.values[info.Position].IsSet = true;
					this.values[info.Position].Value = info.DefaultValue;
				}
			}
		}
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Sets the value of a parameter.
		/// </summary>
		/// <param name="parameter"></param>
		/// <param name="value"></param>
		public void SetValue(ParameterInfo parameter, object value)
		{
			if (parameter == null)
				throw new ArgumentNullException(nameof(parameter));

			if (parameter.Member != this.Method)
				throw new ArgumentException("Parameter does not belong to method.", nameof(parameter));

			this.values[parameter.Position].Value = value;
			this.values[parameter.Position].IsSet = true;
		}

		/// <summary>
		/// Invokes the method with the parameters from this builder.
		/// </summary>
		/// <param name="target">The target object to invoke the method on, if it is an instance method.</param>
		/// <returns>The return value of the method.</returns>
		public object Invoke(object target)
		{
			if (!this.IsComplete)
				throw new InvalidOperationException("Parameter list is not complete.");

			object[] args = new object[this.values.Length];
			for (int i = 0; i < this.values.Length; ++i)
				args[i] = this.values[i].Value;

			if (this.Method is ConstructorInfo constructor)
			{
				return constructor.Invoke(args);
			}

			return this.Method.Invoke(target, args);
		}

		#endregion

		#region Nested Types

		private struct ParameterValue
		{
			public bool IsSet;
			public object Value;
		}

		#endregion
	}
}
