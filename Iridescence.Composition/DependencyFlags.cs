﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Flags for dependencies.
	/// </summary>
	[Flags]
	public enum DependencyFlags
	{
		/// <summary>
		/// No flags.
		/// </summary>
		None = 0,

		/// <summary>
		/// Indicates that the dependency must be satisfied before corresponding exports can be retrieved.
		/// </summary>
		Prerequisite = 1,

		/// <summary>
		/// Indicates that the dependency should be hidden from the parent request.
		/// If two parts A and B match a contract, but A has a dependency on B with <see cref="Shadow"/> flag set, B will not appear in the results of the original request because A used it as a dependency.
		/// This flag is useful for building transparent proxies or caches for other parts.
		/// </summary>
		Shadow = 2,
	}
}
