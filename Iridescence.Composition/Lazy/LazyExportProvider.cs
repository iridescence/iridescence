﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides promises for <see cref="Lazy{T}"/> exports.
	/// </summary>
	public class LazyExportProvider : IExportDescriptorProvider
	{
		private static readonly MethodInfo method = typeof(LazyExportProvider).GetMethod(nameof(getExports), BindingFlags.Static | BindingFlags.NonPublic) ?? throw new MissingMethodException();

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyFactory)
		{
			Type type = contract.Type;
			if (!type.IsConstructedGenericType || type.GetGenericTypeDefinition() != typeof(Lazy<>))
				return Enumerable.Empty<ExportDescriptorPromise>();

			Type valueType = type.GetGenericArguments()[0];

			MethodInfo constructedMethod = method.MakeGenericMethod(valueType);
			Func<CompositionContract, DependencyAccessor, IEnumerable<ExportDescriptorPromise>> func = (Func<CompositionContract, DependencyAccessor, IEnumerable<ExportDescriptorPromise>>)constructedMethod.CreateDelegate(typeof(Func<CompositionContract, DependencyAccessor, IEnumerable<ExportDescriptorPromise>>));
			return func(contract, dependencyFactory);
		}

		private static IEnumerable<ExportDescriptorPromise> getExports<T>(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			return dependencyAccessor.ResolveDependencies("value", contract.ChangeType(typeof(T)))
				.Select(d => new ExportDescriptorPromise(
					contract,
					typeof(Lazy<T>).GetDisplayName(),
					false,
					() => EnumerableExtensions.Once(d),
					_ =>
					{
						ExportDescriptor descriptor = d.Promise.GetDescriptor();
						CompositeActivator activator = descriptor.Activator;
						return ExportDescriptor.Create((ctx, op) => new Lazy<T>(() => (T)CompositionOperation.Run(ctx, activator)), descriptor.Metadata);
					}));
		}
	}
}
