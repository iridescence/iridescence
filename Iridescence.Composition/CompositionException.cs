﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Composition
{
	/// <summary>
	/// The exception that is thrown when a composition fails.
	/// </summary>
	public class CompositionException : Exception
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		public CompositionException()
		{
		
		}

		public CompositionException(string message) : base(message)
		{
		}

		public CompositionException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected CompositionException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		#endregion

		#region Methods

		internal static CompositionException NotFound(CompositionContract contract)
		{
			return new CompositionException($"No export found for contract \"{contract}\"");
		}

		internal static CompositionException TooManyExports(CompositionContract contract)
		{
			return new CompositionException($"More than one export matches contract \"{contract}\"");
		}

		#endregion
	}
}
