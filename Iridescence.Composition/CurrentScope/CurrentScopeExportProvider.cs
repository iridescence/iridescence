﻿using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides the current <see cref="CompositionContext"/>.
	/// </summary>
	public class CurrentScopeExportDescriptorProvider : IExportDescriptorProvider
	{
		private static readonly CompositionContract exportedContract = new CompositionContract(typeof(CompositionContext));

		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			if (!exportedContract.Equals(contract))
				yield break;

			yield return new ExportDescriptorPromise(
				contract,
				nameof(CompositionContext),
				true,
				Dependency.NoDependencies,
				_ => { return ExportDescriptor.Create((c, o) => c); });
		}
	}
}
