﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Promises an <see cref="ExportDescriptor"/> for a specific <see cref="CompositionContract"/>.
	/// </summary>
	public class ExportDescriptorPromise : ExportDescriptorPromiseInfo
	{
		#region Fields

		private readonly Lazy<ExportDescriptor> descriptor;

		private bool isCreating;

		#endregion 
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ExportDescriptorPromise"/> class. 
		/// </summary>
		public ExportDescriptorPromise(CompositionContract contract, string origin, bool isShared, DependencyEnumerator getDependencies, ExportDescriptorCreator createDescriptor)
			: base(contract, origin, isShared, getDependencies)
		{
			this.descriptor = new Lazy<ExportDescriptor>(() => createDescriptor(this.Dependencies));
		}

		internal ExportDescriptorPromise(ExportDescriptorPromise promise, ExportDescriptorCreator createDescriptor)
			: base(promise)
		{
			this.descriptor = new Lazy<ExportDescriptor>(() => createDescriptor(this.Dependencies));
		}

		#endregion

		#region Methods

		/// <summary>
		/// Retrieves the promised <see cref="ExportDescriptor"/>.
		/// </summary>
		/// <returns></returns>
		public ExportDescriptor GetDescriptor()
		{
			if (this.isCreating && !this.descriptor.IsValueCreated)
			{
				return new CycleBreakingExportDescriptor(this.descriptor);
			}

			this.isCreating = true;
			try
			{
				ExportDescriptor descriptor = this.descriptor.Value;
				if (descriptor == null)
					throw new InvalidOperationException("A promised descriptor may not be null.");
				return descriptor;
			}
			finally
			{
				this.isCreating = false;
			}
		}

		#endregion
	}
}
