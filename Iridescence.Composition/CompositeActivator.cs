﻿namespace Iridescence.Composition
{
	/// <summary>
	/// Creates an instance of a part and returns it.
	/// </summary>
	/// <param name="context">The <see cref="CompositionContext"/> that owns the activated instance.</param>
	/// <param name="operation">The composition operation for the whole graph.</param>
	public delegate object CompositeActivator(CompositionContext context, CompositionOperation operation);
}
