﻿using System;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Contains the <see cref="ExportDescriptor"/> instances and promise providers of a transaction.
	/// </summary>
	internal sealed class TransactionLayer
	{
		#region Fields

		private bool closed;
		private readonly Dictionary<ExportKey, DescriptorCacheEntry> currentDescriptors;
		private readonly Dictionary<ExportKey, DescriptorCacheEntry> newDescriptors;

		#endregion

		#region Properties
		
		public IReadOnlyList<IExportDescriptorProvider> Providers { get; }

		public Func<ExportDescriptorPromise, ExportDescriptor> GetDescriptor { get; }

		#endregion

		#region Constructors

		public TransactionLayer(
			IReadOnlyList<IExportDescriptorProvider> providers,
			Dictionary<ExportKey, DescriptorCacheEntry> descriptors, 
			Func<ExportDescriptorPromise, ExportDescriptor> getDescriptor)
		{
			this.Providers = providers;
			this.currentDescriptors = descriptors;
			this.newDescriptors = new Dictionary<ExportKey, DescriptorCacheEntry>();
			this.GetDescriptor = getDescriptor;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the descriptors for the specified key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="descriptors"></param>
		/// <returns></returns>
		public bool TryGetValue(ExportKey key, out DescriptorCacheEntry descriptors)
		{
			if (this.currentDescriptors.TryGetValue(key, out descriptors))
				return true;

			if (this.newDescriptors.TryGetValue(key, out descriptors))
				return true;

			return false;
		}

		/// <summary>
		/// Adds a bunch of descriptors for the specified key.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="descriptors"></param>
		public void Add(ExportKey key, DescriptorCacheEntry descriptors)
		{
			if (this.closed)
				throw new TransactionCompletedException();

			this.newDescriptors.Add(key, descriptors);
		}

		/// <summary>
		/// Commits the added descriptors.
		/// </summary>
		public void Commit()
		{
			if (this.closed)
				throw new TransactionCompletedException();

			this.closed = true;

			foreach (KeyValuePair<ExportKey, DescriptorCacheEntry> pair in this.newDescriptors)
			{
				this.currentDescriptors.Add(pair.Key, pair.Value);
			}

			this.newDescriptors.Clear();
		}

		#endregion
	}
}
