﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Iridescence.Composition
{
	/// <summary>
	/// Creates <see cref="Dependency">dependencies</see>.
	/// </summary>
	public abstract class DependencyAccessor
	{
		#region Methods

		/// <summary>
		/// Returns the promises for the specified import contract.
		/// </summary>
		/// <param name="query"></param>
		/// <returns></returns>
		public abstract IEnumerable<ExportDescriptorPromise> GetPromises(DependencyQuery query);

		/// <summary>
		/// Resolve dependencies on all implementations of a contract.
		/// </summary>
		/// <param name="site">A tag describing the dependency site.</param>
		/// <param name="contract">The contract required by the site.</param>
		/// <param name="flags">The dependency flags.</param>
		/// <returns>Dependencies for all implementations of the contact.</returns>
		public ImmutableArray<Dependency> ResolveDependencies(object site, CompositionContract contract, DependencyFlags flags = DependencyFlags.None)
		{
			IEnumerable<ExportDescriptorPromise> promises = this.GetPromises(new DependencyQuery(this, contract, (flags & DependencyFlags.Shadow) != 0));

			var result = ImmutableArray.CreateBuilder<Dependency>();

			foreach (ExportDescriptorPromise promise in promises)
			{
				result.Add(Dependency.Satisfied(contract, site, promise, flags));
			}

			return result.ToImmutable();
		}

		/// <summary>
		/// Resolve a required dependency on exactly one implementation of a contract.
		/// </summary>
		/// <param name="site">A tag describing the dependency site.</param>
		/// <param name="contract">The contract required by the site.</param>
		/// <param name="flags">The dependency flags.</param>
		/// <returns>The dependency.</returns>
		public Dependency ResolveRequiredDependency(object site, CompositionContract contract, DependencyFlags flags = DependencyFlags.None)
		{
			if (!this.TryResolveOptionalDependency(site, contract, flags, out Dependency result))
				return Dependency.Missing(contract, site);

			return result;
		}
		
		/// <summary>
		/// Resolve an optional dependency on exactly one implementation of a contract.
		/// </summary>
		/// <param name="site">A tag describing the dependency site.</param>
		/// <param name="contract">The contract required by the site.</param>
		/// <param name="flags">The dependency flags.</param>
		/// <param name="dependency">The dependency, or null.</param>
		/// <returns>True if the dependency could be resolved; otherwise, false.</returns>
		public bool TryResolveOptionalDependency(object site, CompositionContract contract, DependencyFlags flags, out Dependency dependency)
		{
			ExportDescriptorPromise[] all = this.GetPromises(new DependencyQuery(this, contract, (flags & DependencyFlags.Shadow) != 0)).ToArray();
			if (all.Length == 0)
			{
				dependency = null;
				return false;
			}

			if (all.Length != 1)
			{
				dependency = Dependency.Oversupplied(contract, site, all);
				return true;
			}

			dependency = Dependency.Satisfied(contract, site, all[0], flags);
			return true;
		}

		#endregion
	}
}
