﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents the transaction entry for a specific <see cref="CompositionContract"/> in a <see cref="Transaction"/>.
	/// </summary>
	internal sealed class TransactionEntry
	{
		#region Fields

		private readonly IReadOnlyList<TransactionLayer> layers;
		private int currentLayer;
		private int currentProvider;
		private bool done;
		private readonly List<EntryPromise> promises;
		
		internal int RecursionCounter;
		
		#endregion
		
		#region Properties

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TransactionEntry"/> class. 
		/// </summary>
		public TransactionEntry(IReadOnlyList<TransactionLayer> layers)
		{
			this.layers = layers;
			this.promises = new List<EntryPromise>();
		}
		
		#endregion
		
		#region Methods

		public IEnumerable<Provider> GetProviders()
		{
			while(!this.done)
			{
				if (this.currentProvider >= this.layers[this.currentLayer].Providers.Count)
				{
					this.currentProvider = 0;
					++this.currentLayer;
					if (this.currentLayer >= this.layers.Count)
					{
						this.done = true;
						yield break;
					}

					continue;
				}

				yield return new Provider(this, this.layers[this.currentLayer], this.layers[this.currentLayer].Providers[this.currentProvider++]);
			}
		}

		public IEnumerable<(TransactionLayer, ImmutableArray<ExportDescriptor>)> GetDescriptors(int? requestId)
		{
			if (!this.done)
				throw new InvalidOperationException("Transation entry is not done enumerating providers. Descriptors can not be returned yet.");

			foreach (TransactionLayer layer in this.layers)
			{
				yield return (layer,
					this.promises
						.Where(p => p.RequestId == null || p.RequestId == requestId)
						.Where(p => p.Layer == layer)
						.Select(p => layer.GetDescriptor?.Invoke(p.Promise) ?? p.Promise.GetDescriptor())
						.ToImmutableArray());
			}
		}
		
		public IEnumerable<ExportDescriptorPromise> GetPromises(int? requestId)
		{
			if (!this.done)
				throw new InvalidOperationException("Transation entry is not done enumerating providers. Promises can not be returned yet.");

			return this.promises
				.Where(p => p.RequestId == null || p.RequestId == requestId)
				.Select(t =>
				{
					TransactionLayer layer = t.Layer;
					ExportDescriptorPromise promise = t.Promise;
					if (layer.GetDescriptor == null)
						return promise;

					return new ExportDescriptorPromise(promise, _ => layer.GetDescriptor(promise));
				});
		}

		#endregion

		#region Nested Types

		private readonly struct EntryPromise
		{
			public readonly TransactionLayer Layer;
			public readonly ExportDescriptorPromise Promise;
			public readonly int? RequestId;

			public EntryPromise(TransactionLayer layer, ExportDescriptorPromise promise, int? requestId)
			{
				this.Layer = layer;
				this.Promise = promise;
				this.RequestId = requestId;
			}
		}

		public struct Provider
		{
			private readonly TransactionEntry entry;
			private readonly TransactionLayer layer;
			public IExportDescriptorProvider PromiseProvider { get; }

			public Provider(TransactionEntry entry, TransactionLayer layer, IExportDescriptorProvider provider)
			{
				this.entry = entry;
				this.layer = layer;
				this.PromiseProvider = provider;
			}

			public void AddPromise(ExportDescriptorPromise promise, int? requestId)
			{
				this.entry.promises.Add(new EntryPromise(this.layer, promise, requestId));
			}
		}

		#endregion
	}
}
