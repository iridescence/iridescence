﻿using System;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents a query for a dependency promise.
	/// </summary>
	public readonly struct DependencyQuery
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the original <see cref="DependencyAccessor"/> that generated the query.
		/// </summary>
		public DependencyAccessor Accessor { get; }

		/// <summary>
		/// Gets the contract of the dependency.
		/// </summary>
		public CompositionContract Contract { get; }

		/// <summary>
		/// Gets a value that indicates whether the resulting promise should be hidden from all further queries.
		/// </summary>
		public bool Shadow { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DependencyQuery"/> class. 
		/// </summary>
		public DependencyQuery(DependencyAccessor accessor, CompositionContract contract, bool shadow = false)
		{
			this.Accessor = accessor ?? throw new ArgumentNullException(nameof(accessor));
			this.Contract = contract ?? throw new ArgumentNullException(nameof(contract));
			this.Shadow = shadow;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the promises from a provider.
		/// </summary>
		/// <param name="provider"></param>
		/// <returns></returns>
		public IEnumerable<ExportDescriptorPromise> GetPromises(IExportDescriptorProvider provider)
		{
			return provider.GetPromises(this.Contract, this.Accessor);
		}

		#endregion
	}
}
