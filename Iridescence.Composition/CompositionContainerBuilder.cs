﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Helper class to build <see cref="CompositionContainer">containers</see>.
	/// </summary>
	public class CompositionContainerBuilder
	{
		#region Fields

		private ImmutableList<Type> types;
		private ImmutableList<(CompositionContract, object)> instances;
		private ImmutableList<IExportDescriptorProvider> providers;
		private ImmutableList<IDisposable> disposableObjects;
		
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContainerBuilder"/> class. 
		/// </summary>
		private CompositionContainerBuilder(CompositionContainerBuilder other)
		{
			this.types = other.types;
			this.instances = other.instances;
			this.providers = other.providers;
			this.disposableObjects = other.disposableObjects;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContainerBuilder"/> class. 
		/// </summary>
		public CompositionContainerBuilder()
		{
			this.types = ImmutableList<Type>.Empty;
			this.instances = ImmutableList<(CompositionContract, object)>.Empty;
			this.providers = ImmutableList<IExportDescriptorProvider>.Empty;
			this.disposableObjects = ImmutableList<IDisposable>.Empty;
		}
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Adds the specified type to the container. A <see cref="TypedPartExportProvider"/> will be added that provides the promises for this type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public CompositionContainerBuilder WithPart(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			return new CompositionContainerBuilder(this)
			{
				types = this.types.Add(type)
			};
		}

		/// <summary>
		/// Adds the specified types to the container. A <see cref="TypedPartExportProvider"/> will be added that provides the promises for these types.
		/// </summary>
		/// <param name="types"></param>
		/// <returns></returns>
		public CompositionContainerBuilder WithParts(params Type[] types)
		{
			if (types == null)
				throw new ArgumentNullException(nameof(types));

			if (types.Any(t => t == null))
				throw new ArgumentNullException(nameof(types));

			return new CompositionContainerBuilder(this)
			{
				types = this.types.AddRange(types)
			};
		}

		/// <summary>
		/// Adds the specified object instance to the container for the specified contract. An <see cref="InstanceExportProvider"/> will be added that provides the promises for this type.
		/// </summary>
		/// <param name="contract">The contract for the object instance.</param>
		/// <param name="instance">The object instance.</param>
		/// <returns></returns>
		public CompositionContainerBuilder WithInstance(CompositionContract contract, object instance)
		{
			if (contract == null)
				throw new ArgumentNullException(nameof(contract));

			return new CompositionContainerBuilder(this)
			{
				instances = this.instances.Add((contract, instance))
			};
		}
		
		/// <summary>
		/// Adds the specified <see cref="IExportDescriptorProvider"/> to the container.
		/// </summary>
		/// <param name="provider"></param>
		/// <returns></returns>
		public CompositionContainerBuilder WithProvider(IExportDescriptorProvider provider)
		{
			if (provider == null)
				throw new ArgumentNullException(nameof(provider));

			return new CompositionContainerBuilder(this)
			{
				providers = this.providers.Add(provider)
			};
		}

		/// <summary>
		/// Inserts the specified <see cref="IExportDescriptorProvider"/> to the container.
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="index"></param>
		/// <returns></returns>
		public CompositionContainerBuilder WithProvider(IExportDescriptorProvider provider, int index)
		{
			if (provider == null)
				throw new ArgumentNullException(nameof(provider));

			return new CompositionContainerBuilder(this)
			{
				providers = this.providers.Insert(index, provider)
			};
		}

		/// <summary>
		/// Adds a <see cref="TypedPartExportProvider"/> for the specified assembly.
		/// </summary>
		/// <param name="assembly"></param>
		/// <returns></returns>
		public CompositionContainerBuilder WithAssembly(Assembly assembly)
		{
			return this.WithProvider(new TypedPartExportProvider(assembly));
		}

		/// <summary>
		/// Binds the specified object to the composition container, meaning it will be disposed when the container is disposed.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public CompositionContainerBuilder WithDisposable(IDisposable obj)
		{
			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			return new CompositionContainerBuilder(this)
			{
				disposableObjects = this.disposableObjects.Add(obj)
			};
		}

		/// <summary>
		/// Returns the baked <see cref="IExportDescriptorProvider"/> from the builder.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<IExportDescriptorProvider> GetProviders()
		{
			List<IExportDescriptorProvider> bakedProviders = new(this.providers);

			HashSet<Type> bakedTypes = new(this.types);
			if (bakedTypes.Count > 0)
				bakedProviders.Add(new TypedPartExportProvider(bakedTypes));

			foreach ((CompositionContract contract, object instance) in this.instances)
			{
				bakedProviders.Add(new InstanceExportProvider(instance, contract));
			}

			return bakedProviders;
		}

		/// <summary>
		/// Builds the <see cref="CompositionContainer"/>.
		/// </summary>
		/// <returns></returns>
		public CompositionContainer Build(CompositionContainer baseContainer = null)
		{
			CompositionContainer container = new CompositionContainer(baseContainer, this.GetProviders());

			foreach (IDisposable obj in this.disposableObjects)
			{
				container.AddDisposable(obj);
			}

			return container;
		}

		#endregion
	}
}
