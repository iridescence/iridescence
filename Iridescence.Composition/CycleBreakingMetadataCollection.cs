﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	internal class CycleBreakingMetadataCollection : IReadOnlyList<CompositionMetadata>
	{
		#region Fields

		private readonly Lazy<ExportDescriptor> exportDescriptor;
		
		#endregion
		
		#region Properties

		private IReadOnlyList<CompositionMetadata> actualMetadata
		{
			get
			{
				if (!this.exportDescriptor.IsValueCreated)
					throw new NotSupportedException();

				return this.exportDescriptor.Value.Metadata;
			}
		}
		
		public int Count => this.actualMetadata.Count;

		public CompositionMetadata this[int index] => this.actualMetadata[index];

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CycleBreakingMetadataCollection"/> class. 
		/// </summary>
		public CycleBreakingMetadataCollection(Lazy<ExportDescriptor> exportDescriptor)
		{
			this.exportDescriptor = exportDescriptor;
		}
		
		#endregion
		
		#region Methods
		
		public IEnumerator<CompositionMetadata> GetEnumerator() => this.actualMetadata.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		#endregion
	}
}
