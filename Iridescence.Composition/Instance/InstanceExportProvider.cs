﻿using System;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides a single object instance as an export.
	/// </summary>
	public class InstanceExportProvider : IExportDescriptorProvider
	{
		#region Fields

		private readonly object instance;
		private readonly string origin;
		private readonly Predicate<CompositionContract> contractPredicate;

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="InstanceExportProvider"/> class. 
		/// </summary>
		public InstanceExportProvider(object instance, CompositionContract contract, string origin = null)
		{
			if (contract == null) 
				throw new ArgumentNullException(nameof(contract));
			
			this.contractPredicate = c => c.Equals(contract);
			this.instance = instance;
			this.origin = origin ?? instance?.ToString() ?? "Singleton";
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="InstanceExportProvider"/> class. 
		/// </summary>
		public InstanceExportProvider(object instance, Predicate<CompositionContract> contractPredicate, string origin = null)
		{
			this.contractPredicate = contractPredicate ?? throw new ArgumentNullException(nameof(contractPredicate));
			this.instance = instance;
			this.origin = origin ?? instance?.ToString() ?? "Singleton";
		}

		#endregion
		
		#region Methods
		
		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			if (this.contractPredicate(contract))
			{
				yield return new ExportDescriptorPromise(contract, this.origin, true, Dependency.NoDependencies, _ => ExportDescriptor.Create((c, o) => this.instance));
			}
		}
		
		#endregion
	}
}
