﻿using System.Collections.Immutable;

namespace Iridescence.Composition
{
	/// <summary>
	/// Retrieves the <see cref="ExportDescriptor"/> from a <see cref="ExportDescriptorPromise"/>.
	/// </summary>
	/// <param name="dependencies">The dependencies of the export.</param>
	public delegate ExportDescriptor ExportDescriptorCreator(ImmutableArray<Dependency> dependencies);
}
