﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Iridescence.Composition
{
	/// <summary>
	/// Exception that is thrown when there are missing or too many matches for dependencies.
	/// </summary>
	public class DependencyException : CompositionException
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the missing dependencies.
		/// </summary>
		public IReadOnlyList<Dependency> Dependencies { get; } = EmptyReadOnlyList<Dependency>.Instance;

		#endregion

		#region Constructors

		public DependencyException()
		{
		}

		public DependencyException(string message) 
			: base(message)
		{
		}

		public DependencyException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		public DependencyException(string message, IEnumerable<Dependency> missingDependencies)
			: base(message)
		{
			this.Dependencies = missingDependencies.ToReadOnlyList();
		}

		protected DependencyException(SerializationInfo info, StreamingContext context) 
			: base(info, context)
		{
		}

		#endregion

		#region Methods

		#endregion
	}
}
