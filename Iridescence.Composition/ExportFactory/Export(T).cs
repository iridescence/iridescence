﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents an export that allows the graph of parts associated with it to be disposed.
	/// </summary>
	[Serializable]
	public sealed class Export<T> : IDisposable
	{
		#region Fields

		private readonly Action dispose;

		#endregion

		#region Properties
		
		/// <summary>
		/// Gets the exported instance.
		/// </summary>
		public T Value { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Export{T}"/> class. 
		/// </summary>
		public Export(T value, Action dispose)
		{
			this.Value = value;
			this.dispose = dispose;
		}
		
		#endregion
		
		#region Methods

		public void Dispose()
		{
			this.dispose?.Invoke();
		}
		
		#endregion
	}
}
