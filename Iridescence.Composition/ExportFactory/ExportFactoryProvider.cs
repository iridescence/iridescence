﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides <see cref="ExportFactory{T}"/> for any part.
	/// </summary>
	public class ExportFactoryProvider : IExportDescriptorProvider
	{
		#region Fields

		private static readonly MethodInfo getPromisesMethod = typeof(ExportFactoryProvider).GetMethod(nameof(getPromises), BindingFlags.Static | BindingFlags.NonPublic);

		private delegate IEnumerable<ExportDescriptorPromise> GetPromisesFunc(CompositionContract contract, DependencyAccessor dependencyAccessor);

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ExportFactoryProvider"/> class. 
		/// </summary>
		public ExportFactoryProvider()
		{
		
		}
		
		#endregion
		
		#region Methods
		
		public IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			if (!contract.Type.IsConstructedGenericType || contract.Type.GetGenericTypeDefinition() != typeof(ExportFactory<>))
				return Enumerable.Empty<ExportDescriptorPromise>();

			GetPromisesFunc func = (GetPromisesFunc)getPromisesMethod.MakeGenericMethod(contract.Type.GetGenericArguments()[0]).CreateDelegate(typeof(GetPromisesFunc));
			return func(contract, dependencyAccessor);
		}

		private static IEnumerable<ExportDescriptorPromise> getPromises<T>(CompositionContract contract, DependencyAccessor dependencyAccessor)
		{
			CompositionContract productContract = contract.ChangeType(typeof(T));

			return dependencyAccessor.ResolveDependencies("product", productContract)
				.Select(d => new ExportDescriptorPromise(
					contract,
					typeof(ExportFactory<T>).GetDisplayName(),
					false,
					() => EnumerableExtensions.Once(d),
					_ =>
					{
						ExportDescriptor descriptor = d.Promise.GetDescriptor();
						CompositeActivator activator = descriptor.Activator;
						return ExportDescriptor.Create((c, o) =>
						{
							return new ExportFactory<T>(() =>
							{
								LifetimeContext context = new LifetimeContext(c);
								return ((T)CompositionOperation.Run(context, activator), context.Dispose);
							});
						});
					}));
		}

		#endregion
	}
}
