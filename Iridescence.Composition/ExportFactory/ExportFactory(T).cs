﻿using System;

namespace Iridescence.Composition
{
	/// <summary>
	/// Implements a factory for exports which allows consumers to create multiple instances of a part on demand.
	/// </summary>
	[Serializable]
	public class ExportFactory<T>
	{
		#region Fields

		private readonly Func<(T, Action)> createExport;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ExportFactory{T}"/> class. 
		/// </summary>
		public ExportFactory(Func<(T instance, Action onDispose)> createExport)
		{
			this.createExport = createExport ?? throw new ArgumentNullException(nameof(createExport));
		}

		#endregion
		
		#region Methods

		public Export<T> Create()
		{
			(T instance, Action onDispose) = this.createExport();
			return new Export<T>(instance, onDispose);
		}

		#endregion
	}
}
