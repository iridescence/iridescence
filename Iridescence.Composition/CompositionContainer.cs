﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Reflection;

namespace Iridescence.Composition
{
	/// <summary>
	/// Implements an <see cref="CompositionContext"/> that activates parts from export descriptors provided by a <see cref="IExportDescriptorProvider"/>.
	/// </summary>
	public class CompositionContainer : CompositionContext
	{
		#region Fields

		private static readonly MethodInfo getDescriptorMethod = typeof(CompositionContainer).GetMethod(nameof(GetDescriptor), BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		private readonly DescriptorCache cache;

		#endregion

		#region Properties

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContainer"/> class. 
		/// </summary>
		public CompositionContainer(CompositionContainer parent, IEnumerable<IExportDescriptorProvider> descriptorProviders = null)
			: this(parent, descriptorProviders?.ToImmutableArray() ?? ImmutableArray<IExportDescriptorProvider>.Empty)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContainer"/> class. 
		/// </summary>
		public CompositionContainer(CompositionContainer parent, ImmutableArray<IExportDescriptorProvider> descriptorProviders)
		{
			ChainedDescriptorFunc getDescriptor = null;
			
			Type me = this.GetType();
			if (me != typeof(CompositionContainer) && me.OverridesMethod(getDescriptorMethod))
				getDescriptor = this.GetDescriptor;

			this.cache = new DescriptorCache(parent?.cache, descriptorProviders, getDescriptor);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContainer"/> class. 
		/// </summary>
		public CompositionContainer(IEnumerable<IExportDescriptorProvider> descriptorProviders)
			: this(null, descriptorProviders)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContainer"/> class. 
		/// </summary>
		public CompositionContainer(CompositionContainer parent, params IExportDescriptorProvider[] descriptorProviders)
			: this(parent, (IEnumerable<IExportDescriptorProvider>)descriptorProviders)
		{

		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CompositionContainer"/> class. 
		/// </summary>
		public CompositionContainer(params IExportDescriptorProvider[] descriptorProviders)
			: this((IEnumerable<IExportDescriptorProvider>)descriptorProviders)
		{

		}

		#endregion
		
		#region Methods

		protected virtual ExportDescriptor GetDescriptor(ExportDescriptorPromiseInfo promise, Func<ExportDescriptor> getDescriptor)
		{
			return getDescriptor();
		}

		public override IEnumerable<ExportDescriptor> GetExportDescriptors(CompositionContract contract)
		{
			if (contract == null)
				throw new ArgumentNullException(nameof(contract));

			return this.cache.GetDescriptors(this, contract);
		}
				
		#endregion
	}
}
