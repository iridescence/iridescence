﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents a dependency of a composite part.
	/// </summary>
	public class Dependency
	{
		#region Fields

		private readonly IEnumerable<ExportDescriptorPromise> oversuppliedPromises;

		#endregion
		
		#region Properties
		
		/// <summary>
		/// Gets the dependency contract.
		/// </summary>
		public CompositionContract Contract { get; }

		/// <summary>
		/// Gets an object that identifies the dependency.
		/// </summary>
		public object Site { get; }

		/// <summary>
		/// Gets the <see cref="ExportDescriptorPromise"/> that was promised for the dependency.
		/// </summary>
		public ExportDescriptorPromise Promise { get; }

		/// <summary>
		/// Gets the dependency flags.
		/// </summary>
		public DependencyFlags Flags { get; }

		/// <summary>
		/// Gets a value that indicates whether the dependency could not be properly resolved.
		/// </summary>
		internal bool IsError => this.Promise == null;

		/// <summary>
		/// A <see cref="DependencyEnumerator"/> that yields no dependencies.
		/// </summary>
		public static DependencyEnumerator NoDependencies { get; }= Enumerable.Empty<Dependency>;

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Dependency"/> class. 
		/// </summary>
		private Dependency(CompositionContract contract, object site, ExportDescriptorPromise promise, DependencyFlags flags, IEnumerable<ExportDescriptorPromise> oversuppliedPromises)
		{
			this.Contract = contract;
			this.Site = site;
			this.Promise = promise;
			this.Flags = flags;
			this.oversuppliedPromises = oversuppliedPromises;
		}
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Returns a satisfied <see cref="Dependency"/> for the specified <see cref="CompositionContract"/>.
		/// </summary>
		/// <param name="contract">The contract of the dependency.</param>
		/// <param name="site">The site that identifies this dependency on the part.</param>
		/// <param name="promise">The promised export descriptor for the contract.</param>
		/// <param name="flags">The dependency flags.</param>
		/// <returns></returns>
		public static Dependency Satisfied(CompositionContract contract, object site, ExportDescriptorPromise promise, DependencyFlags flags)
		{
			return new Dependency(contract, site, promise, flags, null);
		}
		
		/// <summary>
		/// Returns a missing <see cref="Dependency"/> for the specified <see cref="CompositionContract"/>.
		/// </summary>
		/// <param name="contract">The contract of the dependency.</param>
		/// <param name="site">The site that identifies this dependency on the part.</param>
		/// <returns></returns>
		public static Dependency Missing(CompositionContract contract, object site)
		{
			return new Dependency(contract, site, null, DependencyFlags.None, null);
		}

		/// <summary>
		/// Returns an oversupplied <see cref="Dependency"/> for the specified <see cref="CompositionContract"/>.
		/// </summary>
		/// <param name="contract">The contract of the dependency.</param>
		/// <param name="site">The site that identifies this dependency on the part.</param>
		/// <param name="promises">The promises returned for the contract.</param>
		/// <returns></returns>
		public static Dependency Oversupplied(CompositionContract contract, object site, IEnumerable<ExportDescriptorPromise> promises)
		{
			return new Dependency(contract, site, null, DependencyFlags.None, promises);
		}

		internal void AppendError(StringBuilder str)
		{
			if(!this.IsError)
				throw new InvalidOperationException("Dependency is not in error state.");

			if (this.oversuppliedPromises != null)
			{
				str.Append($"More than one part matches the dependency {this}:");
				foreach (ExportDescriptorPromise promise in this.oversuppliedPromises)
				{
					str.AppendLine();
					str.Append($" - {promise}");
				}
			}
			else
			{
				str.Append($"Missing dependency {this}.");
			}
		}

		public override string ToString()
		{
			return $"\"{this.Contract}\" for \"{this.Site}\"";
		}

		#endregion
	}
}
