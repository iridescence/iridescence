﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents a method to retrieve the <see cref="ExportDescriptor"/> from a <see cref="ExportDescriptorPromise"/>.
	/// Can be used to customize the descriptor.
	/// </summary>
	/// <param name="promise">The <see cref="ExportDescriptorPromise"/> with the original GetDescriptor method.</param>
	/// <param name="getDescriptor">The next GetDescriptor override in the chain.</param>
	/// <returns>A patched export descriptor, or just the result of the previous (or original) GetDescriptor method.</returns>
	public delegate ExportDescriptor ChainedDescriptorFunc(ExportDescriptorPromiseInfo promise, Func<ExportDescriptor> getDescriptor);

	/// <summary>
	/// Caches the <see cref="ExportDescriptor">export descriptors</see> that were promised for a contract.
	/// </summary>
	internal sealed class DescriptorCache
	{
		#region Fields

		private readonly DescriptorCache parent;
		private readonly object cacheLock;

		private readonly ImmutableArray<IExportDescriptorProvider> providers;
		private readonly Dictionary<ExportKey, DescriptorCacheEntry> cache;
		private readonly Func<ExportDescriptorPromise, ExportDescriptor> getDescriptor;
		private readonly int depth;

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DescriptorCache"/> class. 
		/// </summary>
		public DescriptorCache(DescriptorCache parent, ImmutableArray<IExportDescriptorProvider> providers, ChainedDescriptorFunc getDescriptor)
		{
			this.providers = providers;
			if (this.providers.Any(p => p == null))
				throw new ArgumentException("Provider list must not contain null.", nameof(providers));

			this.parent = parent;
			this.cache = new Dictionary<ExportKey, DescriptorCacheEntry>();

			DescriptorCache root;
			DescriptorCache current = this;
			for (;;)
			{
				++this.depth;

				if (current.parent == null)
				{
					root = current;
					break;
				}

				current = current.parent;

				if (this.getDescriptor == null && current.getDescriptor != null)
				{
					if (getDescriptor != null)
					{
						// If the GetDescriptor method is overridden, chain it to the parent override.
						DescriptorCache c = current;
						this.getDescriptor = promise => { return getDescriptor(promise, () => c.getDescriptor(promise)); };
					}
					else
					{
						// We don't have a custom GetDescriptor, but the parent has, use that.
						this.getDescriptor = current.getDescriptor;
					}
				}
			}

			// There is a GetDescriptor override, but no parent has an override, so we make a simple unchained function.
			if (getDescriptor != null && this.getDescriptor == null)
			{
				this.getDescriptor = promise => getDescriptor(promise, promise.GetDescriptor);
			}

			this.cacheLock = root == this ? new object() : root.cacheLock;
		}

		#endregion
		
		#region Methods

		public IEnumerable<ExportDescriptor> GetDescriptors(CompositionContext sourceContext, CompositionContract contract)
		{
			lock (this.cacheLock)
			{
				// Check if the specified export is already cached within this cache.
				ExportKey key = contract.GetKey();
				if (this.cache.TryGetValue(key, out DescriptorCacheEntry descriptors))
				{
					// Return the cached descriptors.
					foreach (ExportDescriptor d in descriptors)
					{
						yield return d;
					}

					// Go up the hierarchy and return all their cached descriptors.
					DescriptorCache current = this;
					for (int i = 1; i < this.depth; ++i)
					{
						current = current.parent;
						if (current.cache.TryGetValue(key, out descriptors))
						{
							foreach (ExportDescriptor d in descriptors)
							{
								yield return d;
							}
						}
					}
				}
				else
				{
					TransactionLayer[] layers = ArrayPool<TransactionLayer>.Shared.Rent(this.depth);

					DescriptorCache current = this;
					for (int i = 0; i < this.depth; ++i)
					{
						layers[i] = new TransactionLayer(current.providers, current.cache, this.getDescriptor);
						current = current.parent;
					}

					Transaction transaction = new Transaction(new ArraySegment<TransactionLayer>(layers, 0, this.depth));
					transaction.Execute(contract);

					for (int i = 0; i < this.depth; ++i)
					{
						layers[i].Commit();

						if (layers[i].TryGetValue(key, out DescriptorCacheEntry value) && !value.Descriptors.IsDefaultOrEmpty)
						{
							foreach (ExportDescriptor d in value)
							{
								yield return d;
							}
						}
					}

					ArrayPool<TransactionLayer>.Shared.Return(layers, true);
				}
			}
		}

		#endregion
	}
}
