﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Composition
{
	[Serializable]
	public class TransactionCompletedException : InvalidOperationException
	{
		//
		// For guidelines regarding the creation of new exception types, see
		//    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
		// and
		//    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
		//

		public TransactionCompletedException()
			: this("The transaction has already been completed.")
		{
		}

		public TransactionCompletedException(string message) : base(message)
		{
		}

		public TransactionCompletedException(string message, Exception inner) : base(message, inner)
		{
		}

		protected TransactionCompletedException(
			SerializationInfo info,
			StreamingContext context) : base(info, context)
		{
		}
	}
}
