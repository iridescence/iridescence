﻿using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Lists the dependencies required for a part.
	/// </summary>
	public delegate IEnumerable<Dependency> DependencyEnumerator();
}
