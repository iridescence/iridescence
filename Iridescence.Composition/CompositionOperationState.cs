﻿namespace Iridescence.Composition
{
	public enum CompositionOperationState
	{
		Running,
		Completing,
		Completed
	}
}