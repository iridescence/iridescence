﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents an isolated chain of exports and their dependencies.
	/// </summary>
	internal sealed class Transaction : DependencyAccessor
	{
		#region Fields

		private bool closed;
		private readonly IReadOnlyList<TransactionLayer> layers;
		private readonly Dictionary<ExportKey, TransactionEntry> entries;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Transaction"/> class. 
		/// </summary>
		public Transaction(IReadOnlyList<TransactionLayer> layers)
		{
			this.layers = layers ?? throw new ArgumentNullException(nameof(layers));
			this.entries = new Dictionary<ExportKey, TransactionEntry>();
		}

		#endregion

		#region Methods

		public void Execute(CompositionContract contract)
		{
			if (this.closed)
				throw new TransactionCompletedException();

			HashSet<ExportDescriptorPromise> @checked = new HashSet<ExportDescriptorPromise>();
			Stack<Dependency> checking = new Stack<Dependency>();
			foreach (Dependency dep in this.ResolveDependencies("Root", contract, DependencyFlags.Prerequisite))
			{
				this.checkPromise(dep, @checked, checking);
				Debug.Assert(checking.Count == 0);
			}

			this.closed = true;

			// Add new export descriptors to their respective layers.
			foreach (var entry in this.entries)
			{
				foreach ((TransactionLayer layer, ImmutableArray<ExportDescriptor> descriptors) in entry.Value.GetDescriptors(null))
				{
					// Do not add empty results to parent layers.
					if (descriptors.IsDefaultOrEmpty && layer != this.layers[0] && layer.TryGetValue(entry.Key, out DescriptorCacheEntry existing))
					{
						Debug.Assert(existing.Descriptors.IsDefaultOrEmpty);
						continue;
					}

					layer.Add(entry.Key, new DescriptorCacheEntry(descriptors));
				}
			}
		}

		private void checkPromise(Dependency dependency, HashSet<ExportDescriptorPromise> @checked, Stack<Dependency> checking)
		{
			if (dependency.IsError)
			{
				var message = new StringBuilder();
				dependency.AppendError(message);
				message.AppendLine();
				writeCompositionStack(message, dependency, checking);

				throw new DependencyException(message.ToString(), EnumerableExtensions.Once(dependency));
			}

			if (@checked.Contains(dependency.Promise))
				return;

			@checked.Add(dependency.Promise);

			checking.Push(dependency);
			foreach (var dep in dependency.Promise.Dependencies)
				this.checkDependency(dep, @checked, checking);
			checking.Pop();
		}

		private void checkDependency(Dependency dependency, HashSet<ExportDescriptorPromise> @checked, Stack<Dependency> checking)
		{
			if (@checked.Contains(dependency.Promise))
			{
				var sharedSeen = false;
				var nonPrereqSeen = (dependency.Flags & DependencyFlags.Prerequisite) == 0;

				foreach (var step in checking)
				{
					if (step.Promise.IsShared)
						sharedSeen = true;

					if (sharedSeen && nonPrereqSeen)
						break;

					if (step.Promise.Equals(dependency.Promise))
					{
						var message = new StringBuilder();
						message.AppendLine($"Detected an unsupported cycle for part \"{dependency.Promise.Origin}\". To construct a valid cycle, at least one part in the cycle must be shared, and at least one import in the cycle must be non-prerequisite (e.g. a property).");
						writeCompositionStack(message, dependency, checking);
						throw new CompositionException(message.ToString());
					}

					if ((dependency.Flags & DependencyFlags.Prerequisite) == 0)
						nonPrereqSeen = true;
				}
			}

			this.checkPromise(dependency, @checked, checking);
		}

		private static void writeCompositionStack(StringBuilder str, Dependency import, IEnumerable<Dependency> dependencies)
		{
			bool any = false;

			foreach (var step in dependencies)
			{
				any = true;
				str.AppendLine($"   required by import \"{import.Site}\" of part \"{step.Promise.Origin}\".");
				import = step;
			}

			if (!any)
				return;

			str.Append($"   required by request for contract \"{import.Contract}\".");
		}

		public override IEnumerable<ExportDescriptorPromise> GetPromises(DependencyQuery query)
		{
			if (this.closed)
				throw new TransactionCompletedException();

			// Get the key that identifies the contract.
			ExportKey key = query.Contract.GetKey();

			// First, search all layers (from top to bottom) whether they already have the descriptors cached.
			List<ExportDescriptor> existing = null;
			int emptyLayers = 0;
			foreach (var layer in this.layers)
			{
				if (layer.TryGetValue(key, out DescriptorCacheEntry ds))
				{
					if (ds.Descriptors.IsDefaultOrEmpty)
					{
						++emptyLayers;
					}
					else
					{
						if (existing == null)
						{
							existing = new List<ExportDescriptor>(ds);
						}
						else
						{
							existing.AddRange(ds);
						}
					}
				}
			}

			if (existing != null)
			{
				return existing.Select(d => new ExportDescriptorPromise(query.Contract, "Preexisting", false, Dependency.NoDependencies, _ => d));
			}

			// All layers had an empty array in their cache, meaning they were all queried for that contract before.
			if (emptyLayers == this.layers.Count)
			{
				return Enumerable.Empty<ExportDescriptorPromise>();
			}

			// Create a transaction entry for the key so that future requests for the same contract from within the current graph go to the same entry.
			// This makes sure that a promise provider is not queried multiple times for the same contract and prevents infinite loops.
			if (!this.entries.TryGetValue(key, out TransactionEntry entry))
			{
				entry = new TransactionEntry(this.layers);
				this.entries.Add(key, entry);
			}

			// Query all providers and store their promises.
			foreach (TransactionEntry.Provider provider in entry.GetProviders())
			{
				foreach (ExportDescriptorPromise promise in query.GetPromises(provider.PromiseProvider))
				{
					provider.AddPromise(promise, query.Shadow ? (int?)entry.RecursionCounter : null);
				}
			}

			return entry.GetPromises(entry.RecursionCounter++);
		}

		#endregion
	}
}
