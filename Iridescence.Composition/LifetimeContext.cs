﻿using System;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Implements a <see cref="CompositionContext"/> that provides its own management of bound instances, but gets the export descriptors from a parent <see cref="CompositionContext"/>.
	/// </summary>
	public class LifetimeContext : CompositionContext
	{
		#region Fields

		private List<IDisposable> boundInstances;
		private readonly CompositionContext parent;
		private bool isDisposed;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="LifetimeContext"/> class. 
		/// </summary>
		public LifetimeContext(CompositionContext parent)
		{
			this.parent = parent;
		}

		#endregion

		#region Methods

		public override void AddDisposable(IDisposable obj)
		{
			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			if (this.isDisposed)
				throw new ObjectDisposedException(this.GetType().Name);

			if (this.boundInstances == null)
				this.boundInstances = new List<IDisposable>();

			this.boundInstances.Add(obj);
		}

		public override IEnumerable<ExportDescriptor> GetExportDescriptors(CompositionContract contract)
		{
			if (contract == null)
				throw new ArgumentNullException(nameof(contract));

			if (this.isDisposed)
				throw new ObjectDisposedException(this.GetType().Name);

			return this.parent.GetExportDescriptors(contract);
		}

		protected override void Dispose(bool managed)
		{
			if (this.isDisposed)
				return;

			this.isDisposed = true;

			if (this.boundInstances != null)
			{
				for (int i = this.boundInstances.Count - 1; i >= 0; --i)
				{
					this.boundInstances[i].Dispose();
				}
			}
		}

		#endregion
	}
}
