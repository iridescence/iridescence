﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Composition
{
	/// <summary>
	/// Represents an abstract composition context that is able to provide object instances based on a contract.
	/// </summary>
	public abstract class CompositionContext : IDisposable, IServiceProvider
	{
		#region Fields

		private ConcurrentFlag isDisposed;
		private readonly object boundInstancesLock;
		private List<IDisposable> boundInstances;

		#endregion

		#region Properties

		public bool IsDisposed => this.isDisposed.IsSet;

		#endregion

		#region Constructors

		protected CompositionContext()
		{
			this.isDisposed = new ConcurrentFlag();
			this.boundInstancesLock = new object();
		}

		~CompositionContext()
		{
			if (!this.isDisposed.TrySet())
				return;

			this.Dispose(false);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns all export descriptors for parts matching the specified contract.
		/// </summary>
		/// <param name="contract">The requested contract.</param>
		/// <returns>An enumerable of the exported part descriptors.</returns>
		public abstract IEnumerable<ExportDescriptor> GetExportDescriptors(CompositionContract contract);

		object IServiceProvider.GetService(Type serviceType)
		{
			if (!this.TryGetExport(new CompositionContract(serviceType), out object obj))
				return null;

			return obj;
		}

		#region TryGetExport

		/// <summary>
		/// Searches for zero or one part that matches the specified contract.
		/// </summary>
		/// <param name="contract">The contract that defines what to look for.</param>
		/// <param name="obj">The resulting object.</param>
		/// <returns>True, if there is one export that matches the contract. False, if there are none.</returns>
		public bool TryGetExport(CompositionContract contract, out object obj)
		{
			using PooledArray<ExportDescriptor> descriptors = this.GetExportDescriptors(contract).ToPooledArray();

			if (descriptors.Count == 0)
			{
				obj = null;
				return false;
			}

			if (descriptors.Count == 1)
			{
				obj = CompositionOperation.Run(this, descriptors[0].Activator);
				return true;
			}

			throw CompositionException.TooManyExports(contract);
		}

		/// <summary>
		/// Searches for zero or one part that matches the specified contract.
		/// </summary>
		/// <param name="type">The type of the part.</param>
		/// <param name="name">The name of the part.</param>
		/// <param name="obj">The resulting object.</param>
		/// <returns>True, if there is one export that matches the contract. False, if there are none.</returns>
		public bool TryGetExport(Type type, string name, out object obj)
		{
			return this.TryGetExport(new CompositionContract(type, name), out obj);
		}

		/// <summary>
		/// Searches for zero or one part that matches the specified contract.
		/// </summary>
		/// <param name="type">The type of the part.</param>
		/// <param name="obj">The resulting object.</param>
		/// <returns>True, if there is one export that matches the contract. False, if there are none.</returns>
		public bool TryGetExport(Type type, out object obj)
		{
			return this.TryGetExport(new CompositionContract(type), out obj);
		}

		/// <summary>
		/// Searches for zero or one part that matches the specified contract.
		/// </summary>
		/// <typeparam name="T">The type of the part.</typeparam>
		/// <param name="name">The name of the part.</param>
		/// <param name="obj">The resulting object.</param>
		/// <returns>True, if there is one export that matches the contract. False, if there are none.</returns>
		public bool TryGetExport<T>(string name, out T obj)
		{
			obj = default;
			if (!this.TryGetExport(typeof(T), name, out object obj2))
				return false;
			obj = (T)obj2;
			return true;
		}

		/// <summary>
		/// Searches for zero or one part that matches the specified contract.
		/// </summary>
		/// <typeparam name="T">The type of the part.</typeparam>
		/// <param name="obj">The resulting object.</param>
		/// <returns>True, if there is one export that matches the contract. False, if there are none.</returns>
		public bool TryGetExport<T>(out T obj)
		{
			obj = default;
			if (!this.TryGetExport(typeof(T), out object obj2))
				return false;
			obj = (T)obj2;
			return true;
		}

		#endregion

		#region GetExport

		/// <summary>
		/// Searches for exactly one part that matches the specified contract.
		/// </summary>
		/// <param name="contract">The contract that defines what to look for.</param>
		/// <returns>The resulting object.</returns>
		public object GetExport(CompositionContract contract)
		{
			if(!this.TryGetExport(contract, out object obj))
				throw CompositionException.NotFound(contract);

			return obj;
		}

		/// <summary>
		/// Searches for exactly one part that matches the specified contract.
		/// </summary>
		/// <param name="type">The type of the part.</param>
		/// <param name="name">The name of the part.</param>
		/// <returns>The resulting object.</returns>
		public object GetExport(Type type, string name)
		{
			return this.GetExport(new CompositionContract(type, name));
		}

		/// <summary>
		/// Searches for exactly one part that matches the specified contract.
		/// </summary>
		/// <param name="type">The type of the part.</param>
		/// <returns>The resulting object.</returns>
		public object GetExport(Type type)
		{
			return this.GetExport(new CompositionContract(type));
		}

		/// <summary>
		/// Searches for exactly one part that matches the specified contract.
		/// </summary>
		/// <typeparam name="T">The type of the part.</typeparam>
		/// <param name="name">The name of the part.</param>
		/// <returns>The resulting object.</returns>
		public T GetExport<T>(string name)
		{
			return (T)this.GetExport(typeof(T), name);
		}
		 
		/// <summary>
		/// Searches for exactly one part that matches the specified contract.
		/// </summary>
		/// <typeparam name="T">The type of the part.</typeparam>
		/// <returns>The resulting object.</returns>
		public T GetExport<T>()
		{
			return (T)this.GetExport(typeof(T));
		}

		#endregion

		#region GetExports

		/// <summary>
		/// Searches for all parts matching the specified contract.
		/// </summary>
		/// <param name="contract"></param>
		/// <returns></returns>
		public IEnumerable<object> GetExports(CompositionContract contract)
		{
			foreach (ExportDescriptor descriptor in this.GetExportDescriptors(contract))
			{
				yield return CompositionOperation.Run(this, descriptor.Activator);
			}
		}

		/// <summary>
		/// Searches for all parts matching the specified contract.
		/// </summary>
		/// <param name="type">The type of the parts.</param>
		/// <param name="name">The name of the parts.</param>
		/// <returns></returns>
		public IEnumerable<object> GetExports(Type type, string name = null)
		{
			return this.GetExports(new CompositionContract(type, name));
		}
		
		/// <summary>
		/// Searches for all parts matching the specified contract.
		/// </summary>
		/// <typeparam name="T">The type of the part.</typeparam>
		/// <param name="name">The name of the parts.</param>
		/// <returns></returns>
		public IEnumerable<T> GetExports<T>(string name = null)
		{
			return this.GetExports(typeof(T), name).Cast<T>();
		}

		#endregion

		/// <summary>
		/// Adds an <see cref="IDisposable"/> to the context which will be disposed when the context is disposed.
		/// </summary>
		/// <param name="obj"></param>	
		public virtual void AddDisposable(IDisposable obj)
		{
			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			if (this.isDisposed.IsSet)
				throw new ObjectDisposedException(this.GetType().Name);

			lock (this.boundInstancesLock)
			{
				if (this.boundInstances == null)
					this.boundInstances = new List<IDisposable>();

				this.boundInstances.Add(obj);
			}
		}

		protected virtual void Dispose(bool managed)
		{
			if (!managed)
				return;

			lock (this.boundInstancesLock)
			{
				if (this.boundInstances != null)
				{
					for (int i = this.boundInstances.Count - 1; i >= 0; --i)
					{
						this.boundInstances[i].Dispose();
					}
				}
			}
		}

		public void Dispose()
		{
			if (!this.isDisposed.TrySet())
				return;

			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion
	}
}
