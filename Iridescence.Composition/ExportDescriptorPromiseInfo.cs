﻿using System;
using System.Collections.Immutable;

namespace Iridescence.Composition
{
	/// <summary>
	/// Provides information about a promise for an <see cref="ExportDescriptor"/>.
	/// </summary>
	public class ExportDescriptorPromiseInfo
	{
		#region Fields

		private readonly Lazy<ImmutableArray<Dependency>> dependencies;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the contract that the descriptor was promised for.
		/// </summary>
		public CompositionContract Contract { get; }

		/// <summary>
		/// Gets the dependencies that have to be satisfied before the descriptor can be retrieved.
		/// </summary>
		public ImmutableArray<Dependency> Dependencies => this.dependencies.Value;

		/// <summary>
		/// Gets a string that identifies the origin of this export. Used for meaningful errors.
		/// </summary>
		public string Origin { get; }

		/// <summary>
		/// Gets a value that indicates whether the promise is shared within a context.
		/// </summary>
		public bool IsShared { get; }

		#endregion

		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ExportDescriptorPromise"/> class. 
		/// </summary>
		public ExportDescriptorPromiseInfo(CompositionContract contract, string origin, bool isShared, DependencyEnumerator getDependencies)
		{
			this.Contract = contract;
			this.Origin = origin;
			this.IsShared = isShared;
			this.dependencies = new Lazy<ImmutableArray<Dependency>>(() => getDependencies().ToImmutableArray());
		}

		internal ExportDescriptorPromiseInfo(ExportDescriptorPromiseInfo promise)
		{
			this.Contract = promise.Contract;
			this.Origin = promise.Origin;
			this.IsShared = promise.IsShared;
			this.dependencies = promise.dependencies;
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			return $"Promise for \"{this.Contract}\" from \"{this.Origin}\"";
		}

		#endregion
	}
}