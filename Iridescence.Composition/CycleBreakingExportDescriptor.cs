// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System;
using System.Collections.Generic;

namespace Iridescence.Composition
{
	internal class CycleBreakingExportDescriptor : ExportDescriptor
	{
		#region Fields

		private readonly Lazy<ExportDescriptor> exportDescriptor;

		#endregion

		#region Properties

		public override CompositeActivator Activator
		{
			get
			{
				if (!this.exportDescriptor.IsValueCreated)
					return this.activate;

				return this.exportDescriptor.Value.Activator;
			}
		}
	
		public override IReadOnlyList<CompositionMetadata> Metadata
		{
			get
			{
				if (!this.exportDescriptor.IsValueCreated)
					return new CycleBreakingMetadataCollection(this.exportDescriptor);

				return this.exportDescriptor.Value.Metadata;
			}
		}

		#endregion

		#region Constructors

		public CycleBreakingExportDescriptor(Lazy<ExportDescriptor> exportDescriptor)
		{
			this.exportDescriptor = exportDescriptor;
		}

		#endregion

		#region Methods

		private object activate(CompositionContext context, CompositionOperation operation)
		{
			if (!this.exportDescriptor.IsValueCreated)
				throw new NotSupportedException();

			return this.exportDescriptor.Value.Activator(context, operation);
		}
		
		#endregion
	}
}
