﻿using System.Collections.Generic;

namespace Iridescence.Composition
{
	/// <summary>
	/// Makes <see cref="ExportDescriptorPromise">promises</see> for <see cref="CompositionContract">contracts</see>.
	/// </summary>
	public interface IExportDescriptorProvider
	{
		/// <summary>
		/// Returns promises for all exports that match the specified contract.
		/// </summary>
		/// <param name="contract">The <see cref="CompositionContract"/> to match.</param>
		/// <param name="dependencyAccessor">A factory for dependencies.</param>
		/// <returns></returns>
		IEnumerable<ExportDescriptorPromise> GetPromises(CompositionContract contract, DependencyAccessor dependencyAccessor);
	}
}
