﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Iridescence.Graphics.OpenGL.WGL.WinForms
{
	/// <summary>
	/// Represents a WGL OpenGL control.
	/// </summary>
	public class WGLControl : Control, IDeviceContext
	{
		#region Fields

		private readonly IntPtr handle;
		private IntPtr deviceContext;
		
		#endregion

		#region Properties

		private IntPtr DeviceContext
		{
			get
			{
				if (this.deviceContext == IntPtr.Zero)
				{
					this.deviceContext = Win32.GetDC(this.handle);
					if (this.deviceContext == IntPtr.Zero)
						throw new Win32Exception(Marshal.GetLastWin32Error());

					this.SetPixelFormat(PixelFormatDescriptor.Default);
				}

				return this.deviceContext;
			}
		}

		IntPtr IDeviceContext.DeviceContext => this.DeviceContext;

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams p = base.CreateParams;
				p.Style |= 0x20; // CS_OWNDC
				return p;
			}
		}

		#endregion

		#region Constructors

		public WGLControl()
		{
			this.handle = this.Handle;
		}

		#endregion

		#region Methods

		protected override void DestroyHandle()
		{
			WGL.wglMakeCurrent(this.DeviceContext, IntPtr.Zero);
			base.DestroyHandle();
		}

		#endregion
	}
}
