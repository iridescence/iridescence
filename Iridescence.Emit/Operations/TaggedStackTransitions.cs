﻿using System;
using System.Collections.Generic;

namespace Iridescence.Emit
{
	internal readonly struct TaggedStackTransitions
	{
		#region Properties

		public string Name { get; }

		public IReadOnlyList<IStackTransition> Transitions { get; }

		#endregion

		#region Constructors

		public TaggedStackTransitions(string name, IStackTransition transition)
		{
			this.Name = name;
			this.Transitions = new[] {transition};
		}

		public TaggedStackTransitions(string name, IReadOnlyList<IStackTransition> transitions)
		{
			this.Name = name;

			this.Transitions = transitions ?? throw new ArgumentNullException(nameof(transitions));

			if (this.Transitions.Count == 0)
				throw new ArgumentException("Transitions can't be empty.", nameof(transitions));
		}

		#endregion
	}
}
