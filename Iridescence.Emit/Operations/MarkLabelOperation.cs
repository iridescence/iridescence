using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Marks a <see cref="Label"/>.
	/// </summary>
	public sealed class MarkLabelOperation : Operation
	{
		public Label Label { get; }

		internal MarkLabelOperation(OperationCollection collection, Label label)
			: base(collection)
		{
			this.Label = label;
		}

		internal override void ToString(StringBuilder str)
		{
			if (this.Label.Name != null)
			{
				str.Append(this.Label.Name);
				str.AppendLine(":");
			}
		}
	}
}