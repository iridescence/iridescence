﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Abstract stack transition interface.
	/// </summary>
	internal interface IStackTransition
	{
		/// <summary>
		/// Tries to apply this stack transition to the specified <see cref="EvaluationStack"/>
		/// If the transition succeeds, the method returns true and optionally modifies the <see cref="EvaluationStack"/> given to it.
		/// </summary>
		/// <param name="stack">The stack. Can be modified by the method.</param>
		/// <returns>True, if the transition is successful. False otherwise.</returns>
		bool Apply(ref EvaluationStack stack);
	}

	/// <summary>
	/// Utility and extension methods for stack transitions.
	/// </summary>
	internal static class StackTransition
	{
		private static class Cache<T>
		{
			public static readonly PushStackTransition Push = Push(typeof(T));
			public static readonly PopStackTransition Pop = Pop(typeof(T));
		}

		public static readonly NopStackTransition None = new NopStackTransition();
		public static readonly ClearStackTransition Clear = new ClearStackTransition();
		public static readonly DuplicateStackTransition Duplicate = new DuplicateStackTransition();

		public static IStackTransition Chain(this IStackTransition first, IStackTransition second)
		{
			if (first == null && second == null)
				return null;

			if (first == null || first is NopStackTransition)
				return second;

			if (second == null || second is NopStackTransition)
				return first;

			return new ChainedStackTransition(first, second);
		}

		#region EmptyStack

		private static readonly StackSizeMarker emptyStack = new StackSizeMarker(0);

		public static StackSizeMarker EmptyStack()
		{
			return emptyStack;
		}

		public static IStackTransition EmptyStack(this IStackTransition previous)
		{
			return previous.Chain(emptyStack);
		}
		
		#endregion

		#region SingleItemStack

		private static readonly StackSizeMarker singleItemStack = new StackSizeMarker(1);

		public static StackSizeMarker SingleItemStack()
		{
			return singleItemStack;
		}

		public static IStackTransition SingleItemStack(this IStackTransition previous)
		{
			return previous.Chain(singleItemStack);
		}
		
		#endregion
		
		#region PopReference

		private static readonly PopReferenceStackTransition popReference = new PopReferenceStackTransition();

		public static PopReferenceStackTransition PopReference()
		{
			return popReference;
		}

		public static IStackTransition PopReference(this IStackTransition previous)
		{
			return previous.Chain(popReference);
		}

		#endregion

		#region PopAny

		private static readonly PopAnyStackTransition popAny = new PopAnyStackTransition();

		public static PopAnyStackTransition PopAny()
		{
			return popAny;
		}

		public static IStackTransition PopAny(this IStackTransition previous)
		{
			return previous.Chain(popAny);
		}

		#endregion
		
		#region PopObject

		private static readonly PopObjectStackTransition popObject = new PopObjectStackTransition();

		public static PopObjectStackTransition PopObject()
		{
			return popObject;
		}

		public static IStackTransition PopObject(this IStackTransition previous)
		{
			return previous.Chain(popObject);
		}

		#endregion

		#region Pop

		public static PopStackTransition Pop(Type type)
		{
			return new PopStackTransition(type);
		}
	
		public static IStackTransition Pop(this IStackTransition previous, Type type)
		{
			return previous.Chain(Pop(type));
		}

		public static PopStackTransition Pop<T>()
		{
			return Cache<T>.Pop;
		}
		
		public static IStackTransition Pop<T>(this IStackTransition previous)
		{
			return previous.Chain(Pop<T>());
		}

		#endregion

		#region Push

		public static PushStackTransition Push(Type type)
		{
			return new PushStackTransition(type);
		}

		public static IStackTransition Push(this IStackTransition previous, Type type)
		{
			return previous.Chain(Push(type));
		}

		public static PushStackTransition Push<T>()
		{
			return Cache<T>.Push;
		}
	
		public static IStackTransition Push<T>(this IStackTransition previous)
		{
			return previous.Chain(Push<T>());
		}

		#endregion

		public static PopPushSameStackTransition PopPushSame(int pushedTypeIndex, params Type[] poppedTypes)
		{
			return new PopPushSameStackTransition(pushedTypeIndex, poppedTypes);
		}
	}

	/// <summary>
	/// Represents a no-op stack transition that does nothing.
	/// </summary>
	internal sealed class NopStackTransition : IStackTransition
	{
		public bool Apply(ref EvaluationStack input)
		{
			return true;
		}	

		public override string ToString()
		{
			return String.Empty;
		}
	}

	/// <summary>
	/// Represents a chained stack transition that applies two transitions sequentially.
	/// </summary>
	internal sealed class ChainedStackTransition : IStackTransition
	{
		#region Properties

		public IStackTransition First { get; }

		public IStackTransition Second { get; }

		#endregion

		#region Constructors

		public ChainedStackTransition(IStackTransition first, IStackTransition second)
		{
			this.First = first;
			this.Second = second;
		}

		#endregion

		#region Methods

		public bool Apply(ref EvaluationStack stack)
		{
			if (!this.First.Apply(ref stack))
				return false;

			return this.Second.Apply(ref stack);
		}

		public override string ToString()
		{
			return $"{this.First}, {this.Second}";
		}

		#endregion
	}

	/// <summary>
	/// Represents a stack transition that pops types from the stack.
	/// </summary>
	internal sealed class PopStackTransition : IStackTransition
	{
		#region Properties

		public Type Type {get;}

		#endregion

		#region Constructors

		public PopStackTransition(Type type)
		{
			this.Type = type;
		}

		#endregion

		#region Methods

		public bool Apply(ref EvaluationStack stack)
		{
			return stack.TryPop(t => t.StackAssignableTo(this.Type), out _, out stack);
		}

		public override string ToString()
		{
			return $"Pop {this.Type}";
		}

		#endregion
	}

	/// <summary>
	/// Represents a stack transition that pops any item from the stack, regardless of type.
	/// </summary>
	internal sealed class PopAnyStackTransition : IStackTransition
	{
		public bool Apply(ref EvaluationStack stack)
		{
			return stack.TryPop(_ => true, out _, out stack);
		}

		public override string ToString()
		{
			return "Pop";
		}
	}

	/// <summary>
	/// Represents a stack transition that pops only an item of type "Object" from the stack and nothing else (no derived types either).
	/// </summary>
	internal sealed class PopObjectStackTransition : IStackTransition
	{
		public bool Apply(ref EvaluationStack stack)
		{
			return stack.TryPop(t => t == typeof(object), out _, out stack);
		}

		public override string ToString()
		{
			return "Pop Object";
		}
	}

	/// <summary>
	/// Represents a stack transition that pops any reference from the stack.
	/// </summary>
	internal sealed class PopReferenceStackTransition : IStackTransition
	{
		public bool Apply(ref EvaluationStack stack)
		{
			return stack.TryPop(t => !t.IsValueType, out _, out stack);
		}

		public override string ToString()
		{
			return "Pop Reference";
		}
	}
	
	/// <summary>
	/// Represents a stack transition that pushes types onto the stack.
	/// </summary>
	internal sealed class PushStackTransition : IStackTransition
	{
		#region Properties
		
		public Type Type { get; }

		#endregion

		#region Constructors

		public PushStackTransition(Type type)
		{
			this.Type = type;
		}

		#endregion

		#region Methods

		public bool Apply(ref EvaluationStack stack)
		{
			stack = stack.Push(this.Type);
			return true;
		}
		
		public override string ToString()
		{
			return $"Push {this.Type}";
		}

		#endregion
	}

	/// <summary>
	/// Pops the specified types, then puts one of the popped types back on the stack.
	/// This is useful for arithmetic operations.
	/// </summary>
	internal sealed class PopPushSameStackTransition : IStackTransition
	{
		#region Properties

		public int PushedTypeIndex { get; }

		public IReadOnlyList<Type> PoppedTypes { get; }

		#endregion

		#region Constructors

		public PopPushSameStackTransition(int pushedTypeIndex, IEnumerable<Type> poppedTypes)
		{
			this.PushedTypeIndex = pushedTypeIndex;
			this.PoppedTypes = poppedTypes.ToArray();
		}

		#endregion

		#region Methods

		public bool Apply(ref EvaluationStack stack)
		{
			Type[] types = new Type[this.PoppedTypes.Count];
			for (int i = 0; i < this.PoppedTypes.Count; ++i)
			{
				if (!stack.TryPop(t => t.StackAssignableTo(this.PoppedTypes[i]), out types[i], out stack))
					return false;
			}

			stack = stack.Push(types[this.PushedTypeIndex]);
			return true;
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			if (this.PoppedTypes.Count == 1)
			{
				str.Append("Pop ");
				str.Append(this.PoppedTypes[0]);
			}
			else if (this.PoppedTypes.Count > 1)
			{
				str.Append("Pop (");

				bool first = true;

				foreach (Type type in this.PoppedTypes)
				{
					if (first)
						first = false;
					else
						str.Append(", ");
					str.Append(type);
				}

				str.Append(")");
			}

			str.Append(" Push @");
			str.Append(this.PushedTypeIndex);

			return str.ToString();
		}

		#endregion
	}

	internal sealed class LoadElementRefTransition : IStackTransition
	{
		public bool Apply(ref EvaluationStack stack)
		{
			if (!stack.TryPop(t =>
			{
				Type t2 = t.GetStackType();
				return t2 == typeof(NativeInt) || t2 == typeof(int);
			}, out _, out stack))
				return false;

			if (!stack.TryPop(t => t.IsArray && !t.GetElementType().IsValueType, out Type arrayType, out stack))
				return false;

			stack = stack.Push(arrayType.GetElementType());
			return true;
		}

		public override string ToString()
		{
			return "Pop int32 or native int, Pop T[], Push T";
		}
	}

	internal sealed class LoadIndirectRefTransition : IStackTransition
	{
		public bool Apply(ref EvaluationStack stack)
		{
			if (!stack.TryPop(t => (t.IsByRef || t.IsPointer) && !t.GetElementType().IsValueType, out Type refType, out stack))
				return false;

			stack = stack.Push(refType.GetElementType());
			return true;
		}

		public override string ToString()
		{
			return "Pop T&, Push T";
		}
	}


	/// <summary>
	/// Represents a stack transition that duplicates the value on top of the stack.
	/// </summary>
	internal sealed class DuplicateStackTransition : IStackTransition
	{
		public bool Apply(ref EvaluationStack stack)
		{
			if (!stack.TryPop(_ => true, out Type type, out stack))
				return false;

			stack = stack.Push(type);
			stack = stack.Push(type);
			return true;
		}

		public override string ToString()
		{
			return "Dup";
		}
	}

	/// <summary>
	/// Represents a pseudo stack transition that ensures the stack has a certain size.
	/// </summary>
	internal sealed class StackSizeMarker : IStackTransition
	{
		public int Count { get; }

		public StackSizeMarker(int count)
		{
			this.Count = count;
		}

		public bool Apply(ref EvaluationStack stack)
		{
			return stack.Count == this.Count;
		}

		public override string ToString()
		{
			return $"Ensure stack size is {this.Count}";
		}
	}

	/// <summary>
	/// Clears the evaluation stack.
	/// </summary>
	internal sealed class ClearStackTransition : IStackTransition
	{
		public bool Apply(ref EvaluationStack stack)
		{
			stack = EvaluationStack.Empty;
			return true;
		}

		public override string ToString()
		{
			return "Clear";
		}
	}
}

