using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Ends a catch block. This operation does nothing and is only used for debugging or commenting.
	/// </summary>
	public sealed class CatchBlockEndOperation : Operation
	{
		internal CatchBlockEndOperation(OperationCollection collection)
			: base(collection)
		{
			
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine("// End of catch block.");
		}
	}
}