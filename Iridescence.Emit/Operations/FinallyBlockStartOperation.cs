using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Begins a finally block.
	/// </summary>
	public sealed class FinallyBlockStartOperation : Operation
	{
		public override int ByteSize
		{
			get
			{
				ExceptionBlockState state = this.Collection.GetExceptionState(this.OperationIndex);

				if (state != ExceptionBlockState.Try)
					return 2 * (OpCodes.Leave.Size + 4); // Leave for preceding catch clause and leave for try clause.

				return OpCodes.Leave.Size + 4;

			}
		}

		internal FinallyBlockStartOperation(OperationCollection collection)
			: base(collection)
		{
			
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine("// Start of finally block.");
		}
	}
}