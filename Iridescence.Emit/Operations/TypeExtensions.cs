﻿using System;
using System.Reflection;

namespace Iridescence.Emit
{
	internal static class TypeExtensions
	{
		private static readonly Func<Type, bool> isSzArrayFunc;

		static TypeExtensions()
		{
			PropertyInfo property = typeof(Type).GetProperty("IsSzArray", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public) ??
			                        typeof(Type).GetProperty("IsSZArray", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
			MethodInfo getter = property.GetGetMethod(true);
			isSzArrayFunc = (Func<Type, bool>)Delegate.CreateDelegate(typeof(Func<Type, bool>), getter);
		}

		private static Type exchangeElementType(Type type, Type elementType, Type newElementType, bool arrayOnly = false)
		{
			if (!arrayOnly)
			{
				if (type.IsPointer)
					return exchangeElementType(type.GetElementType(), elementType, newElementType).MakePointerType();

				if (type.IsByRef)
					return exchangeElementType(type.GetElementType(), elementType, newElementType).MakeByRefType();
			}

			if (isSzArrayFunc(type))
				return exchangeElementType(type.GetElementType(), elementType, newElementType).MakeArrayType();

			if (type == elementType)
				return newElementType;

			return type;
		}

		public static Type GetStackType(this Type type)
		{
			if (type.IsEnum)
				type = type.GetEnumUnderlyingType();

			if (type == typeof(bool) || 
			    type == typeof(byte) || 
			    type == typeof(sbyte) || 
			    type == typeof(short) || 
			    type == typeof(ushort) || 
			    type == typeof(char))
				return typeof(int);

			type = exchangeElementType(type, typeof(bool), typeof(byte), true);
			type = exchangeElementType(type, typeof(char), typeof(short));
			type = exchangeElementType(type, typeof(sbyte), typeof(byte));
			type = exchangeElementType(type, typeof(ushort), typeof(short));
			type = exchangeElementType(type, typeof(uint), typeof(int));
			type = exchangeElementType(type, typeof(ulong), typeof(long));
			type = exchangeElementType(type, typeof(IntPtr), typeof(NativeInt));
			type = exchangeElementType(type, typeof(UIntPtr), typeof(NativeInt));

			return type;
		}

		public static bool StackAssignableTo(this Type from, Type to)
		{
			if (from == null)
				throw new ArgumentNullException(nameof(from));

			if (to == null)
				throw new ArgumentNullException(nameof(to));

			if (from is MethodPointerType && (to == typeof(IntPtr) || to == typeof(UIntPtr) || to == typeof(NativeInt)))
				return true;

			if (from == to || to.IsAssignableFrom(from))
				return true;

			from = from.GetStackType();
			to = to.GetStackType();

			if (from == to || to.IsAssignableFrom(from))
				return true;

			if (from == typeof(NullType))
				return !to.IsValueType;

			if (to == typeof(AnyPointer) && from.IsPointer)
				return true;

			if (to == typeof(AnyByRef) && from.IsByRef)
				return true;

			if ((to.IsPointer || to.IsByRef) && from == typeof(NativeInt))
				return true;

			if (to == typeof(NativeInt) && (from.IsPointer || from.IsByRef))
				return true;


			return (to.IsPointer || to.IsByRef) && (from.IsPointer || from.IsByRef);
			/*
			while (to.IsPointer || to.IsByRef)
				to = to.GetElementType();

			while (from.IsPointer || from.IsByRef)
				from = from.GetElementType();

			if (to == from)
				return true;*/
		}
	}
}