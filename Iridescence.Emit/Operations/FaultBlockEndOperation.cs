using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Ends a fault block. This operation does nothing and is only used for debugging or commenting.
	/// </summary>
	public sealed class FaultBlockEndOperation : Operation
	{
		internal FaultBlockEndOperation(OperationCollection collection)
			: base(collection)
		{
			
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine("// End of fault block.");
		}

	}
}