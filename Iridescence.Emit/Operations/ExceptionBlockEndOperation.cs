using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Ends an exception block. The instructions emitted by this operation depend on the context that this operation appears in.
	/// </summary>
	public sealed class ExceptionBlockEndOperation : Operation
	{
		public override int ByteSize
		{
			get
			{
				ExceptionBlockState state = this.Collection.GetExceptionState(this.OperationIndex);

				if (state == ExceptionBlockState.Catch)
					return OpCodes.Leave.Size + 4;

				if (state == ExceptionBlockState.Finally)
					return OpCodes.Endfinally.Size;

				return 0;
			}
		}

		public ExceptionBlockEndOperation(OperationCollection collection)
			: base(collection)
		{
			
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine("// End of exception block.");

			ExceptionBlockState state = this.Collection.GetExceptionState(this.OperationIndex);

			if (state == ExceptionBlockState.Catch)
			{
				this.AppendByteOffsetLabel(str);
				str.AppendLine(OpCodes.Leave.Name);
			}
			
			if (state == ExceptionBlockState.Finally)
			{
				this.AppendByteOffsetLabel(str);
				str.AppendLine(OpCodes.Endfinally.Name);
			}
		}
	}
}