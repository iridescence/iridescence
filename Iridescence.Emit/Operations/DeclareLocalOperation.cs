using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents the declaration of a <see cref="Local"/> as an <see cref="Operation"/>.
	/// </summary>
	public sealed class DeclareLocalOperation : Operation
	{
		#region Properties

		/// <summary>
		/// Gets the local to declare.
		/// </summary>
		public Local Local { get; }

		#endregion

		#region Constructors

		internal DeclareLocalOperation(OperationCollection collection, Local local)
			: base(collection)
		{
			this.Local = local;
		}

		#endregion

		#region Methods

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine($"// Local {this.Local} is declared.");
		}


		#endregion
	}
}