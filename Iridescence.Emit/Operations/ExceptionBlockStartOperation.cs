using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Begins an exception block.
	/// </summary>
	public sealed class ExceptionBlockStartOperation : Operation
	{
		public Label Label {get;}

		public ExceptionBlockStartOperation(OperationCollection collection, Label label)
			: base(collection)
		{
			this.Label = label;
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine("// Start of exception block.");
		}
	}
}