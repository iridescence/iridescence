using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Ends a filter block. This operation does nothing and is only used for debugging or commenting.
	/// </summary>
	public sealed class FilterBlockEndOperation : Operation
	{
		internal FilterBlockEndOperation(OperationCollection collection)
			: base(collection)
		{
			
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine("// End of filter block.");
		}
	}
}