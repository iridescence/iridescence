﻿namespace Iridescence.Emit
{
	/// <summary>
	/// Defines the results of an opcode transform.
	/// </summary>
	public enum OpCodeTransformResult
	{
		NoChange,
		Required,
		Optimization,
	}
}
