using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Begins a fault block.
	/// </summary>
	public sealed class FaultBlockStartOperation : Operation
	{
		public override int ByteSize => OpCodes.Leave.Size + 4; // Code + branch target.

		internal FaultBlockStartOperation(OperationCollection collection)
			: base(collection)
		{
			
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine("// Start of fault block.");
		}
	}
}