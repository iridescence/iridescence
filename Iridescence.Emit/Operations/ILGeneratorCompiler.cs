﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	/// <summary>
	/// Implements a translator from <see cref="OperationCollection"/> to an <see cref="ILGenerator"/>.
	/// </summary>
	internal static class ILGeneratorCompiler
	{
		private static void emitInstruction(ILGenerator il, OpCode op, object operand, IReadOnlyDictionary<Label, System.Reflection.Emit.Label> labels)
		{
			switch (op.OperandType)
			{
				case OperandType.InlineBrTarget:
					il.Emit(op, labels[(Label)operand]);
					break;

				case OperandType.InlineField:
					il.Emit(op, (FieldInfo)operand);
					break;

				case OperandType.InlineI:
					il.Emit(op, (int)operand);
					break;

				case OperandType.InlineI8:
					il.Emit(op, (long)operand);
					break;

				case OperandType.InlineMethod:
					InlineMethod inlineMethod = (InlineMethod)operand;
					if (inlineMethod.Method is ConstructorInfo ci)
					{
						il.Emit(op, ci);
					}
					else
					{
						MethodInfo mi = (MethodInfo)inlineMethod.Method;

						if (inlineMethod.OptionalParameterTypes != null)
						{
							il.EmitCall(op, mi, inlineMethod.OptionalParameterTypes.ToArray());
						}
						else
						{
							il.Emit(op, mi);
						}
					}

					break;

				case OperandType.InlineNone:
					if (operand != null)
						throw new ArgumentException("Supplied operand for op-code that doesn't take one.", nameof(operand));
					il.Emit(op);
					break;

				case OperandType.InlineR:
					il.Emit(op, (double)operand);
					break;

				case OperandType.InlineSig:
					InlineSig sig = (InlineSig)operand;
					il.EmitCalli(op, sig.CallingConventions, sig.ReturnType, sig.ParameterTypes.ToArray(), sig.OptionalParameterTypes.IsDefault ? null : sig.OptionalParameterTypes.ToArray());
					break;

				case OperandType.InlineString:
					il.Emit(op, (string)operand);
					break;

				case OperandType.InlineSwitch:
					il.Emit(op, ((IEnumerable<Label>)operand).Select(l => labels[l]).ToArray());
					break;

				case OperandType.InlineTok:
					if (operand is FieldInfo field)
						il.Emit(op, field);
					else if (operand is MethodInfo method)
						il.Emit(op, method);
					else if (operand is Type type)
						il.Emit(op, type);
					break;

				case OperandType.InlineType:
					il.Emit(op, (Type)operand);
					break;

				case OperandType.InlineVar:
				{
					short index;
					if (operand is Local local)
						index = checked((short)local.Index);
					else
						index = (short)operand;

					il.Emit(op, index);
					break;
				}

				case OperandType.ShortInlineBrTarget:
					il.Emit(op, labels[(Label)operand]);
					break;

				case OperandType.ShortInlineI:
					il.Emit(op, (byte)operand);
					break;

				case OperandType.ShortInlineR:
					il.Emit(op, (float)operand);
					break;

				case OperandType.ShortInlineVar:
				{
					byte index;
					if (operand is Local local)
						index = checked((byte)local.Index);
					else
						index = (byte)operand;

					il.Emit(op, index);
					break;
				}

				default:
					throw new ArgumentOutOfRangeException(nameof(op));
			}
		}

		public static void Compile(IReadOnlyList<Operation> operations, ILGenerator il)
		{
			Dictionary<Label, System.Reflection.Emit.Label> labels = new Dictionary<Label, System.Reflection.Emit.Label>();

			foreach (Operation operation in operations)
			{
				switch(operation)
				{
					case CatchBlockStartOperation op:
						il.BeginCatchBlock(op.ExceptionType);
						break;

					case CatchBlockEndOperation op:
						// No-op.
						break;

					case DeclareLocalOperation op:
						LocalBuilder localBuilder = il.DeclareLocal(op.Local.LocalType, op.Local.IsPinned);

						// There is no way to make 100% sure the index return by ILGenerator is the same that we have come up with since it's technically implementation-dependent.
						// In the open source version of ILGenerator on GitHub however, a counter is incremented with each call to DeclareLocal, just like we do.
						// see https://github.com/dotnet/coreclr/blob/master/src/mscorlib/src/System/Reflection/Emit/ILGenerator.cs
						if (localBuilder.LocalIndex != op.Local.Index)
							throw new Exception("Local index mismatch.");
						break;

					case DefineLabelOperation op:
						labels.Add(op.Label, il.DefineLabel());
						break;

					case DisposeLocalOperation op:
						// No-op.
						break;

					case ExceptionBlockStartOperation op:
						labels.Add(op.Label, il.BeginExceptionBlock());
						break;

					case ExceptionBlockEndOperation op:
						il.EndExceptionBlock();
						break;

					case FaultBlockStartOperation op:
						il.BeginFaultBlock();
						break;

					case FaultBlockEndOperation op:
						// No-op.
						break;

					case FilterBlockStartOperation op:
						il.BeginExceptFilterBlock();
						break;

					case FilterBlockEndOperation op:
						// No-op.
						break;

					case FinallyBlockStartOperation op:
						il.BeginFinallyBlock();
						break;

					case FinallyBlockEndOperation op:
						// No-op.
						break;

					case Instruction op:
						Debug.Assert(il.ILOffset == op.ByteOffset);
						emitInstruction(il, op.OpCode, op.Operand, labels);
						break;

					case MarkLabelOperation op:
						il.MarkLabel(labels[op.Label]);
						break;

					case ReuseLocalOperation op:
						break;
					
					default:
						throw new NotSupportedException($"Unsupported operation \"{operation.GetType()}\".");
				}
			}
		}
	}
}
