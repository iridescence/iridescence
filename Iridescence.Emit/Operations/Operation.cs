﻿using System;
using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents an abstract <see cref="ILGenerator"/> operation.
	/// </summary>
	public abstract class Operation
	{
		#region Fields

		private int operationIndex;
		private int? byteOffset;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the <see cref="OperationCollection"/> this <see cref="Operation"/> belongs to.
		/// </summary>
		public OperationCollection Collection { get; private set; }

		/// <summary>
		/// Gets the size (in bytes) of this operation.
		/// Some operations do not actually take up space in the byte-stream.
		/// </summary>
		public virtual int ByteSize => 0;

		/// <summary>
		/// Gets the index of the <see cref="Operation"/> in the <see cref="Collection"/>.
		/// </summary>
		public int OperationIndex
		{
			get
			{
				if (this.Collection == null)
					throw new ObjectDisposedException(nameof(Operation));

				return this.operationIndex;
			}
			internal set
			{
				if (this.Collection == null)
					throw new ObjectDisposedException(nameof(Operation));

				this.operationIndex = value;
				this.byteOffset = null;
			}
		}

		/// <summary>
		/// Gets the byte offset of this <see cref="Operation"/>.
		/// </summary>
		public int ByteOffset
		{
			get
			{
				if (this.Collection == null)
					throw new ObjectDisposedException(nameof(Operation));

				if (!this.byteOffset.HasValue)
				{
					if (this.OperationIndex == 0)
					{
						this.byteOffset = 0;
					}
					else
					{
						Operation previousOp = this.Collection[this.OperationIndex - 1];
						if (previousOp.byteOffset.HasValue)
						{
							this.byteOffset = previousOp.byteOffset.Value + previousOp.ByteSize;
						}
						else
						{
							int sum = 0;
							for (int i = 0; i < this.OperationIndex; ++i)
							{
								sum += this.Collection[i].ByteSize;
							}
							this.byteOffset = sum;
						}
					}
				}

				return this.byteOffset.Value;
			}
		}

		public Operation Next
		{
			get
			{
				if (this.Collection == null)
					throw new ObjectDisposedException(nameof(Operation));

				if (this.OperationIndex == this.Collection.Count - 1)
					return null;

				return this.Collection[this.OperationIndex + 1];
			}
		}

		public Operation Previous
		{
			get
			{
				if (this.Collection == null)
					throw new ObjectDisposedException(nameof(Operation));

				if (this.OperationIndex == 0)
					return null;

				return this.Collection[this.OperationIndex - 1];
			}
		}

		public Instruction NextInstruction
		{
			get
			{
				if (this.Collection == null)
					throw new ObjectDisposedException(nameof(Operation));

				for (int i = this.OperationIndex + 1; i < this.Collection.Count; ++i)
				{
					if (this.Collection[i] is Instruction instr)
						return instr;
				}

				return null;
			}
		}

		public Instruction PreviousInstruction
		{
			get
			{
				if (this.Collection == null)
					throw new ObjectDisposedException(nameof(Operation));

				for (int i = this.OperationIndex - 1; i >= 0; --i)
				{
					if (this.Collection[i] is Instruction instr)
						return instr;
				}

				return null;
			}
		}

		#endregion

		#region Constructors

		internal Operation(OperationCollection collection)
		{
			this.Collection = collection;
		}

		#endregion

		#region Methods

		internal void RemoveFromCollection()
		{
			this.Collection = null;
			this.byteOffset = null;
		}

		internal void InvalidateByteOffset()
		{
			this.byteOffset = null;
		}

		protected void AppendByteOffsetLabel(StringBuilder str)
		{
			str.Append("IL_");
			str.Append(this.ByteOffset.ToString("X4"));
			str.Append(": ");
		}

		internal virtual void ToString(StringBuilder str)
		{
			
		}

		public sealed override string ToString()
		{
			StringBuilder str = new StringBuilder();
			this.ToString(str);
			return str.ToString().Trim();
		}

		#endregion
	}
}
