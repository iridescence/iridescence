﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents an IL instruction without operand.
	/// </summary>
	public class Instruction : Operation
	{
		#region Properties

		/// <summary>
		/// The OpCode that corresponds to an Emit call.
		/// Note that the opcode may not correspond to the final short forms and other optimizations.
		/// </summary>
		public OpCode OpCode { get; private set; }

		/// <summary>
		/// Gets the operand of the instruction.
		/// </summary>
		public object Operand { get; private set; }

		/// <summary>
		/// Gets the byte size (including the operand) of the instruction.
		/// </summary>
		public override int ByteSize
		{
			get
			{
				int operandSize;
				switch (this.OpCode.OperandType)
				{
					case OperandType.InlineBrTarget: operandSize = 4; break;
					case OperandType.InlineField: operandSize = 4; break;
					case OperandType.InlineI: operandSize = 4; break;
					case OperandType.InlineI8: operandSize = 8; break;
					case OperandType.InlineMethod: operandSize = 4; break;
					case OperandType.InlineNone: operandSize = 0; break;
					case OperandType.InlineR: operandSize = 8; break;
					case OperandType.InlineSig: operandSize = 4; break;
					case OperandType.InlineString: operandSize = 4; break;
					case OperandType.InlineSwitch: operandSize = 4 + ((IEnumerable<Label>)this.Operand).Count() * 4; break;
					case OperandType.InlineTok: operandSize = 4; break;
					case OperandType.InlineType: operandSize = 4; break;
					case OperandType.InlineVar: operandSize = 2; break;
					case OperandType.ShortInlineBrTarget: operandSize = 1; break;
					case OperandType.ShortInlineI: operandSize = 1; break;
					case OperandType.ShortInlineR: operandSize = 4; break;
					case OperandType.ShortInlineVar: operandSize = 1; break;
					default: throw new NotSupportedException();
				}

				return this.OpCode.Size + operandSize;
			}
		}

		/// <summary>
		/// Gets the prefixes of the instruction.
		/// </summary>
		public IEnumerable<Instruction> Prefixes
		{
			get
			{
				if (this.OpCode.OpCodeType == OpCodeType.Prefix)
					yield break;

				for (int i = this.OperationIndex - 1; i >= 0; --i)
				{
					if (this.Collection[i] is Instruction instr)
					{
						if (instr.OpCode.OpCodeType == OpCodeType.Prefix)
							yield return instr;
						else
							break;
					}
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Instruction"/> class. 
		/// </summary>
		internal Instruction(OperationCollection collection, OpCode opCode, object operand = null)
			: base(collection)
		{
			Exception ex(params Type[] types)
			{
				if (types.Length == 0)
					throw new ArgumentException($"The OpCode {opCode} does not require an operand, found {operand?.GetType().Name}.", nameof(operand));
				if (types.Length == 1)
					return new ArgumentException($"The OpCode {opCode} requires a {types[0]} as operand, found {operand?.GetType().Name}", nameof(operand));
				return new ArgumentException($"The OpCode {opCode} requires a {string.Join(" or ", types.Select(t => t.Name))} as operand, found {operand?.GetType().Name}", nameof(operand));
			}

			switch (opCode.OperandType)
			{
				case OperandType.InlineBrTarget:
					if (!(operand is Label)) throw ex(typeof(Label));
					break;
				case OperandType.InlineField:
					if (!(operand is FieldInfo)) throw ex(typeof(FieldInfo));
					break;
				case OperandType.InlineI:
					if (!(operand is int)) throw ex(typeof(int));
					break;
				case OperandType.InlineI8:
					if (!(operand is long)) throw ex(typeof(long));
					break;
				case OperandType.InlineMethod:
					if (!(operand is InlineMethod)) throw ex(typeof(InlineMethod));
					break;
				case OperandType.InlineNone:
					if(operand != null) throw ex();
					break;
				case OperandType.InlineR:
					if (!(operand is double)) throw ex(typeof(double));
					break;
				case OperandType.InlineSig:
					if (!(operand is InlineSig)) throw ex(typeof(InlineSig));
					break;
				case OperandType.InlineString:
					if (!(operand is string)) throw ex(typeof(string));
					break;
				case OperandType.InlineSwitch:
					if (!(operand is IEnumerable<Label>)) throw ex(typeof(IEnumerable<Label>));
					break;
				case OperandType.InlineTok:
					if (!(operand is Type) && !(operand is MethodInfo) && !(operand is FieldInfo)) throw ex(typeof(Type), typeof(MethodInfo), typeof(FieldInfo));
					break;
				case OperandType.InlineType:
					if (!(operand is Type)) throw ex(typeof(Type));
					break;
				case OperandType.InlineVar:
					if (!(operand is Local) && !(operand is short)) throw ex(typeof(Local), typeof(short));
					break;
				case OperandType.ShortInlineBrTarget:
					if (!(operand is Label)) throw ex(typeof(Label));
					break;
				case OperandType.ShortInlineI:
					if (!(operand is byte)) throw ex(typeof(byte));
					break;
				case OperandType.ShortInlineR:
					if (!(operand is float)) throw ex(typeof(float));
					break;
				case OperandType.ShortInlineVar:
					if (!(operand is Local) && !(operand is byte)) throw ex(typeof(Local), typeof(byte));
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(opCode));
			}

			this.OpCode = opCode;
			this.Operand = operand;
		}

		#endregion

		#region Methods

		private void operandToString(object operand, StringBuilder str)
		{
			if (operand is Local local)
			{
				str.Append(local.Index);
				str.Append(" // ");

				if (local.Name != null)
				{
					str.Append(local.Name);
					str.Append(' ');
				}
				
				str.Append('(');
				str.Append(local.LocalType.GetIlasmName());
				str.Append(')');
			}
			else if (operand is Label label)
			{
				if (!label.IsMarked)
				{
					str.Append("<Unmarked Label>");
				}
				else
				{
					str.Append("IL_");
					str.Append(label.MarkOperation.ByteOffset.ToString("X4"));
				}

				if (label.Name != null)
				{
					str.Append(" // ");
					str.Append(label.Name);
				}
			}
			else if (operand is string strOperand)
			{
				str.Append('"');

				for (int i = 0; i < strOperand.Length; ++i)
				{
					if (char.IsControl(strOperand[i]))
					{
						str.Append('\\');
						str.Append('x');
						str.Append(((int)strOperand[i]).ToString("X4"));
					}
					else if (strOperand[i] == '"')
					{
						str.Append("\\\"");
					}
					else
					{
						str.Append(strOperand[i]);
					}
				}

				str.Append('"');
			}
			else if (operand is Type type)
			{
				str.Append(type.GetIlasmName());
			}
			else if (operand is InlineMethod method)
			{
				str.Append(method.Method.GetIlasmName());
			}
			else if (operand is FieldInfo field)
			{
				str.Append(field.GetIlasmName());
			}
			else
			{
				str.Append(operand);	
			}
		}

		internal override void ToString(StringBuilder str)
		{
			if (!this.Prefixes.Any())
			{
				this.AppendByteOffsetLabel(str);
			}

			string opCodeStr = this.OpCode.ToString();
			str.Append(opCodeStr);

			if (this.OpCode.OpCodeType == OpCodeType.Prefix)
			{
				if (this.Operand != null)
				{
					this.operandToString(this.Operand, str);
					str.Append('.');
				}
			}
			else
			{
				if (this.Operand != null)
				{
					for (int i = opCodeStr.Length; i < 12; ++i)
					{
						str.Append(' ');
					}
					this.operandToString(this.Operand, str);
				}
				str.AppendLine();
			}	
		}

		internal static OpCode GetShortOpCode(OpCode op)
		{
			if (op == OpCodes.Beq) return OpCodes.Beq_S;
			if (op == OpCodes.Bge) return OpCodes.Bge_S;
			if (op == OpCodes.Bge_Un) return OpCodes.Bge_Un_S;
			if (op == OpCodes.Bgt) return OpCodes.Bgt_S;
			if (op == OpCodes.Bgt_Un) return OpCodes.Bgt_Un_S;
			if (op == OpCodes.Ble) return OpCodes.Ble_S;
			if (op == OpCodes.Ble_Un) return OpCodes.Ble_Un_S;
			if (op == OpCodes.Blt) return OpCodes.Blt_S;
			if (op == OpCodes.Blt_Un) return OpCodes.Blt_Un_S;
			if (op == OpCodes.Bne_Un) return OpCodes.Bne_Un_S;
			if (op == OpCodes.Br) return OpCodes.Br_S;
			if (op == OpCodes.Brtrue) return OpCodes.Brtrue_S;
			if (op == OpCodes.Brfalse) return OpCodes.Brfalse_S;
			if (op == OpCodes.Leave) return OpCodes.Leave_S;
			throw new ArgumentException("Invalid OpCode.", nameof(op));
		}

		private static int getVarIndex(object operand)
		{
			if (operand is byte b)
				return b;

			if (operand is short s)
				return s;

			if (operand is Local loc)
				return loc.Index;

			throw new ArgumentException("Operand is not an InlineVar.", nameof(operand));
		}

		/// <summary>
		/// Returns the optimal or correct opcode.
		/// </summary>
		/// <param name="op"></param>
		/// <param name="operand"></param>
		/// <returns>True, if the change is necessary (e.g. because a branch is too far for a Br_s). False, if it is an optional optimization.</returns>
		internal OpCodeTransformResult TransformInstruction(ref OpCode op, ref object operand)
		{
			if (op == OpCodes.Ldarg || op == OpCodes.Ldarg_S)
			{
				int index = getVarIndex(operand);

				if (index == 0)
				{
					op = OpCodes.Ldarg_0;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index == 1)
				{
					op = OpCodes.Ldarg_1;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index == 2)
				{
					op = OpCodes.Ldarg_2;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index == 3)
				{
					op = OpCodes.Ldarg_3;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index <= byte.MaxValue && op != OpCodes.Ldarg_S)
				{
					op = OpCodes.Ldarg_S;
					operand = (byte)index;
					return OpCodeTransformResult.Optimization;
				}

				if (index > byte.MaxValue && op != OpCodes.Ldarg)
				{
					op = OpCodes.Ldarg;
					operand = (short)index;
					return OpCodeTransformResult.Required;
				}
			}
			else if (op == OpCodes.Starg || op == OpCodes.Starg_S)
			{
				int index = getVarIndex(operand);

				if (index <= byte.MaxValue && op != OpCodes.Starg_S)
				{
					op = OpCodes.Starg_S;
					operand = (byte)index;
					return OpCodeTransformResult.Optimization;
				}

				if (index > byte.MaxValue && op != OpCodes.Starg)
				{
					op = OpCodes.Starg;
					operand = (short)index;
					return OpCodeTransformResult.Required;
				}
			}
			else if (op == OpCodes.Ldarga || op == OpCodes.Ldarga_S)
			{
				int index = getVarIndex(operand);

				if (index <= byte.MaxValue && op != OpCodes.Ldarga_S)
				{
					op = OpCodes.Ldarga_S;
					operand = (byte)index;
					return OpCodeTransformResult.Optimization;
				}

				if (index > byte.MaxValue && op != OpCodes.Ldarga)
				{
					op = OpCodes.Ldarga;
					operand = (short)index;
					return OpCodeTransformResult.Required;
				}
			}
			else if (op == OpCodes.Ldloc_S || op == OpCodes.Ldloc)
			{
				int index = getVarIndex(operand);

				if (index == 0)
				{
					op = OpCodes.Ldloc_0;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index == 1)
				{
					op = OpCodes.Ldloc_1;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index == 2)
				{
					op = OpCodes.Ldloc_2;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index == 3)
				{
					op = OpCodes.Ldloc_3;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index <= byte.MaxValue && op != OpCodes.Ldloc_S)
				{
					op = OpCodes.Ldloc_S;
					operand = (byte)index;
					return OpCodeTransformResult.Optimization;
				}

				if (index > byte.MaxValue && op != OpCodes.Ldloc)
				{
					op = OpCodes.Ldloc;
					operand = (short)index;
					return OpCodeTransformResult.Required;
				}
			}
			else if (op == OpCodes.Stloc_S || op == OpCodes.Stloc)
			{
				int index = getVarIndex(operand);

				if (index == 0)
				{
					op = OpCodes.Stloc_0;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index == 1)
				{
					op = OpCodes.Stloc_1;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index == 2)
				{
					op = OpCodes.Stloc_2;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index == 3)
				{
					op = OpCodes.Stloc_3;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (index <= byte.MaxValue && op != OpCodes.Stloc_S)
				{
					op = OpCodes.Stloc_S;
					operand = (byte)index;
					return OpCodeTransformResult.Optimization;
				}

				if (index > byte.MaxValue && op != OpCodes.Stloc)
				{
					op = OpCodes.Stloc;
					operand = (short)index;
					return OpCodeTransformResult.Required;
				}
			}
			else if (op == OpCodes.Ldloca || op == OpCodes.Ldloca_S)
			{
				int index = getVarIndex(operand);

				if (index <= byte.MaxValue && op != OpCodes.Ldloca_S)
				{
					op = OpCodes.Ldloca_S;
					operand = (byte)index;
					return OpCodeTransformResult.Optimization;
				}

				if (index > byte.MaxValue && op != OpCodes.Ldloca)
				{
					op = OpCodes.Ldloca;
					operand = (short)index;
					return OpCodeTransformResult.Required;
				}
			}
			else if (op == OpCodes.Ldc_I4 || op == OpCodes.Ldc_I4_S)
			{
				int value = op == OpCodes.Ldc_I4 ? (int)operand : (byte)operand;
				if (value == -1)
				{
					op = OpCodes.Ldc_I4_M1;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value == 0)
				{
					op = OpCodes.Ldc_I4_0;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value == 1)
				{
					op = OpCodes.Ldc_I4_1;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value == 2)
				{
					op = OpCodes.Ldc_I4_2;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value == 3)
				{
					op = OpCodes.Ldc_I4_3;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value == 4)
				{
					op = OpCodes.Ldc_I4_4;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value == 5)
				{
					op = OpCodes.Ldc_I4_5;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value == 6)
				{
					op = OpCodes.Ldc_I4_6;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value == 7)
				{
					op = OpCodes.Ldc_I4_7;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value == 8)
				{
					op = OpCodes.Ldc_I4_8;
					operand = null;
					return OpCodeTransformResult.Optimization;
				}

				if (value >= sbyte.MinValue && value <= sbyte.MaxValue)
				{
					op = OpCodes.Ldc_I4_S;
					operand = unchecked((byte)value);
					return OpCodeTransformResult.Optimization;
				}
			}
			else if (op == OpCodes.Beq ||
			         op == OpCodes.Bge ||
			         op == OpCodes.Bge_Un ||
			         op == OpCodes.Bgt ||
			         op == OpCodes.Bgt_Un ||
			         op == OpCodes.Ble ||
			         op == OpCodes.Ble_Un ||
			         op == OpCodes.Blt ||
			         op == OpCodes.Blt_Un ||
			         op == OpCodes.Bne_Un ||
			         op == OpCodes.Br ||
			         op == OpCodes.Brtrue ||
			         op == OpCodes.Brfalse ||
			         op == OpCodes.Leave)
			{
				OpCode shortOp = GetShortOpCode(op);
				int baseOffset = this.ByteOffset + shortOp.Size + 1; // Op + short branch operand.

				Label dest = (Label)operand;
				Operation mark = dest.MarkOperation;
				int diff = mark.ByteOffset - baseOffset;

				if (diff >= sbyte.MinValue && diff <= sbyte.MaxValue)
				{
					op = shortOp;
					return OpCodeTransformResult.Optimization;
				}
			}

			return OpCodeTransformResult.NoChange;
		}

		/// <summary>
		/// Changes the op code to the optimal or correct op code for the specified operand, without changing the semantics.
		/// For example, Br might become Br_S if the branch is short enough, or a Br_S might become a Br if the branch is too far.
		/// </summary>
		internal void Transform(bool optimize)
		{
			OpCode op = this.OpCode;
			object operand = this.Operand;

			OpCodeTransformResult result = this.TransformInstruction(ref op, ref operand);

			if (result == OpCodeTransformResult.NoChange)
				return;

			if (result == OpCodeTransformResult.Required || (result == OpCodeTransformResult.Optimization && optimize))
			{
				this.OpCode = op;
				this.Operand = operand;
				this.Collection.InvalidateOffsets(this);
			}
		}

		#endregion
	}
}
