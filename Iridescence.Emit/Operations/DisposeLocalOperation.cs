﻿using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents the disposal of a <see cref="Local"/> as a pseudo-operation.
	/// </summary>
	public sealed class DisposeLocalOperation : Operation
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		/// <summary>
		/// Gets the local that was disposed.
		/// </summary>
		public Local Local { get; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="DisposeLocalOperation"/> class. 
		/// </summary>
		internal DisposeLocalOperation(OperationCollection collection, Local local)
			: base(collection)
		{
			this.Local = local;
		}
		
		#endregion
		
		#region Methods

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine($"// Local {this.Local} is disposed.");
		}
		
		#endregion
	}
}
