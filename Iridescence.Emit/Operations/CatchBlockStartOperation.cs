using System;
using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Begins a catch block.
	/// </summary>
	public sealed class CatchBlockStartOperation : Operation
	{
		public Type ExceptionType { get; }

		public override int ByteSize
		{
			get
			{
				ExceptionBlockState state = this.Collection.GetExceptionState(this.OperationIndex);

				if (state == ExceptionBlockState.Filter)
					return OpCodes.Endfilter.Size;

				return OpCodes.Leave.Size + 4;
			}
		}

		internal CatchBlockStartOperation(OperationCollection collection, Type exceptionType)
			: base(collection)
		{
			this.ExceptionType = exceptionType;
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine($"// Start of catch block for exception \"{this.ExceptionType}\".");
		}
	}
}