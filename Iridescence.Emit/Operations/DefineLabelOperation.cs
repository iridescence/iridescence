using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Defines a <see cref="Label"/>.
	/// </summary>
	public sealed class DefineLabelOperation : Operation
	{
		#region Fields

		#endregion

		#region Properties

		public Label Label { get; }

		#endregion

		#region Constructors

		internal DefineLabelOperation(OperationCollection collection, Label label)
			: base(collection)
		{
			this.Label = label;
		}

		#endregion

		#region Methods

		internal override void ToString(StringBuilder str)
		{
			
		}


		#endregion

	}
}