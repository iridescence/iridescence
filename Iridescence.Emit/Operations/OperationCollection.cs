﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents a collection low-level IL operations that can be replayed back onto an <see cref="ILGenerator"/>.
	/// </summary>
	public sealed class OperationCollection : IReadOnlyList<Operation>
	{
		#region Fields

		private readonly List<Operation> operations;
		private int localIndex;

		private int? byteSize;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the total number of buffered operations.
		/// </summary>
		public int Count => this.operations.Count;

		/// <summary>
		/// Gets the number of IL <see cref="Instruction">instructions</see> in the buffer.
		/// </summary>
		public int InstructionCount { get; private set; }
		
		/// <summary>
		/// Gets the total size (in bytes) of the byte stream.
		/// </summary>
		public int ByteSize
		{
			get { return this.byteSize ?? (this.byteSize = this.operations.Sum(o => o.ByteSize)).Value; }
		}

		/// <summary>
		/// Gets the operation at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public Operation this[int index] => this.operations[index];

		#endregion

		#region Constructors

		public OperationCollection()
		{
			this.operations = new List<Operation>();
			this.localIndex = 0;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds the specified <see cref="Operation"/>
		/// </summary>
		/// <param name="op">The operation to add.</param>
		/// <returns>The operation, for convenience.</returns>
		private TOp add<TOp>(TOp op) where TOp : Operation
		{
			return this.insert(this.Count, op);
		}

		/// <summary>
		/// Inserts the specified <see cref="Operation"/> at the index.
		/// </summary>
		/// <param name="index">The index to insert the instruction at.</param>
		/// <param name="op">The operation to add.</param>
		/// <returns>The operation, for convenience.</returns>
		private TOp insert<TOp>(int index, TOp op) where TOp : Operation
		{
			this.operations.Insert(index, op);

			// Fix indices.
			for (int i = index; i < this.Count; ++i)
			{
				this[i].OperationIndex = i;
			}

			if (op is Instruction)
			{
				++this.InstructionCount;
			}

			this.InvalidateOffsets(index);
			this.byteSize = null;

			return op;
		}

		/// <summary>
		/// Removes the specified <see cref="Operation"/>.
		/// </summary>
		/// <param name="op"></param>
		/// <returns></returns>
		public void Remove(Operation op)
		{
			if(!ReferenceEquals(op.Collection, this))
				throw new ArgumentException("Operation does not belong to this collection.", nameof(op));

			this.RemoveAt(op.OperationIndex);
		}

		/// <summary>
		/// Removes the <see cref="Operation"/> at the specified index.
		/// </summary>
		/// <param name="index"></param>
		public void RemoveAt(int index)
		{
			if (unchecked((uint)index) >= this.Count)
				throw new ArgumentOutOfRangeException(nameof(index));

			Operation op = this[index];
			this.operations.RemoveAt(index);

			op.RemoveFromCollection();

			// Fix indices.
			for (int i = index; i < this.Count; ++i)
			{
				this[i].OperationIndex = i;
			}

			if (op is Instruction)
			{
				--this.InstructionCount;
			}

			this.InvalidateOffsets(index);
			this.byteSize = null;
		}

		internal void InvalidateOffsets(Operation startingFrom)
		{
			this.InvalidateOffsets(startingFrom.OperationIndex);
		}

		internal void InvalidateOffsets(int startingFrom)
		{
			for (int i = startingFrom; i < this.Count; ++i)
			{
				this[i].InvalidateByteOffset();
			}
		}

		/// <summary>
		/// Replaces instructions with shorter (or longer) representations if possible.
		/// </summary>
		internal void TransformInstructions(bool optimize)
		{
			for (int i = 0; i < this.Count; ++i)
			{
				if (this[i] is Instruction ins)
				{
					ins.Transform(optimize);
				}
			}
		}

		internal ExceptionBlockState GetExceptionState(int index)
		{
			if (unchecked((uint)index) >= this.Count)
				throw new ArgumentOutOfRangeException(nameof(index));

			for (int j = index; j >= 0; --j)
			{
				if (this[j] is ExceptionBlockStartOperation)
					return ExceptionBlockState.Try;

				if (this[j] is CatchBlockStartOperation)
					return ExceptionBlockState.Catch;

				if (this[j] is FinallyBlockStartOperation)
					return ExceptionBlockState.Finally;

				if (this[j] is FilterBlockStartOperation)
					return ExceptionBlockState.Filter;

				if (this[j] is FaultBlockStartOperation)
					return ExceptionBlockState.Fault;

				if (this[j] is ExceptionBlockEndOperation)
					return ExceptionBlockState.None;			
			}

			return ExceptionBlockState.None;
		}

		public Instruction Add(OpCode op)
		{
			return this.Add(this.Count, op);
		}

		public Instruction Add(int index, OpCode op)
		{
			return this.insert(index, new Instruction(this, op));
		}


		public Instruction Add(OpCode op, byte operand)
		{
			return this.Add(this.Count, op, operand);
		}

		public Instruction Add(int index, OpCode op, byte operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}


		public Instruction Add(OpCode op, short operand)
		{
			return this.Add(this.Count, op, operand);
		}

		public Instruction Add(int index, OpCode op, short operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}


		public Instruction Add(OpCode op, int operand)
		{
			return this.Add(this.Count, op, operand);
		}

		public Instruction Add(int index, OpCode op, int operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}

		
		public Instruction Add(OpCode op, long operand)
		{
			return this.Add(this.Count, op, operand);
		}

		public Instruction Add(int index, OpCode op, long operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}


		public Instruction Add(OpCode op, float operand)
		{
			return this.Add(this.Count, op, operand);
		}

		public Instruction Add(int index, OpCode op, float operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}


		public Instruction Add(OpCode op, double operand)
		{
			return this.Add(this.Count, op, operand);
		}

		public Instruction Add(int index, OpCode op, double operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}


		public Instruction Add(OpCode op, Type operand)
		{
			return this.Add(this.Count, op, operand);
		}

		public Instruction Add(int index, OpCode op, Type operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}


		public Instruction Add(OpCode op, FieldInfo operand)
		{
			return this.Add(this.Count, op, operand);
		}

		public Instruction Add(int index, OpCode op, FieldInfo operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}


		public Instruction Add(OpCode op, MethodBase operand)
		{
			return this.Add(this.Count, op, operand);
		}
		
		public Instruction Add(int index, OpCode op, MethodBase operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}


		public Instruction Add(OpCode op, string operand)
		{
			return this.Add(this.Count, op, operand);
		}

		public Instruction Add(int index, OpCode op, string operand)
		{
			return this.insert(index, new Instruction(this, op, operand));
		}


		public Instruction Add(OpCode op, Label label)
		{
			return this.Add(this.Count, op, label);
		}

		public Instruction Add(int index, OpCode op, Label label)
		{
			return this.insert(index, new Instruction(this, op, label));
		}


		public Instruction Add(OpCode op, Label[] labels)
		{
			return this.Add(this.Count, op, labels);
		}

		public Instruction Add(int index, OpCode op, Label[] labels)
		{
			return this.insert(index, new Instruction(this, op, labels));
		}


		public Instruction Add(OpCode op, Local local)
		{
			return this.Add(this.Count, op, local);
		}

		public Instruction Add(int index, OpCode op, Local local)
		{
			return this.insert(index, new Instruction(this, op, local));
		}


		public Instruction Add(OpCode op, InlineMethod method)
		{
			return this.Add(this.Count, op, method);
		}

		public Instruction Add(int index, OpCode op, InlineMethod method)
		{
			return this.insert(index, new Instruction(this, op, method));
		}


		public Instruction AddCalli(InlineSig sig)
		{
			return this.InsertCalli(this.Count, sig);
		}

		public Instruction InsertCalli(int index, InlineSig sig)
		{
			return this.insert(index, new Instruction(this, OpCodes.Calli, sig));
		}


		public ExceptionBlockStartOperation BeginExceptionBlock()
		{
			return this.BeginExceptionBlock(this.Count);
		}

		public ExceptionBlockStartOperation BeginExceptionBlock(int index)
		{
			Label label = new Label(null);
			ExceptionBlockStartOperation op = new ExceptionBlockStartOperation(this, label);
			label.DefineOperation = op;
			return this.insert(index, op);
		}


		public ExceptionBlockEndOperation EndExceptionBlock()
		{
			return this.EndExceptionBlock(this.Count);
		}

		public ExceptionBlockEndOperation EndExceptionBlock(int index)
		{
			return this.insert(index, new ExceptionBlockEndOperation(this));
		}


		public CatchBlockStartOperation BeginCatchBlock(Type exception)
		{
			return this.BeginCatchBlock(this.Count, exception);
		}

		public CatchBlockStartOperation BeginCatchBlock(int index, Type exception)
		{
			return this.insert(index, new CatchBlockStartOperation(this, exception));
		}


		public CatchBlockEndOperation EndCatchBlock()
		{
			return this.EndCatchBlock(this.Count);
		}

		public CatchBlockEndOperation EndCatchBlock(int index)
		{
			return this.insert(index, new CatchBlockEndOperation(this));
		}


		public FinallyBlockStartOperation BeginFinallyBlock()
		{
			return this.BeginFinallyBlock(this.Count);
		}

		public FinallyBlockStartOperation BeginFinallyBlock(int index)
		{
			return this.insert(index, new FinallyBlockStartOperation(this));
		}


		public FinallyBlockEndOperation EndFinallyBlock()
		{
			return this.EndFinallyBlock(this.Count);
		}

		public FinallyBlockEndOperation EndFinallyBlock(int index)
		{
			return this.insert(index, new FinallyBlockEndOperation(this));
		}


		public FaultBlockStartOperation BeginFaultBlock()
		{
			return this.BeginFaultBlock(this.Count);
		}

		public FaultBlockStartOperation BeginFaultBlock(int index)
		{
			return this.insert(index, new FaultBlockStartOperation(this));
		}


		public FaultBlockEndOperation EndFaultBlock()
		{
			return this.EndFaultBlock(this.Count);
		}

		public FaultBlockEndOperation EndFaultBlock(int index)
		{
			return this.insert(index, new FaultBlockEndOperation(this));
		}


		public FilterBlockStartOperation BeginFilterBlock()
		{
			return this.BeginFilterBlock(this.Count);
		}

		public FilterBlockStartOperation BeginFilterBlock(int index)
		{
			return this.insert(index, new FilterBlockStartOperation(this));
		}


		public FilterBlockEndOperation EndFilterBlock()
		{
			return this.EndFilterBlock(this.Count);
		}

		public FilterBlockEndOperation EndFilterBlock(int index)
		{
			return this.insert(index, new FilterBlockEndOperation(this));
		}


		public DefineLabelOperation DefineLabel(string name = null)
		{
			return this.DefineLabel(this.Count, name);
		}

		public DefineLabelOperation DefineLabel(int index, string name = null)
		{
			Label label = new Label(name);
			DefineLabelOperation op = new DefineLabelOperation(this, label);
			label.DefineOperation = op;
			return this.insert(index, op);
		}
		

		public MarkLabelOperation MarkLabel(Label label)
		{
			return this.MarkLabel(this.Count, label);
		}

		public MarkLabelOperation MarkLabel(int index, Label label)
		{
			if(!ReferenceEquals(label.DefineOperation?.Collection, this))
				throw new ArgumentException($"The specified label does not belong to this {nameof(OperationCollection)}", nameof(label));

			MarkLabelOperation op = new MarkLabelOperation(this, label);
			label.MarkOperation = op;

			return this.insert(index, op);
		}


		public Local DeclareLocal(Type localType, string name = null, bool pinned = false)
		{
			return this.DeclareLocal(this.Count, localType, name, pinned);
		}

		public Local DeclareLocal(int index, Type localType, string name = null, bool pinned = false)
		{
			if (localType == null)
				throw new ArgumentNullException(nameof(localType));

			Local local = new Local(localType, this.localIndex++, name, this.disposeLocal, pinned);
			DeclareLocalOperation op = new DeclareLocalOperation(this, local);
			local.DeclarationOperation = op;
			this.insert(index, op);
			return local;
		}


		public Local ReuseLocal(Local local, string newName = null)
		{
			return this.ReuseLocal(this.Count, local, newName);
		}

		public Local ReuseLocal(int index, Local local, string newName = null)
		{
			if (!local.IsDisposed)
				throw new ArgumentException("Can't reuse a local that is still in use. Dispose the local first.", nameof(local));

			Local newLocal = new Local(local, newName);
			ReuseLocalOperation op = new ReuseLocalOperation(this, local, newLocal);
			newLocal.DeclarationOperation = op;
			this.add(op);
			return newLocal;
		}


		private void disposeLocal(Local local)
		{
			DisposeLocalOperation op = new DisposeLocalOperation(this, local);
			local.DisposeOperation = op;
			this.add(op);
		}

		internal void ToString(StringBuilder str)
		{
			for (int i = 0; i < this.Count; ++i)
			{
				this[i].ToString(str);
			}
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();
			this.ToString(str);
			return str.ToString();
		}

		public IEnumerator<Operation> GetEnumerator()
		{
			return this.operations.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)this.operations).GetEnumerator();
		}
		
		#endregion
	}
}
