﻿namespace Iridescence.Emit
{
	/// <summary>
	/// Defines the exception block states.
	/// </summary>
	public enum ExceptionBlockState
	{
		None,
		Try,
		Catch,
		Finally,
		Filter,
		Fault,
	}
}
