using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Ends a finally block. This operation does nothing and is only used for debugging or commenting.
	/// </summary>
	public sealed class FinallyBlockEndOperation : Operation
	{
		internal FinallyBlockEndOperation(OperationCollection collection)
			: base(collection)
		{
			
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine("// End of finally block.");
		}
	}
}