﻿using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents the reuse of a <see cref="Local"/> as a pseudo-operation.
	/// </summary>
	public sealed class ReuseLocalOperation : Operation
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		/// <summary>
		/// Gets the old local that is being reused.
		/// </summary>
		public Local OldLocal { get; }

		/// <summary>
		/// Gets the new local that takes the place of the old local.
		/// </summary>
		public Local NewLocal { get; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ReuseLocalOperation"/> class. 
		/// </summary>
		internal ReuseLocalOperation(OperationCollection collection, Local oldLocal, Local newLocal)
			: base(collection)
		{
			this.OldLocal = oldLocal;
			this.NewLocal = newLocal;
		}
		
		#endregion
		
		#region Methods

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine($"// Local {this.OldLocal} is reused as {this.NewLocal}.");
		}

		#endregion
	}
}
