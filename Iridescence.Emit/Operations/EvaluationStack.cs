﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents the IL evaluation stack.
	/// </summary>
	internal readonly struct EvaluationStack : IReadOnlyList<Type>, IEquatable<EvaluationStack>
	{
		#region Fields

		private readonly ReadOnlyMemory<Type> memory;

		#endregion

		#region Properties

		public int Count => this.memory.Length;

		public Type this[int index] => this.memory.Span[index];

		public static EvaluationStack Empty { get; } = new EvaluationStack(ReadOnlyMemory<Type>.Empty);

		#endregion

		#region Constructors

		internal EvaluationStack(ReadOnlyMemory<Type> types)
		{
			this.memory = types;
		}

		#endregion

		#region Methods

		public bool TryTransitions(IEnumerable<IStackTransition> transitions, out IStackTransition chosenTransition, out EvaluationStack stack)
		{
			foreach (IStackTransition transition in transitions)
			{
				EvaluationStack current = this;
				if (transition.Apply(ref current))
				{
					chosenTransition = transition;
					stack = current;
					return true;
				}
			}

			chosenTransition = null;
			stack = default;
			return false;
		}

		public EvaluationStack Push(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			Type[] newBuffer = new Type[this.memory.Length + 1];
			this.memory.Span.CopyTo(new Span<Type>(newBuffer, 0, this.memory.Length));
			newBuffer[this.memory.Length] = type;
			return new EvaluationStack(new ReadOnlyMemory<Type>(newBuffer));
		}

		public EvaluationStack Push(params Type[] types)
		{
			if (types == null)
				throw new ArgumentNullException(nameof(types));

			if (types.Length == 0)
				return this;

			if (types.Any(t => t == null))
				throw new ArgumentNullException(nameof(types));

			Type[] newBuffer = new Type[this.memory.Length + types.Length];
			this.memory.Span.CopyTo(new Span<Type>(newBuffer, 0, this.memory.Length));
			Array.Copy(types, 0, newBuffer, this.memory.Length, types.Length);
			return new EvaluationStack(new ReadOnlyMemory<Type>(newBuffer));
		}

		public bool TryPop(Predicate<Type> typePredicate, out Type type, out EvaluationStack stack)
		{
			if (this.Count == 0)
			{
				stack = default;
				type = null;
				return false;
			}

			Type lastType = this.memory.Span[this.memory.Length - 1];
			if (!typePredicate(lastType))
			{
				stack = default;
				type = null;
				return false;
			}

			stack = new EvaluationStack(this.memory.Slice(0, this.memory.Length - 1));
			type = lastType;
			return true;
		}

		public IEnumerator<Type> GetEnumerator()
		{
			for (int i = 0; i < this.memory.Length; ++i)
			{
				yield return this.memory.Span[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		public bool IsCompatible(in EvaluationStack other)
		{
			if (this.Count != other.Count)
				return false;

			for (int i = 0; i < this.Count; ++i)
			{
				Type typeA = this[i].GetStackType();
				Type typeB = this[i].GetStackType();

				if (typeA == typeB)
					continue;

				if (typeA == typeof(NullType) && !typeB.IsValueType)
					continue;

				if (typeB == typeof(NullType) && !typeA.IsValueType)
					continue;

				return false;
			}

			return true;
		}

		public bool Equals(in EvaluationStack other)
		{
			if (this.Count != other.Count)
				return false;

			for (int i = 0; i < this.Count; ++i)
			{
				if (this[i] != other[i])
					return false;
			}

			return true;
		}

		bool IEquatable<EvaluationStack>.Equals(EvaluationStack other) => this.Equals(other);

		public override bool Equals(object obj)
		{
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((EvaluationStack)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hash = this.Count;
				foreach (Type type in this)
					hash = (hash * 397) ^ type.GetHashCode();
				return hash;
			}
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			int index = 0;
			foreach (Type type in this)
			{
				if (index > 0)
					str.AppendLine();

				Type stackType = type.GetStackType();

				str.Append("[");
				str.Append(index++);
				str.Append("]: ");
				str.Append(stackType);

				if (stackType != type)
				{
					str.Append(" (");
					str.Append(type);
					str.Append(")");
				}
			}

			return str.ToString();
		}

		#endregion
	}
}