﻿using System;
using System.Globalization;
using System.Reflection;

namespace Iridescence.Emit
{
	internal sealed class NullType
	{
		private NullType() { }
	}

	internal sealed class NativeInt
	{
		private NativeInt() { }
	}

	internal sealed class AnyPointer
	{
		private AnyPointer() { }
	}

	internal sealed class AnyByRef
	{
		private AnyByRef() { }
	}

	internal sealed class BoxedType : Type
	{
		public Type ValueType { get; }

		public override bool IsDefined(Type attributeType, bool inherit) => this.ValueType.IsDefined(attributeType, inherit);
		public override Module Module => this.ValueType.Module;
		public override string Namespace => this.ValueType.Namespace;
		public override string Name => this.ValueType.Name;
		public override Assembly Assembly => this.ValueType.Assembly;
		public override string AssemblyQualifiedName => this.ValueType.AssemblyQualifiedName;
		public override Type BaseType => this.ValueType.BaseType;
		public override string FullName => this.ValueType.FullName;
		public override Guid GUID => this.ValueType.GUID;

		public BoxedType(Type valueType)
		{
			if (valueType == null)
				throw new ArgumentNullException(nameof(valueType));

			if (!valueType.IsValueType)
				throw new ArgumentException("Type must be a value type.", nameof(valueType));

			this.ValueType = valueType;
		}

		protected override bool IsValueTypeImpl() => false;
		public override object[] GetCustomAttributes(bool inherit) => this.ValueType.GetCustomAttributes(inherit);
		public override object[] GetCustomAttributes(Type attributeType, bool inherit) => this.ValueType.GetCustomAttributes(attributeType, inherit);
		protected override TypeAttributes GetAttributeFlagsImpl() => this.ValueType.Attributes;
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers) => this.ValueType.GetConstructor(bindingAttr, binder, callConvention, types, modifiers);
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr) => this.ValueType.GetConstructors(bindingAttr);
		public override Type GetElementType() => this.ValueType.GetElementType();
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr) => this.ValueType.GetEvent(name, bindingAttr);
		public override EventInfo[] GetEvents(BindingFlags bindingAttr) => this.ValueType.GetEvents(bindingAttr);
		public override FieldInfo GetField(string name, BindingFlags bindingAttr) => this.ValueType.GetField(name, bindingAttr);
		public override FieldInfo[] GetFields(BindingFlags bindingAttr) => this.ValueType.GetFields(bindingAttr);
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr) => this.ValueType.GetMembers(bindingAttr);
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers) => this.ValueType.GetMethod(name, bindingAttr, binder, callConvention, types, modifiers);
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr) => this.ValueType.GetMethods(bindingAttr);
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr) => this.ValueType.GetProperties(bindingAttr);
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters) => this.ValueType.InvokeMember(name, invokeAttr, binder, target, args, modifiers, culture, namedParameters);
		public override Type UnderlyingSystemType => this.ValueType.UnderlyingSystemType;
		protected override bool IsArrayImpl() => this.ValueType.IsArray;
		protected override bool IsByRefImpl() => this.ValueType.IsByRef;
		protected override bool IsCOMObjectImpl() => this.ValueType.IsCOMObject;
		protected override bool IsPointerImpl() => this.ValueType.IsPointer;
		protected override bool IsPrimitiveImpl() => this.ValueType.IsPrimitive;
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers) => this.ValueType.GetProperty(name, bindingAttr, binder, returnType, types, modifiers);
		protected override bool HasElementTypeImpl() => this.ValueType.HasElementType;
		public override Type GetNestedType(string name, BindingFlags bindingAttr) => this.ValueType.GetNestedType(name, bindingAttr);
		public override Type[] GetNestedTypes(BindingFlags bindingAttr) => this.ValueType.GetNestedTypes(bindingAttr);
		public override Type GetInterface(string name, bool ignoreCase) => this.ValueType.GetInterface(name, ignoreCase);
		public override Type[] GetInterfaces() => this.ValueType.GetInterfaces();
		public override int GetHashCode() => -this.ValueType.GetHashCode();
		public override bool Equals(Type o) => o is BoxedType t && t.ValueType == this.ValueType;
		public override string ToString() => $"Boxed {this.ValueType}";
	}

	internal sealed class PinnedType : Type
	{
		public Type ElementType { get; }

		public override bool IsDefined(Type attributeType, bool inherit) => this.ElementType.IsDefined(attributeType, inherit);
		public override Module Module => this.ElementType.Module;
		public override string Namespace => this.ElementType.Namespace;
		public override string Name => this.ElementType.Name;
		public override Assembly Assembly => this.ElementType.Assembly;
		public override string AssemblyQualifiedName => this.ElementType.AssemblyQualifiedName;
		public override Type BaseType => this.ElementType.BaseType;
		public override string FullName => this.ElementType.FullName;
		public override Guid GUID => this.ElementType.GUID;

		public PinnedType(Type elementType)
		{
			this.ElementType = elementType ?? throw new ArgumentNullException(nameof(elementType));
		}

		protected override bool IsValueTypeImpl() => false;
		public override object[] GetCustomAttributes(bool inherit) => this.ElementType.GetCustomAttributes(inherit);
		public override object[] GetCustomAttributes(Type attributeType, bool inherit) => this.ElementType.GetCustomAttributes(attributeType, inherit);
		protected override TypeAttributes GetAttributeFlagsImpl() => this.ElementType.Attributes;
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers) => this.ElementType.GetConstructor(bindingAttr, binder, callConvention, types, modifiers);
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr) => this.ElementType.GetConstructors(bindingAttr);
		public override Type GetElementType() => this.ElementType;
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr) => this.ElementType.GetEvent(name, bindingAttr);
		public override EventInfo[] GetEvents(BindingFlags bindingAttr) => this.ElementType.GetEvents(bindingAttr);
		public override FieldInfo GetField(string name, BindingFlags bindingAttr) => this.ElementType.GetField(name, bindingAttr);
		public override FieldInfo[] GetFields(BindingFlags bindingAttr) => this.ElementType.GetFields(bindingAttr);
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr) => this.ElementType.GetMembers(bindingAttr);
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers) => this.ElementType.GetMethod(name, bindingAttr, binder, callConvention, types, modifiers);
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr) => this.ElementType.GetMethods(bindingAttr);
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr) => this.ElementType.GetProperties(bindingAttr);
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters) => this.ElementType.InvokeMember(name, invokeAttr, binder, target, args, modifiers, culture, namedParameters);
		public override Type UnderlyingSystemType => this.ElementType.UnderlyingSystemType;
		protected override bool IsArrayImpl() => this.ElementType.IsArray;
		protected override bool IsByRefImpl() => this.ElementType.IsByRef;
		protected override bool IsCOMObjectImpl() => this.ElementType.IsCOMObject;
		protected override bool IsPointerImpl() => this.ElementType.IsPointer;
		protected override bool IsPrimitiveImpl() => this.ElementType.IsPrimitive;
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers) => this.ElementType.GetProperty(name, bindingAttr, binder, returnType, types, modifiers);
		protected override bool HasElementTypeImpl() => true;
		public override Type GetNestedType(string name, BindingFlags bindingAttr) => this.ElementType.GetNestedType(name, bindingAttr);
		public override Type[] GetNestedTypes(BindingFlags bindingAttr) => this.ElementType.GetNestedTypes(bindingAttr);
		public override Type GetInterface(string name, bool ignoreCase) => this.ElementType.GetInterface(name, ignoreCase);
		public override Type[] GetInterfaces() => this.ElementType.GetInterfaces();
		public override int GetHashCode() => -this.ElementType.GetHashCode();
		public override bool Equals(Type o) => o is PinnedType t && t.ElementType == this.ElementType;
		public override string ToString() => $"Pinned {this.ElementType}";
	}


	internal sealed class MethodPointerType : Type
	{
		public InlineSig Signature { get; }

		public override Module Module => throw new NotImplementedException();
		public override string Namespace => throw new NotImplementedException();
		public override string Name => "Method Pointer";
		public override Assembly Assembly => throw new NotImplementedException();
		public override string AssemblyQualifiedName => throw new NotImplementedException();
		public override Type BaseType => throw new NotImplementedException();
		public override string FullName => throw new NotImplementedException();
		public override Guid GUID => throw new NotImplementedException();

		public MethodPointerType(InlineSig signature)
		{
			this.Signature = signature;
		}

		public override object[] GetCustomAttributes(bool inherit) => throw new NotImplementedException();
		public override object[] GetCustomAttributes(Type attributeType, bool inherit) => throw new NotImplementedException();
		public override bool IsDefined(Type attributeType, bool inherit) => throw new NotImplementedException();
		protected override TypeAttributes GetAttributeFlagsImpl() => throw new NotImplementedException();
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers) => throw new NotImplementedException();
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr) => throw new NotImplementedException();
		public override Type GetElementType() => throw new NotImplementedException();
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr) => throw new NotImplementedException();
		public override EventInfo[] GetEvents(BindingFlags bindingAttr) => throw new NotImplementedException();
		public override FieldInfo GetField(string name, BindingFlags bindingAttr) => throw new NotImplementedException();
		public override FieldInfo[] GetFields(BindingFlags bindingAttr) => throw new NotImplementedException();
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr) => throw new NotImplementedException();
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers) => throw new NotImplementedException();
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr) => throw new NotImplementedException();
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr) => throw new NotImplementedException();
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers) => throw new NotImplementedException();
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters) => throw new NotImplementedException();
		public override Type UnderlyingSystemType => throw new NotImplementedException();
		protected override bool IsArrayImpl() => false;
		protected override bool IsByRefImpl() => false;
		protected override bool IsCOMObjectImpl() => false;
		protected override bool IsPointerImpl() => false;
		protected override bool IsPrimitiveImpl() => false;
		protected override bool HasElementTypeImpl() => throw new NotImplementedException();
		public override Type GetNestedType(string name, BindingFlags bindingAttr) => throw new NotImplementedException();
		public override Type[] GetNestedTypes(BindingFlags bindingAttr) => throw new NotImplementedException();
		public override Type GetInterface(string name, bool ignoreCase) => throw new NotImplementedException();
		public override Type[] GetInterfaces() => throw new NotImplementedException();
		public override string ToString() => $"Method Pointer for \"{this.Signature}\"";

		public override bool Equals(object o)
		{
			if (!(o is MethodPointerType other))
				return false;

			return other.Signature.Equals(this.Signature);
		}

		public override int GetHashCode()
		{
			return this.Signature.GetHashCode();
		}
	}
}
