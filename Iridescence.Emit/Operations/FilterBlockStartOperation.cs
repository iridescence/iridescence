using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Begins a filter block.
	/// </summary>
	public sealed class FilterBlockStartOperation : Operation
	{
		public override int ByteSize => OpCodes.Leave.Size + 4; // Code + branch target.

		internal FilterBlockStartOperation(OperationCollection collection)
			: base(collection)
		{
			
		}

		internal override void ToString(StringBuilder str)
		{
			str.AppendLine("// Start of filter block.");
		}
	}
}