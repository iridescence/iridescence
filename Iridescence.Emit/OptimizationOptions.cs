﻿using System;

namespace Iridescence.Emit
{
	/// <summary>
	/// Sigil can perform optimizations to the emitted IL. This enum tells Sigil which optimizations to perform.
	/// </summary>
	[Flags]
	public enum OptimizationOptions
	{
		/// <summary>
		/// Perform no optional optimizations.
		/// </summary>
		None = 0,

		/// <summary>
		/// Choose short form of an IL instruction where possible.
		/// </summary>
		ShortInstructions = 1,

		/// <summary>
		/// Elide CastClass and IsInstance instructions which are no-ops, such as casting a System.String to a System.Object.
		/// </summary>
		TrivialCastEliding = 2,

		/// <summary>
		/// Turn the last call in the method into a tailcall if possible.
		/// </summary>
		TailCailInjection = 4,

		/// <summary>
		/// Use the readonly prefix where possible.
		/// </summary>
		ReadOnlyInjection = 8,

		/// <summary>
		/// Perform all optimizations.
		/// </summary>
		All = ShortInstructions | TrivialCastEliding | TailCailInjection | ReadOnlyInjection
	}
}