﻿using System;
using System.Reflection;

namespace Iridescence.Emit
{
	public class DummyILBuilder : ILBuilderImpl
	{
		public override CallableMethod Method => throw new NotSupportedException();
		protected override bool IsConstructor { get; }
		protected override Type DeclaringType { get; }

		public DummyILBuilder(MethodSignature signature, CallingConventions callConvention = CallingConventions.Standard, ILBuilderOptions options = ILBuilderOptions.Default, bool isConstructor = false, Type declaringType = null)
			: base(signature, callConvention, options)
		{
			this.IsConstructor = isConstructor;
			this.DeclaringType = declaringType;
		}
	}
}