﻿using System;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents a catch block which appears in an ExceptionBlock.
	/// </summary>
	public class CatchBlock
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the <see cref="ExceptionBlock"/> this <see cref="CatchBlock"/> appears in.
		/// </summary>
		public ExceptionBlock ExceptionBlock { get; }

		/// <summary>
		/// Gets type of exception being caught by this <see cref="CatchBlock"/>.
		/// This property is null when the catch block is preceded by a filter block.
		/// </summary>
		public Type ExceptionType { get; }

		public bool IsOpen => this.EndOperation == null;

		internal Operation StartOperation { get; set; }

		internal Operation EndOperation { get; set; }

		#endregion

		#region Constructors

		internal CatchBlock(Type exceptionType, ExceptionBlock forTry)
		{
			this.ExceptionType = exceptionType;
			this.ExceptionBlock = forTry;
		}
		
		#endregion

		#region Methods

		#endregion
	}
}
