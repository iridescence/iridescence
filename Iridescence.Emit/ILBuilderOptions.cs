﻿using System;

namespace Iridescence.Emit
{
	/// <summary>
	/// Options for emitting code.
	/// </summary>
	[Flags]
	public enum ILBuilderOptions
	{
		/// <summary>
		/// No options.
		/// </summary>
		None = 0,

		/// <summary>
		/// Whether the emitter should verify whether the emitted instructions are valid.
		/// </summary>
		Validate = 1 << 0,

		/// <summary>
		/// Whether the emitter should allow unverifiable/unsafe constructs and instructions.
		/// </summary>
		AllowUnverifiable = 1 << 1,

		/// <summary>
		/// Allow unreachable code.
		/// </summary>
		AllowUnreachable = 1 << 2,

		/// <summary>
		/// Default flags.
		/// </summary>
		Default = Validate | AllowUnverifiable,
	}
}
