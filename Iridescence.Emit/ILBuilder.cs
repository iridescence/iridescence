﻿using System;
using System.Collections.Immutable;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	public abstract class ILBuilderWrapper : ILBuilder
	{
		private readonly ILBuilder inner;

		public override CallableMethod Method => this.inner.Method;

		/// <summary>
		/// Initializes a new instance of the <see cref="ILBuilder"/> class. 
		/// </summary>
		protected ILBuilderWrapper(ILBuilder inner)
		{
			this.inner = inner ?? throw new ArgumentNullException(nameof(ILBuilder));
		}
		
		public override Local DeclareLocal(Type localType, string name = null, bool initializeReused = true, bool pinned = false) => this.inner.DeclareLocal(localType, name, initializeReused, pinned);
		public override Label DefineLabel(string name = null) => this.inner.DefineLabel(name);
		public override void MarkLabel(Label label) => this.inner.MarkLabel(label);
		public override ExceptionBlock BeginExceptionBlock() => this.inner.BeginExceptionBlock();
		public override void EndExceptionBlock(ExceptionBlock exceptionBlock) => this.inner.EndExceptionBlock(exceptionBlock);
		public override CatchBlock BeginCatchBlock(ExceptionBlock forTry, Type exceptionType) => this.inner.BeginCatchBlock(forTry, exceptionType);
		public override void EndCatchBlock(CatchBlock catchBlock) => this.inner.EndCatchBlock(catchBlock);
		public override FilterBlock BeginFilterBlock(ExceptionBlock forTry) => this.inner.BeginFilterBlock(forTry);
		public override void EndFilterBlock(FilterBlock filterBlock) => this.inner.EndFilterBlock(filterBlock);
		public override FinallyBlock BeginFinallyBlock(ExceptionBlock forTry) => this.inner.BeginFinallyBlock(forTry);
		public override void EndFinallyBlock(FinallyBlock finallyBlock) => this.inner.EndFinallyBlock(finallyBlock);

		public override void Add() => this.inner.Add();
		public override void AddOverflow() => this.inner.AddOverflow();
		public override void AddOverflowUnsigned() => this.inner.AddOverflowUnsigned();
		public override void And() => this.inner.And();
		public override void ArgumentList() => this.inner.ArgumentList();
		public override void BranchIfEqual(Label label) => this.inner.BranchIfEqual(label);
		public override void BranchIfGreaterOrEqual(Label label) => this.inner.BranchIfGreaterOrEqual(label);
		public override void BranchIfGreaterOrEqualUnsigned(Label label) => this.inner.BranchIfGreaterOrEqualUnsigned(label);
		public override void BranchIfGreater(Label label) => this.inner.BranchIfGreater(label);
		public override void BranchIfGreaterUnsigned(Label label) => this.inner.BranchIfGreaterUnsigned(label);
		public override void BranchIfLessOrEqual(Label label) => this.inner.BranchIfLessOrEqual(label);
		public override void BranchIfLessOrEqualUnsigned(Label label) => this.inner.BranchIfLessOrEqualUnsigned(label);
		public override void BranchIfLess(Label label) => this.inner.BranchIfLess(label);
		public override void BranchIfLessUnsigned(Label label) => this.inner.BranchIfLessUnsigned(label);
		public override void BranchIfNotEqualUnsigned(Label label) => this.inner.BranchIfNotEqualUnsigned(label);
		public override void Box(Type valueType) => this.inner.Box(valueType);
		public override void Branch(Label label) => this.inner.Branch(label);
		public override void Break() => this.inner.Break();
		public override void BranchIfFalse(Label label) => this.inner.BranchIfFalse(label);
		public override void BranchIfTrue(Label label) => this.inner.BranchIfTrue(label);
		public override void Call(ConstructorInfo constructor, bool tailCall = false) => this.inner.Call(constructor, tailCall);
		public override void Call(CallableMethod method, ImmutableArray<Type> arglist = default, bool tailCall = false) => this.inner.Call(method, arglist, tailCall);
		public override void CallIndirect(CallingConventions callConventions, Type returnType, ImmutableArray<Type> parameterTypes, ImmutableArray<Type> arglist = default, bool tailCall = false) => this.inner.CallIndirect(callConventions, returnType, parameterTypes, arglist, tailCall);
		public override void CallVirtual(CallableMethod method, Type constrained = default, ImmutableArray<Type> arglist = default, bool tailCall = false) => this.inner.CallVirtual(method, constrained, arglist, tailCall);
		public override void CastClass(Type referenceType) => this.inner.CastClass(referenceType);
		public override void CompareEqual() => this.inner.CompareEqual();
		public override void CompareGreaterThan() => this.inner.CompareGreaterThan();
		public override void CompareGreaterThanUnsigned() => this.inner.CompareGreaterThanUnsigned();
		public override void CheckFinite() => this.inner.CheckFinite();
		public override void CompareLessThan() => this.inner.CompareLessThan();
		public override void CompareLessThanUnsigned() => this.inner.CompareLessThanUnsigned();
		public override void ConvertToNativeInt() => this.inner.ConvertToNativeInt();
		public override void ConvertToInt8() => this.inner.ConvertToInt8();
		public override void ConvertToInt16() => this.inner.ConvertToInt16();
		public override void ConvertToInt32() => this.inner.ConvertToInt32();
		public override void ConvertToInt64() => this.inner.ConvertToInt64();
		public override void ConvertToNativeIntOverflow() => this.inner.ConvertToNativeIntOverflow();
		public override void ConvertToNativeIntOverflowUnsigned() => this.inner.ConvertToNativeIntOverflowUnsigned();
		public override void ConvertToInt8Overflow() => this.inner.ConvertToInt8Overflow();
		public override void ConvertToInt8OverflowUnsigned() => this.inner.ConvertToInt8OverflowUnsigned();
		public override void ConvertToInt16Overflow() => this.inner.ConvertToInt16Overflow();
		public override void ConvertToInt16OverflowUnsigned() => this.inner.ConvertToInt16OverflowUnsigned();
		public override void ConvertToInt32Overflow() => this.inner.ConvertToInt32Overflow();
		public override void ConvertToInt32OverflowUnsigned() => this.inner.ConvertToInt32OverflowUnsigned();
		public override void ConvertToInt64Overflow() => this.inner.ConvertToInt64Overflow();
		public override void ConvertToInt64OverflowUnsigned() => this.inner.ConvertToInt64OverflowUnsigned();
		public override void ConvertToNativeUIntOverflow() => this.inner.ConvertToNativeUIntOverflow();
		public override void ConvertToNativeUIntOverflowUnsigned() => this.inner.ConvertToNativeUIntOverflowUnsigned();
		public override void ConvertToUInt8Overflow() => this.inner.ConvertToUInt8Overflow();
		public override void ConvertToUInt8OverflowUnsigned() => this.inner.ConvertToUInt8OverflowUnsigned();
		public override void ConvertToUInt16Overflow() => this.inner.ConvertToUInt16Overflow();
		public override void ConvertToUInt16OverflowUnsigned() => this.inner.ConvertToUInt16OverflowUnsigned();
		public override void ConvertToUInt32Overflow() => this.inner.ConvertToUInt32Overflow();
		public override void ConvertToUInt32OverflowUnsigned() => this.inner.ConvertToUInt32OverflowUnsigned();
		public override void ConvertToUInt64Overflow() => this.inner.ConvertToUInt64Overflow();
		public override void ConvertToUInt64OverflowUnsigned() => this.inner.ConvertToUInt64OverflowUnsigned();
		public override void ConvertToFloat32Unsigned() => this.inner.ConvertToFloat32Unsigned();
		public override void ConvertToFloat32() => this.inner.ConvertToFloat32();
		public override void ConvertToFloat64() => this.inner.ConvertToFloat64();
		public override void ConvertToNativeUInt() => this.inner.ConvertToNativeUInt();
		public override void ConvertToUInt8() => this.inner.ConvertToUInt8();
		public override void ConvertToUInt16() => this.inner.ConvertToUInt16();
		public override void ConvertToUInt32() => this.inner.ConvertToUInt32();
		public override void ConvertToUInt64() => this.inner.ConvertToUInt64();
		public override void CopyBlock(bool isVolatile = false, int? unaligned = null) => this.inner.CopyBlock(isVolatile, unaligned);
		public override void CopyObject(Type valueType) => this.inner.CopyObject(valueType);
		public override void Divide() => this.inner.Divide();
		public override void DivideUnsigned() => this.inner.DivideUnsigned();
		public override void Duplicate() => this.inner.Duplicate();
		public override void InitializeBlock(bool isVolatile = false, int? unaligned = null) => this.inner.InitializeBlock(isVolatile, unaligned);
		public override void InitializeObject(Type valueType) => this.inner.InitializeObject(valueType);
		public override void IsInstance(Type type) => this.inner.IsInstance(type);
		public override void Jump(CallableMethod method) => this.inner.Jump(method);
		public override void LoadArgument(ushort index) => this.inner.LoadArgument(index);
		public override void LoadArgumentAddress(ushort index) => this.inner.LoadArgumentAddress(index);
		public override void LoadConstant(int value) => this.inner.LoadConstant(value);
		public override void LoadConstant(uint value) => this.inner.LoadConstant(value);
		public override void LoadConstant(long value) => this.inner.LoadConstant(value);
		public override void LoadConstant(ulong value) => this.inner.LoadConstant(value);
		public override void LoadConstant(float value) => this.inner.LoadConstant(value);
		public override void LoadConstant(double value) => this.inner.LoadConstant(value);
		public override void LoadConstant(string str) => this.inner.LoadConstant(str);
		public override void LoadConstant(MethodInfo method) => this.inner.LoadConstant(method);
		public override void LoadConstant(FieldInfo field) => this.inner.LoadConstant(field);
		public override void LoadConstant(Type type) => this.inner.LoadConstant(type);
		public override void LoadElement(Type elementType) => this.inner.LoadElement(elementType);
		public override void LoadElementAddress(Type elementType, bool readOnly = false) => this.inner.LoadElementAddress(elementType, readOnly);
		public override void LoadField(FieldInfo field, bool? isVolatile = null, int? unaligned = null) => this.inner.LoadField(field, isVolatile, unaligned);
		public override void LoadFieldAddress(FieldInfo field) => this.inner.LoadFieldAddress(field);
		public override void LoadFunctionPointer(CallableMethod method) => this.inner.LoadFunctionPointer(method);
		public override void LoadIndirect(Type type, bool isVolatile = false, int? unaligned = null) => this.inner.LoadIndirect(type, isVolatile, unaligned);
		public override void LoadLength() => this.inner.LoadLength();
		public override void LoadLocal(Local local) => this.inner.LoadLocal(local);
		public override void LoadLocalAddress(Local local) => this.inner.LoadLocalAddress(local);
		public override void LoadNull() => this.inner.LoadNull();
		public override void LoadVirtualFunctionPointer(CallableMethod method) => this.inner.LoadVirtualFunctionPointer(method);
		public override void Leave(Label label) => this.inner.Leave(label);
		public override void LocalAllocate() => this.inner.LocalAllocate();
		public override void MakeReferenceAny(Type type) => this.inner.MakeReferenceAny(type);
		public override void Multiply() => this.inner.Multiply();
		public override void MultiplyOverflow() => this.inner.MultiplyOverflow();
		public override void MultiplyOverflowUnsigned() => this.inner.MultiplyOverflowUnsigned();
		public override void Negate() => this.inner.Negate();
		public override void NewArray(Type elementType) => this.inner.NewArray(elementType);
		public override void NewObject(ConstructorInfo constructor) => this.inner.NewObject(constructor);
		public override void NewObject(ConstructorBuilder constructor, ImmutableArray<Type> parameterTypes) => this.inner.NewObject(constructor, parameterTypes);
		public override void Nop() => this.inner.Nop();
		public override void Not() => this.inner.Not();
		public override void Or() => this.inner.Or();
		public override void Pop() => this.inner.Pop();
		public override void ReferenceAnyType() => this.inner.ReferenceAnyType();
		public override void ReferenceAnyValue(Type type) => this.inner.ReferenceAnyValue(type);
		public override void Remainder() => this.inner.Remainder();
		public override void RemainderUnsigned() => this.inner.RemainderUnsigned();
		public override void Return() => this.inner.Return();
		public override void ReThrow() => this.inner.ReThrow();
		public override void ShiftLeft() => this.inner.ShiftLeft();
		public override void ShiftRight() => this.inner.ShiftRight();
		public override void ShiftRightUnsigned() => this.inner.ShiftRightUnsigned();
		public override void SizeOf(Type valueType) => this.inner.SizeOf(valueType);
		public override void StoreArgument(ushort index) => this.inner.StoreArgument(index);
		public override void StoreElement(Type elementType) => this.inner.StoreElement(elementType);
		public override void StoreField(FieldInfo field, bool? isVolatile = null, int? unaligned = null) => this.inner.StoreField(field, isVolatile, unaligned);
		public override void StoreIndirect(Type type, bool isVolatile = false, int? unaligned = null) => this.inner.StoreIndirect(type, isVolatile, unaligned);
		public override void StoreLocal(Local local) => this.inner.StoreLocal(local);
		public override void Subtract() => this.inner.Subtract();
		public override void SubtractOverflow() => this.inner.SubtractOverflow();
		public override void SubtractOverflowUnsigned() => this.inner.SubtractOverflowUnsigned();
		public override void Switch(params Label[] labels) => this.inner.Switch(labels);
		public override void Throw() => this.inner.Throw();
		public override void Unbox(Type valueType) => this.inner.Unbox(valueType);
		public override void UnboxAny(Type valueType) => this.inner.UnboxAny(valueType);
		public override void Xor() => this.inner.Xor();

		public override void ToString(StringBuilder str) => this.inner.ToString(str);
	}

	/// <summary>
	/// Abstract interface for IL code generation.
	/// </summary>
	public abstract class ILBuilder
	{
		public abstract CallableMethod Method { get; }

		public abstract Local DeclareLocal(Type localType, string name = null, bool initializeReused = true, bool pinned = false);
		public abstract Label DefineLabel(string name = null);
		public abstract void MarkLabel(Label label);
		public abstract ExceptionBlock BeginExceptionBlock();
		public abstract void EndExceptionBlock(ExceptionBlock exceptionBlock);
		public abstract CatchBlock BeginCatchBlock(ExceptionBlock forTry, Type exceptionType);
		public abstract void EndCatchBlock(CatchBlock catchBlock);
		public abstract FilterBlock BeginFilterBlock(ExceptionBlock forTry);
		public abstract void EndFilterBlock(FilterBlock filterBlock);
		public abstract FinallyBlock BeginFinallyBlock(ExceptionBlock forTry);
		public abstract void EndFinallyBlock(FinallyBlock finallyBlock);

		public abstract void Add();
		public abstract void AddOverflow();
		public abstract void AddOverflowUnsigned();
		public abstract void And();
		public abstract void ArgumentList();
		public abstract void BranchIfEqual(Label label);
		public abstract void BranchIfGreaterOrEqual(Label label);
		public abstract void BranchIfGreaterOrEqualUnsigned(Label label);
		public abstract void BranchIfGreater(Label label);
		public abstract void BranchIfGreaterUnsigned(Label label);
		public abstract void BranchIfLessOrEqual(Label label);
		public abstract void BranchIfLessOrEqualUnsigned(Label label);
		public abstract void BranchIfLess(Label label);
		public abstract void BranchIfLessUnsigned(Label label);
		public abstract void BranchIfNotEqualUnsigned(Label label);
		public abstract void Box(Type valueType);
		public abstract void Branch(Label label);
		public abstract void Break();
		public abstract void BranchIfFalse(Label label);
		public abstract void BranchIfTrue(Label label);
		public abstract void Call(ConstructorInfo constructor, bool tailCall = false);
		public abstract void Call(CallableMethod method, ImmutableArray<Type> arglist = default, bool tailCall = false);
		public abstract void CallIndirect(CallingConventions callConventions, Type returnType, ImmutableArray<Type> parameterTypes, ImmutableArray<Type> arglist = default, bool tailCall = false);
		public abstract void CallVirtual(CallableMethod method, Type constrained = default, ImmutableArray<Type> arglist = default, bool tailCall = false);
		public abstract void CastClass(Type referenceType);
		public abstract void CompareEqual();
		public abstract void CompareGreaterThan();
		public abstract void CompareGreaterThanUnsigned();
		public abstract void CheckFinite();
		public abstract void CompareLessThan();
		public abstract void CompareLessThanUnsigned();
		public abstract void ConvertToNativeInt();
		public abstract void ConvertToInt8();
		public abstract void ConvertToInt16();
		public abstract void ConvertToInt32();
		public abstract void ConvertToInt64();
		public abstract void ConvertToNativeIntOverflow();
		public abstract void ConvertToNativeIntOverflowUnsigned();
		public abstract void ConvertToInt8Overflow();
		public abstract void ConvertToInt8OverflowUnsigned();
		public abstract void ConvertToInt16Overflow();
		public abstract void ConvertToInt16OverflowUnsigned();
		public abstract void ConvertToInt32Overflow();
		public abstract void ConvertToInt32OverflowUnsigned();
		public abstract void ConvertToInt64Overflow();
		public abstract void ConvertToInt64OverflowUnsigned();
		public abstract void ConvertToNativeUIntOverflow();
		public abstract void ConvertToNativeUIntOverflowUnsigned();
		public abstract void ConvertToUInt8Overflow();
		public abstract void ConvertToUInt8OverflowUnsigned();
		public abstract void ConvertToUInt16Overflow();
		public abstract void ConvertToUInt16OverflowUnsigned();
		public abstract void ConvertToUInt32Overflow();
		public abstract void ConvertToUInt32OverflowUnsigned();
		public abstract void ConvertToUInt64Overflow();
		public abstract void ConvertToUInt64OverflowUnsigned();
		public abstract void ConvertToFloat32Unsigned();
		public abstract void ConvertToFloat32();
		public abstract void ConvertToFloat64();
		public abstract void ConvertToNativeUInt();
		public abstract void ConvertToUInt8();
		public abstract void ConvertToUInt16();
		public abstract void ConvertToUInt32();
		public abstract void ConvertToUInt64();
		public abstract void CopyBlock(bool isVolatile = false, int? unaligned = null);
		public abstract void CopyObject(Type valueType);
		public abstract void Divide();
		public abstract void DivideUnsigned();
		public abstract void Duplicate();
		public abstract void InitializeBlock(bool isVolatile = false, int? unaligned = null);
		public abstract void InitializeObject(Type valueType);
		public abstract void IsInstance(Type type);
		public abstract void Jump(CallableMethod method);
		public abstract void LoadArgument(ushort index);
		public abstract void LoadArgumentAddress(ushort index);
		public abstract void LoadConstant(int value);
		public abstract void LoadConstant(uint value);
		public abstract void LoadConstant(long value);
		public abstract void LoadConstant(ulong value);
		public abstract void LoadConstant(float value);
		public abstract void LoadConstant(double value);
		public abstract void LoadConstant(string str);
		public abstract void LoadConstant(MethodInfo method);
		public abstract void LoadConstant(FieldInfo field);
		public abstract void LoadConstant(Type type);
		public abstract void LoadElement(Type elementType);
		public abstract void LoadElementAddress(Type elementType, bool readOnly = false);
		public abstract void LoadField(FieldInfo field, bool? isVolatile = null, int? unaligned = null);
		public abstract void LoadFieldAddress(FieldInfo field);
		public abstract void LoadFunctionPointer(CallableMethod method);
		public abstract void LoadIndirect(Type type, bool isVolatile = false, int? unaligned = null);
		public abstract void LoadLength();
		public abstract void LoadLocal(Local local);
		public abstract void LoadLocalAddress(Local local);
		public abstract void LoadNull();
		public abstract void LoadVirtualFunctionPointer(CallableMethod method);
		public abstract void Leave(Label label);
		public abstract void LocalAllocate();
		public abstract void MakeReferenceAny(Type type);
		public abstract void Multiply();
		public abstract void MultiplyOverflow();
		public abstract void MultiplyOverflowUnsigned();
		public abstract void Negate();
		public abstract void NewArray(Type elementType);
		public abstract void NewObject(ConstructorInfo constructor);
		public abstract void NewObject(ConstructorBuilder constructor, ImmutableArray<Type> parameterTypes);
		public abstract void Nop();
		public abstract void Not();
		public abstract void Or();
		public abstract void Pop();
		public abstract void ReferenceAnyType();
		public abstract void ReferenceAnyValue(Type type);
		public abstract void Remainder();
		public abstract void RemainderUnsigned();
		public abstract void Return();
		public abstract void ReThrow();
		public abstract void ShiftLeft();
		public abstract void ShiftRight();
		public abstract void ShiftRightUnsigned();
		public abstract void SizeOf(Type valueType);
		public abstract void StoreArgument(ushort index);
		public abstract void StoreElement(Type elementType);
		public abstract void StoreField(FieldInfo field, bool? isVolatile = null, int? unaligned = null);
		public abstract void StoreIndirect(Type type, bool isVolatile = false, int? unaligned = null);
		public abstract void StoreLocal(Local local);
		public abstract void Subtract();
		public abstract void SubtractOverflow();
		public abstract void SubtractOverflowUnsigned();
		public abstract void Switch(params Label[] labels);
		public abstract void Throw();
		public abstract void Unbox(Type valueType);
		public abstract void UnboxAny(Type valueType);
		public abstract void Xor();

		public abstract void ToString(StringBuilder str);
		public override string ToString()
		{
			StringBuilder str = new StringBuilder();
			this.ToString(str);
			return str.ToString();
		}
	}
}

