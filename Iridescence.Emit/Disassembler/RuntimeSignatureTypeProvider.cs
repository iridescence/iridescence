﻿using System;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Reflection.Metadata;

namespace Iridescence.Emit.Disassembler
{
	internal sealed class RuntimeSignatureTypeProvider : ISignatureTypeProvider2<Type, RuntimeSignatureGenericContext>
	{
		public static RuntimeSignatureTypeProvider Instance { get; } = new RuntimeSignatureTypeProvider();

		public Type GetSZArrayType(Type elementType)
		{
			return elementType.MakeArrayType();
		}

		public Type GetArrayType(Type elementType, ArrayShape shape)
		{
			return elementType.MakeArrayType(shape.Rank);
		}

		public Type GetByReferenceType(Type elementType)
		{
			return elementType.MakeByRefType();
		}

		public Type GetGenericInstantiation(Type genericType, ImmutableArray<Type> typeArguments)
		{
			Type[] array = new Type[typeArguments.Length];
			typeArguments.CopyTo(array);
			return genericType.MakeGenericType(array);
		}

		public Type GetPointerType(Type elementType)
		{
			return elementType.MakePointerType();
		}

		public Type GetPrimitiveType(PrimitiveTypeCode typeCode)
		{
			switch (typeCode)
			{
				case PrimitiveTypeCode.Boolean:
					return typeof(bool);
				case PrimitiveTypeCode.Byte:
					return typeof(byte);
				case PrimitiveTypeCode.Char:
					return typeof(char);
				case PrimitiveTypeCode.Double:
					return typeof(double);
				case PrimitiveTypeCode.Int16:
					return typeof(short);
				case PrimitiveTypeCode.Int32:
					return typeof(int);
				case PrimitiveTypeCode.Int64:
					return typeof(long);
				case PrimitiveTypeCode.IntPtr:
					return typeof(IntPtr);
				case PrimitiveTypeCode.Object:
					return typeof(object);
				case PrimitiveTypeCode.SByte:
					return typeof(sbyte);
				case PrimitiveTypeCode.Single:
					return typeof(float);
				case PrimitiveTypeCode.String:
					return typeof(string);
				case PrimitiveTypeCode.TypedReference:
					return typeof(TypedReference);
				case PrimitiveTypeCode.UInt16:
					return typeof(ushort);
				case PrimitiveTypeCode.UInt32:
					return typeof(uint);
				case PrimitiveTypeCode.UInt64:
					return typeof(ulong);
				case PrimitiveTypeCode.UIntPtr:
					return typeof(UIntPtr);
				case PrimitiveTypeCode.Void:
					return typeof(void);
				default:
					throw new InvalidEnumArgumentException();
			}
		}

		public Type GetTypeFromDefinition(MetadataReader reader, TypeDefinitionHandle handle, byte rawTypeKind)
		{
			throw new NotImplementedException();
		}

		public Type GetTypeFromReference(MetadataReader reader, TypeReferenceHandle handle, byte rawTypeKind)
		{
			throw new NotImplementedException();
		}

		public Type GetFunctionPointerType(MethodSignature<Type> signature)
		{
			throw new NotImplementedException();
		}

		public Type GetGenericMethodParameter(RuntimeSignatureGenericContext genericContext, int index)
		{
			return genericContext.MethodGenericParameters[index];
		}

		public Type GetGenericTypeParameter(RuntimeSignatureGenericContext genericContext, int index)
		{
			return genericContext.TypeGenericParameters[index];
		}

		public Type GetModifiedType(Type modifier, Type unmodifiedType, bool isRequired)
		{
			throw new NotImplementedException();
		}

		public Type GetPinnedType(Type elementType)
		{
			return new PinnedType(elementType);
		}

		public Type GetTypeFromSpecification(MetadataReader reader, RuntimeSignatureGenericContext genericContext, TypeSpecificationHandle handle, byte rawTypeKind)
		{
			throw new NotImplementedException();
		}

		public Type GetInternalType(IntPtr ptr)
		{
			return TypeUtility.GetTypeFromHandle(ptr);
		}
	}
}