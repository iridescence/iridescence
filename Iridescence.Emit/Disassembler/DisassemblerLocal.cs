﻿using System;
using System.Reflection;

namespace Iridescence.Emit.Disassembler
{
	internal sealed class DisassemblerLocal : LocalVariableInfo
	{
		#region Properties

		public override int LocalIndex { get; }
		public override Type LocalType { get; }
		public override bool IsPinned { get; }

		#endregion

		#region Constructors

		public DisassemblerLocal(int index, Type type, bool isPinned)
		{
			this.LocalIndex = index;
			this.LocalType = type;
			this.IsPinned = isPinned;
		}

		#endregion
	}
}