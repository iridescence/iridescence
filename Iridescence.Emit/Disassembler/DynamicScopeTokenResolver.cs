﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit.Disassembler
{
	internal sealed class DynamicScopeTokenResolver : ITokenResolver
	{
		private static readonly PropertyInfo indexer;
		private static readonly FieldInfo scopeFi;

		private static readonly Type genMethodInfoType;
		private static readonly FieldInfo genmethFi1, genmethFi2;

		private static readonly Type varArgMethodType;
		private static readonly FieldInfo varargFi1, varargFi2;

		private static readonly Type genFieldInfoType;
		private static readonly FieldInfo genfieldFi1, genfieldFi2;

		static DynamicScopeTokenResolver()
		{
			Type dynamicScope = Type.GetType("System.Reflection.Emit.DynamicScope", throwOnError: true);
			Type dynamicILGenerator = Type.GetType("System.Reflection.Emit.DynamicILGenerator", throwOnError: true);

			indexer = dynamicScope.GetProperty("Item", BindingFlags.Instance | BindingFlags.NonPublic);
			scopeFi = dynamicILGenerator.GetField("m_scope", BindingFlags.Instance | BindingFlags.NonPublic);

			varArgMethodType = Type.GetType("System.Reflection.Emit.VarArgMethod", throwOnError: true);
			varargFi1 = varArgMethodType.GetField("m_method", BindingFlags.Instance | BindingFlags.NonPublic);
			varargFi2 = varArgMethodType.GetField("m_signature", BindingFlags.Instance | BindingFlags.NonPublic);

			genMethodInfoType = Type.GetType("System.Reflection.Emit.GenericMethodInfo", throwOnError: true);
			genmethFi1 = genMethodInfoType.GetField("m_methodHandle", BindingFlags.Instance | BindingFlags.NonPublic);
			genmethFi2 = genMethodInfoType.GetField("m_context", BindingFlags.Instance | BindingFlags.NonPublic);

			genFieldInfoType = Type.GetType("System.Reflection.Emit.GenericFieldInfo", throwOnError: false);
			if (genFieldInfoType != null)
			{
				genfieldFi1 = genFieldInfoType.GetField("m_fieldHandle", BindingFlags.Instance | BindingFlags.NonPublic);
				genfieldFi2 = genFieldInfoType.GetField("m_context", BindingFlags.Instance | BindingFlags.NonPublic);
			}
			else
			{
				genfieldFi1 = genfieldFi2 = null;
			}
		}

		private readonly object scope = null;

		internal object this[int token] => indexer.GetValue(this.scope, new object[] { token });

		public DynamicScopeTokenResolver(DynamicMethod dm)
		{
			this.scope = scopeFi.GetValue(dm.GetILGenerator());
		}

		public string ResolveString(int token) => this[token] as string;

		public FieldInfo ResolveField(int token)
		{
			object item = this[token];

			if (item is RuntimeFieldHandle)
				return FieldInfo.GetFieldFromHandle((RuntimeFieldHandle)item);

			if (item.GetType() == genFieldInfoType)
			{
				return FieldInfo.GetFieldFromHandle(
					(RuntimeFieldHandle)genfieldFi1.GetValue(item),
					(RuntimeTypeHandle)genfieldFi2.GetValue(item));
			}

			Debug.Fail($"unexpected type: {item.GetType()}");
			return null;
		}

		public Type ResolveType(int token) => Type.GetTypeFromHandle((RuntimeTypeHandle)this[token]);

		public MethodBase ResolveMethod(int token)
		{
			object item = this[token];

			if (item is DynamicMethod)
				return item as DynamicMethod;

			if (item is RuntimeMethodHandle)
				return MethodBase.GetMethodFromHandle((RuntimeMethodHandle)item);

			if (item.GetType() == genMethodInfoType)
				return MethodBase.GetMethodFromHandle(
					(RuntimeMethodHandle)genmethFi1.GetValue(item),
					(RuntimeTypeHandle)genmethFi2.GetValue(item));

			if (item.GetType() == varArgMethodType)
				return (MethodInfo)varargFi1.GetValue(item);

			Debug.Fail($"unexpected type: {item.GetType()}");
			return null;
		}

		public MemberInfo ResolveMember(int token)
		{
			if ((token & 0x02000000) == 0x02000000)
				return this.ResolveType(token);
			if ((token & 0x06000000) == 0x06000000)
				return this.ResolveMethod(token);
			if ((token & 0x04000000) == 0x04000000)
				return this.ResolveField(token);

			Debug.Fail($"unexpected token type: {token:x8}");
			return null;
		}

		public byte[] ResolveSignature(int token) => this[token] as byte[];
	}
}