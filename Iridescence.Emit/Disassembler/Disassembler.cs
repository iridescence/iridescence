﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Reflection.Metadata;

namespace Iridescence.Emit.Disassembler
{
	/// <summary>
	/// Provides a disassembler that is able to read the IL code of a method and return the <see cref="OperationCollection"/>.
	/// </summary>
	public static class Disassembler
	{
		private static readonly OpCode[] opCodes = typeof(OpCodes).GetFields(BindingFlags.Public | BindingFlags.Static).Select(f => (OpCode)f.GetValue(null)).ToArray();
		private static readonly FieldInfo resolverField = typeof(DynamicMethod).GetInternalField("m_resolver");

		internal delegate byte[] GetCodeInfo(ref int stackSize, ref int initLocals, ref int ehCount);

		internal static object GetResolver(this DynamicMethod method)
		{
			return resolverField?.GetValue(method);
		}

		internal static MethodInfo GetInternalMethod(this Type type, string name)
		{
			return type.GetMethod(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic) ?? throw new MissingMethodException(type.FullName, name);
		}

		internal static FieldInfo GetInternalField(this Type type, string name)
		{
			return type.GetField(name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic) ?? throw new MissingFieldException(type.FullName, name);
		}

		internal static bool TryGetDynamicMethod(MethodBase method, out DynamicMethod dynMethod)
		{
			dynMethod = method as DynamicMethod;
			if (dynMethod != null)
				return true;

			// RTDynamicMethod.
			FieldInfo ownerField = method.GetType().GetField("m_owner", BindingFlags.Instance | BindingFlags.NonPublic);
			if (ownerField != null && ownerField.FieldType == typeof(DynamicMethod))
			{
				dynMethod = (DynamicMethod)ownerField.GetValue(method);
				return true;
			}

			dynMethod = null;
			return false;
		}

		/// <summary>
		/// Returns the encoded IL instructions of the specified method.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		public static bool TryGetILData(this MethodBase method, out byte[] il)
		{
			if (TryGetDynamicMethod(method, out DynamicMethod dynMethod))
			{
				var resolver = dynMethod.GetResolver();
				if (resolver != null)
				{
					Type resolverType = resolver.GetType();
					MethodInfo getCodeInfoMethod = resolverType.GetInternalMethod("GetCodeInfo");
					GetCodeInfo getCodeInfo = (GetCodeInfo)getCodeInfoMethod.CreateDelegate(typeof(GetCodeInfo), resolver);

					int stackSize = 0;
					int initLocals = 0;
					int ehCount = 0;
					il = getCodeInfo(ref stackSize, ref initLocals, ref ehCount);
					return true;
				}
			}

			MethodBody body = method.GetMethodBody();
			if (body == null)
			{
				il = null;
				return false;
			}

			il = body.GetILAsByteArray();
			return true;
		}

		/// <summary>
		/// Returns the <see cref="LocalVariableInfo"/> list for the locals of the specified method.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		public static bool TryGetLocals(this MethodBase method, out IReadOnlyList<LocalVariableInfo> locals)
		{
			if (method == null)
				throw new ArgumentNullException(nameof(method));

			if (TryGetDynamicMethod(method, out DynamicMethod dynMethod))
			{
				var resolver = dynMethod.GetResolver();
				if (resolver != null)
				{
					Type resolverType = resolver.GetType();
					MethodInfo getLocalsSignatureMethod = resolverType.GetInternalMethod("GetLocalsSignature");
					Func<byte[]> getLocalsSignature = (Func<byte[]>)getLocalsSignatureMethod.CreateDelegate(typeof(Func<byte[]>), resolver);
					byte[] localsSig = getLocalsSignature();
					ImmutableArray<Type> localTypes;

					unsafe
					{
						fixed (byte* buffer = localsSig)
						{
							BlobReader reader = new BlobReader(buffer, localsSig.Length);
							SignatureDecoder2<Type, RuntimeSignatureGenericContext> decoder = new SignatureDecoder2<Type, RuntimeSignatureGenericContext>(RuntimeSignatureTypeProvider.Instance, null, null);
							localTypes = decoder.DecodeLocalSignature(ref reader);
						}
					}

					DisassemblerLocal[] localArray = new DisassemblerLocal[localTypes.Length];

					for (int i = 0; i < localTypes.Length; ++i)
					{
						if (localTypes[i] is PinnedType pinnedType)
						{
							localArray[i] = new DisassemblerLocal(i, pinnedType.GetElementType(), true);
						}
						else
						{
							localArray[i] = new DisassemblerLocal(i, localTypes[i], false);
						}
					}

					locals = localArray;
					return true;
				}
			}

			MethodBody body = method.GetMethodBody();
			if (body == null)
			{
				locals = null;
				return false;
			}

			locals = body.LocalVariables.ToArray();
			return true;
		}

		/// <summary>
		/// Returns the <see cref="ExceptionHandlingClause"/> list of the specified method.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		public static bool TryGetExceptionHandlingClauses(this MethodBase method, out IReadOnlyList<ExceptionHandlingClause> clauses)
		{
			if (TryGetDynamicMethod(method, out DynamicMethod dynMethod))
			{
				var resolver = dynMethod.GetResolver();
				if (resolver != null)
				{
					Type resolverType = resolver.GetType();
					FieldInfo exceptionsField = resolverType.GetInternalField("m_exceptions");
					Array exceptions = (Array)exceptionsField.GetValue(resolver);

					if (exceptions == null)
					{
						clauses = Array.Empty<ExceptionHandlingClause>();
						return true;
					}

					List<ExceptionHandlingClause> clausesList = new List<ExceptionHandlingClause>();

					for (int i = 0; i < exceptions.Length; ++i)
					{
						object exInfo = exceptions.GetValue(i);
						foreach (ExceptionHandlingClause clause in DisassemblerEHClause.GetClausesFromExceptionInfo(exInfo))
						{
							clausesList.Add(clause);
						}
					}

					clauses = clausesList;
					return true;
				}
			}

			MethodBody body = method.GetMethodBody();
			if (body == null)
			{
				clauses = null;
				return false;
			}

			clauses = body.ExceptionHandlingClauses.ToArray();
			return true;
		}

		internal static ITokenResolver GetTokenResolver(MethodBase method)
		{
			if (TryGetDynamicMethod(method, out DynamicMethod dynMethod))
			{
				return new DynamicScopeTokenResolver(dynMethod);
			}

			// Get generic args from type.
			Type[] genericTypeArguments;
			try
			{
				genericTypeArguments = method.DeclaringType?.GetGenericArguments() ?? Array.Empty<Type>();
			}
			catch (NotSupportedException)
			{
				genericTypeArguments = Array.Empty<Type>();
			}

			// Get generic args from method.
			Type[] genericMethodArguments;
			try
			{
				genericMethodArguments = method.GetGenericArguments();
			}
			catch (NotSupportedException)
			{
				genericMethodArguments = Array.Empty<Type>();
			}

			return new ModuleTokenResolver(method.Module, genericTypeArguments, genericMethodArguments);
		}

		/// <summary>
		/// Disassembles the specified method into an <see cref="OperationCollection"/>.
		/// </summary>
		/// <param name="method"></param>
		/// <returns></returns>
		public static bool TryDisassemble(this MethodBase method, out OperationCollection ops)
		{
			ops = null;
			if (method == null)
				throw new ArgumentNullException(nameof(method));

			// Get locals.
			if (!method.TryGetLocals(out IReadOnlyList<LocalVariableInfo> disassembledLocals))
				return false;

			// Get IL.
			if (!method.TryGetILData(out byte[] il))
				return false;

			// Get EH clauses.
			if (!method.TryGetExceptionHandlingClauses(out IReadOnlyList<ExceptionHandlingClause> ehClauses))
				return false;

			// Begin disassembly.
			ops = new OperationCollection();

			// Recreate locals.
			Dictionary<int, Local> locals = new Dictionary<int, Local>();
			foreach (LocalVariableInfo localInfo in disassembledLocals)
			{
				Local local = ops.DeclareLocal(localInfo.LocalType, pinned: localInfo.IsPinned);
				locals.Add(localInfo.LocalIndex, local);
			}
			
			// Create label for every IL byte.
			Label[] labels = new Label[il.Length];
			for (int i = 0; i < il.Length; ++i)
				labels[i] = ops.DefineLabel().Label;

			ITokenResolver module = GetTokenResolver(method);

			Dictionary<int, Operation> offsetMap = new Dictionary<int, Operation>();

			// Parse IL.
			using (MemoryStream stream = new MemoryStream(il))
			{
				byte[] temp = new byte[8];

				void fillTemp(int count)
				{
					int numBytesRead = 0;

					while (numBytesRead < count)
					{
						int n = stream.Read(temp, numBytesRead, count - numBytesRead);
						if (n == 0)
							throw new EndOfStreamException();
						numBytesRead += n;
					}
				}

				byte readByte()
				{
					fillTemp(1);
					return temp[0];
				}

				short readInt16()
				{
					fillTemp(2);
					return BitConverter.ToInt16(temp, 0);
				}

				int readInt32()
				{
					fillTemp(4);
					return BitConverter.ToInt32(temp, 0);
				}

				long readInt64()
				{
					fillTemp(8);
					return BitConverter.ToInt64(temp, 0);
				}

				float readSingle()
				{
					fillTemp(4);
					return BitConverter.ToSingle(temp, 0);
				}

				double readDouble()
				{
					fillTemp(8);
					return BitConverter.ToDouble(temp, 0);
				}

				for (;;)
				{
					int pos = (int)stream.Position;
					int code = stream.ReadByte();

					if (code < 0)
						break;

					if (code == 0xFE)
					{
						int code2 = stream.ReadByte();
						if (code2 < 0)
							throw new EndOfStreamException();

						code = (code << 8) | code2;
					}

					Debug.Assert(ops.ByteSize == pos);

					for (int i = 0; i < pos; ++i)
					{
						if (labels[i] != null && !labels[i].IsMarked)
						{
							labels[i] = null;
						}
					}

					ops.MarkLabel(labels[pos]);

					OpCode op = opCodes.Single(o => o.Value == unchecked((short)code));

					switch (op.OperandType)
					{
						case OperandType.InlineBrTarget:
						{
							int offset = readInt32();
							int target = (int)(stream.Position + offset);
							offsetMap.Add(pos, ops.Add(op, labels[target]));
							break;
						}

						case OperandType.InlineField:
						{
							int token = readInt32();
							FieldInfo field = module.ResolveField(token);
							offsetMap.Add(pos, ops.Add(op, field));
							break;
						}

						case OperandType.InlineI:
						{
							int value = readInt32();
							offsetMap.Add(pos, ops.Add(op, value));
							break;
						}

						case OperandType.InlineI8:
						{
							long value = readInt64();
							offsetMap.Add(pos, ops.Add(op, value));
							break;
						}

						case OperandType.InlineMethod:
						{
							int token = readInt32();
							MethodBase m = module.ResolveMethod(token);
							offsetMap.Add(pos, ops.Add(op, new InlineMethod(m)));
							break;
						}

						case OperandType.InlineNone:
						{
							offsetMap.Add(pos, ops.Add(op));
							break;
						}

						case OperandType.InlineR:
						{
							double value = readDouble();
							offsetMap.Add(pos, ops.Add(op, value));

							break;
						}

						case OperandType.InlineSig:
						{
							int token = readInt32();
							byte[] sig = module.ResolveSignature(token);
							throw new NotImplementedException();
						}

						case OperandType.InlineString:
						{
							int token = readInt32();
							string str = module.ResolveString(token);
							offsetMap.Add(pos, ops.Add(op, str));
							break;
						}

						case OperandType.InlineSwitch:
						{
							int numLabels = readInt32();
							int offsetBase = (int)(stream.Position + numLabels * 4);
							Label[] switchTargets = new Label[numLabels];
							for (int i = 0; i < numLabels; ++i)
							{
								int offset = readInt32();
								int target = offsetBase + offset;
								switchTargets[i] = labels[target];
							}

							offsetMap.Add(pos, ops.Add(op, switchTargets));

							break;
						}

						case OperandType.InlineTok:
						{
							int token = readInt32();
							MemberInfo member = module.ResolveMember(token);

							if (member is FieldInfo f)
								offsetMap.Add(pos, ops.Add(op, f));
							else if (member is MethodBase m)
								offsetMap.Add(pos, ops.Add(op, m));
							else if (member is Type t)
								offsetMap.Add(pos, ops.Add(op, t));
							else
								throw new NotSupportedException();

							break;
						}


						case OperandType.InlineType:
						{
							int token = readInt32();
							Type type = module.ResolveType(token);
							offsetMap.Add(pos, ops.Add(op, type));
							break;
						}

						case OperandType.InlineVar:
						{
							short index = readInt16();

							if (op.Name.Contains("arg"))
							{
								offsetMap.Add(pos, ops.Add(op, index));
							}
							else
							{
								offsetMap.Add(pos, ops.Add(op, locals[index]));
							}

							break;
						}

						case OperandType.ShortInlineBrTarget:
						{
							sbyte offset = unchecked((sbyte)readByte());
							int target = (int)(stream.Position + offset);
							offsetMap.Add(pos, ops.Add(op, labels[target]));
							break;
						}

						case OperandType.ShortInlineI:
						{
							byte value = readByte();
							offsetMap.Add(pos, ops.Add(op, value));
							break;
						}

						case OperandType.ShortInlineR:
						{
							float value = readSingle();
							offsetMap.Add(pos, ops.Add(op, value));
							break;
						}

						case OperandType.ShortInlineVar:
						{
							byte index = readByte();

							if (op.Name.Contains("arg"))
							{
								offsetMap.Add(pos, ops.Add(op, index));
							}
							else
							{
								offsetMap.Add(pos, ops.Add(op, locals[index]));
							}

							break;
						}

						default:
							throw new InvalidEnumArgumentException();
					}
				}
			}

			DisassembleExceptionHandlingClauses(ops, offsetMap, ehClauses);
			return true;
		}

		private sealed class EHClause
		{
			public Operation Start;
			public Operation End;
			public Operation FilterStart;
			public Operation HandlerStart;
			public Operation HandlerEnd;
			public Type CatchType;
			public ExceptionHandlingClauseOptions Flags;

			public void ReplaceOp(Operation oldOp, Operation newOp)
			{
				if (this.Start == oldOp) this.Start = newOp;
				if (this.End == oldOp) this.End = newOp;
				if (this.FilterStart == oldOp) this.FilterStart = newOp;
				if (this.HandlerStart == oldOp) this.HandlerStart = newOp;
				if (this.HandlerEnd == oldOp) this.HandlerEnd = newOp;
			}
		}

		internal static void DisassembleExceptionHandlingClauses(OperationCollection ops, IReadOnlyDictionary<int, Operation> offsetMap, IEnumerable<ExceptionHandlingClause> clauses)
		{
			int maxOffset = offsetMap.Keys.Max();

			Operation getOp(int byteOffset)
			{
				if (byteOffset > maxOffset)
					return null;
				return offsetMap[byteOffset];
			}

			List<EHClause> tempClauses = new List<EHClause>();

			foreach (ExceptionHandlingClause clause in clauses)
			{
				EHClause temp = new EHClause();
				temp.Start = getOp(clause.TryOffset);
				temp.End = getOp(clause.HandlerOffset + clause.HandlerLength);
				temp.Flags = clause.Flags;
				temp.CatchType = temp.Flags == ExceptionHandlingClauseOptions.Clause ? clause.CatchType : null;
				switch (clause.Flags)
				{
					case ExceptionHandlingClauseOptions.Clause:
					case ExceptionHandlingClauseOptions.Fault:
					case ExceptionHandlingClauseOptions.Finally:
						temp.HandlerStart = getOp(clause.HandlerOffset);
						temp.HandlerEnd = getOp(clause.HandlerOffset + clause.HandlerLength);
						break;
					case ExceptionHandlingClauseOptions.Filter:
						temp.HandlerStart = getOp(clause.HandlerOffset);
						temp.HandlerEnd = getOp(clause.HandlerOffset + clause.HandlerLength);
						temp.FilterStart = getOp(clause.FilterOffset);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				tempClauses.Add(temp);
			}

			void replaceOp(Operation oldOp, Operation newOp)
			{
				foreach (EHClause clause in tempClauses)
					clause.ReplaceOp(oldOp, newOp);

				ops.Remove(oldOp);
			}

			foreach (EHClause clause in tempClauses)
			{
				ops.BeginExceptionBlock(clause.Start?.OperationIndex ?? ops.Count);

				switch (clause.Flags)
				{
					case ExceptionHandlingClauseOptions.Clause:
					{
						Operation newOp = ops.BeginCatchBlock(clause.HandlerStart?.OperationIndex ?? ops.Count, clause.CatchType);
						ops.EndCatchBlock(clause.HandlerEnd?.OperationIndex ?? ops.Count);

						Instruction instr = (clause.HandlerStart ?? ops[ops.Count - 1]).PreviousInstruction;
						if (instr.OpCode == OpCodes.Leave ||
						    instr.OpCode == OpCodes.Leave_S ||
						    instr.OpCode == OpCodes.Endfilter)
						{
							replaceOp(instr, newOp);
						}

						break;
					}

					case ExceptionHandlingClauseOptions.Filter:
					{
						ops.BeginFilterBlock(clause.FilterStart?.OperationIndex ?? ops.Count);
						ops.EndFilterBlock(clause.HandlerStart?.OperationIndex ?? ops.Count);
						Operation newOp = ops.BeginCatchBlock(clause.HandlerStart?.OperationIndex ?? ops.Count, null);
						ops.EndCatchBlock(clause.HandlerEnd?.OperationIndex ?? ops.Count);

						Instruction instr = (clause.HandlerStart ?? ops[ops.Count - 1]).PreviousInstruction;
						if (instr.OpCode == OpCodes.Leave ||
						    instr.OpCode == OpCodes.Leave_S ||
						    instr.OpCode == OpCodes.Endfilter)
						{
							replaceOp(instr, newOp);
						}

						break;
					}

					case ExceptionHandlingClauseOptions.Finally:
					{
						Operation startOp = ops.BeginFinallyBlock(clause.HandlerStart?.OperationIndex ?? ops.Count);
						Operation endOp = ops.EndFinallyBlock(clause.HandlerEnd?.OperationIndex ?? ops.Count);

						Instruction instr = (clause.HandlerStart ?? ops[ops.Count - 1]).PreviousInstruction;
						if (instr.OpCode == OpCodes.Leave ||
						    instr.OpCode == OpCodes.Leave_S)
						{
							replaceOp(instr, startOp);
						}

						instr = (clause.HandlerEnd ?? ops[ops.Count - 1]).PreviousInstruction;
						if (instr.OpCode == OpCodes.Endfinally)
						{
							replaceOp(instr, endOp);
						}

						break;
					}

					case ExceptionHandlingClauseOptions.Fault:
					{
						Operation startOp = ops.BeginFaultBlock(clause.HandlerStart?.OperationIndex ?? ops.Count);
						Operation endOp = ops.EndFaultBlock(clause.HandlerEnd?.OperationIndex ?? ops.Count);

						Instruction instr = (clause.HandlerStart ?? ops[ops.Count - 1]).PreviousInstruction;
						if (instr.OpCode == OpCodes.Leave ||
						    instr.OpCode == OpCodes.Leave_S)
						{
							replaceOp(instr, startOp);
						}

						instr = (clause.HandlerEnd ?? ops[ops.Count - 1]).PreviousInstruction;
						if (instr.OpCode == OpCodes.Endfinally)
						{
							replaceOp(instr, endOp);
						}

						break;
					}

					default:
						throw new ArgumentOutOfRangeException();
				}
				
				ops.EndExceptionBlock(clause.End?.OperationIndex ?? ops.Count);
			}
		}
	}
}
