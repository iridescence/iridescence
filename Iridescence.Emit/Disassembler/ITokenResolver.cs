﻿using System;
using System.Reflection;

namespace Iridescence.Emit.Disassembler
{
	/// <summary>
	/// Token resolver for the disassembler.
	/// </summary>
	internal interface ITokenResolver
	{
		Type ResolveType(int token);
		FieldInfo ResolveField(int token);
		MethodBase ResolveMethod(int token);
		MemberInfo ResolveMember(int token);
		string ResolveString(int token);
		byte[] ResolveSignature(int token);
	}
}
