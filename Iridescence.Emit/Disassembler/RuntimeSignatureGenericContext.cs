﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Emit.Disassembler
{
	internal sealed class RuntimeSignatureGenericContext
	{
		public IReadOnlyList<Type> TypeGenericParameters { get; }
		public IReadOnlyList<Type> MethodGenericParameters { get; }

		public RuntimeSignatureGenericContext(IEnumerable<Type> typeParams, IEnumerable<Type> methodParams)
		{
			this.TypeGenericParameters = typeParams.ToArray();
			this.MethodGenericParameters = methodParams.ToArray();
		}
	}
}