﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Emit.Disassembler
{
	internal sealed class DisassemblerEHClause : ExceptionHandlingClause
	{
		#region Fields

		private static readonly Type exceptionInfoType = Type.GetType("System.Reflection.Emit.__ExceptionInfo", throwOnError: true);
		private static readonly MethodInfo getStartAddressMethod = exceptionInfoType.GetInternalMethod("GetStartAddress");
		private static readonly MethodInfo getEndAddressMethod = exceptionInfoType.GetInternalMethod("GetEndAddress");
		private static readonly MethodInfo getFinallyEndAddressMethod = exceptionInfoType.GetInternalMethod("GetFinallyEndAddress");
		private static readonly MethodInfo getNumberOfCatchesMethod = exceptionInfoType.GetInternalMethod("GetNumberOfCatches");
		private static readonly MethodInfo getCatchAddressesMethod = exceptionInfoType.GetInternalMethod("GetCatchAddresses");
		private static readonly MethodInfo getCatchEndAddressesMethod = exceptionInfoType.GetInternalMethod("GetCatchEndAddresses");
		private static readonly MethodInfo getFilterAddressesMethod = exceptionInfoType.GetInternalMethod("GetFilterAddresses");
		private static readonly MethodInfo getCatchClassMethod = exceptionInfoType.GetInternalMethod("GetCatchClass");
		private static readonly MethodInfo getExceptionTypesMethod = exceptionInfoType.GetInternalMethod("GetExceptionTypes");

		#endregion

		#region Properties

		public override int TryOffset { get; }
		public override int TryLength { get; }
		public override int FilterOffset { get; }
		public override int HandlerOffset { get; }
		public override int HandlerLength { get; }
		public override Type CatchType { get; }
		public override ExceptionHandlingClauseOptions Flags { get; }

		#endregion

		#region Constructors

		public DisassemblerEHClause(int tryOffset, int tryLength, int filterOffset, int handlerOffset, int handlerLength, Type catchType, ExceptionHandlingClauseOptions flags)
		{
			this.TryOffset = tryOffset;
			this.TryLength = tryLength;
			this.FilterOffset = filterOffset;
			this.HandlerOffset = handlerOffset;
			this.HandlerLength = handlerLength;
			this.CatchType = catchType;
			this.Flags = flags;
		}

		#endregion

		#region Methods

		public static IEnumerable<ExceptionHandlingClause> GetClausesFromExceptionInfo(object exceptionInfo)
		{
			int startAddress = (int)getStartAddressMethod.Invoke(exceptionInfo, Array.Empty<object>());
			int endAddress = (int)getEndAddressMethod.Invoke(exceptionInfo, Array.Empty<object>());
			int count = (int)getNumberOfCatchesMethod.Invoke(exceptionInfo, Array.Empty<object>());

			if (count > 0)
			{
				int[] catchAddresses = (int[])getCatchAddressesMethod.Invoke(exceptionInfo, Array.Empty<object>());
				int[] catchEndAddresses = (int[])getCatchEndAddressesMethod.Invoke(exceptionInfo, Array.Empty<object>());
				Type[] catchClasses = (Type[])getCatchClassMethod.Invoke(exceptionInfo, Array.Empty<object>());
				int[] filterAddresses = (int[])getFilterAddressesMethod.Invoke(exceptionInfo, Array.Empty<object>());
				int[] types = (int[])getExceptionTypesMethod.Invoke(exceptionInfo, Array.Empty<object>());

				for (int i = 0; i < count; ++i)
				{
					ExceptionHandlingClauseOptions flags = (ExceptionHandlingClauseOptions)types[i];

					int tryOffset = startAddress;
					int tryLength;

					if ((flags & ExceptionHandlingClauseOptions.Finally) != ExceptionHandlingClauseOptions.Finally)
					{
						tryLength = endAddress - tryOffset;
					}
					else
					{
						int finallyEnd = (int)getFinallyEndAddressMethod.Invoke(exceptionInfo, Array.Empty<object>());
						tryLength = finallyEnd - tryOffset;
					}

					int handlerOffset = catchAddresses[i];
					int handlerLength = catchEndAddresses[i] - handlerOffset;

					Type catchType = null;
					int filterOffset = 0;

					if ((flags & ExceptionHandlingClauseOptions.Filter) != 0)
					{
						filterOffset = filterAddresses[i];
					}
					else
					{
						catchType = catchClasses[i];
					}

					yield return new DisassemblerEHClause(tryOffset, tryLength, filterOffset, handlerOffset, handlerLength, catchType, flags);
				}
			}
		}

		#endregion
	}
}
