﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Iridescence.Emit.Disassembler
{
	internal sealed class ModuleTokenResolver : ITokenResolver
	{
		private readonly Module module;
		private readonly Type[] typeArgs;
		private readonly Type[] methodArgs;

		public ModuleTokenResolver(Module module, IEnumerable<Type> typeGenericParameters, IEnumerable<Type> methodGenericParameters)
		{
			this.module = module;
			this.typeArgs = typeGenericParameters.ToArray();
			this.methodArgs = methodGenericParameters.ToArray();
		}

		public Type ResolveType(int token)
		{
			return this.module.ResolveType(token, this.typeArgs, this.methodArgs);
		}

		public FieldInfo ResolveField(int token)
		{
			return this.module.ResolveField(token, this.typeArgs, this.methodArgs);
		}

		public MethodBase ResolveMethod(int token)
		{
			return this.module.ResolveMethod(token, this.typeArgs, this.methodArgs);
		}

		public MemberInfo ResolveMember(int token)
		{
			return this.module.ResolveMember(token, this.typeArgs, this.methodArgs);
		}

		public string ResolveString(int token)
		{
			return this.module.ResolveString(token);
		}

		public byte[] ResolveSignature(int token)
		{
			return this.module.ResolveSignature(token);
		}
	}
}