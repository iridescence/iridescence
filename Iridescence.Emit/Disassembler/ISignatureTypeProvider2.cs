﻿using System;
using System.Reflection.Metadata;

namespace Iridescence.Emit.Disassembler
{
	internal interface ISignatureTypeProvider2<TType, TGenericContext> : ISignatureTypeProvider<TType, TGenericContext>
	{
		TType GetInternalType(IntPtr ptr);
	}
}