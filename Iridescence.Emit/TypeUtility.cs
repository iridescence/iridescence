﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Type utility functions.
	/// </summary>
	public static class TypeUtility
	{
		#region Fields

		private static readonly Func<IntPtr, Type> getTypeFromHandle;

		#endregion

		#region Constructor

		static TypeUtility()
		{
			MethodInfo method = typeof(Type).GetMethod("GetTypeFromHandleUnsafe", BindingFlags.Static | BindingFlags.NonPublic);
			getTypeFromHandle = (Func<IntPtr, Type>)method?.CreateDelegate(typeof(Func<IntPtr, Type>));
		}

		#endregion

		#region Methods

		public static bool IsVolatile(this FieldInfo field)
		{
			// FieldBuilder doesn't implement GetRequiredCustomModifiers
			if (field is FieldBuilder)
				return false;

			return Array.IndexOf(field.GetRequiredCustomModifiers(), typeof(IsVolatile)) >= 0;
		}

		public static bool IsNativeInt(this Type type)
		{
			return type.IsPointer || type == typeof(IntPtr) || type == typeof(UIntPtr);
		}

		public static bool IsDelegateType(this Type type)
		{
			return type.IsSubclassOf(typeof(Delegate));
		}

		internal static Type GetTypeFromHandle(IntPtr handle)
		{
			return getTypeFromHandle(handle);
		}

		/// <summary>
		/// Returns an ilasm/ildasm-compatible name for the type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static string GetIlasmName(this Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (type == typeof(void)) return "void";
			if (type == typeof(bool)) return "bool";
			if (type == typeof(char)) return "char";
			if (type == typeof(sbyte)) return "int8";
			if (type == typeof(byte)) return "uint8";
			if (type == typeof(short)) return "int16";
			if (type == typeof(ushort)) return "uint16";
			if (type == typeof(int)) return "int32";
			if (type == typeof(uint)) return "uint32";
			if (type == typeof(long)) return "int64";
			if (type == typeof(ulong)) return "uint64";
			if (type == typeof(float)) return "float32";
			if (type == typeof(double)) return "float64";
			if (type == typeof(UIntPtr)) return "native uint";
			if (type == typeof(IntPtr)) return "native int";
			if (type == typeof(object)) return "object";
			if (type == typeof(string)) return "string";
			if (type == typeof(TypedReference)) return "typedref";

			if (type.IsPointer) return $"{type.GetElementType().GetIlasmName()}*";
			if (type.IsByRef) return $"{type.GetElementType().GetIlasmName()}&";
			if (type.IsGenericParameter)
			{
				if (type.DeclaringMethod != null)
					return $"!!{type.GenericParameterPosition}";
				return $"!{type.GenericParameterPosition}";
			}

			if (type.IsArray)
			{
				StringBuilder str = new StringBuilder(type.GetElementType().GetIlasmName());
				int rank = type.GetArrayRank();

				str.Append('[');
				for (int i = 1; i < rank; ++i)
					str.Append(',');
				str.Append(']');
				return str.ToString();
			}

			StringBuilder name = new StringBuilder();
			if (type.IsValueType)
			{
				name.Append("valuetype ");
			}
			else
			{
				name.Append("class ");
			}

			name.Append('[');
			name.Append(type.Assembly.GetName().Name);
			name.Append(']');

			if (type.IsNested)
			{
				List<Type> declaringTypes = new List<Type>();

				Type current = type.DeclaringType;
				while (current != null)
				{
					declaringTypes.Insert(0, current);
					current = current.DeclaringType;
				}

				foreach (Type declType in declaringTypes)
				{
					name.Append(declType.Name);
					name.Append('/');	
				}
				
				name.Append(type.Name);
			}
			else
			{
				name.Append(type.Name);
			}

			return name.ToString();
		}

		public static string GetIlasmName(this MethodBase method)
		{
			StringBuilder str = new StringBuilder();

			if (!method.IsStatic)
			{
				str.Append("instance ");
			}

			if (method is MethodInfo mi)
			{
				str.Append(mi.ReturnType.GetIlasmName());
				str.Append(' ');
			}

			if (method.DeclaringType != null)
			{
				str.Append(method.DeclaringType.GetIlasmName());
				str.Append("::");
			}

			str.Append(method.Name);
			str.Append('(');

			IEnumerable<Type> parameterTypes = method.GetParameters().Select(p => p.ParameterType);

			bool first=true;

			foreach (Type type in parameterTypes)
			{
				if (first)
					first = false;
				else
					str.Append(", ");

				str.Append(type.GetIlasmName());
			}

			str.Append(')');

			return str.ToString();
		}

		public static string GetIlasmName(this FieldInfo field)
		{
			StringBuilder str = new StringBuilder();

			if (!field.IsStatic)
			{
				str.Append("instance ");
			}

			str.Append(field.FieldType.GetIlasmName());
			str.Append(' ');

			str.Append(field.DeclaringType.GetIlasmName());
			str.Append("::");
			str.Append(field.Name);

			return str.ToString();
		}

		#endregion
	}
}
