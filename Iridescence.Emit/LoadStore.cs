﻿using System;
using System.Reflection;
using System.Threading;

namespace Iridescence.Emit
{
	/// <summary>
	/// Defines the load modes.
	/// </summary>
	public enum LoadMode
	{
		/// <summary>
		/// The value is loaded onto the stack.
		/// </summary>
		Value,

		/// <summary>
		/// The address of the value is loaded onto the stack.
		/// </summary>
		Address,
	}

	/// <summary>
	/// Provides means to laod a value (or address thereof) onto the stack.
	/// </summary>
	public interface ILoadProvider
	{
		/// <summary>
		/// Loads a value or address of a value onto the evulation stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="mode"></param>
		void Load(ILBuilder emit, LoadMode mode);
	}


	/// <summary>
	/// Provides means to store a value from the stack.
	/// </summary>
	public interface IStoreProvider
	{
		/// <summary>
		/// Begins a store operation.
		/// </summary>
		/// <param name="emit"></param>
		/// <returns></returns>
		StoreOperation Begin(ILBuilder emit);

		/// <summary>
		/// Ends a store operation.
		/// </summary>
		/// <param name="op"></param>
		void End(StoreOperation op);
	}
	
	public class StoreOperation : IDisposable
	{
		private IStoreProvider provider;

		public ILBuilder Emit { get; }
		
		internal StoreOperation(IStoreProvider provider, ILBuilder emit)
		{
			this.provider = provider ?? throw new ArgumentNullException(nameof(provider));
			this.Emit = emit ?? throw new ArgumentNullException(nameof(emit));
		}

		public void End()
		{
			IStoreProvider p = Interlocked.Exchange(ref this.provider, null);

			if (p == null)
				throw new InvalidOperationException("Can not end a store operation more than once.");

			p.End(this);
		}

		void IDisposable.Dispose()
		{
			this.End();
		}
	}

	internal sealed class LocalLoadStore : ILoadProvider, IStoreProvider
	{
		private readonly Local local;

		public LocalLoadStore(Local local)
		{
			this.local = local;
		}

		public void Load(ILBuilder emit, LoadMode mode)
		{
			
			if (mode == LoadMode.Value)
			{
				emit.LoadLocal(this.local);
			}
			else
			{
				emit.LoadLocalAddress(this.local);
			}
		}

		public StoreOperation Begin(ILBuilder emit)
		{
			return new StoreOperation(this, emit);
		}

		public void End(StoreOperation op)
		{
			op.Emit.StoreLocal(this.local);
		}
	}

	internal sealed class ArgumentLoadStore : ILoadProvider, IStoreProvider
	{
		private readonly int argumentIndex;
		private readonly bool isAddress;
		private readonly Type elementType;

		public ArgumentLoadStore(int argumentIndex, bool isAddress, Type elementType = null)
		{
			this.argumentIndex = argumentIndex;
			this.isAddress = isAddress;

			if (isAddress)
			{
				this.elementType = elementType ?? throw new ArgumentNullException(nameof(elementType));
			}
		}

		public void Load(ILBuilder emit, LoadMode mode)
		{
			if(this.isAddress)
			{
				if (mode == LoadMode.Address)
				{
					emit.LoadArgument(this.argumentIndex);
				}
				else
				{
					emit.LoadArgument(this.argumentIndex);
					emit.LoadIndirect(this.elementType);
				}
			}
			else
			{
				if (mode == LoadMode.Address)
				{
					emit.LoadArgumentAddress(this.argumentIndex);
				}
				else
				{
					emit.LoadArgument(this.argumentIndex);
				}
			}
		}

		public StoreOperation Begin(ILBuilder emit)
		{
			if (this.isAddress)
			{
				emit.LoadArgument(this.argumentIndex);
			}
			
			return new StoreOperation(this, emit);
		}

		public void End(StoreOperation op)
		{
			if (this.isAddress)
			{
				op.Emit.StoreIndirect(this.elementType);
			}
			else
			{
				op.Emit.StoreArgument(this.argumentIndex);
			}
		}
	}

	internal sealed class FieldLoadStore : ILoadProvider, IStoreProvider
	{
		private readonly FieldInfo field;
		private readonly ILoadProvider loadInstance;
		private readonly bool? isVolatile;
		private readonly int? unaligned;

		public FieldLoadStore(FieldInfo field, ILoadProvider loadInstance, bool? isVolatile, int? unaligned)
		{
			this.field = field ?? throw new ArgumentNullException(nameof(field));
			if (!field.IsStatic)
			{
				this.loadInstance = loadInstance ?? throw new ArgumentNullException(nameof(loadInstance));
			}

			this.isVolatile = isVolatile;
			this.unaligned = unaligned;
		}

		public void Load(ILBuilder emit, LoadMode mode)
		{
			if (!this.field.IsStatic)
			{
				this.loadInstance.Load(emit, this.field.DeclaringType.IsValueType ? LoadMode.Address : LoadMode.Value);
			}

			if (mode == LoadMode.Address)
			{
				emit.LoadFieldAddress(this.field);
			}
			else
			{
				emit.LoadField(this.field, this.isVolatile, this.unaligned);
			}
		}

		public StoreOperation Begin(ILBuilder emit)
		{
			if (!this.field.IsStatic)
			{
				this.loadInstance.Load(emit, this.field.DeclaringType.IsValueType ? LoadMode.Address : LoadMode.Value);
			}

			return new StoreOperation(this, emit);
		}

		public void End(StoreOperation op)
		{
			op.Emit.StoreField(this.field, this.isVolatile, this.unaligned);
		}
	}

	#region Old

	/// <summary>
	/// Loads a value onto the stack.
	/// </summary>
	/// <param name="mode">Specifies whether the address should be loaded instead of the value itself.</param>
	public delegate void LoadFunc(LoadMode mode);

	/// <summary>
	/// Begins a code block to store the value on the stack.
	/// After the value was placed on the stack, the callback returned from this method must be called to finalize the store.
	/// </summary>
	public delegate StoreEnd StoreFunc();

	/// <summary>
	/// A structure that can be used to end a store operation by disposing it.
	/// </summary>
	public struct StoreEnd : IDisposable
	{
		private Action action;

		public StoreEnd(Action action)
		{
			this.action = action;
		}

		public void Dispose()
		{
			Interlocked.Exchange(ref this.action, null)?.Invoke();
		}
	}

	#endregion

	/// <summary>
	/// Common load/store functions.
	/// </summary>
	public static class LoadStore
	{
		public static ILoadProvider LoadLocal(Local local)
		{
			return new LocalLoadStore(local);
		}

		public static IStoreProvider StoreLocal(Local local)
		{
			return new LocalLoadStore(local);
		}

		public static ILoadProvider LoadArgument(int argIndex)
		{
			return new ArgumentLoadStore(argIndex, false);
		}

		public static IStoreProvider StoreArgument(int argIndex)
		{
			return new ArgumentLoadStore(argIndex, false);
		}

		public static ILoadProvider LoadArgumentRef(int argIndex, Type elementType)
		{
			return new ArgumentLoadStore(argIndex, true, elementType);
		}

		public static IStoreProvider StoreArgumentRef(int argIndex, Type elementType)
		{
			return new ArgumentLoadStore(argIndex, false, elementType);
		}

		public static ILoadProvider LoadArgumentAuto(int argIndex, Type argType)
		{
			Type e = null;
			if (argType.IsByRef || argType.IsPointer)
			{
				e = argType.GetElementType();
			}

			return new ArgumentLoadStore(argIndex, e != null, e);
		}

		public static IStoreProvider StoreArgumentAuto(int argIndex, Type argType)
		{
			Type e = null;
			if (argType.IsByRef || argType.IsPointer)
			{
				e = argType.GetElementType();
			}

			return new ArgumentLoadStore(argIndex, e != null, e);
		}

		public static ILoadProvider LoadStaticField(FieldInfo staticField, bool? isVolatile = null)
		{
			if (!staticField.IsStatic) throw new ArgumentException("Field is not static.", nameof(staticField));
			return new FieldLoadStore(staticField, null, isVolatile, null);
		}

		public static IStoreProvider StoreStaticField(FieldInfo staticField, bool? isVolatile = null)
		{
			if (!staticField.IsStatic) throw new ArgumentException("Field is not static.", nameof(staticField));
			return new FieldLoadStore(staticField, null, isVolatile, null);
		}

		public static ILoadProvider LoadField(this ILoadProvider loadInstance, FieldInfo instanceField, bool? isVolatile = null, int? unaligned = null)
		{
			if (loadInstance == null) throw new ArgumentNullException(nameof(loadInstance));
			if (instanceField == null) throw new ArgumentNullException(nameof(instanceField));
			if (instanceField.IsStatic) throw new ArgumentException("Field is static.", nameof(instanceField));
			return new FieldLoadStore(instanceField, loadInstance, isVolatile, unaligned);
		}

		public static IStoreProvider StoreField(this ILoadProvider loadInstance, FieldInfo instanceField, bool? isVolatile = null, int? unaligned = null)
		{
			if (loadInstance == null) throw new ArgumentNullException(nameof(loadInstance));
			if (instanceField == null) throw new ArgumentNullException(nameof(instanceField));
			if (instanceField.IsStatic) throw new ArgumentException("Field is static.", nameof(instanceField));
			return new FieldLoadStore(instanceField, loadInstance, isVolatile, unaligned);
		}

		#region Old

		/// <summary>
		/// Returns the <see cref="LoadFunc"/> to load a local onto the stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="local"></param>
		/// <returns></returns>
		public static LoadFunc LoadLocal(ILBuilder emit, Local local)
		{
			return mode =>
			{
				if (mode == LoadMode.Address)
				{
					emit.LoadLocalAddress(local);
				}
				else
				{
					emit.LoadLocal(local);
				}
			};
		}

		/// <summary>
		/// Returns the <see cref="StoreFunc"/> to store a local.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="local"></param>
		/// <returns></returns>
		public static StoreFunc StoreLocal(ILBuilder emit, Local local)
		{
			return () => { return new StoreEnd(() => { emit.StoreLocal(local); }); };
		}

		/// <summary>
		/// Returns the <see cref="LoadFunc"/> to load an argument onto the stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="argType"></param>
		/// <param name="argIndex"></param>
		/// <returns></returns>
		public static LoadFunc LoadArgument(ILBuilder emit, Type argType, int argIndex)
		{
			if (argType.IsByRef)
			{
				return mode =>
				{
					if (mode == LoadMode.Address)
					{
						emit.LoadArgument(argIndex);
					}
					else
					{
						emit.LoadArgument(argIndex);
						emit.LoadIndirect(argType.GetElementType());
					}
				};
			}

			return mode =>
			{
				if (mode == LoadMode.Address)
				{
					emit.LoadArgumentAddress(argIndex);
				}
				else
				{
					emit.LoadArgument(argIndex);
				}
			};
		}

		/// <summary>
		/// Returns the <see cref="StoreFunc"/> to store an argument.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="argType"></param>
		/// <param name="argIndex"></param>
		/// <returns></returns>
		public static StoreFunc StoreArgument(ILBuilder emit, Type argType, int argIndex)
		{
			if (argType.IsByRef)
			{
				return () =>
				{
					emit.LoadArgument(argIndex);
					return new StoreEnd(() =>
					{
						Type elementType = argType.GetElementType();
						emit.StoreIndirect(elementType);
					});
				};
			}

			return () => { return new StoreEnd(() => { emit.StoreArgument(argIndex); }); };
		}

		public static LoadFunc LoadField(ILBuilder emit, FieldInfo field, LoadFunc loadInstance = null)
		{
			if (!field.IsStatic && loadInstance == null)
				throw new ArgumentException("The LoadFunc for the object instance must not be null if the field is an instance field.", nameof(loadInstance));

			return mode =>
			{
				if (!field.IsStatic)
				{
					loadInstance(field.DeclaringType.IsValueType ? LoadMode.Address : LoadMode.Value);
				}

				if (mode == LoadMode.Address)
				{
					emit.LoadFieldAddress(field);
				}
				else
				{
					emit.LoadField(field);
				}
			};
		}

		public static StoreFunc StoreField(ILBuilder emit, FieldInfo field, LoadFunc loadInstance = null)
		{
			if (!field.IsStatic && loadInstance == null)
				throw new ArgumentException("The LoadFunc for the object instance must not be null if the field is an instance field.", nameof(loadInstance));

			return () =>
			{
				if (!field.IsStatic)
				{
					loadInstance(field.DeclaringType.IsValueType ? LoadMode.Address : LoadMode.Value);
				}

				return new StoreEnd(() =>
				{
					emit.StoreField(field);
				});
			};
		}

		public static LoadFunc LoadMethodResult(ILBuilder emit, MethodInfo method, LoadFunc loadInstanceAndArgs = null)
		{
			if (!method.IsStatic && loadInstanceAndArgs == null)
				throw new ArgumentException("The LoadFunc for the object instance must not be null if the method is an instance method.", nameof(loadInstanceAndArgs));

			if (method.ReturnType.IsByRef)
			{
				return mode =>
				{
					loadInstanceAndArgs(method.DeclaringType.IsValueType ? LoadMode.Address : LoadMode.Value);

					if (method.IsStatic)
					{
						emit.Call(method);
					}
					else
					{
						emit.CallVirtual(method);
					}
					
					if (mode == LoadMode.Value)
					{
						emit.LoadIndirect(method.ReturnType.GetElementType());
					}
				};
			}

			return mode =>
			{
				loadInstanceAndArgs(method.DeclaringType.IsValueType ? LoadMode.Address : LoadMode.Value);
					
				if (mode == LoadMode.Address)
				{
					using (Local temp = emit.DeclareLocal(method.ReturnType, initializeReused: false))
					{
						if (method.IsStatic)
						{
							emit.Call(method);
						}
						else
						{
							emit.CallVirtual(method);
						}

						emit.StoreLocal(temp);
						emit.LoadLocalAddress(temp);
					}
				}
				else
				{
					if (method.IsStatic)
					{
						emit.Call(method);
					}
					else
					{
						emit.CallVirtual(method);
					}
				}
			};
		}

		public static StoreFunc StoreMethodLastArgument(ILBuilder emit, MethodInfo method, LoadFunc loadInstanceAndArgs = null)
		{
			if (!method.IsStatic && loadInstanceAndArgs == null)
				throw new ArgumentException("The LoadFunc for the object instance must not be null if the method is an instance method.", nameof(loadInstanceAndArgs));

			return () =>
			{
				loadInstanceAndArgs(method.DeclaringType.IsValueType ? LoadMode.Address : LoadMode.Value);
				return new StoreEnd(() =>
				{
					if (method.IsStatic)
					{
						emit.Call(method);
					}
					else
					{
						emit.CallVirtual(method);
					}
				});
			};
		}

		public static LoadFunc LoadProperty(ILBuilder emit, PropertyInfo property, LoadFunc loadInstance = null)
		{
			MethodInfo getter = property.GetGetMethod();
			return LoadMethodResult(emit, getter, loadInstance);
		}

		public static StoreFunc StoreProperty(ILBuilder emit, PropertyInfo property, LoadFunc loadInstance = null)
		{
			MethodInfo setter = property.GetSetMethod();
			return StoreMethodLastArgument(emit, setter, loadInstance);
		}

		#endregion
	}
}
