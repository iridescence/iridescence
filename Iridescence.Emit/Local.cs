﻿using System;
using System.Text;
using System.Threading;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents a variable local to the delegate being created.
	/// 
	/// To create a Local, call DeclareLocal().
	/// </summary>
	public class Local : IDisposable
	{
		#region Fields

		private Action<Local> onDispose;

		#endregion

		#region Properties
	
		/// <summary>
		/// Gets the name of this local.
		/// </summary>
		public string Name { get; }

		/// <summary>
		/// Gets the index of the local.
		/// </summary>
		public int Index { get; }

		/// <summary>
		/// The type stored in this local.
		/// </summary>
		public Type LocalType { get; }
		
		/// <summary>
		/// Gets a value that indicates whether the local is pinned.
		/// </summary>
		public bool IsPinned { get; }

		/// <summary>
		/// Gets a value that indicates whether this local has been disposed and can not be used anymore.
		/// </summary>
		public bool IsDisposed => this.DisposeOperation != null;

		internal bool IsReused { get; set; }

		/// <summary>
		/// The original local that this local is reusing.
		/// </summary>
		internal Local ReusedLocal { get; }

		/// <summary>
		/// Gets the operation at which the local was declared.
		/// </summary>
		internal Operation DeclarationOperation { get; set; }

		/// <summary>
		/// Gets the operation at which the local was released.
		/// </summary>
		internal Operation DisposeOperation { get; set; }
		
		#endregion

		#region Constructors

		internal Local(Type localType, int index, string name, Action<Local> onDispose, bool pinned)
		{
			if (localType == null)
				throw new ArgumentNullException(nameof(localType));

			this.onDispose = onDispose;
			this.LocalType = localType;
			this.Name = name;
			this.Index = index;
			this.IsPinned = pinned;
		}

		internal Local(Local reusedLocal, string name)
		{
			if (reusedLocal == null)
				throw new ArgumentNullException(nameof(reusedLocal));

			this.ReusedLocal = reusedLocal.ReusedLocal ?? reusedLocal;
			this.Index = this.ReusedLocal.Index;
			this.LocalType = this.ReusedLocal.LocalType;
			this.onDispose = this.ReusedLocal.onDispose;
			this.Name = name;
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Returns the type and name of this Local, in string form.
		/// </summary>
		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			str.Append(this.Index);

			if (this.Name != null)
			{
				str.Append(' ');
				str.Append(this.Name);
			}

			str.Append(" (");
			str.Append(this.LocalType.GetIlasmName());
			str.Append(")");
			return str.ToString();
		}

		/// <summary>
		/// Frees this local.
		/// 
		/// While not strictly required, freeing a local allows it's index to be reused.
		/// 
		/// Locals are only eligible for reuse when the new local is exactly the same type.
		/// </summary>
		public void Dispose()
		{
			Interlocked.Exchange(ref this.onDispose, null)?.Invoke(this);
		}
		
		#endregion
	}
}
