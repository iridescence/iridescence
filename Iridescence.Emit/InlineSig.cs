﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents the instruction operand for indirect calls, see (<see cref="OperandType.InlineSig"/>).
	/// </summary>
	public struct InlineSig : IEquatable<InlineSig>
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		public CallingConventions CallingConventions { get; }
		public Type ReturnType { get; }
		public ImmutableArray<Type> ParameterTypes { get; }
		public ImmutableArray<Type> OptionalParameterTypes { get; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="InlineSig"/> class. 
		/// </summary>
		public InlineSig(CallingConventions callingConvention, Type returnType, ImmutableArray<Type> parameterTypes, ImmutableArray<Type> optionalParameterTypes = default)
		{
			if (parameterTypes.IsDefault)
				throw new ArgumentNullException(nameof(parameterTypes));

			this.CallingConventions = callingConvention;
			this.ReturnType = returnType;
			this.ParameterTypes = parameterTypes;
			this.OptionalParameterTypes = optionalParameterTypes;
		}

		#endregion
		
		#region Methods

		/// <summary>
		/// Returns the encoded signature.
		/// </summary>
		/// <returns></returns>
		public byte[] GetSignature()
		{
			SignatureHelper sig = SignatureHelper.GetMethodSigHelper(this.CallingConventions, this.ReturnType);

			foreach (Type t in this.ParameterTypes)
			{
				sig.AddArgument(t);
			}

			if (!this.OptionalParameterTypes.IsDefault)
			{
				// add the sentinel
				sig.AddSentinel();
				foreach (Type t in this.OptionalParameterTypes)
				{
					sig.AddArgument(t);
				}					
			}

			return sig.GetSignature();
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			str.Append(this.CallingConventions);
			str.Append(' ');
			str.Append(this.ReturnType);

			foreach (Type parameterType in this.ParameterTypes)
			{
				str.Append(' ');
				str.Append(parameterType);
			}

			if (!this.OptionalParameterTypes.IsDefault)
			{
				str.Append(" __arglist(");
				bool first = true;
				foreach (Type parameterType in this.OptionalParameterTypes)
				{
					if (first)
						first = false;
					else
						str.Append(", ");

					str.Append(parameterType);
				}
				str.Append(")");
			}

			return str.ToString();
		}

		public bool Equals(InlineSig other)
		{
			if (this.CallingConventions != other.CallingConventions)
				return false;

			if(this.ReturnType != other.ReturnType)
				return false;

			if (!this.ParameterTypes.SequenceEqual(other.ParameterTypes))
				return false;

			if (this.OptionalParameterTypes == null && other.OptionalParameterTypes == null)
				return true;

			return this.OptionalParameterTypes.SequenceEqual(other.OptionalParameterTypes);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is InlineSig sig && this.Equals(sig);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = (int)this.CallingConventions;
				hashCode = (hashCode * 397) ^ this.ReturnType.GetHashCode();

				foreach (Type type in this.ParameterTypes)
				{
					hashCode = (hashCode * 397) ^ type.GetHashCode();
				}

				if (this.OptionalParameterTypes != null)
				{
					foreach (Type type in this.OptionalParameterTypes)
					{
						hashCode = (hashCode * 397) ^ type.GetHashCode();
					}
				}
				
				return hashCode;
			}
		}
		
		#endregion
	}
}
