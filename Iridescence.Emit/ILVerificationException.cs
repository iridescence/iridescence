﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Emit
{
	[Serializable]
	public class ILVerificationException : Exception
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors
		
		public ILVerificationException()
		{
		}

		public ILVerificationException(string message) : base(message)
		{
		}

		public ILVerificationException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected ILVerificationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		#endregion

		#region Methods

		#endregion
	}
}
