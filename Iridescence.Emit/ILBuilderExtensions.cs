﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	/// <summary>
	/// Convenience extensions for <see cref="ILBuilder"/>.
	/// </summary>
	public static class ILBuilderExtensions
	{
		/// <summary>
		/// Declare a new local of the given type in the current method.
		/// 
		/// Name is optional, and only provided for debugging purposes.  It has no
		/// effect on emitted IL.
		/// 
		/// Be aware that each local takes some space on the stack, inefficient use of locals
		/// could lead to StackOverflowExceptions at runtime.
		/// 
		/// Jil will reuse local index on the stack if the corresponding Local instance has been disposed.
		/// By default Jil will set reused locals to their default value, you can change this behavior
		/// by passing initializeReused = false.
		/// </summary>
		public static Local DeclareLocal<T>(this ILBuilder emit, string name = null, bool initializeReused = true)
		{
			return emit.DeclareLocal(typeof(T), name, initializeReused);
		}
		
		/// <summary>
		/// Begins a catch block for the given exception type in the given exception block.
		/// 
		/// The given exception block must still be open.
		/// </summary>
		public static CatchBlock BeginCatchBlock<TException>(this ILBuilder emit, ExceptionBlock forTry)
			where TException : Exception
		{
			return emit.BeginCatchBlock(forTry, typeof(TException));
		}

		/// <summary>
		/// Begins a catch block for all exceptions in the given exception block
		///
		/// The given exception block must still be open.
		/// 
		/// Equivalent to BeginCatchBlock(typeof(Exception), forTry).
		/// </summary>
		public static CatchBlock BeginCatchAllBlock(this ILBuilder emit, ExceptionBlock forTry)
		{
			return emit.BeginCatchBlock<Exception>(forTry);
		}

		/// <summary>
		/// Boxes the given value type on the stack, converting it into a reference.
		/// </summary>
		public static void Box<TValue>(this ILBuilder emit) where TValue : struct
		{
			emit.Box(typeof(TValue));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes a void return and no parameters.
		/// </summary>
		public static void CallIndirect(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(void), Array.Empty<Type>());
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// </summary>
		public static void CallIndirect(this ILBuilder emit, CallingConventions callConventions, Type returnType, params Type[] parameterTypes)
		{
			emit.CallIndirect(callConventions, returnType, parameterTypes.ToImmutableArray());
		}
		
		#region Generic CallIndirect Finder Helpers

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and no parameters.
		/// </summary>
		public static void CallIndirect<ReturnType>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void  CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public  static void  CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12, ParameterType13>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12), typeof(ParameterType13));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12, ParameterType13, ParameterType14>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12), typeof(ParameterType13), typeof(ParameterType14));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12, ParameterType13, ParameterType14, ParameterType15>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12), typeof(ParameterType13), typeof(ParameterType14), typeof(ParameterType15));
		}

		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This helper assumes ReturnType as a return and parameters of the types given in ParameterType*.
		/// </summary>
		public static void CallIndirect<ReturnType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12, ParameterType13, ParameterType14, ParameterType15, ParameterType16>(this ILBuilder emit, CallingConventions callConventions)
		{
			emit.CallIndirect(callConventions, typeof(ReturnType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12), typeof(ParameterType13), typeof(ParameterType14), typeof(ParameterType15), typeof(ParameterType16));
		}

		#endregion

		/// <summary>
		/// Cast a reference on the stack to the given reference type.
		/// 
		/// If the cast is not legal, a CastClassException will be thrown at runtime.
		/// </summary>
		public static void CastClass<TReference>(this ILBuilder emit) where TReference : class
		{
			emit.CastClass(typeof(TReference));
		}

		/// <summary>
		/// Convert a value on the stack to the given non-character primitive type.
		/// 
		/// Primitives are int8, uint8, int16, uint16, int32, uint32, int64, uint64, float, double, native int (IntPtr), and unsigned native int (UIntPtr). 
		/// </summary>
		public static void Convert(this ILBuilder emit, Type primitiveType)
		{
			if (primitiveType == null)
			{
				throw new ArgumentNullException(nameof(primitiveType));
			}

			if (!primitiveType.IsPrimitive)
			{
				throw new ArgumentException("Convert expects a primitive type.", nameof(primitiveType));
			}
			
			if (primitiveType == typeof(byte))
			{
				emit.ConvertToUInt8();
			}
			else if (primitiveType == typeof(sbyte) || primitiveType == typeof(bool)) // bool is an int8 on the stack
			{
				emit.ConvertToInt8();
			}
			else if (primitiveType == typeof(short))
			{
				emit.ConvertToInt16();
			}
			else if (primitiveType == typeof(ushort) || primitiveType == typeof(char))
			{
				emit.ConvertToUInt16();
			}
			else if (primitiveType == typeof(int))
			{
				emit.ConvertToInt32();
			}
			else if (primitiveType == typeof(uint))
			{
				emit.ConvertToUInt32();
			}
			else if (primitiveType == typeof(long))
			{
				emit.ConvertToInt64();
			}
			else if (primitiveType == typeof(ulong))
			{
				emit.ConvertToUInt64();
			}
			else if (primitiveType == typeof(IntPtr))
			{
				emit.ConvertToNativeInt();
			}
			else if (primitiveType == typeof(UIntPtr))
			{
				emit.ConvertToNativeUInt();
			}
			else if (primitiveType == typeof(float))
			{
				emit.ConvertToFloat32();
			}
			else if (primitiveType == typeof(double))
			{
				emit.ConvertToFloat64();
			}
			else
			{
				throw new Exception("Shouldn't be possible");	
			}
		}

		/// <summary>
		/// Convert a value on the stack to the given non-character primitive type.
		/// 
		/// Primitives are int8, uint8, int16, uint16, int32, uint32, int64, uint64, float, double, native int (IntPtr), and unsigned native int (UIntPtr). 
		/// </summary>
		public static void Convert<TPrimtive>(this ILBuilder emit) where TPrimtive : struct
		{
			emit.Convert(typeof(TPrimtive));
		}

		/// <summary>
		/// Convert a value on the stack to the given non-character, non-float, non-double primitive type.
		/// If the conversion would overflow at runtime, an OverflowException is thrown.
		/// 
		/// Primitives are int8, uint8, int16, uint16, int32, uint32, int64, uint64, float, double, native int (IntPtr), and unsigned native int (UIntPtr). 
		/// </summary>
		public static void ConvertOverflow(this ILBuilder emit, Type primitiveType)
		{
			if (primitiveType == null)
				throw new ArgumentNullException(nameof(primitiveType));

			if (!primitiveType.IsPrimitive || primitiveType == typeof(char))
				throw new ArgumentException("ConvertOverflow expects a non-character primitive type", nameof(primitiveType));
			
			if (primitiveType == typeof(byte))
			{
				emit.ConvertToUInt8Overflow();
			}
			else if (primitiveType == typeof(sbyte) || primitiveType == typeof(bool)) // bool is an int8 on the stack
			{
				emit.ConvertToInt8Overflow();
			}
			else if (primitiveType == typeof(short))
			{
				emit.ConvertToInt16Overflow();
			}
			else if (primitiveType == typeof(ushort))
			{
				emit.ConvertToUInt16Overflow();
			}
			else if (primitiveType == typeof(int))
			{
				emit.ConvertToInt32Overflow();
			}
			else if (primitiveType == typeof(uint))
			{
				emit.ConvertToUInt32Overflow();
			}
			else if (primitiveType == typeof(long))
			{
				emit.ConvertToInt64Overflow();
			}
			else if (primitiveType == typeof(ulong))
			{
				emit.ConvertToUInt64Overflow();
			}
			else if (primitiveType == typeof(IntPtr))
			{
				emit.ConvertToNativeIntOverflow();
			}
			else if (primitiveType == typeof(UIntPtr))
			{
				emit.ConvertToNativeUIntOverflow();
			}
			else if (primitiveType == typeof(float))
			{
				throw new InvalidOperationException("There is no operation for converting to a float with overflow checking");
			}
			else if (primitiveType == typeof(double))
			{
				throw new InvalidOperationException("There is no operation for converting to a double with overflow checking");
			}
			else
			{
				throw new Exception("Shouldn't be possible");	
			}
		}
		
		/// <summary>
		/// Convert a value on the stack to the given non-character, non-float, non-double primitive type.
		/// If the conversion would overflow at runtime, an OverflowException is thrown.
		/// 
		/// Primitives are int8, uint8, int16, uint16, int32, uint32, int64, uint64, float, double, native int (IntPtr), and unsigned native int (UIntPtr). 
		/// </summary>
		public static void ConvertOverflow<TPrimtive>(this ILBuilder emit) where TPrimtive : struct
		{
			emit.ConvertOverflow(typeof(TPrimtive));
		}

		/// <summary>
		/// Convert a value on the stack to the given non-character, non-float, non-double primitive type as if it were unsigned.
		/// If the conversion would overflow at runtime, an OverflowException is thrown.
		/// 
		/// Primitives are int8, uint8, int16, uint16, int32, uint32, int64, uint64, float, double, native int (IntPtr), and unsigned native int (UIntPtr). 
		/// </summary>
		public static void ConvertOverflowUnsigned(this ILBuilder emit, Type primitiveType)
		{
			if (primitiveType == null)
				throw new ArgumentNullException(nameof(primitiveType));

			if (!primitiveType.IsPrimitive || primitiveType == typeof(char))
				throw new ArgumentException("ConvertOverflowUnsigned expects a non-character primitive type", nameof(primitiveType));
			
			if (primitiveType == typeof(byte))
			{
				emit.ConvertToUInt8OverflowUnsigned();
			}
			else if (primitiveType == typeof(sbyte) || primitiveType == typeof(bool)) // bool is an int8 on the stack
			{
				emit.ConvertToInt8OverflowUnsigned();
			}
			else if (primitiveType == typeof(short))
			{
				emit.ConvertToInt16OverflowUnsigned();
			}
			else if (primitiveType == typeof(ushort))
			{
				emit.ConvertToUInt16OverflowUnsigned();
			}
			else if (primitiveType == typeof(int))
			{
				emit.ConvertToInt32OverflowUnsigned();
			}
			else if (primitiveType == typeof(uint))
			{
				emit.ConvertToUInt32OverflowUnsigned();
			}
			else if (primitiveType == typeof(long))
			{
				emit.ConvertToInt64OverflowUnsigned();
			}
			else if (primitiveType == typeof(ulong))
			{
				emit.ConvertToUInt64OverflowUnsigned();
			}
			else if (primitiveType == typeof(IntPtr))
			{
				emit.ConvertToNativeIntOverflowUnsigned();
			}
			else if (primitiveType == typeof(UIntPtr))
			{
				emit.ConvertToNativeUIntOverflowUnsigned();
			}
			else if (primitiveType == typeof(float))
			{
				throw new InvalidOperationException("There is no operation for converting to a float with overflow checking");
			}
			else if (primitiveType == typeof(double))
			{
				throw new InvalidOperationException("There is no operation for converting to a double with overflow checking");
			}
			else
			{
				throw new Exception("Shouldn't be possible");
			}
		}

		/// <summary>
		/// Convert a value on the stack to the given non-character, non-float, non-double primitive type as if it were unsigned.
		/// If the conversion would overflow at runtime, an OverflowException is thrown.
		/// 
		/// Primitives are int8, uint8, int16, uint16, int32, uint32, int64, uint64, float, double, native int (IntPtr), and unsigned native int (UIntPtr). 
		/// </summary>
		public static void ConvertOverflowUnsigned<TPrimtive>(this ILBuilder emit) where TPrimtive : struct
		{
			emit.ConvertOverflowUnsigned(typeof(TPrimtive));
		}

		/// <summary>
		/// Takes a destination pointer, a source pointer as arguments.  Pops both off the stack.
		/// 
		/// Copies the given value type from the source to the destination.
		/// </summary>
		public static void CopyObject<TValue>(this ILBuilder emit) where TValue : struct
		{
			emit.CopyObject(typeof(TValue));
		}
		
		/// <summary>
		/// Expects an instance of the type to be initialized on the stack.
		/// 
		/// Initializes all the fields on a value type to null or an appropriate zero value.
		/// </summary>
		public static void InitializeObject<TValue>(this ILBuilder emit)
		{
			emit.InitializeObject(typeof(TValue));
		}
		
		/// <summary>
		/// Pops a value from the stack and casts to the given type if possible pushing the result, otherwise pushes a null.
		/// 
		/// This is analogous to C#'s `as` operator.
		/// </summary>
		public static void IsInstance<T>(this ILBuilder emit)
		{
			emit.IsInstance(typeof(T));
		}

		public static void LoadArgument(this ILBuilder emit, int index)
		{
			if (index < ushort.MinValue || index > ushort.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(index));

			emit.LoadArgument((ushort)index);
		}

		public static void LoadArgumentAddress(this ILBuilder emit, int index)
		{
			if (index < ushort.MinValue || index > ushort.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(index));

			emit.LoadArgumentAddress((ushort)index);
		}

		/// <summary>
		/// Push a 1 onto the stack if b is true, and 0 if false.
		/// 
		/// Pushed values are int32s.
		/// </summary>
		public static void LoadConstant(this ILBuilder emit, bool b)
		{
			emit.LoadConstant(b ? 1 : 0);
		}

		/// <summary>
		/// Loads a constant value onto the stack.
		/// This calls the appropriate <see cref="LoadConstant"/> overload for the specified type parameter.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="emit"></param>
		/// <param name="value"></param>
		public static void LoadConstant<T>(this ILBuilder emit, T value)
		{
			switch (value)
			{
				case bool v:
					emit.LoadConstant(v);
					break;
					
				case sbyte v:
					emit.LoadConstant(v);
					break;

				case byte v:
					emit.LoadConstant(v);
					break;
				
				case short v:
					emit.LoadConstant(v);
					break;

				case ushort v:
					emit.LoadConstant(v);
					break;

				case char v:
					emit.LoadConstant(v);
					break;

				case int v:
					emit.LoadConstant(v);
					break;

				case uint v:
					emit.LoadConstant(v);
					break;

				case long v:
					emit.LoadConstant(v);
					break;

				case ulong v:
					emit.LoadConstant(v);
					break;

				case float v:
					emit.LoadConstant(v);
					break;

				case double v:
					emit.LoadConstant(v);
					break;

				case string v:
					emit.LoadConstant(v);
					break;

				case FieldInfo v:
					emit.LoadConstant(v);
					break;

				case MethodInfo v:
					emit.LoadConstant(v);
					break;

				case Type v:
					emit.LoadConstant(v);
					break;

				default:
					throw new ArgumentException($"A constant value of type {typeof(T)} can not be loaded onto the stack.", nameof(value));
			}
		}

		/// <summary>
		/// Expects a reference to an array and an index on the stack.
		/// 
		/// Pops both, and pushes the element in the array at the index onto the stack.
		/// </summary>
		public static void LoadElement<TElement>(this ILBuilder emit)
		{
			emit.LoadElement(typeof(TElement));
		}
		
		/// <summary>
		/// Expects a reference to an array of the given element type and an index on the stack.
		/// 
		/// Pops both, and pushes the address of the element at the given index.
		/// </summary>
		public static void LoadElementAddress<TElement>(this ILBuilder emit)
		{
			emit.LoadElementAddress(typeof(TElement));
		}

		/// <summary>
		/// Pops a pointer from the stack and pushes the value (of the given type) at that address onto the stack.
		/// </summary>
		public static void LoadIndirect<T>(this ILBuilder emit, bool isVolatile = false, int? unaligned = null)
		{
			emit.LoadIndirect(typeof(T), isVolatile, unaligned);
		}
		
		/// <summary>
		/// Converts a pointer or reference to a value on the stack into a TypedReference of the given type.
		/// 
		/// TypedReferences can be used with ReferenceAnyType and ReferenceAnyValue to pass arbitrary types as parameters.
		/// </summary>
		public static void MakeReferenceAny<T>(this ILBuilder emit)
		{
			emit.MakeReferenceAny(typeof(T));
		}

		/// <summary>
		/// Pops a size from the stack, allocates a rank-1 array of the given type, and pushes a reference to the new array onto the stack.
		/// </summary>
		public static void NewArray<TElement>(this ILBuilder emit)
		{
			emit.NewArray(typeof(TElement));
		}

		/// <summary>
		/// Pops parameterTypes.Length arguments from the stack, invokes the constructor on the given type that matches parameterTypes, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject(this ILBuilder emit, Type type, params Type[] parameterTypes)
		{
			if (type == null)
			{
				throw new ArgumentNullException(nameof(type));
			}

			if (parameterTypes == null)
			{
				throw new ArgumentNullException(nameof(parameterTypes));
			}

			ConstructorInfo[] allCons = type.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance
#if !COREFX
				| BindingFlags.CreateInstance
#endif
			);
			ConstructorInfo cons =
				allCons.SingleOrDefault(c =>
					c.GetParameters().Length == parameterTypes.Length &&
					c.GetParameters().Select((p, i) => p.ParameterType == parameterTypes[i]).Aggregate(true, (a, b) => a && b));

			if (cons == null)
			{
				throw new InvalidOperationException("Type " + type + " must have a constructor that matches parameters [" + String.Join(", ", parameterTypes.AsEnumerable()) + "]");
			}

			emit.NewObject(cons);
		}

		public static void NewObject(this ILBuilder il, ConstructorBuilder ctor, params Type[] types)
		{
			il.NewObject(ctor, types.ToImmutableArray());
		}

		#region Generic NewObject Constructor Finder Helpers

				/// <summary>
		/// Invokes the parameterless constructor of the given type, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12, ParameterType13>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12), typeof(ParameterType13));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12, ParameterType13, ParameterType14>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12), typeof(ParameterType13), typeof(ParameterType14));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12, ParameterType13, ParameterType14, ParameterType15>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12), typeof(ParameterType13), typeof(ParameterType14), typeof(ParameterType15));
		}

		/// <summary>
		/// Pops # of parameter arguments from the stack, invokes the the constructor of the given reference type that matches the given parameter types, and pushes a reference to the new object onto the stack.
		/// </summary>
		public static void NewObject<ReferenceType, ParameterType1, ParameterType2, ParameterType3, ParameterType4, ParameterType5, ParameterType6, ParameterType7, ParameterType8, ParameterType9, ParameterType10, ParameterType11, ParameterType12, ParameterType13, ParameterType14, ParameterType15, ParameterType16>(this ILBuilder emit)
		{
			emit.NewObject(typeof(ReferenceType), typeof(ParameterType1), typeof(ParameterType2), typeof(ParameterType3), typeof(ParameterType4), typeof(ParameterType5), typeof(ParameterType6), typeof(ParameterType7), typeof(ParameterType8), typeof(ParameterType9), typeof(ParameterType10), typeof(ParameterType11), typeof(ParameterType12), typeof(ParameterType13), typeof(ParameterType14), typeof(ParameterType15), typeof(ParameterType16));
		}

		#endregion

		/// <summary>
		/// Converts a TypedReference on the stack into a reference to the contained object, given the type contained in the TypedReference.
		/// 
		/// __makeref(int) on the stack would become an int&amp;, for example.
		/// </summary>
		public static void ReferenceAnyValue<T>(this ILBuilder emit)
		{
			emit.ReferenceAnyValue(typeof(T));
		}

		/// <summary>
		/// Pushes the size of the given value type onto the stack.
		/// </summary>
		public static void SizeOf<TValue>(this ILBuilder emit) where TValue : struct
		{
			emit.SizeOf(typeof(TValue));
		}

		public static void StoreArgument(this ILBuilder emit, int index)
		{
			emit.StoreArgument(checked((ushort)index));
		}

		/// <summary>
		/// Pops a value, an index, and a reference to an array off the stack.  Places the given value into the given array at the given index.
		/// </summary>
		public static void StoreElement<TElement>(this ILBuilder emit)
		{
			emit.StoreElement(typeof(TElement));
		}

		/// <summary>
		/// Pops a value of the given type and a pointer off the stack, and stores the value at the address in the pointer.
		/// </summary>
		public static void StoreIndirect<T>(this ILBuilder emit, bool isVolatile = false, int? unaligned = null)
		{
			emit.StoreIndirect(typeof(T), isVolatile, unaligned);
		}

		/// <summary>
		/// Pops a boxed value from the stack and pushes a pointer to it's unboxed value.
		/// 
		/// To load the value directly onto the stack, use UnboxAny().
		/// </summary>
		public static void Unbox<TValue>(this ILBuilder emit)
		{
			emit.Unbox(typeof(TValue));
		}
		
		/// <summary>
		/// Pops a boxed value from the stack, unboxes it and pushes the value onto the stack.
		/// 
		/// To get an address for the unboxed value instead, use Unbox().
		/// </summary>
		public static void UnboxAny<TValue>(this ILBuilder emit)
		{
			emit.UnboxAny(typeof(TValue));
		}

		/// <summary>
		/// Emits a call to <see cref="String.Format(string, object[])"/> with the specified locals as arguments.
		/// The format string is expected to be on the stack already.
		/// </summary>
		public static void FormatString(this ILBuilder emit, IReadOnlyList<Local> locals)
		{
			if (locals == null)
				throw new ArgumentNullException(nameof(locals));

			int count = locals.Count;
			if (count == 0)
				return;
			
			// Check if there is an overload that takes that many objects directly without having to create an object[].
			Type[] testArgs = new Type[1 + count];
			testArgs[0] = typeof(string);
			for (int i = 0; i < count; ++i)
			{
				testArgs[i + 1] = typeof(object);
			}

			MethodInfo method = typeof(string).GetMethod(nameof(string.Format), testArgs);
			if (method != null)
			{
				for (int i = 0; i < count; i++)
				{
					emit.LoadLocal(locals[i]);
					if (locals[i].LocalType.IsValueType)
					{
						emit.Box(locals[i].LocalType);
					}
				}

				// Call string.Format(string, object, [...])
				emit.Call(method);
				return;
			}

			// Create object[] with the locals as items.
			emit.LoadConstant(count);
			emit.NewArray<object>();
			
			for (int i = 0; i < count; i++)
			{
				emit.Duplicate();
				emit.LoadConstant(i);
				emit.LoadLocal(locals[i]);

				if (locals[i].LocalType.IsValueType)
				{
					emit.Box(locals[i].LocalType);
				}

				emit.StoreElement<object>();
			}
			
			// Call string.Format(string, object[])
			emit.Call(typeof(string).GetMethod(nameof(string.Format), new [] { typeof(string), typeof(object[]) }));
		}

		/// <summary>
		/// Emits a call to <see cref="String.Format(string, object[])"/> with the specified locals as arguments.
		/// The format string is expected to be on the stack already.
		/// </summary>
		public static void FormatString(this ILBuilder emit, params Local[] locals)
		{
			emit.FormatString((IReadOnlyList<Local>)locals);
		}

		/// <summary>
		/// Emits a call to <see cref="Console.WriteLine(string, object[])"/> with the specified locals as arguments.
		/// The format string is expected to be on the stack already.
		/// </summary>
		public static void WriteLine(this ILBuilder emit, IReadOnlyList<Local> locals)
		{
			if (locals == null)
				throw new ArgumentNullException(nameof(locals));

			int count = locals.Count;
			if (count == 0)
			{
				emit.Call(typeof(Console).GetMethod(nameof(Console.WriteLine), new[] { typeof(string) }));
				return;
			}

			emit.LoadConstant(count);
			emit.NewArray<object>();
			
			for (int i = 0; i < count; i++)
			{
				emit.Duplicate();
				emit.LoadConstant(i);
				emit.LoadLocal(locals[i]);

				if (locals[i].LocalType.IsValueType)
				{
					emit.Box(locals[i].LocalType);
				}

				emit.StoreElement<object>();
			}
			
			emit.Call(typeof(Console).GetMethod(nameof(Console.WriteLine), new [] { typeof(string), typeof(object[]) }));
		}

		/// <summary>
		/// Emits a call to <see cref="Console.WriteLine(string, object[])"/> with the specified locals as arguments.
		/// The format string is expected to be on the stack already.
		/// </summary>
		public static void WriteLine(this ILBuilder emit, params Local[] locals)
		{
			emit.WriteLine((IReadOnlyList<Local>)locals);
		}

		private static readonly MethodInfo getTypeFromHandleMethod = typeof(Type).GetMethod(nameof(Type.GetTypeFromHandle), BindingFlags.Public | BindingFlags.Static);

		/// <summary>
		/// Loads the specified <see cref="Type"/> onto the stack. Equivalent to the C# typeof operator.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="type"></param>
		public static void LoadType(this ILBuilder emit, Type type)
		{
			emit.LoadConstant(type);
			emit.Call(getTypeFromHandleMethod);
		}

		/// <summary>
		/// Emits a call to the "Invoke" method of the specified delegate type.
		/// The delegate instance is expected to be on the stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="delegateType"></param>
		public static void InvokeDelegate(this ILBuilder emit, Type delegateType)
		{
			if(!delegateType.IsSubclassOf(typeof(Delegate)))
				throw new ArgumentException("delegateType must be a subclass of Delegate.", nameof(delegateType));

			MethodInfo invokeMethod = delegateType.GetMethod("Invoke") ?? throw new MissingMethodException();

			emit.CallVirtual(invokeMethod);
		}

		#region Span

		/// <summary>
		/// Pushes the length of a <see cref="Span{T}"/> onto the stack.
		/// Expects the address of a <see cref="Span{T}"/> is already on the stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="elementType">The element type.</param>
		public static void LoadSpanLength(this ILBuilder emit, Type elementType)
		{
			emit.Call(typeof(Span<>).MakeGenericType(elementType).GetMethod("get_Length"));
		}

		/// <summary>
		/// Pushes the length of a <see cref="Span{T}"/> onto the stack.
		/// Expects the address of a <see cref="Span{T}"/> is already on the stack.
		/// </summary>
		/// <typeparam name="T">The element type.</typeparam>
		/// <param name="emit"></param>
		public static void LoadSpanLength<T>(this ILBuilder emit)
		{
			emit.Call(typeof(Span<T>).GetMethod("get_Length"));
		}

		/// <summary>
		/// Pushes the address to an element of a <see cref="Span{T}"/> onto the stack.
		/// Expects the address of a <see cref="Span{T}"/> and index are already on the stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="elementType">The element type.</param>
		public static void LoadSpanElementAddress(this ILBuilder emit, Type elementType)
		{
			emit.Call(typeof(Span<>).MakeGenericType(elementType).GetMethod("get_Item"));
		}

		/// <summary>
		/// Pushes the address to an element of a <see cref="Span{T}"/> onto the stack.
		/// Expects the address of a <see cref="Span{T}"/> and index are already on the stack.
		/// </summary>
		/// <typeparam name="T">The element type.</typeparam>
		/// <param name="emit"></param>
		public static void LoadSpanElementAddress<T>(this ILBuilder emit)
		{
			emit.Call(typeof(Span<T>).GetMethod("get_Item"));
		}
		
		/// <summary>
		/// Pushes the element of a <see cref="Span{T}"/> onto the stack.
		/// Expects the address of a <see cref="Span{T}"/> and index are already on the stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="elementType">The element type.</param>
		public static void LoadSpanElement(this ILBuilder emit, Type elementType)
		{
			emit.Call(typeof(Span<>).MakeGenericType(elementType).GetMethod("get_Item"));
			emit.LoadIndirect(elementType);
		}

		/// <summary>
		/// Pushes the element of a <see cref="Span{T}"/> onto the stack.
		/// Expects the address of a <see cref="Span{T}"/> and index are already on the stack.
		/// </summary>
		/// <typeparam name="T">The element type.</typeparam>
		/// <param name="emit"></param>
		public static void LoadSpanElement<T>(this ILBuilder emit)
		{
			emit.Call(typeof(Span<T>).GetMethod("get_Item"));
			emit.LoadIndirect<T>();
		}
		
		#endregion

		#region ReadOnlySpan

		/// <summary>
		/// Pushes the length of a <see cref="ReadOnlySpan{T}"/> onto the stack.
		/// Expects the address of a <see cref="ReadOnlySpan{T}"/> is already on the stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="elementType">The element type.</param>
		public static void LoadReadOnlySpanLength(this ILBuilder emit, Type elementType)
		{
			emit.Call(typeof(ReadOnlySpan<>).MakeGenericType(elementType).GetMethod("get_Length"));
		}

		/// <summary>
		/// Pushes the length of a <see cref="ReadOnlySpan{T}"/> onto the stack.
		/// Expects the address of a <see cref="ReadOnlySpan{T}"/> is already on the stack.
		/// </summary>
		/// <typeparam name="T">The element type.</typeparam>
		/// <param name="emit"></param>
		public static void LoadReadOnlySpanLength<T>(this ILBuilder emit)
		{
			emit.Call(typeof(ReadOnlySpan<T>).GetMethod("get_Length"));
		}

		/// <summary>
		/// Pushes the address to an element of a <see cref="ReadOnlySpan{T}"/> onto the stack.
		/// Expects the address of a <see cref="ReadOnlySpan{T}"/> and index are already on the stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="elementType">The element type.</param>
		public static void LoadReadOnlySpanElementAddress(this ILBuilder emit, Type elementType)
		{
			emit.Call(typeof(ReadOnlySpan<>).MakeGenericType(elementType).GetMethod("get_Item"));
		}

		/// <summary>
		/// Pushes the address to an element of a <see cref="ReadOnlySpan{T}"/> onto the stack.
		/// Expects the address of a <see cref="ReadOnlySpan{T}"/> and index are already on the stack.
		/// </summary>
		/// <typeparam name="T">The element type.</typeparam>
		/// <param name="emit"></param>
		public static void LoadReadOnlySpanElementAddress<T>(this ILBuilder emit)
		{
			emit.Call(typeof(ReadOnlySpan<T>).GetMethod("get_Item"));
		}
		
		/// <summary>
		/// Pushes the element of a <see cref="ReadOnlySpan{T}"/> onto the stack.
		/// Expects the address of a <see cref="ReadOnlySpan{T}"/> and index are already on the stack.
		/// </summary>
		/// <param name="emit"></param>
		/// <param name="elementType">The element type.</param>
		public static void LoadReadOnlySpanElement(this ILBuilder emit, Type elementType)
		{
			emit.Call(typeof(ReadOnlySpan<>).MakeGenericType(elementType).GetMethod("get_Item"));
			emit.LoadIndirect(elementType);
		}

		/// <summary>
		/// Pushes the element of a <see cref="ReadOnlySpan{T}"/> onto the stack.
		/// Expects the address of a <see cref="ReadOnlySpan{T}"/> and index are already on the stack.
		/// </summary>
		/// <typeparam name="T">The element type.</typeparam>
		/// <param name="emit"></param>
		public static void LoadReadOnlySpanElement<T>(this ILBuilder emit)
		{
			emit.Call(typeof(ReadOnlySpan<T>).GetMethod("get_Item"));
			emit.LoadIndirect<T>();
		}
		
		#endregion

	}
}