﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public class ConstructorILBuilder : ILBuilderImpl
	{
		private readonly ConstructorBuilder constructorBuilder;
		private bool isBuilt;

		public override CallableMethod Method => new CallableMethod(this.constructorBuilder, this.Signature, this.CallingConvention);
		protected override bool IsConstructor => true;
		protected override Type DeclaringType => this.constructorBuilder.DeclaringType;

		protected ConstructorILBuilder(MethodSignature signature, CallingConventions callConvention, ILBuilderOptions options, ConstructorBuilder constructorBuilder)
			: base(signature, callConvention, options)
		{
			this.constructorBuilder = constructorBuilder ?? throw new ArgumentNullException(nameof(constructorBuilder));
		}

		public static ConstructorILBuilder Create(Type[] parameterTypes, TypeBuilder type, MethodAttributes attributes, CallingConventions callingConvention = CallingConventions.HasThis, ILBuilderOptions options = ILBuilderOptions.Default)
		{
			if (parameterTypes == null)
				throw new ArgumentNullException(nameof(parameterTypes));

			if (type == null)
				throw new ArgumentNullException(nameof(type));

			ValidateAttributesAndConvention(attributes, callingConvention);

			if ((callingConvention & CallingConventions.HasThis) == 0)
				throw new ArgumentException("Constructors always have a this reference");

			ConstructorBuilder constructorBuilder = type.DefineConstructor(attributes, callingConvention, parameterTypes);

			// Constructors always have a `this`
			MethodSignature signature = new MethodSignature(typeof(void), parameterTypes).WithThis(type);

			return new ConstructorILBuilder(signature, callingConvention, options, constructorBuilder);
		}

		public static ConstructorILBuilder CreateStatic(MethodSignature signature, TypeBuilder type, ILBuilderOptions options = ILBuilderOptions.Default)
		{
			if (signature == null)
				throw new ArgumentNullException(nameof(signature));

			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (signature.ReturnType != typeof(void))
				throw new ArgumentException("DelegateType used must return void");

			if (signature.ParameterTypes.Length > 0)
				throw new ArgumentException("A type initializer can have no arguments.");

			ConstructorBuilder constructorBuilder = type.DefineTypeInitializer();

			return new ConstructorILBuilder(signature, CallingConventions.Standard, options, constructorBuilder);
		}

		public ConstructorBuilder Compile(OptimizationOptions optimizationOptions = OptimizationOptions.All)
		{
			if (this.constructorBuilder == null)
				throw new InvalidOperationException("Emit was not created to build a constructor, thus CreateConstructor cannot be called");

			if (!this.isBuilt)
			{
				this.ValidateAndOptimize(optimizationOptions);
				this.isBuilt = true;

				ILGeneratorCompiler.Compile(this.Operations, this.constructorBuilder.GetILGenerator());
			}

			return this.constructorBuilder;
		}
	}
}