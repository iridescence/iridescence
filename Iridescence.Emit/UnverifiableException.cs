﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Emit
{
	/// <summary>
	/// Exception that is thrown when trying to perform an unverifiable operation when not allowed.
	/// </summary>
	public class UnverifiableException : ILVerificationException
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public UnverifiableException(string message) : base(message)
		{
		}

		public UnverifiableException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected UnverifiableException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		#endregion

		#region Methods

		#endregion
	}
}
