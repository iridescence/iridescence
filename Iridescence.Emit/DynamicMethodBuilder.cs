﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public class DynamicMethodBuilder : ILBuilderImpl
	{
		private readonly DynamicMethod dynamicMethod;
		private bool isBuilt;

		public override CallableMethod Method => new CallableMethod(this.dynamicMethod, this.Signature, this.CallingConvention);
		protected override bool IsConstructor => false;
		protected override Type DeclaringType => null;

		protected DynamicMethodBuilder(MethodSignature signature, CallingConventions callConvention, ILBuilderOptions options, DynamicMethod dynamicMethod)
			: base(signature, callConvention, options)
		{
			this.dynamicMethod = dynamicMethod ?? throw new ArgumentNullException(nameof(dynamicMethod));
		}

		public static DynamicMethodBuilder Create(MethodSignature signature, string name = null, ModuleBuilder module = null, ILBuilderOptions options = ILBuilderOptions.Default)
		{
			if (signature == null)
				throw new ArgumentNullException(nameof(signature));

			module ??= DynamicModule.Module;

			if ((options & ILBuilderOptions.AllowUnverifiable) != 0 && !DynamicModule.AllowsUnverifiableCode(module))
				throw new InvalidOperationException("The module does not allow unverifiable code.");

			name ??= $"DynamicMethod-{Guid.NewGuid()}";

			DynamicMethod dynMethod = new DynamicMethod(name, signature.ReturnType, signature.ParameterTypes.ToArray(), module, skipVisibility: true);

			return new DynamicMethodBuilder(signature, dynMethod.CallingConvention, options, dynMethod);
		}

		public static DynamicMethodBuilder Create(MethodSignature sig, Type owner, string name = null, ILBuilderOptions options = ILBuilderOptions.Default)
		{
			if (sig == null)
				throw new ArgumentNullException(nameof(sig));

			if (owner == null)
				return Create(sig, name);

			if ((options & ILBuilderOptions.AllowUnverifiable) != 0 && !DynamicModule.AllowsUnverifiableCode(owner.Module))
				throw new InvalidOperationException("The module does not allow unverifiable code.");

			name ??= $"DynamicMethod-{Guid.NewGuid()}";

			DynamicMethod dynMethod = new DynamicMethod(name, sig.ReturnType, sig.ParameterTypes.ToArray(), owner, skipVisibility: true);

			return new DynamicMethodBuilder(sig, dynMethod.CallingConvention, options, dynMethod);
		}

		protected virtual void Compile(DynamicMethod method)
		{
			ILGeneratorCompiler.Compile(this.Operations, this.dynamicMethod.GetILGenerator());
		}

		public DynamicMethod Compile(OptimizationOptions optimizationOptions = OptimizationOptions.All)
		{
			if (!this.isBuilt)
			{
				this.ValidateAndOptimize(optimizationOptions);
				this.isBuilt = true;
				this.Compile(this.dynamicMethod);
			}

			return this.dynamicMethod;
		}

		public Delegate CreateDelegate(Type delegateType, object target = null, OptimizationOptions optimizationOptions = OptimizationOptions.All)
		{
			if (!delegateType.IsDelegateType())
				throw new ArgumentException("delegateType must be a Delegate.", nameof(delegateType));

			DynamicMethod method = this.Compile(optimizationOptions);
			return method.CreateDelegate(delegateType, target);
		}

		public TDelegate CreateDelegate<TDelegate>(object target = null, OptimizationOptions optimizationOptions = OptimizationOptions.All)
			where TDelegate : Delegate
		{
			return (TDelegate)this.CreateDelegate(typeof(TDelegate), target, optimizationOptions);
		}
	}
}