﻿namespace Iridescence.Emit
{
	/// <summary>
	/// Represents a filter block. This block is part of an <see cref="ExceptionBlock"/>.
	/// </summary>
	public class FilterBlock
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		/// <summary>
		/// Gets the <see cref="ExceptionBlock"/> this <see cref="FilterBlock"/> appears in.
		/// </summary>
		public ExceptionBlock ExceptionBlock { get; }

		public bool IsOpen => this.EndOperation == null;

		internal Operation StartOperation { get; set; }

		internal Operation EndOperation { get; set; }
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="FilterBlock"/> class. 
		/// </summary>
		internal FilterBlock(ExceptionBlock forTry)
		{
			this.ExceptionBlock = forTry;
		}
		
		#endregion
		
		#region Methods
		
		#endregion
	}
}
