﻿namespace Iridescence.Emit
{
	/// <summary>
	/// Represents a finally block, which appears within an ExceptionBlock.
	/// 
	/// This is roughly analogous to `finally` in C#.
	/// </summary>
	public class FinallyBlock
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// The ExceptionBlock this FinallyBlock appears as part of.
		/// </summary>
		public ExceptionBlock ExceptionBlock { get; }

		public bool IsOpen => this.EndOperation == null;

		internal Operation StartOperation { get; set; }

		internal Operation EndOperation { get; set; }

		#endregion

		#region Constructors

		internal FinallyBlock(ExceptionBlock exceptionBlock)
		{
			this.ExceptionBlock = exceptionBlock;
		}

		#endregion

		#region Methods

		#endregion
	}
}
