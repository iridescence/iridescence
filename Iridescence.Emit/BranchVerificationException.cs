﻿using System.Runtime.Serialization;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents a branch verification exception, containing information about the branch operation and target.
	/// </summary>
	public class BranchVerificationException : OperationVerificationException
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		/// <summary>
		/// Gets the branch target.
		/// </summary>
		public Label BranchTarget { get; }

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="BranchVerificationException"/> class. 
		/// </summary>
		public BranchVerificationException(string operationName, Label branchTarget, string message)
			: base(operationName, message)
		{
			this.BranchTarget = branchTarget;
		}

		protected BranchVerificationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{

		}

		#endregion

		#region Methods

		internal static BranchVerificationException CreateStackMismatch(string operationName, Label target, EvaluationStack expectedStack, EvaluationStack currentStack)
		{
			StringBuilder str = new StringBuilder();

			str.AppendLine("Branch evaluation stack mismatch.");
			str.Append("Target label: ");
			str.AppendLine(target.ToString());
			str.AppendLine("The evaluation stack at the label is expected to be the following:");
			str.AppendLine(expectedStack.ToString());
			str.AppendLine($"But the operation \"{operationName}\" has this stack:");
			str.AppendLine(currentStack.ToString());

			return new BranchVerificationException(operationName, target, str.ToString());
		}

		#endregion
	}
}
