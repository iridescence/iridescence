﻿using System.Collections.Generic;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents an ExceptionBlock, which is roughly analogous to a try + catch + finally block in C#.
	/// 
	/// To create an ExceptionBlock call BeginExceptionBlock().
	/// </summary>
	public class ExceptionBlock
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// A label which marks the end of the ExceptionBlock.
		/// 
		/// This Label is meant to be targetted by Leave() from anywhere except a FinallyBlock
		/// in the ExceptionBlock.
		/// 
		/// Remember that it is illegal to branch from within an ExceptionBlock to outside.
		/// </summary>
		public Label Label { get; }

		public bool IsOpen => this.EndOperation == null;

		internal Operation StartOperation { get; set; }

		internal Operation EndOperation { get; set; }

		internal List<FilterBlock> FilterBlocks { get; }

		internal List<CatchBlock> CatchBlocks { get; }

		internal FinallyBlock FinallyBlock { get; set; }

		#endregion

		#region Constructors

		internal ExceptionBlock(Label label)
		{
			this.Label = label;
			this.FilterBlocks = new List<FilterBlock>();
			this.CatchBlocks = new List<CatchBlock>();
		}
		
		#endregion

		#region Methods

		#endregion
	}
}
