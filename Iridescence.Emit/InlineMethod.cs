﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents the instruction operand for calls, see (<see cref="OperandType.InlineMethod"/>).
	/// </summary>
	public struct InlineMethod
	{
		#region Properties
		
		/// <summary>
		/// Gets the method.
		/// </summary>
		public MethodBase Method { get; }

		/// <summary>
		/// Gets the return type of the method, or void if it doesn't have one.
		/// </summary>
		public Type ReturnType => (this.Method as MethodInfo)?.ReturnType ?? typeof(void);

		/// <summary>
		/// Gets the parameter types of the method.
		/// Normally, these can be inferred from the <see cref="Method"/> itself, but in the case of a <see cref="MethodBuilder"/>, these have to be specified by the user.
		/// </summary>
		public ImmutableArray<Type> ParameterTypes { get; }

		/// <summary>
		/// Gets the optional parameter types for arglist.
		/// </summary>
		public ImmutableArray<Type> OptionalParameterTypes { get; }

		/// <summary>
		/// Gets the <see cref="InlineSig"/> for this method.
		/// </summary>
		public InlineSig Signature => new InlineSig(this.Method.CallingConvention, this.ReturnType, this.ParameterTypes, this.OptionalParameterTypes);

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="InlineMethod"/> class. 
		/// </summary>
		public InlineMethod(MethodBase method, ImmutableArray<Type> optionalParameterTypes = default)
		{
			this.Method = method;
			this.ParameterTypes = this.Method.GetParameters().Select(t => t.ParameterType).ToImmutableArray();
			this.OptionalParameterTypes = optionalParameterTypes;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="InlineMethod"/> class. 
		/// </summary>
		public InlineMethod(MethodBase method, ImmutableArray<Type> optionalParameterTypes, ImmutableArray<Type> parameterTypes)
		{
			if (parameterTypes.IsDefault)
				throw new ArgumentNullException(nameof(parameterTypes));

			this.Method = method;
			this.ParameterTypes = parameterTypes;
			this.OptionalParameterTypes = optionalParameterTypes;
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			if (this.Method is MethodBuilder)
			{
				str.Append(this.Method.Name);
			}
			else
			{
				str.Append(this.Method);
			}

			if (!this.OptionalParameterTypes.IsDefault)
			{
				str.Append(" __arglist(");
				bool first = true;
				foreach (Type parameterType in this.OptionalParameterTypes)
				{
					if (first)
						first = false;
					else
						str.Append(", ");

					str.Append(parameterType);
				}
				str.Append(")");
			}

			return str.ToString();
		}

		#endregion
	}
}
