﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents the signature of a method or delegate.
	/// </summary>
	public sealed class MethodSignature
	{
		#region Fields
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the method return type.
		/// </summary>
		public Type ReturnType { get; }

		/// <summary>
		/// Gets the parameter types.
		/// </summary>
		public ImmutableArray<Type> ParameterTypes { get; }

		/// <summary>
		/// Gets a method signature that does not return anything and has no parameters.
		/// </summary>
		public static MethodSignature Void { get; } = new MethodSignature(typeof(void), ImmutableArray<Type>.Empty); 

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MethodSignature"/> class. 
		/// </summary>
		public MethodSignature(Type returnType, ImmutableArray<Type> parameterTypes)
		{
			if (parameterTypes == null)
				throw new ArgumentNullException(nameof(parameterTypes));

			if (parameterTypes.Any(t => t == null))
				throw new ArgumentException("A parameter type can not be null.", nameof(parameterTypes));
			
			this.ReturnType = returnType ?? throw new ArgumentNullException(nameof(returnType));
			this.ParameterTypes = parameterTypes;
		}

		
		/// <summary>
		/// Initializes a new instance of the <see cref="MethodSignature"/> class. 
		/// </summary>
		public MethodSignature(Type returnType, IEnumerable<Type> parameterTypes)
			: this(returnType, parameterTypes.ToImmutableArray())
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MethodSignature"/> class. 
		/// </summary>
		public MethodSignature(Type returnType, params Type[] parameterTypes)
			: this(returnType, parameterTypes.ToImmutableArray())
		{

		}

		#endregion
		
		#region Methods

		public static MethodSignature FromDelegate(Type delegateType)
		{
			if (delegateType == null)
				throw new ArgumentNullException(nameof(delegateType));
			
			if (!delegateType.IsDelegateType())
				throw new ArgumentException("Type is not a Delegate.", nameof(delegateType));

			MethodInfo invoke = delegateType.GetMethod("Invoke") ?? throw new MissingMethodException(delegateType.FullName, "Invoke");

			return new MethodSignature(invoke.ReturnType, invoke.GetParameters().Select(s => s.ParameterType));
		}

		public static MethodSignature FromDelegate(Delegate del)
		{
			if (del == null)
				throw new ArgumentNullException(nameof(del));

			MethodSignature sig = MethodSignature.FromDelegate(del.GetType());

			if (del.Target != null)
				sig = sig.WithThis(del.Target.GetType());

			return sig;
		}

		public static MethodSignature FromMethod(MethodInfo method)
		{
			return new MethodSignature(method.ReturnType, method.GetParameters().Select(p => p.ParameterType));
		}

		/// <summary>
		/// Returns a <see cref="MethodSignature"/> with an additional parameter inserted at the start.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public MethodSignature WithThis(Type type)
		{
			return new MethodSignature(this.ReturnType, this.ParameterTypes.Insert(0, type));
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			str.Append('(');

			bool first = true;

			foreach (Type p in this.ParameterTypes)
			{
				if (first)
					first = false;
				else
					str.Append(", ");

				str.Append(p);
			}

			str.Append(')');

			if (this.ReturnType != null)
			{
				str.Append(" => ");
				str.Append(this.ReturnType);
			}

			return str.ToString();
		}

		#endregion
	}
}
