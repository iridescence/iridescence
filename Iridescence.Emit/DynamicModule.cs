﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	/// <summary>
	/// <see cref="ModuleBuilder"/> singleton.
	/// </summary>
	public static class DynamicModule
	{
		#region Fields

		private static readonly object mutex = new object();
		private static ModuleBuilder module;

		#endregion

		#region Properties
		
		/// <summary>
		/// Provides a default <see cref="ModuleBuilder"/>.
		/// </summary>
		public static ModuleBuilder Module
		{
			get
			{
				lock (mutex)
				{
					if (module == null)
					{
						string name = typeof(DynamicModule).Assembly.GetName().Name + ".DynamicModule";
						AssemblyBuilder asm = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName(name), AssemblyBuilderAccess.Run);

#if DEBUG
						Type dbgAttr = typeof(DebuggableAttribute);
						ConstructorInfo dbgCtor = dbgAttr.GetConstructor(new[] {typeof(DebuggableAttribute.DebuggingModes)});

						asm.SetCustomAttribute(new CustomAttributeBuilder(dbgCtor, new object[]
						{
							DebuggableAttribute.DebuggingModes.Default | DebuggableAttribute.DebuggingModes.DisableOptimizations
						}));
#endif

						module = asm.DefineDynamicModule(name);
					}

					return module;
				}
			}
		}
		
		#endregion

		#region Constructors

		#endregion

		#region Methods

		public static bool AllowsUnverifiableCode(Module m)
		{
			return Attribute.IsDefined(m, typeof(System.Security.UnverifiableCodeAttribute));
		}

		public static bool AllowsUnverifiableCode(ModuleBuilder m)
		{
			DynamicMethod canaryMethod = new DynamicMethod("__Canary" + Guid.NewGuid(), typeof(void), Array.Empty<Type>(), m);
			ILGenerator il = canaryMethod.GetILGenerator();
			il.Emit(OpCodes.Ldc_I4, 1024);
			il.Emit(OpCodes.Localloc);
			il.Emit(OpCodes.Pop);
			il.Emit(OpCodes.Ret);

			Action d1 = (Action)canaryMethod.CreateDelegate(typeof(Action));

			try
			{
				d1();
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}
		
		#endregion
	}
}
