﻿using System.Runtime.Serialization;

namespace Iridescence.Emit
{
	/// <summary>
	/// Represents an exception that is thrown when an invalid operation is emitted.
	/// </summary>
	public class OperationVerificationException : ILVerificationException
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		/// <summary>
		/// Gets the name of the operation that caused the exception.
		/// </summary>
		public string OperationName { get; }

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="OperationVerificationException"/> class. 
		/// </summary>
		public OperationVerificationException(string operationName, string message)
			: base(message)
		{
			this.OperationName = operationName;
		}

		protected OperationVerificationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		#endregion

		#region Methods

		#endregion
	}
}
