﻿using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	/// <summary>
	/// Wrapper around various kinds of methods. This is required because e.g. <see cref="DynamicMethod"/> throws exceptions when trying to access some of its properties during IL generation.
	/// </summary>
	public readonly struct CallableMethod
	{
		#region Properties

		/// <summary>
		/// Gets the (possibly incomplete) method.
		/// </summary>
		public MethodBase Method { get; }

		/// <summary>
		/// Gets the method signature. This should always be used because the <see cref="Method"/> might not be finalized yet.
		/// </summary>
		public MethodSignature Signature { get; }

		/// <summary>
		/// Gets the calling convention for the emitted method.
		/// </summary>
		public CallingConventions CallingConvention { get; }

		#endregion

		#region Constructors

		public CallableMethod(MethodBase method, MethodSignature signature, CallingConventions callingConventions)
		{
			this.Method = method;
			this.Signature = signature;
			this.CallingConvention = callingConventions;
		}

		#endregion

		#region Methods

		public static implicit operator CallableMethod(MethodInfo method)
		{
			return new CallableMethod(method, MethodSignature.FromMethod(method), method.CallingConvention);
		}

		public static implicit operator CallableMethod(ILBuilder emit)
		{
			return emit.Method;
		}

		#endregion
	}
}