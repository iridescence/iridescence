﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public class MethodILBuilder : ILBuilderImpl
	{
		private readonly MethodBuilder methodBuilder;
		private bool isBuilt;

		public override CallableMethod Method => new CallableMethod(this.methodBuilder, this.Signature, this.CallingConvention);
		protected override bool IsConstructor => false;
		protected override Type DeclaringType => this.methodBuilder.DeclaringType;

		protected MethodILBuilder(MethodSignature signature, CallingConventions callConvention, ILBuilderOptions options, MethodBuilder methodBuilder) 
			: base(signature, callConvention, options)
		{
			this.methodBuilder = methodBuilder ?? throw new ArgumentNullException(nameof(methodBuilder));
		}

		public static MethodILBuilder Create(MethodSignature signature, TypeBuilder type, string name, MethodAttributes attributes = MethodAttributes.Public | MethodAttributes.Static, CallingConventions callingConventions = CallingConventions.Standard, ILBuilderOptions options = ILBuilderOptions.Default)
		{
			if (signature == null)
				throw new ArgumentNullException(nameof(signature));

			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (name == null)
				throw new ArgumentNullException(nameof(name));

			ValidateAttributesAndConvention(attributes, callingConventions);

			MethodBuilder methodBuilder = type.DefineMethod(name, attributes, callingConventions, signature.ReturnType, signature.ParameterTypes.ToArray());

			if ((callingConventions & CallingConventions.HasThis) != 0)
			{
				// Shove `this` in front, can't require it because it doesn't exist yet!
				signature = signature.WithThis(type);
			}

			return new MethodILBuilder(signature, callingConventions, options, methodBuilder);
		}

		public static MethodILBuilder CreateOverride(
			TypeBuilder type,
			MethodInfo methodToOverride,
			MethodAttributes attributes = MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig | MethodAttributes.Virtual)
		{
			return Create(MethodSignature.FromMethod(methodToOverride), type, methodToOverride.Name, attributes, methodToOverride.CallingConvention);
		}

		public MethodBuilder Compile(OptimizationOptions optimizationOptions = OptimizationOptions.All)
		{
			if (this.methodBuilder == null)
				throw new InvalidOperationException("Emit was not created to build a method, thus CreateMethod cannot be called.");

			if (!this.isBuilt)
			{
				this.ValidateAndOptimize(optimizationOptions);
				this.isBuilt = true;

				ILGeneratorCompiler.Compile(this.Operations, this.methodBuilder.GetILGenerator());
			}

			return this.methodBuilder;
		}

	}
}
