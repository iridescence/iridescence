﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pops a reference to a rank 1 array off the stack, and pushes it's length onto the stack.
		/// </summary>
		public override void LoadLength()
		{
			IStackTransition t = StackTransition.Pop(typeof(Array)).Push(typeof(int));

			this.emit(OpCodes.Ldlen, tag(t));
		}
	}
}
