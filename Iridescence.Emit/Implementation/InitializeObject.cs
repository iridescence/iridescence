﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Expects an instance of the type to be initialized on the stack.
		/// 
		/// Initializes all the fields on a value type to null or an appropriate zero value.
		/// </summary>
		public override void InitializeObject(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			IStackTransition[] transitions =
			{
				StackTransition.Pop<NativeInt>(),
				StackTransition.Pop(type.MakePointerType()),
				StackTransition.Pop(type.MakeByRefType()),
			};

			this.emit(OpCodes.Initobj, type, tag(transitions));
		}
	}
}
