﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Leave an exception or catch block, branching to the given label.
		/// 
		/// This instruction empties the stack.
		/// </summary>
		public override void Leave(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			if (!this.IsInExceptionBlock || this.IsInFinallyBlock)
				throw new InvalidOperationException("Leave can only be used within an exception block and can not be used to exit a finally block.");

			// Note that Leave *always* nuked the stack; nothing survies exiting an exception block
			this.emit(OpCodes.Leave, label, tag(new ClearStackTransition()));

			this.mustMark = true;

			//VerificationResult valid = this.CurrentVerifiers.UnconditionalBranch(label);
			//if (!valid.Success)
			//{
			//	throw new EmitVerificationException(this.IL, valid);
			//}
		}
	}
}
