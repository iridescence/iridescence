﻿using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pushes a copy of the current top value on the stack.
		/// </summary>
		public override void Duplicate()
		{
			this.emit(OpCodes.Dup, tag(StackTransition.Duplicate));
		}
	}
}
