﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pops a value from the stack and stores it in the given field.
		/// 
		/// If the field is an instance member, both a value and a reference to the instance are popped from the stack.
		/// </summary>
		public override void StoreField(FieldInfo field, bool? isVolatile = null, int? unaligned = null)
		{
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			if (unaligned.HasValue && (unaligned != 1 && unaligned != 2 && unaligned != 4))
				throw new ArgumentException("unaligned must be null, 1, 2, or 4", nameof(unaligned));

			bool useVolatile = isVolatile ?? field.IsVolatile();

			if (!field.IsStatic)
			{
				if (useVolatile)
					this.emit(OpCodes.Volatile, tag(StackTransition.None));

				if (unaligned.HasValue)
					this.emit(OpCodes.Unaligned, (byte)unaligned.Value, tag(StackTransition.None));

				Type onType = field.DeclaringType;
				if (onType.IsValueType)
					onType = onType.MakePointerType();

				IStackTransition t = StackTransition.Pop(field.FieldType).Pop(onType);

				this.emit(OpCodes.Stfld, field, tag(t));
			}
			else
			{
				if (unaligned.HasValue)
					throw new ArgumentException("unaligned cannot be used with static fields.", nameof(unaligned));

				if (useVolatile)
					this.emit(OpCodes.Volatile, tag(StackTransition.None));

				IStackTransition t = StackTransition.Pop(field.FieldType);

				this.emit(OpCodes.Stsfld, field, tag(t));
			}
		}
	}
}
