﻿using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Emits an instruction that does nothing.
		/// </summary>
		public override void Nop()
		{
			this.emit(OpCodes.Nop, tag(StackTransition.None));
		}
	}
}
