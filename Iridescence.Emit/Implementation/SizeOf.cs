﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pushes the size of the given type onto the stack.
		/// </summary>
		public override void SizeOf(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			this.emit(OpCodes.Sizeof, type, tag(StackTransition.Push<int>()));
		}
	}
}
