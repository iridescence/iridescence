﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition[] makeReferenceTransitions =
		{
			StackTransition.Pop<NativeInt>().Push(typeof(TypedReference)),
			StackTransition.Pop<AnyPointer>().Push(typeof(TypedReference)),
			StackTransition.Pop<AnyByRef>().Push(typeof(TypedReference)),
		};

		private static readonly IStackTransition referenceAnyTypeTransition = StackTransition.Pop(typeof(TypedReference)).Push(typeof(RuntimeTypeHandle));

		/// <summary>
		/// Converts a pointer or reference to a value on the stack into a TypedReference of the given type.
		/// 
		/// TypedReferences can be used with ReferenceAnyType and ReferenceAnyValue to pass arbitrary types as parameters.
		/// </summary>
		public override void MakeReferenceAny(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			this.emit(OpCodes.Mkrefany, type, tag(makeReferenceTransitions));
		}
		
		/// <summary>
		/// Converts a TypedReference on the stack into a RuntimeTypeHandle for the type contained with it.
		/// 
		/// __makeref(int) on the stack would become the RuntimeTypeHandle for typeof(int), for example.
		/// </summary>
		public override void ReferenceAnyType()
		{
			this.emit(OpCodes.Refanytype, tag(referenceAnyTypeTransition));
		}

		/// <summary>
		/// Converts a TypedReference on the stack into a reference to the contained object, given the type contained in the TypedReference.
		/// 
		/// __makeref(int) on the stack would become an int&amp;, for example.
		/// </summary>
		public override void ReferenceAnyValue(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			IStackTransition t = StackTransition.Pop(typeof(TypedReference)).Push(type.MakeByRefType());

			this.emit(OpCodes.Refanyval, type, tag(t));
		}
	}
}
