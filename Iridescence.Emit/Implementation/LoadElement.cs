﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static IStackTransition[] makeLoadElementTransitions(Type arrayType, Type elementType, bool loadAddress = false)
		{
			if (loadAddress)
			{
				elementType = elementType.MakeByRefType();
			}

			return new[]
			{
				StackTransition.Pop<int>().Pop(arrayType).Push(elementType),
				StackTransition.Pop<NativeInt>().Pop(arrayType).Push(elementType),
			};
		}

		/// <summary>
		/// Expects a reference to an array and an index on the stack.
		/// 
		/// Pops both, and pushes the element in the array at the index onto the stack.
		/// </summary>
		public override void LoadElement(Type elementType)
		{
			if (elementType == null)
				throw new ArgumentNullException(nameof(elementType));

			Type arrayType = elementType.MakeArrayType();

			OpCode instr;
			IStackTransition[] transitions;

			if (elementType.IsPointer)
			{
				transitions = makeLoadElementTransitions(arrayType, elementType);
				instr = OpCodes.Ldelem_I;
			}
			else if (!elementType.IsValueType)
			{
				transitions = new IStackTransition[] {new LoadElementRefTransition()};
				instr = OpCodes.Ldelem_Ref;
			}
			else if (elementType == typeof(sbyte))
			{
				transitions = makeLoadElementTransitions(arrayType, typeof(int));
				instr = OpCodes.Ldelem_I1;
			}
			else if (elementType == typeof(byte))
			{
				transitions = makeLoadElementTransitions(arrayType, typeof(int));
				instr = OpCodes.Ldelem_U1;
			}
			else if (elementType == typeof(short))
			{
				transitions = makeLoadElementTransitions(arrayType, typeof(int));
				instr = OpCodes.Ldelem_I2;
			}
			else if (elementType == typeof(ushort))
			{
				transitions = makeLoadElementTransitions(arrayType, typeof(int));
				instr = OpCodes.Ldelem_U2;
			}
			else if (elementType == typeof(int))
			{
				transitions = makeLoadElementTransitions(arrayType, typeof(int));
				instr = OpCodes.Ldelem_I4;
			}
			else if (elementType == typeof(uint))
			{
				transitions = makeLoadElementTransitions(arrayType, typeof(int));
				instr = OpCodes.Ldelem_U4;
			}
			else if ((elementType == typeof(long) || elementType == typeof(ulong)))
			{
				transitions = makeLoadElementTransitions(arrayType, typeof(long));
				instr = OpCodes.Ldelem_I8;
			}
			else if (elementType == typeof(float))
			{
				transitions = makeLoadElementTransitions(arrayType, typeof(float));
				instr = OpCodes.Ldelem_R4;
			}
			else if (elementType == typeof(double))
			{
				transitions = makeLoadElementTransitions(arrayType, typeof(double));
				instr = OpCodes.Ldelem_R8;
			}
			else
			{
				transitions = makeLoadElementTransitions(arrayType, elementType);
				this.emit(OpCodes.Ldelem, elementType, tag(transitions));
				return;
			}

			this.emit(instr, tag(transitions));
		}

				/*
		private void InjectReadOnly()
		{
			while(this.ReadonlyPatches.Count > 0)
			{
				Tuple<int, TypeOnStack> elem = this.ReadonlyPatches[0];
				this.ReadonlyPatches.RemoveAt(0);

				int at = elem.Item1;
				TypeOnStack value = elem.Item2;

				int asObjToLdfd = value.CountMarks(OpCodes.Ldfld, 0);
				int asObjToLdfda = value.CountMarks(OpCodes.Ldflda, 0);
				int asObjToStfd = value.CountMarks(OpCodes.Stfld, 0);

				int asObjToCall = value.CountMarks(OpCodes.Call, 0);

				int asPtrToLdobj = value.CountMarks(OpCodes.Ldobj, 0);
				int asPtrToLdind =
					value.CountMarks(OpCodes.Ldind_I, 0) +
					value.CountMarks(OpCodes.Ldind_I1, 0) +
					value.CountMarks(OpCodes.Ldind_I2, 0) +
					value.CountMarks(OpCodes.Ldind_I4, 0) +
					value.CountMarks(OpCodes.Ldind_I8, 0) +
					value.CountMarks(OpCodes.Ldind_R4, 0) +
					value.CountMarks(OpCodes.Ldind_R8, 0) +
					value.CountMarks(OpCodes.Ldind_Ref, 0) +
					value.CountMarks(OpCodes.Ldind_U1, 0) +
					value.CountMarks(OpCodes.Ldind_U2, 0) +
					value.CountMarks(OpCodes.Ldind_U4, 0);

				int asSourceToCpobj = value.CountMarks(OpCodes.Cpobj, 0);

				int totalAllowedUses =
					asObjToLdfd +
					asObjToLdfda +
					asObjToStfd +
					asPtrToLdobj +
					asPtrToLdind +
					asSourceToCpobj +
					asObjToCall;

				int totalActualUses = value.CountMarks();

				if (totalActualUses == totalAllowedUses)
				{
					this.InsertInstruction(at, OpCodes.Readonly);
				}
			}
		}*/

		/// <summary>
		/// Expects a reference to an array of the given element type and an index on the stack.
		/// 
		/// Pops both, and pushes the address of the element at the given index.
		/// </summary>
		public override void LoadElementAddress(Type elementType, bool readOnly = false)
		{
			Type arrayType = elementType.MakeArrayType();

			if (readOnly)
			{
				this.emit(OpCodes.Readonly, tag(StackTransition.None));
			}

			this.emit(OpCodes.Ldelema, elementType, tag(makeLoadElementTransitions(arrayType, elementType, true)));
		}
	}
}
