﻿using System.Linq;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition[] binaryNumericOperations =
		{
			StackTransition.Pop<int>().Pop<int>().Push<int>(),
			StackTransition.Pop<int>().Pop<NativeInt>().Push<NativeInt>(),
			StackTransition.Pop<NativeInt>().Pop<int>().Push<NativeInt>(),
			StackTransition.Pop<NativeInt>().Pop<NativeInt>().Push<NativeInt>(),

			StackTransition.Pop<long>().Pop<long>().Push<long>(),

			StackTransition.Pop<float>().Pop<float>().Push<float>(),
			StackTransition.Pop<double>().Pop<double>().Push<double>(),
		};

		private static readonly IStackTransition[] binaryNumericOperationsAdd = binaryNumericOperations.Concat(new[]
		{
			StackTransition.PopPushSame(0, typeof(AnyPointer), typeof(int)),
			StackTransition.PopPushSame(0, typeof(AnyByRef), typeof(int)),

			StackTransition.PopPushSame(0, typeof(AnyPointer), typeof(NativeInt)),
			StackTransition.PopPushSame(0, typeof(AnyByRef), typeof(NativeInt)),

			StackTransition.PopPushSame(1, typeof(int), typeof(AnyPointer)),
			StackTransition.PopPushSame(1, typeof(int), typeof(AnyByRef)),

			StackTransition.PopPushSame(1, typeof(NativeInt), typeof(AnyPointer)),
			StackTransition.PopPushSame(1, typeof(NativeInt), typeof(AnyByRef)),
		}).ToArray();

		private static readonly IStackTransition[] binaryNumericOperationsSub = binaryNumericOperations.Concat(new[]
		{
			StackTransition.PopPushSame(0, typeof(AnyPointer), typeof(int)),
			StackTransition.PopPushSame(0, typeof(AnyByRef), typeof(int)),

			StackTransition.PopPushSame(0, typeof(AnyPointer), typeof(NativeInt)),
			StackTransition.PopPushSame(0, typeof(AnyByRef), typeof(NativeInt)),

			StackTransition.Pop<AnyByRef>().Pop<AnyByRef>().Push<NativeInt>(),
		}).ToArray();

		private static readonly IStackTransition[] negateTransitions =
		{
			StackTransition.Pop(typeof(int)).Push(typeof(int)),
			StackTransition.Pop(typeof(long)).Push(typeof(long)),
			StackTransition.Pop(typeof(NativeInt)).Push(typeof(NativeInt)),
			StackTransition.Pop(typeof(float)).Push(typeof(float)),
			StackTransition.Pop(typeof(double)).Push(typeof(double)),
		};

		/// <summary>
		/// Pops two arguments off the stack, adds them, and pushes the result.
		/// </summary>
		public override void Add()
		{
			this.emit(OpCodes.Add, tag(binaryNumericOperationsAdd));
		}

		/// <summary>
		/// Pops two arguments off the stack, adds them, and pushes the result.
		/// 
		/// Throws an OverflowException if the result overflows the destination type.
		/// </summary>
		public override void AddOverflow()
		{
			this.emit(OpCodes.Add_Ovf, tag(binaryNumericOperationsAdd));
		}

		/// <summary>
		/// Pops two arguments off the stack, adds them as if they were unsigned, and pushes the result.
		/// 
		/// Throws an OverflowException if the result overflows the destination type.
		/// </summary>
		public override void AddOverflowUnsigned()
		{
			this.emit(OpCodes.Add_Ovf_Un, tag(binaryNumericOperationsAdd));
		}

		/// <summary>
		/// Pops two arguments off the stack, divides the second by the first, and pushes the result.
		/// </summary>
		public override void Divide()
		{
			this.emit(OpCodes.Div, tag(binaryNumericOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, divides the second by the first as if they were unsigned, and pushes the result.
		/// </summary>
		public override void DivideUnsigned()
		{
			this.emit(OpCodes.Div_Un, tag(integerOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, multiplies them, and pushes the result.
		/// </summary>
		public override void Multiply()
		{
			this.emit(OpCodes.Mul, tag(binaryNumericOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, multiplies them, and pushes the result.
		/// 
		/// Throws an OverflowException if the result overflows the destination type.
		/// </summary>
		public override void MultiplyOverflow()
		{
			this.emit(OpCodes.Mul_Ovf, tag(binaryNumericOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, multiplies them as if they were unsigned, and pushes the result.
		/// 
		/// Throws an OverflowException if the result overflows the destination type.
		/// </summary>
		public override void MultiplyOverflowUnsigned()
		{
			this.emit(OpCodes.Mul_Ovf_Un, tag(binaryNumericOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, calculates the remainder of the second divided by the first, and pushes the result.
		/// </summary>
		public override void Remainder()
		{
			this.emit(OpCodes.Rem, tag(binaryNumericOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, calculates the remainder of the second divided by the first as if both were unsigned, and pushes the result.
		/// </summary>
		public override void RemainderUnsigned()
		{
			this.emit(OpCodes.Rem_Un, tag(binaryNumericOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, subtracts the first from the second, and pushes the result.
		/// </summary>
		public override void Subtract()
		{
			this.emit(OpCodes.Sub, tag(binaryNumericOperationsSub));
		}

		/// <summary>
		/// Pops two arguments off the stack, subtracts the first from the second, and pushes the result.
		/// 
		/// Throws an OverflowException if the result overflows the destination type.
		/// </summary>
		public override void SubtractOverflow()
		{
			this.emit(OpCodes.Sub_Ovf, tag(binaryNumericOperationsSub));
		}

		/// <summary>
		/// Pops two arguments off the stack, subtracts the first from the second as if they were unsigned, and pushes the result.
		/// 
		/// Throws an OverflowException if the result overflows the destination type.
		/// </summary>
		public override void SubtractOverflowUnsigned()
		{
			this.emit(OpCodes.Sub_Ovf_Un, tag(binaryNumericOperationsSub));
		}

		/// <summary>
		/// Pops an argument off the stack, negates it, and pushes the result.
		/// </summary>
		public override void Negate()
		{
			this.emit(OpCodes.Neg, tag(negateTransitions));
		}
	}
}
