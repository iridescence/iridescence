﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Loads the argument at the given index (starting at 0) for the current method onto the stack.
		/// </summary>
		public override void LoadArgument(ushort index)
		{
			if (this.Signature.ParameterTypes.Length == 0)
				throw new ArgumentException("Method has no arguments to load.");
			
			if (index >= this.Signature.ParameterTypes.Length)
				throw new ArgumentOutOfRangeException(nameof(index), $"index must be between 0 and {this.Signature.ParameterTypes.Length - 1}, inclusive.");

			TaggedStackTransitions transitions = tag(StackTransition.Push(this.Signature.ParameterTypes[index]));

			this.emit(OpCodes.Ldarg, unchecked((short)index), transitions);
		}

		/// <summary>
		/// Loads a pointer to the argument at index (starting at zero) onto the stack.
		/// </summary>
		public override void LoadArgumentAddress(ushort index)
		{
			if (this.Signature.ParameterTypes.Length == 0)
				throw new ArgumentException("Method has no arguments to load.");
			
			if (index >= this.Signature.ParameterTypes.Length)
				throw new ArgumentOutOfRangeException(nameof(index), "index must be between 0 and " + (this.Signature.ParameterTypes.Length - 1) + ", inclusive.");

			Type argType = this.Signature.ParameterTypes[index].MakeByRefType();
			this.emit(OpCodes.Ldarga, unchecked((short)index), tag(StackTransition.Push(argType)));
		}
	}
}
