﻿using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition[] checkFiniteTransitions =
		{
			StackTransition.Pop(typeof(float)).Push(typeof(float)),
			StackTransition.Pop(typeof(float)).Push(typeof(float)),
		};

		/// <summary>
		/// Throws an ArithmeticException on runtime if the value on the stack is not a finite number.
		/// 
		/// This leaves the value checked on the stack, rather than popping it as might be expected.
		/// </summary>
		public override void CheckFinite()
		{
			// ckfinite leaves the value on the stack, oddly enough
			this.emit(OpCodes.Ckfinite, tag(checkFiniteTransitions));
		}
	}
}
