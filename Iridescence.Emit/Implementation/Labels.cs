﻿using System;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Defines a new label.
		/// 
		/// This label can be used for branching, leave, and switch instructions.
		/// 
		/// A label must be marked exactly once after being defined, using the MarkLabel() method.        
		/// </summary>
		public override Label DefineLabel(string name = null)
		{
			Label label = this.Operations.DefineLabel(name).Label;
			return label;
		}

		/// <summary>
		/// Marks a label in the instruction stream.
		/// 
		/// When branching, leaving, or switching with a label control will be transferred to where it was *marked* not defined.
		/// 
		/// Labels can only be marked once, and *must* be marked before creating a delegate.
		/// </summary>
		public override void MarkLabel(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			if (!this.mustMark)
			{
				if (this.labelStacks.TryGetValue(label, out EvaluationStack expectedStack))
				{
					if (!this.currentStack.IsCompatible(expectedStack))
					{
						throw BranchVerificationException.CreateStackMismatch(nameof(this.MarkLabel), label, expectedStack, this.currentStack);
					}
				}
			}
		
			this.Operations.MarkLabel(label);

			if (this.mustMark)
			{
				if (this.labelStacks.TryGetValue(label, out EvaluationStack expectedStack))
				{
					this.currentStack = expectedStack;
				}
				else
				{
					this.currentStack = EvaluationStack.Empty;
				}

				this.mustMark = false;
			}
		}
	}
}
