﻿using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition throwTransition = StackTransition.Pop(typeof(object)).Chain(new ClearStackTransition());

		/// <summary>
		/// Pops a value off the stack and throws it as an exception.
		/// 
		/// Throw expects the value to be or extend from a System.Exception.
		/// </summary>
		public override void Throw()
		{
			this.emit(OpCodes.Throw, tag(throwTransition));
			this.mustMark = true;

			//VerificationResult verify = this.CurrentVerifiers.Throw();
			//if (!verify.Success)
			//{
			//	throw new EmitVerificationException(this.IL, verify);
			//}
		}
	}
}
