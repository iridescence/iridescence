﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Loads a field onto the stack.
		/// 
		/// Instance fields expect a reference on the stack, which is popped.
		/// </summary>
		public override void LoadField(FieldInfo field, bool? isVolatile = null, int? unaligned = null)
		{
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			if (unaligned.HasValue && (unaligned != 1 && unaligned != 2 && unaligned != 4))
				throw new ArgumentException("unaligned must be null, 1, 2, or 4", nameof(unaligned));

			bool useVolatile = isVolatile ?? field.IsVolatile();

			if (!field.IsStatic)
			{
				if (useVolatile)
					this.emit(OpCodes.Volatile, tag(StackTransition.None));

				if (unaligned.HasValue)
					this.emit(OpCodes.Unaligned, (byte)unaligned.Value, tag(StackTransition.None));

				IStackTransition[] transitions;
				if (field.DeclaringType.IsValueType)
				{
					transitions = new[]
					{
						StackTransition.Pop(field.DeclaringType).Push(field.FieldType),
						StackTransition.Pop(field.DeclaringType.MakePointerType()).Push(field.FieldType),
					};
				}
				else
				{
					transitions = new[]
					{
						StackTransition.Pop(field.DeclaringType).Push(field.FieldType),
					};
				}

				this.emit(OpCodes.Ldfld, field, tag(transitions));
			}
			else
			{
				if (unaligned.HasValue)
					throw new ArgumentException("unaligned cannot be used with static fields.", nameof(unaligned));

				if (useVolatile)
					this.emit(OpCodes.Volatile, tag(StackTransition.None));

				this.emit(OpCodes.Ldsfld, field, tag(StackTransition.Push(field.FieldType)));
			}
		}

		/// <summary>
		/// Loads the address of the given field onto the stack.
		/// 
		/// If the field is an instance field, a `this` reference is expected on the stack and will be popped.
		/// </summary>
		public override void LoadFieldAddress(FieldInfo field)
		{
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			if ((this.Options & ILBuilderOptions.AllowUnverifiable) == 0 && field.IsInitOnly)
				throw new InvalidOperationException("LoadFieldAddress on InitOnly fields is not verifiable.");

			if (!field.IsStatic)
			{
				IStackTransition[] transitions;
				if (field.DeclaringType.IsValueType)
				{
					transitions = new[]
					{
						// This transition isn't really to spec... but it seems to work consistently in .NET soooo.... yeah
						StackTransition.Pop(field.DeclaringType).Push(field.FieldType.MakeByRefType()),
						StackTransition.Pop(field.DeclaringType.MakePointerType()).Push(field.FieldType.MakeByRefType()),
					};
				}
				else
				{
					transitions = new[]
					{
						StackTransition.Pop(field.DeclaringType).Push(field.FieldType.MakeByRefType()),
					};
				}

				this.emit(OpCodes.Ldflda, field, tag(transitions));
			}
			else
			{
				this.emit(OpCodes.Ldsflda, field, tag(StackTransition.Push(field.FieldType.MakeByRefType())));
			}
		}
	}
}
