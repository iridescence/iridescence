﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pops a size from the stack, allocates a rank-1 array of the given type, and pushes a reference to the new array onto the stack.
		/// </summary>
		public override void NewArray(Type elementType)
		{
			if (elementType == null)
				throw new ArgumentNullException(nameof(elementType));

			Type arrayType = elementType.MakeArrayType();

			IStackTransition[] transitions =
			{
				StackTransition.Pop<NativeInt>().Push(arrayType),
				StackTransition.Pop<int>().Push(arrayType),
			};

			this.emit(OpCodes.Newarr, elementType, tag(transitions));
		}
	}
}
