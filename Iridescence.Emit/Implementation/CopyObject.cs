﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Takes a destination pointer, a source pointer as arguments.  Pops both off the stack.
		/// 
		/// Copies the given value type from the source to the destination.
		/// </summary>
		public override void CopyObject(Type valueType)
		{
			if (valueType == null)
				throw new ArgumentNullException(nameof(valueType));

			if (!valueType.IsValueType && !valueType.IsGenericParameter)
				throw new ArgumentException($"CopyObject expects a ValueType, found {valueType}", nameof(valueType));

			Type ptrType = valueType.MakePointerType();
			Type refType = valueType.MakeByRefType();

			IStackTransition[] transitions =
			{
				StackTransition.Pop(ptrType).Pop(ptrType),
				StackTransition.Pop(refType).Pop(ptrType),
				StackTransition.Pop<NativeInt>().Pop(ptrType),

				StackTransition.Pop(ptrType).Pop(refType),
				StackTransition.Pop(refType).Pop(refType),
				StackTransition.Pop<NativeInt>().Pop(refType),

				StackTransition.Pop(ptrType).Pop<NativeInt>(),
				StackTransition.Pop(refType).Pop<NativeInt>(),
				StackTransition.Pop<NativeInt>().Pop<NativeInt>()
			};

			this.emit(OpCodes.Cpobj, valueType, tag(transitions));
		}
	}
}
