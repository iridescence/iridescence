﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Push a constant int32 onto the stack.
		/// </summary>
		public override void LoadConstant(int i)
		{
			TaggedStackTransitions transitions = tag(StackTransition.Push<int>());
			this.emit(OpCodes.Ldc_I4, i, transitions);
		}

		/// <summary>
		/// Push a constant int32 onto the stack.
		/// </summary>
		public override void LoadConstant(uint value)
		{
			this.LoadConstant(unchecked((int)value));
		}

		/// <summary>
		/// Push a constant int64 onto the stack.
		/// </summary>
		public override void LoadConstant(ulong value)
		{
			this.LoadConstant(unchecked((long)value));
		}

		/// <summary>
		/// Push a constant int64 onto the stack.
		/// </summary>
		public override void LoadConstant(long l)
		{
			this.emit(OpCodes.Ldc_I8, l, tag(StackTransition.Push<long>()));
		}

		/// <summary>
		/// Push a constant float onto the stack.
		/// </summary>
		public override void LoadConstant(float f)
		{
			this.emit(OpCodes.Ldc_R4, f, tag(StackTransition.Push<float>()));
		}

		/// <summary>
		/// Push a constant double onto the stack.
		/// </summary>
		public override void LoadConstant(double d)
		{
			this.emit(OpCodes.Ldc_R8, d, tag(StackTransition.Push<double>()));
		}

		/// <summary>
		/// Push a constant string onto the stack.
		/// </summary>
		public override void LoadConstant(string str)
		{
			this.emit(OpCodes.Ldstr, str, tag(StackTransition.Push<string>()));
		}

		/// <summary>
		/// Push a constant RuntimeFieldHandle onto the stack.
		/// </summary>
		public override void LoadConstant(FieldInfo field)
		{
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			this.emit(OpCodes.Ldtoken, field, tag(StackTransition.Push<RuntimeFieldHandle>()));
		}

		/// <summary>
		/// Push a constant RuntimeMethodHandle onto the stack.
		/// </summary>
		public override void LoadConstant(MethodInfo method)
		{
			if (method == null)
				throw new ArgumentNullException(nameof(method));

			this.emit(OpCodes.Ldtoken, method, tag(StackTransition.Push<RuntimeMethodHandle>()));
		}

		/// <summary>
		/// Push a constant RuntimeTypeHandle onto the stack.
		/// </summary>
		public override void LoadConstant(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			this.emit(OpCodes.Ldtoken, type, tag(StackTransition.Push<RuntimeTypeHandle>()));
		}

		/// <summary>
		/// Loads a null reference onto the stack.
		/// </summary>
		public override void LoadNull()
		{
			this.emit(OpCodes.Ldnull, tag(StackTransition.Push<NullType>()));
		}
	}
}
