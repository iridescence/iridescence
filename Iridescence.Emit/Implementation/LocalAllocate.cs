﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pops a size from the stack, allocates size bytes on the local dynamic memory pool, and pushes a pointer to the allocated block.
		/// 
		/// LocalAllocate can only be called if the stack is empty aside from the size value.
		/// 
		/// Memory allocated with LocalAllocate is released when the current method ends execution.
		/// </summary>
		public override void LocalAllocate()
		{
			if ((this.Options & ILBuilderOptions.AllowUnverifiable) == 0)
				throw new UnverifiableException("LocalAllocate is not verifiable.");

			if (this.IsInCatchBlock)
				throw new InvalidOperationException("LocalAllocate cannot be used in a catch block.");

			if (this.IsInFinallyBlock)
				throw new InvalidOperationException("LocalAllocate cannot be used in a finally block.");

			IStackTransition[] transitions =
			{
				StackTransition.SingleItemStack().Pop<int>().Push<NativeInt>(),
				StackTransition.SingleItemStack().Pop<NativeInt>().Push<NativeInt>()
			};

			this.emit(OpCodes.Localloc, tag(transitions));
		}
	}
}
