﻿using System;
using System.Linq;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition[] switchTransition =
		{
			StackTransition.Pop<int>(),
			StackTransition.Pop<NativeInt>(),
		};

		/// <summary>
		/// Pops a value off the stack and branches to the label at the index of that value in the given labels.
		/// 
		/// If the value is out of range, execution falls through to the next instruction.
		/// </summary>
		public override void Switch(params Label[] labels)
		{
			if (labels == null)
				throw new ArgumentNullException(nameof(labels));

			if (labels.Length == 0)
				throw new ArgumentException("labels must have at least one element.", nameof(labels));

			Label[] labelsCopy = labels.ToArray();

			this.emit(OpCodes.Switch, labelsCopy, tag(switchTransition));

			//VerificationResult valid = this.CurrentVerifiers.ConditionalBranch(labels);
			//if (!valid.Success)
			//{
			//	throw new EmitVerificationException(this.IL, valid);
			//}
		}
	}
}
