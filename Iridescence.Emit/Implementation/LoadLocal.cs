﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Loads the value in the given local onto the stack.
		/// 
		/// To create a local, use DeclareLocal().
		/// </summary>
		public override void LoadLocal(Local local)
		{
			if (local == null)
				throw new ArgumentNullException(nameof(local));

			this.emit(OpCodes.Ldloc, local, tag(StackTransition.Push(local.LocalType)));
		}

		/// <summary>
		/// Pushes a pointer to the given local onto the stack.
		/// 
		/// To create a local, use DeclareLocal.
		/// </summary>
		public override void LoadLocalAddress(Local local)
		{
			if (local == null)
				throw new ArgumentNullException(nameof(local));

			Type localType = local.LocalType.MakeByRefType();
			this.emit(OpCodes.Ldloca, local, tag(StackTransition.Push(localType)));
		}
	}
}
