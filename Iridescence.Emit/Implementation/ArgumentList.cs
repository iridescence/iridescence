﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pushes a pointer to the current argument list onto the stack.
		/// 
		/// This instruction can only be used in VarArgs methods.
		/// </summary>
		public override void ArgumentList()
		{
			if (this.CallingConvention != System.Reflection.CallingConventions.VarArgs)
				throw new InvalidOperationException("ArgumentList can only be called in VarArgs methods");

			this.emit(OpCodes.Arglist, tag(StackTransition.Push(typeof(RuntimeArgumentHandle))));
		}
	}
}
