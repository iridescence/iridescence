﻿using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Ends the execution of the current method.
		/// 
		/// If the current method does not return void, pops a value from the stack and returns it to the calling method.
		/// 
		/// Return should leave the stack empty.
		/// </summary>
		public override void Return()
		{
			IStackTransition t;

			if (this.Signature.ReturnType != typeof(void))
			{
				t = StackTransition.Pop(this.Signature.ReturnType).EmptyStack();
			}
			else
			{
				t = StackTransition.EmptyStack();
			}

			this.emit(OpCodes.Ret, tag(t));
			this.mustMark = true;
		}
	}
}
