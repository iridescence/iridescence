﻿using System;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private void validateExceptionBlocks()
		{
			foreach (ExceptionBlock exceptionBlock in this.exceptionBlocks)
			{
				if (exceptionBlock.IsOpen)
					throw new ILVerificationException("Exception block is still open.");

				foreach (CatchBlock catchBlock in exceptionBlock.CatchBlocks)
				{
					if (catchBlock.IsOpen)
						throw new Exception("Invalid state. Closed exception block has open catch block.");
				}

				if (exceptionBlock.FinallyBlock != null && exceptionBlock.FinallyBlock.IsOpen)
				{
					throw new Exception("Invalid state. Closed exception block has open finally block.");
				}
			}
		}

		private ExceptionBlock getExceptionBlock(Operation op)
		{
			foreach (ExceptionBlock exceptionBlock in this.exceptionBlocks)
			{
				if (op.OperationIndex < exceptionBlock.StartOperation.OperationIndex)
					continue;

				if (exceptionBlock.IsOpen || op.OperationIndex <= exceptionBlock.EndOperation.OperationIndex)
					return exceptionBlock;
			}

			return null;
		}

		private CatchBlock getCatchBlock(Operation op)
		{
			foreach (CatchBlock catchBlock in this.exceptionBlocks.SelectMany(eb => eb.CatchBlocks))
			{
				if (op.OperationIndex < catchBlock.StartOperation.OperationIndex)
					continue;

				if (catchBlock.IsOpen || op.OperationIndex <= catchBlock.EndOperation.OperationIndex)
					return catchBlock;
			}

			return null;
		}

		private FinallyBlock getFinallyBlock(Operation op)
		{
			foreach (FinallyBlock finallyBlock in this.exceptionBlocks.Where(eb => eb.FinallyBlock != null).Select(eb => eb.FinallyBlock))
			{
				if (op.OperationIndex < finallyBlock.StartOperation.OperationIndex)
					continue;

				if (finallyBlock.IsOpen || op.OperationIndex <= finallyBlock.EndOperation.OperationIndex)
					return finallyBlock;
			}

			return null;
		}

		private void validateExceptionBranch(Instruction instruction)
		{
			Label branchLabel = (Label)instruction.Operand;
			Operation target = branchLabel.MarkOperation;

			if (target == null)
				return; // Branch label is not marked yet.

			CatchBlock sourceCatch = this.getCatchBlock(instruction);
			if (sourceCatch != null)
			{
				CatchBlock targetCatch = this.getCatchBlock(target);
				if (targetCatch != sourceCatch)
					throw new BranchVerificationException(instruction.OpCode.Name, branchLabel, "Branching from inside a catch block to the outside is not allowed.");
			}

			FinallyBlock sourceFinally = this.getFinallyBlock(instruction);
			if (sourceFinally != null)
			{
				FinallyBlock targetFinally = this.getFinallyBlock(instruction);
				if (targetFinally != sourceFinally)
					throw new BranchVerificationException(instruction.OpCode.Name, branchLabel, "Branching from inside a finally block to the outside is not allowed.");
			}

			ExceptionBlock sourceException = this.getExceptionBlock(instruction);
			ExceptionBlock targetException = this.getExceptionBlock(target);
			if (sourceException == null && targetException != null)
			{
				throw new BranchVerificationException(instruction.OpCode.Name, branchLabel, "Branching from outside an exception block into an exception block is not allowed.");
			}

			if (instruction.OpCode != OpCodes.Leave &&
			    instruction.OpCode != OpCodes.Leave_S &&
			    sourceException != null && targetException == null)
			{
				throw new BranchVerificationException(instruction.OpCode.Name, branchLabel, "Branching from inside an exception block to the outside is not allowed.");
			}
		}

		private void validateExceptionBranches()
		{
			foreach (Instruction instruction in this.Operations
				.Where(op => op is Instruction instr &&
				             (instr.OpCode.FlowControl == FlowControl.Branch ||
				              instr.OpCode.FlowControl == FlowControl.Cond_Branch))
				.Cast<Instruction>())
			{
				this.validateExceptionBranch(instruction);
			}
		}

		private void validateBranches()
		{
			StringBuilder message = new StringBuilder();

			// Find all branch instructions with unmarked labels and group them by label to generate a message.
			foreach ((Label label, Instruction[] instructions) in
				from (Instruction instr, Label label) branch in
				(from instr in
						(from op in this.Operations where op is Instruction select (Instruction)op)
					where (instr.OpCode.FlowControl == FlowControl.Branch || instr.OpCode.FlowControl == FlowControl.Cond_Branch)
					where instr.Operand is Label
					select (instr, (Label)instr.Operand))
				where !branch.label.IsMarked
				group branch.instr by branch.label
				into g
				select (g.Key, g.ToArray()))
			{
				message.Append("Label \"");
				message.Append(label);
				message.Append("\" used by the instruction");
				if (instructions.Length > 1) message.Append('s');
				message.Append(' ');

				bool first = true;

				foreach (Instruction instr in instructions)
				{
					if (first) first = false;
					else message.Append(", ");
					message.Append('"');
					message.Append(instr);
					message.Append('"');
				}

				message.Append(" has not been marked.");
			}

			if (message.Length > 0)
			{
				throw new ILVerificationException(message.ToString());
			}
		}

		/// <summary>
		/// Called to confirm that the IL emit'd to date can be turned into a delegate without error.
		/// 
		/// Checks that the stack is empty, that all paths returns, that all labels are marked, etc. etc.
		/// </summary>
		private void validate(bool building)
		{
			if (building)
			{
				this.validateBranches();
			}

			this.validateExceptionBlocks();
			this.validateExceptionBranches();
		}
	}
}
