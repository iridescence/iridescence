﻿using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Emits a break instruction for use with a debugger.
		/// </summary>
		public override void Break()
		{
			this.emit(OpCodes.Break, tag(StackTransition.None));
		}
	}
}
