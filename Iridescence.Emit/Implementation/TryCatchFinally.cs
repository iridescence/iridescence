﻿using System;
using System.Linq;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Gets a value that indicates whether an exception block is currently open.
		/// </summary>
		public bool IsInExceptionBlock => this.exceptionBlockStack.Count > 0;

		/// <summary>
		/// Gets a value that indicates whether a catch block is currently open.
		/// </summary>
		public bool IsInCatchBlock => this.IsInExceptionBlock && this.exceptionBlockStack.Peek().CatchBlocks.Any(b => b.IsOpen);

		/// <summary>
		/// Gets a value that indicates whether a catch block is currently open in any of the open exception blocks.
		/// </summary>
		public bool IsInAnyCatchBlock => this.IsInExceptionBlock && this.exceptionBlockStack.Any(e => e.CatchBlocks.Any(b => b.IsOpen));

		/// <summary>
		/// Gets a value that indicates whether a finally block is currently open.
		/// </summary>
		public bool IsInFinallyBlock => this.IsInExceptionBlock && (this.exceptionBlockStack.Peek().FinallyBlock?.IsOpen ?? false);

		private FilterBlock getPrecedingFilterBlock()
		{
			if (this.exceptionBlockStack.Count == 0)
				return null;

			ExceptionBlock block = this.exceptionBlockStack.Peek();

			foreach (FilterBlock filter in block.FilterBlocks)
			{
				if (filter.EndOperation != null && filter.EndOperation.NextInstruction == null)
					return filter;
			}

			return null;
		}

		/// <summary>
		/// Start a new exception block.  This is roughly analogous to a `try` block in C#, but an exception block contains it's catch and finally blocks.
		/// </summary>
		public override ExceptionBlock BeginExceptionBlock()
		{
			if (this.mustMark)
				this.MarkLabel(this.DefineLabel());

			ExceptionBlockStartOperation op = this.Operations.BeginExceptionBlock();
			this.addTransition(op, tag(StackTransition.EmptyStack()));

			ExceptionBlock block = new ExceptionBlock(op.Label);
			block.StartOperation = op;

			this.exceptionBlocks.Add(block);
			this.exceptionBlockStack.Push(block);

			return block;
		}

		/// <summary>
		/// Ends the given exception block.
		/// 
		/// All catch and finally blocks associated with the given exception block must be ended before this method is called.
		/// </summary>
		public override void EndExceptionBlock(ExceptionBlock exceptionBlock)
		{
			if (exceptionBlock == null)
				throw new ArgumentNullException(nameof(exceptionBlock));

			if (!exceptionBlock.IsOpen)
				throw new InvalidOperationException("Exception block is not open.");

			if (this.exceptionBlockStack.Peek() != exceptionBlock)
				throw new InvalidOperationException("The exception block is not the top-most block on the exception block stack.");

			if (exceptionBlock.CatchBlocks.Count == 0 && exceptionBlock.FinallyBlock == null)
				throw new InvalidOperationException("The exception block does not have neither have catch blocks nor a finally block.");

			if (exceptionBlock.CatchBlocks.Any(b => b.IsOpen))
				throw new InvalidOperationException("A catch block in the exception block is still open.");

			if (exceptionBlock.FilterBlocks.Any(b => b.IsOpen))
				throw new InvalidOperationException("A filter block in the exception block is still open.");

			if (exceptionBlock.FinallyBlock != null && exceptionBlock.FinallyBlock.IsOpen)
				throw new InvalidOperationException("The finally block of the exception block is still open.");

			exceptionBlock.EndOperation = this.Operations.EndExceptionBlock();
			this.addTransition(exceptionBlock.EndOperation, tag(StackTransition.EmptyStack()));

			this.exceptionBlockStack.Pop();

			if (this.mustMark)
				this.MarkLabel(this.DefineLabel());
		}

		/// <summary>
		/// Begins a catch block for the given exception type in the given exception block.
		/// 
		/// The given exception block must still be open.
		/// </summary>
		public override CatchBlock BeginCatchBlock(ExceptionBlock exceptionBlock, Type exceptionType)
		{
			if (exceptionBlock == null)
				throw new ArgumentNullException(nameof(exceptionBlock));
			
			FilterBlock filter = this.getPrecedingFilterBlock();

			if (filter == null)
			{
				if (exceptionType == null)
					throw new ArgumentNullException(nameof(exceptionType));

				if (!typeof(Exception).IsAssignableFrom(exceptionType))
					throw new ArgumentException("The exception type must be a subclass of System.Exception.", nameof(exceptionType));
			}
			else
			{
				if (exceptionType != null)
					throw new ArgumentException("The catch block is preceded by a filter block and can not have an exception type filter. Please implement the exception type filter in the filter block instead.", nameof(exceptionType));
			}
			
			if (!exceptionBlock.IsOpen)
				throw new InvalidOperationException("Exception block is not open.");

			if (this.exceptionBlockStack.Peek() != exceptionBlock)
				throw new InvalidOperationException("The exception block is not the top-most block on the exception block stack.");

			if (exceptionBlock.CatchBlocks.Any(b => b.IsOpen))
				throw new InvalidOperationException("The exception block has an open catch block.");

			if (exceptionBlock.FilterBlocks.Any(b => b.IsOpen))
				throw new InvalidOperationException("The exception block has an open filter block.");

			if (this.mustMark)
				this.MarkLabel(this.DefineLabel());

			CatchBlockStartOperation op = this.Operations.BeginCatchBlock(exceptionType);
			this.addTransition(op, tag(StackTransition.EmptyStack().Push(exceptionType ?? typeof(Exception))));

			CatchBlock catchBlock = new CatchBlock(exceptionType, exceptionBlock);
			catchBlock.StartOperation = op;

			exceptionBlock.CatchBlocks.Add(catchBlock);

			return catchBlock;
		}

		/// <summary>
		/// Ends the given catch block.
		/// </summary>
		public override void EndCatchBlock(CatchBlock catchBlock)
		{
			if (catchBlock == null)
				throw new ArgumentNullException(nameof(catchBlock));

			if (!catchBlock.IsOpen)
				throw new InvalidOperationException("Catch block is not open.");

			if (this.exceptionBlockStack.Peek() != catchBlock.ExceptionBlock)
				throw new InvalidOperationException("The exception block is not the top-most block on the exception block stack.");

			if (this.mustMark)
				this.MarkLabel(this.DefineLabel());

			catchBlock.EndOperation = this.Operations.EndCatchBlock();
			this.addTransition(catchBlock.EndOperation, tag(StackTransition.None));
		}


		/// <summary>
		/// Begins a finally block on the given exception block.
		/// 
		/// Only one finally block can be defined per exception block, and the block cannot appear within a catch block.
		/// 
		/// The given exception block must still be open.
		/// </summary>
		public override FinallyBlock BeginFinallyBlock(ExceptionBlock exceptionBlock)
		{
			if (exceptionBlock == null)
				throw new ArgumentNullException(nameof(exceptionBlock));

			if (!exceptionBlock.IsOpen)
				throw new InvalidOperationException("Exception block is not open.");

			if (this.exceptionBlockStack.Peek() != exceptionBlock)
				throw new InvalidOperationException("The exception block is not the top-most block on the exception block stack.");

			if (exceptionBlock.CatchBlocks.Any(b => b.IsOpen))
				throw new InvalidOperationException("The exception block has an open catch block.");

			if (exceptionBlock.FilterBlocks.Any(b => b.IsOpen))
				throw new InvalidOperationException("The exception block has an open filter block.");

			if (exceptionBlock.FinallyBlock != null)
				throw new InvalidOperationException("The exception block already has a finally block.");

			if (this.mustMark)
				this.MarkLabel(this.DefineLabel());

			FinallyBlockStartOperation op = this.Operations.BeginFinallyBlock();
			this.addTransition(op, tag(StackTransition.None));

			FinallyBlock finallyBlock = new FinallyBlock(exceptionBlock);
			finallyBlock.StartOperation = op;

			exceptionBlock.FinallyBlock = finallyBlock;

			return finallyBlock;
		}

		/// <summary>
		/// Ends the given finally block.
		/// </summary>
		public override void EndFinallyBlock(FinallyBlock finallyBlock)
		{
			if (finallyBlock == null)
				throw new ArgumentNullException(nameof(finallyBlock));

			if (!finallyBlock.IsOpen)
				throw new InvalidOperationException("Finally block is not open.");

			if (this.exceptionBlockStack.Peek() != finallyBlock.ExceptionBlock)
				throw new InvalidOperationException("The exception block is not the top-most block on the exception block stack.");

			if (this.mustMark)
				this.MarkLabel(this.DefineLabel());

			finallyBlock.EndOperation = this.Operations.EndFinallyBlock();
			this.addTransition(finallyBlock.EndOperation, tag(StackTransition.None));
		}

		public override FilterBlock BeginFilterBlock(ExceptionBlock exceptionBlock)
		{
			if (exceptionBlock == null)
				throw new ArgumentNullException(nameof(exceptionBlock));

			if (!exceptionBlock.IsOpen)
				throw new InvalidOperationException("Exception block is not open.");

			if (this.exceptionBlockStack.Peek() != exceptionBlock)
				throw new InvalidOperationException("The exception block is not the top-most block on the exception block stack.");

			if (exceptionBlock.CatchBlocks.Any(b => b.IsOpen))
				throw new InvalidOperationException("The exception block has an open catch block.");

			if (exceptionBlock.FilterBlocks.Any(b => b.IsOpen))
				throw new InvalidOperationException("The exception block has an open filter block.");

			if (this.mustMark)
				this.MarkLabel(this.DefineLabel());

			FilterBlockStartOperation op = this.Operations.BeginFilterBlock();
			this.addTransition(op, tag(StackTransition.EmptyStack().Push(typeof(Exception))));

			FilterBlock filterBlock = new FilterBlock(exceptionBlock);
			filterBlock.StartOperation = op;

			exceptionBlock.FilterBlocks.Add(filterBlock);

			return filterBlock;
		}

		public override void EndFilterBlock(FilterBlock filterBlock)
		{
			if (filterBlock == null)
				throw new ArgumentNullException(nameof(filterBlock));

			if (!filterBlock.IsOpen)
				throw new InvalidOperationException("Filter block is not open.");

			if (this.exceptionBlockStack.Peek() != filterBlock.ExceptionBlock)
				throw new InvalidOperationException("The exception block is not the top-most block on the exception block stack.");

			if (this.mustMark)
				this.MarkLabel(this.DefineLabel());

			filterBlock.EndOperation = this.Operations.EndFilterBlock();
			this.addTransition(filterBlock.EndOperation, tag(StackTransition.Pop<int>().EmptyStack()));
		}
	}
}
