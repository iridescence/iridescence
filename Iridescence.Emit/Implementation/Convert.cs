﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static class ConversionTransitions<T>
		{
			public static IStackTransition[] Transitions {get;}

			static ConversionTransitions()
			{
				Type toType = typeof(T);

				List<IStackTransition> transitions = new List<IStackTransition>();
				transitions.Add(StackTransition.Pop<int>().Push(toType));
				transitions.Add(StackTransition.Pop<NativeInt>().Push(toType));
				transitions.Add(StackTransition.Pop<long>().Push(toType));
				transitions.Add(StackTransition.Pop<float>().Push(toType));
				transitions.Add(StackTransition.Pop<double>().Push(toType));

				if (toType == typeof(long) || toType == typeof(ulong) || toType == typeof(IntPtr) || toType == typeof(UIntPtr))
				{
					transitions.Add(StackTransition.Pop<AnyPointer>().Push(toType));
					transitions.Add(StackTransition.Pop<AnyByRef>().Push(toType));
					transitions.Add(StackTransition.PopReference().Push(toType));
				}

				Transitions = transitions.ToArray();
			}
		}

		#region Native Int

		public override void ConvertToNativeInt()
		{
			this.emit(OpCodes.Conv_I, tag(ConversionTransitions<IntPtr>.Transitions));
		}

		public override void ConvertToNativeIntOverflow()
		{
			this.emit(OpCodes.Conv_Ovf_I, tag(ConversionTransitions<IntPtr>.Transitions));
		}

		public override void ConvertToNativeIntOverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_I_Un, tag(ConversionTransitions<IntPtr>.Transitions));
		}

		public override void ConvertToNativeUInt()
		{
			this.emit(OpCodes.Conv_U, tag(ConversionTransitions<UIntPtr>.Transitions));
		}

		public override void ConvertToNativeUIntOverflow()
		{
			this.emit(OpCodes.Conv_Ovf_U, tag(ConversionTransitions<UIntPtr>.Transitions));
		}

		public override void ConvertToNativeUIntOverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_U_Un, tag(ConversionTransitions<UIntPtr>.Transitions));
		}
		
		#endregion

		#region Int8

		public override void ConvertToInt8()
		{
			this.emit(OpCodes.Conv_I1, tag(ConversionTransitions<sbyte>.Transitions));
		}

		public override void ConvertToInt8Overflow()
		{
			this.emit(OpCodes.Conv_Ovf_I1, tag(ConversionTransitions<sbyte>.Transitions));
		}

		public override void ConvertToInt8OverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_I1_Un, tag(ConversionTransitions<sbyte>.Transitions));
		}

		public override void ConvertToUInt8()
		{
			this.emit(OpCodes.Conv_U1, tag(ConversionTransitions<byte>.Transitions));
		}

		public override void ConvertToUInt8Overflow()
		{
			this.emit(OpCodes.Conv_Ovf_U1, tag(ConversionTransitions<byte>.Transitions));
		}

		public override void ConvertToUInt8OverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_U1_Un, tag(ConversionTransitions<byte>.Transitions));
		}

		#endregion

		#region Int16

		public override void ConvertToInt16()
		{
			this.emit(OpCodes.Conv_I2, tag(ConversionTransitions<short>.Transitions));
		}

		public override void ConvertToInt16Overflow()
		{
			this.emit(OpCodes.Conv_Ovf_I2, tag(ConversionTransitions<short>.Transitions));
		}

		public override void ConvertToInt16OverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_I2_Un, tag(ConversionTransitions<short>.Transitions));
		}

		public override void ConvertToUInt16()
		{
			this.emit(OpCodes.Conv_U2, tag(ConversionTransitions<ushort>.Transitions));
		}

		public override void ConvertToUInt16Overflow()
		{
			this.emit(OpCodes.Conv_Ovf_U2, tag(ConversionTransitions<ushort>.Transitions));
		}

		public override void ConvertToUInt16OverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_U2_Un, tag(ConversionTransitions<ushort>.Transitions));
		}

		#endregion

		#region Int32

		public override void ConvertToInt32()
		{
			this.emit(OpCodes.Conv_I4, tag(ConversionTransitions<int>.Transitions));
		}

		public override void ConvertToInt32Overflow()
		{
			this.emit(OpCodes.Conv_Ovf_I4, tag(ConversionTransitions<int>.Transitions));
		}

		public override void ConvertToInt32OverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_I4_Un, tag(ConversionTransitions<int>.Transitions));
		}

		public override void ConvertToUInt32()
		{
			this.emit(OpCodes.Conv_U4, tag(ConversionTransitions<uint>.Transitions));
		}

		public override void ConvertToUInt32Overflow()
		{
			this.emit(OpCodes.Conv_Ovf_U4, tag(ConversionTransitions<uint>.Transitions));
		}

		public override void ConvertToUInt32OverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_U4_Un, tag(ConversionTransitions<uint>.Transitions));
		}

		#endregion

		#region Int64

		public override void ConvertToInt64()
		{
			this.emit(OpCodes.Conv_I8, tag(ConversionTransitions<long>.Transitions));
		}

		public override void ConvertToInt64Overflow()
		{
			this.emit(OpCodes.Conv_Ovf_I8, tag(ConversionTransitions<long>.Transitions));
		}

		public override void ConvertToInt64OverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_I8_Un, tag(ConversionTransitions<long>.Transitions));
		}

		public override void ConvertToUInt64()
		{
			this.emit(OpCodes.Conv_U8, tag(ConversionTransitions<ulong>.Transitions));
		}

		public override void ConvertToUInt64Overflow()
		{
			this.emit(OpCodes.Conv_Ovf_U8, tag(ConversionTransitions<ulong>.Transitions));
		}

		public override void ConvertToUInt64OverflowUnsigned()
		{
			this.emit(OpCodes.Conv_Ovf_U8_Un, tag(ConversionTransitions<ulong>.Transitions));
		}

		#endregion

		#region Float

		public override void ConvertToFloat32()
		{
			this.emit(OpCodes.Conv_R4, tag(ConversionTransitions<float>.Transitions));
		}

		public override void ConvertToFloat32Unsigned()
		{
			this.emit(OpCodes.Conv_R_Un, tag(ConversionTransitions<float>.Transitions));
		}

		public override void ConvertToFloat64()
		{
			this.emit(OpCodes.Conv_R8, tag(ConversionTransitions<double>.Transitions));
		}

		#endregion
	}
}
