﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pushes a pointer to the given function onto the stack, as a native int.
		/// 
		/// To resolve a method at runtime using an object, use LoadVirtualFunctionPointer instead.
		/// </summary>
		public override void LoadFunctionPointer(CallableMethod method)
		{
			if (method.Method == null)
				throw new ArgumentNullException(nameof(method));

			InlineMethod inlMethod = new InlineMethod(method.Method, default, method.Signature.ParameterTypes);
			IStackTransition t = StackTransition.Push(new MethodPointerType(inlMethod.Signature));
			this.emit(OpCodes.Ldftn, inlMethod, tag(t));
		}

		/// <summary>
		/// Pops an object reference off the stack, and pushes a pointer to the given method's implementation on that object.
		/// 
		/// For static or non-virtual functions, use LoadFunctionPointer
		/// </summary>
		public override void LoadVirtualFunctionPointer(CallableMethod method)
		{
			if (method.Method == null)
				throw new ArgumentNullException(nameof(method));

			if (method.Method.IsStatic)
				throw new ArgumentException("Only non-static methods can be passed to LoadVirtualFunctionPointer, found " + method);

			Type thisType = method.Method.DeclaringType;
			if (thisType != null && thisType.IsValueType)
			{
				thisType = thisType.MakeByRefType();
			}

			InlineMethod inlMethod = new InlineMethod(method.Method, default, method.Signature.ParameterTypes);
			IStackTransition t = StackTransition.Pop(thisType).Push(new MethodPointerType(inlMethod.Signature));
			this.emit(OpCodes.Ldvirtftn, inlMethod, tag(t));
		}
	}
}
