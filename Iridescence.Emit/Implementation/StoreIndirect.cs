﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static IStackTransition[] makeStoreIndirectTransitions(Type type)
		{
			return new[]
			{
				StackTransition.Pop(type).Pop<NativeInt>(),
				StackTransition.Pop(type).Pop(type.MakePointerType()),
				StackTransition.Pop(type).Pop(type.MakeByRefType()),
			};
		}

		/// <summary>
		/// Pops a value of the given type and a pointer off the stack, and stores the value at the address in the pointer.
		/// </summary>
		public override void StoreIndirect(Type type, bool isVolatile = false, int? unaligned = null)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (unaligned.HasValue && (unaligned != 1 && unaligned != 2 && unaligned != 4))
				throw new ArgumentException("unaligned must be null, 1, 2, or 4");

			if (isVolatile)
				this.emit(OpCodes.Volatile, tag(StackTransition.None));

			if (unaligned.HasValue)
				this.emit(OpCodes.Unaligned, (byte)unaligned.Value, tag(StackTransition.None));

			if (type.IsPointer || type == typeof(IntPtr) || type == typeof(UIntPtr))
			{
				IStackTransition t = StackTransition.Pop<NativeInt>().Pop<NativeInt>();
				this.emit(OpCodes.Stind_I, tag(t));
				return;
			}

			if (!type.IsValueType)
			{
				this.emit(OpCodes.Stind_Ref, tag(makeStoreIndirectTransitions(type)));
				return;
			}

			if (type == typeof(sbyte) || type == typeof(byte))
			{
				this.emit(OpCodes.Stind_I1, tag(makeStoreIndirectTransitions(typeof(byte))));
				return;
			}

			if (type == typeof(short) || type == typeof(ushort))
			{
				this.emit(OpCodes.Stind_I2, tag(makeStoreIndirectTransitions(typeof(short))));
				return;
			}

			if (type == typeof(int) || type == typeof(uint))
			{
				this.emit(OpCodes.Stind_I4, tag(makeStoreIndirectTransitions(typeof(int))));
				return;
			}

			if (type == typeof(long) || type == typeof(ulong))
			{
				this.emit(OpCodes.Stind_I8, tag(makeStoreIndirectTransitions(typeof(long))));
				return;
			}

			if (type == typeof(float))
			{
				this.emit(OpCodes.Stind_R4, tag(makeStoreIndirectTransitions(typeof(float))));
				return;
			}

			if (type == typeof(double))
			{
				this.emit(OpCodes.Stind_R8, tag(makeStoreIndirectTransitions(typeof(double))));
				return;
			}

			{
				this.emit(OpCodes.Stobj, type, tag(makeStoreIndirectTransitions(type)));
			}
		}
	}
}
