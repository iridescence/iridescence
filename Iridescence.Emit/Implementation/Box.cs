﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Boxes the given value type on the stack, converting it into a reference.
		/// </summary>
		public override void Box(Type valueType)
		{
			if (valueType == null)
				throw new ArgumentNullException(nameof(valueType));

			if (valueType == typeof(void))
				throw new ArgumentException("Void can not be boxed.");

			if ((this.Options & ILBuilderOptions.AllowUnverifiable) == 0 && valueType.IsByRef)
				throw new UnverifiableException("Box with by-ref types is not verifiable.");

			IStackTransition t = StackTransition.Pop(valueType);

			if (valueType.IsValueType)
				t = t.Push(new BoxedType(valueType));
			else
				t = t.Push(valueType);

			this.emit(OpCodes.Box, valueType, tag(t));
		}

		/// <summary>
		/// Pops a boxed value from the stack and pushes a pointer to it's unboxed value.
		/// 
		/// To load the value directly onto the stack, use UnboxAny().
		/// </summary>
		public override void Unbox(Type valueType)
		{
			if (valueType == null)
				throw new ArgumentNullException(nameof(valueType));

			if (!valueType.IsValueType || valueType.IsByRef || valueType.IsPointer)
				throw new ArgumentException($"Unbox expects a ValueType, found {valueType}.");
			
			if (valueType == typeof(void))
				throw new ArgumentException("Void can not be unboxed.");

			IStackTransition t = StackTransition.PopObject().Push(valueType.MakeByRefType());
			this.emit(OpCodes.Unbox, valueType, tag(t));
		}

		/// <summary>
		/// Pops a boxed value from the stack, unboxes it and pushes the value onto the stack.
		/// 
		/// To get an address for the unboxed value instead, use Unbox().
		/// </summary>
		public override void UnboxAny(Type valueType)
		{
			if (valueType == null)
				throw new ArgumentNullException(nameof(valueType));

			if (valueType.IsByRef || valueType.IsPointer)
				throw new ArgumentException($"UnboxAny expects a ValueType, found {valueType}.");

			if (valueType == typeof(void))
				throw new ArgumentException("Void can not be unboxed.");

			IStackTransition t = StackTransition.PopReference().Push(valueType);
			this.emit(OpCodes.Unbox_Any, valueType, tag(t));
		}
	}
}
