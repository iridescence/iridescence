﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition[] initializeBlockTransform =
		{
			StackTransition.Pop<int>().Pop<byte>().Pop<NativeInt>(),
			StackTransition.Pop<int>().Pop<byte>().Pop<AnyByRef>(),
			StackTransition.Pop<int>().Pop<byte>().Pop<AnyPointer>(),
		};

		/// <summary>
		/// Expects a pointer, an initialization value, and a count on the stack.  Pops all three.
		/// 
		/// Writes the initialization value to count bytes at the passed pointer.
		/// </summary>
		public override void InitializeBlock(bool isVolatile = false, int? unaligned = null)
		{
			if ((this.Options & ILBuilderOptions.AllowUnverifiable) == 0)
				throw new UnverifiableException("InitializeBlock is not verifiable.");

			if (unaligned.HasValue && (unaligned != 1 && unaligned != 2 && unaligned != 4))
				throw new ArgumentException("unaligned must be null, 1, 2, or 4");

			if (isVolatile)
				this.emit(OpCodes.Volatile, tag(StackTransition.None));

			if (unaligned.HasValue)
				this.emit(OpCodes.Unaligned, (byte)unaligned.Value, tag(StackTransition.None));

			this.emit(OpCodes.Initblk, tag(initializeBlockTransform));
		}
	}
}
