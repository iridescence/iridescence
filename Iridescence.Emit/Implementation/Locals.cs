﻿using System;
using System.Linq;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private void initializeLocal(Local local)
		{
			Type type = local.LocalType;
			if (!type.IsValueType)
			{
				// reference types all get nulled
				this.LoadNull();
				this.StoreLocal(local);
			}
			else
			{
				// handle known primitives better
				//   so as not to confuse the JIT
				bool defaultLoaded = false;

				if (type == typeof(bool))
				{
					this.LoadConstant(default(bool));
					defaultLoaded = true; 
				}
				else if (type == typeof(byte))
				{
					this.LoadConstant(default(byte));
					defaultLoaded = true;
				}
				else if (type == typeof(sbyte))
				{
					this.LoadConstant(default(sbyte));
					defaultLoaded = true;
				}
				else if (type == typeof(short))
				{
					this.LoadConstant(default(short));
					defaultLoaded = true;
				}
				else if (type == typeof(ushort))
				{
					this.LoadConstant(default(ushort));
					defaultLoaded = true;
				}
				else if (type == typeof(int))
				{
					this.LoadConstant(default(int));
					defaultLoaded = true;
				}
				else if (type == typeof(uint))
				{
					this.LoadConstant(default(uint));
					defaultLoaded = true;
				}
				else if (type == typeof(long))
				{
					this.LoadConstant(default(long));
					defaultLoaded = true;
				}
				else if (type == typeof(ulong))
				{
					this.LoadConstant(default(ulong));
					defaultLoaded = true;
				}
				else if (type == typeof(float))
				{
					this.LoadConstant(default(float));
					defaultLoaded = true;
				}
				else if (type == typeof(double))
				{
					this.LoadConstant(default(double));
					defaultLoaded = true;
				}

				if (defaultLoaded)
				{
					this.StoreLocal(local);
				}
				else
				{
					this.LoadLocalAddress(local);
					this.InitializeObject(type);
				}
			}
		}

		/// <summary>
		/// Declare a new local of the given type in the current method.
		/// 
		/// Name is optional, and only provided for debugging purposes.  It has no
		/// effect on emitted IL.
		/// 
		/// Be aware that each local takes some space on the stack, inefficient use of locals
		/// could lead to StackOverflowExceptions at runtime.
		/// 
		/// Jil will reuse local index on the stack if the corresponding Local instance has been disposed.
		/// By default Jil will set reused locals to their default value, you can change this behavior
		/// by passing initializeReused = false.
		/// </summary>
		public override Local DeclareLocal(Type type, string name = null, bool initializeReused = true, bool pinned = false)
		{
			if (type == null)
			{
				throw new ArgumentNullException(nameof(type));
			}

			Local local = this.locals.FirstOrDefault(l => l.IsDisposed && !l.IsReused && l.LocalType == type && l.IsPinned == pinned);

			if (local != null)
			{
				local.IsReused = true;
				local = this.Operations.ReuseLocal(local, name);
				if (initializeReused)
				{
					this.initializeLocal(local);
				}
			}
			else
			{
				local = this.Operations.DeclareLocal(type, name);
			}

			this.locals.Add(local);

			return local;
		}
	}
}
