﻿using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition[] integerOperations =
		{
			StackTransition.Pop<int>().Pop<int>().Push<int>(),
			StackTransition.Pop<int>().Pop<NativeInt>().Push<NativeInt>(),
			StackTransition.Pop<NativeInt>().Pop<int>().Push<NativeInt>(),
			StackTransition.Pop<NativeInt>().Pop<NativeInt>().Push<NativeInt>(),
			StackTransition.Pop<long>().Pop<long>().Push<long>(),
		};

		private static readonly IStackTransition[] notTransitions =
		{
			StackTransition.Pop(typeof(int)).Push(typeof(int)),
			StackTransition.Pop(typeof(NativeInt)).Push(typeof(NativeInt)),
			StackTransition.Pop(typeof(long)).Push(typeof(long)),
		};

		private static readonly IStackTransition[] shiftOperations =
		{
			StackTransition.Pop<int>().Pop<int>().Push<int>(),
			StackTransition.Pop<NativeInt>().Pop<int>().Push<int>(),

			StackTransition.Pop<int>().Pop<long>().Push<long>(),
			StackTransition.Pop<NativeInt>().Pop<long>().Push<long>(),

			StackTransition.Pop<int>().Pop<NativeInt>().Push<NativeInt>(),
			StackTransition.Pop<NativeInt>().Pop<NativeInt>().Push<NativeInt>(),
		};

		/// <summary>
		/// Pops two arguments off the stack, performs a bitwise and, and pushes the result.
		/// </summary>
		public override void And()
		{
			this.emit(OpCodes.And, tag(integerOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, performs a bitwise or, and pushes the result.
		/// </summary>
		public override void Or()
		{
			this.emit(OpCodes.Or, tag(integerOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, performs a bitwise xor, and pushes the result.
		/// </summary>
		public override void Xor()
		{
			this.emit(OpCodes.Xor, tag(integerOperations));
		}

		/// <summary>
		/// Pops one argument off the stack, performs a bitwise inversion, and pushes the result.
		/// </summary>
		public override void Not()
		{
			this.emit(OpCodes.Not, tag(notTransitions));
		}

		/// <summary>
		/// Pops two arguments off the stack, shifts the second value left by the first value.
		/// </summary>
		public override void ShiftLeft()
		{
			this.emit(OpCodes.Shl, tag(shiftOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, shifts the second value right by the first value.
		/// 
		/// Sign extends from the left.
		/// </summary>
		public override void ShiftRight()
		{
			this.emit(OpCodes.Shr, tag(shiftOperations));
		}

		/// <summary>
		/// Pops two arguments off the stack, shifts the second value right by the first value.
		/// 
		/// Acts as if the value were unsigned, zeros always coming in from the left.
		/// </summary>
		public override void ShiftRightUnsigned()
		{
			this.emit(OpCodes.Shr_Un, tag(shiftOperations));
		}
	}
}
