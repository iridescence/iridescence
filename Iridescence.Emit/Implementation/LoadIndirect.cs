﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static IStackTransition[] makeLoadIndirectTransition(Type type)
		{
			return new[]
			{
				StackTransition.Pop(type.MakeByRefType()).Push(type),
				StackTransition.Pop(type.MakePointerType()).Push(type),
				StackTransition.Pop<NativeInt>().Push(type),
			};
		}

		/// <summary>
		/// Pops a pointer from the stack and pushes the value (of the given type) at that address onto the stack.
		/// </summary>
		public override void LoadIndirect(Type type, bool isVolatile = false, int? unaligned = null)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (unaligned.HasValue && (unaligned != 1 && unaligned != 2 && unaligned != 4))
				throw new ArgumentException("unaligned must be null, 1, 2, or 4");

			OpCode instr;
			IStackTransition[] transitions;

			if (type.IsGenericParameter)
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldobj;
			}
			else if (type.IsNativeInt())
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_I;
			}
			else if (!type.IsValueType)
			{
				transitions = new IStackTransition[] { new LoadIndirectRefTransition() };
				instr = OpCodes.Ldind_Ref;
			}
			else if (type == typeof(bool))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_I1;
			}
			else if (type == typeof(sbyte))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_I1;
			}
			else if (type == typeof(byte))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_U1;
			}
			else if (type == typeof(short))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_I2;
			}
			else if (type == typeof(ushort))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_U2;
			}
			else if (type == typeof(char))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_U2;
			}
			else if (type == typeof(int))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_I4;
			}
			else if (type == typeof(uint))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_U4;
			}
			else if (type == typeof(long))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_I8;
			}
			else if (type == typeof(ulong))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_I8;
			}
			else if (type == typeof(float))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_R4;
			}
			else if (type == typeof(double))
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldind_R8;
			}
			else
			{
				transitions = makeLoadIndirectTransition(type);
				instr = OpCodes.Ldobj;
			}

			if (isVolatile)
				this.emit(OpCodes.Volatile, tag(StackTransition.None));

			if (unaligned.HasValue)
				this.emit(OpCodes.Unaligned, (byte)unaligned.Value, tag(StackTransition.None));


			if(instr == OpCodes.Ldobj)
				this.emit(instr, type, tag(transitions));
			else
				this.emit(instr, tag(transitions));
		}
	}
}
