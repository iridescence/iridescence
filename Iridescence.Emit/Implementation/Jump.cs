﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Transfers control to another method.
		/// 
		/// The parameters and calling convention of method must match the current one's.
		/// 
		/// The stack must be empty to jump.
		/// 
		/// Like the branching instructions, Jump cannot leave exception blocks.
		/// </summary>
		public override void Jump(CallableMethod method)
		{
			if ((this.Options & ILBuilderOptions.AllowUnverifiable) == 0)
				throw new UnverifiableException("Jump is not verifiable.");

			if (method.Method == null)
				throw new ArgumentNullException(nameof(method));

			if (method.CallingConvention != this.CallingConvention)
				throw new ArgumentException($"Jump expected a calling convention of {this.CallingConvention}, found {method.CallingConvention}");

			if (method.Signature.ParameterTypes.Length != this.Signature.ParameterTypes.Length)
				throw new ArgumentException($"Jump expected a method with {this.Signature.ParameterTypes.Length} parameters, found {method.Signature.ParameterTypes.Length}");

			if (this.IsInExceptionBlock)
				throw new InvalidOperationException("Jump cannot transfer control from an exception block.");

			for (int i = 0; i < method.Signature.ParameterTypes.Length; i++)
			{
				Type shouldBe = method.Signature.ParameterTypes[i];
				Type actuallyIs = this.Signature.ParameterTypes[i];

				if (!shouldBe.IsAssignableFrom(actuallyIs))
				{
					throw new ILVerificationException($"Jump expected the #{i} parameter to be assignable from {actuallyIs}, but found {shouldBe}");
				}
			}

			this.emit(OpCodes.Jmp, new InlineMethod(method.Method, default, method.Signature.ParameterTypes), tag(StackTransition.EmptyStack()));
		}
	}
}
