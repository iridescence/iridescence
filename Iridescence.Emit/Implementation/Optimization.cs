﻿using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static bool canTailCall(InlineMethod method)
		{
			if (method.Method is MethodInfo mi)
			{
				if (mi.ReturnType == typeof(TypedReference) ||
					mi.ReturnType.IsPointer)
					return false;
			}

			if (method.ParameterTypes.Any(p =>
				p == typeof(TypedReference) ||
				p.IsPointer ||
				p.IsByRef))
				return false;

			return true;
		}

		/// <summary>
		/// Injects the <see cref="OpCodes.Tailcall"/> prefix where it's possible.
		/// </summary>
		private void injectTailCall()
		{
			for (int i = 0; i < this.Operations.Count; i++)
			{
				if (!(this.Operations[i] is Instruction instruction))
					continue;

				if (instruction.OpCode == OpCodes.Ret)
				{
					int callIndex = -1;
					InlineMethod method = default;

					for (int j = i - 1; j >= 0; j--)
					{
						Operation opAtJ = this.Operations[j];

						if (opAtJ is MarkLabelOperation)
							break;

						if (opAtJ is Instruction instructionAtJ)
						{
							if (instructionAtJ.OpCode == OpCodes.Call ||
								instructionAtJ.OpCode == OpCodes.Callvirt)
							{
								callIndex = j;
								method = (InlineMethod)instructionAtJ.Operand;
							}

							break;
						}
					}

					if (callIndex == -1) continue;
					if (!canTailCall(method)) continue;

					Type callReturns = method.ReturnType;
					Type delegateReturns = this.Signature.ReturnType;

					if (!callReturns.StackAssignableTo(delegateReturns))
						continue;

					this.Operations.Add(callIndex, OpCodes.Tailcall);
					i++;
				}
			}
		}

		/// <summary>
		/// Removes unnecessary casts.
		/// </summary>
		private void elideCasts()
		{
			EvaluationStack verifierStack = EvaluationStack.Empty;
			for (int i = 0; i < this.Operations.Count; ++i)
			{
				Operation operation = this.Operations[i];
				if (operation is Instruction instr && instr.OpCode == OpCodes.Castclass)
				{
					Type operand = (Type)instr.Operand;
					Type topOnStack = verifierStack[verifierStack.Count - 1];

					if (operand.IsAssignableFrom(topOnStack))
					{
						this.Operations.RemoveAt(i);
						this.opStacks.Remove(operation);
						--i;
					}
				}

				if (this.opStacks.TryGetValue(operation, out EvaluationStack stack))
				{
					verifierStack = stack;
				}
			}
		}
	}
}
