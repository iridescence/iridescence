﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pops a value off the stack and stores it into the argument to the current method identified by index.
		/// </summary>
		public override void StoreArgument(ushort index)
		{
			if (this.Signature.ParameterTypes.Length == 0)
				throw new ArgumentException("Method has no arguments to store.");
			
			if (index >= this.Signature.ParameterTypes.Length)
				throw new ArgumentOutOfRangeException(nameof(index), "index must be between 0 and " + (this.Signature.ParameterTypes.Length - 1) + ", inclusive.");

			this.emit(OpCodes.Starg, unchecked((short)index), tag(StackTransition.Pop(this.Signature.ParameterTypes[index])));
		}
	}
}
