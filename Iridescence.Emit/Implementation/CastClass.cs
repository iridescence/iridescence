﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Cast a reference on the stack to the given reference type.
		/// 
		/// If the cast is not legal, a CastClassException will be thrown at runtime.
		/// </summary>
		public override void CastClass(Type referenceType)
		{
			if (referenceType == null)
				throw new ArgumentNullException(nameof(referenceType));

			if (referenceType.IsValueType)
				throw new ArgumentException("Can only cast to ReferenceTypes, found " + referenceType);

			IStackTransition t = StackTransition.Pop(typeof(object)).Push(referenceType);

			this.emit(OpCodes.Castclass, referenceType, tag(t));
		}

		/// <summary>
		/// Pops a value from the stack and casts to the given type if possible pushing the result, otherwise pushes a null.
		/// 
		/// This is analogous to C#'s `as` operator.
		/// </summary>
		public override void IsInstance(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			IStackTransition t = StackTransition.PopReference();

			if (type.IsValueType)
				t = t.Push(new BoxedType(type));
			else
				t = t.Push(type);

			this.emit(OpCodes.Isinst, type, tag(t));
		}
	}
}
