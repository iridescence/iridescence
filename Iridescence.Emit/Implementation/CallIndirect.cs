﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pops a pointer to a method, and then all it's arguments (in reverse order, left-most parameter is deepest on the stack) and calls
		/// invokes the method pointer.  If the method returns a non-void result, it is pushed onto the stack.
		/// 
		/// This override allows an arglist to be passed for calling VarArgs methods.
		/// </summary>
		public override void CallIndirect(CallingConventions callConventions, Type returnType, ImmutableArray<Type> parameterTypes, ImmutableArray<Type> optionalParameterTypes = default, bool tailCall = false)
		{
			if (returnType == null)
				throw new ArgumentNullException(nameof(returnType));

			if (parameterTypes.IsDefault)
				throw new ArgumentNullException(nameof(parameterTypes));

			CallingConventions known = CallingConventions.Any | CallingConventions.ExplicitThis | CallingConventions.HasThis | CallingConventions.Standard | CallingConventions.VarArgs;
			known = ~known;

			if ((callConventions & known) != 0)
				throw new ArgumentException("Unexpected value in CallingConventions.", nameof(callConventions));

			if (callConventions.HasFlag(CallingConventions.VarArgs) && !callConventions.HasFlag(CallingConventions.Standard))
			{
				if (optionalParameterTypes.IsDefault)
					throw new ArgumentException("When calling a VarArgs method, optionalParameterTypes must be set", nameof(optionalParameterTypes));
			}

			IStackTransition t = null;
			//if (callConventions.HasFlag(CallingConventions.HasThis))
			//{
			//	// Pop "this".
			//	t = StackTransition.Pop<Any>();
			//}

			// Pop parameters.
			foreach (Type paramType in parameterTypes.Reverse())
			{
				t = t.Pop(paramType);
			}

			// Pop function pointer.
			t = t.Pop<NativeInt>();

			if (returnType != typeof(void))
			{
				// Push result.
				t = t.Push(returnType);
			}

			if (tailCall)
				this.emit(OpCodes.Tailcall, tag(StackTransition.None));

			InlineSig sig = new InlineSig(callConventions, returnType, parameterTypes, optionalParameterTypes);
			this.emitCalli(sig, tag(t));
		}
	}
}
