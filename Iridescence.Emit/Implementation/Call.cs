﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Calls the method being constructed by the given emit.  Emits so used must have been constructed with BuildMethod or related methods.
		/// 
		/// Pops its arguments in reverse order (left-most deepest in the stack), and pushes the return value if it is non-void.
		/// 
		/// If the given method is an instance method, the `this` reference should appear before any parameters.
		/// 
		/// Call does not respect overrides, the implementation defined by the given MethodInfo is what will be called at runtime.
		/// 
		/// To call overrides of instance methods, use CallVirtual.
		/// 
		/// Recursive calls can only be performed with DynamicMethods, other passed in Emits must already have their methods created.
		/// 
		/// When calling VarArgs methods, arglist should be set to the types of the extra parameters to be passed.
		/// </summary>
		public override void Call(CallableMethod method, ImmutableArray<Type> arglist = default, bool tailCall = false)
		{
			MethodBase m = method.Method;

			if (m == null)
				throw new ArgumentNullException(nameof(method));

			IStackTransition t = StackTransition.None;

			if (method.CallingConvention.HasFlag(CallingConventions.VarArgs) && !method.CallingConvention.HasFlag(CallingConventions.Standard))
			{
				if (arglist.IsDefault)
					throw new InvalidOperationException("When calling a VarArgs method, arglist must be set");

				// Pop varargs.
				foreach (Type paramType in arglist.Reverse())
				{
					t = t.Pop(paramType);
				}
			}

			// Pop parameters.
			foreach (Type paramType in method.Signature.ParameterTypes.Reverse())
			{
				t = t.Pop(paramType);
			}

			if (method.CallingConvention.HasFlag(CallingConventions.HasThis))
			{
				// Pop "this"
				Type thisType = method.Method.DeclaringType;
				if (thisType != null)
				{
					if (thisType.IsValueType)
						thisType = thisType.MakePointerType();
			
					t = t.Pop(thisType);
				}
			}

			// Push return value.
			if (method.Signature.ReturnType != typeof(void))
			{
				t = t.Push(method.Signature.ReturnType);
			}

			if (tailCall)
				this.emit(OpCodes.Tailcall, tag(StackTransition.None));

			this.emit(OpCodes.Call, new InlineMethod(m, arglist, method.Signature.ParameterTypes), tag(t));
		}

		private bool isLegalConstructorCall(ConstructorInfo ctor)
		{
			// Calls to struct constructors are always valid.
			if (ctor.DeclaringType.IsValueType)
				return true;

			// Calls to constructors on the same type are ok.
			Type myType = this.DeclaringType;
			if (ctor.DeclaringType == myType)
				return true;

			// Calls to a base constructor are ok too.
			Type myBaseType = myType.BaseType;
			if (ctor.DeclaringType == myBaseType)
				return true;

			// Everything else is not.
			return false;
		}

		/// <summary>
		/// Calls the given constructor.  Pops its arguments in reverse order (left-most deepest in the stack).
		/// 
		/// The `this` reference should appear before any parameters.
		/// </summary>
		public override void Call(ConstructorInfo cons, bool tailCall = false)
		{
			if (cons == null)
				throw new ArgumentNullException(nameof(cons));

			if (cons.CallingConvention.HasFlag(CallingConventions.VarArgs) && !cons.CallingConvention.HasFlag(CallingConventions.Standard))
				throw new NotSupportedException("Calling constructors with VarArgs is currently not supported.");

			if (!this.IsConstructor && !cons.DeclaringType.IsValueType)
				throw new ILVerificationException("Constructors may only be called directly from within a constructor, use NewObject to allocate a new object with a specific constructor.");

			if (!this.isLegalConstructorCall(cons))
				throw new ILVerificationException("Only constructors defined in the current class or it's base class may be called.");

			IStackTransition t = StackTransition.None;

			// Pop parameters.
			foreach (Type paramType in cons.GetParameters().Select(p => p.ParameterType).Reverse())
			{
				t = t.Pop(paramType);
			}
	
			// Pop "this"
			Type thisType = cons.DeclaringType;
			if (thisType != null)
			{
				if (thisType.IsValueType)
					thisType = thisType.MakePointerType();
			
				t = t.Pop(thisType);
			}

			if (tailCall)
				this.emit(OpCodes.Tailcall, tag(StackTransition.None));

			this.emit(OpCodes.Call, new InlineMethod(cons), tag(t));
		}

		/// <summary>
		/// Calls the given method virtually.  Pops its arguments in reverse order (left-most deepest in the stack), and pushes the return value if it is non-void.
		/// 
		/// The `this` reference should appear before any arguments (deepest in the stack).
		/// 
		/// The method invoked at runtime is determined by the type of the `this` reference.
		/// 
		/// If the method invoked shouldn't vary (or if the method is static), use Call instead.
		/// </summary>
		public override void CallVirtual(CallableMethod method, Type constrained = null, ImmutableArray<Type> arglist = default, bool tailCall = false)
		{
			if (method.Method == null)
				throw new ArgumentNullException(nameof(method));

			if (method.Method.IsStatic)
				throw new ArgumentException($"Only non-static methods can be called using CallVirtual, found {method}");

			IStackTransition t = StackTransition.None;

			if (method.CallingConvention.HasFlag(CallingConventions.VarArgs) && !method.CallingConvention.HasFlag(CallingConventions.Standard))
			{
				if (arglist.IsDefault)
					throw new InvalidOperationException("When calling a VarArgs method, arglist must be set");

				// Pop varargs.
				foreach (Type paramType in arglist.Reverse())
				{
					t = t.Pop(paramType);
				}
			}

			// Pop parameters.
			foreach (Type paramType in method.Signature.ParameterTypes.Reverse())
			{
				t = t.Pop(paramType);
			}

			// Pop "this"
			Type thisType = constrained?.MakePointerType();
			if (thisType == null)
			{
				thisType = method.Method.DeclaringType;
				if (thisType.IsValueType)
					thisType = thisType.MakePointerType();
			}

			t = t.Pop(thisType);
			
			// Push return value.
			if (method.Signature.ReturnType != typeof(void))
			{
				t = t.Push(method.Signature.ReturnType);
			}

			if (constrained != null)
			{
				// Emit constrained prefix opcode.
				this.emit(OpCodes.Constrained, constrained, tag(StackTransition.None));
			}

			if (tailCall)
				this.emit(OpCodes.Tailcall, tag(StackTransition.None));

			// Emit call.
			this.emit(OpCodes.Callvirt, new InlineMethod(method.Method, arglist, method.Signature.ParameterTypes), tag(t));
		}
	}
}
