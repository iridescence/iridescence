﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// From within a catch block, rethrows the exception that caused the catch block to be entered.
		/// </summary>
		public override void ReThrow()
		{
			if(!this.IsInAnyCatchBlock)
				throw new InvalidOperationException("ReThrow is only legal in a catch block");
			
			this.emit(OpCodes.Rethrow, tag(new ClearStackTransition()));
			this.mustMark = true;

			//VerificationResult verify = this.CurrentVerifiers.ReThrow();
			//if (!verify.Success)
			//{
			//	throw new EmitVerificationException(this.IL, verify);
			//}
		}
	}
}
