﻿using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Removes the top value on the stack.
		/// </summary>
		public override void Pop()
		{
			this.emit(OpCodes.Pop, tag(StackTransition.PopAny()));
		}
	}
}
