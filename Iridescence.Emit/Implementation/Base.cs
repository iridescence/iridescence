﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Text;

namespace Iridescence.Emit
{
	/// <summary>
	/// Helper for CIL generation that fails as soon as a sequence of instructions
	/// can be shown to be invalid.
	/// </summary>
	public abstract partial class ILBuilderImpl : ILBuilder
	{
		#region Fields

		private readonly List<Local> locals;
		private readonly List<ExceptionBlock> exceptionBlocks;
		private readonly Stack<ExceptionBlock> exceptionBlockStack;
		private bool mustMark;

		private EvaluationStack currentStack;
		private readonly Dictionary<Operation, EvaluationStack> opStacks;
		private readonly Dictionary<Label, EvaluationStack> labelStacks;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the method's signature.
		/// </summary>
		internal MethodSignature Signature { get; }

		/// <summary>
		/// Gets the <see cref="CallingConventions"/> of the method being emitted.
		/// </summary>
		internal CallingConventions CallingConvention { get; }
		
		/// <summary>
		/// Gets the flags the <see cref="ILBuilderImpl"/> was created with.
		/// </summary>
		internal ILBuilderOptions Options { get; }

		/// <summary>
		/// Gets the operations added to the builder.
		/// </summary>
		protected OperationCollection Operations { get; }

		/// <summary>
		/// Gets or sets whether the <see cref="ILBuilder"/> is locked and does not allow any new instructions to be added.
		/// </summary>
		protected bool IsLocked { get; set; }
		
		/// <summary>
		/// Gets a value that determines whether this builder is building a constructor. Used for verifying certain operations.
		/// </summary>
		protected abstract bool IsConstructor { get; }

		/// <summary>
		/// Gets the declaring type of the method built by this builder. Used for verifying certain operations.
		/// </summary>
		protected abstract Type DeclaringType { get; }

		#endregion

		#region Constructors

		protected ILBuilderImpl(MethodSignature signature, CallingConventions callingConvention, ILBuilderOptions options)
		{
			this.Operations = new OperationCollection();
			this.locals = new List<Local>();
			this.exceptionBlocks = new List<ExceptionBlock>();
			this.exceptionBlockStack = new Stack<ExceptionBlock>();

			this.CallingConvention = callingConvention;
			this.Signature = signature;
			this.Options = options;

			if ((options & ILBuilderOptions.Validate) != 0)
			{
				this.currentStack = EvaluationStack.Empty;
				this.opStacks = new Dictionary<Operation, EvaluationStack>();
				this.labelStacks = new Dictionary<Label, EvaluationStack>();
			}
		}

		#endregion

		#region Methods

		public void ToString(StringBuilder str, bool printStacks)
		{
			if (printStacks && (this.Options & ILBuilderOptions.Validate) != 0)
			{
				for (int i = 0; i < this.Operations.Count; ++i)
				{
					this.Operations[i].ToString(str);

					if (this.opStacks.TryGetValue(this.Operations[i], out EvaluationStack stack))
					{
						str.AppendLine(stack.ToString());
						str.AppendLine();
					}
				}
			}
			else
			{
				this.Operations.ToString(str);
			}
		}

		public override void ToString(StringBuilder str)
		{
			this.ToString(str, false);
		}

		protected void ValidateAndOptimize(OptimizationOptions optimizationOptions)
		{
			this.validate(true);

			if ((optimizationOptions & ~OptimizationOptions.All) != 0)
			{
				throw new ArgumentException("optimizationOptions contained unknown flags, found " + optimizationOptions);
			}

			if ((optimizationOptions & OptimizationOptions.TrivialCastEliding) != 0)
			{
				this.elideCasts();
			}

			if ((optimizationOptions & OptimizationOptions.TailCailInjection) != 0)
			{
				this.injectTailCall();
			}

			if ((optimizationOptions & OptimizationOptions.ReadOnlyInjection) != 0)
			{
				//this.InjectReadOnly();
			}

			this.Operations.TransformInstructions((optimizationOptions & OptimizationOptions.ShortInstructions) != 0);
		}

		protected static void ValidateAttributesAndConvention(MethodAttributes attributes, CallingConventions callingConvention)
		{
			if ((attributes & ~(MethodAttributes.Abstract | MethodAttributes.Assembly | MethodAttributes.CheckAccessOnOverride | MethodAttributes.FamANDAssem | MethodAttributes.Family | MethodAttributes.FamORAssem | MethodAttributes.Final | MethodAttributes.HasSecurity | MethodAttributes.HideBySig | MethodAttributes.MemberAccessMask | MethodAttributes.NewSlot | MethodAttributes.PinvokeImpl | MethodAttributes.Private | MethodAttributes.PrivateScope | MethodAttributes.Public | MethodAttributes.RequireSecObject | MethodAttributes.ReuseSlot | MethodAttributes.RTSpecialName | MethodAttributes.SpecialName | MethodAttributes.Static | MethodAttributes.UnmanagedExport | MethodAttributes.Virtual | MethodAttributes.VtableLayoutMask)) != 0)
			{
				throw new ArgumentException("Unrecognized flag in attributes.", nameof(attributes));
			}

			if ((callingConvention & ~(CallingConventions.Any | CallingConventions.ExplicitThis | CallingConventions.HasThis | CallingConventions.Standard | CallingConventions.VarArgs)) != 0)
			{
				throw new ArgumentException("Unrecognized flag in callingConvention.", nameof(callingConvention));
			}

			if ((attributes & MethodAttributes.Static) != 0 && (callingConvention & CallingConventions.HasThis) != 0)
			{
				throw new ArgumentException("Static methods cannot have a this reference.");
			}

			if ((callingConvention & CallingConventions.ExplicitThis) != 0 && (callingConvention & CallingConventions.HasThis) == 0)
			{
				throw new ArgumentException("If ExplicitThis is set, HasThis must also be set.", nameof(callingConvention));
			}
		}

		private EvaluationStack getStackBefore(Operation op)
		{
			op = op.Previous;

			while (op != null)
			{
				if (this.opStacks.TryGetValue(op, out EvaluationStack stack))
					return stack;

				op = op.Previous;
			}

			return EvaluationStack.Empty;
		}

		private EvaluationStack getStackAfter(Operation op)
		{
			while (op != null)
			{
				if (this.opStacks.TryGetValue(op, out EvaluationStack stack))
					return stack;

				op = op.Next;
			}

			throw new InvalidOperationException("The stack after the specified operation is not known.");
		}

		private Operation addTransition(Operation operation, in TaggedStackTransitions transitions)
		{
			if (this.IsLocked)
				throw new InvalidOperationException("The method has already been built and can not be modified anymore.");

			if (this.mustMark && (this.Options & ILBuilderOptions.AllowUnreachable) == 0)
				throw new OperationVerificationException(transitions.Name, "Unreachable code detected.");

			if ((this.Options & ILBuilderOptions.Validate) != 0)
			{
				if (!this.currentStack.TryTransitions(transitions.Transitions, out _, out EvaluationStack newStack))
				{
					// Retrieve operation properties before removing it.
					int opIndex = operation.OperationIndex;
					string faultOpStr = operation.ToString();
					this.Operations.Remove(operation);

					// Build a human-readable exception about what went wrong.
					StringBuilder str = new StringBuilder();
					str.AppendLine("There is no valid stack transition.");
					str.AppendLine("Last operations: ");

					void printOp(string opStr, bool current = false)
					{
						if (string.IsNullOrEmpty(opStr))
							return;

						if (current)
						{
							str.Append("-> ");
						}
						else
						{
							str.Append("   ");
						}

						str.AppendLine(opStr);
					}

					int start = Math.Max(0, opIndex - 10);
					for (int i = start; i < opIndex; ++i)
					{
						printOp(this.Operations[i].ToString());
					}

					printOp(faultOpStr, true);

					str.AppendLine("Current stack:");
					str.AppendLine(this.currentStack.ToString());

					str.AppendLine("Possible transitions:");
					foreach (IStackTransition transition in transitions.Transitions)
					{
						str.AppendLine(transition.ToString());
					}

					throw new OperationVerificationException(transitions.Name, str.ToString());
				}

				this.opStacks.Add(operation, newStack);
				this.currentStack = newStack;
			}

			return operation;
		}

		private Operation emit(OpCode instr, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr), transitions);
		}

		private Operation emit(OpCode instr, byte param, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, param), transitions);
		}

		private Operation emit(OpCode instr, short param, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, param), transitions);
		}

		private Operation emit(OpCode instr, int param, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, param), transitions);
		}

		private Operation emit(OpCode instr, long param, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, param), transitions);
		}

		private Operation emit(OpCode instr, float param, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, param), transitions);
		}

		private Operation emit(OpCode instr, double param, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, param), transitions);
		}

		private Operation emit(OpCode instr, Local param, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, param), transitions);
		}

		private Operation emit(OpCode instr, Label param, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, param), transitions);
		}

		private Operation emit(OpCode instr, Label[] param, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, param), transitions);
		}

		private Operation emit(OpCode instr, InlineMethod method, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, method), transitions);
		}

		private Operation emit(OpCode instr, Type type, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, type), transitions);
		}

		private Operation emit(OpCode instr, FieldInfo field, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, field), transitions);
		}

		private Operation emit(OpCode instr, MethodInfo method, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, method), transitions);
		}

		private Operation emit(OpCode instr, string str, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.Add(instr, str), transitions);
		}

		private Operation emitCalli(InlineSig sig, TaggedStackTransitions transitions)
		{
			return this.addTransition(this.Operations.AddCalli(sig), transitions);
		}

		private static TaggedStackTransitions tag(IStackTransition trans, [CallerMemberName] string name = null)
		{
			return new TaggedStackTransitions(name, trans);
		}

		private static TaggedStackTransitions tag(IReadOnlyList<IStackTransition> trans, [CallerMemberName] string name = null)
		{
			return new TaggedStackTransitions(name, trans);
		}

		#endregion
	}
}
