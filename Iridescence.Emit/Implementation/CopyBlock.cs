﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition[] copyBlockTransitions =
		{
			StackTransition.Pop<int>().Pop<NativeInt>().Pop<NativeInt>(),
			StackTransition.Pop<int>().Pop<AnyByRef>().Pop<NativeInt>(),
			StackTransition.Pop<int>().Pop<NativeInt>().Pop<AnyByRef>(),
			StackTransition.Pop<int>().Pop<AnyByRef>().Pop<AnyByRef>(),
		};

		/// <summary>
		/// Expects a destination pointer, a source pointer, and a length on the stack.  Pops all three values.
		/// 
		/// Copies length bytes from destination to the source.
		/// </summary>
		public override void CopyBlock(bool isVolatile = false, int? unaligned = null)
		{
			if ((this.Options & ILBuilderOptions.AllowUnverifiable) == 0)
				throw new UnverifiableException("CopyBlock is not verifiable.");

			if (unaligned.HasValue && (unaligned != 1 && unaligned != 2 && unaligned != 4))
				throw new ArgumentException("unaligned must be null, 1, 2, or 4", nameof(unaligned));

			if (isVolatile)
				this.emit(OpCodes.Volatile, tag(StackTransition.None));

			if (unaligned.HasValue)
				this.emit(OpCodes.Unaligned, (byte)unaligned.Value, tag(StackTransition.None));

			this.emit(OpCodes.Cpblk, tag(copyBlockTransitions));
		}
	}
}
