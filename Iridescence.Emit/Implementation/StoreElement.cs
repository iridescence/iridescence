﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static IStackTransition[] makeStoreElementTransitions(Type arrayType, Type elementType)
		{
			return new[]
			{
				StackTransition.Pop(elementType).Pop<int>().Pop(arrayType),
				StackTransition.Pop(elementType).Pop<NativeInt>().Pop(arrayType),
			};
		}

		/// <summary>
		/// Pops a value, an index, and a reference to an array off the stack.  Places the given value into the given array at the given index.
		/// </summary>
		public override void StoreElement(Type elementType)
		{
			if (elementType == null)
				throw new ArgumentNullException(nameof(elementType));

			Type arrayType = elementType.MakeArrayType();
			IStackTransition[] transitions;
			OpCode instr;

			if (elementType.IsPointer)
			{
				transitions = makeStoreElementTransitions(arrayType, elementType);
				instr = OpCodes.Stelem_I;
			}
			else if (!elementType.IsValueType)
			{
				transitions = makeStoreElementTransitions(arrayType, elementType);
				instr = OpCodes.Stelem_Ref;
			}
			else if (elementType == typeof(sbyte) || elementType == typeof(byte) || elementType == typeof(bool))
			{
				transitions = makeStoreElementTransitions(arrayType, typeof(int));
				instr = OpCodes.Stelem_I1;
			}
			else if (elementType == typeof(short) || elementType == typeof(ushort) || elementType == typeof(char))
			{
				transitions = makeStoreElementTransitions(arrayType, typeof(int));
				instr = OpCodes.Stelem_I2;
			}
			else if (elementType == typeof(int) || elementType == typeof(uint))
			{
				transitions = makeStoreElementTransitions(arrayType, typeof(int));
				instr = OpCodes.Stelem_I4;
			}
			else if (elementType == typeof(long) || elementType == typeof(ulong))
			{
				transitions = makeStoreElementTransitions(arrayType, typeof(long));
				instr = OpCodes.Stelem_I8;
			}
			else if (elementType == typeof(float))
			{
				transitions = makeStoreElementTransitions(arrayType, typeof(float));
				instr = OpCodes.Stelem_R4;
			}
			else if (elementType == typeof(double))
			{
				transitions = makeStoreElementTransitions(arrayType, typeof(double));
				instr = OpCodes.Stelem_R8;
			}
			else
			{
				transitions = makeStoreElementTransitions(arrayType, elementType);
				this.emit(OpCodes.Stelem, elementType, tag(transitions));
				return;
			}

			this.emit(instr, tag(transitions));
		}
	}
}
