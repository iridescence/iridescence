﻿using System.Linq;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition[] compareTransitions = conditionalTransitions.Select(t => t.Push<int>()).ToArray();

		private static readonly IStackTransition[] compareGtUnTransitions = conditionalGtUnTransitions.Select(t => t.Push<int>()).ToArray();

		private static readonly IStackTransition[] compareEqualTransitions = conditionalEqualTransitions.Select(t => t.Push<int>()).ToArray();
		
		/// <summary>
		/// Pops two values from the stack, and pushes a 1 if they are equal and 0 if they are not.
		/// 
		/// New value on the stack is an Int32.
		/// </summary>
		public override void CompareEqual()
		{
			this.emit(OpCodes.Ceq, tag(compareEqualTransitions));
		}

		/// <summary>
		/// Pops two arguments from the stack, pushes a 1 if the second value is greater than the first value and a 0 otherwise.
		/// 
		/// New value on the stack is an Int32.
		/// </summary>
		public override void CompareGreaterThan()
		{
			this.emit(OpCodes.Cgt, tag(compareTransitions));
		}

		/// <summary>
		/// Pops two arguments from the stack, pushes a 1 if the second value is greater than the first value (as unsigned values) and a 0 otherwise.
		/// 
		/// New value on the stack is an Int32.
		/// </summary>
		public override void CompareGreaterThanUnsigned()
		{
			this.emit(OpCodes.Cgt_Un, tag(compareGtUnTransitions));
		}

		/// <summary>
		/// Pops two arguments from the stack, pushes a 1 if the second value is less than the first value and a 0 otherwise.
		/// 
		/// New value on the stack is an Int32.
		/// </summary>
		public override void CompareLessThan()
		{
			this.emit(OpCodes.Clt, tag(compareTransitions));
		}

		/// <summary>
		/// Pops two arguments from the stack, pushes a 1 if the second value is less than the first value (as unsigned values) and a 0 otherwise.
		/// 
		/// New value on the stack is an Int32.
		/// </summary>
		public override void CompareLessThanUnsigned()
		{
			this.emit(OpCodes.Clt_Un, tag(compareTransitions));
		}
	}
}