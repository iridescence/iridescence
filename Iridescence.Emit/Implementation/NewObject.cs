﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pops # of parameters to the given constructor arguments from the stack, invokes the constructor, and pushes a reference to the new object onto the stack.
		/// </summary>
		public override void NewObject(ConstructorInfo constructor)
		{
			if (constructor == null)
				throw new ArgumentNullException(nameof(constructor));

			ImmutableArray<Type> pts = constructor.GetParameters().Select(p => p.ParameterType).ToImmutableArray();

			this.InnerNewObject(constructor, pts);
		}

		/// <summary>
		/// Pops # of parameters from the stack, invokes the constructor, and pushes a reference to the new object onto the stack.
		/// 
		/// This method is provided as ConstructorBuilder cannot be inspected for parameter information at runtime.  If the passed parameterTypes
		/// do not match the given constructor, the produced code will be invalid.
		/// </summary>
		public override void NewObject(ConstructorBuilder constructor, ImmutableArray<Type> parameterTypes)
		{
			if(constructor == null)
				throw new ArgumentNullException(nameof(constructor));

			if (parameterTypes == null)
				throw new ArgumentNullException(nameof(parameterTypes));

			this.InnerNewObject(constructor, parameterTypes);
		}

		internal void InnerNewObject(ConstructorInfo constructor, ImmutableArray<Type> parameterTypes)
		{
			IStackTransition t = null;

			foreach (Type paramType in parameterTypes.Reverse())
			{
				t = t.Pop(paramType);
			}

			t = t.Push(constructor.DeclaringType);

			this.emit(OpCodes.Newobj, new InlineMethod(constructor, default, parameterTypes), tag(t));
		}
	}
}
