﻿using System;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		private static readonly IStackTransition[] conditionalUnaryTransitions =
		{
			StackTransition.Pop<int>(),
			StackTransition.Pop<long>(),
			StackTransition.PopReference(),
			StackTransition.Pop<AnyByRef>(),
			StackTransition.Pop<AnyPointer>(),
			StackTransition.Pop<NativeInt>(),
		};

		private static readonly IStackTransition[] conditionalTransitions =
		{
			StackTransition.Pop<int>().Pop<int>(),
			StackTransition.Pop<int>().Pop<NativeInt>(),
			StackTransition.Pop<NativeInt>().Pop<int>(),
			StackTransition.Pop<NativeInt>().Pop<NativeInt>(),
			StackTransition.Pop<long>().Pop<long>(),
			StackTransition.Pop<float>().Pop<float>(),
			StackTransition.Pop<double>().Pop<double>(),
			StackTransition.Pop<AnyByRef>().Pop<AnyByRef>(),
		};

		private static readonly IStackTransition[] conditionalGtUnTransitions = conditionalTransitions.Concat(new[]
		{
			StackTransition.PopReference().PopReference(),
		}).ToArray();


		private static readonly IStackTransition[] conditionalEqualTransitions = conditionalTransitions.Concat(new[]
		{
			StackTransition.Pop<AnyByRef>().Pop<NativeInt>(),
			StackTransition.Pop<NativeInt>().Pop<AnyByRef>(),
			StackTransition.PopReference().PopReference()
		}).ToArray();

		private void checkBranchStack(Label label, in EvaluationStack stack, [CallerMemberName] string name = null)
		{
			if (label.IsMarked)
			{
				EvaluationStack stackAtMark = this.getStackBefore(label.MarkOperation);
				if (!stack.IsCompatible(stackAtMark))
				{
					throw BranchVerificationException.CreateStackMismatch(name, label, stackAtMark, stack);
				}
			}
			else
			{
				if (this.labelStacks.TryGetValue(label, out EvaluationStack expectedStack))
				{
					if (!stack.IsCompatible(expectedStack))
					{
						throw BranchVerificationException.CreateStackMismatch(name, label, expectedStack, stack);
					}
				}
				else
				{
					this.labelStacks.Add(label, stack);
				}
			}
		}

		/// <summary>
		/// Unconditionally branches to the given label.
		/// </summary>
		public override void Branch(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.checkBranchStack(label, this.currentStack);
			this.emit(OpCodes.Br, label, tag(StackTransition.None)); // Stack is reset when a label is marked.
			this.mustMark = true;
		}

		/// <summary>
		/// Pops two arguments from the stack, if both are equal branches to the given label.
		/// </summary>
		public override void BranchIfEqual(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Beq, label, tag(conditionalEqualTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops two arguments from the stack, if they are not equal (when treated as unsigned values) branches to the given label.
		/// </summary>
		public override void BranchIfNotEqualUnsigned(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Bne_Un, label, tag(conditionalEqualTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops two arguments from the stack, branches to the given label if the second value is greater than or equal to the first value.
		/// </summary>
		public override void BranchIfGreaterOrEqual(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Bge, label, tag(conditionalTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops two arguments from the stack, branches to the given label if the second value is greater than or equal to the first value (when treated as unsigned values).
		/// </summary>
		public override void BranchIfGreaterOrEqualUnsigned(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Bge_Un, label, tag(conditionalTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops two arguments from the stack, branches to the given label if the second value is greater than the first value.
		/// </summary>
		public override void BranchIfGreater(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Bgt, label, tag(conditionalTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops two arguments from the stack, branches to the given label if the second value is greater than the first value (when treated as unsigned values).
		/// </summary>
		public override void BranchIfGreaterUnsigned(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Bgt_Un, label, tag(conditionalGtUnTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops two arguments from the stack, branches to the given label if the second value is less than or equal to the first value.
		/// </summary>
		public override void BranchIfLessOrEqual(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Ble, label, tag(conditionalTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops two arguments from the stack, branches to the given label if the second value is less than or equal to the first value (when treated as unsigned values).
		/// </summary>
		public override void BranchIfLessOrEqualUnsigned(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Ble_Un, label, tag(conditionalTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops two arguments from the stack, branches to the given label if the second value is less than the first value.
		/// </summary>
		public override void BranchIfLess(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Blt, label, tag(conditionalTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops two arguments from the stack, branches to the given label if the second value is less than the first value (when treated as unsigned values).
		/// </summary>
		public override void BranchIfLessUnsigned(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Blt_Un, label, tag(conditionalTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops one argument from the stack, branches to the given label if the value is false.
		/// 
		/// A value is false if it is zero or null.
		/// </summary>
		public override void BranchIfFalse(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Brfalse, label, tag(conditionalUnaryTransitions));
			this.checkBranchStack(label, this.currentStack);
		}

		/// <summary>
		/// Pops one argument from the stack, branches to the given label if the value is true.
		/// 
		/// A value is true if it is non-zero or non-null.
		/// </summary>
		public override void BranchIfTrue(Label label)
		{
			if (label == null)
				throw new ArgumentNullException(nameof(label));

			this.emit(OpCodes.Brtrue, label, tag(conditionalUnaryTransitions));
			this.checkBranchStack(label, this.currentStack);
		}
	}
}
