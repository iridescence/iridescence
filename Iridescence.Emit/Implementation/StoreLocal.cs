﻿using System;
using System.Reflection.Emit;

namespace Iridescence.Emit
{
	public partial class ILBuilderImpl
	{
		/// <summary>
		/// Pops a value off the stack and stores it into the given local.
		/// 
		/// To create a local, use DeclareLocal().
		/// </summary>
		public override void StoreLocal(Local local)
		{
			if (local == null)
				throw new ArgumentNullException(nameof(local));

			this.emit(OpCodes.Stloc, local, tag(StackTransition.Pop(local.LocalType)));
		}
	}
}
