﻿namespace Iridescence.Emit
{
	/// <summary>
	/// Represents a Label in a CIL stream, and thus a Leave and Branch target.
	/// 
	/// To create a Label call DefineLabel().
	/// 
	/// Before creating a delegate, all Labels must be marked.  To mark a label, call MarkLabel().
	/// </summary>
	public class Label
	{
		#region Fields

		#endregion

		#region Properties

		public string Name { get; }

		public bool IsMarked => this.MarkOperation != null;

		internal Operation DefineOperation { get; set; }

		internal Operation MarkOperation { get; set; }

		#endregion

		#region Constructors

		internal Label(string name)
		{
			this.Name = name;
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			if (this.Name != null)
				return this.Name;

			if (this.MarkOperation != null)
				return $"IL_{this.MarkOperation.ByteOffset}";

			return "?";
		}

		#endregion
	}
}
