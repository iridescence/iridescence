﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Represents a wrapper around <see cref="HashSet{T}"/> that creates the internal hashset once an item is added.
	/// Should never be exposed as a property on a class and only used as private field or local variable.
	/// </summary>
	[Serializable]
	public struct LightweightHashset<T> : ICollection<T>, IEnumerable<T>, IEnumerable, ISet<T>, IReadOnlyCollection<T>
	{
		#region Fields

		private HashSet<T> hashSet;
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the set. Creates it if it has not been created yet.
		/// </summary>
		public HashSet<T> HashSet
		{
			get
			{
				if (this.hashSet == null)
				{
					this.hashSet = new HashSet<T>();
				}

				return this.hashSet;
			}
		}

		public int Count => this.hashSet?.Count ?? 0;

		bool ICollection<T>.IsReadOnly => ((ICollection<T>)this.HashSet).IsReadOnly;

		#endregion
		
		#region Constructors
		
		#endregion
		
		#region Methods
		
		public IEnumerator<T> GetEnumerator()
		{
			if(this.hashSet == null)
				return EmptyEnumerator<T>.Instance;

			return this.hashSet.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)this.HashSet).GetEnumerator();
		}

		public bool Add(T item)
		{
			return this.HashSet.Add(item);
		}

		void ICollection<T>.Add(T item)
		{
			this.HashSet.Add(item);
		}

		public void Clear()
		{
			this.hashSet?.Clear();
		}

		public bool Contains(T item)
		{
			if (this.hashSet == null)
				return false;

			return this.hashSet.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			this.HashSet.CopyTo(array, arrayIndex);
		}

		public bool Remove(T item)
		{
			if(this.hashSet == null)
				return false;

			return this.hashSet.Remove(item);
		}
		
		public void ExceptWith(IEnumerable<T> other)
		{
			this.HashSet.ExceptWith(other);
		}

		public void IntersectWith(IEnumerable<T> other)
		{
			this.HashSet.IntersectWith(other);
		}

		public bool IsProperSubsetOf(IEnumerable<T> other)
		{
			return this.HashSet.IsProperSubsetOf(other);
		}

		public bool IsProperSupersetOf(IEnumerable<T> other)
		{
			return this.HashSet.IsProperSupersetOf(other);
		}

		public bool IsSubsetOf(IEnumerable<T> other)
		{
			return this.HashSet.IsSubsetOf(other);
		}

		public bool IsSupersetOf(IEnumerable<T> other)
		{
			return this.HashSet.IsSupersetOf(other);
		}

		public bool Overlaps(IEnumerable<T> other)
		{
			return this.HashSet.Overlaps(other);
		}

		public bool SetEquals(IEnumerable<T> other)
		{
			return this.HashSet.SetEquals(other);
		}

		public void SymmetricExceptWith(IEnumerable<T> other)
		{
			this.HashSet.SymmetricExceptWith(other);
		}

		public void UnionWith(IEnumerable<T> other)
		{
			this.HashSet.UnionWith(other);
		}
		
		#endregion
	}
}
