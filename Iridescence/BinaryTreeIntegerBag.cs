﻿using System;

namespace Iridescence
{
	/// <summary>
	/// Implements a collection of unique integers using a binary tree.
	/// Can be used for ID generation.
	/// </summary>
	public class BinaryTreeIntegerBag
	{
		#region Fields

		private Node root;
		private long count;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="BinaryTreeIntegerBag"/> class.
		/// The bag is initially full, i.e. it contains every possible integer.
		/// </summary>
		public BinaryTreeIntegerBag()
		{
			this.root = new Node(NodeState.Free);
			this.count = 1;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a new unique integer.
		/// </summary>
		/// <returns>The value.</returns>
		public long Take()
		{
			long value;
			if (!this.root.TryTake(0, this.count, out value))
			{
				// expand tree.
				this.root = new Node(NodeState.Both)
				{
					Left = this.root,
					Right = new Node(NodeState.Free)
				};

				this.count *= 2;

				if (!this.root.TryTake(0, this.count, out value))
					throw new InvalidOperationException();
			}

			return value;
		}

		/// <summary>
		/// Adds a previously taken integer back to the bag.
		/// </summary>
		/// <param name="value">The value to add.</param>
		public void Add(long value)
		{
			this.root.Add(this.count, value);

			while (this.root.HasChildren && this.root.Right.State == NodeState.Free)
			{
				// discard right side of the tree.
				this.root = this.root.Left;
				this.count /= 2;
			}

			// check if the tree is empty.
			if (this.root.State == NodeState.Free)
				this.count = 1; // reset to just 1 item.
		}

		#endregion

		#region Nested Types

		/// <summary>
		/// Defines the node states.
		/// </summary>
		private enum NodeState
		{
			/// <summary>
			/// The node is completely free.
			/// </summary>
			Free,

			/// <summary>
			/// The node is completely used.
			/// </summary>
			Used,

			/// <summary>
			/// The node has free and used children.
			/// </summary>
			Both,
		}

		/// <summary>
		/// Represents a node in a binary tree.
		/// </summary>
		private sealed class Node
		{
			/// <summary>
			/// The left node.
			/// </summary>
			public Node Left;

			/// <summary>
			/// The right node.
			/// </summary>
			public Node Right;

			/// <summary>
			/// The state of the node.
			/// </summary>
			public NodeState State;

			/// <summary>
			/// Indicates whether this node has children.
			/// </summary>
			public bool HasChildren => this.Left != null || this.Right != null;

			/// <summary>
			/// Creates a new node.
			/// </summary>
			/// <param name="state"></param>
			public Node(NodeState state)
			{
				this.State = state;
			}

			/// <summary>
			/// Tries to take an integer.
			/// </summary>
			/// <param name="offset">The offset of this node.</param>
			/// <param name="count">The count of this node.</param>
			/// <param name="value">The result.</param>
			/// <returns>True, if an ID was generated. False otherwise.</returns>
			public bool TryTake(long offset, long count, out long value)
			{
				if (this.State == NodeState.Used)
				{
					// node isn't free, return.
					value = -1;
					return false;
				}

				if (!this.HasChildren)
				{
					if (count == 1)
					{
						// this is a leaf.
						value = offset;
						this.State = NodeState.Used;
						return true;
					}

					// split node.
					this.Left = new Node(NodeState.Free);
					this.Right = new Node(NodeState.Free);
				}

				// try child nodes.
				long middle = count / 2;
				if (!this.Left.TryTake(offset, middle, out value) &&
					!this.Right.TryTake(offset + middle, middle, out value))
				{
					// this should never happen.
					throw new InvalidOperationException("Node has no free slots.");
				}

				if (this.Left.State == NodeState.Used && this.Right.State == NodeState.Used)
				{
					// everything is used, remove children.
					this.State = NodeState.Used;
					this.Left = null;
					this.Right = null;
				}
				else
				{
					this.State = NodeState.Both;
				}

				return true;
			}

			/// <summary>
			/// Add an integer.
			/// </summary>
			/// <param name="count">The count of this node.</param>
			/// <param name="value">The value of the ID to release.</param>
			public void Add(long count, long value)
			{
				if (this.State == NodeState.Free)
					throw new InvalidOperationException("Value is not taken.");

				if (count == 1)
				{
					// this is a leaf.
					this.State = NodeState.Free;
					return;
				}

				// split node.
				if (!this.HasChildren)
				{
					this.Left = new Node(NodeState.Used);
					this.Right = new Node(NodeState.Used);
				}

				// release ID on the correct side.
				long middle = count / 2;
				if (value < middle)
					this.Left.Add(middle, value);
				else
					this.Right.Add(middle, value - middle);

				if (this.Left.State == NodeState.Free && this.Right.State == NodeState.Free)
				{
					// everything is free, remove children.
					this.State = NodeState.Free;
					this.Left = null;
					this.Right = null;
				}
				else
				{
					this.State = NodeState.Both;
				}
			}

			public override string ToString()
			{
				return $"{this.State}";
			}
		}

		#endregion
	}
}
