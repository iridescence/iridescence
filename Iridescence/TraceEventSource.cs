﻿using System.Diagnostics.Tracing;

namespace Iridescence
{
	[EventSource(Name = EventSourceName)]
	public sealed class TraceEventSource : EventSource
	{
		#region Fields

		public const string EventSourceName = "Iridescence-Core-Trace";

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public static readonly TraceEventSource Instance = new TraceEventSource();

		#endregion

		#region Methods

		[Event(1, Level = EventLevel.Critical)]
		public void WriteCritical(string message, string source)
		{
			if(!this.IsEnabled()) return;
			this.WriteEvent(1, message, source);
		}

		[Event(2, Level = EventLevel.Error)]
		public void WriteError(string message, string source)
		{
			if(!this.IsEnabled()) return;
			this.WriteEvent(2, message, source);
		}

		[Event(3, Level = EventLevel.Warning)]
		public void WriteWarning(string message, string source)
		{
			if(!this.IsEnabled()) return;
			this.WriteEvent(3, message, source);
		}
		
		[Event(4, Level = EventLevel.Informational)]
		public void WriteInformation(string message, string source)
		{
			if(!this.IsEnabled()) return;
			this.WriteEvent(4, message, source);
		}

		[Event(5, Level = EventLevel.Verbose)]
		public void WriteVerbose(string message, string source)
		{
			if(!this.IsEnabled()) return;
			this.WriteEvent(5, message, source);
		}

		#endregion
	}
}