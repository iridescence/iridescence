﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace Iridescence
{
	/// <summary>
	/// Low-level implementation of the SHA-3 algorithm.
	/// </summary>
	/// <remarks>
	/// Port of https://github.com/mjosaarinen/tiny_sha3/blob/master/sha3.c
	/// </remarks>
	public unsafe struct SHA3
	{
		[StructLayout(LayoutKind.Explicit)]
		private struct Storage
		{
			[FieldOffset(0)]
			public fixed byte b[200];
			[FieldOffset(0)]
			public fixed ulong q[25];
		}

		private const int rounds = 24;

		private static readonly ulong[] roundConstants =
		{
			0x0000000000000001, 0x0000000000008082, 0x800000000000808a,
			0x8000000080008000, 0x000000000000808b, 0x0000000080000001,
			0x8000000080008081, 0x8000000000008009, 0x000000000000008a,
			0x0000000000000088, 0x0000000080008009, 0x000000008000000a,
			0x000000008000808b, 0x800000000000008b, 0x8000000000008089,
			0x8000000000008003, 0x8000000000008002, 0x8000000000000080, 
			0x000000000000800a, 0x800000008000000a, 0x8000000080008081,
			0x8000000000008080, 0x0000000080000001, 0x8000000080008008
		};

		private static readonly int[] rotc = { 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 2, 14, 27, 41, 56, 8, 25, 43, 62, 18, 39, 61, 20, 44 };
		private static readonly int[] keccakf_piln = { 10, 7, 11, 17, 18, 3, 5, 16, 8, 21, 24, 4, 15, 23, 19, 13, 12, 2, 20, 14, 22, 9, 6, 1 };

		private Storage storage;
		private int pt;
		private int rsiz;
		private int mdlen;

		public void Init(int mdlen)
		{
			if (mdlen <= 0 || mdlen > 64)
				throw new ArgumentException("length must be greater than 0 and less than or equal to 64.", nameof(mdlen));

			for (int i = 0; i < 25; i++)
				this.storage.q[i] = 0;

			this.mdlen = mdlen;
			this.rsiz = 200 - 2 * mdlen;
			this.pt = 0;
		}

		public void Update(ReadOnlySpan<byte> data)
		{
			int j = this.pt;
			Storage s = this.storage;

			for (int i = 0; i < data.Length; i++)
			{
				s.b[j++] ^= data[i];
				if (j >= this.rsiz)
				{
					s = keccakf(s);
					j = 0;
				}
			}

			this.storage = s;
			this.pt = j;
		}

		public void Final(Span<byte> md)
		{
			this.storage.b[this.pt] ^= 0x06;
			this.storage.b[this.rsiz - 1] ^= 0x80;
			this.storage = keccakf(this.storage);

			for (int i = 0; i < this.mdlen; i++)
			{
				md[i] = this.storage.b[i];
			}
		}

		public void InitShake128() => this.Init(16);

		public void InitShake256() => this.Init(32);

		public void ShakeXof()
		{
			this.storage.b[this.pt] ^= 0x1F;
			this.storage.b[this.rsiz - 1] ^= 0x80;
			this.storage = keccakf(this.storage);
			this.pt = 0;
		}

		public void ShakeOut(Span<byte> buf)
		{
			int j = this.pt;
			Storage s = this.storage;

			for (int i = 0; i < buf.Length; i++)
			{
				if (j >= this.rsiz)
				{
					s = keccakf(s);
					j = 0;
				}

				buf[i] = s.b[j++];
			}

			this.storage = s;
			this.pt = j;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static ulong rotL64(ulong x, int n)
		{
			return (x << n) | (x >> (64 - n));
		}

		private static Storage keccakf(Storage s)
		{
			Span<ulong> bc = stackalloc ulong[5];
			for (int round = 0; round < rounds; round++)
			{
				ulong t;

				// Theta
				for (int i = 0; i < 5; i++)
					bc[i] = s.q[i] ^ s.q[i + 5] ^ s.q[i + 10] ^ s.q[i + 15] ^ s.q[i + 20];

				for (int i = 0; i < 5; i++)
				{
					t = bc[(i + 4) % 5] ^ rotL64(bc[(i + 1) % 5], 1);
					for (int j = 0; j < 25; j += 5)
						s.q[j + i] ^= t;
				}

				// Rho Pi
				t = s.q[1];
				for (int i = 0; i < 24; i++)
				{
					int j = keccakf_piln[i];
					bc[0] = s.q[j];
					s.q[j] = rotL64(t, rotc[i]);
					t = bc[0];
				}

				// Chi
				for (int j = 0; j < 25; j += 5)
				{
					for (int i = 0; i < 5; i++)
						bc[i] = s.q[j + i];
					for (int i = 0; i < 5; i++)
						s.q[j + i] ^= (~bc[(i + 1) % 5]) & bc[(i + 2) % 5];
				}

				// Iota
				s.q[0] ^= roundConstants[round];
			}

			return s;
		}

		/// <summary>
		/// Computes the SHA-3 hash of the specified input span.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="hash"></param>
		public static void ComputeHash(ReadOnlySpan<byte> input, Span<byte> hash)
		{
			SHA3 sha3 = default;
			sha3.Init(hash.Length);
			sha3.Update(input);
			sha3.Final(hash);
		}

		/// <summary>
		/// Computes the SHA-3 hash of the specified input stream.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="hash"></param>
		public static void ComputeHash(Stream input, Span<byte> hash)
		{
			SHA3 sha3 = default;
			sha3.Init(hash.Length);

			byte[] temp = new byte[65536];
			for (;;)
			{
				int n = input.Read(temp, 0, temp.Length);
				if (n == 0)
					break;

				sha3.Update(new ReadOnlySpan<byte>(temp, 0, n));
			}

			sha3.Final(hash);
		}
	}

	/// <summary>
	/// Implements a <see cref="HashAlgorithm"/> that computes SHA-3 hashes.
	/// </summary>
	public sealed class SHA3HashAlgorithm : HashAlgorithm
	{
		#region Fields

		private SHA3 sha3;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Keccak algorithm with the specified length.
		/// </summary>
		/// <param name="length">The number of output bytes.</param>
		public SHA3HashAlgorithm(int length)
		{
			this.sha3.Init(length);

			this.HashSizeValue = length;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the hash algorithm.
		/// </summary>
		public override void Initialize()
		{
			this.sha3.Init(this.HashSize);
		}

		/// <summary>
		/// Computes the hash for the specified data.
		/// </summary>
		/// <param name="array"></param>
		/// <param name="ibStart"></param>
		/// <param name="cbSize"></param>
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.sha3.Update(new ReadOnlySpan<byte>(array, ibStart, cbSize));
		}

		/// <summary>
		/// Finalizes and returns the hash.
		/// </summary>
		/// <returns></returns>
		protected override byte[] HashFinal()
		{
			byte[] hash = new byte[this.HashSize];
			this.sha3.Final(hash);
			return hash;
		}
	
		#endregion
	}
}
