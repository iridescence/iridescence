﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence
{
	/// <summary>
	/// Implements a <see cref="IReadOnlyList{T}"/> that exposes the contents of multiple <see cref="IReadOnlyList{T}">lists</see> as one.
	/// </summary>
	[Serializable]
	public class ConcatReadOnlyList<T> : IReadOnlyList<T>
	{
		#region Fields

		private readonly IReadOnlyList<T>[] lists;
		
		#endregion
		
		#region Properties

		public int Count
		{
			get
			{
				int count = 0;
				for (int i = 0; i < this.lists.Length; ++i)
				{
					count += this.lists[i].Count;
				}
				return count;
			}
		}
		
		public T this[int index]
		{
			get
			{
				if(index < 0)
					throw new IndexOutOfRangeException();

				int currentList = 0;
				for (;;)
				{
					if(currentList >= this.lists.Length)
						throw new IndexOutOfRangeException();

					int count = this.lists[currentList].Count;
					if (index < count)
						return this.lists[currentList][index];

					index -= count;
					++currentList;
				}
			}
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ConcatReadOnlyList{T}"/> class. 
		/// </summary>
		public ConcatReadOnlyList(params IReadOnlyList<T>[] lists)
			: this((IEnumerable<IReadOnlyList<T>>)lists)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ConcatReadOnlyList{T}"/> class. 
		/// </summary>
		public ConcatReadOnlyList(IEnumerable<IReadOnlyList<T>> lists)
		{
			this.lists = lists.ToArray();
		}
		
		#endregion
		
		#region Methods
		
		public IEnumerator<T> GetEnumerator()
		{
			foreach (IReadOnlyList<T> list in this.lists)
			{
				foreach (T item in list)
				{
					yield return item;
				}
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}
}
