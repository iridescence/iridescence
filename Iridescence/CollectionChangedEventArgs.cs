﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence
{
	/// <summary>
	/// Event args for collection modification events.
	/// </summary>
	public class CollectionChangedEventArgs<T> : EventArgs
	{
		#region Fields
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the <see cref="CollectionModificationFlags"/>.
		/// </summary>
		public CollectionModificationFlags Flags { get; }

		/// <summary>
		/// Gets an enumerable of the items that have been removed.
		/// </summary>
		public IEnumerable<T> RemovedItems { get; }

		/// <summary>
		/// Gets an enumerable of the items that have been added.
		/// </summary>
		public IEnumerable<T> AddedItems { get; }
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="INotifyCollectionChanged{T}"/> class. 
		/// </summary>
		internal CollectionChangedEventArgs(CollectionModificationFlags flags, IEnumerable<T> removedItems, IEnumerable<T> addedItems)
		{
			this.Flags = flags;
			this.RemovedItems = removedItems;
			this.AddedItems = addedItems;
		}
		
		#endregion
	}

	/// <summary>
	/// Exposes methods to create <see cref="CollectionChangedEventArgs{T}"/>.
	/// </summary>
	public static class CollectionChange
	{
		public static CollectionChangedEventArgs<T> Add<T>(T item)
		{
			return new CollectionChangedEventArgs<T>(CollectionModificationFlags.Add, Enumerable.Empty<T>(), EnumerableExtensions.Once(item));
		}

		public static CollectionChangedEventArgs<T> Add<T>(IEnumerable<T> items)
		{
			return new CollectionChangedEventArgs<T>(CollectionModificationFlags.Add, Enumerable.Empty<T>(), items);
		}
		
		public static CollectionChangedEventArgs<T> Remove<T>(T item)
		{
			return new CollectionChangedEventArgs<T>(CollectionModificationFlags.Remove, EnumerableExtensions.Once(item), Enumerable.Empty<T>());
		}

		public static CollectionChangedEventArgs<T> Remove<T>(IEnumerable<T> items)
		{
			return new CollectionChangedEventArgs<T>(CollectionModificationFlags.Remove, items, Enumerable.Empty<T>());
		}

		public static CollectionChangedEventArgs<T> Clear<T>(IEnumerable<T> items)
		{
			return new CollectionChangedEventArgs<T>(CollectionModificationFlags.Clear | CollectionModificationFlags.Remove, items, Enumerable.Empty<T>());
		}

		public static CollectionChangedEventArgs<T> Replace<T>(T oldItem, T newItem)
		{
			return new CollectionChangedEventArgs<T>(CollectionModificationFlags.Replace | CollectionModificationFlags.Remove | CollectionModificationFlags.Add, EnumerableExtensions.Once(oldItem), EnumerableExtensions.Once(newItem));
		}
	}

	/// <summary>
	/// Exposes methods to create <see cref="CollectionChangedEventArgs{T}"/> for lists.
	/// </summary>
	public static class ListChange
	{
		public static CollectionChangedEventArgs<ListItem<T>> Add<T>(int index, T item)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Add, Enumerable.Empty<ListItem<T>>(), EnumerableExtensions.Once(new ListItem<T>(index, item)));
		}

		public static CollectionChangedEventArgs<ListItem<T>> Add<T>(ListItem<T> item)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Add, Enumerable.Empty<ListItem<T>>(), EnumerableExtensions.Once(item));
		}

		public static CollectionChangedEventArgs<ListItem<T>> Add<T>(IEnumerable<ListItem<T>> items)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Add, Enumerable.Empty<ListItem<T>>(), items);
		}
		
		public static CollectionChangedEventArgs<ListItem<T>> Remove<T>(int index, T item)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Remove, EnumerableExtensions.Once(new ListItem<T>(index, item)), Enumerable.Empty<ListItem<T>>());
		}

		public static CollectionChangedEventArgs<ListItem<T>> Remove<T>(ListItem<T> item)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Remove, EnumerableExtensions.Once(item), Enumerable.Empty<ListItem<T>>());
		}

		public static CollectionChangedEventArgs<ListItem<T>> Remove<T>(IEnumerable<ListItem<T>> items)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Remove, items, Enumerable.Empty<ListItem<T>>());
		}

		public static CollectionChangedEventArgs<ListItem<T>> Clear<T>(IEnumerable<ListItem<T>> items)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Clear | CollectionModificationFlags.Remove, items, Enumerable.Empty<ListItem<T>>());
		}

		public static CollectionChangedEventArgs<ListItem<T>> Clear<T>(IEnumerable<T> items)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Clear | CollectionModificationFlags.Remove, items.Select((v, i) => new ListItem<T>(i, v)), Enumerable.Empty<ListItem<T>>());
		}

		public static CollectionChangedEventArgs<ListItem<T>> Replace<T>(ListItem<T> oldItem, ListItem<T> newItem)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Replace | CollectionModificationFlags.Remove | CollectionModificationFlags.Add, EnumerableExtensions.Once(oldItem), EnumerableExtensions.Once(newItem));
		}

		public static CollectionChangedEventArgs<ListItem<T>> Replace<T>(int index, T oldItem, T newItem)
		{
			return new CollectionChangedEventArgs<ListItem<T>>(CollectionModificationFlags.Replace | CollectionModificationFlags.Remove | CollectionModificationFlags.Add, EnumerableExtensions.Once(new ListItem<T>(index, oldItem)), EnumerableExtensions.Once(new ListItem<T>(index, newItem)));
		}
	}
}