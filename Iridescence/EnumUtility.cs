﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence
{
	/// <summary>
	/// Enum utilities.
	/// </summary>
	/// <typeparam name="TEnum">The enum type.</typeparam>
	/// <typeparam name="TBase">The underlying type of the enum.</typeparam>
	public static class EnumUtility<TEnum, TBase> where TEnum : Enum
	{
		#region Properties

		/// <summary>
		/// The minimum of all values defined in the enum.
		/// </summary>
		public static TBase MinValue { get; }

		/// <summary>
		/// The maximum of all values defined in the enum.
		/// </summary>
		public static TBase MaxValue { get; }

		/// <summary>
		/// Gets all values defined in the enum.
		/// </summary>
		public static IReadOnlyDictionary<string, TBase> Fields { get; }

		#endregion

		#region Constructors

		static EnumUtility()
		{
			Type enumType = typeof(TEnum);
			Type baseType = typeof(TBase);

			if (!enumType.IsEnum)
				throw new ArgumentException($"\"{enumType}\" is not an enum.", nameof(TEnum));

			if (enumType.GetEnumUnderlyingType() != baseType)
				throw new ArgumentException($"\"{baseType}\" is not the underlying type of \"{enumType}\".", nameof(TBase));

			string[] names = Enum.GetNames(enumType);
			TBase[] values = (TBase[])Enum.GetValues(enumType);

			MinValue = values.Min();
			MaxValue = values.Max();

			Dictionary<string, TBase> dict = new Dictionary<string, TBase>();
			for (int i = 0; i < values.Length; ++i)
			{
				dict.Add(names[i], values[i]);
			}

			Fields = dict;
		}

		public static bool IsDefined(TEnum value)
		{
			return IsDefined((TBase)(object)value);
		}

		public static bool IsDefined(TBase value)
		{
			return Fields.Values.Contains(value);
		}

		#endregion
	}
}
