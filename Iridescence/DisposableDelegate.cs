﻿using System;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Implements an <see cref="IDisposable"/> that invokes a delegate when its dispose method is called.
	/// </summary>
	public struct DisposableDelegate : IDisposable
	{
		#region Fields

		private Action dispose;
		private readonly bool once;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DisposableDelegate"/> class. 
		/// </summary>
		/// <param name="dispose">The delegate to invoke.</param>
		/// <param name="once">
		/// True, if the delegate is only invoked when on the first call to <see cref="Dispose"/> and further calls are ignored.
		/// False, if the delegate should be invoked for each call to <see cref="Dispose"/>.
		/// </param>
		public DisposableDelegate(Action dispose, bool once = true)
		{
			this.dispose = dispose;
			this.once = once;
		}
		
		#endregion

		#region Methods

		public void Dispose()
		{
			if (this.once)
			{
				Action action = Interlocked.Exchange(ref this.dispose, null);
				action?.Invoke();
			}
			else
			{
				this.dispose?.Invoke();
			}
		}

		#endregion
	}
}
