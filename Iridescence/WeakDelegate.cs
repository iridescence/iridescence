﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using Iridescence.Emit;
using Label = Iridescence.Emit.Label;

namespace Iridescence
{
	/// <summary>
	/// Provides methods to generate weak-referenced delegates.
	/// </summary>
	public static class WeakDelegate
	{
		#region Methods

		private static readonly ConditionalWeakTable<object, List<Delegate>> bindings = new ConditionalWeakTable<object, List<Delegate>>();
		private static readonly ConcurrentDictionary<Type, DynamicMethod> wrappers = new ConcurrentDictionary<Type, DynamicMethod>();

		/// <summary>
		/// Creates a weak wrapper around the specified delegate.
		/// </summary>
		/// <param name="del">The delegate to wrap.</param>
		/// <param name="dispose">The method to call when the delegate can no longer be invoked because it was collected.</param>
		/// <param name="binder">An object that the original delegate is "bound" to. As long as this object is alive, the delegate will be as well. If null, the delegate's target is used.</param>
		/// <returns>The wrapper.</returns>
		public static TDelegate Create<TDelegate>(TDelegate del, WeakDelegateDispose<TDelegate> dispose = null, object binder = null)
			where TDelegate : Delegate
		{
			if (!(del is Delegate del2))
				throw new ArgumentException("Parameter must be a Delegate.", nameof(del));

			WeakDelegateDispose<Delegate> disposeAdapter = null;

			if (dispose != null)
			{
				disposeAdapter = (disposing, wrapperDelegate) => { dispose(disposing, (TDelegate)wrapperDelegate); };
			}

			return (TDelegate)Create(del2, disposeAdapter, binder);
		}

		/// <summary>
		/// Creates a weak wrapper around the specified <see cref="EventHandler"/> delegate.
		/// </summary>
		/// <param name="del">The delegate to wrap.</param>
		/// <param name="dispose">The method to call when the delegate can no longer be invoked because it was collected.</param>
		/// <param name="binder">An object that the original delegate is "bound" to. As long as this object is alive, the delegate will be as well. If null, the delegate's target is used.</param>
		/// <returns>The wrapper.</returns>
		public static EventHandler Create(EventHandler del, WeakDelegateDispose<EventHandler> dispose = null, object binder = null)
		{
			return Create<EventHandler>(del, dispose, binder);
		}
		
		/// <summary>
		/// Creates a weak wrapper around the specified <see cref="EventHandler{TArgs}"/> delegate.
		/// </summary>
		/// <param name="del">The delegate to wrap.</param>
		/// <param name="dispose">The method to call when the delegate can no longer be invoked because it was collected.</param>
		/// <param name="binder">An object that the original delegate is "bound" to. As long as this object is alive, the delegate will be as well. If null, the delegate's target is used.</param>
		/// <returns>The wrapper.</returns>
		public static EventHandler<TArgs> Create<TArgs>(EventHandler<TArgs> del, WeakDelegateDispose<EventHandler<TArgs>> dispose = null, object binder = null)
		{
			return Create<EventHandler<TArgs>>(del, dispose, binder);
		}

		/// <summary>
		/// Creates a weak wrapper around the specified <see cref="ValueChangedEventHandler{T}"/> delegate.
		/// </summary>
		/// <param name="del">The delegate to wrap.</param>
		/// <param name="dispose">The method to call when the delegate can no longer be invoked because it was collected.</param>
		/// <param name="binder">An object that the original delegate is "bound" to. As long as this object is alive, the delegate will be as well. If null, the delegate's target is used.</param>
		/// <returns>The wrapper.</returns>
		public static ValueChangedEventHandler<TValue> Create<TValue>(ValueChangedEventHandler<TValue> del, WeakDelegateDispose<ValueChangedEventHandler<TValue>> dispose = null, object binder = null)
		{
			return Create<ValueChangedEventHandler<TValue>>(del, dispose, binder);
		}

		/// <summary>
		/// Creates a weak wrapper around the specified delegate.
		/// </summary>
		/// <param name="del">The delegate to wrap.</param>
		/// <param name="dispose">The method to call when the delegate can no longer be invoked because it was collected.</param>
		/// <param name="binder">An object that the original delegate is "bound" to. As long as this object is alive, the delegate will be as well. If null, the delegate's target is used.</param>
		/// <returns>The wrapper.</returns>
		public static Delegate Create(Delegate del, WeakDelegateDispose<Delegate> dispose = null, object binder = null)
		{
			if (del == null)
				throw new ArgumentNullException(nameof(del));

			Delegate[] invocations = del.GetInvocationList();
			if (invocations.Length > 1)
				throw new ArgumentException("Multicast delegates are not supported.", nameof(del));

			object target = invocations[0].Target;
			if (target == null)
				throw new ArgumentException("Delegate has no target.", nameof(del));

			if (binder == null)
				binder = target;

			if (binder.GetType().IsValueType)
				throw new ArgumentException("The object that the weak delegate is bound to can not be a value type.", nameof(binder));

			Type delegateType = del.GetType();

			Type infoType = typeof(WeakDelegateInfo<>).MakeGenericType(delegateType);
			WeakDelegateInfoBase info = (WeakDelegateInfoBase)Activator.CreateInstance(infoType, del, dispose);
			
			DynamicMethod wrapperMethod = wrappers.GetOrAdd(delegateType, buildDynamicMethod);
			Delegate wrapper = wrapperMethod.CreateDelegate(delegateType, info);
			info.SetWrapper(wrapper);

			List<Delegate> list = bindings.GetOrCreateValue(binder);
			lock (list)
			{
				list.Add(del);
			}
	
			return wrapper;
		}

		private static DynamicMethod buildDynamicMethod(Type delegateType)
		{
			MethodInfo invokeMethod = delegateType.GetMethod("Invoke") ?? throw new ArgumentException("Invalid type.", nameof(delegateType));
			ParameterInfo[] parameters = invokeMethod.GetParameters();
			Type infoType = typeof(WeakDelegateInfo<>).MakeGenericType(delegateType);
			Type weakType = typeof(WeakReference<>).MakeGenericType(delegateType);
			MethodInfo tryGetTargetMethod = weakType.GetMethod("TryGetTarget");
			FieldInfo targetField = infoType.GetField("Target");
			MethodInfo disposeMethod = infoType.GetMethod("Dispose");

			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(delegateType).WithThis(infoType), $"Weak<{delegateType}>");

			using (Local weakRefLocal = emit.DeclareLocal(weakType, "weakRef"))
			{
				Label disposeLabel = emit.DefineLabel("dispose");

				// weakRef = this.Delegate;
				emit.LoadArgument(0);
				emit.LoadField(targetField);
				emit.StoreLocal(weakRefLocal);

				// if(weakRef == null) goto Dispose;
				emit.LoadLocal(weakRefLocal);
				emit.BranchIfFalse(disposeLabel);

				using (Local targetLocal = emit.DeclareLocal(delegateType, "target"))
				{
					// if(!weakRef.TryGetTarget(out target)) goto Dispose;
					emit.LoadLocal(weakRefLocal);
					emit.LoadLocalAddress(targetLocal);
					emit.CallVirtual(tryGetTargetMethod);
					emit.BranchIfFalse(disposeLabel);

					// Push target and parameters onto stack.
					emit.LoadLocal(targetLocal);
					for (int i = 0; i < parameters.Length; ++i)
					{
						emit.LoadArgument(i + 1);
					}

					// Invoke delegate.
					emit.CallVirtual(invokeMethod);
					emit.Return();
				}

				// Dispose:
				emit.MarkLabel(disposeLabel);
				emit.LoadArgument(0);
				emit.Call(disposeMethod);

				if (invokeMethod.ReturnType != typeof(void))
				{
					using (Local defaultValue = emit.DeclareLocal(invokeMethod.ReturnType, "defaultValue"))
					{
						emit.LoadLocal(defaultValue);
					}
				}

				emit.Return();
			}

			return emit.Compile();
		}

		#endregion
	}

	public delegate void WeakDelegateDispose<in T>(bool disposing, T wrapperDelegate) where T : Delegate;

	internal abstract class WeakDelegateInfoBase : IDisposable
	{
		public abstract void SetWrapper(Delegate wrapper);

		public virtual void Dispose()
		{

		}
	}

	/// <summary>
	/// Contains runtime info for a weak delegate. Used as "this" reference inside the wrapper.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	internal sealed class WeakDelegateInfo<T> : WeakDelegateInfoBase
		where T : Delegate
	{
		#region Fields

		public WeakReference<T> Target;
#pragma warning disable CS0649
		public T Wrapper;
#pragma warning restore CS0649
		public WeakDelegateDispose<T> DisposeFunc;
		private ConcurrentFlag isDisposed;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public WeakDelegateInfo(T target, WeakDelegateDispose<T> dispose)
		{
			this.Target = new WeakReference<T>(target);
			this.DisposeFunc = dispose;
			this.isDisposed = new ConcurrentFlag();
		}

		~WeakDelegateInfo()
		{
			if (!this.isDisposed.TrySet())
				return;

			this.Target = null;
			this.DisposeFunc?.Invoke(false, this.Wrapper);
			this.DisposeFunc = null;
		}

		#endregion

		#region Methods

		public override void SetWrapper(Delegate wrapper)
		{
			this.Wrapper = (T)wrapper;
		}

		public override void Dispose()
		{
			if (!this.isDisposed.TrySet())
				return;

			this.Target = null;
			this.DisposeFunc?.Invoke(true, this.Wrapper);
			this.DisposeFunc = null;
		}

		#endregion
	}
}
