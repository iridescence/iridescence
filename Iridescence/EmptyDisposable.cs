﻿using System;

namespace Iridescence
{
	/// <summary>
	/// Implements an <see cref="IDisposable"/> that does nothing.
	/// </summary>
	public sealed class EmptyDisposable : IDisposable
	{
		public static readonly EmptyDisposable Instance = new EmptyDisposable();

		public void Dispose()
		{
		}
	}
}