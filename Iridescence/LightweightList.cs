﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Represents a wrapper around <see cref="List{T}"/> that creates the internal list once an item is added.
	/// Should never be exposed as a property on a class and only used as private field or local variable.
	/// </summary>
	[Serializable]
	public struct LightweightList<T> : IList<T>, IList, IReadOnlyList<T>
	{
		#region Fields

		private List<T> list;
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the list. Creates it if it has not been created yet.
		/// </summary>
		public List<T> List
		{
			get
			{
				if (this.list == null)
				{
					this.list = new List<T>();
				}

				return this.list;
			}
		}

		public int Count => this.list?.Count ?? 0;

		public T this[int index]
		{
			get => this.List[index];
			set => this.List[index] = value;
		}

		object IList.this[int index]
		{
			get => ((IList)this.List)[index];
			set => ((IList)this.List)[index] = value;
		}
		
		bool IList.IsFixedSize => ((IList)this.List).IsFixedSize;

		bool IList.IsReadOnly => ((IList)this.List).IsReadOnly;

		public bool IsSynchronized => ((ICollection)this.List).IsSynchronized;

		public object SyncRoot => ((ICollection)this.List).SyncRoot;

		bool ICollection<T>.IsReadOnly => ((ICollection<T>)this.List).IsReadOnly;

		#endregion
		
		#region Constructors

		#endregion
		
		#region Methods
		
		public IEnumerator<T> GetEnumerator()
		{
			if(this.list == null)
				return EmptyEnumerator<T>.Instance;

			return this.list.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)this.List).GetEnumerator();
		}

		public void Add(T item)
		{
			this.List.Add(item);
		}

		int IList.Add(object value)
		{
			return ((IList)this.List).Add(value);
		}

		public void Insert(int index, T item)
		{
			this.List.Insert(index, item);
		}

		void IList.Insert(int index, object value)
		{
			((IList)this.List).Insert(index, value);
		}

		public void RemoveAt(int index)
		{
			this.List.RemoveAt(index);
		}

		public bool Remove(T item)
		{
			if(this.list == null)
				return false;

			return this.list.Remove(item);
		}

		void IList.Remove(object value)
		{
			((IList)this.List).Remove(value);
		}

		public void Clear()
		{
			this.list?.Clear();
		}

		public bool Contains(T item)
		{
			if(this.list == null)
				return false;

			return this.list.Contains(item);
		}

		bool IList.Contains(object value)
		{
			if(this.list == null)
				return false;

			return ((IList)this.list).Contains(value);
		}

		public int IndexOf(T item)
		{
			if (this.list == null)
				return -1;

			return this.List.IndexOf(item);
		}

		int IList.IndexOf(object value)
		{
			if (this.list == null)
				return -1;

			return ((IList)this.list).IndexOf(value);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			this.List.CopyTo(array, arrayIndex);
		}

		public void CopyTo(Array array, int index)
		{
			((ICollection)this.List).CopyTo(array, index);
		}
		
		#endregion
	}
}
