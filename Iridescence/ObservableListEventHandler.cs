﻿namespace Iridescence
{
	/// <summary>
	/// Observable list event handler.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void ObservableListEventHandler<T>(object sender, ObservableListEventArgs<T> e);
}