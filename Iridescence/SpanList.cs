﻿using System;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Represents a list that uses a <see cref="Span{T}"/> as its primary storage and fallback back to a <see cref="List{T}"/> if the span is full.
	/// </summary>
	public ref struct SpanList<T>
	{
		#region Fields

		private Span<T> span;
		private List<T> list;
		private int count;

		#endregion

		#region Properties

		public int Count => this.count;

		public T this[int index]
		{
			get
			{
				if (unchecked((uint)index) >= this.count)
					throwIndexOob();

				if (index < this.span.Length)
					return this.span[index];

				return this.list[index - this.span.Length];
			}
		}

		#endregion

		#region Constructors

		public SpanList(Span<T> span, int initialCount = 0)
		{
			if (initialCount > span.Length)
				throw new ArgumentOutOfRangeException(nameof(initialCount));

			this.span = span;
			this.list = null;
			this.count = initialCount;
		}

		#endregion

		#region Methods

		private static void throwIndexOob()
		{
			throw new ArgumentOutOfRangeException("index");
		}

		private void addToHeap(in T item)
		{
			if (this.list == null)
				this.list = new List<T>();

			this.list.Add(item);
			++this.count;
		}

		public void Add(T item)
		{
			if (this.count >= this.span.Length)
			{
				this.addToHeap(item);
			}
			else
			{
				this.span[this.count++] = item;
			}
		}

		public void Add(in T item)
		{
			if (this.count >= this.span.Length)
			{
				this.addToHeap(item);
			}
			else
			{
				this.span[this.count++] = item;
			}
		}

		public void Clear()
		{
			this.list?.Clear();
			this.span.Clear();
			this.count = 0;
		}

		public Enumerator GetEnumerator() => new Enumerator(this);
			
		#endregion

		#region Nested Types

		public ref struct Enumerator
		{
			private readonly SpanList<T> list;
			private int index;

			public T Current => this.list[this.index];

			public Enumerator(SpanList<T> list)
			{
				this.list = list;
				this.index = -1;
			}

			public bool MoveNext()
			{
				return ++this.index < this.list.Count;
			}

			public void Reset()
			{
				this.index = -1;
			}
		}

		#endregion
	}
}
