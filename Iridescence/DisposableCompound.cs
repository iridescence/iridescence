﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence
{
	/// <summary>
	/// An IDisposable implementation that invokes Dispose() on multiple objects when disposed.
	/// </summary>
	public sealed class DisposableCompound : IDisposable
	{
		#region Fields

		private readonly IDisposable[] objects;

		#endregion

		#region Constructors

		public DisposableCompound(IEnumerable<IDisposable> objects)
		{
			this.objects = objects.ToArray();

			if(this.objects.Any(o => o == null))
				throw new ArgumentException("Object list can not contain null.", nameof(objects));
		}

		public DisposableCompound(params IDisposable[] objects)
		{
			if(objects.Any(o => o == null))
				throw new ArgumentException("Object list can not contain null.", nameof(objects));

			this.objects = new IDisposable[objects.Length];
			Array.Copy(objects, this.objects, objects.Length);
		}

		#endregion

		#region Methods

		public void Dispose()
		{
			foreach (IDisposable obj in this.objects)
				obj.Dispose();
		}

		#endregion
	}
}
