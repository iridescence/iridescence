﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Represents a circular buffer. Behaves like a bounded FIFO queue.
	/// </summary>
	public class CircularBuffer<T> : IReadOnlyList<T>
	{
		#region Fields

		private readonly T[] buffer;

		private int count;
		private int tail;
		private int head;
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the maximum number of items the buffer can hold.
		/// </summary>
		public int Capacity => this.buffer.Length;

		/// <summary>
		/// Gets the number of items currently in the buffer.
		/// </summary>
		public int Count => this.count;

		/// <summary>
		/// Gets the item at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public T this[int index]
		{
			get
			{
				if (index <= 0 || index >= this.count)
					throw new IndexOutOfRangeException(nameof(index));

				index += this.tail;
				if (index >= this.buffer.Length)
					index -= this.buffer.Length;

				return this.buffer[index];
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CircularBuffer{T}"/> class. 
		/// </summary>
		public CircularBuffer(T[] buffer)
			: this(buffer, 0, 0)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CircularBuffer{T}"/> class. 
		/// </summary>
		public CircularBuffer(T[] buffer, int tail, int head)
		{
			this.buffer = buffer ?? throw new ArgumentNullException(nameof(buffer));

			if (tail < 0 || tail > buffer.Length)
				throw new ArgumentOutOfRangeException(nameof(tail));

			if (head < 0 || head >= buffer.Length)
				throw new ArgumentOutOfRangeException(nameof(head));

			this.tail = tail;
			this.head = head;

			if (head >= tail)
			{
				this.count = head - tail;
			}
			else
			{
				this.count = this.buffer.Length - tail + head;
			}
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="CircularBuffer{T}"/> class. 
		/// </summary>
		public CircularBuffer(int capacity)
		{
			if (capacity <= 0)
				throw new ArgumentOutOfRangeException(nameof(capacity));

			this.buffer = new T[capacity];
		}
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Tries to write a value to the buffer.
		/// </summary>
		/// <param name="value">The value to write.</param>
		/// <param name="overwrite">Whether to overwrite existing data if the buffer is full.</param>
		/// <returns>False if there was no space in the buffer and the value was not written. True otherwise.</returns>
		public bool Write(T value, bool overwrite = false)
		{
			if (this.count == this.buffer.Length)
			{
				if (!overwrite)
					return false;

				// Discard "oldest" item.
				++this.tail;
				if (this.tail >= this.buffer.Length)
					this.tail = 0;

				--this.count;
			}

			this.buffer[this.head++] = value;
			if (this.head >= this.buffer.Length)
				this.head = 0;

			++this.count;
			return true;
		}

		/// <summary>
		/// Writes a span of values to the buffer.
		/// </summary>
		/// <param name="values">The values to write.</param>
		/// <param name="overwrite">Whether to overwrite existing data if the buffer is full.</param>
		/// <returns>The number of items written. Always equal to the number of input items if overwrite is true.</returns>
		public int Write(ReadOnlySpan<T> values, bool overwrite = false)
		{
			int available = this.buffer.Length - this.count;
			int numWritten = values.Length;

			if (overwrite)
			{
				// Write only the last n values to the buffer (where n = this.Capacity) - the rest would be immediately overwritten in this call anyway.
				values = values.Slice(Math.Max(0, values.Length - this.Capacity));

				int overwritten = values.Length - available;
				if (overwritten > 0)
				{
					// Discard oldest n items that will be overwritten.
					this.tail += overwritten;
					if (this.tail >= this.buffer.Length)
						this.tail -= this.buffer.Length;

					this.count -= overwritten;
				}
			}
			else if (available < values.Length)
			{
				// Only write as much items as there is space available.
				values = values.Slice(0, available);
				numWritten = available;
			}

			// Increment count.
			this.count += values.Length;

			// Copy values to buffer.
			while (values.Length > 0)
			{
				int n = Math.Min(values.Length, this.buffer.Length - this.head);
				values.Slice(0, n).CopyTo(this.buffer.ToSpan(this.head, n));
				this.head += n;
				if (this.head >= this.buffer.Length)
					this.head = 0;
				values = values.Slice(n);
			}


			return numWritten;
		}

		/// <summary>
		/// Tries to read one value from the buffer.
		/// </summary>
		/// <param name="value">The value that was read.</param>
		/// <returns>False if no value was available. True otherwise.</returns>
		public bool Read(out T value)
		{
			if (this.count == 0)
			{
				value = default;
				return false;
			}

			value = this.buffer[this.tail++];
			if (this.tail >= this.buffer.Length)
				this.tail = 0;

			--this.count;
			return true;
		}

		/// <summary>
		/// Reads a span of values from the buffer.
		/// </summary>
		/// <param name="values">The destination buffer.</param>
		/// <returns>The number of items that have been read.</returns>
		public int Read(Span<T> values)
		{
			int numRead = Math.Min(this.count, values.Length);
			values = values.Slice(0, numRead);

			// Decrement count.
			this.count -= values.Length;
			
			// Read from buffer.
			while (values.Length > 0)
			{
				int n = Math.Min(values.Length, this.buffer.Length - this.tail);
				this.buffer.ToSpan(this.tail, n).CopyTo(values.Slice(0, n));
				this.tail += n;
				if (this.tail >= this.buffer.Length)
					this.tail = 0;
				values = values.Slice(n);
			}

			return numRead;
		}

		/// <summary>
		/// Discards the specified number of items.
		/// Works the same as <see cref="Read(Span{T})"/> without actually returning any items.
		/// </summary>
		/// <param name="count"></param>
		/// <returns>The number of items actually discarded.</returns>
		public int Discard(int count)
		{
			int numDiscarded = Math.Min(this.count, count);

			// Decrement count.
			this.count -= numDiscarded;
			this.tail += numDiscarded;
			if (this.tail >= this.buffer.Length)
				this.tail -= this.buffer.Length;

			return numDiscarded;
		}
		
		public IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < this.count; ++i)
			{
				yield return this[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}
}
