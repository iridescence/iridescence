﻿using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Interface for generic collections with Add and Remove events.
	/// </summary>
	public interface IObservableCollection<T>
	{
		#region Events

		/// <summary>
		/// Occurs when an item was added to the collection.
		/// </summary>
		event ObservableListEventHandler<T> Added;

		/// <summary>
		/// Occurs when an item was removed from the collection.
		/// </summary>
		event ObservableListEventHandler<T> Removed;

		#endregion
	}

	/// <summary>
	/// Interface for generic lists with Add and Remove events.
	/// </summary>
	public interface IObservableList<T> : IList<T>, IObservableCollection<T>
	{

	}

	/// <summary>
	/// Interface for generic read-only lists with Add and Remove events.
	/// </summary>
	public interface IObservableReadOnlyList<T> : IReadOnlyList<T>, IObservableCollection<T>
	{

	}
}
