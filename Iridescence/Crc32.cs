﻿using System;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;

namespace Iridescence
{
	/// <summary>
	/// Holds the parameters of a CRC-32 checksum algorithm.
	/// </summary>
	public sealed class Crc32
	{
		#region Fields

		public static readonly Crc32 Default = new Crc32(0xEDB88320u, 0xFFFFFFFFu, 0xFFFFFFFFu);

		/// <summary>
		/// The polynomial of the CRC-32.
		/// </summary>
		public readonly uint Polynomial;

		/// <summary>
		/// The initial value of the checksum.
		/// </summary>
		public readonly uint InitialValue;

		/// <summary>
		/// A XOR mask applied to the checksum on return.
		/// </summary>
		public readonly uint Mask;

		/// <summary>
		/// The look-up table.
		/// </summary>
		public readonly uint[] Table;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new CRC-32 hash algorithm using the specified polynomial.
		/// </summary>
		/// <param name="polynomial"></param>
		/// <param name="initialValue"></param>
		/// <param name="mask"></param>
		public Crc32(uint polynomial, uint initialValue, uint mask)
		{
			this.Polynomial = polynomial;
			this.InitialValue = initialValue;
			this.Mask = mask;
			this.Table = new uint[256];

			for (uint i = 0; i < 256; i++)
			{
				uint entry = i;

				for (uint j = 0; j < 8; j++)
					if ((entry & 1) == 1)
						entry = (entry >> 1) ^ polynomial;
					else
						entry = entry >> 1;

				this.Table[i] = entry;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Computes the checksum for the specified byte array.
		/// </summary>
		/// <param name="data">The input data.</param>
		/// <returns>The checksum.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Pure]
		public uint ComputeChecksum(ReadOnlySpan<byte> data)
		{
			return this.ComputeChecksum(this.InitialValue, data) ^ this.Mask;
		}

		/// <summary>
		/// Computes the checksum for the specified byte array.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The initial value.</param>
		/// <param name="data">The input data.</param>
		/// <returns>The checksum.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Pure]
		public uint ComputeChecksum(uint current, ReadOnlySpan<byte> data)
		{
			this.ComputeChecksum(ref current, data);
			return current;
		}

		/// <summary>
		/// Computes the checksum for the specified byte array.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The initial value.</param>
		/// <param name="data">The input data.</param>
		/// <returns>The checksum.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void ComputeChecksum(ref uint current, ReadOnlySpan<byte> data)
		{
			for (int i = 0; i < data.Length; i++)
			{
				current = (current >> 8) ^ this.Table[(current & 0xFF) ^ data[i]];
			}
		}

		/// <summary>
		/// Computes the checksum for the specified byte.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Pure]
		public uint ComputeChecksum(uint current, byte data)
		{
			return (current >> 8) ^ this.Table[(current & 0xFF) ^ data];
		}

		/// <summary>
		/// Computes the checksum for the specified byte.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current"></param>
		/// <param name="data"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void ComputeChecksum(ref uint current, byte data)
		{
			current = (current >> 8) ^ this.Table[(current & 0xFF) ^ data];
		}

		#endregion
	}
}
