﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence
{
	/// <summary>
	/// Represents a collection of weak references to objects.
	/// </summary>
	public class WeakCollection<T> : ICollection<T> 
		where T : class
	{
		#region Fields

		private readonly List<WeakReference<T>> list;
		private readonly IEqualityComparer<T> comparer;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the number of weak references in the list.
		/// </summary>
		/// <remarks>
		/// This number does not necessarily represents the number of live objects in the collection.
		/// </remarks>
		public int Count => this.list.Count;

		bool ICollection<T>.IsReadOnly => false;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WeakCollection class. 
		/// </summary>
		public WeakCollection()
			: this(EqualityComparer<T>.Default)
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the WeakCollection class. 
		/// </summary>
		public WeakCollection(IEqualityComparer<T> equalityComparer)
		{
			this.list = new List<WeakReference<T>>();
			this.comparer = equalityComparer;
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Adds a weak reference to the specified item to the collection.
		/// </summary>
		/// <param name="item"></param>
		public void Add(T item)
		{
			if (item == null)
				throw new ArgumentNullException(nameof(item));

			this.list.Add(new WeakReference<T>(item));
		}

		/// <summary>
		/// Removes the weak reference to the specified item from the collection.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool Remove(T item)
		{
			for (int i = 0; i < this.list.Count; ++i)
			{
				if (this.list[i].TryGetTarget(out T target))
				{
					if (this.comparer.Equals(item, target))
					{
						this.list.RemoveAt(i);
						return true;
					}
				}
				else
				{
					this.list.RemoveAt(i);
					--i;
				}
			}

			return false;
		}

		/// <summary>
		/// Clears the collection.
		/// </summary>
		public void Clear()
		{
			this.list.Clear();
		}

		/// <summary>
		/// Cleans the collection by removing dead entries and sets the internal capacity to the number of elements.
		/// </summary>
		public void TrimExcess()
		{
			for (int i = 0; i < this.list.Count; ++i)
			{
				if (!this.list[i].TryGetTarget(out _))
				{
					this.list.RemoveAt(i);
					--i;
				}
			}

			this.list.TrimExcess();
		}
		
		/// <summary>
		/// Returns a value that indicates whether a weak reference to the specified item exists in this collection.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool Contains(T item)
		{
			return this.Any(i => this.comparer.Equals(i, item));
		}
		
		/// <summary>
		/// Copies the alive items of this collection to the specified target array-
		/// </summary>
		/// <param name="array"></param>
		/// <param name="arrayIndex"></param>
		void ICollection<T>.CopyTo(T[] array, int arrayIndex)
		{
			foreach (T item in this)
			{
				array[arrayIndex++] = item;
			}
		}
		
		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return new Enumerator(this);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return new Enumerator(this);
		}

		#endregion

		#region Nested Types

		private sealed class Enumerator : IEnumerator<T>
		{
			#region Fields

			private readonly WeakCollection<T> collection;
			private int nextIndex;

			#endregion

			#region Properties

			public T Current { get; private set; }

			object IEnumerator.Current => this.Current;

			#endregion

			#region Constructors

			public Enumerator(WeakCollection<T> collection)
			{
				this.collection = collection;
				this.nextIndex = 0;
			}

			#endregion

			#region Methods
			
			public bool MoveNext()
			{
				for (;;)
				{
					if (this.nextIndex >= this.collection.list.Count)
					{
						this.Current = null;
						return false;
					}

					if (!this.collection.list[this.nextIndex].TryGetTarget(out T item))
					{
						this.collection.list.RemoveAt(this.nextIndex);
						continue;
					}

					++this.nextIndex;
					this.Current = item;
					return true;
				}
			}

			public void Reset()
			{
				this.nextIndex = 0;
				this.Current = null;
			}

			public void Dispose()
			{
				this.Current = null;
			}

			#endregion
		}

		#endregion
	}
}
