﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics.Tracing;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Implements a fancy, colorful console trace listener for detailed debugging.
	/// </summary>
	public class ColoredConsoleTraceWriter : EventListener
	{
		#region Fields

		private readonly ConcurrentQueue<TraceMessage> messageQueue;
		private readonly CancellationTokenSource cancellationTokenSource;
		private readonly Task writeTask;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the width of the message source column (in chars).
		/// </summary>
		public int SourceColumnWidth { get; set; }

		/// <summary>
		/// Gets or set the separator string.
		/// </summary>
		public string Separator { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ColoredConsoleTraceWriter"/> class.
		/// </summary>
		public ColoredConsoleTraceWriter()
		{
			this.SourceColumnWidth = 30;
			this.Separator = " ";

			this.messageQueue = new ConcurrentQueue<TraceMessage>();
			this.cancellationTokenSource = new CancellationTokenSource();
			this.writeTask = this.writeLoop(this.cancellationTokenSource.Token);

			AppDomain.CurrentDomain.UnhandledException += this.onUnhandledException;
		}

		#endregion

		#region Methods

		protected override void OnEventSourceCreated(EventSource eventSource)
		{
			base.OnEventSourceCreated(eventSource);
			if (eventSource.Name == TraceEventSource.EventSourceName)
			{
				this.EnableEvents(eventSource, EventLevel.LogAlways);
			}
		}

		protected virtual bool FilterMessage(in TraceMessage message)
		{
			return true;
		}
		
		protected override void OnEventWritten(EventWrittenEventArgs eventData)
		{
			string text = (string)eventData.Payload[0];
			string source = (string)eventData.Payload[1];
			TraceMessage message = new TraceMessage(text, source, eventData.Level);
			if (!this.FilterMessage(message))
				return;
			this.messageQueue.Enqueue(message);
		}

		private void flush()
		{
			while (this.messageQueue.TryDequeue(out TraceMessage msg))
			{
				this.write(msg);
			}
		}

		private void onUnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			this.flush();
		}

		private async Task writeLoop(CancellationToken cancellationToken)
		{
			while (!cancellationToken.IsCancellationRequested)
			{
				if (!this.messageQueue.TryDequeue(out TraceMessage msg))
				{
					try
					{
						await Task.Delay(1, cancellationToken);
					}
					catch (OperationCanceledException)
					{
						break;
					}

					continue;
				}

				this.write(msg);
			}

			this.flush();
		}

		private void write(TraceMessage message)
		{
			ConsoleColor fgColor = Console.ForegroundColor;
			ConsoleColor bgColor = Console.BackgroundColor;

			int pos = 0;
			string s = message.Source;

			if (s.Length > this.SourceColumnWidth)
			{
				pos = s.Length - this.SourceColumnWidth;
			}

			int pathSeparator = s.LastIndexOf('\\', s.Length - 1, s.Length - pos);
			if (pathSeparator < 0)
				pathSeparator = s.LastIndexOf('/', s.Length - 1, s.Length - pos);
			if (pathSeparator >= 0)
			{
				int end = pathSeparator + 1;
				Console.Write(s.Substring(pos, end - pos));
				pos = end;
			}

			Console.ForegroundColor = ConsoleColor.Blue;

			int lineSeparator = s.LastIndexOf(':', s.Length - 1, s.Length - pos);
			if (lineSeparator > 0)
			{
				Console.Write(s.Substring(pos, lineSeparator - pos));

				Console.ForegroundColor = ConsoleColor.DarkGray;
				Console.Write(s.Substring(lineSeparator));
			}
			else
			{
				Console.Write(s.Substring(pos));
			}

			Console.ForegroundColor = ConsoleColor.Gray;
			Console.Write(this.Separator);
			
			switch (message.Level)
			{
				case EventLevel.Critical:
					Console.ForegroundColor = ConsoleColor.Black;
					Console.BackgroundColor = ConsoleColor.Red;
					break;
				case EventLevel.Error:
					Console.ForegroundColor = ConsoleColor.Red;
					break;
				case EventLevel.Warning:
					Console.ForegroundColor = ConsoleColor.Yellow;
					break;
				case EventLevel.Informational:
					Console.ForegroundColor = ConsoleColor.Gray;
					break;
				case EventLevel.Verbose:
					Console.ForegroundColor = ConsoleColor.DarkGray;
					break;
				default:
					Console.ForegroundColor = ConsoleColor.Gray;
					break;
			}

			Console.WriteLine(message.Message);

			Console.ForegroundColor = fgColor;
			Console.BackgroundColor = bgColor;
		}

		public override void Dispose()
		{
			base.Dispose();
			AppDomain.CurrentDomain.UnhandledException -= this.onUnhandledException;
			this.cancellationTokenSource.Cancel();
			this.writeTask.Wait();
		}

		#endregion

		#region Nested Types

		protected readonly struct TraceMessage
		{
			public readonly string Message;
			public readonly string Source;
			public readonly EventLevel Level;

			public TraceMessage(string message, string source, EventLevel level)
			{
				this.Message = message;
				this.Source = source;
				this.Level = level;
			}
		}

		#endregion
	}
}
