﻿using System;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Utility methods for disposables.
	/// </summary>
	public static class DisposableUtility
	{
		#region Properties

		public static IDisposable Dummy { get; } = new DummyDisposable();

		#endregion

		#region Methods

		public static void DisposeAll<T>(this T[] disposables) where T : IDisposable
		{
			for (int i = 0; i < disposables.Length; ++i)
			{
				disposables[i]?.Dispose();
			}
		}

		public static void DisposeAll<T>(this IEnumerable<T> disposables) where T : IDisposable
		{
			foreach (T disposable in disposables)
			{
				disposable?.Dispose();
			}
		}

		#endregion

		#region Nested Types

		private sealed class DummyDisposable : IDisposable
		{
			public void Dispose()
			{
				
			}
		}

		#endregion
	}
}
