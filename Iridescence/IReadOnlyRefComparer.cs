﻿using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Interface identical to <see cref="IComparer{T}"/>, except that the values to compare are passed by read-only reference.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IReadOnlyRefComparer<T>
	{
		int Compare(in T x, in T y);
	}

}
