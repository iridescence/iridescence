﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Json;

namespace Iridescence
{
	public static class JsonUtility
	{
		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out bool value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static bool GetValue(this IDictionary<string, JsonValue> obj, string key, bool defaultValue = default)
		{
			if (!obj.TryGetValue(key, out bool value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out sbyte value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static sbyte GetValue(this IDictionary<string, JsonValue> obj, string key, sbyte defaultValue = default)
		{
			if (!obj.TryGetValue(key, out sbyte value))
				value = defaultValue;

			return value;
		}
		
		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out byte value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static byte GetValue(this IDictionary<string, JsonValue> obj, string key, byte defaultValue = default)
		{
			if (!obj.TryGetValue(key, out byte value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out short value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static short GetValue(this IDictionary<string, JsonValue> obj, string key, short defaultValue = default)
		{
			if (!obj.TryGetValue(key, out short value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out ushort value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static ushort GetValue(this IDictionary<string, JsonValue> obj, string key, ushort defaultValue = default)
		{
			if (!obj.TryGetValue(key, out ushort value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out int value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static int GetValue(this IDictionary<string, JsonValue> obj, string key, int defaultValue = default)
		{
			if (!obj.TryGetValue(key, out int value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out uint value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static long GetValue(this IDictionary<string, JsonValue> obj, string key, long defaultValue = default)
		{
			if (!obj.TryGetValue(key, out long value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out long value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static ulong GetValue(this IDictionary<string, JsonValue> obj, string key, ulong defaultValue = default)
		{
			if (!obj.TryGetValue(key, out ulong value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out ulong value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static float GetValue(this IDictionary<string, JsonValue> obj, string key, float defaultValue = default)
		{
			if (!obj.TryGetValue(key, out float value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out float value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static double GetValue(this IDictionary<string, JsonValue> obj, string key, double defaultValue = default)
		{
			if (!obj.TryGetValue(key, out double value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out double value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static decimal GetValue(this IDictionary<string, JsonValue> obj, string key, decimal defaultValue = default)
		{
			if (!obj.TryGetValue(key, out decimal value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out decimal value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static char GetValue(this IDictionary<string, JsonValue> obj, string key, char defaultValue = default)
		{
			if (!obj.TryGetValue(key, out char value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out char value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static string GetValue(this IDictionary<string, JsonValue> obj, string key, string defaultValue = default)
		{
			if (!obj.TryGetValue(key, out string value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out string value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out DateTime value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static DateTime GetValue(this IDictionary<string, JsonValue> obj, string key, DateTime defaultValue = default)
		{
			if (!obj.TryGetValue(key, out DateTime value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out DateTimeOffset value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static DateTimeOffset GetValue(this IDictionary<string, JsonValue> obj, string key, DateTimeOffset defaultValue = default)
		{
			if (!obj.TryGetValue(key, out DateTimeOffset value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out Guid value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static Guid GetValue(this IDictionary<string, JsonValue> obj, string key, Guid defaultValue = default)
		{
			if (!obj.TryGetValue(key, out Guid value))
				value = defaultValue;

			return value;
		}
		
		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out TimeSpan value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static TimeSpan GetValue(this IDictionary<string, JsonValue> obj, string key, TimeSpan defaultValue = default)
		{
			if (!obj.TryGetValue(key, out TimeSpan value))
				value = defaultValue;

			return value;
		}
		
		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out Uri value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = jsonVal;
			return true;
		}
		public static Uri GetValue(this IDictionary<string, JsonValue> obj, string key, Uri defaultValue = default)
		{
			if (!obj.TryGetValue(key, out Uri value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out JsonObject value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = (JsonObject)jsonVal;
			return true;
		}
		public static JsonObject GetValue(this IDictionary<string, JsonValue> obj, string key, JsonObject defaultValue = default)
		{
			if (!obj.TryGetValue(key, out JsonObject value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out JsonArray value)
		{
			if (!obj.TryGetValue(key, out JsonValue jsonVal))
			{
				value = default;
				return false;
			}

			value = (JsonArray)jsonVal;
			return true;
		}
		public static JsonArray GetValue(this IDictionary<string, JsonValue> obj, string key, JsonArray defaultValue = default)
		{
			if (!obj.TryGetValue(key, out JsonArray value))
				value = defaultValue;

			return value;
		}

		public static bool TryGetValue(this IDictionary<string, JsonValue> obj, string key, out Version value)
		{
			if (!obj.TryGetValue(key, out string str))
			{
				value = default;
				return false;
			}

			value = Version.Parse(str);
			return true;
		}
		public static Version GetValue(this IDictionary<string, JsonValue> obj, string key, Version defaultValue = default)
		{
			if (!obj.TryGetValue(key, out Version value))
				value = defaultValue;

			return value;
		}

		/// <summary>
		/// Tries to get a list of a specified type from a json object.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="keyName"></param>
		/// <param name="strings"></param>
		/// <returns>Returns false if the key couldn't be found.</returns>
		public static bool TryGetStrings(this IDictionary<string, JsonValue> obj, string keyName, out ImmutableArray<string> strings)
		{
			strings = ImmutableArray<string>.Empty;

			if (!obj.TryGetValue(keyName, out JsonArray array))
				return false;

			if (array == null)
				return false;

			var builder = ImmutableArray.CreateBuilder<string>();

			foreach (JsonValue item in array)
			{
				if (item.JsonType != JsonType.String)
					return false;

				builder.Add(item);
			}

			strings = builder.ToImmutable();
			return true;
		}

		public static bool TryGetEnumValue<T>(this IDictionary<string, JsonValue> obj, string key, out T value)
			where T : struct, Enum
		{
			if (!obj.TryGetValue(key, out string s))
			{
				value = default;
				return false;
			}

			if (!Enum.TryParse(s, true, out value))
			{
				throw new ArgumentException($"'{s}' is not a valid value for enum '{typeof(T)}'.");
			}

			return true;
		}
		public static T GetEnumValue<T>(this IDictionary<string, JsonValue> obj, string key, T defaultValue = default)
			where T : struct, Enum
		{
			if (!obj.TryGetEnumValue(key, out T value))
				value = defaultValue;

			return value;
		}

	}
}
