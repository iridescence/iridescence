﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Implements bit-based reading operations for byte streams.
	/// </summary>
	public class BitReader
	{
		#region Fields

		private ulong value;
		private int valueBits;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the base stream.
		/// </summary>
		public Stream Stream { get; }

		/// <summary>
		/// Gets a value that indicates whether the bit reader is currently aligned and has no cached bits.
		/// </summary>
		public bool IsAligned => this.valueBits == 0;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BitReader.
		/// </summary>
		public BitReader(Stream stream)
		{
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));

			if (!stream.CanRead)
				throw new ArgumentException("Stream is not readable.", nameof(stream));

			this.Stream = stream;
			this.valueBits = 0;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Byte-aligns the reader, forcing it to start reading at the next byte on the next operation.
		/// </summary>
		public void Align()
		{
			this.valueBits = 0;
		}

		/// <summary>
		/// Reads a single bit.
		/// </summary>
		/// <returns>1 or 0.</returns>
		public int ReadBit()
		{
			if (this.valueBits == 0)
			{
				this.value = this.Stream.ReadByteOrThrow();
				this.valueBits = 8;
			}

			int result = (int)(this.value & 1);

			this.value >>= 1;
			--this.valueBits;

			return result;
		}

		/// <summary>
		/// Reads the specified number of bits.
		/// </summary>
		/// <param name="numBits"></param>
		/// <returns></returns>
		public ulong ReadBits(int numBits)
		{
			if (numBits > 64 || numBits < 0)
				throw new ArgumentException("Invalid number of bits.");

			ulong result = 0;
			int position = 0;

			while (numBits > 0)
			{
				if (this.valueBits == 0)
				{
					this.value = this.Stream.ReadByteOrThrow();
					this.valueBits = 8;
				}

				int count = this.valueBits;
				if (count > numBits)
					count = numBits;

				ulong masked = this.value & ((1ul << count) - 1);
				result |= masked << position;

				position += count;
				numBits -= count;

				this.value >>= count;
				this.valueBits -= count;
			}

			return result;
		}

		/// <summary>
		/// Reads a unary-coded value.
		/// </summary>
		/// <returns></returns>
		public ulong ReadUnary()
		{
			ulong n = 0;
			
			for(;;)
			{
				if (this.valueBits == 0)
				{
					this.value = this.Stream.ReadByteOrThrow();
					this.valueBits = 8;
				}

				if((this.value & 1) == 0)
					break;

				++n;
				this.value >>= 1;
				--this.valueBits;
			}

			this.value >>= 1;
			--this.valueBits;
			return n;
		}

		/// <summary>
		/// Reads a Golomb-Rice-coded value.
		/// </summary>
		/// <param name="p"></param>
		/// <returns></returns>
		public ulong ReadGolombRice(int p)
		{
			ulong q = this.ReadUnary();
			ulong r = this.ReadBits(p);
			return (q << p) | r;
		}

		/// <summary>
		/// Reads a variable-length integer.
		/// </summary>
		/// <param name="maxBits">The maximum number of bits possible for the value.</param>
		/// <returns></returns>
		public ulong ReadVUInt(int maxBits)
		{
			if (maxBits < 1 || maxBits > 64)
				throw new ArgumentOutOfRangeException(nameof(maxBits));

			int numBits = 8;

			while (this.ReadBit() != 0)
			{
				numBits += 8;

				if (numBits >= maxBits)
				{
					numBits = maxBits;
					break;
				}
			}

			return this.ReadBits(numBits);
		}

		/// <summary>
		/// Reads a variable-length integer.
		/// </summary>
		/// <param name="maxBits">The maximum number of bits possible for the value.</param>
		/// <returns></returns>
		public long ReadVInt(int maxBits)
		{
			return (long)this.ReadVUInt(maxBits);
		}

		#endregion
	}
}
