﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Represents a stream that writes all data that is read from it or written to it to separate listening streams.
	/// </summary>
	public class MonitorStream : Stream
	{
		#region Fields

		private readonly bool leaveOpen;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the base stream that is being read/written.
		/// </summary>
		public Stream BaseStream { get; }

		/// <summary>
		/// Gets the stream that receives all read data.
		/// </summary>
		public Stream ReadMonitor { get; }

		/// <summary>
		/// Gets the stream that receives all written data.
		/// </summary>
		public Stream WriteMonitor { get; }

		/// <inheritdoc/>
		public override bool CanRead => this.BaseStream.CanRead;

		/// <inheritdoc/>
		public override bool CanSeek => this.BaseStream.CanSeek;

		/// <inheritdoc/>
		public override bool CanWrite => this.BaseStream.CanWrite;

		/// <inheritdoc/>
		public override long Length => this.BaseStream.Length;

		/// <inheritdoc/>
		public override long Position
		{
			get => this.BaseStream.Position;
			set => this.BaseStream.Position = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new MonitorStream.
		/// </summary>
		public MonitorStream(Stream baseStream, Stream readMonitor, Stream writeMonitor, bool leaveOpen)
		{
			this.BaseStream = baseStream;
			this.ReadMonitor = readMonitor;
			this.WriteMonitor = writeMonitor;
			this.leaveOpen = leaveOpen;
		}

		#endregion

		#region Methods

		/// <inheritdoc/>
		public override void Flush()
		{
			this.BaseStream.Flush();
			this.ReadMonitor?.Flush();
			this.WriteMonitor?.Flush();
		}

		/// <inheritdoc/>
		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.BaseStream.Seek(offset, origin);
		}

		/// <inheritdoc/>
		public override void SetLength(long value)
		{
			this.BaseStream.SetLength(value);
		}

		/// <inheritdoc/>
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			int numBytes = this.BaseStream.Read(buffer, offset, count);

			if (numBytes > 0)
			{
				this.ReadMonitor?.Write(buffer, offset, numBytes);
			}

			return numBytes;
		}

		/// <inheritdoc/>
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			this.BaseStream.Write(buffer, offset, count);
			this.WriteMonitor?.Write(buffer, offset, count);
		}

		/// <inheritdoc/>
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing)
			{
				if (!this.leaveOpen)
					this.BaseStream.Close();
			}
		}

		#endregion
	}
}
