﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Represents an abstract <see cref="Stream"/> wrapper class.
	/// </summary>
	public class StreamWrapper : Stream
	{
		#region Fields

		#endregion
		
		#region Properties
		
		/// <summary>
		/// Gets the base stream.
		/// </summary>
		protected Stream BaseStream { get; }

		/// <summary>
		/// Gets a value that indicates whether the base stream should be left open when this stream is closed.
		/// </summary>
		protected bool LeaveOpen { get; }

		/// <inheritdoc />
		public override bool CanRead => this.BaseStream.CanRead;

		/// <inheritdoc />
		public override bool CanSeek => this.BaseStream.CanSeek;

		/// <inheritdoc />
		public override bool CanWrite => this.BaseStream.CanWrite;

		/// <inheritdoc />
		public override bool CanTimeout => this.BaseStream.CanTimeout;

		/// <inheritdoc />
		public override long Length => this.BaseStream.Length;

		/// <inheritdoc />
		public override long Position
		{
			get => this.BaseStream.Position;
			set => this.BaseStream.Position = value;
		}

		/// <inheritdoc />
		public override int ReadTimeout
		{
			get => this.BaseStream.ReadTimeout;
			set => this.BaseStream.ReadTimeout = value;
		}

		/// <inheritdoc />
		public override int WriteTimeout
		{
			get => this.BaseStream.WriteTimeout;
			set => this.BaseStream.WriteTimeout = value;
		}

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="StreamWrapper"/> class. 
		/// </summary>
		protected StreamWrapper(Stream baseStream, bool leaveOpen)
		{
			this.BaseStream = baseStream;
			this.LeaveOpen = leaveOpen;
		}
		
		#endregion
		
		#region Methods
		
		/// <inheritdoc />
		public override void Flush()
		{
			this.BaseStream.Flush();
		}

		/// <inheritdoc />
		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			return this.BaseStream.FlushAsync(cancellationToken);
		}

		/// <inheritdoc />
		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.BaseStream.Seek(offset, origin);
		}

		/// <inheritdoc />
		public override void SetLength(long value)
		{
			this.BaseStream.SetLength(value);
		}

		/// <inheritdoc />
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.BaseStream.Write(buffer, offset, count);
		}

		/// <inheritdoc />
		public override void WriteByte(byte value)
		{
			this.BaseStream.WriteByte(value);
		}

		/// <inheritdoc />
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			return this.BaseStream.BeginWrite(buffer, offset, count, callback, state);
		}

		/// <inheritdoc />
		public override void EndWrite(IAsyncResult asyncResult)
		{
			this.BaseStream.EndWrite(asyncResult);
		}

		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			return this.BaseStream.WriteAsync(buffer, offset, count, cancellationToken);
		}

		/// <inheritdoc />
		public override int Read(byte[] buffer, int offset, int count)
		{
			return this.BaseStream.Read(buffer, offset, count);
		}

		/// <inheritdoc />
		public override int ReadByte()
		{
			return this.BaseStream.ReadByte();
		}

		/// <inheritdoc />
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			return this.BaseStream.BeginRead(buffer, offset, count, callback, state);
		}

		/// <inheritdoc />
		public override int EndRead(IAsyncResult asyncResult)
		{
			return this.BaseStream.EndRead(asyncResult);
		}

		/// <inheritdoc />
		public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			return this.BaseStream.ReadAsync(buffer, offset, count, cancellationToken);
		}

		/// <inheritdoc />
		public override Task CopyToAsync(Stream destination, int bufferSize, CancellationToken cancellationToken)
		{
			return this.BaseStream.CopyToAsync(destination, bufferSize, cancellationToken);
		}

		/// <inheritdoc />
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing)
			{
				if (!this.LeaveOpen)
				{
					this.BaseStream.Dispose();
				}
			}
		}

		#endregion
	}
}
