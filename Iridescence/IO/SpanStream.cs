﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Represents a stream that can read from/write to <see cref="Memory{T}"/>.
	/// </summary>
	public sealed class SpanStream<T> : Stream
		where T : struct
	{
		#region Fields

		private readonly ReadOnlyMemory<T> readMemory;
		private readonly Memory<T> writeMemory;
		private long position;

		#endregion

		#region Properties

		public override long Position
		{
			get => this.position;
			set
			{
				if(value < 0 || value > this.Length)
					throw new ArgumentOutOfRangeException(nameof(value));

				this.position = value;
			}
		}

		public override long Length { get; }

		public override bool CanRead { get; }
		public override bool CanWrite { get; }
		public override bool CanSeek => true;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SpanStream.
		/// </summary>
		public SpanStream(Memory<T> memory, bool canRead, bool canWrite)
		{
			this.readMemory = this.writeMemory = memory;
			this.Length = MemoryMarshal.AsBytes(memory.Span).Length;
			this.CanRead = canRead;
			this.CanWrite = canWrite;
		}

		/// <summary>
		/// Creates a new SpanStream.
		/// </summary>
		public SpanStream(ReadOnlyMemory<T> memory)
		{
			this.readMemory = memory;
			this.Length = MemoryMarshal.AsBytes(memory.Span).Length;
			this.CanRead = true;
			this.CanWrite = false;
		}

		#endregion

		#region Methods

		public override void Flush()
		{
			
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			switch (origin)
			{
				case SeekOrigin.Begin:
					this.Position = offset;
					break;

				case SeekOrigin.Current:
					this.Position += offset;
					break;

				case SeekOrigin.End:
					this.Position = this.Length - offset;
					break;

				default:
					throw new ArgumentException("Invalid origin", nameof(origin));
			}

			return this.Position;
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (!this.CanRead)
				throw new NotSupportedException("The stream does not support reading.");

			if (count > this.Length - this.Position)
				count = (int)(this.Length - this.Position);

			if (count == 0)
				return 0;

			MemoryMarshal.AsBytes(this.readMemory.Span).Slice((int)this.position, count).CopyTo(buffer.ToSpan(offset, count));
			this.Position += count;
			return count;
		}

		public override int ReadByte()
		{
			if (!this.CanRead)
				throw new NotSupportedException("The stream does not support reading.");

			if (this.Position >= this.Length)
				return -1;

			return MemoryMarshal.AsBytes(this.readMemory.Span)[(int)this.position++];
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));
			
			if (!this.CanWrite)
				throw new NotSupportedException("The stream does not support writing.");

			if (count > this.Length - this.Position)
				throw new NotSupportedException("Can not write past the end of the buffer.");

			if (count == 0)
				return;

			buffer.ToSpan(offset, count).CopyTo(MemoryMarshal.AsBytes(this.writeMemory.Span).Slice((int)this.position, count));
			this.Position += count;
		}

		public override void WriteByte(byte value)
		{
			if (!this.CanWrite)
				throw new NotSupportedException("The stream does not support writing.");

			if (this.Position >= this.Length )
				throw new NotSupportedException("Can not write past the end of the buffer.");

			MemoryMarshal.AsBytes(this.writeMemory.Span)[(int)this.position++] = value;
		}

		#endregion
	}
}
