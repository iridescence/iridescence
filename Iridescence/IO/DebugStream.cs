﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Defines the debug stream outputs.
	/// </summary>
	[Flags]
	public enum DebugStreamMask
	{
		None = 0,
		Read = 0x1,
		Write = 0x2,
		Seek = 0x4,
		Flush = 0x8,
		SetLength = 0x10,
		Dispose = 0x20,
		Data = 0x40,
		All = ~0,
	}

	/// <summary>
	/// Represents a stream that prints debug information to a TextWriter.
	/// </summary>
	public class DebugStream : Stream
	{
		#region Fields

		private readonly bool leaveOpen;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the base stream.
		/// </summary>
		public Stream Stream { get; }

		/// <summary>
		/// Gets or sets the output log.
		/// </summary>
		public TextWriter Log { get; set; }
		
		/// <summary>
		/// Gets or sets the output mask.
		/// </summary>
		public DebugStreamMask Mask { get; set; }

		/// <inheritdoc/>
		public override bool CanRead => this.Stream.CanRead;

		/// <inheritdoc/>
		public override bool CanSeek => this.Stream.CanSeek;

		/// <inheritdoc/>
		public override bool CanWrite => this.Stream.CanWrite;

		/// <inheritdoc/>
		public override long Length => this.Stream.Length;

		/// <inheritdoc/>
		public override long Position
		{
			get => this.Stream.Position;
			set
			{
				if (this.Mask.HasFlag(DebugStreamMask.Seek))
					this.Log.WriteLine($"Position = {value}");
				this.Stream.Position = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new DebugStream.
		/// </summary>
		public DebugStream(Stream stream, TextWriter log, DebugStreamMask mask, bool leaveOpen)
		{
			this.Stream = stream;
			this.Log = log;
			this.Mask = mask;
			this.leaveOpen = leaveOpen;
		}

		#endregion

		#region Methods

		/// <inheritdoc/>
		public override void Flush()
		{
			if (this.Mask.HasFlag(DebugStreamMask.Flush))
				this.Log.WriteLine("Flush()");
			this.Stream.Flush();
		}

		/// <inheritdoc/>
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (this.Mask.HasFlag(DebugStreamMask.Dispose))
				this.Log.WriteLine($"Dispose({disposing})");

			if (disposing)
			{
				if (!this.leaveOpen)
					this.Stream.Dispose();
			}
		}

		/// <inheritdoc/>
		public override long Seek(long offset, SeekOrigin origin)
		{
			if (this.Mask.HasFlag(DebugStreamMask.Seek))
			this.Log.WriteLine($"Seek({offset}, {origin})");
			return this.Stream.Seek(offset, origin);
		}

		/// <inheritdoc/>
		public override void SetLength(long value)
		{
			if (this.Mask.HasFlag(DebugStreamMask.SetLength))
				this.Log.WriteLine($"SetLength({value})");
			this.Stream.SetLength(value);
		}

		private static void dumpHex(byte[] buffer, int offset, int count)
		{
			while (offset < count)
			{
				int len = Math.Min(16, count - offset);

				for (int i = 0; i < len; ++i)
				{
					Console.Write($"{buffer[offset + i]:X2} ");
				}

				for (int i = len; i < 16; ++i)
				{
					Console.Write("   ");
				}

				Console.Write("| ");

				for (int i = 0; i < len; ++i)
				{
					if (char.IsControl((char)buffer[offset + i]))
						Console.Write(".");
					else
						Console.Write((char)buffer[offset + i]);
				}

				offset += len;

				Console.WriteLine();
			}
		}

		/// <inheritdoc/>
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (this.Mask.HasFlag(DebugStreamMask.Read))
				this.Log.Write($"Read({buffer}, {offset}, {count})");

			int result = this.Stream.Read(buffer, offset, count);

			if (this.Mask.HasFlag(DebugStreamMask.Read))
				this.Log.WriteLine($" = {result}");

			if (this.Mask.HasFlag(DebugStreamMask.Data))
				dumpHex(buffer, offset, count);

			return result;
		}

		/// <inheritdoc/>
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (this.Mask.HasFlag(DebugStreamMask.Write))
				this.Log.WriteLine($"Write({buffer}, {offset}, {count})");

			if (this.Mask.HasFlag(DebugStreamMask.Data))
				dumpHex(buffer, offset, count);

			this.Stream.Write(buffer, offset, count);
		}

		#endregion
	}
}
