﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Span IO extensions
	/// </summary>
	public static class SpanExtensions
	{
		/// <summary>
		/// Writes the specified value to a byte span.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="span"></param>
		/// <param name="value"></param>
		public static void Write<T>(this Span<byte> span, T value)
			where T : struct
		{
			MemoryMarshal.Write(span, ref value);
		}

		/// <summary>
		/// Reads a value of the specified type from a byte span.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="span"></param>
		/// <returns></returns>
		public static T Read<T>(this Span<byte> span)
			where T : struct
		{
			return MemoryMarshal.Read<T>(span);
		}

		/// <summary>
		/// Reads a value of the specified type from a byte span.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="span"></param>
		/// <returns></returns>
		public static T Read<T>(this ReadOnlySpan<byte> span)
			where T : struct
		{
			return MemoryMarshal.Read<T>(span);
		}

		/// <summary>
		/// Copies contents from the source span to the destination span.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dest"></param>
		/// <param name="source"></param>
		/// <remarks>
		/// Same as <see cref="Span{T}.CopyTo(Span{T})"/>, but with arguments swapped.
		/// </remarks>
		public static void CopyFrom<T>(this Span<T> dest, Span<T> source)
		{
			source.CopyTo(dest);
		}

		/// <summary>
		/// Copies contents from the source span to the destination span.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="dest"></param>
		/// <param name="source"></param>
		/// <remarks>
		/// Same as <see cref="Span{T}.CopyTo(Span{T})"/>, but with arguments swapped.
		/// </remarks>
		public static void CopyFrom<T>(this Span<T> dest, ReadOnlySpan<T> source)
		{
			source.CopyTo(dest);
		}

		/// <summary>
		/// Writes the specified value to the destination span and returns the remaining span.
		/// </summary>
		public static Span<byte> Pack<T>(this Span<byte> dest, T value)
			where T : struct
		{
			int length = Unsafe.SizeOf<T>();
			dest.Slice(0, length).Write(value);
			return dest.Slice(length);
		}

		/// <summary>
		/// Writes the specified span to the destination span and returns the remaining span.
		/// </summary>
		public static Span<byte> Pack(this Span<byte> dest, ReadOnlySpan<byte> source)
		{
			source.CopyTo(dest);
			return dest.Slice(source.Length);
		}

		/// <summary>
		/// Writes the specified span to the destination span and returns the remaining span.
		/// </summary>
		public static Span<byte> Pack(this Span<byte> dest, Span<byte> source)
		{
			source.CopyTo(dest);
			return dest.Slice(source.Length);
		}

		/// <summary>
		/// Reads a value from a span and returns the remaining span.
		/// </summary>
		public static ReadOnlySpan<byte> Unpack<T>(this ReadOnlySpan<byte> source, out T value)
			where T : struct
		{
			int length = Unsafe.SizeOf<T>();
			value = source.Slice(0, length).Read<T>();
			return source.Slice(length);
		}

		/// <summary>
		/// Reads a value from a span and returns the remaining span.
		/// </summary>
		public static Span<byte> Unpack<T>(this Span<byte> source, out T value)
			where T : struct
		{
			int length = Unsafe.SizeOf<T>();
			value = source.Slice(0, length).Read<T>();
			return source.Slice(length);
		}

		/// <summary>
		/// Reads a value from a span and returns the remaining span.
		/// </summary>
		public static ReadOnlySpan<byte> Unpack(this ReadOnlySpan<byte> source, int length, out ReadOnlySpan<byte> segment)
		{
			segment = source.Slice(0, length);
			return source.Slice(length);
		}

		/// <summary>
		/// Reads a value from a span and returns the remaining span.
		/// </summary>
		public static ReadOnlySpan<byte> Unpack(this Span<byte> source, int length, out Span<byte> segment)
		{
			segment = source.Slice(0, length);
			return source.Slice(length);
		}
	}
}
