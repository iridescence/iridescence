﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence
{
	public static partial class StreamExtensions
	{
		#region Reading

		#region Common

		/// <summary>
		/// Asynchronously reads one byte from the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static async Task<int> ReadByteAsync(this Stream stream)
		{
			byte[] buffer = new byte[1];
			int length = await stream.ReadAsync(buffer, 0, 1);
			if (length == 0)
				return -1;

			return buffer[0];
		}
		
		/// <summary>
		/// Asynchronously reads one byte from the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public static async Task<int> ReadByteAsync(this Stream stream, CancellationToken cancellationToken)
		{
			byte[] buffer = new byte[1];
			int length = await stream.ReadAsync(buffer, 0, 1, cancellationToken);
			if (length == 0)
				return -1;

			return buffer[0];
		}

		/// <summary>
		/// Asynchronously reads one byte from a stream.
		/// Throws a <see cref="EndOfStreamException"/> if the stream ended before the byte could be read.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static async Task<byte> ReadByteOrThrowAsync(this Stream stream)
		{
			byte[] buffer = new byte[1];
			int length = await stream.ReadAsync(buffer, 0, 1);
			if (length == 0)
				throw new EndOfStreamException();

			return buffer[0];
		}
		
		/// <summary>
		/// Asynchronously reads one byte from a stream.
		/// Throws a <see cref="EndOfStreamException"/> if the stream ended before the byte could be read.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public static async Task<byte> ReadByteOrThrowAsync(this Stream stream, CancellationToken cancellationToken)
		{
			byte[] buffer = new byte[1];
			int length = await stream.ReadAsync(buffer, 0, 1, cancellationToken);
			if (length == 0)
				throw new EndOfStreamException();

			return buffer[0];
		}

		/// <summary>
		/// Asynchronously reads the specified number of bytes.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		public static async Task ReadOrThrowAsync(this Stream stream, byte[] buffer, int offset, int count)
		{
			while (count > 0)
			{
				int numBytes = await stream.ReadAsync(buffer, offset, count);
				if (numBytes == 0)
					throw new EndOfStreamException();

				count -= numBytes;
				offset += numBytes;
			}
		}

		/// <summary>
		/// Asynchronously reads the specified number of bytes.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="count"></param>
		public static async Task<byte[]> ReadOrThrowAsync(this Stream stream, int count)
		{
			byte[] buffer = new byte[count];
			await stream.ReadOrThrowAsync(buffer, 0, count);
			return buffer;
		}
		
		/// <summary>
		/// Asynchronously reads a string of the specified length from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static Task<string> ReadStringAsync(this Stream stream, int length)
		{
			return stream.ReadStringAsync(DefaultEncoding, length);
		}
		
		/// <summary>
		/// Asynchronously reads a string of the specified length from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static async Task<string> ReadStringAsync(this Stream stream, Encoding encoding, int length)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(length);
			return encoding.GetString(buffer);
		}
		
		#endregion

		#region Little Endian

		/// <summary>
		/// Asynchronously reads a 16 bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<short> ReadInt16LAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(2);
			return (short)(buffer[0] | (buffer[1] << 8));
		}

		/// <summary>
		/// Asynchronously reads a 16 bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<ushort> ReadUInt16LAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(2);
			return (ushort)(buffer[0] | (buffer[1] << 8));
		}

		/// <summary>
		/// Asynchronously reads a 32 bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<int> ReadInt32LAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(4);
			return buffer[0] | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3] << 24);
		}

		/// <summary>
		/// Asynchronously reads a 32 bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<uint> ReadUInt32LAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(4);
			return (uint)buffer[0] | ((uint)buffer[1] << 8) | ((uint)buffer[2] << 16) | ((uint)buffer[3] << 24);
		}

		/// <summary>
		/// Asynchronously reads a 64 bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<long> ReadInt64LAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(8);
			return (long)buffer[0] | ((long)buffer[1] << 8) | ((long)buffer[2] << 16) | ((long)buffer[3] << 24) | ((long)buffer[4] << 32) | ((long)buffer[5] << 40) | ((long)buffer[6] << 48) | ((long)buffer[7] << 56);
		}

		/// <summary>
		/// Asynchronously reads a 64 bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<ulong> ReadUInt64LAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(8);
			return (ulong)buffer[0] | ((ulong)buffer[1] << 8) | ((ulong)buffer[2] << 16) | ((ulong)buffer[3] << 24) | ((ulong)buffer[4] << 32) | ((ulong)buffer[5] << 40) | ((ulong)buffer[6] << 48) | ((ulong)buffer[7] << 56);
		}
		
		/// <summary>
		/// Asynchronously reads a 16 bit floating point value from a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<Half> ReadFloat16LAsync(this Stream stream)
		{
			return PrimitiveUtility.BitsToHalf(await stream.ReadUInt16LAsync());
		}
		
		/// <summary>
		/// Asynchronously reads a 32 bit floating point value from a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<float> ReadFloat32LAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(4);

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			return BitConverter.ToSingle(buffer, 0);
		}

		/// <summary>
		/// Asynchronously reads a 64 bit floating point value from a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<double> ReadFloat64LAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(8);

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;

				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			return BitConverter.ToDouble(buffer, 0);
		}

		/// <summary>
		/// Asynchronously reads a string from a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static Task<string> ReadStringLAsync(this Stream stream)
		{
			return stream.ReadStringLAsync(DefaultEncoding);
		}

		/// <summary>
		/// Asynchronously reads a string from a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static async Task<string> ReadStringLAsync(this Stream stream, Encoding encoding)
		{
			int length = await stream.ReadInt32LAsync();
			if (length < 0)
				return null;

			return await stream.ReadStringAsync(encoding, length);
		}

		#endregion

		#region Big Endian

		/// <summary>
		/// Asynchronously reads a 16 bit integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<short> ReadInt16BAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(2);
			return (short)(buffer[1] | (buffer[0] << 8));
		}

		/// <summary>
		/// Asynchronously reads a 16 bit unsigned integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<ushort> ReadUInt16BAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(2);
			return (ushort)(buffer[1] | (buffer[0] << 8));
		}

		/// <summary>
		/// Asynchronously reads a 32 bit integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<int> ReadInt32BAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(4);
			return buffer[3] | (buffer[2] << 8) | (buffer[1] << 16) | (buffer[0] << 24);
		}

		/// <summary>
		/// Asynchronously reads a 32 bit unsigned integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<uint> ReadUInt32BAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(4);
			return (uint)buffer[3] | ((uint)buffer[2] << 8) | ((uint)buffer[1] << 16) | ((uint)buffer[0] << 24);
		}

		/// <summary>
		/// Asynchronously reads a 64 bit integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<long> ReadInt64BAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(8);
			return (long)buffer[7] | ((long)buffer[6] << 8) | ((long)buffer[5] << 16) | ((long)buffer[4] << 24) | ((long)buffer[3] << 32) | ((long)buffer[2] << 40) | ((long)buffer[1] << 48) | ((long)buffer[0] << 56);
		}

		/// <summary>
		/// Asynchronously reads a 64 bit unsigned integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<ulong> ReadUInt64BAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(8);
			return (ulong)buffer[7] | ((ulong)buffer[6] << 8) | ((ulong)buffer[5] << 16) | ((ulong)buffer[4] << 24) | ((ulong)buffer[3] << 32) | ((ulong)buffer[2] << 40) | ((ulong)buffer[1] << 48) | ((ulong)buffer[0] << 56);
		}

		/// <summary>
		/// Asynchronously reads a 16 bit floating point value from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<Half> ReadFloat16BAsync(this Stream stream)
		{
			return PrimitiveUtility.BitsToHalf(await stream.ReadUInt16BAsync());
		}

		/// <summary>
		/// Asynchronously reads a 32 bit floating point value from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<float> ReadFloat32BAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(4);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			return BitConverter.ToSingle(buffer, 0);
		}

		/// <summary>
		/// Asynchronously reads a 64 bit floating point value from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static async Task<double> ReadFloat64BAsync(this Stream stream)
		{
			byte[] buffer = await stream.ReadOrThrowAsync(8);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;
			
				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			return BitConverter.ToDouble(buffer, 0);
		}

		/// <summary>
		/// Asynchronously reads a string from a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static Task<string> ReadStringBAsync(this Stream stream)
		{
			return stream.ReadStringBAsync(DefaultEncoding);
		}

		/// <summary>
		/// Asynchronously reads a string from a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static async Task<string> ReadStringBAsync(this Stream stream, Encoding encoding)
		{
			int length = await stream.ReadInt32BAsync();
			if (length < 0)
				return null;

			return await stream.ReadStringAsync(encoding, length);
		}

		#endregion

		#endregion

		#region Writing

		#endregion
	}
}
