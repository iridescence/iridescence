﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Represents a stream that can read from/write to arrays via the Buffer class.
	/// </summary>
	public sealed class BufferStream : Stream
	{
		#region Fields

		private readonly Array array;
		private long position;

		#endregion

		#region Properties

		public override long Position
		{
			get => this.position;
			set
			{
				if(value < 0 || value > this.Length)
					throw new ArgumentOutOfRangeException(nameof(value));

				this.position = value;
			}
		}

		public override long Length { get; }

		public override bool CanRead { get; }
		public override bool CanWrite { get; }
		public override bool CanSeek => true;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BufferStream.
		/// </summary>
		public BufferStream(Array array, bool canRead, bool canWrite)
		{
			this.array = array;
			this.Length = Buffer.ByteLength(array);
			this.CanRead = canRead;
			this.CanWrite = canWrite;
		}

		#endregion

		#region Methods

		public override void Flush()
		{
			
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			switch (origin)
			{
				case SeekOrigin.Begin:
					this.Position = offset;
					break;

				case SeekOrigin.Current:
					this.Position += offset;
					break;

				case SeekOrigin.End:
					this.Position = this.Length - offset;
					break;

				default:
					throw new ArgumentException("Invalid origin", nameof(origin));
			}

			return this.Position;
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (!this.CanRead)
				throw new NotSupportedException("The stream does not support reading.");

			if (count > this.Length - this.Position)
				count = (int)(this.Length - this.Position);

			if (count == 0)
				return 0;

			Buffer.BlockCopy(this.array, (int)this.Position, buffer, offset, count);
			this.Position += count;
			return count;
		}

		public override int ReadByte()
		{
			if (!this.CanRead)
				throw new NotSupportedException("The stream does not support reading.");

			if (this.Position >= this.Length)
				return -1;

			return Buffer.GetByte(this.array, (int)this.Position++);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));
			
			if (!this.CanWrite)
				throw new NotSupportedException("The stream does not support writing.");

			if (count > this.Length - this.Position)
				throw new NotSupportedException("Can not write past the end of the buffer.");

			if (count == 0)
				return;

			Buffer.BlockCopy(buffer, offset, this.array, (int)this.Position, count);
			this.Position += count;
		}

		public override void WriteByte(byte value)
		{
			if (!this.CanWrite)
				throw new NotSupportedException("The stream does not support writing.");

			if (this.Position >= this.Length )
				throw new NotSupportedException("Can not write past the end of the buffer.");

			Buffer.SetByte(this.array, (int)this.Position++, value);
		}

		#endregion
	}
}
