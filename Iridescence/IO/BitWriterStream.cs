﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Implements a <see cref="Stream"/> that writes to a <see cref="BitWriter"/>.
	/// </summary>
	public sealed class BitWriterStream : Stream
	{
		#region Fields

		private readonly BitWriter writer;
		private readonly bool aligned;

		#endregion

		#region Properties

		/// <inheritdoc/>
		public override bool CanRead => false;
		
		/// <inheritdoc/>
		public override bool CanSeek => false;
		
		/// <inheritdoc/>
		public override bool CanWrite => true;

		/// <inheritdoc/>
		public override long Length => throw new NotSupportedException();

		/// <inheritdoc/>
		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BitWriterStream.
		/// </summary>
		/// <param name="writer">The BitWriter to write to.</param>
		/// <param name="aligned">Determines whether all write operations should be byte-aligned.</param>
		public BitWriterStream(BitWriter writer, bool aligned)
		{
			this.writer = writer;
			this.aligned = aligned;
		}

		#endregion

		#region Methods

		/// <inheritdoc/>
		public override void Flush()
		{
			this.writer.Flush();
		}

		/// <inheritdoc/>
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override void WriteByte(byte value)
		{
			if (this.aligned)
			{
				this.writer.Align();
				this.writer.Stream.WriteByte(value);
			}
			else
			{
				this.writer.WriteBits(value, 8);
			}
		}

		/// <inheritdoc/>
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.writer.Flush();
			if (this.aligned || this.writer.IsAligned)
			{
				this.writer.Align();
				this.writer.Stream.Write(buffer, offset, count);
			}
			else
			{
				while (count >= 8)
				{
					this.writer.WriteBits(BitConverter.ToUInt64(buffer, offset), 64);
					offset += 8;
					count -= 8;
				}

				while (count > 0)
				{
					this.writer.WriteBits(buffer[offset++], 8);
					--count;
				}
			}
		}

		#endregion
	}
}
