﻿using System;
using System.Buffers;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Iridescence
{
	/// <summary>
	/// Utility class.
	/// </summary>
	public static partial class StreamExtensions
	{
		private const int maxBuffer = 65536;

		/// <summary>
		/// The default encoding for read and write operations with strings.
		/// </summary>
		public static readonly Encoding DefaultEncoding = Encoding.UTF8;

		/// <summary>
		/// Reads one byte from a stream.
		/// Throws a <see cref="EndOfStreamException"/> if the stream ended before the byte could be read.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static byte ReadByteOrThrow(this Stream stream)
		{
			int b = stream.ReadByte();
			if (b < 0)
				throw new EndOfStreamException();

			return (byte)b;
		}

		/// <summary>
		/// Reads the specified number of bytes and blocks the executing thread until all bytes have been read or throws an <see cref="EndOfStreamException"/> if the stream ended.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		public static void ReadOrThrow(this Stream stream, byte[] buffer, int offset, int count)
		{		
			while (count > 0)
			{
				int numBytes = stream.Read(buffer, offset, count);
				if (numBytes == 0)
					throw new EndOfStreamException();

				count -= numBytes;
				offset += numBytes;
			}
		}

		/// <summary>
		/// Reads the specified number of bytes and blocks the executing thread until all bytes have been read or throws an <see cref="EndOfStreamException"/> if the stream ended.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="buffer"></param>
		public static void ReadOrThrow(this Stream stream, Span<byte> buffer)
		{
			byte[] temp = ArrayPool<byte>.Shared.Rent(Math.Min(buffer.Length, maxBuffer));
			try
			{
				while (buffer.Length > 0)
				{
					int numBytes = stream.Read(temp, 0, Math.Min(buffer.Length, temp.Length));
					if (numBytes == 0)
						throw new EndOfStreamException();

					new Span<byte>(temp, 0, numBytes).CopyTo(buffer.Slice(0, numBytes));
					buffer = buffer.Slice(numBytes);
				}
			}
			finally
			{
				ArrayPool<byte>.Shared.Return(temp);
			}
		}

		/// <summary>
		/// Reads the specified number of bytes and blocks the executing thread until all bytes have been read or throws an EndOfStreamException if the stream ended.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="count"></param>
		/// <returns></returns>
		public static byte[] ReadOrThrow(this Stream stream, int count)
		{
			byte[] buffer = new byte[count];
			stream.ReadOrThrow(buffer, 0, count);
			return buffer;
		}
		
		/// <summary>
		/// Reads the specified number of bytes and blocks the executing thread until all bytes have been read or returns false if the stream ended before any bytes could be read.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		/// <returns></returns>
		public static bool TryReadOrThrow(this Stream stream, byte[] buffer, int offset, int count)
		{
			bool anyBytesRead = false;
			while (count > 0)
			{
				int numBytes = stream.Read(buffer, offset, count);
				if (numBytes == 0)
				{
					if (!anyBytesRead)
						return false;
					
					throw new EndOfStreamException();
				}

				count -= numBytes;
				offset += numBytes;
				anyBytesRead = true;
			}

			return true;
		}
		
		/// <summary>
		/// Reads the specified number of bytes and blocks the executing thread until all bytes have been read or returns false if the stream ended before any bytes could be read.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="buffer"></param>
		/// <param name="count"></param>
		/// <returns></returns>
		public static bool TryReadOrThrow(this Stream stream, int count, out byte[] buffer)
		{
			buffer = new byte[count];
			int offset = 0;
			
			while (count > 0)
			{
				int numBytes = stream.Read(buffer, offset, count);
				if (numBytes == 0)
				{
					if (offset == 0)
					{
						buffer = null;
						return false;
					}
					
					throw new EndOfStreamException();
				}

				count -= numBytes;
				offset += numBytes;
			}

			return true;
		}
		
		/// <summary>
		/// Reads from the specified stream until the stream ends.
		/// </summary>
		/// <param name="stream"></param>
		public static void ReadToEnd(this Stream stream)
		{
			byte[] temp = new byte[4096];

			while (stream.Read(temp, 0, temp.Length) > 0) ;
		}

		/// <summary>
		/// Reads a byte array prefixed with its length from the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static byte[] ReadBytesV(this Stream stream)
		{
			int length = stream.ReadInt32V();
			if (length < 0)
				return null;
			return stream.ReadOrThrow(length);
		}

		/// <summary>
		/// Reads a structure or formatted class from a stream.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static T ReadStructure<T>(this Stream stream)
		{
			int size = Marshal.SizeOf<T>();
			byte[] data = stream.ReadOrThrow(size);
			return data.ReadStructure<T>(0);
		}

		/// <summary>
		/// Reads a string of the specified length (in bytes) from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string ReadString(this Stream stream, int length, Encoding encoding)
		{
			return encoding.GetString(stream.ReadOrThrow(length));
		}

		/// <summary>
		/// Reads a string of the specified length (in bytes) from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string ReadString(this Stream stream, int length)
		{
			return stream.ReadString(length, DefaultEncoding);
		}
		
		/// <summary>
		/// Tries to read a string of the specified length from a stream or returns false if the stream ended before any bytes have been read.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <param name="length"></param>
		/// <param name="str"></param>
		/// <returns></returns>
		public static bool TryReadString(this Stream stream, int length, Encoding encoding, out string str)
		{
			str = null;

			if (!stream.TryReadOrThrow(length, out byte[] buffer))
				return false;
			
			str = encoding.GetString(buffer);
			return true;
		}
		
		/// <summary>
		/// Tries to read a string of the specified length from a stream or returns false if the stream ended before any bytes have been read.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="length"></param>
		/// <param name="str"></param>
		/// <returns></returns>
		public static bool TryReadString(this Stream stream, int length, out string str)
		{
			return stream.TryReadString(length, DefaultEncoding, out str);
		}
		
		/// <summary>
		/// Reads a null-terminated string from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadStringNT(this Stream stream)
		{
			return stream.ReadStringNT(DefaultEncoding);
		}

		/// <summary>
		/// Reads a null-terminated string from a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringNT(this Stream stream, Encoding encoding)
		{
			byte[] buffer = new byte[64];
			int pos = 0;

			while ((buffer[pos] = stream.ReadByteOrThrow()) != 0)
			{
				if (++pos == buffer.Length)
					Array.Resize(ref buffer, buffer.Length * 2);
			}

			return encoding.GetString(buffer, 0, pos);
		}

		/// <summary>
		/// Writes the specified byte array prefixed with the length to the stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="bytes"></param>
		public static void WriteBytesV(this Stream stream, byte[] bytes)
		{
			if (bytes == null)
			{
				stream.WriteInt32V(-1);
			}
			else
			{
				stream.WriteInt32V(bytes.Length);
				stream.Write(bytes, 0, bytes.Length);
			}
		}

		/// <summary>
		/// Writes a structure or formatted class to a stream.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStructure<T>(this Stream stream, T value)
		{
			int size = Marshal.SizeOf<T>();
			byte[] data = new byte[size];
			data.WriteStructure<T>(0, value);
			stream.Write(data, 0, size);
		}

		/// <summary>
		/// Writes string to a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteString(this Stream stream, string value)
		{
			stream.WriteString(value, DefaultEncoding);
		}
		
		/// <summary>
		/// Writes string to a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		/// <param name="encoding"></param>
		public static void WriteString(this Stream stream, string value, Encoding encoding)
		{
			byte[] buffer = encoding.GetBytes(value);
			stream.Write(buffer, 0, buffer.Length);
		}
	
		/// <summary>
		/// Writes a null-terminated string to a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStringNT(this Stream stream, string value)
		{
			stream.WriteStringNT(value, DefaultEncoding);
		}
		
		/// <summary>
		/// Writes a null-terminated string to a stream.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		/// <param name="encoding"></param>
		public static void WriteStringNT(this Stream stream, string value, Encoding encoding)
		{
			byte[] buffer = encoding.GetBytes(value);
			stream.Write(buffer, 0, buffer.Length);
			stream.WriteByte(0);
		}
		
		/// <summary>
		/// Copies the specified number of bytes from one stream to another.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="dest"></param>
		/// <param name="count"></param>
		/// <param name="bufferSize"></param>
		public static void CopyTo(this Stream source, Stream dest, long count, int bufferSize)
		{
			if (bufferSize > count)
				bufferSize = (int)count;

			byte[] buffer = new byte[bufferSize];

			while (count > 0)
			{
				if (bufferSize > count)
					bufferSize = (int)count;

				int numBytes = source.Read(buffer, 0, bufferSize);
				if (numBytes == 0)
					throw new EndOfStreamException();

				dest.Write(buffer, 0, numBytes);
				count -= numBytes;
			}
		}
		
		/// <summary>
		/// Returns the length (in bytes) of the variable-length encoding of the specified value.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		public static int GetVarIntLength(this short n)
		{
			return GetVarIntLength(unchecked((ushort)n));
		}

		/// <summary>
		/// Returns the length (in bytes) of the variable-length encoding of the specified value.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		public static int GetVarIntLength(this ushort n)
		{
			if (n < 0x7F)
				return 1;
			if (n < 0x3FFF)
				return 2;
			return 3;
		}

		/// <summary>
		/// Returns the length (in bytes) of the variable-length encoding of the specified value.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		public static int GetVarIntLength(this int n)
		{
			return GetVarIntLength(unchecked((uint)n));
		}
		
		/// <summary>
		/// Returns the length (in bytes) of the variable-length encoding of the specified value.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		public static int GetVarIntLength(this uint n)
		{
			int len = 1;
			while (n > 0x7F)
			{
				n >>= 7;
				++len;
			}
			return len;
		}
		
		/// <summary>
		/// Returns the length (in bytes) of the variable-length encoding of the specified value.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		public static int GetVarIntLength(this long n)
		{
			return GetVarIntLength(unchecked((ulong)n));
		}

		/// <summary>
		/// Returns the length (in bytes) of the variable-length encoding of the specified value.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		public static int GetVarIntLength(this ulong n)
		{
			int len = 1;
			while (n > 0x7F)
			{
				n >>= 7;
				++len;
			}
			return Math.Min(9, len);
		}

		/// <summary>
		/// Writes a <see cref="ReadOnlyMemory{T}"/> to a stream.
		/// </summary>
		public static void Write(this Stream stream, ReadOnlyMemory<byte> mem)
		{
			if (mem.Length == 0)
				return;

			if (MemoryMarshal.TryGetArray(mem, out ArraySegment<byte> seg))
			{
				stream.Write(seg.Array, seg.Offset, seg.Count);
			}
			else
			{
				stream.Write(mem.Span);
			}
		}
		
		/// <summary>
		/// Writes a <see cref="ReadOnlySpan{T}"/> to a stream.
		/// </summary>
		public static void Write(this Stream stream, ReadOnlySpan<byte> span)
		{
			if (span.Length == 0)
				return;

			byte[] temp = ArrayPool<byte>.Shared.Rent(Math.Min(maxBuffer, span.Length));
			try
			{
				while (span.Length > 0)
				{
					int n = Math.Min(temp.Length, span.Length);
					span.Slice(0, n).CopyTo(new Span<byte>(temp, 0, n));
					stream.Write(temp, 0, n);
					span = span.Slice(n);
				}
			}
			finally
			{
				ArrayPool<byte>.Shared.Return(temp);
			}
		}

		/// <summary>
		/// Reads a <see cref="Span{T}"/> from the stream by copying raw memory into the destination span.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream"></param>
		/// <param name="dest"></param>
		public static void ReadUnmanaged<T>(this Stream stream, Span<T> dest)
			where T : unmanaged
		{
			Span<byte> bytes = dest.AsBytes();
			stream.ReadOrThrow(bytes);
		}

		/// <summary>
		/// Reads a <see cref="Span{T}"/> from the stream by copying memory into the output variable.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream"></param>
		public static T ReadUnmanaged<T>(this Stream stream)
			where T : unmanaged
		{
			Span<T> temp = stackalloc T[1];
			stream.ReadUnmanaged(temp);
			return temp[0];
		}

		/// <summary>
		/// Writes a <see cref="Span{T}"/> by copying the source span's raw memory into the stream.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream"></param>
		/// <param name="source"></param>
		public static void WriteUnmanaged<T>(this Stream stream, ReadOnlySpan<T> source)
			where T : unmanaged
		{
			ReadOnlySpan<byte> bytes = source.AsBytes();
			stream.Write(bytes);
		}

		/// <summary>
		/// Writes a <see cref="Span{T}"/> by copying the source struct's raw memory into the stream.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteUnmanaged<T>(this Stream stream, in T value)
			where T : unmanaged
		{
			Span<T> temp = stackalloc T[1];
			temp[0] = value;
			stream.WriteUnmanaged<T>(temp);
		}
	}
}

