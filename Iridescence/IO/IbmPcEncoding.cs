﻿using System.Collections.Generic;
using System.Text;

namespace Iridescence
{
	public sealed class IbmPcEncoding : Encoding
	{
		private static readonly char[] byteToChar;
		private static readonly Dictionary<char, byte> charToByte;

		public static IbmPcEncoding Instance { get; } = new IbmPcEncoding();

		static IbmPcEncoding()
		{
			byteToChar = new char[256];
			byteToChar[0x00] = (char)0x0000; //NULL
			byteToChar[0x01] = (char)0x0001; //START OF HEADING
			byteToChar[0x02] = (char)0x0002; //START OF TEXT
			byteToChar[0x03] = (char)0x0003; //END OF TEXT
			byteToChar[0x04] = (char)0x0004; //END OF TRANSMISSION
			byteToChar[0x05] = (char)0x0005; //ENQUIRY
			byteToChar[0x06] = (char)0x0006; //ACKNOWLEDGE
			byteToChar[0x07] = (char)0x0007; //BELL
			byteToChar[0x08] = (char)0x0008; //BACKSPACE
			byteToChar[0x09] = (char)0x0009; //HORIZONTAL TABULATION
			byteToChar[0x0a] = (char)0x000a; //LINE FEED
			byteToChar[0x0b] = (char)0x000b; //VERTICAL TABULATION
			byteToChar[0x0c] = (char)0x000c; //FORM FEED
			byteToChar[0x0d] = (char)0x000d; //CARRIAGE RETURN
			byteToChar[0x0e] = (char)0x000e; //SHIFT OUT
			byteToChar[0x0f] = (char)0x000f; //SHIFT IN
			byteToChar[0x10] = (char)0x0010; //DATA LINK ESCAPE
			byteToChar[0x11] = (char)0x0011; //DEVICE CONTROL ONE
			byteToChar[0x12] = (char)0x0012; //DEVICE CONTROL TWO
			byteToChar[0x13] = (char)0x0013; //DEVICE CONTROL THREE
			byteToChar[0x14] = (char)0x0014; //DEVICE CONTROL FOUR
			byteToChar[0x15] = (char)0x0015; //NEGATIVE ACKNOWLEDGE
			byteToChar[0x16] = (char)0x0016; //SYNCHRONOUS IDLE
			byteToChar[0x17] = (char)0x0017; //END OF TRANSMISSION BLOCK
			byteToChar[0x18] = (char)0x0018; //CANCEL
			byteToChar[0x19] = (char)0x0019; //END OF MEDIUM
			byteToChar[0x1a] = (char)0x001a; //SUBSTITUTE
			byteToChar[0x1b] = (char)0x001b; //ESCAPE
			byteToChar[0x1c] = (char)0x001c; //FILE SEPARATOR
			byteToChar[0x1d] = (char)0x001d; //GROUP SEPARATOR
			byteToChar[0x1e] = (char)0x001e; //RECORD SEPARATOR
			byteToChar[0x1f] = (char)0x001f; //UNIT SEPARATOR
			byteToChar[0x20] = (char)0x0020; //SPACE
			byteToChar[0x21] = (char)0x0021; //EXCLAMATION MARK
			byteToChar[0x22] = (char)0x0022; //QUOTATION MARK
			byteToChar[0x23] = (char)0x0023; //NUMBER SIGN
			byteToChar[0x24] = (char)0x0024; //DOLLAR SIGN
			byteToChar[0x25] = (char)0x0025; //PERCENT SIGN
			byteToChar[0x26] = (char)0x0026; //AMPERSAND
			byteToChar[0x27] = (char)0x0027; //APOSTROPHE
			byteToChar[0x28] = (char)0x0028; //LEFT PARENTHESIS
			byteToChar[0x29] = (char)0x0029; //RIGHT PARENTHESIS
			byteToChar[0x2a] = (char)0x002a; //ASTERISK
			byteToChar[0x2b] = (char)0x002b; //PLUS SIGN
			byteToChar[0x2c] = (char)0x002c; //COMMA
			byteToChar[0x2d] = (char)0x002d; //HYPHEN-MINUS
			byteToChar[0x2e] = (char)0x002e; //FULL STOP
			byteToChar[0x2f] = (char)0x002f; //SOLIDUS
			byteToChar[0x30] = (char)0x0030; //DIGIT ZERO
			byteToChar[0x31] = (char)0x0031; //DIGIT ONE
			byteToChar[0x32] = (char)0x0032; //DIGIT TWO
			byteToChar[0x33] = (char)0x0033; //DIGIT THREE
			byteToChar[0x34] = (char)0x0034; //DIGIT FOUR
			byteToChar[0x35] = (char)0x0035; //DIGIT FIVE
			byteToChar[0x36] = (char)0x0036; //DIGIT SIX
			byteToChar[0x37] = (char)0x0037; //DIGIT SEVEN
			byteToChar[0x38] = (char)0x0038; //DIGIT EIGHT
			byteToChar[0x39] = (char)0x0039; //DIGIT NINE
			byteToChar[0x3a] = (char)0x003a; //COLON
			byteToChar[0x3b] = (char)0x003b; //SEMICOLON
			byteToChar[0x3c] = (char)0x003c; //LESS-THAN SIGN
			byteToChar[0x3d] = (char)0x003d; //EQUALS SIGN
			byteToChar[0x3e] = (char)0x003e; //GREATER-THAN SIGN
			byteToChar[0x3f] = (char)0x003f; //QUESTION MARK
			byteToChar[0x40] = (char)0x0040; //COMMERCIAL AT
			byteToChar[0x41] = (char)0x0041; //LATIN CAPITAL LETTER A
			byteToChar[0x42] = (char)0x0042; //LATIN CAPITAL LETTER B
			byteToChar[0x43] = (char)0x0043; //LATIN CAPITAL LETTER C
			byteToChar[0x44] = (char)0x0044; //LATIN CAPITAL LETTER D
			byteToChar[0x45] = (char)0x0045; //LATIN CAPITAL LETTER E
			byteToChar[0x46] = (char)0x0046; //LATIN CAPITAL LETTER F
			byteToChar[0x47] = (char)0x0047; //LATIN CAPITAL LETTER G
			byteToChar[0x48] = (char)0x0048; //LATIN CAPITAL LETTER H
			byteToChar[0x49] = (char)0x0049; //LATIN CAPITAL LETTER I
			byteToChar[0x4a] = (char)0x004a; //LATIN CAPITAL LETTER J
			byteToChar[0x4b] = (char)0x004b; //LATIN CAPITAL LETTER K
			byteToChar[0x4c] = (char)0x004c; //LATIN CAPITAL LETTER L
			byteToChar[0x4d] = (char)0x004d; //LATIN CAPITAL LETTER M
			byteToChar[0x4e] = (char)0x004e; //LATIN CAPITAL LETTER N
			byteToChar[0x4f] = (char)0x004f; //LATIN CAPITAL LETTER O
			byteToChar[0x50] = (char)0x0050; //LATIN CAPITAL LETTER P
			byteToChar[0x51] = (char)0x0051; //LATIN CAPITAL LETTER Q
			byteToChar[0x52] = (char)0x0052; //LATIN CAPITAL LETTER R
			byteToChar[0x53] = (char)0x0053; //LATIN CAPITAL LETTER S
			byteToChar[0x54] = (char)0x0054; //LATIN CAPITAL LETTER T
			byteToChar[0x55] = (char)0x0055; //LATIN CAPITAL LETTER U
			byteToChar[0x56] = (char)0x0056; //LATIN CAPITAL LETTER V
			byteToChar[0x57] = (char)0x0057; //LATIN CAPITAL LETTER W
			byteToChar[0x58] = (char)0x0058; //LATIN CAPITAL LETTER X
			byteToChar[0x59] = (char)0x0059; //LATIN CAPITAL LETTER Y
			byteToChar[0x5a] = (char)0x005a; //LATIN CAPITAL LETTER Z
			byteToChar[0x5b] = (char)0x005b; //LEFT SQUARE BRACKET
			byteToChar[0x5c] = (char)0x005c; //REVERSE SOLIDUS
			byteToChar[0x5d] = (char)0x005d; //RIGHT SQUARE BRACKET
			byteToChar[0x5e] = (char)0x005e; //CIRCUMFLEX ACCENT
			byteToChar[0x5f] = (char)0x005f; //LOW LINE
			byteToChar[0x60] = (char)0x0060; //GRAVE ACCENT
			byteToChar[0x61] = (char)0x0061; //LATIN SMALL LETTER A
			byteToChar[0x62] = (char)0x0062; //LATIN SMALL LETTER B
			byteToChar[0x63] = (char)0x0063; //LATIN SMALL LETTER C
			byteToChar[0x64] = (char)0x0064; //LATIN SMALL LETTER D
			byteToChar[0x65] = (char)0x0065; //LATIN SMALL LETTER E
			byteToChar[0x66] = (char)0x0066; //LATIN SMALL LETTER F
			byteToChar[0x67] = (char)0x0067; //LATIN SMALL LETTER G
			byteToChar[0x68] = (char)0x0068; //LATIN SMALL LETTER H
			byteToChar[0x69] = (char)0x0069; //LATIN SMALL LETTER I
			byteToChar[0x6a] = (char)0x006a; //LATIN SMALL LETTER J
			byteToChar[0x6b] = (char)0x006b; //LATIN SMALL LETTER K
			byteToChar[0x6c] = (char)0x006c; //LATIN SMALL LETTER L
			byteToChar[0x6d] = (char)0x006d; //LATIN SMALL LETTER M
			byteToChar[0x6e] = (char)0x006e; //LATIN SMALL LETTER N
			byteToChar[0x6f] = (char)0x006f; //LATIN SMALL LETTER O
			byteToChar[0x70] = (char)0x0070; //LATIN SMALL LETTER P
			byteToChar[0x71] = (char)0x0071; //LATIN SMALL LETTER Q
			byteToChar[0x72] = (char)0x0072; //LATIN SMALL LETTER R
			byteToChar[0x73] = (char)0x0073; //LATIN SMALL LETTER S
			byteToChar[0x74] = (char)0x0074; //LATIN SMALL LETTER T
			byteToChar[0x75] = (char)0x0075; //LATIN SMALL LETTER U
			byteToChar[0x76] = (char)0x0076; //LATIN SMALL LETTER V
			byteToChar[0x77] = (char)0x0077; //LATIN SMALL LETTER W
			byteToChar[0x78] = (char)0x0078; //LATIN SMALL LETTER X
			byteToChar[0x79] = (char)0x0079; //LATIN SMALL LETTER Y
			byteToChar[0x7a] = (char)0x007a; //LATIN SMALL LETTER Z
			byteToChar[0x7b] = (char)0x007b; //LEFT CURLY BRACKET
			byteToChar[0x7c] = (char)0x007c; //VERTICAL LINE
			byteToChar[0x7d] = (char)0x007d; //RIGHT CURLY BRACKET
			byteToChar[0x7e] = (char)0x007e; //TILDE
			byteToChar[0x7f] = (char)0x007f; //DELETE
			byteToChar[0x80] = (char)0x00c7; //LATIN CAPITAL LETTER C WITH CEDILLA
			byteToChar[0x81] = (char)0x00fc; //LATIN SMALL LETTER U WITH DIAERESIS
			byteToChar[0x82] = (char)0x00e9; //LATIN SMALL LETTER E WITH ACUTE
			byteToChar[0x83] = (char)0x00e2; //LATIN SMALL LETTER A WITH CIRCUMFLEX
			byteToChar[0x84] = (char)0x00e4; //LATIN SMALL LETTER A WITH DIAERESIS
			byteToChar[0x85] = (char)0x00e0; //LATIN SMALL LETTER A WITH GRAVE
			byteToChar[0x86] = (char)0x00e5; //LATIN SMALL LETTER A WITH RING ABOVE
			byteToChar[0x87] = (char)0x00e7; //LATIN SMALL LETTER C WITH CEDILLA
			byteToChar[0x88] = (char)0x00ea; //LATIN SMALL LETTER E WITH CIRCUMFLEX
			byteToChar[0x89] = (char)0x00eb; //LATIN SMALL LETTER E WITH DIAERESIS
			byteToChar[0x8a] = (char)0x00e8; //LATIN SMALL LETTER E WITH GRAVE
			byteToChar[0x8b] = (char)0x00ef; //LATIN SMALL LETTER I WITH DIAERESIS
			byteToChar[0x8c] = (char)0x00ee; //LATIN SMALL LETTER I WITH CIRCUMFLEX
			byteToChar[0x8d] = (char)0x00ec; //LATIN SMALL LETTER I WITH GRAVE
			byteToChar[0x8e] = (char)0x00c4; //LATIN CAPITAL LETTER A WITH DIAERESIS
			byteToChar[0x8f] = (char)0x00c5; //LATIN CAPITAL LETTER A WITH RING ABOVE
			byteToChar[0x90] = (char)0x00c9; //LATIN CAPITAL LETTER E WITH ACUTE
			byteToChar[0x91] = (char)0x00e6; //LATIN SMALL LIGATURE AE
			byteToChar[0x92] = (char)0x00c6; //LATIN CAPITAL LIGATURE AE
			byteToChar[0x93] = (char)0x00f4; //LATIN SMALL LETTER O WITH CIRCUMFLEX
			byteToChar[0x94] = (char)0x00f6; //LATIN SMALL LETTER O WITH DIAERESIS
			byteToChar[0x95] = (char)0x00f2; //LATIN SMALL LETTER O WITH GRAVE
			byteToChar[0x96] = (char)0x00fb; //LATIN SMALL LETTER U WITH CIRCUMFLEX
			byteToChar[0x97] = (char)0x00f9; //LATIN SMALL LETTER U WITH GRAVE
			byteToChar[0x98] = (char)0x00ff; //LATIN SMALL LETTER Y WITH DIAERESIS
			byteToChar[0x99] = (char)0x00d6; //LATIN CAPITAL LETTER O WITH DIAERESIS
			byteToChar[0x9a] = (char)0x00dc; //LATIN CAPITAL LETTER U WITH DIAERESIS
			byteToChar[0x9b] = (char)0x00a2; //CENT SIGN
			byteToChar[0x9c] = (char)0x00a3; //POUND SIGN
			byteToChar[0x9d] = (char)0x00a5; //YEN SIGN
			byteToChar[0x9e] = (char)0x20a7; //PESETA SIGN
			byteToChar[0x9f] = (char)0x0192; //LATIN SMALL LETTER F WITH HOOK
			byteToChar[0xa0] = (char)0x00e1; //LATIN SMALL LETTER A WITH ACUTE
			byteToChar[0xa1] = (char)0x00ed; //LATIN SMALL LETTER I WITH ACUTE
			byteToChar[0xa2] = (char)0x00f3; //LATIN SMALL LETTER O WITH ACUTE
			byteToChar[0xa3] = (char)0x00fa; //LATIN SMALL LETTER U WITH ACUTE
			byteToChar[0xa4] = (char)0x00f1; //LATIN SMALL LETTER N WITH TILDE
			byteToChar[0xa5] = (char)0x00d1; //LATIN CAPITAL LETTER N WITH TILDE
			byteToChar[0xa6] = (char)0x00aa; //FEMININE ORDINAL INDICATOR
			byteToChar[0xa7] = (char)0x00ba; //MASCULINE ORDINAL INDICATOR
			byteToChar[0xa8] = (char)0x00bf; //INVERTED QUESTION MARK
			byteToChar[0xa9] = (char)0x2310; //REVERSED NOT SIGN
			byteToChar[0xaa] = (char)0x00ac; //NOT SIGN
			byteToChar[0xab] = (char)0x00bd; //VULGAR FRACTION ONE HALF
			byteToChar[0xac] = (char)0x00bc; //VULGAR FRACTION ONE QUARTER
			byteToChar[0xad] = (char)0x00a1; //INVERTED EXCLAMATION MARK
			byteToChar[0xae] = (char)0x00ab; //LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
			byteToChar[0xaf] = (char)0x00bb; //RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK
			byteToChar[0xb0] = (char)0x2591; //LIGHT SHADE
			byteToChar[0xb1] = (char)0x2592; //MEDIUM SHADE
			byteToChar[0xb2] = (char)0x2593; //DARK SHADE
			byteToChar[0xb3] = (char)0x2502; //BOX DRAWINGS LIGHT VERTICAL
			byteToChar[0xb4] = (char)0x2524; //BOX DRAWINGS LIGHT VERTICAL AND LEFT
			byteToChar[0xb5] = (char)0x2561; //BOX DRAWINGS VERTICAL SINGLE AND LEFT DOUBLE
			byteToChar[0xb6] = (char)0x2562; //BOX DRAWINGS VERTICAL DOUBLE AND LEFT SINGLE
			byteToChar[0xb7] = (char)0x2556; //BOX DRAWINGS DOWN DOUBLE AND LEFT SINGLE
			byteToChar[0xb8] = (char)0x2555; //BOX DRAWINGS DOWN SINGLE AND LEFT DOUBLE
			byteToChar[0xb9] = (char)0x2563; //BOX DRAWINGS DOUBLE VERTICAL AND LEFT
			byteToChar[0xba] = (char)0x2551; //BOX DRAWINGS DOUBLE VERTICAL
			byteToChar[0xbb] = (char)0x2557; //BOX DRAWINGS DOUBLE DOWN AND LEFT
			byteToChar[0xbc] = (char)0x255d; //BOX DRAWINGS DOUBLE UP AND LEFT
			byteToChar[0xbd] = (char)0x255c; //BOX DRAWINGS UP DOUBLE AND LEFT SINGLE
			byteToChar[0xbe] = (char)0x255b; //BOX DRAWINGS UP SINGLE AND LEFT DOUBLE
			byteToChar[0xbf] = (char)0x2510; //BOX DRAWINGS LIGHT DOWN AND LEFT
			byteToChar[0xc0] = (char)0x2514; //BOX DRAWINGS LIGHT UP AND RIGHT
			byteToChar[0xc1] = (char)0x2534; //BOX DRAWINGS LIGHT UP AND HORIZONTAL
			byteToChar[0xc2] = (char)0x252c; //BOX DRAWINGS LIGHT DOWN AND HORIZONTAL
			byteToChar[0xc3] = (char)0x251c; //BOX DRAWINGS LIGHT VERTICAL AND RIGHT
			byteToChar[0xc4] = (char)0x2500; //BOX DRAWINGS LIGHT HORIZONTAL
			byteToChar[0xc5] = (char)0x253c; //BOX DRAWINGS LIGHT VERTICAL AND HORIZONTAL
			byteToChar[0xc6] = (char)0x255e; //BOX DRAWINGS VERTICAL SINGLE AND RIGHT DOUBLE
			byteToChar[0xc7] = (char)0x255f; //BOX DRAWINGS VERTICAL DOUBLE AND RIGHT SINGLE
			byteToChar[0xc8] = (char)0x255a; //BOX DRAWINGS DOUBLE UP AND RIGHT
			byteToChar[0xc9] = (char)0x2554; //BOX DRAWINGS DOUBLE DOWN AND RIGHT
			byteToChar[0xca] = (char)0x2569; //BOX DRAWINGS DOUBLE UP AND HORIZONTAL
			byteToChar[0xcb] = (char)0x2566; //BOX DRAWINGS DOUBLE DOWN AND HORIZONTAL
			byteToChar[0xcc] = (char)0x2560; //BOX DRAWINGS DOUBLE VERTICAL AND RIGHT
			byteToChar[0xcd] = (char)0x2550; //BOX DRAWINGS DOUBLE HORIZONTAL
			byteToChar[0xce] = (char)0x256c; //BOX DRAWINGS DOUBLE VERTICAL AND HORIZONTAL
			byteToChar[0xcf] = (char)0x2567; //BOX DRAWINGS UP SINGLE AND HORIZONTAL DOUBLE
			byteToChar[0xd0] = (char)0x2568; //BOX DRAWINGS UP DOUBLE AND HORIZONTAL SINGLE
			byteToChar[0xd1] = (char)0x2564; //BOX DRAWINGS DOWN SINGLE AND HORIZONTAL DOUBLE
			byteToChar[0xd2] = (char)0x2565; //BOX DRAWINGS DOWN DOUBLE AND HORIZONTAL SINGLE
			byteToChar[0xd3] = (char)0x2559; //BOX DRAWINGS UP DOUBLE AND RIGHT SINGLE
			byteToChar[0xd4] = (char)0x2558; //BOX DRAWINGS UP SINGLE AND RIGHT DOUBLE
			byteToChar[0xd5] = (char)0x2552; //BOX DRAWINGS DOWN SINGLE AND RIGHT DOUBLE
			byteToChar[0xd6] = (char)0x2553; //BOX DRAWINGS DOWN DOUBLE AND RIGHT SINGLE
			byteToChar[0xd7] = (char)0x256b; //BOX DRAWINGS VERTICAL DOUBLE AND HORIZONTAL SINGLE
			byteToChar[0xd8] = (char)0x256a; //BOX DRAWINGS VERTICAL SINGLE AND HORIZONTAL DOUBLE
			byteToChar[0xd9] = (char)0x2518; //BOX DRAWINGS LIGHT UP AND LEFT
			byteToChar[0xda] = (char)0x250c; //BOX DRAWINGS LIGHT DOWN AND RIGHT
			byteToChar[0xdb] = (char)0x2588; //FULL BLOCK
			byteToChar[0xdc] = (char)0x2584; //LOWER HALF BLOCK
			byteToChar[0xdd] = (char)0x258c; //LEFT HALF BLOCK
			byteToChar[0xde] = (char)0x2590; //RIGHT HALF BLOCK
			byteToChar[0xdf] = (char)0x2580; //UPPER HALF BLOCK
			byteToChar[0xe0] = (char)0x03b1; //GREEK SMALL LETTER ALPHA
			byteToChar[0xe1] = (char)0x00df; //LATIN SMALL LETTER SHARP S
			byteToChar[0xe2] = (char)0x0393; //GREEK CAPITAL LETTER GAMMA
			byteToChar[0xe3] = (char)0x03c0; //GREEK SMALL LETTER PI
			byteToChar[0xe4] = (char)0x03a3; //GREEK CAPITAL LETTER SIGMA
			byteToChar[0xe5] = (char)0x03c3; //GREEK SMALL LETTER SIGMA
			byteToChar[0xe6] = (char)0x00b5; //MICRO SIGN
			byteToChar[0xe7] = (char)0x03c4; //GREEK SMALL LETTER TAU
			byteToChar[0xe8] = (char)0x03a6; //GREEK CAPITAL LETTER PHI
			byteToChar[0xe9] = (char)0x0398; //GREEK CAPITAL LETTER THETA
			byteToChar[0xea] = (char)0x03a9; //GREEK CAPITAL LETTER OMEGA
			byteToChar[0xeb] = (char)0x03b4; //GREEK SMALL LETTER DELTA
			byteToChar[0xec] = (char)0x221e; //INFINITY
			byteToChar[0xed] = (char)0x03c6; //GREEK SMALL LETTER PHI
			byteToChar[0xee] = (char)0x03b5; //GREEK SMALL LETTER EPSILON
			byteToChar[0xef] = (char)0x2229; //INTERSECTION
			byteToChar[0xf0] = (char)0x2261; //IDENTICAL TO
			byteToChar[0xf1] = (char)0x00b1; //PLUS-MINUS SIGN
			byteToChar[0xf2] = (char)0x2265; //GREATER-THAN OR EQUAL TO
			byteToChar[0xf3] = (char)0x2264; //LESS-THAN OR EQUAL TO
			byteToChar[0xf4] = (char)0x2320; //TOP HALF INTEGRAL
			byteToChar[0xf5] = (char)0x2321; //BOTTOM HALF INTEGRAL
			byteToChar[0xf6] = (char)0x00f7; //DIVISION SIGN
			byteToChar[0xf7] = (char)0x2248; //ALMOST EQUAL TO
			byteToChar[0xf8] = (char)0x00b0; //DEGREE SIGN
			byteToChar[0xf9] = (char)0x2219; //BULLET OPERATOR
			byteToChar[0xfa] = (char)0x00b7; //MIDDLE DOT
			byteToChar[0xfb] = (char)0x221a; //SQUARE ROOT
			byteToChar[0xfc] = (char)0x207f; //SUPERSCRIPT LATIN SMALL LETTER N
			byteToChar[0xfd] = (char)0x00b2; //SUPERSCRIPT TWO
			byteToChar[0xfe] = (char)0x25a0; //BLACK SQUARE
			byteToChar[0xff] = (char)0x00a0; //NO-BREAK SPACE

			charToByte = new Dictionary<char, byte>();

			for (int i = 0; i < byteToChar.Length; ++i)
			{
				charToByte[byteToChar[i]] = (byte)i;
			}
		}

		public override int GetByteCount(char[] chars, int index, int count)
		{
			return count;
		}

		public override int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex)
		{
			for (int i = 0; i < charCount; ++i)
			{
				if (!charToByte.TryGetValue(chars[charIndex + i], out bytes[byteIndex]))
				{
					bytes[byteIndex + i] = 0x3F; // question mark.
				}
			}

			return charCount;
		}

		public override int GetCharCount(byte[] bytes, int index, int count)
		{
			return count;
		}

		public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
		{
			for (int i = 0; i < byteCount; ++i)
			{
				chars[charIndex + i] = byteToChar[bytes[byteIndex + i]];
			}

			return byteCount;
		}

		public override int GetMaxByteCount(int charCount)
		{
			return charCount;
		}

		public override int GetMaxCharCount(int byteCount)
		{
			return byteCount;
		}
	}
}