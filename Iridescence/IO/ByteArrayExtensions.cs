﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Read/write extensions for byte arrays.
	/// </summary>
	public static class ByteArrayExtensions
	{
		#region Common

		/// <summary>
		/// Reads a structure or formatted class from a buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public static T ReadStructure<T>(this byte[] buffer, int offset)
		{
			GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
			IntPtr addr = handle.AddrOfPinnedObject() + offset;
			T value = (T)Marshal.PtrToStructure(addr, typeof(T));
			handle.Free();
			return value;
		}

		/// <summary>
		/// Writes a structure or formatted class to a buffer.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="value"></param>
		public static void WriteStructure<T>(this byte[] buffer, int offset, T value)
		{
			GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
			IntPtr addr = handle.AddrOfPinnedObject() + offset;
			Marshal.StructureToPtr(value, addr, false);
			handle.Free();
		}
		
		#endregion

		#region Little Endian

		/// <summary>
		/// Reads a 16 bit integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16L(this byte[] buffer, int offset)
		{
			return (short)(buffer[offset] | (buffer[offset + 1] << 8));
		}
		
		/// <summary>
		/// Writes a 16 bit integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt16L(this byte[] buffer, int offset, short value)
		{
			buffer[offset] = (byte)(value);
			buffer[offset + 1] = (byte)(value >> 8);
		}

		/// <summary>
		/// Reads a 16 bit unsigned integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16L(this byte[] buffer, int offset)
		{
			return (ushort)(buffer[offset] | (buffer[offset + 1] << 8));
		}

		/// <summary>
		/// Writes a 16 bit unsigned integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt16L(this byte[] buffer, int offset, ushort value)
		{
			buffer[offset] = (byte)(value);
			buffer[offset + 1] = (byte)(value >> 8);
		}
		
		/// <summary>
		/// Reads a 32 bit integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32L(this byte[] buffer, int offset)
		{
			return buffer[offset] | (buffer[offset + 1] << 8) | (buffer[offset + 2] << 16) | (buffer[offset + 3] << 24);
		}
		
		/// <summary>
		/// Writes a 32 bit integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt32L(this byte[] buffer, int offset, int value)
		{
			buffer[offset] = (byte)(value);
			buffer[offset + 1] = (byte)(value >> 8);
			buffer[offset + 2] = (byte)(value >> 16);
			buffer[offset + 3] = (byte)(value >> 24);
		}
		
		/// <summary>
		/// Reads a 32 bit unsigned integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32L(this byte[] buffer, int offset)
		{
			return (uint)buffer[offset] | ((uint)buffer[offset + 1] << 8) | ((uint)buffer[offset + 2] << 16) | ((uint)buffer[offset + 3] << 24);
		}
		
		/// <summary>
		/// Writes a 32 bit unsigned integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt32L(this byte[] buffer, int offset, uint value)
		{
			buffer[offset] = (byte)(value);
			buffer[offset + 1] = (byte)(value >> 8);
			buffer[offset + 2] = (byte)(value >> 16);
			buffer[offset + 3] = (byte)(value >> 24);
		}

		/// <summary>
		/// Reads a 64 bit integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64L(this byte[] buffer, int offset)
		{
			return (long)buffer[offset] | ((long)buffer[offset + 1] << 8) | ((long)buffer[offset + 2] << 16) | ((long)buffer[offset + 3] << 24) | ((long)buffer[offset + 4] << 32) | ((long)buffer[offset + 5] << 40) | ((long)buffer[offset + 6] << 48) | ((long)buffer[offset + 7] << 56);
		}
		
		/// <summary>
		/// Writes a 64 bit integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt64L(this byte[] buffer, int offset, long value)
		{
			buffer[offset] = (byte)(value);
			buffer[offset + 1] = (byte)(value >> 8);
			buffer[offset + 2] = (byte)(value >> 16);
			buffer[offset + 3] = (byte)(value >> 24);
			buffer[offset + 4] = (byte)(value >> 32);
			buffer[offset + 5] = (byte)(value >> 40);
			buffer[offset + 6] = (byte)(value >> 48);
			buffer[offset + 7] = (byte)(value >> 56);
		}
		
		/// <summary>
		/// Reads a 64 bit unsigned integer from a byte array.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64L(this byte[] buffer, int offset)
		{
			return (ulong)buffer[offset] | ((ulong)buffer[offset + 1] << 8) | ((ulong)buffer[offset + 2] << 16) | ((ulong)buffer[offset + 3] << 24) | ((ulong)buffer[offset + 4] << 32) | ((ulong)buffer[offset + 5] << 40) | ((ulong)buffer[offset + 6] << 48) | ((ulong)buffer[offset + 7] << 56);
		}
		
		/// <summary>
		/// Writes a 64 bit unsigned integer into a byte array.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt64L(this byte[] buffer, int offset, ulong value)
		{
			buffer[offset] = (byte)(value);
			buffer[offset + 1] = (byte)(value >> 8);
			buffer[offset + 2] = (byte)(value >> 16);
			buffer[offset + 3] = (byte)(value >> 24);
			buffer[offset + 4] = (byte)(value >> 32);
			buffer[offset + 5] = (byte)(value >> 40);
			buffer[offset + 6] = (byte)(value >> 48);
			buffer[offset + 7] = (byte)(value >> 56);
		}
		
		/// <summary>
		/// Reads a 16 bit floating point value from a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static Half ReadFloat16L(this byte[] buffer, int offset)
		{
			return PrimitiveUtility.BitsToHalf(buffer.ReadUInt16L(offset));
		}

		/// <summary>
		/// Writes a 16 bit floating point value into a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat16L(this byte[] buffer, int offset, Half value)
		{
			buffer.WriteUInt16L(offset, PrimitiveUtility.HalfToBits(value));
		}
		
		/// <summary>
		/// Reads a 32 bit floating point value from a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static float ReadFloat32L(this byte[] buffer, int offset)
		{
			if (BitConverter.IsLittleEndian)
				return BitConverter.ToSingle(buffer, offset);

			// reverse.
			byte[] temp = new byte[4];
			temp[0] = buffer[offset + 3];
			temp[1] = buffer[offset + 2];
			temp[2] = buffer[offset + 1];
			temp[3] = buffer[offset];
			return BitConverter.ToSingle(temp, 0);
		}
		
		/// <summary>
		/// Writes a 32 bit floating point value into a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat32L(this byte[] buffer, int offset, float value)
		{
			byte[] temp = BitConverter.GetBytes(value);

			if (BitConverter.IsLittleEndian)
			{
				buffer[offset] = temp[0];
				buffer[offset + 1] = temp[1];
				buffer[offset + 2] = temp[2];
				buffer[offset + 3] = temp[3];
			}
			else
			{
				buffer[offset] = temp[3];
				buffer[offset + 1] = temp[2];
				buffer[offset + 2] = temp[1];
				buffer[offset + 3] = temp[0];
			}
		}
		
		/// <summary>
		/// Reads a 64 bit floating point value from a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static double ReadFloat64L(this byte[] buffer, int offset)
		{
			if (BitConverter.IsLittleEndian)
				return BitConverter.ToDouble(buffer, offset);

			// reverse.
			byte[] temp = new byte[8];
			temp[0] = buffer[offset + 7];
			temp[1] = buffer[offset + 6];
			temp[2] = buffer[offset + 5];
			temp[3] = buffer[offset + 4];
			temp[4] = buffer[offset + 3];
			temp[5] = buffer[offset + 2];
			temp[6] = buffer[offset + 1];
			temp[7] = buffer[offset];
			return BitConverter.ToDouble(temp, 0);
		}

		/// <summary>
		/// Writes a 64 bit floating point value into a byte array in little-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat64L(this byte[] buffer, int offset, double value)
		{
			byte[] temp = BitConverter.GetBytes(value);

			if (BitConverter.IsLittleEndian)
			{
				buffer[offset] = temp[0];
				buffer[offset + 1] = temp[1];
				buffer[offset + 2] = temp[2];
				buffer[offset + 3] = temp[3];
				buffer[offset + 4] = temp[4];
				buffer[offset + 5] = temp[5];
				buffer[offset + 6] = temp[6];
				buffer[offset + 7] = temp[7];
			}
			else
			{
				buffer[offset] = temp[7];
				buffer[offset + 1] = temp[6];
				buffer[offset + 2] = temp[5];
				buffer[offset + 3] = temp[4];
				buffer[offset + 4] = temp[3];
				buffer[offset + 5] = temp[2];
				buffer[offset + 6] = temp[1];
				buffer[offset + 7] = temp[0];
			}
		}
		
		#endregion

		#region Big Endian

		/// <summary>
		/// Reads a 16 bit integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16B(this byte[] buffer, int offset)
		{
			return (short)(buffer[offset + 1] | (buffer[offset] << 8));
		}
		
		/// <summary>
		/// Writes a 16 bit integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt16B(this byte[] buffer, int offset, short value)
		{
			buffer[offset + 1] = (byte)(value);
			buffer[offset] = (byte)(value >> 8);
		}
		
		/// <summary>
		/// Reads a 16 bit unsigned integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16B(this byte[] buffer, int offset)
		{
			return (ushort)(buffer[offset + 1] | (buffer[offset + 0] << 8));
		}


		/// <summary>
		/// Writes a 16 bit unsigned integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt16B(this byte[] buffer, int offset, ushort value)
		{
			buffer[offset + 1] = (byte)(value);
			buffer[offset] = (byte)(value >> 8);
		}

		/// <summary>
		/// Reads a 32 bit integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32B(this byte[] buffer, int offset)
		{
			return buffer[offset + 3] | (buffer[offset + 2] << 8) | (buffer[offset + 1] << 16) | (buffer[offset] << 24);
		}
		
		/// <summary>
		/// Writes a 32 bit integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt32B(this byte[] buffer, int offset, int value)
		{
			buffer[offset + 3] = (byte)(value);
			buffer[offset + 2] = (byte)(value >> 8);
			buffer[offset + 1] = (byte)(value >> 16);
			buffer[offset] = (byte)(value >> 24);
		}
		
		/// <summary>
		/// Reads a 32 bit unsigned integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32B(this byte[] buffer, int offset)
		{
			return (uint)buffer[offset + 3] | ((uint)buffer[offset + 2] << 8) | ((uint)buffer[offset + 1] << 16) | ((uint)buffer[offset] << 24);
		}
		
		/// <summary>
		/// Writes a 32 bit unsigned integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt32B(this byte[] buffer, int offset, uint value)
		{
			buffer[offset + 3] = (byte)(value);
			buffer[offset + 2] = (byte)(value >> 8);
			buffer[offset + 1] = (byte)(value >> 16);
			buffer[offset] = (byte)(value >> 24);
		}
		
		/// <summary>
		/// Reads a 64 bit integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64B(this byte[] buffer, int offset)
		{
			return (long)buffer[offset + 7] | ((long)buffer[offset + 6] << 8) | ((long)buffer[offset + 5] << 16) | ((long)buffer[offset + 4] << 24) | ((long)buffer[offset + 3] << 32) | ((long)buffer[offset + 2] << 40) | ((long)buffer[offset + 1] << 48) | ((long)buffer[offset] << 56);
		}
		
		/// <summary>
		/// Writes a 64 bit integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteInt64B(this byte[] buffer, int offset, long value)
		{
			buffer[offset + 7] = (byte)(value);
			buffer[offset + 6] = (byte)(value >> 8);
			buffer[offset + 5] = (byte)(value >> 16);
			buffer[offset + 4] = (byte)(value >> 24);
			buffer[offset + 3] = (byte)(value >> 32);
			buffer[offset + 2] = (byte)(value >> 40);
			buffer[offset + 1] = (byte)(value >> 48);
			buffer[offset] = (byte)(value >> 56);
		}

		/// <summary>
		/// Reads a 64 bit unsigned integer from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64B(this byte[] buffer, int offset)
		{
			return (ulong)buffer[offset + 7] | ((ulong)buffer[offset + 6] << 8) | ((ulong)buffer[offset + 5] << 16) | ((ulong)buffer[offset + 4] << 24) | ((ulong)buffer[offset + 3] << 32) | ((ulong)buffer[offset + 2] << 40) | ((ulong)buffer[offset + 1] << 48) | ((ulong)buffer[offset] << 56);
		}
		
		/// <summary>
		/// Writes a 64 bit unsigned integer into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteUInt64B(this byte[] buffer, int offset, ulong value)
		{
			buffer[offset + 7] = (byte)(value);
			buffer[offset + 6] = (byte)(value >> 8);
			buffer[offset + 5] = (byte)(value >> 16);
			buffer[offset + 4] = (byte)(value >> 24);
			buffer[offset + 3] = (byte)(value >> 32);
			buffer[offset + 2] = (byte)(value >> 40);
			buffer[offset + 1] = (byte)(value >> 48);
			buffer[offset] = (byte)(value >> 56);
		}

		/// <summary>
		/// Reads a 16 bit floating point value from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static Half ReadFloat16B(this byte[] buffer, int offset)
		{
			return PrimitiveUtility.BitsToHalf(buffer.ReadUInt16B(offset));
		}

		/// <summary>
		/// Writes a 16 bit floating point value into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat16B(this byte[] buffer, int offset, Half value)
		{
			buffer.WriteUInt16B(offset, PrimitiveUtility.HalfToBits(value));
		}
		
		/// <summary>
		/// Reads a 32 bit floating point value from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static float ReadFloat32B(this byte[] buffer, int offset)
		{
			if (!BitConverter.IsLittleEndian)
				return BitConverter.ToSingle(buffer, offset);

			// reverse.
			byte[] temp = new byte[4];
			temp[0] = buffer[offset + 3];
			temp[1] = buffer[offset + 2];
			temp[2] = buffer[offset + 1];
			temp[3] = buffer[offset];
			return BitConverter.ToSingle(temp, 0);
		}
		
		/// <summary>
		/// Writes a 32 bit floating point value into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat32B(this byte[] buffer, int offset, float value)
		{
			byte[] temp = BitConverter.GetBytes(value);

			if (!BitConverter.IsLittleEndian)
			{
				buffer[offset] = temp[0];
				buffer[offset + 1] = temp[1];
				buffer[offset + 2] = temp[2];
				buffer[offset + 3] = temp[3];
			}
			else
			{
				buffer[offset] = temp[3];
				buffer[offset + 1] = temp[2];
				buffer[offset + 2] = temp[1];
				buffer[offset + 3] = temp[0];
			}
		}
	
		/// <summary>
		/// Reads a 64 bit floating point value from a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The source byte array.</param>
		/// <param name="offset">The offset from which reading starts.</param>
		/// <returns>The value.</returns>
		public static double ReadFloat64B(this byte[] buffer, int offset)
		{
			if (!BitConverter.IsLittleEndian)
				return BitConverter.ToDouble(buffer, offset);
			
			// reverse.
			byte[] temp = new byte[8];
			temp[0] = buffer[offset + 7];
			temp[1] = buffer[offset + 6];
			temp[2] = buffer[offset + 5];
			temp[3] = buffer[offset + 4];
			temp[4] = buffer[offset + 3];
			temp[5] = buffer[offset + 2];
			temp[6] = buffer[offset + 1];
			temp[7] = buffer[offset];
			return BitConverter.ToDouble(temp, 0);
		}

		/// <summary>
		/// Writes a 64 bit floating point value into a byte array in big-endian byte order.
		/// </summary>
		/// <param name="buffer">The destination byte array.</param>
		/// <param name="offset">The offset at which writing starts.</param>
		/// <param name="value">The value to write</param>
		public static void WriteFloat64B(this byte[] buffer, int offset, double value)
		{
			byte[] temp = BitConverter.GetBytes(value);

			if (!BitConverter.IsLittleEndian)
			{
				buffer[offset] = temp[0];
				buffer[offset + 1] = temp[1];
				buffer[offset + 2] = temp[2];
				buffer[offset + 3] = temp[3];
				buffer[offset + 4] = temp[4];
				buffer[offset + 5] = temp[5];
				buffer[offset + 6] = temp[6];
				buffer[offset + 7] = temp[7];
			}
			else
			{
				buffer[offset] = temp[7];
				buffer[offset + 1] = temp[6];
				buffer[offset + 2] = temp[5];
				buffer[offset + 3] = temp[4];
				buffer[offset + 4] = temp[3];
				buffer[offset + 5] = temp[2];
				buffer[offset + 6] = temp[1];
				buffer[offset + 7] = temp[0];
			}
		}

		#endregion
	}
}
