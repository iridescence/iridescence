﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Represents a sliding stream. It behaves similar to a queue where data written to it can be read using the FIFO-method.
	/// </summary>
	public class SlidingStream : Stream
	{
		#region Fields

		private readonly object mutex;
		private readonly LinkedList<byte[]> buffers;
		private readonly ManualResetEventSlim writeEvent;
		private readonly long maxLength;
		private readonly int segmentSize;
		private long currentLength;
		private bool disposed;
		private bool completed;
		
		private int writePosition;
		private int readPosition;

		#endregion

		#region Properties

		/// <inheritdoc/>
		public override bool CanRead => true;

		/// <inheritdoc/>
		public override bool CanWrite => !this.completed;

		/// <inheritdoc/>
		public override bool CanSeek => false;

		/// <inheritdoc/>
		public override long Length => throw new NotSupportedException();

		/// <inheritdoc/>
		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		public bool DataAvailable
		{
			get
			{
				lock (this.mutex)
				{
					if (this.disposed)
						throw new ObjectDisposedException(nameof(SlidingStream));

					return this.currentLength > 0;
				}
			}
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="SlidingStream"/>.
		/// </summary>
		/// <param name="maxLength">The maximum number of bytes in the buffer.</param>
		/// <param name="segmentSize">The size of the chunks.</param>
		public SlidingStream(long maxLength = 0, int segmentSize = 4096)
		{
			this.mutex = new object();
			this.segmentSize = segmentSize;
			this.buffers = new LinkedList<byte[]>();
			this.buffers.AddFirst(new byte[segmentSize]);
			this.maxLength = maxLength;
			this.writeEvent = new ManualResetEventSlim(false);
		}

		#endregion

		#region Methods

		/// <inheritdoc/>
		public override void Flush()
		{
			
		}

		/// <inheritdoc/>
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (count == 0)
				return 0;

			for (;;)
			{
				lock (this.mutex)
				{
					if (this.disposed)
						throw new ObjectDisposedException(nameof(SlidingStream));
					
					if (this.currentLength > 0)
					{
						if (count > this.currentLength)
							count = (int)this.currentLength;

						this.currentLength -= count;

						int remaining = count;

						while (remaining > 0)
						{
							int numBytes = remaining;
							if (numBytes > this.segmentSize - this.readPosition)
								numBytes = this.segmentSize - this.readPosition;

							Array.Copy(this.buffers.First.Value, this.readPosition, buffer, offset, numBytes);
							remaining -= numBytes;
							offset += numBytes;
							this.readPosition += numBytes;

							if (this.readPosition == this.segmentSize)
							{
								this.buffers.RemoveFirst();
								this.readPosition = 0;
							}
						}

						return count;
					}

					if (this.completed)
					{
						return 0;
					}

					this.writeEvent.Reset();
				}

				this.writeEvent.Wait();
			}
		}

		/// <inheritdoc/>
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));

			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (count == 0)
				return;

			lock (this.mutex)
			{
				if (this.disposed)
					throw new ObjectDisposedException(nameof(SlidingStream));

				if (this.completed)
					throw new InvalidOperationException("The sliding stream is completed and can not be written.");
				
				if (this.maxLength > 0 && this.currentLength + count > this.maxLength)
					throw new InvalidOperationException("The stream would exceed the maximum size.");

				this.currentLength += count;

				while (count > 0)
				{
					int numBytes = count;
					if (numBytes > this.segmentSize - this.writePosition)
						numBytes = this.segmentSize - this.writePosition;

					Array.Copy(buffer, offset, this.buffers.Last.Value, this.writePosition, numBytes);
					count -= numBytes;
					offset += numBytes;
					this.writePosition += numBytes;

					if (this.writePosition == this.segmentSize)
					{
						this.buffers.AddLast(new byte[this.segmentSize]);
						this.writePosition = 0;
					}
				}

				this.writeEvent.Set();
			}
		}

		/// <summary>
		/// Completes the <see cref="SlidingStream"/>.
		/// After completion, no more data can be written to the stream, but the data already buffered can be still be read until the stream ends.
		/// </summary>
		public void Complete()
		{
			lock (this.mutex)
			{
				if (this.disposed)
					throw new ObjectDisposedException(nameof(SlidingStream));

				this.completed = true;
			}
		}

		/// <inheritdoc/>
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing)
			{
				this.disposed = true;
				this.writeEvent.Set();
			}
		}

		#endregion
	}
}
