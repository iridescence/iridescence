﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Implements bit-based writing operations for byte streams.
	/// </summary>
	public class BitWriter
	{
		#region Fields

		private const int registerBits = 64;
		private ulong value;
		private int shift;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the base stream.
		/// </summary>
		public Stream Stream { get; }

		/// <summary>
		/// Gets a value that indicates whether the bit writer is currently aligned and has no cached bits.
		/// </summary>
		public bool IsAligned => this.shift == 0;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BitWriter.
		/// </summary>
		public BitWriter(Stream stream)
		{
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));

			if(!stream.CanWrite)
				throw new ArgumentException("Stream is not writable.", nameof(stream));

			this.Stream = stream;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Writes all cached bytes that can be written. This does not align the writer, i.e. at most one byte is retained.
		/// </summary>
		public void Flush()
		{
			while (this.shift >= 8)
			{
				this.Stream.WriteByte((byte)this.value);
				this.value >>= 8;
				this.shift -= 8;
			}
		}

		/// <summary>
		/// Flushes all bits that have not been written to the underlying stream and byte-aligns the writer.
		/// </summary>
		public void Align()
		{
			while (this.shift > 0)
			{
				this.Stream.WriteByte((byte)this.value);
				this.value >>= 8;
				this.shift -= 8;
			}
			this.shift = 0;
		}
		
		/// <summary>
		/// Writes a single bit.
		/// </summary>
		/// <param name="bit"></param>
		public void WriteBit(int bit)
		{
			if (bit != 0)
			{
				this.value |= (1ul << this.shift);
			}

			// flush if the register is full.
			++this.shift;
			if (this.shift == registerBits)
				this.Flush();
		}

		/// <summary>
		/// Writes the specified number of bits.
		/// </summary>
		/// <param name="bits"></param>
		/// <param name="numBits"></param>
		public void WriteBits(ulong bits, int numBits)
		{
			if (numBits > 64 || numBits < 0)
				throw new ArgumentException("Invalid number of bits.");

			if (this.shift + numBits <= registerBits)
			{
				// bits fit in the register.
				ulong masked = bits & ((1ul << numBits) - 1);
				this.value |= (masked << this.shift);

				// flush if the register is full.
				this.shift += numBits;
				if (this.shift == registerBits)
					this.Flush();
			}
			else
			{
				// bits don't fit, we need to write chunks.
				while (numBits > 0)
				{
					int count = registerBits - this.shift; // the number of bits that fit into the register.
					if (count > numBits)
						count = numBits;

					ulong masked = bits & ((1ul << count) - 1);
					this.value |= (masked << this.shift);

					numBits -= count;
					bits >>= count;

					// flush if the register is full.
					this.shift += count;
					if (this.shift == registerBits)
						this.Flush();
				}
			}
		}

		/// <summary>
		/// Writes n 1 bits followed by one 0 bit.
		/// </summary>
		/// <param name="n"></param>
		public void WriteUnary(ulong n)
		{
			while (n > 0)
			{
				int count = n > 64 ? 64 : (int)n;
				this.WriteBits(ulong.MaxValue, count);
				n -= (uint)count;
			}
			this.WriteBit(0);
		}

		/// <summary>
		/// Writes a Golomb-Rice-coded value.
		/// </summary>
		/// <param name="value">The value to write.</param>
		/// <param name="p">The rice parameter.</param>
		public void WriteGolombRice(ulong value, int p)
		{
			ulong q = value >> p;
			ulong r = value & ((1ul << p) - 1);
			this.WriteUnary(q);
			this.WriteBits(r, p);
		}

		/// <summary>
		/// Writes a variable-length integer.
		/// </summary>
		/// <param name="value">The value to write.</param>
		/// <param name="maxBits">The maximum number of bits for the value.</param>
		public void WriteVUInt(ulong value, int maxBits)
		{
			if (maxBits < 1 || maxBits > 64)
				throw new ArgumentOutOfRangeException(nameof(maxBits));

			int numBits = 8;

			while (value >= (1ul << numBits))
			{
				this.WriteBit(1);
				numBits += 8;

				if (numBits >= maxBits)
				{
					numBits = maxBits;
					break;
				}
			}

			if(numBits < maxBits)
				this.WriteBit(0);

			this.WriteBits(value, numBits);
		}

		/// <summary>
		/// Writes a variable-length integer.
		/// </summary>
		/// <param name="value">The value to write.</param>
		/// <param name="maxBits">The maximum number of bits for the value.</param>
		public void WriteVInt(long value, int maxBits)
		{
			this.WriteVUInt((ulong)value, maxBits);
		}

		#endregion
	}
}
