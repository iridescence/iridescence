﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Represents a CRC-32 stream that progressively calculates the CRC-32 check sum of the data that is being read or written.
	/// </summary>
	public class Crc32Stream : Stream
	{
		#region Fields

		private readonly Crc32 crc;
		private readonly Stream stream;
		private uint checksum;
		private bool enable;
		private readonly bool leaveOpen;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the current hash.
		/// </summary>
		public uint Checksum => this.checksum ^ this.crc.Mask;

		/// <summary>
		/// Toggles the checksum calculation on or off.
		/// </summary>
		public bool Enable
		{
			get => this.enable;
			set => this.enable = value;
		}

		public override bool CanRead
		{
			get
			{
				if (this.stream == null)
					return false;
				return this.stream.CanRead;
			}
		}

		public override bool CanSeek
		{
			get
			{
				if (this.stream == null)
					return false;
				return this.stream.CanSeek;
			}
		}

		public override bool CanWrite
		{
			get
			{
				if (this.stream == null)
					return true;
				return this.stream.CanWrite;
			}
		}

		public override long Length
		{
			get
			{
				if (this.stream == null)
					throw new NotSupportedException();
				return this.stream.Length;
			}
		}

		public override long Position
		{
			get
			{
				if (this.stream == null)
					throw new NotSupportedException();
				return this.stream.Position;
			}
			set
			{
				if (this.stream == null)
					throw new NotSupportedException();
				this.stream.Position = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Crc32Stream.
		/// </summary>
		public Crc32Stream(Crc32 crc, Stream stream = null, bool leaveOpen = false)
		{
			this.crc = crc;
			this.stream = stream;
			this.enable = true;
			this.leaveOpen = leaveOpen;

			this.ResetChecksum();
		}

		#endregion

		#region Methods

		public void ResetChecksum()
		{
			this.checksum = this.crc.InitialValue;
		}

		public override void Flush()
		{
			this.stream?.Flush();
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing)
			{
				if (!this.leaveOpen)
					this.stream.Dispose();
			}
		}

		public override int ReadByte()
		{
			if (this.stream == null)
				throw new NotSupportedException();

			int result = this.stream.ReadByte();
			if (result >= 0)
				this.checksum = (this.checksum >> 8) ^ this.crc.Table[(uint)result ^ (this.checksum & 0xFF)];

			return result;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (this.stream == null)
				throw new NotSupportedException();
			
			int numBytes = this.stream.Read(buffer, offset, count);

			if (this.enable)
			{
				this.crc.ComputeChecksum(ref this.checksum, buffer.ToSpan(offset, count));
			}

			return numBytes;
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (this.enable)
			{
				this.crc.ComputeChecksum(ref this.checksum, buffer.ToSpan(offset, count));
			}

			this.stream?.Write(buffer, offset, count);
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			if (this.stream == null)
				throw new NotSupportedException();
			return this.stream.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			if (this.stream == null)
				throw new NotSupportedException();
			this.stream.SetLength(value);
		}

		#endregion
	}
}
