﻿using System.IO;
using System.Text;

namespace Iridescence
{
	public static partial class StreamExtensions
	{
		#region 16

		/// <summary>
		/// Reads a 7-bit variable-length encoded 16-bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16V(this Stream stream)
		{
			return (short)stream.ReadUInt16V();
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 16-bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16V(this Stream stream)
		{
			byte input = stream.ReadByteOrThrow();
			if (input <= 0x7F)
				return input;

			byte input2 = stream.ReadByteOrThrow();
			if (input2 <= 0x7F)
				return (ushort)((input & 0x7F) | (input2 << 7));

			byte input3 = stream.ReadByteOrThrow();
			if (input3 > 3)
				throw new InvalidDataException();

			return (ushort)((input & 0x7F) | (input2 << 7) | (input3 << 14));
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 16-bit signed integer from a stream with support for an end-of-stream condition.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <param name="value">The value.</param>
		/// <returns>True, if a value was read. False if the stream ended before the value was read.</returns>
		public static bool TryReadInt16V(this Stream stream, out short value)
		{
			if (!stream.TryReadUInt16V(out ushort unsignedValue))
			{
				value = 0;
				return false;
			}

			value = unchecked((short)unsignedValue);
			return true;
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 16-bit unsigned integer from a stream with support for an end-of-stream condition.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <param name="value">The value.</param>
		/// <returns>True, if a value was read. False if the stream ended before the value was read.</returns>
		public static bool TryReadUInt16V(this Stream stream, out ushort value)
		{
			int input = stream.ReadByte();
			if (input < 0)
			{
				value = 0;
				return false;
			}
			
			if (input <= 0x7F)
			{
				value = (ushort)input;
				return true;
			}
			
			byte input2 = stream.ReadByteOrThrow();
			if (input2 <= 0x7F)
			{
				value = (ushort)((input & 0x7F) | (input2 << 7));
				return true;
			}

			byte input3 = stream.ReadByteOrThrow();
			if (input3 > 3)
				throw new InvalidDataException();
			
			value = (ushort)((input & 0x7F) | (input2 << 7) | (input3 << 14));
			return true;
		}

		/// <summary>
		/// Writes a 7-bit variable length encoded 16-bit integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt16V(this Stream stream, short value)
		{
			stream.WriteUInt16V((ushort)value);
		}

		/// <summary>
		/// Writes a 7-bit variable length encoded 16-bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt16V(this Stream stream, ushort value)
		{
			if (value <= 0x7F)
			{
				stream.WriteByte((byte)value);
			}
			else if (value <= 0x3FFF)
			{
				stream.WriteByte((byte)((value & 0x7F) | 0x80));
				stream.WriteByte((byte)(value >> 7));
			}
			else
			{
				stream.WriteByte((byte)((value & 0x7F) | 0x80));
				stream.WriteByte((byte)(((value >> 7) & 0x7F) | 0x80));
				stream.WriteByte((byte)(value >> 14));
			}
		}

		#endregion

		#region 32

		/// <summary>
		/// Reads a 7-bit variable-length encoded 32-bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32V(this Stream stream)
		{
			return (int)stream.ReadUInt32V();
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 32-bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32V(this Stream stream)
		{
			uint value = 0;
			int shift = 0;

			for(;;)
			{
				byte input = stream.ReadByteOrThrow();

				value |= ((byte)input & 0x7Fu) << shift;
				if ((input & 0x80) == 0)
					break;

				if(shift >= 32)
					throw new InvalidDataException("Invalid variable-length coded integer.");

				shift += 7;
			}

			return value;
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 32-bit signed integer from a stream with support for an end-of-stream condition.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <param name="value">The value.</param>
		/// <returns>True, if a value was read. False if the stream ended before the value was read.</returns>
		public static bool TryReadInt32V(this Stream stream, out int value)
		{
			if (!stream.TryReadUInt32V(out uint unsignedValue))
			{
				value = 0;
				return false;
			}

			value = unchecked((int)unsignedValue);
			return true;
		}
		
		/// <summary>
		/// Reads a 7-bit variable-length encoded 32-bit unsigned integer from a stream with support for an end-of-stream condition.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <param name="value">The value.</param>
		/// <returns>True, if a value was read. False if the stream ended before the value was read.</returns>
		public static bool TryReadUInt32V(this Stream stream, out uint value)
		{
			value = 0;
			int shift = 0;

			for(;;)
			{
				byte input;

				if (shift == 0)
				{
					int firstByte = stream.ReadByte();
					if (firstByte < 0)
						return false;
					input = (byte)firstByte;
				}
				else
				{
					input = stream.ReadByteOrThrow();
				}

				value |= ((byte)input & 0x7Fu) << shift;
				if ((input & 0x80) == 0)
					break;

				if(shift >= 32)
					throw new InvalidDataException("Invalid variable-length coded integer.");

				shift += 7;
			}

			return true;
		}

		/// <summary>
		/// Writes a 7-bit variable length encoded 32-bit integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt32V(this Stream stream, int value)
		{
			stream.WriteUInt32V((uint)value);
		}
		
		/// <summary>
		/// Writes a 7-bit variable length encoded 32-bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt32V(this Stream stream, uint value)
		{
			do
			{
				byte current = (byte)(value & 0x7F);
				value >>= 7;

				if (value > 0)
					current |= 0x80;

				stream.WriteByte(current);
			} while (value > 0);
		}
		
		#endregion

		#region 64

		/// <summary>
		/// Reads a 7-bit variable-length encoded 64-bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64V(this Stream stream)
		{
			return (long)stream.ReadUInt64V();
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 64-bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64V(this Stream stream)
		{
			ulong value = 0;
			int shift = 0;

			for (;;)
			{
				byte input = stream.ReadByteOrThrow();

				if (shift == 56)
				{
					value |= (ulong)input << 56;
					break;
				}

				value |= (input & 0x7Ful) << shift;

				if ((input & 0x80) == 0)
					break;

				shift += 7;
			}

			return value;
		}

		/// <summary>
		/// Reads a 7-bit variable-length encoded 64-bit signed integer from a stream with support for an end-of-stream condition.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <param name="value">The value.</param>
		/// <returns>True, if a value was read. False if the stream ended before the value was read.</returns>
		public static bool TryReadInt64V(this Stream stream, out long value)
		{
			if (!stream.TryReadUInt64V(out ulong unsignedValue))
			{
				value = 0;
				return false;
			}

			value = unchecked((long)unsignedValue);
			return true;
		}
		
		/// <summary>
		/// Reads a 7-bit variable-length encoded 64-bit unsigned integer from a stream with support for an end-of-stream condition.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <param name="value">The value.</param>
		/// <returns>True, if a value was read. False if the stream ended before the value was read.</returns>
		public static bool TryReadUInt64V(this Stream stream, out ulong value)
		{
			value = 0;
			int shift = 0;

			for(;;)
			{
				byte input;

				if (shift == 0)
				{
					int firstByte = stream.ReadByte();
					if (firstByte < 0)
						return false;
					input = (byte)firstByte;
				}
				else
				{
					input = stream.ReadByteOrThrow();
				}

				if (shift == 56)
				{
					value |= (ulong)input << 56;
					break;
				}

				value |= (input & 0x7Ful) << shift;

				if ((input & 0x80) == 0)
					break;

				shift += 7;
			}

			return true;
		}
		
		/// <summary>
		/// Reads a 64-bit integer encoded with a special 6/7-bit variable length code for signed integers.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64VS(this Stream stream)
		{
			ulong v = stream.ReadByteOrThrow();

			bool neg = (v & 0x40) != 0;

			if ((v & 0x80) == 0)
				return neg
					? -(long)(v & 0x3Ful) - 1L
					: (long)(v & 0x3Ful);

			int shift = 6;
			v &= 0x3Ful;

			for (;;)
			{
				byte input = stream.ReadByteOrThrow();

				v |= (input & 0x7Ful) << shift;
				if ((input & 0x80) == 0)
					break;

				if (shift >= 63)
					throw new InvalidDataException("Invalid variable-length coded integer.");

				shift += 7;
			}

			return neg
				? -(long)v - 1
				: (long)v;
		}

		/// <summary>
		/// Reads a 64-bit integer encoded with a special 6/7-bit variable length code for signed integers with support for an end-of-stream condition.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <param name="value">The value.</param>
		/// <returns>True, if a value was read. False if the stream ended before the value was read.</returns>
		public static bool TryReadInt64VS(this Stream stream, out long value)
		{
			int firstByte = stream.ReadByte();
			if (firstByte < 0)
			{
				value = 0;
				return false;
			}

			ulong v = (ulong)firstByte;

			bool neg = (v & 0x40) != 0;

			if ((v & 0x80) == 0)
			{
				value = neg
					? -(long)(v & 0x3Ful) - 1L
					: (long)(v & 0x3Ful);
				return true;
			}

			int shift = 6;
			v &= 0x3Ful;

			for (;;)
			{
				byte input = stream.ReadByteOrThrow();

				v |= (input & 0x7Ful) << shift;
				if ((input & 0x80) == 0)
					break;

				if (shift >= 63)
					throw new InvalidDataException("Invalid variable-length coded integer.");

				shift += 7;
			}

			value = neg
				? -(long)v - 1
				: (long)v;
			return true;
		}

		/// <summary>
		/// Writes a 7-bit variable length encoded 64-bit integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt64V(this Stream stream, long value)
		{
			stream.WriteUInt64V((ulong)value);
		}

		/// <summary>
		/// Writes a 7-bit variable length encoded 64-bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The desination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt64V(this Stream stream, ulong value)
		{
			for(int i = 0; i < 8; i++)
			{
				byte current = (byte)(value & 0x7F);
				value >>= 7;

				if (value > 0)
					current |= 0x80;

				stream.WriteByte(current);

				if (value == 0)
					return;
			}

			stream.WriteByte((byte)value);
		}

		/// <summary>
		/// Writes a 64-bit integer encoded with a special 6/7-bit variable length encoding for signed integers.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteInt64VS(this Stream stream, long value)
		{
			ulong unsigned;
			if (value < 0)
			{
				unsigned = (ulong)value;
				unsigned ^= ulong.MaxValue;

				if (unsigned > 0x3Ful)
					stream.WriteByte((byte)(0x40ul | 0x80ul | (unsigned & 0x3Ful)));
				else
					stream.WriteByte((byte)(0x40ul | (unsigned & 0x3Ful)));
			}
			else
			{
				unsigned = (ulong)value;
				if (value > 0x3F)
					stream.WriteByte((byte)(0x80ul | (unsigned & 0x3Ful)));
				else
					stream.WriteByte((byte)(unsigned & 0x3Ful));
			}

			unsigned >>= 6;

			while (unsigned > 0)
			{
				byte current = (byte)(unsigned & 0x7Ful);
				unsigned >>= 7;

				if (unsigned > 0)
					current |= 0x80;

				stream.WriteByte(current);
			}
		}

		#endregion

		#region String

		/// <summary>
		/// Reads a string from a stream, prefixed with a variable length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadStringV(this Stream stream)
		{
			return stream.ReadStringV(DefaultEncoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a variable length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringV(this Stream stream, Encoding encoding)
		{
			int length = stream.ReadInt32V();

			if (length < 0)
				return null;

			return stream.ReadString(length, encoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a variable length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="maxLength">The maximum length of the string (in bytes).</param>
		/// <returns></returns>
		public static string ReadStringV(this Stream stream, int maxLength)
		{
			return stream.ReadStringV(maxLength, DefaultEncoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a variable length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="maxLength">The maximum length of the string (in bytes).</param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringV(this Stream stream, int maxLength, Encoding encoding)
		{
			int length = stream.ReadInt32V();

			if (length < 0)
				return null;

			if (length > maxLength)
				throw new LimitExceededException();

			return stream.ReadString(length, encoding);
		}

		/// <summary>
		/// Writes a string to a stream, prefixed with a variable-length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStringV(this Stream stream, string value)
		{
			stream.WriteStringV(value, DefaultEncoding);
		}

		/// <summary>
		/// Writes a string encoded with the specified encoding to a stream, prefixed with a variable length integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		/// <param name="encoding"></param>
		public static void WriteStringV(this Stream stream, string value, Encoding encoding)
		{
			if (value == null)
			{
				stream.WriteInt32V(-1);
				return;
			}

			byte[] buffer = encoding.GetBytes(value);
			stream.WriteInt32V(buffer.Length);
			stream.Write(buffer, 0, buffer.Length);
		}

		#endregion
	}
}
