﻿using System;
using System.IO;
using System.Text;

namespace Iridescence
{
	public static partial class StreamExtensions
	{
		#region S16

		/// <summary>
		/// Reads a 16 bit integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16B(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(2);
			return (short)(buffer[1] | (buffer[0] << 8));
		}

		public static bool TryReadInt16B(this Stream stream, out short value)
		{
			value = default;
			if (!stream.TryReadOrThrow(2, out byte[] buffer))
				return false;
			value = (short)(buffer[1] | (buffer[0] << 8));
			return true;
		}

		/// <summary>
		/// Writes a 16 bit integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt16B(this Stream stream, short value)
		{
			byte[] buffer = new byte[2];
			buffer[1] = (byte)(value);
			buffer[0] = (byte)(value >> 8);
			stream.Write(buffer, 0, 2);
		}

		#endregion

		#region U16

		/// <summary>
		/// Reads a 16 bit unsigned integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16B(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(2);
			return (ushort)(buffer[1] | (buffer[0] << 8));
		}

		public static bool TryReadUInt16B(this Stream stream, out ushort value)
		{
			value = default;
			if (!stream.TryReadOrThrow(2, out byte[] buffer))
				return false;
			value = (ushort)(buffer[1] | (buffer[0] << 8));
			return true;
		}

		/// <summary>
		/// Writes a 16 bit unsigned integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt16B(this Stream stream, ushort value)
		{
			byte[] buffer = new byte[2];
			buffer[1] = (byte)(value);
			buffer[0] = (byte)(value >> 8);
			stream.Write(buffer, 0, 2);
		}

		#endregion

		#region S32

		/// <summary>
		/// Reads a 32 bit integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32B(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(4);
			return buffer[3] | (buffer[2] << 8) | (buffer[1] << 16) | (buffer[0] << 24);
		}

		public static bool TryReadInt32B(this Stream stream, out int value)
		{
			value = default;
			if (!stream.TryReadOrThrow(4, out byte[] buffer))
				return false;
			value = buffer[3] | (buffer[2] << 8) | (buffer[1] << 16) | (buffer[0] << 24);
			return true;
		}

		/// <summary>
		/// Writes a 32 bit integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt32B(this Stream stream, int value)
		{
			byte[] buffer = new byte[4];
			buffer[3] = (byte)(value);
			buffer[2] = (byte)(value >> 8);
			buffer[1] = (byte)(value >> 16);
			buffer[0] = (byte)(value >> 24);
			stream.Write(buffer, 0, 4);
		}

		#endregion

		#region U32

		/// <summary>
		/// Reads a 32 bit unsigned integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32B(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(4);
			return (uint)buffer[3] | ((uint)buffer[2] << 8) | ((uint)buffer[1] << 16) | ((uint)buffer[0] << 24);
		}

		public static bool TryReadUInt32B(this Stream stream, out uint value)
		{
			value = default;
			if (!stream.TryReadOrThrow(4, out byte[] buffer))
				return false;
			value = (uint)buffer[3] | ((uint)buffer[2] << 8) | ((uint)buffer[1] << 16) | ((uint)buffer[0] << 24);
			return true;
		}

		/// <summary>
		/// Writes a 32 bit unsigned integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt32B(this Stream stream, uint value)
		{
			byte[] buffer = new byte[4];
			buffer[3] = (byte)(value);
			buffer[2] = (byte)(value >> 8);
			buffer[1] = (byte)(value >> 16);
			buffer[0] = (byte)(value >> 24);
			stream.Write(buffer, 0, 4);
		}

		#endregion

		#region S64

		/// <summary>
		/// Reads a 64 bit integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64B(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(8);
			return (long)buffer[7] | ((long)buffer[6] << 8) | ((long)buffer[5] << 16) | ((long)buffer[4] << 24) | ((long)buffer[3] << 32) | ((long)buffer[2] << 40) | ((long)buffer[1] << 48) | ((long)buffer[0] << 56);
		}

		public static bool TryReadInt64B(this Stream stream, out long value)
		{
			value = default;
			if (!stream.TryReadOrThrow(8, out byte[] buffer))
				return false;
			value = (long)buffer[7] | ((long)buffer[6] << 8) | ((long)buffer[5] << 16) | ((long)buffer[4] << 24) | ((long)buffer[3] << 32) | ((long)buffer[2] << 40) | ((long)buffer[1] << 48) | ((long)buffer[0] << 56);
			return true;
		}

		/// <summary>
		/// Writes a 64 bit integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt64B(this Stream stream, long value)
		{
			byte[] buffer = new byte[8];
			buffer[7] = (byte)(value);
			buffer[6] = (byte)(value >> 8);
			buffer[5] = (byte)(value >> 16);
			buffer[4] = (byte)(value >> 24);
			buffer[3] = (byte)(value >> 32);
			buffer[2] = (byte)(value >> 40);
			buffer[1] = (byte)(value >> 48);
			buffer[0] = (byte)(value >> 56);
			stream.Write(buffer, 0, 8);
		}
		
		#endregion

		#region U64

		/// <summary>
		/// Reads a 64 bit unsigned integer from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64B(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(8);
			return (ulong)buffer[7] | ((ulong)buffer[6] << 8) | ((ulong)buffer[5] << 16) | ((ulong)buffer[4] << 24) | ((ulong)buffer[3] << 32) | ((ulong)buffer[2] << 40) | ((ulong)buffer[1] << 48) | ((ulong)buffer[0] << 56);
		}

		public static bool TryReadUInt64B(this Stream stream, out ulong value)
		{
			value = default;
			if (!stream.TryReadOrThrow(8, out byte[] buffer))
				return false;
			value =  (ulong)buffer[7] | ((ulong)buffer[6] << 8) | ((ulong)buffer[5] << 16) | ((ulong)buffer[4] << 24) | ((ulong)buffer[3] << 32) | ((ulong)buffer[2] << 40) | ((ulong)buffer[1] << 48) | ((ulong)buffer[0] << 56);
			return true;
		}

		/// <summary>
		/// Writes a 64 bit unsigned integer to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt64B(this Stream stream, ulong value)
		{
			byte[] buffer = new byte[8];
			buffer[7] = (byte)(value);
			buffer[6] = (byte)(value >> 8);
			buffer[5] = (byte)(value >> 16);
			buffer[4] = (byte)(value >> 24);
			buffer[3] = (byte)(value >> 32);
			buffer[2] = (byte)(value >> 40);
			buffer[1] = (byte)(value >> 48);
			buffer[0] = (byte)(value >> 56);
			stream.Write(buffer, 0, 8);
		}

		#endregion

		#region F16

		/// <summary>
		/// Reads a 16 bit floating point value from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static Half ReadFloat16B(this Stream stream)
		{
			return PrimitiveUtility.BitsToHalf(stream.ReadUInt16B());
		}

		public static bool TryReadFloat16B(this Stream stream, out Half value)
		{
			value = default;
			if (!stream.TryReadUInt16B(out ushort binary))
				return false;
			value = PrimitiveUtility.BitsToHalf(binary);
			return true;
		}

		/// <summary>
		/// Writes a 16 bit floating point value to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat16B(this Stream stream, Half value)
		{
			stream.WriteUInt16B(PrimitiveUtility.HalfToBits(value));
		}

		#endregion

		#region F32

		/// <summary>
		/// Reads a 32 bit floating point value from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static float ReadFloat32B(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(4);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			return BitConverter.ToSingle(buffer, 0);
		}

		public static bool TryReadFloat32B(this Stream stream, out float value)
		{
			value = default;
			if (!stream.TryReadOrThrow(4, out byte[] buffer))
				return false;

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			value = BitConverter.ToSingle(buffer, 0);
			return true;
		}
		
		/// <summary>
		/// Writes a 32 bit floating point value to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat32B(this Stream stream, float value)
		{
			byte[] buffer = BitConverter.GetBytes(value);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			stream.Write(buffer, 0, 4);
		}

		#endregion

		/// <summary>
		/// Reads a 64 bit floating point value from a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static double ReadFloat64B(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(8);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;
			
				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			return BitConverter.ToDouble(buffer, 0);
		}

		public static bool TryReadFloat64B(this Stream stream, out double value)
		{
			value = default;
			if (!stream.TryReadOrThrow(8, out byte[] buffer))
				return false;

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;

				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			value = BitConverter.ToDouble(buffer, 0);
			return true;
		}

		/// <summary>
		/// Writes a 64 bit floating point value to a stream in big-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat64B(this Stream stream, double value)
		{
			byte[] buffer = BitConverter.GetBytes(value);

			if (BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;

				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			stream.Write(buffer, 0, 8);
		}

		#region String

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadStringB(this Stream stream)
		{
			return stream.ReadStringB(DefaultEncoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringB(this Stream stream, Encoding encoding)
		{
			int length = stream.ReadInt32B();
			if (length < 0)
				return null;

			return stream.ReadString(length, encoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="maxLength">The maximum length of the string (in bytes).</param>
		/// <returns></returns>
		public static string ReadStringB(this Stream stream, int maxLength)
		{
			return stream.ReadStringB(maxLength, DefaultEncoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="maxLength">The maximum length of the string (in bytes).</param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringB(this Stream stream, int maxLength, Encoding encoding)
		{
			int length = stream.ReadInt32B();

			if (length < 0)
				return null;

			if (length > maxLength)
				throw new LimitExceededException();

			return stream.ReadString(length, encoding);
		}

		/// <summary>
		/// Writes a string to a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStringB(this Stream stream, string value)
		{
			WriteStringB(stream, value, DefaultEncoding);
		}

		/// <summary>
		/// Writes a string encoded with the specified encoding to a stream, prefixed with a 32 bit integer in big-endian byte order that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		/// <param name="encoding"></param>
		public static void WriteStringB(this Stream stream, string value, Encoding encoding)
		{
			if (value == null)
			{
				stream.WriteInt32B(-1);
				return;
			}

			byte[] buffer = encoding.GetBytes(value);
			stream.WriteInt32B(buffer.Length);
			stream.Write(buffer, 0, buffer.Length);
		}

		#endregion
	}
}
