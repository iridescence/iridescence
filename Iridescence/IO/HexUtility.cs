﻿using System;
using System.IO;
using System.Text;

namespace Iridescence
{
	/// <summary>
	/// Provides hexadecimal conversion functionality.
	/// </summary>
	public static class HexUtility
	{
		private static readonly char[] hexCharsUpper =
		{
			'0', '1', '2', '3',
			'4', '5', '6', '7',
			'8', '9', 'A', 'B',
			'C', 'D', 'E', 'F'
		};

		private static readonly char[] hexCharsLower =
		{
			'0', '1', '2', '3',
			'4', '5', '6', '7',
			'8', '9', 'a', 'b',
			'c', 'd', 'e', 'f'
		};

		/// <summary>
		/// Writes a hex-dump of the specified stream into the writer.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="writer"></param>
		/// <param name="width"></param>
		/// <param name="printChars"></param>
		public static void Dump(this Stream input, TextWriter writer, int width = 16, bool printChars = true)
		{
			Dump(input, writer.WriteLine, width, printChars);
		}

		/// <summary>
		/// Returns the hexadecimal string representing the specified byte array.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="upperCase">Whether the hex characters (A-F) should be upper case.</param>
		/// <returns></returns>
		public static string ToHexString(this byte[] data, bool upperCase = false)
		{
			return new ReadOnlySpan<byte>(data).ToHexString(upperCase);
		}

		/// <summary>
		/// Returns the hexadecimal string representing the specified byte array.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="upperCase">Whether the hex characters (A-F) should be upper case.</param>
		/// <returns></returns>
		public static string ToHexString(this Span<byte> data, bool upperCase = false)
		{
			return ((ReadOnlySpan<byte>)data).ToHexString(upperCase);
		}

		/// <summary>
		/// Returns the hexadecimal string representing the specified byte span.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="upperCase">Whether the hex characters (A-F) should be upper case.</param>
		/// <returns></returns>
		public static string ToHexString(this ReadOnlySpan<byte> data, bool upperCase = false)
		{
			char[] chars = upperCase ? hexCharsUpper : hexCharsLower;

			StringBuilder str = new StringBuilder(data.Length * 2);

			for (int i = 0; i < data.Length; ++i)
			{
				int n1 = data[i] >> 4;
				int n2 = data[i] & 0xF;

				str.Append(chars[n1]);
				str.Append(chars[n2]);
			}

			return str.ToString();
		}


		/// <summary>
		/// Converts a single hexadecimal character to an integer.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public static int HexCharToInt(char c)
		{
			if (c >= '0' && c <= '9')
				return c - '0';

			if (c >= 'a' && c <= 'f')
				return 10 + (c - 'a');

			if (c >= 'A' && c <= 'F')
				return 10 + (c - 'A');

			throw new ArgumentException($"Invalid hex character '{c}'.", nameof(c));
		}

		public static int Parse(string hexString, Span<byte> dest)
		{
			if (hexString == null)
				throw new ArgumentNullException(nameof(hexString));

			if (hexString.Length % 2 != 0)
				throw new ArgumentException("Invalid hex string length. Must be a multiple of 2.", nameof(hexString));

			int len = hexString.Length / 2;
			if (len > dest.Length)
				throw new ArgumentException("Destination span is too small.", nameof(dest));

			for (int i = 0; i < hexString.Length / 2; ++i)
			{
				dest[i] = (byte)(HexCharToInt(hexString[2 * i + 0]) * 16 + HexCharToInt(hexString[2 * i + 1]));
			}

			return len;
		}

		public static void Dump(this Stream input, Action<string> writeLine, int width = 16, bool printChars = true)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));

			if (writeLine == null)
				throw new ArgumentNullException(nameof(writeLine));

			IbmPcEncoding enc = IbmPcEncoding.Instance;

			StringBuilder line = new StringBuilder(width * 3 + (printChars ? 1 + width : 0));
			byte[] buffer = new byte[width];
			char[] chars = new char[1];

			for (;;)
			{
				int bytes = 0;
				while (bytes < width)
				{
					int numBytes = input.Read(buffer, bytes, width - bytes);
					bytes += numBytes;

					if (numBytes == 0)
						break;
				}

				line.Clear();

				for (int i = 0; i < bytes; ++i)
				{
					line.Append(buffer[i].ToString("X2"));
					line.Append(' ');
				}

				for (int i = 0; i < width - bytes; ++i)
				{
					line.Append("   ");
				}

				if (printChars)
				{
					line.Append(' ');

					for (int i = 0; i < bytes; ++i)
					{
						enc.GetChars(buffer, i, 1, chars, 0);
						char c = chars[0];
						if (char.IsControl(c))
						{
							line.Append('.');
						}
						else
						{
							line.Append(c);	
						}
					}
				}

				writeLine(line.ToString());

				if (bytes < width)
					break;
			}
		}
	}
}
