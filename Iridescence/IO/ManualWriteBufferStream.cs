﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Represents a write-only buffered stream with dynamic buffer size that needs to be manually flushed.
	/// </summary>
	public class ManualWriteBufferStream : Stream
	{
		#region Fields

		private bool enabled;
		private RecursionGuard flushGuard;
		
		private readonly Stream stream;
		private readonly bool leaveOpen;

		private byte[] buffer;
		private int bufferSize;
	
		#endregion

		#region Properties

		/// <inheritdoc />
		public override bool CanRead => false;
		
		/// <inheritdoc />
		public override bool CanSeek => false;
		
		/// <inheritdoc />
		public override bool CanWrite => true;

		/// <inheritdoc />
		public override long Length => throw new NotSupportedException();

		/// <inheritdoc />
		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		/// <summary>
		/// Gets or sets an object that is locked for write operations.
		/// </summary>
		public object WriteLock { get; set; }

		/// <summary>
		/// Gets the current size of the buffer.
		/// </summary>
		public int BufferSize => this.bufferSize;

		/// <summary>
		/// Gets or sets a value that determines whether the overrides <see cref="Stream.Flush"/> method is ignored. If it is not ignored, it will call <see cref="FlushBuffer"/>
		/// </summary>
		public bool IgnoreStreamFlush { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ManualWriteBufferStream"/> class. 
		/// </summary>
		public ManualWriteBufferStream(Stream stream, bool leaveOpen, bool enabled = true, int initialBufferSize = 0)
		{
			this.flushGuard = new RecursionGuard();
			this.stream = stream;
			this.leaveOpen = leaveOpen;
			this.enabled = enabled;

			if (initialBufferSize > 0)
			{
				this.buffer = new byte[initialBufferSize];
			}
		}

		#endregion

		#region Methods

		/// <inheritdoc />
		public override void Flush()
		{
			if (this.IgnoreStreamFlush)
				return;

			this.FlushBuffer();
		}

		public override Task FlushAsync(CancellationToken cancellationToken)
		{
			if (this.IgnoreStreamFlush)
				return Task.CompletedTask;

			return this.FlushBufferAsync(cancellationToken);
		}

		private bool acquireBuffer(bool disableIfEmpty, out byte[] buffer, out int length)
		{
			buffer = null;
			length = 0;

			// Try to get the buffer.
			using (DisposableLock.Lock(this.WriteLock))
			{
				if (this.bufferSize == 0)
				{
					// Buffer was empty. If we have to disable to write buffer we can safely do that now.
					if (disableIfEmpty)
					{
						this.enabled = false;
						return true;
					}
					
					return false;
				}
					
				buffer = this.buffer;
				length = this.bufferSize;

				this.buffer = null;
				this.bufferSize = 0;
				return false;
			}
		}

		private bool returnBuffer(bool disableIfEmpty, byte[] buffer)
		{
			// Give back buffer.
			using (DisposableLock.Lock(this.WriteLock))
			{
				// If we should disable the buffer and it is still empty, we can do that.
				if (disableIfEmpty && this.buffer == null)
					this.enabled = false;
					
				if (!this.enabled)
					return true;

				// Give back the buffer so it doesn't have to be re-allocated.
				// If a write call has created a new buffer, we can not give it back.
				if (this.buffer == null)
					this.buffer = buffer;
			}

			return false;
		}

		private bool flush(bool tryDisable)
		{
			using (this.flushGuard.Protect())
			{
				if (this.acquireBuffer(tryDisable, out byte[] buffer, out int length))
					return true;

				if(length == 0)
					return false;

				// Write.
				this.stream.Write(buffer, 0, length);
				this.stream.Flush();

				if (this.returnBuffer(tryDisable, buffer))
					return true;

				return false;
			}
		}

		private async Task<bool> flushAsync(bool tryDisable, CancellationToken cancellationToken)
		{
			using (this.flushGuard.Protect())
			{
				if (this.acquireBuffer(tryDisable, out byte[] buffer, out int length))
					return true;

				if(length == 0)
					return false;

				// Write.
				await this.stream.WriteAsync(buffer, 0, length, cancellationToken);
				await this.stream.FlushAsync(cancellationToken);

				if (this.returnBuffer(tryDisable, buffer))
					return true;

				return false;
			}
		}


		/// <summary>
		/// Enables buffering.
		/// </summary>
		public void Enable()
		{
			using (DisposableLock.Lock(this.WriteLock))
			{
				this.enabled = true;
			}
		}
		
		/// <summary>
		/// Flushes the buffer and disables buffering.
		/// </summary>
		public void Disable()
		{
			while (!this.flush(true))
			{

			}
		}

		/// <summary>
		/// Flushes the buffer and disables buffering.
		/// </summary>
		public async Task DisableAsync(CancellationToken cancellationToken = default)
		{
			while (!await this.flushAsync(true, cancellationToken))
			{

			}
		}
		
		/// <summary>
		/// Writes the data currently buffered in this <see cref="ManualWriteBufferStream"/> to the base stream.
		/// </summary>
		public void FlushBuffer()
		{
			this.flush(false);
		}

		/// <summary>
		/// Writes the data currently buffered in this <see cref="ManualWriteBufferStream"/> to the base stream.
		/// </summary>
		public Task FlushBufferAsync(CancellationToken cancellationToken = default)
		{
			return this.flushAsync(false, cancellationToken);
		}

		/// <summary>
		/// Runs a loop that repeatedly flushes the stream.
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <param name="interval"></param>
		/// <returns></returns>
		public async Task RunAsync(CancellationToken cancellationToken = default, TimeSpan? interval = null)
		{
			if (interval == null)
				interval = TimeSpan.FromMilliseconds(1);

			for (;;)
			{
				cancellationToken.ThrowIfCancellationRequested();

				if (this.bufferSize == 0)
					await Task.Delay(interval.Value, cancellationToken);

				await this.FlushBufferAsync(cancellationToken);
			}
		}
		
		/// <inheritdoc />
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc />
		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		private void ensureCapacity(int count)
		{
			int currentCount = this.buffer?.Length ?? 0;
			
			if (this.bufferSize + count > currentCount)
			{
				int newCount = Math.Max(1, currentCount);
				while (this.bufferSize + count > newCount)
				{
					newCount *= 2;
				}

				if (this.buffer == null)
				{
					this.buffer = new byte[newCount];
				}
				else
				{
					Array.Resize(ref this.buffer, newCount);
				}
			}
		}

		/// <inheritdoc />
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (count == 0)
				return;

			using (DisposableLock.Lock(this.WriteLock))
			{
				if (this.enabled)
				{
					this.ensureCapacity(count);
					Array.Copy(buffer, offset, this.buffer, this.bufferSize, count);
					this.bufferSize += count;
				}
				else
				{
					this.stream.Write(buffer, offset, count);
				}
			}
		}

		/// <inheritdoc />
		public override void WriteByte(byte value)
		{
			using (DisposableLock.Lock(this.WriteLock))
			{
				if (this.enabled)
				{
					this.ensureCapacity(1);
					this.buffer[this.bufferSize++] = value;
				}
				else
				{
					this.stream.WriteByte(value);
				}
			}
		}

		/// <inheritdoc />
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing)
			{
				this.Flush();
				
				if (!this.leaveOpen)
				{
					this.stream.Dispose();
				}
			}
		}

		#endregion
	}
}
