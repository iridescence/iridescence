﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Represents a segment of a stream.
	/// </summary>
	public class SegmentStream : Stream
	{
		#region Fields

		private readonly Stream stream;
		private readonly long start;
		private long length;
		private long position;
		private readonly bool leaveOpen;

		#endregion

		#region Properties

		/// <inheritdoc/>
		public override bool CanRead => this.stream.CanRead;

		/// <inheritdoc/>
		public override bool CanWrite => this.stream.CanWrite;

		/// <inheritdoc/>
		public override bool CanSeek => this.start >= 0;

		/// <inheritdoc/>
		public override long Position
		{
			get => this.position;
			set => this.position = value;
		}

		/// <inheritdoc/>
		public override long Length
		{
			get
			{
				if (this.length < 0)
					throw new NotSupportedException();

				return this.length;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SegmentStream.
		/// </summary>
		/// <param name="stream">The base stream.</param>
		/// <param name="start">An offset in the base stream where the segment starts. Can be -1 to disable seeking and only use length-limited read/write operations.</param>
		/// <param name="length">The length of the segment.</param>
		/// <param name="leaveOpen">True, if the base stream should be left open when this instance is closed.</param>
		public SegmentStream(Stream stream, long start, long length, bool leaveOpen)
		{
			if (start >= 0 && !stream.CanSeek)
				throw new ArgumentException("Stream segments with a starting position can only be used with seekable streams.", nameof(stream));

			this.stream = stream;
			this.start = start;
			this.length = length;
			this.position = 0;
			this.leaveOpen = leaveOpen;
		}

		#endregion

		#region Methods

		#endregion

		/// <inheritdoc/>
		public override void Flush()
		{
			this.stream.Flush();
		}

		/// <inheritdoc/>
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (!this.CanRead)
				throw new NotSupportedException();

			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (this.start >= 0 && this.stream.Position != this.start + this.position)
				this.stream.Seek(this.start + this.position, SeekOrigin.Begin);

			if (this.length >= 0 && this.position + count > this.length)
				count = (int)(this.length - this.position);

			int numBytes = this.stream.Read(buffer, offset, count);
			this.position += numBytes;
			return numBytes;
		}

		/// <inheritdoc/>
		public override int ReadByte()
		{
			if (!this.CanRead)
				throw new NotSupportedException();

			if (this.start >= 0 && this.stream.Position != this.start + this.position)
				this.stream.Seek(this.start + this.position, SeekOrigin.Begin);
			
			if (this.length >= 0 && this.position >= this.length)
				return -1;

			int result = this.stream.ReadByte();
			this.position++;
			return result;
		}

		/// <inheritdoc/>
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (!this.CanWrite)
				throw new NotSupportedException();

			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			if (this.start >= 0 && this.stream.Position != this.start + this.position)
				this.stream.Seek(this.start + this.position, SeekOrigin.Begin);

			if (this.length >= 0 && this.position + count > this.length)
				throw new NotSupportedException("Stream segment can not be expanded.");

			this.stream.Write(buffer, offset, count);
			this.position += count;
		}

		/// <inheritdoc/>
		public override void WriteByte(byte value)
		{
			if (!this.CanWrite)
				throw new NotSupportedException();

			if (this.start >= 0 && this.stream.Position != this.start + this.position)
				this.stream.Seek(this.start + this.position, SeekOrigin.Begin);

			if (this.length >= 0 && this.position + 1 > this.length)
				throw new NotSupportedException("Stream segment can not be expanded.");

			this.stream.WriteByte(value);
			this.position++;
		}

		/// <inheritdoc/>
		public override long Seek(long offset, SeekOrigin origin)
		{
			if (!this.CanSeek)
				throw new NotSupportedException();

			if (origin == SeekOrigin.Begin)
			{
				//this.stream.Seek(this.start + offset, SeekOrigin.Begin);
				this.position = offset;
			}
			else if (origin == SeekOrigin.End)
			{
				//this.stream.Seek(this.start + this.length - offset, SeekOrigin.Begin);
				this.position = this.length - offset;
			}
			else if (origin == SeekOrigin.Current)
			{
				//this.stream.Seek(this.start + this.position + offset, SeekOrigin.Current);
				this.position += offset;
			}
			return this.position;
		}

		/// <inheritdoc/>
		public override void SetLength(long value)
		{
			this.length = value;
		}

		/// <inheritdoc/>
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing)
			{
				if (!this.leaveOpen)
					this.stream.Close();
			}
		}
	}
}
