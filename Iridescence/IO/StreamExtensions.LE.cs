﻿using System;
using System.IO;
using System.Text;

namespace Iridescence
{
	public static partial class StreamExtensions
	{
		#region S16

		/// <summary>
		/// Reads a 16 bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static short ReadInt16L(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(2);
			return (short)(buffer[0] | (buffer[1] << 8));
		}

		public static bool TryReadInt16L(this Stream stream, out short value)
		{
			value = default;
			if (!stream.TryReadOrThrow(2, out byte[] buffer))
				return false;
			value = (short)(buffer[0] | (buffer[1] << 8));
			return true;
		}

		/// <summary>
		/// Writes a 16 bit integer to a stream.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt16L(this Stream stream, short value)
		{
			byte[] buffer = new byte[2];
			buffer[0] = (byte)(value);
			buffer[1] = (byte)(value >> 8);
			stream.Write(buffer, 0, 2);
		}

		#endregion

		#region U16

		/// <summary>
		/// Reads a 16 bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ushort ReadUInt16L(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(2);
			return (ushort)(buffer[0] | (buffer[1] << 8));
		}

		public static bool TryReadUInt16L(this Stream stream, out ushort value)
		{
			value = default;
			if (!stream.TryReadOrThrow(2, out byte[] buffer))
				return false;
			value = (ushort)(buffer[0] | (buffer[1] << 8));
			return true;
		}

		/// <summary>
		/// Writes a 16 bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt16L(this Stream stream, ushort value)
		{
			byte[] buffer = new byte[2];
			buffer[0] = (byte)(value);
			buffer[1] = (byte)(value >> 8);
			stream.Write(buffer, 0, 2);
		}

		#endregion

		#region S32

		/// <summary>
		/// Reads a 32 bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static int ReadInt32L(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(4);
			return buffer[0] | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3] << 24);
		}

		public static bool TryReadInt32L(this Stream stream, out int value)
		{
			value = default;
			if (!stream.TryReadOrThrow(4, out byte[] buffer))
				return false;
			value = buffer[0] | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3] << 24);
			return true;
		}

		/// <summary>
		/// Writes a 32 bit integer to a stream.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt32L(this Stream stream, int value)
		{
			byte[] buffer = new byte[4];
			buffer[0] = (byte)(value);
			buffer[1] = (byte)(value >> 8);
			buffer[2] = (byte)(value >> 16);
			buffer[3] = (byte)(value >> 24);
			stream.Write(buffer, 0, 4);
		}

		#endregion

		#region U32

		/// <summary>
		/// Reads a 32 bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static uint ReadUInt32L(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(4);
			return (uint)buffer[0] | ((uint)buffer[1] << 8) | ((uint)buffer[2] << 16) | ((uint)buffer[3] << 24);
		}

		public static bool TryReadUInt32L(this Stream stream, out uint value)
		{
			value = default;
			if (!stream.TryReadOrThrow(4, out byte[] buffer))
				return false;
			value = (uint)buffer[0] | ((uint)buffer[1] << 8) | ((uint)buffer[2] << 16) | ((uint)buffer[3] << 24);
			return true;
		}
		
		/// <summary>
		/// Writes a 32 bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt32L(this Stream stream, uint value)
		{
			byte[] buffer = new byte[4];
			buffer[0] = (byte)(value);
			buffer[1] = (byte)(value >> 8);
			buffer[2] = (byte)(value >> 16);
			buffer[3] = (byte)(value >> 24);
			stream.Write(buffer, 0, 4);
		}

		#endregion

		#region S64

		/// <summary>
		/// Reads a 64 bit integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static long ReadInt64L(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(8);
			return (long)buffer[0] | ((long)buffer[1] << 8) | ((long)buffer[2] << 16) | ((long)buffer[3] << 24) | ((long)buffer[4] << 32) | ((long)buffer[5] << 40) | ((long)buffer[6] << 48) | ((long)buffer[7] << 56);
		}
		
		public static bool TryReadInt64L(this Stream stream, out long value)
		{
			value = default;
			if (!stream.TryReadOrThrow(8, out byte[] buffer))
				return false;
			value = (long)buffer[0] | ((long)buffer[1] << 8) | ((long)buffer[2] << 16) | ((long)buffer[3] << 24) | ((long)buffer[4] << 32) | ((long)buffer[5] << 40) | ((long)buffer[6] << 48) | ((long)buffer[7] << 56);
			return true;
		}

		/// <summary>
		/// Writes a 64 bit integer to a stream.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteInt64L(this Stream stream, long value)
		{
			byte[] buffer = new byte[8];
			buffer[0] = (byte)(value);
			buffer[1] = (byte)(value >> 8);
			buffer[2] = (byte)(value >> 16);
			buffer[3] = (byte)(value >> 24);
			buffer[4] = (byte)(value >> 32);
			buffer[5] = (byte)(value >> 40);
			buffer[6] = (byte)(value >> 48);
			buffer[7] = (byte)(value >> 56);
			stream.Write(buffer, 0, 8);
		}

		#endregion

		#region U64

		/// <summary>
		/// Reads a 64 bit unsigned integer from a stream.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static ulong ReadUInt64L(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(8);
			return (ulong)buffer[0] | ((ulong)buffer[1] << 8) | ((ulong)buffer[2] << 16) | ((ulong)buffer[3] << 24) | ((ulong)buffer[4] << 32) | ((ulong)buffer[5] << 40) | ((ulong)buffer[6] << 48) | ((ulong)buffer[7] << 56);
		}
		
		public static bool TryReadUInt64L(this Stream stream, out ulong value)
		{
			value = default;
			if (!stream.TryReadOrThrow(8, out byte[] buffer))
				return false;
			value = (ulong)buffer[0] | ((ulong)buffer[1] << 8) | ((ulong)buffer[2] << 16) | ((ulong)buffer[3] << 24) | ((ulong)buffer[4] << 32) | ((ulong)buffer[5] << 40) | ((ulong)buffer[6] << 48) | ((ulong)buffer[7] << 56);
			return true;
		}

		/// <summary>
		/// Writes a 64 bit unsigned integer to a stream.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteUInt64L(this Stream stream, ulong value)
		{
			byte[] buffer = new byte[8];
			buffer[0] = (byte)(value);
			buffer[1] = (byte)(value >> 8);
			buffer[2] = (byte)(value >> 16);
			buffer[3] = (byte)(value >> 24);
			buffer[4] = (byte)(value >> 32);
			buffer[5] = (byte)(value >> 40);
			buffer[6] = (byte)(value >> 48);
			buffer[7] = (byte)(value >> 56);
			stream.Write(buffer, 0, 8);
		}

		#endregion

		#region F16

		/// <summary>
		/// Reads a 16 bit floating point value from a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static Half ReadFloat16L(this Stream stream)
		{
			return PrimitiveUtility.BitsToHalf(stream.ReadUInt16L());
		}

		public static bool TryReadReadFloat16LInt64L(this Stream stream, out Half value)
		{
			value = default;
			if (!stream.TryReadUInt16L(out ushort binary))
				return false;
			value = PrimitiveUtility.BitsToHalf(binary);
			return true;
		}

		/// <summary>
		/// Writes a 16 bit floating point value to a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat16L(this Stream stream, Half value)
		{
			stream.WriteUInt16L(PrimitiveUtility.HalfToBits(value));
		}

		#endregion

		#region F32

		/// <summary>
		/// Reads a 32 bit floating point value from a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static float ReadFloat32L(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(4);

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			return BitConverter.ToSingle(buffer, 0);
		}

		public static bool TryReadFloat32L(this Stream stream, out float value)
		{
			value = default;
			if (!stream.TryReadOrThrow(4, out byte[] buffer))
				return false;

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			value = BitConverter.ToSingle(buffer, 0);
			return true;
		}

		/// <summary>
		/// Writes a 32 bit floating point value to a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat32L(this Stream stream, float value)
		{
			byte[] buffer = BitConverter.GetBytes(value);
			
			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[3];
				buffer[3] = temp;

				temp = buffer[1];
				buffer[1] = buffer[2];
				buffer[2] = temp;
			}

			stream.Write(buffer, 0, 4);
		}

		#endregion

		#region F64

		/// <summary>
		/// Reads a 64 bit floating point value from a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The source stream.</param>
		/// <returns>The value.</returns>
		public static double ReadFloat64L(this Stream stream)
		{
			byte[] buffer = stream.ReadOrThrow(8);

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;

				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			return BitConverter.ToDouble(buffer, 0);
		}

		public static bool TryReadFloat64L(this Stream stream, out double value)
		{
			value = default;
			if (!stream.TryReadOrThrow(8, out byte[] buffer))
				return false;

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;

				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			value = BitConverter.ToDouble(buffer, 0);
			return true;
		}

		/// <summary>
		/// Writes a 64 bit floating point value to a stream in little-endian byte order.
		/// </summary>
		/// <param name="stream">The destination stream.</param>
		/// <param name="value">The value to write.</param>
		public static void WriteFloat64L(this Stream stream, double value)
		{
			byte[] buffer = BitConverter.GetBytes(value);

			if (!BitConverter.IsLittleEndian)
			{
				// reverse.
				byte temp = buffer[0];
				buffer[0] = buffer[7];
				buffer[7] = temp;

				temp = buffer[1];
				buffer[1] = buffer[6];
				buffer[6] = temp;

				temp = buffer[2];
				buffer[2] = buffer[5];
				buffer[5] = temp;

				temp = buffer[3];
				buffer[3] = buffer[4];
				buffer[4] = temp;
			}

			stream.Write(buffer, 0, 8);
		}

		#endregion

		#region String

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static string ReadStringL(this Stream stream)
		{
			return stream.ReadStringL(DefaultEncoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringL(this Stream stream, Encoding encoding)
		{
			int length = stream.ReadInt32L();

			if (length < 0)
				return null;

			return stream.ReadString(length, encoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="maxLength">The maximum length of the string (in bytes).</param>
		/// <returns></returns>
		public static string ReadStringL(this Stream stream, int maxLength)
		{
			return stream.ReadStringL(maxLength, DefaultEncoding);
		}

		/// <summary>
		/// Reads a string from a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="maxLength">The maximum length of the string (in bytes).</param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		public static string ReadStringL(this Stream stream, int maxLength, Encoding encoding)
		{
			int length = stream.ReadInt32L();

			if (length < 0)
				return null;

			if (length > maxLength)
				throw new LimitExceededException();

			return stream.ReadString(length, encoding);
		}

		/// <summary>
		/// Writes a string to a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void WriteStringL(this Stream stream, string value)
		{
			WriteStringL(stream, value, DefaultEncoding);
		}

		/// <summary>
		/// Writes a string encoded with the specified encoding to a stream, prefixed with a 32 bit integer that determines the length.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		/// <param name="encoding"></param>
		public static void WriteStringL(this Stream stream, string value, Encoding encoding)
		{
			if (value == null)
			{
				stream.WriteInt32L(-1);
				return;
			}

			byte[] buffer = encoding.GetBytes(value);
			stream.WriteInt32L(buffer.Length);
			stream.Write(buffer, 0, buffer.Length);
		}

		#endregion
	}
}
