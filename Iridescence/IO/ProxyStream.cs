﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Represents a simple stream proxy that can redirect read and write operations to two different streams.
	/// </summary>
	public sealed class ProxyStream : Stream
	{
		#region Fields

		private readonly bool leaveReadOpen;
		private readonly bool leaveWriteOpen;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the stream that is used for read operations.
		/// </summary>
		public Stream ReadStream { get; set; }

		/// <summary>
		/// Gets the stream that is used for write operations.
		/// </summary>
		public Stream WriteStream { get; set; }

		/// <inheritdoc/>
		public override bool CanRead => this.ReadStream?.CanRead ?? false;
		
		/// <inheritdoc/>
		public override bool CanSeek => false;

		/// <inheritdoc/>
		public override bool CanWrite => this.WriteStream?.CanWrite ?? false;

		/// <inheritdoc/>
		public override long Length => throw new NotSupportedException();

		/// <inheritdoc/>
		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		#endregion

		#region Constructors
		
		public ProxyStream(Stream readStream, Stream writeStream)
			: this(readStream, writeStream, false)
		{

		}

		public ProxyStream(Stream readStream, Stream writeStream, bool leaveOpen)
			: this(readStream, writeStream, leaveOpen, leaveOpen)
		{

		}

		public ProxyStream(Stream readStream, Stream writeStream, bool leaveReadOpen, bool leaveWriteOpen)
		{
			this.ReadStream = readStream;
			this.WriteStream = writeStream;

			this.leaveReadOpen = leaveReadOpen;
			this.leaveWriteOpen = leaveWriteOpen;
		}

		#endregion

		#region Methods

		/// <inheritdoc/>
		public override void Flush()
		{
			//this.ReadStream?.Flush();
			this.WriteStream?.Flush();
		}

		/// <inheritdoc/>
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}
		
		/// <inheritdoc/>
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (this.ReadStream == null)
				throw new NotSupportedException();

			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			return this.ReadStream.Read(buffer, offset, count);
		}

		/// <inheritdoc/>
		public override int ReadByte()
		{
			if (this.ReadStream == null)
				throw new NotSupportedException();

			return this.ReadStream.ReadByte();
		}

		/// <inheritdoc/>
		public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (this.ReadStream == null)
				throw new NotSupportedException();

			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			return this.ReadStream.ReadAsync(buffer, offset, count, cancellationToken);
		}

		/// <inheritdoc/>
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (this.WriteStream == null)
				throw new NotSupportedException();

			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			this.WriteStream.Write(buffer, offset, count);
		}
		
		/// <inheritdoc/>
		public override void WriteByte(byte value)
		{
			if (this.WriteStream == null)
				throw new NotSupportedException();

			this.WriteStream.WriteByte(value);
		}

		/// <inheritdoc/>
		public override Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
		{
			if (this.WriteStream == null)
				throw new NotSupportedException();

			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			return this.WriteStream.WriteAsync(buffer, offset, count, cancellationToken);
		}

		/// <inheritdoc/>
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			if (disposing)
			{
				if (!this.leaveReadOpen)
					this.ReadStream?.Close();

				if (!this.leaveWriteOpen)
					this.WriteStream?.Close();
			}
		}

		#endregion
	}
}
