﻿using System;
using System.IO;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Implements a stream multiplexer that provides multiple IO channels on a single <see cref="T:System.IO.Stream" />.
	/// </summary>
	public class StreamMultiplexer : Stream
	{
		#region Fields

		private readonly Stream stream;
		private readonly bool leaveOpen;

		private readonly object writeLock;
		private readonly object readLock;
		
		private int lastWriteChannel;
		private int lastReadChannel;
		private int currentReadChannel;
		private int currentReadChannelCount;

		private int lockedReadChannel;
		private int readChannelLocks;
		
		#endregion

		#region Properties
		
		public override bool CanRead => this.stream.CanRead;
		
		public override bool CanSeek => false;
		
		public override bool CanWrite => this.stream.CanWrite;
		
		public override long Length => this.stream.Length;

		public override long Position
		{
			get => this.stream.Position;
			set => throw new NotSupportedException();
		}

		/// <summary>
		/// Gets or sets the channel for the next write operation.
		/// </summary>
		public int WriteChannel { get; set; }

		/// <summary>
		/// Gets the channel of the last read operation.
		/// </summary>
		public int ReadChannel => this.lastReadChannel;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamMultiplexer"/> class. 
		/// </summary>
		public StreamMultiplexer(Stream stream, bool leaveOpen)
		{
			this.stream = stream ?? throw new ArgumentNullException(nameof(stream));
			this.leaveOpen = leaveOpen;

			this.readLock = new object();
			this.writeLock = new object();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Retrieves the read channel index for the next read operation.
		/// Reads from the stream if necessary.
		/// </summary>
		/// <param name="channel">The current read channel index.</param>
		/// <returns>True, if the channel index was returned. False, if the stream ended.</returns>
		public bool TryGetNextChannel(out int channel)
		{
			lock (this.readLock)
			{
				while (this.currentReadChannelCount == 0)
				{
					// No bytes left, read new count.
					if (!this.stream.TryReadInt32V(out this.currentReadChannelCount))
					{
						channel = 0;
						return false;
					}

					if (this.currentReadChannelCount == 0)
					{
						// If count = 0, the channel is switched and the new channel index follows.
						this.currentReadChannel = this.stream.ReadInt32V();
					}
				}

				channel = this.currentReadChannel;
				return true;
			}
		}

		/// <summary>
		/// Writes to the current channel specified in <see cref="P:Iridescence.StreamMultiplexer.WriteChannel" />.
		/// </summary>
		/// <param name="buffer">A buffer containing the data to write.</param>
		/// <param name="offset">An offset in the buffer.</param>
		/// <param name="count">The number of bytes to write.</param>
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));
			
			if (count == 0)
				return;

			lock (this.writeLock)
			{
				if (this.lastWriteChannel != this.WriteChannel)
				{
					this.stream.WriteInt32V(0); // Count = 0, new channel ID follows.
					this.stream.WriteInt32V(this.WriteChannel);
					this.lastWriteChannel = this.WriteChannel;
				}

				this.stream.WriteInt32V(count);
				this.stream.Write(buffer, offset, count);
			}
		}
	
		/// <summary>
		/// Reads from the current channel. To get the channel, use <see cref="TryGetNextChannel"/> before calling this method.
		/// </summary>
		/// <param name="buffer">A buffer that will contain the data that was read.</param>
		/// <param name="offset">An offset in the buffer.</param>
		/// <param name="count">The number of bytes to read.</param>
		/// <returns>The number of bytes read, or 0 if the stream ended.</returns>
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));

			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));
			
			if (offset + count > buffer.Length)
				throw new ArgumentException("The sum of offset and count is greater than the buffer length.", nameof(count));

			lock (this.readLock)
			{
				// Get current/next read channel.
				if (!this.TryGetNextChannel(out this.lastReadChannel))
				{
					// Stream ended.
					return 0;
				}

				// Check channel lock.
				if (this.readChannelLocks != 0 && this.currentReadChannel != this.lockedReadChannel)
				{
					throw new IOException("Read channel was changed while locked.");
				}
				
				if (count > this.currentReadChannelCount)
				{
					// Only read to channel end.
					count = this.currentReadChannelCount;
				}

				count = this.stream.Read(buffer, offset, count);
				this.currentReadChannelCount -= count;

				return count;				
			}
		}

		/// <summary>
		/// Locks the read channel to the channel index of the next read operation (see <see cref="TryGetNextChannel"/>).
		/// If a read operation tries to read from a different channel, an exception is thrown.
		/// To release the lock, dispose the return value of this method.
		/// </summary>
		public ChannelLock LockReadChannel()
		{
			lock (this.readLock)
			{
				if (!this.TryGetNextChannel(out int channel))
				{
					throw new EndOfStreamException();
				}

				return this.LockReadChannel(channel);
			}
		}

		/// <summary>
		/// Locks the read channel to the specified channel index.
		/// If a read operation tries to read from a different channel, an exception is thrown.
		/// To release the lock, dispose the return value of this method.
		/// </summary>
		/// <param name="channel">The channel index.</param>
		/// <returns>A disposable lock.</returns>
		public ChannelLock LockReadChannel(int channel)
		{
			lock (this.readLock)
			{
				if (this.readChannelLocks != 0 && this.lockedReadChannel != channel)
					throw new InvalidOperationException("Read channel is already locked.");

				++this.readChannelLocks;
				this.lockedReadChannel = channel;

				return new ChannelLock(this);
			}
		}

		private void releaseReadChanneLock()
		{
			lock (this.readLock)
			{
				--this.readChannelLocks;
			}
		}

		public override void Close()
		{
			if (!this.leaveOpen)
				this.stream.Close();
		}

		public override void Flush()
		{
			this.stream.Flush();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		#endregion

		#region Nested Types

		public struct ChannelLock : IDisposable
		{
			private StreamMultiplexer multiplexer;
			
			internal ChannelLock(StreamMultiplexer multiplexer)
			{
				this.multiplexer = multiplexer;
			}
			
			public void Dispose()
			{
				Interlocked.Exchange(ref this.multiplexer, null)?.releaseReadChanneLock();
			}
		}

		#endregion
	}
}
