﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Implements a <see cref="Stream"/> that reads from a <see cref="BitReader"/>.
	/// </summary>
	public sealed class BitReaderStream : Stream
	{
		#region Fields

		private readonly BitReader reader;
		private readonly bool aligned;

		#endregion

		#region Properties

		/// <inheritdoc/>
		public override bool CanRead => true;
		
		/// <inheritdoc/>
		public override bool CanSeek => false;
		
		/// <inheritdoc/>
		public override bool CanWrite => false;

		/// <inheritdoc/>
		public override long Length => throw new NotSupportedException();

		/// <inheritdoc/>
		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BitReaderStream.
		/// </summary>
		/// <param name="reader">The BitReader to from from.</param>
		/// <param name="aligned">Determines whether all read operations should be byte-aligned.</param>
		public BitReaderStream(BitReader reader, bool aligned)
		{
			this.reader = reader;
			this.aligned = aligned;
		}

		#endregion

		#region Methods

		/// <inheritdoc/>
		public override void Flush()
		{

		}

		/// <inheritdoc/>
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		/// <inheritdoc/>
		public override int ReadByte()
		{
			if (this.aligned || this.reader.IsAligned)
			{
				this.reader.Align();
				return this.reader.Stream.ReadByte();
			}

			return (int)this.reader.ReadBits(8);
		}

		/// <inheritdoc/>
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (this.aligned || this.reader.IsAligned)
			{
				this.reader.Align();
				return this.reader.Stream.Read(buffer, offset, count);
			}

			int remaining = count;
			while (remaining >= 8)
			{
				buffer.WriteUInt64L(offset, this.reader.ReadBits(64));
				offset += 8;
				remaining -= 8;
			}

			while (remaining > 0)
			{
				buffer[offset++] = (byte)this.reader.ReadBits(8);
				--remaining;
			}

			return count;
		}

		/// <inheritdoc/>
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		#endregion
	}
}
