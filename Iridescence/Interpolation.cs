﻿using System;
using System.Linq.Expressions;

namespace Iridescence
{
	/// <summary>
	/// Represents a generic interpolation function.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="a"></param>
	/// <param name="b"></param>
	/// <param name="t"></param>
	/// <returns></returns>
	public delegate T InterpolationFunc<T>(T a, T b, float t);

	/// <summary>
	/// Generic value interpolater.
	/// </summary>
	public static class Interpolation<T>
	{
		#region Fields

		private static InterpolationFunc<T> linear;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the linear interpolation function for the generic type.
		/// </summary>
		public static InterpolationFunc<T> Linear
		{
			get
			{
				if (linear == null)
				{
					ParameterExpression paramA = Expression.Parameter(typeof(T));
					ParameterExpression paramB = Expression.Parameter(typeof(T));
					ParameterExpression paramT = Expression.Parameter(typeof(float));

					UnaryExpression paramAFloat = Expression.Convert(paramA, typeof(float));
					UnaryExpression paramBFloat = Expression.Convert(paramB, typeof(float));
					
					BinaryExpression expr = Expression.Add(Expression.Multiply(paramAFloat, Expression.Subtract(Expression.Constant(1f), paramT)), Expression.Multiply(paramBFloat, paramT));
					UnaryExpression converted = Expression.Convert(expr, typeof(T));
					linear = Expression.Lambda<InterpolationFunc<T>>(converted, paramA, paramB, paramT).Compile();
				}

				return linear;
			}
			set => linear = value;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Cosine interpolation between a and b.
		/// </summary>
		/// <param name="a">The start value.</param>
		/// <param name="b">The end value.</param>
		/// <param name="t">The interpolation factor, clamped between 0.0 and 1.0.</param>
		/// <returns></returns>
		public static T Cosine(T a, T b, float t)
		{
			return Linear(a, b, 0.5f + -0.5f * (float)Math.Cos(t * Math.PI));
		}

		#endregion
	}
}
