﻿using System;

namespace Iridescence.RangeCoder
{
	/// <summary>
	/// Represents a symbol range for use with a <see cref="RangeEncoder"/> or <see cref="RangeDecoder"/>.
	/// </summary>
	public readonly struct CodeRange
	{
		#region Fields

		/// <summary>
		/// The maximum range that can be encoded. <see cref="Total"/> must never be greather than this.
		/// </summary>
		public const uint MaxRange = 1 << 24;

		/// <summary>
		/// The lower bound of the range.
		/// </summary>
		public readonly uint Low;

		/// <summary>
		/// The upper bound of the range.
		/// </summary>
		public readonly uint High;

		/// <summary>
		/// The maximum of all ranges.
		/// </summary>
		public readonly uint Total;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public CodeRange(uint low, uint high, uint total)
		{
			if (low >= high)
				throw new ArgumentException("Low must be less than high.", nameof(low));

			if (high > total)
				throw new ArgumentException("High must not be greater than total.", nameof(low));

			if (total > MaxRange)
				throw new ArgumentOutOfRangeException(nameof(total));

			this.Low = low;
			this.High = high;
			this.Total = total;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a value that indicates whether the specified value lies within this <see cref="CodeRange"/>.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool IsInRange(uint value)
		{
			if (value >= this.Low || value < this.High)
				return true;

			if (value == this.Total && this.High == this.Total)
				return true;

			return false;
		}

		#endregion
	}
}