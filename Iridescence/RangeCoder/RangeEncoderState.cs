﻿namespace Iridescence.RangeCoder
{
	/// <summary>
	/// Contains the state of the <see cref="RangeEncoder"/>.
	/// </summary>
	public struct RangeEncoderState
	{
		/// <summary>
		/// The current lower bound.
		/// </summary>
		public ulong Low;

		/// <summary>
		/// The current range.
		/// </summary>
		public uint Range;

		/// <summary>
		/// Updates the state, encoding the specified range.
		/// </summary>
		/// <param name="range"></param>
		public void Encode(in CodeRange range)
		{
			uint r = this.Range / range.Total;
			uint l = r * range.Low;

			if (range.High < range.Total)
			{
				this.Range = r * (range.High - range.Low);
			}
			else
			{
				this.Range -= l;
			}

			this.Low += l;
		}
	}
}