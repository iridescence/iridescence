﻿using System.IO;

namespace Iridescence.RangeCoder
{
	/// <summary>
	/// Implements an arithmetic/range decoder, the counterpart to the <see cref="RangeEncoder"/>.
	/// </summary>
	public sealed class RangeDecoder
	{
		#region Fields

		private readonly Stream stream;
		private RangeDecoderState state;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public RangeDecoder(Stream stream)
		{
			this.stream = stream;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the range decoder, reading a total of 5 bytes from the input stream where the first byte must be 0.
		/// </summary>
		public void Begin()
		{
			//int input = this.stream.ReadByte();
			//if (input < 0)
			//	throw new EndOfStreamException();

			//if (input != 0)
			//	throw new InvalidDataException("Range decoder corrupted.");

			this.state.Code = 0;
			this.state.Low = 0;
			this.state.Range = uint.MaxValue;

			for (int i = 0; i < 4; i++)
			{
				this.readLow();
			}
			
			if (this.state.Code == this.state.Range)
				throw new InvalidDataException("Range decoder corrupted.");
		}

		/// <summary>
		/// Ends the decoding.
		/// </summary>
		public void End()
		{
			if (this.state.Low != this.state.Code)
				throw new InvalidDataException("Range decoder corrupted.");

			this.state.Code = 0;
			this.state.Low = 0;
			this.state.Range = 0;
		}

		/// <summary>
		/// Renormalizes the range decoder.
		/// </summary>
		private void normalize()
		{
			while (this.state.Range < CodeRange.MaxRange)
			{
				this.state.Range <<= 8;
				this.readLow();
			}
		}

		/// <summary>
		/// Partially reads the 'Code' value.
		/// </summary>
		private void readLow()
		{
			int input = this.stream.ReadByte();
			if (input < 0)
				throw new EndOfStreamException();

			this.state.Code = (this.state.Code << 8) | (byte)input;
			this.state.Low = (this.state.Low << 8);
		}
		
		/// <summary>
		/// Reads bits directly.
		/// This is the same as decoding a 0 as range (0,1) and a 1 as range (1,2).
		/// </summary>
		/// <param name="numBits"></param>
		/// <returns></returns>
		public uint ReadVerbatim(int numBits)
		{
			uint result = 0;

			do
			{
				uint half = this.state.Range >> 1;
				uint current = (this.state.Code - this.state.Low) - half;
				uint mask = 0 - (current >> 31);

				this.state.Low += half & ~mask;
				this.state.Range = half;
				this.normalize();

				result = (result << 1) | (mask + 1);
			} while (--numBits > 0);

			return result;
		}

		/// <summary>
		/// Returns the current value of the decoder.
		/// A consumer must check in which of the expected ranges this value falls. After it determined the correct range, <see cref="ReadRange"/> with that range must be called.
		/// </summary>
		/// <param name="total">The total range of all possible ranges.</param>
		/// <returns></returns>
		public uint GetCurrent(uint total)
		{
			return this.state.GetCurrent(total);
		}

		/// <summary>
		/// Reads the specified <see cref="CodeRange"/>.
		/// </summary>
		/// <param name="range"></param>
		public void ReadRange(in CodeRange range)
		{
			this.state.Decode(range);
			this.normalize();
		}

		/// <summary>
		/// Reads a value using the speified <see cref="IRangeCodec{T}"/>.
		/// </summary>
		/// <typeparam name="TCodec"></typeparam>
		/// <typeparam name="T"></typeparam>
		/// <param name="codec"></param>
		/// <returns></returns>
		public T Read<TCodec, T>(ref TCodec codec)
			where TCodec : IRangeCodec<T>
		{
			T result = codec.Decode(ref this.state);
			this.normalize();
			return result;
		}

		#endregion
	}
}