﻿namespace Iridescence.RangeCoder
{
	/// <summary>
	/// Contains the state of the <see cref="RangeDecoder"/>
	/// </summary>
	public struct RangeDecoderState
	{
		/// <summary>
		/// The current decoder value read from the stream.
		/// </summary>
		public uint Code;
		
		/// <summary>
		/// The current lower bound.
		/// </summary>
		public uint Low;

		/// <summary>
		/// The current range.
		/// </summary>
		public uint Range;

		/// <summary>
		/// Returns the current value of the decoder.
		/// A consumer must check in which of the expected ranges this value falls. After it determined the correct range, <see cref="Decode"/> with that range must be called.
		/// </summary>
		/// <param name="total">The total range of all possible ranges.</param>
		/// <returns></returns>
		public uint GetCurrent(uint total)
		{
			return (this.Code - this.Low) / (this.Range / total);
		}

		/// <summary>
		/// Updates the state, decoding the specified range.
		/// </summary>
		/// <param name="range"></param>
		public void Decode(in CodeRange range)
		{
			uint r = this.Range / range.Total;
			uint l = r * range.Low;

			if (range.High < range.Total)
			{
				this.Range = r * (range.High - range.Low);
			}
			else
			{
				this.Range -= l;
			}

			this.Low += l;
		}
	}
}