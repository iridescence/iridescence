﻿namespace Iridescence.RangeCoder
{
	/// <summary>
	/// An interface that implements encoding symbols to a <see cref="RangeEncoder"/> and decoding them from a <see cref="RangeDecoder"/>
	/// </summary>
	public interface IRangeCodec<T>
	{
		#region Methods

		void Encode(ref RangeEncoderState state, T value);

		T Decode(ref RangeDecoderState state);

		#endregion
	}
}
