﻿using System.IO;

namespace Iridescence.RangeCoder
{
	/// <summary>
	/// Implements an arithmetic/range encoder.
	/// </summary>
	public sealed class RangeEncoder
	{
		#region Fields

		private readonly Stream stream;
		private RangeEncoderState state;
		
		private byte buffer;
		private uint carryCount;

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="RangeEncoder"/> class. 
		/// </summary>
		public RangeEncoder(Stream stream)
		{
			this.stream = stream;
		}
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Resets the encoder state to the initial state.
		/// </summary>
		public void Begin()
		{
			this.state.Low = 0;
			this.state.Range = uint.MaxValue;

			this.carryCount = 0;
			this.buffer = 0;
		}
		
		/// <summary>
		/// Flushes the data of the range encoder and ends encoding.
		/// </summary>
		public void End()
		{
			for (int j = 0; j < 4; ++j)
			{
				this.writeLow();
			}

			byte temp = this.buffer;
			for (uint i = 0; i < this.carryCount; ++i)
			{
				this.stream.WriteByte(temp);
				temp = 0xFF;
			}

			this.state.Low = 0;
			this.state.Range = 0;

			this.carryCount = 0;
			this.buffer = 0;
		}
		
		/// <summary>
		/// Renormalizes the encoder.
		/// </summary>
		private void normalize()
		{
			while (this.state.Range < CodeRange.MaxRange)
			{
				this.state.Range <<= 8;
				this.writeLow();
			}
		}

		/// <summary>
		/// Partially flushes the 'Low' value to the stream.
		/// </summary>
		private void writeLow()
		{
			unchecked
			{
				int overflow = 0;
				ulong l = this.state.Low;
				uint n = this.carryCount;
	
				if (l < 0xFF000000u ||
				    (overflow = (int)(l >> 32)) != 0 ||
				    n == uint.MaxValue)
				{
					byte temp = this.buffer;

					for (uint i = 0; i < n; ++i)
					{
						this.stream.WriteByte((byte)(temp + overflow));
						temp = 0xFF;
					}

					n = 0;
					this.buffer = (byte)(l >> 24);
				}

				this.carryCount = n + 1;
				this.state.Low = (uint)(l << 8);
			}
		}

		/// <summary>
		/// Writes the specified <see cref="CodeRange"/>.
		/// </summary>
		/// <param name="range"></param>
		public void WriteRange(in CodeRange range)
		{
			this.state.Encode(range);
			this.normalize();
		}

		/// <summary>
		/// Writes a value using the specified <see cref="IRangeCodec{T}"/>.
		/// </summary>
		/// <typeparam name="TCodec"></typeparam>
		/// <typeparam name="T"></typeparam>
		/// <param name="codec"></param>
		/// <param name="value"></param>
		public void Write<TCodec, T>(ref TCodec codec, T value)
			where TCodec : IRangeCodec<T>
		{
			codec.Encode(ref this.state, value);
			this.normalize();
		}

		/// <summary>
		/// Encodes bits directly.
		/// This is the same as encoding a 0 as range (0,1) and a 1 as range (1,2).
		/// </summary>
		/// <param name="value">An integer containing the bit values.</param>
		/// <param name="numBits">The number of bits to encode</param>
		public void WriteVerbatim(uint value, int numBits)
		{
			for (int i = numBits - 1; i >= 0; i--)
			{
				uint half = this.state.Range >> 1;
				uint mask = 0 - ((value >> i) & 1);

				this.state.Low += half & mask;
				this.state.Range = half;
				this.normalize();
			} 
		}

		#endregion
	}
}
