﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence
{
	/// <summary>
	/// Represents a dictionary that is layered on top of another dictionary.
	/// All write operations will go to this dictionary, but read operations will first this dictionary, and if the value does not exist, the base dictionary will be queried.
	/// </summary>
	public class LayeredDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>
	{
		#region Fields

		private IReadOnlyDictionary<TKey, TValue> baseDictionary;
		private readonly IDictionary<TKey, TValue> myDictionary;
		private readonly HashSet<TKey> maskedKeys;

		private KeyCollection keys;
		private ValueCollection values;

		#endregion

		#region Properties

		public LayeredDictionaryOptions Options { get; }

		public ICollection<TKey> Keys => this.keys ?? (this.keys = new KeyCollection(this));

		IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => this.Keys;

		public ICollection<TValue> Values => this.values ?? (this.values = new ValueCollection(this));
		
		IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => this.Values;

		public bool IsReadOnly => false;

		public int Count
		{
			get
			{
				int count = 0;
				foreach (var _ in this)
				{
					++count;
				}

				return count;
			}
		}

		public TValue this[TKey key]
		{
			get
			{
				if (!this.TryGetValue(key, out TValue value))
					throw new KeyNotFoundException();

				return value;
			}
			set
			{
				if ((this.Options & LayeredDictionaryOptions.ShadowBaseKeys) == 0)
				{
					if (this.baseDictionary.ContainsKey(key))
					{
						throw new InvalidOperationException("The specified key already exists in the base dictionary and can not be overwritten.");
					}
				}

				this.myDictionary[key] = value;
			}
		}

		public IEnumerable<TKey> MaskedKeys => this.maskedKeys;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="LayeredDictionary{TKey,TValue}"/> class. 
		/// </summary>
		public LayeredDictionary(IReadOnlyDictionary<TKey, TValue> baseDictionary, IDictionary<TKey, TValue> myDictionary, LayeredDictionaryOptions options = LayeredDictionaryOptions.None)
		{
			this.baseDictionary = baseDictionary ?? throw new ArgumentNullException(nameof(baseDictionary));
			this.myDictionary = myDictionary ?? throw new ArgumentNullException(nameof(myDictionary));
			this.maskedKeys = new HashSet<TKey>();
			this.Options = options;
		}

		#endregion
		
		#region Methods
		
		public void Clear()
		{
			if ((this.Options & LayeredDictionaryOptions.ThrowOnRemove) != 0)
				throw new InvalidOperationException("The dictionary can not be cleared.");

			this.maskedKeys.Clear();
			this.myDictionary.Clear();

			if ((this.Options & LayeredDictionaryOptions.MaskBaseKeys) != 0)
			{
				if ((this.Options & LayeredDictionaryOptions.MaskNonExistingBaseKeys) != 0)
				{
					this.baseDictionary = null;
				}
				else
				{
					foreach (TKey key in this.baseDictionary.Keys)
						this.maskedKeys.Add(key);
				}
			}
		}

		public void Add(TKey key, TValue value)
		{
			if (this.baseDictionary != null && (this.Options & LayeredDictionaryOptions.ShadowBaseKeys) == 0)
			{
				if (!this.maskedKeys.Contains(key) && this.baseDictionary.ContainsKey(key))
					throw new ArgumentException("The specified key already exists in the base dictionary and can not be overwritten.");
			}

			this.myDictionary.Add(key, value);
		}

		void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
		{
			this.Add(item.Key, item.Value);
		}

		public bool Remove(TKey key)
		{
			if ((this.Options & LayeredDictionaryOptions.ThrowOnRemove) != 0)
				throw new InvalidOperationException("The dictionary does not allow removing items.");

			if ((this.Options & LayeredDictionaryOptions.MaskBaseKeys) != 0)
			{
				bool result = false;

				if (this.baseDictionary != null)
				{
					if (this.baseDictionary.ContainsKey(key))
					{
						result = this.maskedKeys.Add(key);
					}
					else if ((this.Options & LayeredDictionaryOptions.MaskNonExistingBaseKeys) != 0)
					{
						this.maskedKeys.Add(key);
					}
				}

				return result | this.myDictionary.Remove(key);
			}

			if (!this.myDictionary.Remove(key))
				return false;

			if (this.baseDictionary.ContainsKey(key))
				return false;

			return true;
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
		{
			if ((this.Options & LayeredDictionaryOptions.ThrowOnRemove) != 0)
				throw new InvalidOperationException("The dictionary does not allow removing items.");

			if (!this.TryGetValue(item.Key, out TValue value))
				return false;

			if (!Equals(item.Value, value))
				return false;

			return this.Remove(item.Key);
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			value = default;

			if ((this.Options & LayeredDictionaryOptions.ShadowBaseKeys) == 0)
			{
				if (this.baseDictionary != null &&
				    !this.maskedKeys.Contains(key) &&
				    this.baseDictionary.TryGetValue(key, out value))
					return true;

				return this.myDictionary.TryGetValue(key, out value);
			}

			if (this.myDictionary.TryGetValue(key, out value))
				return true;

			if (this.baseDictionary == null || this.maskedKeys.Contains(key))
				return false;

			return this.baseDictionary.TryGetValue(key, out value);
		}

		public bool ContainsKey(TKey key)
		{
			return this.TryGetValue(key, out _);
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
		{
			if (this.myDictionary.Contains(item))
				return true;

			if (this.baseDictionary == null)
				return false;

			if (this.maskedKeys.Contains(item.Key))
				return false;

			return this.baseDictionary.Contains(item);
		}

		void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			foreach (KeyValuePair<TKey, TValue> pair in this)
			{
				array[arrayIndex++] = pair;
			}
		}

		private IEnumerator<KeyValuePair<TKey, TValue>> enumerateShadowed()
		{
			HashSet<TKey> yieldedKeys = new HashSet<TKey>();

			foreach (KeyValuePair<TKey, TValue> pair in this.myDictionary)
			{
				yield return pair;
				yieldedKeys.Add(pair.Key);
			}

			foreach (KeyValuePair<TKey, TValue> pair in this.baseDictionary)
			{
				if (this.maskedKeys.Contains(pair.Key))
					continue;

				if (!yieldedKeys.Contains(pair.Key))
					yield return pair;
			}
		}

		private IEnumerator<KeyValuePair<TKey, TValue>> enumerateNonShadowed()
		{
			HashSet<TKey> yieldedKeys = new HashSet<TKey>();

			foreach (KeyValuePair<TKey, TValue> pair in this.baseDictionary)
			{
				if (this.maskedKeys.Contains(pair.Key))
					continue;

				yield return pair;
				yieldedKeys.Add(pair.Key);
			}

			foreach (KeyValuePair<TKey, TValue> pair in this.myDictionary)
			{
				if (!yieldedKeys.Contains(pair.Key))
					yield return pair;
			}
		}

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			if (this.baseDictionary == null)
			{
				return this.myDictionary.GetEnumerator();
			}

			if ((this.Options & LayeredDictionaryOptions.ShadowBaseKeys) != 0)
			{
				return this.enumerateShadowed();
			}

			return this.enumerateNonShadowed();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((IEnumerable)this).GetEnumerator();
		}

		#endregion

		#region Nested Types

		private sealed class KeyCollection : ICollection<TKey>
		{
			private readonly LayeredDictionary<TKey, TValue> dict;

			public int Count => this.dict.Count;

			public bool IsReadOnly => true;

			public KeyCollection(LayeredDictionary<TKey, TValue> dict)
			{
				this.dict = dict;
			}

			public IEnumerator<TKey> GetEnumerator()
			{
				foreach (KeyValuePair<TKey, TValue> pair in this.dict)
				{
					yield return pair.Key;
				}
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			public void Add(TKey item)
			{
				throw new NotSupportedException();
			}

			public bool Remove(TKey item)
			{
				throw new NotSupportedException();
			}

			public void Clear()
			{
				throw new NotSupportedException();
			}

			public bool Contains(TKey item)
			{
				if(item == null)
					return false;
				return this.dict.ContainsKey(item);
			}

			public void CopyTo(TKey[] array, int arrayIndex)
			{
				foreach (TKey key in this)
				{
					array[arrayIndex++] = key;
				}
			}
		}

		private sealed class ValueCollection : ICollection<TValue>
		{
			private readonly LayeredDictionary<TKey, TValue> dict;

			public int Count => this.dict.Count;

			public bool IsReadOnly => true;

			public ValueCollection(LayeredDictionary<TKey, TValue> dict)
			{
				this.dict = dict;
			}

			public IEnumerator<TValue> GetEnumerator()
			{
				foreach (KeyValuePair<TKey, TValue> pair in this.dict)
				{
					yield return pair.Value;
				}
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			public void Add(TValue item)
			{
				throw new NotSupportedException();
			}

			public bool Remove(TValue item)
			{
				throw new NotSupportedException();
			}

			public void Clear()
			{
				throw new NotSupportedException();
			}

			public bool Contains(TValue item)
			{
				return this.dict.Any(p => Equals(p.Value, item));
			}

			public void CopyTo(TValue[] array, int arrayIndex)
			{
				foreach (TValue value in this)
				{
					array[arrayIndex++] = value;
				}
			}
		}
		
		#endregion
	}

	/// <summary>
	/// Options for the <see cref="LayeredDictionary{TKey,TValue}"/>.
	/// </summary>
	[Flags]
	public enum LayeredDictionaryOptions
	{
		None = 0,

		/// <summary>
		/// When adding a key that already exists in the base dictionary and this flag is set, the base key will be shadowed by the new value of the layered dictionary.
		/// If this flag is not set, an <see cref="ArgumentException"/> is thrown in <see cref="LayeredDictionary{TKey,TValue}.Add"/>.
		/// </summary>
		ShadowBaseKeys = 1,

		/// <summary>
		/// When removing a key from the layered dictionary, the key will be "masked" from the base dictionary.
		/// The base dictionary will no longer be queried for this key and a new value may be assigned in the layered dictionary.
		/// The key is removed from the new dictionary regardless of whether this flag is set or not.
		/// Only keys that actually exist in the base dictionary will be masked
		/// If this flag is not set, <see cref="LayeredDictionary{TKey,TValue}.Remove"/> will return false without removing the key from the base dictionary.
		/// </summary>
		MaskBaseKeys = 2,

		/// <summary>
		/// This flag has the same effect as <see cref="MaskBaseKeys"/> but keys that do not exist in the base dictionary will also be masked so that even if they get added to the base dictionary at some point, the layered dictionary will ignore it.
		/// </summary>
		MaskNonExistingBaseKeys = MaskBaseKeys | 4,

		/// <summary>
		/// If this flag is set, removing from or clearing the layered dictionary will throw an <see cref="InvalidOperationException"/> so that the dictionary can essentially become a "copy on write" dictionary for adding.
		/// </summary>
		ThrowOnRemove = 8,
	}
}
