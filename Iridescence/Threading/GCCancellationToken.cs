﻿using System.Runtime.CompilerServices;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Provides a <see cref="CancellationToken"/> that is cancelled when an object is garbage collected.
	/// </summary>
	public static class GCCancellationToken
	{
		#region Fields

		private static readonly ConditionalWeakTable<object, Ephemeron> tokens;

		#endregion

		#region Constructor

		static GCCancellationToken()
		{
			tokens = new ConditionalWeakTable<object, Ephemeron>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a cancellation token that is cancelled when the specified object is garbage collected.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static CancellationToken GetToken(object obj)
		{
			Ephemeron e = tokens.GetOrCreateValue(obj);
			return e.Token;
		}

		#endregion

		#region Nested Types

		private sealed class Ephemeron
		{
			private readonly CancellationTokenSource cancellationTokenSource;

			public CancellationToken Token => this.cancellationTokenSource.Token;

			public Ephemeron()
			{
				this.cancellationTokenSource = new CancellationTokenSource();
			}

			~Ephemeron()
			{
				this.cancellationTokenSource.Cancel();
			}
		}

		#endregion
	}
}
