﻿using System;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Implements an <see cref="ITaskDispatcher"/> for a <see cref="TaskScheduler"/>.
	/// </summary>
	public class TaskSchedulerDispatcher : ITaskDispatcher
	{
		private readonly TaskFactory taskFactory;
		private readonly IValueTaskScheduler valueTaskScheduler;

		public TaskSchedulerDispatcher(TaskScheduler scheduler)
		{
			this.valueTaskScheduler = scheduler as IValueTaskScheduler;
			this.taskFactory = new TaskFactory(scheduler);
		}

		public ValueTask InvokeAsync(Action action)
		{
			if (this.valueTaskScheduler != null && this.valueTaskScheduler.TryStartValueTask(action, out ValueTask task))
			{
				return task;
			}

			return new ValueTask(this.taskFactory.StartNew(action));
		}
		
		public ValueTask<T> InvokeAsync<T>(Func<T> func)
		{
			if (this.valueTaskScheduler != null && this.valueTaskScheduler.TryStartValueTask(func, out ValueTask<T> task))
			{
				return task;
			}

			return new ValueTask<T>(this.taskFactory.StartNew(func));
		}

		public void Invoke(Action action)
		{
			ValueTask task = this.InvokeAsync(action);
			if (!task.IsCompleted)
			{
				task.AsTask().Wait();
			}
		}

		public T Invoke<T>(Func<T> func)
		{
			ValueTask<T> task = this.InvokeAsync(func);
			return task.Result;
		}

		public void Post(Action action)
		{
			this.InvokeAsync(action);
		}
	}
}