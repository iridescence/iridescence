﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Represents a pulse mechanism that can be using with 'await'.
	/// </summary>
	public class AsyncPulse<T> where T : class
	{
		#region Fields

		private AsyncPulseAwaiter<T> current;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AsyncPulse{T}"/> class. 
		/// </summary>
		public AsyncPulse()
		{
			this.current = new AsyncPulseAwaiter<T>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Invokes all currently waiting callbacks with the specified result and creates a new <see cref="AsyncPulseAwaiter{T}"/> for new callbacks, in an atomic operation.
		/// </summary>
		/// <param name="result"></param>
		public void Pulse(T result)
		{
			Interlocked.Exchange(ref this.current, new AsyncPulseAwaiter<T>()).Complete(result);
		}

		/// <summary>
		/// Invokes all currently waiting callbacks, but does not create a new <see cref="AsyncPulseAwaiter{T}"/>. 
		/// </summary>
		/// <param name="result"></param>
		public void Complete(T result)
		{
			this.current.Complete(result);
		}

		/// <summary>
		/// Returns the current <see cref="AsyncPulseAwaiter{T}"/>.
		/// </summary>
		/// <returns></returns>
		public AsyncPulseAwaiter<T> GetAwaiter()
		{
			return Volatile.Read(ref this.current);
		}

		/// <summary>
		/// Returns the current <see cref="AsyncPulseAwaiter{T}"/>, but with the option to cancel the await using a <see cref="CancellationToken"/>.
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public AsyncPulseAwaiter<T> GetAwaiter(CancellationToken cancellationToken)
		{
			return Volatile.Read(ref this.current).GetAwaiter(cancellationToken);
		}

		#endregion
	}

	/// <summary>
	/// Implements the awaitable pattern for <see cref="AsyncPulse{T}"/>.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public sealed class AsyncPulseAwaiter<T> : INotifyCompletion where T:class
	{
		private Action pendingContinuations;
		private ConcurrentFlag isCompleted;
		private T result;
		private Exception exception;

		public bool IsCompleted => this.isCompleted.IsSet;

		internal AsyncPulseAwaiter()
		{
			this.isCompleted = new ConcurrentFlag();
		}

		internal void Complete(T result)
		{
			Interlocked.Exchange(ref this.result, result);

			if (!this.isCompleted.TrySet())
				throw new InvalidOperationException("AsyncPulseAwaiter is already complete.");

			Interlocked.Exchange(ref this.pendingContinuations, null)?.Invoke();
		}

		internal void Fault(Exception exception)
		{
			if (exception == null)
				throw new ArgumentNullException(nameof(exception));

			Interlocked.Exchange(ref this.exception, exception);
			this.Complete(default(T));
		}

		public AsyncPulseAwaiter<T> GetAwaiter()
		{
			return this;
		}

		public AsyncPulseAwaiter<T> GetAwaiter(CancellationToken cancellationToken)
		{
			AsyncPulseAwaiter<T> cancellableAwaitable = new AsyncPulseAwaiter<T>();

			ConcurrentFlag completed = new ConcurrentFlag();

			cancellationToken.Register(() =>
			{
				if (!completed.TrySet())
					return;

				cancellableAwaitable.Fault(new OperationCanceledException(cancellationToken));
			});

			this.OnCompleted(() =>
			{
				if (!completed.TrySet())
					return;

				T myResult;
				try
				{
					myResult = this.GetResult();
				}
				catch (Exception ex)
				{
					cancellableAwaitable.Fault(ex);
					return;
				}
				cancellableAwaitable.Complete(myResult);
			});

			return cancellableAwaitable;
		}

		public void OnCompleted(Action continuation)
		{
			if (continuation == null)
				throw new ArgumentNullException(nameof(continuation));

			var oldContinuations = Volatile.Read(ref this.pendingContinuations);
			for (;;)
			{
				var newContinuations = (oldContinuations + continuation);
				var actualContinuations = Interlocked.CompareExchange(ref this.pendingContinuations, newContinuations, oldContinuations);
				if (actualContinuations == oldContinuations)
					break;

				oldContinuations = actualContinuations;
			}

			if (this.IsCompleted)
				Interlocked.Exchange(ref this.pendingContinuations, null)?.Invoke();
		}

		public T GetResult()
		{
			if (!this.IsCompleted)
				throw new NotSupportedException("Synchronous waits are not supported. Use 'await' or OnCompleted to wait asynchronously");

			Exception ex = Volatile.Read(ref this.exception);
			if (ex != null)
				throw ex;

			return Volatile.Read(ref this.result);
		}
	}
}
