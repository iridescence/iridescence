﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Implements a <see cref="TaskScheduler"/> on top of a <see cref="SynchronizationContext"/>.
	/// Queued tasks are posted to the context and sent if inlining is attempted.
	/// </summary>
	public class SynchronizationContextScheduler : TaskScheduler
	{
		#region Fields

		private readonly SynchronizationContext context;
		private readonly SendOrPostCallback postCallbackInstance;

		#endregion

		#region Properties

		public override int MaximumConcurrencyLevel => 1;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SynchronizationContextScheduler"/> class. 
		/// </summary>
		public SynchronizationContextScheduler(SynchronizationContext context)
		{
			this.context = context;
			this.postCallbackInstance = this.postCallback;
		}

		#endregion

		#region Methods

		private void postCallback(object state)
		{
			Task task = (Task)state;
			this.TryExecuteTask(task);
		}

		protected override void QueueTask(Task task)
		{
			this.context.Post(this.postCallbackInstance, task);
		}

		protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
		{
			// Send Task and check whether it ran synchronously.
			ConcurrentFlag flag = new ConcurrentFlag();
			this.context.Send(state =>
			{
				if (flag.TrySet())
					this.TryExecuteTask(task);
			}, null);

			if (flag.TrySet())
				return false;

			return true;
		}

		protected override IEnumerable<Task> GetScheduledTasks()
		{
			return Enumerable.Empty<Task>();
		}

		#endregion
	}
}
