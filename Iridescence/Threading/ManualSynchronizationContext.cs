﻿using System.Collections.Concurrent;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Implements a <see cref="SynchronizationContext"/> where all asynchronous messages are dispatched manually.
	/// </summary>
	public class ManualSynchronizationContext : SynchronizationContext
	{
		#region Fields

		private readonly ConcurrentQueue<Item> queue;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ManualSynchronizationContext"/> class.
		/// </summary>
		public ManualSynchronizationContext()
		{
			this.queue = new ConcurrentQueue<Item>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Dispatches all posted messages.
		/// </summary>
		public int Dispatch()
		{
			int n = 0;

			while (this.queue.TryDequeue(out Item item))
			{
				item.Callback(item.State);
				++n;
			}

			return n;
		}

		public override void Post(SendOrPostCallback d, object state)
		{
			this.queue.Enqueue(new Item(d, state));
		}

		public override SynchronizationContext CreateCopy()
		{
			return new ManualSynchronizationContext();
		}

		#endregion

		#region Nested Types

		private readonly struct Item
		{
			public readonly SendOrPostCallback Callback;
			public readonly object State;

			public Item(SendOrPostCallback callback, object state)
			{
				this.Callback = callback;
				this.State = state;
			}
		}

		#endregion
	}
}
