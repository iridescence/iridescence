﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace Iridescence
{
	public class ManualDispatcher : ITaskDispatcher
	{
		private readonly ConcurrentQueue<Action> queue;

		public ManualDispatcher()
		{
			this.queue = new();
		}

		public void DispatchAll()
		{
			int count = this.queue.Count;

			while (count-- > 0)
			{
				if (!this.queue.TryDequeue(out Action task))
					break;

				task();
			}
		}

		public ValueTask InvokeAsync(Action action)
		{
			TaskCompletionSource<bool> tcs = new();

			this.queue.Enqueue(() =>
			{
				action();
				tcs.SetResult(true);
			});

			return new ValueTask(tcs.Task);
		}

		public ValueTask<T> InvokeAsync<T>(Func<T> func)
		{
			TaskCompletionSource<T> tcs = new();

			this.queue.Enqueue(() =>
			{
				try
				{
					T res = func();
					tcs.SetResult(res);
				}
				catch (Exception ex)
				{
					tcs.SetException(ex);
				}
			});

			return new ValueTask<T>(tcs.Task);
		}

		public void Invoke(Action action)
		{
			ValueTask task = this.InvokeAsync(action);
			if (!task.IsCompleted)
			{
				task.AsTask().Wait();
			}
		}

		public T Invoke<T>(Func<T> func)
		{
			ValueTask<T> task = this.InvokeAsync(func);
			return task.Result;
		}

		public void Post(Action action)
		{
			this.InvokeAsync(action);
		}
	}
}
