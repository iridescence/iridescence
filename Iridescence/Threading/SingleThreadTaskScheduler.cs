﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Implements a task scheduler that executes all tasks on a single thread.
	/// </summary>
	public sealed class SingleThreadTaskScheduler : TaskScheduler, IValueTaskScheduler
	{
		#region Fields

		private readonly ConcurrentQueue<Task> queue;
		private readonly SemaphoreSlim semaphore;

		private Thread worker;

		#endregion

		#region Properties

		public override int MaximumConcurrencyLevel => 1;

		/// <summary>
		/// Gets the number of tasks currently waiting to be run on this scheduler.
		/// </summary>
		public int TaskCount => this.queue.Count;

		private bool canRunImmediately => Thread.CurrentThread == this.worker/* && this.queue.Count == 0*/;

		#endregion

		#region Constructors

		public SingleThreadTaskScheduler()
		{
			this.queue = new ConcurrentQueue<Task>();
			this.semaphore = new SemaphoreSlim(0);
		}

		#endregion

		#region Methods

		private bool runSingleTask()
		{
			if (!this.queue.TryDequeue(out Task task))
				return false;

			if (!task.IsCompleted)
			{
				this.TryExecuteTask(task);
			}

			return true;
		}

		private void run(CancellationToken cancellationToken, bool exitWhenEmpty)
		{
			// Claim thread.
			if (Interlocked.CompareExchange(ref this.worker, Thread.CurrentThread, null) != null)
				throw new InvalidOperationException("Scheduler is already running.");

			try
			{
				// Run tasks until cancellation toked is cancelled.
				while (!cancellationToken.IsCancellationRequested)
				{
					if (!this.runSingleTask())
					{
						if (exitWhenEmpty)
							break;

						try
						{
							this.semaphore.Wait(cancellationToken);
						}
						catch (OperationCanceledException)
						{
							break;
						}
					}
				}
			}
			finally
			{
				// Release thread.
				this.worker = null;
			}
		}

		/// <summary>
		/// Runs the scheduler on the current thread until the specified <see cref="CancellationToken"/> is canceled.
		/// </summary>
		public void Run(CancellationToken cancellationToken)
		{
			this.run(cancellationToken, false);
		}

		/// <summary>
		/// Runs the scheduler until the task queue is empty or the specified <see cref="CancellationToken"/> is canceled.
		/// </summary>
		/// <param name="cancellationToken"></param>
		public void RunOnce(CancellationToken cancellationToken)
		{
			this.run(cancellationToken, true);
		}
		
		public bool TryStartValueTask(Action action, out ValueTask task)
		{
			if (this.canRunImmediately)
			{
				// Immediately execute task if we're already on the right thread and no other tasks are awaiting execution.
				action();
				task = new ValueTask();
				return true;
			}

			task = default;
			return false;
		}

		public bool TryStartValueTask<T>(Func<T> func, out ValueTask<T> task)
		{
			if (this.canRunImmediately)
			{
				// Immediately execute task if we're already on the right thread and no other tasks are awaiting execution.
				T result = func();
				task = new ValueTask<T>(result);
				return true;
			}

			task = default;
			return false;
		}

		protected override void QueueTask(Task task)
		{
			if (this.canRunImmediately)
			{
				// Immediately execute task if we're already on the right thread and no other tasks are awaiting execution.
				this.TryExecuteTask(task);
			}
			else
			{
				// Enqueue task and signal worker thread to do something.
				this.queue.Enqueue(task);
				this.semaphore.Release();
			}
		}

		protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
		{
			if (Thread.CurrentThread == this.worker)
			{
				return this.TryExecuteTask(task);
			}

			return false;
		}

		protected override IEnumerable<Task> GetScheduledTasks()
		{
			return this.queue;
		}
		
		#endregion
	}
}
