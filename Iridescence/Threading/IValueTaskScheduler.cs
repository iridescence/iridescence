﻿using System;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Represents an interface for a scheduler that is able to create <see cref="ValueTask">value tasks</see> when the current state allows it.
	/// </summary>
	public interface IValueTaskScheduler
	{
		#region Methods

		/// <summary>
		/// Tries to schedule the specified <see cref="Action"/> as a <see cref="ValueTask"/>.
		/// If the operation succeeds, the <see cref="ValueTask"/> is returned. If not, the user should instead run the action as normal <see cref="Task"/>.
		/// </summary>
		/// <param name="action">The action to run.</param>
		/// <param name="task">The resulting <see cref="ValueTask"/></param>
		/// <returns>True if the action was scheduled and a <see cref="ValueTask"/> has been created for it.</returns>
		bool TryStartValueTask(Action action, out ValueTask task);

		/// <summary>
		/// Tries to schedule the specified <see cref="Func{TResult}"/> as a <see cref="ValueTask{T}"/>.
		/// If the operation succeeds, the <see cref="ValueTask{T}"/> is returned. If not, the user should instead run the action as normal <see cref="Task{T}"/>.
		/// </summary>
		/// <param name="func">The function to run.</param>
		/// <param name="task">The resulting <see cref="ValueTask{T}"/></param>
		/// <returns>True if the action was scheduled and a <see cref="ValueTask{T}"/> has been created for it.</returns>
		bool TryStartValueTask<T>(Func<T> func, out ValueTask<T> task);

		#endregion
	}
	
}
