﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Implements a task scheduler that synchronously runs all tasks on the current thread.
	/// </summary>
	public sealed class CurrentThreadTaskScheduler : TaskScheduler, IValueTaskScheduler
	{
		#region Properties

		public override int MaximumConcurrencyLevel => 1;

		#endregion

		#region Methods

		protected override void QueueTask(Task task)
		{
			this.TryExecuteTask(task);
		}

		protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
		{
			return this.TryExecuteTask(task);
		}

		protected override IEnumerable<Task> GetScheduledTasks()
		{
			return Enumerable.Empty<Task>();
		}

		public bool TryStartValueTask(Action action, out ValueTask task)
		{
			action();
			task = new ValueTask();
			return true;
		}

		public bool TryStartValueTask<T>(Func<T> func, out ValueTask<T> task)
		{
			task = new ValueTask<T>(func());
			return true;
		}
		
		#endregion
	}
}
