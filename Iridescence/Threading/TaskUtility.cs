﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Task-related utility methods.
	/// </summary>
	public static class TaskUtility
	{
		#region Fields

		private	static readonly TaskFactory syncTaskFactory = new TaskFactory(CancellationToken.None, TaskCreationOptions.None, TaskContinuationOptions.None, new CurrentThreadTaskScheduler());
		
		#endregion
		
		#region Methods

		/// <summary>
		/// Waits for the <see cref="Task"/> to complete execution and catches <see cref="OperationCanceledException"/>.
		/// </summary>
		/// <param name="task"></param>
		/// <returns>True, if the <see cref="Task"/> ended normally. False if it was canceled and threw a <see cref="OperationCanceledException"/>.</returns>
		public static bool WaitAndCatchCancellation(this Task task)
		{
			try
			{
				task.Wait();
				return true;
			}
			catch (AggregateException ex) when (ex.InnerExceptions.All(e => e is OperationCanceledException))
			{
				return false;
			}
			catch (OperationCanceledException)
			{
				return false;
			}
		}

		public static T RunSync<T>(Func<Task<T>> func)
		{
			return syncTaskFactory.StartNew(func).Unwrap().ConfigureAwait(false).GetAwaiter().GetResult();
		}

		public static void RunSync(Func<Task> func)
		{
			syncTaskFactory.StartNew(func).Unwrap().ConfigureAwait(false).GetAwaiter().GetResult();
		}

		public static IDisposable WithSynchronizationContext(SynchronizationContext context)
		{
			SynchronizationContext current = SynchronizationContext.Current;
			SynchronizationContext.SetSynchronizationContext(context);

			return new DisposableDelegate(() =>
			{
				SynchronizationContext.SetSynchronizationContext(current);
			});
		}
		
		#endregion
	}
}
