﻿using System;
using System.Threading.Tasks;

namespace Iridescence
{
	/// <summary>
	/// Interface for task dispatching.
	/// </summary>
	public interface ITaskDispatcher
	{
		/// <summary>
		/// Asynchronously runs the specified delegate.
		/// </summary>
		/// <param name="action"></param>
		/// <returns></returns>
		ValueTask InvokeAsync(Action action);

		/// <summary>
		/// Asynchronously runs the specified delegate.
		/// </summary>
		/// <param name="func"></param>
		/// <returns></returns>
		ValueTask<T> InvokeAsync<T>(Func<T> func);

		/// <summary>
		/// Runs the specified delegate and blocks the calling thread until the operation is complete.
		/// </summary>
		/// <param name="action"></param>
		/// <returns></returns>
		void Invoke(Action action);

		/// <summary>
		/// Runs the specified delegate and blocks the calling thread until the operation is complete.
		/// </summary>
		/// <param name="func"></param>
		/// <returns></returns>
		T Invoke<T>(Func<T> func);

		/// <summary>
		/// Schedules the specified delegate to be run without blocking the calling thread.
		/// </summary>
		/// <param name="action"></param>
		/// <returns></returns>
		void Post(Action action);
	}
}
