﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Represents an empty <see cref="IReadOnlyList{T}"/>
	/// </summary>
	[Serializable]
	public sealed class EmptyReadOnlyList<T> : IReadOnlyList<T>
	{
		#region Fields

		/// <summary>
		/// An static instance of the class for the specified generic type.
		/// </summary>
		public static readonly EmptyReadOnlyList<T> Instance = new EmptyReadOnlyList<T>();

		#endregion
		
		#region Properties
		
		public T this[int index] => throw new IndexOutOfRangeException();

		public int Count => 0;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="EmptyReadOnlyList{T}"/> class. 
		/// </summary>
		public EmptyReadOnlyList()
		{
		
		}
		
		#endregion
		
		#region Methods
		
		public IEnumerator<T> GetEnumerator()
		{
			yield break;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}
}
