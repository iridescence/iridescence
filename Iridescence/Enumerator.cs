﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Implements an empty enumerator.
	/// </summary>
	public class EmptyEnumerator<T> : IEnumerator<T>
	{
		#region Fields

		public static readonly EmptyEnumerator<T> Instance = new EmptyEnumerator<T>();

		#endregion

		#region Properties

		public T Current => throw new InvalidOperationException();

		object IEnumerator.Current => this.Current;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="EmptyEnumerator{T}"/> class. 
		/// </summary>
		public EmptyEnumerator()
		{
		
		}
		
		#endregion
		
		#region Methods
		
		public bool MoveNext()
		{
			return false;
		}

		public void Reset()
		{

		}

		public void Dispose()
		{

		}
		
		#endregion
	}
}
