﻿using System;

namespace Iridescence
{
	/// <summary>
	/// Represents a method which handles an event in which a property's value changed. 
	/// </summary>
	/// <typeparam name="T">The type of the property.</typeparam>
	/// <param name="sender">The object which fired the event.</param>
	/// <param name="e">The event arguments.</param>
	public delegate void ValueChangedEventHandler<T>(object sender, ValueChangedEventArgs<T> e);

	/// <summary>
	/// Event args for property modification events.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ValueChangedEventArgs<T> : EventArgs
	{
		#region Properties

		/// <summary>
		/// Gets the old value of the modified property.
		/// The new value can be retreived directly from the sender.
		/// </summary>
		public T OldValue { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ValueChangedEventArgs
		/// </summary>
		/// <param name="oldValue">The old value of the changed property.</param>
		public ValueChangedEventArgs(T oldValue)
		{
			this.OldValue = oldValue;
		}

		#endregion
	}
}
