﻿using System;
using System.Collections.Concurrent;

namespace Iridescence
{
	/// <summary>
	/// Represents a resource pool.
	/// </summary>
	public class Pool<T> where T : class
	{
		#region Fields

		private readonly ConcurrentBag<T> pool;
		private readonly Func<T> factory;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the number of items in the pool.
		/// </summary>
		public int Count => this.pool.Count;
		
		/// <summary>
		/// Gets the capacity of the pool.
		/// </summary>
		public int Capacity { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="Pool{T}"/> with the specified factory and capacity.
		/// </summary>
		/// <param name="factory">A function that creates new instances in case the pool is empty.</param>
		public Pool(Func<T> factory)
			: this(factory, 0)
		{
			this.pool = new ConcurrentBag<T>();
			this.factory = factory;
		}

		/// <summary>
		/// Creates a new <see cref="Pool{T}"/> with the specified factory and capacity.
		/// </summary>
		/// <param name="factory">A function that creates new instances in case the pool is empty.</param>
		/// <param name="capacity">The capacity of the pool. Specify 0 for unlimited capacity.</param>
		public Pool(Func<T> factory, int capacity)
		{
			this.pool = new ConcurrentBag<T>();
			this.Capacity = capacity;
			this.factory = factory;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns an object from the pool.
		/// Creates a new object if the pool is empty.
		/// </summary>
		/// <returns></returns>
		public T Take()
		{
			if (this.pool.TryTake(out T item))
				return item;
			return this.factory();
		}

		/// <summary>
		/// Tries to return an object from the pool but does not create a new one if the pool is empty.
		/// </summary>
		/// <returns></returns>
		public bool TryTake(out T item)
		{
			return this.pool.TryTake(out item);
		}

		/// <summary>
		/// Puts the specified object into the pool.
		/// </summary>
		/// <param name="obj"></param>
		public bool Give(T obj)
		{
			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			if (this.Capacity > 0 && this.Count >= this.Capacity)
				return false;
			
			this.pool.Add(obj);
			return true;
		}

		#endregion
	}
}
