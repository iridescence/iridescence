﻿using System;

namespace Iridescence
{
	/// <summary>
	/// Event args for <see cref="MemoryHeap"/> resize events.
	/// </summary>
	public class MemoryHeapResizeEventArgs : EventArgs
	{
		#region Properties

		/// <summary>
		/// Gets the old size of the heap.
		/// </summary>
		public int OldSize { get; }

		/// <summary>
		/// Gets the new size of the heap.
		/// </summary>
		public int NewSize { get; }

		#endregion

		#region Constructors

		public MemoryHeapResizeEventArgs(int oldSize, int newSize)
		{
			this.OldSize = oldSize;
			this.NewSize = newSize;
		}

		#endregion
	}
}
