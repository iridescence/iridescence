namespace Iridescence
{
	/// <summary>
	/// The callback that is invoked for moved segments.
	/// </summary>
	/// <param name="segment">The original segment.</param>
	/// <param name="newIndex">The new index of the segment.</param>
	public delegate void MemoryHeapSegmentMoveCallback(MemoryHeapSegment segment, int newIndex);
}