﻿namespace Iridescence
{
	/// <summary>
	/// Implements a <see cref="MemoryHeap"/> using an array.
	/// </summary>
	public class ArrayHeap<T> : MemoryHeap
	{
		#region Fields

		private T[] array;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the array containing the data.
		/// </summary>
		public T[] Array => this.array;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ArrayHeap{T}"/> class. 
		/// </summary>
		public ArrayHeap(int initialCapacity)
			: base(initialCapacity)
		{
			this.array = new T[initialCapacity];
		}

		#endregion

		#region Methods

		protected override void Initialize(int index, int count)
		{
			int end = index + count;
			for (int i = index; i < end; ++i)
				this.array[i] = default(T);
		}

		protected override void Move(int fromIndex, int toIndex, int count)
		{
			base.Move(fromIndex, toIndex, count);
			System.Array.Copy(this.array, fromIndex, this.array, toIndex, count);
		}

		protected override void OnResized(MemoryHeapResizeEventArgs e)
		{
			base.OnResized(e);
			System.Array.Resize(ref this.array, e.NewSize);
		}

		#endregion
	}
}
