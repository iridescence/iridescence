namespace Iridescence
{
	/// <summary>
	/// Represents a segment of an <see cref="MemoryHeap"/>.
	/// </summary>
	public struct MemoryHeapSegment
	{
		public readonly int Index;
		public readonly int Count;

		public MemoryHeapSegment(int index, int count)
		{
			this.Index = index;
			this.Count = count;
		}

		public override string ToString()
		{
			return $"{this.Index}, {this.Count}";
		}
	}
}