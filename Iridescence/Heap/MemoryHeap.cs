﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence
{
	/// <summary>
	/// Implements a memory heap using an array.
	/// </summary>
	public class MemoryHeap
	{
		#region Events

		public event EventHandler<MemoryHeapResizeEventArgs> Resized;
		
		#endregion

		#region Fields

		private int capacity;
		private readonly SortedList<int, int> free;
		private readonly SortedList<int, int> used;

		#endregion

		#region Properties

		public int Capacity => this.capacity;

		/// <summary>
		/// Gets an enumerable of the free segments in the heap.
		/// </summary>
		public IEnumerable<MemoryHeapSegment> FreeSegments
		{
			get { return this.free.Select(pair => new MemoryHeapSegment(pair.Key, pair.Value)); }
		}

		/// <summary>
		/// Gets an enumerable of the used segments in the heap.
		/// </summary>
		public IEnumerable<MemoryHeapSegment> UsedSegments
		{
			get { return this.used.Select(pair => new MemoryHeapSegment(pair.Key, pair.Value)); }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MemoryHeap"/> class. 
		/// </summary>
		public MemoryHeap(int initialCapacity)
		{
			this.free = new SortedList<int, int>();
			this.used = new SortedList<int, int>();

			this.capacity = initialCapacity;
			this.free.Add(0, initialCapacity);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes a region in the heap.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="count"></param>
		protected virtual void Initialize(int index, int count)
		{
			
		}

		/// <summary>
		/// Resizes the underlying memory.
		/// </summary>
		/// <param name="newSize"></param>
		private void resize(int newSize)
		{
			int oldSize = this.capacity;
			this.capacity = newSize;
			this.OnResized(new MemoryHeapResizeEventArgs(oldSize,newSize));
		}

		/// <summary>
		/// Called when the heap was resized.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnResized(MemoryHeapResizeEventArgs e)
		{
			this.Resized?.Invoke(this, e);
		}

		/// <summary>
		/// Moves a block of items.
		/// </summary>
		/// <param name="fromIndex"></param>
		/// <param name="toIndex"></param>
		/// <param name="count"></param>
		protected virtual void Move(int fromIndex, int toIndex, int count)
		{
			
		}
		
		/// <summary>
		/// Returns the size of the array for the specified minimum size.
		/// Can be used to allocate more space in advance.
		/// </summary>
		/// <param name="minimum">The minimum required array size.</param>
		/// <returns>The preferred array size.</returns>
		protected virtual int GetSize(int minimum)
		{
			return minimum;
		}

		/// <summary>
		/// Returns the value of the closest item in the list.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		private static int findClosest(IList<int> list, int value)
		{
			return list[findClosestIndex(list, value, 0, list.Count-1)];
		}

		/// <summary>
		/// Returns the index of the closest item in the list.
		/// </summary>
		/// <param name="list"></param>
		/// <param name="value"></param>
		/// <param name="min"></param>
		/// <param name="max"></param>
		/// <returns></returns>
		private static int findClosestIndex(IList<int> list, int value, int min, int max)
		{
			while (min < max)
			{
				int mid = (max + min) / 2;
				if (list[mid] > value)
					max = mid - 1;
				else
					min = mid + 1;
			}

			if (min > 0 && list[min] > value)
				--min;

			return min;
		}

		/// <summary>
		/// Returns a value that indicates whether there are any used segments between the two indices.
		/// </summary>
		/// <param name="index1"></param>
		/// <param name="index2"></param>
		/// <returns></returns>
		private bool isUsed(int index1, int index2)
		{
			if (this.used.Count == 0)
				return false;

			int closest = findClosest(this.used.Keys, index2);
			int closestLen = this.used[closest];

			return closest >= index1 && closest <= index2 ||
				   index1 >= closest && index1 < closest + closestLen;
		}

		/// <summary>
		/// Allocates the specified number of elements and returns the index.
		/// </summary>
		/// <param name="count">The number of elements.</param>
		/// <returns>The index of the first allocated entry in the array.</returns>
		public int Allocate(int count)
		{
			return this.Allocate(count, false);
		}

		/// <summary>
		/// Allocates the specified number of elements and returns the index.
		/// </summary>
		/// <param name="count">The number of elements.</param>
		/// <param name="clear">Determines whether the allocated segment in the array should be reset to the default value of T before returning.</param>
		/// <returns>The index of the first allocated entry in the array.</returns>
		public int Allocate(int count, bool clear)
		{
			if (count <= 0)
				throw new ArgumentException("Count must be greater than 0.", nameof(count));

			for (;;)
			{
				foreach (KeyValuePair<int, int> segment in this.free)
				{
					if (segment.Value >= count)
					{
						// found a region that is big enough.
						int index = segment.Key;
						this.free.Remove(index);

						if (segment.Value > count)
						{
							// region was not fully used, add rest back to free regions.
							this.free.Add(index + count, segment.Value - count);
						}

						this.used.Add(index, count);

						if (clear)
						{
							// clear segment.
							this.Initialize(index, count);
						}

						return index;
					}
				}

				// get the size of the free segment at the end of the array, if there is one.
				int endSegmentSize = 0;
				int endSegment = -1;
				if (this.free.Count > 0)
				{
					endSegment = findClosest(this.free.Keys, this.Capacity);
					if (this.isUsed(endSegment, this.Capacity))
					{
						endSegment = -1;
					}
					else
					{
						endSegmentSize = this.free[endSegment];
					}
				}

				// decide on new size of array.
				int oldSize = this.Capacity;
				int newSize = this.GetSize(oldSize - endSegmentSize + count);

				// resize the array.
				this.resize(newSize);
				
				if (endSegment >= 0)
				{
					// make end segment bigger.
					this.free[endSegment] += newSize - oldSize;
				}
				else
				{
					// add free space.
					this.free.Add(oldSize, newSize - oldSize);
				}
			}
		}

		/// <summary>
		/// Resizes the segment at the specified index to the new size, moving and copying it if necessary.
		/// </summary>
		/// <param name="index">The index of the segment.</param>
		/// <param name="newSize">The new size of the segment.</param>
		/// <returns>The new index of the segment.</returns>
		public int Reallocate(int index, int newSize)
		{
			int oldSize;
			if (!this.used.TryGetValue(index, out oldSize))
				throw new ArgumentException("Invalid index.", nameof(index));

			if (!this.Free(index))
				throw new ArgumentException("Invalid index.", nameof(index));

			int newIndex = this.Allocate(newSize, false);

			if (index != newIndex)
			{
				this.Move(index, newIndex, Math.Min(oldSize, newSize));
			}

			return newIndex;
		}
	
		/// <summary>
		/// Frees a previously allocated segment.
		/// </summary>
		/// <param name="index">The index of the first element, as returned by Allocate</param>
		/// <returns>True, if the array region was freed. False, if the specified index was not allocated.</returns>
		public bool Free(int index)
		{
			int count;
			if (!this.used.TryGetValue(index, out count))
				return false;
			
			this.used.Remove(index);

			if (this.free.Count == 0)
			{
				// this is the only free segment.
				this.free.Add(index, count);
				return true;
			}

			int countAfter;
			if (this.free.TryGetValue(index + count, out countAfter))
			{
				// merge with segment after this.
				this.free.Remove(index + count);
				count += countAfter;
			}

			if (this.free.Count > 0)
			{
				int before = findClosest(this.free.Keys, index);
				if (before < index && !this.isUsed(before, index))
				{
					// merge with segment before this.
					this.free[before] += count;
					return true;
				}
			}

			// add new free segment.
			this.free.Add(index, count);
			return true;
		}

		/// <summary>
		/// Removes the unused free space at the end of the array by resizing the array.
		/// </summary>
		public void TrimExcess()
		{
			if (this.free.Count == 0)
				return;

			int endSegment = findClosest(this.free.Keys, this.Capacity);
			if (this.isUsed(endSegment, this.Capacity))
				return;

			this.free.Remove(endSegment);

			this.resize(endSegment);
		}

		/// <summary>
		/// Compacts the heap.
		/// This removes all free segments (except free space at the end) so that there are no empty spaces between used segments.
		/// </summary>
		/// <param name="callback">A callback that is invoked for all moved segments.</param>
		public void Compact(MemoryHeapSegmentMoveCallback callback)
		{
			if (this.free.Count == 0 || this.used.Count == 0)
				return;

			int usedIndex = -1;
			for (;;)
			{
				int freeSegment = this.free.Keys[0];
				int freeSize = this.free.Values[0];

				int nextFreeSegment;

				if (this.free.Count > 1)
				{
					nextFreeSegment = this.free.Keys[1];
					int nextFreeSize = this.free.Values[1];

					// remove first two free segments.
					this.free.RemoveAt(0);
					this.free.RemoveAt(0);

					// merge into one.
					this.free.Add(nextFreeSegment - freeSize, nextFreeSize + freeSize);
				}
				else
				{
					// last free segment, move until end of array.
					nextFreeSegment = this.Capacity;
					this.free.RemoveAt(0);
				}

				// find first used index if this is the first move.
				if (usedIndex == -1)
				{
					usedIndex = findClosestIndex(this.used.Keys, freeSegment, 0, this.used.Count - 1);
					if (this.used.Keys[usedIndex] < freeSegment)
						++usedIndex;
				}

				// move all used segments between this free segment and the next one.
				while (usedIndex < this.used.Count && this.used.Keys[usedIndex] < nextFreeSegment)
				{
					int usedSegment = this.used.Keys[usedIndex];
					int usedSize = this.used.Values[usedIndex];
					int newPosition = usedSegment - freeSize;

					// move segment.
					this.used.Remove(usedSegment);
					this.used.Add(newPosition, usedSize);
					this.Move(usedSegment, newPosition, usedSize);

					// inform user.
					callback(new MemoryHeapSegment(usedSegment, usedSize), newPosition);

					// go to next used segment.
					++usedIndex;
				}

				if (nextFreeSegment == this.Capacity)
					break; // end reached.
			}

			// add free region at the end.
			int last = this.used.Keys[this.used.Count - 1] + this.used.Values[this.used.Count - 1];
			if (last < this.Capacity)
				this.free.Add(last, this.Capacity - last);
		}

		/// <summary>
		/// Removes all used regions from the heap.
		/// </summary>
		public void Clear()
		{
			this.free.Clear();
			this.used.Clear();
			this.used.Add(0, this.Capacity);
		}

		#endregion
	}
}
