﻿using System;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Represents a list item (an index/value tuple). This is simply a special case of a <see cref="KeyValuePair{TKey,TValue}"/>.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	public struct ListItem<T> : IEquatable<ListItem<T>>
	{
		#region Properties

		public int Index { get; }
		public T Value { get; }

		#endregion

		#region Constructors

		public ListItem(int index, T value)
		{
			this.Index = index;
			this.Value = value;
		}

		#endregion

		#region Methods

		public bool Equals(ListItem<T> other)
		{
			return this.Index == other.Index && EqualityComparer<T>.Default.Equals(this.Value, other.Value);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is ListItem<T> item && this.Equals(item);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (this.Index * 397) ^ EqualityComparer<T>.Default.GetHashCode(this.Value);
			}
		}

		public static bool operator ==(ListItem<T> left, ListItem<T> right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(ListItem<T> left, ListItem<T> right)
		{
			return !left.Equals(right);
		}

		public static implicit operator KeyValuePair<int, T>(ListItem<T> item)
		{
			return new KeyValuePair<int, T>(item.Index, item.Value);
		}

		public static implicit operator ListItem<T>(KeyValuePair<int, T> item)
		{
			return new ListItem<T>(item.Key, item.Value);
		}

		public static implicit operator (int, T)(ListItem<T> item)
		{
			return (item.Index, item.Value);
		}

		public static implicit operator ListItem<T>((int index, T value) item)
		{
			return new ListItem<T>(item.index, item.value);
		}

		public static implicit operator T(ListItem<T> item)
		{
			return item.Value;
		}

		#endregion
	}
}