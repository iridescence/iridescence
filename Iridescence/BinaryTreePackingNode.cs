﻿using System;
using System.Json;

namespace Iridescence
{
	/// <summary>
	/// Represents a node of a binary tree used for 2D bin packing.
	/// </summary>
	[Serializable]
	public sealed class BinaryTreePackingNode
	{
		#region Fields

		private BinaryTreePackingNode nodeA;
		private BinaryTreePackingNode nodeB;
		private bool isEmpty;
		
		#endregion

		#region Properties
		
		/// <summary>
		/// Gets the parent node.
		/// </summary>
		public BinaryTreePackingNode Parent { get; private set; }

		/// <summary>
		/// Gets the first child node.
		/// </summary>
		public BinaryTreePackingNode NodeA => this.nodeA;

		/// <summary>
		/// Gets the second child node.
		/// </summary>
		public BinaryTreePackingNode NodeB => this.nodeB;

		/// <summary>
		/// Gets the sibling of this node.
		/// </summary>
		public BinaryTreePackingNode Sibling
		{
			get
			{
				if (this.Parent == null)
					return null;

				if (this == this.Parent.NodeA)
					return this.Parent.NodeB;

				return this.Parent.NodeA;
			}
		}

		/// <summary>
		/// Gets the absolute X position.
		/// </summary>
		public int X { get; }

		/// <summary>
		/// Gets the absolute Y position.
		/// </summary>
		public int Y { get; }

		/// <summary>
		/// Gets the width of the node.
		/// </summary>
		public int Width { get; }

		/// <summary>
		/// Gets the height of the node.
		/// </summary>
		public int Height { get; }

		/// <summary>
		/// Gets a value that indicates whether this node is a leaf and has no further child elements.
		/// </summary>
		public bool IsLeaf => this.nodeA == null && this.nodeB == null;

		/// <summary>
		/// Gets a value that indicates whether this node is currently empty (i.e. its space can be used or subdivided).
		/// </summary>
		public bool IsEmpty => this.isEmpty;

		#endregion

		#region Constructors

		private BinaryTreePackingNode(BinaryTreePackingNode parent, int x, int y, int width, int height)
		{
			this.Parent = parent;
			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;
			this.isEmpty = true;
		}

		/// <summary>
		/// Creates a new binary tree node with the specified dimensions.
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		public BinaryTreePackingNode(int width, int height)
		{
			this.Width = width;
			this.Height = height;
			this.isEmpty = true;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Empties the node, making the space it occupies available for allocation.
		/// </summary>
		public void Empty()
		{
			if (!this.IsLeaf)
				throw new InvalidOperationException("Node is not a leaf.");

			if (this.isEmpty)
				return;

			this.isEmpty = true;

			BinaryTreePackingNode sibling = this.Sibling;
			if (sibling != null && sibling.isEmpty)
			{
				// sibling is empty as well, make parent a leaf again.
				this.Parent.nodeA = null;
				this.Parent.nodeB = null;

				this.Parent.Empty();

				this.Parent = null;
				sibling.Parent = null;
			}
		}

		/// <summary>
		/// Finds the root node.
		/// </summary>
		/// <returns></returns>
		public BinaryTreePackingNode FindRoot()
		{
			BinaryTreePackingNode current = this;
			while (current.Parent != null)
			{
				current = current.Parent;
			}
			return current;
		}

		/// <summary>
		/// Recursively tries to add a rectangle with the specified dimenions.
		/// </summary>
		/// <param name="width">The width of the rectangle to add.</param>
		/// <param name="height">The height of the rectangle to add.</param>
		/// <returns>The new node with the specified dimensions, or null if the a rectangle of the specified size does not fit into the tree.</returns>
		public BinaryTreePackingNode Add(int width, int height)
		{
			if (this.Width < width || this.Height < height)
				return null; // doesn't fit into this node.

			if (!this.IsLeaf)
				return this.nodeA.Add(width, height) ?? this.nodeB.Add(width, height); // try children.

			if (!this.isEmpty)
				return null; // node is already occupied.

			this.isEmpty = false;

			if (this.Width == width && this.Height == height)
			{
				// fits perfectly into this node.
				return this;
			}

			if (this.Width - width > this.Height - height)
			{
				// split horizontally.
				this.nodeA = new BinaryTreePackingNode(this, this.X, this.Y, width, this.Height);
				this.nodeB = new BinaryTreePackingNode(this, this.X + width, this.Y, this.Width - width, this.Height);
			}
			else
			{
				// split vertically.
				this.nodeA = new BinaryTreePackingNode(this, this.X, this.Y, this.Width, height);
				this.nodeB = new BinaryTreePackingNode(this, this.X, this.Y + height, this.Width, this.Height - height);
			}

			// it'll fit into A.
			return this.nodeA.Add(width, height);
		}

		public override string ToString()
		{
			return $"{(this.isEmpty ? "Empty" : "Occupied")}, X={this.X}, Y={this.Y}, Width={this.Width}, Height={this.Height}";
		}

		#region JSON

		/// <summary>
		/// Returns a JSON object that represents this node.
		/// </summary>
		/// <returns></returns>
		public JsonObject ToJson()
		{
			JsonObject obj = new JsonObject();
			obj.Add("x", this.X);
			obj.Add("y", this.Y);
			obj.Add("width", this.Width);
			obj.Add("height", this.Height);
			obj.Add("a", this.nodeA?.ToJson());
			obj.Add("b", this.nodeB?.ToJson());
			obj.Add("empty", this.isEmpty);
			return obj;
		}

		private static BinaryTreePackingNode fromJson(JsonObject obj, BinaryTreePackingNode parent)
		{
			if (obj == null)
				return null;

			int x = obj["x"];
			int y = obj["y"];
			int width = obj["width"];
			int height = obj["height"];

			BinaryTreePackingNode node = new BinaryTreePackingNode(parent, x, y, width, height);
			node.isEmpty = obj["empty"];
			node.nodeA = fromJson((JsonObject)obj["a"], node);
			node.nodeB = fromJson((JsonObject)obj["b"], node);

			return node;
		}

		/// <summary>
		/// Parses a node from a JSON object.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static BinaryTreePackingNode FromJson(JsonObject obj)
		{
			return fromJson(obj, null);
		}

		#endregion

		#endregion
	}
}
