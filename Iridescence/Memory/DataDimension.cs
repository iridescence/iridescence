﻿using System;

namespace Iridescence.Memory
{
	/// <summary>
	/// Describes one dimension of a <see cref="DataDescriptor"/>.
	/// </summary>
	[Serializable]
	public sealed class DataDimension : IEquatable<DataDimension>
	{
		#region Properties

		/// <summary>
		/// Gets the stride of the dimension.
		/// </summary>
		public int Stride { get; }

		/// <summary>
		/// Gets the flags of the dimension.
		/// </summary>
		public DataDimensionFlags Flags { get; }

		/// <summary>
		/// Gets the minimum index (inclusive).
		/// </summary>
		public int Minimum { get; }

		/// <summary>
		/// Gets the maximum index (inclusive).
		/// </summary>
		public int Maximum { get; }

		/// <summary>
		/// Gets the name of the dimension.
		/// </summary>
		public string Name { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DataDimension"/> class.
		/// </summary>
		/// <param name="flags">The <see cref="DataDimensionFlags"/>.</param>
		/// <param name="stride">The stride of the dimension.</param>
		/// <param name="minimum">The minimum valid index (inclusive) for this dimension.</param>
		/// <param name="maximum">The maximum valid index (inclusive) for this dimension.</param>
		/// <param name="name">The name of the dimension.</param>
		public DataDimension(DataDimensionFlags flags, int stride, int minimum, int maximum, string name)
		{
			this.Stride = stride;
			this.Flags = flags;
			this.Minimum = minimum;
			this.Maximum = maximum;
			this.Name = name;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DataDimension"/> class.
		/// </summary>
		/// <param name="flags">The <see cref="DataDimensionFlags"/>.</param>
		/// <param name="stride">The stride of the dimension.</param>
		/// <param name="minimum">The minimum valid index (inclusive) for this dimension.</param>
		/// <param name="maximum">The maximum valid index (inclusive) for this dimension.</param>
		public DataDimension(DataDimensionFlags flags, int stride, int minimum, int maximum)
			: this(flags, stride, minimum, maximum, null)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DataDimension"/> class.
		/// </summary>
		/// <param name="flags">The <see cref="DataDimensionFlags"/>.</param>
		/// <param name="stride">The stride of the dimension.</param>
		public DataDimension(DataDimensionFlags flags, int stride)
			: this(flags, stride, int.MinValue, int.MaxValue)
		{

		}

		#endregion

		#region Methods

		public bool Equals(DataDimension other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return this.Stride == other.Stride && 
			       this.Flags == other.Flags &&
			       this.Minimum == other.Minimum && 
			       this.Maximum == other.Maximum && 
			       string.Equals(this.Name, other.Name);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return obj is DataDimension dimension && this.Equals(dimension);
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(this.Stride, this.Flags, this.Minimum, this.Maximum, this.Name);
		}
		
		#endregion
	}
}
