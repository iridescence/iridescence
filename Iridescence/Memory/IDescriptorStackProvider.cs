﻿using Iridescence.Emit;

namespace Iridescence.Memory
{
	/// <summary>
	/// Provides means to load descriptor information onto the stack.
	/// </summary>
	internal interface IDescriptorStackProvider
	{
		/// <summary>
		/// Loads the stride of the specified dimension onto the stack.
		/// </summary>
		void LoadStride(ILBuilder il, int dimension);

		/// <summary>
		/// Loads the minimum index of the specified dimension onto the stack.
		/// </summary>
		void LoadMinimum(ILBuilder il, int dimension);

		/// <summary>
		/// Loads the maximum index of the specified dimension onto the stack.
		/// </summary>
		void LoadMaximum(ILBuilder il, int dimension);
	}
}
