﻿using System;
using System.Collections.Generic;

namespace Iridescence.Memory
{
	/// <summary>
	/// Represents a descriptor for a multi-dimensional data array, also known as Dope Vector.
	/// </summary>
	[Serializable]
	public class DataDescriptor : IEquatable<DataDescriptor>
	{
		#region Properties

		/// <summary>
		/// Gets the element type.
		/// </summary>
		public Type ElementType { get; }

		/// <summary>
		/// Gets the dimensions of the data descriptor.
		/// </summary>
		public IReadOnlyList<DataDimension> Dimensions { get; }

		/// <summary>
		/// Gets the rank of the array (i.e. the number of dimensions).
		/// </summary>
		public int Rank => this.Dimensions.Count;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DataDescriptor"/> class. 
		/// </summary>
		public DataDescriptor(Type elementType, params DataDimension[] dimensions)
			: this(elementType, (IEnumerable<DataDimension>)dimensions)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DataDescriptor"/> class. 
		/// </summary>
		public DataDescriptor(Type elementType, IEnumerable<DataDimension> dimensions)
		{
			this.ElementType = elementType ?? throw new ArgumentNullException(nameof(elementType));
			this.Dimensions = dimensions.ToReadOnlyList();
		}

		#endregion

		#region Methods

		public bool Equals(DataDescriptor other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;

			if(this.ElementType != other.ElementType)
				return false;
			
			if(this.Rank != other.Rank)
				return false;

			for (int i = 0; i < this.Rank; ++i)
			{
				if(!this.Dimensions[i].Equals(other.Dimensions[i]))
					return false;
			}

			return true;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return this.Equals((DataDescriptor)obj);
		}

		public override int GetHashCode()
		{
			HashCode h = new HashCode();

			h.Add(this.ElementType);

			foreach (DataDimension dim in this.Dimensions)
			{
				h.Add(dim);
			}

			return h.ToHashCode();
		}
		
		#endregion
	}
}
