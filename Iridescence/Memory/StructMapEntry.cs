﻿using System;
using Iridescence.Emit;

namespace Iridescence.Memory
{
	/// <summary>
	/// Represents an entry in a structure mapping conversion.
	/// </summary>
	public abstract class StructMapEntry
	{
		#region Fields
		
		#endregion
		
		#region Properties

		/// <summary>
		/// The name of the destination field or property.
		/// </summary>
		public string Destination { get; }
		
		#endregion
		
		#region Constructors

		protected StructMapEntry(string destination)
		{
			this.Destination = destination ?? throw new ArgumentNullException(nameof(destination));
		}

		#endregion
		
		#region Methods

		public abstract void Emit(ILBuilder emit, Type sourceType, Type destType, StructMapConversion conversion, LoadFunc loadSourceStruct, StoreFunc storeDestMember);

		#endregion
	}
}
