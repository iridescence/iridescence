﻿using System;

namespace Iridescence.Memory
{
	public static class MemoryExtensions
	{
		/// <summary>
		/// Converts the specified generic memory to byte memory.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="memory"></param>
		/// <returns></returns>
		public static Memory<byte> AsBytes<T>(this Memory<T> memory) where T : struct
		{
			return new AsBytesMemoryManager<T>(memory).Memory;
		}

		/// <summary>
		/// Converts the specified generic memory to byte memory.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="memory"></param>
		/// <returns></returns>
		public static ReadOnlyMemory<byte> AsBytes<T>(this ReadOnlyMemory<T> memory) where T : struct
		{
			return new AsBytesMemoryManager<T>(memory).Memory;
		}
	}
}