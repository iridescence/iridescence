using System;

namespace Iridescence.Memory
{
	/// <summary>
	/// Defines the flags of a <see cref="DataDimension"/>.
	/// </summary>
	[Flags]
	public enum DataDimensionFlags
	{
		/// <summary>
		/// No flags.
		/// </summary>
		None = 0,

		/// <summary>
		/// Specifies that the accessor should check an index against the minimum and maximum defined in the dimension.
		/// </summary>
		CheckBounds = 1,

		/// <summary>
		/// Specifies that the stride is meant to be interpreted as whole-element offsets, not byte offsets.
		/// When calculating the offset, by default, the stride is interpreted as a byte offset from the source/base pointer. With this flag set, it is instead interpreted as an element-index.
		/// This flag must be set when accessing data from an array or <see cref="Span{T}"/> as these types do not allow byte-based offsets.
		/// </summary>
		ElementStride = 2,
	}
}