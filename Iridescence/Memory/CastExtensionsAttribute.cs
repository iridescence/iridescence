﻿using System;

namespace Iridescence.Memory
{
	/// <summary>
	/// Attribute that defines where additional cast operators for the type can be found.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public class CastExtensionsAttribute : Attribute
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the type that declares public, static methods with 1 parameter and a return value that act as cast operators for types.
		/// </summary>
		public Type DeclaringType { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CastExtensionsAttribute"/> class. 
		/// </summary>
		public CastExtensionsAttribute(Type declaringType)
		{
			this.DeclaringType = declaringType;
		}

		#endregion

		#region Methods

		#endregion
	}
}
