﻿using System;
using Iridescence.Emit;

namespace Iridescence.Memory
{
	/// <summary>
	/// Interface for code generators that emit code to convert from one element type to a different type.
	/// </summary>
	public interface IMemoryConversionCodeGenerator
	{
		/// <summary>
		/// Emits code to convert between two types.
		/// </summary>
		/// <param name="emit">The code emitter.</param>
		/// <param name="sourceType">The source type to convert from.</param>
		/// <param name="destType">The destination type to convert to.</param>
		/// <param name="loadSource">The <see cref="LoadFunc"/> to load the source value.</param>
		/// <param name="store">The <see cref="StoreFunc"/> to store the dest value.</param>
		void Emit(ILBuilder emit, Type sourceType, Type destType, LoadFunc loadSource, StoreFunc store);
	}
}
