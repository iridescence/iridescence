﻿using System;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Memory
{
	/// <summary>
	/// Assigns the value of a static property, field or parameterless method to a destination field.
	/// </summary>
	public class StructMapStaticEntry : StructMapEntry
	{
		#region Fields
		
		#endregion
		
		#region Properties

		public MemberInfo Member { get;  }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StructMapStaticEntry"/> class. 
		/// </summary>
		public StructMapStaticEntry(MemberInfo member, string destination)
			: base(destination)
		{
			if (member == null)
				throw new ArgumentNullException(nameof(member));

			if (member is FieldInfo field)
			{
				if (!field.IsStatic)
					throw new ArgumentException("Field is not static", nameof(member));
			}
			else if (member is PropertyInfo prop)
			{
				MethodInfo getter = prop.GetGetMethod();

				if (!getter.IsStatic)
					throw new ArgumentException("Property is not static", nameof(member));

				if (getter.GetParameters().Length != 0)
					throw new ArgumentException("Property must not have any arguments", nameof(member));
			}
			else if (member is MethodInfo method)
			{
				if (!method.IsStatic)
					throw new ArgumentException("Method is not static", nameof(member));

				if (method.GetParameters().Length != 0)
					throw new ArgumentException("Method must not have any arguments", nameof(member));
			}
			else
			{
				throw new ArgumentException("Member not supported.", nameof(member));
			}

			this.Member = member;
		}

		#endregion
		
		#region Methods
		
		public override void Emit(ILBuilder emit, Type sourceType, Type destType, StructMapConversion conversion, LoadFunc loadSourceStruct, StoreFunc storeDestField)
		{
			using (storeDestField())
			{
				if (this.Member is FieldInfo field)
				{
					emit.LoadField(field);
				}
				else if (this.Member is PropertyInfo prop)
				{
					emit.Call(prop.GetGetMethod());
				}
				else if (this.Member is MethodInfo method)
				{
					emit.Call(method);
				}
			}
		}
		
		#endregion
	}
}
