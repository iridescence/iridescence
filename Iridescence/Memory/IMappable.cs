﻿using System.Buffers;

namespace Iridescence.Memory
{
	/// <summary>
	/// Represents an interface for memory that can be mapped to virtual memory.
	/// </summary>
	public interface IMappable
	{
		/// <summary>
		/// Maps the memory and returns the <see cref="MemoryHandle"/>.
		/// </summary>
		/// <param name="mode"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		MemoryHandle Map(AccessMode mode, int start, int length);

		/// <summary>
		/// Unmaps the memory.
		/// </summary>
		void Unmap();
	}
}
