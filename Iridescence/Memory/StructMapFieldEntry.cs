﻿using System;
using Iridescence.Emit;

namespace Iridescence.Memory
{
	/// <summary>
	/// Maps a source field to a dest field.
	/// </summary>
	public class StructMapFieldEntry : StructMapEntry
	{
		#region Fields
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the name of the source field or property.
		/// </summary>
		public string Source {get;}
		
		/// <summary>
		/// Gets the <see cref="IMemoryConversionCodeGenerator"/> that generates the code to convert the source field value to the destination type.
		/// </summary>
		public IMemoryConversionCodeGenerator Conversion { get; }

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StructMapFieldEntry"/> class. 
		/// </summary>
		public StructMapFieldEntry(string source, string destination, IMemoryConversionCodeGenerator conversion = null)
			: base(destination)
		{
			this.Source = source ?? throw new ArgumentNullException(nameof(source));
			this.Conversion = conversion;
		}

		#endregion
		
		#region Methods

		public override void Emit(ILBuilder emit, Type sourceType, Type destType, StructMapConversion conversion, LoadFunc loadSourceStruct, StoreFunc storeDestMember)
		{
			Type sourceMemberType = sourceType.GetFieldOrPropertyType(this.Source);
			Type destMemberType = destType.GetFieldOrPropertyType(this.Destination);

			if (this.Conversion != null)
			{
				this.Conversion.Emit(emit, sourceMemberType, destMemberType, StructMapConversion.GetFieldOrPropertyLoadFunc(emit, sourceType, this.Source, loadSourceStruct), storeDestMember);
			}
			else
			{
				using (storeDestMember())
				{
					emit.LoadConvertedValue(StructMapConversion.GetFieldOrPropertyLoadFunc(emit, sourceType, this.Source, loadSourceStruct), sourceMemberType, destMemberType);
				}
			}
		}

		#endregion
	}
}
