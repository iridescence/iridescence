﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Memory
{
	public delegate void MemoryConversionCodeGenerator(Local source, Local dest);

	public static class MemoryAccessorBuilder
	{
		#region Fields
		
		private static readonly ConstructorInfo indexOutOfRangeExceptionConstructor = typeof(IndexOutOfRangeException).GetConstructor(new[] {typeof(string)}) ?? throw new MissingMethodException();

		#endregion

		#region Methods
		
		private static bool isInteger(Type type)
		{
			return type == typeof(sbyte) ||
			       type == typeof(byte) ||
			       type == typeof(short) ||
			       type == typeof(ushort) ||
			       type == typeof(int) ||
			       type == typeof(uint) ||
			       type == typeof(long) ||
			       type == typeof(ulong) ||
			       type == typeof(IntPtr) ||
			       type == typeof(UIntPtr);
		}

		private static MethodInfo findIndexerGetter(Type type, Type returnType, out bool isRef)
		{
			Type refReturnType = returnType.MakeByRefType();
			foreach (MethodInfo method in type.GetMethods(BindingFlags.Public | BindingFlags.Instance))
			{
				if (method.Name != "get_Item")
					continue;

				ParameterInfo[] parameters = method.GetParameters();
				if (parameters.Length != 1)
					continue;

				if (!isInteger(parameters[0].ParameterType))
					continue;

				if (method.ReturnType == refReturnType)
				{
					isRef = true;
					return method;
				}

				if (method.ReturnType == returnType)
				{
					isRef = false;
					return method;
				}
			}

			isRef = false;
			return null;
		}

		private static MethodInfo findIndexerSetter(Type type, Type valueType, out bool isRef)
		{
			Type refValueType = valueType.MakeByRefType();
			foreach (MethodInfo method in type.GetMethods(BindingFlags.Public | BindingFlags.Instance))
			{
				if (method.Name != "get_Item" && method.Name != "set_Item")
					continue;

				ParameterInfo[] parameters = method.GetParameters();
				if (parameters.Length < 1)
					continue;

				if (!isInteger(parameters[0].ParameterType))
					continue;

				if (method.Name == "get_Item" && parameters.Length == 1 && method.ReturnType == refValueType)
				{
					isRef = true;
					return method;
				}

				if (method.Name == "set_Item" && parameters.Length == 2 && parameters[1].ParameterType == valueType)
				{
					isRef = false;
					return method;
				}
			}

			isRef = false;
			return null;
		}

		private static MethodInfo getCastOperator(Type fromType, Type toType)
		{
			List<MethodInfo> methods = new List<MethodInfo>();

			// Gather cast methods.
			methods.AddRange(fromType.GetMethods(BindingFlags.Public | BindingFlags.Static).Where(m => (m.Name == "op_Explicit" || m.Name == "op_Implicit")));
			methods.AddRange(toType.GetMethods(BindingFlags.Public | BindingFlags.Static).Where(m => (m.Name == "op_Explicit" || m.Name == "op_Implicit")));

			foreach (CastExtensionsAttribute attr in fromType.GetCustomAttributes<CastExtensionsAttribute>(false))
			{
				methods.AddRange(attr.DeclaringType.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static));
			}
			
			foreach (CastExtensionsAttribute attr in toType.GetCustomAttributes<CastExtensionsAttribute>(false))
			{
				methods.AddRange(attr.DeclaringType.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static));
			}

			// Find methods with correct signature.
			Type fromRef = fromType.MakeByRefType();
			MethodInfo[] applicableMethods = methods
				.Where(m => m.ReturnType == toType)
				.Where(m =>
				{
					ParameterInfo[] parameters = m.GetParameters();
					return parameters.Length == 1 &&
					       (parameters[0].ParameterType == fromType ||
					        parameters[0].ParameterType == fromRef);
				}).ToArray();

			// Prefer by-ref method.
			return applicableMethods.FirstOrDefault(m => m.GetParameters()[0].ParameterType.IsByRef) ?? applicableMethods.FirstOrDefault();
		}

		private static MemoryType getMemoryType(Type type, Type elementType, bool store, out MethodInfo indexer)
		{
			indexer = null;
			if (type == typeof(IntPtr) || type == typeof(UIntPtr))
			{
				return MemoryType.Pointer;
			}
			if (type.IsPointer || type.IsByRef)
			{
				Type e = type.GetElementType();
				if (e == elementType || e == typeof(byte) || e == typeof(void))
					return MemoryType.Pointer;
			}
			else if (type.IsArray)
			{
				Type e = type.GetElementType();
				if (e == elementType)
					return MemoryType.Array;
			}
			else
			{
				indexer = store ? findIndexerSetter(type, elementType, out bool isRef) : findIndexerGetter(type, elementType, out isRef);
				
				if (indexer != null)
				{
					return isRef ? MemoryType.RefIndexer : MemoryType.Indexer;
				}

				/*
				if (type == typeof(Span<byte>))
				{
					indexer = store ? findIndexerSetter(type, typeof(byte), out isRef) : findIndexerGetter(type, typeof(byte), out isRef);
					return MemoryType.RefIndexer;
				}

				if (type == typeof(ReadOnlySpan<byte>) && !store)
				{
					indexer = store ? findIndexerSetter(type, typeof(byte), out isRef) : findIndexerGetter(type, typeof(byte), out isRef);
					return MemoryType.RefIndexer;
				}
				*/
			}

			throw new ArgumentException($"{type.GetDisplayName()} is not a valid memory representation for {elementType.GetDisplayName()}.", nameof(type));
		}

		private static void ensureSupported(DataDescriptor descriptor, MemoryType memoryType)
		{
			if (memoryType != MemoryType.Pointer)
			{
				if (descriptor.Dimensions.Any(dim => (dim.Flags & DataDimensionFlags.ElementStride) == 0))
					throw new ArgumentException("The memory representation does not support byte offsets but at least one dimension has a byte-based stride.");
			}
		}

		internal static void CheckIndices(this ILBuilder emit, DataDescriptor descriptor, Action<int> loadIndex)
		{
			for (int i = 0; i < descriptor.Rank; ++i)
			{
				DataDimension dim = descriptor.Dimensions[i];
				if (dim.Flags.HasFlag(DataDimensionFlags.CheckBounds))
				{
					Label throwException = emit.DefineLabel();
					Label checkMax = emit.DefineLabel();
					
					// if (index < minimum)
					loadIndex(i);
					emit.LoadConstant(dim.Minimum);
					emit.CompareLessThan();
					emit.BranchIfFalse(checkMax);

					emit.MarkLabel(throwException);
					emit.LoadConstant($"Index {i} is out of range.");
					emit.NewObject(indexOutOfRangeExceptionConstructor);
					emit.Throw();

					// if (index > maximum)
					emit.MarkLabel(checkMax);

					loadIndex(i);
					emit.LoadConstant(dim.Maximum);
					emit.CompareGreaterThan();

					emit.BranchIfTrue(throwException);
				}
			}
		}

		internal static void AddValueAddress(this ILBuilder emit, DataDescriptor descriptor, Action<int> loadIndex)
		{
			// Calculate index0 * stride0 + index1 * stride1 + ... + indexN * strideN.
			for (int i = 0; i < descriptor.Rank; ++i)
			{
				DataDimension dim = descriptor.Dimensions[i];

				// Add index[i] * stride[i] to address.
				loadIndex(i);
				emit.LoadConstant(dim.Stride);

				if ((dim.Flags & DataDimensionFlags.ElementStride) != 0)
				{
					// index[i] * sizeof(T)
					emit.SizeOf(descriptor.ElementType);
					emit.Multiply();
				}
				
				emit.Multiply();
				emit.ConvertToNativeInt();
				emit.Add();
			}
		}

		internal static void LoadValueIndex(this ILBuilder emit, DataDescriptor descriptor, Action<int> loadIndex)
		{
			// Calculate index0 * stride0 + index1 * stride1 + ... + indexN * strideN.
			for (int i = 0; i < descriptor.Rank; ++i)
			{
				DataDimension dim = descriptor.Dimensions[i];

				// Add index[i] * stride[i] to address.
				loadIndex(i);
				emit.LoadConstant(dim.Stride);
				emit.Multiply();

				if (i > 0)
				{
					emit.Add();
				}
			}
		}

		internal static void LoadConvertedValue(this ILBuilder emit, LoadFunc load, Type srcType, Type dstType)
		{
			if (srcType == dstType)
			{
				load(LoadMode.Value);
				return;
			}

			if (srcType.IsPrimitive && dstType.IsPrimitive)
			{
				load(LoadMode.Value);

				if (dstType == typeof(bool))
				{
					Label endLabel = emit.DefineLabel();
					Label falseLabel = emit.DefineLabel();

					emit.BranchIfFalse(falseLabel);
					emit.LoadConstant(1);
					emit.Branch(endLabel);
					emit.MarkLabel(falseLabel);
					emit.LoadConstant(0);
					emit.MarkLabel(endLabel);
				}
				else
				{
					emit.Convert(dstType);
				}

				return;
			}

			MethodInfo castOperator = getCastOperator(srcType, dstType);

			if (castOperator == null)
				throw new InvalidOperationException($"Can not convert from {srcType} to {dstType}.");

			if (castOperator.GetParameters()[0].ParameterType.IsByRef)
			{
				load(LoadMode.Address);
				emit.Call(castOperator);
			}
			else
			{
				load(LoadMode.Value);
				emit.Call(castOperator);
			}
		}

		public static bool CanConvert(Type srcType, Type dstType)
		{
			if (srcType == dstType)
			{
				return true;
			}

			if (srcType.IsPrimitive && dstType.IsPrimitive)
			{
				return true;
			}

			MethodInfo castOperator = getCastOperator(srcType, dstType);

			return castOperator != null;
		}

		private static LoadFunc getLoadFunc(ILBuilder emit, DataDescriptor descriptor, Action<int> loadIndex, Type memParam, MemoryType memType, MethodInfo memIndexer, IntPtr? baseAddress)
		{
			return mode =>
			{
				switch (memType)
				{
					case MemoryType.Pointer:
						if (baseAddress != null)
						{
							if (IntPtr.Size == 4)
								emit.LoadConstant(baseAddress.Value.ToInt32());
							else
								emit.LoadConstant(baseAddress.Value.ToInt64());
						}
						else
						{
							emit.LoadArgument(0);
						}

						emit.ConvertToNativeInt();
						emit.AddValueAddress(descriptor, loadIndex);

						if (mode == LoadMode.Value)
							emit.LoadIndirect(descriptor.ElementType);

						break;

					case MemoryType.Array:
						emit.LoadArgument(0);
						emit.LoadValueIndex(descriptor, loadIndex);

						if (mode == LoadMode.Value)
							emit.LoadElement(descriptor.ElementType);
						else
							emit.LoadElementAddress(descriptor.ElementType);

						break;

					case MemoryType.Indexer:
						if (memParam.IsValueType)
							emit.LoadArgumentAddress(0);
						else
							emit.LoadArgument(0);

						emit.LoadValueIndex(descriptor, loadIndex);

						if (mode == LoadMode.Value)
						{
							if (memParam.IsValueType)
								emit.Call(memIndexer);
							else
								emit.CallVirtual(memIndexer);
						}
						else
						{
							using (Local temp = emit.DeclareLocal(descriptor.ElementType))
							{
								if (memParam.IsValueType)
									emit.Call(memIndexer);
								else
									emit.CallVirtual(memIndexer);

								emit.StoreLocal(temp);
								emit.LoadLocalAddress(temp);
							}
						}

						break;

					case MemoryType.RefIndexer:
						if (memParam.IsValueType)
							emit.LoadArgumentAddress(0);
						else
							emit.LoadArgument(0);

						emit.LoadValueIndex(descriptor, loadIndex);

						if (memParam.IsValueType)
							emit.Call(memIndexer);
						else
							emit.CallVirtual(memIndexer);

						if (mode == LoadMode.Value)
							emit.LoadIndirect(descriptor.ElementType);

						break;
				}
			};
		}

		private static StoreFunc getStoreFunc(ILBuilder emit, DataDescriptor descriptor, Action<int> loadIndex, Type memParam, MemoryType memType, MethodInfo memIndexer, IntPtr? baseAddress)
		{
			return () =>
			{
				switch (memType)
				{
					case MemoryType.Pointer:
						if (baseAddress != null)
						{
							if (IntPtr.Size == 4)
								emit.LoadConstant(baseAddress.Value.ToInt32());
							else
								emit.LoadConstant(baseAddress.Value.ToInt64());
						}
						else
						{
							emit.LoadArgument(0);
						}

						emit.ConvertToNativeInt();
						emit.AddValueAddress(descriptor, loadIndex);

						return new StoreEnd(() => { emit.StoreIndirect(descriptor.ElementType); });

					case MemoryType.Array:
						emit.LoadArgument(0);
						emit.LoadValueIndex(descriptor, loadIndex);

						return new StoreEnd(() => { emit.StoreElement(descriptor.ElementType); });

					case MemoryType.Indexer:
						if (memParam.IsValueType)
							emit.LoadArgumentAddress(0);
						else
							emit.LoadArgument(0);

						emit.LoadValueIndex(descriptor, loadIndex);

						return new StoreEnd(() =>
						{
							if (memParam.IsValueType)
								emit.Call(memIndexer);
							else
								emit.CallVirtual(memIndexer);
						});

					case MemoryType.RefIndexer:
						if (memParam.IsValueType)
							emit.LoadArgumentAddress(0);
						else
							emit.LoadArgument(0);

						emit.LoadValueIndex(descriptor, loadIndex);

						if (memParam.IsValueType)
							emit.Call(memIndexer);
						else
							emit.CallVirtual(memIndexer);

						return new StoreEnd(() => { emit.StoreIndirect(descriptor.ElementType); });
				}

				throw new InvalidOperationException();
			};
		}

		/// <summary>
		/// Dynamically compiles an optimized read accessor for the specified <see cref="DataDescriptor"/>.
		/// </summary>
		/// <param name="descriptor">The <see cref="DataDescriptor"/> that describes the memory that the reader accesses.</param>
		/// <param name="delegateType">The delegate type.</param>
		/// <param name="baseAddress">A constant base address. If this is specified, The method must not have a parameter that takes an the address.</param>
		/// <returns>A delegate that reads from the data.</returns>
		/// <remarks>
		/// The delegate type must have one pointer/array/span parameter (unless a base address is specified) and n integer (any integer primitive) parameters, where n is the number of dimensions in the descriptor.
		/// The first parameter is the memory source and can be a native integer (IntPtr, UIntPtr), a managed pointer (ByRef types or ref), an array, a <see cref="Span{T}"/> or a <see cref="ReadOnlySpan{T}"/> or any other type with an indexer that takes one index and returns the element type.
		/// The data type of the descriptor must be convertible to the return type of the delegate, either natively by the runtime or via an implicit or explicit cast operator on either of the types.
		/// If the memory source is an indexed type (e.g. array, span), all dimensions must have the <see cref="DataDimensionFlags.ElementStride"/> flag set because these can not use byte offsets.
		/// </remarks>
		public static Delegate CompileReader(this DataDescriptor descriptor, Type delegateType, IntPtr? baseAddress = null)
		{
			// Method signature.
			MethodInfo delegateInvoke = delegateType.GetInvokeMethod();
			ParameterInfo[] delegateParameters = delegateInvoke.GetParameters();

			// Memory access.
			Type memParam = delegateParameters[0].ParameterType;
			MemoryType memType;
			MethodInfo memIndexer = null;
			int argIndexOffset = 0;
			if (baseAddress != null)
			{
				if (delegateParameters.Length != descriptor.Rank)
					throw new ArgumentException($"Delegate must have {descriptor.Rank} integer parameters.", nameof(delegateType));

				memType = MemoryType.Pointer;
			}
			else
			{
				if (delegateParameters.Length != descriptor.Rank + 1)
					throw new ArgumentException($"Delegate must have one pointer/array/span parameter followed by {descriptor.Rank} integer parameters.", nameof(delegateType));

				argIndexOffset = 1;
				memType = getMemoryType(memParam, descriptor.ElementType, false, out memIndexer);
			}

			ensureSupported(descriptor, memType);
		
			// Indexer args.
			for (int i = argIndexOffset; i < delegateParameters.Length; ++i)
			{
				if (!isInteger(delegateParameters[i].ParameterType))
					throw new ArgumentException($"Delegate parameter {i} is not an integer.", nameof(delegateType));
			}

			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(delegateType), "Lightweight memory reader");

			void loadIndex(int i)
			{
				emit.LoadArgument(i + argIndexOffset);
			}

			emit.CheckIndices(descriptor, loadIndex);
			emit.LoadConvertedValue(getLoadFunc(emit, descriptor, loadIndex, memParam, memType, memIndexer, baseAddress), descriptor.ElementType, delegateInvoke.ReturnType);
			emit.Return();

			return emit.CreateDelegate(delegateType);
		}

		/// <summary>
		/// Dynamically compiles an optimized read accessor for the specified <see cref="DataDescriptor"/>.
		/// </summary>
		/// <typeparam name="T">The delegate type.</typeparam>
		/// <param name="descriptor">The <see cref="DataDescriptor"/> that describes the memory that the reader accesses.</param>
		/// <param name="baseAddress">A constant base address. If this is specified, The method must not have a parameter that takes an the address.</param>
		/// <returns>A delegate that reads from the data.</returns>
		/// <remarks>
		/// The delegate type must have one pointer/array/span parameter (unless a base address is specified) and n integer (any integer primitive) parameters, where n is the number of dimensions in the descriptor.
		/// The first parameter is the memory source and can be a native integer (IntPtr, UIntPtr), a managed pointer (ByRef types or ref), an array, a <see cref="Span{T}"/> or a <see cref="ReadOnlySpan{T}"/> or any other type with an indexer that takes one index and returns the element type.
		/// The data type of the descriptor must be convertible to the return type of the delegate, either natively by the runtime or via an implicit or explicit cast operator on either of the types.
		/// If the memory source is an indexed type (e.g. array, span), all dimensions must have the <see cref="DataDimensionFlags.ElementStride"/> flag set because these can not use byte offsets.
		/// </remarks>
		public static T CompileReader<T>(this DataDescriptor descriptor, IntPtr? baseAddress = null) where T : class
		{
			return (T)(object)descriptor.CompileReader(typeof(T), baseAddress);
		}

		/// <summary>
		/// Dynamically compiles an optimized write accessor for the specified <see cref="DataDescriptor"/>.
		/// </summary>
		/// <param name="descriptor">The <see cref="DataDescriptor"/> that describes the memory that the writer accesses.</param>
		/// <param name="delegateType">The delegate type.</param>
		/// <param name="baseAddress">A constant base address. If this is specified, The method must not have a parameter that takes an the address.</param>
		/// <returns>A delegate that writes to the data.</returns>
		/// <remarks>
		/// The delegate type must have one pointer/array/span parameter (unless a base address is specified), n integer (any integer primitive) parameters, where n is the number of dimensions in the descriptor, and one value parameter.
		/// The first parameter is the memory source and can be a native integer (IntPtr, UIntPtr), a managed pointer (ByRef types or ref), an array, a <see cref="Span{T}"/> or a <see cref="ReadOnlySpan{T}"/> or any other type with an indexer that can be set or returns a reference.
		/// The data type of the value must be convertible to the element type of the descriptor, either natively by the runtime or via an implicit or explicit cast operator on either of the types.
		/// If the memory source is an indexed type (e.g. array, span), all dimensions must have the <see cref="DataDimensionFlags.ElementStride"/> flag set because these can not use byte offsets.
		/// </remarks>
		public static Delegate CompileWriter(this DataDescriptor descriptor, Type delegateType, IntPtr? baseAddress = null)
		{
			// Method signature.
			MethodInfo delegateInvoke = delegateType.GetInvokeMethod();
			ParameterInfo[] delegateParameters = delegateInvoke.GetParameters();

			// Memory access.
			Type memParam = delegateParameters[0].ParameterType;
			MemoryType memType;
			MethodInfo memIndexer = null;
			int argIndexOffset = 0;
			if (baseAddress != null)
			{
				if (delegateParameters.Length != descriptor.Rank + 1)
					throw new ArgumentException($"Delegate must have {descriptor.Rank} integer parameters followed by a value parameter.", nameof(delegateType));

				memType = MemoryType.Pointer;
			}
			else
			{
				if (delegateParameters.Length != descriptor.Rank + 2)
					throw new ArgumentException($"Delegate must have one pointer/array/span parameter followed by {descriptor.Rank} integer parameters and a value parameter.", nameof(delegateType));

				argIndexOffset = 1;
				memType = getMemoryType(memParam, descriptor.ElementType, false, out memIndexer);
			}

			ensureSupported(descriptor, memType);
		
			// Indexer args.
			for (int i = argIndexOffset; i < delegateParameters.Length - 1; ++i)
			{
				if (!isInteger(delegateParameters[i].ParameterType))
					throw new ArgumentException($"Delegate parameter {i} is not an integer.", nameof(delegateType));
			}

			Type argType = delegateParameters[delegateParameters.Length - 1].ParameterType;
			Type valueType = argType.IsByRef ? argType.GetElementType() : argType;

			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(delegateType), "Lightweight memory writer");

			void loadIndex(int i)
			{
				emit.LoadArgument(i + argIndexOffset);
			}
			
			emit.CheckIndices(descriptor, loadIndex);

			using (getStoreFunc(emit, descriptor, loadIndex, memParam, memType, memIndexer, baseAddress)())
			{
				emit.LoadConvertedValue(LoadStore.LoadArgument(emit, argType, delegateParameters.Length - 1), valueType, descriptor.ElementType);
			}

			emit.Return();
			
			return emit.CreateDelegate(delegateType);
		}

		/// <summary>
		/// Dynamically compiles an optimized write accessor for the specified <see cref="DataDescriptor"/>.
		/// </summary>
		/// <typeparam name="T">The delegate type.</typeparam>
		/// <param name="descriptor">The <see cref="DataDescriptor"/> that describes the memory that the writer accesses.</param>
		/// <param name="baseAddress">A constant base address. If this is specified, The method must not have a parameter that takes an the address.</param>
		/// <returns>A delegate that writes to the data.</returns>
		/// <remarks>
		/// The delegate type must have one pointer/array/span parameter (unless a base address is specified), n integer (any integer primitive) parameters, where n is the number of dimensions in the descriptor, and one value parameter.
		/// The first parameter is the memory source and can be a native integer (IntPtr, UIntPtr), a managed pointer (ByRef types or ref), an array, a <see cref="Span{T}"/> or a <see cref="ReadOnlySpan{T}"/> or any other type with an indexer that can be set or returns a reference.
		/// The data type of the value must be convertible to the element type of the descriptor, either natively by the runtime or via an implicit or explicit cast operator on either of the types.
		/// If the memory source is an indexed type (e.g. array, span), all dimensions must have the <see cref="DataDimensionFlags.ElementStride"/> flag set because these can not use byte offsets.
		/// </remarks>
		public static T CompileWriter<T>(this DataDescriptor descriptor, IntPtr? baseAddress = null) where T : class
		{
			return (T)(object)descriptor.CompileWriter(typeof(T), baseAddress);
		}
		
		/// <summary>
		/// Dynamically compiles an optimized block-copy method that copies elements from memory described by a <see cref="DataDescriptor"/> to memory described by a different <see cref="DataDescriptor"/>.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="dest"></param>
		/// <param name="delegateType"></param>
		/// <param name="conversionCodeGen"></param>
		/// <returns></returns>
		/// <remarks>
		/// The delegate signature must look like this:
		/// <code>
		/// delegate void BlockCopy(
		///	IntPtr srcAddress, int srcIndex0, int srcIndex1, ..., int srcIndexN,
		/// IntPtr dstAddress, int dstIndex0, int dstIndex1, ..., int dstIndexN,
		/// int count0, int count1, ..., int countN);
		/// </code>
		/// srcAddress and dstAddress can also be pointers. The index and count parameters may be any integer type.
		/// </remarks>
		public static Delegate CompileBlockCopy(this DataDescriptor source, DataDescriptor dest, Type delegateType, IMemoryConversionCodeGenerator conversionCodeGen = null)
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));

			if (dest == null)
				throw new ArgumentNullException(nameof(dest));

			if (source.Rank != dest.Rank)
				throw new ArgumentException("Source and dest must have the same rank.", nameof(source));

			int rank = source.Rank;

			// Method signature.
			MethodInfo delegateInvoke = delegateType.GetInvokeMethod();
			ParameterInfo[] delegateParameters = delegateInvoke.GetParameters();
			if (delegateParameters.Length != 2 + 3 * rank)
				throw new ArgumentException("Delegate does not have a valid signature.", nameof(delegateType));

			Type srcPtrType = delegateParameters[0].ParameterType;
			if (getMemoryType(srcPtrType, source.ElementType, false, out _) != MemoryType.Pointer)
				throw new ArgumentException("The first parameter must be a pointer.", nameof(delegateType));

			Type dstPtrType = delegateParameters[1].ParameterType;
			if (getMemoryType(dstPtrType, dest.ElementType, false, out _) != MemoryType.Pointer)
				throw new ArgumentException("The second parameter must be a pointer.", nameof(delegateType));

			for (int i = 0; i < rank; ++i)
			{
				if (!isInteger(delegateParameters[2 + i].ParameterType))
					throw new ArgumentException($"Delegate parameter {3 + i} is not an integer.", nameof(delegateType));

				if (!isInteger(delegateParameters[2 + rank + i].ParameterType))
					throw new ArgumentException($"Delegate parameter {3 + rank + i} is not an integer.", nameof(delegateType));

				if (!isInteger(delegateParameters[2 + 2 * rank + i].ParameterType))
					throw new ArgumentException($"Delegate parameter {3 + 2 * rank + i} is not an integer.", nameof(delegateType));
			}

			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(delegateType), "Lightweight block-copy");

			void loadSourceIndex(int i)
			{
				emit.LoadArgument(2 + i);
			}

			void loadDestIndex(int i)
			{
				emit.LoadArgument(2 + rank + i);
			}

			void loadCount(int i)
			{
				emit.LoadArgument(2 + 2 * rank + i);
			}

			void checkDimension(DataDescriptor desc, string text, int i, Action<int> loadIndex)
			{
				DataDimension dim = desc.Dimensions[i];
				if (dim.Flags.HasFlag(DataDimensionFlags.CheckBounds))
				{
					Label throwException = emit.DefineLabel();
					Label checkMax = emit.DefineLabel();
					
					// if (index < minimum)
					loadIndex(i);
					emit.LoadConstant(dim.Minimum);
					emit.CompareLessThan();
					emit.BranchIfFalse(checkMax);

					emit.MarkLabel(throwException);
					emit.LoadConstant(text);
					emit.NewObject(indexOutOfRangeExceptionConstructor);
					emit.Throw();

					// if (index + count - 1 > maximum)
					emit.MarkLabel(checkMax);

					loadIndex(i);
					loadCount(i);
					emit.Add();
					emit.LoadConstant(1);
					emit.Subtract();
					emit.LoadConstant(dim.Maximum);
					emit.CompareGreaterThan();

					emit.BranchIfTrue(throwException);
				}
			}

			Local sourceBase = emit.DeclareLocal(srcPtrType);
			emit.LoadArgument(0);
			emit.AddValueAddress(source, loadSourceIndex);
			emit.StoreLocal(sourceBase);

			Local destBase = emit.DeclareLocal(dstPtrType);
			emit.LoadArgument(1);
			emit.AddValueAddress(dest, loadDestIndex);
			emit.StoreLocal(destBase);

			Local[] indexLocals = new Local[rank];
			Local[] sourceLocals = new Local[rank];
			Local[] destLocals = new Local[rank];
			Label[] loopStartLabels = new Label[rank];
			Label[] loopEndLabels = new Label[rank];

			for (int i = 0; i < rank; ++i)
			{
				checkDimension(source, $"Source index {i} is out of range.", i, loadSourceIndex);
				checkDimension(dest, $"Destination index {i} is out of range.", i, loadDestIndex);
			}

			for (int i = 0; i < rank; ++i)
			{
				indexLocals[i] = emit.DeclareLocal(typeof(int));
				sourceLocals[i] = emit.DeclareLocal(srcPtrType);
				destLocals[i] = emit.DeclareLocal(dstPtrType);
				loopStartLabels[i] = emit.DefineLabel();
				loopEndLabels[i] = emit.DefineLabel();

				// sourcePtr[i] = sourcePtr[i-1]
				emit.LoadLocal(i == 0 ? sourceBase : sourceLocals[i - 1]);
				emit.StoreLocal(sourceLocals[i]);

				// destPtr[i] = destPtr[i-1]
				emit.LoadLocal(i == 0 ? destBase : destLocals[i - 1]);
				emit.StoreLocal(destLocals[i]);

				// index[i] = 0
				emit.LoadConstant(0);
				emit.StoreLocal(indexLocals[i]);
				emit.Branch(loopEndLabels[i]);
				emit.MarkLabel(loopStartLabels[i]);
			}

			void loadSource(LoadMode mode)
			{
				emit.LoadLocal(sourceLocals[rank - 1]);
				if (mode == LoadMode.Value)
				{
					emit.LoadIndirect(source.ElementType);
				}
			}

			StoreEnd storeDest()
			{
				emit.LoadLocal(destLocals[rank - 1]);
				return new StoreEnd(() => { emit.StoreIndirect(dest.ElementType); });
			}

			//StringBuilder debugStr = new StringBuilder();
			//List<Local> debugLocs = new List<Local>();
			//debugStr.Append('[');
			//for (int i = 0; i < rank; ++i)
			//{
			//	if (i > 0)
			//		debugStr.Append(',');
			//	debugStr.Append('{');
			//	debugStr.Append(i);
			//	debugStr.Append('}');
			//	debugLocs.Add(indexLocals[i]);
			//}
			//debugStr.Append(']');
			//emit.LoadConstant(debugStr.ToString());
			//emit.WriteLine(debugLocs);

			if (conversionCodeGen == null)
			{
				StoreEnd e = storeDest();
				emit.LoadConvertedValue(loadSource, source.ElementType, dest.ElementType);
				e.Dispose();
			}
			else
			{
				conversionCodeGen.Emit(emit, source.ElementType, dest.ElementType, loadSource, storeDest);
			}

			for (int i = rank - 1; i >= 0; --i)
			{
				// sourcePtr[i] += sourceStride[i]
				emit.LoadLocal(sourceLocals[i]);
				emit.LoadConstant(source.Dimensions[i].Stride);
				emit.Add();
				emit.StoreLocal(sourceLocals[i]);

				// destPtr[i] += destStride[i]
				emit.LoadLocal(destLocals[i]);
				emit.LoadConstant(dest.Dimensions[i].Stride);
				emit.Add();
				emit.StoreLocal(destLocals[i]);

				// ++index[i]
				emit.LoadLocal(indexLocals[i]);
				emit.LoadConstant(1);
				emit.Add();
				emit.StoreLocal(indexLocals[i]);

				// if index[i] < count[i] goto loopstart[i]
				emit.MarkLabel(loopEndLabels[i]);
				emit.LoadLocal(indexLocals[i]);
				loadCount(i);
				emit.BranchIfLess(loopStartLabels[i]);
			}

			emit.Return();

			//Console.WriteLine(emit.ToString());
			return emit.CreateDelegate(delegateType);
		}

		public static T CompileBlockCopy<T>(this DataDescriptor source, DataDescriptor dest, IMemoryConversionCodeGenerator conversionCodeGen = null) where T : class
		{
			return (T)(object)source.CompileBlockCopy(dest, typeof(T), conversionCodeGen);
		}

		#endregion

		#region Nested Types

		private enum MemoryType
		{
			Pointer,
			Array,
			Indexer,
			RefIndexer,
		}

		#endregion
	}
}
