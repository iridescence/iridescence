﻿using System;
using Iridescence.Emit;

namespace Iridescence.Memory
{
	/// <summary>
	/// Assigns a constant value to a destination field.
	/// </summary>
	public class StructMapConstantEntry<T> : StructMapEntry
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		public T Value { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StructMapConstantEntry{T}"/> class. 
		/// </summary>
		public StructMapConstantEntry(T value, string destination)
			: base(destination)
		{
			this.Value = value;
		}

		#endregion
		
		#region Methods
		
		public override void Emit(ILBuilder emit, Type sourceType, Type destType, StructMapConversion conversion, LoadFunc loadSourceStruct, StoreFunc storeDestField)
		{
			using (storeDestField())
			{
				emit.LoadConstant(this.Value);
			}
		}
		
		#endregion
	}
}
