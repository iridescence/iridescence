﻿using System;
using System.Buffers;
using System.Threading;

namespace Iridescence.Memory
{
	/// <summary>
	/// Implements a <see cref="MemoryManager{T}"/> for an unmanaged pointer.
	/// </summary>
	public class UnmanagedMemoryManager : MemoryManager<byte>
	{
		#region Fields

		private readonly IntPtr pointer;
		private readonly int size;
		private bool isDisposed;
		private int numPins;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="UnmanagedMemoryManager"/> class. 
		/// </summary>
		public UnmanagedMemoryManager(IntPtr pointer, int size)
		{
			this.pointer = pointer;
			this.size = size;
		}

		#endregion

		#region Methods

		protected override void Dispose(bool disposing)
		{
			this.isDisposed = true;
		}

		public override unsafe Span<byte> GetSpan()
		{
			if (this.isDisposed)
				throw new ObjectDisposedException(nameof(UnmanagedMemoryManager));

			return new Span<byte>(this.pointer.ToPointer(), this.size);
		}

		public override unsafe MemoryHandle Pin(int elementIndex = 0)
		{
			Interlocked.Increment(ref this.numPins);
			return new MemoryHandle((this.pointer + elementIndex).ToPointer());
		}

		public override void Unpin()
		{
			for (;;)
			{
				int n = this.numPins;
				if (n <= 0)
					throw new InvalidOperationException();

				if (Interlocked.CompareExchange(ref this.numPins, n - 1, n) == n)
					break;
			}
		}

		#endregion
	}
}
