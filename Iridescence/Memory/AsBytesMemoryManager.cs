﻿using System;
using System.Buffers;
using System.Runtime.InteropServices;
using System.Threading;

namespace Iridescence.Memory
{
	/// <summary>
	/// Converts an existing <see cref="Memory{T}"/> to <see cref="byte"/>.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	[Serializable]
	internal sealed class AsBytesMemoryManager<T> : MemoryManager<byte>
		where T : struct
	{
		#region Fields

		private MemoryManager<T> memoryManager;
		private Memory<T> memory;
		private int byteSize;
		[NonSerialized] private int refCount;
		
		#endregion

		#region Properties

		#endregion

		#region Constructors

		public AsBytesMemoryManager(MemoryManager<T> memoryManager)
		{
			this.memoryManager = memoryManager ?? throw new ArgumentNullException(nameof(memoryManager));
			this.byteSize = -1;
		}

		public AsBytesMemoryManager(Memory<T> memory)
		{
			this.memory = memory;
			this.byteSize = -1;
		}

		public AsBytesMemoryManager(ReadOnlyMemory<T> memory)
			: this(MemoryMarshal.AsMemory(memory))
		{
			
		}

		#endregion

		#region Methods

		public override Span<byte> GetSpan()
		{
			Span<T> span;
			if (this.memoryManager != null)
			{
				span = this.memoryManager.GetSpan();
			}
			else
			{
				span = this.memory.Span;
			}

			return MemoryMarshal.AsBytes(span);
		}

		public override MemoryHandle Pin(int elementIndex = 0)
		{
			if (this.byteSize == -2)
				throw new ObjectDisposedException(nameof(AsBytesMemoryManager<T>));

			if (this.byteSize == -1)
				this.byteSize = this.GetSpan().Length;

			if (unchecked((uint)elementIndex) >= (uint)this.byteSize)
				throw new ArgumentOutOfRangeException(nameof(elementIndex));

			Interlocked.Increment(ref this.refCount);

			try
			{
				MemoryHandle handle;

				if (this.memoryManager != null)
				{
					handle = this.memoryManager.Pin();
				}
				else
				{
					handle = this.memory.Pin();
				}

				unsafe
				{
					byte* ptr = (byte*)handle.Pointer;
					ptr += elementIndex;

					return new MemoryHandle(ptr, pinnable: new MemoryHandlePinnable(this, handle));
				}
			}
			catch
			{
				this.Unpin();
				throw;
			}
		}

		public override void Unpin()
		{
			int newRefCount = Interlocked.Decrement(ref this.refCount);

			if (newRefCount < 0)
				throw new InvalidOperationException();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.memoryManager = null;
				this.memory = Memory<T>.Empty;
			}

			this.byteSize = -2;
		}

		#endregion

		#region Nested Types

		private sealed class MemoryHandlePinnable : IPinnable
		{
			private readonly AsBytesMemoryManager<T> owner;
			private MemoryHandle handle;

			public MemoryHandlePinnable(AsBytesMemoryManager<T> owner, MemoryHandle handle)
			{
				this.owner = owner;
				this.handle = handle;
			}

			public MemoryHandle Pin(int elementIndex)
			{
				throw new NotSupportedException();
			}

			public void Unpin()
			{
				this.owner.Unpin();
				this.handle.Dispose();
			}
		}

		#endregion
	}
}
