﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;

namespace Iridescence.Memory
{
	/// <summary>
	/// Emits code to map fields from one structure to another.
	/// </summary>
	public class StructMapConversion : IMemoryConversionCodeGenerator
	{
		#region Fields

		public IReadOnlyList<StructMapEntry> Entries { get; }

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StructMapConversion"/> class. 
		/// </summary>
		public StructMapConversion(IEnumerable<StructMapEntry> entries)
		{
			this.Entries = entries.ToReadOnlyList(true);

			foreach (StructMapEntry entry in this.Entries)
			{
				if (this.Entries.Count(e => e.Destination == entry.Destination) > 1)
					throw new ArgumentException($"Duplicate mapping for field \"{entry.Destination}\".", nameof(entries));
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="StructMapConversion"/> class. 
		/// </summary>
		public StructMapConversion(params StructMapEntry[] entries)
			: this((IEnumerable<StructMapEntry>)entries)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="StructMapConversion"/> class. 
		/// </summary>
		public StructMapConversion(params (string src, string dst)[] entries)
			: this(entries.Select(e => new StructMapFieldEntry(e.src, e.dst)))
		{

		}

		#endregion
		
		#region Methods

		public void Emit(ILBuilder emit, Type sourceType, Type destType, LoadFunc loadSource, StoreFunc store)
		{
			if (!sourceType.IsValueType)
				throw new ArgumentException("Source type must be a value type.", nameof(sourceType));

			if (!destType.IsValueType)
				throw new ArgumentException("Dest type must be a value type.", nameof(destType));

			using (Local source = emit.DeclareLocal(sourceType))
			using (Local dest = emit.DeclareLocal(destType))
			{
				loadSource(LoadMode.Value);
				emit.StoreLocal(source);

				LoadFunc loadSourceStruct = LoadStore.LoadLocal(emit, source);
				LoadFunc loadDestStruct = LoadStore.LoadLocal(emit, dest);

				foreach (StructMapEntry entry in this.Entries)
				{
					entry.Emit(emit, sourceType, destType, this, loadSourceStruct, GetFieldOrPropertyStoreFunc(emit, destType, entry.Destination, loadDestStruct));
				}

				using (store())
				{
					emit.LoadLocal(dest);
				}
			}
		}

		internal static LoadFunc GetFieldOrPropertyLoadFunc(ILBuilder emit, Type type, string name, LoadFunc loadInstance)
		{
			MemberInfo member = type.GetFieldOrProperty(name);
			if (member is PropertyInfo prop)
			{
				return LoadStore.LoadProperty(emit, prop, loadInstance);
			}

			return LoadStore.LoadField(emit, (FieldInfo)member, loadInstance);
		}

		internal static StoreFunc GetFieldOrPropertyStoreFunc(ILBuilder emit, Type type, string name, LoadFunc loadInstance)
		{
			MemberInfo member = type.GetFieldOrProperty(name);
			if (member is PropertyInfo prop)
			{
				return LoadStore.StoreProperty(emit, prop, loadInstance);
			}

			return LoadStore.StoreField(emit, (FieldInfo)member, loadInstance);
		}
		
		#endregion
	}
}
