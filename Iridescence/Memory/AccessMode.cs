﻿using System;

namespace Iridescence.Memory
{
	/// <summary>
	/// Defines memory access modes.
	/// </summary>
	[Flags]
	public enum AccessMode
	{
		/// <summary>
		/// Read access.
		/// Writing to the pinned address results in undefined behavior.
		/// </summary>
		Read = 1,

		/// <summary>
		/// Write access.
		/// Old contents will be discarded and have to be completely overwritten.
		/// Reading from the pinned address will result in undefined data.
		/// </summary>
		Write = 2,

		/// <summary>
		/// Both read and write access.
		/// </summary>
		ReadWrite = Read | Write,
	}
}
