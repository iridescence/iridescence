﻿using System;
using Iridescence.Emit;

namespace Iridescence.Memory
{
	/// <summary>
	/// Assigns the default value to a destination field.
	/// </summary>
	public class StructMapDefaultEntry : StructMapEntry
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StructMapDefaultEntry"/> class. 
		/// </summary>
		public StructMapDefaultEntry(string destination)
			: base(destination)
		{

		}

		#endregion
		
		#region Methods
		
		public override void Emit(ILBuilder emit, Type sourceType, Type destType, StructMapConversion conversion, LoadFunc loadSourceStruct, StoreFunc storeDestField)
		{
			Type destMemberType = destType.GetFieldOrPropertyType(this.Destination);

			using (storeDestField())
			{
				if (destMemberType == typeof(bool))
				{
					emit.LoadConstant(false);
				}
				else if (destMemberType == typeof(sbyte) || 
				         destMemberType == typeof(byte) || 
				         destMemberType == typeof(short) ||
				         destMemberType == typeof(ushort) ||
				         destMemberType == typeof(char) ||
				         destMemberType == typeof(int))
				{
					emit.LoadConstant(0);
				}
				else if (destMemberType == typeof(uint))
				{
					emit.LoadConstant(0U);
				}
				else if (destMemberType == typeof(long))
				{
					emit.LoadConstant(0L);
				}
				else if (destMemberType == typeof(ulong))
				{
					emit.LoadConstant(0UL);
				}
				else if (destMemberType == typeof(float))
				{
					emit.LoadConstant(0.0f);
				}
				else if (destMemberType == typeof(double))
				{
					emit.LoadConstant(0.0);
				}
				else if (destMemberType.IsValueType)
				{
					using (Local defaultLocal = emit.DeclareLocal(destMemberType, "default", false))
					{
						emit.LoadLocalAddress(defaultLocal);
						emit.InitializeObject(destMemberType);
						emit.LoadLocal(defaultLocal);
					}
				}
				else
				{
					emit.LoadNull();
				}
			}
		}
		
		#endregion
	}
}
