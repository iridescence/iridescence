﻿using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Represents a pooled array from an <see cref="ArrayPool{T}"/>.
	/// </summary>
	public readonly struct PooledArray<T> : IReadOnlyList<T>, IDisposable
	{
		#region Fields

		private readonly ArrayPool<T> pool;
		private readonly T[] array;
		private readonly int length;

		#endregion

		#region Properties

		public int Count => this.length;

		public T this[int index]
		{
			get
			{
				if (unchecked((uint)index) >= this.length)
					throw new ArgumentOutOfRangeException(nameof(index));

				return this.array[index];
			}
		}

		public ReadOnlyMemory<T> Memory => this.array == null ? ReadOnlyMemory<T>.Empty : new ReadOnlyMemory<T>(this.array, 0, this.length);

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PooledArray{T}"/> class. 
		/// </summary>
		public PooledArray(ArrayPool<T> pool, T[] array, int length)
		{
			this.pool = pool;
			this.array = array;
			this.length = length;
		}

		#endregion

		#region Methods

		public IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < this.length; ++i)
			{
				yield return this.array[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		public void Dispose()
		{
			if (this.array != null)
				this.pool.Return(this.array, true);
		}
		
		#endregion
	}
}
