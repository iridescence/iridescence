﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Iridescence
{
	/// <summary>
	/// Represents a dictionary that only keeps weak-references to the keys.
	/// The values are kept alive as long as the associated key is alive.
	/// </summary>
	/// <remarks>
	/// This dictionary wraps a CnditionalWeakTable and exposes additional functionality, such as clearing and retrieving the key and value collections.
	/// </remarks>
	public class WeakDictionary<TKey, TValue> : IDictionary<TKey, TValue>
		where TKey : class
		where TValue : class
	{
		#region Fields

		private readonly object mutex;
		private ConditionalWeakTable<TKey, TValue> table;
		private readonly WeakCollection<TKey> keys;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the number of entries in the dictionary.
		/// </summary>
		/// <remarks>
		/// This number does not necessarily represents the number of live key-value-pairs in the collection.
		/// </remarks>
		public int Count => this.keys.Count;

		/// <summary>
		/// Gets or sets the value associated with the specified key.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public TValue this[TKey key]
		{
			get
			{
				TValue value;
				if (!this.table.TryGetValue(key, out value))
					throw new KeyNotFoundException();
				
				return value;
			}
			set
			{
				lock (this.mutex)
				{
					this.Remove(key);
					this.Add(key, value);
				}
			}
		}

		/// <summary>
		/// Gets the key collection of the dictionary.
		/// </summary>
		public ICollection<TKey> Keys { get; }

		/// <summary>
		/// Gets the value collection of the dictionary.
		/// </summary>
		public ICollection<TValue> Values { get; }

		bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly => false;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the WeakDictionary class. 
		/// </summary>
		public WeakDictionary()
		{
			this.mutex = new object();
			this.table = new ConditionalWeakTable<TKey, TValue>();

			if (typeof(TKey) == typeof(string))
			{
				this.keys = new WeakCollection<TKey>();	
			}
			else
			{
				this.keys = new WeakCollection<TKey>(new ReferenceEqualityComparer());
			}
			
			this.Keys = new KeyCollection(this);
			this.Values = new ValueCollection(this);
		}

		#endregion

		#region Methods

		private void add(TKey key, TValue value)
		{
			this.table.Add(key, value);
			this.keys.Add(key);	
		}

		private bool remove(TKey key)
		{
			this.table.Remove(key);
			return this.keys.Remove(key);
		}

		/// <summary>
		/// Adds the specified key/value pair to the dictionary.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public void Add(TKey key, TValue value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			lock (this.mutex)
			{
				this.add(key, value);
			}
		}

		/// <summary>
		/// Uses the specified functions to add a key/value pair if the key does not already exist, or to update a key/value pair if the key already exists.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="addValueFactory"></param>
		/// <param name="updateValueFactory"></param>
		/// <returns></returns>
		public TValue AddOrUpdate(TKey key, Func<TKey, TValue> addValueFactory, Func<TKey, TValue, TValue> updateValueFactory)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			lock (this.mutex)
			{
				if (this.table.TryGetValue(key, out TValue currentValue))
				{
					TValue newValue = updateValueFactory(key, currentValue);
					if (!object.ReferenceEquals(currentValue, newValue))
					{
						this.table.Remove(key);
						this.table.Add(key, newValue);
					}
					return newValue;
				}

				TValue value = addValueFactory(key);
				this.add(key, value);
				return value;
			}
		}

		/// <summary>
		/// Adds a key/value pair if the key does not already exist, or updates a key/value pair by using the specified function if the key already exists.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="addValue"></param>
		/// <param name="updateValueFactory"></param>
		/// <returns></returns>
		public TValue AddOrUpdate(TKey key, TValue addValue, Func<TKey, TValue, TValue> updateValueFactory)
		{
			return this.AddOrUpdate(key, k => addValue, updateValueFactory);
		}

		/// <summary>
		/// Adds a key/value pair by using the specified function if the key does not already exist and returns the new value.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="valueFactory"></param>
		/// <returns></returns>
		public TValue GetOrAdd(TKey key, Func<TKey, TValue> valueFactory)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			lock (this.mutex)
			{
				if (this.table.TryGetValue(key, out TValue currentValue))
					return currentValue;

				TValue value = valueFactory(key);
				this.add(key, value);
				return value;
			}
		}

		/// <summary>
		/// Adds a key/value pair if the key does not already exist and returns the new value.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public TValue GetOrAdd(TKey key, TValue value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			lock (this.mutex)
			{
				if (this.table.TryGetValue(key, out TValue currentValue))
					return currentValue;

				this.add(key, value);
				return value;
			}
		}

		/// <summary>
		/// Attempts to add the specified key and value.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool TryAdd(TKey key, TValue value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			lock (this.mutex)
			{
				if (this.table.TryGetValue(key, out _))
					return false;

				this.add(key, value);
				return true;
			}
		}

		/// <summary>
		/// Compares the existing value for the specified key with a specified value, and if they are equal, updates the key with a third value.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="newValue"></param>
		/// <param name="comparisonValue"></param>
		/// <returns></returns>
		public bool TryUpdate(TKey key, TValue newValue, TValue comparisonValue)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			lock (this.mutex)
			{
				if (this.table.TryGetValue(key, out TValue currentValue))
					return false;

				if (object.ReferenceEquals(currentValue, comparisonValue))
				{
					this.table.Remove(key);
					this.table.Add(key, newValue);
					return true;
				}

				return false;
			}
		}

		/// <summary>
		/// Removes the specified key from the dictionary.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool Remove(TKey key)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			lock (this.mutex)
			{
				return this.remove(key);
			}
		}

		/// <summary>
		/// Attempts to remove the specified key and returns the value associated with it, if at has been removed.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool TryRemove(TKey key, out TValue value)
		{
			if (key == null)
				throw new ArgumentNullException(nameof(key));

			lock (this.mutex)
			{
				if (this.table.TryGetValue(key, out value))
					return this.remove(key);
			}

			return false;
		}

		/// <summary>
		/// Clears the dictionary.
		/// </summary>
		public void Clear()
		{
			lock (this.mutex)
			{
				this.table = new ConditionalWeakTable<TKey, TValue>();
				this.keys.Clear();
			}
		}

		/// <summary>
		/// Tries to retrieve the value associated with the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value associated with the key.</param>
		/// <returns></returns>
		public bool TryGetValue(TKey key, out TValue value)
		{
			return this.table.TryGetValue(key, out value);
		}

		/// <summary>
		/// Returns a value that indicates whether the specified key exists in the dictionary.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool ContainsKey(TKey key)
		{
			return this.table.TryGetValue(key, out _);
		}

		void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
		{
			this.Add(item.Key, item.Value);
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
		{
			if (!this.table.TryGetValue(item.Key, out TValue value))
				return false;

			return object.ReferenceEquals(item.Value, value);
		}

		void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			lock (this.mutex)
			{
				foreach (KeyValuePair<TKey, TValue> pair in this)
				{
					array[arrayIndex++] = pair;
				}
			}
		}

		bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
		{
			lock (this.mutex)
			{
				if (!this.table.TryGetValue(item.Key, out TValue value))
					return false;

				if (!object.ReferenceEquals(item.Value, value))
					return false;

				return this.Remove(item.Key);
			}
		}

		IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
		{
			foreach (TKey key in this.keys)
			{
				if (this.table.TryGetValue(key, out TValue value))
					yield return new KeyValuePair<TKey, TValue>(key, value);
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return ((ICollection<KeyValuePair<TKey, TValue>>)this).GetEnumerator();
		}

		#endregion

		#region Nested Types

		private sealed class ReferenceEqualityComparer : IEqualityComparer<TKey>
		{
			public bool Equals(TKey x, TKey y)
			{
				return ReferenceEquals(x, y);
			}

			public int GetHashCode(TKey obj)
			{
				return RuntimeHelpers.GetHashCode(obj);
			}
		}

		private sealed class KeyCollection : ICollection<TKey>
		{
			#region Fields

			private readonly WeakDictionary<TKey, TValue> dictionary;

			#endregion

			#region Properties

			public int Count => this.dictionary.Count;
			public bool IsReadOnly => true;

			#endregion

			#region Constructors

			public KeyCollection(WeakDictionary<TKey, TValue> dictionary)
			{
				this.dictionary = dictionary;
			}

			#endregion

			#region Methods

			public void Add(TKey item)
			{
				throw new NotSupportedException("The collection is read-only.");
			}

			public bool Remove(TKey item)
			{
				throw new NotSupportedException("The collection is read-only.");
			}

			public void Clear()
			{
				throw new NotSupportedException("The collection is read-only.");
			}

			public bool Contains(TKey item)
			{
				return this.dictionary.keys.Contains(item);
			}

			public void CopyTo(TKey[] array, int arrayIndex)
			{
				((ICollection<TKey>)this.dictionary.keys).CopyTo(array, arrayIndex);
			}

			IEnumerator<TKey> IEnumerable<TKey>.GetEnumerator()
			{
				return ((IEnumerable<TKey>)this.dictionary.keys).GetEnumerator();
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return ((IEnumerable<TKey>)this).GetEnumerator();
			}

			#endregion
		}

		private sealed class ValueCollection : ICollection<TValue>
		{
			#region Fields

			private readonly WeakDictionary<TKey, TValue> dictionary;

			#endregion

			#region Properties

			public int Count => this.dictionary.Count;
			public bool IsReadOnly => true;

			#endregion

			#region Constructors

			public ValueCollection(WeakDictionary<TKey, TValue> dictionary)
			{
				this.dictionary = dictionary;
			}

			#endregion

			#region Methods

			public void Add(TValue item)
			{
				throw new NotSupportedException("The collection is read-only.");
			}

			public bool Remove(TValue item)
			{
				throw new NotSupportedException("The collection is read-only.");
			}

			public void Clear()
			{
				throw new NotSupportedException("The collection is read-only.");
			}

			public bool Contains(TValue item)
			{
				return this.Any(v => object.ReferenceEquals(item, v));
			}

			public void CopyTo(TValue[] array, int arrayIndex)
			{
				foreach (TValue value in this)
					array[arrayIndex++] = value;
			}

			IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
			{
				return this.dictionary.Select(pair => pair.Value).GetEnumerator();
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return ((IEnumerable<TValue>)this).GetEnumerator();
			}

			#endregion
		}

		#endregion
}
}
