﻿namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// Implements an event router.
	/// </summary>
	public static class EventRouter
	{
		#region Methods

		/// <summary>
		/// Routes an event.
		/// </summary>
		/// <param name="event"></param>
		/// <param name="source"></param>
		/// <returns>True, if the event was handled. False otherwise.</returns>
		public static bool Invoke(this IRoutedEvent @event, IRoutingNode source)
		{
			IRoutingStrategy strategy = @event.Strategy;
			foreach (IRoutingNode node in strategy.GetNodes(source))
			{
				if (@event.Handle(node))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Routes an event.
		/// </summary>
		/// <param name="event"></param>
		/// <param name="source"></param>
		/// <param name="eventArgs"></param>
		/// <returns>True, if the event was handled. False otherwise.</returns>
		public static bool Invoke<T>(this IRoutedEvent<T> @event, IRoutingNode source, T eventArgs)
		{
			IRoutingStrategy strategy = @event.Strategy;
			foreach (IRoutingNode node in strategy.GetNodes(source))
			{
				if (@event.Handle(node, eventArgs))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Returns the root node of the specified <see cref="IRoutingNode"/>.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public static IRoutingNode GetRoot(this IRoutingNode node)
		{
			IRoutingNode current = node;

			for (;;)
			{
				IRoutingNode parent = current.Parent;
				if (parent == null)
					return node;
				current = parent;
			}
		}

		#endregion
	}
}
