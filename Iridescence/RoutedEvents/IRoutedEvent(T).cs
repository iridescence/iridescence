﻿namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// Interface for routed events with a generic argument.
	/// </summary>
	/// <typeparam name="T">The argument type.</typeparam>
	public interface IRoutedEvent<in T>
	{
		#region Properties

		/// <summary>
		/// Gets the routing strategy of this event.
		/// </summary>
		IRoutingStrategy Strategy { get; }
		
		#endregion

		#region Methods

		/// <summary>
		/// Handles the event for the specified <see cref="IRoutingNode"/>.
		/// </summary>
		/// <param name="node">The <see cref="IRoutingNode"/> that is supposed to handle the event.</param>
		/// <param name="args">The event arguments.</param>
		/// <returns>True, if the <see cref="IRoutingNode"/> handled the event and routing should terminate. False otherwise.</returns>
		bool Handle(IRoutingNode node, T args);
		
		#endregion
	}
}
