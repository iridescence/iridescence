﻿using System;

namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// Delegate for functions that invoke events.
	/// </summary>
	/// <typeparam name="TTarget"></typeparam>
	/// <typeparam name="TArgs"></typeparam>
	/// <param name="target"></param>
	/// <param name="args"></param>
	public delegate void EventInvoker<in TTarget, in TArgs>(TTarget target, TArgs args);

	/// <summary>
	/// Implements a <see cref="IRoutedEvent{T}"/> that simply invokes a handler delegate.
	/// </summary>
	/// <typeparam name="TTarget"></typeparam>
	/// <typeparam name="TArgs"></typeparam>
	public class RoutedEvent<TTarget, TArgs> : IRoutedEvent<TArgs>
		where TArgs : RoutedEventArgs
	{
		#region Fields

		private readonly EventInvoker<TTarget, TArgs> invoke;
		
		#endregion

		#region Properties
		
		/// <summary>
		/// Gets the routing strategy.
		/// </summary>
		public IRoutingStrategy Strategy { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="RoutedEvent{TTarget, TArgs}"/> class. 
		/// </summary>
		public RoutedEvent(IRoutingStrategy strategy, EventInvoker<TTarget, TArgs> invoke)
		{
			this.Strategy = strategy ?? throw new ArgumentNullException(nameof(strategy));
			this.invoke = invoke ?? throw new ArgumentNullException(nameof(invoke));
		}

		#endregion

		#region Methods

		public bool Handle(IRoutingNode node, TArgs args)
		{
			if (!(node is TTarget))
				return false;

			TTarget target = (TTarget)node;
			this.invoke(target, args);

			return args.Handled;
		}
		
		#endregion
	}
}
