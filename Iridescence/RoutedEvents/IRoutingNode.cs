﻿using System.Collections.Generic;

namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// An interface for objects that can route events.
	/// </summary>
	public interface IRoutingNode
	{
		#region Properties
		
		/// <summary>
		/// Gets the parent command handler.
		/// </summary>
		IRoutingNode Parent { get; }
		
		/// <summary>
		/// Gets an enumerable of child nodes.
		/// </summary>
		IEnumerable<IRoutingNode> Children { get; }

		#endregion
	}
}
