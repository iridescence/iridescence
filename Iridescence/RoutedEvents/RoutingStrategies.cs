﻿namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// Provides the standard routing strategies.
	/// </summary>
	public static class RoutingStrategies
	{
		#region Properties

		/// <summary>
		/// Gets the source to root routing strategy.
		/// </summary>
		public static IRoutingStrategy SourceToRoot { get; } = new SourceToRootStrategy();

		#endregion
	}
}
