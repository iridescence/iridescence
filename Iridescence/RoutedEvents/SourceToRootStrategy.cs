﻿using System.Collections.Generic;

namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// Implements the source to root routing strategy.
	/// </summary>
	internal sealed class SourceToRootStrategy : IRoutingStrategy
	{
		#region Methods

		public IEnumerable<IRoutingNode> GetNodes(IRoutingNode source)
		{
			IRoutingNode current = source;

			while(current != null)
			{
				yield return current;
				current = current.Parent;
			}
		}
		
		#endregion
	}
}
