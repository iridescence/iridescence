﻿using System;

namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// Routed event args.
	/// </summary>
	public abstract class RoutedEventArgs : EventArgs
	{
		#region Fields

		#endregion

		#region Properties
		
		/// <summary>
		/// Gets or sets a value that determines whether the event was handled.
		/// </summary>
		public bool Handled { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="RoutedEventArgs"/> class. 
		/// </summary>
		protected RoutedEventArgs()
		{

		}

		#endregion

		#region Methods

		#endregion
	}
}
