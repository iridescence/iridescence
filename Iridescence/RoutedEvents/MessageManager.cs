﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// Handles the specified message.
	/// </summary>
	/// <param name="source">The source of the message.</param>
	/// <param name="message">The <see cref="RoutedMessage"/> that was sent.</param>
	/// <returns>True, if the message was handled. False otherwise.</returns>
	public delegate bool MessageHandler<in T>(T source, RoutedMessage message);

	/// <summary>
	/// Handles the specified message.
	/// </summary>
	/// <param name="source">The source of the message.</param>
	/// <param name="message">The <see cref="RoutedMessage"/> that was sent.</param>
	/// <param name="args">The message arguments.</param>
	/// <returns>True, if the message was handled. False otherwise.</returns>
	public delegate bool MessageHandler<in T, TArgs>(T source, RoutedMessage<TArgs> message, TArgs args);

	/// <summary>
	/// Handles the specified message.
	/// </summary>
	/// <param name="source">The source of the message.</param>
	/// <param name="message">The <see cref="RoutedMessage"/> that was sent.</param>
	/// <returns>True, if the message was handled. False otherwise.</returns>
	public delegate ValueTask<bool> MessageHandlerAsync<in T>(T source, RoutedMessage message);

	/// <summary>
	/// Handles the specified message.
	/// </summary>
	/// <param name="source">The source of the message.</param>
	/// <param name="message">The <see cref="RoutedMessage"/> that was sent.</param>
	/// <param name="args">The message arguments.</param>
	/// <returns>True, if the message was handled. False otherwise.</returns>
	public delegate ValueTask<bool> MessageHandlerAsync<in T, TArgs>(T source, RoutedMessage<TArgs> message, TArgs args);
	
	/// <summary>
	/// Manages handling of messages.
	/// </summary>
	public class MessageManager<T> : IMessageManager where T : class, IRoutingNode
	{
		#region Nested Types

		private interface IMessageHandlerInvoker
		{
			bool Invoke(IRoutingNode node);
		}

		private interface IMessageHandlerInvoker<in TArgs>
		{
			bool Invoke(IRoutingNode node, TArgs args);
		}

		private abstract class MessageHandlerBase : IEquatable<MessageHandlerBase>
		{
			public abstract object Filter { get; }
			public abstract bool Equals(MessageHandlerBase other);
		}

		private class MessageHandlerInfo : MessageHandlerBase, IMessageHandlerInvoker
		{
			public RoutedMessage Message { get; }
			public override object Filter => this.Message;
			public MessageHandler<T> Handler { get; }

			public MessageHandlerInfo(RoutedMessage message, MessageHandler<T> handler)
			{
				this.Message = message;
				this.Handler = handler;
			}

			public bool Invoke(IRoutingNode node)
			{
				if (!(node is T))
					return false;
				return this.Handler((T)node, this.Message);
			}

			public override bool Equals(MessageHandlerBase obj)
			{
				if (!(obj is MessageHandlerInfo other))
					return false;

				return Equals(this.Message, other.Message) && Equals(this.Handler, other.Handler);
			}

			public override int GetHashCode()
			{
				return this.Message.GetHashCode() ^ this.Handler.GetHashCode();
			}
		}

		private class MessageHandlerInfo<TArgs> : MessageHandlerBase, IMessageHandlerInvoker<TArgs>
		{
			public RoutedMessage<TArgs> Message { get; }
			public override object Filter => this.Message;
			public MessageHandler<T, TArgs> Handler { get; }

			public MessageHandlerInfo(RoutedMessage<TArgs> message, MessageHandler<T, TArgs> handler)
			{
				this.Message = message;
				this.Handler = handler;
			}

			public bool Invoke(IRoutingNode node, TArgs args)
			{
				if (!(node is T))
					return false;
				return this.Handler((T)node, this.Message, args);
			}

			public override bool Equals(MessageHandlerBase obj)
			{
				if (!(obj is MessageHandlerInfo<TArgs> other))
					return false;

				return Equals(this.Message, other.Message) && Equals(this.Handler, other.Handler);
			}
			
			public override int GetHashCode()
			{
				return this.Message.GetHashCode() ^ this.Handler.GetHashCode();
			}
		}

		private sealed class RoutingNodeHandlers
		{
			public HashSet<MessageHandlerBase> HandlerInfos { get; }

			public RoutingNodeHandlers()
			{
				this.HandlerInfos = new HashSet<MessageHandlerBase>();
			}
		}
		
		#endregion

		#region Fields

		private readonly ConditionalWeakTable<T, RoutingNodeHandlers> nodes;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MessageManager2"/> class. 
		/// </summary>
		public MessageManager()
		{
			this.nodes = new ConditionalWeakTable<T, RoutingNodeHandlers>();
		}

		#endregion

		#region Methods

		#region AddHandler

		/// <summary>
		/// Registers the specified <see cref="MessageHandler{TSource}"/> as a handler for the <see cref="RoutedMessage"/> on the specified object.
		/// The object is weakly referenced and not kept alive.
		/// </summary>
		/// <param name="message">The message to handle.</param>
		/// <param name="obj">The object (implementing <see cref="IRoutingNode"/>) that handles the message.</param>
		/// <param name="handler">A handler delegate to invoke when the message is received by the object.</param>
		/// <returns>True, if the handler was added succesfully. False, if the handler is already registered.</returns>
		public bool AddHandler(RoutedMessage message, T obj, MessageHandler<T> handler)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			if (handler == null)
				throw new ArgumentNullException(nameof(handler));

			RoutingNodeHandlers handlers = this.nodes.GetOrCreateValue(obj);

			bool result;
			lock (handlers.HandlerInfos)
			{
				result = handlers.HandlerInfos.Add(new MessageHandlerInfo(message, handler));
			}
			
			if (result)
				message.InvokeHandlersChanged();

			return result;
		}

		/// <summary>
		/// Registers the specified <see cref="MessageHandler{T}"/> as a handler for the <see cref="RoutedMessage{TArgs}"/> on the specified object.
		/// The object is weakly referenced and not kept alive.
		/// </summary>
		/// <param name="message">The message to handle.</param>
		/// <param name="obj">The object (implementing <see cref="IRoutingNode"/>) that handles the message.</param>
		/// <param name="handler">A handler delegate to invoke when the message is received by the object.</param>
		/// <returns>True, if the handler was added succesfully. False, if the handler is already registered.</returns>
		public bool AddHandler<TArgs>(RoutedMessage<TArgs> message, T obj, MessageHandler<T, TArgs> handler)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			if (handler == null)
				throw new ArgumentNullException(nameof(handler));

			RoutingNodeHandlers handlers = this.nodes.GetOrCreateValue(obj);

			bool result;
			lock (handlers.HandlerInfos)
			{
				result = handlers.HandlerInfos.Add(new MessageHandlerInfo<TArgs>(message, handler));
			}

			if (result)
				message.InvokeHandlersChanged();

			return result;
		}

		#endregion

		#region RemoveHandler

		/// <summary>
		/// Removes the specified <see cref="MessageHandler{T}"/> as a handler for the <see cref="RoutedMessage"/> on the specified object.
		/// </summary>
		/// <param name="message">The message that should not be handled anymore.</param>
		/// <param name="obj">The object (implementing <see cref="IRoutingNode"/>) that currently handles the message.</param>
		/// <param name="handler">A handler delegate that is invoked when the message is received by the object.</param>
		/// <returns>True, if the handler was removed succesfully. False, if the handler is not registered.</returns>
		public bool RemoveHandler(RoutedMessage message, T obj, MessageHandler<T> handler)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			if (handler == null)
				throw new ArgumentNullException(nameof(handler));

			if (!this.nodes.TryGetValue(obj, out RoutingNodeHandlers handlers))
				return false;

			bool result;
			lock (handlers.HandlerInfos)
			{
				result = handlers.HandlerInfos.Remove(new MessageHandlerInfo(message, handler));
			}

			if (result)
				message.InvokeHandlersChanged();

			return result;
		}

		/// <summary>
		/// Removes the specified <see cref="MessageHandler{T}"/> as a handler for the <see cref="RoutedMessage{TArgs}"/> on the specified object.
		/// </summary>
		/// <param name="message">The message that should not be handled anymore.</param>
		/// <param name="obj">The object (implementing <see cref="IRoutingNode"/>) that currently handles the message.</param>
		/// <param name="handler">A handler delegate that is invoked when the message is received by the object.</param>
		/// <returns>True, if the handler was removed succesfully. False, if the handler is not registered.</returns>
		public bool RemoveHandler<TArgs>(RoutedMessage<TArgs> message, T obj, MessageHandler<T, TArgs> handler)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			if (handler == null)
				throw new ArgumentNullException(nameof(handler));

			if (!this.nodes.TryGetValue(obj, out RoutingNodeHandlers handlers))
				return false;

			bool result;
			lock (handlers.HandlerInfos)
			{
				result = handlers.HandlerInfos.Remove(new MessageHandlerInfo<TArgs>(message, handler));
			}
			
			if (result)
				message.InvokeHandlersChanged();

			return result;
		}

		#endregion

		#region GetHandlers

		/// <summary>
		/// Returns all <see cref="MessageHandler{T}"/> that handle the <see cref="RoutedMessage"/> on the specified object.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="obj"></param>
		/// <returns></returns>
		public IEnumerable<MessageHandler<T>> GetHandlers(RoutedMessage message, T obj)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			if (!this.nodes.TryGetValue(obj, out RoutingNodeHandlers handlers))
				yield break;

			lock (handlers.HandlerInfos)
			{
				foreach (MessageHandlerBase item in handlers.HandlerInfos)
				{
					if(!Equals(item.Filter, message))
						continue;

					if (item is MessageHandlerInfo info)
						yield return info.Handler;
				}
			}
		}

		/// <summary>
		/// Returns all <see cref="MessageHandler{T}"/> that handle the <see cref="RoutedMessage{TArgs}"/> on the specified object.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="obj"></param>
		/// <returns></returns>
		public IEnumerable<MessageHandler<T, TArgs>> GetHandlers<TArgs>(RoutedMessage<TArgs> message, T obj)
		{
			if (message == null)
				throw new ArgumentNullException(nameof(message));

			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			if (!this.nodes.TryGetValue(obj, out RoutingNodeHandlers handlers))
				yield break;

			lock (handlers.HandlerInfos)
			{
				foreach (MessageHandlerBase item in handlers.HandlerInfos)
				{
					if(!Equals(item.Filter, message))
						continue;

					if (item is MessageHandlerInfo<TArgs> info)
						yield return info.Handler;
				}
			}
		}

		IEnumerable<RoutingNodeMessageHandler> IMessageManager.GetHandlers(RoutedMessage message, IRoutingNode node)
		{
			if (!(node is T))
				yield break;

			T obj = (T)node;
			
			if (!this.nodes.TryGetValue(obj, out RoutingNodeHandlers handlers))
				yield break;
			
			lock (handlers.HandlerInfos)
			{
				foreach (MessageHandlerBase item in handlers.HandlerInfos)
				{
					if(!Equals(item.Filter, message))
						continue;

					if (item is IMessageHandlerInvoker invoker)
						yield return invoker.Invoke;
				}
			}
		}

		IEnumerable<RoutingNodeMessageHandler<TArgs>> IMessageManager.GetHandlers<TArgs>(RoutedMessage<TArgs> message, IRoutingNode node)
		{
			if (!(node is T))
				yield break;

			T obj = (T)node;

			if (!this.nodes.TryGetValue(obj, out RoutingNodeHandlers handlers))
				yield break;
			
			lock (handlers.HandlerInfos)
			{
				foreach (MessageHandlerBase item in handlers.HandlerInfos)
				{
					if(!Equals(item.Filter, message))
						continue;

					if (item is IMessageHandlerInvoker<TArgs> invoker)
						yield return invoker.Invoke;
				}
			}
		}

		#endregion

		#endregion
	}
}
