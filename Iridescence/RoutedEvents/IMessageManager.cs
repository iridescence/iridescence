﻿using System.Collections.Generic;

namespace Iridescence.RoutedEvents
{
	public delegate bool RoutingNodeMessageHandler(IRoutingNode source);

	public delegate bool RoutingNodeMessageHandler<in TArgs>(IRoutingNode source, TArgs args);

	/// <summary>
	/// Interface for a message manager that provides <see cref="RoutingNodeMessageHandler">message handlers</see> for <see cref="IRoutingNode">nodes</see>.
	/// </summary>
	public interface IMessageManager
	{
		IEnumerable<RoutingNodeMessageHandler> GetHandlers(RoutedMessage message, IRoutingNode node);

		IEnumerable<RoutingNodeMessageHandler<TArgs>> GetHandlers<TArgs>(RoutedMessage<TArgs> message, IRoutingNode node);
	}
}
