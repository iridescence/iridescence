﻿using System.Collections.Generic;

namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// Interface for routing strategies.
	/// </summary>
	public interface IRoutingStrategy
	{
		#region Methods

		/// <summary>
		/// Returns the nodes that an event is routed to using this strategy.
		/// </summary>
		/// <param name="source">The node where the event occured.</param>
		/// <returns>An enumeration of routing nodes. The event is offered to the nodes in the order they appear in the enumeration.</returns>
		IEnumerable<IRoutingNode> GetNodes(IRoutingNode source);

		#endregion
	}
}
