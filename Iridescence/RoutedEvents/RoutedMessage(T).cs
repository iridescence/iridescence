﻿using System;
using System.Collections.Generic;

namespace Iridescence.RoutedEvents
{
	/// <summary>
	/// Represents an abstract routed message.
	/// </summary>
	public abstract class RoutedMessage<T> : IRoutedEvent<T>
	{
		#region Events

		/// <summary>
		/// Occurs when a handler for this message have been added or removed in the <see cref="MessageManager"/>.
		/// </summary>
		public event EventHandler HandlersChanged;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the <see cref="IMessageManager"/> associated with this <see cref="RoutedMessage"/>.
		/// </summary>
		public IMessageManager Manager { get; }

		public IRoutingStrategy Strategy { get; }

		#endregion

		#region Constructors

		protected RoutedMessage(IMessageManager manager, IRoutingStrategy strategy)
		{
			this.Manager = manager;
			this.Strategy = strategy;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets the <see cref="RoutingNodeMessageHandler{TArgs}">message handlers</see> that handle this <see cref="RoutedMessage{T}"/> for the specified <see cref="IRoutingNode"/>.
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
		public IEnumerable<RoutingNodeMessageHandler<T>> GetHandlers(IRoutingNode node)
		{
			return this.Manager.GetHandlers(this, node);
		}

		public bool Handle(IRoutingNode node, T args)
		{
			foreach (RoutingNodeMessageHandler<T> handler in this.GetHandlers(node))
			{
				if (handler(node, args))
					return true;
			}

			return false;
		}

		internal void InvokeHandlersChanged()
		{
			this.HandlersChanged?.Invoke(this, EventArgs.Empty);
		}
		
		#endregion
	}
}
