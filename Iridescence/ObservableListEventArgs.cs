﻿using System;

namespace Iridescence
{
	/// <summary>
	/// Item event args.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ObservableListEventArgs<T> : EventArgs
	{
		#region Properties
		
		/// <summary>
		/// Gets the item's index in the list.
		/// </summary>
		public int Index { get; }

		/// <summary>
		/// Gets the item.
		/// </summary>
		public T Item { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates new observable list event args.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		public ObservableListEventArgs(int index, T item)
		{
			this.Index = index;
			this.Item = item;
		}

		#endregion
	}
}