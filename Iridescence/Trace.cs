﻿using System.Runtime.CompilerServices;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Provides simple logging functionality.
	/// </summary>
	public static class Trace
	{
		#region Events
	
		private static int traceID;
		private static readonly ConditionalWeakTable<object, TraceInfo> traceInfo = new ConditionalWeakTable<object, TraceInfo>();
		
		#endregion

		#region Methods

		/// <summary>
		/// Writes an error message.
		/// </summary>
		public static void WriteCritical(string message, [CallerFilePath] string sourceFile = "?", [CallerLineNumber] int sourceLine = 0)
		{
			TraceEventSource.Instance.WriteCritical(message, $"{sourceFile}:{sourceLine}");
		}

		/// <summary>
		/// Writes an error message.
		/// </summary>
		public static void WriteError(string message, [CallerFilePath] string sourceFile = "?", [CallerLineNumber] int sourceLine = 0)
		{
			TraceEventSource.Instance.WriteError(message, $"{sourceFile}:{sourceLine}");
		}
		
		/// <summary>
		/// Writes a warning message.
		/// </summary>
		public static void WriteWarning(string message, [CallerFilePath] string sourceFile = "?", [CallerLineNumber] int sourceLine = 0)
		{
			TraceEventSource.Instance.WriteWarning(message, $"{sourceFile}:{sourceLine}");
		}
		
		/// <summary>
		/// Writes an informational message.
		/// </summary>
		public static void WriteInformation(string message, [CallerFilePath] string sourceFile = "?", [CallerLineNumber] int sourceLine = 0)
		{
			TraceEventSource.Instance.WriteInformation(message, $"{sourceFile}:{sourceLine}");
		}
		
		/// <summary>
		/// Writes a verbose debug message.
		/// </summary>
		public static void WriteVerbose(string message, [CallerFilePath] string sourceFile = "?", [CallerLineNumber] int sourceLine = 0)
		{
			TraceEventSource.Instance.WriteVerbose(message, $"{sourceFile}:{sourceLine}");
		}
		
		public static int GetTraceObjectID(object obj)
		{
			return traceInfo.GetValue(obj, o => new TraceInfo(Interlocked.Increment(ref traceID))).ID;
		}

		#endregion
	}
}
