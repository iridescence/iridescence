﻿using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Interface identical to <see cref="IComparer{T}"/>, except that the values to compare are passed by reference.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IRefComparer<T>
	{
		int Compare(ref T x, ref T y);
	}

}
