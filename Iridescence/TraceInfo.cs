﻿namespace Iridescence
{
	internal sealed class TraceInfo
	{
		public readonly int ID;

		public TraceInfo(int id)
		{
			this.ID = id;
		}
	}
}