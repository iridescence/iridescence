﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Guards code against recursive execution.
	/// </summary>
	public sealed class RecursionGuard
	{
		#region Fields

		private int value;

		#endregion
		
		#region Methods

		/// <summary>
		/// Protects code inside a 'using' block against recursion.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public RecursionLock Protect([CallerMemberName] string name = null)
		{
			if (Interlocked.CompareExchange(ref this.value, 1, 0) != 0)
				throw new InvalidOperationException($"Recursive call to \"{name}\" is not allowed.");

			return new RecursionLock(this);
		}

		#endregion

		#region Nested Types

		public readonly struct RecursionLock : IDisposable
		{
			private readonly RecursionGuard guard;

			internal RecursionLock(RecursionGuard guard)
			{
				this.guard = guard;
			}

			public void Dispose()
			{
				if (Interlocked.CompareExchange(ref this.guard.value, 0, 1) != 1)
					throw new InvalidOperationException("Recursion protection violated.");
			}
		}

		#endregion
	}
}
