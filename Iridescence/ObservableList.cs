﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence
{
	/// <summary>
	/// Represents a list that allows custom behavior on add and remove events.
	/// </summary>
	[Serializable]
	public class ObservableList<T> : IObservableList<T>, IObservableReadOnlyList<T>, IList
	{
		#region Events

		/// <summary>
		/// Occurs when an item was added to the list.
		/// </summary>
		[field: NonSerialized]
		public event ObservableListEventHandler<T> Added;

		/// <summary>
		/// Occurs when an item was removed from the list.
		/// </summary>
		[field: NonSerialized]
		public event ObservableListEventHandler<T> Removed;

		#endregion

		#region Fields

		private readonly List<T> list;

		#endregion

		#region Properties

		public object SyncRoot => this.list;

		/// <summary>
		/// Gets a value that determines whether all access to this collection is synchronized.
		/// </summary>
		public bool IsSynchronized => false;

		/// <summary>
		/// Gets or sets the item at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public T this[int index]
		{
			get => this.list[index];
			set => this.exchange(index, value);
		}

		/// <summary>
		/// Gets the number of items in the collection.
		/// </summary>
		public int Count => this.list.Count;

		bool ICollection<T>.IsReadOnly => false;

		bool IList.IsReadOnly => false;

		bool IList.IsFixedSize => false;

		object IList.this[int index]
		{
			get => this[index];
			set => this[index] = (T)value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="ObservableList{T}"/>.
		/// </summary>
		public ObservableList()
		{
			this.list = new List<T>();
		}
		
		/// <summary>
		/// Creates a new <see cref="ObservableList{T}"/> with the specified initial items.
		/// </summary>
		public ObservableList(params T[] items)
		{
			this.list = new List<T>(items);
		}

		/// <summary>
		/// Creates a new <see cref="ObservableList{T}"/> with the specified initial items.
		/// </summary>
		public ObservableList(IEnumerable<T> collection)
		{
			this.list = new List<T>(collection);
		}

		#endregion

		#region Methods

		private void insert(int index, T item)
		{
			this.OnAdding(index, item);
			this.list.Insert(index, item);
			this.OnAdded(index, item);
		}

		private void removeAt(int index)
		{
			T item = this.list[index];
			this.OnRemoving(index, item);
			this.list.RemoveAt(index);
			this.OnRemoved(index, item);
		}

		private void exchange(int index, T value)
		{
			this.removeAt(index);
			this.insert(index, value);
		}

		/// <summary>
		/// Called when a new item is about to be added.
		/// Can be used to check if the item can be added to the collection and throw exceptions.
		/// </summary>
		/// <param name="index">The index of the item.</param>
		/// <param name="item">The item.</param>
		protected virtual void OnAdding(int index, T item)
		{
			
		}

		/// <summary>
		/// Called when an item has been added to the collection.
		/// </summary>
		/// <param name="index">The index of the item.</param>
		/// <param name="item">The item.</param>
		protected virtual void OnAdded(int index, T item)
		{
			this.Added?.Invoke(this, new ObservableListEventArgs<T>(index, item));
		}

		/// <summary>
		/// Called when an item is about to be removed.
		/// </summary>
		/// <param name="index">The index of the item.</param>
		/// <param name="item">The item.</param>
		protected virtual void OnRemoving(int index, T item)
		{
			
		}

		/// <summary>
		/// Called when an item is removed from the collection.
		/// </summary>
		/// <param name="index">The index of the item.</param>
		/// <param name="item">The item.</param>
		protected virtual void OnRemoved(int index, T item)
		{
			this.Removed?.Invoke(this, new ObservableListEventArgs<T>(index, item));
		}

		/// <summary>
		/// Adds an item to the collection.
		/// </summary>
		/// <param name="item"></param>
		public void Add(T item)
		{
			this.insert(this.list.Count, item);
		}

		int IList.Add(object value)
		{
			int index = this.list.Count;
			this.insert(index, (T)value);
			return index;
		}

		/// <summary>
		/// Adds a range of items to the collection.
		/// </summary>
		/// <param name="items"></param>
		public void AddRange(IEnumerable<T> items)
		{
			foreach (T item in items)
			{
				this.insert(this.list.Count, item);
			}
		}

		/// <summary>
		/// Inserts an item to the collection.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		public void Insert(int index, T item)
		{
			this.insert(index, item);
		}

		void IList.Insert(int index, object value)
		{
			this.Insert(index, (T)value);
		}

		/// <summary>
		/// Inserts a range of items to the collection, starting at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="items"></param>
		public void InsertRange(int index, IEnumerable<T> items)
		{
			foreach (T item in items)
			{
				this.insert(index++, item);
			}
		}

		/// <summary>
		/// Removes an item from the collection.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool Remove(T item)
		{
			int index = this.IndexOf(item);
			if (index < 0)
				return false;

			this.removeAt(index);

			return true;
		}

		void IList.Remove(object value)
		{
			if (value is T)
				this.Remove((T)value);
		}

		/// <summary>
		/// Removes a range of items from the collection.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="count"></param>
		public void RemoveRange(int index, int count)
		{
			if (index < 0 || index + count >= this.list.Count)
				throw new ArgumentOutOfRangeException();

			for (int i = 0; i < count; ++i)
			{
				this.removeAt(index);
			}
		}

		/// <summary>
		/// Removes the item the specified index from the collection.
		/// </summary>
		/// <param name="index"></param>
		public void RemoveAt(int index)
		{
			this.removeAt(index);
		}

		private void removeAll(Predicate<T> match)
		{
			T[] items = this.Where(item => match(item)).ToArray();

			foreach (T item in items)
			{
				this.Remove(item);
			}
		}

		/// <summary>
		/// Removes all items that match the specified predicate.
		/// </summary>
		/// <param name="match"></param>
		public void RemoveAll(Predicate<T> match)
		{
			this.removeAll(match);
		}

		/// <summary>
		/// Clears the collection.
		/// </summary>
		public void Clear()
		{
			for (int i = this.list.Count - 1; i >= 0; i--)
			{
				this.removeAt(i);
			}
		}

		/// <summary>
		/// Gets the index of the specified item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public int IndexOf(T item)
		{
			return this.list.IndexOf(item);
		}

		int IList.IndexOf(object value)
		{
			if (!(value is T))
				return -1;

			return this.IndexOf((T)value);
		}

		/// <summary>
		/// Returns a value that indicates whether the collections contains the specified item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool Contains(T item)
		{
			return this.list.Contains(item);
		}

		bool IList.Contains(object value)
		{
			if (!(value is T))
				return false;

			return this.Contains((T)value);
		}

		/// <summary>
		/// Copies the items of the collection to the specified array.
		/// </summary>
		/// <param name="array"></param>
		/// <param name="arrayIndex"></param>
		public void CopyTo(T[] array, int arrayIndex)
		{
			this.list.CopyTo(array, arrayIndex);
		}

		public void CopyTo(Array array, int index)
		{
			((ICollection)this.list).CopyTo(array, index);
		}
		
		public IEnumerator<T> GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		#endregion
	}
}
