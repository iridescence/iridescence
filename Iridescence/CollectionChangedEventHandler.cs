﻿namespace Iridescence
{
	/// <summary>
	/// Event handler for collection modification events.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void CollectionChangedEventHandler<T>(object sender, CollectionChangedEventArgs<T> e);
}