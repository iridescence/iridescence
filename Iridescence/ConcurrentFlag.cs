﻿using System;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Represents a thread-safe flag that can be set once.
	/// </summary>
	[Serializable]
	public struct ConcurrentFlag
	{
		#region Fields

		private int isSet;

		#endregion

		#region Properties

		/// <summary>
		/// Returns whether the flag is set.
		/// </summary>
		public bool IsSet => Volatile.Read(ref this.isSet) != 0;

		#endregion

		#region Constructors

		#endregion

		#region Methods

		/// <summary>
		/// Tries setting the flag.
		/// </summary>
		/// <returns>True, if the flag was set. False if it was already set.</returns>
		public bool TrySet()
		{
			return Interlocked.CompareExchange(ref this.isSet, 1, 0) == 0;
		}

		public override string ToString()
		{
			return this.IsSet.ToString();
		}

		#endregion
	}
}
