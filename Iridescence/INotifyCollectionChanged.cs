﻿using System.ComponentModel;

namespace Iridescence
{
	/// <summary>
	/// Interface for collections that fire a change event when they were modified.
	/// Works analogous to <see cref="INotifyPropertyChanged"/>.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface INotifyCollectionChanged<T>
	{
		/// <summary>
		/// Occurs when the collection was modified.
		/// </summary>
		event CollectionChangedEventHandler<T> CollectionChanged;
	}

	/// <summary>
	/// Interface for lists that fire a change event when they were modified.
	/// Works the same as <see cref="INotifyCollectionChanged{T}"/> but also includes the index of items in addition to their value.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface INotifyListChanged<T> : INotifyCollectionChanged<ListItem<T>>
	{

	}
}
