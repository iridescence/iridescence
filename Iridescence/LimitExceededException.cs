﻿using System;

namespace Iridescence
{
	/// <summary>
	/// Represents the exception that is thrown when data read from e.g. a stream exceeds a size limit.
	/// </summary>
	[Serializable]
	public class LimitExceededException : Exception
	{
		public LimitExceededException() : base("Input data exceeded the size restriction.") { }
		public LimitExceededException(string message) : base(message) { }
		public LimitExceededException(string message, Exception inner) : base(message, inner) { }
		protected LimitExceededException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}
