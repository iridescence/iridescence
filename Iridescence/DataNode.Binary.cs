﻿using System.Collections.Generic;
using System.IO;

namespace Iridescence
{
	public sealed partial class DataNode
	{
		#region Constants

		private const uint signature = 0x62646530u;

		#endregion

		#region Methods

		#region ReadBinary

		/// <summary>
		/// Reads a data element from the stream that was written with WriteBinary.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static DataNode ReadBinary(Stream stream)
		{
			if (stream.ReadUInt32B() != signature)
				throw new InvalidDataException();

			return readBinaryFragment(stream);
		}

		private static DataNode readBinaryFragment(Stream stream)
		{
			DataNode element = new DataNode(stream.ReadStringV(), stream.ReadStringV());

			int numAttributes = stream.ReadInt32V();
			for (int i = 0; i < numAttributes; i++)
				element.attributes[stream.ReadStringV()] = stream.ReadStringV();
		
			int numElements = stream.ReadInt32V();
			for (int i = 0; i < numElements; i++)
				element.Add(readBinaryFragment(stream));

			return element;
		}

		#endregion

		#region WriteBinary

		/// <summary>
		/// Writes the data element to the stream in a binary format.
		/// </summary>
		/// <param name="stream"></param>
		public void WriteBinary(Stream stream)
		{
			stream.WriteUInt32B(signature);
			this.writeBinaryFragment(stream);
		}

		private void writeBinaryFragment(Stream stream)
		{
			stream.WriteStringV(this.name);
			stream.WriteStringV(this.value);
	
			stream.WriteInt32V(this.attributes.Count);
			foreach (KeyValuePair<string, string> attr in this.attributes)
			{
				stream.WriteStringV(attr.Key);
				stream.WriteStringV(attr.Value);
			}

			stream.WriteInt32V(this.Count);
			foreach (DataNode element in this)
			{
				element.writeBinaryFragment(stream);
			}
		}

		#endregion

		#endregion
	}
}

