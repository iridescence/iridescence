﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Array utility functions.
	/// </summary>
	public static class ArrayUtility
	{
		#region Methods
		
		/// <summary>
		/// Compares all items of the specified arrays and returns whether they are equal.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array1"></param>
		/// <param name="array2"></param>
		/// <returns></returns>
		public static bool ItemsEqual<T>(this T[] array1, T[] array2)
		{
			if (array1.Length != array2.Length)
				return false;

			for (int i = 0; i < array1.Length; ++i)
			{
				if (!object.Equals(array1[i], array2[i]))
					return false;
			}

			return true;
		}
		
		/// <summary>
		/// Returns the length of each array dimension.
		/// </summary>
		/// <param name="array"></param>
		/// <returns></returns>
		public static int[] GetLengths(this Array array)
		{
			int[] lengths = new int[array.Rank];
			
			for (int i = 0; i < lengths.Length; ++i)
			{
				lengths[i] = array.GetLength(i);
			}

			return lengths;
		}

		/// <summary>
		/// Returns the length of each array dimension.
		/// </summary>
		/// <param name="array"></param>
		/// <returns></returns>
		public static long[] GetLongLengths(this Array array)
		{
			long[] lengths = new long[array.Rank];

			for (int i = 0; i < lengths.Length; ++i)
			{
				lengths[i] = array.GetLongLength(i);
			}

			return lengths;
		}
		
		/// <summary>
		/// Calls the specified action for each item in the array.
		/// </summary>
		/// <param name="array"></param>
		/// <param name="action"></param>
		public static void ForEach(this Array array, Action<long[]> action)
		{
			int rank = array.Rank;
			long[] lengths = new long[rank];
			long total = 1;

			// Get lengths and total array size.
			for (int i = 0; i < rank; i++)
			{
				lengths[i] = array.GetLongLength(i);
				total *= lengths[i];
			}

			// Go through all items.
			long[] indices = new long[rank];
			long index = 0;
			
			while (index++ < total)
			{
				action(indices);

				// Increment index.
				for (int i = 0; i < rank; i++)
				{
					if (++indices[i] >= lengths[i])
						indices[i] = 0;
					else
						break;
				}
			}
		}

		/// <summary>
		/// Casts a generic <see cref="Span{T}"/> into a byte <see cref="Span{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="span"></param>
		/// <returns></returns>
		public static Span<byte> AsBytes<T>(this Span<T> span) where T : struct
		{
			return MemoryMarshal.AsBytes(span);
		}

		/// <summary>
		/// Casts a generic <see cref="ReadOnlySpan{T}"/> into a byte <see cref="ReadOnlySpan{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="span"></param>
		/// <returns></returns>
		public static ReadOnlySpan<byte> AsBytes<T>(this ReadOnlySpan<T> span) where T : struct
		{
			return MemoryMarshal.AsBytes(span);
		}

		/// <summary>
		/// Copies from the specified source to a destination span, slicing the destination span afterwards so that it represents the remaining part after the copied part.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="dest"></param>
		public static void CopyTo<T>(this Span<T> source, ref Span<T> dest)
		{
			source.CopyTo(dest);
			dest = dest.Slice(source.Length);
		}

		/// <summary>
		/// Copies from the specified source to a destination span, slicing the destination span afterwards so that it represents the remaining part after the copied part.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="dest"></param>
		public static void CopyTo<T>(this ReadOnlySpan<T> source, ref Span<T> dest)
		{
			source.CopyTo(dest);
			dest = dest.Slice(source.Length);
		}

		/// <summary>
		/// Copies from the specified source to a destination span, slicing the destination span afterwards so that it represents the remaining part after the copied part.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="dest"></param>
		public static void CopyTo<T>(this T[] source, ref Span<T> dest)
		{
			source.CopyTo(dest);
			dest = dest.Slice(source.Length);
		}

		/// <summary>
		/// Returns a slice of an array as <see cref="Memory{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <returns></returns>
		public static Memory<T> ToMemory<T>(this T[] array, int start)
		{
			return new Memory<T>(array).Slice(start);
		}

		/// <summary>
		/// Returns a slice of an array as <see cref="Memory{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static Memory<T> ToMemory<T>(this T[] array, int start, int length)
		{
			return new Memory<T>(array, start, length);
		}

		/// <summary>
		/// Returns a slice of an array as <see cref="Span{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <returns></returns>
		public static Span<T> ToSpan<T>(this T[] array, int start)
		{
			return new Span<T>(array).Slice(start);
		}

		/// <summary>
		/// Returns a slice of an array as <see cref="Span{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static Span<T> ToSpan<T>(this T[] array, int start, int length)
		{
			return new Span<T>(array, start, length);
		}

		/// <summary>
		/// Sorts an array using an <see cref="IInComparer{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TComparer"></typeparam>
		/// <param name="array"></param>
		/// <param name="comparer"></param>
		public static void Sort<T, TComparer>(this T[] array, TComparer comparer)
			where TComparer : IComparer<T>
		{
			array.Sort(0, array.Length, comparer);
		}

		/// <summary>
		/// Sorts an array using an <see cref="IInComparer{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TComparer"></typeparam>
		/// <param name="array"></param>
		/// <param name="comparer"></param>
		public static void SortRef<T, TComparer>(this T[] array, TComparer comparer)
			where TComparer : IRefComparer<T>
		{
			array.SortRef(0, array.Length, comparer);
		}

		/// <summary>
		/// Sorts an array using an <see cref="IInComparer{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TComparer"></typeparam>
		/// <param name="array"></param>
		/// <param name="comparer"></param>
		public static void SortReadOnlyRef<T, TComparer>(this T[] array, TComparer comparer)
			where TComparer : IReadOnlyRefComparer<T>
		{
			array.SortReadOnlyRef(0, array.Length, comparer);
		}

		/// <summary>
		/// Sorts an array using an <see cref="IInComparer{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TComparer"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <param name="comparer"></param>
		public static void Sort<T, TComparer>(this T[] array, int start, int length, TComparer comparer)
			where TComparer : IComparer<T>
		{
			ArraySort<T, TComparer>.Sort(array, start, length, comparer);
		}

		/// <summary>
		/// Sorts an array using an <see cref="IInComparer{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TComparer"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <param name="comparer"></param>
		public static void SortRef<T, TComparer>(this T[] array, int start, int length, TComparer comparer)
			where TComparer : IRefComparer<T>
		{
			ArraySortRef<T, TComparer>.Sort(array, start, length, comparer);
		}

		/// <summary>
		/// Sorts an array using an <see cref="IInComparer{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TComparer"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <param name="comparer"></param>
		public static void SortReadOnlyRef<T, TComparer>(this T[] array, int start, int length, TComparer comparer)
			where TComparer : IReadOnlyRefComparer<T>
		{
			ArraySortReadOnlyRef<T, TComparer>.Sort(array, start, length, comparer);
		}

		/// <summary>
		/// Performs a binary search for the specified item in a sorted array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TComparer"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <param name="value"></param>
		/// <param name="comparer"></param>
		/// <returns></returns>
		public static int BinarySearch<T, TComparer>(this T[] array, int start, int length, T value, TComparer comparer)
			where TComparer : IComparer<T>
		{
			return ArraySort<T, TComparer>.BinarySearch(array, start, length, value, comparer);
		}
		
		/// <summary>
		/// Performs a binary search for the specified item in a sorted array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TComparer"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <param name="value"></param>
		/// <param name="comparer"></param>
		/// <returns></returns>
		public static int BinarySearchRef<T, TComparer>(this T[] array, int start, int length, ref T value, TComparer comparer)
			where TComparer : IRefComparer<T>
		{
			return ArraySortRef<T, TComparer>.BinarySearch(array, start, length, ref value, comparer);
		}

		/// <summary>
		/// Performs a binary search for the specified item in a sorted array.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="TComparer"></typeparam>
		/// <param name="array"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <param name="value"></param>
		/// <param name="comparer"></param>
		/// <returns></returns>
		public static int BinarySearchReadOnlyRef<T, TComparer>(this T[] array, int start, int length, in T value, TComparer comparer)
			where TComparer : IReadOnlyRefComparer<T>
		{
			return ArraySortReadOnlyRef<T, TComparer>.BinarySearch(array, start, length, value, comparer);
		}

		#endregion
	}
}
