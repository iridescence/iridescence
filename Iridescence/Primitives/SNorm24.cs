﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Represents a signed 24-bit integer that is interpreted as float ranging from -1 to 1.
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Size = 3, Pack = 1)]
	[Serializable]
	public struct SNorm24 : IComparable, IFormattable, IConvertible, IComparable<SNorm24>, IEquatable<SNorm24>
	{
		#region Fields

		/// <summary>
		/// Gets or sets the raw int24 value.
		/// </summary>
		public Int24 Value;

		private const int limit = 0x7FFFFF;
		private const float scaleF = limit;
		private const double scaleD = limit;

		/// <summary>
		/// Zero represented as SNorm24.
		/// </summary>
		public static readonly SNorm24 Zero = new SNorm24 {Value = (Int24)0};

		/// <summary>
		/// One represented as SNorm24.
		/// </summary>
		public static readonly SNorm24 One = new SNorm24 {Value = (Int24)limit};

		/// <summary>
		/// Negative one represented as SNorm24.
		/// </summary>
		public static readonly SNorm24 NegativeOne = new SNorm24 {Value = (Int24)(-limit)};

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the value as single-precision floating-point number.
		/// </summary>
		public float SingleValue
		{
			get => this.Value.Value / scaleF;
			set => this.Value.Value = convert(value);
		}

		/// <summary>
		/// Gets or sets the value as double-precision floating-point number.
		/// </summary>
		public double DoubleValue
		{
			get => this.Value.Value / scaleD;
			set => this.Value.Value = convert(value);
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SNorm24 from the specified single-precision floating-point value.
		/// </summary>
		public SNorm24(float value)
		{
			this.Value = new Int24(convert(value));
		}

		/// <summary>
		/// Creates a new SNorm24 from the specified double-precision floating-point value.
		/// </summary>
		public SNorm24(double value)
		{
			this.Value = new Int24(convert(value));
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Returns a new <see cref="SNorm24"/> with the specified underlying raw value (no conversion is performed).
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static SNorm24 FromRawValue(Int24 value)
		{
			SNorm24 v;
			v.Value = value;
			return v;
		}

		public bool Equals(SNorm24 other)
		{
			return this.Value == other.Value;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is SNorm24))
				return false;

			SNorm24 other = (SNorm24)obj;
			return other.Value == this.Value;
		}

		public override int GetHashCode()
		{
			return this.Value;
		}

		public override string ToString()
		{
			return this.DoubleValue.ToString(CultureInfo.InvariantCulture);
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			return this.DoubleValue.ToString(format, formatProvider);
		}

		public int CompareTo(SNorm24 other)
		{
			return this.Value.CompareTo(other.Value);
		}

		public int CompareTo(object obj)
		{
			return this.Value.CompareTo(obj);
		}

		private static int convert(float value)
		{
			int intValue = (int)(value * scaleF + (value > 0.0f ? 0.5f : -0.5f));
			if (intValue <= -limit) return -limit;
			if (intValue >= limit) return limit;
			return intValue;
		}

		private static int convert(double value)
		{
			int intValue = (int)(value * scaleD + (value > 0.0f ? 0.5f : -0.5f));
			if (intValue <= -limit) return -limit;
			if (intValue >= limit) return limit;
			return intValue;
		}

		#endregion

		#region Unary

		public static SNorm24 operator +(SNorm24 a)
		{
			return a;
		}

		public static SNorm24 operator -(SNorm24 a)
		{
			if (a.Value == Int24.MinValue)
				return SNorm24.One;
			return FromRawValue(-a.Value);
		}

		#endregion

		#region Binary

		public static SNorm24 operator +(SNorm24 a, SNorm24 b)
		{
			int result = a.Value + b.Value;
			if (result > limit)
				result = limit;
			else if (result < -limit)
				result = -limit;

			SNorm24 norm;
			norm.Value = (sbyte)result;
			return norm;
		}
		
		public static SNorm24 operator -(SNorm24 a, SNorm24 b)
		{
			int result = a.Value - b.Value;
			if (result > limit)
				result = limit;
			else if (result < -limit)
				result = -limit;

			SNorm24 norm;
			norm.Value = (sbyte)result;
			return norm;
		}

		public static SNorm24 operator *(SNorm24 a, SNorm24 b)
		{
			return new SNorm24(a.SingleValue * b.SingleValue);
		}

		public static SNorm24 operator /(SNorm24 a, SNorm24 b)
		{
			return new SNorm24(a.SingleValue / b.SingleValue);
		}

		#endregion

		#region Comparison

		public static bool operator ==(SNorm24 a, SNorm24 b)
		{
			return a.Value == b.Value;
		}

		public static bool operator !=(SNorm24 a, SNorm24 b)
		{
			return a.Value != b.Value;
		}

		public static bool operator <(SNorm24 a, SNorm24 b)
		{
			return a.Value < b.Value;
		}

		public static bool operator >(SNorm24 a, SNorm24 b)
		{
			return a.Value > b.Value;
		}

		public static bool operator <=(SNorm24 a, SNorm24 b)
		{
			return a.Value <= b.Value;
		}

		public static bool operator >=(SNorm24 a, SNorm24 b)
		{
			return a.Value >= b.Value;
		}

		#endregion

		#region Cast

		public static explicit operator Half(SNorm24 value)
		{
			return (Half)value.SingleValue;
		}

		public static explicit operator SNorm24(Half value)
		{
			SNorm24 norm;
			norm.Value = new Int24(convert((float)value));
			return norm;
		}

		public static implicit operator float(SNorm24 value)
		{
			return value.Value / scaleF;
		}

		public static explicit operator SNorm24(float value)
		{
			SNorm24 norm;
			norm.Value = new Int24(convert(value));
			return norm;
		}

		public static implicit operator double(SNorm24 value)
		{
			return value.Value / scaleD;
		}

		public static explicit operator SNorm24(double value)
		{
			SNorm24 norm;
			norm.Value = new Int24(convert(value));
			return norm;
		}

		#endregion
	
		#region IConvertible

		TypeCode IConvertible.GetTypeCode()
		{
			return TypeCode.Object;
		}

		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this.DoubleValue);
		}

		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this.DoubleValue);
		}

		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this.DoubleValue);
		}

		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this.DoubleValue);
		}

		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this.DoubleValue);
		}

		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this.DoubleValue);
		}

		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this.DoubleValue);
		}

		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this.DoubleValue);
		}

		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this.DoubleValue);
		}

		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this.DoubleValue);
		}

		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return this.SingleValue;
		}

		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return this.DoubleValue;
		}

		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this.DoubleValue);
		}

		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this.DoubleValue);
		}

		string IConvertible.ToString(IFormatProvider provider)
		{
			return Convert.ToString(this.DoubleValue, provider);
		}

		object IConvertible.ToType(Type conversionType, IFormatProvider provider)
		{
			return Convert.ChangeType(this.DoubleValue, conversionType, provider);
		}

		#endregion
	}
}
