﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iridescence
{
	public static class PrimitiveUtility
	{
		public static unsafe Half BitsToHalf(ushort bits)
		{
			return *(Half*)&bits;
		}

		public static unsafe ushort HalfToBits(Half value)
		{
			return *(ushort*)&value;
		}
	}
}
