﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Represents a byte that is interpreted as float ranging from 0 to 1.
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Size = 1, Pack = 1)]
	[Serializable]
	public struct UNorm8 : IComparable, IFormattable, IConvertible, IComparable<UNorm8>, IEquatable<UNorm8>
	{
		#region Fields

		/// <summary>
		/// Gets or sets the raw byte value.
		/// </summary>
		public byte Value;

		/// <summary>
		/// Zero represented as UNorm8.
		/// </summary>
		public static readonly UNorm8 Zero = new UNorm8 {Value = 0};

		/// <summary>
		/// One represented as UNorm8.
		/// </summary>
		public static readonly UNorm8 One = new UNorm8 {Value = 0xFF};

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the value as single-precision floating-point number.
		/// </summary>
		public float SingleValue
		{
			get => this.Value / 255.0f;
			set => this.Value = convert(value);
		}

		/// <summary>
		/// Gets or sets the value as double-precision floating-point number.
		/// </summary>
		public double DoubleValue
		{
			get => this.Value / 255.0d;
			set => this.Value = convert(value);
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new UNorm8 from the specified single-precision floating-point value.
		/// </summary>
		public UNorm8(float value)
		{
			this.Value = convert(value);
		}

		/// <summary>
		/// Creates a new UNorm8 from the specified double-precision floating-point value.
		/// </summary>
		public UNorm8(double value)
		{
			this.Value = convert(value);
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Returns a new <see cref="UNorm8"/> with the specified underlying raw value (no conversion is performed).
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static UNorm8 FromRawValue(byte value)
		{
			UNorm8 v;
			v.Value = value;
			return v;
		}

		public bool Equals(UNorm8 other)
		{
			return this.Value == other.Value;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is UNorm8))
				return false;

			UNorm8 other = (UNorm8)obj;
			return other.Value == this.Value;
		}

		public override int GetHashCode()
		{
			return this.Value;
		}
		
		public override string ToString()
		{
			return this.DoubleValue.ToString(CultureInfo.InvariantCulture);
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			return this.DoubleValue.ToString(format, formatProvider);
		}

		public int CompareTo(UNorm8 other)
		{
			return this.Value.CompareTo(other.Value);
		}

		public int CompareTo(object obj)
		{
			return this.Value.CompareTo(obj);
		}

		private static byte convert(float value)
		{
			int intValue = (int)(value * 255.0f + 0.5f);
			if (intValue <= 0) return 0;
			if (intValue >= 255) return 255;
			return (byte)intValue;
		}

		private static byte convert(double value)
		{
			int intValue = (int)(value * 255.0d + 0.5f);
			if (intValue <= 0) return 0;
			if (intValue >= 255) return 255;
			return (byte)intValue;
		}

		#endregion

		#region Unary

		public static UNorm8 operator +(UNorm8 a)
		{
			return a;
		}

		#endregion

		#region Binary

		public static UNorm8 operator +(UNorm8 a, UNorm8 b)
		{
			int result = a.Value + b.Value;
			if (result > 255)
				result = 255;

			UNorm8 norm;
			norm.Value = (byte)result;
			return norm;
		}

		public static UNorm8 operator -(UNorm8 a, UNorm8 b)
		{
			int result = a.Value - b.Value;
			if (result < 0)
				result = 0;

			UNorm8 norm;
			norm.Value = (byte)result;
			return norm;
		}
		
		public static UNorm8 operator *(UNorm8 a, UNorm8 b)
		{
			return new UNorm8(a.SingleValue * b.SingleValue);
		}

		public static UNorm8 operator /(UNorm8 a, UNorm8 b)
		{
			return new UNorm8(a.SingleValue / b.SingleValue);
		}

		#endregion

		#region Comparison

		public static bool operator ==(UNorm8 a, UNorm8 b)
		{
			return a.Value == b.Value;
		}

		public static bool operator !=(UNorm8 a, UNorm8 b)
		{
			return a.Value != b.Value;
		}

		public static bool operator <(UNorm8 a, UNorm8 b)
		{
			return a.Value < b.Value;
		}

		public static bool operator >(UNorm8 a, UNorm8 b)
		{
			return a.Value > b.Value;
		}
		
		public static bool operator <=(UNorm8 a, UNorm8 b)
		{
			return a.Value <= b.Value;
		}
		
		public static bool operator >=(UNorm8 a, UNorm8 b)
		{
			return a.Value >= b.Value;
		}
		
		#endregion

		#region Cast

		public static implicit operator Half(UNorm8 value)
		{
			return (Half)(value.Value / 255.0f);
		}

		public static explicit operator UNorm8(Half value)
		{
			UNorm8 norm;
			norm.Value = convert((float)value);
			return norm;
		}

		public static implicit operator float(UNorm8 value)
		{
			return value.Value / 255.0f;
		}

		public static explicit operator UNorm8(float value)
		{
			UNorm8 norm;
			norm.Value = convert(value);
			return norm;
		}

		public static implicit operator double(UNorm8 value)
		{
			return value.Value / 255.0d;
		}

		public static explicit operator UNorm8(double value)
		{
			UNorm8 norm;
			norm.Value = convert(value);
			return norm;
		}
		
		#endregion
		
		#region IConvertible

		TypeCode IConvertible.GetTypeCode()
		{
			return TypeCode.Object;
		}

		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this.DoubleValue);
		}

		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this.DoubleValue);
		}

		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this.DoubleValue);
		}

		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this.DoubleValue);
		}

		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this.DoubleValue);
		}

		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this.DoubleValue);
		}

		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this.DoubleValue);
		}

		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this.DoubleValue);
		}

		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this.DoubleValue);
		}

		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this.DoubleValue);
		}

		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return this.SingleValue;
		}

		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return this.DoubleValue;
		}

		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this.DoubleValue);
		}

		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this.DoubleValue);
		}

		string IConvertible.ToString(IFormatProvider provider)
		{
			return Convert.ToString(this.DoubleValue, provider);
		}

		object IConvertible.ToType(Type conversionType, IFormatProvider provider)
		{
			return Convert.ChangeType(this.DoubleValue, conversionType, provider);
		}

		#endregion
	}
}
