﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Represents a 24-bit unsigned integer data-type.
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Size = 3, Pack = 1)]
	[Serializable]
	public struct UInt24 : IComparable, IFormattable, IConvertible, IComparable<UInt24>, IEquatable<UInt24>
	{
		#region Fields

		public byte Byte1;
		public byte Byte2;
		public byte Byte3;

		public static readonly UInt24 MinValue = new UInt24(0);
		public static readonly UInt24 MaxValue = new UInt24(0xFFFFFFu);
	
		#endregion
		
		#region Properties

		public uint Value
		{
			get
			{
				uint v = this.Byte1 | ((uint)this.Byte2 << 8) | ((uint)this.Byte3 << 16);
				return v;
			}
			set
			{
				unsafe
				{
					byte* b = (byte*)&value;
					this.Byte1 = b[0];
					this.Byte2 = b[1];
					this.Byte3 = b[2];
				}
			}
		}

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="UInt24"/> class. 
		/// </summary>
		public UInt24(uint value)
		{
			unsafe
			{
				byte* b = (byte*)&value;
				this.Byte1 = b[0];
				this.Byte2 = b[1];
				this.Byte3 = b[2];
			}
		}

		#endregion
		
		#region Methods

		public bool Equals(UInt24 other)
		{
			return other.Byte1 == this.Byte1 &&
			       other.Byte2 == this.Byte2 &&
			       other.Byte3 == this.Byte3;
		}

		public override bool Equals(object obj)
		{
			if (obj is UInt24 other)
				return this.Value == other.Value;
			return false;
		}

		public override int GetHashCode()
		{
			return unchecked((int)this.Value);
		}

		public override string ToString()
		{
			return this.Value.ToString();
		}

		public string ToString(IFormatProvider provider)
		{
			return this.Value.ToString(provider);
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			return this.Value.ToString(format, formatProvider);
		}

		public int CompareTo(UInt24 other)
		{
			return this.Value.CompareTo(other.Value);
		}

		public int CompareTo(object obj)
		{
			return this.Value.CompareTo(obj);
		}

		#endregion

		#region Unary

		public static UInt24 operator +(UInt24 a)
		{
			return new UInt24(+a.Value);
		}

		public static UInt24 operator ~(UInt24 a)
		{
			UInt24 r;
			r.Byte1 = (byte)~a.Byte1;
			r.Byte2 = (byte)~a.Byte2;
			r.Byte3 = (byte)~a.Byte3;
			return r;
		}

		public static UInt24 operator ++(UInt24 a)
		{
			return new UInt24(a.Value + 1);
		}

		public static UInt24 operator --(UInt24 a)
		{
			return new UInt24(a.Value - 1);
		}

		#endregion

		#region Binary

		public static UInt24 operator +(UInt24 a, UInt24 b)
		{
			return new UInt24(a.Value + b.Value);
		}

		public static UInt24 operator -(UInt24 a, UInt24 b)
		{
			return new UInt24(a.Value - b.Value);
		}

		public static UInt24 operator *(UInt24 a, UInt24 b)
		{
			return new UInt24(a.Value * b.Value);
		}

		public static UInt24 operator /(UInt24 a, UInt24 b)
		{
			return new UInt24(a.Value / b.Value);
		}

		public static UInt24 operator %(UInt24 a, UInt24 b)
		{
			return new UInt24(a.Value % b.Value);
		}
		
		public static UInt24 operator &(UInt24 a, UInt24 b)
		{
			UInt24 r;
			r.Byte1 = (byte)(a.Byte1 & b.Byte1);
			r.Byte2 = (byte)(a.Byte2 & b.Byte2);
			r.Byte3 = (byte)(a.Byte3 & b.Byte3);
			return r;
		}
		
		public static UInt24 operator |(UInt24 a, UInt24 b)
		{
			UInt24 r;
			r.Byte1 = (byte)(a.Byte1 | b.Byte1);
			r.Byte2 = (byte)(a.Byte2 | b.Byte2);
			r.Byte3 = (byte)(a.Byte3 | b.Byte3);
			return r;
		}

		public static UInt24 operator ^(UInt24 a, UInt24 b)
		{
			UInt24 r;
			r.Byte1 = (byte)(a.Byte1 ^ b.Byte1);
			r.Byte2 = (byte)(a.Byte2 ^ b.Byte2);
			r.Byte3 = (byte)(a.Byte3 ^ b.Byte3);
			return r;
		}
		
		public static UInt24 operator <<(UInt24 a, int b)
		{
			return new UInt24(a.Value << b);
		}

		public static UInt24 operator >>(UInt24 a, int b)
		{
			return new UInt24(a.Value >> b);
		}

		#endregion

		#region Comparison

		public static bool operator ==(UInt24 a, UInt24 b)
		{
			return a.Value == b.Value;
		}

		public static bool operator !=(UInt24 a, UInt24 b)
		{
			return a.Value == b.Value;
		}

		public static bool operator <(UInt24 a, UInt24 b)
		{
			return a.Value < b.Value;
		}

		public static bool operator >(UInt24 a, UInt24 b)
		{
			return a.Value > b.Value;
		}
		
		public static bool operator <=(UInt24 a, UInt24 b)
		{
			return a.Value <= b.Value;
		}

		public static bool operator >=(UInt24 a, UInt24 b)
		{
			return a.Value >= b.Value;
		}

		#endregion

		#region Cast

		public static explicit operator sbyte(UInt24 value)
		{
			return (sbyte)value.Value;
		}

		public static explicit operator UInt24(sbyte value)
		{
			return new UInt24((uint)value);
		}

		public static explicit operator byte(UInt24 value)
		{
			return (byte)value.Value;
		}

		public static implicit operator UInt24(byte value)
		{
			return new UInt24(value);
		}
		
		public static explicit operator short(UInt24 value)
		{
			return (short)value.Value;
		}

		public static explicit operator UInt24(short value)
		{
			return new UInt24((uint)value);
		}

		public static explicit operator ushort(UInt24 value)
		{
			return (ushort)value.Value;
		}

		public static implicit operator UInt24(ushort value)
		{
			return new UInt24(value);
		}

		public static explicit operator Int24(UInt24 value)
		{
			return new Int24((int)value.Value);
		}

		public static explicit operator int(UInt24 value)
		{
			return (int)value.Value;
		}

		public static explicit operator UInt24(int value)
		{
			return new UInt24((uint)value);
		}

		public static implicit operator uint(UInt24 value)
		{
			return value.Value;
		}

		public static explicit operator UInt24(uint value)
		{
			return new UInt24(value);
		}

		public static implicit operator long(UInt24 value)
		{
			return value.Value;
		}

		public static explicit operator UInt24(long value)
		{
			return new UInt24((uint)value);
		}

		public static implicit operator ulong(UInt24 value)
		{
			return value.Value;
		}

		public static explicit operator UInt24(ulong value)
		{
			return new UInt24((uint)value);
		}
		
		public static implicit operator float(UInt24 value)
		{
			return value.Value;
		}

		public static explicit operator UInt24(float value)
		{
			return new UInt24((uint)value);
		}

		public static implicit operator double(UInt24 value)
		{
			return value.Value;
		}

		public static explicit operator UInt24(double value)
		{
			return new UInt24((uint)value);
		}

		public static implicit operator decimal(UInt24 value)
		{
			return value.Value;
		}

		public static explicit operator UInt24(decimal value)
		{
			return new UInt24((uint)value);
		}

		public static explicit operator char(UInt24 value)
		{
			return (char)value.Value;
		}

		public static implicit operator UInt24(char value)
		{
			return new UInt24(value);
		}

		#endregion

		#region IConvertible
		
		public TypeCode GetTypeCode()
		{
			return TypeCode.Object;
		}

		public bool ToBoolean(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToBoolean(provider);
		}

		public byte ToByte(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToByte(provider);
		}

		public char ToChar(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToChar(provider);
		}

		public DateTime ToDateTime(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToDateTime(provider);
		}

		public decimal ToDecimal(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToDecimal(provider);
		}

		public double ToDouble(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToDouble(provider);
		}

		public short ToInt16(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToInt16(provider);
		}

		public int ToInt32(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToInt32(provider);
		}

		public long ToInt64(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToInt64(provider);
		}

		public sbyte ToSByte(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToSByte(provider);
		}

		public float ToSingle(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToSingle(provider);
		}
		
		public object ToType(Type conversionType, IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToType(conversionType, provider);
		}

		public ushort ToUInt16(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToUInt16(provider);
		}

		public uint ToUInt32(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToUInt32(provider);
		}

		public ulong ToUInt64(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToUInt64(provider);
		}

		#endregion
	}
}
