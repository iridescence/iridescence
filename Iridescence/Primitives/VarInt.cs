﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Represents a variable-length integer.
	/// </summary>
	[Serializable]
	public struct VarInt : IComparable, IComparable<VarInt>, IEquatable<VarInt>, IConvertible, IFormattable
	{
		#region Fields

		private readonly long value;

		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the value.
		/// </summary>
		public long Value => this.value;

		/// <summary>
		/// Gets the length (in bytes) of the variable-length encoding.
		/// </summary>
		public int Length => GetLength(this.value);
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="VarInt"/> structure. 
		/// </summary>
		public VarInt(long value)
		{
			this.value = value;
		}

		#endregion

		#region Methods

		#region IConvertible

		TypeCode IConvertible.GetTypeCode()
		{
			return this.value.GetTypeCode();
		}

		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToBoolean(provider);
		}

		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToByte(provider);
		}

		char IConvertible.ToChar(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToChar(provider);
		}

		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToDateTime(provider);
		}

		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToDecimal(provider);
		}

		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToDouble(provider);
		}

		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToInt16(provider);
		}

		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToInt32(provider);
		}

		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToInt64(provider);
		}

		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToSByte(provider);
		}

		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToSingle(provider);
		}

		string IConvertible.ToString(IFormatProvider provider)
		{
			return this.value.ToString(provider);
		}

		object IConvertible.ToType(Type conversionType, IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToType(conversionType, provider);
		}

		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToUInt16(provider);
		}

		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToUInt32(provider);
		}

		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToUInt64(provider);
		}

		#endregion

		public override string ToString()
		{
			return this.value.ToString();
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			return this.value.ToString(format, formatProvider);
		}

		public int CompareTo(VarInt other)
		{
			return this.value.CompareTo(other.value);
		}

		public int CompareTo(object obj)
		{
			return this.value.CompareTo(obj);
		}

		public bool Equals(VarInt other)
		{
			return this.value == other.value;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is VarInt && this.Equals((VarInt)obj);
		}

		public override int GetHashCode()
		{
			return this.value.GetHashCode();
		}

		public byte[] GetBytes()
		{
			return Encode(this.value);
		}

		public static bool operator ==(VarInt left, VarInt right)
		{
			return left.value == right.value;
		}

		public static bool operator !=(VarInt left, VarInt right)
		{
			return left.value != right.value;
		}

		public static bool operator <(VarInt left, VarInt right)
		{
			return left.value < right.value;
		}

		public static bool operator >(VarInt left, VarInt right)
		{
			return left.value > right.value;
		}

		public static bool operator <=(VarInt left, VarInt right)
		{
			return left.value <= right.value;
		}

		public static bool operator >=(VarInt left, VarInt right)
		{
			return left.value >= right.value;
		}

		public static implicit operator VarInt(sbyte value)
		{
			return new VarInt(value);
		}

		public static explicit operator sbyte(VarInt value)
		{
			return (sbyte)value.value;
		}

		public static implicit operator VarInt(byte value)
		{
			return new VarInt(value);
		}

		public static explicit operator byte(VarInt value)
		{
			return (byte)value.value;
		}

		public static implicit operator VarInt(short value)
		{
			return new VarInt(value);
		}

		public static explicit operator short(VarInt value)
		{
			return (short)value.value;
		}

		public static implicit operator VarInt(ushort value)
		{
			return new VarInt(value);
		}

		public static explicit operator ushort(VarInt value)
		{
			return (ushort)value.value;
		}

		public static implicit operator VarInt(int value)
		{
			return new VarInt(value);
		}

		public static explicit operator int(VarInt value)
		{
			return (int)value.value;
		}

		public static implicit operator VarInt(uint value)
		{
			return new VarInt(value);
		}

		public static explicit operator uint(VarInt value)
		{
			return (uint)value.value;
		}

		public static implicit operator VarInt(long value)
		{
			return new VarInt(value);
		}

		public static implicit operator long(VarInt value)
		{
			return value.value;
		}

		public static explicit operator VarInt(ulong value)
		{
			return new VarInt((long)value);
		}

		public static explicit operator ulong(VarInt value)
		{
			return (ulong)value.value;
		}

		public static implicit operator VarInt(char value)
		{
			return new VarInt(value);
		}
	
		public static explicit operator char(VarInt value)
		{
			return (char)value.value;
		}
		
		/// <summary>
		/// Returns the length for the variable-length encoding of the specified value.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static int GetLength(long value)
		{
			ulong v = unchecked((ulong)value);
			if (v < 1ul << 7) return 1;
			if (v < 1ul << 14) return 2;
			if (v < 1ul << 21) return 3;
			if (v < 1ul << 28) return 4;
			if (v < 1ul << 35) return 5;
			if (v < 1ul << 42) return 6;
			if (v < 1ul << 49) return 7;
			if (v < 1ul << 56) return 8;
			return 9;
		}

		/// <summary>
		/// Encodes the value and returns the bytes.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static byte[] Encode(long value)
		{
			byte[] buffer = new byte[GetLength(value)];
			int offset = 0;
			Encode(buffer, ref offset, value);
			return buffer;
		}
		
		/// <summary>
		/// Encodes the value into the specified buffer.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		public static void Encode(byte[] buffer, int offset, long value)
		{
			Encode(buffer, ref offset, value);
		}

		/// <summary>
		/// Encodes the value into the specified buffer.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		public static void Encode(byte[] buffer, ref int offset, long value)
		{
			ulong v = unchecked((ulong)value);
			for (int i = 0; i < 8; i++)
			{
				byte current = (byte)(v & 0x7F);
				v >>= 7;

				if (v > 0)
					current |= 0x80;

				buffer[offset++] = current;

				if (v == 0)
					return;
			}

			buffer[offset++] = unchecked((byte)v);
		}
		
		/// <summary>
		/// Encodes the value into the specified buffer.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="buffer"></param>
		public static void Encode(Span<byte> buffer, long value)
		{
			Encode(ref buffer, value);
		}

		/// <summary>
		/// Encodes the value into the specified buffer.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="buffer"></param>
		public static void Encode(ref Span<byte> buffer, long value)
		{
			ulong v = unchecked((ulong)value);
			for (int i = 0; i < 8; i++)
			{
				byte current = (byte)(v & 0x7F);
				v >>= 7;

				if (v > 0)
					current |= 0x80;

				buffer[i] = current;

				if (v == 0)
				{
					buffer = buffer.Slice(i + 1);
					return;
				}
			}

			buffer[8] = unchecked((byte)v);
			buffer = buffer.Slice(9);
		}

		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public static long Decode(byte[] buffer, int offset)
		{
			return Decode(buffer, ref offset);
		}

		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public static long Decode(byte[] buffer, ref int offset)
		{
			ulong value = 0;
			int shift = 0;

			for (;;)
			{
				byte input = buffer[offset++];

				if (shift == 56)
				{
					value |= (ulong)input << 56;
					break;
				}

				value |= (input & 0x7Ful) << shift;

				if ((input & 0x80) == 0)
					break;

				shift += 7;
			}

			return unchecked((long)value);
		}

		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <returns></returns>
		public static long Decode(ReadOnlySpan<byte> buffer)
		{
			return Decode(ref buffer, out _);
		}

		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <returns></returns>
		public static long Decode(ref ReadOnlySpan<byte> buffer)
		{
			return Decode(ref buffer, out _);
		}
		
		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static long Decode(ReadOnlySpan<byte> buffer, out int length)
		{
			return Decode(ref buffer, out length);
		}
	
		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static long Decode(ref ReadOnlySpan<byte> buffer, out int length)
		{
			ulong value = 0;
			int shift = 0;
			length = 0;

			for (;;)
			{
				byte input = buffer[length++];

				if (shift == 56)
				{
					value |= (ulong)input << 56;
					break;
				}

				value |= (input & 0x7Ful) << shift;

				if ((input & 0x80) == 0)
					break;

				shift += 7;
			}

			buffer = buffer.Slice(length);
			return unchecked((long)value);
		}

		/// <summary>
		/// Writes a value to a stream using variable-length encoding.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void Write(Stream stream, long value)
		{
			ulong v = unchecked((ulong)value);
			for(int i = 0; i < 8; i++)
			{
				byte current = (byte)(v & 0x7F);
				v >>= 7;

				if (v > 0)
					current |= 0x80;

				stream.WriteByte(current);

				if (v == 0)
					return;
			}

			stream.WriteByte(unchecked((byte)v));
		}

		/// <summary>
		/// Reads a value from a stream using variable-length encoding.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static long Read(Stream stream)
		{
			ulong value = 0;
			int shift = 0;

			for (;;)
			{
				byte input = stream.ReadByteOrThrow();

				if (shift == 56)
				{
					value |= (ulong)input << 56;
					break;
				}

				value |= (input & 0x7Ful) << shift;

				if ((input & 0x80) == 0)
					break;

				shift += 7;
			}

			return unchecked((long)value);
		}
		
		#endregion
	}
}
