﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Represents a 24-bit signed integer data-type.
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Size = 3, Pack = 1)]
	[Serializable]
	public struct Int24 : IComparable, IFormattable, IConvertible, IComparable<Int24>, IEquatable<Int24>
	{
		#region Fields

		public byte Byte1;
		public byte Byte2;
		public byte Byte3;
		
		public static readonly Int24 MinValue = new Int24(0x800000);
		public static readonly Int24 MaxValue = new Int24(0x7FFFFF);

		#endregion
		
		#region Properties

		public int Value
		{
			get
			{
				int v = this.Byte1 | ((int)this.Byte2 << 8) | ((int)this.Byte3 << 16);
				return (v << 8) >> 8;
			}
			set
			{
				unsafe
				{
					byte* b = (byte*)&value;
					this.Byte1 = b[0];
					this.Byte2 = b[1];
					this.Byte3 = b[2];
				}
			}
		}

		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Int24"/> class. 
		/// </summary>
		public Int24(int value)
		{
			unsafe
			{
				byte* b = (byte*)&value;
				this.Byte1 = b[0];
				this.Byte2 = b[1];
				this.Byte3 = b[2];
			}
		}

		#endregion
		
		#region Methods

		public bool Equals(Int24 other)
		{
			return other.Byte1 == this.Byte1 &&
			       other.Byte2 == this.Byte2 &&
			       other.Byte3 == this.Byte3;
		}

		public override bool Equals(object obj)
		{
			if (obj is Int24 other)
				return this.Value == other.Value;
			return false;
		}

		public override int GetHashCode()
		{
			return this.Value;
		}

		public override string ToString()
		{
			return this.Value.ToString();
		}

		public string ToString(IFormatProvider provider)
		{
			return this.Value.ToString(provider);
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			return this.Value.ToString(format, formatProvider);
		}

		public int CompareTo(Int24 other)
		{
			return this.Value.CompareTo(other.Value);
		}

		public int CompareTo(object obj)
		{
			return this.Value.CompareTo(obj);
		}

		#endregion

		#region Unary

		public static Int24 operator +(Int24 a)
		{
			return new Int24(+a.Value);
		}

		public static Int24 operator -(Int24 a)
		{
			return new Int24(-a.Value);
		}

		public static Int24 operator ~(Int24 a)
		{
			Int24 r;
			r.Byte1 = (byte)~a.Byte1;
			r.Byte2 = (byte)~a.Byte2;
			r.Byte3 = (byte)~a.Byte3;
			return r;
		}

		public static Int24 operator ++(Int24 a)
		{
			return new Int24(a.Value + 1);
		}

		public static Int24 operator --(Int24 a)
		{
			return new Int24(a.Value - 1);
		}

		#endregion

		#region Binary

		public static Int24 operator +(Int24 a, Int24 b)
		{
			return new Int24(a.Value + b.Value);
		}

		public static Int24 operator -(Int24 a, Int24 b)
		{
			return new Int24(a.Value - b.Value);
		}

		public static Int24 operator *(Int24 a, Int24 b)
		{
			return new Int24(a.Value * b.Value);
		}

		public static Int24 operator /(Int24 a, Int24 b)
		{
			return new Int24(a.Value / b.Value);
		}

		public static Int24 operator %(Int24 a, Int24 b)
		{
			return new Int24(a.Value % b.Value);
		}
		
		public static Int24 operator &(Int24 a, Int24 b)
		{
			Int24 r;
			r.Byte1 = (byte)(a.Byte1 & b.Byte1);
			r.Byte2 = (byte)(a.Byte2 & b.Byte2);
			r.Byte3 = (byte)(a.Byte3 & b.Byte3);
			return r;
		}
		
		public static Int24 operator |(Int24 a, Int24 b)
		{
			Int24 r;
			r.Byte1 = (byte)(a.Byte1 | b.Byte1);
			r.Byte2 = (byte)(a.Byte2 | b.Byte2);
			r.Byte3 = (byte)(a.Byte3 | b.Byte3);
			return r;
		}

		public static Int24 operator ^(Int24 a, Int24 b)
		{
			Int24 r;
			r.Byte1 = (byte)(a.Byte1 ^ b.Byte1);
			r.Byte2 = (byte)(a.Byte2 ^ b.Byte2);
			r.Byte3 = (byte)(a.Byte3 ^ b.Byte3);
			return r;
		}
		
		public static Int24 operator <<(Int24 a, int b)
		{
			return new Int24(a.Value << b);
		}

		public static Int24 operator >>(Int24 a, int b)
		{
			return new Int24(a.Value >> b);
		}

		#endregion

		#region Comparison

		public static bool operator ==(Int24 a, Int24 b)
		{
			return a.Value == b.Value;
		}

		public static bool operator !=(Int24 a, Int24 b)
		{
			return a.Value == b.Value;
		}

		public static bool operator <(Int24 a, Int24 b)
		{
			return a.Value < b.Value;
		}

		public static bool operator >(Int24 a, Int24 b)
		{
			return a.Value > b.Value;
		}
		
		public static bool operator <=(Int24 a, Int24 b)
		{
			return a.Value <= b.Value;
		}

		public static bool operator >=(Int24 a, Int24 b)
		{
			return a.Value >= b.Value;
		}

		#endregion

		#region Cast

		public static explicit operator sbyte(Int24 value)
		{
			return (sbyte)value.Value;
		}

		public static implicit operator Int24(sbyte value)
		{
			return new Int24(value);
		}

		public static explicit operator byte(Int24 value)
		{
			return (byte)value.Value;
		}

		public static implicit operator Int24(byte value)
		{
			return new Int24(value);
		}
		
		public static explicit operator short(Int24 value)
		{
			return (short)value.Value;
		}

		public static implicit operator Int24(short value)
		{
			return new Int24(value);
		}

		public static explicit operator ushort(Int24 value)
		{
			return (ushort)value.Value;
		}

		public static implicit operator Int24(ushort value)
		{
			return new Int24(value);
		}

		public static explicit operator UInt24(Int24 value)
		{
			return new UInt24((uint)value.Value);
		}
		
		public static implicit operator int(Int24 value)
		{
			return value.Value;
		}

		public static explicit operator Int24(int value)
		{
			return new Int24(value);
		}

		public static explicit operator uint(Int24 value)
		{
			return (uint)value.Value;
		}

		public static explicit operator Int24(uint value)
		{
			return new Int24((int)value);
		}

		public static implicit operator long(Int24 value)
		{
			return value.Value;
		}

		public static explicit operator Int24(long value)
		{
			return new Int24((int)value);
		}

		public static explicit operator ulong(Int24 value)
		{
			return (ulong)value.Value;
		}

		public static explicit operator Int24(ulong value)
		{
			return new Int24((int)value);
		}
		
		public static implicit operator float(Int24 value)
		{
			return value.Value;
		}

		public static explicit operator Int24(float value)
		{
			return new Int24((int)value);
		}

		public static implicit operator double(Int24 value)
		{
			return value.Value;
		}

		public static explicit operator Int24(double value)
		{
			return new Int24((int)value);
		}

		public static implicit operator decimal(Int24 value)
		{
			return value.Value;
		}

		public static explicit operator Int24(decimal value)
		{
			return new Int24((int)value);
		}

		public static explicit operator char(Int24 value)
		{
			return (char)value.Value;
		}

		public static implicit operator Int24(char value)
		{
			return new Int24(value);
		}

		#endregion

		#region IConvertible
		
		public TypeCode GetTypeCode()
		{
			return TypeCode.Object;
		}

		public bool ToBoolean(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToBoolean(provider);
		}

		public byte ToByte(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToByte(provider);
		}

		public char ToChar(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToChar(provider);
		}

		public DateTime ToDateTime(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToDateTime(provider);
		}

		public decimal ToDecimal(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToDecimal(provider);
		}

		public double ToDouble(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToDouble(provider);
		}

		public short ToInt16(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToInt16(provider);
		}

		public int ToInt32(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToInt32(provider);
		}

		public long ToInt64(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToInt64(provider);
		}

		public sbyte ToSByte(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToSByte(provider);
		}

		public float ToSingle(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToSingle(provider);
		}
		
		public object ToType(Type conversionType, IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToType(conversionType, provider);
		}

		public ushort ToUInt16(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToUInt16(provider);
		}

		public uint ToUInt32(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToUInt32(provider);
		}

		public ulong ToUInt64(IFormatProvider provider)
		{
			return ((IConvertible)this.Value).ToUInt64(provider);
		}

		#endregion
	}
}
