﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Represents a signed 16-bit integer that is interpreted as float ranging from -1 to 1.
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Size = 2, Pack = 2)]
	[Serializable]
	public struct SNorm16 : IComparable, IFormattable, IConvertible, IComparable<SNorm16>, IEquatable<SNorm16>
	{
		#region Fields

		/// <summary>
		/// Gets or sets the raw int16 value.
		/// </summary>
		public short Value;

		private const int limit = 0x7FFF;
		private const float scaleF = limit;
		private const double scaleD = limit;

		/// <summary>
		/// Zero represented as SNorm16.
		/// </summary>
		public static readonly SNorm16 Zero = new SNorm16 {Value = 0};

		/// <summary>
		/// One represented as SNorm16.
		/// </summary>
		public static readonly SNorm16 One = new SNorm16 {Value = limit};

		/// <summary>
		/// Negative one represented as SNorm16.
		/// </summary>
		public static readonly SNorm16 NegativeOne = new SNorm16 {Value = -limit};

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the value as single-precision floating-point number.
		/// </summary>
		public float SingleValue
		{
			get => this.Value / scaleF;
			set => this.Value = convert(value);
		}

		/// <summary>
		/// Gets or sets the value as double-precision floating-point number.
		/// </summary>
		public double DoubleValue
		{
			get => this.Value / scaleD;
			set => this.Value = convert(value);
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SNorm16 from the specified single-precision floating-point value.
		/// </summary>
		public SNorm16(float value)
		{
			this.Value = convert(value);
		}

		/// <summary>
		/// Creates a new SNorm16 from the specified double-precision floating-point value.
		/// </summary>
		public SNorm16(double value)
		{
			this.Value = convert(value);
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Returns a new <see cref="SNorm16"/> with the specified underlying raw value (no conversion is performed).
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static SNorm16 FromRawValue(short value)
		{
			SNorm16 v;
			v.Value = value;
			return v;
		}

		public bool Equals(SNorm16 other)
		{
			return this.Value == other.Value;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is SNorm16))
				return false;

			SNorm16 other = (SNorm16)obj;
			return other.Value == this.Value;
		}

		public override int GetHashCode()
		{
			return this.Value;
		}

		public override string ToString()
		{
			return this.DoubleValue.ToString(CultureInfo.InvariantCulture);
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			return this.DoubleValue.ToString(format, formatProvider);
		}

		public int CompareTo(SNorm16 other)
		{
			return this.Value.CompareTo(other.Value);
		}

		public int CompareTo(object obj)
		{
			return this.Value.CompareTo(obj);
		}

		private static short convert(float value)
		{
			int intValue = (int)(value * scaleF + (value > 0.0f ? 0.5f : -0.5f));
			if (intValue <= -limit) return -limit;
			if (intValue >= limit) return limit;
			return (short)intValue;
		}

		private static short convert(double value)
		{
			int intValue = (int)(value * scaleD + (value > 0.0f ? 0.5f : -0.5f));
			if (intValue <= -limit) return -limit;
			if (intValue >= limit) return limit;
			return (short)intValue;
		}

		#endregion

		#region Unary

		public static SNorm16 operator +(SNorm16 a)
		{
			return a;
		}

		public static SNorm16 operator -(SNorm16 a)
		{
			if (a.Value == short.MinValue)
				return SNorm16.One;
			return new SNorm16(-a.Value);
		}

		#endregion

		#region Binary

		public static SNorm16 operator +(SNorm16 a, SNorm16 b)
		{
			int result = a.Value + b.Value;
			if (result > limit)
				result = limit;
			else if (result < -limit)
				result = -limit;

			SNorm16 norm;
			norm.Value = (short)result;
			return norm;
		}
		
		public static SNorm16 operator -(SNorm16 a, SNorm16 b)
		{
			int result = a.Value - b.Value;
			if (result > limit)
				result = limit;
			else if (result < -limit)
				result = -limit;

			SNorm16 norm;
			norm.Value = (short)result;
			return norm;
		}

		public static SNorm16 operator *(SNorm16 a, SNorm16 b)
		{
			return new SNorm16(a.SingleValue * b.SingleValue);
		}

		public static SNorm16 operator /(SNorm16 a, SNorm16 b)
		{
			return new SNorm16(a.SingleValue / b.SingleValue);
		}

		#endregion

		#region Comparison

		public static bool operator ==(SNorm16 a, SNorm16 b)
		{
			return a.Value == b.Value;
		}

		public static bool operator !=(SNorm16 a, SNorm16 b)
		{
			return a.Value != b.Value;
		}

		public static bool operator <(SNorm16 a, SNorm16 b)
		{
			return a.Value < b.Value;
		}

		public static bool operator >(SNorm16 a, SNorm16 b)
		{
			return a.Value > b.Value;
		}

		public static bool operator <=(SNorm16 a, SNorm16 b)
		{
			return a.Value <= b.Value;
		}

		public static bool operator >=(SNorm16 a, SNorm16 b)
		{
			return a.Value >= b.Value;
		}

		#endregion

		#region Cast

		public static implicit operator Half(SNorm16 value)
		{
			return (Half)value.SingleValue;
		}

		public static explicit operator SNorm16(Half value)
		{
			SNorm16 norm;
			norm.Value = convert((float)value);
			return norm;
		}

		public static implicit operator float(SNorm16 value)
		{
			return value.Value / scaleF;
		}

		public static explicit operator SNorm16(float value)
		{
			SNorm16 norm;
			norm.Value = convert(value);
			return norm;
		}

		public static implicit operator double(SNorm16 value)
		{
			return value.Value / scaleD;
		}

		public static explicit operator SNorm16(double value)
		{
			SNorm16 norm;
			norm.Value = convert(value);
			return norm;
		}

		#endregion
	
		#region IConvertible

		TypeCode IConvertible.GetTypeCode()
		{
			return TypeCode.Object;
		}

		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this.DoubleValue);
		}

		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this.DoubleValue);
		}

		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this.DoubleValue);
		}

		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this.DoubleValue);
		}

		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this.DoubleValue);
		}

		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this.DoubleValue);
		}

		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this.DoubleValue);
		}

		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this.DoubleValue);
		}

		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this.DoubleValue);
		}

		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this.DoubleValue);
		}

		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return this.SingleValue;
		}

		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return this.DoubleValue;
		}

		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this.DoubleValue);
		}

		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this.DoubleValue);
		}

		string IConvertible.ToString(IFormatProvider provider)
		{
			return Convert.ToString(this.DoubleValue, provider);
		}

		object IConvertible.ToType(Type conversionType, IFormatProvider provider)
		{
			return Convert.ChangeType(this.DoubleValue, conversionType, provider);
		}

		#endregion
	}
}
