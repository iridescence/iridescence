﻿using System;
using System.IO;

namespace Iridescence
{
	/// <summary>
	/// Represents a variable-length unsigned integer.
	/// </summary>
	[Serializable]
	public struct VarUInt : IComparable, IComparable<VarUInt>, IEquatable<VarUInt>, IConvertible, IFormattable
	{
		#region Fields

		private readonly ulong value;

		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the value.
		/// </summary>
		public ulong Value => this.value;

		/// <summary>
		/// Gets the length (in bytes) of the variable-length encoding.
		/// </summary>
		public int Length => GetLength(this.value);
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="VarUInt"/> structure. 
		/// </summary>
		public VarUInt(ulong value)
		{
			this.value = value;
		}

		#endregion

		#region Methods

		#region IConvertible

		TypeCode IConvertible.GetTypeCode()
		{
			return this.value.GetTypeCode();
		}

		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToBoolean(provider);
		}

		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToByte(provider);
		}

		char IConvertible.ToChar(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToChar(provider);
		}

		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToDateTime(provider);
		}

		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToDecimal(provider);
		}

		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToDouble(provider);
		}

		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToInt16(provider);
		}

		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToInt32(provider);
		}

		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToInt64(provider);
		}

		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToSByte(provider);
		}

		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToSingle(provider);
		}

		string IConvertible.ToString(IFormatProvider provider)
		{
			return this.value.ToString(provider);
		}

		object IConvertible.ToType(Type conversionType, IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToType(conversionType, provider);
		}

		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToUInt16(provider);
		}

		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToUInt32(provider);
		}

		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return ((IConvertible)this.value).ToUInt64(provider);
		}

		#endregion

		public override string ToString()
		{
			return this.value.ToString();
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			return this.value.ToString(format, formatProvider);
		}

		public int CompareTo(VarUInt other)
		{
			return this.value.CompareTo(other.value);
		}

		public int CompareTo(object obj)
		{
			return this.value.CompareTo(obj);
		}

		public bool Equals(VarUInt other)
		{
			return this.value == other.value;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is VarUInt && this.Equals((VarUInt)obj);
		}

		public override int GetHashCode()
		{
			return this.value.GetHashCode();
		}

		public byte[] GetBytes()
		{
			return Encode(this.value);
		}

		public static bool operator ==(VarUInt left, VarUInt right)
		{
			return left.value == right.value;
		}

		public static bool operator !=(VarUInt left, VarUInt right)
		{
			return left.value != right.value;
		}

		public static bool operator <(VarUInt left, VarUInt right)
		{
			return left.value < right.value;
		}

		public static bool operator >(VarUInt left, VarUInt right)
		{
			return left.value > right.value;
		}

		public static bool operator <=(VarUInt left, VarUInt right)
		{
			return left.value <= right.value;
		}

		public static bool operator >=(VarUInt left, VarUInt right)
		{
			return left.value >= right.value;
		}

		public static explicit operator VarUInt(sbyte value)
		{
			return new VarUInt((ulong)value);
		}

		public static explicit operator sbyte(VarUInt value)
		{
			return (sbyte)value.value;
		}

		public static implicit operator VarUInt(byte value)
		{
			return new VarUInt(value);
		}

		public static explicit operator byte(VarUInt value)
		{
			return (byte)value.value;
		}

		public static explicit operator VarUInt(short value)
		{
			return new VarUInt((ulong)value);
		}

		public static explicit operator short(VarUInt value)
		{
			return (short)value.value;
		}

		public static implicit operator VarUInt(ushort value)
		{
			return new VarUInt(value);
		}

		public static explicit operator ushort(VarUInt value)
		{
			return (ushort)value.value;
		}

		public static explicit operator VarUInt(int value)
		{
			return new VarUInt((ulong)value);
		}

		public static explicit operator int(VarUInt value)
		{
			return (int)value.value;
		}

		public static implicit operator VarUInt(uint value)
		{
			return new VarUInt(value);
		}

		public static explicit operator uint(VarUInt value)
		{
			return (uint)value.value;
		}

		public static explicit operator VarUInt(long value)
		{
			return new VarUInt((ulong)value);
		}

		public static explicit operator long(VarUInt value)
		{
			return (long)value.value;
		}

		public static implicit operator VarUInt(ulong value)
		{
			return new VarUInt(value);
		}

		public static implicit operator ulong(VarUInt value)
		{
			return value.value;
		}

		public static implicit operator VarUInt(char value)
		{
			return new VarUInt(value);
		}
	
		public static explicit operator char(VarUInt value)
		{
			return (char)value.value;
		}
		
		/// <summary>
		/// Returns the length for the variable-length encoding of the specified value.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static int GetLength(ulong value)
		{
			if (value < 1ul << 7) return 1;
			if (value < 1ul << 14) return 2;
			if (value < 1ul << 21) return 3;
			if (value < 1ul << 28) return 4;
			if (value < 1ul << 35) return 5;
			if (value < 1ul << 42) return 6;
			if (value < 1ul << 49) return 7;
			if (value < 1ul << 56) return 8;
			return 9;
		}

		/// <summary>
		/// Encodes the value and returns the bytes.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static byte[] Encode(ulong value)
		{
			byte[] buffer = new byte[GetLength(value)];
			int offset = 0;
			Encode(buffer, ref offset, value);
			return buffer;
		}
		
		/// <summary>
		/// Encodes the value into the specified buffer.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		public static void Encode(byte[] buffer, int offset, ulong value)
		{
			Encode(buffer, ref offset, value);
		}

		/// <summary>
		/// Encodes the value into the specified buffer.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		public static void Encode(byte[] buffer, ref int offset, ulong value)
		{
			for (int i = 0; i < 8; i++)
			{
				byte current = (byte)(value & 0x7F);
				value >>= 7;

				if (value > 0)
					current |= 0x80;

				buffer[offset++] = current;

				if (value == 0)
					return;
			}

			buffer[offset++] = unchecked((byte)value);
		}
		
		/// <summary>
		/// Encodes the value into the specified buffer.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="buffer"></param>
		public static void Encode(Span<byte> buffer, ulong value)
		{
			Encode(ref buffer, value);
		}

		/// <summary>
		/// Encodes the value into the specified buffer.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="buffer"></param>
		public static void Encode(ref Span<byte> buffer, ulong value)
		{
			for (int i = 0; i < 8; i++)
			{
				byte current = (byte)(value & 0x7F);
				value >>= 7;

				if (value > 0)
					current |= 0x80;

				buffer[i] = current;

				if (value == 0)
				{
					buffer = buffer.Slice(i + 1);
					return;
				}
			}

			buffer[8] = unchecked((byte)value);
			buffer = buffer.Slice(9);
		}

		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public static ulong Decode(byte[] buffer, int offset)
		{
			return Decode(buffer, ref offset);
		}

		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		public static ulong Decode(byte[] buffer, ref int offset)
		{
			ulong value = 0;
			int shift = 0;

			for (;;)
			{
				byte input = buffer[offset++];

				if (shift == 56)
				{
					value |= (ulong)input << 56;
					break;
				}

				value |= (input & 0x7Ful) << shift;

				if ((input & 0x80) == 0)
					break;

				shift += 7;
			}

			return value;
		}

		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <returns></returns>
		public static ulong Decode(ReadOnlySpan<byte> buffer)
		{
			return Decode(ref buffer, out _);
		}

		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <returns></returns>
		public static ulong Decode(ref ReadOnlySpan<byte> buffer)
		{
			return Decode(ref buffer, out _);
		}
		
		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static ulong Decode(ReadOnlySpan<byte> buffer, out int length)
		{
			return Decode(ref buffer, out length);
		}
	
		/// <summary>
		/// Decodes a value from a buffer.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static ulong Decode(ref ReadOnlySpan<byte> buffer, out int length)
		{
			ulong value = 0;
			int shift = 0;
			length = 0;

			for (;;)
			{
				byte input = buffer[length++];

				if (shift == 56)
				{
					value |= (ulong)input << 56;
					break;
				}

				value |= (input & 0x7Ful) << shift;

				if ((input & 0x80) == 0)
					break;

				shift += 7;
			}

			buffer = buffer.Slice(length);
			return value;
		}

		/// <summary>
		/// Writes a value to a stream using variable-length encoding.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="value"></param>
		public static void Write(Stream stream, ulong value)
		{
			for(int i = 0; i < 8; i++)
			{
				byte current = (byte)(value & 0x7F);
				value >>= 7;

				if (value > 0)
					current |= 0x80;

				stream.WriteByte(current);

				if (value == 0)
					return;
			}

			stream.WriteByte(unchecked((byte)value));
		}

		/// <summary>
		/// Reads a value from a stream using variable-length encoding.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static ulong Read(Stream stream)
		{
			ulong value = 0;
			int shift = 0;

			for (;;)
			{
				byte input = stream.ReadByteOrThrow();

				if (shift == 56)
				{
					value |= (ulong)input << 56;
					break;
				}

				value |= (input & 0x7Ful) << shift;

				if ((input & 0x80) == 0)
					break;

				shift += 7;
			}

			return value;
		}
		
		#endregion
	}
}
