﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// Represents a 16 bit unsigned integer that is interpreted as float ranging from 0 to 1.
	/// </summary>
	[StructLayout(LayoutKind.Sequential, Size = 2, Pack = 2)]
	[Serializable]
	public struct UNorm16 : IComparable, IFormattable, IConvertible, IComparable<UNorm16>, IEquatable<UNorm16>
	{
		#region Fields

		/// <summary>
		/// Gets or sets the raw uint16 value.
		/// </summary>
		public ushort Value;

		/// <summary>
		/// Zero represented as UNorm16.
		/// </summary>
		public static readonly UNorm16 Zero = new UNorm16 {Value = 0};

		/// <summary>
		/// One represented as UNorm16.
		/// </summary>
		public static readonly UNorm16 One = new UNorm16 {Value = 0xFFFF};

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the value as single-precision floating-point number.
		/// </summary>
		public float SingleValue
		{
			get => this.Value / (float)0xFFFF;
			set => this.Value = convert(value);
		}

		/// <summary>
		/// Gets or sets the value as double-precision floating-point number.
		/// </summary>
		public double DoubleValue
		{
			get => this.Value / (double)0xFFFF;
			set => this.Value = convert(value);
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new UNorm16 from the specified single-precision floating-point value.
		/// </summary>
		public UNorm16(float value)
		{
			this.Value = convert(value);
		}

		/// <summary>
		/// Creates a new UNorm16 from the specified double-precision floating-point value.
		/// </summary>
		public UNorm16(double value)
		{
			this.Value = convert(value);
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Returns a new <see cref="UNorm16"/> with the specified underlying raw value (no conversion is performed).
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static UNorm16 FromRawValue(ushort value)
		{
			UNorm16 v;
			v.Value = value;
			return v;
		}

		public bool Equals(UNorm16 other)
		{
			return this.Value == other.Value;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is UNorm16))
				return false;

			UNorm16 other = (UNorm16)obj;
			return other.Value == this.Value;
		}

		public override int GetHashCode()
		{
			return this.Value;
		}

		public override string ToString()
		{
			return this.DoubleValue.ToString(CultureInfo.InvariantCulture);
		}

		public string ToString(string format, IFormatProvider formatProvider)
		{
			return this.DoubleValue.ToString(format, formatProvider);
		}

		public int CompareTo(UNorm16 other)
		{
			return this.Value.CompareTo(other.Value);
		}

		public int CompareTo(object obj)
		{
			return this.Value.CompareTo(obj);
		}

		private static ushort convert(float value)
		{
			int intValue = (int)(value * 65535.0f + 0.5f);
			if (intValue <= 0) return 0;
			if (intValue >= 65535) return 65535;
			return (ushort)intValue;
		}

		private static ushort convert(double value)
		{
			int intValue = (int)(value * 65535.0d + 0.5f);
			if (intValue <= 0) return 0;
			if (intValue >= 65535) return 65535;
			return (ushort)intValue;
		}

		#endregion

		#region Unary

		public static UNorm16 operator +(UNorm16 a)
		{
			return a;
		}

		#endregion

		#region Binary

		public static UNorm16 operator +(UNorm16 a, UNorm16 b)
		{
			int result = a.Value + b.Value;
			if (result > 65535)
				result = 65535;

			UNorm16 norm;
			norm.Value = (ushort)result;
			return norm;
		}
		
		public static UNorm16 operator -(UNorm16 a, UNorm16 b)
		{
			int result = a.Value - b.Value;
			if (result < 0)
				result = 0;

			UNorm16 norm;
			norm.Value = (ushort)result;
			return norm;
		}

		public static UNorm16 operator *(UNorm16 a, UNorm16 b)
		{
			return new UNorm16(a.SingleValue * b.SingleValue);
		}

		public static UNorm16 operator /(UNorm16 a, UNorm16 b)
		{
			return new UNorm16(a.SingleValue / b.SingleValue);
		}

		#endregion

		#region Comparison

		public static bool operator ==(UNorm16 a, UNorm16 b)
		{
			return a.Value == b.Value;
		}

		public static bool operator !=(UNorm16 a, UNorm16 b)
		{
			return a.Value != b.Value;
		}

		public static bool operator <(UNorm16 a, UNorm16 b)
		{
			return a.Value < b.Value;
		}

		public static bool operator >(UNorm16 a, UNorm16 b)
		{
			return a.Value > b.Value;
		}

		public static bool operator <=(UNorm16 a, UNorm16 b)
		{
			return a.Value <= b.Value;
		}

		public static bool operator >=(UNorm16 a, UNorm16 b)
		{
			return a.Value >= b.Value;
		}
		
		#endregion

		#region Cast

		public static implicit operator Half(UNorm16 value)
		{
			return (Half)value.SingleValue;
		}

		public static explicit operator UNorm16(Half value)
		{
			UNorm16 norm;
			norm.Value = convert((float)value);
			return norm;
		}

		public static implicit operator float(UNorm16 value)
		{
			return value.Value / 65535.0f;
		}

		public static explicit operator UNorm16(float value)
		{
			UNorm16 norm;
			norm.Value = convert(value);
			return norm;
		}

		public static implicit operator double(UNorm16 value)
		{
			return value.Value / 65535.0d;
		}

		public static explicit operator UNorm16(double value)
		{
			UNorm16 norm;
			norm.Value = convert(value);
			return norm;
		}
		
		#endregion
		
		#region IConvertible

		TypeCode IConvertible.GetTypeCode()
		{
			return TypeCode.Object;
		}

		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this.DoubleValue);
		}

		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this.DoubleValue);
		}

		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this.DoubleValue);
		}

		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this.DoubleValue);
		}

		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this.DoubleValue);
		}

		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this.DoubleValue);
		}

		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this.DoubleValue);
		}

		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this.DoubleValue);
		}

		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this.DoubleValue);
		}

		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this.DoubleValue);
		}

		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return this.SingleValue;
		}

		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return this.DoubleValue;
		}

		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this.DoubleValue);
		}

		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this.DoubleValue);
		}

		string IConvertible.ToString(IFormatProvider provider)
		{
			return Convert.ToString(this.DoubleValue, provider);
		}

		object IConvertible.ToType(Type conversionType, IFormatProvider provider)
		{
			return Convert.ChangeType(this.DoubleValue, conversionType, provider);
		}

		#endregion
	}
}
