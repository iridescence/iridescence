﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Iridescence
{
	/// <summary>
	/// Reflection utility functions.
	/// </summary>
	public static class ReflectionUtility
	{
		#region Fields

		private static readonly Func<Type, bool> isSzArrayFunc;

		private static readonly Type[] actionTypes =
		{
			typeof(Action<>),
			typeof(Action<,>),
			typeof(Action<,,>),
			typeof(Action<,,,>),
			typeof(Action<,,,,>),
			typeof(Action<,,,,,>),
			typeof(Action<,,,,,,>),
			typeof(Action<,,,,,,,>),
			typeof(Action<,,,,,,,,>),
			typeof(Action<,,,,,,,,,>),
			typeof(Action<,,,,,,,,,,>),
			typeof(Action<,,,,,,,,,,,>),
			typeof(Action<,,,,,,,,,,,,>),
			typeof(Action<,,,,,,,,,,,,,>),
			typeof(Action<,,,,,,,,,,,,,,>),
			typeof(Action<,,,,,,,,,,,,,,,>),
		};

		private static readonly Type[] funcTypes =
		{
			typeof(Func<>),
			typeof(Func<,>),
			typeof(Func<,,>),
			typeof(Func<,,,>),
			typeof(Func<,,,,>),
			typeof(Func<,,,,,>),
			typeof(Func<,,,,,,>),
			typeof(Func<,,,,,,,>),
			typeof(Func<,,,,,,,,>),
			typeof(Func<,,,,,,,,,>),
			typeof(Func<,,,,,,,,,,>),
			typeof(Func<,,,,,,,,,,,>),
			typeof(Func<,,,,,,,,,,,,>),
			typeof(Func<,,,,,,,,,,,,,>),
			typeof(Func<,,,,,,,,,,,,,,>),
			typeof(Func<,,,,,,,,,,,,,,,>),
			typeof(Func<,,,,,,,,,,,,,,,,>),
		};

		#endregion

		#region Constructor

		static ReflectionUtility()
		{
			PropertyInfo property = typeof(Type).GetProperty("IsSzArray", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public) ??
			                        typeof(Type).GetProperty("IsSZArray", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
			MethodInfo getter = property.GetGetMethod(true);
			isSzArrayFunc = (Func<Type, bool>)Delegate.CreateDelegate(typeof(Func<Type, bool>), getter);
		}
		
		#endregion

		#region Methods

		private static readonly HashSet<string> displayNameImports = new HashSet<string>()
		{
			"System",
			"System.Collections",
			"System.Collections.Generic",
			"System.Linq",
			"System.Reflection"
		};

		/// <summary>
		/// Returns a C#-esque name for the type.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static string GetDisplayName(this Type type, IEnumerable<string> importedNamespaces = null)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			if (type == typeof(void)) return "void";
			if (type == typeof(bool)) return "bool";
			if (type == typeof(sbyte)) return "sbyte";
			if (type == typeof(byte)) return "byte";
			if (type == typeof(short)) return "short";
			if (type == typeof(ushort)) return "ushort";
			if (type == typeof(int)) return "int";
			if (type == typeof(uint)) return "uint";
			if (type == typeof(long)) return "long";
			if (type == typeof(ulong)) return "ulong";
			if (type == typeof(float)) return "float";
			if (type == typeof(double)) return "double";
			if (type == typeof(char)) return "char";
			if (type == typeof(decimal)) return "decimal";
			if (type == typeof(string)) return "string";
			if (type == typeof(object)) return "object";
			if (type.IsPointer) return $"{type.GetElementType().GetDisplayName(importedNamespaces)}*";
			if (type.IsByRef) return $"{type.GetElementType().GetDisplayName(importedNamespaces)}&";
			if (type.IsGenericParameter) return type.Name;

			if (type.IsArray)
			{
				StringBuilder str = new StringBuilder(type.GetElementType().GetDisplayName(importedNamespaces));
				int rank = type.GetArrayRank();

				str.Append('[');
				for (int i = 1; i < rank; ++i)
					str.Append(',');
				str.Append(']');
				return str.ToString();
			}

			StringBuilder name = new StringBuilder();
			if (type.IsNested)
			{
				name.Append(type.DeclaringType.GetDisplayName(importedNamespaces));
				name.Append('.');
				name.Append(type.Name);
			}
			else
			{
				bool imported = displayNameImports.Contains(type.Namespace);

				if (!imported && importedNamespaces != null)
					imported = importedNamespaces.Contains(type.Namespace);

				if (!imported)
				{
					name.Append(type.Namespace);
					name.Append('.');
				}

				name.Append(type.Name);
			}

			if (type.IsGenericType)
			{
				for (int i = name.Length - 1; i > 0; --i)
				{
					if (name[i] == '`')
					{
						name.Length = i;
						break;
					}
				}

				Type[] args = type.GetGenericArguments();
				name.Append('<');

				for (int i = 0; i < args.Length; ++i)
				{
					if (i > 0)
						name.Append(',');

					name.Append(args[i].GetDisplayName(importedNamespaces));
				}

				name.Append('>');
			}

			return name.ToString();
		}

		/// <summary>
		/// Returns a delegate type that takes the specified parameters and no return type.
		/// </summary>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public static Type GetActionType(params Type[] parameters)
		{
			if (parameters.Length == 0)
				return typeof(Action);

			int index = parameters.Length - 1;
			if (index >= actionTypes.Length)
				throw new ArgumentException("Too many parameters.", nameof(parameters));
			
			return actionTypes[index].MakeGenericType(parameters);
		}
		
		/// <summary>
		/// Returns a delegate type that takes the specified parameters and return type.
		/// </summary>
		/// <param name="returnType"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		public static Type GetFuncType(Type returnType, params Type[] parameters)
		{
			if (parameters.Length >= funcTypes.Length)
				throw new ArgumentException("Too many parameters.", nameof(parameters));

			Type[] genericArgs = new Type[parameters.Length + 1];
			Array.Copy(parameters, 0, genericArgs, 0, parameters.Length);
			genericArgs[genericArgs.Length - 1] = returnType;
			
			return funcTypes[parameters.Length].MakeGenericType(genericArgs);
		}

		/// <summary>
		/// Returns a value that indicates whether the specified type is a <see cref="Nullable{T}"/>.
		/// </summary>
		/// <param name="type">The type to check.</param>
		/// <returns>True, if the type is a <see cref="Nullable{T}"/></returns>
		public static bool IsNullable(this Type type)
		{
			return type.IsConstructedGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
		}

		/// <summary>
		/// Returns a value that indicates whether the specified type is a <see cref="Nullable{T}"/> and returns the type of the nullable value.
		/// </summary>
		/// <param name="type">The type to check.</param>
		/// <param name="baseType">If the type is a <see cref="Nullable{T}"/>, contains the value type. Otherwise, the input type is returned.</param>
		/// <returns>True, if the type is a <see cref="Nullable{T}"/></returns>
		public static bool IsNullable(this Type type, out Type baseType)
		{
			if (type.IsConstructedGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				baseType = type.GenericTypeArguments[0];
				return true;
			}

			baseType = type;
			return false;
		}

		/// <summary>
		/// Returns a value that indicates whether the specified type is a single-dimension, zero-based array (vector in .NET).
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static bool IsSzArray(this Type type)
		{
			// For some reason, the property for this is internal, but we can access it via reflection.
			// It seems like .NET Core will make this property public. Until then, we need to use this hack.
			return isSzArrayFunc(type);
		}

		/// <summary>
		/// Returns all methods that have the specified custom attribute.
		/// </summary>
		/// <param name="declType"></param>
		/// <param name="attribType"></param>
		/// <returns></returns>
		public static IEnumerable<MethodInfo> GetMethodsWithAttribute(this Type declType, Type attribType)
		{
			while (declType != null)
			{
				foreach (MethodInfo method in declType.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
				{
					if (method.GetCustomAttributes(attribType).Any())
					{
						yield return method;
					}
				}

				declType = declType.BaseType;
			}
		}

		/// <summary>
		/// Returns the "Invoke" method of a delegate type.
		/// </summary>
		/// <param name="delegateType"></param>
		/// <returns></returns>
		public static MethodInfo GetInvokeMethod(this Type delegateType)
		{
			if (!delegateType.IsSubclassOf(typeof(Delegate)))
				throw new ArgumentException("Type is not a Delegate.", nameof(delegateType));

			return delegateType.GetMethod("Invoke");
		}

		/// <summary>
		/// Returns a value that indicates whether the specified method overrides the specified base method.
		/// </summary>
		/// <param name="method"></param>
		/// <param name="baseMethod"></param>
		/// <returns></returns>
		public static bool IsOverrideOf(this MethodInfo method, MethodInfo baseMethod)
		{
			if (method == baseMethod)
				return false;

			return method.GetBaseDefinition() == baseMethod;
		}

		/// <summary>
		/// Returns a value that indicates whether the specified type contains an override of the specified method.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="baseMethod"></param>
		/// <returns></returns>
		public static bool OverridesMethod(this Type type, MethodInfo baseMethod)
		{
			return type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).Any(m => m.IsOverrideOf(baseMethod));
		}

		public static MemberInfo GetFieldOrProperty(this Type type, string name, BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
		{
			FieldInfo field = type.GetField(name, flags);
			if(field != null)
				return field;

			PropertyInfo property = type.GetProperty(name, flags);
			if (property != null)
				return property;

			throw new MissingMemberException(type.GetDisplayName(), name);
		}

		public static Type GetFieldOrPropertyType(this Type type, string name)
		{
			MemberInfo member = GetFieldOrProperty(type, name);
			if (member is PropertyInfo prop)
			{
				return prop.PropertyType;
			}

			return ((FieldInfo)member).FieldType;
		}


		#endregion
		
	}
}
