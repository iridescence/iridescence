﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Implements a list that exposes its backing storage as <see cref="Span{T}"/> and <see cref="Memory{T}"/>.
	/// Ideal for fast struct storage.
	/// </summary>
	public class MemoryList<T> : IList<T>
	{
		#region Fields

		private T[] array;
		private int count;
		private readonly bool clearOnRemove;

		#endregion
		
		#region Properties

		public int Count => this.count;

		public bool IsReadOnly => false;

		public ref T this[int index]
		{
			get
			{
				if (unchecked((uint)index) >= this.count)
					throw new ArgumentOutOfRangeException(nameof(index));

				return ref this.array[index];
			}
		}

		T IList<T>.this[int index]
		{
			get
			{
				if (unchecked((uint)index) >= this.count)
					throw new ArgumentOutOfRangeException(nameof(index));

				return this.array[index];
			}
			set
			{
				if (unchecked((uint)index) >= this.count)
					throw new ArgumentOutOfRangeException(nameof(index));

				this.array[index] = value;
			}
		}

		public Span<T> Span => this.array == null ? Span<T>.Empty : new Span<T>(this.array, 0, this.count);

		public Memory<T> Memory => this.array == null ? Memory<T>.Empty : new Memory<T>(this.array, 0, this.count);

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MemoryList{T}"/> class. 
		/// </summary>
		public MemoryList(bool clearOnRemove = true)
		{
			this.clearOnRemove = clearOnRemove;
		}
		
		#endregion
		
		#region Methods

		private void ensureCapacity(int numItems)
		{
			int newSize = this.count + numItems;

			if (this.array == null || newSize >= this.array.Length)
			{
				long capacity = 1;
				while (capacity < newSize)
					capacity <<= 1;

				if (this.array == null)
					this.array = new T[newSize];
				else
					Array.Resize(ref this.array, checked((int)capacity));
			}
		}

		void ICollection<T>.Add(T item)
		{
			this.Add(in item);
		}
		
		public void Add(in T item)
		{
			this.ensureCapacity(1);
			this.array[this.count++] = item;
		}

		public void Add(ReadOnlySpan<T> items)
		{
			this.ensureCapacity(items.Length);
			items.CopyTo(this.array.ToSpan(this.count, items.Length));
			this.count += items.Length;
		}

		void IList<T>.Insert(int index, T item)
		{
			this.Insert(index, in item);
		}

		public void Insert(int index, in T item)
		{
			if (unchecked((uint)index) > this.count)
				throw new ArgumentOutOfRangeException(nameof(index));

			this.ensureCapacity(1);

			Array.Copy(this.array, index, this.array, index + 1, this.count - index);
			this.array[index] = item;
			++this.count;
		}

		public void Insert(int index, ReadOnlySpan<T> items)
		{
			if (unchecked((uint)index) > this.count)
				throw new ArgumentOutOfRangeException(nameof(index));

			this.ensureCapacity(items.Length);
			Array.Copy(this.array, index, this.array, index + items.Length, this.count - index);
			items.CopyTo(this.array.ToSpan(index, items.Length));
			this.count += items.Length;
		}

		public void RemoveAt(int index)
		{
			if (unchecked((uint)index) >= this.count)
				throw new ArgumentOutOfRangeException(nameof(index));

			this.RemoveRange(index, 1);
		}

		public void RemoveRange(int index, int count)
		{
			if (count < 0 || count > this.count)
				throw new ArgumentOutOfRangeException(nameof(count));

			if (index < 0 || index + count > this.count)
				throw new ArgumentOutOfRangeException(nameof(index));

			if (count == 0)
				return;

			this.count -= count;
			if (index < this.count)
			{
				Array.Copy(this.array, index + count, this.array, index, this.count - index);
			}

			if (this.clearOnRemove)
			{
				Array.Clear(this.array, this.count, count);
			}
		}

		bool ICollection<T>.Remove(T item)
		{
			return this.Remove(in item);
		}

		public bool Remove(in T item)
		{
			int index = this.IndexOf(item);
			if (index < 0)
				return false;

			this.RemoveAt(index);
			return true;
		}

		int IList<T>.IndexOf(T item)
		{
			return this.IndexOf(in item);
		}

		public int IndexOf(in T item)
		{
			return Array.IndexOf(this.array, item, 0, this.count);
		}

		bool ICollection<T>.Contains(T item)
		{
			return this.Contains(in item);
		}

		public bool Contains(in T item)
		{
			return this.IndexOf(item) >= 0;
		}

		public void Clear()
		{
			this.RemoveRange(0, this.count);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			Array.Copy(this.array, 0, array, arrayIndex, this.count);
		}

		public IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < this.count; ++i)
			{
				yield return this.array[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}
}
