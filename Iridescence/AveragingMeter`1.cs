﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;

namespace Iridescence
{
	/// <summary>
	/// Utility class to measure the average rate of change of a value over a period of time.
	/// </summary>
	public class AveragingMeter<T>
		where T : struct
	{
		#region Fields
		
		private readonly TimeSpan minTime;
		private readonly List<Sample> samples;
		private T currentValue;

		private static Func<T, T, T> difference;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the difference function.
		/// </summary>
		private static Func<T, T, T> Difference
		{
			get
			{
				if (difference == null)
				{
					ParameterExpression paramA = Expression.Parameter(typeof(T));
					ParameterExpression paramB = Expression.Parameter(typeof(T));
					BinaryExpression expr = Expression.Subtract(paramA, paramB);
					difference = Expression.Lambda<Func<T, T, T>>(expr, paramA, paramB).Compile();
				}

				return difference;
			}
		}

		/// <summary>
		/// Gets or sets the current value.
		/// </summary>
		public T Value
		{
			get => this.currentValue;
			set
			{
				this.currentValue = value;

				this.removeOldSamples();

				// Add sample.
				Sample sample = new Sample(DateTime.Now, value);

				int n = this.samples.Count;
				if (n > 0 && sample.Time - this.samples[n - 1].Time <= this.minTime)
				{
					this.samples[n - 1] = sample;
				}
				else
				{
					this.samples.Add(sample);
				}
			}
		}

		/// <summary>
		/// Gets the period of time for which the value is measured.
		/// </summary>
		public TimeSpan Period { get; }

		/// <summary>
		/// Gets a value that represents the difference between the current value and the first value in the period.
		/// </summary>
		public T Rate
		{
			get
			{
				T oldValue = this.getValueAt(DateTime.Now - this.Period);
				return Difference(this.currentValue, oldValue);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AveragingMeter{T}"/> class. 
		/// </summary>
		/// <param name="initialValue">The initial value.</param>
		/// <param name="period">The period of time for which to measure the value.</param>
		public AveragingMeter(T initialValue, TimeSpan period)
		{
			this.currentValue = initialValue;
			this.Period = period;
			this.minTime = new TimeSpan(period.Ticks / 1000);
			this.samples = new List<Sample>();
			this.samples.Add(new Sample(DateTime.Now, initialValue));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AveragingMeter{T}"/> class using the default period of 1 second.
		/// </summary>
		/// <param name="initialValue">The initial value.</param>
		public AveragingMeter(T initialValue)
			: this(initialValue, TimeSpan.FromSeconds(1.0))
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AveragingMeter{T}"/> class using the default value and a period of 1 second.
		/// </summary>
		public AveragingMeter()
			: this(default(T))
		{
			
		}

		#endregion

		#region Methods

		private int findSample(DateTime time)
		{
			int min = 0;
			int max = this.samples.Count - 1;

			while (min <= max)
			{
				int mid = (min + max) >> 1;
				DateTime midTime = this.samples[mid].Time;

				if (midTime >= time)
					max = mid - 1;
				else
					min = mid + 1;
			}

			return min - 1;
		}

		private T getValueAt(DateTime time)
		{
			if (this.samples.Count == 0)
				return this.currentValue;

			int index1 = this.findSample(time);
			if (index1 < 0)
				return this.samples[0].Value;

			int index2 = index1 + 1;
			if (index2 >= this.samples.Count)
				return this.samples[this.samples.Count - 1].Value;

			// Interpolate.
			TimeSpan span = this.samples[index2].Time - this.samples[index1].Time;
			TimeSpan offset = time - this.samples[index1].Time;
			float t = (float)offset.Ticks / span.Ticks;
			return Interpolation<T>.Linear(this.samples[index1].Value, this.samples[index2].Value, t);
		}

		private void removeOldSamples()
		{
			int index1 = this.findSample(DateTime.Now - this.Period);
			if (index1 <= 0)
				return;

			this.samples.RemoveRange(0, index1);
		}

		#endregion

		#region Nested Types

		[DebuggerDisplay("{Time} {Value}")]
		private struct Sample
		{
			public readonly DateTime Time;
			public readonly T Value;

			public Sample(DateTime time, T value)
			{
				this.Time = time;
				this.Value = value;
			}
		}

		#endregion
	}
}
