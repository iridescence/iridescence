﻿using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence
{
	/// <summary>
	/// Extension methods for enumerables.
	/// </summary>
	public static class EnumerableExtensions
	{
		#region Methods

		/// <summary>
		/// Returns an enumerable that returns the specified item once.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="item"></param>
		/// <returns></returns>
		public static IEnumerable<T> Once<T>(T item)
		{
			yield return item;
		}

		public static IEnumerable<T> AsEnumerable<T>(this IEnumerable enumerable)
		{
			foreach (T item in enumerable)
			{
				yield return item;
			}
		}

		/// <summary>
		/// Runs the specified <see cref="Action"/> on each item in the <see cref="IEnumerable{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="enumerable"></param>
		/// <param name="action"></param>
		public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
		{
			if (enumerable == null)
				throw new ArgumentNullException(nameof(enumerable));

			if (action == null)
				throw new ArgumentNullException(nameof(action));
			
			foreach (T item in enumerable)
			{
				action(item);
			}
		}

		/// <summary>
		/// Returns an <see cref="IReadOnlyList{T}"/> that contains all elements from the specified <see cref="IEnumerable{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="enumerable"></param>
		/// <param name="throwOnNullElement">Throw an exception if one of the elements is null.</param>
		/// <returns></returns>
		public static IReadOnlyList<T> ToReadOnlyList<T>(this IEnumerable<T> enumerable, bool throwOnNullElement = false)
		{
			if (enumerable == null)
				throw new ArgumentNullException(nameof(enumerable));

			if (enumerable is ICollection<T> collection && collection.Count == 0)
				return EmptyReadOnlyList<T>.Instance;

			if (enumerable is IReadOnlyCollection<T> roCollection && roCollection.Count == 0)
				return EmptyReadOnlyList<T>.Instance;

			T[] array = enumerable.ToArray();

			if (throwOnNullElement)
			{
				for (int i = 0; i < array.Length; ++i)
				{
					if (array[i] == null)
						throw new ArgumentException($"The item at position {i} is null.", nameof(enumerable));
				}
			}

			return array;
		}

		/// <summary>
		/// Returns a <see cref="PooledArray{T}"/> from the specified <see cref="ArrayPool{T}"/> containing the items for the specified <see cref="ICollection{T}"/>.
		/// </summary>
		/// <param name="collection"></param>
		/// <param name="pool">The pool. If null, <see cref="ArrayPool{T}.Shared"/> is used.</param>
		/// <returns></returns>
		public static PooledArray<T> ToPooledArray<T>(this ICollection<T> collection, ArrayPool<T> pool = null)
		{
			if (pool == null)
				pool = ArrayPool<T>.Shared;

			int count = collection.Count;
			T[] array = pool.Rent(count);

			collection.CopyTo(array, 0);
			return new PooledArray<T>(pool, array, count);
		}

		/// <summary>
		/// Returns a <see cref="PooledArray{T}"/> from the specified <see cref="ArrayPool{T}"/> containing the items for the specified <see cref="IEnumerable{T}"/>.
		/// </summary>
		/// <param name="enumerable"></param>
		/// <param name="pool">The pool. If null, <see cref="ArrayPool{T}.Shared"/> is used.</param>
		/// <param name="initialCapacity">The initial size of the array to rent.</param>
		/// <returns></returns>
		public static PooledArray<T> ToPooledArray<T>(this IEnumerable<T> enumerable, ArrayPool<T> pool = null, int initialCapacity = 16)
		{
			if (pool == null)
				pool = ArrayPool<T>.Shared;

			if (initialCapacity <= 0)
				initialCapacity = 1;

			T[] array = null;
			int index = 0;

			foreach (T item in enumerable)
			{
				if (array == null || array.Length <= index)
				{
					T[] newArray = pool.Rent(Math.Max(initialCapacity, index * 2));

					if (array != null)
					{
						Array.Copy(array, 0, newArray, 0, index);
						pool.Return(array, true);
					}

					array = newArray;
				}

				array[index] = item;
				++index;
			}

			return new PooledArray<T>(pool, array, index);
		}

		/// <summary>
		/// Returns true if the <see cref="IEnumerable{T}"/> is null or contains null references.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="enumerable"></param>
		/// <returns></returns>
		public static bool IsNullOrContainsNull<T>(this IEnumerable<T> enumerable)
		{
			if(enumerable == null)
				return true;

			return enumerable.Any(item => item == null);
		}

		/// <summary>
		/// Returns true if the <see cref="IEnumerable{T}"/> is null, empty or contains null references.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="enumerable"></param>
		/// <returns></returns>
		public static bool IsEmptyOrContainsNull<T>(this IEnumerable<T> enumerable)
		{
			if(enumerable == null)
				return true;

			int count = 0;
			foreach(T item in enumerable)
			{
				if(item == null)
					return true;

				++count;
			}

			return count == 0;
		}

		#endregion
	}
}
