﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;

namespace Iridescence
{
	/// <summary>
	/// Respresents a simple data node with a name, a value and attributes.
	/// It can have multiple child nodes, thus creating a tree.
	/// </summary>
	public sealed partial class DataNode : List<DataNode>
	{
		#region Fields

		private string name;
		private string value;
		private Dictionary<string, string> attributes;
		
		/// <summary>
		/// Settings for the XML reader.
		/// </summary>
		private static readonly XmlReaderSettings readerSettings = new XmlReaderSettings
		{
			CloseInput = false
		};

		/// <summary>
		/// Settings for the XML reader.
		/// </summary>
		private static readonly XmlWriterSettings writerSettings = new XmlWriterSettings
		{
			Indent = true,
			IndentChars = "\t",
			NewLineChars = "\n",
			NewLineOnAttributes = false,
			CloseOutput = false,
		};

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the name of the element.
		/// </summary>
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		/// <summary>
		/// Gets or sets the value of the element.
		/// </summary>
		public string Value
		{
			get { return this.value; }
			set { this.value = value; }
		}

		/// <summary>
		/// Gets or sets the attributes of the element.
		/// </summary>
		public Dictionary<string, string> Attributes
		{
			get { return this.attributes; }
			set { this.attributes = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new DataElement.
		/// </summary>
		public DataNode()
			: this(null)
		{
		
		}

		/// <summary>
		/// Creates a new DataElement.
		/// </summary>
		public DataNode(string name)
			: this(name, null)
		{
		
		}

		/// <summary>
		/// Creates a new DataElement.
		/// </summary>
		public DataNode(string name, string value)
		{
			this.name = name;
			this.value = value;
			this.attributes = new Dictionary<string, string>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the first element with the specified name.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public DataNode GetElement(string name)
		{
			foreach (DataNode element in this)
			{
				if (element.name == name)
					return element;
			}

			return null;
		}

		/// <summary>
		/// Returns all element with the specified name.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public IEnumerable<DataNode> GetElements(string name)
		{
			foreach (DataNode element in this)
			{
				if (element.name == name)
					yield return element;
			}
		}

		/// <summary>
		/// Returns the value of an attribute.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public string GetAttribute(string name, string defaultValue = null)
		{
			string value;
			if (!this.attributes.TryGetValue(name, out value))
				return defaultValue;

			return value;
		}

		/// <summary>
		/// Parses an attribute.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="name"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public T ParseAttribute<T>(string name, T defaultValue = default(T)) where T : struct
		{
			string str = this.GetAttribute(name);
			if (str == null)
				return defaultValue;
				
			Type type = typeof(T);
			if (type.IsEnum)
			{
				T value = default(T);
				if (!Enum.TryParse(str, out value))
					throw new Exception($"Could not parse attribute \"{str}\" as {type.Name}.");

				return value;
			}

			if (type == typeof(sbyte)) return (T)(object)sbyte.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(byte)) return (T)(object)byte.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(short)) return (T)(object)short.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(ushort)) return (T)(object)ushort.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(int)) return (T)(object)int.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(uint)) return (T)(object)uint.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(long)) return (T)(object)long.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(ulong)) return (T)(object)ulong.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(float)) return (T)(object)float.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(double)) return (T)(object)double.Parse(str, CultureInfo.InvariantCulture);
			if (type == typeof(char)) return (T)(object)char.Parse(str);

			throw new NotSupportedException($"Type {type.Name} is not supported.");
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			str.Append(this.name);

			if (this.attributes.Count > 0)
			{
				bool first = true;

				foreach (KeyValuePair<string, string> attribute in this.attributes)
				{
					if (first)
						first = false;
					else
						str.Append(", ");

					str.Append(attribute.Key);
					str.Append(" = \"");
					str.Append(attribute.Value);
					str.Append("\"");
				}
			}

			return str.ToString();
		}

		#region ReadXml

		/// <summary>
		/// Reads a data element tree from the specified stream in the XML format.
		/// </summary>
		/// <param name="stream"></param>
		/// <returns></returns>
		public static DataNode ReadXml(Stream stream)
		{
			using (XmlReader xmlReader = XmlReader.Create(stream, readerSettings))
			{
				return readXml(xmlReader);
			}
		}

		/// <summary>
		/// Reads a data element tree from the specified text reader in the XML format.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public static DataNode ReadXml(TextReader reader)
		{
			using (XmlReader xmlReader = XmlReader.Create(reader, readerSettings))
			{
				return readXml(xmlReader);
			}
		}

		/// <summary>
		/// Reads a data element tree from the specified XML reader.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		private static DataNode readXml(XmlReader reader)
		{
			while (reader.NodeType != XmlNodeType.Element)
				reader.Read();

			DataNode element = new DataNode();
			element.Name = reader.Name;

			if (reader.MoveToFirstAttribute())
			{
				do
				{
					element.attributes[reader.Name] = reader.Value;
				} while (reader.MoveToNextAttribute());
				reader.MoveToElement();
			}

			if (!reader.IsEmptyElement)
			{
				bool exitLoop = false;

				while (!exitLoop)
				{
					if (!reader.Read())
						throw new Exception("XML parsing error.");

					switch (reader.NodeType)
					{
						case XmlNodeType.Element:
							element.Add(readXml(reader));
							break;

						case XmlNodeType.CDATA:
						case XmlNodeType.Text:
							if (element.Value == null)
								element.Value = reader.Value;
							else
								element.Value += reader.Value;

							break;

						case XmlNodeType.EndElement:
							if (reader.Name != element.Name)
								throw new Exception("XML parsing error.");

							exitLoop = true;
							break;
					}
				}
			}

			return element;
		}

		#endregion

		#region WriteXml

		/// <summary>
		/// Writes the data element to the stream using the XML format.
		/// </summary>
		/// <param name="stream"></param>
		public void WriteXml(Stream stream)
		{
			using (XmlWriter xmlWriter = XmlWriter.Create(stream, writerSettings))
			{
				this.writeXml(xmlWriter);
			}
		}

		/// <summary>
		/// Writes the data element to the stream using the XML format.
		/// </summary>
		/// <param name="writer"></param>
		public void WriteXml(TextWriter writer)
		{
			using (XmlWriter xmlWriter = XmlWriter.Create(writer, writerSettings))
			{
				this.writeXml(xmlWriter);
			}
		}

		private void writeXml(XmlWriter writer)
		{
			writer.WriteStartElement(this.name);

			foreach (KeyValuePair<string, string> attribute in this.attributes)
			{
				if (attribute.Value == null)
					continue;

				writer.WriteStartAttribute(attribute.Key);
				writer.WriteValue(attribute.Value);
				writer.WriteEndAttribute();
			}

			foreach (DataNode element in this)
			{
				element.writeXml(writer);
			}

			writer.WriteValue(this.value);
			writer.WriteEndElement();
		}

		#endregion

		#endregion
	}
}
