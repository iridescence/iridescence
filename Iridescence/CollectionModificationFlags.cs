﻿using System;

namespace Iridescence
{
	/// <summary>
	/// The flags for collection modification events.
	/// </summary>
	[Flags]
	public enum CollectionModificationFlags
	{
		/// <summary>
		/// One or more items were added.
		/// </summary>
		Add = 0x01,

		/// <summary>
		/// One or more items were removed.
		/// </summary>
		Remove = 0x02,

		/// <summary>
		/// The collection was cleared. This is always used in combination with <see cref="Remove"/>.
		/// </summary>
		Clear = 0x04,

		/// <summary>
		/// The event represents a replace modification (e.g. by doing collection[i] = value). This is always used in combination with <see cref="Add"/> and <see cref="Remove"/>.
		/// </summary>
		Replace = 0x08,
	}
}