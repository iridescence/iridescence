﻿using System;
using System.Collections.Generic;

namespace Iridescence
{
	/// <summary>
	/// Implements a stack that uses a span for storage. If the stack grows beyond what the span can store, additional space is allocated on the heap.
	/// </summary>
	public ref struct SpanStack<T>
	{
		#region Fields

		private readonly Span<T> span;
		private Stack<T> stack;
		private int count;
		
		#endregion
		
		#region Properties

		public int Count => this.count;
		
		#endregion
		
		#region Constructors

		public SpanStack(Span<T> span, int initialCount = 0)
		{
			if (initialCount > span.Length)
				throw new ArgumentOutOfRangeException(nameof(initialCount));

			this.span = span;
			this.stack = null;
			this.count = initialCount;
		}

		#endregion
		
		#region Methods

		private void pushOnHeap(in T item)
		{
			if (this.stack == null)
				this.stack = new Stack<T>();

			this.stack.Push(item);
			++this.count;
		}

		public void Push(T item)
		{
			if (this.count >= this.span.Length)
			{
				this.pushOnHeap(item);
			}
			else
			{
				this.span[this.count++] = item;
			}
		}

		public void Push(in T item)
		{
			if (this.count >= this.span.Length)
			{
				this.pushOnHeap(item);
			}
			else
			{
				this.span[this.count++] = item;
			}
		}

		private static void throwInvalidOp()
		{
			throw new InvalidOperationException("Stack is empty.");
		}

		public T Pop()
		{
			if (this.count == 0)
				throwInvalidOp();

			if (this.count <= this.span.Length)
				return this.span[--this.count];

			T result = this.stack.Pop();
			--this.count;
			return result;
		}

		public void Pop(out T item)
		{
			if (this.count == 0)
				throwInvalidOp();

			if (this.count <= this.span.Length)
			{
				item = this.span[--this.count];
			}
			else
			{
				item = this.stack.Pop();
				--this.count;
			}
		}

		#endregion
	}
}
