﻿using System;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;

namespace Iridescence
{
	/// <summary>
	/// Holds the parameters of a CRC-64 checksum algorithm.
	/// </summary>
	public sealed class Crc64
	{
		#region Fields

		public static readonly Crc64 Default = new Crc64(0xC96C5795D7870F42ul, 0xFFFFFFFFFFFFFFFFul, 0xFFFFFFFFFFFFFFFFul);

		/// <summary>
		/// The polynomial of the CRC-64.
		/// </summary>
		public readonly ulong Polynomial;

		/// <summary>
		/// The initial value of the checksum.
		/// </summary>
		public readonly ulong InitialValue;

		/// <summary>
		/// A XOR mask applied to the checksum on return.
		/// </summary>
		public readonly ulong Mask;

		/// <summary>
		/// The look-up table.
		/// </summary>
		public readonly ulong[] Table;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new CRC-64 hash algorithm using the specified polynomial.
		/// </summary>
		/// <param name="polynomial"></param>
		/// <param name="initialValue"></param>
		/// <param name="mask"></param>
		public Crc64(ulong polynomial, ulong initialValue, ulong mask)
		{
			this.Polynomial = polynomial;
			this.InitialValue = initialValue;
			this.Mask = mask;
			this.Table = new ulong[256];

			for (ulong i = 0; i < 256; i++)
			{
				ulong entry = i;

				for (ulong j = 0; j < 8; j++)
					if ((entry & 1) == 1)
						entry = (entry >> 1) ^ polynomial;
					else
						entry = entry >> 1;

				this.Table[i] = entry;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Computes the checksum for the specified byte array.
		/// </summary>
		/// <param name="data">The input data.</param>
		/// <returns>The checksum.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Pure]
		public ulong ComputeChecksum(ReadOnlySpan<byte> data)
		{
			return this.ComputeChecksum(this.InitialValue, data) ^ this.Mask;
		}

		/// <summary>
		/// Computes the checksum for the specified byte array. Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The initial value.</param>
		/// <param name="data">The input data.</param>
		/// <returns>The checksum.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Pure]
		public ulong ComputeChecksum(ulong current, ReadOnlySpan<byte> data)
		{
			this.ComputeChecksum(ref current, data);
			return current;
		}

		/// <summary>
		/// Computes the checksum for the specified byte array. Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The initial value.</param>
		/// <param name="data">The input data.</param>
		/// <returns>The checksum.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void ComputeChecksum(ref ulong current, ReadOnlySpan<byte> data)
		{
			for (int i = 0; i < data.Length; i++)
			{
				current = (current >> 8) ^ this.Table[(current & 0xFF) ^ data[i]];
			}
		}

		/// <summary>
		/// Computes the checksum for the specified byte.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The initial value.</param>
		/// <param name="data">The input data.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Pure]
		public ulong ComputeChecksum(ulong current, byte data)
		{
			return (current >> 8) ^ this.Table[(current & 0xFF) ^ data];
		}

		/// <summary>
		/// Computes the checksum for the specified byte.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The initial value.</param>
		/// <param name="data">The input data.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void ComputeChecksum(ref ulong current, byte data)
		{
			current = (current >> 8) ^ this.Table[(current & 0xFF) ^ data];
		}

		#endregion
	}
}
