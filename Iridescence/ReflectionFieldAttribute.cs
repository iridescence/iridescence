﻿using System;
using System.Reflection;

namespace Iridescence
{
	/// <summary>
	/// Selects a field that a <see cref="MemberInfo"/> of a member marked with this attribute is stored in.
	/// </summary>
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Event | AttributeTargets.Field|AttributeTargets.Method|AttributeTargets.Property)]
	public class ReflectionFieldAttribute : Attribute
	{
		#region Properties

		public string FieldName { get; }
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="ReflectionFieldAttribute"/> class. 
		/// </summary>
		public ReflectionFieldAttribute(string fieldName)
		{
			this.FieldName = fieldName;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Goes through the members of the specified type that have been marked with <see cref="ReflectionFieldAttribute"/> and stores the member in the specified field.
		/// </summary>
		/// <param name="type"></param>
		public static void AssignFields(Type type)
		{
			foreach (MemberInfo member in type.GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly))
			{
				ReflectionFieldAttribute attr = member.GetCustomAttribute<ReflectionFieldAttribute>();
				if (attr == null)
					continue;

				FieldInfo field = type.GetField(attr.FieldName, BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
				if (field == null)
					throw new MissingFieldException(type.FullName, attr.FieldName);
				field.SetValue(null, member);
			}
		}

		#endregion
	}
}
