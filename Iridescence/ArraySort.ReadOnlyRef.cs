﻿using System;
using System.Diagnostics;

namespace Iridescence
{
	internal static class ArraySortReadOnlyRef<T, TComparer>
		where TComparer : IReadOnlyRefComparer<T>
	{
		public static void Sort(T[] keys, int index, int length, TComparer comparer)
		{
			if (comparer == null)
				throw new ArgumentNullException(nameof(comparer));

			Debug.Assert(keys != null, "Check the arguments in the caller!");
			Debug.Assert(index >= 0 && length >= 0 && (keys.Length - index >= length), "Check the arguments in the caller!");

			// Add a try block here to detect IComparers (or their
			// underlying IComparables, etc) that are bogus.
			try
			{
				IntrospectiveSort(keys, index, length, comparer);
			}
			catch (IndexOutOfRangeException)
			{
				throw new ArgumentException($"Unable to sort because the IComparer.Compare() method returns inconsistent results. Either a value does not compare equal to itself, or one value repeatedly compared to another value yields different results. IComparer: '{comparer}'.", nameof(comparer));
			}
			catch (Exception e)
			{
				throw new InvalidOperationException("Failed to compare two elements in the array.", e);
			}
		}

		public static int BinarySearch(T[] array, int index, int length, in T value, TComparer comparer)
		{
			if (comparer == null)
				throw new ArgumentNullException(nameof(comparer));

			try
			{
				return InternalBinarySearch(array, index, length, value, comparer);
			}
			catch (Exception e)
			{
				throw new InvalidOperationException("Failed to compare two elements in the array.", e);
			}
		}


#if HW_INTRINSICS
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
#endif
		internal static int FloorLog2PlusOne(int n)
		{
#if HW_INTRINSICS
			if (System.Runtime.Intrinsics.X86.Lzcnt.IsSupported)
				return 32 - unchecked((int)System.Runtime.Intrinsics.X86.Lzcnt.LeadingZeroCount((uint)n));
#endif
			int result = 0;
			while (n >= 1)
			{
				result++;
				n = n / 2;
			}
			return result;
		}

		internal static int InternalBinarySearch(T[] array, int index, int length, in T value, TComparer comparer)
		{
			Debug.Assert(array != null, "Check the arguments in the caller!");
			Debug.Assert(index >= 0 && length >= 0 && (array.Length - index >= length), "Check the arguments in the caller!");

			int lo = index;
			int hi = index + length - 1;
			while (lo <= hi)
			{
				int i = lo + ((hi - lo) >> 1);
				int order = comparer.Compare(array[i], value);

				if (order == 0) return i;
				if (order < 0)
				{
					lo = i + 1;
				}
				else
				{
					hi = i - 1;
				}
			}

			return ~lo;
		}

		private static void swapIfGreater(T[] keys, TComparer comparer, int a, int b)
		{
			if (a != b)
			{
				if (comparer.Compare(keys[a], keys[b]) > 0)
				{
					T key = keys[a];
					keys[a] = keys[b];
					keys[b] = key;
				}
			}
		}

		private static void swap(T[] a, int i, int j)
		{
			if (i != j)
			{
				T t = a[i];
				a[i] = a[j];
				a[j] = t;
			}
		}

		internal static void IntrospectiveSort(T[] keys, int left, int length, TComparer comparer)
		{
			Debug.Assert(keys != null);
			Debug.Assert(comparer != null);
			Debug.Assert(left >= 0);
			Debug.Assert(length >= 0);
			Debug.Assert(length <= keys.Length);
			Debug.Assert(length + left <= keys.Length);

			if (length < 2)
				return;

			introSort(keys, left, length + left - 1, 2 * FloorLog2PlusOne(length), comparer);
		}

		private static void introSort(T[] keys, int lo, int hi, int depthLimit, TComparer comparer)
		{
			Debug.Assert(keys != null);
			Debug.Assert(comparer != null);
			Debug.Assert(lo >= 0);
			Debug.Assert(hi < keys.Length);

			while (hi > lo)
			{
				int partitionSize = hi - lo + 1;
				if (partitionSize <= ArraySort.IntrosortSizeThreshold)
				{
					if (partitionSize == 1)
					{
						return;
					}
					if (partitionSize == 2)
					{
						swapIfGreater(keys, comparer, lo, hi);
						return;
					}
					if (partitionSize == 3)
					{
						swapIfGreater(keys, comparer, lo, hi - 1);
						swapIfGreater(keys, comparer, lo, hi);
						swapIfGreater(keys, comparer, hi - 1, hi);
						return;
					}

					insertionSort(keys, lo, hi, comparer);
					return;
				}

				if (depthLimit == 0)
				{
					heapsort(keys, lo, hi, comparer);
					return;
				}
				depthLimit--;

				int p = pickPivotAndPartition(keys, lo, hi, comparer);
				// Note we've already partitioned around the pivot and do not have to move the pivot again.
				introSort(keys, p + 1, hi, depthLimit, comparer);
				hi = p - 1;
			}
		}

		private static int pickPivotAndPartition(T[] keys, int lo, int hi, TComparer comparer)
		{
			Debug.Assert(keys != null);
			Debug.Assert(comparer != null);
			Debug.Assert(lo >= 0);
			Debug.Assert(hi > lo);
			Debug.Assert(hi < keys.Length);

			// Compute median-of-three.  But also partition them, since we've done the comparison.
			int middle = lo + ((hi - lo) / 2);

			// Sort lo, mid and hi appropriately, then pick mid as the pivot.
			swapIfGreater(keys, comparer, lo, middle);  // swap the low with the mid point
			swapIfGreater(keys, comparer, lo, hi);   // swap the low with the high
			swapIfGreater(keys, comparer, middle, hi); // swap the middle with the high

			T pivot = keys[middle];
			swap(keys, middle, hi - 1);
			int left = lo, right = hi - 1;  // We already partitioned lo and hi and put the pivot in hi - 1.  And we pre-increment & decrement below.

			while (left < right)
			{
				while (comparer.Compare(keys[++left], pivot) < 0) ;
				while (comparer.Compare(pivot, keys[--right]) < 0) ;

				if (left >= right)
					break;

				swap(keys, left, right);
			}

			// Put pivot in the right location.
			swap(keys, left, (hi - 1));
			return left;
		}

		private static void heapsort(T[] keys, int lo, int hi, TComparer comparer)
		{
			Debug.Assert(keys != null);
			Debug.Assert(comparer != null);
			Debug.Assert(lo >= 0);
			Debug.Assert(hi > lo);
			Debug.Assert(hi < keys.Length);

			int n = hi - lo + 1;
			for (int i = n / 2; i >= 1; i = i - 1)
			{
				downHeap(keys, i, n, lo, comparer);
			}
			for (int i = n; i > 1; i = i - 1)
			{
				swap(keys, lo, lo + i - 1);
				downHeap(keys, 1, i - 1, lo, comparer);
			}
		}

		private static void downHeap(T[] keys, int i, int n, int lo, TComparer comparer)
		{
			Debug.Assert(keys != null);
			Debug.Assert(comparer != null);
			Debug.Assert(lo >= 0);
			Debug.Assert(lo < keys.Length);

			T d = keys[lo + i - 1];
			while (i <= n / 2)
			{
				int child = 2 * i;
				if (child < n && comparer.Compare(keys[lo + child - 1], keys[lo + child]) < 0)
				{
					child++;
				}
				if (!(comparer.Compare(d, keys[lo + child - 1]) < 0))
					break;
				keys[lo + i - 1] = keys[lo + child - 1];
				i = child;
			}
			keys[lo + i - 1] = d;
		}

		private static void insertionSort(T[] keys, int lo, int hi, TComparer comparer)
		{
			Debug.Assert(keys != null);
			Debug.Assert(lo >= 0);
			Debug.Assert(hi >= lo);
			Debug.Assert(hi <= keys.Length);

			for (int i = lo; i < hi; i++)
			{
				int j = i;
				var t = keys[i + 1];
				while (j >= lo && comparer.Compare(t, keys[j]) < 0)
				{
					keys[j + 1] = keys[j];
					j--;
				}
				keys[j + 1] = t;
			}
		}
	}
}
