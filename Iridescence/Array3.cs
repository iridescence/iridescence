﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Iridescence
{
	/// <summary>
	/// 3-element fixed array.
	/// </summary>
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public struct Array3<T> : IList<T>, IReadOnlyList<T>
	{
		#region Fields

		public T A;
		public T B;
		public T C;

		#endregion

		#region Properties

		public int Count => 3;

		public bool IsReadOnly => true;

		public T this[int i]
		{
			get
			{
				switch (i)
				{
					case 0: return this.A;
					case 1: return this.B;
					case 2: return this.C;
				}
				throw new Exception();
			}
			set
			{
				switch (i)
				{
					case 0: this.A = value; break;
					case 1: this.B = value; break;
					case 2: this.C = value; break;
					default:
						throw new Exception();
				}
			}
		}
		#endregion

		#region Constructors

		public Array3(T a, T b, T c)
		{
			this.A = a;
			this.B = b;
			this.C = c;
		}

		#endregion

		#region Methods

		public void Add(T item)
		{
			throw new NotSupportedException();
		}

		public void Clear()
		{
			throw new NotSupportedException();
		}

		public bool Contains(T item)
		{
			return Equals(this.A, item) || Equals(this.B, item) || Equals(this.C, item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			if (array == null)
				throw new ArgumentNullException(nameof(array));

			if (arrayIndex < 0)
				throw new ArgumentOutOfRangeException(nameof(arrayIndex));

			if (arrayIndex + this.Count >= array.Length)
				throw new ArgumentOutOfRangeException(nameof(arrayIndex));

			array[arrayIndex + 0] = this.A;
			array[arrayIndex + 1] = this.B;
			array[arrayIndex + 2] = this.C;
		}

		public bool Remove(T item)
		{
			throw new NotSupportedException();
		}

		public int IndexOf(T item)
		{
			if (Equals(this.A, item)) return 0;
			if (Equals(this.B, item)) return 1;
			if (Equals(this.C, item)) return 2;
			return -1;
		}

		public void Insert(int index, T item)
		{
			throw new NotSupportedException();
		}

		public void RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		public IEnumerator<T> GetEnumerator()
		{
			yield return this.A;
			yield return this.B;
			yield return this.C;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}
}
