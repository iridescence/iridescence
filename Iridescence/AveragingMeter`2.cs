﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Iridescence
{
	/// <summary>
	/// Utility class to measure the average rate of change of a value over a period of time.
	/// </summary>
	public abstract class AveragingMeter<TValue, TInterpolated>
	{
		#region Fields
		
		private readonly TimeSpan minTime;
		private readonly List<Sample> samples;
		private TValue currentValue;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the current value.
		/// </summary>
		public TValue Value
		{
			get => this.currentValue;
			set
			{
				this.currentValue = value;

				this.removeOldSamples();

				// Add sample.
				Sample sample = new Sample(DateTime.Now, value);

				int n = this.samples.Count;
				if (n > 0 && sample.Time - this.samples[n - 1].Time <= this.minTime)
				{
					this.samples[n - 1] = sample;
				}
				else
				{
					this.samples.Add(sample);
				}
			}
		}

		/// <summary>
		/// Gets the period of time for which the value is measured.
		/// </summary>
		public TimeSpan Period { get; }

		/// <summary>
		/// Gets a value that represents the difference between the current value and the first value in the period.
		/// </summary>
		public TInterpolated Rate
		{
			get
			{
				TInterpolated oldValue = this.getValueAt(DateTime.Now - this.Period);
				return this.GetRate(this.currentValue, oldValue, 1.0);
			}
		}

		/// <summary>
		/// Gets a value change per second.
		/// </summary>
		public TInterpolated RatePerSecond
		{
			get
			{
				TInterpolated oldValue = this.getValueAt(DateTime.Now - this.Period);
				return this.GetRate(this.currentValue, oldValue, 1.0 / this.Period.TotalSeconds);
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AveragingMeter{T}"/> class. 
		/// </summary>
		/// <param name="initialValue">The initial value.</param>
		/// <param name="period">The period of time for which to measure the value.</param>
		protected AveragingMeter(TValue initialValue, TimeSpan period)
		{
			this.currentValue = initialValue;
			this.Period = period;
			this.minTime = new TimeSpan(period.Ticks / 1000);
			this.samples = new List<Sample>();
			this.samples.Add(new Sample(DateTime.Now, initialValue));
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AveragingMeter{T}"/> class using the default period of 1 second.
		/// </summary>
		/// <param name="initialValue">The initial value.</param>
		protected AveragingMeter(TValue initialValue)
			: this(initialValue, TimeSpan.FromSeconds(1.0))
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="AveragingMeter{T}"/> class using the default value and a period of 1 second.
		/// </summary>
		protected AveragingMeter()
			: this(default)
		{
			
		}

		#endregion

		#region Methods

		protected abstract TInterpolated Interpolate(TValue a, TValue b, double t);

		protected abstract TInterpolated GetRate(TValue currentValue, TInterpolated interpolatedBaseValue, double factor);

		protected virtual TInterpolated Convert(TValue a) => this.Interpolate(a, a, 0.0);
		
		private int findSample(DateTime time)
		{
			int min = 0;
			int max = this.samples.Count - 1;

			while (min <= max)
			{
				int mid = (min + max) >> 1;
				DateTime midTime = this.samples[mid].Time;

				if (midTime >= time)
					max = mid - 1;
				else
					min = mid + 1;
			}

			return min - 1;
		}

		private TInterpolated getValueAt(DateTime time)
		{
			if (this.samples.Count == 0)
				return this.Convert(this.currentValue);

			int index1 = this.findSample(time);
			if (index1 < 0)
				return this.Convert(this.samples[0].Value);

			int index2 = index1 + 1;
			if (index2 >= this.samples.Count)
				return this.Convert(this.samples[this.samples.Count - 1].Value);

			// Interpolate.
			TimeSpan span = this.samples[index2].Time - this.samples[index1].Time;
			TimeSpan offset = time - this.samples[index1].Time;
			double t = (double)offset.Ticks / span.Ticks;
			return this.Interpolate(this.samples[index1].Value, this.samples[index2].Value, t);
		}

		private void removeOldSamples()
		{
			int index1 = this.findSample(DateTime.Now - this.Period);
			if (index1 <= 0)
				return;

			this.samples.RemoveRange(0, index1);
		}

		public override string ToString()
		{
			return $"{this.Value} ({this.Rate})";
		}

		#endregion

		#region Nested Types

		[DebuggerDisplay("{Time} {Value}")]
		private struct Sample
		{
			public readonly DateTime Time;
			public readonly TValue Value;

			public Sample(DateTime time, TValue value)
			{
				this.Time = time;
				this.Value = value;
			}
		}

		#endregion
	}
}
