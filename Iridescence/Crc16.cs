﻿using System;
using System.Diagnostics.Contracts;
using System.Runtime.CompilerServices;

namespace Iridescence
{
	/// <summary>
	/// Holds the parameters of a CRC-16 checksum algorithm.
	/// </summary>
	public sealed class Crc16
	{
		#region Fields

		/// <summary>
		/// The polynomial of the CRC-16.
		/// </summary>
		public readonly ushort Polynomial;

		/// <summary>
		/// The initial value of the checksum.
		/// </summary>
		public readonly ushort InitialValue;

		/// <summary>
		/// A XOR mask applied to the checksum on return.
		/// </summary>
		public readonly ushort Mask;

		/// <summary>
		/// The look-up table.
		/// </summary>
		public readonly ushort[] Table;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new CRC-16 hash algorithm using the specified polynomial.
		/// </summary>
		/// <param name="polynomial"></param>
		/// <param name="initialValue"></param>
		/// <param name="mask"></param>
		public Crc16(ushort polynomial, ushort initialValue, ushort mask)
		{
			this.Polynomial = polynomial;
			this.InitialValue = initialValue;
			this.Mask = mask;
			this.Table = new ushort[256];

			for (ushort i = 0; i < 256; i++)
			{
				ushort entry = i;

				for (ushort j = 0; j < 8; j++)
					if ((entry & 1) == 1)
						entry = (ushort)((entry >> 1) ^ polynomial);
					else
						entry = (ushort)(entry >> 1);

				this.Table[i] = entry;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Computes the checksum for the specified byte array.
		/// </summary>
		/// <param name="data">The input data.</param>
		/// <returns>The checksum.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Pure]
		public ushort ComputeChecksum(ReadOnlySpan<byte> data)
		{
			return (ushort)(this.ComputeChecksum(this.InitialValue, data) ^ this.Mask);
		}

		/// <summary>
		/// Computes the checksum for the specified byte array.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The current value.</param>
		/// <param name="data">The input data.</param>
		/// <returns>The checksum.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Pure]
		public ushort ComputeChecksum(ushort current, ReadOnlySpan<byte> data)
		{
			this.ComputeChecksum(ref current, data);
			return current;
		}

		/// <summary>
		/// Computes the checksum for the specified byte array.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The current value.</param>
		/// <param name="data">The input data.</param>
		/// <returns>The checksum.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void ComputeChecksum(ref ushort current, ReadOnlySpan<byte> data)
		{
			for (int i = 0; i < data.Length; i++)
			{
				current = (ushort)((current >> 8) ^ this.Table[(current & 0xFF) ^ data[i]]);
			}
		}

		/// <summary>
		/// Computes the checksum for the specified byte.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The current value.</param>
		/// <param name="data">The input data.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		[Pure]
		public ushort ComputeChecksum(ushort current, byte data)
		{
			return (ushort)((current >> 8) ^ this.Table[(current & 0xFF) ^ data]);
		}

		/// <summary>
		/// Computes the checksum for the specified byte.
		/// Note: This ignores the "Mask" and "InitialValue" properties of the CRC.
		/// </summary>
		/// <param name="current">The current value.</param>
		/// <param name="data">The input data.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void ComputeChecksum(ref ushort current, byte data)
		{
			current = (ushort)((current >> 8) ^ this.Table[(current & 0xFF) ^ data]);
		}
		
		#endregion
	}
}
