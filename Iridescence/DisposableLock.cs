﻿using System;
using System.Threading;

namespace Iridescence
{
	/// <summary>
	/// Represents a disposable lock.
	/// </summary>
	public struct DisposableLock : IDisposable
	{
		#region Fields

		private readonly object obj;
		private int lockTaken;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value that indicates whether the lock was taken.
		/// </summary>
		public bool LockTaken => Volatile.Read(ref this.lockTaken) != 0;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Locks on the specified object.
		/// </summary>
		public DisposableLock(object obj)
		{
			this.obj = obj;

			if (obj == null)
			{
				this.lockTaken = 0;
			}
			else
			{
				bool taken = false;
				Monitor.Enter(obj, ref taken);

				this.lockTaken = taken ? 1 : 0;
			}
		}

		#endregion

		#region Methods

		public void Dispose()
		{
			if (Interlocked.Exchange(ref this.lockTaken, 0) != 0)
			{
				Monitor.Exit(this.obj);
			}
		}

		/// <summary>
		/// Locks on the specified object and returns a <see cref="DisposableLock"/> that needs to be disposed to unlock the object.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static DisposableLock Lock(object obj)
		{
			if (obj == null)
			{
				return default;
			}
			
			return new DisposableLock(obj);
		}

		#endregion
	}
}
