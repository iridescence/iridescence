﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Iridescence.Emit.DynamicIL
{
	/// <summary>
	/// Implements a <see cref="DynamicILInfo"/>-based compiler.
	/// </summary>
	public class DynamicILBuilder : DynamicMethodBuilder
	{
		protected DynamicILBuilder(MethodSignature signature, CallingConventions callConvention, ILBuilderOptions options, DynamicMethod dynamicMethod) 
			: base(signature, callConvention, options, dynamicMethod)
		{
		}
		
		public new static DynamicILBuilder Create(MethodSignature signature, string name = null, ModuleBuilder module = null, ILBuilderOptions options = ILBuilderOptions.Default)
		{
			if (signature == null)
				throw new ArgumentNullException(nameof(signature));

			module ??= DynamicModule.Module;

			if ((options & ILBuilderOptions.AllowUnverifiable) != 0 && !DynamicModule.AllowsUnverifiableCode(module))
				throw new InvalidOperationException("The module does not allow unverifiable code.");

			name ??= $"DynamicMethod-{Guid.NewGuid()}";

			DynamicMethod dynMethod = new DynamicMethod(name, signature.ReturnType, signature.ParameterTypes.ToArray(), module, skipVisibility: true);

			return new DynamicILBuilder(signature, dynMethod.CallingConvention, options, dynMethod);
		}

		public new static DynamicILBuilder Create(MethodSignature sig, Type owner, string name = null, ILBuilderOptions options = ILBuilderOptions.Default)
		{
			if (sig == null)
				throw new ArgumentNullException(nameof(sig));

			if (owner == null)
				return Create(sig, name);

			if ((options & ILBuilderOptions.AllowUnverifiable) != 0 && !DynamicModule.AllowsUnverifiableCode(owner.Module))
				throw new InvalidOperationException("The module does not allow unverifiable code.");

			name ??= $"DynamicMethod-{Guid.NewGuid()}";

			DynamicMethod dynMethod = new DynamicMethod(name, sig.ReturnType, sig.ParameterTypes.ToArray(), owner, skipVisibility: true);

			return new DynamicILBuilder(sig, dynMethod.CallingConvention, options, dynMethod);
		}

		private readonly ref struct ILBuffer
		{
			public readonly DynamicILInfo IL;
			public readonly int CurrentOffset;
			public readonly Span<byte> Buffer;

			public ILBuffer(DynamicILInfo il, int currentOffset, Span<byte> buffer)
			{
				this.IL = il;
				this.CurrentOffset = currentOffset;
				this.Buffer = buffer;
			}

			public ILBuffer Put<T>(T val)
				where T : unmanaged
			{
				int size = Unsafe.SizeOf<T>();
				MemoryMarshal.Write<T>(this.Buffer, ref val);
				return new ILBuffer(this.IL, this.CurrentOffset + size, this.Buffer.Slice(size));
			}
		}

		private static ILBuffer emitInstruction(ILBuffer buffer, OpCode op, object operand, IReadOnlyDictionary<Label, int> labels)
		{
			if(op.Size == 1)
			{
				buffer = buffer.Put((byte)(op.Value));				
			}
			else if (op.Size == 2)
			{
				buffer = buffer.Put((byte)(op.Value >> 8));
				buffer = buffer.Put((byte)(op.Value));
			}
			else
			{
				throw new ArgumentException("Invalid op-code.", nameof(op));
			}

			switch (op.OperandType)
			{
				case OperandType.InlineBrTarget:
				{
					int offset = labels[(Label)operand] - (buffer.CurrentOffset + 4);
					return buffer.Put(offset);
				}

				case OperandType.InlineField:
				{
					int token = buffer.IL.GetTokenFor(((FieldInfo)operand).FieldHandle);
					return buffer.Put(token);
				}

				case OperandType.InlineI:
					return buffer.Put((int)operand);
				
				case OperandType.InlineI8:
					return buffer.Put((long)operand);

				case OperandType.InlineMethod:
				{
					InlineMethod inlineMethod = (InlineMethod)operand;
					int token;
					if (inlineMethod.Method is ConstructorInfo ci)
					{
						token = buffer.IL.GetTokenFor(ci.MethodHandle);
					}
					else
					{
						MethodInfo mi = (MethodInfo)inlineMethod.Method;
						if (inlineMethod.OptionalParameterTypes != null)
						{
							InlineSig sig = inlineMethod.Signature;
							token = buffer.IL.GetTokenFor(sig.GetSignature());							
						}
						else
						{
							token = buffer.IL.GetTokenFor(mi.MethodHandle);
						}
					}
					return buffer.Put(token);
				}

				case OperandType.InlineNone:
					if (operand != null)
						throw new ArgumentException("Supplied operand for op-code that doesn't take one.", nameof(operand));
					return buffer;

				case OperandType.InlineR:
					return buffer.Put((double)operand);

				case OperandType.InlineSig:
				{
					InlineSig sig = (InlineSig)operand;
					byte[] encoded = sig.GetSignature();
					int token = buffer.IL.GetTokenFor(encoded);
					return buffer.Put(token);
				}

				case OperandType.InlineString:
				{
					int token = buffer.IL.GetTokenFor((string)operand);
					return buffer.Put(token);
				}

				case OperandType.InlineSwitch:
				{
					int[] targets = ((IEnumerable<Label>)operand).Select(l => labels[l]).ToArray();
					buffer = buffer.Put(targets.Length);
					int offsetBase = buffer.CurrentOffset + targets.Length * 4;
					foreach(int target in targets)
					{
						buffer = buffer.Put(target - offsetBase);
					}

					return buffer;
				}

				case OperandType.InlineTok:
				{
					int token;
					if (operand is FieldInfo field)
						token = buffer.IL.GetTokenFor(field.FieldHandle);
					else if (operand is MethodInfo method)
						token = buffer.IL.GetTokenFor(method.MethodHandle);
					else if (operand is Type type)
						token = buffer.IL.GetTokenFor(type.TypeHandle);
					else
						throw new ArgumentException("Invalid operand.", nameof(operand));
					return buffer.Put(token);
				}

				case OperandType.InlineType:
				{
					int token = buffer.IL.GetTokenFor(((Type)operand).TypeHandle);
					return buffer.Put(token);
				}

				case OperandType.InlineVar:
				{
					short index;
					if (operand is Local local)
						index = checked((short)local.Index);
					else
						index = (short)operand;

					return buffer.Put(index);
				}

				case OperandType.ShortInlineBrTarget:
				{
					int offset = labels[(Label)operand] - (buffer.CurrentOffset + 1);
					return buffer.Put(checked((byte)offset));
				}

				case OperandType.ShortInlineI:
					return buffer.Put((byte)operand);
					
				case OperandType.ShortInlineR:
					return buffer.Put((float)operand);

				case OperandType.ShortInlineVar:
				{
					byte index;
					if (operand is Local local)
						index = checked((byte)local.Index);
					else
						index = (byte)operand;

					return buffer.Put(index);
				}

				default:
					throw new ArgumentOutOfRangeException(nameof(op));
			}
			
		}

		protected override void Compile(DynamicMethod method)
		{
			DynamicILInfo info = method.GetDynamicILInfo();

			int byteSize = this.Operations.ByteSize;
			byte[] data = new byte[byteSize];

			ILBuffer buffer = new ILBuffer(info, 0, data);

			SignatureHelper helper = SignatureHelper.GetLocalVarSigHelper();

			Dictionary<Label, int> labels = new Dictionary<Label, int>();

			foreach (Operation operation in this.Operations)
			{
				switch (operation)
				{
					case MarkLabelOperation op:
						labels[op.Label] = op.ByteOffset;
						break;

					case DeclareLocalOperation op:
						helper.AddArgument(op.Local.LocalType, op.Local.IsPinned);
						break;
				}
			}

			info.SetLocalSignature(helper.GetSignature());

			foreach (Operation operation in this.Operations)
			{
				switch(operation)
				{
					case CatchBlockStartOperation op:
						//il.BeginCatchBlock(op.ExceptionType);
						break;

					case CatchBlockEndOperation op:
						// No-op.
						break;

					case DeclareLocalOperation op:
						//helper.AddArgument(op.Local.LocalType, op.Local.IsPinned);
						break;

					case DefineLabelOperation op:
						//labels.Add(op.Label, il.DefineLabel());
						break;

					case DisposeLocalOperation op:
						// No-op.
						break;

					case ExceptionBlockStartOperation op:
						//labels.Add(op.Label, il.BeginExceptionBlock());
						break;

					case ExceptionBlockEndOperation op:
						//il.EndExceptionBlock();
						break;

					case FaultBlockStartOperation op:
						//il.BeginFaultBlock();
						break;

					case FaultBlockEndOperation op:
						// No-op.
						break;

					case FilterBlockStartOperation op:
						//il.BeginExceptFilterBlock();
						break;

					case FilterBlockEndOperation op:
						// No-op.
						break;

					case FinallyBlockStartOperation op:
						//il.BeginFinallyBlock();
						break;

					case FinallyBlockEndOperation op:
						// No-op.
						break;

					case Instruction op:
						Debug.Assert(buffer.CurrentOffset == op.ByteOffset);
						buffer = emitInstruction(buffer, op.OpCode, op.Operand, labels);
						break;

					case MarkLabelOperation op:
						//il.MarkLabel(labels[op.Label]);
						break;

					case ReuseLocalOperation op:
						break;
					
					default:
						throw new NotSupportedException($"Unsupported operation \"{operation.GetType()}\".");
				}
			}

			Debug.Assert(buffer.CurrentOffset == data.Length);
			info.SetCode(data, 10);
		}
	}
}
