﻿using System;
using System.Runtime.InteropServices;
using Iridescence.Audio.CoreAudio.Constants;
using Iridescence.Audio.CoreAudio.Enumerations;
using Iridescence.Audio.CoreAudio.Externals;
using Iridescence.Audio.CoreAudio.Interfaces;

namespace Iridescence.Audio.CoreAudio
{
	/// <summary>
	/// Represents an abstract CoreAudio device.
	/// </summary>
	public sealed class MMDevice
	{
		#region Fields

		private readonly IMMDevice device;
		private ConcurrentFlag isDisposed;

		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the data flow direction of this device.
		/// </summary>
		public EndPointDataFlow DataFlow
		{
			get
			{
				IMMEndpoint ep = (IMMEndpoint)this.device;
				Marshal.ThrowExceptionForHR(ep.GetDataFlow(out EndPointDataFlow flow));
				return flow;
			}
		}

		/// <summary>
		/// Gets the ID of the device.
		/// </summary>
		public string ID
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.device.GetId(out string id));
				return id;
			}
		}

		/// <summary>
		/// Gets the current state of the device.
		/// </summary>
		public DeviceState State
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.device.GetState(out DeviceState state));
				return state;
			}
		}

		/// <summary>
		/// Gets the device properties.
		/// </summary>
		public PropertyStore Properties
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.device.OpenPropertyStore(STGM.STGM_READ, out IPropertyStore properties));
				return new PropertyStore(properties);
			}
		}

		/// <summary>
		/// Gets the friendly name of the device.
		/// </summary>
		public string FriendlyName
		{
			get
			{
				using (PropertyStore props = this.Properties)
				{
					if (!props.TryGetValue(new PropertyKey(PropertyKeys.PKEY_Device_FriendlyName, 2), out PropertyVariant value))
						return null;
					return value.GetAsString();
				}
			}
		}

		/// <summary>
		/// Gets the friendly name of the device interface.
		/// </summary>
		public string InterfaceFriendlyName
		{
			get
			{
				using (PropertyStore props = this.Properties)
				{
					if (!props.TryGetValue(new PropertyKey(PropertyKeys.PKEY_DeviceInterface_FriendlyName, 2), out PropertyVariant value))
						return null;
					return value.GetAsString();
				}
			}
		}

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MMDevice"/> class. 
		/// </summary>
		internal MMDevice(IMMDevice device)
		{
			this.device = device;
		}

		~MMDevice()
		{
			this.Dispose();
		}

		#endregion
		
		#region Methods

		public AudioClient Activate()
		{
			Marshal.ThrowExceptionForHR(this.device.Activate(
				new Guid(ComIIDs.IAudioClientIID),
				(uint)CLSCTX.CLSCTX_INPROC_HANDLER,
				IntPtr.Zero,
				out object instance));

			IAudioClient client = (IAudioClient)instance;
			return new AudioClient(client);
		}

		public void Dispose()
		{
			if (!this.isDisposed.TrySet())
				return;

			Marshal.ReleaseComObject(this.device);

			GC.SuppressFinalize(this);
		}

		public override string ToString()
		{
			return this.FriendlyName;
		}

		#endregion
	}
}
