﻿using System;
using System.Runtime.InteropServices;
using Iridescence.Audio.CoreAudio.Enumerations;
using Iridescence.Audio.CoreAudio.Interfaces;

namespace Iridescence.Audio.CoreAudio
{
	/// <summary>
	/// Represents the audio render client service.
	/// </summary>
	public sealed class AudioRenderClient : IDisposable
	{
		#region Fields

		private readonly IAudioRenderClient renderClient;
		private ConcurrentFlag isDisposed;
		
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="AudioRenderClient"/> class. 
		/// </summary>
		internal AudioRenderClient(IAudioRenderClient renderClient)
		{
			this.renderClient = renderClient;
		}

		~AudioRenderClient()
		{
			this.Dispose();
		}
		
		#endregion
		
		#region Methods

		public IntPtr GetBuffer(int frameCount)
		{
			Marshal.ThrowExceptionForHR(this.renderClient.GetBuffer(checked((uint)frameCount), out IntPtr dataPointer));
			return dataPointer;
		}

		public void ReleaseBuffer(int frameCount, AudioClientBufferFlags flags)
		{
			Marshal.ThrowExceptionForHR(this.renderClient.ReleaseBuffer(checked((uint)frameCount), flags));
		}

		public void Dispose()
		{
			if (!this.isDisposed.TrySet())
				return;

			Marshal.ReleaseComObject(this.renderClient);

			GC.SuppressFinalize(this);
		}
		
		#endregion
	}
}
