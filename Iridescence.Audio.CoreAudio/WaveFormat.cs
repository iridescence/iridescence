﻿using System.Runtime.InteropServices;
using Iridescence.Audio.CoreAudio.Enumerations;

namespace Iridescence.Audio.CoreAudio.Structures
{
	/// <summary>
	/// Stores the video port identifier.
	/// </summary>
	/// <remarks>
	/// MSDN Reference: http://msdn.microsoft.com/en-us/library/windows/desktop/dd390970
	/// </remarks>
	[StructLayout(LayoutKind.Sequential)]
	public struct WaveFormat
	{
		public WaveFormatTag Tag;
		public short Channels;
		public int SamplesPerSec;
		public int AvgBytesPerSec;
		public short BlockAlign;
		public short BitsPerSample;
		public short ExtraSize;

		public override string ToString()
		{
			return $"{this.Channels} channels, {this.SamplesPerSec} Hz, {this.BitsPerSample} bits";
		}

		public static WaveFormat Pcm(int samplesPerSec, int bitsPerSample, int channels)
		{
			return new WaveFormat
			{
				Tag = WaveFormatTag.Pcm,
				Channels = checked((short)channels),
				SamplesPerSec = samplesPerSec,
				AvgBytesPerSec = samplesPerSec * bitsPerSample * channels / 8,
				BlockAlign = checked((short)(bitsPerSample * channels / 8)),
				BitsPerSample = checked((short)bitsPerSample),
				ExtraSize = 0
			};
		}
	}
}