﻿// -----------------------------------------
// SoundScribe (TM) and related software.
// 
// Copyright (C) 2007-2011 Vannatech
// http://www.vannatech.com
// All rights reserved.
// 
// This source code is subject to the MIT License.
// http://www.opensource.org/licenses/mit-license.php
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------

using System;
using System.Runtime.InteropServices;

namespace Iridescence.Audio.CoreAudio.Externals
{
	/// <summary>
	/// Specifies the FMTID/PID identifier that programmatically identifies a property.
	/// </summary>
	/// <remarks>
	/// MSDN Reference: http://msdn.microsoft.com/en-us/library/bb773381.aspx
	/// Note: This item is external to CoreAudio API, and is defined in the Windows Property System API.
	/// </remarks>
	[StructLayout(LayoutKind.Sequential)]
	public struct PropertyKey : IEquatable<PropertyKey>
	{
		#region Fields

		/// <summary>
		/// A unique GUID for the property.
		/// </summary>
		public Guid ID;

		/// <summary>
		/// A property identifier (PID).
		/// </summary>
		public int PropertyID;

		#endregion

		#region Constructors

		public PropertyKey(Guid id, int propertyID)
		{
			this.ID = id;
			this.PropertyID = propertyID;
		}

		#endregion

		#region Methods

		public bool Equals(PropertyKey other)
		{
			return this.ID.Equals(other.ID) && this.PropertyID == other.PropertyID;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is PropertyKey key && this.Equals(key);
		}

		public override int GetHashCode()
		{
			return this.ID.GetHashCode() ^ this.PropertyID;
		}

		public static bool operator ==(PropertyKey left, PropertyKey right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(PropertyKey left, PropertyKey right)
		{
			return !left.Equals(right);
		}
		
		#endregion
	}
}
