﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Iridescence.Audio.CoreAudio.Interfaces;

namespace Iridescence.Audio.CoreAudio
{
	/// <summary>
	/// Represents a collection of <see cref="MMDevice"/>.
	/// </summary>
	public sealed class MMDeviceCollection : IReadOnlyList<MMDevice>
	{
		#region Fields

		private readonly IMMDeviceCollection collection;

		#endregion
		
		#region Properties

		public int Count
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.collection.GetCount(out uint count));
				return checked((int)count);
			}
		}

		public MMDevice this[int index]
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.collection.Item(checked((uint)index), out IMMDevice dev));
				return new MMDevice(dev);
			}
		}

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MMDeviceCollection"/> class. 
		/// </summary>
		internal MMDeviceCollection(IMMDeviceCollection collection)
		{
			this.collection = collection;
		}
		
		#endregion
		
		#region Methods
		
		public IEnumerator<MMDevice> GetEnumerator()
		{
			for (int i = 0; i < this.Count; ++i)
			{
				yield return this[i];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		#endregion
	}
}
