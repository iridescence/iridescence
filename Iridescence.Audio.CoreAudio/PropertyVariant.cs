﻿// -----------------------------------------
// SoundScribe (TM) and related software.
// 
// Copyright (C) 2007-2011 Vannatech
// http://www.vannatech.com
// All rights reserved.
// 
// This source code is subject to the MIT License.
// http://www.opensource.org/licenses/mit-license.php
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
// -----------------------------------------

using System;
using System.Runtime.InteropServices;

namespace Iridescence.Audio.CoreAudio.Externals
{
	/// <summary>
	/// Used by the <see cref="IPropertyStore.GetValue"/> and <see cref="IPropertyStore.SetValue"/> methods
	/// of <see cref="IPropertyStore"/> as the primary way to program item properties.
	/// </summary>
	/// <remarks>
	/// MSDN Reference: http://msdn.microsoft.com/en-us/library/aa380072.aspx
	/// Note: This item is external to CoreAudio API, and is defined in the Windows Structured Storage API.
	/// </remarks>
	[StructLayout(LayoutKind.Explicit)]
	public struct PropertyVariant
	{
		/// <summary>
		/// Value type tag.
		/// </summary>
		[FieldOffset(0)] public short vt;

		/// <summary>
		/// Reserved for future use.
		/// </summary>
		[FieldOffset(2)] public short wReserved1;

		/// <summary>
		/// Reserved for future use.
		/// </summary>
		[FieldOffset(4)] public short wReserved2;

		/// <summary>
		/// Reserved for future use.
		/// </summary>
		[FieldOffset(6)] public short wReserved3;

		[FieldOffset(8)] public IntPtr IntPtr;
		[FieldOffset(8)] public bool Boolean;
		[FieldOffset(8)] public short Int16;
		[FieldOffset(8)] public ushort UInt16;
		[FieldOffset(8)] public int Int32;
		[FieldOffset(8)] public uint UInt32;
		[FieldOffset(8)] public long Int64;
		[FieldOffset(8)] public ulong UInt64;
		[FieldOffset(8)] public float Single;
		[FieldOffset(8)] public double Double;
		
		public string GetAsString()
		{
			return Marshal.PtrToStringAuto(this.IntPtr);
		}
	}
}
