﻿using System;
using System.Runtime.InteropServices;
using Iridescence.Audio.CoreAudio.Constants;
using Iridescence.Audio.CoreAudio.Enumerations;
using Iridescence.Audio.CoreAudio.Interfaces;

namespace Iridescence.Audio.CoreAudio
{
	/// <summary>
	/// Implements CoreAudio device enumeration.
	/// </summary>
	public sealed class MMDeviceEnumerator : IDisposable
	{
		#region Fields
		
		internal const int REFTIMES_PER_SEC = 10000000;
		internal const int REFTIMES_PER_MILLISEC = 10000;

		private readonly IMMDeviceEnumerator deviceEnumerator;

		private ConcurrentFlag isDisposed;

		#endregion
		
		#region Properties

		public MMDeviceCollection Devices
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.deviceEnumerator.EnumAudioEndpoints(Enumerations.EndPointDataFlow.All, DeviceState.All, out IMMDeviceCollection collection));
				return new MMDeviceCollection(collection);
			}
		}

		public MMDeviceCollection RenderDevices
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.deviceEnumerator.EnumAudioEndpoints(Enumerations.EndPointDataFlow.Render, DeviceState.All, out IMMDeviceCollection collection));
				return new MMDeviceCollection(collection);
			}
		}
		
		public MMDeviceCollection CaptureDevices
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.deviceEnumerator.EnumAudioEndpoints(Enumerations.EndPointDataFlow.Capture, DeviceState.All, out IMMDeviceCollection collection));
				return new MMDeviceCollection(collection);
			}
		}

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MMDeviceEnumerator"/> class. 
		/// </summary>
		public MMDeviceEnumerator()
		{
			var deviceEnumeratorType = Type.GetTypeFromCLSID(new Guid(ComCLSIDs.MMDeviceEnumeratorCLSID));
			this.deviceEnumerator = (IMMDeviceEnumerator)Activator.CreateInstance(deviceEnumeratorType);
		}

		~MMDeviceEnumerator()
		{
			this.Dispose();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the default device for the specified data flow and role.
		/// </summary>
		/// <param name="dataFlow"></param>
		/// <param name="role"></param>
		/// <returns></returns>
		public MMDevice GetDefaultDevice(EndPointDataFlow dataFlow, EndPointRole role)
		{
			Marshal.ThrowExceptionForHR(this.deviceEnumerator.GetDefaultAudioEndpoint(dataFlow, role, out IMMDevice dev));
			return new MMDevice(dev);
		}

		public void Dispose()
		{
			if (!this.isDisposed.TrySet())
				return;

			Marshal.ReleaseComObject(this.deviceEnumerator);

			GC.SuppressFinalize(this);
		}

		#endregion
	}
}
