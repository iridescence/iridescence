﻿using System;
using System.Runtime.InteropServices;
using Iridescence.Audio.CoreAudio.Constants;
using Iridescence.Audio.CoreAudio.Enumerations;
using Iridescence.Audio.CoreAudio.Interfaces;
using Iridescence.Audio.CoreAudio.Structures;

namespace Iridescence.Audio.CoreAudio
{
	/// <summary>
	/// Represents a CoreAudio audio client.
	/// </summary>
	public sealed class AudioClient : IDisposable
	{
		#region Fields

		private readonly IAudioClient client;
		private ConcurrentFlag isDisposed;
		
		#endregion
		
		#region Properties

		public int BufferSize
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.client.GetBufferSize(out uint bufferSize));
				return checked((int)bufferSize);
			}
		}

		public WaveFormat MixFormat
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.client.GetMixFormat(out IntPtr formatPtr));
				WaveFormat format = Marshal.PtrToStructure<WaveFormat>(formatPtr);
				Marshal.FreeCoTaskMem(formatPtr);
				return format;
			}
		}

		public int CurrentPadding
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.client.GetCurrentPadding(out uint frameCount));
				return checked((int)frameCount);
			}
		}

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="AudioClient"/> class. 
		/// </summary>
		internal AudioClient(IAudioClient client)
		{
			this.client = client;
		}
		
		#endregion
		
		#region Methods

		public bool IsFormatSupported(AudioClientShareMode shareMode, WaveFormat format, out WaveFormat? closestMatch)
		{
			IntPtr formatPtr = Marshal.AllocCoTaskMem(Marshal.SizeOf<WaveFormat>());
			try
			{
				Marshal.StructureToPtr(format, formatPtr, false);
				int res = this.client.IsFormatSupported(shareMode, formatPtr, out IntPtr closestMatchPtr);

				try
				{
					closestMatch = null;
					if (closestMatchPtr != IntPtr.Zero)
					{
						closestMatch = Marshal.PtrToStructure<WaveFormat>(closestMatchPtr);
					}

					switch ((uint)res)
					{
						case 0: // S_OK
							return true;

						case 1: // S_FALSE;
							return false;

						case 0x88890008u: // AUDCLNT_E_UNSUPPORTED_FORMAT
							return false;

						default:
							Marshal.ThrowExceptionForHR(res);
							throw new InvalidOperationException();
					}
				}
				finally
				{
					if (closestMatchPtr != IntPtr.Zero)
						Marshal.FreeCoTaskMem(closestMatchPtr);
				}
			}
			finally
			{
				Marshal.FreeCoTaskMem(formatPtr);
			}
		}

		public void Initialize(AudioClientShareMode shareMode, AudioClientStreamFlags streamFlags, long bufferDuration, long devicePeriod, WaveFormat waveFormat, Guid audioSessionGuid)
		{
			IntPtr p = Marshal.AllocCoTaskMem(Marshal.SizeOf<WaveFormat>());
			try
			{
				Marshal.StructureToPtr(waveFormat, p, false);
				Marshal.ThrowExceptionForHR(this.client.Initialize(shareMode, streamFlags, (ulong)bufferDuration, (ulong)devicePeriod, p, audioSessionGuid));
			}
			finally
			{
				Marshal.FreeCoTaskMem(p);
			}
		}

		public void Initialize(AudioClientShareMode shareMode, AudioClientStreamFlags streamFlags, long bufferDuration, long devicePeriod, WaveFormat waveFormat)
		{
			this.Initialize(shareMode, streamFlags, bufferDuration, devicePeriod, waveFormat, Guid.Empty);
		}

		public void Initialize(AudioClientShareMode shareMode, AudioClientStreamFlags streamFlags, TimeSpan bufferDuration, TimeSpan devicePeriod, WaveFormat waveFormat, Guid audioSessionGuid)
		{
			this.Initialize(shareMode, streamFlags, bufferDuration.Ticks, devicePeriod.Ticks, waveFormat, audioSessionGuid);
		}

		public void Initialize(AudioClientShareMode shareMode, AudioClientStreamFlags streamFlags, TimeSpan bufferDuration, TimeSpan devicePeriod, WaveFormat waveFormat)
		{
			this.Initialize(shareMode, streamFlags, bufferDuration, devicePeriod, waveFormat, Guid.Empty);
		}

		public void Start()
		{
			Marshal.ThrowExceptionForHR(this.client.Start());
		}

		public void Stop()
		{
			Marshal.ThrowExceptionForHR(this.client.Stop());
		}

		public AudioRenderClient GetAudioRenderClient()
		{
			Marshal.ThrowExceptionForHR(this.client.GetService(new Guid(ComIIDs.IAudioRenderClientIID), out object instance));
			IAudioRenderClient renderClient = (IAudioRenderClient)instance;
			return new AudioRenderClient(renderClient);
		}

		public void Dispose()
		{
			if (!this.isDisposed.TrySet())
				return;

			Marshal.ReleaseComObject(this.client);

			GC.SuppressFinalize(this);
		}
		
		#endregion
	}
}
