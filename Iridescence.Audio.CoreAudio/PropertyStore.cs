﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Iridescence.Audio.CoreAudio.Externals;

namespace Iridescence.Audio.CoreAudio
{
	/// <summary>
	/// Represents a property store.
	/// </summary>
	public sealed class PropertyStore : IDisposable, IReadOnlyDictionary<PropertyKey, PropertyVariant>
	{
		#region Fields

		private readonly IPropertyStore propertyStore;
		private ConcurrentFlag isDisposed;

		#endregion

		#region Properties

		public int Count
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.propertyStore.GetCount(out uint count));
				return checked((int)count);
			}
		}

		public PropertyVariant this[PropertyKey key]
		{
			get
			{
				Marshal.ThrowExceptionForHR(this.propertyStore.GetValue(ref key, out PropertyVariant value));
				return value;
			}
		}

		public IEnumerable<PropertyKey> Keys
		{
			get
			{
				int n = this.Count;
				for (int i = 0; i < n; ++i)
				{
					yield return this.GetKeyAt(i);
				}
			}
		}

		public IEnumerable<PropertyVariant> Values
		{
			get
			{
				int n = this.Count;
				for (int i = 0; i < n; ++i)
				{
					yield return this[this.GetKeyAt(i)];
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PropertyStore"/> class. 
		/// </summary>
		internal PropertyStore(IPropertyStore propertyStore)
		{
			this.propertyStore = propertyStore;
			this.isDisposed = new ConcurrentFlag();
		}

		~PropertyStore()
		{
			this.Dispose();
		}

		#endregion
		
		#region Methods

		public bool ContainsKey(PropertyKey key)
		{
			return this.Keys.Any(key.Equals);
		}

		public bool TryGetValue(PropertyKey key, out PropertyVariant value)
		{
			value = default;
			if (!this.ContainsKey(key))
				return false;

			value = this[key];
			return true;
		}

		public PropertyKey GetKeyAt(int index)
		{
			Marshal.ThrowExceptionForHR(this.propertyStore.GetAt(checked((uint)index), out PropertyKey key));
			return key;
		}

		public void Dispose()
		{
			if (!this.isDisposed.TrySet())
				return;

			Marshal.ReleaseComObject(this.propertyStore);
		}
		
		public IEnumerator<KeyValuePair<PropertyKey, PropertyVariant>> GetEnumerator()
		{
			int n = this.Count;
			for (int i = 0; i < n; ++i)
			{
				PropertyKey k = this.GetKeyAt(i);
				PropertyVariant v = this[k];
				yield return new KeyValuePair<PropertyKey, PropertyVariant>(k, v);
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
		
		#endregion
	}
}
