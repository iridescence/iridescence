﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace Iridescence.Networking
{
	/// <summary>
	/// Represents an item in the object heap.
	/// </summary>
	/// <remarks>
	/// This class is used to keep the contained object alive until the heap determines it is safe to delete.
	/// It reports all finalize attempts by the .NET GC to the heap, which can then be used to notify a remote peer
	/// of the fact that the object has no local references anymore. Once the remote peer acknowledges this by sending its
	/// finalization notification to the local peer, the object is seen as safe to delete.
	/// </remarks>
	public sealed class HeapItem<TIdentifier>
	{
		#region Fields

		private const int reportThreshold = 1_000_000;
		
		private volatile object obj;
		private volatile int unacknowledgedUses;
		private volatile int unreportedUses;

		internal ConcurrentFlag IsRemoved;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the heap.
		/// </summary>
		public Heap<TIdentifier> Heap { get; }

		/// <summary>
		/// Gets the identifier of the object.
		/// </summary>
		public TIdentifier Identifier { get; }

		/// <summary>
		/// Gets the object.
		/// </summary>
		public object Object => this.obj;

		/// <summary>
		/// Gets the number of unacknowledged uses of this item.
		/// </summary>
		public int UnacknowledgedUses => this.unacknowledgedUses;

		/// <summary>
		/// Gets a value indicating whether this item is disposed.
		/// </summary>
		public bool IsDisposed => this.obj == null;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HeapItem.
		/// </summary>
		internal HeapItem(Heap<TIdentifier> heap, TIdentifier identifier, object obj)
		{
			Debug.Assert(!(obj is ValueType));

			this.Heap = heap;
			this.Identifier = identifier;
			this.obj = obj;
			this.IsRemoved = new ConcurrentFlag();

			// Set uses to 1 so it does not report as invalid right after initialization.
			this.unacknowledgedUses = 1;
			this.unreportedUses = 1;
			
			// Don't allow the object to be finalized.
			GC.SuppressFinalize(obj);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Reports unreported uses of the object to the heap.
		/// </summary>
		/// <param name="obj">The object of the item.</param>
		/// <param name="keep">The number of unreported uses to keep in the item.</param>
		/// <param name="requestRemoval">Specifies whether the item should be removed from the heap.</param>
		/// <returns>The number of uses reported.</returns>
		private int reportUses(object obj, int keep, bool requestRemoval)
		{
			for (;;)
			{
				int oldValue = this.unreportedUses;
				int uses = oldValue - keep;
				if (uses <= 0)
				{
					if (requestRemoval)
						this.Heap.ReportUses(this, 0, obj, true, requestRemoval);
					return 0;
				}

				if (oldValue == Interlocked.CompareExchange(ref this.unreportedUses, keep, oldValue))
				{
					this.Heap.ReportUses(this, uses, obj, keep == 0, requestRemoval);
					return uses;
				}
			}
		}
		
		/// <summary>
		/// Tries to retreive the object of the heap item, and if it succeeds, increments the use counter, all as an atomic operation.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns>True, if the object has been retreived. False otherwise.</returns>
		public bool TryGetObject(out object obj)
		{
			// First, get the object.
			obj = this.obj;
			if (obj == null)
			{
				// If the object is null the heap item was already disposed and we've got nothing to do.
				return false;
			}
			
			for(;;)
			{
				int oldValue = this.unacknowledgedUses;
				if (oldValue == 0)
				{
					// The heap item is inaccessible and should be disposed.
					obj = null;
					return false;
				}
				
				// Try incrementing the use counter.
				int newValue = oldValue + 1;
				if (oldValue == Interlocked.CompareExchange(ref this.unacknowledgedUses, newValue, oldValue))
				{
					// Increment the unreported uses for the finalizer usage report.
					if (Interlocked.Increment(ref this.unreportedUses) >= reportThreshold)
					{
						// Reduce number of uses every now and then.
						// We only need the use counter to be != 0, so we could theoretically report every
						// unreported use > 1, but this would lead to spam and a lot of bandwidth getting
						// wasted when networking these reports.
						this.reportUses(obj, 1, false);
					}

					// Remove the strong reference to this item so it can be collected.
					this.Heap.RemoveStrongReference(this);
					
					return true;
				}
			}
		}

		/// <summary>
		/// Acknowledges a number of uses and retreives the object as an atomic operation.
		/// </summary>
		/// <param name="numUses"></param>
		/// <returns>False, if the object became invalid. True otherwise.</returns>
		internal bool AcknowledgeUses(int numUses, out object obj)
		{
			// Retreive the object.
			obj = this.obj;
			if (obj == null)
			{
				// The item must not be disposed to acknowledge uses.
				throw new InvalidOperationException("Heap item is disposed.");
			}

			for (;;)
			{
				int oldValue = this.unacknowledgedUses;
				if (oldValue < numUses)
				{
					throw new ArgumentOutOfRangeException(nameof(numUses));
				}

				// Try subtracting the specified number of uses.
				int newValue = oldValue - numUses;
				if (oldValue == Interlocked.CompareExchange(ref this.unacknowledgedUses, newValue, oldValue))
				{
					if (newValue == 0)
					{
						// The heap item became inaccessible, dispose it.
						this.Dispose();

						// Report unreported uses and also schedule the item for removal.
						this.reportUses(obj, 0, true);
						
						return false;
					}
					
					return true;
				}
			}
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();

			if (this.IsDisposed)
				str.Append("Disposed ");

			str.Append("Heap=");
			str.Append(this.Heap);
			str.Append(' ');
			
			str.Append("Id=");
			str.Append(this.Identifier);
			str.Append(' ');
			
			str.Append("Uses=");
			str.Append(this.unacknowledgedUses);
			str.Append(' ');

			object obj = this.obj;
			if (obj == null)
			{
				str.Append("NULL");
			}
			else
			{
				str.Append("Type=");
				str.Append(obj.GetType().Name);
				str.Append(' ');
			
				str.Append("Value='");
				str.Append(obj);
				str.Append("' ");
			}
			
			return str.ToString();
		}

		/// <summary>
		/// Disposes the heap item and releases the contained object.
		/// </summary>
		internal void Dispose()
		{
			// Set object to null so it's not kept alive by the HeapItem anymore.
			object obj = Interlocked.Exchange(ref this.obj, null);
			if (obj != null)
			{
				// Free strong reference to the heap item.
				this.Heap.RemoveStrongReference(this);

				// Don't call our finalizer again.
				GC.SuppressFinalize(this);

				// Allow our object to be finalized.
				GC.ReRegisterForFinalize(obj);
			}
		}

		/// <summary>
		/// Destructor.
		/// </summary>
		~HeapItem()
		{
			object obj = this.obj;
			if (obj == null)
				return;

			HeapTrace.Instance.TraceItem(HeapTraceAction.Finalize, this);

			if (this.unacknowledgedUses != 0)
			{
				// Make the object stay.
				// This is done to prevent the object from getting finalized repeatedly because it has no paths to root.
				// The handle is freed once either the object is used again (which means it has paths to root again), or all
				// uses were acknowledged (which means the other end does not require the object anymore and it can be collected).
				this.Heap.AddStrongReference(this);

				// Re-register finalizer.
				GC.ReRegisterForFinalize(this);
			}

			// Report to heap.
			this.reportUses(obj, 0, false);
		}

		#endregion
	}
}
