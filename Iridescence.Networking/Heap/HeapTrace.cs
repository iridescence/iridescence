﻿using System;
using System.Diagnostics.Tracing;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Diagnostics;

namespace Iridescence.Networking
{
	/// <summary>
	/// Provides heap trace events.
	/// </summary>
	[EventSource(Name = "Iridescence-Networking-HeapTrace")]
	internal sealed class HeapTrace : EventSource
	{
		#region Fields

		private int nextObjectID;
		private readonly ConditionalWeakTable<object, StrongBox<int>> objectIDs;

		public static readonly HeapTrace Instance = new HeapTrace();

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="HeapTrace"/> class. 
		/// </summary>
		public HeapTrace()
		{
			this.nextObjectID = 0;
			this.objectIDs = new ConditionalWeakTable<object, StrongBox<int>>();
		}

		#endregion

		#region Methods

		[NonEvent]
		private int getObjectID(object obj)
		{
			if (ReferenceEquals(obj, null))
				return 0;

			return this.objectIDs.GetValue(obj, _ => new StrongBox<int>(Interlocked.Increment(ref this.nextObjectID))).Value;
		}

		[Event(1)]
		[Conditional("DEBUG")]
		public void Create(int heapID, string typeName)
		{
			this.WriteEvent(1, heapID, typeName);
		}

		[Event(2)]
		[Conditional("DEBUG")]
		public void Dispose(int heapID, bool managed)
		{
			this.WriteEvent(2, heapID, managed);
		}

		[Event(3)]
		[Conditional("DEBUG")]
		public unsafe void TraceItem(int heapID, int action, int objID, int useCount, string identifier, string typeName, string objName)
		{
			if (!this.IsEnabled())
				return;

			fixed (char* identifierPtr = identifier)
			fixed (char* typeNamePtr = typeName)
			fixed (char* objNamePtr = objName)
			{
				EventData* data = stackalloc EventData[7];

				data[0].DataPointer = (IntPtr)(&heapID);
				data[0].Size = 4;

				data[1].DataPointer = (IntPtr)(&action);
				data[1].Size = 4;

				data[2].DataPointer = (IntPtr)(&objID);
				data[2].Size = 4;

				data[3].DataPointer = (IntPtr)(&useCount);
				data[3].Size = 4;

				data[4].DataPointer = (IntPtr)identifierPtr;
				data[4].Size = 2 * (identifier.Length + 1);

				data[5].DataPointer = (IntPtr)typeNamePtr;
				data[5].Size = 2 * (typeName.Length + 1);

				data[6].DataPointer = (IntPtr)objNamePtr;
				data[6].Size = 2 * (objName.Length + 1);

				this.WriteEventCore(3, 7, data);
			}
		}

		[NonEvent]
		[Conditional("DEBUG")]
		public void TraceItem<TIdentifier>(HeapTraceAction action, HeapItem<TIdentifier> item)
		{
			if (!this.IsEnabled())
				return;

			object obj = item.Object;
			int objID = this.getObjectID(obj);
			string identifier = item.Identifier?.ToString() ?? string.Empty;
			string typeName = obj?.GetType().FullName ?? string.Empty;
			string objName = obj?.ToString() ?? string.Empty;
			this.TraceItem(item.Heap.HeapID, (int)action, objID, item.UnacknowledgedUses, identifier, typeName, objName);
		}


		[Event(5)]
		[Conditional("DEBUG")]
		public unsafe void Error(int heapID, string message, int objID, int useCount, string identifier, string typeName, string objName)
		{
			if (!this.IsEnabled())
				return;

			fixed (char* messagePtr = message)
			fixed (char* identifierPtr = identifier)
			fixed (char* typeNamePtr = typeName)
			fixed (char* objNamePtr = objName)
			{
				EventData* data = stackalloc EventData[7];

				data[0].DataPointer = (IntPtr)(&heapID);
				data[0].Size = 4;

				data[1].DataPointer = (IntPtr)messagePtr;
				data[1].Size = 2 * (message.Length + 1);

				data[2].DataPointer = (IntPtr)(&objID);
				data[2].Size = 4;

				data[3].DataPointer = (IntPtr)(&useCount);
				data[3].Size = 4;

				data[4].DataPointer = (IntPtr)identifierPtr;
				data[4].Size = 2 * (identifier.Length + 1);

				data[5].DataPointer = (IntPtr)typeNamePtr;
				data[5].Size = 2 * (typeName.Length + 1);

				data[6].DataPointer = (IntPtr)objNamePtr;
				data[6].Size = 2 * (objName.Length + 1);

				this.WriteEventCore(5, 7, data);
			}
		}

		[NonEvent]
		[Conditional("DEBUG")]
		public void Error<TIdentifier>(string message, HeapItem<TIdentifier> item)
		{
			if (!this.IsEnabled())
				return;
			
			object obj = item.Object;
			int objID = this.getObjectID(obj);
			string identifier = item.Identifier?.ToString() ?? string.Empty;
			string typeName = obj?.GetType().FullName ?? string.Empty;
			string objName = obj?.ToString() ?? string.Empty;
			this.Error(item.Heap.HeapID, message, objID, item.UnacknowledgedUses, identifier, typeName, objName);
		}

		#endregion
	}
}
