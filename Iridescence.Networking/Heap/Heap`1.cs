﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Represents an object heap for networking.
	/// </summary>
	public abstract class Heap<TIdentifier> : Heap, IEnumerable<HeapItem<TIdentifier>>, IReferenceProvider<object, TIdentifier>, IReferenceResolver<object, TIdentifier>
	{
		#region Nested Types

		/// <summary>
		/// Info structure for garbage collection.
		/// </summary>
		private struct UseInfo
		{
			/// <summary>
			/// The item that was collected.
			/// </summary>
			public HeapItem<TIdentifier> Item;

			/// <summary>
			/// Determines whether the heap item should be removed once this garbage info is retreived in a call to <see cref="Heap{TIdentifier}.GetUsage"/>.
			/// </summary>
			public bool Remove;

			/// <summary>
			/// The number of uses.
			/// </summary>
			public int Uses;
		}

		#endregion

		#region Events

		public delegate void InvalidationHandler(IEnumerable<KeyValuePair<TIdentifier, object>> items);

		/// <summary>
		/// Occurs when heap items in the become invalid.
		/// </summary>
		public event InvalidationHandler ItemsInvalidated;

		#endregion

		#region Fields

		private ConditionalWeakTable<object, HeapItem<TIdentifier>> objectToHeapItem;
		private readonly ConcurrentDictionary<TIdentifier, WeakReference<HeapItem<TIdentifier>>> identifierToHeapItem;
		private readonly ConcurrentDictionary<TIdentifier, UseInfo> useInfo;


#if HEAPPERF
		private readonly HeapPerformanceCounters<TIdentifier> perfCounters;
		internal long CollectedObjects;
#endif

		#endregion

		#region Properties

		/// <inheritdoc />
		public override int Count => this.identifierToHeapItem.Count;
		
		/// <summary>
		/// Gets the object identifier that represents a null reference.
		/// </summary>
		protected abstract TIdentifier NullIdentifier { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="Heap{TIdentifier}"/>.
		/// </summary>
		protected Heap()
		{
			this.objectToHeapItem = new ConditionalWeakTable<object, HeapItem<TIdentifier>>();
			this.identifierToHeapItem = new ConcurrentDictionary<TIdentifier, WeakReference<HeapItem<TIdentifier>>>();
			this.useInfo = new ConcurrentDictionary<TIdentifier, UseInfo>();


#if HEAPPERF
			this.perfCounters = new HeapPerformanceCounters<TIdentifier>(this);
#endif
		}

		#endregion

		#region Methods

		private void throwIfDisposed()
		{
			if (this.IsDisposed)
				throw new ObjectDisposedException(this.ToString());
		}

		/// <summary>
		/// Creates a new, unused identifier for the specified object.
		/// </summary>
		protected abstract TIdentifier CreateIdentifier(object obj);

		/// <summary>
		/// Removes the specified identifier.
		/// </summary>
		/// <param name="id"></param>
		protected abstract void RemoveIdentifier(TIdentifier id);

		/// <summary>
		/// Returns the heap item with the specified ID.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		private HeapItem<TIdentifier> getHeapItem(TIdentifier id)
		{
			if (!this.identifierToHeapItem.TryGetValue(id, out WeakReference<HeapItem<TIdentifier>> weakRef))
				return null;

			if (!weakRef.TryGetTarget(out HeapItem<TIdentifier> item))
				return null;

			return item;
		}

		/// <summary>
		/// Reports heap item uses.
		/// </summary>
		/// <param name="item">The item that was finalized.</param>
		/// <param name="numUses">The number of uses since the last notification.</param>
		/// <param name="obj">The object of the item.</param>
		/// <param name="objInaccessible">Specifies whether the object has become inaccessible (i.e. garbage collected).</param>
		/// <param name="requestRemoval">Specifies whether the item should be removed.</param>
		internal void ReportUses(HeapItem<TIdentifier> item, int numUses, object obj, bool objInaccessible, bool requestRemoval)
		{
			HeapTrace.Instance.TraceItem(HeapTraceAction.ReportUses, item);

			var map = this.objectToHeapItem;
			if (map == null)
				return; // Heap is already disposed (or disposing).

			// Heap items can't be removed immediately when they're disposed because the identifier must
			// be kept occupied until the user retrieves the info (e.g. through the event) that an object
			// has been collected.

			// Create a UseInfo entry.
			UseInfo info;
			info.Item = item;
			info.Uses = numUses;
			info.Remove = requestRemoval;

			if (objInaccessible)
			{
				// Remove object->id entry now to prevent reuse of resurrected objects.
				map.Remove(obj);
			}

			// Add garbage or update the old entry if there is one.
			this.useInfo.AddOrUpdate(item.Identifier, info, (id, oldInfo) =>
			{
				// Check if the old garbage info belongs to a different item.
				HeapItem<TIdentifier> oldItem = oldInfo.Item;
				if (!ReferenceEquals(item, oldItem))
				{
					// The UseInfo belongs to an old HeapItem that must be dead by now and was replaced 
					// in Add, which did not remove the UseInfo. But since the remove was also  requested 
					// by the UseInfo, we can safely discard it here as the HeapItem was already removed.
					Debug.Assert(oldInfo.Remove);

					// It can not have any uses though.
					Debug.Assert(oldInfo.Uses == 0);

					return info;
				}

				// Integrate old info.
				info.Remove |= oldInfo.Remove;
				info.Uses += oldInfo.Uses;
				return info;
			});
		}

		/// <summary>
		/// Returns object usage information for the objects in the heap.
		/// </summary>
		/// <returns></returns>
		public IReadOnlyList<HeapUsageToken<TIdentifier>> GetUsage()
		{
			List<HeapUsageToken<TIdentifier>> tokens = new List<HeapUsageToken<TIdentifier>>();

			foreach (TIdentifier id in this.useInfo.Keys)
			{
				if (!this.useInfo.TryRemove(id, out UseInfo info))
					continue;

				// If the item has reported uses, return a usage token.
				if (info.Uses != 0)
				{
					tokens.Add(new HeapUsageToken<TIdentifier>(id, info.Uses));
					HeapTrace.Instance.TraceItem(HeapTraceAction.GetGarbage, info.Item);
				}

				// Remove item now if requested.
				if (info.Remove)
				{
					this.Remove(info.Item);
				}
			}

#if HEAPPERF
			Interlocked.Add(ref this.CollectedObjects, tokens.Count);
#endif
			return tokens;
		}

		/// <summary>
		/// Acknowledges uses of the objects specified in the array.
		/// </summary>
		/// <param name="tokens">The usage tokens.</param>
		public void AcknowledgeUses(HeapUsageToken<TIdentifier>[] tokens)
		{
			if (tokens.Length == 0)
				return;

			List<KeyValuePair<TIdentifier, object>> invalidItems = new List<KeyValuePair<TIdentifier, object>>();

			for (int i = 0; i < tokens.Length; ++i)
			{
				HeapItem<TIdentifier> item = this.getHeapItem(tokens[i].Identifier);

				if (item == null)
					throw new NetworkingException($"Heap item {tokens[i].Identifier} not found.");

				if (!item.AcknowledgeUses(tokens[i].Uses, out object obj))
				{
					// Keep track of objects that become invalid.
					invalidItems.Add(new KeyValuePair<TIdentifier, object>(item.Identifier, obj));
				}

				HeapTrace.Instance.TraceItem(HeapTraceAction.AcknowledgeUses, item);
			}

			// Invoke event.
			this.ItemsInvalidated?.Invoke(invalidItems);
		}

		/// <summary>
		/// Returns the identifier of the specified object.
		/// If the object wasn't found, a new idenfifier is generated and the object is added to the heap.
		/// </summary>
		/// <param name="obj">The object.</param>
		/// <param name="wasAdded">Returns true if the object was added in this call because it wasn't found in the heap. False otherwise.</param>
		/// <returns>The ID of the object.</returns>
		public virtual TIdentifier GetOrAdd(object obj, out bool wasAdded)
		{
			this.throwIfDisposed();

			if (obj == null)
			{
				// Return null identifier.
				wasAdded = false;
				return this.NullIdentifier;
			}

			// Try to get the item from the weak table.
			if (this.objectToHeapItem.TryGetValue(obj, out HeapItem<TIdentifier> item))
			{
				if (item.TryGetObject(out object itemObj))
				{
					Debug.Assert(ReferenceEquals(obj, itemObj));
					wasAdded = false;
					return item.Identifier;
				}
			}

			// Item does not exist (or is not usable), generate ID and add it.
			TIdentifier id = this.CreateIdentifier(obj);
			this.Add(id, obj);

			wasAdded = true;
			return id;
		}

		/// <summary>
		/// Adds the specified object.
		/// </summary>
		/// <param name="id">The identifier of the object.</param>
		/// <param name="obj">The object to add.</param>
		public virtual void Add(TIdentifier id, object obj)
		{
			this.throwIfDisposed();

			if (obj == null)
				throw new ArgumentNullException(nameof(obj));

			// Create new item and add it to the dictionary.
			HeapItem<TIdentifier> item = new HeapItem<TIdentifier>(this, id, obj);

			this.identifierToHeapItem.AddOrUpdate(id,
				new WeakReference<HeapItem<TIdentifier>>(item, true),
				(key, oldWeakRef) =>
				{
					// There might still be an old heap item in the dictionary that had its removal requested
					// in ReportUses, but the it has not been removed yet. In this case we can simply replace it.
					if (oldWeakRef.TryGetTarget(out HeapItem<TIdentifier> oldItem))
					{
						if (oldItem.TryGetObject(out _))
						{
							string msg = $"Can't replace \"{oldItem}\" because it's still in use.";
							HeapTrace.Instance.Error(msg, oldItem);
							throw new InvalidOperationException(msg);
						}

						oldItem.IsRemoved.TrySet();
					}

					HeapTrace.Instance.TraceItem(HeapTraceAction.Replace, oldItem);
					return new WeakReference<HeapItem<TIdentifier>>(item, true);
				});

			HeapTrace.Instance.TraceItem(HeapTraceAction.Add, item);

			// ConditionalWeakTable should really have an AddOrUpdate method. But for now, this will do.
			this.objectToHeapItem.Remove(obj);
			this.objectToHeapItem.GetValue(obj, k => item); // "TryAdd".
		}

		/// <summary>
		/// Removes a heap item from the heap.
		/// </summary>
		internal void Remove(HeapItem<TIdentifier> item)
		{
			// Check if item is already removed.
			if (!item.IsRemoved.TrySet())
				return;

			HeapTrace.Instance.TraceItem(HeapTraceAction.Remove, item);

			if (!this.identifierToHeapItem.TryRemove(item.Identifier, out WeakReference<HeapItem<TIdentifier>> weakRef))
			{
				string msg = "Failed to remove the heap item: Item not found in the heap.";

				HeapTrace.Instance.Error(msg, item);
				throw new InvalidOperationException(msg);
			}

			if (weakRef.TryGetTarget(out HeapItem<TIdentifier> currentItem) && !ReferenceEquals(item, currentItem))
			{
				string msg = "Failed to remove the heap item: Different item with same identifier found.";

				HeapTrace.Instance.Error(msg, item);
				throw new InvalidOperationException(msg);
			}

			// Release the identifier.
			this.RemoveIdentifier(item.Identifier);
		}

		/// <summary>
		/// Returns the object with the specified identifier.
		/// </summary>
		/// <param name="id">The identifier of the object to return.</param>
		/// <param name="obj">The object.</param>
		/// <returns>True, if the object has been found. False otherwise.</returns>
		public virtual bool TryGetValue(TIdentifier id, out object obj)
		{
			this.throwIfDisposed();

			obj = null;

			// Check null identifier for equality.
			if (this.NullIdentifier.Equals(id))
				return true;

			// Get heap item.
			HeapItem<TIdentifier> item = this.getHeapItem(id);
			if (item == null)
				return false;

			// Retrieve the item's object.
			if (!item.TryGetObject(out obj))
				return false;

			// Check if the item can be used.
			if (obj == null)
				return false;

			// Handle the case when an item was already collected locally and has been resurrected 
			// by the remote peer. This is often the case with instances of reflection classes,
			// such as Type, MethodInfo, FieldInfo etc. These often get collected, but once they
			// are reused, e.g. by calling GetType() again, the same old instance that already
			// expired is returned by the runtime. So we restore the weak reference if there is
			// none. GetOrAdd does not resurrect weak references so it might happen that a
			// different identifier (and HeapItem) for the same object already exists. In that case,
			// the new HeapItem should stay and we will treat the identifier as "read-only" alias
			// for the new one which should get collected eventually.
			this.objectToHeapItem.GetValue(obj, k => item); // "TryAdd".

			return true;
		}

		public IEnumerator<HeapItem<TIdentifier>> GetEnumerator()
		{
			this.throwIfDisposed();

			List<HeapItem<TIdentifier>> items = new List<HeapItem<TIdentifier>>(this.Count);

			foreach (WeakReference<HeapItem<TIdentifier>> weakRef in this.identifierToHeapItem.Values)
			{
				if (weakRef.TryGetTarget(out HeapItem<TIdentifier> item))
					items.Add(item);
			}

			return items.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		/// <summary>
		/// Disposes the heap.
		/// </summary>
		/// <param name="managed"></param>
		protected override void Dispose(bool managed)
		{
			// Get all heap items that are still alive.
			foreach (WeakReference<HeapItem<TIdentifier>> weakRef in this.identifierToHeapItem.Values.ToArray())
			{
				if (weakRef.TryGetTarget(out HeapItem<TIdentifier> item))
				{
					item.Dispose();
				}
			}

			this.objectToHeapItem = null;
			this.identifierToHeapItem.Clear();

#if HEAPPERF
			this.perfCounters.Dispose();
#endif

			base.Dispose(managed);
		}

		#endregion
	}
}
