﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// A serializable object usage report for networked <see cref="Heap{TIdentifier}"/> garbage collection.
	/// </summary>
	[Serializable]
	public readonly struct HeapUsageReport<TIdentifier>
	{
		#region Fields

		[SerializeAsValue]
		private readonly TIdentifier[] identifiers;

		[SerializeAsValue]
		private readonly Range[] ranges;

		public static readonly HeapUsageReport<TIdentifier> Empty = new HeapUsageReport<TIdentifier>(new TIdentifier[0], new Range[0]);

		#endregion

		#region Properties

		/// <summary>
		/// Gets the number of objects in the report.
		/// </summary>
		public int Count => this.identifiers.Length;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="HeapUsageReport{TIdentifier}"/> class. 
		/// </summary>
		private HeapUsageReport(TIdentifier[] identifiers, Range[] ranges)
		{
			this.identifiers = identifiers;
			this.ranges = ranges;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Collects the <see cref="HeapUsageToken{TIdentifier}">tokens</see> from the specified <see cref="Heap{TIdentifier}"/> and returns a <see cref="HeapUsageReport{TIdentifier}"/>.
		/// </summary>
		/// <param name="heap"></param>
		/// <returns></returns>
		public static HeapUsageReport<TIdentifier> Create(Heap<TIdentifier> heap)
		{
			HeapUsageToken<TIdentifier>[] tokens = heap.GetUsage().ToArray();

			if (tokens.Length == 0)
			{
				return Empty;
			}

			// Sort tokens by uses to improve locality for compression.
			Array.Sort(tokens, (a, b) => a.Uses.CompareTo(b.Uses));
			
			// Simple run-length encoding of revisions.
			TIdentifier[] identifiers = new TIdentifier[tokens.Length];
			List<Range> ranges = new List<Range>();
			Range currentRange = new Range {Uses = tokens[0].Uses};

			for (int i = 0; i < tokens.Length; ++i)
			{
				identifiers[i] = tokens[i].Identifier;
				
				if (tokens[i].Uses == currentRange.Uses)
				{
					++currentRange.Count;
				}
				else
				{
					ranges.Add(currentRange);
					currentRange.Uses = tokens[i].Uses;
					currentRange.Count = 1;
				}
			}

			ranges.Add(currentRange);

			return new HeapUsageReport<TIdentifier>(identifiers, ranges.ToArray());
		}

		/// <summary>
		/// Returns the <see cref="HeapUsageToken{TIdentifier}">tokens</see> contained in the report.
		/// </summary>
		/// <returns></returns>
		public HeapUsageToken<TIdentifier>[] GetTokens()
		{
			if (this.identifiers.Length == 0)
				return Array.Empty<HeapUsageToken<TIdentifier>>();
			
			// Reconstruct tokens.
			HeapUsageToken<TIdentifier>[] tokens = new HeapUsageToken<TIdentifier>[this.identifiers.Length];

			int rangeIndex = 0;
			int currentUses = this.ranges[0].Uses;
			int remainingTokens = this.ranges[0].Count;
			
			for (int i = 0; i < tokens.Length; ++i)
			{
				if (remainingTokens == 0)
				{
					++rangeIndex;
					currentUses = this.ranges[rangeIndex].Uses;
					remainingTokens = this.ranges[rangeIndex].Count;
				}
				
				tokens[i].Identifier = this.identifiers[i];
				tokens[i].Uses = currentUses;

				--remainingTokens;
			}

			return tokens;
		}
		
		/// <summary>
		/// Acknowledges uses of the objects contained in this report in the specified <see cref="Heap{TIdentifier}"/>.
		/// </summary>
		/// <param name="heap"></param>
		public void Apply(Heap<TIdentifier> heap)
		{
			if (this.identifiers.Length == 0)
				return;
			
			heap.AcknowledgeUses(this.GetTokens());
		}

		public override string ToString()
		{
			return $"Heap update for {this.Count} objects";
		}

		#endregion

		#region Nested Types

		[Serializable]
		private struct Range
		{
			[SerializeVariableLength]
			public int Uses;
			[SerializeVariableLength]
			public int Count;
		}
		
		#endregion
	}
}
