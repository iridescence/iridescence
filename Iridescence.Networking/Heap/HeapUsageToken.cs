﻿using System;

namespace Iridescence.Networking
{
	/// <summary>
	/// Represents a "snapshot" of an object's usage state.
	/// </summary>
	public struct HeapUsageToken<TIdentifier>
	{
		#region Fields

		/// <summary>
		/// The identifier of the object that was garbage collected.
		/// </summary>
		public TIdentifier Identifier;

		/// <summary>
		/// The number of uses.
		/// </summary>
		public int Uses;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="HeapUsageToken{TIdentifier}"/>.
		/// </summary>
		public HeapUsageToken(TIdentifier id, int uses)
		{
			if (uses <= 0)
				throw new ArgumentException("Uses must be greater than 0.", nameof(uses));

			this.Identifier = id;
			this.Uses = uses;
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			return $"{this.Identifier} +{this.Uses}";
		}

		#endregion
	}
}
