﻿#if HEAPPERF
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence.Networking
{
	/// <summary>
	/// Heap performance counters.
	/// </summary>
	internal sealed class HeapPerformanceCounters<TIdentifier> : IDisposable
	{
		#region Fields

		private const string CategoryName = "Iridescence.Networking.Heap";
		private const string CurrentNumberOfItemsName = "Items";
		private const string CollectedItemsName = "Garbage";

		private static int instanceCount;
		
		private readonly Heap<TIdentifier> heap;
		private readonly CancellationTokenSource cancellationTokenSource;
		private readonly Task samplingTask;

		#endregion

		#region Properties

		public PerformanceCounter CurrentNumberOfItems { get; }
		
		public PerformanceCounter CollectedItems { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="HeapPerformanceCounters{TIdentifier}"/> class. 
		/// </summary>
		public HeapPerformanceCounters(Heap<TIdentifier> heap)
		{
			if (!PerformanceCounterCategory.Exists(CategoryName))
			{
				CounterCreationDataCollection counterData = new CounterCreationDataCollection();
				counterData.Add(new CounterCreationData(CurrentNumberOfItemsName, "The number of items in the heap.", PerformanceCounterType.NumberOfItems64));
				counterData.Add(new CounterCreationData(CollectedItemsName, "The number of items that have been garbage collected.", PerformanceCounterType.CounterDelta64));
				PerformanceCounterCategory.Create(CategoryName, "Iridescence network heap performance.", PerformanceCounterCategoryType.MultiInstance, counterData);
			}

			this.heap = heap;

			int instanceId = Interlocked.Increment(ref instanceCount);
			string instanceName = $"{Process.GetCurrentProcess().Id}.{instanceId}";
			
			this.CurrentNumberOfItems = new PerformanceCounter(CategoryName, CurrentNumberOfItemsName, instanceName, false);
			this.CollectedItems = new PerformanceCounter(CategoryName, CollectedItemsName, instanceName, false);

			this.cancellationTokenSource = new CancellationTokenSource();
			this.samplingTask = this.sampleLoop();
		}

		#endregion

		#region Methods

		private async Task sampleLoop()
		{
			while (!this.cancellationTokenSource.IsCancellationRequested)
			{
				try
				{
					await Task.Delay(100, this.cancellationTokenSource.Token);
				}
				catch (TaskCanceledException)
				{
					break;
				}
				
				this.CurrentNumberOfItems.RawValue = this.heap.Count;
				this.CollectedItems.RawValue = this.heap.CollectedObjects;

				this.CurrentNumberOfItems.NextSample();
				this.CollectedItems.NextSample();
			}
		}

		public void Dispose()
		{
			this.cancellationTokenSource.Cancel();
			this.samplingTask.Wait();
			this.CurrentNumberOfItems.Dispose();
			this.CollectedItems.Dispose();
		}

		#endregion
	}
}
#endif
