﻿namespace Iridescence.Networking
{
	public enum HeapTraceAction
	{
		Add,
		Remove,
		Replace,
		Finalize,
		ReportUses,
		AcknowledgeUses,
		GetGarbage,
	}
}