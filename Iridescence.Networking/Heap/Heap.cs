﻿using System;

namespace Iridescence.Networking
{
	/// <summary>
	/// Base class for networking object heaps.
	/// </summary>
	public abstract class Heap : IDisposable
	{
		#region Fields

		private readonly ConcurrentHashSet<object> strongReferences;
		private ConcurrentFlag isDisposed;

		internal readonly int HeapID;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the number of objects in the heap.
		/// </summary>
		public abstract int Count { get; }

		/// <summary>
		/// Gets a value that indicates whether the heap has been disposed.
		/// </summary>
		public bool IsDisposed => this.isDisposed.IsSet;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Heap"/> class. 
		/// </summary>
		protected Heap()
		{
			this.HeapID = Trace.GetTraceObjectID(this);
			HeapTrace.Instance.Create(this.HeapID, this.GetType().FullName);

			this.strongReferences = new ConcurrentHashSet<object>();
			this.isDisposed = new ConcurrentFlag();
		}

		~Heap()
		{
			if (this.isDisposed.TrySet())
			{
				this.Dispose(false);
			}
		}
		
		#endregion

		#region Methods

		/// <summary>
		/// Adds a strong reference to the specified object to make it stay indefinitely.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		protected internal bool AddStrongReference(object obj)
		{
			return this.strongReferences.Add(obj);
		}

		/// <summary>
		/// Removes the strong reference to the specified object.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		protected internal bool RemoveStrongReference(object obj)
		{
			return this.strongReferences.TryRemove(obj);
		}

		protected virtual void Dispose(bool managed)
		{
			HeapTrace.Instance.Dispose(this.HeapID, managed);

			this.strongReferences.Clear();
		}

		/// <summary>
		/// Disposes the heap and releases the objects that are currently kept alive by it.
		/// </summary>
		public void Dispose()
		{
			if (this.isDisposed.TrySet())
			{
				this.Dispose(true);
				GC.SuppressFinalize(this);
			}
		}
		
		#endregion
	}
}
