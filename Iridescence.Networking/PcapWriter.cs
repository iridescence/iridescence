﻿using System;
using System.IO;

namespace Iridescence.Networking
{
	/// <summary>
	/// Writer for libpcap files.
	/// </summary>
	public class PcapWriter
	{
		#region Fields

		private static readonly DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

		private readonly int maxPacketLength;

		#endregion

		#region Properties

		public Stream Stream { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PcapWriter"/> class. 
		/// </summary>
		/// <param name="stream">The stream to write to.</param>
		/// <param name="maxPacketLength">The maximum numbers of bytes to store in a record.</param>
		/// <param name="network">The data link type.</param>
		public PcapWriter(Stream stream, int maxPacketLength, int network)
		{
			this.Stream = stream;
			this.maxPacketLength = maxPacketLength;

			// Magic
			this.Stream.WriteUInt32B(0xD4C3B2A1u);

			// Version
			this.Stream.WriteUInt16L(2);
			this.Stream.WriteUInt16L(4);

			// Timezone.
			this.Stream.WriteUInt32L(0);

			// Accuracy of timestamps.
			this.Stream.WriteUInt32L(0);

			// Max packet length and link type.
			this.Stream.WriteInt32L(maxPacketLength);
			this.Stream.WriteInt32L(network);
		}

		#endregion

		#region Methods

		/// <summary>
		/// Writes a record.
		/// </summary>
		/// <param name="data">The packet data.</param>
		/// <param name="offset">An offset in the packet data.</param>
		/// <param name="count">The number of bytes in the packet.</param>
		/// <param name="timestamp">The packet timestamp.</param>
		public void WriteRecord(byte[] data, int offset, int count, DateTime timestamp)
		{
			TimeSpan time = timestamp.ToUniversalTime() - origin;

			uint totalSeconds = (uint)time.TotalSeconds;

			TimeSpan timeBase = TimeSpan.FromSeconds(totalSeconds);

			this.Stream.WriteUInt32L(totalSeconds);
			this.Stream.WriteUInt32L((uint)((time - timeBase).Ticks / 10));

			int inclLen = Math.Min(count, this.maxPacketLength);
			this.Stream.WriteUInt32L((uint)inclLen);
			this.Stream.WriteUInt32L((uint)count);

			this.Stream.Write(data, offset, inclLen);
		}

		/// <summary>
		/// Writes a record.
		/// </summary>
		/// <param name="data">The packet data.</param>
		/// <param name="offset">An offset in the packet data.</param>
		/// <param name="count">The number of bytes in the packet.</param>
		public void WriteRecord(byte[] data, int offset, int count)
		{
			this.WriteRecord(data, 0, count, DateTime.UtcNow);
		}

		/// <summary>
		/// Writes a record.
		/// </summary>
		/// <param name="data">The packet data.</param>
		public void WriteRecord(byte[] data)
		{
			this.WriteRecord(data, 0, data.Length);
		}
		
		#endregion
	}
}
