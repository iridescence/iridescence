﻿using System.Text;

namespace Iridescence.Networking
{
	/// <summary>
	/// Holds some meta information about objects that have been (de-)serialized.
	/// </summary>
	public sealed class ObjectInfo<TIdentifier>
	{
		#region Fields

		/// <summary>
		/// The object identifier.
		/// </summary>
		public TIdentifier Identifier { get; }

		/// <summary>
		/// The object.
		/// </summary>
		public object Object { get; }

		/// <summary>
		/// Indicates whether the object was first (de-)serialized.
		/// </summary>
		public bool IsNew { get; }

		#endregion

		#region Constructors

		internal ObjectInfo(TIdentifier identifier, object obj, bool isNew)
		{
			this.Identifier = identifier;
			this.Object = obj;
			this.IsNew = isNew;
		}

		#endregion

		#region

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();
			
			if (this.IsNew)
			{
				str.Append("New ");
			}

			str.Append(this.Identifier);
			str.Append(' ');
			str.Append(this.Object.GetType());

			return str.ToString();
		}

		#endregion
	}
}