﻿using System;
using System.Collections.Generic;

namespace Iridescence.Networking
{
	/// <summary>
	/// Object (de)serialization event arguments.
	/// </summary>
	public class ObjectInfoEventArgs<TIdentifier> : EventArgs
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the object that have been (de)serialized.
		/// </summary>
		public IReadOnlyList<ObjectInfo<TIdentifier>> Objects { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ObjectInfoEventArgs class. 
		/// </summary>
		public ObjectInfoEventArgs(IEnumerable<ObjectInfo<TIdentifier>> objects)
		{
			this.Objects = objects.ToReadOnlyList();
		}

		#endregion

		#region Methods

		#endregion
	}
}
