﻿using System.Collections.Generic;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Tracks objects passing through a <see cref="IReferenceResolver{T,TIdentifier}"/>.
	/// </summary>
	internal sealed class ReferenceResolverTracker<TIdentifier> : IReferenceResolver<object, TIdentifier>
	{
		#region Fields

		private readonly IReferenceResolver<object, TIdentifier> resolver;
		private readonly Dictionary<TIdentifier, ObjectInfo<TIdentifier>> objects;
		private readonly bool cache;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the objects that have been serialized.
		/// </summary>
		public IEnumerable<ObjectInfo<TIdentifier>> Objects => this.objects.Values;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="ReferenceResolverTracker{TIdentifier}"/> that acts as a proxy for the spefified resolver.
		/// </summary>
		/// <param name="resolver">The upstream <see cref="IReferenceResolver{T,TIdentifier}"/> that provides references.</param>
		/// <param name="cache">Specifies whether the local object dictionary acts as a cache to the upstream reference resolver.</param>
		public ReferenceResolverTracker(IReferenceResolver<object, TIdentifier> resolver, bool cache)
		{
			this.resolver = resolver;
			this.objects = new Dictionary<TIdentifier, ObjectInfo<TIdentifier>>();
			this.cache = cache;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Clears the recorded objects.
		/// </summary>
		public void Clear()
		{
			this.objects.Clear();
		}

		public bool TryGetValue(TIdentifier id, out object obj)
		{
			// Try the cache first, if enabled.
			if (this.cache && this.objects.TryGetValue(id, out ObjectInfo<TIdentifier> existing))
			{
				obj = existing.Object;
				return true;
			}

			// Ask the upstream reference resolver.
			if (this.resolver.TryGetValue(id, out obj))
			{
				if (obj != null)
				{
					// Add to local cache.
					this.objects.Add(id, new ObjectInfo<TIdentifier>(id, obj, false));
				}

				return true;
			}

			return false;
		}

		public void Add(TIdentifier id, object obj)
		{
			// Add to resolver and cache.
			this.resolver.Add(id, obj);
			this.objects.Add(id, new ObjectInfo<TIdentifier>(id, obj, true));
		}

		#endregion
	}
}
