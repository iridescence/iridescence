﻿using System;

namespace Iridescence.Networking
{
	/// <summary>
	/// Marks the value of a static field or property as networking static value.
	/// This means that the value itself is never serialized. Instead, a reference to the field or property is sent which can then be used to reconstruct the value on the other end.
	/// </summary>
	/// <remarks>
	/// Only static fields and properties are considered.
	/// The current value of the field/property at the time of networking initializtion is used.
	/// The property/field should be readonly and its value should not change after networking was initialized.
	/// A new value will be ignored in case the value of the field or property is changed after networking was initialized. 
	/// </remarks>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public sealed class NetworkingStaticAttribute : Attribute
	{
		
	}
}
