﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements a serialization object replacer that replaces values of static fields/properties marked with a NetworkingStaticAttribute with a reference to the field/property.
	/// </summary>
	public sealed class StaticObjectReplacer : IObjectReplacer
	{
		#region Fields

		private readonly Dictionary<object, IObjectReference> references;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new StaticReplacer.
		/// </summary>
		public StaticObjectReplacer()
		{
			this.references = new Dictionary<object, IObjectReference>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Scans the specified type for static fields and properties marked with a NetworkingStaticAttribute.
		/// </summary>
		/// <param name="type">The type to search.</param>
		public void AddType(Type type)
		{
			// search fields.
			foreach (FieldInfo field in type.GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
			{
				NetworkingStaticAttribute staticAttr = field.GetCustomAttribute<NetworkingStaticAttribute>();
				if (staticAttr == null)
					continue;

				object value = field.GetValue(null);
				if (value == null)
				{
					Trace.WriteWarning($"Value of NetworkingStatic field {field} on type {type} is null.");
					continue;
				}

				if (this.references.TryGetValue(value, out IObjectReference existingRef))
				{
					Trace.WriteWarning($"Value of NetworkingStatic field {field} on type {type} is already referenced by {existingRef}.");
					continue;
				}

				FieldReference reference = new FieldReference(null, field);
				this.references.Add(value, reference);
			}

			// search properties.
			foreach (PropertyInfo property in type.GetProperties(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic))
			{
				NetworkingStaticAttribute staticAttr = property.GetCustomAttribute<NetworkingStaticAttribute>();
				if (staticAttr == null)
					continue;

				object value = property.GetValue(null);
				if (value == null)
				{
					Trace.WriteWarning($"Value of NetworkingStatic property {property} on type {type} is null.");
					continue;
				}

				if (this.references.TryGetValue(value, out IObjectReference existingRef))
				{
					Trace.WriteWarning($"Value of NetworkingStatic property {property} on type {type} is already referenced by {existingRef}.");
					continue;
				}

				PropertyReference reference = new PropertyReference(null, property);
				this.references.Add(value, reference);
			}
		}

		/// <summary>
		/// Scans the specified types for static fields and properties marked with a NetworkingStaticAttribute.
		/// </summary>
		/// <param name="types">The types to search.</param>
		public void AddTypes(IEnumerable<Type> types)
		{
			foreach (Type type in types)
			{
				this.AddType(type);
			}
		}

		/// <summary>
		/// Scans the specified assembly for static fields and properties marked with a NetworkingStaticAttribute.
		/// </summary>
		/// <param name="assembly">The assembly to search.</param>
		public void AddAssembly(Assembly assembly)
		{
			foreach (TypeInfo type in assembly.DefinedTypes)
			{
				this.AddType(type);
			}
		}

		public object Replace(object obj)
		{

			if (this.references.TryGetValue(obj, out IObjectReference reference))
				return reference;

			return obj;
		}
		
		#endregion
	}
}
