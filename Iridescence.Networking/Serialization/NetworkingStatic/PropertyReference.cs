﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Represents a reference to a property.
	/// </summary>
	[Serializable]
	public sealed class PropertyReference : IObjectReference
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the target object.
		/// </summary>
		public object Target { get; }

		/// <summary>
		/// Gets the referenced property.
		/// </summary>
		public PropertyInfo Property { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new PropertyReference.
		/// </summary>
		public PropertyReference(object target, PropertyInfo property)
		{
			if(property == null)
				throw new ArgumentNullException(nameof(property));

			this.Target = target;
			this.Property = property;
		}

		#endregion

		#region Methods

		public object GetRealObject(StreamingContext context)
		{
			return this.Property.GetValue(this.Target);
		}
		
		public override string ToString()
		{
			return this.Property.ToString();
		}

		#endregion
	}
}
