﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Iridescence.Networking
{
	/// <summary>
	/// Maps fields and properties to identifiers so that a remote peer can reconstruct object references coming from fields and properties marked with <see cref="NetworkingStaticAttribute"/>.
	/// </summary>
	[Serializable]
	public class NetworkingStaticReferences<TIdentifier>
	{
		#region Fields

		private readonly Dictionary<MemberInfo, TIdentifier> members;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="NetworkingStaticReferences{TIdentifier}"/> class. 
		/// </summary>
		public NetworkingStaticReferences()
		{
			this.members = new Dictionary<MemberInfo, TIdentifier>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Retreives the objects with their respective IDs.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<NetworkingStaticReference<TIdentifier>> GetObjects()
		{
			foreach (KeyValuePair<MemberInfo, TIdentifier> entry in this.members)
			{
				object value;

				if (entry.Key is FieldInfo field)
				{
					value = field.GetValue(null);
				}
				else if (entry.Key is PropertyInfo property)
				{
					value = property.GetValue(null);
				}
				else
				{
					throw new NotSupportedException();
				}

				yield return new NetworkingStaticReference<TIdentifier>(entry.Value, entry.Key, value);
			}
		}
		
		/// <summary>
		/// References a field by the specified ID.
		/// </summary>
		/// <param name="field">The field to add.</param>
		/// <param name="id">The identifier.</param>
		public void AddField(FieldInfo field, TIdentifier id)
		{
			if (field == null)
				throw new ArgumentNullException(nameof(field));

			if (!field.IsStatic)
				throw new ArgumentException("Only static fields can be used for network static references.", nameof(field));

			this.members.Add(field, id);
		}

		/// <summary>
		/// References a property by the specified ID.
		/// </summary>
		/// <param name="property">The property to add.</param>
		/// <param name="id">The identifier.</param>
		public void AddProperty(PropertyInfo property, TIdentifier id)
		{
			if (property == null)
				throw new ArgumentNullException(nameof(property));

			if (!property.GetGetMethod().IsStatic)
				throw new ArgumentException("Only static properties can be used for network static references.", nameof(property));

			this.members.Add(property, id);
		}

		/// <summary>
		/// Searches the specified type for static fields and properties marked with <see cref="NetworkingStaticAttribute"/>.
		/// </summary>
		/// <param name="type">The type to scan.</param>
		/// <param name="idFactory">The function used to generate IDs.</param>
		/// <param name="recursive">Whether to go through all base types of the specified type.</param>
		public void AddType(Type type, Func<MemberInfo, TIdentifier> idFactory, bool recursive = true)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			while(type != null && type != typeof(object))
			{
				foreach (FieldInfo field in type.GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly))
				{
					NetworkingStaticAttribute attr = field.GetCustomAttribute<NetworkingStaticAttribute>();
					if (attr != null && !this.members.ContainsKey(field))
					{
						TIdentifier id = idFactory(field);
						this.AddField(field, id);
					}
				}

				foreach (PropertyInfo property in type.GetProperties(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly))
				{
					NetworkingStaticAttribute attr = property.GetCustomAttribute<NetworkingStaticAttribute>();
					if (attr != null && !this.members.ContainsKey(property))
					{
						TIdentifier id = idFactory(property);
						this.AddProperty(property, id);
					}
				}

				if (!recursive)
					break;

				type = type.BaseType;
			}
		}

		/// <summary>
		/// Searches the specified assembly for static fields and property marked with <see cref="NetworkingStaticAttribute"/>.
		/// </summary>
		/// <param name="assembly">The assembly to scan.</param>
		/// <param name="idFactory">The function used to generate IDs.</param>
		public void AddAssembly(Assembly assembly, Func<MemberInfo, TIdentifier> idFactory)
		{
			foreach (TypeInfo type in assembly.DefinedTypes)
			{
				this.AddType(type, idFactory);
			}
		}

		#endregion
	}
}
