﻿using System.Reflection;

namespace Iridescence.Networking
{
	/// <summary>
	/// Represents a networking static object reference.
	/// </summary>
	public struct NetworkingStaticReference<TIdentifier>
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the identifier.
		/// </summary>
		public TIdentifier Identifier { get; }

		/// <summary>
		/// Gets the source member that provides the value.
		/// </summary>
		public MemberInfo Source { get; }

		/// <summary>
		/// Gets the value.
		/// </summary>
		public object Value { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="NetworkingStaticReference{TIdentifier}"/> class. 
		/// </summary>
		public NetworkingStaticReference(TIdentifier id, MemberInfo source, object value)
		{
			this.Identifier = id;
			this.Source = source;
			this.Value = value;
		}

		#endregion

		#region Methods

		#endregion
	}
}
