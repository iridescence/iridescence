﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Represents a reference to a field.
	/// </summary>
	[Serializable]
	public sealed class FieldReference : IObjectReference
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the target object.
		/// </summary>
		public object Target { get; }

		/// <summary>
		/// Gets the referenced field.
		/// </summary>
		public FieldInfo Field { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FieldReference.
		/// </summary>
		public FieldReference(object target, FieldInfo field)
		{
			if(field == null)
				throw new ArgumentNullException(nameof(field));

			this.Target = target;
			this.Field = field;
		}

		#endregion

		#region Methods

		public object GetRealObject(StreamingContext context)
		{
			return this.Field.GetValue(this.Target);
		}

		public override string ToString()
		{
			return this.Field.ToString();
		}

		#endregion
	}
}
