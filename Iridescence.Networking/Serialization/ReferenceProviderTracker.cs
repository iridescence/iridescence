﻿using System.Collections.Generic;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Tracks objects passing through a <see cref="IReferenceProvider{T,TIdentifier}"/>.
	/// </summary>
	internal sealed class ReferenceProviderTracker<TIdentifier> : IReferenceProvider<object, TIdentifier>
	{
		#region Fields

		private readonly IReferenceProvider<object, TIdentifier> provider;
		private readonly Dictionary<object, ObjectInfo<TIdentifier>> objects;
		private readonly bool cache;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the objects that have been serialized.
		/// </summary>
		public IEnumerable<ObjectInfo<TIdentifier>> Objects => this.objects.Values;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="ReferenceProviderTracker{TIdentifier}"/> that acts as a proxy for the spefified provider.
		/// </summary>
		/// <param name="provider">The upstream <see cref="IReferenceProvider{T,TIdentifier}"/> that provides references.</param>
		/// <param name="cache">Specifies whether the local object dictionary acts as a cache to the upstream reference provider.</param>
		public ReferenceProviderTracker(IReferenceProvider<object, TIdentifier> provider, bool cache)
		{
			this.provider = provider;
			this.objects = new Dictionary<object, ObjectInfo<TIdentifier>>(new ReferenceEqualityComparer<object>());
			this.cache = cache;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Clears the recorded objects.
		/// </summary>
		public void Clear()
		{
			this.objects.Clear();
		}

		public TIdentifier GetOrAdd(object obj, out bool wasAdded)
		{
			// Try the cache first, if enabled.
			if (this.cache && obj != null && this.objects.TryGetValue(obj, out ObjectInfo<TIdentifier> existing))
			{
				wasAdded = false;
				return existing.Identifier;
			}
			
			// Get it from the upstream provider.
			TIdentifier id = this.provider.GetOrAdd(obj, out wasAdded);

			if (obj != null)
			{
				// Add to tracker/cache.
				this.objects[obj] = new ObjectInfo<TIdentifier>(id, obj, wasAdded);
			}

			return id;
		}

		#endregion
	}
}
