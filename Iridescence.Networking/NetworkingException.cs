﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Networking exception.
	/// </summary>
	[Serializable]
	public class NetworkingException : Exception
	{
		#region Constructors

		protected NetworkingException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			
		}

		/// <summary>
		/// Creates a new NetworkingException.
		/// </summary>
		public NetworkingException()
		{
			
		}

		/// <summary>
		/// Creates a new NetworkingException.
		/// </summary>
		public NetworkingException(string message)
			: base(message)
		{

		}

		/// <summary>
		/// Creates a new NetworkingException.
		/// </summary>
		public NetworkingException(string message, Exception innerException)
			: base(message, innerException)
		{

		}

		#endregion
	}
}
