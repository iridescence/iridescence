﻿using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Iridescence.Networking
{
	/// <summary>
	/// Extensions for <see cref="TcpListener"/>.
	/// </summary>
	public static class TcpListenerExtensions
	{
		#region Methods

		/// <summary>
		/// Accepts a pending connection request as an asynchronous operation with cancellation support.
		/// </summary>
		/// <param name="listener"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public static async Task<Socket> AcceptSocketAsync(this TcpListener listener, CancellationToken cancellationToken)
		{
			for (;;)
			{
				cancellationToken.ThrowIfCancellationRequested();
				
				if (listener.Pending())
				{
					return await listener.AcceptSocketAsync();
				}

				await Task.Delay(1, cancellationToken);
			}
		}

		
		/// <summary>
		/// Accepts a pending connection request as an asynchronous operation with cancellation support.
		/// </summary>
		/// <param name="listener"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public static async Task<TcpClient> AcceptTcpClientAsync(this TcpListener listener, CancellationToken cancellationToken)
		{
			for (;;)
			{
				cancellationToken.ThrowIfCancellationRequested();
				
				if (listener.Pending())
				{
					return await listener.AcceptTcpClientAsync();
				}

				await Task.Delay(1, cancellationToken);
			}
		}

		#endregion
	}
}
