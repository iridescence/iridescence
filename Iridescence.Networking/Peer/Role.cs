﻿using System;

namespace Iridescence.Networking
{
	/// <summary>
	/// Defines networking roles.
	/// </summary>
	[Flags]
	public enum Role
	{
		/// <summary>
		/// No role.
		/// </summary>
		None = 0,

		/// <summary>
		/// Server role.
		/// </summary>
		Server = 1,

		/// <summary>
		/// Client role.
		/// </summary>
		Client = 2,
	}
}
