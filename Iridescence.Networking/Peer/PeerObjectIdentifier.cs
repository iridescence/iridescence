﻿using System;
using System.Globalization;
using System.Text;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements a networking peer object identifier.
	/// </summary>
	[Serializable]
	public struct PeerObjectIdentifier : IEquatable<PeerObjectIdentifier>, IComparable<PeerObjectIdentifier>
	{
		#region Fields

		[SerializeVariableLength]
		internal readonly ulong Value;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the role of the peer that created the object.
		/// </summary>
		public Role Role => (Role)(this.Value & 3);

		/// <summary>
		/// Gets the ID of the object.
		/// </summary>
		public long ID => (long)(this.Value >> 2);

		#endregion

		#region Constructors

		internal PeerObjectIdentifier(ulong value)
		{
			this.Value = value;
		}

		/// <summary>
		/// Initializes a new instance of the PeerObjectIdentifier class. 
		/// </summary>
		public PeerObjectIdentifier(Role role, long id)
		{
			if ((int)role < 0 || (int)role > 3)
				throw new ArgumentException("Invalid role.", nameof(role));

			if (id < 0 || id > (long.MaxValue >> 2))
				throw new ArgumentOutOfRangeException(nameof(id));

			this.Value = (ulong)role | ((ulong)id << 2);
		}

		#endregion

		#region Methods

		public override int GetHashCode()
		{
			return this.Value.GetHashCode();
		}

		public bool Equals(PeerObjectIdentifier other)
		{
			return this.Value == other.Value;
		}

		public override bool Equals(object obj)
		{
			if (!(obj is PeerObjectIdentifier))
				return false;

			PeerObjectIdentifier other = (PeerObjectIdentifier)obj;
			return other.Value == this.Value;
		}

		public int CompareTo(PeerObjectIdentifier other)
		{
			int v = ((int)this.Role).CompareTo((int)other.Role);
			if (v != 0) return v;
			return ((int)this.ID).CompareTo((int)other.ID);
		}

		public override string ToString()
		{
			StringBuilder str = new StringBuilder();
			str.Append(this.Role);
			str.Append('.');
			str.Append(this.ID.ToString(CultureInfo.InvariantCulture));
			return str.ToString();
		}

		#endregion
	}
}
