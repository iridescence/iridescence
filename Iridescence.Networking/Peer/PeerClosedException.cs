﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Exception that is thrown when an attempt is made to read from or write to a closed peer.
	/// </summary>
	[Serializable]
	public class PeerClosedException : PeerIOException
	{
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="PeerClosedException"/> class. 
		/// </summary>
		public PeerClosedException()
			: this("The peer was closed.")
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PeerClosedException"/> class. 
		/// </summary>
		public PeerClosedException(string message) : base(message)
		{
			
		}
	
		/// <summary>
		/// Initializes a new instance of the <see cref="PeerClosedException"/> class. 
		/// </summary>
		public PeerClosedException(string message, Exception innerException) : base(message, innerException)
		{
			
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="PeerClosedException"/> class. 
		/// </summary>
		protected PeerClosedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			
		}

		#endregion
	}
}
