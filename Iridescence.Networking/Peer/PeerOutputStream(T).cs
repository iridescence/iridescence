﻿using System.IO;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements an <see cref="BufferedOutputStream"/> for peers.
	/// </summary>
	public abstract class PeerOutputStream<TIdentifier> : CompactOutputStream, IReferencingOutputStream<TIdentifier>
	{
		#region Fields

		private readonly IReferenceProvider<object, TIdentifier> objectReferenceProvider;

		#endregion

		#region Properties

		#endregion

		#region Constructors
		
		protected PeerOutputStream(Stream stream, int initialCapacity, bool leaveOpen, IReferenceProvider<object, TIdentifier> objectReferenceProvider)
			: base(stream, initialCapacity, leaveOpen, CompactMetadataOptions.None)
		{
			this.objectReferenceProvider = objectReferenceProvider;
		}

		#endregion

		#region Methods

		public abstract void WriteIdentifier(TIdentifier id);

		public TIdentifier GetOrAdd(object obj, out bool wasAdded)
		{
			return this.objectReferenceProvider.GetOrAdd(obj, out wasAdded);
		}
		
		#endregion
	}
}
