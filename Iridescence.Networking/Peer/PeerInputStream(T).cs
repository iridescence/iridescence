﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements an <see cref="BufferedInputStream"/> for peers.
	/// </summary>
	public abstract class PeerInputStream<TIdentifier> : CompactInputStream, IReferencingInputStream<TIdentifier>, IDeserializationCallbackCollector
	{
		#region Fields

		private readonly DeferredResolver<object, TIdentifier> refResolver;
		private List<IDeserializationCallback> dCallbacks;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		protected PeerInputStream(Stream stream, int initialCapacity, bool leaveOpen, IReferenceResolver<object, TIdentifier> objectReferenceResolver, Func<AssemblyName, Assembly> assemblyResolver)
			: base(stream, initialCapacity, leaveOpen, CompactMetadataOptions.None, assemblyResolver)
		{
			this.refResolver = new DeferredResolver<object, TIdentifier>(objectReferenceResolver);
			this.dCallbacks = new List<IDeserializationCallback>();
		}

		#endregion

		#region Methods

		public abstract TIdentifier ReadIdentifier();

		public DeferredReferencingResult Get<TValue, TState>(TIdentifier identifier, DeferredCallback<TValue, TState> callback, TState state)
		{
			return this.refResolver.Get(identifier, callback, state);
		}

		public void Set(TIdentifier identifier, object value)
		{
			this.refResolver.Set(identifier, value);
		}
		
		public void Add(IDeserializationCallback obj)
		{
			this.dCallbacks.Add(obj);
		}

		public IDeserializationCallback[] GetDeserializationCallbacksAndClear()
		{
			Debug.Assert(!this.refResolver.MissingObjects.Any());
			var array = this.dCallbacks.ToArray();
			this.dCallbacks.Clear();
			return array;
		}
		
		#endregion
	}
}
