﻿using System;
using System.Reflection;

namespace Iridescence.Networking
{
	/// <summary>
	/// Peer RPC extension methods.
	/// </summary>
	public static class PeerRpcExtensions
	{
		/// <summary>
		/// Dynamically invokes the specified method on the remote end.
		/// </summary>
		/// <param name="peer">The peer to send the request to.</param>
		/// <param name="method">The delegate to invoke.</param>
		/// <param name="arguments">The arguments.</param>
		public static void DynamicInvoke(this IPeer peer, Delegate method, params object[] arguments)
		{
			if (arguments.Length == 1)
			{
				peer.Write(new DynamicCallSingleArgument
				{
					Delegate = method,
					Parameter = arguments[0],
				});
			}
			else
			{
				peer.Write(new DynamicCall
				{
					Delegate = method,
					Parameters = arguments,
				});				
			}
		}

		/// <summary>
		/// Dynamically invokes the specified method on the remote end.
		/// </summary>
		/// <param name="peer">The peer to send the request to.</param>
		/// <param name="method">The delegate to invoke.</param>
		/// <param name="arguments">The arguments.</param>
		public static bool TryDynamicInvoke(this IPeer peer, Delegate method, params object[] arguments)
		{
			if (arguments.Length == 1)
			{
				return peer.TryWrite(new DynamicCallSingleArgument
				{
					Delegate = method,
					Parameter = arguments[0],
				});
			}
			
			return peer.TryWrite(new DynamicCall
			{
				Delegate = method,
				Parameters = arguments,
			});				
		}
		
		private static void invokePropertySetter(IPeer peer, object target, PropertyInfo property, object value)
		{
			// get the setter method.
			MethodInfo setter = property.GetSetMethod(true);
			if (setter == null)
				throw new InvalidOperationException("Property has no setter.");

			// create a delegate for the setter.
			Type setterDelegateType = typeof(Action<>).MakeGenericType(property.PropertyType);
			Delegate setterDelegate = Delegate.CreateDelegate(setterDelegateType, target, setter);

			peer.DynamicInvoke(setterDelegate, value);
		}
		
		private static bool tryInvokePropertySetter(IPeer peer, object target, PropertyInfo property, object value)
		{
			// get the setter method.
			MethodInfo setter = property.GetSetMethod(true);
			if (setter == null)
				throw new InvalidOperationException("Property has no setter.");

			// create a delegate for the setter.
			Type setterDelegateType = typeof(Action<>).MakeGenericType(property.PropertyType);
			Delegate setterDelegate = Delegate.CreateDelegate(setterDelegateType, target, setter);

			return peer.TryDynamicInvoke(setterDelegate, value);
		}

		/// <summary>
		/// Attempts to set a property via an RPC to the property setter on the remote end.
		/// </summary>
		/// <param name="peer"></param>
		/// <param name="obj"></param>
		/// <param name="propertyName"></param>
		/// <param name="value"></param>
		public static void SetProperty<T>(this IPeer peer, T obj, string propertyName, object value)
		{
			if(obj == null)
				throw new ArgumentNullException(nameof(obj));

			// get the property.
			Type type = typeof(T);
			PropertyInfo property = type.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
			if(property == null)
				throw new ArgumentException($"Could not find property \"{propertyName}\" in \"{type.Name}\".");

			// call setter.
			invokePropertySetter(peer, obj, property, value);
		}
		
		/// <summary>
		/// Attempts to set a property via an RPC to the property setter on the remote end.
		/// </summary>
		/// <param name="peer"></param>
		/// <param name="obj"></param>
		/// <param name="propertyName"></param>
		/// <param name="value"></param>
		public static bool TrySetProperty<T>(this IPeer peer, T obj, string propertyName, object value)
		{
			if(obj == null)
				throw new ArgumentNullException(nameof(obj));

			// get the property.
			Type type = typeof(T);
			PropertyInfo property = type.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
			if(property == null)
				throw new ArgumentException($"Could not find property \"{propertyName}\" in \"{type.Name}\".");

			// call setter.
			return tryInvokePropertySetter(peer, obj, property, value);
		}

		/// <summary>
		/// Attempts to synchronize a property via an RPC to the property setter on the remote end.
		/// </summary>
		/// <param name="peer"></param>
		/// <param name="obj"></param>
		/// <param name="propertyName"></param>
		public static void SyncProperty<T>(this IPeer peer, T obj, string propertyName)
		{
			if(obj == null)
				throw new ArgumentNullException(nameof(obj));

			// get the property.
			Type type = typeof(T);
			PropertyInfo property = type.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy);
			if(property == null)
				throw new ArgumentException($"Could not find property \"{propertyName}\" in \"{type.Name}\".");

			// get value.
			object value = property.GetValue(obj);

			// call setter.
			invokePropertySetter(peer, obj, property, value);
		}
		
		/// <summary>
		/// Attempts to synchronize a property via an RPC to the property setter on the remote end.
		/// </summary>
		/// <param name="peer"></param>
		/// <param name="obj"></param>
		/// <param name="propertyName"></param>
		public static bool TrySyncProperty<T>(this IPeer peer, T obj, string propertyName)
		{
			if(obj == null)
				throw new ArgumentNullException(nameof(obj));

			// get the property.
			Type type = typeof(T);
			PropertyInfo property = type.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy);
			if(property == null)
				throw new ArgumentException($"Could not find property \"{propertyName}\" in \"{type.Name}\".");

			// get value.
			object value = property.GetValue(obj);

			// call setter.
			return tryInvokePropertySetter(peer, obj, property, value);
		}

		/// <summary>
		/// Handles an <see cref="ICall"/> that arrived on the specified <see cref="Peer"/>
		/// </summary>
		/// <param name="peer"></param>
		/// <param name="call"></param>
		public static void HandleCall(this IPeer peer, ICall call)
		{
			if (peer == null)
				throw new ArgumentNullException(nameof(peer));

			if (call == null)
				throw new ArgumentNullException(nameof(call));

			// Verify.
			call.VerifyCall(peer);

			// Invoke.
			call.Invoke();
		}

		/// <summary>
		/// Reads from the peer and handles all <see cref="ICall">calls</see> by invoking <see cref="HandleCall"/> until a non-<see cref="ICall"/> object was read, which is then returned.
		/// </summary>
		/// <param name="peer"></param>
		/// <returns></returns>
		public static object ReadObjectHandleCalls(this IPeer peer)
		{
			for (;;)
			{
				object obj = peer.Read();
				
				// Handle calls.
				if (obj is ICall call)
				{
					peer.HandleCall(call);
					continue;
				}

				return obj;
			}
		}

		#region Action

		public static void Invoke(this IPeer peer, Action action)
		{
			peer.Write(new ActionCall
			{
				Delegate = action,
			});
		}

		public static void Invoke<T>(this IPeer peer, Action<T> action, T arg)
		{
			peer.Write(new ActionCall<T>
			{
				Delegate = action,
				Arg = arg,
			});
		}

		public static void Invoke<T1, T2>(this IPeer peer, Action<T1, T2> action, T1 arg1, T2 arg2)
		{
			peer.Write(new ActionCall<T1, T2>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
			});
		}

		public static void Invoke<T1, T2, T3>(this IPeer peer, Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3)
		{
			peer.Write(new ActionCall<T1, T2, T3>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
			});
		}

		public static void Invoke<T1, T2, T3, T4>(this IPeer peer, Action<T1, T2, T3, T4> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			peer.Write(new ActionCall<T1, T2, T3, T4>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4
			});
		}

		public static void Invoke<T1, T2, T3, T4, T5>(this IPeer peer, Action<T1, T2, T3, T4, T5> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
		{
			peer.Write(new ActionCall<T1, T2, T3, T4, T5>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5
			});
		}

		public static void Invoke<T1, T2, T3, T4, T5, T6>(this IPeer peer, Action<T1, T2, T3, T4, T5, T6> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
		{
			peer.Write(new ActionCall<T1, T2, T3, T4, T5, T6>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6
			});
		}
		
		public static void Invoke<T1, T2, T3, T4, T5, T6, T7>(this IPeer peer, Action<T1, T2, T3, T4, T5, T6, T7> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
		{
			peer.Write(new ActionCall<T1, T2, T3, T4, T5, T6, T7>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6,
				Arg7 = arg7
			});
		}
		
		public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8>(this IPeer peer, Action<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8)
		{
			peer.Write(new ActionCall<T1, T2, T3, T4, T5, T6, T7, T8>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6,
				Arg7 = arg7,
				Arg8 = arg8,
			});
		}
		
		#endregion

		#region Action Try
		 
		public static bool TryInvoke(this IPeer peer, Action action)
		{
			return peer.TryWrite(new ActionCall
			{
				Delegate = action,
			});
		}

		public static bool TryInvoke<T>(this IPeer peer, Action<T> action, T arg)
		{
			return peer.TryWrite(new ActionCall<T>
			{
				Delegate = action,
				Arg = arg,
			});
		}

		public static bool TryInvoke<T1, T2>(this IPeer peer, Action<T1, T2> action, T1 arg1, T2 arg2)
		{
			return peer.TryWrite(new ActionCall<T1, T2>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
			});
		}

		public static bool TryInvoke<T1, T2, T3>(this IPeer peer, Action<T1, T2, T3> action, T1 arg1, T2 arg2, T3 arg3)
		{
			return peer.TryWrite(new ActionCall<T1, T2, T3>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
			});
		}

		public static bool TryInvoke<T1, T2, T3, T4>(this IPeer peer, Action<T1, T2, T3, T4> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			return peer.TryWrite(new ActionCall<T1, T2, T3, T4>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4
			});
		}

		public static bool TryInvoke<T1, T2, T3, T4, T5>(this IPeer peer, Action<T1, T2, T3, T4, T5> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
		{
			return peer.TryWrite(new ActionCall<T1, T2, T3, T4, T5>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5
			});
		}

		public static bool TryInvoke<T1, T2, T3, T4, T5, T6>(this IPeer peer, Action<T1, T2, T3, T4, T5, T6> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
		{
			return peer.TryWrite(new ActionCall<T1, T2, T3, T4, T5, T6>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6
			});
		}
		
		public static bool TryInvoke<T1, T2, T3, T4, T5, T6, T7>(this IPeer peer, Action<T1, T2, T3, T4, T5, T6, T7> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
		{
			return peer.TryWrite(new ActionCall<T1, T2, T3, T4, T5, T6, T7>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6,
				Arg7 = arg7
			});
		}
		
		public static bool TryInvoke<T1, T2, T3, T4, T5, T6, T7, T8>(this IPeer peer, Action<T1, T2, T3, T4, T5, T6, T7, T8> action, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8)
		{
			return peer.TryWrite(new ActionCall<T1, T2, T3, T4, T5, T6, T7, T8>
			{
				Delegate = action,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6,
				Arg7 = arg7,
				Arg8 = arg8,
			});
		}
		
		#endregion

		#region Func

		public static void Invoke<TResult>(this IPeer peer, Func<TResult> func)
		{
			peer.Write(new FuncCall<TResult>
			{
				Delegate = func,
			});
		}

		public static void Invoke<T, TResult>(this IPeer peer, Func<T, TResult> func, T arg)
		{
			peer.Write(new FuncCall<T, TResult>
			{
				Delegate = func,
				Arg = arg,
			});
		}

		public static void Invoke<T1, T2, TResult>(this IPeer peer, Func<T1, T2, TResult> func, T1 arg1, T2 arg2)
		{
			peer.Write(new FuncCall<T1, T2, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
			});
		}

		public static void Invoke<T1, T2, T3, TResult>(this IPeer peer, Func<T1, T2, T3, TResult> func, T1 arg1, T2 arg2, T3 arg3)
		{
			peer.Write(new FuncCall<T1, T2, T3, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
			});
		}

		public static void Invoke<T1, T2, T3, T4, TResult>(this IPeer peer, Func<T1, T2, T3, T4, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			peer.Write(new FuncCall<T1, T2, T3, T4, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4
			});
		}

		public static void Invoke<T1, T2, T3, T4, T5, TResult>(this IPeer peer, Func<T1, T2, T3, T4, T5, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
		{
			peer.Write(new FuncCall<T1, T2, T3, T4, T5, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5
			});
		}

		public static void Invoke<T1, T2, T3, T4, T5, T6, TResult>(this IPeer peer, Func<T1, T2, T3, T4, T5, T6, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
		{
			peer.Write(new FuncCall<T1, T2, T3, T4, T5, T6, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6
			});
		}

		public static void Invoke<T1, T2, T3, T4, T5, T6, T7, TResult>(this IPeer peer, Func<T1, T2, T3, T4, T5, T6, T7, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
		{
			peer.Write(new FuncCall<T1, T2, T3, T4, T5, T6, T7, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6,
				Arg7 = arg7
			});
		}

		public static void Invoke<T1, T2, T3, T4, T5, T6, T7, T8, TResult>(this IPeer peer, Func<T1, T2, T3, T4, T5, T6, T7, T8, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8)
		{
			peer.Write(new FuncCall<T1, T2, T3, T4, T5, T6, T7, T8, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6,
				Arg7 = arg7,
				Arg8 = arg8
			});
		}

		#endregion

		#region Func Try

		public static bool TryInvoke<TResult>(this IPeer peer, Func<TResult> func)
		{
			return peer.TryWrite(new FuncCall<TResult>
			{
				Delegate = func,
			});
		}

		public static bool TryInvoke<T, TResult>(this IPeer peer, Func<T, TResult> func, T arg)
		{
			return peer.TryWrite(new FuncCall<T, TResult>
			{
				Delegate = func,
				Arg = arg,
			});
		}

		public static bool TryInvoke<T1, T2, TResult>(this IPeer peer, Func<T1, T2, TResult> func, T1 arg1, T2 arg2)
		{
			return peer.TryWrite(new FuncCall<T1, T2, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
			});
		}

		public static bool TryInvoke<T1, T2, T3, TResult>(this IPeer peer, Func<T1, T2, T3, TResult> func, T1 arg1, T2 arg2, T3 arg3)
		{
			return peer.TryWrite(new FuncCall<T1, T2, T3, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
			});
		}

		public static bool TryInvoke<T1, T2, T3, T4, TResult>(this IPeer peer, Func<T1, T2, T3, T4, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			return peer.TryWrite(new FuncCall<T1, T2, T3, T4, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4
			});
		}

		public static bool TryInvoke<T1, T2, T3, T4, T5, TResult>(this IPeer peer, Func<T1, T2, T3, T4, T5, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
		{
			return peer.TryWrite(new FuncCall<T1, T2, T3, T4, T5, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5
			});
		}

		public static bool TryInvoke<T1, T2, T3, T4, T5, T6, TResult>(this IPeer peer, Func<T1, T2, T3, T4, T5, T6, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
		{
			return peer.TryWrite(new FuncCall<T1, T2, T3, T4, T5, T6, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6
			});
		}

		public static bool TryInvoke<T1, T2, T3, T4, T5, T6, T7, TResult>(this IPeer peer, Func<T1, T2, T3, T4, T5, T6, T7, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
		{
			return peer.TryWrite(new FuncCall<T1, T2, T3, T4, T5, T6, T7, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6,
				Arg7 = arg7
			});
		}

		public static bool TryInvoke<T1, T2, T3, T4, T5, T6, T7, T8, TResult>(this IPeer peer, Func<T1, T2, T3, T4, T5, T6, T7, T8, TResult> func, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8)
		{
			return peer.TryWrite(new FuncCall<T1, T2, T3, T4, T5, T6, T7, T8, TResult>
			{
				Delegate = func,
				Arg1 = arg1,
				Arg2 = arg2,
				Arg3 = arg3,
				Arg4 = arg4,
				Arg5 = arg5,
				Arg6 = arg6,
				Arg7 = arg7,
				Arg8 = arg8
			});
		}

		#endregion

	}
}
