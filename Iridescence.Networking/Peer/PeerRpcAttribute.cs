﻿using System;

namespace Iridescence.Networking
{
	/// <summary>
	/// Marks a method as callable on any peer.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method)]
	public class PeerRpcAttribute : RpcAttribute
	{
		public override void VerifyCall(object invoker, ICall call)
		{
			IPeer peer = invoker as IPeer;
			if (peer == null)
				throw new RpcVerificationException("The call can only be invoked by peers.");

			if (call.IsCustomImplementation())
				throw new RpcVerificationException("User-defined implementations of ICall are not allowed.");
		}
	}
}
