﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Iridescence.Emit;
using Label = Iridescence.Emit.Label;

namespace Iridescence.Networking
{
	/// <summary>
	/// Invocation helper for methods marked with <see cref="OnNetworkedAttribute"/>.
	/// </summary>
	public static class OnNetworkedInvoker
	{
		#region Nested Types

		private delegate IReadOnlyList<ILifetimeObserver> CallbackInvoker(object target, NetworkContext context);

		#endregion

		#region Fields

		private static readonly ConcurrentDictionary<Type, CallbackInvoker> cache;

		private static readonly ConstructorInfo listConstructor;
		private static readonly MethodInfo listAddMethod;

		private static readonly IReadOnlyList<ILifetimeObserver> emptyObservers = new ILifetimeObserver[0];

		#endregion

		#region Constructors

		static OnNetworkedInvoker()
		{
			cache = new ConcurrentDictionary<Type, CallbackInvoker>();
			listConstructor = typeof(List<ILifetimeObserver>).GetConstructor(new[] {typeof(int)});
			listAddMethod = typeof(List<ILifetimeObserver>).GetMethod(nameof(List<ILifetimeObserver>.Add));
		}

		#endregion

		#region Methods

		private static CallbackInvoker generateInvoker(Type type)
		{
			IReadOnlyList<MethodInfo> methods = getMethods(type);
			if (methods.Count == 0)
				return null;

			int capacity = methods.Count(m => m.ReturnType != typeof(void));

			DynamicMethodBuilder emit = DynamicMethodBuilder.Create(MethodSignature.FromDelegate(typeof(CallbackInvoker)), type, $"{type.Name}_OnNetworked", options: ILBuilderOptions.Validate);
			Local targetLocal = emit.DeclareLocal(type);
			Local resultLocal = emit.DeclareLocal(typeof(List<ILifetimeObserver>));
			Local tempLocal = emit.DeclareLocal(typeof(ILifetimeObserver));

			// Cast target to type.
			emit.LoadArgument(0);
			emit.CastClass(type);
			emit.StoreLocal(targetLocal);

			// Create new list.
			emit.LoadConstant(capacity);
			emit.NewObject(listConstructor);
			emit.StoreLocal(resultLocal);

			foreach (MethodInfo method in methods)
			{
				// Invoke callback.
				emit.LoadLocal(targetLocal);
				emit.LoadArgument(1);
				emit.CallVirtual(method);

				if (method.ReturnType != typeof(void))
				{
					emit.StoreLocal(tempLocal);

					Label skipAdd = emit.DefineLabel();

					// Check if return value is null.
					emit.LoadLocal(tempLocal);
					emit.BranchIfFalse(skipAdd);

					// Add to list.
					emit.LoadLocal(resultLocal);
					emit.LoadLocal(tempLocal);
					emit.CallVirtual(listAddMethod);

					emit.MarkLabel(skipAdd);
				}
			}

			// Return list.
			emit.LoadLocal(resultLocal);
			emit.Return();
			return (CallbackInvoker)emit.CreateDelegate(typeof(CallbackInvoker));
		}

		private static IReadOnlyList<MethodInfo> getMethods(Type type)
		{
			List<MethodInfo> list = new List<MethodInfo>();
			int i = 0;

			// Go through type hierarchy and add methods to the list.
			// They are sorted in such a way that methods of a type are placed after methods of its base type.
			while (type != null)
			{
				foreach (MethodInfo method in type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly))
				{
					if (method.GetCustomAttribute<OnNetworkedAttribute>() == null)
						continue;

					ParameterInfo[] parameters = method.GetParameters();

					if (parameters.Length != 1 || parameters[0].ParameterType != typeof(NetworkContext))
					{
						throw new Exception($"Method \"{method}\" in \"{type}\" must have one parameter of type {nameof(NetworkContext)}.");
					}

					if (method.ReturnType != typeof(void) && method.ReturnType != typeof(ILifetimeObserver))
					{
						throw new Exception($"Method \"{method}\" in \"{type}\" must either be void or return an {nameof(ILifetimeObserver)}.");
					}

					// Insert into list.
					list.Insert(i++, method);
				}

				type = type.BaseType;

				// Begin at list start.
				i = 0;
			}

			return list;
		}

		/// <summary>
		/// Precaches the specified type.
		/// </summary>
		/// <param name="type"></param>
		public static void PreCache(Type type)
		{
			if (type == null)
				throw new ArgumentNullException(nameof(type));

			cache.GetOrAdd(type, generateInvoker);
		}

		/// <summary>
		/// Invokes all methods marked with the <see cref="OnNetworkedAttribute"/> and returns their results combined into a list.
		/// </summary>
		/// <param name="target">The object to synchronize.</param>
		/// <param name="context">The networking context.</param>
		/// <returns>All sync handler returned by the initializer methods.</returns>
		public static IReadOnlyList<ILifetimeObserver> Invoke(object target, NetworkContext context)
		{
			if (target == null)
				throw new ArgumentNullException(nameof(target));

			Type type = target.GetType();
			CallbackInvoker invoker = cache.GetOrAdd(type, generateInvoker);
			if (invoker == null)
				return emptyObservers;

			return invoker(target, context);
		}

		/// <summary>
		/// Invokes all methods marked with the <see cref="OnNetworkedAttribute"/> and returns their results combined into a single <see cref="ILifetimeObserver"/>.
		/// </summary>
		/// <param name="target">The object to synchronize.</param>
		/// <param name="context">The networking context.</param>
		/// <returns>A combined disposable object of all sync handlers returned by the initializer methods.</returns>
		public static ILifetimeObserver InvokeCombined(object target, NetworkContext context)
		{
			IReadOnlyList<ILifetimeObserver> list = Invoke(target, context);
			if (list.Count == 0)
				return null;

			if (list.Count == 1)
				return list[0];

			return new CompoundLifetimeObserver(list);
		}

		#endregion
	}
}
