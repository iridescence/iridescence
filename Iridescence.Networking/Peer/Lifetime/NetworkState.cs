﻿namespace Iridescence.Networking
{
	/// <summary>
	/// Defines the lifetime states of a networked object.
	/// </summary>
	public enum NetworkState
	{
		/// <summary>
		/// The object is connected.
		/// </summary>
		Connected = 1,

		/// <summary>
		/// The network was shutdown and the object is not connected anymore (e.g. because the peer has been disposed).
		/// </summary>
		Disconnected = 2,
		
		/// <summary>
		/// The object was garbage collected on the remote end. It is not "disconnected" yet as it can still be reused.
		/// </summary>
		Collected = 3,
	}
}
