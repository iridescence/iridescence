﻿using System.Collections.Generic;

namespace Iridescence.Networking
{
	/// <summary>
	/// A compound <see cref="ILifetimeObserver"/>.
	/// </summary>
	public class CompoundLifetimeObserver : ILifetimeObserver
	{
		#region Fields

		private readonly List<ILifetimeObserver> observers;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CompoundLifetimeObserver"/> class. 
		/// </summary>
		public CompoundLifetimeObserver(params ILifetimeObserver[] observers)
		{
			this.observers = new List<ILifetimeObserver>(observers);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CompoundLifetimeObserver"/> class. 
		/// </summary>
		public CompoundLifetimeObserver(IEnumerable<ILifetimeObserver> observers)
		{
			this.observers = new List<ILifetimeObserver>(observers);
		}
		
		#endregion

		#region Methods

		public bool OnStateChanged(object obj, NetworkState newState)
		{
			for (int i = 0; i < this.observers.Count; ++i)
			{
				if (!this.observers[i].OnStateChanged(obj, newState))
				{
					this.observers.RemoveAt(i);
					--i;
				}
			}

			return this.observers.Count > 0;
		}

		public override string ToString()
		{
			return string.Join(", ", this.observers);
		}

		#endregion
	}
}
