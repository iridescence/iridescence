﻿namespace Iridescence.Networking
{
	/// <summary>
	/// Represents an interface for objects that "observe" the networking lifetime of an object.
	/// </summary>
	public interface ILifetimeObserver
	{
		/// <summary>
		/// Called when the network state of the specified object has changed.
		/// </summary>
		/// <param name="obj">The object.</param>
		/// <param name="newState">The new <see cref="NetworkState"/> of that object.</param>
		/// <returns>False if the lifetime observer for the specified object should be unregistered. True otherwise.</returns>
		/// <remarks>
		/// If this method returns false, the lifetime observer will be unregistered for the object. Should the object be reused, its OnNetworked
		/// methods will be invoked again to retreive a new lifetime observer.
		/// </remarks>
		bool OnStateChanged(object obj, NetworkState newState);
	}
}
