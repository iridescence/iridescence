﻿namespace Iridescence.Networking
{
	/// <summary>
	/// Holds context information about object networking.
	/// </summary>
	public readonly struct NetworkContext
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets the peer that an object is being networked over.
		/// </summary>
		public IPeer Peer { get; }

		/// <summary>
		/// Gets the direction in which the object was used.
		/// </summary>
		public NetworkDirection Direction { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="NetworkContext"/> structure.
		/// </summary>
		public NetworkContext(IPeer peer, NetworkDirection direction)
		{
			this.Peer = peer;
			this.Direction = direction;
		}

		#endregion

		#region Methods

		#endregion
	}
}
