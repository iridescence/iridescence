﻿using System;
using System.Collections.Generic;

namespace Iridescence.Networking
{
	/// <summary>
	/// Manages network lifetime logic for objects and the invocation of methods marked with <see cref="OnNetworkedAttribute"/>.
	/// </summary>
	public class LifetimeManager : IDisposable
	{
		#region Fields

		private readonly WeakDictionary<object, ILifetimeObserver> dictionary;
		private ConcurrentFlag isDisposed;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the peer.
		/// </summary>
		public IPeer Peer { get; }

		/// <summary>
		/// Gets the objects currently tracked in this <see cref="LifetimeManager"/>.
		/// </summary>
		public IEnumerable<object> Objects => this.dictionary.Keys;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="LifetimeManager"/>.
		/// </summary>
		public LifetimeManager(IPeer peer)
		{
			this.Peer = peer;
			this.dictionary = new WeakDictionary<object, ILifetimeObserver>();
			this.isDisposed = new ConcurrentFlag();
		}

		~LifetimeManager()
		{
			if (this.isDisposed.TrySet())
			{
				this.Dispose(false);
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Adds multiple objects to the manager and sets their state to <see cref="NetworkState.Connected"/>.
		/// Invokes all of their OnNetworked callbacks if they're not registered yet.
		/// </summary>
		/// <param name="objects">The objects to add.</param>
		/// <param name="direction">The network direction.</param>
		public void Add(IEnumerable<object> objects, NetworkDirection direction)
		{
			NetworkContext context = new NetworkContext(this.Peer, direction);

			lock (this.dictionary)
			{
				foreach (object obj in objects)
				{
					if (this.dictionary.TryGetValue(obj, out ILifetimeObserver observer))
					{
						if (!observer.OnStateChanged(obj, NetworkState.Connected))
						{
							this.dictionary.Remove(obj);
						}
					}
					else
					{
						observer = OnNetworkedInvoker.InvokeCombined(obj, context);	
						
						if (observer == null)
							continue;

						if (observer.OnStateChanged(obj, NetworkState.Connected))
						{
							this.dictionary.Add(obj, observer);
						}
					}
				}
			}
		}

		/// <summary>
		/// Sets the state of the specified objects.
		/// </summary>
		/// <param name="objects"></param>
		/// <param name="state"></param>
		public void SetState(IEnumerable<object> objects, NetworkState state)
		{
			lock (this.dictionary)
			{
				foreach (object obj in objects)
				{
					if (this.dictionary.TryGetValue(obj, out ILifetimeObserver observer))
					{
						if (!observer.OnStateChanged(obj, state))
						{
							this.dictionary.Remove(obj);
						}
					}
				}
			}
		}
		
		/// <summary>
		/// Disposes all observer object instances, setting their states to <see cref="NetworkState.Disconnected"/>.
		/// </summary>
		public void DisconnectAll()
		{
			lock (this.dictionary)
			{
				foreach (KeyValuePair<object, ILifetimeObserver> pair in this.dictionary)
				{
					pair.Value.OnStateChanged(pair.Key, NetworkState.Disconnected);
				}
				
				this.dictionary.Clear();
			}
		}

		/// <summary>
		/// Disposes the <see cref="LifetimeManager"/>.
		/// </summary>
		public void Dispose()
		{
			if (this.isDisposed.TrySet())
			{
				GC.SuppressFinalize(this);
				this.Dispose(true);
			}
		}

		protected virtual void Dispose(bool managed)
		{
			if (managed)
			{
				this.DisconnectAll();	
			}
		}
		
		#endregion
	}
}
