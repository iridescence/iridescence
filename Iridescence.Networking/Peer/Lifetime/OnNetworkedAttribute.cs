﻿using System;

namespace Iridescence.Networking
{
	/// <summary>
	/// Specifies that the method marked with this attribute is called when an object is first networked over a <see cref="Peer"/>.
	/// </summary>
	/// <remarks>
	/// Use this attribute to implement custom networking logic for objects.
	/// The method must take one argument of type <see cref="NetworkContext"/> and must return either a <see cref="IDisposable"/> or nothing (void).
	/// The <see cref="IDisposable"/> returned by the method can be used as a networking observer or synchronizer.
	/// It is disposed once the object is no longer part of the networking heap (e.g. when the connection is closed).
	/// </remarks>
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class OnNetworkedAttribute : Attribute
	{

	}
}
