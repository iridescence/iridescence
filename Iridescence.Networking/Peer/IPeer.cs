﻿using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Interface for a networking peer.
	/// </summary>
	public interface IPeer
	{
		/// <summary>
		/// Attempts to read an object, returning a <see cref="UninitializedObjectGraph"/> that runs callbacks and events once its value is retrieved.
		/// Blocks the calling thread until an object has been read.
		/// </summary>
		/// <param name="lazy">The <see cref="UninitializedObjectGraph"/> that retrieves the object that has been read.</param>
		/// <returns>True, if an object was read. False, if the peer was closed/stream has ended.</returns>
		/// <remarks>
		/// If this method returns false, all further attempts to read objects from this peer will fail as well.
		/// The method does not return false on exceptions. They will be thrown and need to be handled.
		/// It only returns false when the peer was closed or the stream has ended, as there would be no other way to detect this case.
		/// </remarks>
		bool TryReadLazy(out UninitializedObjectGraph lazy);

		/// <summary>
		/// Tries to write an object.
		/// </summary>
		/// <param name="obj">The object to write.</param>
		/// <returns></returns>
		bool TryWrite(object obj);
		
		/// <summary>
		/// Closes the reader. No further messages can be read after this call.
		/// </summary>
		/// <returns></returns>
		bool CloseReader();

		/// <summary>
		/// Closes the writer. No further messages can be written after this call.
		/// </summary>
		/// <returns></returns>
		bool CloseWriter();
	}

	public static class PeerExtensions
	{
		/// <summary>
		/// Reads an object from the peer.
		/// Blocks the calling thread until an object has been read.
		/// </summary>
		/// <returns>The object that was read.</returns>
		/// <exception cref="PeerClosedException">The peer was closed.</exception>
		public static UninitializedObjectGraph ReadLazy(this IPeer peer)
		{
			if (!peer.TryReadLazy(out UninitializedObjectGraph lazy))
				throw new PeerClosedException();

			return lazy;
		}

		/// <summary>
		/// Attempts to read an object.
		/// Blocks the calling thread until an object has been read.
		/// </summary>
		/// <param name="obj">The object that has been read.</param>
		/// <returns>True, if an object was read. False, if the peer was closed/stream has ended.</returns>
		/// <remarks>
		/// If this method returns false, all further attempts to read objects from this peer will fail as well.
		/// The method does not return false on exceptions. They will be thrown and need to be handled.
		/// It only returns false when the peer was closed or the stream has ended, as there would be no other way to detect this case.
		/// </remarks>
		public static bool TryRead(this IPeer peer, out object obj)
		{
			if (!peer.TryReadLazy(out UninitializedObjectGraph lazy))
			{
				obj = null;
				return false;
			}

			obj = lazy.Value;
			return true;
		}

		/// <summary>
		/// Reads an object.
		/// Blocks the calling thread until an object has been read.
		/// </summary>
		/// <returns>The object that was read.</returns>
		/// <exception cref="PeerClosedException">The peer was closed.</exception>
		public static object Read(this IPeer peer)
		{
			if (!peer.TryRead(out object obj))
				throw new PeerClosedException();

			return obj;
		}

		/// <summary>
		/// Writes an object.
		/// </summary>
		/// <param name="obj"></param>
		/// <exception cref="PeerClosedException">The peer was closed.</exception>
		public static void Write(this IPeer peer, object obj)
		{
			if (!peer.TryWrite(obj))
				throw new PeerClosedException();
		}
	}
}
