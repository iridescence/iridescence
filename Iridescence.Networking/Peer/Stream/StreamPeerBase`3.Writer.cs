﻿using System.IO;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements peer networking packet writing.
	/// </summary>
	public partial class StreamPeerBase<TIdentifier, TInputStream, TOutputStream>
	{
		#region Fields

		private bool writerInitialized;
		private bool writerClosed;
	
		private readonly ObjectWriter<TOutputStream, object> writer;
		private ReferenceProviderTracker<TIdentifier> objectReferenceProvider;
		private TOutputStream outputStream;
		private readonly RecursionGuard writerGuard;

		private int writeID;
#if DEBUG
		private long? writeStartPos;
#endif

		#endregion

		#region Methods

		protected abstract TOutputStream CreateOutputStream(Stream stream, IReferenceProvider<object, TIdentifier> referenceProvider);

		public override bool CloseWriter()
		{
			using (this.WriteLock?.Lock())
			{
				if (this.writerClosed)
					return false;

				this.writerClosed = true;
				return true;
			}
		}

		private MemoryStream createPreamble()
		{
			MemoryStream temp = new MemoryStream();
			temp.WriteString(Magic);
			temp.WriteInt32V(Version);
			this.WriteHandshake(temp);
			return temp;
		}

		private void writePreamble()
		{
			bool success = false;
			
			try
			{
				using (MemoryStream temp = this.createPreamble())
				{
					this.stream.Write(temp.GetBuffer(), 0, (int)temp.Length);
					this.stream.Flush();
				}

				success = true;
			}
			finally
			{
				if (!success)
				{
					this.writerClosed = true;
				}
			}
		}

		private void initializeWriter()
		{
			StreamPeerTrace.Instance.InitWriter(this.streamID);

			this.objectReferenceProvider = new ReferenceProviderTracker<TIdentifier>(this.Heap, true);

			this.outputStream = this.CreateOutputStream(this.stream, this.objectReferenceProvider);
			
			this.writePreamble();
			this.writerInitialized = true;
		}

		/// <summary>
		/// Writes custom handshake data to the handshake packet.
		/// </summary>
		/// <param name="handshakePacket"></param>
		protected virtual void WriteHandshake(Stream handshakePacket)
		{

		}
		
		protected virtual void OnBeginWrite(int packetID, object obj)
		{
#if DEBUG
			// For tracing.
			this.writeStartPos = this.outputStream.Position;
#endif
		}

		protected virtual void OnEndWrite(int packetID, object obj)
		{
#if DEBUG
			// For tracing.
			long length = -1;
			if (this.writeStartPos != null)
				length = this.outputStream.Position - this.writeStartPos.Value;

			this.writeStartPos = null;

			if (obj is ICall call)
			{
				StreamPeerTrace.Instance.WriteCallObject(this.streamID, packetID, obj.GetType().FullName, length, call.Method.DeclaringType?.FullName, call.Method.Name);
			}
			else
			{
				StreamPeerTrace.Instance.WriteObject(this.streamID, packetID, obj?.GetType().FullName, length);
			}
#endif
		}

		/// <inheritdoc/>
		public override bool TryWrite(object obj)
		{
			ObjectInfoEventArgs<TIdentifier> e;

			using (this.WriteLock?.Lock())
			{
				using (this.writerGuard.Protect())
				{
					if (!this.writerInitialized)
						this.initializeWriter();

					if (this.writerClosed)
						return false;

					bool success = false;

					try
					{
						int packetID = this.writeID++;
						this.OnBeginWrite(packetID, obj);

						this.writer(this.outputStream, obj);
						this.outputStream.Flush(true);

						this.OnEndWrite(packetID, obj);

						success = true;

						// Fire event.
						e = new ObjectInfoEventArgs<TIdentifier>(this.objectReferenceProvider.Objects);
					}
					finally
					{
						// Clear tracker.
						this.objectReferenceProvider.Clear();

						if (!success)
						{
							// If an exception occured while writing/serializing, the writer state might be undefined and we can't allow any further writes.
							this.writerClosed = true;
						}
					}
				}
			}

			this.OnGraphWritten(e);

			return true;
		}

		#endregion
	}
}