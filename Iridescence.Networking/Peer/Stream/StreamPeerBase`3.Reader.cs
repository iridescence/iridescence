﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements peer networking packet reading.
	/// </summary>
	public partial class StreamPeerBase<TIdentifier, TInputStream, TOutputStream>
	{
		#region Fields

		private bool readerInitialized;
		private bool readerClosed;

		private readonly ObjectReader<TInputStream, object> reader;
		private ReferenceResolverTracker<TIdentifier> objectReferenceResolver;
		private ProxyStream inputProxy;
		private TInputStream inputStream;
		private readonly RecursionGuard readerGuard;

		private int readID;
#if DEBUG
		private long? readStartPos;
#endif

		#endregion

		#region Methods

		protected abstract TInputStream CreateInputStream(Stream stream, IReferenceResolver<object, TIdentifier> referenceResolver);

		public override bool CloseReader()
		{
			using (this.ReadLock?.Lock())
			{
				if (this.readerClosed)
					return false;
				
				this.readerClosed = true;
				return true;
			}
		}

		private void initializeReader()
		{
			StreamPeerTrace.Instance.InitReader(this.streamID);

			this.objectReferenceResolver = new ReferenceResolverTracker<TIdentifier>(this.Heap, true);
			this.inputProxy = new ProxyStream(this.stream, null);
			this.inputStream = this.CreateInputStream(this.inputProxy, this.objectReferenceResolver);

			this.readPreamble();
			this.readerInitialized = true;
		}

		private void readPreamble()
		{
			bool success = false;
			try
			{
				// Read magic. Allow for immediate end-of-stream.
				if (!this.inputStream.TryReadString(Magic.Length, out string magic))
				{
					success = true;
					this.readerClosed = true;
					return;
				}
				
				// Check magic.
				if (!string.Equals(magic, Magic, StringComparison.Ordinal))
				{
					throw new NetworkingException("Invalid magic.");
				}

				// Check version.
				int version = this.inputStream.ReadInt32V();
				if (version != Version)
				{
					throw new NetworkingException("Unsupported version.");
				}
			
				this.ReadHandshake(this.inputStream);
				
				success = true;
			}
			finally
			{
				if (!success)
				{
					this.readerClosed = true;
				}
			}
		}

		/// <summary>
		/// Reads custom handshake data from the handshake packet.
		/// </summary>
		/// <param name="handshakePacket"></param>
		protected virtual void ReadHandshake(Stream handshakePacket)
		{

		}

		/// <summary>
		/// Called right before an object is read from the underlying stream.
		/// </summary>
		protected virtual void OnBeginRead(int packetID)
		{
#if DEBUG
			// For tracing.
			this.readStartPos = this.inputStream.Position;
#endif
		}

		/// <summary>
		/// Called right after an object has been read and deserialized from the stream, but before anything has been initialized.
		/// </summary>
		protected virtual void OnEndRead(int packetID, object result)
		{
#if DEBUG
			// For tracing.
			long length = -1;
			if (this.readStartPos != null)
				length = this.inputStream.Position - this.readStartPos.Value;

			this.readStartPos = null;

			if (result is ICall call)
			{
				StreamPeerTrace.Instance.ReadCallObject(this.streamID, packetID, result.GetType().FullName, length, call.Method.DeclaringType?.FullName, call.Method.Name);
			}
			else
			{
				StreamPeerTrace.Instance.ReadObject(this.streamID, packetID, result?.GetType().FullName, length);
			}
#endif
		}

		/// <summary>
		/// Runs the <see cref="IDeserializationCallback">callbacks</see>.
		/// </summary>
		/// <param name="callbacks"></param>
		protected virtual void InvokeCallbacks(IDeserializationCallback[] callbacks)
		{
			// Run callbacks in reverse.
			// Running them in reverse makes more sense because "inner" objects will have their callback run
			// before their outer/containing object because they are usually stored later in the stream.
			// However, the order in which OnDeserialization should be run is officially not defined (source MSDN).
			// Implementors shouldn't rely on the order.
			for (int i = callbacks.Length - 1; i >= 0; --i)
			{
				callbacks[i].OnDeserialization(this);
			}
		}

		/// <inheritdoc/>
		public override bool TryReadLazy(out UninitializedObjectGraph lazy)
		{
			lazy = null;

			using (this.ReadLock?.Lock())
			using (this.readerGuard.Protect())
			{
				if (!this.readerInitialized)
					this.initializeReader();

				if (this.readerClosed)
					return false;

				bool success = false;

				try
				{
					if (!this.inputStream.TryPeek())
						return false;

					int packetID = this.readID++;
					this.OnBeginRead(packetID);

					// Deserialize object.
					this.reader(this.inputStream, out object obj);

					this.OnEndRead(packetID, obj);

					// Retrieve callbacks.
					IDeserializationCallback[] callbacks = this.inputStream.GetDeserializationCallbacksAndClear();

					success = true;

					// Get objects from tracker.
					ObjectInfoEventArgs<TIdentifier> e = new ObjectInfoEventArgs<TIdentifier>(this.objectReferenceResolver.Objects);

					// Create lazy that invokes the event.
					lazy = new UninitializedObjectGraph(obj, () =>
					{
						// Invoke IDeserializationCallbacks.
						this.InvokeCallbacks(callbacks);

						// Fire event.
						this.OnGraphRead(e);

						return obj;
					});
				}
				finally
				{
					// Clear tracker.
					this.objectReferenceResolver.Clear();

					if (!success)
					{
						this.readerClosed = true;
					}
				}

				return true;
			}
		}

		#endregion
	}
}
