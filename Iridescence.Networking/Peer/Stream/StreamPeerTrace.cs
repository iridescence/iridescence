﻿using System.Diagnostics.Tracing;

namespace Iridescence.Networking
{
	[EventSource(Name = "Iridescence-Networking-StreamPeerTrace")]
	internal sealed class StreamPeerTrace : EventSource
	{
		#region Fields

		public static readonly StreamPeerTrace Instance = new StreamPeerTrace();

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamPeerTrace"/> class. 
		/// </summary>
		public StreamPeerTrace()
		{

		}

		#endregion

		#region Methods

		[Event(1)]
		public void Create(int streamID, string streamType)
		{
			this.WriteEvent(1, streamID, streamType);
		}

		[Event(2)]
		public void Dispose(int streamID, bool managed)
		{
			this.WriteEvent(2, streamID, managed);
		}

		[Event(10)]
		public void InitWriter(int streamID)
		{
			this.WriteEvent(10, streamID);
		}

		[Event(11)]
		public void WriteObject(int streamID, int packetID, string objType, long length)
		{
			if (!this.IsEnabled())
				return;

			this.WriteEvent(11, streamID, packetID, objType, length);
		}

		[Event(12)]
		public void WriteCallObject(int streamID, int packetID, string objType, long length, string declType, string method)
		{
			if (!this.IsEnabled())
				return;

			this.WriteEvent(12, streamID, packetID, objType, length, declType, method);
		}

		[Event(20)]
		public void InitReader(int streamID)
		{
			this.WriteEvent(20, streamID);
		}

		[Event(21)]
		public void ReadObject(int streamID, int packetID, string objType, long length)
		{
			if (!this.IsEnabled())
				return;

			this.WriteEvent(21, streamID, packetID, objType, length);
		}

		[Event(22)]
		public void ReadCallObject(int streamID, int packetID, string objType, long length, string declType, string method)
		{
			if (!this.IsEnabled())
				return;

			this.WriteEvent(22, streamID, packetID, objType, length, declType, method);
		}

		#endregion
	}
}
