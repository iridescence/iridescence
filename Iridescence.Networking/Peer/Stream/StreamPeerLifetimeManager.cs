﻿using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements <see cref="LifetimeManager"/> for <see cref="StreamPeer"/>.
	/// </summary>
	public class StreamPeerLifetimeManager<TIdentifier> : LifetimeManager
	{
		#region Fields

		private readonly StreamPeerBase<TIdentifier> peer;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamPeerLifetimeManager{T}"/> class.
		/// This subscribes to the <see cref="StreamPeer.GraphRead"/> and <see cref="StreamPeer.GraphWritten"/>
		/// events to track incoming and outgoing objects, invoking their callbacks whenever appropriate.
		/// </summary>
		public StreamPeerLifetimeManager(StreamPeerBase<TIdentifier> peer)
			: base(peer)
		{
			this.peer = peer;
			this.peer.GraphRead += this.onGraphRead;
			this.peer.GraphWritten += this.onGraphWritten;
			this.peer.Heap.ItemsInvalidated += this.onItemsInvalidated;
		}

		private void onGraphRead(object sender, ObjectInfoEventArgs<TIdentifier> e)
		{
			// Add only new objects to observer manager.
			this.Add(e.Objects.Where(o => o.IsNew).Select(o => o.Object), NetworkDirection.Incoming);
		}

		private void onGraphWritten(object sender, ObjectInfoEventArgs<TIdentifier> e)
		{
			// Add only new objects to observer manager.
			this.Add(e.Objects.Where(o => o.IsNew).Select(o => o.Object), NetworkDirection.Outgoing);
		}

		private void onItemsInvalidated(IEnumerable<KeyValuePair<TIdentifier, object>> items)
		{
			// Set state of invalidated objects to Collected.
			this.SetState(items.Select(item => item.Value), NetworkState.Collected);
		}

		#endregion

		#region Methods

		protected override void Dispose(bool managed)
		{
			base.Dispose(managed);
			this.peer.GraphRead -= this.onGraphRead;
			this.peer.GraphWritten -= this.onGraphWritten;
			this.peer.Heap.ItemsInvalidated -= this.onItemsInvalidated;
		}

		#endregion
	}
}
