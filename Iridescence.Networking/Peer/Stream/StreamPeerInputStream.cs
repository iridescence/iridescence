﻿using System;
using System.IO;
using System.Reflection;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements an <see cref="BufferedInputStream"/> for peers.
	/// </summary>
	public class StreamPeerInputStream : PeerInputStream<PeerObjectIdentifier>
	{
		public StreamPeerInputStream(Stream stream, int initialCapacity, bool leaveOpen, IReferenceResolver<object, PeerObjectIdentifier> objectReferenceResolver, Func<AssemblyName, Assembly> assemblyResolver)
			: base(stream, initialCapacity, leaveOpen, objectReferenceResolver, assemblyResolver)
		{

		}

		public override PeerObjectIdentifier ReadIdentifier()
		{
			return new PeerObjectIdentifier(this.ReadVarUInt());
		}
	}
}
