﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	public abstract class StreamPeerBase<TIdentifier> : IPeer, IDisposable
	{
		#region Events

		/// <summary>
		/// Occurs after an object graph was read.
		/// </summary>
		public event EventHandler<ObjectInfoEventArgs<TIdentifier>> GraphRead;

		/// <summary>
		/// Occurs after an object graph was written.
		/// </summary>
		public event EventHandler<ObjectInfoEventArgs<TIdentifier>> GraphWritten;

		#endregion

		#region Fields

		protected const string Magic = "Iridescence";
		protected const int Version = 1;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the heap used for the object stream.
		/// </summary>
		public Heap<TIdentifier> Heap { get; }

		/// <summary>
		/// Gets or sets the peer's read lock.
		/// If not null, this object instance is locked each time the peer is reading from the stream and can be used externally to serialize access to the stream.
		/// </summary>
		public ILockMechanism ReadLock { get; set; }

		/// <summary>
		/// Gets or sets the peer's write lock.
		/// If not null, this object instance is locked each time the peer is writing to the stream and can be used externally to serialize access to the stream.
		/// </summary>
		public ILockMechanism WriteLock { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamPeerBase{T}"/> class. 
		/// </summary>
		protected StreamPeerBase(Heap<TIdentifier> heap)
		{
			this.Heap = heap;
		}

		#endregion

		#region Methods

		public abstract bool TryReadLazy(out UninitializedObjectGraph lazy);

		public abstract bool TryWrite(object obj);

//		public void Write(object obj) => ((IPeer)this).Write(obj);

		public abstract bool CloseReader();
		
		public abstract bool CloseWriter();
	
		/// <summary>
		/// Called after an object graph was read.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnGraphRead(ObjectInfoEventArgs<TIdentifier> e)
		{
			this.GraphRead?.Invoke(this, e);
		}

		/// <summary>
		/// Called after an object graph was written.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnGraphWritten(ObjectInfoEventArgs<TIdentifier> e)
		{
			this.GraphWritten?.Invoke(this, e);
		}
		
		public virtual void Dispose()
		{
			this.Heap.Dispose();
		}

		/// <summary>
		/// Periodically collects garbage for the specified peer in the background.
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <param name="interval"></param>
		/// <returns></returns>
		public async Task CollectGarbageAsync(CancellationToken cancellationToken, TimeSpan? interval = null)
		{
			TimeSpan t = interval ?? TimeSpan.FromMilliseconds(250);

			for (;;)
			{
				cancellationToken.ThrowIfCancellationRequested();

				using (DisposableLock.Lock(this.WriteLock))
				{
					if(!this.TryWrite(HeapUsageReport<TIdentifier>.Create(this.Heap)))
						break;
				}

				await Task.Delay(t, cancellationToken);
			}
		}

		/// <summary>
		/// Reads all objects from the specified peer, but handles <see cref="HeapUsageReport{TIdentifier}"/>.
		/// </summary>
		public IEnumerable<UninitializedObjectGraph> ReadAll(CancellationToken cancellationToken)
		{
			// Main loop.
			while (this.TryReadLazy(out UninitializedObjectGraph lazy))
			{
				if (lazy.UninitializedRoot is HeapUsageReport<TIdentifier>)
				{
					HeapUsageReport<TIdentifier> report = (HeapUsageReport<TIdentifier>)lazy.Value;
					//Trace.WriteVerbose($"Received heap usage report ({report.Count} objects).");
					report.Apply(this.Heap);
				}
				else
				{
					yield return lazy;
				}

				cancellationToken.ThrowIfCancellationRequested();
			}

			Trace.WriteVerbose("End of stream.");
		}

		/// <summary>
		/// Reads all objects from the specified peer, but handles <see cref="HeapUsageReport{TIdentifier}"/>.
		/// </summary>
		public IEnumerable<UninitializedObjectGraph> ReadAllAndCollectGarbage(CancellationToken cancellationToken, TimeSpan? gcInterval = null)
		{
			// Begin collecting garbage.
			CancellationTokenSource gcCancellationTokenSource = new CancellationTokenSource();
			Task gcTask = this.CollectGarbageAsync(gcCancellationTokenSource.Token, gcInterval);

			try
			{
				foreach (UninitializedObjectGraph g in this.ReadAll(cancellationToken))
				{
					yield return g;
				}
			}
			finally
			{
				gcCancellationTokenSource.Cancel();
				gcTask.WaitAndCatchCancellation();
			}
		}

		#endregion
	}
}
