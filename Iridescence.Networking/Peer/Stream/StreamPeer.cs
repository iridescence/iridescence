﻿using System.IO;
using System.Reflection;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements a standard peer derived from <see cref="StreamPeerBase{T}"/>.
	/// </summary>
	public class StreamPeer : StreamPeerBase<PeerObjectIdentifier, StreamPeerInputStream, StreamPeerOutputStream>
	{
		#region Fields

		private const int initialBuffer = 8192;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public StreamPeer(
			Role role, Stream stream, bool leaveOpen,
			ObjectWriter<PeerOutputStream<PeerObjectIdentifier>, object> writer, 
			ObjectReader<PeerInputStream<PeerObjectIdentifier>, object> reader)
			: base(stream, leaveOpen, writer, reader, new PeerHeap(role))
		{

		}

		public StreamPeer(Role role, Stream stream, bool leaveOpen)
			: this(role, stream, leaveOpen, StreamPeerSerializer.Writer, StreamPeerSerializer.Reader)
		{

		}

		#endregion

		#region Methods

		protected override StreamPeerInputStream CreateInputStream(Stream stream, IReferenceResolver<object, PeerObjectIdentifier> referenceResolver)
		{
			return new StreamPeerInputStream(stream, initialBuffer, true, referenceResolver, Assembly.Load);
		}

		protected override StreamPeerOutputStream CreateOutputStream(Stream stream, IReferenceProvider<object, PeerObjectIdentifier> referenceProvider)
		{
			return new StreamPeerOutputStream(stream, initialBuffer, true, referenceProvider);
		}

		#endregion
	}
}
