﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Networking
{
	/// <summary>
	/// Lock mechanism that supports synchronous and asynchronous locking.
	/// </summary>
	public interface ILockMechanism
	{
		IDisposable Lock();

		ValueTask<IDisposable> LockAsync();
	}
}
