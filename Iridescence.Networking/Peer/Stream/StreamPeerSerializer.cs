﻿using System.Runtime.Serialization;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;

namespace Iridescence.Networking
{
	/// <summary>
	/// Contains the default implementation of the <see cref="StreamPeer"/> serializer.
	/// </summary>
	public static class StreamPeerSerializer
	{
		private static readonly StreamingContext streamingContext = new StreamingContext(StreamingContextStates.CrossMachine);

		/// <summary>
		/// Gets the default <see cref="CompilerConfiguration"/> for the serializer/deserializer compiler.
		/// </summary>
		public static CompilerConfiguration BuilderConfiguration { get; } = new CompilerConfiguration()
			.WithArbiter(new SerializableAttributeArbiter(SerializationArbiterFlags.AllowMemory | SerializationArbiterFlags.AllowRuntimeMetadata, typeof(ISerializable)))
			.WithIOModule(new BufferedStreamIOModule())
			.WithContentModule(new SerializationEventContentModule(streamingContext))
			.WithContentModule(new DeserializationCallbackContentModule())
			.WithContentModule(new PrimitiveContentModule())
			.WithContentModule(new StringContentModule())
			.WithContentModule(new NullableContentModule())
			.WithContentModule(new ArrayContentModule())
			.WithContentModule(new MemoryContentModule())
			.WithContentModule(new DelegateContentModule())
			.WithContentModule(new MetadataContentModule())
			.WithContentModule(new MethodInvocationContentModule())
			.WithContentModule(new ISerializableModule(streamingContext, new FormatterConverter()))
			.WithContentModule(new MemberContentModule())
			.WithMemberModule(new InlineMemberModule())
			.WithMemberModule(new ReferencingMemberModule(typeof(PeerObjectIdentifier)));

		static StreamPeerSerializer()
		{
			Compiler compiler = new Compiler(BuilderConfiguration);
			WriterBuilder = compiler.To<PeerOutputStream<PeerObjectIdentifier>>();
			ReaderBuilder = compiler.From<PeerInputStream<PeerObjectIdentifier>>();
			Writer = WriterBuilder.For<object>().Member.NonGeneric;
			Reader = ReaderBuilder.For<object>().Deferred<EmptyDeferredState>().Member.NonGeneric.UnDefer();
		}

		/// <summary>
		/// Gets the default <see cref="WriterBuilder"/>.
		/// </summary>
		public static IWriterCompiler<PeerOutputStream<PeerObjectIdentifier>> WriterBuilder { get; }

		/// <summary>
		/// Gets the default <see cref="ReaderBuilder"/>.
		/// </summary>
		public static IReaderCompiler<PeerInputStream<PeerObjectIdentifier>> ReaderBuilder { get; } 

		/// <summary>
		/// Gets the default <see cref="ObjectWriter{TStream,T}"/>.
		/// </summary>
		public static ObjectWriter<PeerOutputStream<PeerObjectIdentifier>, object> Writer { get; }

		/// <summary>
		/// Gets the default <see cref="ObjectReader{TStream,T}"/>.
		/// </summary>
		public static ObjectReader<PeerInputStream<PeerObjectIdentifier>, object> Reader { get; }

	}
}
