﻿using System.IO;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements a Peer that reads from and writes to a <see cref="Stream"/> using a serialization.
	/// A <see cref="PeerHeap"/> is used to keep track of objects and references.
	/// </summary>
	public abstract partial class StreamPeerBase<TIdentifier, TInputStream, TOutputStream> : StreamPeerBase<TIdentifier>
		where TInputStream : PeerInputStream<TIdentifier>
		where TOutputStream : PeerOutputStream<TIdentifier>
	{
		#region Fields

		private readonly Stream stream;
		private readonly bool leaveOpen;
		private readonly int streamID;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StreamPeer"/> class. 
		/// </summary>
		/// <param name="stream">The stream to read from and write to.</param>
		/// <param name="leaveOpen">Specifies whether the stream should be left open when the peer is disposed.</param>
		/// <param name="writer">The serializer implementation.</param>
		/// <param name="reader">The deserializer implementation.</param>
		/// <param name="heap"></param>
		protected StreamPeerBase(
			Stream stream, bool leaveOpen,
			ObjectWriter<TOutputStream, object> writer,
			ObjectReader<TInputStream, object> reader,
			Heap<TIdentifier> heap)
			: base(heap)
		{
			this.streamID = Trace.GetTraceObjectID(this);
			StreamPeerTrace.Instance.Create(this.streamID, this.GetType().FullName);

			this.stream = stream;
			this.leaveOpen = leaveOpen;

			this.writer = writer;
			this.reader = reader;

			this.readerGuard = new RecursionGuard();
			this.writerGuard = new RecursionGuard();
		}

		#endregion

		#region Methods

		public override void Dispose()
		{
			StreamPeerTrace.Instance.Dispose(this.streamID, true);

			this.writerClosed = true;
			this.readerClosed = true;
			
			base.Dispose();

			if (!this.leaveOpen)
				this.stream.Dispose();
		}
		
		#endregion
	}
}
