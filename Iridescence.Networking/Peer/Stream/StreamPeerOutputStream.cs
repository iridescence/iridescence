﻿using System.IO;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements an <see cref="BufferedOutputStream"/> for peers.
	/// </summary>
	public class StreamPeerOutputStream : PeerOutputStream<PeerObjectIdentifier>
	{
		public StreamPeerOutputStream(Stream stream, int initialCapacity, bool leaveOpen, IReferenceProvider<object, PeerObjectIdentifier> objectReferenceProvider)
			: base(stream, initialCapacity, leaveOpen, objectReferenceProvider)
		{

		}

		public override void WriteIdentifier(PeerObjectIdentifier id)
		{
			this.WriteVarUInt(id.Value);
		}
	}
}
