﻿using System;
using System.Threading.Tasks;
using Nito.AsyncEx;

namespace Iridescence.Networking
{
	public class DefaultLockMechanism : ILockMechanism
	{
		private readonly AsyncLock obj;

		public DefaultLockMechanism()
		{
			this.obj = new AsyncLock();
		}

		public IDisposable Lock()
		{
			return this.obj.Lock();
		}

		public async ValueTask<IDisposable> LockAsync()
		{
			return await this.obj.LockAsync();
		}
	}
}
