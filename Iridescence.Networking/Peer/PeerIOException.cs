﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Exception that is thrown when peer IO fails.
	/// </summary>
	[Serializable]
	public class PeerIOException : NetworkingException
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		protected PeerIOException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			
		}

		/// <summary>
		/// Initializes a new instance of the PeerIOException class. 
		/// </summary>
		public PeerIOException(string message)
			: base(message)
		{

		}

		/// <summary>
		/// Initializes a new instance of the PeerIOException class. 
		/// </summary>
		public PeerIOException(string message, Exception innerException)
			: base(message, innerException)
		{

		}

		#endregion

		#region Methods

		#endregion
	}
}
