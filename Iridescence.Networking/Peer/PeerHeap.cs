﻿using System.Collections.Concurrent;
using System.Threading;

namespace Iridescence.Networking
{
	/// <summary>
	/// Implements a heap with <see cref="Role"/>-based identifiers.
	/// </summary>
	public sealed class PeerHeap : Heap<PeerObjectIdentifier>
	{
		#region Fields

		//private readonly BinaryTreeIntegerBag ids;

		private readonly ConcurrentStack<long> freeIDs;
		private long nextID;

		private static readonly PeerObjectIdentifier nullId = new PeerObjectIdentifier(Role.None, 0);

		#endregion

		#region Properties

		/// <summary>
		/// Gets the role assigned to the <see cref="PeerHeap"/>.
		/// </summary>
		public Role Role { get; }

		protected override PeerObjectIdentifier NullIdentifier => nullId;

		#endregion

		#region Constructors

		public PeerHeap(Role role)
		{
			this.Role = role;
			//this.ids = new BinaryTreeIntegerBag();
			this.freeIDs = new ConcurrentStack<long>();
		}

		#endregion

		#region Methods

		protected override PeerObjectIdentifier CreateIdentifier(object obj)
		{
			if (!this.freeIDs.TryPop(out long id))
			{
				id = Interlocked.Increment(ref this.nextID);
			}

			return new PeerObjectIdentifier(this.Role, id);
		}

		protected override void RemoveIdentifier(PeerObjectIdentifier id)
		{
			if (id.Role != this.Role)
				return;

			this.freeIDs.Push(id.ID);
		}

		public override string ToString()
		{
			return $"{this.Role} Heap";
		}

		#endregion
	}
}
