﻿namespace Iridescence.Networking
{
	/// <summary>
	/// Internal RPC attribute for responses of <see cref="AsyncCallBase"/>.
	/// </summary>
	internal sealed class AsyncCallResponseRpcAttribute : RpcAttribute
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AsyncCallResponseRpcAttribute"/> class. 
		/// </summary>
		public AsyncCallResponseRpcAttribute()
		{

		}

		#endregion

		#region Methods

		public override void VerifyCall(object invoker, ICall call)
		{
			IPeer peer = invoker as IPeer;
			if (peer == null)
				throw new RpcVerificationException("Async calls can only be invoked by peers.");
			
			AsyncCallBase acb = call.Target as AsyncCallBase;
			if (acb == null)
				throw new RpcVerificationException("Async calls can only be invoked on AsyncCallBase.");

			if (acb.Peer != peer)
				throw new RpcVerificationException("Invalid peer.");

			if (acb.Direction != NetworkDirection.Outgoing && !acb.IsInvoked)
				throw new RpcVerificationException("Async call responses can not be called on inbound async calls.");
		}
		
		#endregion
	}
}
