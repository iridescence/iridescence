﻿using System;
using System.Reflection;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// Base class for asynchronous method invokation over a peer.
	/// </summary>
	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public abstract class AsyncCallBase : ICall
	{
		#region Fields

		[NonSerialized] private IPeer peer;
		private ConcurrentFlag isInvoked;
		private NetworkDirection? direction;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the peer.
		/// </summary>
		public IPeer Peer => this.peer;

		/// <summary>
		/// Gets the method to invoke.
		/// </summary>
		public abstract MethodBase Method { get; }

		/// <summary>
		/// Gets the target object.
		/// </summary>
		public abstract object Target { get; }

		internal NetworkDirection? Direction => this.direction;
		
		internal bool IsInvoked => this.isInvoked.IsSet;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AsyncCall class. 
		/// </summary>
		protected AsyncCallBase()
		{
			this.isInvoked = new ConcurrentFlag();
		}

		#endregion

		#region Methods

		[OnNetworked]
		private void onNetworked(NetworkContext context)
		{
			this.peer = context.Peer;
			this.direction = context.Direction;
		}

		protected abstract void HandleInvocation();

		/// <summary>
		/// Invokes the method.
		/// </summary>
		public void Invoke()
		{
			if (this.peer == null)
				throw new InvalidOperationException("No peer is associated with this call.");
			
			if (this.direction != NetworkDirection.Incoming)
				throw new InvalidOperationException("AsyncCall must be invoked on the remote peer.");

			if (!this.isInvoked.TrySet())
				throw new InvalidOperationException("Method was already called.");

			this.HandleInvocation();
		}
		
		#endregion
	}
}
