﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Networking
{
	/// <summary>
	/// Base class for asynchronous method invokation with return value.
	/// </summary>
	[Serializable]
	public abstract class AsyncCall<TResult> : AsyncCallBase
	{
		#region Fields

		[NonSerialized] private readonly TaskCompletionSource<TResult> taskCompletionSource;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the method invokation task.
		/// </summary>
		public Task<TResult> Task => this.taskCompletionSource?.Task;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AsyncCall class. 
		/// </summary>
		protected AsyncCall()
		{
			this.taskCompletionSource = new TaskCompletionSource<TResult>();
		}

		#endregion

		#region Methods

		[AsyncCallResponseRpc]
		private void setResult(TResult result)
		{
			// The task completion source is null if the async call was not constructed on this peer.
			// That means it was sent from a remote peer directly (not as part of a response), which can only
			// happen if an AsyncCall is collected locally before the result has been received, in which
			// case the remote peer sents a "new" AsyncCall object instance.
			this.taskCompletionSource?.SetResult(result);
		}

		[AsyncCallResponseRpc]
		private void setException(Exception exception)
		{
			this.taskCompletionSource?.SetException(exception);
		}

		protected override void HandleInvocation()
		{
			TResult result;
			try
			{
				result = this.DoInvoke();
			}
			catch (Exception ex)
			{
				this.Peer.TryInvoke(this.setException, ex);
				return;
			}

			this.Peer.TryInvoke(this.setResult, result);
		}

		protected abstract TResult DoInvoke();

		#endregion
	}
}
