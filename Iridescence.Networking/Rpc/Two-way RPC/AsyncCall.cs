﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Networking
{
	/// <summary>
	/// Base class for asynchronous method invokation without return value.
	/// </summary>
	[Serializable]
	public abstract class AsyncCall : AsyncCallBase
	{
		#region Fields

		[NonSerialized] private readonly TaskCompletionSource<object> taskCompletionSource;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the method invokation task.
		/// </summary>
		public Task Task => this.taskCompletionSource.Task;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AsyncCall class. 
		/// </summary>
		protected AsyncCall()
		{
			this.taskCompletionSource = new TaskCompletionSource<object>();
		}

		#endregion

		#region Methods

		protected abstract void DoInvoke();

		[AsyncCallResponseRpc]
		private void setCompleted()
		{
			this.taskCompletionSource.SetResult(null);
		}

		[AsyncCallResponseRpc]
		private void setException(Exception exception)
		{
			this.taskCompletionSource.SetException(exception);
		}

		protected override void HandleInvocation()
		{
			try
			{
				this.DoInvoke();
			}
			catch (Exception ex)
			{
				this.Peer.TryInvoke(this.setException, ex);
				return;
			}

			this.Peer.TryInvoke(this.setCompleted);
		}
		
		#endregion
	}
}
