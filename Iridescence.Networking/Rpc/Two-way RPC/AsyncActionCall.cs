﻿using System;
using System.Reflection;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	[Serializable]
	public sealed class AsyncActionCall : AsyncCall
	{
		private readonly Action action;

		public override MethodBase Method => this.action.Method;
		public override object Target => this.action.Target;

		public AsyncActionCall(Action action)
		{
			this.action = action;
		}

		protected override void DoInvoke()
		{
			this.action();
		}
	}

	[Serializable]
	public sealed class AsyncActionCall<T> : AsyncCall
	{
		private readonly Action<T> action;
		[MethodParameter(0)] private readonly T arg;

		public override MethodBase Method => this.action.Method;
		public override object Target => this.action.Target;

		public AsyncActionCall(Action<T> action, T arg)
		{
			this.action = action;
			this.arg = arg;
		}

		protected override void DoInvoke()
		{
			this.action(this.arg);
		}
	}

	[Serializable]
	public sealed class AsyncActionCall<T1, T2> : AsyncCall
	{
		private readonly Action<T1, T2> action;
		[MethodParameter(0)] private readonly T1 arg1;
		[MethodParameter(1)] private readonly T2 arg2;

		public override MethodBase Method => this.action.Method;
		public override object Target => this.action.Target;

		public AsyncActionCall(Action<T1, T2> action, T1 arg1, T2 arg2)
		{
			this.action = action;
			this.arg1 = arg1;
			this.arg2 = arg2;
		}

		protected override void DoInvoke()
		{
			this.action(this.arg1, this.arg2);
		}
	}
}
