﻿using System;
using System.Threading.Tasks;

namespace Iridescence.Networking
{
	/// <summary>
	/// Async RPC extensions.
	/// </summary>
	public static class PeerAsyncRpcExtensions
	{
		#region Methods

		public static Task<TResult> InvokeAsync<TResult>(this IPeer peer, Func<TResult> func)
		{
			AsyncFuncCall<TResult> call = new AsyncFuncCall<TResult>(func);
			peer.Write(call);
			return call.Task;
		}

		public static Task<TResult> InvokeAsync<T, TResult>(this IPeer peer, Func<T, TResult> func, T arg)
		{
			AsyncFuncCall<T, TResult> call = new AsyncFuncCall<T, TResult>(func, arg);
			peer.Write(call);
			return call.Task;
		}
		
		public static Task<TResult> InvokeAsync<T1, T2, TResult>(this IPeer peer, Func<T1, T2, TResult> func, T1 arg1, T2 arg2)
		{
			AsyncFuncCall<T1, T2, TResult> call = new AsyncFuncCall<T1, T2, TResult>(func, arg1, arg2);
			peer.Write(call);
			return call.Task;
		}
		
		#endregion
	}
}
