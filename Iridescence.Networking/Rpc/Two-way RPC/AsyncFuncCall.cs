﻿using System;
using System.Reflection;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	[Serializable]
	public sealed class AsyncFuncCall<TResult> : AsyncCall<TResult>
	{
		private readonly Func<TResult> func;

		public override MethodBase Method => this.func.Method;
		public override object Target => this.func.Target;

		public AsyncFuncCall(Func<TResult> func)
		{
			this.func = func;
		}

		protected override TResult DoInvoke()
		{
			return this.func();
		}
	}

	[Serializable]
	public sealed class AsyncFuncCall<T, TResult> : AsyncCall<TResult>
	{
		private readonly Func<T, TResult> func;
		[MethodParameter(0)] private readonly T arg;

		public override MethodBase Method => this.func.Method;
		public override object Target => this.func.Target;

		public AsyncFuncCall(Func<T, TResult> func, T arg)
		{
			this.func = func;
			this.arg = arg;
		}

		protected override TResult DoInvoke()
		{
			return this.func(this.arg);
		}
	}

	[Serializable]
	public sealed class AsyncFuncCall<T1, T2, TResult> : AsyncCall<TResult>
	{
		private readonly Func<T1, T2, TResult> func;
		[MethodParameter(0)] private readonly T1 arg1;
		[MethodParameter(1)] private readonly T2 arg2;

		public override MethodBase Method => this.func.Method;
		public override object Target => this.func.Target;

		public AsyncFuncCall(Func<T1, T2, TResult> func, T1 arg1, T2 arg2)
		{
			this.func = func;
			this.arg1 = arg1;
			this.arg2 = arg2;
		}

		protected override TResult DoInvoke()
		{
			return this.func(this.arg1, this.arg2);
		}
	}
}
