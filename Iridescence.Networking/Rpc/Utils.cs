﻿using System;
using System.Text;

namespace Iridescence.Networking
{
	internal static class Utils
	{
		internal static string ToStringWithParameters(this Delegate func, params object[] parameters)
		{
			StringBuilder str = new StringBuilder();

			if (func.Target != null)
			{
				str.Append(func.Target.GetType().GetDisplayName());
				str.Append('.');
			}

			str.Append(func.Method.Name);
			str.Append('(');

			bool first = true;
			foreach (object p in parameters)
			{
				if (first)
					first = false;
				else
					str.Append(", ");

				str.Append(p?.ToString() ?? "null");
			}

			str.Append(')');

			return str.ToString();
		}
	}
}
