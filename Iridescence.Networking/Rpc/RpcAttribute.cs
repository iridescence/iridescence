﻿using System;

namespace Iridescence.Networking
{
	/// <summary>
	/// Abstract attribute to mark methods for remote procedure calls and to control access permissions.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public abstract class RpcAttribute : Attribute
	{
		#region Fields

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new RpcAttribute.
		/// </summary>
		protected RpcAttribute()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Verifies whether the specified call is valid in respect to the specified invoker.
		/// This method can be used to throw exceptions.
		/// </summary>
		/// <param name="invoker">The object that wants to invoke the method (e.g. a <see cref="Peer"/>).</param>
		/// <param name="call">The call parameters.</param>
		public abstract void VerifyCall(object invoker, ICall call);

		#endregion
	}
}
