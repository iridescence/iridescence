﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Iridescence.Networking
{
	/// <summary>
	/// ICall extension methods.
	/// </summary>
	public static class CallExtensions
	{
		private static readonly ConditionalWeakTable<MethodBase, RpcAttribute[]> attributeCache = new ConditionalWeakTable<MethodBase, RpcAttribute[]>();

		/// <summary>
		/// Returns a value that indicates whether the specified call is defined in the networking assembly.
		/// </summary>
		/// <param name="call">The call object.</param>
		/// <returns>True, if the call is a custom implementation of the ICall interface. False, if it is a built-in implementation defined in the networking assembly.</returns>
		public static bool IsCustomImplementation(this ICall call)
		{
			return call.GetType().Assembly != typeof(ICall).Assembly;
		}

		/// <summary>
		/// Verifies the specified call in respect to the specified invoker.
		/// </summary>
		/// <param name="call">The call object.</param>
		/// <param name="invoker">The object that wants to invoke the call (e.g. a <see cref="Peer"/>).</param>
		public static void VerifyCall(this ICall call, object invoker)
		{
			if (!attributeCache.TryGetValue(call.Method, out RpcAttribute[] attributes))
			{
				attributes = call.Method.GetCustomAttributes<RpcAttribute>().ToArray();
				
				try
				{
					attributeCache.Add(call.Method, attributes);
				}
				catch (ArgumentException)
				{
					
				}
			}

			if (attributes == null || attributes.Length == 0)
			{
				throw new RpcVerificationException("Method is not marked with any RPC verification attributes and can not be called.");
			}

			foreach(RpcAttribute attribute in attributes)
			{
				attribute.VerifyCall(invoker, call);
			}
		}
	}
}