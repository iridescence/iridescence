﻿using System;
using System.Reflection;
using System.Runtime.ExceptionServices;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct DynamicCall : ICall
	{
		public Delegate Delegate;
		public object[] Parameters;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			try
			{
				this.Delegate.DynamicInvoke(this.Parameters);
			}
			catch (TargetInvocationException ex)
			{
				ExceptionDispatchInfo.Capture(ex.InnerException).Throw();
			}
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters(this.Parameters);
		}
	}
}