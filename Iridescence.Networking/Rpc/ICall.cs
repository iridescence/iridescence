﻿using System.Reflection;

namespace Iridescence.Networking
{
	/// <summary>
	/// Represents an abstract call.
	/// </summary>
	public interface ICall
	{
		/// <summary>
		/// Gets the method to invoke.
		/// </summary>
		MethodBase Method { get; }

		/// <summary>
		/// Gets the target object on which the method is invoked.
		/// </summary>
		object Target { get; }

		/// <summary>
		/// Invokes the method.
		/// </summary>
		void Invoke();
	}
}