﻿using System;
using System.Runtime.Serialization;

namespace Iridescence.Networking
{
	/// <summary>
	/// RPC verification exception.
	/// </summary>
	[Serializable]
	public class RpcVerificationException : Exception
	{
		#region Constructors

		protected RpcVerificationException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
			
		}

		/// <summary>
		/// Creates a new RpcVerificationException.
		/// </summary>
		public RpcVerificationException()
		{
			
		}

		/// <summary>
		/// Creates a new RpcVerificationException.
		/// </summary>
		public RpcVerificationException(string message)
			: base(message)
		{

		}

		/// <summary>
		/// Creates a new RpcVerificationException.
		/// </summary>
		public RpcVerificationException(string message, Exception innerException)
			: base(message, innerException)
		{

		}

		#endregion
	}
}
