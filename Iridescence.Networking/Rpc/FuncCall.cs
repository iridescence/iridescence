﻿using System;
using System.Reflection;
using Iridescence.Serialization;

namespace Iridescence.Networking
{
	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct FuncCall<TResult> : ICall
	{
		public Func<TResult> Delegate;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			this.Delegate();
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters();
		}
	}

	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct FuncCall<T, TResult> : ICall
	{
		public Func<T, TResult> Delegate;
		[MethodParameter(0)] public T Arg;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			this.Delegate(this.Arg);
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters(this.Arg);
		}
	}

	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct FuncCall<T1, T2, TResult> : ICall
	{
		public Func<T1,T2, TResult> Delegate;
		[MethodParameter(0)] public T1 Arg1;
		[MethodParameter(1)] public T2 Arg2;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			this.Delegate(this.Arg1, this.Arg2);
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters(this.Arg1, this.Arg2);
		}
	}
	
	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct FuncCall<T1, T2, T3, TResult> : ICall
	{
		public Func<T1, T2, T3, TResult> Delegate;
		[MethodParameter(0)] public T1 Arg1;
		[MethodParameter(1)] public T2 Arg2;
		[MethodParameter(2)] public T3 Arg3;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			this.Delegate(this.Arg1, this.Arg2, this.Arg3);
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters(this.Arg1, this.Arg2, this.Arg3);
		}
	}

	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct FuncCall<T1, T2, T3, T4, TResult> : ICall
	{
		public Func<T1, T2, T3, T4, TResult> Delegate;
		[MethodParameter(0)] public T1 Arg1;
		[MethodParameter(1)] public T2 Arg2;
		[MethodParameter(2)] public T3 Arg3;
		[MethodParameter(3)] public T4 Arg4;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			this.Delegate(this.Arg1, this.Arg2, this.Arg3, this.Arg4);
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters(this.Arg1, this.Arg2, this.Arg3, this.Arg4);
		}
	}

	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct FuncCall<T1, T2, T3, T4, T5, TResult> : ICall
	{
		public Func<T1, T2, T3, T4, T5, TResult> Delegate;
		[MethodParameter(0)] public T1 Arg1;
		[MethodParameter(1)] public T2 Arg2;
		[MethodParameter(2)] public T3 Arg3;
		[MethodParameter(3)] public T4 Arg4;
		[MethodParameter(4)] public T5 Arg5;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			this.Delegate(this.Arg1, this.Arg2, this.Arg3, this.Arg4, this.Arg5);
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters(this.Arg1, this.Arg2, this.Arg3, this.Arg4, this.Arg5);
		}
	}

	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct FuncCall<T1, T2, T3, T4, T5, T6, TResult> : ICall
	{
		public Func<T1, T2, T3, T4, T5, T6, TResult> Delegate;
		[MethodParameter(0)] public T1 Arg1;
		[MethodParameter(1)] public T2 Arg2;
		[MethodParameter(2)] public T3 Arg3;
		[MethodParameter(3)] public T4 Arg4;
		[MethodParameter(4)] public T5 Arg5;
		[MethodParameter(5)] public T6 Arg6;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			this.Delegate(this.Arg1, this.Arg2, this.Arg3, this.Arg4, this.Arg5, this.Arg6);
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters(this.Arg1, this.Arg2, this.Arg3, this.Arg4, this.Arg5, this.Arg6);
		}
	}

	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct FuncCall<T1, T2, T3, T4, T5, T6, T7, TResult> : ICall
	{
		public Func<T1, T2, T3, T4, T5, T6, T7, TResult> Delegate;
		[MethodParameter(0)] public T1 Arg1;
		[MethodParameter(1)] public T2 Arg2;
		[MethodParameter(2)] public T3 Arg3;
		[MethodParameter(3)] public T4 Arg4;
		[MethodParameter(4)] public T5 Arg5;
		[MethodParameter(5)] public T6 Arg6;
		[MethodParameter(6)] public T7 Arg7;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			this.Delegate(this.Arg1, this.Arg2, this.Arg3, this.Arg4, this.Arg5, this.Arg6, this.Arg7);
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters(this.Arg1, this.Arg2, this.Arg3, this.Arg4, this.Arg5, this.Arg6, this.Arg7);
		}
	}

	[Serializable]
	[DynamicMethodInvocation(nameof(Method))]
	public struct FuncCall<T1, T2, T3, T4, T5, T6, T7, T8, TResult> : ICall
	{
		public Func<T1, T2, T3, T4, T5, T6, T7, T8, TResult> Delegate;
		[MethodParameter(0)] public T1 Arg1;
		[MethodParameter(1)] public T2 Arg2;
		[MethodParameter(2)] public T3 Arg3;
		[MethodParameter(3)] public T4 Arg4;
		[MethodParameter(4)] public T5 Arg5;
		[MethodParameter(5)] public T6 Arg6;
		[MethodParameter(6)] public T7 Arg7;
		[MethodParameter(7)] public T8 Arg8;

		public object Target => this.Delegate?.Target;
		public MethodBase Method => this.Delegate?.Method;

		public void Invoke()
		{
			this.Delegate(this.Arg1, this.Arg2, this.Arg3, this.Arg4, this.Arg5, this.Arg6, this.Arg7, this.Arg8);
		}

		public override string ToString()
		{
			return this.Delegate.ToStringWithParameters(this.Arg1, this.Arg2, this.Arg3, this.Arg4, this.Arg5, this.Arg6, this.Arg7, this.Arg8);
		}
	}
}
