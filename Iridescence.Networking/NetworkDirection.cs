﻿namespace Iridescence.Networking
{
	/// <summary>
	/// Defines the networking directions.
	/// </summary>
	public enum NetworkDirection
	{
		/// <summary>
		/// The object is incoming from a remote peer.
		/// </summary>
		Incoming = 1,

		/// <summary>
		/// The object was sent to a remote peer.
		/// </summary>
		Outgoing = 2,
	}
}
