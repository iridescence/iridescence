﻿using System;
using Avalonia.OpenGL;
using Iridescence.Graphics.OpenGL;

namespace Iridescence.Avalonia
{
	public class IridescenceGlContext : IGlContext
	{
		public IIridescenceRuntime Runtime { get; }

		public GlVersion Version => new GlVersion(GlProfileType.OpenGL, 3, 3);
	
		public GL GL { get; }
		
		public GlInterface GlInterface { get; }
		
		public int SampleCount => 1;
		
		public int StencilSize => 8;

		public IridescenceGlContext(IIridescenceRuntime rt, GlInterface glInterface)
		{
			this.Runtime = rt;
			this.GL = rt.OpenGLContext.GL;
			this.GlInterface = glInterface;
		}

		public void Dispose()
		{

		}

		public IDisposable MakeCurrent() => this.Runtime.MakeCurrent();

		public IDisposable EnsureCurrent() => this.MakeCurrent();

		public bool IsSharedWith(IGlContext context) => false;
	}
}