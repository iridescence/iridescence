﻿using System;
using System.Threading.Tasks;
using Avalonia.Input;
using TextCopy;
using IClipboard = Avalonia.Input.Platform.IClipboard;

namespace Iridescence.Avalonia
{
	/// <summary>
	/// Implements a <see cref="IClipboard"/> via <see cref="ClipboardService"/>
	/// </summary>
	public class TextCopyClipboard : IClipboard
	{
		public TextCopyClipboard()
		{
		}

		public async Task<string> GetTextAsync()
		{
			return await ClipboardService.GetTextAsync();
		}

		public async Task SetTextAsync(string text)
		{
			await ClipboardService.SetTextAsync(text);
		}

		public async Task ClearAsync()
		{
			await ClipboardService.SetTextAsync(string.Empty);
		}

		public Task SetDataObjectAsync(IDataObject data)
		{
			return Task.CompletedTask;
		}

		public async Task<string[]> GetFormatsAsync()
		{
			if (await this.GetTextAsync() != null)
				return new[] {DataFormats.Text};
			return Array.Empty<string>();
		}

		public async Task<object> GetDataAsync(string format)
		{
			if (format == DataFormats.Text)
				return await this.GetTextAsync();
			return null;
		}
	}
}
