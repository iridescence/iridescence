﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Platform;
using Avalonia.Input;
using Avalonia.Input.Platform;
using Avalonia.OpenGL;
using Avalonia.Platform;
using Avalonia.Rendering;

namespace Iridescence.Avalonia
{
	internal sealed class IridescencePlatform
	{
		private readonly IridescenceGlContext wrapper;
		private readonly GlInterface glInterface;
		private readonly Surface surface;

		public IridescencePlatform(IIridescenceRuntime rt)
		{
			using (rt.MakeCurrent())
			{
				this.glInterface = new GlInterface(new GlVersion(GlProfileType.OpenGL, 3, 3), rt.GetProcAddress);
				this.wrapper = new IridescenceGlContext(rt, this.glInterface);
				this.surface = new Surface(this.wrapper);
			}
		}

		private void initialize(IPlatformThreadingInterface threadingInterface, IRenderTimer renderTimer)
		{
			threadingInterface ??= new InternalPlatformThreadingInterface();
			renderTimer ??= new DefaultRenderTimer(60);

			AvaloniaLocator.CurrentMutable
				.Bind<IPlatformOpenGlInterface>().ToConstant(new DummyOpenGlInterface(this.wrapper))
				.Bind<IPlatformThreadingInterface>().ToConstant(threadingInterface)
				.Bind<IRenderTimer>().ToConstant(renderTimer)
				.Bind<IRenderLoop>().ToConstant(new RenderLoop())
				.Bind<IStandardCursorFactory>().ToTransient<CursorFactory>()
				.Bind<IClipboard>().ToConstant(new TextCopyClipboard())
				.Bind<IKeyboardDevice>().ToConstant(new KeyboardDevice())
				.Bind<IPlatformSettings>().ToSingleton<PlatformSettings>()
				.Bind<PlatformHotkeyConfiguration>().ToSingleton<PlatformHotkeyConfiguration>();
		}

		internal static IridescenceLifetime Initialize<T>(T builder, IIridescenceRuntime rt, IPlatformThreadingInterface threadingInterface, IRenderTimer renderTimer) where T : AppBuilderBase<T>, new()
		{
			var platform = new IridescencePlatform(rt);
			builder.UseWindowingSubsystem(() => platform.initialize(threadingInterface, renderTimer));

			IridescenceLifetime lifetime = new IridescenceLifetime(platform.surface, rt);
			return lifetime;
		}
	}
}