﻿using System;
using Avalonia;
using Avalonia.OpenGL;
using Avalonia.OpenGL.Surfaces;

namespace Iridescence.Avalonia
{
	public class Surface : IGlPlatformSurface
	{
		#region Events

		public event EventHandler SizeChanged;

		#endregion

		#region Fields

		private readonly IridescenceGlContext wrapper;
		private readonly IIridescenceRuntime runtime;
		private SurfaceRenderTarget renderTarget;
		
		#endregion

		#region Properties

		public IGlContext MainContext { get; }

		public PixelSize Size => this.runtime.Size;

		public uint? TargetFramebuffer => this.renderTarget?.Framebuffer;
		
		public uint? TargetTexture => this.renderTarget?.Texture;

		#endregion

		#region Constructors

		public Surface(IridescenceGlContext wrapper)
		{
			this.wrapper = wrapper;
			this.runtime = wrapper.Runtime;
			this.MainContext = this.wrapper;
			//this.Size = this.runtime.Size;
			this.runtime.SizeChanged += this.onSizeChanged;
		}
		
		#endregion

		#region Methods
		
		private void onSizeChanged(object sender, EventArgs e)
		{
			this.SizeChanged?.Invoke(sender, e);
		}

		public IGlPlatformSurfaceRenderTarget CreateGlRenderTarget()
		{
			if (this.renderTarget != null)
				throw new InvalidOperationException();

			this.renderTarget = new SurfaceRenderTarget(this.wrapper);
			return this.renderTarget;
		}

		#endregion
	}
}