﻿using System;
using Avalonia.Rendering;

namespace Iridescence.Avalonia
{
	public class ManualRenderTimer : IRenderTimer
	{
		public event Action<TimeSpan> Tick;

		public void Dispatch(TimeSpan elapsed)
		{
			this.Tick?.Invoke(elapsed);
		}
	}
}