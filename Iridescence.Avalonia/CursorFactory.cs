﻿using System;
using Avalonia.Input;
using Avalonia.Platform;

namespace Iridescence.Avalonia
{
	class CursorFactory : IStandardCursorFactory
	{
		public IPlatformHandle GetCursor(StandardCursorType cursorType)
		{
			return new PlatformHandle(IntPtr.Zero, null);
		}
	}
}