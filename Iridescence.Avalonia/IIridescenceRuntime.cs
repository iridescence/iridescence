﻿using System;
using Avalonia;
using Iridescence.Graphics.OpenGL;

namespace Iridescence.Avalonia
{
	/// <summary>
	/// Interface that provides the minimum feature set for embedding Avalonia.
	/// </summary>
	public interface IIridescenceRuntime : IInputBackend
	{
		/// <summary>
		/// Occurs when the framebuffer size has changed.
		/// </summary>
		event EventHandler SizeChanged;
		
		/// <summary>
		/// Gets the current framebuffer size.
		/// </summary>
		PixelSize Size { get; }

		/// <summary>
		/// Gets the <see cref="GLContext"/>.
		/// </summary>
		GLContext OpenGLContext { get; }
		
		IDisposable MakeCurrent();
		
		IntPtr GetProcAddress(string name);
	}
}