﻿using System;
using Avalonia;
using Avalonia.OpenGL.Surfaces;
using Iridescence.Graphics.OpenGL;

namespace Iridescence.Avalonia
{
	internal sealed class SurfaceRenderTarget : IGlPlatformSurfaceRenderTargetWithCorruptionInfo
	{
		#region Fields

		private readonly IridescenceGlContext context;
		private uint fbo;
		private uint tex;
		private PixelSize currentSize;

		#endregion

		#region Properties

		public bool IsCorrupted => false;

		public uint Framebuffer => this.fbo;
		
		public uint Texture => this.tex;

		#endregion

		#region Constructors

		public SurfaceRenderTarget(IridescenceGlContext context)
		{
			this.context = context;
		}

		#endregion

		#region Methods

		public void Dispose()
		{
			if (this.tex != 0)
				this.context.GL.DeleteTexture(this.tex);

			if (this.fbo != 0)
				this.context.GL.DeleteFramebuffer(this.fbo);
		}

		public IGlPlatformSurfaceRenderingSession BeginDraw()
		{
			IDisposable contextDisposable = this.context.Runtime.MakeCurrent();
			PixelSize size = this.context.Runtime.Size;
			if (size.Width <= 0) size = size.WithWidth(1);
			if (size.Height <= 0) size = size.WithHeight(1);

			var gl = this.context.GL;

			const PixelInternalFormat format = PixelInternalFormat.Srgb8Alpha8;
			
			if (this.fbo == 0)
			{
				this.fbo = gl.GenFramebuffer();
				gl.BindFramebuffer(FramebufferTarget.Framebuffer, this.fbo);
				this.tex = gl.GenTexture();
				gl.BindTexture(TextureTarget.Texture2D, this.tex);
				gl.TexImage2D(TextureTarget.Texture2D, 0, format, size.Width, size.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
				gl.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
				gl.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMinFilter.Linear);
				gl.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, this.tex, 0);
				this.currentSize = size;
			}

			if (this.currentSize != size)
			{
				gl.BindTexture(TextureTarget.Texture2D, this.tex);
				gl.TexImage2D(TextureTarget.Texture2D, 0, format, size.Width, size.Height, 0, PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
				this.currentSize = size;
			}

			gl.BindFramebuffer(FramebufferTarget.Framebuffer, this.fbo);

			return new RenderingSession(this.context, size, () =>
			{
				gl.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
				contextDisposable?.Dispose();
			});
		}

		#endregion
	}
}