﻿using System;
using Avalonia.OpenGL;

namespace Iridescence.Avalonia
{
	/// <summary>
	/// Implements a stub for <see cref="IPlatformOpenGlInterface"/> that does not support creating or sharing contexts.
	/// </summary>
	internal sealed class DummyOpenGlInterface : IPlatformOpenGlInterface
	{
		public IGlContext PrimaryContext { get; }

		public DummyOpenGlInterface(IGlContext primaryContext)
		{
			this.PrimaryContext = primaryContext;
		}

		public bool CanShareContexts => false;
		public IGlContext CreateSharedContext()
		{
			throw new NotSupportedException();
		}

		public bool CanCreateContexts => false;
		public IGlContext CreateContext()
		{
			throw new NotSupportedException();
		}
	}
}