﻿using System;
using Avalonia;
using Avalonia.Platform;

namespace Iridescence.Avalonia
{
	internal class PlatformSettings : IPlatformSettings
	{
		public Size DoubleClickSize { get; } = new Size(4, 4);
		
		public TimeSpan DoubleClickTime { get; } = new TimeSpan(0, 0, 0, 0, 500);
	}
}