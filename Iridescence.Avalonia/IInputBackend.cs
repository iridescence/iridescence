﻿using System;
using Avalonia.Input;
using Avalonia.Input.Raw;

namespace Iridescence.Avalonia
{
	public interface IInputBackend
	{
		void AttachInputRoot(IInputRoot root, IMouseDevice mouse, Action<RawInputEventArgs> inputHandler);
	}
}
