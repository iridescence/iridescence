﻿using System;
using System.Threading;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Embedding;
using Avalonia.Media;

namespace Iridescence.Avalonia
{
	public class IridescenceLifetime : IControlledApplicationLifetime, ISingleViewApplicationLifetime
	{
		#region Events

		public event EventHandler<ControlledApplicationLifetimeStartupEventArgs> Startup;
		public event EventHandler<ControlledApplicationLifetimeExitEventArgs> Exit;

		#endregion

		#region Fields

		private readonly Surface surface;
		private TopLevel topLevel;
		private readonly IInputBackend inputBackend;
		private readonly CancellationTokenSource cts = new CancellationTokenSource();
		
		#endregion

		#region Properties

		public CancellationToken Token => this.cts.Token;

		public int ExitCode { get; set; }

		public uint? SurfaceFramebuffer => this.surface.TargetFramebuffer;
		
		public uint? SurfaceTexture => this.surface.TargetTexture;
		
		internal TopLevelImpl TopLevelImpl { get; private set; }

		public Control MainView
		{
			get => this.topLevel?.Content as Control;
			set
			{
				if (this.topLevel == null)
				{
					this.TopLevelImpl = new TopLevelImpl(this.surface, this.inputBackend);
					
					EmbeddableControlRoot root = new EmbeddableControlRoot(this.TopLevelImpl)
					{
						TransparencyLevelHint = WindowTransparencyLevel.Transparent,
						Background = null
					};
					
					root.Prepare();
					
					this.topLevel = root;
					this.topLevel.Renderer.Start();
				}

				this.topLevel.Content = value;
			}
		}

		#endregion

		#region Constructors

		public IridescenceLifetime(Surface surface, IInputBackend inputBackend)
		{
			this.surface = surface ?? throw new ArgumentNullException(nameof(surface));
			this.inputBackend = inputBackend;
		}

		#endregion

		#region Methods

		public void Shutdown(int exitCode = 0)
		{
			this.ExitCode = exitCode;
			var e = new ControlledApplicationLifetimeExitEventArgs(exitCode);
			this.Exit?.Invoke(this, e);
			this.ExitCode = e.ApplicationExitCode;
			this.cts.Cancel();
		}

		public void Start(string[] args)
		{
			this.Startup?.Invoke(this, new ControlledApplicationLifetimeStartupEventArgs(args));
		}

		#endregion
	}
}