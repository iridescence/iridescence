﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using Avalonia.Platform;
using Avalonia.Threading;

namespace Iridescence.Avalonia
{
	/// <summary>
	/// Implements <see cref="IPlatformThreadingInterface"/> using a simple manual signaling method.
	/// </summary>
	public class ManualThreadingInterface : IPlatformThreadingInterface
	{
		[ThreadStatic] private static bool currentThreadIsLoopThread;
		private int signal;

		public bool CurrentThreadIsLoopThread => currentThreadIsLoopThread;
		
		public event Action<DispatcherPriority?> Signaled;

		public ManualThreadingInterface()
		{
			currentThreadIsLoopThread = true;
		}

		public void RunLoop(CancellationToken cancellationToken)
		{
			throw new NotSupportedException();
		}

		public IDisposable StartTimer(DispatcherPriority priority, TimeSpan interval, Action tick)
		{
			return new TimerImpl(priority, interval, tick);
		}

		public void Dispatch()
		{
			if (Interlocked.Exchange(ref this.signal, 0) != 0)
				this.Signaled?.Invoke(null);
		}

		public void Signal(DispatcherPriority prio)
		{
			Interlocked.Exchange(ref this.signal, 1);
		}
		
		private sealed class TimerImpl : IDisposable
		{
			private readonly DispatcherPriority priority;
			private readonly TimeSpan interval;
			private readonly Action tick;
			private Timer timer;
			private GCHandle handle;

			public TimerImpl(DispatcherPriority priority, TimeSpan interval, Action tick)
			{
				this.priority = priority;
				this.interval = interval;
				this.tick = tick;
				this.timer = new Timer(this.OnTimer, null, interval, TimeSpan.FromMilliseconds(-1));
				this.handle = GCHandle.Alloc(this.timer);
			}

			private void OnTimer(object state)
			{
				if (this.timer == null)
					return;
				
				Dispatcher.UIThread.Post(() =>
				{
                    if (this.timer == null)
						return;
                    
					this.tick();
					this.timer?.Change(this.interval, TimeSpan.FromMilliseconds(-1));
				});
			}

			public void Dispose()
			{
				this.handle.Free();
				this.timer.Dispose();
				this.timer = null;
			}
		}
	}
}