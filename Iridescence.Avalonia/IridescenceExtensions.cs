﻿using Avalonia.Controls;
using Avalonia.Platform;
using Avalonia.Rendering;

namespace Iridescence.Avalonia
{
	public static class IridescenceExtensions
	{
		public static int StartIridescence<T>(this T builder, string[] args, IIridescenceRuntime rt, IPlatformThreadingInterface threadingInterface = null, IRenderTimer renderTimer = null)
			where T : AppBuilderBase<T>, new()
		{
			IridescenceLifetime lifetime = builder.CreateIridescence(args, rt, threadingInterface, renderTimer);
			builder.Instance.Run(lifetime.Token);
			return lifetime.ExitCode;
		}

		public static IridescenceLifetime CreateIridescence<T>(this T builder, string[] args, IIridescenceRuntime rt, IPlatformThreadingInterface threadingInterface = null, IRenderTimer renderTimer = null)
			where T : AppBuilderBase<T>, new()
		{
			IridescenceLifetime lifetime = IridescencePlatform.Initialize(builder, rt, threadingInterface, renderTimer);
			builder.SetupWithLifetime(lifetime);
			lifetime.Start(args);
			
			return lifetime;
		}
	}
}