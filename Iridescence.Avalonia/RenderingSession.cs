﻿using System;
using System.Threading;
using Avalonia;
using Avalonia.OpenGL;
using Avalonia.OpenGL.Surfaces;
using Iridescence.Graphics.OpenGL;

namespace Iridescence.Avalonia
{
	internal sealed class RenderingSession : IGlPlatformSurfaceRenderingSession
	{
		private Action disposeAction;

		public IGlContext Context { get; }
		public PixelSize Size { get; }
		public double Scaling { get; }
		public bool IsYFlipped { get; }

		public RenderingSession(IGlContext context, PixelSize size, Action disposeAction)
		{
			this.disposeAction = disposeAction;
			
			this.Context = context;
			this.Size = size;
			this.Scaling = 1;
			this.IsYFlipped = false;
		}
		
		public void Dispose()
		{
			Interlocked.Exchange(ref this.disposeAction, null)?.Invoke();
		}
	}
}