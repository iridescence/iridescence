﻿using System;
using System.Collections.Generic;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Input.Raw;
using Avalonia.Platform;
using Avalonia.Rendering;
using Avalonia.Threading;

namespace Iridescence.Avalonia
{
	internal sealed class TopLevelImpl : ITopLevelImpl
	{
		private readonly Surface surface;
		private readonly IInputBackend inputBackend;

		public IInputRoot InputRoot { get; private set; }
		public double RenderScaling { get; } = 1.0;
		public IEnumerable<object> Surfaces { get; }
		public IMouseDevice MouseDevice { get; }
		public WindowTransparencyLevel TransparencyLevel { get; }
		public AcrylicPlatformCompensationLevels AcrylicCompensationLevels { get; }
		public Action LostFocus { get; set; }
		public Action<RawInputEventArgs> Input { get; set; }
		public Action<Rect> Paint { get; set; }
		public Action<Size> Resized { get; set; }
		public Action<double> ScalingChanged { get; set; }
		public Action<WindowTransparencyLevel> TransparencyLevelChanged { get; set; }
		public Action Closed { get; set; }
		public Size ClientSize => new(this.surface.Size.Width, this.surface.Size.Height);

		public TopLevelImpl(Surface surface, IInputBackend inputBackend)
		{
			this.surface = surface;
			this.MouseDevice = new MouseDevice();
			this.Surfaces = EnumerableExtensions.Once(surface);
			this.TransparencyLevel = WindowTransparencyLevel.Transparent;
			this.AcrylicCompensationLevels = new AcrylicPlatformCompensationLevels(1, 1, 1);
			
			this.surface.SizeChanged += onSizeChanged;
			this.inputBackend = inputBackend;
		}

		private void onSizeChanged(object sender, EventArgs e)
		{
			Size size = this.surface.Size.ToSize(this.RenderScaling);
			Dispatcher.UIThread.InvokeAsync(() =>
			{
				this.Resized?.Invoke(size);
			});
		}

		public void Dispose()
		{
			this.surface.SizeChanged -= onSizeChanged;
		}

		public IRenderer CreateRenderer(IRenderRoot root) => new DeferredRenderer(root, AvaloniaLocator.Current.GetService<IRenderLoop>());

		public void Invalidate(Rect rect)
		{

		}

		public void SetInputRoot(IInputRoot inputRoot)
		{
			this.InputRoot = inputRoot;
			this.inputBackend?.AttachInputRoot(inputRoot, this.MouseDevice, e => this.Input(e));
		}

		public Point PointToClient(PixelPoint point)
		{
			return point.ToPoint(1);
		}

		public PixelPoint PointToScreen(Point point)
		{
			return PixelPoint.FromPoint(point, 1);
		}

		public void SetCursor(IPlatformHandle cursor)
		{

		}

		public IPopupImpl CreatePopup() => null;

		public void SetTransparencyLevelHint(WindowTransparencyLevel transparencyLevel)
		{
			
		}
	}
}