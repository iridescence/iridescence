﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents a stereo sample.
	/// </summary>
	[StructLayout(LayoutKind.Sequential)]
	public struct Stereo<T> : IEquatable<Stereo<T>>
	{
		#region Fields

		public T L;
		public T R;
		
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Creates a new StereoSample.
		/// </summary>
		public Stereo(T l, T r)
		{
			this.L = l;
			this.R = r;
		}

		/// <summary>
		/// Creates a new StereoSample.
		/// </summary>
		public Stereo(T value)
		{
			this.L = value;
			this.R = value;
		}

		#endregion

		#region Methods

		public bool Equals(Stereo<T> other)
		{
			return Equals(other.L, this.L) && Equals(other.R, this.R);
		}

		public override bool Equals(object obj)
		{
			if (obj is Stereo<T> other)
				return this.Equals(other);
			return false;
		}

		public override int GetHashCode()
		{
			return this.L.GetHashCode() ^ this.R.GetHashCode();
		}

		public override string ToString()
		{
			return $"{this.L} {this.R}";
		}

		public static implicit operator (T l, T r)(Stereo<T> value)
		{
			(T l, T r) s;
			s.l = value.L;
			s.r = value.R;
			return s;
		}

		public static implicit operator Stereo<T>((T l, T r) value)
		{
			Stereo<T> s;
			s.L = value.l;
			s.R = value.r;
			return s;
		}

		public static bool operator ==(Stereo<T> left, Stereo<T> right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Stereo<T> left, Stereo<T> right)
		{
			return !left.Equals(right);
		}

		#endregion
	}
}
