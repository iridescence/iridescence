﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

#if NETCOREAPP3_0
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
#endif

namespace Iridescence.Audio
{
	/// <summary>
	/// Adds one or more sources together and outputs the sum. Optionally uses a constant summand as well.
	/// </summary>
	public class SignalAdder : OperatorSignal<float, float>
	{
		/// <summary>
		/// Gets or sets the constant summand that is added to each sample.
		/// </summary>
		public float Constant { get; set; }

		public SignalAdder(int sampleRate, OperatorSignalFlags flags, float constant = default, int bufferSize = AudioConstants.DefaultBufferSize) 
			: base(sampleRate, flags, bufferSize)
		{
			this.Constant = constant;
		}

		protected override void Process(ReadOnlyChannelData<float> channelData, Span<float> result)
		{
			channelData[0].CopyTo(result);
			for (int i = 1; i < channelData.ChannelCount; ++i)
			{
				ReadOnlySpan<float> data = channelData[i];
				for (int j = 0; j < result.Length; ++j)
					result[j] += data[j];
			}

			float c = this.Constant;
			if (c != 0.0f)
			{
				for (int j = 0; j < result.Length; ++j)
					result[j] += c;
			}
		}
	}

	/// <summary>
	/// Adds one or more sources together and outputs the sum. Optionally uses a constant summand as well.
	/// </summary>
	public class StereoSignalAdder : OperatorSignal<Stereo<float>, Stereo<float>>
	{
		/// <summary>
		/// Gets or sets the constant summand that is added to each sample.
		/// </summary>
		public Stereo<float> Constant { get; set; }

		public StereoSignalAdder(int sampleRate, OperatorSignalFlags flags, Stereo<float> constant = default, int bufferSize = AudioConstants.DefaultBufferSize)
			: base(sampleRate, flags, bufferSize)
		{
			this.Constant = constant;
		}

#if NETCOREAPP3_0

		private static void sse(ReadOnlyChannelData<Stereo<float>> channelData, Span<Stereo<float>> result, Stereo<float> constant)
		{
			channelData[0].CopyTo(result);
			Span<Vector128<float>> vres = MemoryMarshal.Cast<Stereo<float>, Vector128<float>>(result);
			for (int i = 1; i < channelData.ChannelCount; ++i)
			{
				ReadOnlySpan<Vector128<float>> vdata = MemoryMarshal.Cast<Stereo<float>, Vector128<float>>(channelData[i]);
				for (int j = 0; j < vres.Length; ++j)
				{
					vres[j] = Sse.Add(vres[j], vdata[j]);
				}
			}

			if (constant.L != 0.0f || constant.R != 0.0f)
			{
				Vector128<float> vc = Vector128.Create(
					constant.L, constant.R, 
					constant.L, constant.R);
				for (int j = 0; j < vres.Length; ++j)
				{
					vres[j] = Sse.Add(vres[j], vc);
				}
			}
		}

		private static void avx(ReadOnlyChannelData<Stereo<float>> channelData, Span<Stereo<float>> result, Stereo<float> constant)
		{
			channelData[0].CopyTo(result);
			Span<Vector256<float>> vres = MemoryMarshal.Cast<Stereo<float>, Vector256<float>>(result);
			for (int i = 1; i < channelData.ChannelCount; ++i)
			{
				ReadOnlySpan<Vector256<float>> vdata = MemoryMarshal.Cast<Stereo<float>, Vector256<float>>(channelData[i]);
				for (int j = 0; j < vres.Length; ++j)
				{
					vres[j] = Avx.Add(vres[j], vdata[j]);
				}
			}

			if (constant.L != 0.0f || constant.R != 0.0f)
			{
				Vector256<float> vc = Vector256.Create(
					constant.L, constant.R, 
					constant.L, constant.R,
					constant.L, constant.R,
					constant.L, constant.R);
				for (int j = 0; j < vres.Length; ++j)
				{
					vres[j] = Avx.Add(vres[j], vc);
				}
			}
		}
		
#endif

		private static void generic(ReadOnlyChannelData<Stereo<float>> channelData, Span<Stereo<float>> result, Stereo<float> constant)
		{
			channelData[0].CopyTo(result);
			for (int i = 1; i < channelData.ChannelCount; ++i)
			{
				ReadOnlySpan<Stereo<float>> data = channelData[i];
				for (int j = 0; j < result.Length; ++j)
				{
					result[j].L += data[j].L;
					result[j].R += data[j].R;
				}
			}

			if (constant.L != 0.0f || constant.R != 0.0f)
			{
				for (int j = 0; j < result.Length; ++j)
				{
					result[j].L += constant.L;
					result[j].R += constant.R;
				}
			}
		}

		protected override void Process(ReadOnlyChannelData<Stereo<float>> channelData, Span<Stereo<float>> result)
		{
#if NETCOREAPP3_0
			if (Avx.IsSupported)
			{
				int numVectors = (channelData.SampleCount / 4) * 4;
				avx(channelData.Slice(0, numVectors), result.Slice(0, numVectors), this.Constant);
				generic(channelData.Slice(numVectors), result.Slice(numVectors), this.Constant);
			}
			else if (Sse.IsSupported)
			{
				int numVectors = (channelData.SampleCount / 2) * 2;
				sse(channelData.Slice(0, numVectors), result.Slice(0, numVectors), this.Constant);
				generic(channelData.Slice(numVectors), result.Slice(numVectors), this.Constant);
			}
			else
			{
				generic(channelData, result, this.Constant);
			}
#else
			generic(channelData, result, this.Constant);
#endif
		}
	}

	public static class SignalAdderExtensions
	{
		/// <summary>
		/// Returns an audio source that emits the sum of multiple input sources (and optionally a constant summand).
		/// </summary>
		/// <param name="source">The first summand source.</param>
		/// <param name="summands">The other summand sources.</param>
		/// <param name="constant">A constant summand.</param>
		/// <param name="flags">The flags for the <see cref="ImmutableOperatorSignal{TIn,TOut}"/>.</param>
		/// <param name="bufferSize">The internal buffer size in samples.</param>
		/// <returns></returns>
		[Pure]
		public static SignalAdder Add(this ISignal<float> source, IEnumerable<ISignal<float>> summands, float constant = 0.0f, OperatorSignalFlags flags = OperatorSignalFlags.Default, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			SignalAdder adder = new SignalAdder(source.SampleRate, flags, constant, bufferSize);
			adder.Channels.Add(source);
			adder.Channels.Add(summands);
			return adder;
		}

		/// <summary>
		/// Returns an audio source that emits the sum of multiple input sources.
		/// </summary>
		/// <param name="source">The first summand.</param>
		/// <param name="factors">The other summands.</param>
		/// <returns></returns>
		[Pure]
		public static SignalAdder Add(this ISignal<float> source, params ISignal<float>[] factors)
		{
			return source.Add((IEnumerable<ISignal<float>>)factors);
		}

		/// <summary>
		/// Returns an audio source that emits the sum of an audio source and a constant value.
		/// </summary>
		/// <param name="source">The audio source.</param>
		/// <param name="constant">A constant summand.</param>
		/// <param name="flags">The flags for the <see cref="ImmutableOperatorSignal{TIn,TOut}"/>.</param>
		/// <param name="bufferSize">The internal buffer size in samples.</param>
		/// <returns></returns>
		[Pure]
		public static SignalAdder Add(this ISignal<float> source, float constant = 0.0f, OperatorSignalFlags flags = OperatorSignalFlags.Default, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			return source.Add(Enumerable.Empty<ISignal<float>>(), constant, flags, bufferSize);
		}


		/// <summary>
		/// Returns an audio source that emits the sum of multiple input sources (and optionally a constant summand).
		/// </summary>
		/// <param name="source">The first factor source.</param>
		/// <param name="summands">The other summand sources.</param>
		/// <param name="constant">A constant summand.</param>
		/// <param name="flags">The flags for the <see cref="ImmutableOperatorSignal{TIn,TOut}"/>.</param>
		/// <param name="bufferSize">The internal buffer size in samples.</param>
		/// <returns></returns>
		[Pure]
		public static StereoSignalAdder Add(this ISignal<Stereo<float>> source, IEnumerable<ISignal<Stereo<float>>> summands, Stereo<float> constant = default, OperatorSignalFlags flags = OperatorSignalFlags.Default, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			StereoSignalAdder adder = new StereoSignalAdder(source.SampleRate, flags, constant, bufferSize);
			adder.Channels.Add(source);
			adder.Channels.Add(summands);
			return adder;
		}

		/// <summary>
		/// Returns an audio source that emits the sum of multiple input sources.
		/// </summary>
		/// <param name="source">The first summands.</param>
		/// <param name="factors">The other summands.</param>
		/// <returns></returns>
		[Pure]
		public static StereoSignalAdder Add(this ISignal<Stereo<float>> source, params ISignal<Stereo<float>>[] factors)
		{
			return source.Add((IEnumerable<ISignal<Stereo<float>>>)factors);
		}

		/// <summary>
		/// Returns an audio source that emits the sum of an audio source and a constant value.
		/// </summary>
		/// <param name="source">The audio source.</param>
		/// <param name="constant">A constant summand.</param>
		/// <param name="flags">The flags for the <see cref="ImmutableOperatorSignal{TIn,TOut}"/>.</param>
		/// <param name="bufferSize">The internal buffer size in samples.</param>
		/// <returns></returns>
		[Pure]
		public static StereoSignalAdder Add(this ISignal<Stereo<float>> source, Stereo<float> constant, OperatorSignalFlags flags = OperatorSignalFlags.Default, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			return source.Add(Enumerable.Empty<ISignal<Stereo<float>>>(), constant, flags, bufferSize);
		}
	}
}
