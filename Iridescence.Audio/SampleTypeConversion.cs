﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents an interface for sample type conversion.
	/// </summary>
	public static class SampleTypeConversion
	{
		private static readonly Dictionary<Tuple<Type, Type>, Func<ISignal, ISignal>> conversionFunctions;

		static SampleTypeConversion()
		{
			conversionFunctions = new Dictionary<Tuple<Type, Type>, Func<ISignal, ISignal>>();

			add<SNorm16, float>(I16ToF32, I16ToF32);
			add<float, SNorm16>(F32ToI16, F32ToI16);

			add<SNorm24, float>(I24ToF32, I24ToF32);
			add<float, SNorm24>(F32ToI24, F32ToI24);

			addMonoToStereo<SNorm16>();
			addMonoToStereo<SNorm24>();
			addMonoToStereo<float>();
		}

		private static void add<TFrom, TTo>(SampleTypeConversionFunction<TFrom, TTo> func)
		{
			conversionFunctions.Add(new Tuple<Type, Type>(typeof(TFrom), typeof(TTo)), input => new SampleTypeConverter<TFrom, TTo>((ISignal<TFrom>)input, func));
		}

		private static void add<TFrom, TTo>(SampleTypeConversionFunction<TFrom, TTo> func, SampleTypeConversionFunction<Stereo<TFrom>, Stereo<TTo>> stereoFunc)
		{
			add(func);
			add(stereoFunc);
		}

		private static void addMonoToStereo<T>()
		{
			add<T, Stereo<T>>(ConvertMonoToStereo);
		}

		#region Int16 -> Float

		/// <summary>
		/// Converts 16-bit integer samples to 32-bit float samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		public static void I16ToF32(ReadOnlySpan<SNorm16> source, Span<float> destination)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i] = source[i].SingleValue;
			}
		}

		/// <summary>
		/// Converts 16-bit integer samples to 32-bit float samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <param name="factor"></param>
		public static void I16ToF32(ReadOnlySpan<SNorm16> source, Span<float> destination, float factor)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i] = source[i].SingleValue * factor;
			}
		}

		/// <summary>
		/// Converts 16-bit integer samples to 32-bit float samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		public static void I16ToF32(ReadOnlySpan<Stereo<SNorm16>> source, Span<Stereo<float>> destination)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].L = source[i].L.SingleValue;
				destination[i].R = source[i].R.SingleValue;
			}
		}

		/// <summary>
		/// Converts 16-bit integer samples to 32-bit float samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <param name="factor"></param>
		public static void I16ToF32(ReadOnlySpan<Stereo<SNorm16>> source, Span<Stereo<float>> destination, float factor)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].L = source[i].L.SingleValue * factor;
				destination[i].R = source[i].R.SingleValue * factor;
			}
		}

		#endregion

		#region Float -> Int16

		/// <summary>
		/// Converts 32-bit float samples to 16-bit integer samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		public static void F32ToI16(ReadOnlySpan<float> source, Span<SNorm16> destination)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].SingleValue = source[i];
			}
		}

		/// <summary>
		/// Converts 32-bit float samples to 16-bit integer samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <param name="factor"></param>
		public static void F32ToI16(ReadOnlySpan<float> source, Span<SNorm16> destination, float factor)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].SingleValue = source[i] * factor;
			}
		}

		/// <summary>
		/// Converts 32-bit float samples to 16-bit integer samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		public static void F32ToI16(ReadOnlySpan<Stereo<float>> source, Span<Stereo<SNorm16>> destination)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].L.SingleValue = source[i].L;
				destination[i].R.SingleValue = source[i].R;
			}
		}

		/// <summary>
		/// Converts 32-bit float samples to 16-bit integer samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <param name="factor"></param>
		public static void F32ToI16(ReadOnlySpan<Stereo<float>> source, Span<Stereo<SNorm16>> destination, float factor)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].L.SingleValue = source[i].L * factor;
				destination[i].R.SingleValue = source[i].R * factor;
			}
		}

		#endregion

		#region Int24 -> Float

		/// <summary>
		/// Converts 24-bit integer samples to 32-bit float samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		public static void I24ToF32(ReadOnlySpan<SNorm24> source, Span<float> destination)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i] = source[i].SingleValue;
			}
		}

		/// <summary>
		/// Converts 24-bit integer samples to 32-bit float samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <param name="factor"></param>
		public static void I24ToF32(ReadOnlySpan<SNorm24> source, Span<float> destination, float factor)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i] = source[i].SingleValue * factor;
			}
		}

		/// <summary>
		/// Converts 24-bit integer samples to 32-bit float samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		public static void I24ToF32(ReadOnlySpan<Stereo<SNorm24>> source, Span<Stereo<float>> destination)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].L = source[i].L.SingleValue;
				destination[i].R = source[i].R.SingleValue;
			}
		}

		/// <summary>
		/// Converts 24-bit integer samples to 32-bit float samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <param name="factor"></param>
		public static void I24ToF32(ReadOnlySpan<Stereo<SNorm24>> source, Span<Stereo<float>> destination, float factor)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].L = source[i].L.SingleValue * factor;
				destination[i].R = source[i].R.SingleValue * factor;
			}
		}

		#endregion

		#region Float -> Int24

		/// <summary>
		/// Converts 32-bit float samples to 24-bit integer samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		public static void F32ToI24(ReadOnlySpan<float> source, Span<SNorm24> destination)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].SingleValue = source[i];
			}
		}

		/// <summary>
		/// Converts 32-bit float samples to 24-bit integer samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <param name="factor"></param>
		public static void F32ToI24(ReadOnlySpan<float> source, Span<SNorm24> destination, float factor)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].SingleValue = source[i] * factor;
			}
		}

		/// <summary>
		/// Converts 32-bit float samples to 24-bit integer samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		public static void F32ToI24(ReadOnlySpan<Stereo<float>> source, Span<Stereo<SNorm24>> destination)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].L.SingleValue = source[i].L;
				destination[i].R.SingleValue = source[i].R;
			}
		}

		/// <summary>
		/// Converts 32-bit float samples to 24-bit integer samples.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		/// <param name="factor"></param>
		public static void F32ToI24(ReadOnlySpan<Stereo<float>> source, Span<Stereo<SNorm24>> destination, float factor)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].L.SingleValue = source[i].L * factor;
				destination[i].R.SingleValue = source[i].R * factor;
			}
		}

		#endregion

		/// <summary>
		/// Converts mono audio to stereo audio by duplicating the single channel to both channels.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="destination"></param>
		public static void ConvertMonoToStereo<T>(ReadOnlySpan<T> source, Span<Stereo<T>> destination)
		{
			if (source.Length != destination.Length)
				throw new ArgumentException("Source and destination span lengths do not match.");

			for (int i = 0; i < source.Length; ++i)
			{
				destination[i].R = destination[i].L = source[i];
			}
		}

		private static bool isStereo(Type type)
		{
			return type.IsConstructedGenericType && type.GetGenericTypeDefinition() == typeof(Stereo<>);
		}

		/// <summary>
		/// Tries to create a sample type conversion wrapper for the specified input.
		/// </summary>
		/// <returns></returns>
		private static bool tryConvert(this ISignal input, Type typeIn, Type typeOut, out ISignal output, bool allowMonoToStereo)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));

			if (typeIn == typeOut)
			{
				// TIn and TOut match, nothing to do here.
				output = input;
				return true;
			}

			// Find converter.
			Tuple<Type, Type> k = new Tuple<Type, Type>(typeIn, typeOut);
			if (conversionFunctions.TryGetValue(k, out var func))
			{
				output = func(input);
				return true;
			}

			// Try Mono<TIn> -> Stereo<TIn> -> Stereo<TOut>
			if (!isStereo(typeIn) && isStereo(typeOut) && allowMonoToStereo)
			{
				Type typeInStereo = typeof(Stereo<>).MakeGenericType(typeIn);

				if (tryConvert(input, typeIn, typeInStereo, out ISignal intermediate, false) &&
				    tryConvert(intermediate, typeInStereo, typeOut, out output, false))
					return true;
			}

			output = null;
			return false;
		}

		/// <summary>
		/// Tries to create a sample type conversion wrapper for the specified input.
		/// </summary>
		/// <returns></returns>
		public static bool TryConvert(this ISignal input, Type typeIn, Type typeOut, out ISignal output)
		{
			return tryConvert(input, typeIn, typeOut, out output, true);
		}
	
		/// <summary>
		/// Tries to create a sample type conversion wrapper for the specified input.
		/// </summary>
		/// <returns></returns>
		public static bool TryConvert<TIn, TOut>(this ISignal<TIn> input, out ISignal<TOut> output)
		{
			output = null;
			if (!TryConvert(input, typeof(TIn), typeof(TOut), out ISignal temp))
				return false;

			output = (ISignal<TOut>)temp;
			return true;
		}

		/// <summary>
		/// Returns a sample type conversion wrapper for the specified input.
		/// </summary>
		/// <typeparam name="TIn"></typeparam>
		/// <typeparam name="TOut"></typeparam>
		/// <param name="input"></param>
		/// <returns></returns>
		public static ISignal<TOut> Convert<TIn, TOut>(this ISignal<TIn> input)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));

			if (!input.TryConvert(out ISignal<TOut> output))
			{
				throw new InvalidCastException($"There is no method defined to convert samples from {typeof(TIn)} to {typeof(TOut)}.");
			}

			return output;
		}

		/// <summary>
		/// Tries to convert an <see cref="ISignal"/> to a readable audio source of a certain sample type.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="input"></param>
		/// <param name="output"></param>
		/// <returns></returns>
		public static bool TryConvert<T>(this ISignal input, out ISignal<T> output)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));

			if (input is ISignal<T> input2)
			{
				output = input2;
				return true;
			}

			Type inputType = input.GetType();
			Type outType = typeof(T);

			foreach (Type iface in inputType.GetInterfaces())
			{
				if (!iface.IsConstructedGenericType)
					continue;

				if (iface.GetGenericTypeDefinition() != typeof(ISignal<>))
					continue;

				Type sampleType = iface.GetGenericArguments()[0];

				if (TryConvert(input, sampleType, outType, out ISignal temp))
				{
					output = (ISignal<T>)temp;
					return true;
				}
			}

			output = null;
			return false;
		}

		/// <summary>
		/// Converts an <see cref="ISignal"/> to a readable audio source of a certain sample type.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="input"></param>
		/// <returns></returns>
		public static ISignal<T> Convert<T>(this ISignal input)
		{
			if (input == null)
				throw new ArgumentNullException(nameof(input));

			if (!input.TryConvert(out ISignal<T> output))
			{
				if (!input.GetType().GetInterfaces().Any(iface => iface.IsConstructedGenericType && iface.GetGenericTypeDefinition() == typeof(ISignal<>)))
					throw new InvalidCastException("The audio source does not implement IReadableAudioSource<T>.");

				throw new InvalidCastException($"There is no method defined to convert samples from the audio source to {typeof(T)}.");
			}

			return output;
		}
	}
}
