﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Flags that change the behavior of <see cref="ImmutableOperatorSignal{TIn,TOut}"/>.
	/// </summary>
	[Flags]
	public enum OperatorSignalFlags
	{
		None = 0,

		/// <summary>
		/// If this flag is not set, all sources added to the <see cref="ImmutableOperatorSignal{TIn,TOut}"/> will be disposed when the <see cref="ImmutableOperatorSignal{TIn,TOut}"/> is diposed.
		/// </summary>
		LeaveOpen = 0x01,

		/// <summary>
		/// If this flag is set, the <see cref="ImmutableOperatorSignal{TIn,TOut}"/> will never return -1 in <see cref="ImmutableOperatorSignal{TIn,TOut}.Read"/>.
		/// It will instead return 0 until there are samples available again.
		/// For example, a mixer that is supposed to be used for a longer period of time but has no channels at some point should set this flag so that it does not end prematurely.
		/// </summary>
		Continuous = 0x02,

		Default = None,
	}
}