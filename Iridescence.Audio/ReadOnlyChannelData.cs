﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents multi-channel audio data.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public readonly ref struct ReadOnlyChannelData<T>
	{
		private readonly ReadOnlySpan<ReadOnlyMemory<T>> data;
		private readonly int offset;
		private readonly int count;

		/// <summary>
		/// The number of samples in the buffer.
		/// </summary>
		public int SampleCount => this.count;

		/// <summary>
		/// The number of channels in the buffer.
		/// </summary>
		public int ChannelCount => this.data.Length;

		public ReadOnlySpan<T> this[int i] => this.data[i].Slice(this.offset, this.count).Span;

		public ReadOnlyChannelData(ReadOnlySpan<ReadOnlyMemory<T>> data)
			: this(data, 0, data[0].Length)
		{

		}

		public ReadOnlyChannelData(ReadOnlySpan<ReadOnlyMemory<T>> data, int offset, int count)
		{
			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));

			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count));

			int end = offset + count;
			for (int i = 0; i < data.Length; ++i)
			{
				if (end > data[i].Length)
					throw new ArgumentOutOfRangeException(nameof(count));
			}

			this.data = data;
			this.offset = offset;
			this.count = count;
		}

		public ReadOnlyChannelData<T> Slice(int start)
		{
			return new ReadOnlyChannelData<T>(this.data, this.offset + start, this.count - start);
		}
		
		public ReadOnlyChannelData<T> Slice(int start, int length)
		{
			return new ReadOnlyChannelData<T>(this.data, this.offset + start, length);
		}
	}
}
