﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using Iridescence.Audio.Flac;
using Iridescence.Math;

namespace Iridescence.Audio.IO
{
	/// <summary>
	/// Implements opening FLAC files as <see cref="ISignal"/>.
	/// </summary>
	public static class Flac
	{
		public static ISignal Open(Stream stream, bool leaveOpen = false)
		{
			FlacReader decoder = new FlacReader(stream);

			decoder.ReadFlacHeader();

			int channels = -1;
			int sampleRate = -1;
			int bitsPerSample = -1;
			long length = -1;

			MetadataBlock block;
			while ((block = decoder.NextMetadataBlock()) != null)
			{
				if (block is StreamInfo info)
				{
					channels = info.Channels;
					sampleRate = info.SampleRate;
					length = info.Samples;
					bitsPerSample = info.BitsPerSample;
				}
			}

			if (bitsPerSample <= 0)
				throw new InvalidDataException("Invalid flac stream.");

			if (channels == 1)
			{
				if (bitsPerSample <= 8)
					return new CastIntToSNorm8(new FlacSignal(stream, decoder, leaveOpen, sampleRate, length, 8 - bitsPerSample), false);
				
				if (bitsPerSample <= 16)
					return new CastIntToSNorm16(new FlacSignal(stream, decoder, leaveOpen, sampleRate, length, 16 - bitsPerSample), false);
				
				if (bitsPerSample <= 24)
					return new CastIntToSNorm24(new FlacSignal(stream, decoder, leaveOpen, sampleRate, length, 24 - bitsPerSample), false);

				return new FlacSignal(stream, decoder, leaveOpen, sampleRate, length, 32 - bitsPerSample);
			}

			if (channels == 2)
			{
				if (bitsPerSample <= 8)
					return new StereoCastIntToSNorm8(new StereoFlacSignal(stream, decoder, leaveOpen, sampleRate, length, 8 - bitsPerSample), false);
				
				if (bitsPerSample <= 16)
					return new StereoCastIntToSNorm16(new StereoFlacSignal(stream, decoder, leaveOpen, sampleRate, length, 16 - bitsPerSample), false);
				
				if (bitsPerSample <= 24)
					return new StereoCastIntToSNorm24(new StereoFlacSignal(stream, decoder, leaveOpen, sampleRate, length, 24 - bitsPerSample), false);

				return new StereoFlacSignal(stream, decoder, leaveOpen, sampleRate, length, 32 - bitsPerSample);
			}

			throw new NotSupportedException("Only mono and stereo FLAC files are supported");
		}

		private abstract class FlacSignalBase : ISignal
		{
			#region Fields

			private readonly Stream stream;
			private readonly FlacReader decoder;
			private readonly bool leaveOpen;
			private Memory<int>[] memoryArray;
			private int[] buffer;
			private long frameOffset;
			private bool anyFrameRead;

			#endregion

			#region Properties

			public bool CanSeek => this.stream.CanSeek;

			public long Position
			{
				get => this.frameOffset + this.SampleOffset;
				set
				{
					if (!this.anyFrameRead)
					{
						this.decoder.NextFrame(out _);
					}

					long newPos = this.decoder.Seek(value);

					this.frameOffset = newPos;
					this.SampleOffset = (int)(value - newPos);
					this.SampleCount = 0;
					this.BufferStride = 0;
				}
			}

			public long Length { get; }
			public int SampleRate { get; }

			protected int[] Buffer => this.buffer;
			protected int BufferStride { get; private set; }

			protected int SampleOffset { get; set; }
			protected int SampleCount { get; private set; }

			#endregion

			#region Constructors

			protected FlacSignalBase(Stream stream, FlacReader decoder, bool leaveOpen, int sampleRate, long length)
			{
				this.stream = stream;
				this.decoder = decoder;
				this.leaveOpen = leaveOpen;
				this.SampleRate = sampleRate;
				this.Length = length;
			}

			#endregion

			#region Methods

			protected bool ReadFrame()
			{
				this.frameOffset += this.SampleCount;

				if (!this.decoder.NextFrame(out FrameHeader header))
				{
					this.SampleCount = -1;
					this.BufferStride = 0;
					return false;
				}

				this.anyFrameRead = true;

				this.BufferStride = header.SampleCount;

				int totalSamples = this.BufferStride * header.Channels;
				if (this.buffer == null)
				{
					this.buffer = new int[totalSamples];
				}
				else if (totalSamples > this.buffer.Length)
				{
					Array.Resize(ref this.buffer, totalSamples);
				}

				if (this.memoryArray == null || this.memoryArray.Length < header.Channels)
				{
					this.memoryArray = new Memory<int>[header.Channels];
				}

				for (int i = 0; i < header.Channels; ++i)
				{
					this.memoryArray[i] = new Memory<int>(this.buffer, i * this.BufferStride, this.BufferStride);
				}

				this.decoder.ReadFrame(this.memoryArray);

				this.SampleCount = header.SampleCount;

				return true;
			}

			public void Dispose()
			{
				if (!this.leaveOpen)
				{
					this.stream.Close();
				}
			}

			#endregion
		}

		private sealed class FlacSignal : FlacSignalBase, ISignal<int>
		{
			#region Fields

			private readonly int shift;

			#endregion

			#region Constructors

			public FlacSignal(Stream stream, FlacReader decoder, bool leaveOpen, int sampleRate, long length, int shift)
				: base(stream, decoder, leaveOpen, sampleRate, length)
			{
				this.shift = shift;
			}

			#endregion

			#region Methods

			public int Read(Span<int> samples)
			{
				if (samples.Length == 0)
					return 0;

				int total = -1;

				while (samples.Length > 0)
				{
					bool eof = false;
					while (this.SampleOffset >= this.SampleCount)
					{
						if (!this.ReadFrame())
						{
							eof = true;
							break;
						}

						this.SampleOffset -= this.SampleCount;
					}

					if (eof)
						break;

					int n = Utility.Min(samples.Length, this.SampleCount - this.SampleOffset);

					for (int i = 0, j = this.SampleOffset; i < n; ++i, ++j)
					{
						samples[i] = this.Buffer[j] << this.shift;
					}

					this.SampleOffset += n;

					if (total < 0)
						total = n;
					else
						total += n;

					samples = samples.Slice(n);
				}

				return total;
			}

			#endregion
		}

		private sealed class StereoFlacSignal : FlacSignalBase, ISignal<Stereo<int>>
		{
			#region Fields

			private readonly int shift;

			#endregion

			#region Constructors

			public StereoFlacSignal(Stream stream, FlacReader decoder, bool leaveOpen, int sampleRate, long length, int shift)
				: base(stream, decoder, leaveOpen, sampleRate, length)
			{
				this.shift = shift;
			}

			#endregion

			#region Methods

			public int Read(Span<Stereo<int>> samples)
			{
				if (samples.Length == 0)
					return 0;

				int total = -1;

				while (samples.Length > 0)
				{
					bool eof = false;
					while (this.SampleOffset >= this.SampleCount)
					{
						this.SampleOffset -= this.SampleCount;

						if (!this.ReadFrame())
						{
							eof = true;
							break;
						}
					}

					if (eof)
						break;

					int n = Utility.Min(samples.Length, this.SampleCount - this.SampleOffset);

					for (int i = 0, j = this.SampleOffset; i < n; ++i, ++j)
					{
						samples[i].L = this.Buffer[j] << this.shift;
						samples[i].R = this.Buffer[this.BufferStride + j] << this.shift;
					}

					this.SampleOffset += n;

					if (total < 0)
						total = n;
					else
						total += n;

					samples = samples.Slice(n);
				}

				return total;
			}

			#endregion
		}

		private sealed class CastIntToSNorm8 : SignalProcessorBase<int, SNorm8>
		{
			public CastIntToSNorm8(ISignal<int> input, bool leaveOpen) : base(input, leaveOpen)
			{
			}

			public override int Read(Span<SNorm8> samples)
			{
				if (samples.Length == 0)
					return 0;

				Span<int> input = stackalloc int[Utility.Min(256, samples.Length)];
				int total = -1;
				while (samples.Length > 0)
				{
					int requested = Utility.Min(samples.Length, input.Length);
					int n = this.Input.Read(input.Slice(0, requested));
					if (n < 0)
						break;

					for (int i = 0; i < n; ++i)
					{
						samples[i] = SNorm8.FromRawValue(unchecked((sbyte)input[i]));
					}

					if (total < 0)
						total = n;
					else
						total += n;

					if(n != requested)
						break;
					samples = samples.Slice(n);
				}

				return total;
			}
		}

		private sealed class CastIntToSNorm16 : SignalProcessorBase<int, SNorm16>
		{
			public CastIntToSNorm16(ISignal<int> input, bool leaveOpen) : base(input, leaveOpen)
			{
			}

			public override int Read(Span<SNorm16> samples)
			{
				if (samples.Length == 0)
					return 0;

				Span<int> input = stackalloc int[Utility.Min(256, samples.Length)];
				int total = -1;
				while (samples.Length > 0)
				{
					int requested = Utility.Min(samples.Length, input.Length);
					int n = this.Input.Read(input.Slice(0, requested));
					if (n < 0)
						break;

					for (int i = 0; i < n; ++i)
					{
						samples[i] = SNorm16.FromRawValue(unchecked((short)input[i]));
					}

					if (total < 0)
						total = n;
					else
						total += n;

					if(n != requested)
						break;
					samples = samples.Slice(n);
				}

				return total;
			}
		}

		private sealed class CastIntToSNorm24 : SignalProcessorBase<int, SNorm24>
		{
			public CastIntToSNorm24(ISignal<int> input, bool leaveOpen) : base(input, leaveOpen)
			{
			}

			public override int Read(Span<SNorm24> samples)
			{
				if (samples.Length == 0)
					return 0;

				Span<int> input = stackalloc int[Utility.Min(256, samples.Length)];
				int total = -1;
				while (samples.Length > 0)
				{
					int requested = Utility.Min(samples.Length, input.Length);
					int n = this.Input.Read(input.Slice(0, requested));
					if (n < 0)
						break;

					for (int i = 0; i < n; ++i)
					{
						samples[i] = SNorm24.FromRawValue(new Int24(input[i]));
					}

					if (total < 0)
						total = n;
					else
						total += n;

					if(n != requested)
						break;
					samples = samples.Slice(n);
				}

				return total;
			}
		}

		private sealed class StereoCastIntToSNorm8 : SignalProcessorBase<Stereo<int>, Stereo<SNorm8>>
		{
			public StereoCastIntToSNorm8(ISignal<Stereo<int>> input, bool leaveOpen) : base(input, leaveOpen)
			{
			}

			public override int Read(Span<Stereo<SNorm8>> samples)
			{
				if (samples.Length == 0)
					return 0;

				Span<int> temp = stackalloc int[Utility.Min(512, 2 * samples.Length)];
				Span<Stereo<int>> input = MemoryMarshal.Cast<int, Stereo<int>>(temp);
				int total = -1;
				while (samples.Length > 0)
				{
					int requested = Utility.Min(samples.Length, input.Length);
					int n = this.Input.Read(input.Slice(0, requested));
					if (n < 0)
						break;

					for (int i = 0; i < n; ++i)
					{
						samples[i].L = SNorm8.FromRawValue(unchecked((sbyte)input[i].L));
						samples[i].R = SNorm8.FromRawValue(unchecked((sbyte)input[i].R));
					}

					if (total < 0)
						total = n;
					else
						total += n;

					if (n != requested)
						break;
					samples = samples.Slice(n);
				}

				return total;
			}
		}

		private sealed class StereoCastIntToSNorm16 : SignalProcessorBase<Stereo<int>, Stereo<SNorm16>>
		{
			public StereoCastIntToSNorm16(ISignal<Stereo<int>> input, bool leaveOpen) : base(input, leaveOpen)
			{
			}

			public override int Read(Span<Stereo<SNorm16>> samples)
			{
				if (samples.Length == 0)
					return 0;

				Span<int> temp = stackalloc int[Utility.Min(512, 2 * samples.Length)];
				Span<Stereo<int>> input = MemoryMarshal.Cast<int, Stereo<int>>(temp);
				int total = -1;
				while (samples.Length > 0)
				{
					int requested = Utility.Min(samples.Length, input.Length);
					int n = this.Input.Read(input.Slice(0, requested));
					if (n < 0)
						break;

					for (int i = 0; i < n; ++i)
					{
						samples[i].L = SNorm16.FromRawValue(unchecked((short)input[i].L));
						samples[i].R = SNorm16.FromRawValue(unchecked((short)input[i].R));
					}

					if (total < 0)
						total = n;
					else
						total += n;

					if(n != requested)
						break;
					samples = samples.Slice(n);
				}

				return total;
			}
		}

		private sealed class StereoCastIntToSNorm24 : SignalProcessorBase<Stereo<int>, Stereo<SNorm24>>
		{
			public StereoCastIntToSNorm24(ISignal<Stereo<int>> input, bool leaveOpen) : base(input, leaveOpen)
			{
			}

			public override int Read(Span<Stereo<SNorm24>> samples)
			{
				if (samples.Length == 0)
					return 0;

				Span<int> temp = stackalloc int[Utility.Min(512, 2 * samples.Length)];
				Span<Stereo<int>> input = MemoryMarshal.Cast<int, Stereo<int>>(temp);
				int total = -1;
				while (samples.Length > 0)
				{
					int requested = Utility.Min(samples.Length, input.Length);
					int n = this.Input.Read(input.Slice(0, requested));
					if (n < 0)
						break;

					for (int i = 0; i < n; ++i)
					{
						samples[i].L = SNorm24.FromRawValue(new Int24(input[i].L));
						samples[i].R = SNorm24.FromRawValue(new Int24(input[i].R));
					}

					if (total < 0)
						total = n;
					else
						total += n;

					if(n != requested)
						break;
					samples = samples.Slice(n);
				}

				return total;
			}
		}
	}
}
