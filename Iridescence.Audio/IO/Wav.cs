﻿using System;
using System.IO;
using Iridescence.Math;

namespace Iridescence.Audio.IO
{
	/// <summary>
	/// Implements IO of PCM .wav files.
	/// </summary>
	public static class Wav
	{
		private const uint formatChunk = 0x666D7420u;
		private const uint dataChunk = 0x64617461u;
		
		/// <summary>
		/// Opens the specified stream as <see cref="ISignal"/>.
		/// </summary>
		/// <param name="stream">The underlying stream that contains the WAV data.</param>
		/// <param name="leaveOpen">True, if the stream should be kept open when the <see cref="ISignal"/> is disposed. False to also dispose of the stream.</param>
		/// <returns></returns>
		/// <remarks>The stream has to be kept open as long as samples are read from the audio source. Closing the stream will also close the audio source.</remarks>
		public static ISignal Open(Stream stream, bool leaveOpen = false)
		{
			if (stream.ReadUInt32B() != 0x52494646u) // RIFF
				throw new InvalidDataException("No 'RIFF' header found.");

			uint size = stream.ReadUInt32L();
			stream = new SegmentStream(stream, stream.CanSeek ? stream.Position : -1, size, false);
			if (stream.ReadUInt32B() != 0x57415645u) // WAVE
				throw new InvalidDataException("No 'WAVE' header found.");
			
			bool fmtFound = false;
			int type = 0;
			int numChannels = 0;
			int sampleRate = 0;
			int stride = 0;
			int bitsPerSample = 0;

			for (;;)
			{
				uint chunkType = stream.ReadUInt32B();
				uint chunkSize = stream.ReadUInt32L();
				Stream chunkStream = new SegmentStream(stream, stream.CanSeek ? stream.Position : -1, chunkSize, false);

				switch (chunkType)
				{
					case formatChunk: // fmt
						type = chunkStream.ReadUInt16L();
						numChannels = chunkStream.ReadUInt16L();
						sampleRate = chunkStream.ReadInt32L();
						chunkStream.ReadInt32L(); // byte rate ??
						stride = chunkStream.ReadUInt16L();
						bitsPerSample = chunkStream.ReadUInt16L();
						fmtFound = true;
						break;

					case dataChunk: // data
						if (!fmtFound)
							throw new InvalidDataException("Found data before format.");

						if (type == 1)
						{
							if (bitsPerSample == 8)
							{
								if (numChannels == 1)
									return new SampleStream<byte>(chunkStream, stride, sampleRate, leaveOpen);
								if (numChannels == 2)
									return new SampleStream<Stereo<byte>>(chunkStream, stride, sampleRate, leaveOpen);
							}
							else if (bitsPerSample == 16)
							{
								if (numChannels == 1)
									return new SampleStream<SNorm16>(chunkStream, stride, sampleRate, leaveOpen);
								if (numChannels == 2)
									return new SampleStream<Stereo<SNorm16>>(chunkStream, stride, sampleRate, leaveOpen);
							}
							else if (bitsPerSample == 24)
							{
								if (numChannels == 1)
									return new SampleStream<SNorm24>(chunkStream, stride, sampleRate, leaveOpen);
								if (numChannels == 2)
									return new SampleStream<Stereo<SNorm24>>(chunkStream, stride, sampleRate, leaveOpen);
							}
							else
							{
								throw new NotSupportedException("The wave sample format is not supported.");
							}
						}
						else if (type == 3)
						{
							if (bitsPerSample == 32)
							{
								if (numChannels == 1)
									return new SampleStream<float>(chunkStream, stride, sampleRate, leaveOpen);
								if (numChannels == 2)
									return new SampleStream<Stereo<float>>(chunkStream, stride, sampleRate, leaveOpen);
							}
							else
							{
								throw new NotSupportedException("The wave sample format is not supported.");
							}
						}

						break;
				}

				chunkStream.ReadToEnd();
			}
		}

		private sealed class SampleStream<T> : ISignal<T> where T : struct
		{
			private readonly bool leaveOpen;
			private readonly byte[] buffer;
			private int bufferOffset;
			private readonly Stream stream;
			private readonly int stride;
			private long position;

			public bool CanSeek => this.stream.CanSeek;

			public long Position
			{
				get => this.position;
				set
				{
					this.position = value;
					this.stream.Position = this.position * this.stride;
					this.bufferOffset = 0;
				}
			}

			public long Length { get; }

			public int SampleRate { get; }

			public SampleStream(Stream stream, int stride, int sampleRate, bool leaveOpen)
			{
				this.stream = stream;
				this.stride = stride;
				this.buffer = new byte[stride * 4096];
				this.Length = stream.Length / stride;
				this.SampleRate = sampleRate;
				this.leaveOpen = leaveOpen;
			}

			public int Read(Span<T> samples)
			{
				return this.readBuffer(samples.Length, samples.AsBytes());
			}

			private int readBuffer(int maxSamples, Span<byte> dest)
			{
				int total = -1;

				while(dest.Length > 0)
				{
					int numBytesToRead = Utility.Min(this.buffer.Length, maxSamples * this.stride) - this.bufferOffset;
					int numBytesRead = this.stream.Read(this.buffer, this.bufferOffset, numBytesToRead);
					if (numBytesRead == 0)
						break;

					int bufferedBytes = this.bufferOffset + numBytesRead;
					int bufferedSamples = bufferedBytes / this.stride;
					if (bufferedSamples == 0)
						continue;

					int usedBytes = bufferedSamples * this.stride;
					new Span<byte>(this.buffer, 0, usedBytes).CopyTo(dest);
					dest = dest.Slice(usedBytes);

					if (total < 0)
						total = bufferedSamples;
					else
						total += bufferedSamples;

					this.bufferOffset = bufferedBytes - usedBytes;
					Array.Copy(this.buffer, usedBytes, this.buffer, 0, this.bufferOffset);

					this.position += bufferedSamples;
				}

				return total;
			}

			public void Dispose()
			{
				if (!this.leaveOpen)
				{
					this.stream.Dispose();
				}
			}
		}
	}
}
