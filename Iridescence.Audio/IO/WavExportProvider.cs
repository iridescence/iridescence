﻿using System;
using System.IO;
using Iridescence.FileSystem.Composition;
using File = Iridescence.FileSystem.File;

namespace Iridescence.Audio.IO
{
	/// <summary>
	/// Exports <see cref="ISignal"/> instances from .wav files.
	/// </summary>
	public class WavExportProvider : FileExportDescriptorProviderBase
	{
		#region Properties

		protected override Type PartType => typeof(ISignal);

		protected override FileExportSharingMode SharingMode => FileExportSharingMode.None;

		#endregion
	
		#region Methods

		protected override bool IsSupportedName(string name)
		{
			return name.EndsWith(".wav");
		}
		
		protected override object Load(File file)
		{
			Stream stream = file.Open(FileMode.Open, FileAccess.Read);
			return Wav.Open(stream);
		}

		#endregion
	}
}
