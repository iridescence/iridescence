﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Implements an <see cref="ISignal{T}"/> that can be paused. The source will output silence for as long as it is paused.
	/// </summary>
	public class PausableSignal<T> : SignalProcessorBase<T, T>
	{
		#region Fields

		private bool ended;

		#endregion

		#region Properties

		public bool Paused { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PausableSignal{T}"/> class. 
		/// </summary>
		public PausableSignal(ISignal<T> source, bool leaveOpen)
			: base(source, leaveOpen)
		{

		}

		#endregion
		
		#region Methods
		
		public override int Read(Span<T> samples)
		{
			if (this.ended)
				return -1;

			if (this.Paused)
			{
				samples.Clear();
				return samples.Length;
			}

			return this.Input.Read(samples);
		}

		public override void Dispose()
		{
			this.ended = true;
			base.Dispose();
		}

		#endregion
	}

	public static class PausableSignalExtensions
	{
		/// <summary>
		/// Returns a <see cref="PausableSignal{T}"/> that wraps the specified source.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="leaveOpen"></param>
		/// <returns></returns>
		public static PausableSignal<T> AsPausable<T>(this ISignal<T> source, bool leaveOpen = false)
		{
			return new PausableSignal<T>(source, leaveOpen);
		}
	}
}
