﻿namespace Iridescence.Audio
{
	internal static class AudioConstants
	{
		/// <summary>
		/// The default buffer size for most audio buffers used in various components.
		/// </summary>
		public const int DefaultBufferSize = 4096;
	}
}
