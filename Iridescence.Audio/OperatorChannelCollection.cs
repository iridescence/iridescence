﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Audio
{
	/// <summary>
	/// Manages the sources of a <see cref="ImmutableOperatorSignal{TIn,TOut}"/>.
	/// </summary>
	public class OperatorChannelCollection<T> : IList<ISignal<T>>, INotifyListChanged<ISignal<T>>
	{
		#region Events

		public event CollectionChangedEventHandler<ListItem<ISignal<T>>> CollectionChanged; 

		#endregion

		#region Fields

		private readonly Func<ISignal<T>, OperatorChannelFlags, OperatorChannel<T>> channelFactory;
		private readonly List<OperatorChannel<T>> channels;

		#endregion

		#region Properties

		internal object Lock { get; }

		internal IReadOnlyList<OperatorChannel<T>> Channels => this.channels;

		bool ICollection<ISignal<T>>.IsReadOnly => false;

		public ISignal<T> this[int index]
		{
			get
			{
				lock (this.Lock)
				{
					return this.channels[index].Source;
				}
			}
			set
			{
				lock (this.Lock)
				{
					OperatorChannel<T> channel = this.channels[index];
					if (ReferenceEquals(channel.Source, value))
						return;

					this.channels[index] = this.channelFactory(value, OperatorChannelFlags.Default);
					this.OnCollectionChanged(ListChange.Replace(
						new ListItem<ISignal<T>>(index, channel.Source),
						new ListItem<ISignal<T>>(index, value)));
				}
			}
		}

		public int Count
		{
			get
			{
				lock (this.Lock)
				{
					return this.channels.Count;
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="OperatorChannelCollection{T}"/> class. 
		/// </summary>
		internal OperatorChannelCollection(Func<ISignal<T>, OperatorChannelFlags, OperatorChannel<T>> channelFactory)
		{
			this.channelFactory = channelFactory;
			this.channels = new List<OperatorChannel<T>>();
			this.Lock = new object();
		}
		
		#endregion
		
		#region Methods
		
		protected void OnCollectionChanged(CollectionChangedEventArgs<ListItem<ISignal<T>>> e)
		{
			this.CollectionChanged?.Invoke(this, e);
		}

		private void removeAt(int index)
		{
			OperatorChannel<T> ch = this.channels[index];
			var e = ListChange.Remove(index, ch.Source);
			this.channels.RemoveAt(index);
			this.OnCollectionChanged(e);
		}
		
		void IList<ISignal<T>>.Insert(int index, ISignal<T> item) => this.Insert(index, item);
		
		public void Insert(int index, ISignal<T> item, OperatorChannelFlags flags = OperatorChannelFlags.Default)
		{
			if (item == null)
				throw new ArgumentNullException(nameof(item));
			
			lock (this.Lock)
			{
				this.channels.Insert(index, this.channelFactory(item, flags));
				this.OnCollectionChanged(ListChange.Add(index, item));
			}
		}

		void ICollection<ISignal<T>>.Add(ISignal<T> item) => this.Add(item);

		public void Add(ISignal<T> item, OperatorChannelFlags flags = OperatorChannelFlags.Default)
		{
			if (item == null)
				throw new ArgumentNullException(nameof(item));

			lock (this.Lock)
			{
				this.channels.Add(this.channelFactory(item, flags));
				this.OnCollectionChanged(ListChange.Add(this.channels.Count - 1, item));
			}
		}

		public void Add(IEnumerable<ISignal<T>> sources, OperatorChannelFlags flags = OperatorChannelFlags.Default)
		{
			if (sources == null)
				throw new ArgumentNullException(nameof(sources));

			lock(this.Lock)
			{
				OperatorChannel<T>[] addedChannels = sources.Select(s => this.channelFactory(s, flags)).ToArray();
				ListItem<ISignal<T>>[] addedItems = new ListItem<ISignal<T>>[addedChannels.Length];
				for (int i = 0; i < addedChannels.Length; ++i)
				{
					addedItems[i] = (this.channels.Count, addedChannels[i].Source);
					this.channels.Add(this.channelFactory(addedChannels[i].Source, OperatorChannelFlags.Default));
				}

				this.OnCollectionChanged(ListChange.Add(addedItems));
			}
		}

		public void Add(params ISignal<T>[] sources)
		{
			if (sources == null)
				throw new ArgumentNullException(nameof(sources));

			lock (this.Lock)
			{
				ListItem<ISignal<T>>[] addedItems = new ListItem<ISignal<T>>[sources.Length];
				for (int i = 0; i < sources.Length; ++i)
				{
					ISignal<T> source = sources[i];
					addedItems[i] = (this.channels.Count, source);
					this.channels.Add(this.channelFactory(source, OperatorChannelFlags.Default));
				}

				this.OnCollectionChanged(ListChange.Add(addedItems));
			}
		}

		internal bool Remove(OperatorChannel<T> channel)
		{
			lock (this.Lock)
			{
				int index = this.channels.IndexOf(channel);
				if (index < 0)
					return false;

				this.removeAt(index);
				return true;
			}
		}

		public bool Remove(ISignal<T> item)
		{
			lock (this.Lock)
			{
				int index = this.channels.FindIndex(c => ReferenceEquals(item, c.Source));
				if (index < 0)
					return false;

				this.removeAt(index);
				return true;
			}
		}
		
		public void RemoveAt(int index)
		{
			lock (this.Lock)
			{
				this.removeAt(index);
			}
		}

		public void Clear()
		{
			lock (this.Lock)
			{
				var e = ListChange.Clear(this.channels
					.Select((ch, i) => new ListItem<ISignal<T>>(i, ch.Source))
					.ToArray());
				this.channels.Clear();
				this.OnCollectionChanged(e);
			}
		}

		public bool Contains(ISignal<T> item)
		{
			if (item == null)
				return false;

			lock (this.Lock)
			{
				return this.channels.Any(ch => ReferenceEquals(item, ch.Source));
			}
		}
	
		public int IndexOf(ISignal<T> item)
		{
			if (item == null)
				return -1;

			lock (this.Lock)
			{
				return this.channels.FindIndex(ch => ReferenceEquals(item, ch.Source));
			}
		}

		public void CopyTo(ISignal<T>[] array, int arrayIndex)
		{
			lock (this.Lock)
			{
				for (int i = 0; i < this.channels.Count; ++i)
				{
					array[arrayIndex + i] = this.channels[i].Source;
				}
			}
		}
		
		public IEnumerator<ISignal<T>> GetEnumerator()
		{
			lock (this.Lock)
			{
				return this.channels.Select(ch => ch.Source).GetEnumerator();
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		#endregion
	}
}
