﻿using System;
using Iridescence.Math;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents a single channel in a <see cref="ImmutableOperatorSignal{TIn,TOut}"/>.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	internal sealed class OperatorChannel<T>
	{
		#region Fields

		public readonly ISignal<T> Source;
		public readonly long InitialPosition;
		public readonly T[] Buffer;
		public int BufferCount;
		public int SamplesAvailable;
		public bool Ended;
		public readonly OperatorChannelFlags Flags;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public OperatorChannel(ISignal<T> source, OperatorChannelFlags flags, int bufferSize)
		{
			this.Source = source;
			this.InitialPosition = source.CanSeek ? source.Position : -1;
			this.Buffer = new T[bufferSize];
			this.BufferCount = 0;
			this.SamplesAvailable = 0;
			this.Ended = false;
			this.Flags = flags;
		}
		
		#endregion

		#region Methods

		public void Clear()
		{
			this.BufferCount = 0;
			this.Ended = false;
			this.SamplesAvailable = 0;
		}

		public void Acquire(int samplesRequired)
		{
			int missingSamples = Utility.Min(this.Buffer.Length, samplesRequired) - this.BufferCount;
			int samplesRead;
			if (missingSamples > 0 && !this.Ended)
			{
				samplesRead = this.Source.Read(new Span<T>(this.Buffer, this.BufferCount, missingSamples));
				if (samplesRead < 0)
				{
					this.Ended = true;
					if (this.BufferCount > 0)
						samplesRead = Utility.Min(samplesRequired, this.BufferCount);
				}
				else
				{
					samplesRead += this.BufferCount;
				}
			}
			else
			{
				if (this.BufferCount == 0 && this.Ended)
					samplesRead = -1;
				else
					samplesRead = Utility.Min(samplesRequired, this.BufferCount);
			}

			this.SamplesAvailable = samplesRead;
		}

		public void Move(int samplesProcessed)
		{
			if (samplesProcessed < this.SamplesAvailable)
			{
				if (samplesProcessed > 0)
				{
					this.BufferCount = this.SamplesAvailable - samplesProcessed;
					Array.Copy(this.Buffer, samplesProcessed, this.Buffer, 0, this.BufferCount);
				}
			}
			else
			{
				this.BufferCount = 0;
			}
		}

		#endregion
	}
}
