﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents a looping audio source that loops part of another <see cref="ISignal{T}"/>.
	/// </summary>
	public class Loop<T> : SignalProcessorBase<T, T>, ICompletableSignal
	{
		#region Fields

		private bool isCompleted;

		#endregion

		#region Properties

		public override bool CanSeek => false;

		public override long Position 
		{
			get => throw new NotSupportedException("The audio source does not support seeking.");
			set => throw new NotSupportedException("The audio source does not support seeking.");
		}

		public override long Length => throw new NotSupportedException("The audio source has no length because it is an infinite loop.");

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Loop{T}"/> class. 
		/// </summary>
		public Loop(ISignal<T> source, bool leaveOpen)
			: base(source, leaveOpen)
		{

		}

		#endregion
		
		#region Methods

		public override int Read(Span<T> samples)
		{
			if (this.isCompleted)
				return -1;

			int total = 0;
			bool isLooped = false;
			while (samples.Length > 0)
			{
				int n = this.Input.Read(samples);
				if (n < 0)
				{
					if (isLooped)
					{
						if (total == 0)
							total = -1;

						break;
					}

					isLooped = true;
					this.Input.Position = 0;
					continue;
				}

				if(n == 0)
					break;

				isLooped = false;
				samples = samples.Slice(n);
				total += n;
			}

			return total;
		}

		public void Complete()
		{
			this.isCompleted = true;
		}

		public override void Dispose()
		{
			this.isCompleted = true;
			base.Dispose();
		}
		
		#endregion
	}

	public static class LoopExtensions
	{
		/// <summary>
		/// Creates a <see cref="Audio.Loop{T}"/> that wraps the specified <see cref="ISignal{T}"/>.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="leaveOpen"></param>
		/// <returns></returns>
		public static Loop<T> Loop<T>(this ISignal<T> source, bool leaveOpen = false)
		{
			return new Loop<T>(source, leaveOpen);
		}
	}
}
