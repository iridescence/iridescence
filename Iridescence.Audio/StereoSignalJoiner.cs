﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Merges two independent mono audio sources into one stereo audio source.
	/// </summary>
	public class StereoSignalJoiner<T> : ImmutableOperatorSignal<T, Stereo<T>>
	{
		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StereoSignalJoiner{T}"/> class. 
		/// </summary>
		public StereoSignalJoiner(ISignal<T> left, ISignal<T> right, OperatorSignalFlags flags = OperatorSignalFlags.Default, int bufferSize = AudioConstants.DefaultBufferSize)
			: base(left.SampleRate, flags, bufferSize)
		{
			this.Channels.Add(left);
			this.Channels.Add(right);
		}

		#endregion

		#region Methods

		protected override void Process(ReadOnlyChannelData<T> channelData, Span<Stereo<T>> result)
		{
			ReadOnlySpan<T> left = channelData[0];
			ReadOnlySpan<T> right = channelData[1];
			for (int i = 0; i < result.Length; ++i)
			{
				result[i].L = left[i];
				result[i].R = right[i];
			}
		}

		#endregion
	}
}
