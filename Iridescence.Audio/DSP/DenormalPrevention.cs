﻿namespace Iridescence.Audio.DSP
{
	public struct DenormalPrevention
	{
		#region Constants

		public const double Epsilon = 1.0e-8d;

		#endregion

		#region Fields

		private double current;

		public static readonly DenormalPrevention Default = new DenormalPrevention(Epsilon);

		#endregion

		#region Properties

		public DenormalPrevention(double initial)
		{
			this.current = initial;
		}

		#endregion

		#region Methods

		public double Next()
		{
			this.current = -this.current;
			return this.current;
		}

		#endregion
	}
}
