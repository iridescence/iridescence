﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a design for a simple comb filter.
	/// </summary>
	public class CombFilterDesign : IIRFilterDesign
	{
		#region Fields

		private bool negative;
		private double delay;
		private double sharpness;

		#endregion

		#region Properties

		public bool Negative
		{
			get => this.negative;
			set
			{
				if (value != this.negative)
				{
					this.negative = value;
					this.OnPropertyChanged();
				}
			}
		}

		public double Delay
		{
			get => this.delay;
			set
			{
				if (value != this.delay)
				{
					this.delay = value;
					this.OnPropertyChanged();
				}
			}
		}

		public double Sharpness
		{
			get => this.sharpness;
			set
			{
				if (this.sharpness != value)
				{
					this.sharpness = value;
					this.OnPropertyChanged();
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new CombFilterDesign.
		/// </summary>
		public CombFilterDesign()
		{
			this.negative = false;
			this.delay = 100.0d;
			this.sharpness = 0.5d;
		}

		#endregion

		#region Methods

		protected override IIRTap[] GetTaps()
		{
			IIRTap[] coeffs = new IIRTap[2];

			if (this.delay == double.PositiveInfinity || this.delay == double.NegativeInfinity)
			{
				coeffs[0].InputFactor = 1.0d;
				coeffs[1].OutputFactor = 0.0d;
				coeffs[1].Delay = 0.0d;
			}
			else
			{
				coeffs[0].InputFactor = 1.0d - this.sharpness;
				coeffs[1].OutputFactor = (this.negative ? this.sharpness : -this.sharpness);
				coeffs[1].Delay = this.delay;
			}

			return coeffs;
		}

		#endregion
	}
}
