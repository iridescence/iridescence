﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a FIR filter design that delays an input signal.
	/// </summary>
	public class DelayFilterDesign : FIRFilterDesign
	{
		#region Fields

		#endregion

		#region Properties

		#region Delay
		
		/// <summary>
		/// Occurs when <see cref="Delay" /> was changed.
		/// </summary>
		public event ValueChangedEventHandler<double> DelayChanged;

		private double delay;

		/// <summary>
		/// Gets or sets the delay (in samples).
		/// </summary>
		public double Delay
		{
			get => this.delay;
			set
			{
				if (this.delay != value)
				{
					ValueChangedEventArgs<double> e = new ValueChangedEventArgs<double>(this.delay);
					this.delay = value;
					this.OnDelayChanged(e);
					this.OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Called when <see cref="Delay" /> was changed.
		/// </summary>
		/// <param name="e">Holds the old value of the property.</param>
		protected virtual void OnDelayChanged(ValueChangedEventArgs<double> e)
		{
			this.DelayChanged?.Invoke(this, e);
		}


		#endregion

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="DelayFilterDesign"/> class. 
		/// </summary>
		public DelayFilterDesign()
		{
		
		}
		
		#endregion
		
		#region Methods
		
		protected override FIRTap[] GetTaps()
		{
			FIRTap[] tap = new FIRTap[1];
			tap[0].Delay = this.delay;
			tap[0].Factor = 1.0;
			return tap;
		}

		#endregion
	}
}
