﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Contains pole filter normal gain and W.
	/// </summary>
	public struct PoleFilterNormal
	{
		#region Fields

		public double W;
		public double Gain;

		public static readonly PoleFilterNormal Identity = new PoleFilterNormal(0.0, 1.0);

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="PoleFilterNormal"/> class. 
		/// </summary>
		public PoleFilterNormal(double w, double gain)
		{
			this.W = w;
			this.Gain = gain;
		}
		
		#endregion
		
		#region Methods
		
		#endregion
	}
}
