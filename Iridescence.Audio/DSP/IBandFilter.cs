﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents an abstract band pass filter design.
	/// </summary>
	public interface IBandFilter
	{
		/// <summary>
		/// Gets or sets the normalized center frequency of the band.
		/// </summary>
		double FrequencyCenter { get; set; }

		/// <summary>
		/// Gets or sets the normalized frequency width of the band.
		/// </summary>
		double FrequencyWidth { get; set; }
	}
}
