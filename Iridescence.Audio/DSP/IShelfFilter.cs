﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Interface for shelf filters.
	/// </summary>
	public interface IShelfFilter
	{
		/// <summary>
		/// Gets or sets the gain (in decibels) of the shelf filter frequence range.
		/// </summary>
		double Gain { get; set; }
	}
}
