﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Transposed direct form II biquad processor.
	/// https://ccrma.stanford.edu/~jos/fp/Transposed_Direct_Forms.html
	/// </summary>
	public class TransposedDirectFormII : BiquadProcessor
	{
		#region Fields

		private double s1;
		private double s2;

		private DenormalPrevention dp;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TransposedDirectFormII.
		/// </summary>
		public TransposedDirectFormII()
		{
			this.dp = DenormalPrevention.Default;
		}

		#endregion

		#region Methods

		public override double Process(in Biquad biquad, double sample)
		{
			double vOut = this.s1 + biquad.B0 * sample + this.dp.Next();
			this.s1 = this.s2 + biquad.B1 * sample - biquad.A1 * vOut;
			this.s2 = biquad.B2 * sample - biquad.A2 * vOut;
			return vOut;
		}

		#endregion
	}
}
