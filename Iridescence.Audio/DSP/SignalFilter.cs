﻿using System;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Implements an audio source that puts another audio source through a filter.
	/// </summary>
	public class SignalFilter : SignalProcessorBase<float, float>
	{
		#region Fields
		
		#endregion
		
		#region Properties
		
		/// <summary>
		/// Gets the filter.
		/// </summary>
		public Filter Filter { get; }
		
		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignalFilter"/> class. 
		/// </summary>
		public SignalFilter(ISignal<float> source, Filter filter, bool leaveOpen)
			: base(source, leaveOpen)
		{
			this.Filter = filter;
		}

		#endregion
		
		#region Methods
		
		public override int Read(Span<float> samples)
		{
			int n = this.Input.Read(samples);
			if (n <= 0)
				return n;

			this.Filter?.Process(samples.Slice(0, n), samples.Slice(0, n));
			return n;
		}

		#endregion
	}

	public static partial class FilterSourceExtensions
	{
		/// <summary>
		/// Returns an audio source that represents the input signal filtered using the specified <see cref="T:Filter"/>.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="filter"></param>
		/// <param name="leaveOpen"></param>
		/// <returns></returns>
		public static SignalFilter Filter(this ISignal<float> source, Filter filter, bool leaveOpen = false)
		{
			return new SignalFilter(source, filter, leaveOpen);
		}

		/// <summary>
		/// Returns an audio source that represents the input signal filtered using a <see cref="T:Filter"/> created from the specified <see cref="FilterDesign"/>.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="design"></param>
		/// <param name="leaveOpen"></param>
		/// <returns></returns>
		public static SignalFilter Filter(this ISignal<float> source, FilterDesign design, bool leaveOpen = false)
		{
			return source.Filter(design.CreateFilter(), leaveOpen);
		}
	}
}
