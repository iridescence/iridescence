﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a band-stop pole filter design.
	/// </summary>
	public class PoleBandStopFilterDesign : PoleTransformFilterDesign, IBandFilter
	{
		#region Fields

		private double frequencyCenter;
		private double frequencyWidth;

		#endregion

		#region Properties

		public double FrequencyCenter
		{
			get => this.frequencyCenter;
			set
			{
				value = Utility.Clamp(value, 0.000001d, 0.5d);
				if (value != this.frequencyCenter)
				{
					this.frequencyCenter = value;
					this.OnPropertyChanged();
				}
			}
		}

		public double FrequencyWidth
		{
			get => this.frequencyWidth;
			set
			{
				value = Utility.Clamp(value, 0.000001d, 0.5d);
				if (value != this.frequencyWidth)
				{
					this.frequencyWidth = value;
					this.OnPropertyChanged();
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BandStopFilterDesign.
		/// </summary>
		public PoleBandStopFilterDesign(PoleFilterDesign prototype)
			: base(prototype)
		{
		}

		#endregion

		#region Methods

	
		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			List<PoleZeroPair> list = new List<PoleZeroPair>();

			double ww = 2 * ConstantsD.Pi * this.FrequencyWidth;
			double wc2 = 2 * ConstantsD.Pi * this.FrequencyCenter - (ww / 2.0d);
			double wc = wc2 + ww;

			const double epsilon = 1e-8d;

			if (wc2 < epsilon)
				wc2 = epsilon;

			if (wc > ConstantsD.Pi - epsilon)
				wc = ConstantsD.Pi - epsilon;

			double a = Utility.Cos((wc + wc2) * 0.5d) / Utility.Cos((wc - wc2) * 0.5d);
			double b = Utility.Tan((wc - wc2) * 0.5d);
			double a2 = a * a;
			double b2 = b * b;

			(ComplexD, ComplexD) bandStopTransform(ComplexD c)
			{
				if (c == ComplexD.PositiveInfinity)
					c = -1.0d;
				else
					c = (1.0d + c) / (1.0d - c);

				ComplexD u = 0.0d;
				u = u.AddMult(4 * (b2 + a2 - 1), c);
				u += 8 * (b2 - a2 + 1);
				u *= c;
				u += 4 * (a2 + b2 - 1);
				u = ComplexD.Sqrt(u);

				ComplexD v = u * -.5;
				v += a;
				v = v.AddMult(-a, c);

				u *= .5;
				u += a;
				u = u.AddMult(-a, c);

				ComplexD d = b + 1;
				d = d.AddMult(b - 1, c);

				return (u / d, v / d);
			}

			int numPoles = this.Prototype.PoleCount;
			int pairs = numPoles / 2;
			for (int i = 0; i < pairs; ++i)
			{
				(ComplexD a, ComplexD b) p = bandStopTransform(this.Prototype.Pairs[i].Pole1);
				(ComplexD a, ComplexD b) z = bandStopTransform(this.Prototype.Pairs[i].Zero1);

				list.AddConjugatePair(p.a, z.a);
				list.AddConjugatePair(p.b, z.b);
			}

			if ((numPoles & 1) > 0)
			{
				(ComplexD a, ComplexD b) p = bandStopTransform(this.Prototype.Pairs[pairs].Pole1);
				(ComplexD a, ComplexD b) z = bandStopTransform(this.Prototype.Pairs[pairs].Zero1);

				list.Add(new PoleZeroPair(p.a, z.a, p.b, z.b));
			}

			normal.Gain = this.Prototype.Normal.Gain;
			normal.W = this.frequencyCenter < 0.25d ? ConstantsD.Pi : 0.0d;

			return list;
		}
	


		#endregion
	}
}
