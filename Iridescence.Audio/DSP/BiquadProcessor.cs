﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents an abstract biquad filter processor.
	/// </summary>
	public abstract class BiquadProcessor
	{
		#region Constructors

		/// <summary>
		/// Creates a new BiquadProcessor.
		/// </summary>
		protected BiquadProcessor()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Computes the next output sample based on the specified biquad and input sample.
		/// </summary>
		/// <param name="biquad">The biquad coefficients.</param>
		/// <param name="value">The input sample.</param>
		/// <returns>The output sample.</returns>
		public abstract double Process(in Biquad biquad, double value);

		#endregion
	}
}
