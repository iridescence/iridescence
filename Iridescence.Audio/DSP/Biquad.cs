﻿using System;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents biquad coefficients.
	/// </summary>
	public struct Biquad : IEquatable<Biquad>
	{
		#region Fields

		public double A0;
		public double A1;
		public double A2;

		public double B0;
		public double B1;
		public double B2;

		#endregion

		#region Constructors

		public Biquad(double a0, double a1, double a2, double b0, double b1, double b2)
		{
			if (double.IsNaN(a0) || double.IsNaN(a1) || double.IsNaN(a2) ||
			    double.IsNaN(b0) || double.IsNaN(b1) || double.IsNaN(b2))
				throw new ArgumentException("Biquad coefficient is NaN.");

			this.A0 = a0;
			this.A1 = a1;
			this.A2 = a2;
			this.B0 = b0;
			this.B1 = b1;
			this.B2 = b2;
		}

		#endregion

		#region Methods

		public void ApplyScale(double scale)
		{
			this.B0 *= scale;
			this.B1 *= scale;
			this.B2 *= scale;
		}

		public bool Equals(Biquad other)
		{
			return this.A0.Equals(other.A0) && this.A1.Equals(other.A1) && this.A2.Equals(other.A2) && this.B0.Equals(other.B0) && this.B1.Equals(other.B1) && this.B2.Equals(other.B2);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is Biquad && Equals((Biquad)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.A0.GetHashCode();
				hashCode = (hashCode * 397) ^ this.A1.GetHashCode();
				hashCode = (hashCode * 397) ^ this.A2.GetHashCode();
				hashCode = (hashCode * 397) ^ this.B0.GetHashCode();
				hashCode = (hashCode * 397) ^ this.B1.GetHashCode();
				hashCode = (hashCode * 397) ^ this.B2.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(Biquad left, Biquad right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Biquad left, Biquad right)
		{
			return !left.Equals(right);
		}

		#endregion
	}
}
