﻿using System;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a filter design with a finite impulse response.
	/// </summary>
	public abstract class FIRFilterDesign : FilterDesign
	{
		#region Fields

		private bool tapsInvalid;
		private FIRTap[] taps;

		#endregion

		#region Properties

		public ReadOnlySpan<FIRTap> Taps
		{
			get
			{
				this.update();
				return this.taps;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FIRFilterDesign.
		/// </summary>
		protected FIRFilterDesign()
		{
			this.tapsInvalid = true;
		}

		#endregion

		#region Methods

		private void update()
		{
			if (!this.tapsInvalid)
				return;

			this.taps = this.GetTaps();
			this.tapsInvalid = false;
		}

		protected override void OnDesignChanged()
		{
			base.OnDesignChanged();
			this.tapsInvalid = true;
		}

		protected abstract FIRTap[] GetTaps();
		
		public override Filter CreateFilter()
		{
			return new FIRFilter(this);
		}

		#endregion
	}
}
