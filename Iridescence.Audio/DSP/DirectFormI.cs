﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Direct form I biquad processor.
	/// https://ccrma.stanford.edu/~jos/filters/Direct_Form_I.html
	/// </summary>
	public class DirectFormI : BiquadProcessor
	{
		#region Fields

		private double x1;
		private double y1;

		private double x2;
		private double y2;

		private DenormalPrevention dp;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new DirectFormI.
		/// </summary>
		public DirectFormI()
		{
			this.dp = DenormalPrevention.Default;
		}

		#endregion

		#region Methods

		public override double Process(in Biquad biquad, double sample)
		{
			double vOut = biquad.B0 * sample
			              + biquad.B1 * this.x1
			              + biquad.B2 * this.x2
			              - biquad.A1 * this.y1
			              - biquad.A2 * this.y2
			              + this.dp.Next();
			this.x2 = this.x1;
			this.y2 = this.y1;
			this.x1 = sample;
			this.y1 = vOut;
			return vOut;
		}

		#endregion
	}
}
