﻿using System.Collections.Generic;
using System.Linq;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents an abstract filter design that can be represented using one or more biquads.
	/// </summary>
	public abstract class BiquadFilterDesign : FilterDesign
	{
		#region Fields

		private bool biquadsInvalid;
		private Biquad[] biquads;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the biquads of this filter design.
		/// </summary>
		public IReadOnlyList<Biquad> Biquads
		{
			get
			{
				this.update();
				return this.biquads;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BiquadFilterDesign.
		/// </summary>
		protected BiquadFilterDesign()
		{
			this.biquadsInvalid = true;
		}

		#endregion

		#region Methods

		private void update()
		{
			if (!this.biquadsInvalid)
				return;

			this.biquads = this.GetBiquads().ToArray();
			this.biquadsInvalid = false;
		}

		protected override void OnDesignChanged()
		{
			base.OnDesignChanged();
			this.biquadsInvalid = true;
		}

		protected abstract IEnumerable<Biquad> GetBiquads();

		public override Filter CreateFilter()
		{
			return new BiquadFilter(this, () => new TransposedDirectFormII());
		}

		#endregion
	}
}
