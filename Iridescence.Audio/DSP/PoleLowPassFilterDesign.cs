﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a low-pass pole filter design.
	/// </summary>
	public class PoleLowPassFilterDesign : PoleTransformFilterDesign, IFrequencyFilter
	{
		#region Fields

		private double frequency;

		#endregion

		#region Properties

		public double Frequency
		{
			get => this.frequency;
			set
			{
				value = Utility.Clamp(value, 0.000001d, 0.5d);
				if (value != this.frequency)
				{
					this.frequency = value;
					this.OnPropertyChanged();
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LowPassFilterDesign.
		/// </summary>
		public PoleLowPassFilterDesign(PoleFilterDesign prototype)
			: base(prototype)
		{
			this.frequency = 0.1d;
		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			List<PoleZeroPair> list = new List<PoleZeroPair>();
			double f = Utility.Tan(ConstantsD.Pi * this.frequency);

			int poleCount = this.Prototype.PoleCount;
			int pairs = poleCount / 2;

			ComplexD lowPassTransform(ComplexD value)
			{
				if (value == ComplexD.PositiveInfinity)
					return new ComplexD(-1.0d, 0.0d);

				value = f * value;
				return (1.0d + value) / (1.0d - value);
			}

			for (int i = 0; i < pairs; i++)
			{
				PoleZeroPair pair = this.Prototype.Pairs[i];
				list.AddConjugatePair(lowPassTransform(pair.Pole1), lowPassTransform(pair.Zero1));
			}

			if ((poleCount & 1) > 0)
			{
				PoleZeroPair pair = this.Prototype.Pairs[pairs];
				list.Add(lowPassTransform(pair.Pole1), lowPassTransform(pair.Zero1));
			}

			normal = this.Prototype.Normal;
			return list;
		}

		#endregion
	}
}
