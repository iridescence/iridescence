﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a pole filter design.
	/// </summary>
	public abstract class PoleFilterDesign : BiquadFilterDesign
	{
		#region Fields

		private bool polesInvalid;

		private PoleZeroPair[] pairs;
		private int poleCount;
		private PoleFilterNormal normal;

		#endregion

		#region Properties

		public IReadOnlyList<PoleZeroPair> Pairs
		{
			get
			{
				this.update();
				return this.pairs;
			}
		}

		public int PoleCount
		{
			get
			{
				this.update();
				return this.poleCount;
			}
		}

		public PoleFilterNormal Normal
		{
			get
			{
				this.update();
				return this.normal;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new PoleFilterDesign.
		/// </summary>
		protected PoleFilterDesign()
		{
			this.polesInvalid = true;
		}

		#endregion

		#region Methods

		protected override void OnDesignChanged()
		{
			base.OnDesignChanged();
			this.polesInvalid = true;
		}

		private void update()
		{
			if (!this.polesInvalid)
				return;

			PoleZeroPair[] newPairs = this.GetPoleZeroPairs(out PoleFilterNormal newNormal)?.ToArray();
			if (newPairs == null)
				throw new InvalidOperationException($"{nameof(this.GetPoleZeroPairs)} must not return null.");

			int newCount = 0;
			for (int i = 0; i < newPairs.Length; ++i)
			{
				if (newPairs[i].IsSingle)
				{
					if (i != newPairs.Length - 1)
						throw new InvalidOperationException("A single pole must always appear last in the pole/zero pair collection.");

					++newCount;
				}
				else
				{
					newCount += 2;
				}
			}

			this.pairs = newPairs;
			this.poleCount = newCount;
			this.normal = newNormal;
			this.polesInvalid = false;
		}

		protected abstract IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal);

		protected override IEnumerable<Biquad> GetBiquads()
		{
			this.update();

			Biquad[] biquads = new Biquad[this.pairs.Length];

			if (this.pairs.Length > 0)
			{
				for (int i = 0; i < this.pairs.Length; i++)
				{
					biquads[i] = this.pairs[i].GetBiquad();
				}

				double scale = this.normal.Gain / BiquadFilter.GetResponse(this.normal.W / ConstantsD.Pi2, biquads).Magnitude;
				biquads[0].ApplyScale(scale);
			}

			return biquads;
		}

		#endregion
	}
}
