﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Direct form II biquad processor.
	/// https://ccrma.stanford.edu/~jos/filters/Direct_Form_II.html
	/// </summary>
	public class DirectFormII : BiquadProcessor
	{
		#region Fields

		private double x1;
		private double x2;

		private DenormalPrevention dp;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new DirectFormII.
		/// </summary>
		public DirectFormII()
		{
			this.dp = DenormalPrevention.Default;
		}

		#endregion

		#region Methods

		public override double Process(in Biquad biquad, double sample)
		{
			double w = sample
			           - biquad.A1 * this.x1
			           - biquad.A2 * this.x2
			           + this.dp.Next();
			double vOut = biquad.B0 * w
			              + biquad.B1 * this.x1
			              + biquad.B2 * this.x2;
			this.x2 = this.x1;
			this.x1 = w;
			return vOut;
		}

		#endregion
	}
}
