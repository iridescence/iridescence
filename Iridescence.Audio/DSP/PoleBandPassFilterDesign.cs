﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a band-pass pole filter design.
	/// </summary>
	public class PoleBandPassFilterDesign : PoleTransformFilterDesign, IBandFilter
	{
		#region Fields

		private double frequencyCenter;
		private double frequencyWidth;

		#endregion

		#region Properties

		public double FrequencyCenter
		{
			get => this.frequencyCenter;
			set
			{
				value = Utility.Clamp(value, 0.000001d, 0.5d);
				if (value != this.frequencyCenter)
				{
					this.frequencyCenter = value;
					this.OnPropertyChanged();
				}
			}
		}

		public double FrequencyWidth
		{
			get => this.frequencyWidth;
			set
			{
				value = Utility.Clamp(value, 0.000001d, 0.5d);
				if (value != this.frequencyWidth)
				{
					this.frequencyWidth = value;
					this.OnPropertyChanged();
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BandPassFilterDesign.
		/// </summary>
		public PoleBandPassFilterDesign(PoleFilterDesign prototype)
			: base(prototype)
		{
		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			List<PoleZeroPair> list = new List<PoleZeroPair>();

			double ww = 2 * ConstantsD.Pi * this.FrequencyWidth;

			double wc2 = 2 * ConstantsD.Pi * this.FrequencyCenter - (ww / 2.0d);
			double wc = wc2 + ww;

			const double epsilon = 1e-8d;

			if (wc2 < epsilon)
				wc2 = epsilon;

			if (wc > ConstantsD.Pi - epsilon)
				wc = ConstantsD.Pi - epsilon;

			double a = Utility.Cos((wc + wc2) * 0.5d) / Utility.Cos((wc - wc2) * 0.5d);
			double b = 1 / Utility.Tan((wc - wc2) * 0.5d);
			double a2 = a * a;
			double b2 = b * b;
			double ab = a * b;
			double ab2 = 2.0d * ab;

			(ComplexD, ComplexD) bandPassTransform(ComplexD c)
			{
				if (c == ComplexD.PositiveInfinity)
					return (-1, 1);

				c = (1.0d + c) / (1.0d - c);

				ComplexD v = 0;

				v = v.AddMult(4 * (b2 * (a2 - 1) + 1), c);
				v += 8 * (b2 * (a2 - 1) - 1);
				v *= c;
				v += 4 * (b2 * (a2 - 1) + 1);
				v = ComplexD.Sqrt(v);

				ComplexD u = -v;
				u = u.AddMult(ab2, c);
				u += ab2;

				v = v.AddMult(ab2, c);
				v += ab2;

				ComplexD d = 0;
				d = d.AddMult(2 * (b - 1), c) + 2 * (1 + b);

				return (u / d, v / d);
			}

			int numPoles = this.Prototype.PoleCount;
			int pairs = numPoles / 2;
			for (int i = 0; i < pairs; ++i)
			{
				(ComplexD a, ComplexD b) p = bandPassTransform(this.Prototype.Pairs[i].Pole1);
				(ComplexD a, ComplexD b) z = bandPassTransform(this.Prototype.Pairs[i].Zero1);

				list.AddConjugatePair(p.a, z.a);
				list.AddConjugatePair(p.b, z.b);
			}

			if ((numPoles & 1) > 0)
			{
				(ComplexD a, ComplexD b) p = bandPassTransform(this.Prototype.Pairs[pairs].Pole1);
				(ComplexD a, ComplexD b) z = bandPassTransform(this.Prototype.Pairs[pairs].Zero1);

				list.Add(new PoleZeroPair(p.a, z.a, p.b, z.b));
			}

			normal = this.Prototype.Normal;
			normal.W = 2 * Utility.Atan(Utility.Sqrt(Utility.Tan((wc + normal.W) * 0.5d) * Utility.Tan((wc2 + normal.W) * 0.5d)));
			
			return list;
		}

		#endregion
	}
}
