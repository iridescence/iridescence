﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Audio.DSP.ChebyshevII
{
	public interface IChebyshevIIFilter
	{
		/// <summary>
		/// Gets or sets the stop band ripple (in decibels).
		/// </summary>
		double StopBandRipple { get; set; }
	}
	
	/// <summary>
	/// An abstract Chebyshev II filter design.
	/// </summary>
	public abstract class ChebyshevIIFilterDesign : PoleFilterDesign
	{
		#region Fields

		private double stopBandRipple;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets the order of the Chebyshev II filter.
		/// </summary>
		public int Order { get; }

		public double StopBandRipple
		{
			get => this.stopBandRipple;
			set
			{
				value = Utility.Clamp(value, 3.0d, 60.0d);
				if (value != this.stopBandRipple)
				{
					this.stopBandRipple = value;
					this.OnPropertyChanged();
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ChebyshevIIFilterDesign.
		/// </summary>
		protected ChebyshevIIFilterDesign(int order)
		{
			this.Order = order;
			this.StopBandRipple = 48.0d;
		}

		#endregion
	}

	/// <summary>
	/// Represents a ChebyshevII low-pass or high-pass filter prototype.
	/// </summary>
	public class PassFilterBase : ChebyshevIIFilterDesign
	{
		#region Constructors

		/// <summary>
		/// Creates a new PassFilterBase.
		/// </summary>
		public PassFilterBase(int order)
			: base(order)
		{
		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			List<PoleZeroPair> list = new List<PoleZeroPair>();

			const double doubleLn10 = 2.3025850929940456840179914546844d;
			double eps = Utility.Sqrt(1.0d / (Utility.Exp(this.StopBandRipple * 0.1d * doubleLn10) - 1.0d));
			double v0 = Utility.Asinh(1.0d / eps) / this.Order;
			double sinh_v0 = -Utility.Sinh(v0);
			double cosh_v0 = Utility.Cosh(v0);
			double fn = ConstantsD.Pi / (2.0d * this.Order);

			int k = 1;
			for (int i = this.Order / 2; --i >= 0; k += 2)
			{
				double a = sinh_v0 * Utility.Cos((k - this.Order) * fn);
				double b = cosh_v0 * Utility.Sin((k - this.Order) * fn);
				double d2 = a * a + b * b;
				double im = 1.0d / Utility.Cos(k * fn);
				list.AddConjugatePair(new ComplexD(a / d2, b / d2), new ComplexD(0.0d, im));
			}

			if ((this.Order & 1) > 0)
			{
				list.Add(1.0d / sinh_v0, ComplexD.PositiveInfinity);
			}

			normal.W = 0.0;
			normal.Gain = 1.0;
			return list;
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth low-pass filter design.
	/// </summary>
	public class LowPass : PoleLowPassFilterDesign, IChebyshevIIFilter
	{
		#region Properties

		public double StopBandRipple
		{
			get => ((PassFilterBase)this.Prototype).StopBandRipple;
			set => ((PassFilterBase)this.Prototype).StopBandRipple = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LowPass.
		/// </summary>
		public LowPass(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth high-pass filter design.
	/// </summary>
	public class HighPass : PoleHighPassFilterDesign, IChebyshevIIFilter
	{
		#region Properties

		public double StopBandRipple
		{
			get => ((PassFilterBase)this.Prototype).StopBandRipple;
			set => ((PassFilterBase)this.Prototype).StopBandRipple = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HighPass.
		/// </summary>
		public HighPass(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth band-pass filter design.
	/// </summary>
	public class BandPass : PoleBandPassFilterDesign, IChebyshevIIFilter
	{
		#region Properties

		public double StopBandRipple
		{
			get => ((PassFilterBase)this.Prototype).StopBandRipple;
			set => ((PassFilterBase)this.Prototype).StopBandRipple = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BandPass.
		/// </summary>
		public BandPass(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth band-stop filter design.
	/// </summary>
	public class BandStop : PoleBandStopFilterDesign, IChebyshevIIFilter
	{
		#region Properties

		public double StopBandRipple
		{
			get => ((PassFilterBase)this.Prototype).StopBandRipple;
			set => ((PassFilterBase)this.Prototype).StopBandRipple = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BandStop.
		/// </summary>
		public BandStop(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	public class ShelfFilterBase : ChebyshevIIFilterDesign, IShelfFilter, IChebyshevIIFilter
	{
		#region Fields

		private double gain;

		#endregion

		#region Properties

		public double Gain
		{
			get => this.gain;
			set
			{
				if (value != this.gain)
				{
					this.gain = value;
					this.OnPropertyChanged();
				}
			}
		}

		#endregion

		#region Constructors

		public ShelfFilterBase(int order) : base(order)
		{
		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			List<PoleZeroPair> list = new List<PoleZeroPair>();

			double gainDb = -this.gain;
			double stopBandDb = this.StopBandRipple;

			if (stopBandDb >= Utility.Absolute(gainDb))
				stopBandDb = Utility.Absolute(gainDb);

			if (gainDb < 0.0)
				stopBandDb = -stopBandDb;

			double G = Utility.Pow(10.0, gainDb / 20.0);
			double Gb = Utility.Pow(10.0, (gainDb - stopBandDb) / 20.0);
			double G0 = 1.0;
			double g0 = Utility.Pow(G0, 1.0 / this.Order);

			double eps;
			if (Gb != G0)
				eps = Utility.Sqrt((G * G - Gb * Gb) / (Gb * Gb - G0 * G0));
			else
				eps = G - 1.0; // This is surely wrong

			double b = Utility.Pow(G / eps + Gb * Utility.Sqrt(1.0 + 1.0 / (eps * eps)), 1.0 / this.Order);
			double u = Utility.Ln(b / g0);
			double v = Utility.Ln(Utility.Pow(1.0 / eps + Utility.Sqrt(1 + 1 / (eps * eps)), 1.0 / this.Order));

			double sinh_u = Utility.Sinh(u);
			double sinh_v = Utility.Sinh(v);
			double cosh_u = Utility.Cosh(u);
			double cosh_v = Utility.Cosh(v);
			double n2 = 2 * this.Order;
			int pairs = this.Order / 2;
			for (int i = 1; i <= pairs; ++i)
			{
				double a = ConstantsD.Pi * (2 * i - 1) / n2;
				double sn = Utility.Sin(a);
				double cs = Utility.Cos(a);
				list.AddConjugatePair(
					new ComplexD(-sn * sinh_u, cs * cosh_u),
					new ComplexD(-sn * sinh_v, cs * cosh_v));
			}

			if ((this.Order & 1) != 0)
				list.Add(-sinh_u, -sinh_v);

			normal.W = ConstantsD.Pi;
			normal.Gain = 1.0;
			return list;
		}

		#endregion
	}

	
	/// <summary>
	/// Represents a Butterworth low-shelf filter design.
	/// </summary>
	public class LowShelf : PoleLowPassFilterDesign, IShelfFilter, IChebyshevIIFilter
	{
		#region Properties

		public double StopBandRipple
		{
			get => ((ShelfFilterBase)this.Prototype).StopBandRipple;
			set => ((ShelfFilterBase)this.Prototype).StopBandRipple = value;
		}

		public double Gain
		{
			get => ((ShelfFilterBase)this.Prototype).Gain;
			set => ((ShelfFilterBase)this.Prototype).Gain = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LowShelf.
		/// </summary>
		public LowShelf(int order)
			: base(new ShelfFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth high-shelf filter design.
	/// </summary>
	public class HighShelf : PoleHighPassFilterDesign, IShelfFilter, IChebyshevIIFilter
	{
		#region Properties

		public double StopBandRipple
		{
			get => ((ShelfFilterBase)this.Prototype).StopBandRipple;
			set => ((ShelfFilterBase)this.Prototype).StopBandRipple = value;
		}

		public double Gain
		{
			get => ((ShelfFilterBase)this.Prototype).Gain;
			set => ((ShelfFilterBase)this.Prototype).Gain = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HighShelf.
		/// </summary>
		public HighShelf(int order)
			: base(new ShelfFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth band-shelf filter design.
	/// </summary>
	public class BandShelf : PoleBandPassFilterDesign, IShelfFilter, IChebyshevIIFilter
	{
		#region Properties

		public double StopBandRipple
		{
			get => ((ShelfFilterBase)this.Prototype).StopBandRipple;
			set => ((ShelfFilterBase)this.Prototype).StopBandRipple = value;
		}

		public double Gain
		{
			get => ((ShelfFilterBase)this.Prototype).Gain;
			set => ((ShelfFilterBase)this.Prototype).Gain = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BandShelf.
		/// </summary>
		public BandShelf(int order)
			: base(new ShelfFilterBase(order))
		{
		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			IEnumerable<PoleZeroPair> r = base.GetPoleZeroPairs(out normal);
			normal.W = this.FrequencyCenter < 0.25d ? ConstantsD.Pi : 0.0d;
			return r;
		}

		#endregion
	}
}
