﻿using System;
using Iridescence.Math;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a filter with finite impulse response.
	/// </summary>
	public class FIRFilter : Filter
	{
		#region Fields

		private readonly FIRFilterDesign design;

		private FIRTap[] taps;

		private double[] history;
		private int bufferOffset;

		#endregion

		#region Properties

		public int Size => this.history.Length;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="FIRFilter"/>.
		/// </summary>
		public FIRFilter(FIRFilterDesign design)
			: base(design)
		{
			this.design = design;
		}

		#endregion

		#region Methods

		protected override void Update()
		{
			this.taps = this.design.Taps.ToArray();

			int bufferSize = 1;
			for (int i = 0; i < this.taps.Length; i++)
			{
				bufferSize = Utility.Max(bufferSize, (int)Utility.Ceiling(this.taps[i].Delay));
			}

			++bufferSize;

			if (this.history == null)
			{
				this.history = new double[bufferSize];
			}
			else if (this.history.Length != bufferSize)
			{
				Array.Resize(ref this.history, bufferSize);
			}
		}

		protected override void Run(ReadOnlySpan<double> input, Span<double> output)
		{
			for (int i = 0; i < input.Length; ++i)
			{
				double vIn = input[i];
				this.history[this.bufferOffset] = vIn;

				double vOut = 0.0;
				for (int j = 0; j < this.taps.Length; ++j)
				{
					double pos = this.bufferOffset - this.taps[j].Delay;
					int i0 = (int)pos;
					int i1 = i0 + 1;
					double t = pos - i0;
					double t2 = 1.0d - t;

					if (i0 < 0)
						i0 += this.history.Length;
					else if (i0 >= this.history.Length)
						i0 -= this.history.Length;

					if (i1 < 0)
						i1 += this.history.Length;
					else if (i1 >= this.history.Length)
						i1 -= this.history.Length;

					double hIn = this.history[i0] * t2 + this.history[i1] * t;

					vOut = vOut + hIn;
				}

				output[i] = vOut;

				++this.bufferOffset;
				if (this.bufferOffset >= this.history.Length)
					this.bufferOffset = 0;
			}
		}

		public override ComplexD GetResponse(double normalizedFrequency)
		{
			return ComplexD.Zero;
		}

		#endregion
	}
}
