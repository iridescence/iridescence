﻿using System;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents an FIR filter tap (coefficient + delay pair).
	/// </summary>
	public struct FIRTap : IEquatable<FIRTap>
	{
		#region Fields

		/// <summary>
		/// The delay (in samples) of the tap.
		/// </summary>
		public double Delay;

		/// <summary>
		/// The input gain factor.
		/// </summary>
		public double Factor;
	
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="FIRTap"/> structure. 
		/// </summary>
		public FIRTap(double delay, double factor)
		{
			this.Factor = factor;
			this.Delay = delay;
		}
		
		#endregion
		
		#region Methods
		
		public bool Equals(FIRTap other)
		{
			return this.Delay.Equals(other.Delay) && this.Factor.Equals(other.Factor);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is IIRTap tap && this.Equals(tap);
		}

		public override int GetHashCode()
		{
			return this.Delay.GetHashCode() ^ this.Factor.GetHashCode();
		}

		public static bool operator ==(FIRTap left, FIRTap right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(FIRTap left, FIRTap right)
		{
			return !left.Equals(right);
		}
		
		#endregion
	}
}
