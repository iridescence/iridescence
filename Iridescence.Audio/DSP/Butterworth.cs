﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Audio.DSP.Butterworth
{
	/// <summary>
	/// An abstract Butterworth filter design.
	/// </summary>
	public abstract class ButterworthFilterDesign : PoleFilterDesign
	{
		#region Properties

		/// <summary>
		/// Gets the order of the butterworth filter.
		/// </summary>
		public int Order { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ButterworthFilterDesign.
		/// </summary>
		protected ButterworthFilterDesign(int order)
		{
			this.Order = order;
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth low-pass, high-pass, band-pass or band-stop filter prototype.
	/// </summary>
	public class PassFilterBase : ButterworthFilterDesign
	{
		#region Constructors

		/// <summary>
		/// Creates a new PassFilterBase.
		/// </summary>
		public PassFilterBase(int order)
			: base(order)
		{
		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			List<PoleZeroPair> list = new List<PoleZeroPair>();
			
			double n2 = 2.0d * this.Order;

			int pairs = this.Order / 2;
			for (int i = 0; i < pairs; i++)
			{
				list.AddConjugatePair(ComplexD.Polar(1.0d, ConstantsD.PiOver2 + (2.0d * i + 1.0d) * ConstantsD.Pi / n2), ComplexD.PositiveInfinity);
			}

			if ((this.Order & 1) > 0)
			{
				list.Add(-1d, ComplexD.PositiveInfinity);
			}

			normal = PoleFilterNormal.Identity;
			return list;
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth low-pass filter design.
	/// </summary>
	public class LowPass : PoleLowPassFilterDesign
	{
		#region Constructors

		/// <summary>
		/// Creates a new LowPass.
		/// </summary>
		public LowPass(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth high-pass filter design.
	/// </summary>
	public class HighPass : PoleHighPassFilterDesign
	{
		#region Constructors

		/// <summary>
		/// Creates a new HighPass.
		/// </summary>
		public HighPass(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth band-pass filter design.
	/// </summary>
	public class BandPass : PoleBandPassFilterDesign
	{
		#region Constructors

		/// <summary>
		/// Creates a new BandPass.
		/// </summary>
		public BandPass(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth band-stop filter design.
	/// </summary>
	public class BandStop : PoleBandStopFilterDesign
	{
		#region Constructors

		/// <summary>
		/// Creates a new BandStop.
		/// </summary>
		public BandStop(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth low-shelf, high-shelf or band-shelf filter prototype.
	/// </summary>
	public class ShelfFilterBase : ButterworthFilterDesign, IShelfFilter
	{
		#region Fields

		private double gain;

		#endregion

		#region Properties

		public double Gain
		{
			get => this.gain;
			set
			{
				if (value != this.gain)
				{
					this.gain = value;
					this.OnPropertyChanged();
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ShelfFilterBase.
		/// </summary>
		public ShelfFilterBase(int order)
			: base(order)
		{

		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			List<PoleZeroPair> list = new List<PoleZeroPair>();

			double n2 = 2.0d * this.Order;
			double g = Utility.Pow(Utility.Pow(10.0d, this.Gain / 20.0d), 1.0d / n2);
			double gp = -1.0d / g;
			double gz = -g;

			int pairs = this.Order / 2;
			for (int i = 1; i <= pairs; i++)
			{
				double theta = ConstantsD.Pi * (0.5d - (2.0d * i - 1.0d) / n2);
				list.AddConjugatePair(ComplexD.Polar(gp, theta), ComplexD.Polar(gz, theta));
			}

			if ((this.Order & 1) > 0)
				list.Add(gp, gz);

			normal.Gain = 1.0;
			normal.W = ConstantsD.Pi;
			return list;
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth low-shelf filter design.
	/// </summary>
	public class LowShelf : PoleLowPassFilterDesign, IShelfFilter
	{
		#region Properties

		public double Gain
		{
			get => ((ShelfFilterBase)this.Prototype).Gain;
			set => ((ShelfFilterBase)this.Prototype).Gain = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LowShelf.
		/// </summary>
		public LowShelf(int order)
			: base(new ShelfFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth high-shelf filter design.
	/// </summary>
	public class HighShelf : PoleHighPassFilterDesign, IShelfFilter
	{
		#region Properties

		public double Gain
		{
			get => ((ShelfFilterBase)this.Prototype).Gain;
			set => ((ShelfFilterBase)this.Prototype).Gain = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HighShelf.
		/// </summary>
		public HighShelf(int order)
			: base(new ShelfFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Butterworth band-shelf filter design.
	/// </summary>
	public class BandShelf : PoleBandPassFilterDesign, IShelfFilter
	{
		#region Properties

		public double Gain
		{
			get => ((ShelfFilterBase)this.Prototype).Gain;
			set => ((ShelfFilterBase)this.Prototype).Gain = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BandShelf.
		/// </summary>
		public BandShelf(int order)
			: base(new ShelfFilterBase(order))
		{
		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			IEnumerable<PoleZeroPair> r = base.GetPoleZeroPairs(out normal);
			normal.W = this.FrequencyCenter < 0.25d ? ConstantsD.Pi : 0.0d;
			return r;
		}

		#endregion
	}
}
