﻿using System;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents an IIR filter tap (coefficient + delay pair).
	/// </summary>
	public struct IIRTap : IEquatable<IIRTap>
	{
		#region Fields

		/// <summary>
		/// The delay (in samples) of the tap.
		/// </summary>
		public double Delay;

		/// <summary>
		/// The input gain factor.
		/// </summary>
		public double InputFactor;

		/// <summary>
		/// The output gain factor.
		/// </summary>
		public double OutputFactor;
	
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="IIRTap"/> structure. 
		/// </summary>
		public IIRTap(double delay, double input, double output)
		{
			this.InputFactor = input;
			this.OutputFactor = output;
			this.Delay = delay;
		}
		
		#endregion
		
		#region Methods
		
		public bool Equals(IIRTap other)
		{
			return this.Delay.Equals(other.Delay) &&
			       this.InputFactor.Equals(other.InputFactor) &&
			       this.OutputFactor.Equals(other.OutputFactor);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is IIRTap tap && this.Equals(tap);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.Delay.GetHashCode();
				hashCode = (hashCode * 397) ^ this.InputFactor.GetHashCode();
				hashCode = (hashCode * 397) ^ this.OutputFactor.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(IIRTap left, IIRTap right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(IIRTap left, IIRTap right)
		{
			return !left.Equals(right);
		}
		
		#endregion
	}
}
