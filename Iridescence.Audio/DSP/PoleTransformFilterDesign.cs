﻿using System;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a pole/zero transformation filter.
	/// </summary>
	public abstract class PoleTransformFilterDesign : PoleFilterDesign
	{
		#region Fields

		#endregion

		#region Properties

		public PoleFilterDesign Prototype { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TransformFilterDesign.
		/// </summary>
		protected PoleTransformFilterDesign(PoleFilterDesign prototype)
		{
			this.Prototype = prototype;
			this.Prototype.DesignChanged += WeakDelegate.Create(this.onPrototypeChanged, (d, h) => this.Prototype.DesignChanged -= h);
		}

		#endregion

		#region Methods

		private void onPrototypeChanged(object sender, EventArgs e)
		{
			this.OnDesignChanged();
		}

		#endregion
	}
}
