﻿using System;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a filter design for an infinite impulse response filter.
	/// </summary>
	public abstract class IIRFilterDesign : FilterDesign
	{
		#region Fields

		private bool tapsInvalid;
		private IIRTap[] taps;

		#endregion

		#region Properties

		public ReadOnlySpan<IIRTap> Taps
		{
			get
			{
				this.update();
				return this.taps;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new IIRFilterDesign.
		/// </summary>
		protected IIRFilterDesign()
		{
			this.tapsInvalid = true;
		}

		#endregion

		#region Methods

		private void update()
		{
			if (!this.tapsInvalid)
				return;

			this.taps = this.GetTaps();
			this.tapsInvalid = false;
		}

		protected override void OnDesignChanged()
		{
			base.OnDesignChanged();
			this.tapsInvalid = true;
		}

		protected abstract IIRTap[] GetTaps();
		
		public override Filter CreateFilter()
		{
			return new IIRFilter(this);
		}

		#endregion
	}
}
