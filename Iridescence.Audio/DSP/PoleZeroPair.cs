﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents a pair of poles and zeros.
	/// </summary>
	public struct PoleZeroPair : IEquatable<PoleZeroPair>
	{
		#region Fields

		public ComplexD Pole1;
		public ComplexD Zero1;

		public ComplexD Pole2;
		public ComplexD Zero2;

		#endregion

		#region Properties

		public bool IsSingle => this.Pole2 == ComplexD.Zero && this.Zero2 == ComplexD.Zero;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new PoleZeroPair.
		/// </summary>
		public PoleZeroPair(ComplexD pole, ComplexD zero)
		{
			this.Pole1 = pole;
			this.Zero1 = zero;
			this.Pole2 = ComplexD.Zero;
			this.Zero2 = ComplexD.Zero;
		}

		/// <summary>
		/// Creates a new PoleZeroPair.
		/// </summary>
		public PoleZeroPair(ComplexD pole1, ComplexD zero1, ComplexD pole2, ComplexD zero2)
		{
			this.Pole1 = pole1;
			this.Zero1 = zero1;
			this.Pole2 = pole2;
			this.Zero2 = zero2;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns the biquad that represents this pole/zero pair.
		/// </summary>
		/// <returns></returns>
		public Biquad GetBiquad()
		{
			double a0, a1, a2, b0, b1, b2;
			if (this.IsSingle)
			{
				a0 = 1.0d;
				a1 = -this.Pole1.Real;
				a2 = 0.0d;

				b0 = -this.Zero1.Real;
				b1 = 1.0d;
				b2 = 0.0d;
			}
			else
			{
				a0 = 1.0d;
				if (this.Pole1.Imaginary != 0.0d)
				{
					if (this.Pole2 != this.Pole1.Conjugate)
						throw new Exception("Poles must be conjugates.");
					a1 = -2.0d * this.Pole1.Real;
					a2 = this.Pole1.MagnitudeSquared;
				}
				else
				{
					if (this.Pole2.Imaginary != 0.0d)
						throw new Exception("Second pole must be real.");
					a1 = -(this.Pole1.Real + this.Pole2.Real);
					a2 = this.Pole1.Real * this.Pole2.Real;
				}

				b0 = 1.0f;
				if (this.Zero1.Imaginary != 0.0d)
				{
					if (this.Zero2 != this.Zero1.Conjugate)
						throw new Exception("Zeros must be conjugates.");
					b1 = -2.0d * this.Zero1.Real;
					b2 = this.Zero1.MagnitudeSquared;
				}
				else
				{
					if (this.Zero2.Imaginary != 0.0d)
						throw new Exception("Second zero must be real.");
					b1 = -(this.Zero1.Real + this.Zero2.Real);
					b2 = this.Zero1.Real * this.Zero2.Real;
				}
			}

			double a0Inv = 1.0 / a0;
			return new Biquad(a0, a1 * a0Inv, a2 * a0Inv, b0 * a0Inv, b1 * a0Inv, b2 * a0Inv);
		}

		public bool Equals(PoleZeroPair other)
		{
			return this.Pole1.Equals(other.Pole1) && this.Zero1.Equals(other.Zero1) && this.Pole2.Equals(other.Pole2) && this.Zero2.Equals(other.Zero2);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is PoleZeroPair && Equals((PoleZeroPair)obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode = this.Pole1.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Zero1.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Pole2.GetHashCode();
				hashCode = (hashCode * 397) ^ this.Zero2.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(PoleZeroPair left, PoleZeroPair right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(PoleZeroPair left, PoleZeroPair right)
		{
			return !left.Equals(right);
		}

		#endregion
	}

	public static class PoleZeroPairCollectionExtensions
	{
		/// <summary>
		/// Adds a pole/zero pair.
		/// </summary>
		/// <param name="pole"></param>
		/// <param name="zero"></param>
		public static void Add(this ICollection<PoleZeroPair> pairs, ComplexD pole, ComplexD zero)
		{
			if (pairs.Any(p => p.IsSingle))
				throw new InvalidOperationException("The single pole must be added after the pole pairs.");

			pairs.Add(new PoleZeroPair(pole, zero));
		}

		/// <summary>
		/// Adds a pole/zero pair and its conjugate.
		/// </summary>
		/// <param name="pole"></param>
		/// <param name="zero"></param>
		public static void AddConjugatePair(this ICollection<PoleZeroPair> pairs, ComplexD pole, ComplexD zero)
		{
			if (pairs.Any(p => p.IsSingle))
				throw new InvalidOperationException("The single pole must be added after the pole pairs.");

			pairs.Add(new PoleZeroPair(pole, zero, pole.Conjugate, zero.Conjugate));
		}
	}
}
