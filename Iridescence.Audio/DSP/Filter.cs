﻿using System;
using Iridescence.Math;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents an abstract digital filter that is able to apply a filter on the specified input signal.
	/// </summary>
	public abstract class Filter
	{
		#region Fields

		private bool designChanged;

		#endregion

		#region Properties

		public FilterDesign Design { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Filter.
		/// </summary>
		protected Filter(FilterDesign design)
		{
			this.Design = design;
			this.Design.DesignChanged += WeakDelegate.Create(this.OnDesignChanged, (d, h) => this.Design.DesignChanged -= h);
			this.designChanged = true;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Runs the filter on the specified range of samples.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="output"></param>
		protected abstract void Run(ReadOnlySpan<double> input, Span<double> output);

		/// <summary>
		/// Runs the filter on the specified range of samples.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="output"></param>
		protected virtual void Run(ReadOnlySpan<float> input, Span<float> output)
		{
			int bufSize = Utility.Min(input.Length, 64);

			unsafe
			{
				double* inPtr = stackalloc double[bufSize];
				double* outPtr = stackalloc double[bufSize];
				Span<double> inBuf = new Span<double>(inPtr, bufSize);
				Span<double> outBuf = new Span<double>(outPtr, bufSize);

				while (input.Length > 0)
				{
					int n = Utility.Min(bufSize, input.Length);
					for (int i = 0; i < n; ++i)
					{
						inBuf[i] = input[i];
					}

					this.Process(inBuf, outBuf);

					for (int i = 0; i < n; ++i)
					{
						output[i] = (float)outBuf[i];
					}

					input = input.Slice(n);
					output = output.Slice(n);
				}
			}
		}

		/// <summary>
		/// Updates the filter from its design.
		/// </summary>
		protected abstract void Update();

		/// <summary>
		/// Filters the input sample and returns the result.
		/// </summary>
		/// <param name="input">The input samples.</param>
		/// <param name="output">The output samples.</param>
		/// <returns></returns>
		public void Process(ReadOnlySpan<double> input, Span<double> output)
		{
			if (input.Length != output.Length)
				throw new ArgumentException("The input and output buffer lengths must be equal.", nameof(input));

			if (input.IsEmpty)
				return;

			if (this.designChanged)
			{
				this.Update();
				this.designChanged = false;
			}

			this.Run(input, output);
		}

		/// <summary>
		/// Filters the input sample and returns the result.
		/// </summary>
		/// <param name="input">The input samples.</param>
		/// <param name="output">The output samples.</param>
		/// <returns></returns>
		public void Process(ReadOnlySpan<float> input, Span<float> output)
		{
			if (input.Length != output.Length)
				throw new ArgumentException("The input and output buffer lengths must be equal.", nameof(input));

			if (input.IsEmpty)
				return;

			if (this.designChanged)
			{
				this.Update();
				this.designChanged = false;
			}

			this.Run(input, output);
		}

		protected virtual void OnDesignChanged(object sender, EventArgs e)
		{
			this.designChanged = true;
		}

		/// <summary>
		/// Calculates the filter response for the specified normalized frequency.
		/// </summary>
		/// <param name="normalizedFrequency"></param>
		/// <returns></returns>
		public abstract ComplexD GetResponse(double normalizedFrequency);
		
		#endregion
	}
}
