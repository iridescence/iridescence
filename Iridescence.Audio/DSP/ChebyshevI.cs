﻿using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Audio.DSP.ChebyshevI
{
	public interface IChebyshevIFilter
	{
		/// <summary>
		/// Gets or sets the pass-band ripple (in decibels).
		/// </summary>
		double PassBandRipple { get; set; }
	}

	/// <summary>
	/// An abstract Chebyshev I filter design.
	/// </summary>
	public abstract class ChebyshevIFilterDesign : PoleFilterDesign, IChebyshevIFilter
	{
		#region Fields

		private double passBandRipple;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the order of the Chebyshev I filter.
		/// </summary>
		public int Order { get; }
		
		/// <summary>
		/// Gets or sets the pass-band ripple (in decibels).
		/// </summary>
		public double PassBandRipple
		{
			get => this.passBandRipple;
			set
			{
				value = Utility.Clamp(value, 0.0000001d, 60.0d);
				if (value != this.passBandRipple)
				{
					this.passBandRipple = value;
					this.OnPropertyChanged();
				}
			}
		}
		
		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ChebyshevIFilterDesign.
		/// </summary>
		protected ChebyshevIFilterDesign(int order)
		{
			this.Order = order;
			this.PassBandRipple = 0.01d;
		}

		#endregion
	}

	/// <summary>
	/// Represents a Chebyshev Type I low-pass, high-pass, band-pass or band-stop filter prototype.
	/// </summary>
	public class PassFilterBase : ChebyshevIFilterDesign
	{
		#region Constructors

		/// <summary>
		/// Creates a new PassFilterBase.
		/// </summary>
		public PassFilterBase(int order)
			: base(order)
		{
		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			List<PoleZeroPair> list = new List<PoleZeroPair>();

			const double ln10_over_10 = 0.23025850929940456840179914546844d;

			double epsilon = Utility.Sqrt(Utility.Exp(this.PassBandRipple * ln10_over_10) - 1.0d);
			double v0 = Utility.Asinh(1.0d / epsilon) / this.Order;
			double sinh_v0 = -Utility.Sinh(v0);
			double cosh_v0 = Utility.Cosh(v0);

			double n2 = 2.0d * this.Order;
			int pairs = this.Order / 2;

			for (int i = 0; i < pairs; i++)
			{
				int k = 2 * i + 1 - this.Order;
				double a = sinh_v0 * Utility.Cos(k * ConstantsD.Pi / n2);
				double b = cosh_v0 * Utility.Sin(k * ConstantsD.Pi / n2);

				list.AddConjugatePair(new ComplexD(a, b), ComplexD.PositiveInfinity);
			}

			normal.W = 0.0d;
			if ((this.Order & 1) > 0)
			{
				list.Add(new ComplexD(sinh_v0, 0d), ComplexD.PositiveInfinity);
				normal.Gain = 1.0d;
			}
			else
			{
				normal.Gain = Utility.Pow(10.0d, -this.PassBandRipple / 20.0d);
			}

			return list;
		}

		#endregion
	}

	/// <summary>
	/// Represents a Chebyshev Type I low-pass filter design.
	/// </summary>
	public class LowPass : PoleLowPassFilterDesign, IChebyshevIFilter
	{
		#region Properties

		public double PassBandRipple
		{
			get => ((PassFilterBase)this.Prototype).PassBandRipple;
			set => ((PassFilterBase)this.Prototype).PassBandRipple = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LowPass.
		/// </summary>
		public LowPass(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Chebyshev Type I high-pass filter design.
	/// </summary>
	public class HighPass : PoleHighPassFilterDesign, IChebyshevIFilter
	{
		#region Properties

		public double PassBandRipple
		{
			get => ((PassFilterBase)this.Prototype).PassBandRipple;
			set => ((PassFilterBase)this.Prototype).PassBandRipple = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HighPass.
		/// </summary>
		public HighPass(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Chebyshev Type I band-pass filter design.
	/// </summary>
	public class BandPass : PoleBandPassFilterDesign, IChebyshevIFilter
	{
		#region Properties

		public double PassBandRipple
		{
			get => ((PassFilterBase)this.Prototype).PassBandRipple;
			set => ((PassFilterBase)this.Prototype).PassBandRipple = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BandPass.
		/// </summary>
		public BandPass(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Chebyshev Type I band-stop filter design.
	/// </summary>
	public class BandStop : PoleBandStopFilterDesign, IChebyshevIFilter
	{
		#region Properties

		public double PassBandRipple
		{
			get => ((PassFilterBase)this.Prototype).PassBandRipple;
			set => ((PassFilterBase)this.Prototype).PassBandRipple = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BandStop.
		/// </summary>
		public BandStop(int order)
			: base(new PassFilterBase(order))
		{
		}

		#endregion
	}

		/// <summary>
	/// Represents a Chebyshev Type I low-shelf, high-shelf or band-shelf filter prototype.
	/// </summary>
	public class ShelfFilterBase : ChebyshevIFilterDesign, IShelfFilter
	{
		#region Fields

		private double gain;

		#endregion

		#region Properties

		public double Gain
		{
			get => this.gain;
			set
			{
				if (value != this.gain)
				{
					this.gain = value;
					this.OnPropertyChanged();
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ShelfFilterBase.
		/// </summary>
		public ShelfFilterBase(int order)
			: base(order)
		{
			this.Gain = -6.0d;
		}

		#endregion

		#region Methods

		protected override IEnumerable<PoleZeroPair> GetPoleZeroPairs(out PoleFilterNormal normal)
		{
			List<PoleZeroPair> list = new List<PoleZeroPair>();

			double gainDb = -this.Gain;
			double rippleDb = this.PassBandRipple;

			if (rippleDb >= Utility.Absolute(gainDb))
				rippleDb = Utility.Absolute(gainDb);
			if (gainDb < 0.0d)
				rippleDb = -rippleDb;

			double G = Utility.Pow(10.0d, gainDb / 20.0d);
			double Gb = Utility.Pow(10.0d, (gainDb - rippleDb) / 20.0d);
			double G0 = 1.0d;
			double g0 = Utility.Pow(G0, 1.0d / (double)this.Order);

			double eps;
			if (Gb != G0)
				eps = Utility.Sqrt((G * G - Gb * Gb) / (Gb * Gb - G0 * G0));
			else
				eps = G - 1.0d; // This is surely wrong

			double b = Utility.Pow(G / eps + Gb * Utility.Sqrt(1.0d + 1.0d / (eps * eps)), 1.0d / (double)this.Order);
			double u = Utility.Ln(b / g0);
			double v = Utility.Ln(Utility.Pow(1.0d / eps + Utility.Sqrt(1.0d + 1.0d / (eps * eps)), 1.0d / (double)this.Order));

			double sinh_u = Utility.Sinh(u);
			double sinh_v = Utility.Sinh(v);
			double cosh_u = Utility.Cosh(u);
			double cosh_v = Utility.Cosh(v);
			double n2 = 2.0d * (double)this.Order;
			int pairs = this.Order / 2;
			for (int i = 1; i <= pairs; ++i)
			{
				double a = ConstantsD.Pi * (2.0d * i - 1.0d) / n2;
				double sn = Utility.Sin(a);
				double cs = Utility.Cos(a);
				list.AddConjugatePair(new ComplexD(-sn * sinh_u, cs * cosh_u),
				                      new ComplexD(-sn * sinh_v, cs * cosh_v));
			}

			if ((this.Order & 1) > 0)
				list.Add(-sinh_u, -sinh_v);

			normal.W = ConstantsD.Pi;
			normal.Gain = 1.0;
			return list;
		}

		#endregion
	}

	/// <summary>
	/// Represents a Chebyshev Type I low-shelf filter design.
	/// </summary>
	public class LowShelf : PoleLowPassFilterDesign, IChebyshevIFilter, IShelfFilter
	{
		#region Properties

		public double PassBandRipple
		{
			get => ((ShelfFilterBase)this.Prototype).PassBandRipple;
			set => ((ShelfFilterBase)this.Prototype).PassBandRipple = value;
		}

		public double Gain
		{
			get => ((ShelfFilterBase)this.Prototype).Gain;
			set => ((ShelfFilterBase)this.Prototype).Gain = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LowShelf.
		/// </summary>
		public LowShelf(int order)
			: base(new ShelfFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Chebyshev Type I high-shelf filter design.
	/// </summary>
	public class HighShelf : PoleHighPassFilterDesign, IChebyshevIFilter, IShelfFilter
	{
		#region Properties

		public double PassBandRipple
		{
			get => ((ShelfFilterBase)this.Prototype).PassBandRipple;
			set => ((ShelfFilterBase)this.Prototype).PassBandRipple = value;
		}

		public double Gain
		{
			get => ((ShelfFilterBase)this.Prototype).Gain;
			set => ((ShelfFilterBase)this.Prototype).Gain = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new HighShelf.
		/// </summary>
		public HighShelf(int order)
			: base(new ShelfFilterBase(order))
		{
		}

		#endregion
	}

	/// <summary>
	/// Represents a Chebyshev Type I band-shelf filter design.
	/// </summary>
	public class BandShelf : PoleBandPassFilterDesign, IChebyshevIFilter, IShelfFilter
	{
		#region Properties

		public double PassBandRipple
		{
			get => ((ShelfFilterBase)this.Prototype).PassBandRipple;
			set => ((ShelfFilterBase)this.Prototype).PassBandRipple = value;
		}

		public double Gain
		{
			get => ((ShelfFilterBase)this.Prototype).Gain;
			set => ((ShelfFilterBase)this.Prototype).Gain = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BandShelf.
		/// </summary>
		public BandShelf(int order)
			: base(new ShelfFilterBase(order))
		{
		}

		#endregion
	}
}
