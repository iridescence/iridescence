﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Transposed direct form I biquad processor.
	/// https://ccrma.stanford.edu/~jos/fp/Transposed_Direct_Forms.html
	/// </summary>
	public class TransposedDirectFormI : BiquadProcessor
	{
		#region Fields

		private double s1;
		private double s2;
		private double s3;
		private double s4;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new TransposedDirectFormI.
		/// </summary>
		public TransposedDirectFormI()
		{

		}

		#endregion

		#region Methods

		public override double Process(in Biquad biquad, double sample)
		{
			double vIn = sample + this.s1;
			double vOut = biquad.B0 * vIn + this.s3;
			this.s1 = this.s2 - biquad.A1 * vIn;
			this.s2 = -biquad.A2 * vIn;
			this.s3 = biquad.B1 * vIn + this.s4;
			this.s4 = biquad.B2 * vIn;
			return vOut;
		}

		#endregion
	}
}
