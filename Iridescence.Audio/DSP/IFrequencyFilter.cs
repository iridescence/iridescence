﻿namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Interface for shelf filters.
	/// </summary>
	public interface IFrequencyFilter
	{
		/// <summary>
		/// Gets or sets the normalized frequency of the filter.
		/// </summary>
		double Frequency { get; set; }
	}
}
