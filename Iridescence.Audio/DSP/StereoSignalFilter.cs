﻿using System;
using Iridescence.Math;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Implements an audio source that puts another audio source through a filter.
	/// </summary>
	public class StereoSignalFilter : SignalProcessorBase<Stereo<float>, Stereo<float>>
	{
		#region Properties

		/// <summary>
		/// Gets the filter for the left channel.
		/// </summary>
		public Filter LeftFilter { get; }

		/// <summary>
		/// Gets the filter for the right channel.
		/// </summary>
		public Filter RightFilter { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignalFilter"/> class. 
		/// </summary>
		public StereoSignalFilter(ISignal<Stereo<float>> source, Filter leftFilter, Filter rightFilter, bool leaveOpen)
			: base(source, leaveOpen)
		{
			this.LeftFilter = leftFilter;
			this.RightFilter = rightFilter;
		}

		#endregion

		#region Methods

		public override int Read(Span<Stereo<float>> samples)
		{
			int n = this.Input.Read(samples);
			if (n <= 0)
				return n;

			samples = samples.Slice(0, n);
			int bufSize = Utility.Min(samples.Length, 128);

			Span<float> temp = stackalloc float[bufSize];
			while (samples.Length > 0)
			{
				int numSamples = Utility.Min(bufSize, samples.Length);

				// Copy left channel to temporary buffer.
				for (int i = 0; i < numSamples; ++i)
				{
					temp[i] = samples[i].L;
				}

				this.LeftFilter?.Process(temp.Slice(0, numSamples), temp.Slice(0, numSamples));

				// Copy temporary buffer back to left channel and right channel to temporary buffer.
				for (int i = 0; i < numSamples; ++i)
				{
					samples[i].L = temp[i];
					temp[i] = samples[i].R;
				}

				this.RightFilter?.Process(temp.Slice(0, numSamples), temp.Slice(0, numSamples));

				// Copy temporary buffer back to right channel.
				for (int i = 0; i < numSamples; ++i)
				{
					samples[i].R = temp[i];
				}

				samples = samples.Slice(numSamples);
			}

			return n;
		}

		#endregion
	}

	public static partial class FilterSourceExtensions
	{
		/// <summary>
		/// Returns an audio source that represents the input signal filtered using the specified <see cref="T:Filter"/>.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="leftFilter"></param>
		/// <param name="rightFilter"></param>
		/// <param name="leaveOpen"></param>
		/// <returns></returns>
		public static StereoSignalFilter Filter(this ISignal<Stereo<float>> source, Filter leftFilter, Filter rightFilter, bool leaveOpen = false)
		{
			return new StereoSignalFilter(source, leftFilter, rightFilter, leaveOpen);
		}

		/// <summary>
		/// Returns an audio source that represents the input signal filtered using a <see cref="T:Filter"/> created from the specified <see cref="FilterDesign"/>.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="leftDesign"></param>
		/// <param name="rightDesign"></param>
		/// <param name="leaveOpen"></param>
		/// <returns></returns>
		public static StereoSignalFilter Filter(this ISignal<Stereo<float>> source, FilterDesign leftDesign, FilterDesign rightDesign, bool leaveOpen = false)
		{
			return source.Filter(leftDesign.CreateFilter(), rightDesign.CreateFilter(), leaveOpen);
		}

		/// <summary>
		/// Returns an audio source that represents the input signal filtered using a <see cref="T:Filter"/> created from the specified <see cref="FilterDesign"/>.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="design"></param>
		/// <param name="leaveOpen"></param>
		/// <returns></returns>
		public static StereoSignalFilter Filter(this ISignal<Stereo<float>> source, FilterDesign design, bool leaveOpen = false)
		{
			return source.Filter(design.CreateFilter(), design.CreateFilter(), leaveOpen);
		}
	}
}
