﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Represents an abstract filter design.
	/// </summary>
	public abstract class FilterDesign : INotifyPropertyChanged
	{
		#region Events

		/// <summary>
		/// Occurs when the filter design has been altered in any way relevant to the output of filters using this design.
		/// </summary>
		public event EventHandler DesignChanged;

		/// <summary>
		/// Occurs when a filter property was changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FilterDesign.
		/// </summary>
		protected FilterDesign()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Returns a new <see cref="Filter"/> instance that utilizes this <see cref="FilterDesign"/>.
		/// </summary>
		/// <returns></returns>
		public abstract Filter CreateFilter();

		/// <summary>
		/// Called if a filtering-related setting is changed in the design and filters need to update themselves.
		/// </summary>
		protected virtual void OnDesignChanged()
		{
			this.DesignChanged?.Invoke(this, EventArgs.Empty);
		}

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null, bool designChanged = true)
		{
			this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

			if (designChanged)
			{
				this.OnDesignChanged();
			}
		}

		#endregion
	}
}
