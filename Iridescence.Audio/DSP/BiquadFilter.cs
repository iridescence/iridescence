﻿using System;
using System.Linq;
using Iridescence.Math;

namespace Iridescence.Audio.DSP
{
	/// <summary>
	/// Implements a <see cref="Filter"/> that uses biquads to filter samples.
	/// </summary>
	public class BiquadFilter : Filter
	{
		#region Fields

		private readonly BiquadFilterDesign biquadDesign;
		private readonly Func<BiquadProcessor> biquadProcessorFactory;

		private Biquad[] biquads;
		private BiquadProcessor[] processors;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new BiquadFilter.
		/// </summary>
		public BiquadFilter(BiquadFilterDesign design, Func<BiquadProcessor> biquadProcessorFactory)
			: base(design)
		{
			this.biquadDesign = design;
			this.biquadProcessorFactory = biquadProcessorFactory;
		}

		#endregion

		#region Methods

		protected override void Update()
		{
			this.biquads = this.biquadDesign.Biquads.ToArray();

			if (this.processors == null || this.processors.Length != this.biquads.Length)
			{
				this.processors = new BiquadProcessor[this.biquads.Length];

				for (int i = 0; i < this.processors.Length; i++)
					this.processors[i] = this.biquadProcessorFactory();
			}
		}

		protected override void Run(ReadOnlySpan<double> input, Span<double> output)
		{
			for (int i = 0; i < input.Length; ++i)
			{
				double vOut = input[i];
				for (int j = 0; j < this.processors.Length; ++j)
				{
					vOut = this.processors[j].Process(in this.biquads[j], vOut);
				}
				output[i] = vOut;
			}
		}

		public override ComplexD GetResponse(double normalizedFrequency)
		{
			return GetResponse(normalizedFrequency, this.biquads);
		}

		public static ComplexD GetResponse(double normalizedFrequency, Biquad[] biquads)
		{
			double w = ConstantsD.Pi2 * normalizedFrequency;
			ComplexD czn1 = ComplexD.Polar(1.0d, -w);
			ComplexD czn2 = ComplexD.Polar(1.0d, -2.0d * w);
			ComplexD ch = 1.0d;
			ComplexD cbot = 1.0d;

			foreach (Biquad stage in biquads)
			{
				ComplexD cb = 1.0d;
				ComplexD ct = stage.B0 / stage.A0;
				ct = ct.AddMult(stage.B1 / stage.A0, czn1);
				ct = ct.AddMult(stage.B2 / stage.A0, czn2);
				cb = cb.AddMult(stage.A1 / stage.A0, czn1);
				cb = cb.AddMult(stage.A2 / stage.A0, czn2);
				ch *= ct;
				cbot *= cb;
			}

			if (cbot == 0.0d)
				return double.NaN;

			return ch / cbot;
		}

		#endregion
	}
}
