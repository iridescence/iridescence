﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents an abstract source of audio samples.
	/// </summary>
	public interface ISignal : IDisposable
	{
		/// <summary>
		/// Gets a value that indicates whether this audio source supports seeking.
		/// </summary>
		bool CanSeek { get; }

		/// <summary>
		/// Gets or sets the position in the audio stream in samples.
		/// </summary>
		long Position { get; set; }

		/// <summary>
		/// Gets the length of the audio stream (in samples).
		/// </summary>
		long Length { get; }

		/// <summary>
		/// Gets the number of samples per second.
		/// </summary>
		int SampleRate { get; }
	}
}
