﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents an <see cref="ISignal"/> with samples of type <see cref="T"/> that can be read.
	/// </summary>
	public interface ISignal<T> : ISignal
	{
		/// <summary>
		/// Reads samples from the stream and returns how many were read.
		/// </summary>
		/// <param name="samples">A <see cref="Span{T}"/> to store the samples.</param>
		/// <returns>The number of samples read, or -1 if the audio source ended.</returns>
		/// <remarks>
		/// The <see cref="Read"/> method should always read as many samples as have been requested, if possible.
		/// If less samples are returned than have been requested, it should be assumed that the source has no more samples to read at the moment.
		/// This can be the case for real-time audio sources (such as a microphone or a web radio stream).
		/// This behaviour is different from the behaviour of the <see cref="System.IO.Stream"/> class.
		/// </remarks>
		int Read(Span<T> samples);
	}
}
