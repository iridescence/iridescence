﻿namespace Iridescence.Audio
{
	/// <summary>
	/// Represents an abstract audio source that applies an operator to one or more sources to produce one new source.
	/// The channels of the <see cref="OperatorSignal{TIn,TOut}"/> can be modified at any time, unlike the <see cref="ImmutableOperatorSignal{TIn,TOut}"/>.
	/// </summary>
	public abstract class OperatorSignal<TIn, TOut> : ImmutableOperatorSignal<TIn, TOut>
	{
		#region Fields
		
		#endregion
		
		#region Properties

		/// <summary>
		/// Gets the channels of the <see cref="OperatorSignal{TIn,TOut}"/>.
		/// </summary>
		public new OperatorChannelCollection<TIn> Channels => base.Channels;
		
		#endregion
		
		#region Constructors
		
		protected OperatorSignal(int sampleRate, OperatorSignalFlags flags, int bufferSize = AudioConstants.DefaultBufferSize)
			: base(sampleRate, flags, bufferSize)
		{

		}
		
		#endregion
		
		#region Methods
		
		#endregion
	}
}
