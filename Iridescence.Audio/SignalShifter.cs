﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Defines the direction of a bitwise shift.
	/// </summary>
	public enum BitwiseShiftDirection
	{
		Left,
		Right
	}

	/// <summary>
	/// Implements an <see cref="ISignal{T}"/> that performs a bitwise shift on each sample in the input.
	/// </summary>
	public class SignalShifter : SignalProcessorBase<int, int>
	{
		#region Fields

		private readonly BitwiseShiftDirection direction;
		private readonly int shift;
		
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignalShifter"/> class. 
		/// </summary>
		public SignalShifter(ISignal<int> input, bool leaveOpen, BitwiseShiftDirection direction, int shift)
			: base(input, leaveOpen)
		{
			if (shift < 0 || shift >= 32)
				throw new ArgumentOutOfRangeException(nameof(shift));

			this.direction = direction;
			this.shift = shift;
		}

		#endregion

		#region Methods

		private static void shiftLeft(Span<int> samples, int shift)
		{
			for (int i = 0; i < samples.Length; ++i)
			{
				samples[i] <<= shift;
			}
		}

		private static void shiftRight(Span<int> samples, int shift)
		{
			for (int i = 0; i < samples.Length; ++i)
			{
				samples[i] >>= shift;
			}
		}

		public override int Read(Span<int> samples)
		{
			int n = base.Input.Read(samples);

			if (n > 0)
			{
				if (this.direction == BitwiseShiftDirection.Left)
				{
					shiftLeft(samples.Slice(0, n), this.shift);
				}
				else
				{
					shiftRight(samples.Slice(0, n), this.shift);
				}
			}
			
			return n;
		}
		
		#endregion
	}

	/// <summary>
	/// Implements an <see cref="ISignal{T}"/> that performs a bitwise shift on each sample in the input.
	/// </summary>
	public class StereoSignalShifter : SignalProcessorBase<Stereo<int>, Stereo<int>>
	{
		#region Fields

		private readonly BitwiseShiftDirection direction;
		private readonly Stereo<int> shift;
		
		#endregion
		
		#region Properties
		
		#endregion
		
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="StereoSignalShifter"/> class. 
		/// </summary>
		public StereoSignalShifter(ISignal<Stereo<int>> input, bool leaveOpen, BitwiseShiftDirection direction, Stereo<int> shift)
			: base(input, leaveOpen)
		{
			if (shift.L < 0 || shift.L >= 32 || shift.R < 0 || shift.R >= 32)
				throw new ArgumentOutOfRangeException(nameof(shift));

			this.direction = direction;
			this.shift = shift;
		}

		#endregion

		#region Methods

		private static void shiftLeft(Span<Stereo<int>> samples, Stereo<int> shift)
		{
			for (int i = 0; i < samples.Length; ++i)
			{
				samples[i].L <<= shift.L;
				samples[i].R <<= shift.R;
			}
		}

		private static void shiftRight(Span<Stereo<int>> samples, Stereo<int> shift)
		{
			for (int i = 0; i < samples.Length; ++i)
			{
				samples[i].L >>= shift.L;
				samples[i].R >>= shift.R;
			}
		}

		public override int Read(Span<Stereo<int>> samples)
		{
			int n = base.Input.Read(samples);

			if (n > 0)
			{
				if (this.direction == BitwiseShiftDirection.Left)
				{
					shiftLeft(samples.Slice(0, n), this.shift);
				}
				else
				{
					shiftRight(samples.Slice(0, n), this.shift);
				}
			}
			
			return n;
		}
		
		#endregion
	}
}
