﻿using System;
using Iridescence.Math;

namespace Iridescence.Audio
{
	public delegate void SampleTypeConversionFunction<TFrom, TTo>(ReadOnlySpan<TFrom> from, Span<TTo> to);

	/// <summary>
	/// Implements a sample type conversion wrapper for an audio source.
	/// </summary>
	public class SampleTypeConverter<TFrom, TTo> : SignalProcessorBase<TFrom, TTo>
	{
		#region Fields

		private readonly SampleTypeConversionFunction<TFrom, TTo> func;
		private readonly TFrom[] buffer;
		private bool ended;
		
		#endregion
		
		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SampleTypeConverter{TFrom,TTo}"/> class. 
		/// </summary>
		public SampleTypeConverter(ISignal<TFrom> source, SampleTypeConversionFunction<TFrom, TTo> func, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
			: base(source, leaveOpen)
		{
			if (bufferSize < 1)
				throw new ArgumentOutOfRangeException(nameof(bufferSize));

			this.func = func ?? throw new ArgumentNullException(nameof(func));
			this.buffer = new TFrom[bufferSize];
		}
		
		#endregion
		
		#region Methods
		
		public override int Read(Span<TTo> samples)
		{
			if (this.ended)
				return -1;

			int totalSamples = -1;
			while (samples.Length > 0)
			{
				// Read from source to buffer.
				int numSamples = Utility.Min(this.buffer.Length, samples.Length);
				int numSamplesRead = this.Input.Read(new Span<TFrom>(this.buffer, 0, numSamples));
				if (numSamplesRead < 0)
					break;

				// Convert buffer.
				this.func(new ReadOnlySpan<TFrom>(this.buffer, 0, numSamplesRead), samples.Slice(0, numSamplesRead));
				samples = samples.Slice(numSamplesRead);

				if (totalSamples < 0)
					totalSamples = numSamplesRead;
				else
					totalSamples += numSamplesRead;

				// We did not get all samples we have requested, assume that the source does not have any more samples at the time.
				if (numSamples != numSamplesRead)
					break;
			}

			return totalSamples;
		}

		public override void Dispose()
		{
			this.ended = true;
			base.Dispose();
		}

		#endregion
	}
}
