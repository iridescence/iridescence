﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Implements an <see cref="ISignal{T}"/> that always returns the number of samples that have been requested, padding the source with zeroes if necessary.
	/// </summary>
	public class PaddingSignal<T> : SignalProcessorBase<T, T>, ICompletableSignal
	{
		#region Fields

		private bool isCompleted;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PaddingSignal{T}"/> class. 
		/// </summary>
		public PaddingSignal(ISignal<T> source, bool leaveOpen)
			: base(source, leaveOpen)
		{

		}

		#endregion
		
		#region Methods
		
		public override int Read(Span<T> samples)
		{
			if (this.isCompleted)
				return -1;

			int numSamples = this.Input.Read(samples);
			if (numSamples < 0)
				numSamples = 0;

			if (numSamples != samples.Length)
			{
				samples.Slice(numSamples, samples.Length - numSamples).Clear();
			}

			return samples.Length;
		}

		public void Complete()
		{
			this.isCompleted = true;
		}

		public override void Dispose()
		{
			this.isCompleted = true;
			base.Dispose();
		}

		#endregion
	}

	public static class PaddingSourceExtensions
	{
		/// <summary>
		/// Creates a <see cref="PaddingSignal{T}"/> that wraps the specified source.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="source"></param>
		/// <param name="leaveOpen"></param>
		/// <returns></returns>
		public static PaddingSignal<T> Pad<T>(this ISignal<T> source, bool leaveOpen = false)
		{
			if (source is PaddingSignal<T> pad)
				return pad;

			return new PaddingSignal<T>(source, leaveOpen);
		}
	}
}
