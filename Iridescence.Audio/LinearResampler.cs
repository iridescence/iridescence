﻿using System;
using Iridescence.Math;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents a resampling function.
	/// </summary>
	/// <typeparam name="T">The sample type.</typeparam>
	/// <returns></returns>
	public delegate int LinearResamplingMethod<T>(ref ResamplerState state, ReadOnlySpan<T> source, Span<T> dest, Span<double> factor);

	/// <summary>
	/// Represents a linear interpolation resampler.
	/// </summary>
	public class LinearResampler<T> : SignalProcessorBase<T, T>
	{
		#region Fields

		private readonly LinearResamplingMethod<T> resample;

		private bool readRequired;
		private readonly T[] buffer;
		private readonly double[] factorBuffer;
		private int bufferIndex;
		private int bufferCount;

		private readonly ISignal<double> factor;
		private readonly double constantFactor;
		private long position;

		private ResamplerState state;

		#endregion

		#region Properties

		public override bool CanSeek
		{
			get
			{
				if(this.constantFactor <= 0.0)
					return false;

				return base.CanSeek;
			}
		}

		public override long Position
		{
			get => this.position;
			set
			{
				if (this.constantFactor <= 0.0)
					throw new NotSupportedException("Can't seek in a resampler with variable factor signal.");
				
				if (value < 0 || value > this.Length)
					throw new ArgumentOutOfRangeException(nameof(value));
				
				if (value == this.position)
					return;

				this.bufferIndex = 0;
				this.bufferCount = 0;
				this.position = value;

				double pos = this.position * this.constantFactor;
				long samplePos = (long)pos;
				this.Input.Position = samplePos;
			}
		}

		public override long Length
		{
			get
			{
				if (this.constantFactor <= 0.0)
					throw new NotSupportedException("Can't seek in a resampler with variable factor signal.");
				
				double len = 1 + (this.Input.Length - 1) / this.constantFactor;
				return Utility.Max(0, (long)len);
			}
		}

		public override int SampleRate { get; }

		#endregion

		#region Constructors

		private LinearResampler(ISignal<T> source, bool leaveOpen, int sampleRate, int bufferSize, LinearResamplingMethod<T> resample)
			: base(source, leaveOpen)
		{
			if (sampleRate <= 0)
				throw new ArgumentOutOfRangeException(nameof(sampleRate));

			if (bufferSize <= 2)
				throw new ArgumentOutOfRangeException(nameof(bufferSize));

			this.SampleRate = sampleRate;

			this.readRequired = true;
			this.buffer = new T[bufferSize];
			this.factorBuffer = new double[bufferSize];
			this.bufferCount = 0;

			this.resample = resample ?? throw new ArgumentNullException(nameof(resample));
			this.constantFactor = double.NegativeInfinity;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="LinearResampler{T}"/> class. 
		/// </summary>
		public LinearResampler(ISignal<T> source, LinearResamplingMethod<T> resample, int outputSampleRate, double factor, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
			: this(source, leaveOpen, outputSampleRate, bufferSize, resample)
		{
			this.constantFactor = factor;
			this.factor = new ConstantSignal<double>(this.constantFactor, source.SampleRate);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="LinearResampler{T}"/> class. 
		/// </summary>
		public LinearResampler(ISignal<T> source, LinearResamplingMethod<T> resample, int outputSampleRate, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
			: this(source, resample, outputSampleRate, (double)source.SampleRate / (double)outputSampleRate, leaveOpen, bufferSize)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="LinearResampler{T}"/> class. 
		/// </summary>
		public LinearResampler(ISignal<T> source, LinearResamplingMethod<T> resample, int outputSampleRate, ISignal<double> factor, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
			: this(source, leaveOpen, outputSampleRate, bufferSize, resample)
		{
			if (factor == null)
				throw new ArgumentNullException(nameof(factor));

			if (factor.SampleRate != source.SampleRate)
				throw new ArgumentException("The speed factor signal must have the same sample rate as the source signal.", nameof(factor));
			
			this.factor = factor;

			if(this.factor is ConstantSignal<double> constFactor)
				this.constantFactor = constFactor.Value;
		}

		#endregion

		#region Methods
		
		private enum ReadBufferResult
		{
			EndOfSource,
			MoreData,
			Stalling,
		}

		private ReadBufferResult readBuffer(int requiredSamples)
		{
			if (this.bufferIndex < this.bufferCount)
			{
				Array.Copy(this.buffer, this.bufferIndex, this.buffer, 0, this.bufferCount - this.bufferIndex);
				Array.Copy(this.factorBuffer, this.bufferIndex, this.factorBuffer, 0, this.bufferCount - this.bufferIndex);
				this.bufferCount -= this.bufferIndex;
				this.bufferIndex = 0;
			}
			else if (this.bufferIndex > this.bufferCount)
			{
				Array.Copy(this.buffer, this.bufferCount, this.buffer, 0, this.buffer.Length - this.bufferCount);
				Array.Copy(this.factorBuffer, this.bufferIndex, this.factorBuffer, 0, this.bufferCount - this.bufferIndex);
				this.bufferIndex -= this.bufferCount;
				this.bufferCount = 0;
			}
			else
			{
				this.bufferIndex = 0;
				this.bufferCount = 0;
			}

			int numSamples = this.buffer.Length - this.bufferCount;

			if (this.constantFactor > 0.0)
			{
				int estimate = (int)(requiredSamples * this.constantFactor) + 1;
				if (numSamples > estimate)
					numSamples = estimate;
			}

			int numSamplesRead = this.Input.Read(new Span<T>(this.buffer, this.bufferCount, numSamples));
			if (numSamplesRead < 0)
				return ReadBufferResult.EndOfSource;

			if (this.factor.Read(new Span<double>(this.factorBuffer, this.bufferCount, numSamplesRead)) != numSamplesRead)
				throw new InvalidOperationException("The factor signal must not return less samples than the source signal.");

			this.bufferCount += numSamplesRead;
			if (numSamplesRead < numSamples)
				return ReadBufferResult.Stalling;

			return ReadBufferResult.MoreData;
		}

		public override int Read(Span<T> samples)
		{
			int total = -1;
			while (samples.Length > 0)
			{
				ReadBufferResult result = ReadBufferResult.MoreData;
				if (this.readRequired)
				{
					// We need to fill the buffer.
					result = this.readBuffer(samples.Length);

					if (result == ReadBufferResult.EndOfSource)
					{
						if (total == 0)
							total = -1;
						break; // There are no more samples to read but we would need more: The stream has ended.
					}

					this.readRequired = false;
				}

				// Resample the samples in our buffer.
				this.state.SourceIndex = this.bufferIndex;
				int numSamples = this.resample(ref this.state, this.buffer.ToSpan(0, this.bufferCount), samples, this.factorBuffer.ToSpan(0, this.bufferCount));
				this.bufferIndex = this.state.SourceIndex;

				if (total < 0)
					total = numSamples;
				else
					total += numSamples;

				if (numSamples > 0)
				{
					this.position += numSamples;

					if (numSamples == samples.Length)
						break; // We have read all samples we were supposed to read, exit the loop.

					samples = samples.Slice(numSamples);
				}

				this.readRequired = true;

				if (result == ReadBufferResult.Stalling)
					break; // The readBuffer method reported that the source returned less samples than it requested. That means there are currently no more samples to read.
			}

			return total;
		}

		#endregion
	}

	public struct ResamplerState
	{
		public int SourceIndex;
		public double Position;
	}

	/// <summary>
	/// Implements the resampling methods for different sample types.
	/// </summary>
	public static class LinearResamplers
	{
		private const double epsilon = 1e-10;

		/// <summary>
		/// Linear resampling for stereo float samples.
		/// </summary>
		public static int Resample(ref ResamplerState state, ReadOnlySpan<Stereo<float>> source, Span<Stereo<float>> dest, Span<double> factor)
		{
			int destIndex;
			for (destIndex = 0; destIndex < dest.Length; ++destIndex)
			{
				int offset = (int)state.Position;
				double fraction = state.Position - offset;
				int sourceIndex = state.SourceIndex + offset;
				
				if (fraction < epsilon)
				{
					if (sourceIndex >= source.Length)
						break;

					dest[destIndex] = source[sourceIndex];
				}
				else
				{
					if (sourceIndex + 1 >= source.Length)
						break;

					float l0 = source[sourceIndex].L;
					float l1 = source[sourceIndex + 1].L;
					float r0 = source[sourceIndex].R;
					float r1 = source[sourceIndex + 1].R;

					dest[destIndex].L = (float)(l0 * (1.0 - fraction) + l1 * fraction);
					dest[destIndex].R = (float)(r0 * (1.0 - fraction) + r1 * fraction);
				}

				double nextFactor = factor[sourceIndex];
				if (nextFactor < 0.0)
					throw new InvalidOperationException("Factor value can not be negative.");

				state.Position = fraction + nextFactor;
				state.SourceIndex = sourceIndex;
			}

			return destIndex;
		}

		/// <summary>
		/// Linear resampling for stereo float samples.
		/// </summary>
		public static int Resample(ref ResamplerState state, ReadOnlySpan<float> source, Span<float> dest, Span<double> factor)
		{
			int destIndex;
			for (destIndex = 0; destIndex < dest.Length; ++destIndex)
			{
				int offset = (int)state.Position;
				double fraction = state.Position - offset;
				int sourceIndex = state.SourceIndex + offset;
				
				if (fraction < epsilon)
				{
					if (sourceIndex >= source.Length)
						break;

					dest[destIndex] = source[sourceIndex];
				}
				else
				{
					if (sourceIndex + 1 >= source.Length)
						break;

					float l0 = source[sourceIndex];
					float l1 = source[sourceIndex + 1];

					dest[destIndex] = (float)(l0 * (1.0 - fraction) + l1 * fraction);
				}

				double nextFactor = factor[sourceIndex];
				if (nextFactor < 0.0)
					throw new InvalidOperationException("Factor value can not be negative.");

				state.Position = fraction + nextFactor;
				state.SourceIndex = sourceIndex;
			}

			return destIndex;
		}
	}

	public static class LinearResamplerExtensions
	{
		/// <summary>
		/// Resamples the specified audio source, if necessary.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="newSampleRate"></param>
		/// <param name="leaveOpen"></param>
		/// <param name="bufferSize"></param>
		/// <returns></returns>
		public static ISignal<float> Resample(this ISignal<float> source, int newSampleRate, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			if (source.SampleRate == newSampleRate)
				return source;

			return new LinearResampler<float>(source, LinearResamplers.Resample, newSampleRate, leaveOpen, bufferSize);
		}

		/// <summary>
		/// Resamples the specified audio source, if necessary.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="newSampleRate"></param>
		/// <param name="leaveOpen"></param>
		/// <param name="bufferSize"></param>
		/// <returns></returns>
		public static ISignal<Stereo<float>> Resample(this ISignal<Stereo<float>> source, int newSampleRate, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			if (source.SampleRate == newSampleRate)
				return source;

			return new LinearResampler<Stereo<float>>(source, LinearResamplers.Resample, newSampleRate, leaveOpen, bufferSize);
		}

		/// <summary>
		/// Changes the playback speed of the source signal by the specified factor, resulting in a pitch-bend effect.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="factor"></param>
		/// <param name="leaveOpen"></param>
		/// <param name="bufferSize"></param>
		/// <returns></returns>
		public static ISignal<float> ChangeSpeed(this ISignal<float> source, double factor, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			if (factor == 1.0)
				return source;

			return new LinearResampler<float>(source, LinearResamplers.Resample, source.SampleRate, source.SampleRate * factor, leaveOpen, bufferSize);
		}

		/// <summary>
		/// Changes the playback speed of the source signal by the specified factor, resulting in a pitch-bend effect.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="factor"></param>
		/// <param name="leaveOpen"></param>
		/// <param name="bufferSize"></param>
		/// <returns></returns>
		public static ISignal<float> ChangeSpeed(this ISignal<float> source, ISignal<double> factor, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			return new LinearResampler<float>(source, LinearResamplers.Resample, source.SampleRate, factor, leaveOpen, bufferSize);
		}
		
		/// <summary>
		/// Changes the playback speed of the source signal by the specified factor, resulting in a pitch-bend effect.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="factor"></param>
		/// <param name="leaveOpen"></param>
		/// <param name="bufferSize"></param>
		/// <returns></returns>
		public static ISignal<Stereo<float>> ChangeSpeed(this ISignal<Stereo<float>> source, double factor, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			if (factor == 1.0)
				return source;

			return new LinearResampler<Stereo<float>>(source, LinearResamplers.Resample, source.SampleRate, source.SampleRate * factor, leaveOpen, bufferSize);
		}

		
		/// <summary>
		/// Changes the playback speed of the source signal by the specified factor, resulting in a pitch-bend effect.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="factor"></param>
		/// <param name="leaveOpen"></param>
		/// <param name="bufferSize"></param>
		/// <returns></returns>
		public static ISignal<Stereo<float>> ChangeSpeed(this ISignal<Stereo<float>> source, ISignal<double> factor, bool leaveOpen = false, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			return new LinearResampler<Stereo<float>>(source, LinearResamplers.Resample, source.SampleRate, factor, leaveOpen, bufferSize);
		}
	}
}
