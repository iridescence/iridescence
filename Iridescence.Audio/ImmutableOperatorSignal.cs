﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Iridescence.Math;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents an abstract audio source that applies an operator to one or more sources to produce one new source.
	/// </summary>
	public abstract class ImmutableOperatorSignal<TIn, TOut> : ISignal<TOut>, ISignalProcessor, ICompletableSignal
	{
		#region Fields

		private OperatorChannel<TIn>[] channelCopy;
		private ReadOnlyMemory<TIn>[] channelData;

		private readonly int bufferSize;
		private readonly OperatorSignalFlags flags;
		private long position;
		private bool isComplete;

		private int isReading;

		#endregion
		
		#region Properties

		public bool CanSeek
		{
			get
			{
				lock (this.Channels.Lock)
				{
					return this.Channels.All(c => c.CanSeek);
				}
			}
		}

		public long Position
		{
			get => this.position;
			set
			{
				lock (this.Channels.Lock)
				{
					if (!this.CanSeek)
						throw new InvalidOperationException("The audio source does not support seeking.");

					this.position = value;
					for (int i = 0; i < this.Channels.Channels.Count; ++i)
					{
						this.Channels.Channels[i].Clear();
						this.Channels.Channels[i].Source.Position = this.Channels.Channels[i].InitialPosition + this.position;
					}
				}
			}
		}

		public long Length
		{
			get
			{
				lock (this.Channels.Lock)
				{
					long? length = null;

					foreach (OperatorChannel<TIn> channel in this.Channels.Channels.Where(c => (c.Flags & (OperatorChannelFlags.RemoveWhenEnded | OperatorChannelFlags.DisposeWhenEnded)) != 0))
					{
						length = length == null ? channel.Source.Length : Utility.Max(length.Value, channel.Source.Length);
					}

					foreach (OperatorChannel<TIn> channel in this.Channels.Channels.Where(c => (c.Flags & (OperatorChannelFlags.RemoveWhenEnded | OperatorChannelFlags.DisposeWhenEnded)) == 0))
					{
						length = length == null ? channel.Source.Length : Utility.Min(length.Value, channel.Source.Length);
					}

					return length ?? 0;
				}
			}
		}

		public int SampleRate { get; }

		/// <summary>
		/// Gets the channels of the <see cref="ImmutableOperatorSignal{TIn,TOut}"/>
		/// </summary>
		protected OperatorChannelCollection<TIn> Channels { get; }

		IEnumerable<ISignal> ISignalProcessor.Sources => this.Channels;
		
		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ImmutableOperatorSignal{TIn,TOut}"/> class. 
		/// </summary>
		protected ImmutableOperatorSignal(int sampleRate, OperatorSignalFlags flags, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			this.SampleRate = sampleRate;
			this.Channels = new OperatorChannelCollection<TIn>(this.createChannel);
			this.flags = flags;
			this.bufferSize = bufferSize;
		}

		#endregion

		#region Methods

		private OperatorChannel<TIn> createChannel(ISignal<TIn> source, OperatorChannelFlags flags)
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));

			if (source.SampleRate != this.SampleRate)
				throw new ArgumentException("Sample rate mismatch.", nameof(source));

			return new OperatorChannel<TIn>(source, flags, this.bufferSize);
		}

		public int Read(Span<TOut> samples)
		{
			if (this.isComplete)
				return -1;

			if (Interlocked.CompareExchange(ref this.isReading, 1, 0) != 0)
				throw new InvalidOperationException("Re-entrant or multi-threaded reading from operator sources is not allowed.");

			try
			{
				// Get the channels to process.
				Span<OperatorChannel<TIn>> channels;
				lock (this.Channels.Lock)
				{
					int count = this.Channels.Count;

					if (count == 0)
						return this.ReadEmpty(samples);

					if (this.channelCopy == null)
					{
						this.channelCopy = new OperatorChannel<TIn>[count];
						this.channelData = new ReadOnlyMemory<TIn>[count];
					}
					else if (this.channelCopy.Length < count)
					{
						Array.Resize(ref this.channelCopy, count);
						Array.Resize(ref this.channelData, count);
					}

					for (int i = 0; i < count; ++i)
					{
						this.channelCopy[i] = this.Channels.Channels[i];
					}

					channels = this.channelCopy.ToSpan(0, count);
				}

				// Process channels.
				int total = -1;
				while (samples.Length > 0)
				{
					// Read from sources.
					int? samplesAvailable = null;

					for (int i = 0; i < channels.Length; ++i)
					{
						OperatorChannel<TIn> channel = channels[i];
						if(channel == null)
							continue;

						channel.Acquire(samples.Length);

						if (channel.SamplesAvailable < 0 && (channel.Flags & (OperatorChannelFlags.RemoveWhenEnded | OperatorChannelFlags.DisposeWhenEnded)) != 0)
						{
							// Remove channel.
							this.Channels.Remove(channel);

							// Optionally dispose source.
							if ((channel.Flags & OperatorChannelFlags.DisposeWhenEnded) != 0)
								channel.Source.Dispose();

							channels[i] = null;
						}
						else
						{
							samplesAvailable = samplesAvailable == null ? channel.SamplesAvailable : Utility.Min(samplesAvailable.Value, channel.SamplesAvailable);
						}
					}

					samplesAvailable ??= -1;

					if (samplesAvailable > 0)
					{
						// Process samples.
						int j = 0;
						foreach (OperatorChannel<TIn> channel in channels)
						{
							if (channel == null)
								continue;

							this.channelData[j++] = new ReadOnlyMemory<TIn>(channel.Buffer, 0, samplesAvailable.Value);
						}

						ReadOnlyChannelData<TIn> cd = new ReadOnlyChannelData<TIn>(this.channelData.ToSpan(0, j));
						this.Process(cd, samples.Slice(0, samplesAvailable.Value));
					}

					// Finish source channel reading.
					int processedSamples = Utility.Max(0, samplesAvailable.Value);
					foreach (OperatorChannel<TIn> channel in channels)
					{
						channel?.Move(processedSamples);
					}

					if (samplesAvailable < 0)
						break;

					if (total < 0)
						total = samplesAvailable.Value;
					else
						total += samplesAvailable.Value;

					if (samplesAvailable <= 0)
						break;

					samples = samples.Slice(samplesAvailable.Value);
				}

				if (total > 0)
				{
					this.position += total;
				}
				else if (total < 0)
				{
					if ((this.flags & OperatorSignalFlags.Continuous) != 0)
						total = 0;
					else
						this.isComplete = true;
				}

				return total;
			}
			finally
			{
				Interlocked.Exchange(ref this.isReading, 0);
			}
		}

		protected virtual int ReadEmpty(Span<TOut> samples)
		{
			if ((this.flags & OperatorSignalFlags.Continuous) != 0)
				return 0;
			return -1;
		}

		protected abstract void Process(ReadOnlyChannelData<TIn> channelData, Span<TOut> result);

		public void Complete()
		{
			this.isComplete = true;
		}

		public void Dispose()
		{
			lock (this.Channels.Lock)
			{
				this.isComplete = true;

				if ((this.flags & OperatorSignalFlags.LeaveOpen) == 0)
				{
					foreach (OperatorChannel<TIn> channel in this.Channels.Channels)
					{
						channel.Source.Dispose();
					}
				}
			}
		}

		#endregion
	}
}
