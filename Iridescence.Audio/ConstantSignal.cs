﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Implements an <see cref="ISignal{T}"/> that returns an infinite constant signal of the specified value.
	/// </summary>
	public class ConstantSignal<T> : ISignal<T>, ICompletableSignal
	{
		#region Fields

		private bool isCompleted;

		#endregion

		#region Properties

		public bool CanSeek => false;

		public long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		public long Length => throw new NotSupportedException();

		public int SampleRate { get; }

		/// <summary>
		/// Gets the constant value that is emitted by this signal.
		/// </summary>
		public T Value { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ConstantSignal{T}"/> class. 
		/// </summary>
		public ConstantSignal(T value, int sampleRate)
		{
			this.Value = value;
			this.SampleRate = sampleRate;
		}

		#endregion

		#region Methods

		public int Read(Span<T> samples)
		{
			if (this.isCompleted)
				return -1;

			samples.Fill(this.Value);
			return samples.Length;
		}

		public void Complete()
		{
			this.isCompleted = true;
		}

		public void Dispose()
		{
			this.isCompleted = true;
		}

		#endregion
	}
}
