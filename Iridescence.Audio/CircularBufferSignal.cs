﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Represents a circular audio buffer.
	/// </summary>
	public class CircularBufferSignal<T> : ISignal<T>, ICompletableSignal
	{
		#region Fields

		private readonly CircularBuffer<T> buffer;
		private bool isComplete;

		#endregion

		#region Properties

		public bool CanSeek => false;

		public long Position
		{
			get => throw new NotSupportedException("The audio source does not support seeking.");
			set => throw new NotSupportedException("The audio source does not support seeking.");
		}

		public long Length => throw new NotSupportedException("The audio source has no length because it is infinite.");

		public int SampleRate { get; }

		public int Count => this.buffer.Count;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="CircularBufferSignal{T}"/> class. 
		/// </summary>
		public CircularBufferSignal(int sampleRate, int bufferSize)
		{
			if (bufferSize <= 0)
				throw new ArgumentOutOfRangeException(nameof(bufferSize));

			this.buffer = new CircularBuffer<T>(bufferSize);
			this.SampleRate = sampleRate;
		}

		#endregion

		#region Methods

		public int Write(ReadOnlySpan<T> samples, bool overwrite)
		{
			if (this.isComplete)
				throw new InvalidOperationException("The circular audio buffer has been completed and can not be written to anymore.");

			return this.buffer.Write(samples, overwrite);
		}

		public int Read(Span<T> samples)
		{
			int n = this.buffer.Read(samples);
			if (n == 0 && this.isComplete)
				return -1;

			return n;
		}

		public void Complete()
		{
			this.isComplete = true;
		}

		public void Dispose()
		{
			this.Complete();
		}

		#endregion
	}
}
