﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Iridescence.Audio
{
	/// <summary>
	/// Base class for single input <see cref="ISignal{T}"/> processors.
	/// </summary>
	[DebuggerDisplay("{" + nameof(DebugString) + "}")]
	public abstract class SignalProcessorBase<TIn, TOut> : ISignal<TOut>, ISignalProcessor
	{
		#region Fields

		private readonly bool leaveOpen;

		#endregion

		#region Properties

		public virtual bool CanSeek => this.Input.CanSeek;

		public virtual long Position
		{
			get => this.Input.Position;
			set => this.Input.Position = value;
		}

		public virtual long Length => this.Input.Length;

		public virtual int SampleRate => this.Input.SampleRate;

		protected ISignal<TIn> Input { get; }

		IEnumerable<ISignal> ISignalProcessor.Sources
		{
			get { yield return this.Input; }
		}

		private string DebugString => this.Visualize();

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="SignalProcessorBase{TIn,TOut}"/> class. 
		/// </summary>
		/// <param name="input">The input source.</param>
		/// <param name="leaveOpen">Whether the input should be left open when this instance is diposed. If set to false, input will be disposed as well.</param>
		protected SignalProcessorBase(ISignal<TIn> input, bool leaveOpen)
		{
			this.Input = input ?? throw new ArgumentNullException(nameof(input));
			this.leaveOpen = leaveOpen;
		}
		
		#endregion
		
		#region Methods

		public abstract int Read(Span<TOut> samples);

		public virtual void Dispose()
		{
			if (!this.leaveOpen)
			{
				this.Input.Dispose();
			}
		}

		#endregion
	}
}
