﻿using System;
using Iridescence.Math;

namespace Iridescence.Audio
{
	/// <summary>
	/// Implements an audio source that reads from an array.
	/// </summary>
	public class MemorySignal<T> : ISignal<T>, ICompletableSignal
	{
		#region Fields

		private bool isComplete;
		private ReadOnlyMemory<T> memory;
		private int position;
		
		#endregion
		
		#region Properties

		public bool CanSeek => true;

		public long Position
		{
			get => this.position;
			set
			{
				if (value < 0 || value > this.memory.Length)
					throw new ArgumentOutOfRangeException(nameof(value));

				this.position = (int)value;
			}
		}

		public long Length => this.memory.Length;

		public int SampleRate { get; }


		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MemorySignal{T}"/> class. 
		/// </summary>
		public MemorySignal(ReadOnlyMemory<T> memory, int sampleRate)
		{
			this.memory = memory;

			if (sampleRate <= 0)
				throw new ArgumentOutOfRangeException(nameof(sampleRate));

			this.SampleRate = sampleRate;
		}
		
		#endregion
		
		#region Methods
		
		public int Read(Span<T> samples)
		{
			if (this.isComplete)
				return -1;

			if (samples.Length == 0)
				return 0;

			int n = Utility.Min(samples.Length, this.memory.Length - this.position);
			if (n == 0)
				return -1;

			this.memory.Span.Slice(this.position, n).CopyTo(samples);
			this.position += n;
			
			return n;
		}
		
		public void Complete()
		{
			this.isComplete = true;
		}

		public void Dispose()
		{
			this.isComplete = true;
			this.memory = null;
		}

		#endregion
	}
}
