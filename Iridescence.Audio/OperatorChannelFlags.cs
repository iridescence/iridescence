﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Flags that apply to individual member sources in a <see cref="ImmutableOperatorSignal{TIn,TOut}"/>.
	/// </summary>
	[Flags]
	public enum OperatorChannelFlags
	{
		None = 0,

		/// <summary>
		/// Remove the source when its <see cref="ISignal{T}.Read"/> method returns -1 (i.e. the source ended).
		/// </summary>
		RemoveWhenEnded = 0x01,

		/// <summary>
		/// Does the same as <see cref="RemoveWhenEnded"/> but additionally disposes the source after it was removed.
		/// </summary>
		DisposeWhenEnded = 0x02,

		Default = None,
	}
}