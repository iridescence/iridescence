﻿using System.Collections.Generic;

namespace Iridescence.Audio
{
	/// <summary>
	/// An interface for sources that process other sources. Useful for debugging purposes (e.g. displaying all sources as a graph).
	/// </summary>
	public interface ISignalProcessor
	{
		IEnumerable<ISignal> Sources { get; }
	}
}
