﻿using System;
using System.Threading;
using Iridescence.Math;

namespace Iridescence.Audio
{
	/// <summary>
	/// Implements a sink for an <see cref="ISignal{T}"/> that renders audio in regular intervals.
	/// </summary>
	public sealed class AudioSink<T> : IDisposable
	{
		#region Events

		public event EventHandler<ReadOnlyMemory<T>> SamplesRendered;

		#endregion

		#region Fields

		private readonly ISignal<T> source;
		private readonly CancellationTokenSource disposeTokenSource;
		private readonly CancellationToken cancellationToken;
		private readonly T[] buffer;
		private readonly Thread playbackThread;

		#endregion

		#region Properties

		public ISignal<T> Source => this.source;
	
		#endregion

		#region Constructors

		public AudioSink(ISignal<T> source, CancellationToken cancellationToken, int bufferSize = AudioConstants.DefaultBufferSize)
		{
			this.source = source;
			this.disposeTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
			this.cancellationToken = this.disposeTokenSource.Token;
			this.playbackThread = new Thread(this.playbackLoop);
			this.playbackThread.Name = "Audio Thread";
			this.playbackThread.Start();
			this.buffer = new T[bufferSize];
		}

		#endregion

		#region Methods

		public void Dispose()
		{
			this.disposeTokenSource.Cancel();
			this.playbackThread.Join();
		}

		private void playbackLoop()
		{
			const int minPacketSize = 1;

			DateTime clock = DateTime.Now;

			int accumulatedMod = 0;
			int sampleRate = this.source.SampleRate;

			while (!this.cancellationToken.IsCancellationRequested)
			{
				DateTime now = DateTime.Now;
				TimeSpan timePassed = now - clock;

				// Calculate number of samples since last iteration.
				int numSamples = (int)(timePassed.Ticks * sampleRate / TimeSpan.TicksPerSecond);
				if (numSamples < minPacketSize)
				{
					Thread.Sleep(1);
					continue;
				}

				long ticks = numSamples * TimeSpan.TicksPerSecond / sampleRate;

				// Account for error.
				int error = accumulatedMod / sampleRate;
				ticks += error;
				accumulatedMod -= error * sampleRate;

				// Move clock forward.
				clock += new TimeSpan(ticks);
				accumulatedMod += (int)(numSamples * TimeSpan.TicksPerSecond % sampleRate);

				int samplesReadTotal = 0;
				while (samplesReadTotal < numSamples)
				{
					int samplesToRead = Utility.Min(this.buffer.Length, numSamples - samplesReadTotal);
					int samplesRead = this.source.Read(new Span<T>(this.buffer, 0, samplesToRead));

					if (samplesRead < 0)
					{
						samplesReadTotal = -1;
						break;
					}

					if (samplesRead > 0)
					{
						samplesReadTotal += samplesRead;
						this.SamplesRendered?.Invoke(this, new ReadOnlyMemory<T>(this.buffer, 0, samplesRead));
					}

					if (samplesRead != samplesToRead)
						break;
				}

				if (samplesReadTotal < 0)
					break;
			}
		}

		#endregion
	}
}

