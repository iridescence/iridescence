﻿using System;
using System.IO;
using Iridescence.Audio.Flac.Prediction;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Implements an FIR predicition sub frame decoder.
	/// </summary>
	internal sealed class FIRPredictorDecoder : PredictorDecoder
	{
		#region Fields

		private readonly PredictorFactory factory;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new LPC sub frame decoder.
		/// </summary>
		public FIRPredictorDecoder(BitReader reader, PredictorFactory factory)
			: base(reader)
		{
			this.factory = factory;
		}

		#endregion

		#region Methods

		public override void Initialize()
		{
			// read warm-up samples.
			for (int i = 0; i < this.Order; i++)
				this.WarmupSamples[i] = this.Reader.ReadBitsSigned(this.BitsPerSample);

			// get coefficient precision.
			int precision = (int)this.Reader.ReadBits(4) + 1;
			if (precision == 16)
				throw new InvalidDataException("Invalid coefficient precision.");

			// read shift.
			int shift = this.Reader.ReadBitsSigned(5);

			// read coefficients.
			Span<int> coefficients = stackalloc int[this.Order];
			for (int i = 0; i < this.Order; i++)
				coefficients[i] = this.Reader.ReadBitsSigned(precision);

			// get predictor.
			FIRPredictor p = this.factory.GetFIRPredictor(this.Order, shift);
			p.SetCoefficients(coefficients);
			p.SetSamples(new ReadOnlySpan<int>(this.WarmupSamples, 0, this.Order));
			this.Predictor = p;

			base.Initialize();
		}

		#endregion
	}
}
