﻿using System;
using Iridescence.Audio.Flac.Prediction;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Implements a fixed prediction sub frame decoder.
	/// </summary>
	internal sealed class FixedPredictorDecoder : PredictorDecoder
	{
		#region Fields

		private readonly PredictorFactory factory;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new fixed sub frame decoder.
		/// </summary>
		///<param name="reader"></param> 
		/// <param name="predictors"></param>
		public FixedPredictorDecoder(BitReader reader, PredictorFactory factory)
			: base(reader)
		{
			this.factory = factory;
		}

		#endregion

		#region Methods

		public override void Initialize()
		{
			// read warm-up samples.
			for (int i = 0; i < this.Order; i++)
				this.WarmupSamples[i] = this.Reader.ReadBitsSigned(this.BitsPerSample);

			// get predictor.
			FIRPredictor p = this.factory.GetFixedPredictor(this.Order);
			p.SetSamples(new ReadOnlySpan<int>(this.WarmupSamples, 0, this.Order));
			this.Predictor = p;

			base.Initialize();
		}

		#endregion
	}
}
