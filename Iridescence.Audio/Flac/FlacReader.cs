﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Iridescence.Audio.Flac.Prediction;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Represents a FLAC stream reader/writer.
	/// </summary>
	public sealed class FlacReader
	{
		#region Nested Types

		private enum State
		{
			Invalid,

			StreamHeader,
			MetadataBlock,
			FrameHeader,
			SubFrameHeader,
			SubFrame,
			EndOfStream,
		}

		#endregion

		#region Fields

		private readonly Stream stream;
		private readonly BitReader reader;
		private State state;
		private StreamInfo streamInfo;
		private SeekPoint[] seekPoints;
		private FrameHeader currentFrame;
		private int currentSubFrame;
		private int currentSampleIndex;
		private SubFrameDecoder currentDecoder;
		private readonly ConstantDecoder constantDecoder;
		private readonly VerbatimDecoder verbatimDecoder;
		private readonly FixedPredictorDecoder fixedDecoder;
		private readonly FIRPredictorDecoder lpcDecoder;
		private int wastedBitsPerSample;
		private long firstFrameHeaderOffset;
		
		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new FLAC reader that operates on the specified stream.
		/// </summary>
		public FlacReader(Stream stream)
		{
			this.stream = stream;
			this.reader = new BitReader(this.stream);
			this.state = State.StreamHeader;

			PredictorFactory predictors = new PredictorFactory();
			this.constantDecoder = new ConstantDecoder(this.reader);
			this.verbatimDecoder = new VerbatimDecoder(this.reader);
			this.fixedDecoder = new FixedPredictorDecoder(this.reader, predictors);
			this.lpcDecoder = new FIRPredictorDecoder(this.reader, predictors);
			this.firstFrameHeaderOffset = -1;
		}

		#endregion

		#region Methods

		private bool ensureState(State state)
		{
			if (state == State.MetadataBlock && this.state > State.MetadataBlock)
				return false;
			
			while(this.state != state)
			{
				switch (this.state)
				{
					case State.Invalid:
						throw new InvalidDataException("The decoder encountered invalid data.");

					case State.StreamHeader:
						this.ReadFlacHeader();
						break;

					case State.MetadataBlock:
						this.NextMetadataBlock();
						break;

					case State.FrameHeader:
						this.NextFrame(out _);
						break;

					case State.SubFrameHeader:
						this.NextSubFrame();
						break;

					case State.SubFrame:
						Span<int> dummy = stackalloc int[256];
						this.ReadSubFrame(dummy);
						break;

					case State.EndOfStream:
						return state == State.EndOfStream;
				}
			}

			return true;
		}

		/// <summary>
		/// Reads the header of FLAC data embedded in an Ogg stream.
		/// </summary>
		public void ReadOggHeader()
		{
			Span<byte> header = stackalloc byte[9];
			this.reader.Read(header);

			if (header[0] != 0x7F ||
			    header[1] != 'F' ||
			    header[2] != 'L' ||
			    header[3] != 'A' ||
			    header[4] != 'C' ||
			    header[5] != 0x01 ||
			    header[6] != 0x00)
				throw new InvalidDataException("Invalid FLAC Ogg header.");
		}

		/// <summary>
		/// Reads the stream header.
		/// </summary>
		public void ReadFlacHeader()
		{
			bool success = false;
			try
			{
				if (!this.ensureState(State.StreamHeader))
					throw new InvalidOperationException("Stream header already read.");

				Span<byte> magic = stackalloc byte[4];
				this.reader.Read(magic);

				if (magic[0] != 'f' || magic[1] != 'L' || magic[2] != 'a' || magic[3] != 'C')
					throw new InvalidDataException("Invalid magic number.");

				this.state = State.MetadataBlock;
				success = true;
			}
			finally
			{
				if (!success)
					this.state = State.Invalid;
			}
		}

		/// <summary>
		/// Reads one metadata block.
		/// </summary>
		/// <returns>The decoded metadata block, or null if no more metadata blocks are available.</returns>
		public MetadataBlock NextMetadataBlock()
		{
			bool success = false;
			try
			{
				if (!this.ensureState(State.MetadataBlock))
				{
					success = true;
					return null;
				}

				int blockHeader = this.reader.ReadByte();

				Span<byte> length = stackalloc byte[3];
				this.reader.Read(length);

				byte[] metadata = new byte[(length[0] << 16) | (length[1] << 8) | length[2]];
				this.reader.Read(metadata);

				MetadataBlock block = MetadataBlockType.CreateMetadataBlock(blockHeader & 0x7F, metadata);

				if (this.streamInfo == null)
					this.streamInfo = block as StreamInfo;

				if (this.seekPoints == null && block is SeekTable table)
					this.seekPoints = table.Points;

				if ((blockHeader & 0x80) != 0)
				{
					if (this.streamInfo == null)
						throw new InvalidDataException("End of metadata blocks reached, but no StreamInfo metadata block has been read.");

					this.state = State.FrameHeader;
				}

				success = true;
				return block;
			}
			finally
			{
				if (!success)
					this.state = State.Invalid;
			}
		}

		private bool frameSync(Span<byte> buffer)
		{
			// look for bit sequence "11111111 111110xx".
			int input;
			while ((input = this.reader.ReadByte(false)) >= 0)
			{
				if (input == 0xFF)
				{
					input = this.reader.ReadByte(false);
					if (input < 0)
						return false;

					if (input >> 1 == 0x7C)
					{
						buffer[0] = 0xFF;
						buffer[1] = (byte)input;
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// Reads a frame header.
		/// </summary>
		/// <param name="frame">The frame header.</param>
		public bool NextFrame(out FrameHeader frame)
		{
			frame = default;

			bool success = false;
			try
			{
				Span<byte> buffer = stackalloc byte[24];

				if (!this.ensureState(State.FrameHeader) || !this.frameSync(buffer))
				{
					this.state = State.EndOfStream;
					success = true;
					return false;
				}

				if (this.firstFrameHeaderOffset < 0 && this.stream.CanSeek)
				{
					this.reader.GetPosition(out this.firstFrameHeaderOffset, out _);

					this.firstFrameHeaderOffset -= 2; // Subtract the two sync bytes.
				}

				int bufferOffset = 1;
				frame.VariableBlockSize = (buffer[bufferOffset++] & 1) != 0;

				// Read the next two bytes.
				this.reader.Read(buffer.Slice(bufferOffset, 2));
				frame.SampleCount = buffer[bufferOffset] >> 4;
				frame.SampleRate = buffer[bufferOffset++] & 0xF;
				frame.ChannelAssignment = buffer[bufferOffset] >> 4;
				frame.BitsPerSample = (buffer[bufferOffset] >> 1) & 0x7;

				// reserved, must be 0.
				if ((buffer[bufferOffset++] & 1) != 0)
					throw new InvalidDataException("Invalid frame header.");

				// frame number, encoded in the UTF-8 variable length coding.
				frame.Number = this.readUTF8(buffer, ref bufferOffset, frame.VariableBlockSize ? 7 : 6);

				// Get block size.
				if (frame.SampleCount == 0)
					throw new InvalidDataException("Invalid block size.");

				if (frame.SampleCount == 6)
				{
					this.reader.Read(buffer.Slice(bufferOffset, 1));
					frame.SampleCount = buffer[bufferOffset] + 1;
					bufferOffset++;
				}
				else if (frame.SampleCount == 7)
				{
					this.reader.Read(buffer.Slice(bufferOffset, 2));
					frame.SampleCount = ((buffer[bufferOffset] << 8) | buffer[bufferOffset + 1]) + 1;
					bufferOffset += 2;
				}
				else
				{
					frame.SampleCount = Constants.BlockSizes[frame.SampleCount];
				}

				// Get sample rate.
				if (frame.SampleRate == 0)
				{
					if (this.streamInfo == null)
						throw new InvalidDataException("Sample rate can't be determined because no StreamInfo metadata block has been read.");

					frame.SampleRate = this.streamInfo.SampleRate;
				}
				else if (frame.SampleRate == 12)
				{
					this.reader.Read(buffer.Slice(bufferOffset, 1));
					frame.SampleRate = buffer[bufferOffset];
					bufferOffset++;
				}
				else if (frame.SampleRate == 13 || frame.SampleRate == 14)
				{
					this.reader.Read(buffer.Slice(bufferOffset, 2));
					frame.SampleRate = ((buffer[bufferOffset] << 8) | buffer[bufferOffset + 1]) + 1;
					bufferOffset += 2;

					if (frame.SampleRate == 14)
						frame.SampleRate *= 10;
				}
				else if (frame.SampleRate == 15)
				{
					throw new InvalidDataException("Invalid sample rate.");
				}
				else
				{
					frame.SampleRate = Constants.SampleRates[frame.SampleRate];
				}

				// Validate channel assignment.
				if (frame.ChannelAssignment > 10)
					throw new InvalidDataException("Invalid channel assignment.");

				// Gets bits per sample.
				if (frame.BitsPerSample == 0)
				{
					if (this.streamInfo == null)
						throw new InvalidDataException("Sample rate can't be determined because no StreamInfo metadata block has been read.");

					frame.BitsPerSample = this.streamInfo.BitsPerSample;
				}
				else if (frame.BitsPerSample == 3 || frame.BitsPerSample == 7)
				{
					throw new InvalidDataException("Invalid sample size.");
				}
				else
				{
					frame.BitsPerSample = Constants.SampleSizes[frame.BitsPerSample];
				}

				// Check header CRC-8.
				int crc8 = this.reader.ReadByte();
				if (Crc.Crc8(0, buffer.Slice(0, bufferOffset)) != crc8)
					throw new InvalidDataException("Invalid CRC-8 in frame header.");

				// Store current frame.
				this.currentFrame = frame;

				// Add CRC-8 byte to buffer in order to calculate CRC-16 of the header.
				buffer[bufferOffset++] = (byte)crc8;
				//this.stream.Crc16 = Crc.Crc16(0, buffer.Slice(0, bufferOffset));

				this.state = State.SubFrameHeader;
				this.currentSubFrame = 0;

				// Align the bit decoder.
				this.reader.Align();

				success = true;
				return true;
			}
			finally
			{
				if (!success)
					this.state = State.Invalid;
			}
		}

		/// <summary>
		/// Reads a sub-frame header and assigns the sample enumeration.
		/// Should be called after <see cref="NextFrame(out FrameHeader)"/>.
		/// </summary>
		public void NextSubFrame()
		{
			bool success = false;
			try
			{
				if (!this.ensureState(State.SubFrameHeader))
					throw new InvalidOperationException("Can't read sub frame header");

				// padding bit.
				if (this.reader.ReadBit() != 0)
					throw new InvalidDataException("Invalid sub frame header.");

				// frame type.
				int type = (int)this.reader.ReadBits(6);

				// 'wasted bits-per-sample' flag.
				if (this.reader.ReadBit() != 0)
					this.wastedBitsPerSample = this.reader.ReadUnary() + 1;
				else
					this.wastedBitsPerSample = 0;

				// difference channel has 1 bit more.
				int bitsPerSample;
				if ((this.currentFrame.ChannelAssignment == 8 && this.currentSubFrame == 1) ||
				    (this.currentFrame.ChannelAssignment == 9 && this.currentSubFrame == 0) ||
				    (this.currentFrame.ChannelAssignment == 10 && this.currentSubFrame == 1))
					bitsPerSample = this.currentFrame.BitsPerSample + 1;
				else
					bitsPerSample = this.currentFrame.BitsPerSample;

				// read the sub frame contents.
				if (type == 0)
				{
					this.currentDecoder = this.constantDecoder;
				}
				else if (type == 1)
				{
					this.currentDecoder = this.verbatimDecoder;
				}
				else if (type >= 8 && type <= 12)
				{
					this.fixedDecoder.Order = type & 7;
					this.currentDecoder = this.fixedDecoder;
				}
				else if (type >= 32)
				{
					this.lpcDecoder.Order = (type & 31) + 1;
					this.currentDecoder = this.lpcDecoder;
				}
				else
				{
					throw new InvalidDataException("Invalid or unsupported sub frame type.");
				}

				this.currentDecoder.BlockSize = this.currentFrame.SampleCount;
				this.currentDecoder.BitsPerSample = bitsPerSample - this.wastedBitsPerSample;
				this.currentDecoder.Initialize();

				this.state = State.SubFrame;
				this.currentSampleIndex = 0;
				success = true;
			}
			finally
			{
				if (!success)
					this.state = State.Invalid;
			}
		}

		/// <summary>
		/// Reads the specified number of samples from the current SubFrame into the buffer.
		/// Should be called after <see cref="NextSubFrame"/>.
		/// </summary>
		/// <param name="buffer"></param>
		/// <returns>The number of samples retreived from the current SubFrame. Returns 0 if the current SubFrame has been read to the end. In this case, <see cref="NextSubFrame"/> should be called.</returns>
		public int ReadSubFrame(Span<int> buffer)
		{
			if (this.state == State.Invalid)
				throw new InvalidDataException("The decoder encountered invalid data.");

			if (this.state != State.SubFrame)
				return 0;
			
			int count = this.currentDecoder.Decode(buffer);

			this.currentSampleIndex += count;

			if (this.currentSampleIndex == this.currentFrame.SampleCount)
			{
				// End of sub frame.
				this.currentDecoder = null;
				++this.currentSubFrame;
				this.currentSampleIndex = 0;

				// Did we read all sub frames of the current frame?
				if (this.currentSubFrame >= this.currentFrame.Channels)
				{
					// Read frame footer.
					// Back up current CRC-16.
					//int streamCrc16 = this.stream.Crc16;

					// Read CRC-16 from stream.
					Span<byte> footer = stackalloc byte[2];
					this.reader.Read(footer);
					//int crc16 = (footer[0] << 8) | footer[1];

					//if (crc16 != streamCrc16)
					//{
					//	//throw new InvalidDataException("Invalid CRC-16 in frame footer.");
					//}
					
					this.state = State.FrameHeader;
				}
				else
				{
					this.state = State.SubFrameHeader;
				}
			}
			else if (this.wastedBitsPerSample > 0)
			{
				// shift samples.
				for (int i = 0; i < count; i++)
					buffer[i] <<= this.wastedBitsPerSample;
			}

			return count;
		}

		/// <summary>
		/// Seeks to the specified sample position and returns the new position in the stream (in samples).
		/// After this, a new frame should be read using <see cref="NextFrame(out FrameHeader)"/>.
		/// </summary>
		/// <param name="samplePos">The requested sample position.</param>
		/// <returns>The closest matching sample position.</returns>
		public long Seek(long samplePos)
		{
			if (this.firstFrameHeaderOffset < 0)
				throw new InvalidOperationException("Can not seek because the first frame offset is not known.");

			SeekPoint point = this.seekPoints?.LastOrDefault(p => p.Sample <= samplePos) ?? default;
			this.reader.Reset();
			this.stream.Position = this.firstFrameHeaderOffset + point.Offset;

			this.state = State.FrameHeader;
			return point.Sample;
		}

		/// <summary>
		/// Reads the samples of all sub frames in the current frame.
		/// Should be called after <see cref="NextFrame(out FrameHeader)"/>.
		/// </summary>
		public void ReadFrame(IReadOnlyList<Memory<int>> samples)
		{
			if (this.state != State.SubFrameHeader || this.currentSubFrame != 0 || this.currentSampleIndex != 0)
				throw new InvalidOperationException($"The decoder is not at the beginning of frame data. Please call {nameof(this.NextFrame)} first.");

			if (samples.Count < this.currentFrame.Channels)
				throw new ArgumentException($"Sample array needs to have room for {this.currentFrame.Channels} channels.", nameof(samples));

			for (int channel = 0; channel < this.currentFrame.Channels; ++channel)
			{
				if (samples[channel].Length < this.currentFrame.SampleCount)
					throw new ArgumentException($"Sample buffer for channel {channel} is too small.");

				// Read sub frame header.
				this.NextSubFrame();

				// ... and contents.
				if (this.ReadSubFrame(samples[channel].Span) != this.currentFrame.SampleCount)
					throw new InvalidOperationException("Some samples of the current frame have already been decoded.");
			}

			if (this.currentFrame.Channels >= 2)
			{
				// Undo inter-channel decorrelation.
				if (this.currentFrame.ChannelAssignment == 8)
					InterChannel.LeftSide(samples[0].Span, samples[1].Span, this.currentFrame.SampleCount);
				else if (this.currentFrame.ChannelAssignment == 9)
					InterChannel.RightSide(samples[0].Span, samples[1].Span, this.currentFrame.SampleCount);
				else if (this.currentFrame.ChannelAssignment == 10)
					InterChannel.MidSide(samples[0].Span, samples[1].Span, this.currentFrame.SampleCount);
			}
		}

		private long readUTF8(Span<byte> buffer, ref int offset, int length)
		{
			int b = this.reader.ReadByte();

			buffer[offset++] = (byte)b;

			if (b < 0x80)
				return b;
			
			if (b >= 0xC0)
			{
				int mask = 0x20;
				int numBytes = 1;
				while (mask > 0 && (b & mask) != 0)
				{
					numBytes++;
					mask >>= 1;
				}

				if (mask == 0)
					throw new InvalidDataException("Invalid UTF-8 data.");

				if (numBytes + 1 > length)
					throw new InvalidDataException("UTF-8 number too long.");

				long value = (b & (mask - 1));
				
				for (int i = 0; i < numBytes; i++)
				{
					b = this.reader.ReadByte();

					if ((b & 0xC0) != 0x80)
						throw new InvalidDataException("Invalid UTF-8 data.");

					value = (value << 6) | (b & 0x3Fu);
					buffer[offset++] = (byte)b;
				}

				return value;
			}

			throw new InvalidDataException("Invalid UTF-8 data.");
		}

		#endregion
	}
}
