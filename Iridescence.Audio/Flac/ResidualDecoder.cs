﻿using System;
using System.IO;
using Iridescence.Math;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Residual decoder.
	/// </summary>
	internal sealed class ResidualDecoder
	{
		#region Fields

		private readonly BitReader reader;

		private int blockSize;
		private int skipSamples;

		private int riceSize;
		private int riceMax;
		private int riceParameter; 
		private int bitsPerSample;
		private int sampleIndex;
		private int sampleCount;

		#endregion

		#region Properties

		public int BlockSize
		{
			get => this.blockSize;
			set => this.blockSize = value;
		}

		/// <summary>
		/// Gets or sets the number of samples to skip at the beginning.
		/// </summary>
		public int SkipSamples
		{
			get => this.skipSamples;
			set => this.skipSamples = value;
		}

		#endregion

		#region Constructor

		/// <summary>
		/// Creates a new residual decoder.
		/// </summary>
		/// <param name="reader"></param>
		public ResidualDecoder(BitReader reader)
		{
			this.reader = reader;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the residual decoder.
		/// </summary>
		public void Initialize()
		{
			// read coding method.
			uint method = this.reader.ReadBits(2);

			if (method == 0)
				this.riceSize = 4;
			else if (method == 1)
				this.riceSize = 5;
			else
				throw new InvalidDataException("Invalid residual coding method.");

			this.riceMax = (1 << this.riceSize) - 1;

			// read partition order.
			int partitionOrder = (int)this.reader.ReadBits(4);

			// number of samples in a partition.
			this.sampleCount = this.blockSize >> partitionOrder;
			if (this.sampleCount < this.skipSamples || this.sampleCount < 1 || (this.sampleCount << partitionOrder) != this.blockSize)
				throw new InvalidDataException("Invalid partition order.");

			// we have to skip the warm-up samples (the number of warm-up samples equals the order of the predictor).
			this.sampleIndex = this.skipSamples;

			// read header of first partition.
			this.readPartitionHeader();
		}

		private void readPartitionHeader()
		{
			// read rice parameter. 
			// if this is equal to riceMax, read the number of bits per sample.
			this.riceParameter = (int)this.reader.ReadBits(this.riceSize);
			if (this.riceParameter == this.riceMax)
				this.bitsPerSample = (int)this.reader.ReadBits(this.riceSize);
			else
				this.bitsPerSample = -1;
		}

		private void decodeRice(Span<int> buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = this.reader.ReadRice(this.riceParameter);
			}
		}

		private void decodeBits(Span<int> buffer)
		{
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = this.reader.ReadBitsSigned(this.bitsPerSample);
			}
		}

		public void Decode(Span<int> buffer)
		{
			while (buffer.Length > 0)
			{
				// did we reach the end of the partition?
				if (this.sampleIndex >= this.sampleCount)
				{
					// read new partition header.
					this.readPartitionHeader();
					this.sampleIndex = 0;
				}

				int numSamples = Utility.Min(buffer.Length, this.sampleCount - this.sampleIndex);

				// decode residual.
				if (this.bitsPerSample < 0)
					this.decodeRice(buffer.Slice(0, numSamples));
				else
					this.decodeBits(buffer.Slice(0, numSamples));

				this.sampleIndex += numSamples;

				buffer = buffer.Slice(numSamples);
			}
		}

		#endregion
	}
}
