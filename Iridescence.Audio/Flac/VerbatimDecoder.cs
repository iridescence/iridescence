﻿using System;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Implements a verbatim sub frame which stores all samples uncompressed.
	/// </summary>
	internal sealed class VerbatimDecoder : SubFrameDecoder
	{
		#region Fields

		private int sampleIndex;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new verbatim sub frame decoder.
		/// </summary>
		/// <param name="reader"></param>
		public VerbatimDecoder(BitReader reader)
			: base(reader)
		{
			
		}

		#endregion

		#region Methods

		public override void Initialize()
		{
			this.sampleIndex = 0;
		}

		public override int Decode(Span<int> buffer)
		{
			if (buffer.Length > this.BlockSize - this.sampleIndex)
				buffer = buffer.Slice(0, this.BlockSize - this.sampleIndex);

			this.sampleIndex += buffer.Length; 

			for (int i = 0; i < buffer.Length; i++)
				buffer[i] = this.Reader.ReadBitsSigned(this.BitsPerSample);

			return buffer.Length;
		}
	
		#endregion
	}
}
