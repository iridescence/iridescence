﻿using System.IO;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Represents a FLAC frame header.
	/// </summary>
	public struct FrameHeader
	{
		#region Fields

		/// <summary>
		/// The blocking strategy.
		/// False means fixed block size. The "Number" field encodes the frame number.
		/// True means variable block size. The "Number" field encodes the sample number.
		/// </summary>
		public bool VariableBlockSize { get; internal set; }

		/// <summary>
		/// The number of samples in the frame.
		/// </summary>
		public int SampleCount { get; internal set; }

		/// <summary>
		/// The sample rate in Hz.
		/// </summary>
		public int SampleRate { get; internal set; }

		/// <summary>
		/// The channel assignment.
		/// </summary>
		public int ChannelAssignment { get; internal set; }

		/// <summary>
		/// The number of bits per sample.
		/// </summary>
		public int BitsPerSample { get; internal set; }

		/// <summary>
		/// Represents either the frame number or the sample number, depending on the "VariableBlockSize" field.
		/// </summary>
		public long Number { get; internal set; }

		#endregion

		#region Properties

		/// <summary>
		/// The number of channels (and thus sub frame count).
		/// </summary>
		public int Channels => Constants.ChannelCounts[this.ChannelAssignment];

		#endregion

		#region Methods

		public override string ToString()
		{
			string channels;
			if (this.ChannelAssignment == 0)
				channels = "mono";
			else if (this.ChannelAssignment == 1)
				channels = "stereo";
			else if (this.ChannelAssignment < 8)
				channels = $"{this.ChannelAssignment + 1} channels";
			else if (this.ChannelAssignment == 8)
				channels = "left/side stereo";
			else if (this.ChannelAssignment == 9)
				channels = "right/side stereo";
			else if (this.ChannelAssignment == 10)
				channels = "mid/side stereo";
			else
				throw new InvalidDataException("Invalid channel assignment.");

			return $"#{this.Number}: {this.SampleCount} {this.BitsPerSample}-bit samples @{this.SampleRate}Hz ({channels})";
		}

		#endregion
	}
}
