﻿using System;
using System.IO;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Represents a stream info metadata block.
	/// </summary>
	public sealed class StreamInfo : MetadataBlock
	{
		#region Fields

		private int minimumBlockSize;
		private int maximumBlockSize;
		private int minimumFrameSize;
		private int maximumFrameSize;
		private int sampleRate;
		private int channels;
		private int bitsPerSample;
		private long samples;
		private byte[] hash;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the minimum block size (in samples) used in the stream.  
		/// </summary>
		public int MinimumBlockSize
		{
			get => this.minimumBlockSize;
			set
			{
				if ((value >> 16) != 0)
					throw new ArgumentException("Value must lie within the range 0 to 65535 (inclusive).");
				
				this.minimumBlockSize = value;
			}
		}

		/// <summary>
		/// Gets the maximum block size (in samples) used in the stream. 
		/// (Minimum blocksize == maximum blocksize) implies a fixed-blocksize stream. 
		/// </summary>
		public int MaximumBlockSize
		{
			get => this.maximumBlockSize;
			set
			{
				if ((value >> 16) != 0)
					throw new ArgumentException("Value must lie within the range 0 to 65535 (inclusive).");

				this.maximumBlockSize = value;
			}
		}

		/// <summary>
		/// Gets the minimum frame size (in bytes) used in the stream.
		/// May be 0 to imply the value is not known. 
		/// </summary>
		public int MinimumFrameSize
		{
			get => this.minimumFrameSize;
			set
			{
				if ((value >> 24) != 0)
					throw new ArgumentException("Value must lie within the range 0 to 16777215 (inclusive).");

				this.minimumFrameSize = value;
			}
		}

		/// <summary>
		/// Gets the maximum frame size (in bytes) used in the stream.
		/// May be 0 to imply the value is not known. 
		/// </summary>
		public int MaximumFrameSize
		{
			get => this.maximumFrameSize;
			set
			{
				if ((value >> 24) != 0)
					throw new ArgumentException("Value must lie within the range 0 to 16777215 (inclusive).");
				
				this.maximumFrameSize = value;
			}
		}

		/// <summary>
		/// Gets the sample rate in Hz. 
		/// Though 20 bits are available, the maximum sample rate is limited by the structure of frame headers to 655350Hz. Also, a value of 0 is invalid. 
		/// </summary>
		public int SampleRate
		{
			get => this.sampleRate;
			set
			{
				if (value < 0 || value > 655350)
					throw new ArgumentException("Value must lie within the range 1 to 655350 (inclusive).");

				this.sampleRate = value;
			}
		}

		/// <summary>
		/// Gets the number of channels.
		/// FLAC supports from 1 to 8 channels 
		/// </summary>
		public int Channels
		{
			get => this.channels;
			set
			{
				if (value < 1 || value > 8)
					throw new ArgumentException("Value must lie within the range 1 to 8 (inclusive).");
				
				this.channels = value;
			}
		}

		/// <summary>
		/// Gets the number of bits per sample.
		/// FLAC supports from 4 to 32 bits per sample. Currently the reference encoder and decoders only support up to 24 bits per sample.
		/// </summary>
		public int BitsPerSample
		{
			get => this.bitsPerSample;
			set
			{
				if (value < 4 || value > 32)
					throw new ArgumentException("Value must lie within the range 4 to 32 (inclusive).");

				this.bitsPerSample = value;
			}
		}

		/// <summary>
		/// Gets the total number of samples in stream.
		/// 'Samples' means inter-channel sample, i.e. one second of 44.1Khz audio will have 44100 samples regardless of the number of channels. A value of zero here means the number of total samples is unknown.
		/// </summary>
		public long Samples
		{
			get => this.samples;
			set
			{
				if ((value >> 36) != 0)
					throw new ArgumentException("Value must lie within the range 0 to 68719476735 (2^36 - 1) (inclusive).");
				this.samples = value;
			}
		}

		/// <summary>
		/// Gets the MD5 signature of the unencoded audio data.
		/// This allows the decoder to determine if an error exists in the audio data even when the error does not result in an invalid bitstream. 
		/// </summary>
		public byte[] Hash
		{
			get => this.hash;
			set
			{
				if (value.Length != 16)
					throw new ArgumentException("Value must be a 128-bit MD5 hash.");
				this.hash = value;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new stream info metadata block.
		/// </summary>
		public StreamInfo(byte[] data)
		{
			if (data.Length < 34)
				throw new InvalidDataException();

			this.minimumBlockSize = (data[0] << 8) | data[1];
			this.maximumBlockSize = (data[2] << 8) | data[3];
			this.minimumFrameSize = (data[4] << 16) | (data[5] << 8) | data[6];
			this.maximumFrameSize = (data[7] << 16) | (data[8] << 8) | data[9];

			ulong temp = ((ulong)data[10] << 56) | ((ulong)data[11] << 48) | ((ulong)data[12] << 40) | ((ulong)data[13] << 32) | ((ulong)data[14] << 24) | ((ulong)data[15] << 16) | ((ulong)data[16] << 8) | data[17];
			
			this.sampleRate = (int)(temp >> 44);
			this.channels = (int)((temp >> 41) & 0x7) + 1;
			this.bitsPerSample = (int)((temp >> 36) & 0x1F) + 1;
			this.samples = (long)(temp & 0xFFFFFFFFFul);

			this.hash = new byte[16];
			Array.Copy(data, 18, this.hash, 0, 16);
		}

		#endregion

		#region Methods

		public override byte[] GetData()
		{
			byte[] buffer = new byte[34];

			buffer[0] = (byte)(this.minimumBlockSize >> 8);
			buffer[1] = (byte)this.minimumBlockSize;

			buffer[2] = (byte)(this.maximumBlockSize >> 8);
			buffer[3] = (byte)this.maximumBlockSize;

			buffer[4] = (byte)(this.minimumFrameSize >> 16);
			buffer[5] = (byte)(this.minimumFrameSize >> 8);
			buffer[6] = (byte)this.minimumFrameSize;

			buffer[7] = (byte)(this.maximumFrameSize >> 16);
			buffer[8] = (byte)(this.maximumFrameSize >> 8);
			buffer[9] = (byte)this.maximumFrameSize;

			ulong temp = ((ulong)this.sampleRate << 44) | ((ulong)(this.channels - 1) << 41) | ((ulong)(this.bitsPerSample - 1) << 36) | (ulong)this.samples;
			buffer[10] = (byte)(temp >> 56);
			buffer[11] = (byte)(temp >> 48);
			buffer[12] = (byte)(temp >> 40);
			buffer[13] = (byte)(temp >> 32);
			buffer[14] = (byte)(temp >> 24);
			buffer[15] = (byte)(temp >> 16);
			buffer[16] = (byte)(temp >> 8);
			buffer[17] = (byte)temp;

			Array.Copy(this.hash, 0, buffer, 18, 16);
			return buffer;
		}

		#endregion
	}
}
