﻿using System;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Represents an applcation metadata block
	/// </summary>
	public sealed class Application : MetadataBlock
	{
		#region Fields

		private readonly uint id;
		private readonly byte[] data;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new application metadata block.
		/// </summary>
		public Application(byte[] data)
		{
			this.id = ((uint)data[0] << 24) | ((uint)data[1] << 16) | ((uint)data[2] << 8) | data[3];
			this.data = new byte[data.Length - 4];
			Array.Copy(data, 4, this.data, 0, this.data.Length);
		}

		#endregion

		#region Methods

		public override byte[] GetData()
		{
			byte[] buffer = new byte[this.data.Length + 4];
			unchecked
			{
				buffer[0] = (byte)(this.id >> 24);
				buffer[1] = (byte)(this.id >> 16);
				buffer[2] = (byte)(this.id >> 8);
				buffer[3] = (byte)(this.id);
			}
			Array.Copy(this.data, 0, buffer, 4, this.data.Length);
			return buffer;
		}

		#endregion
	}
}
