﻿using System;
using System.IO;
using System.Text;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Represents a vorbis comments metadata block.
	/// </summary>
	public sealed class Comment : MetadataBlock
	{
		#region Fields

		private static readonly Encoding encoding = new UTF8Encoding(false);

		private string vendor;
		private string[] comments;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the vendor string.
		/// </summary>
		public string Vendor
		{
			get => this.vendor;
			set => this.vendor = value;
		}

		/// <summary>
		/// Gets or sets an array of comments.
		/// </summary>
		public string[] Comments
		{
			get => this.comments;
			set => this.comments = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new vorbis comment metadata block from the specified data.
		/// </summary>
		public Comment(byte[] data)
		{
			int length = data[0] | (data[1] << 8) | (data[1] << 16) | (data[1] << 24);
			this.vendor = encoding.GetString(data, 4, length);

			int offset = 4 + length;
			int numComments = data[offset++] | (data[offset++] << 8) | (data[offset++] << 16) | (data[offset++] << 24);

			this.comments = new string[numComments];
			for (int i = 0; i < numComments; i++)
			{
				length = data[offset++] | (data[offset++] << 8) | (data[offset++] << 16) | (data[offset++] << 24);
				this.comments[i] = encoding.GetString(data, offset, length);
				offset += length;
			}
		}

		#endregion

		#region Methods

		public override byte[] GetData()
		{
			MemoryStream stream = new MemoryStream();

			stream.WriteByte((byte)this.vendor.Length);
			stream.WriteByte((byte)(this.vendor.Length >> 8));
			stream.WriteByte((byte)(this.vendor.Length >> 16));
			stream.WriteByte((byte)(this.vendor.Length >> 24));

			byte[] buffer = encoding.GetBytes(this.vendor);
			stream.Write(buffer, 0, buffer.Length);

			stream.WriteByte((byte)this.comments.Length);
			stream.WriteByte((byte)(this.comments.Length >> 8));
			stream.WriteByte((byte)(this.comments.Length >> 16));
			stream.WriteByte((byte)(this.comments.Length >> 24));

			for (int i = 0; i < this.comments.Length; i++)
			{
				stream.WriteByte((byte)this.comments[i].Length);
				stream.WriteByte((byte)(this.comments[i].Length >> 8));
				stream.WriteByte((byte)(this.comments[i].Length >> 16));
				stream.WriteByte((byte)(this.comments[i].Length >> 24));

				buffer = encoding.GetBytes(this.comments[i]);
				stream.Write(buffer, 0, buffer.Length);
			}

			buffer = stream.GetBuffer();
			Array.Resize(ref buffer, (int)stream.Length);
			return buffer;
		}

		#endregion
	}
}
