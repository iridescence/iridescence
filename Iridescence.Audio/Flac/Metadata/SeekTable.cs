﻿using System.IO;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Represents a seek table metadata block.
	/// </summary>
	public sealed class SeekTable : MetadataBlock
	{
		#region Fields

		private SeekPoint[] table;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the array of seek points.
		/// </summary>
		public SeekPoint[] Points
		{
			get => this.table;
			set => this.table = value;
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new SeekTable.
		/// </summary>
		public SeekTable(byte[] data)
		{
			int numPoints = data.Length / 18;
			this.table = new SeekPoint[numPoints];

			using (MemoryStream s = new MemoryStream(data, false))
			{
				for (int i = 0; i < this.table.Length; ++i)
				{
					this.table[i].Sample = s.ReadInt64B();
					this.table[i].Offset = s.ReadInt64B();
					this.table[i].SampleCount = s.ReadUInt16B();
				}
			}
		}

		#endregion

		#region Methods

		public override byte[] GetData()
		{
			byte[] buffer = new byte[this.table.Length * 18];

			using (MemoryStream s = new MemoryStream(buffer))
			{
				for (int i = 0; i < this.table.Length; ++i)
				{
					s.WriteInt64B(this.table[i].Sample);
					s.WriteInt64B(this.table[i].Offset);
					s.WriteUInt16B(this.table[i].SampleCount);
				}
			}

			return buffer;
		}

		#endregion
	}
}
