﻿using System;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Represents an abstract FLAC sub frame coding.
	/// </summary>
	internal abstract class SubFrameDecoder
	{
		#region Fields

		protected readonly BitReader Reader;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the block size.
		/// </summary>
		public int BlockSize { get; set; }

		/// <summary>
		/// Gets or sets the number of bits per sample.
		/// </summary>
		public int BitsPerSample { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new sub frame decoder that reads from the specified bit reader.
		/// </summary>
		/// <param name="reader">The bit reader to read from.</param>
		protected SubFrameDecoder(BitReader reader)
		{
			this.Reader = reader;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the sub frame by reading from the bit reader.
		/// </summary>
		public abstract void Initialize();

		/// <summary>
		/// Decodes samples.
		/// </summary>
		/// <param name="buffer">The buffer to store the samples in.</param>
		/// <returns>The number of decoded samples.</returns>
		public abstract int Decode(Span<int> buffer);

		#endregion
	}
}
