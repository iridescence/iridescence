﻿using System;
using Iridescence.Audio.Flac.Prediction;
using Iridescence.Math;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Implements a sub frame decoder that uses prediction and residual.
	/// </summary>
	internal abstract class PredictorDecoder : SubFrameDecoder
	{
		#region Fields

		private readonly ResidualDecoder residual;

		private int order;

		private int sampleIndex;

		protected readonly int[] WarmupSamples;
		protected Predictor Predictor;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the order of the decoder.
		/// </summary>
		public int Order
		{
			get => this.order;
			set => this.order = value;
		}

		#endregion

		#region Constructors

		protected PredictorDecoder(BitReader reader)
			: base(reader)
		{
			this.residual = new ResidualDecoder(reader);
			this.WarmupSamples = new int[32];
		}

		#endregion

		#region Methods

		public override void Initialize()
		{
			this.sampleIndex = 0;

			// initialize residual decoder.
			this.residual.BlockSize = this.BlockSize;
			this.residual.SkipSamples = this.order;
			this.residual.Initialize();
		}

		public override int Decode(Span<int> buffer)
		{
			if (buffer.Length > this.BlockSize - this.sampleIndex)
				buffer = buffer.Slice(0, this.BlockSize - this.sampleIndex);

			int count = buffer.Length;

			if (this.sampleIndex < this.Order)
			{
				int n = Utility.Min(buffer.Length, this.Order - this.sampleIndex);

				new Span<int>(this.WarmupSamples, this.sampleIndex, n).CopyTo(buffer.Slice(0, n));
				buffer = buffer.Slice(n);

				this.sampleIndex += n;
			}

			this.residual.Decode(buffer);
			this.Predictor.Predict(buffer);

			this.sampleIndex += buffer.Length;
			
			return count;
		}

		#endregion
	}
}
