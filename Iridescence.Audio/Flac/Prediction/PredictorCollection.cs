﻿using Iridescence.Math;
using System;

namespace Iridescence.Audio.Flac.Prediction
{
	/// <summary>
	/// A cache of predictors.
	/// </summary>
	internal sealed class PredictorFactory
	{
		#region Fields

		private readonly FIRPredictor[] fixedPredictors;
		private readonly FIRPredictor[] noShift;
		private readonly FIRPredictor[] leftShift;
		private readonly FIRPredictor[] rightShift;

		#endregion

		#region Properties

		#endregion

		#region Constructors

		public PredictorFactory()
		{
			this.fixedPredictors = new FIRPredictor[5];
			this.fixedPredictors[0] = new FIRPredictor0();
			this.fixedPredictors[1] = new FixedPredictor1();
			this.fixedPredictors[2] = new FixedPredictor2();
			this.fixedPredictors[3] = new FixedPredictor3();
			this.fixedPredictors[4] = new FixedPredictor4();

			this.noShift = new FIRPredictor[12];
			this.noShift[0] = new FIRPredictor1N();
			this.noShift[1] = new FIRPredictor2N();
			this.noShift[2] = new FIRPredictor3N();
			this.noShift[3] = new FIRPredictor4N();
			this.noShift[4] = new FIRPredictor5N();
			this.noShift[5] = new FIRPredictor6N();
			this.noShift[6] = new FIRPredictor7N();
			this.noShift[7] = new FIRPredictor8N();
			this.noShift[8] = new FIRPredictor9N();
			this.noShift[9] = new FIRPredictor10N();
			this.noShift[10] = new FIRPredictor11N();
			this.noShift[11] = new FIRPredictor12N();

			this.leftShift = new FIRPredictor[12];
			this.leftShift[0] = new FIRPredictor1L();
			this.leftShift[1] = new FIRPredictor2L();
			this.leftShift[2] = new FIRPredictor3L();
			this.leftShift[3] = new FIRPredictor4L();
			this.leftShift[4] = new FIRPredictor5L();
			this.leftShift[5] = new FIRPredictor6L();
			this.leftShift[6] = new FIRPredictor7L();
			this.leftShift[7] = new FIRPredictor8L();
			this.leftShift[8] = new FIRPredictor9L();
			this.leftShift[9] = new FIRPredictor10L();
			this.leftShift[10] = new FIRPredictor11L();
			this.leftShift[11] = new FIRPredictor12L();

			this.rightShift = new FIRPredictor[12];
			this.rightShift[0] = new FIRPredictor1R();
			this.rightShift[1] = new FIRPredictor2R();
			this.rightShift[2] = new FIRPredictor3R();
			this.rightShift[3] = new FIRPredictor4R();
			this.rightShift[4] = new FIRPredictor5R();
			this.rightShift[5] = new FIRPredictor6R();
			this.rightShift[6] = new FIRPredictor7R();
			this.rightShift[7] = new FIRPredictor8R();
			this.rightShift[8] = new FIRPredictor9R();
			this.rightShift[9] = new FIRPredictor10R();
			this.rightShift[10] = new FIRPredictor11R();
			this.rightShift[11] = new FIRPredictor12R();			
		}

		#endregion

		#region Methods

		public FIRPredictor GetFixedPredictor(int order)
		{
			return this.fixedPredictors[order];
		}

		public FIRPredictor GetFIRPredictor(int order, int shift)
		{
			if (order == 0)
				throw new ArgumentException("Order must be greater than 0.");

			FIRPredictor[] array;
			if(shift > 0)
				array = this.rightShift;
			else if (shift < 0)
				array = this.leftShift;
			else
				array = this.noShift;

			int index = order - 1;
			if (index >= array.Length)
				return new FIRPredictorN(order, shift);

			FIRPredictor predictor = array[index];

			if (shift != 0)
				((ShiftedFIRPredictor)predictor).Shift = Utility.Absolute(shift);

			return predictor;
		}

		#endregion
	}
}
