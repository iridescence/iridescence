﻿using System;

namespace Iridescence.Audio.Flac.Prediction
{
	/// <summary>
	/// Represents an abstract predictor.
	/// </summary>
	internal abstract class Predictor
	{
		#region Methods

		/// <summary>
		/// Predicts the next sample.
		/// </summary>
		/// <param name="buffer">The buffer to add to.</param>
		/// <returns></returns>
		public abstract void Predict(Span<int> buffer);

		#endregion
	}
}
