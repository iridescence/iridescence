﻿using System;

namespace Iridescence.Audio.Flac.Prediction
{
	/// <summary>
	/// LPC null predictor.
	/// </summary>
	internal sealed class FIRPredictor0 : FIRPredictor
	{
		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{

		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{

		}

		public override void Predict(Span<int> buffer)
		{
				
		}

		#endregion
	}
}
