﻿using System;

namespace Iridescence.Audio.Flac.Prediction
{
	/// <summary>
	/// Represents an abstract FIR linear predictor.
	/// </summary>
	internal abstract class FIRPredictor : Predictor
	{
		#region Methods

		/// <summary>
		/// Sets the coefficients of the predictor.
		/// </summary>
		/// <param name="coefficients">The coefficient array. The length of the array must be equal to the order of the predictor.</param>
		public abstract void SetCoefficients(ReadOnlySpan<int> coefficients);

		/// <summary>
		/// Sets the sample history of the predictor.
		/// </summary>
		/// <param name="samples">The samples. The length of the array must be equal to the order of the predictor minus one.</param>
		public abstract void SetSamples(ReadOnlySpan<int> samples);

		#endregion
	}
}
