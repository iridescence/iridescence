﻿using System;

namespace Iridescence.Audio.Flac.Prediction
{
	internal sealed class FixedPredictor1 : FIRPredictor
	{
		#region Fields

		private int sample0;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			throw new InvalidOperationException("Predictor has fixed coefficients.");
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample0 = samples[0];
		}

		public override void Predict(Span<int> buffer)
		{
			int s0 = this.sample0;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
		}

		#endregion
	}

	internal sealed class FixedPredictor2 : FIRPredictor
	{
		#region Fields

		private int sample0;
		private int sample1;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			throw new InvalidOperationException("Predictor has fixed coefficients.");
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample1 = samples[0];
			this.sample0 = samples[1];
		}

		public override void Predict(Span<int> buffer)
		{
			int s0 = this.sample0;
			int s1 = this.sample1;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = -s1;
				s1 = s0;
				next += 2 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
		}

		#endregion
	}

	internal sealed class FixedPredictor3 : FIRPredictor
	{
		#region Fields

		private int sample0;
		private int sample1;
		private int sample2;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			throw new InvalidOperationException("Predictor has fixed coefficients.");
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample2 = samples[0];
			this.sample1 = samples[1];
			this.sample0 = samples[2];
		}

		public override void Predict(Span<int> buffer)
		{
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = s2;
				s2 = s1;
				next += -3 * s1;
				s1 = s0;
				next += 3 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
		}

		#endregion
	}

	internal sealed class FixedPredictor4 : FIRPredictor
	{
		#region Fields

		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			throw new InvalidOperationException("Predictor has fixed coefficients.");
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample3 = samples[0];
			this.sample2 = samples[1];
			this.sample1 = samples[2];
			this.sample0 = samples[3];
		}

		public override void Predict(Span<int> buffer)
		{
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = -s3;
				s3 = s2;
				next += 4 * s2;
				s2 = s1;
				next += -6 * s1;
				s1 = s0;
				next += 4 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
		}

		#endregion
	}
}
