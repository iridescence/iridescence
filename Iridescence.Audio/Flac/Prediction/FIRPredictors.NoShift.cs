using System;

namespace Iridescence.Audio.Flac.Prediction
{
	internal sealed class FIRPredictor1N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int sample0;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample0 = samples[0];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int s0 = this.sample0;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
		}

		#endregion
	}

	internal sealed class FIRPredictor2N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int sample0;
		private int sample1;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample1 = samples[0];
			this.sample0 = samples[1];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int s0 = this.sample0;
			int s1 = this.sample1;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
		}

		#endregion
	}

	internal sealed class FIRPredictor3N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int sample0;
		private int sample1;
		private int sample2;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample2 = samples[0];
			this.sample1 = samples[1];
			this.sample0 = samples[2];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
		}

		#endregion
	}

	internal sealed class FIRPredictor4N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample3 = samples[0];
			this.sample2 = samples[1];
			this.sample1 = samples[2];
			this.sample0 = samples[3];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int c3 = this.coefficient3;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c3 * s3;
				s3 = s2;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
		}

		#endregion
	}

	internal sealed class FIRPredictor5N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample4 = samples[0];
			this.sample3 = samples[1];
			this.sample2 = samples[2];
			this.sample1 = samples[3];
			this.sample0 = samples[4];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int c3 = this.coefficient3;
			int c4 = this.coefficient4;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			int s4 = this.sample4;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c4 * s4;
				s4 = s3;
				next += c3 * s3;
				s3 = s2;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
			this.sample4 = s4;
		}

		#endregion
	}

	internal sealed class FIRPredictor6N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int coefficient5;
		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;
		private int sample5;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
			this.coefficient5 = coefficients[5];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample5 = samples[0];
			this.sample4 = samples[1];
			this.sample3 = samples[2];
			this.sample2 = samples[3];
			this.sample1 = samples[4];
			this.sample0 = samples[5];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int c3 = this.coefficient3;
			int c4 = this.coefficient4;
			int c5 = this.coefficient5;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			int s4 = this.sample4;
			int s5 = this.sample5;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c5 * s5;
				s5 = s4;
				next += c4 * s4;
				s4 = s3;
				next += c3 * s3;
				s3 = s2;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
			this.sample4 = s4;
			this.sample5 = s5;
		}

		#endregion
	}

	internal sealed class FIRPredictor7N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int coefficient5;
		private int coefficient6;
		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;
		private int sample5;
		private int sample6;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
			this.coefficient5 = coefficients[5];
			this.coefficient6 = coefficients[6];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample6 = samples[0];
			this.sample5 = samples[1];
			this.sample4 = samples[2];
			this.sample3 = samples[3];
			this.sample2 = samples[4];
			this.sample1 = samples[5];
			this.sample0 = samples[6];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int c3 = this.coefficient3;
			int c4 = this.coefficient4;
			int c5 = this.coefficient5;
			int c6 = this.coefficient6;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			int s4 = this.sample4;
			int s5 = this.sample5;
			int s6 = this.sample6;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c6 * s6;
				s6 = s5;
				next += c5 * s5;
				s5 = s4;
				next += c4 * s4;
				s4 = s3;
				next += c3 * s3;
				s3 = s2;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
			this.sample4 = s4;
			this.sample5 = s5;
			this.sample6 = s6;
		}

		#endregion
	}

	internal sealed class FIRPredictor8N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int coefficient5;
		private int coefficient6;
		private int coefficient7;
		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;
		private int sample5;
		private int sample6;
		private int sample7;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
			this.coefficient5 = coefficients[5];
			this.coefficient6 = coefficients[6];
			this.coefficient7 = coefficients[7];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample7 = samples[0];
			this.sample6 = samples[1];
			this.sample5 = samples[2];
			this.sample4 = samples[3];
			this.sample3 = samples[4];
			this.sample2 = samples[5];
			this.sample1 = samples[6];
			this.sample0 = samples[7];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int c3 = this.coefficient3;
			int c4 = this.coefficient4;
			int c5 = this.coefficient5;
			int c6 = this.coefficient6;
			int c7 = this.coefficient7;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			int s4 = this.sample4;
			int s5 = this.sample5;
			int s6 = this.sample6;
			int s7 = this.sample7;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c7 * s7;
				s7 = s6;
				next += c6 * s6;
				s6 = s5;
				next += c5 * s5;
				s5 = s4;
				next += c4 * s4;
				s4 = s3;
				next += c3 * s3;
				s3 = s2;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
			this.sample4 = s4;
			this.sample5 = s5;
			this.sample6 = s6;
			this.sample7 = s7;
		}

		#endregion
	}

	internal sealed class FIRPredictor9N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int coefficient5;
		private int coefficient6;
		private int coefficient7;
		private int coefficient8;
		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;
		private int sample5;
		private int sample6;
		private int sample7;
		private int sample8;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
			this.coefficient5 = coefficients[5];
			this.coefficient6 = coefficients[6];
			this.coefficient7 = coefficients[7];
			this.coefficient8 = coefficients[8];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample8 = samples[0];
			this.sample7 = samples[1];
			this.sample6 = samples[2];
			this.sample5 = samples[3];
			this.sample4 = samples[4];
			this.sample3 = samples[5];
			this.sample2 = samples[6];
			this.sample1 = samples[7];
			this.sample0 = samples[8];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int c3 = this.coefficient3;
			int c4 = this.coefficient4;
			int c5 = this.coefficient5;
			int c6 = this.coefficient6;
			int c7 = this.coefficient7;
			int c8 = this.coefficient8;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			int s4 = this.sample4;
			int s5 = this.sample5;
			int s6 = this.sample6;
			int s7 = this.sample7;
			int s8 = this.sample8;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c8 * s8;
				s8 = s7;
				next += c7 * s7;
				s7 = s6;
				next += c6 * s6;
				s6 = s5;
				next += c5 * s5;
				s5 = s4;
				next += c4 * s4;
				s4 = s3;
				next += c3 * s3;
				s3 = s2;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
			this.sample4 = s4;
			this.sample5 = s5;
			this.sample6 = s6;
			this.sample7 = s7;
			this.sample8 = s8;
		}

		#endregion
	}

	internal sealed class FIRPredictor10N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int coefficient5;
		private int coefficient6;
		private int coefficient7;
		private int coefficient8;
		private int coefficient9;
		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;
		private int sample5;
		private int sample6;
		private int sample7;
		private int sample8;
		private int sample9;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
			this.coefficient5 = coefficients[5];
			this.coefficient6 = coefficients[6];
			this.coefficient7 = coefficients[7];
			this.coefficient8 = coefficients[8];
			this.coefficient9 = coefficients[9];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample9 = samples[0];
			this.sample8 = samples[1];
			this.sample7 = samples[2];
			this.sample6 = samples[3];
			this.sample5 = samples[4];
			this.sample4 = samples[5];
			this.sample3 = samples[6];
			this.sample2 = samples[7];
			this.sample1 = samples[8];
			this.sample0 = samples[9];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int c3 = this.coefficient3;
			int c4 = this.coefficient4;
			int c5 = this.coefficient5;
			int c6 = this.coefficient6;
			int c7 = this.coefficient7;
			int c8 = this.coefficient8;
			int c9 = this.coefficient9;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			int s4 = this.sample4;
			int s5 = this.sample5;
			int s6 = this.sample6;
			int s7 = this.sample7;
			int s8 = this.sample8;
			int s9 = this.sample9;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c9 * s9;
				s9 = s8;
				next += c8 * s8;
				s8 = s7;
				next += c7 * s7;
				s7 = s6;
				next += c6 * s6;
				s6 = s5;
				next += c5 * s5;
				s5 = s4;
				next += c4 * s4;
				s4 = s3;
				next += c3 * s3;
				s3 = s2;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
			this.sample4 = s4;
			this.sample5 = s5;
			this.sample6 = s6;
			this.sample7 = s7;
			this.sample8 = s8;
			this.sample9 = s9;
		}

		#endregion
	}

	internal sealed class FIRPredictor11N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int coefficient5;
		private int coefficient6;
		private int coefficient7;
		private int coefficient8;
		private int coefficient9;
		private int coefficient10;
		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;
		private int sample5;
		private int sample6;
		private int sample7;
		private int sample8;
		private int sample9;
		private int sample10;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
			this.coefficient5 = coefficients[5];
			this.coefficient6 = coefficients[6];
			this.coefficient7 = coefficients[7];
			this.coefficient8 = coefficients[8];
			this.coefficient9 = coefficients[9];
			this.coefficient10 = coefficients[10];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample10 = samples[0];
			this.sample9 = samples[1];
			this.sample8 = samples[2];
			this.sample7 = samples[3];
			this.sample6 = samples[4];
			this.sample5 = samples[5];
			this.sample4 = samples[6];
			this.sample3 = samples[7];
			this.sample2 = samples[8];
			this.sample1 = samples[9];
			this.sample0 = samples[10];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int c3 = this.coefficient3;
			int c4 = this.coefficient4;
			int c5 = this.coefficient5;
			int c6 = this.coefficient6;
			int c7 = this.coefficient7;
			int c8 = this.coefficient8;
			int c9 = this.coefficient9;
			int c10 = this.coefficient10;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			int s4 = this.sample4;
			int s5 = this.sample5;
			int s6 = this.sample6;
			int s7 = this.sample7;
			int s8 = this.sample8;
			int s9 = this.sample9;
			int s10 = this.sample10;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c10 * s10;
				s10 = s9;
				next += c9 * s9;
				s9 = s8;
				next += c8 * s8;
				s8 = s7;
				next += c7 * s7;
				s7 = s6;
				next += c6 * s6;
				s6 = s5;
				next += c5 * s5;
				s5 = s4;
				next += c4 * s4;
				s4 = s3;
				next += c3 * s3;
				s3 = s2;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
			this.sample4 = s4;
			this.sample5 = s5;
			this.sample6 = s6;
			this.sample7 = s7;
			this.sample8 = s8;
			this.sample9 = s9;
			this.sample10 = s10;
		}

		#endregion
	}

	internal sealed class FIRPredictor12N : FIRPredictor
	{
		#region Fields

		private int coefficient0;
		private int coefficient1;
		private int coefficient2;
		private int coefficient3;
		private int coefficient4;
		private int coefficient5;
		private int coefficient6;
		private int coefficient7;
		private int coefficient8;
		private int coefficient9;
		private int coefficient10;
		private int coefficient11;
		private int sample0;
		private int sample1;
		private int sample2;
		private int sample3;
		private int sample4;
		private int sample5;
		private int sample6;
		private int sample7;
		private int sample8;
		private int sample9;
		private int sample10;
		private int sample11;

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> coefficients)
		{
			this.coefficient0 = coefficients[0];
			this.coefficient1 = coefficients[1];
			this.coefficient2 = coefficients[2];
			this.coefficient3 = coefficients[3];
			this.coefficient4 = coefficients[4];
			this.coefficient5 = coefficients[5];
			this.coefficient6 = coefficients[6];
			this.coefficient7 = coefficients[7];
			this.coefficient8 = coefficients[8];
			this.coefficient9 = coefficients[9];
			this.coefficient10 = coefficients[10];
			this.coefficient11 = coefficients[11];
		}

		public override void SetSamples(ReadOnlySpan<int> samples)
		{
			this.sample11 = samples[0];
			this.sample10 = samples[1];
			this.sample9 = samples[2];
			this.sample8 = samples[3];
			this.sample7 = samples[4];
			this.sample6 = samples[5];
			this.sample5 = samples[6];
			this.sample4 = samples[7];
			this.sample3 = samples[8];
			this.sample2 = samples[9];
			this.sample1 = samples[10];
			this.sample0 = samples[11];
		}

		public override void Predict(Span<int> buffer)
		{
			int c0 = this.coefficient0;
			int c1 = this.coefficient1;
			int c2 = this.coefficient2;
			int c3 = this.coefficient3;
			int c4 = this.coefficient4;
			int c5 = this.coefficient5;
			int c6 = this.coefficient6;
			int c7 = this.coefficient7;
			int c8 = this.coefficient8;
			int c9 = this.coefficient9;
			int c10 = this.coefficient10;
			int c11 = this.coefficient11;
			int s0 = this.sample0;
			int s1 = this.sample1;
			int s2 = this.sample2;
			int s3 = this.sample3;
			int s4 = this.sample4;
			int s5 = this.sample5;
			int s6 = this.sample6;
			int s7 = this.sample7;
			int s8 = this.sample8;
			int s9 = this.sample9;
			int s10 = this.sample10;
			int s11 = this.sample11;
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;
				next += c11 * s11;
				s11 = s10;
				next += c10 * s10;
				s10 = s9;
				next += c9 * s9;
				s9 = s8;
				next += c8 * s8;
				s8 = s7;
				next += c7 * s7;
				s7 = s6;
				next += c6 * s6;
				s6 = s5;
				next += c5 * s5;
				s5 = s4;
				next += c4 * s4;
				s4 = s3;
				next += c3 * s3;
				s3 = s2;
				next += c2 * s2;
				s2 = s1;
				next += c1 * s1;
				s1 = s0;
				next += c0 * s0;
				s0 = buffer[i] += next;
			}
			this.sample0 = s0;
			this.sample1 = s1;
			this.sample2 = s2;
			this.sample3 = s3;
			this.sample4 = s4;
			this.sample5 = s5;
			this.sample6 = s6;
			this.sample7 = s7;
			this.sample8 = s8;
			this.sample9 = s9;
			this.sample10 = s10;
			this.sample11 = s11;
		}

		#endregion
	}

}
