﻿namespace Iridescence.Audio.Flac.Prediction
{
	/// <summary>
	/// Represents a predictor that shifts it's values.
	/// </summary>
	internal abstract class ShiftedFIRPredictor : FIRPredictor
	{
		/// <summary>
		/// Gets or sets how many bits the signal is shifted.
		/// </summary>
		public int Shift { get; set; }
	}
}
