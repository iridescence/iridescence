﻿using System;

namespace Iridescence.Audio.Flac.Prediction
{
	/// <summary>
	/// LPC n-order predictor.
	/// </summary>
	internal sealed class FIRPredictorN : FIRPredictor
	{
		#region Fields

		private readonly int order;
		private readonly int shift;
		private readonly int[] coefficients;
		private readonly int[] samples;

		#endregion

		#region Constructors

		public FIRPredictorN(int order, int shift)
		{
			this.shift = shift;
			this.order = order;
			this.coefficients = new int[this.order];
			this.samples = new int[this.order];
		}

		#endregion

		#region Methods

		public override void SetCoefficients(ReadOnlySpan<int> newCoefficients)
		{
			newCoefficients.CopyTo(this.coefficients);
		}

		public override void SetSamples(ReadOnlySpan<int> newSamples)
		{
			newSamples.CopyTo(this.samples);
		}

		public override void Predict(Span<int> buffer)
		{
			for (int i = 0; i < buffer.Length; ++i)
			{
				int next = 0;

				for (int j = 0; j < this.order; ++j)
				{
					next += this.coefficients[this.order - 1 - j] * this.samples[j];

					if (j < this.order - 1)
						this.samples[j] = this.samples[j + 1];
				}

				if (this.shift > 0)
					next = next >> this.shift;
				else if (this.shift < 0)
					next = next << (-this.shift);

				this.samples[this.order - 1] = buffer[i] += next;
			}
		}

		#endregion
	}
}
