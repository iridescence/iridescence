﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using Iridescence.Math;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Implements the FLAC bit stream reader.
	/// </summary>
	internal sealed class BitReader
	{
		#region Fields

		private readonly Stream stream;

		private readonly byte[] buffer;
		private int availableBits;
		private int bufferOffset;
		private int bufferLength;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new bit decoder that reads from the specified stream.
		/// </summary>
		/// <param name="stream">The base stream.</param>
		public BitReader(Stream stream, int bufferSize = 4096)
		{
			this.stream = stream;
			this.buffer = new byte[bufferSize];
			this.Align();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Gets the current byte position in the stream and the bit position within that byte.
		/// </summary>
		/// <param name="bytePosition"></param>
		/// <param name="bitPosition"></param>
		public void GetPosition(out long bytePosition, out int bitPosition)
		{
			bytePosition = this.stream.Position - this.bufferLength + this.bufferOffset;
			bitPosition = 7 - (this.availableBits & 0b111);
		}
	
		/// <summary>
		/// Resets the bit decoder.
		/// </summary>
		public void Reset()
		{
			this.availableBits = 0;
			this.bufferOffset = 0;
		}

		/// <summary>
		/// Invalidates the bit reader, forcing it to read a new byte on the next bit operation.
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Align()
		{
			int n = this.availableBits & 0b111;
			if (n != 0)
				this.ReadBits(n);
		}

		/// <summary>
		/// Fill the internal buffer by reading bytes from the underlying stream.
		/// After the method returns, at least minBits bits will be available for consumption. If not, an <see cref="EndOfStreamException"/> is thrown.
		/// </summary>
		/// <param name="minBits"></param>
		internal void FillBuffer(int minBits)
		{
			int usedBytes = (this.availableBits + 7) >> 3;
			if (this.bufferOffset > 0)
			{
				if (usedBytes > 0)
				{
					Array.Copy(this.buffer, this.bufferOffset, this.buffer, 0, usedBytes);
				}

				this.bufferLength -= this.bufferOffset;
				this.bufferOffset = 0;
			}
			
			int freeBytes = this.buffer.Length - usedBytes;

			if(freeBytes == 0)
				return;

			int n = this.stream.Read(this.buffer, usedBytes, freeBytes);

			this.availableBits += n * 8;
			this.bufferLength += n;
			if (this.availableBits < minBits)
				throw new EndOfStreamException();
		}

		/// <summary>
		/// Reads a single byte from the stream.
		/// The reader is aligned to byte-boundaries before reading 8 bits.
		/// </summary>
		/// <param name="throwOnEof"></param>
		/// <returns></returns>
		public int ReadByte(bool throwOnEof = true)
		{
			this.Align();

			if (this.availableBits < 8)
			{
				this.FillBuffer(0);

				if (this.availableBits < 8)
				{
					if (throwOnEof)
						throw new EndOfStreamException();

					return -1;
				}
			}

			this.availableBits -= 8;
			return this.buffer[this.bufferOffset++];
		}

		/// <summary>
		/// Reads multiple bytes from the stream.
		/// The reader is aligned to byte-boundaries before reading the bytes.
		/// </summary>
		/// <param name="dest"></param>
		/// <param name="throwOnEof"></param>
		/// <returns></returns>
		public int Read(Span<byte> dest, bool throwOnEof = true)
		{
			this.Align();

			int total = 0;
			while (dest.Length > 0)
			{
				if (this.availableBits < dest.Length * 8)
				{
					this.FillBuffer(0);
				}

				int n = Utility.Min(dest.Length, this.availableBits / 8);
				if (n == 0)
				{
					if (throwOnEof)
						throw new EndOfStreamException();

					break;
				}

				new Span<byte>(this.buffer, this.bufferOffset, n).CopyTo(dest.Slice(0, n));
				total += n;

				this.availableBits -= n * 8;
				this.bufferOffset += n;

				dest = dest.Slice(n);
			}

			return total;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private uint getBit()
		{
			int remainingBits = this.availableBits - 1;
			byte b = this.buffer[this.bufferOffset];
			uint bit = unchecked((uint)(b >> (remainingBits & 0b111))) & 1u;

			this.availableBits = remainingBits;
			this.bufferOffset += ((remainingBits ^ (remainingBits - 1)) >> 3) & 1;

			return bit;
		}

		/// <summary>
		/// Reads a single bit from the stream.
		/// </summary>
		/// <returns>0 or 1.</returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public uint ReadBit()
		{
			if (this.availableBits < 1)
			{
				this.FillBuffer(1);
			}

			return this.getBit();
		}

		/// <summary>
		/// Reads the specified number of bits from the stream.
		/// </summary>
		/// <param name="n">The number of bits. Must be less than or equal to 32.</param>
		/// <returns>The decoded value.</returns>
		public uint ReadBits(int n)
		{
			if (this.availableBits < n)
			{
				this.FillBuffer(n);
			}

			int avail = this.availableBits;
			int offset = this.bufferOffset;
			byte[] buf = this.buffer;
			uint val = 0;

			do
			{
				int bitsToCopy = avail - (((avail - 1) >> 3) << 3);
				if (bitsToCopy > n)
					bitsToCopy = n;

				int remainingBits = avail - bitsToCopy;
				byte b = buf[offset];
				uint bits = unchecked((uint)(b >> (remainingBits & 0b111))) & ((1u << bitsToCopy) - 1);

				avail = remainingBits;
				offset += ((remainingBits ^ (remainingBits - 1)) >> 3) & 1;

				val = (val << bitsToCopy) | bits;
				n -= bitsToCopy;
			} while (n > 0);

			this.availableBits = avail;
			this.bufferOffset = offset;

			return val;
		}

		/// <summary>
		/// Reads the specified number of bits from the stream, treating the first one as sign bit.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public int ReadBitsSigned(int n)
		{
			int shift = 32 - n;
			return unchecked((int)(this.ReadBits(n) << shift)) >> shift;
		}

		/// <summary>
		/// Reads a unary-encoded value (i.e. something like 0001 = 3).
		/// </summary>
		/// <returns></returns>
		public int ReadUnary()
		{
			int value = -1;
			do
			{
				if (this.availableBits < 1)
					this.FillBuffer(1);

				++value;
			} while (this.getBit() == 0);

			return value;
		}

		/// <summary>
		/// Reads a rice-encoded value.
		/// </summary>
		/// <param name="p">The rice parameter.</param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public int ReadRice(int p)
		{
			uint value;
			if (p == 0)
				value = (uint)this.ReadUnary();
			else
				value = ((uint)this.ReadUnary() << p) | this.ReadBits(p);

			return -(int)(value & 1) ^ (int)(value >> 1);
		}

		#endregion
	}
}
