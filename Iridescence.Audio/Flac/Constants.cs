﻿namespace Iridescence.Audio.Flac
{
	internal static class Constants
	{
		#region Fields

		public static readonly int[] BlockSizes = {0, 192, 576, 1152, 2304, 4608, 0, 0, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768};
		public static readonly int[] SampleRates = {0, 88200, 176400, 192000, 8000, 16000, 22050, 24000, 32000, 44100, 48000, 96000, 0, 0, 0};
		public static readonly int[] SampleSizes = {0, 8, 12, 0, 16, 20, 24};
		public static readonly int[] ChannelCounts = {1, 2, 3, 4, 5, 6, 7, 8, 2, 2, 2};

		#endregion
	}
}
