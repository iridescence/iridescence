﻿using System;

namespace Iridescence.Audio.Flac
{
	/// <summary>
	/// Implements a constant sub frame.
	/// </summary>
	internal sealed class ConstantDecoder : SubFrameDecoder
	{
		#region Fields

		private int sampleIndex;
		private int value;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new constant sub frame decoder.
		/// </summary>
		/// <param name="reader"></param>
		public ConstantDecoder(BitReader reader)
			: base(reader)
		{
			
		}

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the constant sub frame decoder.
		/// This reads the constant sample value from the reader.
		/// </summary>
		public override void Initialize()
		{
			this.sampleIndex = 0;
			
			// read the constant value from the stream.
			this.value = this.Reader.ReadBitsSigned(this.BitsPerSample);
		}

		public override int Decode(Span<int> buffer)
		{
			if (buffer.Length > this.BlockSize - this.sampleIndex)
				buffer = buffer.Slice(0, this.BlockSize - this.sampleIndex);

			this.sampleIndex += buffer.Length; 
			
			for (int i = 0; i < buffer.Length; i++)
				buffer[i] = this.value;

			return buffer.Length;
		}
	
		#endregion
	}
}
