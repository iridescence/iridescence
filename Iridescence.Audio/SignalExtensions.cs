﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Iridescence.Audio
{
	/// <summary>
	/// Extension methods for <see cref="ISignal"/>.
	/// </summary>
	public static class SignalExtensions
	{
		/// <summary>
		/// Returns the length of a signal as <see cref="TimeSpan"/>.
		/// </summary>
		/// <param name="signal"></param>
		/// <returns></returns>
		public static TimeSpan GetTime(this ISignal signal)
		{
			long length = signal.Length;
			int sampleRate = signal.SampleRate;
			return TimeSpan.FromTicks(length * TimeSpan.TicksPerSecond / sampleRate);
		}

		private sealed class Node
		{
			public readonly ISignal Signal;
			public readonly List<Node> Children;

			public Node(ISignal signal)
			{
				this.Signal = signal;
				this.Children = new List<Node>();
			}
		}

		private static Node getNode(ISignal signal, HashSet<ISignal> visited)
		{
			Node node = new Node(signal);

			if (signal is ISignalProcessor processor)
			{
				foreach (ISignal s in processor.Sources)
				{
					if (visited.Add(s))
					{
						node.Children.Add(getNode(s, visited));
					}
				}
			}

			return node;
		}

		private static void appendNode(StringBuilder str, Node node)
		{
			if (node.Children.Count > 0)
			{
				str.Append("(");
				str.Append(node.Signal);

				str.Append(" <= ");

				if (node.Children.Count > 1)
				{
					str.Append("[");
				}

				bool first = true;
				foreach (Node child in node.Children)
				{
					if (first)
						first = false;
					else
						str.Append(", ");

					appendNode(str, child);
				}

				if (node.Children.Count > 1)
				{
					str.Append("]");
				}

				str.Append(")");
			}
			else
			{
				str.Append(node.Signal);
			}
		}

		internal static string Visualize(this ISignal signal)
		{
			Node tree = getNode(signal, new HashSet<ISignal>());
			StringBuilder str = new StringBuilder();
			appendNode(str, tree);
			return str.ToString();
		}
	}
}
