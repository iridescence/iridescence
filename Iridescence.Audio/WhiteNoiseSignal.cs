﻿using System;

namespace Iridescence.Audio
{
	/// <summary>
	/// Implements an audio source that emits white noise when read.
	/// </summary>
	public class WhiteNoiseSignal : ISignal<float>, ICompletableSignal
	{
		#region Fields

		private readonly Random rng;
		private bool isCompleted;

		#endregion
		
		#region Properties

		public bool CanSeek => false;

		public long Position
		{
			get => throw new NotSupportedException("The audio source does not support seeking.");
			set => throw new NotSupportedException("The audio source does not support seeking.");
		}

		public long Length => throw new NotSupportedException("The audio source has no length because it is infinite.");

		public int SampleRate { get; }


		public float Amplitude { get; set; }

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="WhiteNoiseSignal"/> class. 
		/// </summary>
		public WhiteNoiseSignal(int sampleRate, Random rng = null)
		{
			this.SampleRate = sampleRate;
			this.rng = rng ?? new Random();
			this.Amplitude = 1.0f;
		}
		
		#endregion
		
		#region Methods
		
		public int Read(Span<float> samples)
		{
			if (this.isCompleted)
				return -1;

			for (int i = 0; i < samples.Length; ++i)
			{
				samples[i] = this.Amplitude * (float)(-1.0 + 2.0 * this.rng.NextDouble());
			}

			return samples.Length;
		}

		public void Complete()
		{
			this.isCompleted = true;
		}

		public void Dispose()
		{
			this.isCompleted = true;
		}
		
		#endregion
	}
}
