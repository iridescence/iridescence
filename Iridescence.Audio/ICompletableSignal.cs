﻿namespace Iridescence.Audio
{
	/// <summary>
	/// Represents an <see cref="ISignal"/> that can be completed (i.e. ended) by calling a the Complete method.
	/// This does not immediately dispose the signal and its underlying resources. Instead, it results in the signal returning -1 (EOF) on the next call to Read.
	/// </summary>
    public interface ICompletableSignal : ISignal
	{
		/// <summary>
		/// Causes all further calls to <see cref="ISignal{T}.Read(System.Span{T})"/> to return -1.
		/// </summary>
		void Complete();
	}
}
