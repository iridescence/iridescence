﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Iridescence.Input
{
	/// <summary>
	/// Represents an abstract keyboard input device.
	/// </summary>
	public abstract class Keyboard : IInputDevice
	{
		#region Events

		/// <summary>
		/// Occurs when a key is pressed down.
		/// </summary>
		public event EventHandler<KeyEventArgs> KeyDown;

		/// <summary>
		/// Occurs when a key is released.
		/// </summary>
		public event EventHandler<KeyEventArgs> KeyUp;

		#endregion

		#region Fields

		private readonly HashSet<Key> pressedKeys;
		
		#endregion

		#region Properties

		/// <summary>
		/// Gets a value that indicates whether the specified key is currently pressed.
		/// </summary>
		/// <param name="key"></param>
		/// <returns>True if the key is pressed. False otherwise.</returns>
		public bool this[Key key]
		{
			get { return this.pressedKeys.Contains(key); }
			protected set
			{
				if (value)
				{
					if (this.pressedKeys.Add(key))
					{
						this.OnKeyDown(new KeyEventArgs(key));
					}
				}
				else
				{
					if (this.pressedKeys.Remove(key))
					{
						this.OnKeyUp(new KeyEventArgs(key));
					}
				}
			}
		}

		/// <summary>
		/// Gets an enumeration of all currently pressed keys.
		/// </summary>
		public IEnumerable<Key> PressedKeys => this.pressedKeys;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new Keyboard.
		/// </summary>
		protected Keyboard()
		{
			this.pressedKeys = new HashSet<Key>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Updates the keyboard's state.
		/// </summary>
		public abstract void Update();

		/// <summary>
		/// Returns a value that indicates whether the specified key is pressed.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool IsPressed(Key key) => this.pressedKeys.Contains(key);

		/// <summary>
		/// Called when a key is pressed down.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnKeyDown(KeyEventArgs e)
		{
			this.KeyDown?.Invoke(this, e);
		}

		/// <summary>
		/// Called when a key is released.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnKeyUp(KeyEventArgs e)
		{
			this.KeyUp?.Invoke(this, e);
		}
		
		/// <summary>
		/// Converts a Key to a user friendly string.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public static string KeyButtonToUserFriendlyString(KeyCode key)
		{
			switch (key)
			{
				case var k when (int)k >= 110 && (int)k <= 119:
					return ((int)k - 110).ToString(CultureInfo.InvariantCulture);
			}

			return key.ToString();
		}

		#endregion
	}
}
