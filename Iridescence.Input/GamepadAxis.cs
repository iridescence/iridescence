﻿namespace Iridescence.Input
{
	/// <summary>
	/// Defines the analog <see cref="Gamepad"/> axes.
	/// </summary>
    public enum GamepadAxis
    {
	    /// <summary>
	    /// Unknown/unsupported axis.
	    /// </summary>
	    Unknown = 0,

		/// <summary>
		/// The x-axis of the left analog stick.
		/// </summary>
		LeftThumbX = 1,

	    /// <summary>
	    /// The y-axis of the left analog stick.
	    /// </summary>
	    LeftThumbY = 2,

	    /// <summary>
	    /// The x-axis of the right analog stick.
	    /// </summary>
	    RightThumbX = 3,

	    /// <summary>
	    /// The y-axis of the right analog stick.
	    /// </summary>
	    RightThumbY = 4,
		
	    /// <summary>
	    /// The axis of the left analog trigger.
	    /// </summary>
	    LeftTrigger = 5,

	    /// <summary>
	    /// The axis of the right analog trigger.
	    /// </summary>
	    RightTrigger = 6,
    }
}
