﻿using System;
using System.Collections.Generic;

namespace Iridescence.Input
{
	/// <summary>
	/// Represents an abstract mouse input device.
	/// </summary>
	public abstract class Mouse : IInputDevice
	{
		#region Events

		/// <summary>
		/// Occurs when the mouse is moved.
		/// </summary>
		public event EventHandler<MouseMoveEventArgs> Moved;

		/// <summary>
		/// Occurs when a button is pressed down.
		/// </summary>
		public event EventHandler<MouseButtonEventArgs> ButtonDown;

		/// <summary>
		/// Occurs when a button is released.
		/// </summary>
		public event EventHandler<MouseButtonEventArgs> ButtonUp;

		#endregion

		#region Fields

		private readonly HashSet<MouseButton> pressedButtons;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value that indicates whether the specified mouse button is currently pressed.
		/// </summary>
		/// <param name="button">The mouse button.</param>
		/// <returns>True if the button is pressed. False otherwise.</returns>
		public bool this[MouseButton button] 
		{
			get { return this.pressedButtons.Contains(button); }
			protected set
			{
				if (value)
				{
					if (this.pressedButtons.Add(button))
					{
						this.OnButtonDown(new MouseButtonEventArgs(button));
					}
				}
				else
				{
					if (this.pressedButtons.Remove(button))
					{
						this.OnButtonUp(new MouseButtonEventArgs(button));
					}
				}
			}
		}

		/// <summary>
		/// Gets an enumeration of all currently pressed buttons.
		/// </summary>
		public IEnumerable<MouseButton> PressedButtons => this.pressedButtons;

		#endregion

		#region Constructors

		protected Mouse()
		{
			this.pressedButtons = new HashSet<MouseButton>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Updates the state of the mouse.
		/// </summary>
		public abstract void Update();

		/// <summary>
		/// Returns a value that indicates whether the specified button is pressed.
		/// </summary>
		/// <param name="button"></param>
		/// <returns></returns>
		public bool IsPressed(MouseButton button) => this.pressedButtons.Contains(button);

		/// <summary>
		/// Moves the mouse.
		/// </summary>
		/// <param name="x">The delta of the mouse movement on the horizontal axis.</param>
		/// <param name="x">The delta of the mouse movement on the vertical axis.</param>
		/// <param name="x">The mouse wheel movement delta.</param>
		protected void Move(int x, int y, float wheelDelta)
		{
			this.OnMoved(new MouseMoveEventArgs(x, y, wheelDelta));
		}
		
		protected virtual void OnMoved(MouseMoveEventArgs e)
		{
			this.Moved?.Invoke(this, e);
		}

		protected virtual void OnButtonDown(MouseButtonEventArgs e)
		{
			this.ButtonDown?.Invoke(this, e);
		}

		protected virtual void OnButtonUp(MouseButtonEventArgs e)
		{
			this.ButtonUp?.Invoke(this, e);
		}
		
		/// <summary>
		/// Converts a MouseButton to a user friendly string.
		/// </summary>
		/// <param name="button"></param>
		/// <returns></returns>
		public static string MouseButtonToUserFriendlyString(MouseButton button)
		{
			switch (button)
			{
				case MouseButton.Button1:
					return "Left Mouse Button";
				case MouseButton.Button2:
					return "Right Mouse Button";
				case MouseButton.Button3:
					return "Middle Mouse Button";
				default:
					return $"Mouse Button {(uint)button}";
			}
		}

		#endregion
	}
}
