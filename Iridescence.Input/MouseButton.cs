﻿namespace Iridescence.Input
{
	/// <summary>
	/// Defines mouse buttons.
	/// </summary>
	public enum MouseButton : uint
	{
		/// <summary>
		/// No mouse button.
		/// </summary>
		None,

		/// <summary>
		/// The first mouse button (same as Left).
		/// </summary>
		Button1,
		
		/// <summary>
		/// The second mouse button (same as Right).
		/// </summary>
		Button2,
		
		/// <summary>
		/// The third mouse button (same as Middle).
		/// </summary>
		Button3,

		/// <summary>
		/// The fouth mouse button (i.e. the first extra button).
		/// </summary>
		Button4,

		/// <summary>
		/// The fifth mouse button (i.e. the second extra button).
		/// </summary>
		Button5,

		Button6,
		Button7,
		Button8,
		Button9,
		Button10,
		Button11,
		Button12,
		Button13,
		Button14,
		Button15,
		Button16,

		/// <summary>
		/// The left mouse button (same as Button1).
		/// </summary>
		Left = Button1,

		/// <summary>
		/// The right mouse button (same as Button2).
		/// </summary>
		Right = Button2,

		/// <summary>
		/// The middle mouse button (i.e. the wheel, same as Button3).
		/// </summary>
		Middle = Button3,
	}
}
