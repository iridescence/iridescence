﻿using System;

namespace Iridescence.Input
{
	/// <summary>
	/// Keyboard key event arguments.
	/// </summary>
	public class KeyEventArgs : EventArgs
	{
		#region Properties

		/// <summary>
		/// Gets the key.
		/// </summary>
		public Key Key { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new <see cref="KeyEventArgs"/>.
		/// </summary>
		public KeyEventArgs(Key key)
		{
			this.Key = key;
		}

		#endregion
	}
}
