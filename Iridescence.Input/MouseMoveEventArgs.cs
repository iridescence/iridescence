﻿using System;

namespace Iridescence.Input
{
	/// <summary>
	/// Mouse move event arguments.
	/// </summary>
	public class MouseMoveEventArgs : EventArgs
	{
		#region Properties

		/// <summary>
		/// Gets the X delta.
		/// </summary>
		public int X { get; }

		/// <summary>
		/// Gets the Y delta.
		/// </summary>
		public int Y { get; }

		/// <summary>
		/// Gets the wheel delta.
		/// </summary>
		public float Wheel { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new MouseMoveEventArgs.
		/// </summary>
		public MouseMoveEventArgs(int x, int y, float wheel)
		{
			this.X = x;
			this.Y = y;
			this.Wheel = wheel;
		}

		#endregion
	}
}
