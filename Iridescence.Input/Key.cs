﻿namespace Iridescence.Input
{
	/// <summary>
	/// Represents a key on a keyboard.
	/// </summary>
	public class Key
	{
		#region Fields

		#endregion

		#region Properties

		/// <summary>
		/// Gets a platform-indepdent key-code of the key.
		/// </summary>
		public KeyCode Code { get; }

		/// <summary>
		/// Gets a string representation of the key.
		/// </summary>
		public virtual string Name => this.Code.ToString();

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Key"/> class. 
		/// </summary>
		public Key(KeyCode code)
		{
			this.Code = code;
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			return this.Name;
		}

		#endregion
	}
}
