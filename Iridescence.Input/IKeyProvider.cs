﻿namespace Iridescence.Input
{
	/// <summary>
	/// Provides <see cref="Key"/> instances for <see cref="KeyCode">key codes</see>.
	/// </summary>
	public interface IKeyProvider
	{
		/// <summary>
		/// Retrieves the <see cref="Key"/> for the specified <see cref="KeyCode"/>.
		/// </summary>
		/// <param name="code"></param>
		/// <param name="key"></param>
		/// <returns>True, if the <see cref="Key"/> was retrieved successfully. False, if the <see cref="KeyCode"/> is not supported.</returns>
		bool TryGetKey(KeyCode code, out Key key);
	}
}
