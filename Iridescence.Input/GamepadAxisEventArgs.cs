﻿namespace Iridescence.Input
{
	/// <summary>
	/// Gamepad axis event args.
	/// </summary>
	public class GamepadAxisEventArgs : ValueChangedEventArgs<float>
	{
		#region Fields

		#endregion

		#region Properties
		
		/// <summary>
		/// Gets the new value.
		/// </summary>
		public float NewValue { get; }

		/// <summary>
		/// Gets the changed axis.
		/// </summary>
		public GamepadAxis Axis { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GamepadAxisEventArgs class. 
		/// </summary>
		public GamepadAxisEventArgs(float oldValue, float newValue, GamepadAxis axis) : base(oldValue)
		{
			this.NewValue = newValue;
			this.Axis = axis;
		}

		#endregion

		#region Methods

		#endregion
	}
}
