﻿namespace Iridescence.Input
{
	/// <summary>
	/// Defines the <see cref="Gamepad"/> buttons.
	/// </summary>
	public enum GamepadButton
	{
		/// <summary>
		/// Unknown/unsupported button.
		/// </summary>
		Unknown = 0,

		/// <summary>
		/// The first button ("A" on an Xbox controller).
		/// </summary>
		Button1,

		/// <summary>
		/// The second button ("B" on an Xbox controller).
		/// </summary>
		Button2,

		/// <summary>
		/// The third button ("X" on an Xbox controller).
		/// </summary>
		Button3,

		/// <summary>
		/// The fourth button ("Y" on an Xbox controller).
		/// </summary>
		Button4,

		/// <summary>
		/// The fifth button (left shoulder on an Xbox controller).
		/// </summary>
		Button5,
		
		/// <summary>
		/// The sixth button (right shoulder on an Xbox controller).
		/// </summary>
		Button6,

		/// <summary>
		/// The seventh button (pressing down the left thumb stick on an Xbox controller).
		/// </summary>
		Button7,

		/// <summary>
		/// The eighth button (pressing down the right thumb stick on an Xbox controller).
		/// </summary>
		Button8,

		/// <summary>
		/// The ninth button ("Start" on an Xbox controller).
		/// </summary>
		Button9,

		/// <summary>
		/// The tenth button ("Back" on an Xbox controller).
		/// </summary>
		Button10,

		/// <summary>
		/// The eleventh button (Big button in the middle on an Xbox controller).
		/// </summary>
		Button11,

		/// <summary>
		/// The twelfth button (DPad up).
		/// </summary>
		Button12,

		/// <summary>
		/// The thirteenth button (DPad down).
		/// </summary>
		Button13,

		/// <summary>
		/// The fourteenth button (DPad left).
		/// </summary>
		Button14,

		/// <summary>
		/// The fifteenth button (DPad right).
		/// </summary>
		Button15,

		/// <summary>
		/// The sixteenth button.
		/// </summary>
		Button16,

		/// <summary>
		/// The left shoulder button.
		/// Same as Button5
		/// </summary>
		LeftShoulder = Button5,

		/// <summary>
		/// The right shoulder button.
		/// Same as Button6
		/// </summary>
		RightShoulder = Button6,

		/// <summary>
		/// The left thumb stick (i.e. pressing down the left thumb stick).
		/// Same as Button7
		/// </summary>
		LeftThumb = Button7,

		/// <summary>
		/// The right thumb stick (i.e. pressing down the right thumb stick).
		/// Same as Button8
		/// </summary>
		RightThumb = Button8,
		
		/// <summary>
		/// The up button on the DPad.
		/// Same as Button12.
		/// </summary>
		DPadUp = Button12,

		/// <summary>
		/// The down button on the DPad.
		/// Same as Button13.
		/// </summary>
		DPadDown = Button13,

		/// <summary>
		/// The left button on the DPad.
		/// Same as Button14.
		/// </summary>
		DPadLeft = Button14,

		/// <summary>
		/// The right button on the DPad.
		/// Same as Button15.
		/// </summary>
		DPadRight = Button15,
	}
}
