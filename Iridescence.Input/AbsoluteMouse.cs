﻿using Iridescence.Math;

namespace Iridescence.Input
{
	/// <summary>
	/// Represents a <see cref="Mouse"/> with an absolute position (e.g. the mouse inside a window or on a screen).
	/// </summary>
	public abstract class AbsoluteMouse : Mouse
	{
		#region Events

		/// <summary>
		/// Occurs when Position was changed.
		/// </summary>
		public event ValueChangedEventHandler<Vector2> PositionChanged;

		#endregion

		#region Fields

		private Vector2 position;

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the absolute mouse position.
		/// </summary>
		public Vector2 Position
		{
			get { return this.position; }
			set
			{
				if (this.position != value)
				{
					ValueChangedEventArgs<Vector2> e = new ValueChangedEventArgs<Vector2>(this.position);
					this.position = value;
					this.OnPositionChanged(e);
				}
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="AbsoluteMouse"/> class. 
		/// </summary>
		protected AbsoluteMouse()
		{

		}

		#endregion

		#region Methods

		/// <summary>
		/// Called when Position was changed.
		/// </summary>
		/// <param name="e">Holds the old value of the property.</param>
		protected virtual void OnPositionChanged(ValueChangedEventArgs<Vector2> e)
		{
			this.PositionChanged?.Invoke(this, e);
		}

		#endregion
	}
}
