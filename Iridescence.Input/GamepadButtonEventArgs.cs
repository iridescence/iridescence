﻿using System;

namespace Iridescence.Input
{
	/// <summary>
	/// Gamepad button event args.
	/// </summary>
	public class GamepadButtonEventArgs : EventArgs
	{
		#region Fields

		#endregion

		#region Properties

		public GamepadButton Button { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GamepadButtonEventArgs class. 
		/// </summary>
		public GamepadButtonEventArgs(GamepadButton button)
		{
			this.Button = button;
		}

		#endregion

		#region Methods

		#endregion
	}
}
