﻿using System;
using System.Collections.Generic;
using Iridescence.Math;

namespace Iridescence.Input
{
	/// <summary>
	/// Represents an abstract gamepad (e.g. XBOX or PlayStation controller).
	/// </summary>
	public abstract class Gamepad : IInputDevice
	{
		#region Events

		/// <summary>
		/// Occurs when a button is pressed down.
		/// </summary>
		public event EventHandler<GamepadButtonEventArgs> ButtonDown;

		/// <summary>
		/// Occurs when a button is released.
		/// </summary>
		public event EventHandler<GamepadButtonEventArgs> ButtonUp;

		/// <summary>
		/// Occurs when a gamepad axis value has changed.
		/// </summary>
		public event EventHandler<GamepadAxisEventArgs> AxisChanged;

		#endregion

		#region Fields

		private readonly HashSet<GamepadButton> pressedButtons;

		private float leftAnalogStickX;
		private float leftAnalogStickY;
		private float rightAnalogStickX;
		private float rightAnalogStickY;
		private float leftTrigger;
		private float rightTrigger;

		#endregion

		#region Properties

		/// <summary>
		/// Gets a value that indicates whether this gamepad is available (e.g. connected and turned on).
		/// </summary>
		public bool IsAvailable { get; protected set; }

		/// <summary>
		/// Gets the current position of the left analog stick.
		/// </summary>
		/// <remarks>
		/// Both axes are mapped to the interval [-1, 1].
		/// </remarks>
		public Vector2 LeftAnalogStick => (this.leftAnalogStickX, this.leftAnalogStickY);

		/// <summary>
		/// Gets the current position of the right analog stick.
		/// </summary>
		/// <remarks>
		/// Both axes are mapped to the interval [-1, 1].
		/// </remarks>
		public Vector2 RightAnalogStick => (this.rightAnalogStickX, this.rightAnalogStickY);
		
		/// <summary>
		/// Gets the current value of the x-axis of the right analog stick.
		/// </summary>
		/// <remarks>
		/// The value is mapped to the interval [-1, 1].
		/// </remarks>
		public float RightAnalogStickX
		{
			get => this.rightAnalogStickX;
			protected set
			{
				if (this.rightAnalogStickX != value)
				{
					GamepadAxisEventArgs e = new GamepadAxisEventArgs(this.rightAnalogStickX, value, GamepadAxis.RightThumbX);
					this.rightAnalogStickX = value;
					this.OnAxisChanged(e);
				}
			}
		}

		/// <summary>
		/// Gets the current value of the y-axis of the right analog stick.
		/// </summary>
		/// <remarks>
		/// The value is mapped to the interval [-1, 1].
		/// </remarks>
		public float RightAnalogStickY
		{
			get => this.rightAnalogStickY;
			protected set
			{
				if (this.rightAnalogStickY != value)
				{
					GamepadAxisEventArgs e = new GamepadAxisEventArgs(this.rightAnalogStickY, value, GamepadAxis.RightThumbY);
					this.rightAnalogStickY = value;
					this.OnAxisChanged(e);
				}
			}
		}

		/// <summary>
		/// Gets the current value of the x-axis of the left analog stick.
		/// </summary>
		/// <remarks>
		/// The value is mapped to the interval [-1, 1].
		/// </remarks>
		public float LeftAnalogStickX
		{
			get => this.leftAnalogStickX;
			protected set
			{
				if (this.leftAnalogStickX != value)
				{
					GamepadAxisEventArgs e = new GamepadAxisEventArgs(this.leftAnalogStickX, value, GamepadAxis.LeftThumbX);
					this.leftAnalogStickX = value;
					this.OnAxisChanged(e);
				}
			}
		}

		/// <summary>
		/// Gets the current value of the y-axis of the left analog stick.
		/// </summary>
		/// <remarks>
		/// The value is mapped to the interval [-1, 1].
		/// </remarks>
		public float LeftAnalogStickY
		{
			get => this.leftAnalogStickY;
			protected set
			{
				if (this.leftAnalogStickY != value)
				{
					GamepadAxisEventArgs e = new GamepadAxisEventArgs(this.leftAnalogStickY, value, GamepadAxis.LeftThumbY);
					this.leftAnalogStickY = value;
					this.OnAxisChanged(e);
				}
			}
		}

		/// <summary>
		/// Gets the current value of the left analog trigger.
		/// </summary>
		/// <remarks>
		/// The value is mapped to the interval [0, 1].
		/// </remarks>
		public float LeftTrigger
		{
			get => this.leftTrigger;
			protected set
			{
				if (this.leftTrigger != value)
				{
					GamepadAxisEventArgs e = new GamepadAxisEventArgs(this.leftTrigger, value, GamepadAxis.LeftTrigger);
					this.leftTrigger = value;
					this.OnAxisChanged(e);
				}
			}
		}

		/// <summary>
		/// Gets the current value of the left analog trigger.
		/// </summary>
		/// <remarks>
		/// The value is mapped to the interval [0, 1].
		/// </remarks>
		public float RightTrigger
		{
			get => this.rightTrigger;
			protected set
			{
				if (this.rightTrigger != value)
				{
					GamepadAxisEventArgs e = new GamepadAxisEventArgs(this.leftTrigger, value, GamepadAxis.RightTrigger);
					this.rightTrigger = value;
					this.OnAxisChanged(e);
				}
			}
		}

		/// <summary>
		/// Gets the state of the specified button.
		/// </summary>
		/// <param name="button">The gamepad button.</param>
		/// <returns>True if the button is pressed. False otherwise.</returns>
		public bool this[GamepadButton button]
		{
			get => this.pressedButtons.Contains(button);
			protected set
			{
				if (value)
				{
					if (this.pressedButtons.Add(button))
					{
						this.OnButtonDown(new GamepadButtonEventArgs(button));
					}
				}
				else
				{
					if (this.pressedButtons.Remove(button))
					{
						this.OnButtonUp(new GamepadButtonEventArgs(button));
					}
				}
			}
		}

		/// <summary>
		/// Gets or sets the current vibration power.
		/// </summary>
		/// <remarks>
		/// The value is mapped to the interval [0, 1].
		/// </remarks>
		public virtual float VibrationPower
		{
			get => 0f;
			set {}
		}

		/// <summary>
		/// Gets or sets the dead zone for the analog triggers.
		/// </summary>
		public float TriggerDeadZone { get; set; }

		/// <summary>
		/// Gets or sets the dead zone for the left analog stick.
		/// </summary>
		public float LeftAnalogStickDeadZone { get; set; }

		/// <summary>
		/// Gets or sets the dead zone for the right analog stick.
		/// </summary>
		public float RightAnalogStickDeadZone { get; set; }

		/// <summary>
		/// Gets an enumeration of all currently pressed buttons.
		/// </summary>
		public IEnumerable<GamepadButton> PressedButtons => this.pressedButtons;

		/// <summary>
		/// Gets a unique and persistent identifier for the device.
		/// </summary>
		public abstract string DeviceIdentifier { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the Gamepad class. 
		/// </summary>
		protected Gamepad()
		{
			this.pressedButtons = new HashSet<GamepadButton>();
		}

		#endregion

		#region Methods

		/// <summary>
		/// Sets the value of a <see cref="GamepadAxis"/>.
		/// </summary>
		/// <param name="axis">The axis to be set.</param>
		/// <param name="value">The new value for the axis. Must be within the interval [0, 1].</param>
		protected void SetAxis(GamepadAxis axis, float value)
		{
			switch (axis)
			{
				case GamepadAxis.Unknown:
					throw new ArgumentException("Unknown axis.", nameof(axis));
				case GamepadAxis.LeftThumbX:
					this.LeftAnalogStickX = value;
					break;
				case GamepadAxis.LeftThumbY:
					this.LeftAnalogStickY = value;
					break;
				case GamepadAxis.RightThumbX:
					this.RightAnalogStickX = value;
					break;
				case GamepadAxis.RightThumbY:
					this.RightAnalogStickY = value;
					break;
				case GamepadAxis.LeftTrigger:
					this.LeftTrigger = value;
					break;
				case GamepadAxis.RightTrigger:
					this.RightTrigger = value;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(axis), axis, null);
			}
		}
		
		/// <summary>
		/// Gets the value of a <see cref="GamepadAxis"/>.
		/// </summary>
		/// <param name="axis">The axis whose value is to be retrieved.</param>
		public float GetAxis(GamepadAxis axis)
		{
			switch (axis)
			{
				case GamepadAxis.Unknown:
					throw new ArgumentException("Unknown axis.", nameof(axis));
				case GamepadAxis.LeftThumbX:
					return this.LeftAnalogStickX;
				case GamepadAxis.LeftThumbY:
					return this.LeftAnalogStickY;
				case GamepadAxis.RightThumbX:
					return this.RightAnalogStickX;
				case GamepadAxis.RightThumbY:
					return this.RightAnalogStickY;
				case GamepadAxis.LeftTrigger:
					return this.LeftTrigger;
				case GamepadAxis.RightTrigger:
					return this.RightTrigger;
				default:
					throw new ArgumentOutOfRangeException(nameof(axis), axis, null);
			}
		}
	
		/// <summary>
		/// Updates the gamepad's state.
		/// </summary>
		public abstract void Update();

		/// <summary>
		/// Returns a value that indicates whether the specified button is pressed.
		/// </summary>
		/// <param name="button"></param>
		/// <returns></returns>
		public bool IsPressed(GamepadButton button) => this.pressedButtons.Contains(button);

		/// <summary>
		/// Called when a button is pressed down.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnButtonDown(GamepadButtonEventArgs e)
		{
			this.ButtonDown?.Invoke(this, e);
		}

		/// <summary>
		/// Called when a button is released.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnButtonUp(GamepadButtonEventArgs e)
		{
			this.ButtonUp?.Invoke(this, e);
		}
		
		/// <summary>
		/// Called when an axis value has changed.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnAxisChanged(GamepadAxisEventArgs e)
		{
			this.AxisChanged?.Invoke(this, e);
		}

		#endregion
	}
}
