﻿using System;

namespace Iridescence.Input
{
	/// <summary>
	/// Mouse button event args.
	/// </summary>
	public class MouseButtonEventArgs : EventArgs
	{
		#region Properties

		/// <summary>
		/// Gets the mouse button.
		/// </summary>
		public MouseButton Button { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new MouseButtonEventArgs.
		/// </summary>
		public MouseButtonEventArgs(MouseButton button)
		{
			this.Button = button;
		}

		#endregion
	}
}
