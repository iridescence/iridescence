﻿namespace Iridescence.Input
{
	/// <summary>
	/// Interface for input devices.
	/// </summary>
	public interface IInputDevice
	{
		/// <summary>
		/// Updates the input device state.
		/// </summary>
		void Update();
	}
}
