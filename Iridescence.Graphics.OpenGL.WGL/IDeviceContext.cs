﻿using System;

namespace Iridescence.Graphics.OpenGL.WGL
{
	/// <summary>
	/// Interface for objects that provide a hDC.
	/// </summary>
	public interface IDeviceContext
	{
		/// <summary>
		/// Gets the device context (also known as hDC).
		/// </summary>
		IntPtr DeviceContext { get; }
	}
}
