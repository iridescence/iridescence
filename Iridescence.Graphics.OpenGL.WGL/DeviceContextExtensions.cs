﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL.WGL
{
	/// <summary>
	/// Extension methods for objects that provide a hDC.
	/// </summary>
	public static class DeviceContextExtensions
	{
		/// <summary>
		/// Creates a new OpenGL context.
		/// </summary>
		/// <returns></returns>
		public static WGLContext CreateContext(this IDeviceContext dev)
		{
			return WGLContext.CreateContext(dev.DeviceContext);
		}

		/// <summary>
		/// Creates a new OpenGL context.
		/// </summary>
		/// <returns></returns>
		public static WGLContext CreateContext(this IDeviceContext dev, PixelFormatDescriptor pfd, int major, int minor, ContextFlags flags, ProfileMask mask)
		{
			return WGLContext.CreateContext(dev.DeviceContext, major, minor, flags, mask);
		}

		/// <summary>
		/// Sets the pixel format of the control's device context.
		/// </summary>
		public static PixelFormatDescriptor SetPixelFormat(this IDeviceContext dev, PixelFormatDescriptor pfd)
		{
			int pixelFormat = Win32.ChoosePixelFormat(dev.DeviceContext, ref pfd);

			if (!Win32.SetPixelFormat(dev.DeviceContext, pixelFormat, ref pfd))
				throw new Win32Exception(Marshal.GetLastWin32Error());

			if (!Win32.DescribePixelFormat(dev.DeviceContext, pixelFormat, (uint)Marshal.SizeOf<PixelFormatDescriptor>(), out pfd))
				throw new Win32Exception(Marshal.GetLastWin32Error());

			return pfd;
		}

		/// <summary>
		/// Makes the specified WGL context current for the device context.
		/// </summary>
		/// <param name="dev"></param>
		/// <param name="context"></param>
		public static void MakeCurrent(this IDeviceContext dev, IntPtr context)
		{
			if(!WGL.wglMakeCurrent(dev.DeviceContext, context))
				throw new Win32Exception(Marshal.GetLastWin32Error());
		}

		/// <summary>
		/// Makes the specified WGL context current for the device context.
		/// </summary>
		/// <param name="dev"></param>
		/// <param name="context"></param>
		public static void MakeCurrent(this IDeviceContext dev, WGLContext context)
		{
			dev.MakeCurrent(context?.Handle ?? IntPtr.Zero);
		}

		/// <summary>
		/// Swaps the buffers of the specified device context.
		/// </summary>
		/// <param name="dev"></param>
		public static void SwapBuffers(this IDeviceContext dev)
		{
			if(!Win32.SwapBuffers(dev.DeviceContext))
				throw new Win32Exception(Marshal.GetLastWin32Error());
		}
	}
}
