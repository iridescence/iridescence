﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL.WGL
{
	/// <summary>
	/// WGL bindings.
	/// </summary>
	public static class WGL
	{
		#region Methods

		[DllImport("opengl32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern IntPtr wglCreateContext(IntPtr hDc);

		[DllImport("opengl32.dll", ExactSpelling = true, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool wglDeleteContext(IntPtr oldContext);

		[DllImport("opengl32.dll", ExactSpelling = true, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool wglMakeCurrent(IntPtr hDc, IntPtr newContext);

		[DllImport("opengl32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern IntPtr wglGetProcAddress(string lpszProc);

		[DllImport("opengl32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern IntPtr wglGetCurrentContext();
		
		public static T GetDelegate<T>() where T : class
		{
			return GetDelegate<T>(typeof(T).Name);
		}

		public static T GetDelegate<T>(string str) where T : class
		{
			Type type = typeof(T);
			if (!typeof(Delegate).IsAssignableFrom(type))
			{
				throw new ArgumentException("T must be a delegate.");
			}

			IntPtr ptr = GetProcAddress(str);
			if (ptr == IntPtr.Zero)
				return null;

			return Marshal.GetDelegateForFunctionPointer<T>(ptr);
			//return (T)(object)Marshal.GetDelegateForFunctionPointer(ptr, type);
		}

		/// <summary>
		/// Returns the function pointer for the method with the specified name.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static IntPtr GetProcAddress(string str)
		{
			IntPtr ptr = WGL.wglGetProcAddress(str);
			if (ptr == IntPtr.Zero)
			{
				ptr = Win32.GetProcAddress(Win32.OpenGL32, str);
			}

			return ptr;
		}

		#endregion
	}
}
