﻿using System;

namespace Iridescence.Graphics.OpenGL.WGL
{
	[Flags]
	public enum PixelFormatDescriptorFlags : uint
	{
		Doublebuffer = 0x00000001,
		Stereo = 0x00000002,
		DrawToWindow = 0x00000004,
		DrawToBitmap = 0x00000008,
		SupportGdi = 0x00000010,
		SupportOpengl = 0x00000020,
		GenericFormat = 0x00000040,
		NeedPalette = 0x00000080,
		NeedSystemPalette = 0x00000100,
		SwapExchange = 0x00000200,
		SwapCopy = 0x00000400,
		SwapLayerBuffers = 0x00000800,
		GenericAccelerated = 0x00001000,
		SupportDirectdraw = 0x00002000,
		Direct3DAccelerated = 0x00004000,
		SupportComposition = 0x00008000,
		DepthDontcare = 0x20000000,
		DoublebufferDontcare = 0x40000000,
		StereoDontcare = 0x80000000
	}
}