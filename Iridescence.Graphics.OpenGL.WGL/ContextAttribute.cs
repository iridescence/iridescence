﻿namespace Iridescence.Graphics.OpenGL.WGL
{
	public enum ContextAttribute
	{
		ContextMajorVersion = 0x2091,
		ContextMinorVersion = 0x2092,
		LayerPlane = 0x2093,
		Flags = 0x2094,
		ProfileMask = 0x9126
	}
}