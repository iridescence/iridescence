﻿namespace Iridescence.Graphics.OpenGL.WGL
{
	public enum PixelFormatDescriptorPixelType : byte
	{
		PfdTypeRgba = 0,
		PfdTypeColorindex = 1
	}
}