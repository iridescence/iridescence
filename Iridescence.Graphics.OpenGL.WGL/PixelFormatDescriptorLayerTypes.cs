﻿namespace Iridescence.Graphics.OpenGL.WGL
{
	public enum PixelFormatDescriptorLayerTypes : byte
	{
		PfdMainPlane = 0,
		PfdOverlayPlane = 1,
		PfdUnderlayPlane = 255
	}
}