﻿using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL.WGL
{
	public struct PixelFormatDescriptor
	{
		public ushort Size;
		public ushort Version;
		public PixelFormatDescriptorFlags Flags;
		public PixelFormatDescriptorPixelType PixelType;
		public byte ColorBits;
		public byte RedBits;
		public byte RedShift;
		public byte GreenBits;
		public byte GreenShift;
		public byte BlueBits;
		public byte BlueShift;
		public byte AlphaBits;
		public byte AlphaShift;
		public byte AccumBits;
		public byte AccumRedBits;
		public byte AccumGreenBits;
		public byte AccumBlueBits;
		public byte AccumAlphaBits;
		public byte DepthBits;
		public byte StencilBits;
		public byte AuxBuffers;
		public PixelFormatDescriptorLayerTypes LayerType;
		public byte Reserved;
		public uint LayerMask;
		public uint VisibleMask;
		public uint DamageMask;

		public static readonly PixelFormatDescriptor Default = new PixelFormatDescriptor()
		{
			Size = (ushort)Marshal.SizeOf<PixelFormatDescriptor>(),
			Version = 1,
			Flags = PixelFormatDescriptorFlags.DrawToWindow | PixelFormatDescriptorFlags.SupportOpengl | PixelFormatDescriptorFlags.Doublebuffer | PixelFormatDescriptorFlags.SupportComposition,
			PixelType = PixelFormatDescriptorPixelType.PfdTypeRgba,
			ColorBits = 32,
			RedBits = 0,
			RedShift = 0,
			GreenBits = 0,
			GreenShift = 0,
			BlueBits = 0,
			BlueShift = 0,
			AlphaBits = 0,
			AlphaShift = 0,
			AccumBits = 0,
			AccumRedBits = 0,
			AccumGreenBits = 0,
			AccumBlueBits = 0,
			AccumAlphaBits = 0,
			DepthBits = 24,
			StencilBits = 8,
			AuxBuffers = 0,
			LayerType = PixelFormatDescriptorLayerTypes.PfdMainPlane,
			Reserved = 0,
			LayerMask = 0,
			VisibleMask = 0,
			DamageMask = 0,
		};
	}
}
