﻿using System;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL.WGL
{
	[return: MarshalAs(UnmanagedType.Bool)]
	internal delegate bool wglSwapIntervalEXT(int interval);

	internal delegate int wglGetSwapIntervalEXT();

	internal delegate IntPtr wglCreateContextAttribsARB(IntPtr hDc, IntPtr hShareContext, IntPtr attribList);
}
