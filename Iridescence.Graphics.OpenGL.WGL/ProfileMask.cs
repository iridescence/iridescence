﻿using System;

namespace Iridescence.Graphics.OpenGL.WGL
{
	[Flags]
	public enum ProfileMask
	{
		None = 0x0,
		CoreProfileBit = 0x1,
		CompatibilityProfileBit = 0x2,
	}
}
