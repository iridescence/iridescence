﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL.WGL
{
	/// <summary>
	/// Represents a WGL context.
	/// </summary>
	public sealed class WGLContext : IDisposable
	{
		#region Fields

		internal wglSwapIntervalEXT SwapIntervalEXT;
		internal wglGetSwapIntervalEXT GetSwapIntervalEXT;
		internal wglCreateContextAttribsARB CreateContextAttribsARB;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the context handle.
		/// </summary>
		public IntPtr Handle { get; }

		/// <summary>
		/// Gets the current active context for the executing thread.
		/// </summary>
		public static WGLContext CurrentContext
		{
			get
			{
				IntPtr context = WGL.wglGetCurrentContext();
				if (context == IntPtr.Zero)
					return null;

				return new WGLContext(context);
			}
		}

		/// <summary>
		/// Gets or sets the swap interval.
		/// </summary>
		public int SwapInterval
		{
			get
			{
				if (!this.IsCurrent)
					throw new InvalidOperationException("This context is not the current context.");

				return this.GetSwapIntervalEXT?.Invoke() ?? 0;
			}
			set
			{
				if (!this.IsCurrent)
					throw new InvalidOperationException("This context is not the current context.");

				this.SwapIntervalEXT?.Invoke(value);
			}
		}

		/// <summary>
		/// Gets a value that indicates whether this context is the current context for the executing thread.
		/// </summary>
		public bool IsCurrent => this.Equals(CurrentContext);

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new WGLContext.
		/// </summary>
		internal WGLContext(IntPtr handle)
		{
			this.Handle = handle;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates a new OpenGL context.
		/// </summary>
		/// <param name="deviceContext"></param>
		/// <returns></returns>
		public static WGLContext CreateContext(IntPtr deviceContext)
		{
			IntPtr handle = WGL.wglCreateContext(deviceContext);
			if (handle == IntPtr.Zero)
				throw new Win32Exception(Marshal.GetLastWin32Error());

			WGLContext context = new WGLContext(handle);

			if (!WGL.wglMakeCurrent(deviceContext, context.Handle))
				throw new Win32Exception(Marshal.GetLastWin32Error());

			context.LoadExtensions();
			context.SwapInterval = 0;

			return context;
		}

		/// <summary>
		/// Creates a new OpenGL context.
		/// </summary>
		/// <param name="deviceContext"></param>
		/// <param name="major"></param>
		/// <param name="minor"></param>
		/// <param name="flags"></param>
		/// <param name="mask"></param>
		/// <returns></returns>
		public static WGLContext CreateContext(IntPtr deviceContext, int major, int minor, ContextFlags flags, ProfileMask mask)
		{
			int[] attribs = new int[8];

			attribs[0] = (int)ContextAttribute.ContextMajorVersion;
			attribs[1] = major;

			attribs[2] = (int)ContextAttribute.ContextMinorVersion;
			attribs[3] = minor;

			attribs[4] = (int)ContextAttribute.Flags;
			attribs[5] = (int)flags;

			attribs[6] = (int)ContextAttribute.ProfileMask;
			attribs[7] = (int)mask;

			GCHandle attribsHandle = GCHandle.Alloc(attribs, GCHandleType.Pinned);

			// Create a dummy context to get method addresses.
			WGLContext dummy = CreateContext(deviceContext);

			try
			{
				// Create actual context.
				IntPtr handle = dummy.CreateContextAttribsARB(deviceContext, IntPtr.Zero, attribsHandle.AddrOfPinnedObject());
				if (handle == IntPtr.Zero)
					throw new Win32Exception(Marshal.GetLastWin32Error());

				WGLContext context = new WGLContext(handle);
				if (!WGL.wglMakeCurrent(deviceContext, context.Handle))
					throw new Win32Exception(Marshal.GetLastWin32Error());

				// Load extensions.
				context.LoadExtensions();
				context.SwapInterval = 0;

				return context;
			}
			finally
			{
				attribsHandle.Free();
				dummy.Dispose();
			}
		}

		/// <summary>
		/// Returns the address of the specified OpenGL function.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public IntPtr GetProcAddress(string name)
		{
			return WGL.GetProcAddress(name);
		}

		internal void LoadExtensions()
		{
			this.SwapIntervalEXT = WGL.GetDelegate<wglSwapIntervalEXT>();
			this.GetSwapIntervalEXT = WGL.GetDelegate<wglGetSwapIntervalEXT>();
			this.CreateContextAttribsARB = WGL.GetDelegate<wglCreateContextAttribsARB>();
		}

		public override bool Equals(object obj)
		{
			return obj is WGLContext context && context.Handle == this.Handle;
		}
		
		public override int GetHashCode()
		{
			return this.Handle.GetHashCode();
		}

		public void Dispose()
		{
			if (!WGL.wglDeleteContext(this.Handle))
				throw new Win32Exception(Marshal.GetLastWin32Error());
		}

		#endregion
	}
}
