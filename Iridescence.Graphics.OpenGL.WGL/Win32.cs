﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Iridescence.Graphics.OpenGL.WGL
{
	/// <summary>
	/// Native functions
	/// </summary>
	public static class Win32
	{
		#region Fields

		private static IntPtr opengl32;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the module handle for opengl32.dll.
		/// </summary>
		public static IntPtr OpenGL32
		{
			get
			{
				if (opengl32 == IntPtr.Zero)
				{
					opengl32 = LoadLibrary("opengl32.dll");
					if(opengl32 == IntPtr.Zero)
						throw new Win32Exception(Marshal.GetLastWin32Error(), "Could not load OpenGL library.");
				}

				return opengl32;
			}
		}

		/// <summary>
		/// Gets the instance handle (also known as hINSTANCE).
		/// </summary>
		public static IntPtr InstanceHandle { get; } = Process.GetCurrentProcess().Handle;

		#endregion

		#region Methods

		[DllImport("kernel32.dll", ExactSpelling = true, CharSet = CharSet.Ansi, SetLastError = true)]
		public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		public static extern IntPtr LoadLibrary(string lpModuleName);

		[DllImport("user32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern IntPtr GetDC(IntPtr hWnd);

		[DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern int ChoosePixelFormat(IntPtr hdc, [In] ref PixelFormatDescriptor ppfd);

		[DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern bool DescribePixelFormat(IntPtr hdc, int iPixelFormat, uint nBytes, out PixelFormatDescriptor ppfd);

		[DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
		public static extern bool SetPixelFormat(IntPtr hdc, int iPixelFormat, ref PixelFormatDescriptor ppfd);

		[DllImport("gdi32.dll", ExactSpelling = true, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool SwapBuffers(IntPtr hDc);

		#endregion
	}
}
