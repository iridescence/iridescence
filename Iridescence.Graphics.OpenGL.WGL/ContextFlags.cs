﻿using System;

namespace Iridescence.Graphics.OpenGL.WGL
{
	[Flags]
	public enum ContextFlags
	{
		None = 0x0,
		DebugBit = 0x1,
		ForwardCompatibleBit = 0x2,
	}
}