﻿using System.Linq;
using Iridescence.Math;
using Xunit;

namespace Tests.Iridescence.Math
{
	public class LineIntersection
	{
		[Fact]
		public void Same()
		{
			for (int i = 0; i < 360; ++i)
			{
				LineSegmentD a = new LineSegmentD(0.0, 0.0, Utility.Cos(Utility.DegreesToRadians(i)), Utility.Sin(Utility.DegreesToRadians(i)));
				double[] intersection = a.GetIntersections(a).ToArray();
				Assert.Single(intersection, 0.0);
			}

			for (int i = 0; i < 360; ++i)
			{
				LineSegmentD a = new LineSegmentD(0.0, 0.0, Utility.Cos(Utility.DegreesToRadians(i)), Utility.Sin(Utility.DegreesToRadians(i)));
				LineSegmentD b = new LineSegmentD(0.0, 0.0, 1.2 * Utility.Cos(Utility.DegreesToRadians(i)), 1.2 * Utility.Sin(Utility.DegreesToRadians(i)));
				double[] intersection = a.GetIntersections(b).ToArray();
				Assert.Single(intersection, 0.0);
			}
		}

		[Fact]
		public void Cross()
		{
			LineSegmentD a = new LineSegmentD(0.0, 0.0, 5.0, 5.0);
			LineSegmentD b = new LineSegmentD(5.0, 0.0, 0.0, 5.0);

			double[] intersection = a.GetIntersections(b).ToArray();
			Assert.Single(intersection, 0.5);
		}
	}
}
