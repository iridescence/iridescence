﻿using System.IO;

namespace Tests.Iridescence.IO.Compression.LZ4
{
	/// <summary>
	/// Fake the behavior of a network stream where <see cref="Stream.Read(byte[], int, int)"/> will often
	/// return before the specified requested length has been read.
	/// </summary>
	internal class FakeNetworkStream: Stream
	{
		private Stream Inner { get; }
		private int Threshold { get; }

		public FakeNetworkStream(Stream inner, int threshold = 1)
		{
			this.Inner = inner;
			this.Threshold = threshold;
		}

		public override bool CanRead => this.Inner.CanRead;
		public override bool CanSeek => this.Inner.CanSeek;
		public override bool CanWrite => this.Inner.CanWrite;
		public override long Length => this.Inner.Length;

		public override long Position
		{
			get => this.Inner.Position;
			set => this.Inner.Position = value;
		}

		public override void Flush() => this.Inner.Flush();

		public override long Seek(long offset, SeekOrigin origin)
			=> this.Inner.Seek(offset, origin);

		public override void SetLength(long value)
			=> this.Inner.SetLength(value);

		public override void Write(byte[] buffer, int offset, int count)
			=> this.Inner.Write(buffer, offset, count);

		public override int Read(byte[] buffer, int offset, int count)
		{
			if (count > this.Threshold)
				count = this.Threshold;
			return this.Inner.Read(buffer, offset, count);
		}

		protected override void Dispose(bool disposing)
		{
			this.Inner?.Dispose();
		}
	}
}
