﻿using System;
using DuskyDepths.Networking;
using Iridescence.Networking;
using Xunit;

namespace Tests.Iridescence.Networking
{
	public class Rpc : Common
	{
		[Serializable]
		public class RpcTest
		{
			public string String;
			public int Integer;

			[ClientRpc]
			public void CallableFromClient(string str, int i)
			{
				this.String = str;
				this.Integer = i;
			}
		}

		[Fact]
		public void SimpleRpc()
		{
			this.CreatePeerPair(out StreamPeer server, out StreamPeer client);

			RpcTest serverTest = new RpcTest();
			server.Write(serverTest);

			RpcTest clientTest = (RpcTest)client.Read();

			server.Invoke(serverTest.CallableFromClient, "test", 1234);
			server.Dispose();

			((ICall)client.Read()).Invoke();
			Assert.False(client.TryRead(out _));

			Assert.Equal("test", clientTest.String);
			Assert.Equal(1234, clientTest.Integer);
		}
	}
}
