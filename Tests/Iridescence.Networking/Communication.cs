﻿using System;
using System.Threading;
using Iridescence.Networking;
using Xunit;

namespace Tests.Iridescence.Networking
{
	public class Communication : Common
	{
		[Fact]
		public void SimpleCommunication()
		{
			this.CreatePeerPair(out StreamPeer serverPeer, out StreamPeer clientPeer);

			using (serverPeer)
			using (clientPeer)
			{
				serverPeer.Write("Hello");
				Assert.Equal("Hello", clientPeer.Read());

				clientPeer.Write("Hi");
				Assert.Equal("Hi", serverPeer.Read());
			}
		}

		[Fact]
		public void ClientEndTriggersServerEnd()
		{
			this.CreatePeerPair(out StreamPeer serverPeer, out StreamPeer clientPeer);

			clientPeer.Write("Hello");
			clientPeer.Dispose();

			Assert.True(serverPeer.TryRead(out _));
			Assert.False(serverPeer.TryRead(out _));

			serverPeer.Dispose();
		}

		[Fact]
		public void ServerEndTriggersClientEnd()
		{
			this.CreatePeerPair(out StreamPeer serverPeer, out StreamPeer clientPeer);

			serverPeer.Write("Hello");
			serverPeer.Dispose();

			Assert.True(clientPeer.TryRead(out _));
			Assert.False(clientPeer.TryRead(out _));

			clientPeer.Dispose();
		}

		[Fact]
		public void SendPeerOverNetwork()
		{
			this.CreatePeerPair(out StreamPeer serverPeer, out StreamPeer clientPeer);

			// add peers as statically referencable objects.
			serverPeer.Heap.Add(new PeerObjectIdentifier(Role.None, 1234), serverPeer);
			clientPeer.Heap.Add(new PeerObjectIdentifier(Role.None, 1234), clientPeer);

			using (serverPeer)
			using (clientPeer)
			{
				serverPeer.Write(serverPeer);
				Assert.Equal(clientPeer, clientPeer.Read());

				clientPeer.Write(clientPeer);
				Assert.Equal(serverPeer, serverPeer.Read());
			}
		}

		[Fact]
		public void ServerEndWhileClientReading()
		{
			this.CreatePeerPair(out StreamPeer serverPeer, out StreamPeer clientPeer);

			serverPeer.Write("Test");

			Assert.Equal("Test", clientPeer.Read());

			Exception exception = null;

			Thread t = new Thread(() =>
			{
				try
				{
					object msg = clientPeer.Read();
				}
				catch (Exception ex)
				{
					exception = ex;
				}
			});

			t.Start();

			Thread.Sleep(100);

			serverPeer.Dispose();

			t.Join();

			if (!(exception is PeerClosedException))
				Assert.True(false, $"ReadAsync should have thrown a PeerClosedException, but threw {exception} instead.");

			clientPeer.Dispose();
		}

		[Fact]
		public void ClientEndWhileServerReading()
		{
			this.CreatePeerPair(out StreamPeer serverPeer, out StreamPeer clientPeer);

			serverPeer.Write("Test");

			Assert.Equal("Test", clientPeer.Read());

			Exception exception = null;

			Thread t = new Thread(() =>
			{
				try
				{
					object msg = serverPeer.Read();
				}
				catch (Exception ex)
				{
					exception = ex;
				}
			});

			t.Start();

			Thread.Sleep(100);

			clientPeer.Dispose();

			t.Join();

			if (!(exception is PeerClosedException))
				Assert.True(false, $"ReadAsync should have thrown a PeerClosedException, but threw {exception} instead.");

			serverPeer.Dispose();
		}

		/*
		[Fact]
		public async Task ServerEndWhileClientReadingAsync()
		{
			this.CreatePeerPair(out StreamPeer serverPeer, out StreamPeer clientPeer);

			serverPeer.Write("Test");

			Assert.Equal("Test", clientPeer.Read());

			ValueTask<object> readTask = clientPeer.ReadAsync();

			serverPeer.Dispose();

			await Assert.ThrowsAsync<PeerClosedException>(async () =>
			{
				object msg = await readTask;
			});

			clientPeer.Dispose();
		}

		[Fact]
		public async Task ClientEndWhileServerReadingAsync()
		{
			this.CreatePeerPair(out StreamPeer serverPeer, out StreamPeer clientPeer);

			serverPeer.Write("Test");

			Assert.Equal("Test", clientPeer.Read());

			ValueTask<object> readTask = serverPeer.ReadAsync();

			clientPeer.Dispose();

			await Assert.ThrowsAsync<PeerClosedException>(async () =>
			{
				object msg = await readTask;
				Console.WriteLine("hui");
			});

			serverPeer.Dispose();
		}
		*/
	}
}
