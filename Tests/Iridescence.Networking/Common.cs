﻿using System.Net;
using System.Net.Sockets;
using Iridescence.Networking;

namespace Tests.Iridescence.Networking
{
	public abstract class Common
	{
		protected void CreatePeerPair(out StreamPeer serverPeer, out StreamPeer clientPeer)
		{
			// create TCP listener on random port.
			TcpListener listener = new TcpListener(new IPEndPoint(IPAddress.Any, 0));
			listener.Start();

			// create TCP client and connect to listener.
			TcpClient client = new TcpClient();
			client.Connect(new IPEndPoint(IPAddress.Loopback, ((IPEndPoint)listener.LocalEndpoint).Port));

			// accept client.
			TcpClient server = listener.AcceptTcpClient();
			listener.Stop();

			// create peers for the two connected sockets.
			clientPeer = new StreamPeer(Role.Client, client.GetStream(), false);
			serverPeer = new StreamPeer(Role.Server, server.GetStream(), false);
		}

	}
}
