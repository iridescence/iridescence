﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using Iridescence;
using Iridescence.Networking;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	public class ISerializableTest : Common
	{
		public ISerializableTest(ITestOutputHelper stdout)
			: base(stdout)
		{
			
		}

		[Serializable]
		class MyEquality : IEqualityComparer<string>
		{
			public bool Equals(string x, string y)
			{
				throw new NotImplementedException();
			}

			public int GetHashCode(string obj)
			{
				throw new NotImplementedException();
			}
		}

		[Fact]
		public void DictionaryWithEqualityComparer()
		{
			var dict = new Dictionary<string, int>(new MyEquality());

			var dict2 = this.WriteAndRead(dict);

			Assert.True(dict.Comparer is MyEquality);
			Assert.True(dict2.Comparer is MyEquality);
		}

		[Serializable]
		class SomeClass
		{

		}

		[Fact]
		public void HashSet()
		{
			this.WriteAndRead(new HashSet<SomeClass>());
		}


		[Serializable]
		abstract class SomePublicClass
		{

		}

		[Serializable]
		class ClassThatShouldBeHidden : SomePublicClass, ISerializable
		{
			public ClassThatShouldBeHidden()
			{

			}
			
			public void GetObjectData(SerializationInfo info, StreamingContext context)
			{
				info.SetType(typeof(SerializationPlaceholder));
			}
		}

		class SerializationPlaceholder : ISerializable, IObjectReference
		{
			private SerializationPlaceholder(SerializationInfo info, StreamingContext context)
			{

			}

			public void GetObjectData(SerializationInfo info, StreamingContext context)
				=> throw new NotImplementedException();

			public object GetRealObject(StreamingContext context)
			{
				return new ClassThatShouldBeHidden();
			}
		}
		
		[Serializable]
		class Test
		{
			public SomePublicClass A;
			public NetworkingStaticReferences<int> B;
		}

		[NetworkingStatic]
		private static object testField = "a";

		[Fact]
		public void HideISerializableOriginal()
		{
			var serializer = this.Compilers[0].To<TestOutputStream>().For<object>().Member.Generic;

			using (MemoryStream data = new MemoryStream())
			using (MemoryStream metadata = new MemoryStream())
			{
				try
				{
					using (TestOutputStream output = new TestOutputStream(data, 100, true, metadata))
					{
						NetworkingStaticReferences<int> nsr = new NetworkingStaticReferences<int>();
						FieldInfo fld = typeof(ISerializableTest).GetField(nameof(testField), BindingFlags.Static | BindingFlags.NonPublic);
						nsr.AddField(fld, 1337);

						bool failed = false;

						output.TypeWritten += (s, e) =>
						{
							if (e == typeof(ClassThatShouldBeHidden))
								failed = true;
						};

						object t = new Test()
						{
							A = new ClassThatShouldBeHidden(),
							B = nsr,
						};
						serializer(output, ref t);

						output.Flush(false);
						Assert.False(failed);
					}
				}
				finally
				{
					data.Position = 0;
					data.Dump(this.Output.WriteLine);
					data.Position = 0;

					this.Output.WriteLine("");
					this.Output.WriteLine("Metadata:");
					metadata.Position = 0;
					metadata.Dump(this.Output.WriteLine);
					metadata.Position = 0;
				}

				var deserializer = this.Compilers[0].From<TestInputStream>().For<object>().Deferred<EmptyDeferredState>().Member.Generic.UnDefer();
				using (TestInputStream input = new TestInputStream(data, 100, true, metadata))
				{
					deserializer(input, out object _);
				}
			}
		}
	}
}
