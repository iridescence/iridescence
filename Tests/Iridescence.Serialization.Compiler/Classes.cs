﻿using System;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	public class Classes : Common
	{
		public Classes(ITestOutputHelper stdout)
			: base(stdout)
		{
		}

		[Serializable]
		public class EmptyClassType
		{

		}

		[Fact]
		public void EmptyClass()
		{
			this.WriteAndRead<EmptyClassType>(null);
			this.WriteAndRead<EmptyClassType>(new EmptyClassType());
		}

		[Serializable]
		public class SimpleClassType
		{
			public double A;
			public short B;
			public long C;
		}

		[Fact]
		public void SimpleClass()
		{
			this.WriteAndRead<SimpleClassType>(null);
			this.WriteAndRead<SimpleClassType>(new SimpleClassType());
			this.WriteAndRead<SimpleClassType>(new SimpleClassType() {A = 1337.42, B = 1234, C = 1234123412341234});
		}

		[Serializable]
		public class Cycle1Type
		{
			public Cycle1Type Cycle;
		}

		[Fact]
		public void Cycle1()
		{
			this.WriteAndRead<Cycle1Type>(null);

			{
				Cycle1Type val = new Cycle1Type();
				this.WriteAndRead<Cycle1Type>(val);
			}

			{
				Cycle1Type val = new Cycle1Type();
				val.Cycle = val;
				this.WriteAndRead<Cycle1Type>(val);
			}
		}


		[Serializable]
		public class Cycle2Type1
		{
			public Cycle2Type2 Cycle;
		}

		[Serializable]
		public class Cycle2Type2
		{
			public Cycle2Type1 Cycle;
		}

		[Fact]
		public void Cycle2()
		{
			this.WriteAndRead<Cycle2Type1>(null);
			this.WriteAndRead<Cycle2Type2>(null);

			{
				Cycle2Type1 val = new Cycle2Type1();
				this.WriteAndRead<Cycle2Type1>(val);
			}

			{
				Cycle2Type2 val = new Cycle2Type2();
				this.WriteAndRead<Cycle2Type2>(val);
			}

			{
				Cycle2Type1 val = new Cycle2Type1();
				val.Cycle = new Cycle2Type2();
				val.Cycle.Cycle = val;
				this.WriteAndRead<Cycle2Type1>(val);
			}
		}
	}
}
