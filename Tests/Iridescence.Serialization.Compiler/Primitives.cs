﻿using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	public class Primitives : Common
	{
		public Primitives(ITestOutputHelper stdout)
			: base(stdout)
		{

		}

		#region Methods

		[Fact]
		public void Bool()
		{
			this.WriteAndRead<bool>(true);
			this.WriteAndRead<bool>(false);
		}
		
		[Fact]
		public void Int8()
		{
			this.WriteAndRead<sbyte>(sbyte.MinValue);
			this.WriteAndRead<sbyte>(0);
			this.WriteAndRead<sbyte>(sbyte.MaxValue);
		}

		[Fact]
		public void UInt8()
		{
			this.WriteAndRead<byte>(byte.MinValue);
			this.WriteAndRead<byte>(byte.MaxValue);
		}

		[Fact]
		public void Int16()
		{
			this.WriteAndRead<short>(short.MinValue);
			this.WriteAndRead<short>(0);
			this.WriteAndRead<short>(short.MaxValue);
		}

		[Fact]
		public void UInt16()
		{
			this.WriteAndRead<ushort>(ushort.MinValue);
			this.WriteAndRead<ushort>(ushort.MaxValue);
		}

		[Fact]
		public void Int32()
		{
			this.WriteAndRead<int>(int.MinValue);
			this.WriteAndRead<int>(0);
			this.WriteAndRead<int>(int.MaxValue);
		}

		[Fact]
		public void UInt32()
		{
			this.WriteAndRead<uint>(uint.MinValue);
			this.WriteAndRead<uint>(uint.MaxValue);
		}
		
		[Fact]
		public void Int64()
		{
			this.WriteAndRead<long>(long.MinValue);
			this.WriteAndRead<long>(0);
			this.WriteAndRead<long>(long.MaxValue);
		}

		[Fact]
		public void UInt64()
		{
			this.WriteAndRead<ulong>(ulong.MinValue);
			this.WriteAndRead<ulong>(ulong.MaxValue);
		}
		
		[Fact]
		public void Char()
		{
			this.WriteAndRead<char>('\0');
			this.WriteAndRead<char>('A');
			this.WriteAndRead<char>(char.MinValue);
			this.WriteAndRead<char>(char.MaxValue);
		}

		[Fact]
		public void Float32()
		{
			this.WriteAndRead<float>(0.0f);
			this.WriteAndRead<float>(1.0f);
			this.WriteAndRead<float>(float.Epsilon);
			this.WriteAndRead<float>(float.MinValue);
			this.WriteAndRead<float>(float.MaxValue);
			this.WriteAndRead<float>(float.NegativeInfinity);
			this.WriteAndRead<float>(float.PositiveInfinity);
			this.WriteAndRead<float>(float.NaN);
		}

		[Fact]
		public void Float64()
		{
			this.WriteAndRead<double>(0.0);
			this.WriteAndRead<double>(1.0);
			this.WriteAndRead<double>(double.Epsilon);
			this.WriteAndRead<double>(double.MinValue);
			this.WriteAndRead<double>(double.MaxValue);
			this.WriteAndRead<double>(double.NegativeInfinity);
			this.WriteAndRead<double>(double.PositiveInfinity);
			this.WriteAndRead<double>(double.NaN);
		}

		[Fact]
		public void String()
		{
			this.WriteAndRead<string>(null);
			this.WriteAndRead<string>("");
			this.WriteAndRead<string>("asdf");
			this.WriteAndRead<string>("\0asdf");
		}

		[Fact]
		public void Decimal()
		{
			this.WriteAndRead<decimal>(0m);
			this.WriteAndRead<decimal>(decimal.MaxValue);
			this.WriteAndRead<decimal>(decimal.MinValue);
			this.WriteAndRead<decimal>(1234.4321m);
		}

		#endregion
	}
}
