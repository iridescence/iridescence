﻿using System;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	public class BoxingTests : Common
	{
		public BoxingTests(ITestOutputHelper stdout) : base(stdout)
		{
		}

		[Fact]
		public void Boxing()
		{
			this.WriteAndRead<object>((int)1);
			this.WriteAndRead<IEquatable<int>>((int)1);
		}
	}
}
