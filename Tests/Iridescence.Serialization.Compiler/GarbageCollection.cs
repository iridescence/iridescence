﻿using System;
using System.Collections.Generic;
using Iridescence.Emit;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;
using Xunit;

namespace Tests.Iridescence.Serialization.Compiler
{
	using Compiler = global::Iridescence.Serialization.Compiler.Compiler;

	public class GarbageCollection
	{
		private class TestCompiler : ICompilationUnitFactory
		{
			public event EventHandler<TestUnit> UnitCreated;

			public ICompilationUnit CreateUnit()
			{
				TestUnit unit = new TestUnit(true);
				this.UnitCreated?.Invoke(this, unit);
				return unit;
			}
		}

		private class TestUnit : DynamicMethodUnit
		{
			public event EventHandler<CompilableEmitter> MethodCreated;

			public override CompilableEmitter CreateMethod(MethodSignature signature, string name, bool withCapturedValues = true)
			{
				CompilableEmitter m = base.CreateMethod(signature, name, withCapturedValues);
				this.MethodCreated?.Invoke(this, m);
				return m;
			}

			public TestUnit(bool trace) : base(trace)
			{
			}
		}

		private static void build(
			out ObjectWriter<TestOutputStream, object> serializer,
			out ObjectReader<TestInputStream, object> deserializer,
			out WeakReference[] refs)
		{
			List<WeakReference>  refList = new List<WeakReference>();

			var config = Common.GetDefaultConfig();
			var compiler = new TestCompiler();

			compiler.UnitCreated += (s, e) =>
			{
				refList.Add(new WeakReference(e));
				e.MethodCreated += (_, m) => { refList.Add(new WeakReference(m)); };
			};

			config = config.WithCompilerFactory(compiler);

			//refList.Add(new WeakReference(config));
			//refList.Add(new WeakReference(compiler));

			Compiler w = new Compiler(config);

			//refList.Add(new WeakReference(w));
			//refList.Add(new WeakReference(r));

			serializer = w.To<TestOutputStream>().For<string>().Member.NonGeneric;
			deserializer = w.From<TestInputStream>().For<string>().Deferred<EmptyDeferredState>().Member.NonGeneric.UnDefer();

			refs = refList.ToArray();
		}

		[Fact]
		public void NoReferenceToCompilerAreKept()
		{
			build(out ObjectWriter<TestOutputStream, object> serializer, out ObjectReader<TestInputStream, object> deserializer, out WeakReference[] refs);

			GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);
			GC.WaitForPendingFinalizers();

			Assert.True(refs.Length > 1);
			foreach (WeakReference r in refs)
			{
				Assert.False(r.IsAlive, $"{r.Target} is still alive.");
			}
		}

	}
}
