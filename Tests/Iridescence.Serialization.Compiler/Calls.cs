﻿using Iridescence.Math;
using Iridescence.Networking;
using Iridescence.Serialization;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	public class Calls : Common
	{
		public Calls(ITestOutputHelper stdout)
			: base(stdout)
		{
		}

		internal static void TestMethod(string str)
		{
			Assert.Equal("Test123", str);
		}

		internal static void TestRefEquality(object o1, object o2)
		{
			Assert.Same(o1, o2);
		}

		internal static void TestParamAttribs([SerializeAsValue] object o1, [SerializeAsValue] object o2)
		{
			Assert.NotSame(o1, o2);
		}

		internal static void Bug1Method([SerializeAsValue] ushort[] data, RectangleI rect)
		{

		}

		[Fact]
		public void SimpleCalls()
		{
			this.WriteAndRead(new ActionCall<string>()
			{
				Delegate = TestMethod,
				Arg = "Test123"
			}).Invoke();
		}

		[Fact]
		public void RefEquality()
		{
			object o = new object();
			this.WriteAndRead(new ActionCall<object, object>()
			{
				Delegate = TestRefEquality,
				Arg1 = o,
				Arg2 = o,
			}).Invoke();
		}

		[Fact]
		public void ParameterAttributes()
		{
			object o = new object();
			this.WriteAndRead(new ActionCall<object, object>()
			{
				Delegate = TestParamAttribs,
				Arg1 = o,
				Arg2 = o,
			}).Invoke();
		}

		[Fact]
		public void Bug1()
		{
			object o = new object();
			this.WriteAndRead(new ActionCall<ushort[], RectangleI>()
			{
				Delegate = Bug1Method,
				Arg1 = new ushort[100],
				Arg2 = RectangleI.Zero
			}).Invoke();
		}
	}
}
