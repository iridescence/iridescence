﻿using System;
using System.Buffers;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	public class MemoryTests : Common
	{
		public MemoryTests(ITestOutputHelper stdout) : base(stdout)
		{
		}

		[Fact]
		public void EmptyMemory()
		{
			this.WriteAndRead<Memory<byte>>(Memory<byte>.Empty);
			this.WriteAndRead<ReadOnlyMemory<byte>>(ReadOnlyMemory<byte>.Empty);

			this.WriteAndRead<Memory<object>>(Memory<object>.Empty);
			this.WriteAndRead<ReadOnlyMemory<object>>(ReadOnlyMemory<object>.Empty);
		}

		[Fact]
		public void ArrayBasedMemory()
		{
			this.WriteAndRead<Memory<byte>>(new byte[] {1, 2, 3, 4, 5, 6});
			this.WriteAndRead<ReadOnlyMemory<byte>>(new byte[] { 1, 2, 3, 4, 5, 6 });

			this.WriteAndRead<Memory<object>>(new object[] { null, 2, "3", 4.0f });
			this.WriteAndRead<ReadOnlyMemory<object>>(new object[] { null, 2, "3", 4.0f });
		}

		[Fact]
		public void StringBasedMemory()
		{
			this.WriteAndRead<ReadOnlyMemory<char>>(string.Empty.AsMemory());
			this.WriteAndRead<ReadOnlyMemory<char>>("A".AsMemory());
			this.WriteAndRead<ReadOnlyMemory<char>>("asdgaß9ruh89ad„“fub89dfus890dfu .löpååæd89fusd8f9gu q09rg 0ßß".AsMemory());
		}

		[Serializable]
		class CustomMemoryManager : MemoryManager<object>
		{
			private readonly object[] array;

			public CustomMemoryManager(object[] array)
			{
				this.array = array;
			}

			protected override void Dispose(bool disposing)
			{
				
			}

			public override Span<object> GetSpan()
			{
				return this.array;	
			}

			public override MemoryHandle Pin(int elementIndex = 0)
			{
				throw new NotImplementedException();
			}

			public override void Unpin()
			{
				throw new NotImplementedException();
			}
		}

		[Fact]
		public void CustomMemory()
		{
			CustomMemoryManager mgr = new CustomMemoryManager(new object[] {null, 2, "3", 4.0f});

			this.WriteAndRead<Memory<object>>(mgr.Memory);
			this.WriteAndRead<ReadOnlyMemory<object>>(mgr.Memory);

		}
	}
}
