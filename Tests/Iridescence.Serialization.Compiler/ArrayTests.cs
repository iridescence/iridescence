﻿using System;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	public class ArrayTests : Common
	{
		[Serializable]
		public struct EmptyStruct
		{

		}

		[Serializable]
		public struct StructWithValues
		{
			public int X;
			public float Y;
		}


		[Serializable]
		public struct StructWithReferences
		{
			public object X;
			public string Y;
		}

		[Serializable]
		public class ClassWithValues
		{
			public int X;
			public StructWithValues Y;
		}

		[Serializable]
		public class ClassWithReferences
		{
			public object X;
			public ClassWithReferences Y;
		}

		[Fact]
		public void EmptyArray()
		{
			this.WriteAndRead<int[]>(new int[0]);
			this.WriteAndRead<object[]>(new object[0]);
			this.WriteAndRead<string[]>(new string[0]);
			this.WriteAndRead<EmptyStruct[]>(new EmptyStruct[0]);
			this.WriteAndRead<StructWithValues[]>(new StructWithValues[0]);
			this.WriteAndRead<StructWithReferences[]>(new StructWithReferences[0]);
			this.WriteAndRead<ClassWithReferences[]>(new ClassWithReferences[0]);
			this.WriteAndRead<ClassWithValues[]>(new ClassWithValues[0]);
		}

		[Fact]
		public void SzArray()
		{
			this.WriteAndRead<int[]>(new int[] {1,2,3,4});
			this.WriteAndRead<object[]>(new object[] {null, "hey", 1, new EmptyStruct(), new StructWithReferences {X = null, Y = "asdf"}});
			this.WriteAndRead<string[]>(new string[] {null, "a", "BBBB"});

			this.WriteAndRead<EmptyStruct[]>(new EmptyStruct[] {default, default});
			this.WriteAndRead<StructWithValues[]>(new StructWithValues[] {default, new StructWithValues {X = 1, Y = 2}});
			this.WriteAndRead<StructWithReferences[]>(new StructWithReferences[] {default, new StructWithReferences {X = new ClassWithValues {X = 1}, Y = "asdf"}});
			this.WriteAndRead<ClassWithReferences[]>(new ClassWithReferences[] {null, new ClassWithReferences {X = new ClassWithReferences {X = "A", Y = null}, Y = new ClassWithReferences()}});
			this.WriteAndRead<ClassWithValues[]>(new ClassWithValues[] {null, new ClassWithValues {X = 1, Y = new StructWithValues() {X = 1, Y = 2}}});
		}

		[Fact]
		public void MdArray()
		{
			this.WriteAndRead<int[,]>(new int[,]
			{
				{1, 2, 3},
				{2, 3, 4},
				{4, 5, 6}
			});

			this.WriteAndRead<object[,]>(new object[,]
			{
				{1, "2", null},
				{"2", 3.0f, 4m},
				{4d, 5L, new int[] {1, 2, 3}}
			});

		}

		public ArrayTests(ITestOutputHelper stdout) : base(stdout)
		{
		}
	}
}
