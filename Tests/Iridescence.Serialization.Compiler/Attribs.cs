﻿using System;
using Iridescence.Serialization;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	public class Attribs : Common
	{
		public Attribs(ITestOutputHelper stdout)
			: base(stdout)
		{
		}

		[Serializable]
		[SerializeAsValue]
		class TestClass1
		{
			public object A;
		}

		[Serializable]
		class TestClass1Container
		{
			public TestClass1 A;
			public TestClass1 B;
		}

		[Fact]
		public void SerializeAsValueOnType()
		{
			TestClass1 instance = new TestClass1
			{
				A = new object()
			};

			var r = this.WriteAndRead(new TestClass1Container
			{
				A = instance,
				B = instance,
			});

			Assert.NotSame(r.A, r.B);
			Assert.Same(r.A.A, r.B.A);
		}


		[Serializable]
		class TestClass2
		{
			public object A;
		}

		[Serializable]
		class TestClass2Container
		{
			[SerializeAsValue] public TestClass2 A;
			[SerializeAsValue] public TestClass2 B;
			public TestClass2 C;
			public TestClass2 D;
		}

		[Fact]
		public void SerializeAsValueOnMember()
		{
			TestClass2 instance = new TestClass2
			{
				A = new object()
			};

			var r = this.WriteAndRead(new TestClass2Container
			{
				A = instance,
				B = instance,
				C = instance,
				D = instance
			});

			Assert.NotSame(r.A, r.B);
			Assert.NotSame(r.A, r.C);
			Assert.NotSame(r.A, r.D);

			Assert.Same(r.C, r.D);

			Assert.Same(r.A.A, r.B.A);
			Assert.Same(r.A.A, r.C.A);
			Assert.Same(r.A.A, r.D.A);
		}

		[Serializable]
		[SerializeChildrenAsValues]
		class TestClass3
		{
			public TestClass3Child A;
			public TestClass3Child B;
		}

		[Serializable]
		class TestClass3Child
		{
			public object A;
		}

		[Serializable]
		class TestClass3Container
		{
			public TestClass3 A;
			public TestClass3 B;
		}


		[Fact]
		public void SerializeChildrenAsValueOnType()
		{
			TestClass3Child child = new TestClass3Child
			{
				A = new object()
			};

			TestClass3 instance = new TestClass3()
			{
				A = child,
				B = child
			};

			var r = this.WriteAndRead(new TestClass3Container
			{
				A = instance,
				B = instance
			});

			Assert.Same(r.A, r.B);
			Assert.NotSame(r.A.A, r.A.B);
			Assert.NotSame(r.B.A, r.B.B);

			Assert.Same(r.A.A.A, r.A.B.A);
			Assert.Same(r.A.A.A, r.B.A.A);
			Assert.Same(r.A.A.A, r.B.B.A);
		}

	}
}

