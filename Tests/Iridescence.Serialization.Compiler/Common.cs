﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using Iridescence;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;
using KellermanSoftware.CompareNetObjects;
using KellermanSoftware.CompareNetObjects.TypeComparers;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	using Compiler = global::Iridescence.Serialization.Compiler.Compiler;

	public abstract class Common
	{
		protected ITestOutputHelper Output { get; }

		public readonly Compiler[] Compilers;

		protected Common(ITestOutputHelper stdout)
		{
			this.Output = stdout;

			AssemblyBuilder ab = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName("SerializationTest"), AssemblyBuilderAccess.Run);
			List<Assembly> friends = new List<Assembly>
			{
				typeof(Type).Assembly,
				typeof(Common).Assembly,
				typeof(ICompiler).Assembly
			};

			foreach (Assembly friend in friends)
			{
				string assemblyName = friend.GetName().Name;
				ConstructorInfo attrCtor = typeof(IgnoresAccessChecksToAttribute).GetConstructor(new[] { typeof(string) });
				CustomAttributeBuilder attr = new CustomAttributeBuilder(attrCtor, new object[] { assemblyName });
				ab.SetCustomAttribute(attr);
			}

			ModuleBuilder mb = ab.DefineDynamicModule("SerializationTest");

			var config = this.Configure();

			this.Compilers = new Compiler[2];
			this.Compilers[0] = new Compiler(config);
			this.Compilers[1] = new Compiler(config.WithCompilerFactory(new MethodBuilderFactory(mb)));
		}

		public static CompilerConfiguration GetDefaultConfig()
		{
			StreamingContext streamingContext = new StreamingContext(StreamingContextStates.CrossMachine);
			return new CompilerConfiguration()
				.WithArbiter(new SerializableAttributeArbiter(SerializationArbiterFlags.AllowMemory | SerializationArbiterFlags.AllowRuntimeMetadata))
				.WithIOModule(new BufferedStreamIOModule())
				.WithCompilerFactory(new DynamicMethodFactory(true))
				.WithContentModule(new SerializationEventContentModule(streamingContext))
				.WithContentModule(new DeserializationCallbackContentModule())
				.WithContentModule(new PrimitiveContentModule())
				.WithContentModule(new StringContentModule())
				.WithContentModule(new NullableContentModule())
				.WithContentModule(new ArrayContentModule())
				.WithContentModule(new MemoryContentModule())
				.WithContentModule(new DelegateContentModule())
				.WithContentModule(new MetadataContentModule())
				.WithContentModule(new MethodInvocationContentModule())
				.WithContentModule(new TestProxyContentModule())
				.WithContentModule(new ISerializableModule(streamingContext, new FormatterConverter()))
				.WithContentModule(new MemberContentModule())
				.WithContentModule(new TraceContentModule())
				.WithContentModule(new DebugContentModule())
				.WithMemberModule(new InlineMemberModule())
				.WithMemberModule(new ReferencingMemberModule(typeof(int)))
				.WithMemberModule(new TraceMemberModule())
				.WithMemberModule(new DebugMemberModule());
		}

		protected virtual CompilerConfiguration Configure()
		{
			return GetDefaultConfig();
		}

		public void Write<T>(int implIndex, T input, Stream data, Stream metadata, Dictionary<object, int> refDict = null)
		{
			var serializer = this.Compilers[implIndex].To<TestOutputStream>().For<T>().Member.Generic;

			using (TestOutputStream outStream = new TestOutputStream(data, 1, true, metadata, refDict))
			{
				serializer(outStream, ref input);
			}
		}

		public T Read<T>(int implIndex, Stream data, Stream metadata, Dictionary<int, object> refDict = null)
		{
			var deserializer = this.Compilers[implIndex].From<TestInputStream>().For<T>().Deferred<EmptyDeferredState>().Member.Generic.UnDefer();
			
			T output = default;

			data.Position = 0;
			metadata.Position = 0;
			using (TestInputStream inStream = new TestInputStream(data, 1, true, metadata, refDict))
			{
				deserializer(inStream, out output);
				inStream.RunCallbacks();
			}

			return output;
		}

		public void Dump<T>(T input, Stream data, Stream metadata)
		{
			data.Position = 0;
			this.Output.WriteLine(input?.ToString() ?? "NULL");
			data.Dump(this.Output.WriteLine);
			if (metadata.Length == 0)
			{
				this.Output.WriteLine("No Metadata.");
			}
			else
			{
				metadata.Position = 0;
				this.Output.WriteLine("Metadata:");
				metadata.Dump(this.Output.WriteLine);
			}

			this.Output.WriteLine("");
		}

		private sealed class MemoryComparer : BaseTypeComparer
		{
			public MemoryComparer(RootComparer rootComparer) 
				: base(rootComparer)
			{
			}

			public override bool IsTypeMatch(Type type1, Type type2)
			{
				if (!type1.IsConstructedGenericType)
					return false;

				return type1.GetGenericTypeDefinition() == typeof(Memory<>) || type1.GetGenericTypeDefinition() == typeof(ReadOnlyMemory<>);
			}

			private static readonly MethodInfo compareMethod = typeof(MemoryComparer).GetMethod(nameof(compare), BindingFlags.Instance | BindingFlags.NonPublic);
			
			private bool compare<T>(CompareParms parms)
			{
				ReadOnlyMemory<T> memA;
				ReadOnlyMemory<T> memB;

				if (parms.Object1 is Memory<T> ma && parms.Object1 is Memory<T> mb)
				{
					memA = ma;
					memB = mb;
				}
				else
				{
					memA = (ReadOnlyMemory<T>)parms.Object1;
					memB = (ReadOnlyMemory<T>)parms.Object1;
				}

				ReadOnlySpan<T> sa = memA.Span;
				ReadOnlySpan<T> sb = memB.Span;

				if (sa.Length != sb.Length)
					return false;

				for (int i = 0; i < sa.Length; ++i)
				{
					this.RootComparer.Compare(new CompareParms()
					{
						Config = parms.Config,
						ParentObject1 = parms.Object1,
						Object1 = sa[i],
						Object1Type = typeof(T),
						ParentObject2 = parms.Object2,
						Object2 = sb[i],
						Object2Type = typeof(T),
						BreadCrumb = i.ToString(),
						Result = parms.Result,

					});
					if (!Equals(sa[i], sb[i]))
						return false;
				}

				return true;
			}

			public override void CompareType(CompareParms parms)
			{
				if (Equals(parms.Object1, parms.Object2))
					return;

				Type t1 = parms.Object1.GetType();
				Type t2 = parms.Object2.GetType();
				if (t1 != t2)
				{
					parms.Result.Differences.Add(new Difference()
					{
						PropertyName = "Type",
						Object1Value = t1.ToString(),
						Object2Value = t2.ToString(),
					});
					return;
				}

				Type elemType = t1.GetGenericArguments()[0];
				object res = compareMethod.MakeGenericMethod(elemType).Invoke(this, new object[] {parms});

				if (res is bool b && !b)
				{
					parms.Result.Differences.Add(new Difference()
					{
						PropertyName = "Value",
						Object1Value = parms.Object1.ToString(),
						Object2Value = parms.Object2.ToString(),
					});
				}
			}
		}

		public void Compare<T>(T input, T output)
		{
			ComparisonConfig config = new ComparisonConfig();
			config.CustomComparers.Add(new MemoryComparer(RootComparerFactory.GetRootComparer()));
			CompareLogic logic = new CompareLogic(config);
			
			ComparisonResult result = logic.Compare(input, output);

			if (!result.AreEqual)
			{
				Assert.True(result.AreEqual, result.DifferencesString);
			}

		}

		public T WriteAndRead<T>(T input, Dictionary<object, int> writeRefs = null, Dictionary<int, object> readRefs = null)
		{
			T output = default;

			for (int i = 0; i < this.Compilers.Length; ++i)
			{
				using (MemoryStream data = new MemoryStream())
				using (MemoryStream metadata = new MemoryStream())
				{
					this.Write(i, input, data, metadata, writeRefs);
					this.Dump(input, data, metadata);
					output = this.Read<T>(i, data, metadata, readRefs);
					this.Compare(input, output);
				}
			}

			return output;
		}
	}

	
	internal sealed class Analyzer
	{
		private readonly Stack<SerializationTraceInfo> stack;

		public Analyzer()
		{
			this.stack = new Stack<SerializationTraceInfo>();
		}

		public void OnBegin(in SerializationTraceInfo info)
		{
			//string ident = new string(' ', this.stack.Count * 4);
			//Console.WriteLine($"{ident}{member} {mode}");
			//Console.WriteLine($"{ident}{{");
			this.stack.Push(info);
		}

		public void OnEnd(in SerializationTraceInfo info)
		{
			SerializationTraceInfo stackInfo = this.stack.Pop();

			//string ident = new string(' ', this.stack.Count * 4);
			//Console.WriteLine($"{ident}}} {new string(' ', this.stack.Count * 2)}{member} {mode}");

			Assert.Equal(info.Member, stackInfo.Member);
			Assert.Equal(info.Direction, stackInfo.Direction);
			Assert.Equal(info.Mode, stackInfo.Mode);
		}
	}

	internal class TestOutputStream : BufferedOutputStream, IReferencingOutputStream<int>, IMetadataWriter, ISerializationTraceStream
	{
		private readonly Stream metadataStream;
		private readonly CompactMetadataWriter metadataWriter;
		private readonly Analyzer analyzer;

		public event EventHandler<Type> TypeWritten;
		private readonly DictionaryReferenceProvider<object> referenceProvider;

		public TestOutputStream(Stream destination, int initialCapacity, bool leaveOpen, Stream metadataStream, Dictionary<object, int> refDict = null)
			: base(destination, initialCapacity, leaveOpen)
		{
			if (refDict != null)
			{
				this.referenceProvider = new DictionaryReferenceProvider<object>(refDict);
			}
			else
			{
				this.referenceProvider = new DictionaryReferenceProvider<object>();
			}

			this.metadataStream = metadataStream;
			this.metadataWriter = new CompactMetadataWriter(CompactMetadataOptions.FullAssemblyNames);

			this.analyzer = new Analyzer();
		}

		public int GetOrAdd(object obj, out bool wasAdded) => this.referenceProvider.GetOrAdd(obj, out wasAdded);
		public void WriteIdentifier(int id) => this.WriteInt32(id);

		public override void WriteType(Type type)
		{
			this.TypeWritten?.Invoke(this, type);
			this.metadataWriter.WriteType(this.metadataStream, type);
		}

		public void WriteAssemblyName(AssemblyName name)
		{
			this.metadataWriter.WriteAssemblyName(this.metadataStream, name);
		}

		public void WriteMember(MemberInfo member)
		{
			this.metadataWriter.WriteMember(this.metadataStream, member);
		}

		public void OnBegin(in SerializationTraceInfo info)
		{
			this.analyzer.OnBegin(info);
		}

		public void OnEnd(in SerializationTraceInfo info)
		{
			this.analyzer.OnEnd(info);
		}
	}

	internal class TestInputStream : BufferedInputStream, IReferencingInputStream<int>, IDeserializationCallbackCollector, IMetadataReader, ISerializationTraceStream
	{
		private readonly Stream metadataStream;
		private readonly CompactMetadataReader metadataReader;
		private readonly Analyzer analyzer;

		private readonly IDeferredReferenceResolver<object, int> referenceResolver;
		private readonly List<IDeserializationCallback> callbacks;

		public TestInputStream(Stream source, int initialCapacity, bool leaveOpen, Stream metadataStream, Dictionary<int, object> refDict = null)
			: base(source, initialCapacity, leaveOpen)
		{
			if (refDict != null)
			{
				this.referenceResolver = new DeferredResolver<object, int>(new DictionaryReferenceResolver<object>(refDict));
			}
			else
			{
				this.referenceResolver = new DeferredResolver<object, int>(new DictionaryReferenceResolver<object>());
			}
			
			this.callbacks = new List<IDeserializationCallback>();

			this.metadataStream = metadataStream;
			this.metadataReader = new CompactMetadataReader(CompactMetadataOptions.FullAssemblyNames, Assembly.Load);

			this.analyzer = new Analyzer();
		}

		public DeferredReferencingResult Get<TValue, TState>(int identifier, DeferredCallback<TValue, TState> callback, TState state) => this.referenceResolver.Get(identifier, callback, state);
		public void Set(int identifier, object value) => this.referenceResolver.Set(identifier, value);
		public int ReadIdentifier() => this.ReadInt32();
		public void Add(IDeserializationCallback obj) => this.callbacks.Add(obj);
		public void RunCallbacks() => this.callbacks.ForEach(c => c.OnDeserialization(null));

		public override Type ReadType()
		{
			return this.metadataReader.ReadType(this.metadataStream);
		}

		public AssemblyName ReadAssemblyName()
		{
			return this.metadataReader.ReadAssemblyName(this.metadataStream);
		}

		public MemberInfo ReadMember()
		{
			return this.metadataReader.ReadMember(this.metadataStream);
		}

		public void OnBegin(in SerializationTraceInfo info)
		{
			this.analyzer.OnBegin(info);
		}

		public void OnEnd(in SerializationTraceInfo info)
		{
			this.analyzer.OnEnd(info);
		}
	}
}
