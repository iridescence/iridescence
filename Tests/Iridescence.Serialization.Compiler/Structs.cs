﻿using System;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	public class Structs : Common
	{
		public Structs(ITestOutputHelper stdout)
			: base(stdout)
		{
		}

		[Serializable]
		public struct EmptyStructType
		{

		}


		[Fact]
		public void EmptyStruct()
		{
			this.WriteAndRead<EmptyStructType>(new EmptyStructType());
		}

		[Serializable]
		public struct SimpleStructType
		{
			public int A;
			public float B;
			public bool C;
		}

		[Fact]
		public void SimpleStruct()
		{
			this.WriteAndRead<SimpleStructType>(new SimpleStructType());
			this.WriteAndRead<SimpleStructType>(new SimpleStructType() {A = 1337, B = 1.0f, C = true});
		}
	}
}
