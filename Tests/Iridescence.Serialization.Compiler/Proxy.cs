﻿using System;
using Iridescence.Serialization;
using Iridescence.Serialization.Compiler;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Serialization.Compiler
{
	/// <summary>
	/// TODO
	/// </summary>
	public class Proxy : Common
	{
		public Proxy(ITestOutputHelper stdout) : base(stdout)
		{
		}

		[Fact]
		public void A()
		{
			this.WriteAndRead(new ProxyTestA() {Val = 1337});
		}

		[Fact]
		public void B()
		{
			this.WriteAndRead(new ProxyTestB() {Val = 1337});
		}

		[Fact]
		public void C()
		{
			this.WriteAndRead(new ProxyTestC() {Val = 1337});
		}

		[Fact]
		public void D()
		{
			this.WriteAndRead(new ProxyTestD() {Val = 1337});

			new ProxyTestDProxy(new ProxyTestD());
		}
	}

	[Serializable]
	public class ProxyTestA
	{
		public int Val;
	}

	[Serializable]
	public class ProxyTestAProxy : IObjectSurrogate<ProxyTestA>
	{
		public long Val;

		public ProxyTestAProxy(ProxyTestA real)
		{
			this.Val = real.Val;
		}

		public ProxyTestA GetRealObject()
		{
			return new ProxyTestA
			{
				Val = (int)this.Val
			};
		}
	}

	[Serializable]
	public struct ProxyTestB
	{
		public int Val;
	}

	[Serializable]
	public class ProxyTestBProxy : IObjectSurrogate<ProxyTestB>
	{
		public long Val;

		public ProxyTestBProxy(ProxyTestB real)
		{
			this.Val = real.Val;
		}

		public ProxyTestB GetRealObject()
		{
			return new ProxyTestB
			{
				Val = (int)this.Val
			};
		}
	}


	[Serializable]
	public class ProxyTestC
	{
		public int Val;
	}

	[Serializable]
	public struct ProxyTestCProxy : IObjectSurrogate<ProxyTestC>
	{
		public long Val;

		public ProxyTestCProxy(ProxyTestC real)
		{
			this.Val = real.Val;
		}

		public ProxyTestC GetRealObject()
		{
			return new ProxyTestC
			{
				Val = (int)this.Val
			};
		}
	}


	[Serializable]
	public struct ProxyTestD
	{
		public int Val;
	}

	[Serializable]
	public struct ProxyTestDProxy : IObjectSurrogate<ProxyTestD>
	{
		public long Val;

		public ProxyTestDProxy(ProxyTestD real)
		{
			this.Val = real.Val;
		}

		public ProxyTestD GetRealObject()
		{
			return new ProxyTestD
			{
				Val = (int)this.Val
			};
		}
	}

	public class TestProxyContentModule : SurrogateContentModule
	{
		protected override SerializationMember GetSurrogate(SerializationMember member)
		{
			if (member.Type == typeof(ProxyTestA))
			{
				return member.ChangeType(typeof(ProxyTestAProxy));
			}
			if (member.Type == typeof(ProxyTestB))
			{
				return member.ChangeType(typeof(ProxyTestBProxy));
			}
			if (member.Type == typeof(ProxyTestC))
			{
				return member.ChangeType(typeof(ProxyTestCProxy));
			}
			if (member.Type == typeof(ProxyTestD))
			{
				return member.ChangeType(typeof(ProxyTestDProxy));
			}

			return null;
		}
	}
}
