﻿using System;
using System.Collections.Generic;
using Iridescence.Composition;
using Xunit;

namespace Tests.Iridescence.Composition
{
	public class CustomEvent : ContainerTests
	{
		[Export]
		public class A
		{

		}

		[Fact]
		public void EventTest()
		{
			CompositionContainer container = this.CreateContainer(typeof(A));
			EventCompositionContainer ev = new EventCompositionContainer(container, null);
			CompositionContract contract = null;

			ev.PartComposed += (c, obj) =>
			{
				contract = c;
			};

			A a = ev.GetExport<A>();

			Assert.NotNull(a);
			Assert.NotNull(contract);
			Assert.Equal(typeof(A), contract.Type);
		}


		[Fact]
		public void Chaining()
		{
			CompositionContainer container = this.CreateContainer(typeof(A));
			EventCompositionContainer ev1 = new EventCompositionContainer(container, null);
			EventCompositionContainer ev2 = new EventCompositionContainer(ev1, null);
			EventCompositionContainer ev3 = new EventCompositionContainer(ev2, null);
			CompositionContract contract1 = null;
			CompositionContract contract2 = null;
			CompositionContract contract3 = null;

			ev1.PartComposed += (c, obj) =>
			{
				contract1 = c;
				Assert.Null(contract2);
				Assert.Null(contract3);
			};

			ev2.PartComposed += (c, obj) =>
			{
				Assert.NotNull(contract1);
				contract2 = c;
				Assert.Null(contract3);
			};

			ev3.PartComposed += (c, obj) =>
			{
				Assert.NotNull(contract1);
				Assert.NotNull(contract2);
				contract3 = c;
			};

			A a = ev3.GetExport<A>();

			Assert.NotNull(a);
			Assert.NotNull(contract1);
			Assert.NotNull(contract2);
			Assert.NotNull(contract3);
		}

		[Fact]
		public void NonEventOverEvent()
		{
			CompositionContainer container = this.CreateContainer(typeof(A));
			EventCompositionContainer ev = new EventCompositionContainer(container, null);
			CompositionContainer evChild = new CompositionContainer(ev);
			CompositionContract contract = null;

			ev.PartComposed += (c, obj) =>
			{
				contract = c;
			};

			A a = evChild.GetExport<A>();

			Assert.NotNull(a);
			Assert.NotNull(contract);
			Assert.Equal(typeof(A), contract.Type);

		}
	}

	/// <summary>
	/// Handler for part composition.
	/// </summary>
	/// <param name="contract"></param>
	/// <param name="instance"></param>
	public delegate void PartComposedHandler(CompositionContract contract, object instance);

	/// <summary>
	/// Implements a <see cref="CompositionContainer"/> that fires an event for every part created through the composition context.
	/// </summary>
	public class EventCompositionContainer : CompositionContainer
	{
		public event PartComposedHandler PartComposed;

		public EventCompositionContainer(CompositionContainer parent, IEnumerable<IExportDescriptorProvider> descriptorProviders, Predicate<ExportDescriptorPromise> filter = null)
			: base(parent, descriptorProviders)
		{

		}

		protected override ExportDescriptor GetDescriptor(ExportDescriptorPromiseInfo promise, Func<ExportDescriptor> getDescriptor)
		{
			ExportDescriptor original = getDescriptor();

			return ExportDescriptor.Create((c, o) =>
			{
				object instance = original.Activator(c, o);
				this.OnPartComposed(promise.Contract, instance);
				return instance;
			}, original.Metadata);
		}

		protected virtual void OnPartComposed(CompositionContract contract, object instance)
		{
			this.PartComposed?.Invoke(contract, instance);
		}
	}
}
