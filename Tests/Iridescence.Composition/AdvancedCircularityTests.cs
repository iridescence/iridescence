﻿using Iridescence.Composition;
using Xunit;

namespace Tests.Iridescence.Composition
{
	public class AdvancedCircularityTests : ContainerTests
	{
		[Export, Shared]
		public class ClassA
		{
			[Import]
			public ClassB B { get; set; }
		}

		[Export]
		public class ClassB
		{
			[Import]
			public ClassC C { get; set; }
		}

		[Export]
		public class ClassC
		{
			[Import]
			public ClassD D { get; set;  }
		}

		[Export]
		public class ClassD
		{
			[Import]
			public ClassA A { get; set; }
		}

		[Fact]
		public void Test()
		{
			var container = this.CreateContainer(typeof(ClassA), typeof(ClassB), typeof(ClassC), typeof(ClassD));

			ClassA a = container.GetExport<ClassA>();

			Assert.NotNull(a);
			Assert.NotNull(a.B);
			Assert.NotNull(a.B.C);
			Assert.NotNull(a.B.C.D);
			Assert.NotNull(a.B.C.D.A);
			Assert.Same(a, a.B.C.D.A);
		}

	}
}
