﻿using Iridescence.Composition;
using Xunit;

namespace Tests.Iridescence.Composition
{
	public class LooseImportsTests : ContainerTests
	{
		[Export]
		public class Transaction { }

		public class SaveChangesAttribute
		{
			[Import]
			public Transaction Transaction { get; set; }
		}

		[Fact]
		public void SatisfyImportsSetsLooseImportsOnAttributedPart()
		{
			var container = CreateContainer(typeof(Transaction));
			var hasLoose = new SaveChangesAttribute();
			container.SatisfyImports(hasLoose);
			Assert.NotNull(hasLoose.Transaction);
		}
	}
}
