﻿using System;
using Iridescence.Composition;
using Xunit;

namespace Tests.Iridescence.Composition
{
	/// <summary>
	/// TODO
	/// </summary>
	public class InterfaceImplementerTests : ContainerTests
	{
		[Export]
		public class A
		{			
		}

		[Export]
		public class B : IDisposable
		{
			public bool IsDisposed;
			public void Dispose()
			{
				this.IsDisposed = true;
			}
		}

		[Export]
		public class C : IDisposable
		{
			public bool IsDisposed;
			public void Dispose()
			{
				this.IsDisposed = true;
			}
		}

		[Export]
		public class D
		{
		}


		public class BaseClass
		{
			[Import]
			public A A { get; set; }
		}

		[Export("asdf")]
		public class Consumer : BaseClass
		{
			public B B { get; }

			public C C { get; }

			[Import]
			public D D { get; set; }

			[ImportingConstructor]
			public Consumer(B b, C c)
			{
				this.B = b;
				this.C = c;
			}
		}

		[ExportFactory]
		public interface IFactoryWithParameter
		{
			BaseClass Create(C c);
		}

		[ExportFactory]
		public interface IExportFactoryWithParameter
		{
			Export<BaseClass> Create(C c);
		}

		[ExportFactory]
		public interface IFactoryWithHidingParameter
		{
			BaseClass Create([HideNonParameterExports] C c);
		}

		[ExportFactory]
		public interface IDerivedFactory : IFactoryWithParameter
		{

		}

		[ExportFactory]
		public interface IDerivedFactoryInvalid : IFactoryWithParameter
		{
			BaseClass Create2(C c);
		}

		[Fact]
		public void Simple()
		{
			var container = new CompositionContainerBuilder()
				.WithProvider(new TypedFactoryExportProvider(new[] { typeof(IFactoryWithParameter) }))
				.WithParts(typeof(A), typeof(B), typeof(C), typeof(D), typeof(Consumer), typeof(IFactoryWithParameter))
				.Build();

			Consumer obj = container.GetExport<Consumer>("asdf");
			Assert.NotNull(obj.A);
			Assert.NotNull(obj.B);
			Assert.NotNull(obj.C);
			Assert.NotNull(obj.D);
		}

		[Fact]
		public void InterfaceDoesNotGetReemitted()
		{
			var container = new CompositionContainerBuilder()
				.WithProvider(new TypedFactoryExportProvider(new[] { typeof(IFactoryWithParameter) }))
				.WithParts(typeof(A), typeof(B), typeof(D), typeof(Consumer), typeof(IFactoryWithParameter))
				.Build();

			IFactoryWithParameter factory1 = container.GetExport<IFactoryWithParameter>("asdf");
			IFactoryWithParameter factory2 = container.GetExport<IFactoryWithParameter>("asdf");

			Assert.NotSame(factory1, factory2);
			Assert.Same(factory1.GetType(), factory2.GetType());
		}

		[Fact]
		public void FactoryWithParameter()
		{
			var container = new CompositionContainerBuilder()
				.WithProvider(new TypedFactoryExportProvider(new[] { typeof(IFactoryWithParameter) }))
				.WithParts(typeof(A), typeof(B), typeof(D), typeof(Consumer), typeof(IFactoryWithParameter))
				.Build();

			IFactoryWithParameter factory = container.GetExport<IFactoryWithParameter>("asdf");

			C c1 = new C();
			Consumer obj1 = (Consumer)factory.Create(c1);
			Assert.NotNull(obj1.A);
			Assert.NotNull(obj1.B);
			Assert.NotNull(obj1.C);
			Assert.NotNull(obj1.D);
			Assert.Same(c1, obj1.C);

			C c2 = new C();
			Consumer obj2 = (Consumer)factory.Create(c2);
			Assert.NotNull(obj2.A);
			Assert.NotNull(obj2.B);
			Assert.NotNull(obj2.C);
			Assert.NotNull(obj2.D);
			Assert.Same(c2, obj2.C);

			Assert.NotSame(obj1, obj2);
		}

		[Fact]
		public void ParameterHiding()
		{
			var container = new CompositionContainerBuilder()
				.WithProvider(new TypedFactoryExportProvider(new[] { typeof(IFactoryWithParameter), typeof(IFactoryWithHidingParameter) }))
				.WithParts(typeof(A), typeof(B), typeof(C), typeof(D), typeof(Consumer), typeof(IFactoryWithParameter), typeof(IFactoryWithHidingParameter))
				.Build();
			
			Assert.Throws<DependencyException>(() =>
			{
				container.GetExport<IFactoryWithParameter>("asdf");
			});

			IFactoryWithHidingParameter factory = container.GetExport<IFactoryWithHidingParameter>("asdf");

			C c1 = new C();
			Consumer obj1 = (Consumer)factory.Create(c1);
			Assert.Same(c1, obj1.C);
		}

		[Fact]
		public void ExportFactoryWithParameter()
		{
			var container = new CompositionContainerBuilder()
				.WithProvider(new TypedFactoryExportProvider(new[] { typeof(IExportFactoryWithParameter) }))
				.WithParts(typeof(A), typeof(B), typeof(D), typeof(Consumer), typeof(IExportFactoryWithParameter))
				.Build();

			IExportFactoryWithParameter factory = container.GetExport<IExportFactoryWithParameter>("asdf");

			C c1 = new C();
			using (var ex = factory.Create(c1))
			{
				Consumer obj1 = (Consumer)ex.Value;
				Assert.NotNull(obj1.A);
				Assert.NotNull(obj1.B);
				Assert.NotNull(obj1.C);
				Assert.NotNull(obj1.D);
				Assert.Same(c1, obj1.C);
			}
		}

		[Fact]
		public void ExportFactoryWithParameterDoesNotDisposeParameters()
		{
			var container = new CompositionContainerBuilder()
				.WithProvider(new TypedFactoryExportProvider(new[] { typeof(IExportFactoryWithParameter) }))
				.WithParts(typeof(A), typeof(B), typeof(D), typeof(Consumer), typeof(IExportFactoryWithParameter))
				.Build();

			IExportFactoryWithParameter factory = container.GetExport<IExportFactoryWithParameter>("asdf");

			Consumer obj1;
			C c1 = new C();
			Assert.False(c1.IsDisposed);
			using (var ex = factory.Create(c1))
			{
				obj1 = (Consumer)ex.Value;
				Assert.False(obj1.B.IsDisposed);
				Assert.False(c1.IsDisposed);
			}

			Assert.True(obj1.B.IsDisposed);
			Assert.False(c1.IsDisposed);
		}

		[Fact]
		public void DerivedFactory()
		{
			var container = new CompositionContainerBuilder()
				.WithProvider(new TypedFactoryExportProvider(new[] { typeof(IDerivedFactory), typeof(IDerivedFactoryInvalid) }))
				.WithParts(typeof(A), typeof(B), typeof(D), typeof(Consumer))
				.Build();

			IDerivedFactory factory1 = container.GetExport<IDerivedFactory>("asdf");

			Assert.Throws<CompositionException>(() =>
			{
				IDerivedFactoryInvalid factory2 = container.GetExport<IDerivedFactoryInvalid>("asdf");
			});		

		}

		[Fact]
		public void InheritsFactoryBase()
		{
			var container = new CompositionContainerBuilder()
				.WithProvider(new TypedFactoryExportProvider(new[] { typeof(IFactoryWithParameter) }))
				.WithParts(typeof(A), typeof(B), typeof(D), typeof(Consumer), typeof(IFactoryWithParameter))
				.Build();

			IFactoryWithParameter factory = container.GetExport<IFactoryWithParameter>("asdf");

			EmittedFactoryBase factoryBase = factory as EmittedFactoryBase;
			Assert.NotNull(factoryBase);

			/*
			CompositionContract productContract = factoryBase.ProductContract;
			Assert.NotNull(productContract);
			Assert.Equal(typeof(BaseClass), productContract.Type);
			Assert.True(productContract.TryGetName(out string name));
			Assert.Equal("asdf", name);
			*/
		}
	}
}
