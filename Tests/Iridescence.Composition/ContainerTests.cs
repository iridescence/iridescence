﻿using System;
using Iridescence.Composition;

namespace Tests.Iridescence.Composition
{
	public class ContainerTests
	{
		public CompositionContainer CreateContainer(params Type[] types)
		{
			return new CompositionContainerBuilder()
				.WithProvider(new ImportManyExportProvider())
				.WithProvider(new LazyExportProvider())
				.WithProvider(new ExportFactoryProvider())
				.WithParts(types)
				.Build();
		}
	}
}
