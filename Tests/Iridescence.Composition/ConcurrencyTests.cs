// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
// See the LICENSE file in the project root for more information.

using System.Threading.Tasks;
using Iridescence.Composition;
using Xunit;

namespace Tests.Iridescence.Composition
{
    public class ConcurrencyTests : ContainerTests
    {
        [Export, Shared]
        public class PausesDuringActivation
        {
            public bool IsActivationComplete { get; set; }

            [OnImported]
            public void OnImportsSatisfied()
            {
                Task.Delay(200).Wait();
                IsActivationComplete = true;
            }
        }

        // This does not test the desired behaviour deterministically,
        // but is close enough to be repeatable at least on my machine :)
        [Fact]
        public void SharedInstancesAreNotVisibleUntilActivationCompletes()
        {
            var c = this.CreateContainer(typeof(PausesDuringActivation));
            Task.Run(() => c.GetExport<PausesDuringActivation>());
            Task.Delay(50).Wait();
            var pda = c.GetExport<PausesDuringActivation>();
            Assert.True(pda.IsActivationComplete);
        }
    }
}
