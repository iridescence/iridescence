﻿using System.Collections.Generic;
using Iridescence.Composition;
using Xunit;

namespace Tests.Iridescence.Composition
{
	public class PropertyExportTests : ContainerTests
	{
		public class Messenger
		{
			[Export]
			public string Message => "Helo!";
		}

		[Fact]
		public void CanExportProperty()
		{
			var cc = CreateContainer(typeof(Messenger));

			var x = cc.GetExport<string>();

			Assert.Equal("Helo!", x);
		}

		[Export, Shared]
		public class SelfObsessed
		{
			[Export]
			public SelfObsessed Self => this;
		}

		[Export]
		public class Selfless
		{
			[ImportMany]
			public IList<SelfObsessed> Values { get; set; }
		}

		[Fact]
		public void ExportedPropertiesShareTheSameSharedPartInstance()
		{
			var cc = CreateContainer(typeof(SelfObsessed), typeof(Selfless));
			var sl = cc.GetExport<Selfless>();
			Assert.Same(sl.Values[0], sl.Values[1]);
		}
	}
}
