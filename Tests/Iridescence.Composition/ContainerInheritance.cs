﻿using Iridescence.Composition;
using Xunit;

namespace Tests.Iridescence.Composition
{
	public class ContainerInheritance
	{
		[Export]
		public class A
		{

		}

		[Export]
		public class B
		{
			[Import]
			public A A { get; set; }
		}

		[Fact]
		public void Inherit()
		{
			CompositionContainerBuilder builder1 = new CompositionContainerBuilder().WithPart(typeof(A));
			CompositionContainer container1 = builder1.Build();

			Assert.NotNull(container1.GetExport<A>());
			Assert.False(container1.TryGetExport<B>(out _));

			CompositionContainerBuilder builder2 = new CompositionContainerBuilder().WithPart(typeof(B));
			CompositionContainer container2 = builder2.Build(container1);

			B b = container2.GetExport<B>();
			Assert.NotNull(b);
			Assert.NotNull(b.A);

			Assert.NotNull(container1.GetExport<A>());
			Assert.False(container1.TryGetExport<B>(out _));
		}
		
		[Fact]
		public void WithInstances()
		{
			CompositionContainerBuilder builder1 = new CompositionContainerBuilder().WithPart(typeof(B));
			CompositionContainer container1 = builder1.Build();

			Assert.Throws<DependencyException>(() =>
			{
				container1.GetExport<B>();
			});

			CompositionContainerBuilder builder2 = new CompositionContainerBuilder().WithInstance(new CompositionContract(typeof(A)), new A());
			CompositionContainer container2 = builder2.Build(container1);

			B b = container2.GetExport<B>();
			Assert.NotNull(b);
			Assert.NotNull(b.A);
		}
	}
}
