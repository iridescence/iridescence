﻿using Iridescence.Composition;
using Iridescence.FileSystem.Composition;
using Xunit;

namespace Tests.Iridescence.Composition
{
	public class RelativePaths : ContainerTests
	{
		[Export("/A/B/C/D/Export1")]
		public class Class1
		{
			[RelativeImport("Export2")]
			public Class2 A { get; set; }

			[RelativeImport("./Export2")]
			public Class2 B { get; set; }

		}

		[Export("/A/B/C/D/Export2")]
		public class Class2
		{
			[RelativeImport("../Export3")]
			public Class3 C { get; set; }
		}

		[Export("/A/B/C/Export3")]
		public class Class3
		{

		}

		[Fact]
		public void RelativeImports()
		{
			var cc = this.CreateContainer(typeof(Class1), typeof(Class2), typeof(Class3));
			var a = cc.GetExport<Class1>("/A/B/C/D/Export1");
			Assert.NotNull(a);
		}

		[Fact]
		public void RelativeContainer()
		{
			var cc = this.CreateContainer(typeof(Class1), typeof(Class2), typeof(Class3));
			var cc2 = cc.ChangeDirectory("/A/B/");
			var a = cc2.GetExport<Class1>("C/D/Export1");
			Assert.NotNull(a);
		}
	}
}
