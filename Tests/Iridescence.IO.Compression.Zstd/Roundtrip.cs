﻿using System;
using System.IO;
using System.Text;
using Iridescence;
using Iridescence.IO.Compression.Zstd;
using Tests.Iridescence.IO.Compression.LZ4;
using Xunit;

namespace Tests.Iridescence.Zstd
{
	public class Roundtrip
	{
		[Fact]
		public void RoundtripTest()
		{
			Random rng = new Random();

			using (FileStream fs = new FileStream("test.txt", FileMode.Create, FileAccess.Write))
			{
				byte[] data = new byte[1 * 1024 * 1024];
				Lorem.Fill(data, 0, data.Length);

				fs.Write(data, 0, data.Length);
			}

			using (FileStream input = new FileStream("test.txt", FileMode.Open, FileAccess.Read))
			using (FileStream output = new FileStream("test.txt.zst", FileMode.Create, FileAccess.Write))
			using (ZstdEncoderStream enc = new ZstdEncoderStream(output, 10))
			{
				byte[] temp = new byte[10000];
				for(;;)
				{
					int numBytes = rng.Next(1, temp.Length);

					int n = input.Read(temp, 0,numBytes);
					if (n == 0)
						break;

					enc.Write(temp, 0, n);
				}
			}

			using (FileStream input = new FileStream("test.txt.zst", FileMode.Open, FileAccess.Read))
			using (FileStream output = new FileStream("test.decoded.txt", FileMode.Create, FileAccess.Write))
			using (ZstdDecoderStream dec = new ZstdDecoderStream(input))
			{
				byte[] temp = new byte[10000];
				for (; ; )
				{
					int numBytes = rng.Next(1, temp.Length);

					int n = dec.Read(temp, 0, numBytes);
					if (n == 0)
						break;

					output.Write(temp, 0, n);
				}
			}
		}

		[Fact]
		public void Flush()
		{
			using (SlidingStream mem = new SlidingStream(1024))
			using (ZstdEncoderStream enc = new ZstdEncoderStream(mem, 10, leaveOpen: true))
			using (ZstdDecoderStream dec = new ZstdDecoderStream(mem, leaveOpen: true))
			{
				byte[] data1 = Encoding.ASCII.GetBytes("Test1234");
				byte[] data2 = Encoding.ASCII.GetBytes("More test");

				enc.Write(data1, 0, data1.Length);
				enc.Flush();
				enc.Flush();
				enc.Flush();
				enc.Flush();

				byte[] res1 = new byte[data1.Length];
				Assert.Equal(res1.Length, dec.Read(res1, 0, res1.Length));
				Assert.Equal(data1, res1);

				enc.Write(data2, 0, data2.Length);
				enc.Flush();

				byte[] res2 = new byte[data2.Length];
				Assert.Equal(res2.Length, dec.Read(res2, 0, res2.Length));
				Assert.Equal(data2, res2);
			}
		}
	}
}
