﻿using System;
using System.Linq;
using Iridescence.Audio;
using Xunit;

namespace Tests.Iridescence.Audio
{
	/// <summary>
	/// TODO
	/// </summary>
	public class LoopTest
	{
		[Fact]
		public void Empty()
		{
			var s = new MemorySignal<float>(ReadOnlyMemory<float>.Empty, 10).Loop();

			Assert.Equal(-1, s.Read(new float[10]));
			Assert.Equal(-1, s.Read(new float[10]));
			Assert.Equal(-1, s.Read(new float[10]));
		}

		[Fact]
		public void Loop()
		{
			var s = new MemorySignal<float>(new[] { 0.42f }, 10).Loop();

			float[] temp = new float[1];
			Assert.Equal(1, s.Read(temp));
			Assert.True(temp.All(v => v == 0.42f));

			temp = new float[10];
			Assert.Equal(10, s.Read(temp));
			Assert.True(temp.All(v => v == 0.42f));

			temp = new float[100];
			Assert.Equal(100, s.Read(temp));
			Assert.True(temp.All(v => v == 0.42f));
		}

		[Fact]
		public void Loop2()
		{
			var s = new MemorySignal<float>(Enumerable.Repeat(0.42f, 10).ToArray(), 10).Loop();

			float[] temp = new float[1];
			Assert.Equal(1, s.Read(temp));
			Assert.True(temp.All(v => v == 0.42f));

			temp = new float[10];
			Assert.Equal(10, s.Read(temp));
			Assert.True(temp.All(v => v == 0.42f));

			temp = new float[100];
			Assert.Equal(100, s.Read(temp));
			Assert.True(temp.All(v => v == 0.42f));
		}
	}
}
