﻿using System;
using Iridescence.Audio;
using Xunit;

namespace Tests.Iridescence.Audio
{
	public class JoinSource
	{
		[Theory]
		[InlineData(0)]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(3)]
		public void ReadEmpty(int numChannels)
		{
			SignalAdder source = new SignalAdder(100, OperatorSignalFlags.Default);

			for (int i = 0; i < numChannels; ++i)
			{
				source.Channels.Add(new MemorySignal<float>(ReadOnlyMemory<float>.Empty, 100));
			}

			Assert.Equal(0, source.Length);

			Assert.Equal(-1, source.Read(new float[10]));
			Assert.Equal(-1, source.Read(new float[10]));
			Assert.Equal(-1, source.Read(new float[10]));
		}

		[Theory]
		[InlineData(0)]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(3)]
		public void ReadEmptyContinuous(int numChannels)
		{
			SignalAdder source = new SignalAdder(100, OperatorSignalFlags.Continuous);

			for (int i = 0; i < numChannels; ++i)
			{
				source.Channels.Add(new MemorySignal<float>(ReadOnlyMemory<float>.Empty, 100));
			}

			Assert.Equal(0, source.Length);

			Assert.Equal(0, source.Read(new float[10]));
			Assert.Equal(0, source.Read(new float[10]));
			Assert.Equal(0, source.Read(new float[10]));
		}


		[Fact]
		public void RemoveWhenEnded()
		{
			SignalAdder source = new SignalAdder(100, OperatorSignalFlags.Default);

			source.Channels.Add(new MemorySignal<float>(new float[10], 100), OperatorChannelFlags.RemoveWhenEnded);
			source.Channels.Add(new MemorySignal<float>(new float[20], 100), OperatorChannelFlags.RemoveWhenEnded);
			source.Channels.Add(new MemorySignal<float>(new float[30], 100), OperatorChannelFlags.RemoveWhenEnded);

			Assert.Equal(30, source.Length);

			Assert.Equal(3, source.Channels.Count);

			Assert.Equal(5, source.Read(new float[5]));
			Assert.Equal(3, source.Channels.Count);

			Assert.Equal(5, source.Read(new float[5]));
			Assert.Equal(3, source.Channels.Count);

			Assert.Equal(5, source.Read(new float[5]));
			Assert.Equal(2, source.Channels.Count);

			Assert.Equal(10, source.Read(new float[10]));
			Assert.Single(source.Channels);

			Assert.Equal(5, source.Read(new float[10]));
			Assert.Empty(source.Channels);

			Assert.Equal(-1, source.Read(new float[10]));
			Assert.Empty(source.Channels);

			Assert.Equal(-1, source.Read(new float[10]));
			Assert.Empty(source.Channels);
		}

		[Fact]
		public void MixedRemoveWhenEnded()
		{
			SignalAdder source = new SignalAdder(100, OperatorSignalFlags.Default);

			source.Channels.Add(new MemorySignal<float>(new float[10], 100), OperatorChannelFlags.RemoveWhenEnded);
			source.Channels.Add(new MemorySignal<float>(new float[20], 100), OperatorChannelFlags.None);
			source.Channels.Add(new MemorySignal<float>(new float[30], 100), OperatorChannelFlags.RemoveWhenEnded);

			Assert.Equal(20, source.Length);

			Assert.Equal(3, source.Channels.Count);

			Assert.Equal(5, source.Read(new float[5]));
			Assert.Equal(3, source.Channels.Count);

			Assert.Equal(5, source.Read(new float[5]));
			Assert.Equal(3, source.Channels.Count);

			Assert.Equal(5, source.Read(new float[5]));
			Assert.Equal(2, source.Channels.Count);

			Assert.Equal(5, source.Read(new float[10]));
			Assert.Equal(2, source.Channels.Count);

			Assert.Equal(-1, source.Read(new float[10]));
			Assert.Equal(2, source.Channels.Count);

			Assert.Equal(-1, source.Read(new float[10]));
			Assert.Equal(2, source.Channels.Count);
		}
	}
}
