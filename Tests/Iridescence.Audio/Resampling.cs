﻿using System;
using System.Collections.Generic;
using Iridescence.Audio;
using Xunit;

namespace Tests.Iridescence.Audio
{
	public class Resampling
	{
		[Fact]
		public void From1To10Step1()
		{
			var source = new MemorySignal<Stereo<float>>(new Stereo<float>[]
			{
				(0.0f, 0.0f),
				(1.0f, 1.0f)
			}, 1);
			var resampler = new LinearResampler<Stereo<float>>(source, LinearResamplers.Resample, 10);
			Assert.Equal(11, resampler.Length);

			Stereo<float>[] temp = new Stereo<float>[100];
			for (int i = 0; i < resampler.Length; ++i)
			{
				Assert.Equal(1, resampler.Read(new Span<Stereo<float>>(temp, i, 1)));
			}

			for (int i = 0; i < 10; ++i)
			{
				Assert.Equal(-1, resampler.Read(temp));
			}

			for (int i = 0; i <= 10; ++i)
			{
				Assert.True(System.Math.Abs(i * 0.1 - temp[i].L) < 1e-5);
				Assert.True(System.Math.Abs(i * 0.1 - temp[i].R) < 1e-5);
			}
		}

		[Fact]
		public void From1To10Step3()
		{
			var source = new MemorySignal<Stereo<float>>(new Stereo<float>[]
			{
				(0.0f, 0.0f),
				(1.0f, 1.0f)
			}, 1);
			var resampler = new LinearResampler<Stereo<float>>(source, LinearResamplers.Resample, 10);
			Assert.Equal(11, resampler.Length);

			Stereo<float>[] temp = new Stereo<float>[3];
			Assert.Equal(3, resampler.Read(temp));
			Assert.Equal(3, resampler.Read(temp));
			Assert.Equal(3, resampler.Read(temp));
			Assert.Equal(2, resampler.Read(temp));

			for (int i = 0; i < 10; ++i)
			{
				Assert.Equal(-1, resampler.Read(temp));
			}
		}

		[Fact]
		public void From1To10Whole()
		{
			var source = new MemorySignal<Stereo<float>>(new Stereo<float>[]
			{
				(0.0f, 0.0f),
				(1.0f, 1.0f)
			}, 1);
			var resampler = new LinearResampler<Stereo<float>>(source, LinearResamplers.Resample, 10);
			Assert.Equal(11, resampler.Length);

			Stereo<float>[] temp = new Stereo<float>[100];
			Assert.Equal(11, resampler.Read(temp));

			for (int i = 0; i < 10; ++i)
			{
				Assert.Equal(-1, resampler.Read(temp));
			}
		}

		[Fact]
		public void From10To3()
		{
			var source = new MemorySignal<Stereo<float>>(new Stereo<float>[]
			{
				(0.0f, 0.0f),
				(0.1f, 0.1f),
				(0.2f, 0.2f),
				(0.3f, 0.3f),
				(0.4f, 0.4f),
				(0.5f, 0.5f),
				(0.6f, 0.6f),
				(0.7f, 0.7f),
				(0.8f, 0.8f),
				(0.9f, 0.9f),
				(1.0f, 1.0f),
			}, 10);
			var resampler = new LinearResampler<Stereo<float>>(source, LinearResamplers.Resample, 3);
			Assert.Equal(4, resampler.Length);

			Stereo<float>[] temp = new Stereo<float>[100];
			Assert.Equal(4, resampler.Read(temp));

			for (int i = 0; i < 10; ++i)
			{
				Assert.Equal(-1, resampler.Read(temp));
			}
		}

		[Fact]
		public void From10To11()
		{
			var source = new MemorySignal<Stereo<float>>(new Stereo<float>[]
			{
				(0.0f, 0.0f),
				(0.1f, 0.1f),
				(0.2f, 0.2f),
				(0.3f, 0.3f),
				(0.4f, 0.4f),
				(0.5f, 0.5f),
				(0.6f, 0.6f),
				(0.7f, 0.7f),
				(0.8f, 0.8f),
				(0.9f, 0.9f),
				(1.0f, 1.0f),
			}, 10);
			var resampler = new LinearResampler<Stereo<float>>(source, LinearResamplers.Resample, 11);
			//Assert.AreEqual(11, resampler.Length);

			Stereo<float>[] temp = new Stereo<float>[100];
			Assert.Equal(12, resampler.Read(temp));
			for (int i = 0; i < 10; ++i)
			{
				Assert.Equal(-1, resampler.Read(temp));
			}
		}

		[Fact]
		public void Empty()
		{
			var src = new MemorySignal<float>(ReadOnlyMemory<float>.Empty, 100);
			var resampler = new LinearResampler<float>(src, LinearResamplers.Resample, 200);

			Assert.Equal(0, resampler.Length);
			for (int i = 0; i < 10; ++i)
			{
				Assert.Equal(-1, resampler.Read(new float[10]));
			}
		}

		[Fact]
		public void Stalling()
		{
			var src = new CircularBufferSignal<float>(100, 100);
			src.Write(new float[10], false);
			var resampler = new LinearResampler<float>(src, LinearResamplers.Resample, 200);

			// 10 samples @ 100 Hz to 200 Hz => 19 samples

			Assert.Equal(4, resampler.Read(new float[4]));
			Assert.Equal(5, resampler.Read(new float[5]));
			Assert.Equal(6, resampler.Read(new float[6]));
			Assert.Equal(4, resampler.Read(new float[7]));

			for (int i = 0; i < 10; ++i)
			{
				Assert.Equal(0, resampler.Read(new float[10]));
			}

			src.Complete();

			for (int i = 0; i < 10; ++i)
			{
				Assert.Equal(-1, resampler.Read(new float[10]));
			}
		}

		private static void assertCloseEnough(ISignal<float> signal, float expected)
		{
			Span<float> temp = stackalloc float[1];
			Assert.Equal(1, signal.Read(temp));

			if (System.Math.Abs(temp[0] - expected) >= 1e-6)
				Assert.Equal(expected, temp[0]);
		}

		public static IEnumerable<object[]> CorrectnessData
		{
			get
			{
				Random rng = new Random(1337);
				for (int factor = 1; factor <= 20; ++factor)
				{
					for (int i = 1; i <= 5; ++i)
					{
						int sourceRate = rng.Next(1, 1000);
						yield return new object[] {factor,sourceRate};
					}
				}
			}
		}

		[Theory]
		[MemberData(nameof(CorrectnessData))]
		public void Correctness(int factor, int sourceRate)
		{
			var src = new MemorySignal<float>(new float[] {0.0f, 1.0f, 2.0f}, sourceRate);
			var resampler = new LinearResampler<float>(src, LinearResamplers.Resample, sourceRate * factor);

			for (int i = 0; i <= 2 * factor; ++i)
			{
				assertCloseEnough(resampler, (float)i / factor);
			}
			
			Assert.Equal(-1, resampler.Read(new float[1]));
			Assert.Equal(-1, resampler.Read(new float[2]));
		}
	}
}
