﻿using Iridescence.Audio;
using Xunit;

namespace Tests.Iridescence.Audio
{
	public class StereoMerger
	{
		#region Methods

		[Fact]
		public void ReadEmtpyLeftRight()
		{
			var left = new MemorySignal<float>(new float[0], 1000);
			var right = new MemorySignal<float>(new float[0], 1000);
			var stereo = new StereoSignalJoiner<float>(left, right);

			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
		}

		[Fact]
		public void ReadEmtpyLeft()
		{
			var left = new MemorySignal<float>(new float[0], 1000);
			var right = new MemorySignal<float>(new float[1000], 1000);
			var stereo = new StereoSignalJoiner<float>(left, right);

			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
		}
		
		[Fact]
		public void ReadEmtpyRight()
		{
			var left = new MemorySignal<float>(new float[0], 1000);
			var right = new MemorySignal<float>(new float[1000], 1000);
			var stereo = new StereoSignalJoiner<float>(left, right);

			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
		}
		
		[Fact]
		public void ReadLeftSmaller()
		{
			var left = new MemorySignal<float>(new float[10], 1000);
			var right = new MemorySignal<float>(new float[20], 1000);
			var stereo = new StereoSignalJoiner<float>(left, right);

			Assert.Equal(10, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
		}
		
		[Fact]
		public void ReadRightSmaller()
		{
			var left = new MemorySignal<float>(new float[20], 1000);
			var right = new MemorySignal<float>(new float[10], 1000);
			var stereo = new StereoSignalJoiner<float>(left, right);

			Assert.Equal(10, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
		}
		
		[Fact]
		public void Stalling()
		{
			var left = new CircularBufferSignal<float>(1000, 100);
			var right = new CircularBufferSignal<float>(1000, 100);
			var stereo = new StereoSignalJoiner<float>(left, right);

			left.Write(new float[10], false);
			right.Write(new float[20], false);
			Assert.Equal(10, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));

			left.Write(new float[20], false);
			Assert.Equal(10, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));

			right.Write(new float[20], false);
			Assert.Equal(10, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));

			left.Write(new float[10], false);
			Assert.Equal(10, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));

			left.Write(new float[10], false);
			right.Write(new float[10], false);
			Assert.Equal(10, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));

			left.Write(new float[10], false);
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(0, stereo.Read(new Stereo<float>[100]));

			left.Complete();
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));

			right.Complete();
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
			Assert.Equal(-1, stereo.Read(new Stereo<float>[100]));
		}
		
		#endregion
	}
}
