﻿using System;
using System.IO;
using Iridescence;
using Iridescence.Audio;
using Iridescence.Audio.IO;
using Xunit;

namespace Tests.Iridescence.Audio
{
	public class FlacTests
	{
		[Theory]
		[InlineData("Iridescence.Audio/TestAudio.flac", "Iridescence.Audio/TestAudio.s16le.raw")]
		public void TestReadingAndSeeking(string flacFile, string rawDataFile)
		{
			Stereo<SNorm16>[] rawData;
			using (FileStream rawDataStream = new FileStream(rawDataFile, FileMode.Open, FileAccess.Read))
			{
				rawData = new Stereo<SNorm16>[rawDataStream.Length / 4];
				using (Stream s = new SpanStream<Stereo<SNorm16>>(rawData, false, true))
				{
					rawDataStream.CopyTo(s);
				}
			}

			using (FileStream flacStream = new FileStream(flacFile, FileMode.Open, FileAccess.Read))
			{
				ISignal<Stereo<SNorm16>> flacInput = Flac.Open(flacStream, true).Convert<Stereo<SNorm16>>();

				Stereo<SNorm16>[] decodedData = new Stereo<SNorm16>[rawData.Length];

				for (int seekPos = 0; seekPos <= rawData.Length; seekPos += (seekPos < 100 || seekPos > rawData.Length - 100) ? 1 : 100)
				{
					Array.Clear(decodedData, 0, decodedData.Length);

					if (rawData.Length - seekPos == 0)
					{

					}

					flacInput.Position = seekPos;
					Assert.Equal(rawData.Length - seekPos, flacInput.Read(decodedData.ToSpan(seekPos)));

					for (int i = seekPos; i < decodedData.Length; ++i)
					{
						if (rawData[i] != decodedData[i])
						{
							Assert.Equal(rawData[i], decodedData[i]);
						}
					}
				}
			}
		}
	}
}
