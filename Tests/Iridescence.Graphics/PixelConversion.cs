﻿using System;
using System.Threading;
using Iridescence.Graphics;
using Xunit;

namespace Tests.Iridescence.Graphics
{
	public class PixelConversion
	{
		[Fact]
		public void ReadImage()
		{
			const int w = 256;
			const int h = 256;
			const int d = 4;
			byte[] rgbData = new byte[w * h * d * 3];

			for (int z = 0; z < d; ++z)
			{
				for (int y = 0; y < h; ++y)
				{
					for (int x = 0; x < w; ++x)
					{
						int offset = 3 * (z * w * h + y * w + x);
						rgbData[offset + 0] = (byte)x;
						rgbData[offset + 1] = (byte)y;
						rgbData[offset + 2] = (byte)(z * 85);
					}
				}
			}

			MemoryImage image = MemoryImage.Create(rgbData, new Format(FormatType.R8G8B8, FormatSemantic.UNorm,
				(ColorMappingChannel.Red, ColorMappingChannel.Green, ColorMappingChannel.Blue, ColorMappingChannel.None),
				(ColorMappingChannel.Red, ColorMappingChannel.Green, ColorMappingChannel.Blue, ColorMappingChannel.One)), w, h, d);

			RowCopy<byte, Color4> copy = image.GetRowColorCopy<Color4>();
			Span<Color4> temp = stackalloc Color4[w];

			PixelBox pixels = image.PixelBox;
			int sliceIndex = 0;
			foreach (PixelSlice slice in pixels.ZSlices)
			{
				int y = 0;
				foreach (PixelRow row in slice.Rows)
				{
					copy(ref row.Span[0], ref temp[0], 0, 0, row.Width);

					for (int x = 0; x < w; ++x)
					{
						Color4 expected;
						expected.R = x / 255.0f;
						expected.G = y / 255.0f;
						expected.B = sliceIndex / 3.0f;
						expected.A = 1.0f;

						Assert.Equal(expected, temp[x]);
					}

					++y;
				}

				++sliceIndex;
			}
		}

		[Fact]
		public void Bug()
		{
			bool exit = false;

			Thread t = new Thread(() =>
			{
				while (!exit)
				{
					GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true);
					GC.WaitForPendingFinalizers();
					Thread.Sleep(100);
				}
			});

			t.Start();

			object[] temp = new object[10_000];

			for (int i = 0; i < 100_000; ++i)
			{
				byte[] sb = new byte[1024];
				byte[] db = new byte[256];

				MemoryImage s = new MemoryImage(sb, new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm), 16, 16);
				MemoryImage d = new MemoryImage(db, new Format(FormatType.R8, FormatSemantic.UNorm,
					new ColorMapping(ColorMappingChannel.Alpha, ColorMappingChannel.None, ColorMappingChannel.None, ColorMappingChannel.None),
					new ColorMapping(ColorMappingChannel.One, ColorMappingChannel.One, ColorMappingChannel.One, ColorMappingChannel.Red)), 16, 16);

				s.CopyTo(d);

				temp[(2 * i + 0) % temp.Length] = s;
				temp[(2 * i + 1) % temp.Length] = d;

				if (i % 1000 == 0)
					Console.WriteLine(i);
			}

			exit = true;
			t.Join();

		}

		[Fact]
		public void Rgb8ToRgba8Swizzle()
		{
			byte[] s = new byte[3 * 3 * 3];

			s[0] = 255;
			s[1] = 0;
			s[2] = 0;

			s[3] = 0;
			s[4] = 255;
			s[5] = 0;

			s[6] = 0;
			s[7] = 0;
			s[8] = 255;

			s[9] = 255;
			s[10] = 255;
			s[11] = 0;

			s[12] = 0;
			s[13] = 255;
			s[14] = 255;

			s[15] = 255;
			s[16] = 0;
			s[17] = 255;

			s[18] = 0;
			s[19] = 0;
			s[20] = 0;

			s[21] = 127;
			s[22] = 127;
			s[23] = 127;

			s[24] = 255;
			s[25] = 255;
			s[26] = 255;

			MemoryImage si = new MemoryImage(s, new Format(FormatType.R8G8B8, FormatSemantic.UNorm), 3, 3);

			byte[] d = new byte[3 * 3 * 4];
			MemoryImage di = new MemoryImage(d, new Format(FormatType.R8G8B8A8, FormatSemantic.UNorm,
				(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.Alpha),
				(ColorMappingChannel.Blue, ColorMappingChannel.Green, ColorMappingChannel.Red, ColorMappingChannel.Alpha)), 3, 3);

			si.CopyTo(di);

			for (int i = 0; i < 3 * 3; ++i)
			{
				Assert.Equal(s[3 * i + 0], d[4 * i + 2]);
				Assert.Equal(s[3 * i + 1], d[4 * i + 1]);
				Assert.Equal(s[3 * i + 2], d[4 * i + 0]);
				Assert.Equal(255, d[4 * i + 3]);
			}
		}
	}
}

