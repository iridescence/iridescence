﻿using System.IO;
using Iridescence;
using Xunit;

namespace Tests.Iridescence
{
	public class StreamExtensions
	{
		#region 16

		[Fact]
		public void LE_S16()
		{
			byte[] data = new byte[4];
			byte[] expected = {0x22, 0x11, 0x44, 0x33};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteInt16L(0x1122);
				s.WriteInt16L(0x3344);

				Assert.Equal(4, s.Position);
				Assert.Equal(4, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x1122, s.ReadInt16L());
				Assert.Equal(0x3344, s.ReadInt16L());
				Assert.Equal(4, s.Position);

				s.Position = 4;
				Assert.False(s.TryReadInt16L(out _));

				s.Position = 3;
				Assert.Throws<EndOfStreamException>(() => s.TryReadInt16L(out _));

				s.Position = 2;
				Assert.True(s.TryReadInt16L(out short v));
				Assert.Equal(0x3344, v);
			}
		}

		[Fact]
		public void LE_U16()
		{
			byte[] data = new byte[4];
			byte[] expected = {0x22, 0x11, 0x44, 0x33};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteUInt16L(0x1122);
				s.WriteUInt16L(0x3344);

				Assert.Equal(4, s.Position);
				Assert.Equal(4, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x1122, s.ReadUInt16L());
				Assert.Equal(0x3344, s.ReadUInt16L());
				Assert.Equal(4, s.Position);

				s.Position = 4;
				Assert.False(s.TryReadUInt16L(out _));

				s.Position = 3;
				Assert.Throws<EndOfStreamException>(() => s.TryReadUInt16L(out _));

				s.Position = 2;
				Assert.True(s.TryReadUInt16L(out ushort v));
				Assert.Equal(0x3344, v);
			}
		}

		[Fact]
		public void BE_S16()
		{
			byte[] data = new byte[4];
			byte[] expected = {0x11, 0x22, 0x33, 0x44};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteInt16B(0x1122);
				s.WriteInt16B(0x3344);

				Assert.Equal(4, s.Position);
				Assert.Equal(4, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x1122, s.ReadInt16B());
				Assert.Equal(0x3344, s.ReadInt16B());
				Assert.Equal(4, s.Position);

				s.Position = 4;
				Assert.False(s.TryReadInt16B(out _));

				s.Position = 3;
				Assert.Throws<EndOfStreamException>(() => s.TryReadInt16B(out _));

				s.Position = 2;
				Assert.True(s.TryReadInt16B(out short v));
				Assert.Equal(0x3344, v);
			}
		}

		[Fact]
		public void BE_U16()
		{
			byte[] data = new byte[4];
			byte[] expected = {0x11, 0x22, 0x33, 0x44};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteUInt16B(0x1122);
				s.WriteUInt16B(0x3344);

				Assert.Equal(4, s.Position);
				Assert.Equal(4, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x1122, s.ReadUInt16B());
				Assert.Equal(0x3344, s.ReadUInt16B());
				Assert.Equal(4, s.Position);

				s.Position = 4;
				Assert.False(s.TryReadUInt16B(out _));

				s.Position = 3;
				Assert.Throws<EndOfStreamException>(() => s.TryReadUInt16B(out _));

				s.Position = 2;
				Assert.True(s.TryReadUInt16B(out ushort v));
				Assert.Equal(0x3344, v);
			}
		}

		#endregion

		#region 32

		[Fact]
		public void LE_S32()
		{
			byte[] data = new byte[8];
			byte[] expected = {0x44, 0x33, 0x22, 0x11, 0x88, 0x77, 0x66, 0x55};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteInt32L(0x11223344);
				s.WriteInt32L(0x55667788);

				Assert.Equal(8, s.Position);
				Assert.Equal(8, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x11223344, s.ReadInt32L());
				Assert.Equal(0x55667788, s.ReadInt32L());
				Assert.Equal(8, s.Position);

				s.Position = 8;
				Assert.False(s.TryReadInt32L(out _));

				s.Position = 7;
				Assert.Throws<EndOfStreamException>(() => s.TryReadInt32L(out _));

				s.Position = 6;
				Assert.Throws<EndOfStreamException>(() => s.TryReadInt32L(out _));

				s.Position = 5;
				Assert.Throws<EndOfStreamException>(() => s.TryReadInt32L(out _));

				s.Position = 4;
				Assert.True(s.TryReadInt32L(out int v));
				Assert.Equal(0x55667788, v);
			}
		}

		[Fact]
		public void LE_U32()
		{
			byte[] data = new byte[8];
			byte[] expected = {0x44, 0x33, 0x22, 0x11, 0x88, 0x77, 0x66, 0x55};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteUInt32L(0x11223344u);
				s.WriteUInt32L(0x55667788u);

				Assert.Equal(8, s.Position);
				Assert.Equal(8, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x11223344u, s.ReadUInt32L());
				Assert.Equal(0x55667788u, s.ReadUInt32L());
				Assert.Equal(8, s.Position);

				s.Position = 8;
				Assert.False(s.TryReadUInt32L(out _));

				s.Position = 7;
				Assert.Throws<EndOfStreamException>(() => s.TryReadUInt32L(out _));

				s.Position = 6;
				Assert.Throws<EndOfStreamException>(() => s.TryReadUInt32L(out _));

				s.Position = 5;
				Assert.Throws<EndOfStreamException>(() => s.TryReadUInt32L(out _));

				s.Position = 4;
				Assert.True(s.TryReadUInt32L(out uint v));
				Assert.Equal(0x55667788u, v);
			}
		}

		[Fact]
		public void BE_S32()
		{
			byte[] data = new byte[8];
			byte[] expected = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteInt32B(0x11223344);
				s.WriteInt32B(0x55667788);

				Assert.Equal(8, s.Position);
				Assert.Equal(8, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x11223344, s.ReadInt32B());
				Assert.Equal(0x55667788, s.ReadInt32B());
				Assert.Equal(8, s.Position);

				s.Position = 8;
				Assert.False(s.TryReadInt32B(out _));

				s.Position = 7;
				Assert.Throws<EndOfStreamException>(() => s.TryReadInt32B(out _));

				s.Position = 6;
				Assert.Throws<EndOfStreamException>(() => s.TryReadInt32B(out _));

				s.Position = 5;
				Assert.Throws<EndOfStreamException>(() => s.TryReadInt32B(out _));

				s.Position = 4;
				Assert.True(s.TryReadInt32B(out int v));
				Assert.Equal(0x55667788, v);
			}
		}

		[Fact]
		public void BE_U32()
		{
			byte[] data = new byte[8];
			byte[] expected = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteUInt32B(0x11223344u);
				s.WriteUInt32B(0x55667788u);

				Assert.Equal(8, s.Position);
				Assert.Equal(8, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x11223344u, s.ReadUInt32B());
				Assert.Equal(0x55667788u, s.ReadUInt32B());
				Assert.Equal(8, s.Position);

				s.Position = 8;
				Assert.False(s.TryReadUInt32B(out _));

				s.Position = 7;
				Assert.Throws<EndOfStreamException>(() => s.TryReadUInt32B(out _));

				s.Position = 6;
				Assert.Throws<EndOfStreamException>(() => s.TryReadUInt32B(out _));

				s.Position = 5;
				Assert.Throws<EndOfStreamException>(() => s.TryReadUInt32B(out _));

				s.Position = 4;
				Assert.True(s.TryReadUInt32B(out uint v));
				Assert.Equal(0x55667788u, v);
			}
		}

		#endregion

		#region 64

		[Fact]
		public void LE_S64()
		{
			byte[] data = new byte[16];
			byte[] expected =
			{
				0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11,
				0x00, 0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99
			};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteInt64L(0x1122334455667788L);
				s.WriteInt64L(unchecked((long)0x99AABBCCDDEEFF00L));

				Assert.Equal(16, s.Position);
				Assert.Equal(16, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x1122334455667788L, s.ReadInt64L());
				Assert.Equal(unchecked((long)0x99AABBCCDDEEFF00L), s.ReadInt64L());
				Assert.Equal(16, s.Position);

				s.Position = 16;
				Assert.False(s.TryReadInt64L(out _));

				for (int i = 15; i > 8; --i)
				{
					s.Position = i;
					Assert.Throws<EndOfStreamException>(() => s.TryReadInt64L(out _));
				}

				s.Position = 8;
				Assert.True(s.TryReadInt64L(out long v));
				Assert.Equal(unchecked((long)0x99AABBCCDDEEFF00L), v);
			}
		}

		[Fact]
		public void LE_U64()
		{
			byte[] data = new byte[16];
			byte[] expected =
			{
				0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11,
				0x00, 0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99
			};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteUInt64L(0x1122334455667788UL);
				s.WriteUInt64L(0x99AABBCCDDEEFF00UL);

				Assert.Equal(16, s.Position);
				Assert.Equal(16, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x1122334455667788UL, s.ReadUInt64L());
				Assert.Equal(0x99AABBCCDDEEFF00UL, s.ReadUInt64L());
				Assert.Equal(16, s.Position);

				s.Position = 16;
				Assert.False(s.TryReadUInt64L(out _));

				for (int i = 15; i > 8; --i)
				{
					s.Position = i;
					Assert.Throws<EndOfStreamException>(() => s.TryReadUInt64L(out _));
				}

				s.Position = 8;
				Assert.True(s.TryReadUInt64L(out ulong v));
				Assert.Equal(0x99AABBCCDDEEFF00UL, v);
			}
		}

		[Fact]
		public void BE_S64()
		{
			byte[] data = new byte[16];
			byte[] expected =
			{
				0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
				0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00
			};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteInt64B(0x1122334455667788L);
				s.WriteInt64B(unchecked((long)0x99AABBCCDDEEFF00L));

				Assert.Equal(16, s.Position);
				Assert.Equal(16, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x1122334455667788L, s.ReadInt64B());
				Assert.Equal(unchecked((long)0x99AABBCCDDEEFF00L), s.ReadInt64B());
				Assert.Equal(16, s.Position);

				s.Position = 16;
				Assert.False(s.TryReadInt64B(out _));

				for (int i = 15; i > 8; --i)
				{
					s.Position = i;
					Assert.Throws<EndOfStreamException>(() => s.TryReadInt64B(out _));
				}

				s.Position = 8;
				Assert.True(s.TryReadInt64B(out long v));
				Assert.Equal(unchecked((long)0x99AABBCCDDEEFF00L), v);
			}
		}

		[Fact]
		public void BE_U64()
		{
			byte[] data = new byte[16];
			byte[] expected =
			{
				0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
				0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00
			};

			using (MemoryStream s = new MemoryStream(data))
			{
				s.WriteUInt64B(0x1122334455667788UL);
				s.WriteUInt64B(0x99AABBCCDDEEFF00UL);

				Assert.Equal(16, s.Position);
				Assert.Equal(16, s.Length);
				Assert.Equal(expected, data);

				s.Position = 0;

				Assert.Equal(0x1122334455667788UL, s.ReadUInt64B());
				Assert.Equal(0x99AABBCCDDEEFF00UL, s.ReadUInt64B());
				Assert.Equal(16, s.Position);

				s.Position = 16;
				Assert.False(s.TryReadUInt64B(out _));

				for (int i = 15; i > 8; --i)
				{
					s.Position = i;
					Assert.Throws<EndOfStreamException>(() => s.TryReadUInt64B(out _));
				}

				s.Position = 8;
				Assert.True(s.TryReadUInt64B(out ulong v));
				Assert.Equal(0x99AABBCCDDEEFF00UL, v);
			}
		}

		#endregion
	}
}
