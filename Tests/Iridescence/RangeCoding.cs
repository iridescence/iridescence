﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Iridescence;
using Iridescence.RangeCoder;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence
{
	public class RangeCoding
	{
		private static readonly Random rng = new Random();
		private static readonly byte[] temp = new byte[4];

		private static uint randomBits()
		{
			rng.NextBytes(temp);
			return BitConverter.ToUInt32(temp, 0);
		}

		private readonly ITestOutputHelper output;

		public RangeCoding(ITestOutputHelper output)
		{
			this.output = output;
		}

		[Fact]
		public void Empty()
		{
			using (MemoryStream stream = new MemoryStream())
			{
				RangeEncoder e = new RangeEncoder(stream);

				e.Begin();
				e.End();

				stream.Position = 0;
				stream.Dump(s => this.output.WriteLine(s));

				stream.Position = 0;
				RangeDecoder d = new RangeDecoder(stream);

				d.Begin();
				d.End();

				Assert.Equal(stream.Length, stream.Position);
			}
		}

		public static IEnumerable<object[]> NumBits => Enumerable.Range(1, 31).Select(i => new object[] {i});

		private void verbatimRoundTrip(uint value, int numBits)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				RangeEncoder e = new RangeEncoder(stream);

				e.Begin();
				e.WriteVerbatim(value, numBits);
				e.End();

				stream.Position = 0;
				this.output.WriteLine($"Value: {value:X8}, Bits: {numBits}");
				stream.Dump(s => this.output.WriteLine(s));

				stream.Position = 0;
				RangeDecoder d = new RangeDecoder(stream);

				d.Begin();
				Assert.Equal(value, d.ReadVerbatim(numBits));
				d.End();

				Assert.Equal(stream.Length, stream.Position);
			}
		}

		[Theory]
		[MemberData(nameof(NumBits))]
		public void Bits(int numBits)
		{
			for (int pos = 0; pos <= numBits; ++pos)
			{
				this.verbatimRoundTrip(pos == 0 ? 0u : 1u << (pos - 1), numBits);
				this.verbatimRoundTrip((1u << pos) - 1, numBits);
			}

		}

		[Fact]
		public void Verbatim()
		{
			using (MemoryStream stream = new MemoryStream())
			{
				List<(uint, int)> values = new List<(uint, int)>();


				RangeEncoder e = new RangeEncoder(stream);

				e.Begin();

				for (int i = 0; i < 100_000; ++i)
				{
					uint value = randomBits();
					int numBits = rng.Next(1, 32);
					value &= (1u << numBits) - 1;

					values.Add((value, numBits));

					e.WriteVerbatim(value, numBits);
				}

				e.End();

				stream.Position = 0;
				RangeDecoder d = new RangeDecoder(stream);

				d.Begin();

				for (int i = 0; i < values.Count; ++i)
				{
					uint expectedValue = values[i].Item1;
					int numBits = values[i].Item2;

					uint v = d.ReadVerbatim(numBits);
					if(v != expectedValue)
					Assert.Equal(expectedValue, v);
				}

				d.End();

				Assert.Equal(stream.Length, stream.Position);
			}
		}

		[Fact]
		public void Ranges()
		{
			using (MemoryStream stream = new MemoryStream())
			{
				List<CodeRange> values = new List<CodeRange>();
				
				RangeEncoder e = new RangeEncoder(stream);

				e.Begin();

				for (int i = 0; i < 100_000; ++i)
				{
					int total = 2 + i;
					int low = rng.Next(0, total);
					int high = rng.Next(low + 1, total + 1);

					CodeRange r = new CodeRange(0, 1, (uint)total);
					values.Add(r);
					e.WriteRange(r);

					r = new CodeRange((uint)(total - 1), (uint)total, (uint)total);
					values.Add(r);
					e.WriteRange(r);

					r = new CodeRange((uint)low, (uint)high, (uint)total);
					values.Add(r);
					e.WriteRange(r);
				}

				e.End();

				stream.Position = 0;
				RangeDecoder d = new RangeDecoder(stream);

				d.Begin();

				for (int i = 0; i < values.Count; ++i)
				{
					uint current = d.GetCurrent(values[i].Total);

					Assert.True(values[i].IsInRange(current), $"{values[i].Low}-{values[i].High}/{values[i].Total} -> {current}");

					d.ReadRange(values[i]);
				}

				d.End();

				Assert.Equal(stream.Length, stream.Position);
			}
		}

		[Fact]
		public void MaxRange()
		{
			using (MemoryStream stream = new MemoryStream())
			{
				List<CodeRange> values = new List<CodeRange>();
				
				RangeEncoder e = new RangeEncoder(stream);

				e.Begin();

				for (int i = 0; i < 1_000; ++i)
				{
					int total = (int)CodeRange.MaxRange;
					int low = rng.Next(0, total);
					int high = rng.Next(low + 1, total + 1);

					CodeRange r = new CodeRange(0, 1, (uint)total);
					values.Add(r);
					e.WriteRange(r);

					r = new CodeRange((uint)(total - 1), (uint)total, (uint)total);
					values.Add(r);
					e.WriteRange(r);

					r = new CodeRange((uint)low, (uint)high, (uint)total);
					values.Add(r);
					e.WriteRange(r);
				}

				e.End();

				stream.Position = 0;
				RangeDecoder d = new RangeDecoder(stream);

				d.Begin();

				for (int i = 0; i < values.Count; ++i)
				{
					uint current = d.GetCurrent(values[i].Total);

					Assert.True(values[i].IsInRange(current), $"{values[i].Low}-{values[i].High}/{values[i].Total} -> {current}");

					d.ReadRange(values[i]);
				}

				d.End();

				Assert.Equal(stream.Length, stream.Position);
			}
		}
	}
}
