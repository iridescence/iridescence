﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using Iridescence;
using Iridescence.Memory;
using Xunit;

namespace Tests.Iridescence
{
	public class MemoryAccess
	{
		private void testMultiDim<TIn, TOut>(TIn[] input, TOut[] expected)
		{
			TIn[] input2 = new TIn[input.Length];
			TOut[] output = new TOut[input.Length];

			GCHandle handle1 = GCHandle.Alloc(input, GCHandleType.Pinned);
			GCHandle handle2 = GCHandle.Alloc(input2, GCHandleType.Pinned);
			GCHandle handle3 = GCHandle.Alloc(output, GCHandleType.Pinned);
			IntPtr inputAddr = handle1.AddrOfPinnedObject();
			IntPtr inputAddr2 = handle2.AddrOfPinnedObject();
			IntPtr outputAddr = handle3.AddrOfPinnedObject();

			Type inputType = typeof(TIn);
			int inputSize = Marshal.SizeOf<TIn>();

			Type outputType = typeof(TOut);
			int outputSize = Marshal.SizeOf<TOut>();

			{
				int count1 = 256;
				DataDescriptor inputDescriptor = new DataDescriptor(inputType,
					new DataDimension(DataDimensionFlags.CheckBounds, inputSize, 0, count1 - 1, "x"));
				DataDescriptor outputDescriptor = new DataDescriptor(outputType,
					new DataDimension(DataDimensionFlags.CheckBounds, outputSize, 0, count1 - 1, "x"));

				// Reader.
				var reader1 = inputDescriptor.CompileReader<Func<int, TOut>>(inputAddr);
				var reader2 = inputDescriptor.CompileReader<Func<IntPtr, int, TOut>>();

				Assert.Throws<IndexOutOfRangeException>(() => reader1(-1));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(count1));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, -1));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, count1));

				for (int x = 0; x < count1; ++x)
				{
					Assert.Equal(expected[x], reader1(x));
					Assert.Equal(expected[x], reader2(inputAddr, x));
				}

				// Writer.
				Array.Clear(input2, 0, input2.Length);
				var writer1 = inputDescriptor.CompileWriter<Action<int, TOut>>(inputAddr2);
				for (int x = 0; x < count1; ++x)
				{
					writer1(x, expected[x]);
					Assert.Equal(input[x], input2[x]);
				}

				Array.Clear(input2, 0, input2.Length);
				var writer2 = inputDescriptor.CompileWriter<Action<IntPtr, int, TOut>>();
				for (int x = 0; x < count1; ++x)
				{
					writer2(inputAddr2, x, expected[x]);
					Assert.Equal(input[x], input2[x]);
				}

				// Copy.
				var copy = inputDescriptor.CompileBlockCopy<Action<IntPtr, IntPtr, int, int, int>>(outputDescriptor);
				Array.Clear(output, 0, output.Length);
				copy(inputAddr, outputAddr, 0, 0, count1);
				Assert.Equal(expected, output);
			}

			{
				int count1 = 16;
				int count2 = 16;
				DataDescriptor inputDescriptor = new DataDescriptor(inputType,
					new DataDimension(DataDimensionFlags.CheckBounds, inputSize, 0, count1 - 1, "x"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * inputSize, 0, count2 - 1, "y"));

				DataDescriptor outputDescriptor = new DataDescriptor(outputType,
					new DataDimension(DataDimensionFlags.CheckBounds, outputSize, 0, count1 - 1, "x"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * outputSize, 0, count2 - 1, "y"));

				// Reader.
				var reader1 = inputDescriptor.CompileReader<Func<int, int, TOut>>(inputAddr);
				var reader2 = inputDescriptor.CompileReader<Func<IntPtr, int, int, TOut>>();

				Assert.Throws<IndexOutOfRangeException>(() => reader1(-1, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, -1));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(count1, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, count2));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, -1, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, -1));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, count1, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, count2));

				for (int x = 0; x < count1; ++x)
				{
					for (int y = 0; y < count2; ++y)
					{
						Assert.Equal(expected[count1 * y + x], reader1(x, y));
						Assert.Equal(expected[count1 * y + x], reader2(inputAddr, x, y));
					}
				}

				// Writer.
				Array.Clear(input2, 0, input2.Length);
				var writer1 = inputDescriptor.CompileWriter<Action<int, int, TOut>>(inputAddr2);
				for (int x = 0; x < count1; ++x)
				{
					for (int y = 0; y < count2; ++y)
					{
						writer1(x, y, expected[count1 * y + x]);
						Assert.Equal(input[count1 * y + x], input2[count1 * y + x]);
					}
				}

				Array.Clear(input2, 0, input2.Length);
				var writer2 = inputDescriptor.CompileWriter<Action<IntPtr, int, int, TOut>>();
				for (int x = 0; x < count1; ++x)
				{
					for (int y = 0; y < count2; ++y)
					{
						writer2(inputAddr2, x, y, expected[count1 * y + x]);
						Assert.Equal(input[count1 * y + x], input2[count1 * y + x]);
					}
				}

				// Copy.
				var copy = inputDescriptor.CompileBlockCopy<Action<IntPtr, IntPtr, int, int, int, int, int, int>>(outputDescriptor);
				Array.Clear(output, 0, output.Length);
				copy(inputAddr, outputAddr, 0, 0, 0, 0, count1, count2);
				Assert.Equal(expected, output);
			}

			{
				int count1 = 8;
				int count2 = 8;
				int count3 = 4;
				DataDescriptor inputDescriptor = new DataDescriptor(inputType,
					new DataDimension(DataDimensionFlags.CheckBounds, inputSize, 0, count1 - 1, "x"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * inputSize, 0, count2 - 1, "y"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * count2 * inputSize, 0, count3 - 1, "z"));

				DataDescriptor outputDescriptor = new DataDescriptor(outputType,
					new DataDimension(DataDimensionFlags.CheckBounds, outputSize, 0, count1 - 1, "x"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * outputSize, 0, count2 - 1, "y"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * count2 * outputSize, 0, count3 - 1, "z"));

				// Reader.
				var reader1 = inputDescriptor.CompileReader<Func<int, int, int, TOut>>(inputAddr);
				var reader2 = inputDescriptor.CompileReader<Func<IntPtr, int, int, int, TOut>>();

				Assert.Throws<IndexOutOfRangeException>(() => reader1(-1, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, -1, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, 0, -1));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(count1, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, count2, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, 0, count3));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, -1, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, -1, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, 0, -1));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, count1, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, count2, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, 0, count3));

				for (int x = 0; x < count1; ++x)
				{
					for (int y = 0; y < count2; ++y)
					{
						for (int z = 0; z < count3; ++z)
						{
							Assert.Equal(expected[count1 * count2 * z + count1 * y + x], reader1(x, y, z));
							Assert.Equal(expected[count1 * count2 * z + count1 * y + x], reader2(inputAddr, x, y, z));
						}
					}
				}

				// Writer.
				Array.Clear(input2, 0, input2.Length);
				var writer1 = inputDescriptor.CompileWriter<Action<int, int, int, TOut>>(inputAddr2);
				for (int x = 0; x < count1; ++x)
				{
					for (int y = 0; y < count2; ++y)
					{
						for (int z = 0; z < count3; ++z)
						{
							writer1(x, y, z, expected[count1 * count2 * z + count1 * y + x]);
							Assert.Equal(input[count1 * count2 * z + count1 * y + x], input2[count1 * count2 * z + count1 * y + x]);
						}
					}
				}

				Array.Clear(input2, 0, input2.Length);
				var writer2 = inputDescriptor.CompileWriter<Action<IntPtr, int, int, int, TOut>>();
				for (int x = 0; x < count1; ++x)
				{
					for (int y = 0; y < count2; ++y)
					{
						for (int z = 0; z < count3; ++z)
						{
							writer2(inputAddr2, x, y, z, expected[count1 * count2 * z + count1 * y + x]);
							Assert.Equal(input[count1 * count2 * z + count1 * y + x], input2[count1 * count2 * z + count1 * y + x]);
						}
					}
				}
				
				// Copy.
				var copy = inputDescriptor.CompileBlockCopy<Action<IntPtr, IntPtr, int, int, int, int, int, int, int, int, int>>(outputDescriptor);
				Array.Clear(output, 0, output.Length);
				copy(inputAddr, outputAddr, 0, 0, 0, 0, 0, 0, count1, count2, count3);
				Assert.Equal(expected, output);
			}

			{
				int count1 = 4;
				int count2 = 4;
				int count3 = 4;
				int count4 = 4;
				DataDescriptor inputDescriptor = new DataDescriptor(inputType,
					new DataDimension(DataDimensionFlags.CheckBounds, inputSize, 0, count1 - 1, "x"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * inputSize, 0, count2 - 1, "y"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * count2 * inputSize, 0, count3 - 1, "z"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * count2 * count3 * inputSize, 0, count4 - 1, "w"));

				DataDescriptor outputDescriptor = new DataDescriptor(outputType,
					new DataDimension(DataDimensionFlags.CheckBounds, outputSize, 0, count1 - 1, "x"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * outputSize, 0, count2 - 1, "y"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * count2 * outputSize, 0, count3 - 1, "z"),
					new DataDimension(DataDimensionFlags.CheckBounds, count1 * count2 * count3 * outputSize, 0, count4 - 1, "w"));

				var reader1 = inputDescriptor.CompileReader<Func<int, int, int, int, TOut>>(inputAddr);
				var reader2 = inputDescriptor.CompileReader<Func<IntPtr, int, int, int, int, TOut>>();

				Assert.Throws<IndexOutOfRangeException>(() => reader1(-1, 0, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, -1, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, 0, -1, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, 0, 0, -1));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(count1, 0, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, count2, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, 0, count3, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader1(0, 0, 0, count4));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, -1, 0, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, -1, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, 0, -1, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, 0, 0, -1));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, count1, 0, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, count2, 0, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, 0, count3, 0));
				Assert.Throws<IndexOutOfRangeException>(() => reader2(IntPtr.Zero, 0, 0, 0, count4));

				for (int x = 0; x < count1; ++x)
				{
					for (int y = 0; y < count2; ++y)
					{
						for (int z = 0; z < count3; ++z)
						{
							for (int w = 0; w < count4; ++w)
							{
								Assert.Equal(expected[count1 * count2 * count3 * w + count1 * count2 * z + count1 * y + x], reader1(x, y, z, w));
								Assert.Equal(expected[count1 * count2 * count3 * w + count1 * count2 * z + count1 * y + x], reader2(inputAddr, x, y, z, w));
							}
						}
					}
				}
			}

			handle1.Free();
			handle2.Free();
			handle3.Free();
		}

		[Fact]
		public void Int8_Int8()
		{
			byte[] input = Enumerable.Range(0, 256).Select(i => (byte)i).ToArray();
			byte[] output = (byte[])input.Clone();
			this.testMultiDim(input, output);
		}

		[Fact]
		public void Int8_Int16()
		{
			byte[] input = Enumerable.Range(0, 256).Select(i => (byte)i).ToArray();
			short[] output = input.Select(v => (short)v).ToArray();
			this.testMultiDim(input, output);
		}

		[Fact]
		public void UNorm8_Float()
		{
			UNorm8[] input = Enumerable.Range(0, 256).Select(i => UNorm8.FromRawValue((byte)i)).ToArray();
			float[] output = input.Select(v => (float)v).ToArray();
			this.testMultiDim(input, output);
		}

#pragma warning disable 649
		private struct Rgba<T>
		{
			public T R;
			public T G;
			public T B;
			public T A;
		}

		private struct Bgr<T>
		{
			public T B;
			public T G;
			public T R;
		}
#pragma warning restore 649

		private delegate void CopyTest2D(ref byte src, ref byte dest, int srcX, int srcY, int destX, int destY, int width, int height);

		[Fact]
		public void StructRemap()
		{
			const int w = 16;
			const int h = 16;
			Bgr<UNorm8>[] source = new Bgr<UNorm8>[w * h];
			Random rng = new Random();
			for (int i = 0; i < source.Length; ++i)
			{
				source[i].B = UNorm8.FromRawValue((byte)rng.Next(0, 256));
				source[i].G = UNorm8.FromRawValue((byte)rng.Next(0, 256));
				source[i].R = UNorm8.FromRawValue((byte)rng.Next(0, 256));
			}

			Span<byte> sourceBytes = new Span<Bgr<UNorm8>>(source).AsBytes();
			DataDescriptor sourceDesc = new DataDescriptor(typeof(Bgr<UNorm8>), new DataDimension(DataDimensionFlags.CheckBounds, 3, 0, 15), new DataDimension(DataDimensionFlags.CheckBounds, 3 * w, 0, 15));

			Rgba<float>[] dest = new Rgba<float>[w * h];
			Span<byte> destBytes = new Span<Rgba<float>>(dest).AsBytes();
			DataDescriptor destDesc = new DataDescriptor(typeof(Rgba<float>), new DataDimension(DataDimensionFlags.CheckBounds, 16, 0, 15), new DataDimension(DataDimensionFlags.CheckBounds, 16 * w, 0, 15));

			CopyTest2D copyFunc = sourceDesc.CompileBlockCopy<CopyTest2D>(destDesc,
				new StructMapConversion(
					new StructMapFieldEntry("R", "R"),
					new StructMapFieldEntry("G", "G"),
					new StructMapFieldEntry("B", "B"),
					new StructMapConstantEntry<float>(1.0f, "A")
				));

			copyFunc(ref sourceBytes[0], ref destBytes[0], 0, 0, 0, 0, w, h);

			for (int i = 0; i < source.Length; ++i)
			{
				Assert.Equal(source[i].R, dest[i].R);
				Assert.Equal(source[i].G, dest[i].G);
				Assert.Equal(source[i].B, dest[i].B);
				Assert.Equal(1.0f, dest[i].A);
			}
		}
	}
}
