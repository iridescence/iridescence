﻿using System.Text;
using Iridescence;
using Xunit;

namespace Tests.Iridescence
{
	public class SHA3Test
	{
		[Theory]
		[InlineData("6b4e03423667dbb73b6e15454f0eb1abd4597f9a1b078e3f5b5a6bc7", 224, "")]
		[InlineData("a7ffc6f8bf1ed76651c14756a061d662f580ff4de43b49fa82d80a4b80f8434a", 256, "")]
		[InlineData("0c63a75b845e4f7d01107d852e4c2485c51a50aaaa94fc61995e71bbee983a2ac3713831264adb47fb6bd1e058d5f004", 384, "")]
		[InlineData("a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26", 512, "")]
		[InlineData("dd2781f4c51bccdbe23e4d398b8a82261f585c278dbb4b84989fea70e76723a9", 256, "asdf")]
		public void Test(string hash, int bits, string input)
		{
			byte[] data = Encoding.UTF8.GetBytes(input);

			SHA3HashAlgorithm alg = new SHA3HashAlgorithm(bits / 8);
			Assert.Equal(hash, alg.ComputeHash(data).ToHexString());
		}
	}
}
