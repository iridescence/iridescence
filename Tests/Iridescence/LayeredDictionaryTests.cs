﻿using System;
using System.Collections.Generic;
using Iridescence;
using Xunit;

namespace Tests.Iridescence
{
	public class LayeredDictionaryTests
	{
		private static void compareDictionaries<TKey, TValue>(IReadOnlyDictionary<TKey, TValue> a, IReadOnlyDictionary<TKey, TValue> b)
		{
			Assert.Equal(a.Count, b.Count);

			foreach (KeyValuePair<TKey, TValue> p in a)
			{
				Assert.True(b.TryGetValue(p.Key, out TValue value));
				Assert.Equal(p.Value, value);
			}

			foreach (KeyValuePair<TKey, TValue> p in b)
			{
				Assert.True(a.TryGetValue(p.Key, out TValue value));
				Assert.Equal(value, p.Value);
			}
		}

		[Fact]
		public void Simple()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
				},
				new Dictionary<string, string>
				{
					{"b", "B"},
				}, LayeredDictionaryOptions.None);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"}
			}, ld);
		}

		[Fact]
		public void DuplicateKeysNonShadowed()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.None);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"}
			}, ld);
		}

		[Fact]
		public void DuplicateKeysShadowed()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.ShadowBaseKeys);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B2"},
				{"c", "C"}
			}, ld);
		}

		[Fact]
		public void AddDuplicateKeyNonShadowed()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.None);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"}
			}, ld);

			Assert.Throws<ArgumentException>(() => ld.Add("a", "A2"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"}
			}, ld);
			
			Assert.Throws<ArgumentException>(() => ld.Add("b", "B3"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"}
			}, ld);

			Assert.Throws<ArgumentException>(() => ld.Add("c", "C2"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"}
			}, ld);

			ld.Add("d", "D");
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"},
				{"d", "D"}
			}, ld);
		}

		[Fact]
		public void AddDuplicateKeyShadowed()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.ShadowBaseKeys);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B2"},
				{"c", "C"}
			}, ld);

			ld.Add("a", "A2");
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A2"},
				{"b", "B2"},
				{"c", "C"}
			}, ld);

			Assert.Throws<ArgumentException>(() => ld.Add("b", "B3"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A2"},
				{"b", "B2"},
				{"c", "C"}
			}, ld);

			ld.Add("d", "D");
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A2"},
				{"b", "B2"},
				{"c", "C"},
				{"d", "D"}
			}, ld);
		}

		[Fact]
		public void RemoveNonMasking()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.None);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"}
			}, ld);

			Assert.True(ld.Remove("c"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"}
			}, ld);

			Assert.False(ld.Remove("c"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"}
			}, ld);
			
			Assert.False(ld.Remove("b"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"}
			}, ld);

			Assert.False(ld.Remove("a"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"}
			}, ld);
		}

		[Fact]
		public void RemoveMasking()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.MaskBaseKeys);

			Assert.Equal(3, ld.Count);

			Assert.True(ld.Remove("c"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"}
			}, ld);

			Assert.False(ld.Remove("c"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"}
			}, ld);
			
			Assert.True(ld.Remove("b"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
			}, ld);

			Assert.False(ld.Remove("b"));
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
			}, ld);

			Assert.True(ld.Remove("a"));
			compareDictionaries(new Dictionary<string, string>
			{
			}, ld);

			Assert.False(ld.Remove("a"));
			compareDictionaries(new Dictionary<string, string>
			{
			}, ld);

			ld.Add("a", "A2");
			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A2"}
			}, ld);
		}

		[Fact]
		public void Clear()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.None);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"}
			}, ld);

			ld.Clear();

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
			}, ld);
		}

		[Fact]
		public void ClearShadowing()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.ShadowBaseKeys);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B2"},
				{"c", "C"}
			}, ld);

			ld.Clear();

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
			}, ld);
		}

		[Fact]
		public void ClearMasking()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.MaskBaseKeys);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"}
			}, ld);

			ld.Clear();

			compareDictionaries(new Dictionary<string, string>
			{

			}, ld);
		}

		[Fact]
		public void ClearMaskingNonExisting()
		{
			LayeredDictionary<string, string> ld = new LayeredDictionary<string, string>(
				new Dictionary<string, string>
				{
					{"a", "A"},
					{"b", "B"},
				},
				new Dictionary<string, string>
				{
					{"b", "B2"},
					{"c", "C"},
				}, LayeredDictionaryOptions.MaskNonExistingBaseKeys);

			compareDictionaries(new Dictionary<string, string>
			{
				{"a", "A"},
				{"b", "B"},
				{"c", "C"}
			}, ld);

			ld.Clear();

			compareDictionaries(new Dictionary<string, string>
			{

			}, ld);
		}
	}
}
