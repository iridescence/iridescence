﻿using System;
using System.Threading;
using Iridescence;
using Xunit;

namespace Tests.Iridescence
{
	public class WeakDelegates
	{
		class TestBindingTarget
		{

		}

		class TestClass
		{
			public int Invocations;

			public void M1()
			{
				Console.WriteLine("M1");
				++this.Invocations;
			}

			public string M2(int a, string b, DateTime c, Action d)
			{
				Console.WriteLine("M2");
				++this.Invocations;
				return $"M2{a}{b}";
			}
		}

		class LongLivedEventSource
		{
			public event EventHandler<EventArgs> TestEvent;

			public void FireEvent()
			{
				this.TestEvent?.Invoke(this, EventArgs.Empty);
			}
		}

		class ShortLivedEventHandler
		{
			public int Invocations;
			public static int Collected;

			public ShortLivedEventHandler(LongLivedEventSource src)
			{
				src.TestEvent += WeakDelegate.Create<EventHandler<EventArgs>>(this.Handle, (d, h) => src.TestEvent -= h);
			}

			~ShortLivedEventHandler()
			{
				Interlocked.Increment(ref Collected);
				Console.WriteLine("Collect");
			}

			public void Handle(object sender, EventArgs e)
			{
				++this.Invocations;
				Console.WriteLine("Handle");
			}
		}

		[Fact]
		public void Test1()
		{
			bool isDisposed = false;
			TestClass target = new TestClass();

			Action makeWeak()
			{
				TestBindingTarget binder = new TestBindingTarget();
				Action strongAction = target.M1;
				Action weakAction = WeakDelegate.Create(strongAction, (d, del) => { isDisposed = true; }, binder);

				weakAction();
				Assert.False(isDisposed);
				Assert.Equal(1, target.Invocations);

				GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);
				GC.WaitForPendingFinalizers();

				weakAction();
				Assert.False(isDisposed);
				Assert.Equal(2, target.Invocations);

				GC.KeepAlive(binder);
				return weakAction;
			}

			Action m = makeWeak();

			Thread.Sleep(1000);
			GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);
			GC.WaitForPendingFinalizers();
			
			m();
			Assert.True(isDisposed);
			Assert.Equal(2, target.Invocations);
		}

		[Fact]
		public void Test2()
		{
			bool isDisposed = false;
			TestClass target = new TestClass();

			Func<int, string, DateTime, Action, string> makeWeak()
			{
				TestBindingTarget binder = new TestBindingTarget();

				Func<int, string, DateTime, Action, string> strongFunc = target.M2;
				Func<int, string, DateTime, Action, string> weakFunc = WeakDelegate.Create(strongFunc, (d, del) => { isDisposed = true; }, binder);

				Assert.Equal("M21A", strongFunc(1, "A", DateTime.Now, () => { }));
				Assert.False(isDisposed);
				Assert.Equal(1, target.Invocations);

				GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);
				GC.WaitForPendingFinalizers();

				Assert.Equal("M22B", strongFunc(2, "B", DateTime.Now, () => { }));
				Assert.False(isDisposed);
				Assert.Equal(2, target.Invocations);

				GC.KeepAlive(binder);
				return weakFunc;
			}

			Func<int, string, DateTime, Action, string> m = makeWeak();

			GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);
			GC.WaitForPendingFinalizers();

			Assert.Null(m(3, "C", DateTime.Now, () => { }));
			Assert.True(isDisposed);
		}

		[Fact]
		public void EventTest()
		{
			LongLivedEventSource src = new LongLivedEventSource();

			ShortLivedEventHandler[] handlers = new ShortLivedEventHandler[4];
			ShortLivedEventHandler.Collected = 0;

			void makeHandlers()
			{
				for (int i = 0; i < handlers.Length; ++i)
					handlers[i] = new ShortLivedEventHandler(src);
			}

			void checkHandlers(int numInvocations)
			{
				for (int i = 0; i < handlers.Length; ++i)
				{
					if (handlers[i] != null)
						Assert.Equal(numInvocations, handlers[i].Invocations);
				}
			}

			makeHandlers();

			src.FireEvent();

			checkHandlers(1);

			for (int i = 0; i < handlers.Length; ++i)
			{
				handlers[i] = null;
				GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced, true, true);
				GC.WaitForPendingFinalizers();

				src.FireEvent();
				checkHandlers(2 + i);

				Assert.Equal(i + 1, ShortLivedEventHandler.Collected);
			}
		}
	}
}
