﻿using Iridescence.Math;
using Iridescence.Physics2D.Collision.Shapes;
using Iridescence.Physics2D.Dynamics;
using Iridescence.Physics2D.Dynamics.Controllers;
using Iridescence.Physics2D.Dynamics.Joints;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Physics2D
{
	public class Bugs
	{
		private readonly ITestOutputHelper output;

		public Bugs(ITestOutputHelper output)
		{
			this.output = output;
		}

		[Fact]
		public void Test()
		{
			World world = new World();
 
			BodyDescriptor bodyDescriptor = new BodyDescriptor(BodyType.Dynamic);
			FixtureDescriptor fixtureDescriptor = new FixtureDescriptor(new CircleShape(5f), 0.1f);
 
			DampingController damper = new DampingController(0.3f, 0.3f);
 
			for (int i = 0; i < 2; i++)
			{
				//bodyDescriptor.Position = new Vector2(i, i);
				Body body = world.Bodies.Add(bodyDescriptor);
				body.Fixtures.Add(fixtureDescriptor);
				body.Controllers.Add(damper);
			}
 
			foreach (var body in world.Bodies)
			{
				this.output.WriteLine($"Pre-Pos: {body.Position} | {body.LinearVelocity}");
			}
 
			for (int i = 0; i < 60; i++)
			{
				world.Step(1 / 60f, 6, 2);
			}
 
			foreach (var body in world.Bodies)
			{
				this.output.WriteLine($"Post-Pos: {body.Position} | {body.LinearVelocity}");
			}
		}

		[Fact]
		public void Test2()
		{
			World world = new World();
 
			BodyDescriptor bodyDescriptor = new BodyDescriptor(BodyType.Dynamic);
			FixtureDescriptor fixtureDescriptor = new FixtureDescriptor(new CircleShape(5f), 0.011f);
 
			for (int i = 0; i < 2; i++)
			{
				bodyDescriptor.Position = new Vector2(i * 12f, i * 12f);
				Body body = world.Bodies.Add(bodyDescriptor);
				body.Fixtures.Add(fixtureDescriptor);
			}
 
			DistanceJointDescriptor jointDesc = new DistanceJointDescriptor(world.Bodies[0], world.Bodies[1], world.Bodies[0].Position, world.Bodies[1].Position)
			{
				DampingRatio = 0.5f,
				CollideConnected = true,
				Frequency = 20f,
				Length = 1f
			};
 
			DistanceJoint joint = (DistanceJoint)world.Joints.Add(jointDesc);
			
			/*
			RopeJointDescriptor jointDesc = new RopeJointDescriptor(world.Bodies[0], world.Bodies[1], 1f, Vector2.Zero, Vector2.Zero)
			{
				CollideConnected = true,
			};
 
			Joint joint = world.Joints.Add(jointDesc);
			*/
 
			foreach (var body in world.Bodies)
			{
				this.output.WriteLine($"Pre-Pos: {body.Position} | {body.LinearVelocity}");
			}
 
			for (int i = 0; i < 60; i++)
			{
				world.Step(1 / 60f, 6, 2);
			}
 
			foreach (var body in world.Bodies)
			{
				this.output.WriteLine($"Post-Pos: {body.Position} | {body.LinearVelocity}");
			}
		}
	}
}
