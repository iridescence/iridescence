﻿using System;
using System.Linq;
using Iridescence.Physics2D.Dynamics;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Physics2D
{
	public class SimpleTest : TestBase
	{
		public SimpleTest(ITestOutputHelper output) : base(output)
		{
		}

		[Fact]
		public void Test()
		{
			const float worldSize = 100.0f;
			World w = CreateWorld(worldSize);

			Random rng = new Random(1337);

			for (int i = 0; i < 10; ++i)
			{
				float radius = (float)(1.0 + rng.NextDouble());
				Assert.Equal(i + 1, w.Bodies.Count);
				AddCircle(w, RandomVectorInWorld(rng, worldSize, radius + 1.0f), RandomDirectionVector(rng, 2.0f), radius);
				Assert.Equal(i + 2, w.Bodies.Count);
			}

			Step(w, 100);

			Assert.Equal(11, w.Bodies.Count);

			foreach (Body b in w.Bodies.Where(b => b.Type == BodyType.Dynamic))
			{
				Assert.True(b.Position.X >= 0.0f && b.Position.X <= worldSize && b.Position.Y >= 0.0f && b.Position.Y <= worldSize);
			}
		}

	}
}
