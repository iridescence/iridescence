﻿using System;
using Iridescence.Math;
using Iridescence.Physics2D.Collision.Shapes;
using Iridescence.Physics2D.Dynamics;
using Xunit.Abstractions;

namespace Tests.Iridescence.Physics2D
{
	public abstract class TestBase
	{
		public readonly ITestOutputHelper Output;

		public TestBase(ITestOutputHelper output)
		{
			this.Output = output;
		}

		public static World CreateWorld(float size = 100.0f)
		{
			World w = new World();

			if (size > 0.0f)
			{
				Body walls = w.Bodies.Add(new BodyDescriptor(BodyType.Static));

				Vector2[] wallVerts =
				{
					(0, 0),
					(size, 0),
					(size, size),
					(0, size),
				};

				walls.Fixtures.Add(new FixtureDescriptor(new ChainShape(wallVerts, true)));
			}

			return w;
		}

		public static Vector2 RandomDirectionVector(Random rng, float length)
		{
			double angle = rng.NextDouble() * ConstantsD.Pi2;
			double x = Utility.Cos(angle) * length;
			double y = Utility.Sin(angle) * length;
			return ((float)x, (float)y);
		}

		public static Vector2 RandomVectorInWorld(Random rng, float worldSize, float distanceToWalls)
		{
			Vector2 p;
			p.X = (float)(distanceToWalls + (rng.NextDouble() * (worldSize - distanceToWalls * 2.0)));
			p.Y = (float)(distanceToWalls + (rng.NextDouble() * (worldSize - distanceToWalls * 2.0)));
			return p;
		}

		public static Body AddCircle(World w, Vector2 pos, Vector2 vel, float radius, string name = null)
		{
			Body body = w.Bodies.Add(new BodyDescriptor(BodyType.Dynamic, new BodyState(pos, 0, vel, 0)));
			body.UserData = name;
			Fixture fix = body.Fixtures.Add(new FixtureDescriptor(new CircleShape(radius), 1.0f));
			fix.UserData = name;
			return body;
		}

		public static Body AddComplexCircle(World w, Vector2 pos, Vector2 vel, float radius, string name = null)
		{
			Body body = w.Bodies.Add(new BodyDescriptor(BodyType.Dynamic, new BodyState(pos, 0, vel, 0)));
			body.UserData = name;

			Fixture fix = body.Fixtures.Add(new FixtureDescriptor(new CircleShape(radius * 0.5f, (radius * 0.25f, 0.0f)), 1.0f));
			fix.UserData = name;

			fix = body.Fixtures.Add(new FixtureDescriptor(new CircleShape(radius * 0.5f, (radius * -0.25f, 0.0f)), 1.0f));
			fix.UserData = name;

			fix = body.Fixtures.Add(new FixtureDescriptor(new CircleShape(radius * 0.5f, (0.0f, radius * 0.25f)), 1.0f));
			fix.UserData = name;

			fix = body.Fixtures.Add(new FixtureDescriptor(new CircleShape(radius * 0.5f, (0.0f, radius * -0.25f)), 1.0f));
			fix.UserData = name;

			return body;
		}

		public static void Step(World w, int times, float dt = 0.01f)
		{
			for (int i = 0; i < times; ++i)
			{
				w.Step(dt, 8, 4);
			}
		}

	}
}
