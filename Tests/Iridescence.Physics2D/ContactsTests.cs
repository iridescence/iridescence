﻿using System;
using System.Collections.Generic;
using System.Linq;
using Iridescence.Physics2D.Dynamics;
using Iridescence.Physics2D.Dynamics.Contacts;
using Xunit;
using Xunit.Abstractions;

namespace Tests.Iridescence.Physics2D
{
	public class ContactsTests : TestBase
	{
		public ContactsTests(ITestOutputHelper output) : base(output)
		{
		}

		private void runTest(World w, ContactPreSolveEventHandler preSolve = null, ContactPostSolveEventHandler postSolve = null, ContactEventHandler begin = null, ContactEventHandler end = null)
		{
			List<Body> bodies = new List<Body>();
			int numContacts = 0;

			Random rng = new Random(1337);
			
			for (int i = 0; i < 100; ++i)
			{
				float radius = (float)(1.0 + rng.NextDouble());
				Body b = AddComplexCircle(w, RandomVectorInWorld(rng, 100.0f, radius + 1.0f), RandomDirectionVector(rng, 20.0f), radius, $"B{i}");
				bodies.Add(b);

				HashSet<Contact> contactsSolve = new HashSet<Contact>();
				HashSet<Contact> contactsBeginEnd = new HashSet<Contact>();
				b.Fixtures[0].ContactPreSolve += (c, m) =>
				{
					this.Output.WriteLine($"Contact Pre-Solve\t{c.FixtureA.UserData} {c.FixtureB.UserData}");
					Assert.True(contactsSolve.Add(c));
					preSolve?.Invoke(c, m);
				};
				b.Fixtures[0].ContactPostSolve += (Contact c, ref ContactImpulse impulse) =>
				{
					this.Output.WriteLine($"Contact Post-Solve\t{c.FixtureA.UserData} {c.FixtureB.UserData}");
					Assert.True(contactsSolve.Remove(c));
					postSolve?.Invoke(c, ref impulse);
				};
				b.Fixtures[0].BeginContact += c =>
				{
					this.Output.WriteLine($"Contact Begin\t{c.FixtureA.UserData} {c.FixtureB.UserData}");
					Assert.True(contactsBeginEnd.Add(c));
					++numContacts;
					begin?.Invoke(c);
				};
				b.Fixtures[0].EndContact += c =>
				{
					this.Output.WriteLine($"Contact End\t{c.FixtureA.UserData} {c.FixtureB.UserData}");
					Assert.True(contactsBeginEnd.Remove(c));
					--numContacts;
					end?.Invoke(c);
				};
				w.Bodies.CollectionChanged += (s, e) =>
				{
					if (e.RemovedItems.Any(item => item.Value == b))
					{
						//Assert.Empty(contactsSolve);
						Assert.Empty(contactsBeginEnd);
					}
				};
			}

			Step(w, 1000);

			foreach (Body b in bodies)
			{
				w.Bodies.Remove(b);
			}

			Assert.Equal(0, numContacts);
		}

		[Fact]
		public void Basic()
		{
			World w = CreateWorld();
			this.runTest(w, (c, m) => { }, (Contact c, ref ContactImpulse impulse) => { }, c => { }, c => { });
		}

		#region Remove Body

		[Fact]
		public void RemoveBodyAOnBeginContact()
		{
			World w = CreateWorld();
			this.runTest(w, begin: c =>
			{
				if (c.FixtureA.UserData is string)
					w.Bodies.Remove(c.FixtureA.Body);
			});
		}

		[Fact]
		public void RemoveBodyBOnBeginContact()
		{
			World w = CreateWorld();
			this.runTest(w, begin: c =>
			{
				if (c.FixtureB.UserData is string)
					w.Bodies.Remove(c.FixtureB.Body);
			});
		}

		[Fact]
		public void RemoveBodyAOnEndContact()
		{
			World w = CreateWorld();
			this.runTest(w, end: c =>
			{
				if (c.FixtureA.UserData is string)
					w.Bodies.Remove(c.FixtureA.Body);
			});
		}

		[Fact]
		public void RemoveBodyBOnEndContact()
		{
			World w = CreateWorld();
			this.runTest(w, end: c =>
			{
				if (c.FixtureB.UserData is string)
					w.Bodies.Remove(c.FixtureB.Body);
			});
		}

		[Fact]
		public void RemoveBodyAOnPreSolve()
		{
			World w = CreateWorld();
			this.runTest(w, preSolve: (c, _) =>
			{
				if (c.FixtureA.UserData is string)
					w.Bodies.Remove(c.FixtureA.Body);
			});
		}

		[Fact]
		public void RemoveBodyBOnPreSolve()
		{
			World w = CreateWorld();
			this.runTest(w, preSolve: (c, _) =>
			{
				if (c.FixtureB.UserData is string)
					w.Bodies.Remove(c.FixtureB.Body);
			});
		}

		[Fact]
		public void RemoveBodyAOnPostSolve()
		{
			World w = CreateWorld();
			this.runTest(w, postSolve: (Contact c, ref ContactImpulse impulse) =>
			{
				if (c.FixtureA.UserData is string)
					w.Bodies.Remove(c.FixtureA.Body);
			});
		}

		[Fact]
		public void RemoveBodyBOnPostSolve()
		{
			World w = CreateWorld();
			this.runTest(w, postSolve: (Contact c, ref ContactImpulse impulse) =>
			{
				if (c.FixtureB.UserData is string)
					w.Bodies.Remove(c.FixtureB.Body);
			});
		}

		#endregion
		
		#region Remove Fixture

		[Fact]
		public void RemoveFixtureAOnBeginContact()
		{
			World w = CreateWorld();
			this.runTest(w, begin: c =>
			{
				if (c.FixtureA.UserData is string)
					c.FixtureA.Body.Fixtures.Remove(c.FixtureA);
			});
		}

		[Fact]
		public void RemoveFixtureBOnBeginContact()
		{
			World w = CreateWorld();
			this.runTest(w, begin: c =>
			{
				if (c.FixtureB.UserData is string)
					c.FixtureB.Body.Fixtures.Remove(c.FixtureB);
			});
		}

		[Fact]
		public void RemoveFixtureAOnEndContact()
		{
			World w = CreateWorld();
			this.runTest(w, end: c =>
			{
				if (c.FixtureA.UserData is string)
					c.FixtureA.Body.Fixtures.Remove(c.FixtureA);
			});
		}

		[Fact]
		public void RemoveFixtureBOnEndContact()
		{
			World w = CreateWorld();
			this.runTest(w, end: c =>
			{
				if (c.FixtureB.UserData is string)
					c.FixtureB.Body.Fixtures.Remove(c.FixtureB);
			});
		}

		[Fact]
		public void RemoveFixtureAOnPreSolve()
		{
			World w = CreateWorld();
			this.runTest(w, preSolve: (c, _) =>
			{
				if (c.FixtureA.UserData is string)
					c.FixtureA.Body.Fixtures.Remove(c.FixtureA);
			});
		}

		[Fact]
		public void RemoveFixtureBOnPreSolve()
		{
			World w = CreateWorld();
			this.runTest(w, preSolve: (c, _) =>
			{
				if (c.FixtureB.UserData is string)
					c.FixtureB.Body.Fixtures.Remove(c.FixtureB);
			});
		}

		[Fact]
		public void RemoveFixtureAOnPostSolve()
		{
			World w = CreateWorld();
			this.runTest(w, postSolve: (Contact c, ref ContactImpulse impulse) =>
			{
				if (c.FixtureA.UserData is string)
					c.FixtureA.Body.Fixtures.Remove(c.FixtureA);
			});
		}

		[Fact]
		public void RemoveFixtureBOnPostSolve()
		{
			World w = CreateWorld();
			this.runTest(w, postSolve: (Contact c, ref ContactImpulse impulse) =>
			{
				if (c.FixtureB.UserData is string)
					c.FixtureB.Body.Fixtures.Remove(c.FixtureB);
			});
		}

		#endregion



	}
}
