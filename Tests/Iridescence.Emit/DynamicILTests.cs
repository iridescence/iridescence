﻿#if NETCOREAPP3_0

using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using System.Text;
using Iridescence.Emit;
using Iridescence.Emit.DynamicIL;
using Iridescence.Graphics.OpenGL;
using Xunit;

namespace Tests.Iridescence.Emit
{
	public class DynamicILTests
	{
		[Fact]
		public void SimpleTest()
		{
			DynamicILBuilder builder = DynamicILBuilder.Create(new MethodSignature(typeof(int), typeof(int), typeof(int)), "test");

			builder.LoadArgument(0);
			builder.LoadArgument(1);
			builder.Add();
			builder.Return();

			Func<int, int, int> f = (Func<int, int, int>)builder.CreateDelegate(typeof(Func<int, int, int>));
			
			Assert.Equal(10, f(4, 6));
		}

		[Fact]
		public void Locals()
		{
			DynamicILBuilder builder = DynamicILBuilder.Create(new MethodSignature(typeof(int), typeof(int), typeof(int)), "test");

			Local a = builder.DeclareLocal(typeof(int));
			Local b = builder.DeclareLocal(typeof(int));

			builder.LoadArgument(0);
			builder.StoreLocal(a);
			builder.LoadArgument(1);
			builder.StoreLocal(b);

			builder.LoadLocal(a);
			builder.LoadLocal(b);

			builder.Add();
			
			builder.Return();

			Func<int, int, int> f = (Func<int, int, int>)builder.CreateDelegate(typeof(Func<int, int, int>));
			
			Assert.Equal(10, f(4, 6));
		}
	}
}

#endif