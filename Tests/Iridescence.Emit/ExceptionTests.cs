﻿using System;
using Iridescence.Emit;
using Xunit;

namespace Tests.Iridescence.Emit
{
	public class ExceptionTests
	{
		[Fact]
		public void AddInvalid()
		{
			ILBuilder il = new DummyILBuilder(MethodSignature.Void);

			il.LoadConstant("a");
			il.LoadConstant("b");

			Assert.Throws<OperationVerificationException>(() =>
			{
				il.Add();
			});

			il.Pop();
			il.Pop();
			il.Return();		
			
		}

		[Fact]
		public void LoadInvalidArg()
		{
			ILBuilder il = new DummyILBuilder(MethodSignature.Void);

			Assert.Throws<ArgumentOutOfRangeException>(() =>
			{
				il.LoadArgument(-1);
			});

			Assert.Throws<ArgumentException>(() =>
			{
				il.LoadArgument(0);
			});

			Assert.Throws<ArgumentException>(() =>
			{
				il.LoadArgument(1);
			});
		}

		[Fact]
		public void LoadInvalidArg2()
		{
			ILBuilder il = new DummyILBuilder(new MethodSignature(typeof(void), typeof(int), typeof(int)));

			il.LoadArgument(0);
			il.LoadArgument(1);

			Assert.Throws<ArgumentOutOfRangeException>(() =>
			{
				il.LoadArgument(2);
			});
		}

	}
}
