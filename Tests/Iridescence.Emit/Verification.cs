﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using Iridescence;
using Iridescence.Emit;
using Iridescence.Emit.Disassembler;
using Xunit;
using Xunit.Abstractions;
using Label = Iridescence.Emit.Label;

namespace Tests.Iridescence.Emit
{
	public class VerificationAssemblies : IEnumerable<object[]>
	{
		private readonly IEnumerable<Assembly> assemblies;

		public VerificationAssemblies()
		{
			HashSet<Assembly> a = new HashSet<Assembly>();

			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				a.Add(assembly);
			}

			this.assemblies = a;
		}

		public IEnumerator<object[]> GetEnumerator()
		{
			foreach (Assembly assembly in this.assemblies)
			{
				yield return new object[] {assembly};
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}
	}

	public class Verification
	{
		private readonly ITestOutputHelper output;

		public Verification(ITestOutputHelper output)
		{
			this.output = output;
		}

		private void reassemble(MethodBase method, OperationCollection ops)
		{
			ILBuilder emit;

			List<Type> parameterTypes = new List<Type>(method.GetParameters().Select(p => p.ParameterType));

			if (!method.IsStatic)
			{
				Type thisType = method.DeclaringType;
				if (thisType.IsValueType)
					thisType = thisType.MakeByRefType();

				parameterTypes.Insert(0, thisType);
			}

			Type returnType = null;

			if (method is MethodInfo mi)
			{
				returnType = mi.ReturnType;
			}
			else if (method is ConstructorInfo ci)
			{
				returnType = typeof(void);
			}

			try
			{
				MethodSignature sig = new MethodSignature(returnType, parameterTypes.ToArray());
				emit = new DummyILBuilder(sig, method.CallingConvention, ILBuilderOptions.Default, method is ConstructorInfo, method.DeclaringType);
			}
			catch (NotSupportedException)
			{
				this.output.WriteLine(returnType?.GetDisplayName() ?? "NULL");
				throw;
			}

			Stack<ExceptionBlock> exceptionBlocks = new Stack<ExceptionBlock>();
			Stack<CatchBlock> catchBlocks = new Stack<CatchBlock>();
			Stack<FinallyBlock> finallyBlocks = new Stack<FinallyBlock>();
			Stack<FilterBlock> filterBlocks = new Stack<FilterBlock>();
			Dictionary<Local, Local> localTable = new Dictionary<Local, Local>();
			Dictionary<Label, Label> labelTable = new Dictionary<Label, Label>();
			Type constrainedType = null;
			bool? isVolatile = null;
			int? unaligned = null;
			bool readonlyPrefix = false;
			bool tailCallPrefix = false;

			try
			{
				foreach (Operation operation in ops)
				{
					switch (operation)
					{
						case CatchBlockStartOperation catchStart:
							catchBlocks.Push(emit.BeginCatchBlock(exceptionBlocks.Peek(), catchStart.ExceptionType == typeof(object) ? typeof(Exception) : catchStart.ExceptionType));
							break;

						case CatchBlockEndOperation catchEnd:
							emit.EndCatchBlock(catchBlocks.Pop());
							break;

						case DeclareLocalOperation declLocalOp:
							Local newLocal = emit.DeclareLocal(declLocalOp.Local.LocalType, declLocalOp.Local.Name, pinned: declLocalOp.Local.IsPinned);
							localTable.Add(declLocalOp.Local, newLocal);
							break;

						case DefineLabelOperation defLabelOp:
							Label newLabel = emit.DefineLabel(defLabelOp.Label.Name);
							labelTable.Add(defLabelOp.Label, newLabel);
							break;

						case DisposeLocalOperation disposeLocalOp:
							localTable[disposeLocalOp.Local].Dispose();
							break;

						case ExceptionBlockStartOperation exStart:
							exceptionBlocks.Push(emit.BeginExceptionBlock());
							break;

						case ExceptionBlockEndOperation exEnd:
							emit.EndExceptionBlock(exceptionBlocks.Pop());
							break;

						case FaultBlockStartOperation faultStart:
						case FaultBlockEndOperation faultEnd:
							throw new NotImplementedException();

						case FilterBlockStartOperation filterStart:
							filterBlocks.Push(emit.BeginFilterBlock(exceptionBlocks.Peek()));
							break;

						case FilterBlockEndOperation filterEnd:
							emit.EndFilterBlock(filterBlocks.Pop());
							break;

						case FinallyBlockStartOperation finallyStart:
							finallyBlocks.Push(emit.BeginFinallyBlock(exceptionBlocks.Peek()));
							break;

						case FinallyBlockEndOperation finallyEnd:
							emit.EndFinallyBlock(finallyBlocks.Pop());
							break;

						case MarkLabelOperation markLabel:
							emit.MarkLabel(labelTable[markLabel.Label]);
							break;

						case ReuseLocalOperation reuseLocalOp:
							Local reusedLocal = emit.DeclareLocal(reuseLocalOp.NewLocal.LocalType, reuseLocalOp.NewLocal.Name, pinned: reuseLocalOp.NewLocal.IsPinned);
							localTable.Add(reuseLocalOp.NewLocal, reusedLocal);
							break;

						case Instruction instr:
							OpCode op = instr.OpCode;

							Label labelOperand = null;
							if (instr.Operand is Label instrLabel)
								labelOperand = labelTable[instrLabel];

							Label[] switchOperand = null;
							if (instr.Operand is Label[] instrSwitch)
								switchOperand = instrSwitch.Select(s => labelTable[s]).ToArray();

							Local localOperand = null;
							if (instr.Operand is Local instrLocal)
								localOperand = localTable[instrLocal];

							Type typeOperand = instr.Operand as Type;
							InlineMethod? inlineMethodOperand = instr.Operand as InlineMethod?;
							MethodInfo methodOperand = instr.Operand as MethodInfo;
							ConstructorInfo constructorOperand = instr.Operand as ConstructorInfo;
							FieldInfo fieldOperand = instr.Operand as FieldInfo;

							string strOperand = instr.Operand as string;
							byte? u8Operand = null;
							short? i16Operand = null;
							ushort? u16Operand = null;
							int? i32Operand = null;
							long? i64Operand = null;
							float? f32Operand = null;
							double? f64Operand = null;
							ushort? argIndex = null;
							if (instr.Operand is sbyte i8)
							{
								argIndex = unchecked((ushort)i8);
							}

							if (instr.Operand is byte u8)
							{
								argIndex = u8;
								u8Operand = u8;
							}

							if (instr.Operand is short i16)
							{
								i16Operand = i16;
								argIndex = unchecked((ushort)i16);
							}
							else if (instr.Operand is ushort u16)
							{
								u16Operand = u16;
								argIndex = u16;
							}
							else if (instr.Operand is int i32)
							{
								i32Operand = i32;
							}
							else if (instr.Operand is long i64)
							{
								i64Operand = i64;
							}
							else if (instr.Operand is float f32)
							{
								f32Operand = f32;
							}
							else if (instr.Operand is double f64)
							{
								f64Operand = f64;
							}

							if (op == OpCodes.Add)
							{
								emit.Add();
							}
							else if (op == OpCodes.Add_Ovf)
							{
								emit.AddOverflow();
							}
							else if (op == OpCodes.Add_Ovf_Un)
							{
								emit.AddOverflowUnsigned();
							}
							else if (op == OpCodes.And)
							{
								emit.And();
							}
							else if (op == OpCodes.Arglist)
							{
								emit.ArgumentList();
							}
							else if (op == OpCodes.Beq)
							{
								emit.BranchIfEqual(labelOperand);
							}
							else if (op == OpCodes.Beq_S)
							{
								emit.BranchIfEqual(labelOperand);
							}
							else if (op == OpCodes.Bge)
							{
								emit.BranchIfGreaterOrEqual(labelOperand);
							}
							else if (op == OpCodes.Bge_S)
							{
								emit.BranchIfGreaterOrEqual(labelOperand);
							}
							else if (op == OpCodes.Bge_Un)
							{
								emit.BranchIfGreaterOrEqualUnsigned(labelOperand);
							}
							else if (op == OpCodes.Bge_Un_S)
							{
								emit.BranchIfGreaterOrEqualUnsigned(labelOperand);
							}
							else if (op == OpCodes.Bgt)
							{
								emit.BranchIfGreater(labelOperand);
							}
							else if (op == OpCodes.Bgt_S)
							{
								emit.BranchIfGreater(labelOperand);
							}
							else if (op == OpCodes.Bgt_Un)
							{
								emit.BranchIfGreaterUnsigned(labelOperand);
							}
							else if (op == OpCodes.Bgt_Un_S)
							{
								emit.BranchIfGreaterUnsigned(labelOperand);
							}
							else if (op == OpCodes.Ble)
							{
								emit.BranchIfLessOrEqual(labelOperand);
							}
							else if (op == OpCodes.Ble_S)
							{
								emit.BranchIfLessOrEqual(labelOperand);
							}
							else if (op == OpCodes.Ble_Un)
							{
								emit.BranchIfLessOrEqualUnsigned(labelOperand);
							}
							else if (op == OpCodes.Ble_Un_S)
							{
								emit.BranchIfLessOrEqualUnsigned(labelOperand);
							}
							else if (op == OpCodes.Blt)
							{
								emit.BranchIfLess(labelOperand);
							}
							else if (op == OpCodes.Blt_S)
							{
								emit.BranchIfLess(labelOperand);
							}
							else if (op == OpCodes.Blt_Un)
							{
								emit.BranchIfLessUnsigned(labelOperand);
							}
							else if (op == OpCodes.Blt_Un_S)
							{
								emit.BranchIfLessUnsigned(labelOperand);
							}
							else if (op == OpCodes.Bne_Un)
							{
								emit.BranchIfNotEqualUnsigned(labelOperand);
							}
							else if (op == OpCodes.Bne_Un_S)
							{
								emit.BranchIfNotEqualUnsigned(labelOperand);
							}
							else if (op == OpCodes.Box)
							{
								emit.Box(typeOperand);
							}
							else if (op == OpCodes.Br)
							{
								emit.Branch(labelOperand);
							}
							else if (op == OpCodes.Br_S)
							{
								emit.Branch(labelOperand);
							}
							else if (op == OpCodes.Break)
							{
								emit.Break();
							}
							else if (op == OpCodes.Brfalse)
							{
								emit.BranchIfFalse(labelOperand);
							}
							else if (op == OpCodes.Brfalse_S)
							{
								emit.BranchIfFalse(labelOperand);
							}
							else if (op == OpCodes.Brtrue)
							{
								emit.BranchIfTrue(labelOperand);
							}
							else if (op == OpCodes.Brtrue_S)
							{
								emit.BranchIfTrue(labelOperand);
							}
							else if (op == OpCodes.Call)
							{
								if (methodOperand != null)
								{
									emit.Call(methodOperand, tailCall: tailCallPrefix);
								}
								else if (constructorOperand != null)
								{
									emit.Call(constructorOperand, tailCall: tailCallPrefix);
								}
								else
								{
									if (inlineMethodOperand.Value.Method is MethodInfo miOperand)
									{
										emit.Call(miOperand, inlineMethodOperand.Value.ParameterTypes, tailCall: tailCallPrefix);
									}
									else if (inlineMethodOperand.Value.Method is ConstructorInfo ciOperand)
									{
										emit.Call(ciOperand, tailCall: tailCallPrefix);
									}
								}
							}
							else if (op == OpCodes.Calli)
							{
								throw new NotImplementedException();
							}
							else if (op == OpCodes.Callvirt)
							{
								emit.CallVirtual(methodOperand ?? (inlineMethodOperand.Value.Method as MethodInfo), constrainedType, tailCall: tailCallPrefix);
							}
							else if (op == OpCodes.Castclass)
							{
								emit.CastClass(typeOperand);
							}
							else if (op == OpCodes.Ceq)
							{
								emit.CompareEqual();
							}
							else if (op == OpCodes.Cgt)
							{
								emit.CompareGreaterThan();
							}
							else if (op == OpCodes.Cgt_Un)
							{
								emit.CompareGreaterThanUnsigned();
							}
							else if (op == OpCodes.Ckfinite)
							{
								emit.CheckFinite();
							}
							else if (op == OpCodes.Clt)
							{
								emit.CompareLessThan();
							}
							else if (op == OpCodes.Clt_Un)
							{
								emit.CompareLessThanUnsigned();
							}
							else if (op == OpCodes.Constrained)
							{
								constrainedType = typeOperand;
							}
							else if (op == OpCodes.Conv_I)
							{
								emit.ConvertToNativeInt();
							}
							else if (op == OpCodes.Conv_I1)
							{
								emit.ConvertToInt8();
							}
							else if (op == OpCodes.Conv_I2)
							{
								emit.ConvertToInt16();
							}
							else if (op == OpCodes.Conv_I4)
							{
								emit.ConvertToInt32();
							}
							else if (op == OpCodes.Conv_I8)
							{
								emit.ConvertToInt64();
							}
							else if (op == OpCodes.Conv_Ovf_I)
							{
								emit.ConvertToNativeIntOverflow();
							}
							else if (op == OpCodes.Conv_Ovf_I_Un)
							{
								emit.ConvertToNativeIntOverflowUnsigned();
							}
							else if (op == OpCodes.Conv_Ovf_I1)
							{
								emit.ConvertToInt8Overflow();
							}
							else if (op == OpCodes.Conv_Ovf_I1_Un)
							{
								emit.ConvertToInt8OverflowUnsigned();
							}
							else if (op == OpCodes.Conv_Ovf_I2)
							{
								emit.ConvertToInt16Overflow();
							}
							else if (op == OpCodes.Conv_Ovf_I2_Un)
							{
								emit.ConvertToInt16OverflowUnsigned();
							}
							else if (op == OpCodes.Conv_Ovf_I4)
							{
								emit.ConvertToInt32Overflow();
							}
							else if (op == OpCodes.Conv_Ovf_I4_Un)
							{
								emit.ConvertToInt32OverflowUnsigned();
							}
							else if (op == OpCodes.Conv_Ovf_I8)
							{
								emit.ConvertToInt64Overflow();
							}
							else if (op == OpCodes.Conv_Ovf_I8_Un)
							{
								emit.ConvertToInt64OverflowUnsigned();
							}
							else if (op == OpCodes.Conv_Ovf_U)
							{
								emit.ConvertToNativeUIntOverflow();
							}
							else if (op == OpCodes.Conv_Ovf_U_Un)
							{
								emit.ConvertToNativeUIntOverflowUnsigned();
							}
							else if (op == OpCodes.Conv_Ovf_U1)
							{
								emit.ConvertToUInt8Overflow();
							}
							else if (op == OpCodes.Conv_Ovf_U1_Un)
							{
								emit.ConvertToUInt8OverflowUnsigned();
							}
							else if (op == OpCodes.Conv_Ovf_U2)
							{
								emit.ConvertToUInt16Overflow();
							}
							else if (op == OpCodes.Conv_Ovf_U2_Un)
							{
								emit.ConvertToUInt16OverflowUnsigned();
							}
							else if (op == OpCodes.Conv_Ovf_U4)
							{
								emit.ConvertToUInt32Overflow();
							}
							else if (op == OpCodes.Conv_Ovf_U4_Un)
							{
								emit.ConvertToUInt32OverflowUnsigned();
							}
							else if (op == OpCodes.Conv_Ovf_U8)
							{
								emit.ConvertToUInt64Overflow();
							}
							else if (op == OpCodes.Conv_Ovf_U8_Un)
							{
								emit.ConvertToUInt64OverflowUnsigned();
							}
							else if (op == OpCodes.Conv_R_Un)
							{
								emit.ConvertToFloat32Unsigned();
							}
							else if (op == OpCodes.Conv_R4)
							{
								emit.ConvertToFloat32();
							}
							else if (op == OpCodes.Conv_R8)
							{
								emit.ConvertToFloat64();
							}
							else if (op == OpCodes.Conv_U)
							{
								emit.ConvertToNativeUInt();
							}
							else if (op == OpCodes.Conv_U1)
							{
								emit.ConvertToUInt8();
							}
							else if (op == OpCodes.Conv_U2)
							{
								emit.ConvertToUInt16();
							}
							else if (op == OpCodes.Conv_U4)
							{
								emit.ConvertToUInt32();
							}
							else if (op == OpCodes.Conv_U8)
							{
								emit.ConvertToUInt64();
							}
							else if (op == OpCodes.Cpblk)
							{
								emit.CopyBlock();
							}
							else if (op == OpCodes.Cpobj)
							{
								emit.CopyObject(typeOperand);
							}
							else if (op == OpCodes.Div)
							{
								emit.Divide();
							}
							else if (op == OpCodes.Div_Un)
							{
								emit.DivideUnsigned();
							}
							else if (op == OpCodes.Dup)
							{
								emit.Duplicate();
							}
							else if (op == OpCodes.Endfilter)
							{
								throw new InvalidOperationException();
							}
							else if (op == OpCodes.Endfinally)
							{
								throw new InvalidOperationException();
							}
							else if (op == OpCodes.Initblk)
							{
								emit.InitializeBlock();
							}
							else if (op == OpCodes.Initobj)
							{
								emit.InitializeObject(typeOperand);
							}
							else if (op == OpCodes.Isinst)
							{
								emit.IsInstance(typeOperand);
							}
							else if (op == OpCodes.Jmp)
							{
								emit.Jump(methodOperand);
							}
							else if (op == OpCodes.Ldarg)
							{
								emit.LoadArgument(argIndex.Value);
							}
							else if (op == OpCodes.Ldarg_0)
							{
								emit.LoadArgument(0);
							}
							else if (op == OpCodes.Ldarg_1)
							{
								emit.LoadArgument(1);
							}
							else if (op == OpCodes.Ldarg_2)
							{
								emit.LoadArgument(2);
							}
							else if (op == OpCodes.Ldarg_3)
							{
								emit.LoadArgument(3);
							}
							else if (op == OpCodes.Ldarg_S)
							{
								emit.LoadArgument(argIndex.Value);
							}
							else if (op == OpCodes.Ldarga)
							{
								emit.LoadArgumentAddress(argIndex.Value);
							}
							else if (op == OpCodes.Ldarga_S)
							{
								emit.LoadArgumentAddress(argIndex.Value);
							}
							else if (op == OpCodes.Ldc_I4)
							{
								emit.LoadConstant(i32Operand.Value);
							}
							else if (op == OpCodes.Ldc_I4_0)
							{
								emit.LoadConstant(0);
							}
							else if (op == OpCodes.Ldc_I4_1)
							{
								emit.LoadConstant(1);
							}
							else if (op == OpCodes.Ldc_I4_2)
							{
								emit.LoadConstant(2);
							}
							else if (op == OpCodes.Ldc_I4_3)
							{
								emit.LoadConstant(3);
							}
							else if (op == OpCodes.Ldc_I4_4)
							{
								emit.LoadConstant(4);
							}
							else if (op == OpCodes.Ldc_I4_5)
							{
								emit.LoadConstant(5);
							}
							else if (op == OpCodes.Ldc_I4_6)
							{
								emit.LoadConstant(6);
							}
							else if (op == OpCodes.Ldc_I4_7)
							{
								emit.LoadConstant(7);
							}
							else if (op == OpCodes.Ldc_I4_8)
							{
								emit.LoadConstant(8);
							}
							else if (op == OpCodes.Ldc_I4_M1)
							{
								emit.LoadConstant(-1);
							}
							else if (op == OpCodes.Ldc_I4_S)
							{
								emit.LoadConstant((int)u8Operand.Value);
							}
							else if (op == OpCodes.Ldc_I8)
							{
								emit.LoadConstant(i64Operand.Value);
							}
							else if (op == OpCodes.Ldc_R4)
							{
								emit.LoadConstant(f32Operand.Value);
							}
							else if (op == OpCodes.Ldc_R8)
							{
								emit.LoadConstant(f64Operand.Value);
							}
							else if (op == OpCodes.Ldelem)
							{
								emit.LoadElement(typeOperand);
							}
							else if (op == OpCodes.Ldelem_I)
							{
								emit.LoadElement<IntPtr>();
							}
							else if (op == OpCodes.Ldelem_I1)
							{
								emit.LoadElement<sbyte>();
							}
							else if (op == OpCodes.Ldelem_I2)
							{
								emit.LoadElement<short>();
							}
							else if (op == OpCodes.Ldelem_I4)
							{
								emit.LoadElement<int>();
							}
							else if (op == OpCodes.Ldelem_I8)
							{
								emit.LoadElement<long>();
							}
							else if (op == OpCodes.Ldelem_R4)
							{
								emit.LoadElement<float>();
							}
							else if (op == OpCodes.Ldelem_R8)
							{
								emit.LoadElement<double>();
							}
							else if (op == OpCodes.Ldelem_Ref)
							{
								emit.LoadElement<object>();
							}
							else if (op == OpCodes.Ldelem_U1)
							{
								emit.LoadElement<byte>();
							}
							else if (op == OpCodes.Ldelem_U2)
							{
								emit.LoadElement<ushort>();
							}
							else if (op == OpCodes.Ldelem_U4)
							{
								emit.LoadElement<uint>();
							}
							else if (op == OpCodes.Ldelema)
							{
								emit.LoadElementAddress(typeOperand, readonlyPrefix);
							}
							else if (op == OpCodes.Ldfld)
							{
								emit.LoadField(fieldOperand, isVolatile, unaligned);
							}
							else if (op == OpCodes.Ldflda)
							{
								emit.LoadFieldAddress(fieldOperand);
							}
							else if (op == OpCodes.Ldftn)
							{
								emit.LoadFunctionPointer(methodOperand ?? inlineMethodOperand.Value.Method as MethodInfo);
							}
							else if (op == OpCodes.Ldind_I)
							{
								emit.LoadIndirect<IntPtr>();
							}
							else if (op == OpCodes.Ldind_I1)
							{
								emit.LoadIndirect<sbyte>();
							}
							else if (op == OpCodes.Ldind_I2)
							{
								emit.LoadIndirect<short>();
							}
							else if (op == OpCodes.Ldind_I4)
							{
								emit.LoadIndirect<int>();
							}
							else if (op == OpCodes.Ldind_I8)
							{
								emit.LoadIndirect<long>();
							}
							else if (op == OpCodes.Ldind_R4)
							{
								emit.LoadIndirect<float>();
							}
							else if (op == OpCodes.Ldind_R8)
							{
								emit.LoadIndirect<double>();
							}
							else if (op == OpCodes.Ldind_Ref)
							{
								emit.LoadIndirect<object>();
							}
							else if (op == OpCodes.Ldind_U1)
							{
								emit.LoadIndirect<byte>();
							}
							else if (op == OpCodes.Ldind_U2)
							{
								emit.LoadIndirect<ushort>();
							}
							else if (op == OpCodes.Ldind_U4)
							{
								emit.LoadIndirect<uint>();
							}
							else if (op == OpCodes.Ldlen)
							{
								emit.LoadLength();
							}
							else if (op == OpCodes.Ldloc)
							{
								emit.LoadLocal(localOperand);
							}
							else if (op == OpCodes.Ldloc_0)
							{
								emit.LoadLocal(localTable.Keys.First(l => !l.IsDisposed && l.Index == 0));
							}
							else if (op == OpCodes.Ldloc_1)
							{
								emit.LoadLocal(localTable.Keys.First(l => !l.IsDisposed && l.Index == 1));
							}
							else if (op == OpCodes.Ldloc_2)
							{
								emit.LoadLocal(localTable.Keys.First(l => !l.IsDisposed && l.Index == 2));
							}
							else if (op == OpCodes.Ldloc_3)
							{
								emit.LoadLocal(localTable.Keys.First(l => !l.IsDisposed && l.Index == 3));
							}
							else if (op == OpCodes.Ldloc_S)
							{
								emit.LoadLocal(localOperand);
							}
							else if (op == OpCodes.Ldloca)
							{
								emit.LoadLocalAddress(localOperand);
							}
							else if (op == OpCodes.Ldloca_S)
							{
								emit.LoadLocalAddress(localOperand);
							}
							else if (op == OpCodes.Ldnull)
							{
								emit.LoadNull();
							}
							else if (op == OpCodes.Ldobj)
							{
								emit.LoadIndirect(typeOperand, isVolatile ?? false, unaligned);
							}
							else if (op == OpCodes.Ldsfld)
							{
								emit.LoadField(fieldOperand, isVolatile, unaligned);
							}
							else if (op == OpCodes.Ldsflda)
							{
								emit.LoadFieldAddress(fieldOperand);
							}
							else if (op == OpCodes.Ldstr)
							{
								emit.LoadConstant(strOperand);
							}
							else if (op == OpCodes.Ldtoken)
							{
								if (typeOperand != null)
									emit.LoadConstant(typeOperand);
								else if (methodOperand != null)
									emit.LoadConstant(methodOperand);
								else if (fieldOperand != null)
									emit.LoadConstant(fieldOperand);
							}
							else if (op == OpCodes.Ldvirtftn)
							{
								emit.LoadVirtualFunctionPointer(methodOperand ?? inlineMethodOperand.Value.Method as MethodInfo);
							}
							else if (op == OpCodes.Leave)
							{
								emit.Leave(labelOperand);
							}
							else if (op == OpCodes.Leave_S)
							{
								emit.Leave(labelOperand);
							}
							else if (op == OpCodes.Localloc)
							{
								emit.LocalAllocate();
							}
							else if (op == OpCodes.Mkrefany)
							{
								emit.MakeReferenceAny(typeOperand);
							}
							else if (op == OpCodes.Mul)
							{
								emit.Multiply();
							}
							else if (op == OpCodes.Mul_Ovf)
							{
								emit.MultiplyOverflow();
							}
							else if (op == OpCodes.Mul_Ovf_Un)
							{
								emit.MultiplyOverflowUnsigned();
							}
							else if (op == OpCodes.Neg)
							{
								emit.Negate();
							}
							else if (op == OpCodes.Newarr)
							{
								emit.NewArray(typeOperand);
							}
							else if (op == OpCodes.Newobj)
							{
								emit.NewObject(constructorOperand ?? (inlineMethodOperand.Value.Method as ConstructorInfo));
							}
							else if (op == OpCodes.Nop)
							{
								emit.Nop();
							}
							else if (op == OpCodes.Not)
							{
								emit.Not();
							}
							else if (op == OpCodes.Or)
							{
								emit.Or();
							}
							else if (op == OpCodes.Pop)
							{
								emit.Pop();
							}
							else if (op == OpCodes.Prefix1)
							{
								throw new NotSupportedException();
							}
							else if (op == OpCodes.Prefix2)
							{
								throw new NotSupportedException();
							}
							else if (op == OpCodes.Prefix3)
							{
								throw new NotSupportedException();
							}
							else if (op == OpCodes.Prefix4)
							{
								throw new NotSupportedException();
							}
							else if (op == OpCodes.Prefix5)
							{
								throw new NotSupportedException();
							}
							else if (op == OpCodes.Prefix6)
							{
								throw new NotSupportedException();
							}
							else if (op == OpCodes.Prefix7)
							{
								throw new NotSupportedException();
							}
							else if (op == OpCodes.Prefixref)
							{
								throw new NotSupportedException();
							}
							else if (op == OpCodes.Readonly)
							{
								readonlyPrefix = true;
							}
							else if (op == OpCodes.Refanytype)
							{
								emit.ReferenceAnyType();
							}
							else if (op == OpCodes.Refanyval)
							{
								emit.ReferenceAnyValue(typeOperand);
							}
							else if (op == OpCodes.Rem)
							{
								emit.Remainder();
							}
							else if (op == OpCodes.Rem_Un)
							{
								emit.RemainderUnsigned();
							}
							else if (op == OpCodes.Ret)
							{
								emit.Return();
							}
							else if (op == OpCodes.Rethrow)
							{
								emit.ReThrow();
							}
							else if (op == OpCodes.Shl)
							{
								emit.ShiftLeft();
							}
							else if (op == OpCodes.Shr)
							{
								emit.ShiftRight();
							}
							else if (op == OpCodes.Shr_Un)
							{
								emit.ShiftRightUnsigned();
							}
							else if (op == OpCodes.Sizeof)
							{
								emit.SizeOf(typeOperand);
							}
							else if (op == OpCodes.Starg)
							{
								emit.StoreArgument(argIndex.Value);
							}
							else if (op == OpCodes.Starg_S)
							{
								emit.StoreArgument(argIndex.Value);
							}
							else if (op == OpCodes.Stelem)
							{
								emit.StoreElement(typeOperand);
							}
							else if (op == OpCodes.Stelem_I)
							{
								emit.StoreElement<IntPtr>();
							}
							else if (op == OpCodes.Stelem_I1)
							{
								emit.StoreElement<sbyte>();
							}
							else if (op == OpCodes.Stelem_I2)
							{
								emit.StoreElement<short>();
							}
							else if (op == OpCodes.Stelem_I4)
							{
								emit.StoreElement<int>();
							}
							else if (op == OpCodes.Stelem_I8)
							{
								emit.StoreElement<long>();
							}
							else if (op == OpCodes.Stelem_R4)
							{
								emit.StoreElement<float>();
							}
							else if (op == OpCodes.Stelem_R8)
							{
								emit.StoreElement<double>();
							}
							else if (op == OpCodes.Stelem_Ref)
							{
								emit.StoreElement<object>();
							}
							else if (op == OpCodes.Stfld)
							{
								emit.StoreField(fieldOperand, isVolatile ?? false, unaligned);
							}
							else if (op == OpCodes.Stind_I)
							{
								emit.StoreIndirect<IntPtr>();
							}
							else if (op == OpCodes.Stind_I1)
							{
								emit.StoreIndirect<sbyte>();
							}
							else if (op == OpCodes.Stind_I2)
							{
								emit.StoreIndirect<short>();
							}
							else if (op == OpCodes.Stind_I4)
							{
								emit.StoreIndirect<int>();
							}
							else if (op == OpCodes.Stind_I8)
							{
								emit.StoreIndirect<long>();
							}
							else if (op == OpCodes.Stind_R4)
							{
								emit.StoreIndirect<float>();
							}
							else if (op == OpCodes.Stind_R8)
							{
								emit.StoreIndirect<double>();
							}
							else if (op == OpCodes.Stind_Ref)
							{
								emit.StoreIndirect<object>();
							}
							else if (op == OpCodes.Stloc)
							{
								emit.StoreLocal(localOperand);
							}
							else if (op == OpCodes.Stloc_0)
							{
								emit.StoreLocal(localTable.Keys.First(l => !l.IsDisposed && l.Index == 0));
							}
							else if (op == OpCodes.Stloc_1)
							{
								emit.StoreLocal(localTable.Keys.First(l => !l.IsDisposed && l.Index == 1));
							}
							else if (op == OpCodes.Stloc_2)
							{
								emit.StoreLocal(localTable.Keys.First(l => !l.IsDisposed && l.Index == 2));
							}
							else if (op == OpCodes.Stloc_3)
							{
								emit.StoreLocal(localTable.Keys.First(l => !l.IsDisposed && l.Index == 3));
							}
							else if (op == OpCodes.Stloc_S)
							{
								emit.StoreLocal(localOperand);
							}
							else if (op == OpCodes.Stobj)
							{
								emit.StoreIndirect(typeOperand, isVolatile ?? false, unaligned);
							}
							else if (op == OpCodes.Stsfld)
							{
								emit.StoreField(fieldOperand, isVolatile ?? false, unaligned);
							}
							else if (op == OpCodes.Sub)
							{
								emit.Subtract();
							}
							else if (op == OpCodes.Sub_Ovf)
							{
								emit.SubtractOverflow();
							}
							else if (op == OpCodes.Sub_Ovf_Un)
							{
								emit.SubtractOverflowUnsigned();
							}
							else if (op == OpCodes.Switch)
							{
								emit.Switch(switchOperand);
							}
							else if (op == OpCodes.Tailcall)
							{
								if (tailCallPrefix)
									throw new InvalidOperationException("TailCall prefix specified twice.");

								tailCallPrefix = true;
							}
							else if (op == OpCodes.Throw)
							{
								emit.Throw();
							}
							else if (op == OpCodes.Unaligned)
							{
								unaligned = u8Operand.Value;
							}
							else if (op == OpCodes.Unbox)
							{
								emit.Unbox(typeOperand);
							}
							else if (op == OpCodes.Unbox_Any)
							{
								emit.UnboxAny(typeOperand);
							}
							else if (op == OpCodes.Volatile)
							{
								isVolatile = true;
							}
							else if (op == OpCodes.Xor)
							{
								emit.Xor();
							}

							if (op.OpCodeType != OpCodeType.Prefix)
							{
								isVolatile = null;
								readonlyPrefix = false;
								unaligned = null;
								constrainedType = null;
								tailCallPrefix = false;
							}

							break;

					}
				}

			}
			catch (NotImplementedException)
			{

			}
			catch (Exception ex)
			{
				StringBuilder str = new StringBuilder();
				str.AppendLine(method.GetIlasmName());
				str.AppendLine("--- real method ------------------------");
				str.AppendLine(ops.ToString());
				str.AppendLine("--- reassembled ------------------------");
				str.AppendLine(emit.ToString());
				str.AppendLine("----------------------------------------");

				throw new Exception(str.ToString(), ex);
			}
		}

		[Theory]
		[ClassData(typeof(VerificationAssemblies))]
		public void VerifyAssembly(Assembly assembly)
		{
			List<MethodBase> methods = new List<MethodBase>();

			try
			{
				foreach (Type type in assembly.GetTypes())
				{
					foreach (MethodInfo method in type.GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static))
					{
						methods.Add(method);
					}

					foreach (ConstructorInfo ctor in type.GetConstructors(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static))
					{
						methods.Add(ctor);
					}
				}
			}
			catch (ReflectionTypeLoadException)
			{

			}

			foreach (MethodBase m in methods)
			{
				MethodBase method = m;
				if (method is MethodInfo mi && mi.IsGenericMethod && !mi.IsGenericMethodDefinition)
				{
					method = mi.GetGenericMethodDefinition();
				}

				OperationCollection ops;
				try
				{
					if (!method.TryDisassemble(out ops))
						continue;
				}
				catch (FileNotFoundException)
				{
					continue;
				}

				if (ops != null)
				{
					try
					{
						this.reassemble(method, ops);
					}
					catch (TypeLoadException tle) when (tle.TypeName.Contains("TypedReference"))
					{

					}
					catch (Exception e) when (e.InnerException is TypeLoadException tle && tle.TypeName.Contains("TypedReference"))
					{

					}
				}
			}
		}
	}
}

